({
    getLocalCurrency : function(component){
        var action = component.get("c.getLocalCurrency");
        action.setCallback(this, function(response){            
            component.set("v.LocalCurrency", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    getDeliveringOrganization : function(component){
        var action = component.get("c.getDeliveringOrganization");
        action.setCallback(this, function(response){            
            component.set("v.DeliveringOrganization", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },	
    
    getDeliveryLocation : function(component){
        var action = component.get("c.getDeliveryLocation");
        action.setCallback(this, function(response){            
            component.set("v.DeliveryLocation", response.getReturnValue());
            component.set("v.isCompLoading", false);
        });
        $A.enqueueAction(action);
    } ,
    
    getProductsOnload :function(component, page){
        var recordId = component.get("v.recordId"); 
        var oliItemId = component.get("v.DefaultproductId");
        var action = component.get("c.getProductsOnload");
        action.setParams({
            "opportunityID" : recordId,
            "oliItemId" : oliItemId,	
            
        });
        var productNameonLoad;
        action.setCallback(this, function(response){  
            var product;
            var productList = response.getReturnValue();
            var OnChangeService_productDataList =[]; 
            OnChangeService_productDataList.push(JSON.stringify(productList));
            var ProductNameArrayList = [];
            for(var i = 0;i < productList.length; i++){
                ProductNameArrayList.push(productList[i].Name);
            }
            component.set("v.ProductNameArrayList", ProductNameArrayList); 
        });
        $A.enqueueAction(action);
        
    },
    
    getProducts : function(component, page)
    {
        var page = page || 1;
        var recordId = component.get("v.recordId");
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");
        if(IsOnChangeServiceLine == true || IsOnChangeNatureOfWork == true || IsOnChangeProductName == true )		
        {
            console.log(':--ServiceLine_Value--:'+ServiceLine_Value);
            var action = component.get("c.getProductsOnChange");
            action.setParams({
                "opportunityID" : recordId,
                "SelectedServiceLine" : ServiceLine_Value,	
                "SelectedNatureOfWork" : NatureOfWork_Value,
                "SelectedProductName" : ProductName_Value,
            });
            action.setCallback(this, function(response) { 
                var state = response.getState();
                if(state === 'SUCCESS')
                {   
                    component.set("v.isFilterLoading", false);
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var OnChangeService_productDataList =[]; 
                    OnChangeService_productDataList.push(JSON.stringify(productList));
                    component.set("v.OnChangeService_productDataList", OnChangeService_productDataList); 
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.OnChangeService_productDataList");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name);  
                        var pro = {
                            "ID" : product.Id,
                            "Name" : product.Name
                        }
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c);
                        ServiceLineArrayList.push(product.Service_Line__c); 
                    }
                    
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i]);
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i]);
                        }
                    }
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }	                   
                    
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--') 
                    {	
                        component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }	
                    else if(IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value == '--None--') 
                    { 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }
                        else if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value != '--None--') 
                        { 
                            component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList);
                        }
                            else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value == '--None--') 
                            { 
                                component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                            }
                                else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value != '--None--' && ProductName_Value == '--None--') 
                                { 
                                    component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                                }
                                    else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value != '--None--') 
                                    { 
                                        component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);
                                    }
                                        else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--') 
                                        { 
                                            component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);  
                                            component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                                        }
                                            else if(IsOnChangeProductName == true  && ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--') 
                                            {   
                                                component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                                            }
                                                else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--') 
                                                { 
                                                    component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);	
                                                }
                }			
            });
            
            
            if(IsOnChangeServiceLine && IsOnChangeNatureOfWork && ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeNatureOfWork", false);   
            }
            if(IsOnChangeServiceLine && IsOnChangeProductName && ServiceLine_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true); 
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeProductName", false);
            }
            
            if(IsOnChangeNatureOfWork && IsOnChangeProductName && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("NatureOfWork").set("v.disabled", false);
                component.find("ProductName").set("v.disabled", false); 
                component.set("v.OnChangeNatureOfWork", false);
                component.set("v.OnChangeProductName", false);
            }
            if(ProductName_Value != '--None--' && NatureOfWork_Value != '--None--' && ServiceLine_Value != '--None--'){
                component.find("ProductName").set("v.disabled", false); 
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ServiceLine").set("v.disabled", true);
            }
        }			
        else
        {	
            var action = component.get("c.getProducts");
            action.setParams({
                "opportunityID" : recordId,            
            });
            
            action.setCallback(this, function(response){ 
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading", false);   
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var productDataList =[];
                    productDataList.push(JSON.stringify(productList));
                    component.set("v.productDataList", productDataList); 
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.productDataList");
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name); 
                        var pro = {
                            "ID" : product.Id,
                            "Name" : product.Name
                        }
                        ServiceLineArrayList.push(product.Service_Line__c); 
                        
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); 		
                        
                    }
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }	
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList); 
                    component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 	  
                } 
            });
        }
        $A.enqueueAction(action); 
    },
    
    getProductDetails : function(component){
        var action = component.get("c.getProductDetail");
        action.setParams({
            "Product2Id" : component.get("v.DefaultproductId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var ProductDetail = result;
                component.set("v.selectedServiceLineDefault",ProductDetail.Service_Line__c);
                component.set("v.selectedNatureOfWorkDefault",ProductDetail.Nature_of_Work__c); 
                component.set("v.ServiceLineArrayList",ProductDetail.Service_Line__c);
                component.set("v.NatureOfWorkArrayList",ProductDetail.Nature_of_Work__c); 
                component.find("ServiceLine").set("v.disabled", true); 
                component.find("NatureOfWork").set("v.disabled", true);
            }
            else if (state === "INCOMPLETE") {
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
    }
})