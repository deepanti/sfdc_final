({
    doInit : function(component, event, helper) {
        var action = component.get("c.chkContract");
        action.setParams({
            parentId : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var opp = a.getReturnValue();
                component.set("v.opp", opp);
            } else if (a.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);  
    
    var action1 = component.get("c.chkAttach");
        action1.setParams({
            conparentId : component.get("v.recordId")
        });
        
        action1.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var Con = a.getReturnValue();
                component.set("v.con", Con);
            } else if (a.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action1);  
    }
})