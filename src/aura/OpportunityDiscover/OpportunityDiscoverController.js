({
   
    handleActive: function (component, event, helper) {
        console.log('handleActive');
        debugger;
        component.set("v.selectedPrevTab",localStorage.getItem('LatestTabId'));
        //get the maximum tab id
       // var latestTab  = component.get("v.OpportunityRecord").LatestTabId__c;
      //  localStorage.setItem('LatestTabId',event.getParam("id"));
        var latestTab  = localStorage.getItem('MaxTabId');
        //selected tab id
        var tabSelected = event.getParam("id");
        //Previous tabs are allowed
        if(parseInt(tabSelected)<=latestTab)
            component.set("v.isEventFired",true);
        else{
            component.set("v.isEventFired",false);
        }
        helper.handleActive(component, event, event.getParam("id"));
    },
    handleActiveFromParent : function(component, event, helper) {
        component.set("v.isEventFired",true);
        component.set("v.TabIdFromEvent",localStorage.getItem('LatestTabId'));
    },
    handleComponentEventFired : function(component, event, helper) {
        var SelectedTabId = localStorage.getItem('LatestTabId')
        component.set("v.isEventFired",true);
        component.set("v.selectedTab", SelectedTabId);
        component.set("v.TabIdFromEvent",SelectedTabId);
        helper.handleActive(component, event, SelectedTabId);
    },
    //for Refresh
    tabRefresh : function(component,helper,event){
      
        var childComponent = component.find("addprod");
        childComponent.getScoreMethod();
        
    }

    
})