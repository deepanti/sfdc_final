({
    handleActive: function (component, event, tabSelected ) {
        debugger;
       // var childCmp = component.find("addPricing");
        var tab = component.find(tabSelected);        
        var isEventFire = component.get("v.isEventFired");
        if(component.get("v.isEventFired"))
        {
            //Set the previous tab
            component.set("v.selectedPrevTab", component.get("v.selectedTab"));
            localStorage.setItem('LatestTabId',tabSelected);
            $A.get('e.force:refreshView').fire();//fixed added to refresh the component
        }
        else
        {
            //block tab change
            component.set("v.selectedTab", component.get("v.selectedPrevTab"));
        }
        component.set("v.isEventFired",false);
    },
    injectComponent: function (name, target, recId, maxTabId) {
        $A.createComponent(name, {
            MaxSelectedTabId : maxTabId,
            recordId : recId
        }, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
                
            } else {
                throw new Error(error);
            }
        });
    }
})