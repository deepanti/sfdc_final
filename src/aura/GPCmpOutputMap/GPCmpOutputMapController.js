({
	renderValue: function(component, event, helper) {
		var key = component.get("v.key");
		var map = component.get("v.map");
		var hideIfKeyNotAvailable = component.get("v.hideIfKeyNotAvailable");
		var value;

		if(hideIfKeyNotAvailable) {
        	value = map && !$A.util.isEmpty(map[key]) ? map[key] : '';
		} else {
        	value = map && !$A.util.isEmpty(map[key]) ? map[key] : key;
		}
        
        component.set("v.value", value);
	}
})