({
    //Load Account Industry Picklist
    doInit: function(component, event, helper) {
        var action = component.get("c.getDropReasonsList");
        // var mapToSend = new Map()
        var master;
        var dublicity;
        var processRelated;
        var qualifivation;
        var sellerAss;
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                // alert('success result '+JSON.stringify(result.MastValue));
                master = result.MastValue;
              /*  dublicity = result.Duplicity;
                processRelated = result.Qualification;
                qualifivation = result.SellerAssessment;
                sellerAss = result.ProcessRelated;
                */
                dublicity = result.Duplicity;
                processRelated = result.ProcessRelated;
                qualifivation = result.Qualification;
                sellerAss = result.SellerAssessment;
                
                
                
                
                component.set("v.DuplicityLst",dublicity);
                component.set("v.QualificationLst",qualifivation);
                component.set("v.SellerAssessmentLst",sellerAss);
                component.set("v.ProcessRelatedLst",processRelated);
                
                /*var sellercl = component.find('normalwidth');
                $A.util.addClass(cmpDiv, 'changeStyle'); */
            }else{
                alert('apex class fail ');
            }
        });
        $A.enqueueAction(action);
        
        
    },
    closeSuccessToast:function(component,event,helper){
        component.set("v.isSuccess", false);
        component.set("v.isError", false);
        
    },
    
    save:function(component,event,helper){
        
        var newMap = new Map();
        var blankChekFlag = false;
        var checkboxesDupChecked = []; 
        var recordId = component.get("v.recordId");
        var selectedRadioDuplicity = component.find('duplicityRadioId').get('v.value');
        if(selectedRadioDuplicity == true){
            var dupCheckbox = component.find('checkboxDuplicityId');
            for (var i=0; i<dupCheckbox.length; i++) {
                if (dupCheckbox[i].get("v.value") == true) {
                    // alert(dupCheckbox[i].get('v.label'));
                    checkboxesDupChecked.push(dupCheckbox[i].get('v.label'));
                }
            }
             // alert(checkboxesDupChecked);
            if(checkboxesDupChecked != null && checkboxesDupChecked != ''){
                blankChekFlag = true;
            }
            newMap['Duplicity'] = checkboxesDupChecked;
        }
        
        var checkboxesQualificationChecked = []; 
        var selectedRadioQualification = component.find('duplicityQualificationId').get('v.value');
        if(selectedRadioQualification == true){
            var qualificationCheckbox = component.find('checkboxQualificationId');
            for (var i=0; i<qualificationCheckbox.length; i++) {
                if (qualificationCheckbox[i].get("v.value") == true) {
                    //  alert(qualificationCheckbox[i].get('v.label'));
                    checkboxesQualificationChecked.push(qualificationCheckbox[i].get('v.label'));
                }
            }
            //alert(checkboxesQualificationChecked);
            if(checkboxesQualificationChecked != null && checkboxesQualificationChecked != ''){
                blankChekFlag = true;
            }
            newMap['Qualification'] = checkboxesQualificationChecked;
        }
        var checkboxesSellerChecked = []; 
        var selectedRadioSeller = component.find('duplicitySellerId').get('v.value');
        if(selectedRadioSeller == true){
            var sellerCheckbox = component.find('checkboxSellerId');
            for (var i=0; i<sellerCheckbox.length; i++) {
                if (sellerCheckbox[i].get("v.value") == true) {
                    // alert(sellerCheckbox[i].get('v.label'));
                    checkboxesSellerChecked.push(sellerCheckbox[i].get('v.label'));
                }
            }
            if(checkboxesSellerChecked != null && checkboxesSellerChecked != ''){
                blankChekFlag = true;
            }
            newMap['Seller Assessment'] = checkboxesSellerChecked;
            //alert(checkboxesSellerChecked);
        }
        function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
        var checkboxesProcessChecked = []; 
        var selectedRadioProcess = component.find('duplicityProcessId').get('v.value');
        if(selectedRadioProcess == true){
            var processCheckbox = component.find('checkboxProcessId');
            for (var i=0; i<processCheckbox.length; i++) {
                if (processCheckbox[i].get("v.value") == true) {
                    
                    checkboxesProcessChecked.push(processCheckbox[i].get('v.label'));
                }
            }
            if(checkboxesProcessChecked != null && checkboxesProcessChecked != ''){
                blankChekFlag = true;
            }
            newMap['Process Related'] = checkboxesProcessChecked;
           // alert(checkboxesProcessChecked);
        }
        console.log('Test');
        console.log(newMap);
        console.log('Test');
        //alert(':==new map==:'+blankChekFlag);
        if(blankChekFlag){
            component.set("v.isSuccess", true);
            var action = component.get("c.saveDropReasons");
            action.setParams(
                {
                    "resultData" : newMap,
                    "oppId": recordId
                }
                
            );
            action.setCallback(this, function(response){
                var state = response.getState();
                if(state === "SUCCESS"){
                    component.set("v.isSuccess", true);
                    // alert("=====classes ====");
                    if(isLightningExperienceOrSalesforce1()) {
                        sforce.one.navigateToURL('/'+recordId);
                    }
                    else{
                        // alert("=====classes ====");
                        window.location = '/'+recordId;
                    }
                }
                else{ 
                    // alert('false success');
                    window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                }
                
                
            }),
                
                $A.enqueueAction(action);
        }else{
            component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.isError", false);
                }), 3000
            );
        }
    },
    
    handleCancel: function(component, event, helper) 
    {      
        var recordId = component.get("v.recordId");  
        function isLightningExperienceOrSalesforce1() 
        {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        } 
        if(isLightningExperienceOrSalesforce1()) 
        {   
            sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
            //sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 
            //sforce.one.back(true);
        }
        else
        {
            window.location = '/'+recordId;   
        }  
        
    },
    reset: function(component,event){	 
        window.location.reload();		
        
    },
    
    onGroup:function(component,event,helper){
        var selected = event.getSource().get("v.text");
        component.set("v.masterValue",selected);
        //alert(selected);//Qualification
        if(selected =='Duplicity'){
            var spanDiv=component.find('checkboxDuplicitysId');
            $A.util.addClass(spanDiv,'textellipsis1');
            $A.util.removeClass(spanDiv,'textellipsis2');
            component.set("v.dublictyCheck",false);
            helper.uncheckClass(component,event,selected);
        }else{
            component.set("v.dublictyCheck",true);
            var spanDiv=component.find('checkboxDuplicitysId');
            $A.util.removeClass(spanDiv,'textellipsis1');
            $A.util.addClass(spanDiv,'textellipsis2');
            
        }
        if(selected =='Qualification'){
            var spanDiv=component.find('checkQualificationsId');
            $A.util.addClass(spanDiv,'textellipsis1');
            $A.util.removeClass(spanDiv,'textellipsis2');
            component.set("v.QualificationCheck",false);
            helper.uncheckClass(component,event,selected);
        }else{
            var spanDiv=component.find('checkQualificationsId');
            $A.util.removeClass(spanDiv,'textellipsis1');
            $A.util.addClass(spanDiv,'textellipsis2');
            component.set("v.QualificationCheck",true);
        }
        if(selected =='Seller Assessment'){
            var spanDiv=component.find('checkboxSellersId');
            $A.util.addClass(spanDiv,'textellipsis1');
            $A.util.removeClass(spanDiv,'textellipsis2');
            component.set("v.SellerCheck",false);
            helper.uncheckClass(component,event,selected);
        }else{
            var spanDiv=component.find('checkboxSellersId');
            $A.util.removeClass(spanDiv,'textellipsis1');
            $A.util.addClass(spanDiv,'textellipsis2');
            component.set("v.SellerCheck",true);
        }
        if(selected =='Process Related'){
            var spanDiv=component.find('checkboxProcesssId');
            $A.util.addClass(spanDiv,'textellipsis1');
            $A.util.removeClass(spanDiv,'textellipsis2');
            component.set("v.ProcessCheck",false);
            helper.uncheckClass(component,event,selected);
        }else{
            var spanDiv=component.find('checkboxProcesssId');
            $A.util.removeClass(spanDiv,'textellipsis1');
            $A.util.addClass(spanDiv,'textellipsis2');
            component.set("v.ProcessCheck",true);
        }
        
    }
})