({
    doInit : function(component, event, helper) {
        component.set("v.buyingRole",component.get("v.ContactRoleRec.Role__c"));
        component.set("v.isPrimary",component.get("v.ContactRoleRec.IsPrimary__c"));
    },
    openContact : function(component, event, helper) {
        //opening the contact 
        var navEvt = $A.get("e.force:navigateToSObject"); 
        navEvt.setParams({
            "recordId": component.get("v.ContactRoleRec.Contact__c")
        });
        navEvt.fire();
    },
    handleEdit : function(component, event, helper) {
        
        var compEvent = component.getEvent("isVisibleEvent");
        compEvent.setParams({
            "isVisible" :true,
            "isPrimary":component.get("v.ContactRoleRec.IsPrimary__c"),
            "contact":component.get("v.ContactRoleRec"),
            "buyingRole":component.get("v.ContactRoleRec.Role__c"),
            "contactId":component.get("v.ContactRoleRec.Contact__c")
        });
        compEvent.fire();
    },
    closeDialog: function(component, event, helper) {
        component.set("v.isVisible",false);
        var compEvent = component.getEvent("isVisibleEvent");
        compEvent.setParams({"isVisible" :true,
                             "contact":component.get("v.ContactRoleRec")});
        compEvent.fire();
    },
    
    handleDelete:function(component, event, helper) {
        // passing the contact Id for deleting the role item
        var compEvent = component.getEvent("passContactId");
        compEvent.setParams({"contactId" : component.get("v.ContactRoleRec.Contact__c")});
        compEvent.fire();
        component.set("v.isVisibleComponent",false);
    }
})