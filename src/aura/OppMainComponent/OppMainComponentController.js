// Controller Name: LeadMainComponentController.js


// Initscripts: Adds click event to massupdate button and update it to invoke dialog box.
({
	initScripts: function(component, event, helper) {

        //Ignore duplicate notifications that may arrive because other components 
        //loading scripts using the same library. 
        if (component.alreadyhandledEvent)  
            return;
        
            var btn = component.find("massUpdate").getElement();
            var dlg = component.find("modalDlg").getElement();
            jQuery(btn).on("click", function() {
                if(component.get("v.selectedLeads") != ''){
                jQuery(dlg).modal();
                }else{
                    alert('Please Select Any Leads');
                }
            });
            component.alreadyhandledEvent = true;
        },
    // massUpdate: This function is used to update the status of selected leads with the selected status.
    // 'v.selectedLeads' contains Ids of selected leads and it is Json encoded and status to be updated is 
    // retrieved using the id of the component ('status').
    // these two values are passed as parameters to invoke massUpdateLeads(String recids,string status)
    // of leadcontroller class using setParam and returned list is set to 'v.Leads' which is nothing but a
    // List<Leads> available as attribute in LeadmainComponent.cmp
    massupdate:function(component, event) {
        
        if(component.get("v.selectedLeads") != ''){ 
           // document.getElementById("loadicon").style.display = "block";
        var recids =   $A.util.json.encode(component.get("v.selectedLeads"));
        var status = component.find("status");
        status = status.get("v.value");        
        var action = component.get("c.massUpdateOpps");       
        action.setParams({
            "recIds": recids,
            "Status": status
        });

        action.setCallback(this, function(a) {
            var state=a.getState();
            if(state === "SUCCESS"){
            component.set("v.Opps", a.getReturnValue());
            alert('Opportunities Updated Successfully');
			//document.getElementById("loadicon").style.display = "none";
            }
            else
            {
                var errors = a.getError();
                console.log(errors[0].message);
                alert(errors[0].message);
			//document.getElementById("loadicon").style.display = "none";
            }

        });
        $A.enqueueAction(action);
            
        }
    },
    // updateOwner: This function is used to update the status of selected leads with the selected status.
    // 'v.selectedLeads' contains Ids of selected leads which is Json encoded and
    // passed as parameter to invoke massUpdateOwner(String recids)
    // of leadcontroller class using setParam and the returned list is set to 'v.Leads' which is nothing but a
    // List<Leads> available as attribute in LeadmainComponent.cmp
    updateOwner:function(component, event) {
        if(component.get("v.selectedLeads") != ''){
            var recids =   $A.util.json.encode(component.get("v.selectedLeads"));              
            var action = component.get("c.massUpdateOwner");       
            action.setParams({
                "recIds": recids            
            });
            action.setCallback(this, function(a) {
                component.set("v.Leads", a.getReturnValue());
            });
            $A.enqueueAction(action); 
            alert('Leads Updated Successfully');
        }else{
            alert('Please Select Any Leads');
        }
      
    },
    
    // doInit: This function is retrieve list of leads(50) using SOQL (LIMIT 50) and the returned list is set to 'v.Leads' which is nothing but a
    // List<Leads> available as attribute in LeadmainComponent.cmp
    doInit : function(component, event) {
        var action = component.get("c.getOppRecord");
        action.setCallback(this, function(a) {
            component.set("v.Opps", a.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    // updateSelectedLeads: This function is to keep track of leads recIDs selected using Checkboxes and push the same into an array.
    
    updateSelectedLeads : function(component, event) {
        var id = event.source.get("v.text");
        if (event.source.get("v.value")) {
            
            if (component.get("v.selectedLeads").indexOf(id) < 0) {
                component.get("v.selectedLeads").push(id);
                console.log(component.get("v.selectedLeads"));
            }
        } else {
            
            var index = component.get("v.selectedLeads").indexOf(id);
            if (index > -1) {
                component.get("v.selectedLeads").splice(index, 1);
            }
            console.log(component.get("v.selectedLeads"));
        }
},
    // getLeads: This function is retrieve list of leads(50) using SOQL (LIMIT 50) based on searchText which is available in varibale recName and 
    // invokes getLeadByName method of Leadcontroller with searchText(recName) as a parameter using setParam
    // and the returned list is set to 'v.Leads' which is nothing but a
    // List<Leads> available as attribute in LeadmainComponent.cmp
    getLeads: function(component, event) {
    var searchKey = event.getParam("recName");      
    var action = component.get("c.getLeadByName");       
    action.setParams({
      "recName": searchKey
    });
    action.setCallback(this, function(a) {
        component.set("v.Leads", a.getReturnValue());
    });
    $A.enqueueAction(action);
},
    waiting: function(component, event, helper) {
    document.getElementById("loadicon").style.display = "block";
 },
 
   doneWaiting: function(component, event, helper) {
   document.getElementById("loadicon").style.display = "none";
 },
})