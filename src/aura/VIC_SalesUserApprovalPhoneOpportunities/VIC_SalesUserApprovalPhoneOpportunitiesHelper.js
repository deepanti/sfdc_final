({
	opportunityPending : function(component) {		
        var action = component.get('c.getOpportunityPending');
        action.setCallback(this, function(actionResult){
        console.log('Opportunity+ Phone++',actionResult.getReturnValue());
      if(actionResult.getState()=='SUCCESS') { 
        component.set('v.opportunityListPending', actionResult.getReturnValue());
        component.set("v.listSizePending",actionResult.getReturnValue().length); 
        var oppDataWrap = component.get('v.opportunityListPending'); 
           for(var eachOpp in oppDataWrap){
                var oliDataLst = oppDataWrap[eachOpp].opportunityLIWrapperObj;
                component.set('v.LstOLI', oliDataLst);
               
               	break;
           } 
          var opportunityListPending = component.get("v.opportunityListPending");
          if(opportunityListPending.length > 0)
          {
              opportunityListPending[0].DefaultColor = true;
              component.set("v.opportunityListPending",opportunityListPending);
          }
       }
     });        
        $A.enqueueAction(action);
    },
      
    handleComponentEvent : function(component, event, helper) { 
      var oppOLIData = event.getParam("oppId");
      component.set('v.LstOLI', oppOLIData);
      
    },
    
    updateStatus : function(component, event) {	
        
        
        var opportunityListPending = component.get("v.opportunityListPending");
        var selectedOpportunityLI = [];
        var olilist=component.get("v.LstOLI");
        var strcomment;
        var selectedoli=[];
        var selectrecordid;
        
       
        //alert('test'+opportunityListPending.length);
        
        for(var i=0; i < opportunityListPending.length; i++){
           
           for(var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
              
               if(opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected==true){
               
                   strcomment=opportunityListPending[i].opportunityLIWrapperObj[j].strComment;
                   selectrecordid=opportunityListPending[i].opportunityLIWrapperObj[j].strOliId;
                     
                    
                   selectedoli.push(opportunityListPending[i].opportunityLIWrapperObj[j].oppLIObj); 
               
               }
               
            }
           
        }
       
       /* alert(selectedoli.length);
        
       for(var i=0; i < olilist.length; i++){
              
            if(olilist[i].oppLISelected==true){
                
                   strcomment=olilist[i].strComment;
                    selectrecordid=olilist[i].strOliId;
                   alert(selectrecordid)
                    selectedoli.push(olilist[i].oppLIObj);
           }
        }
       */
               
        
       
        if(selectrecordid == null){
            var sMsg = 'Please select Oli to Approve.';
                   
            var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 1000',
                        message: sMsg,
                        type : 'Error'
                    });
                  toastEvent.fire();  
           component.set("v.showModalQuery", false);
        }
        else{
         component.set('v.isSpinnerVisible', true); 
         var action = component.get('c.getupdateStatus');
        action.setParams({"selectedOpportunityLI":selectedoli});
        action.setCallback(this, function(actionResult){
            component.set('v.isSpinnerVisible', false);
     	  if(actionResult.getState()==='SUCCESS') {  
               $A.get('e.force:refreshView').fire();
        /* this is toast message while approve opportunity*/   		
           		  var sMsg = 'Records have been Processed Successfully';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 1000',
                        message: sMsg,
                        type : 'success'
                    });
                   toastEvent.fire();
       			}
    		 });        
        $A.enqueueAction(action); 
            
            
            
        }
         
    },
    
    updateStatusOnHold : function(component, event) {	
        var opportunityListPending = component.get("v.opportunityListPending");
        var selectedOpportunityLI = [];
        var olilist=component.get("v.LstOLI");
        var strcomment;
        var selectedoli=[];
        var selectrecordid;
        var status;
        for(var i=0; i < olilist.length; i++){
               if(olilist[i].oppLISelected){
                   strcomment=olilist[i].strComment;
                    selectrecordid=olilist[i].strOliId;
                    status=olilist[i].strStatus;
                   
                    
           }
        }
       
               
        
       
        if(selectrecordid == null || status==='On-Hold'){
            var sMsg;
            if(selectrecordid == null){
                 sMsg = 'Please select Oli to Query';
            }
            else 
            {
                if(status==='On-Hold'){
                    
                    sMsg = 'The card is already in query.';
                }
             }
                    
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 1000',
                        message: sMsg,
                        type : 'Error'
                    });
                   toastEvent.fire();  
            component.set("v.showModal", false);
        }
        else{
            
            
            if((strcomment==null || strcomment=='')){
            var sMsg = 'Comments are mandatory.';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 1500',
                        message: sMsg,
                        type : 'Error'
                    });
                   toastEvent.fire();  
            component.set("v.showModal", false);
            
            
        }
      
      else{
           component.set('v.isSpinnerVisible', true);  
           var action = component.get('c.updateOLIStatus');
          action.setParams({
		   "strRecordId": selectrecordid,
		   "strDescription": strcomment
		
		
		});
       action.setCallback(this, function(actionResult) {
       component.set('v.isSpinnerVisible', false); 
       if(actionResult.getState()==='SUCCESS') { 
               $A.get('e.force:refreshView').fire();
       			 /* this is toast message while approve opportunity*/   		
           		  var sMsg = 'Records have been Processed Successfully';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 1000',
                        message: sMsg,
                        type : 'success'
                    });
                   toastEvent.fire();
       			}
    	 });
       $A.enqueueAction(action);
       }
      }       
             
             
 },
 
})