({
	doInit : function(component, event, helper) {
        helper.opportunityPending(component);
		var salesRepId = component.get('v.salesRepId');
        console.log('salesRepId: ', salesRepId);
      
	},
  
    handleComponentEvent : function(component, event, helper) { 
      helper.handleComponentEvent(component, event, helper)
    },
     updateStatus: function(component, event, helper) { 
        component.set('v.showModal', false);
        component.set('v.showModalQuery', true); 
         
    },
 
    
    
    updateStatusOnHold : function(component, event, helper) { 
       
      component.set('v.showModal', true);
      component.set('v.showModalQuery', false);   
        
         
    },
    onButtonClickHandlerApprove : function(component, event, helper){
        
        helper.updateStatus(component, event, helper)
        
    },
    onButtonClickHandler : function(component, event, helper){
      
       helper.updateStatusOnHold(component, event, helper);
    },
    hideConfirmBox : function (component, event, helper) { 
        component.set("v.showModal", false);
        component.set('v.showModalQuery', false); 
    },
    
    selectAllOppAndLI: function(component, event, helper) {
  	   
       var selectedHeaderCheck = event.getSource().get("v.value");
       
        var selectoli=[]
        var opportunityListPending = component.get("v.opportunityListPending");
                          
       
        
       for(var i=0; i < opportunityListPending.length; i++){
           
            opportunityListPending[i].oppSelected = selectedHeaderCheck;
           
       for(var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
               opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected = selectedHeaderCheck;
               selectoli.push(opportunityListPending[i].opportunityLIWrapperObj[j]); 
               
            }
           
        }
         
        
        
       component.set("v.LstOLI",selectoli);
      
        
       component.set("v.opportunityListPending", opportunityListPending);
        
          
      
    },
    removeCss:function(component, event, helper) {
      alert('test')
     var cmpTarget = component.find('maintbl');
        alert(cmpTarget)
     $A.util.removeClass(cmpTarget, 'selectcolor');
    }
    

})