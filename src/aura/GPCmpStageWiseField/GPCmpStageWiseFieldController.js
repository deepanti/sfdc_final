({
    doInit: function(component, event, helper) {
        helper.getStageWiseFields(component);
    },
    showStageWiseFields: function(component, event, helper) {
        component.set("v.isStageWiseFieldVisible", true);
    },
    closeDivision: function(component, event, helper) {
        component.set("v.isStageWiseFieldVisible", false);
    },
    toggelStage: function(component, event, helper) {
        debugger;
        var currentStage = event.target.id;
        var listOfStages;
        var recordTypeName = component.get("v.recordTypeName");

        if (recordTypeName === "Indirect PID") {
            listOfStages = helper.LIST_OF_INDIRECT_STAGES;
        } else {
            listOfStages = helper.LIST_OF_NORMAL_STAGES;
        }

        for (var i = 0; i < listOfStages.length; i++) {
            var stageContentName = listOfStages[i] + '_' + "content";
            var stageDom = component.find(listOfStages[i]);
            var stageContentDom = component.find(stageContentName);
            if (currentStage === listOfStages[i]) {
                $A.util.addClass(stageDom, "current");
                $A.util.removeClass(stageDom, "incomplete");
                $A.util.removeClass(stageContentDom, "slds-hide");
            } else {
                $A.util.removeClass(stageDom, "current");
                $A.util.addClass(stageDom, "incomplete");
                $A.util.addClass(stageContentDom, "slds-hide");
            }
        }
    }
})