({
    LIST_OF_NORMAL_STAGES: [
        "CostCharging",
        "BillingCostCharging",
        "RevenueCostCharging",
        "Approved"
    ],
    LIST_OF_INDIRECT_STAGES: [
        "CostCharging",
        "BillingCostCharging",
        "RevenueCostCharging",
        "Approved"
    ],
    getStageWiseFields: function(component) {
        var getStageWiseFieldForProject = component.get("c.getStageWiseFieldForProject");
        var recordId = component.get("v.recordId");
        
        if(!recordId) {
            return;
        }

        getStageWiseFieldForProject.setParams({
            "projectId": recordId
        });

        getStageWiseFieldForProject.setCallback(this, function(response) {
            this.getStageWiseFieldForProjectHandler(response, component);
        });

        $A.enqueueAction(getStageWiseFieldForProject);
    },
    getStageWiseFieldForProjectHandler: function(response, component) {
        debugger;
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            
            var responseData = JSON.parse(responseData.response);
            var stageWiseFields = JSON.parse(responseData.stageWiseFields); 
            var leadershipMaster = responseData.leadershipMaster;
            var mapOfFieldNameToLabel = responseData.mapOfFieldNameToLabel;
            var sanatizedFields = this.getSanatizedFields(stageWiseFields, leadershipMaster, responseData.RecordTypeName);

            component.set("v.mapOfFieldNameToLabel", mapOfFieldNameToLabel);
            component.set("v.stageWiseFields", sanatizedFields);
            component.set("v.recordTypeName", responseData.RecordTypeName);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    getSanatizedFields: function(stageWiseFields, leadershipMaster, recordTypeName) {
        var sanatizedObjectFields = {};
        debugger;
        for(var key in stageWiseFields) {
            var sanatizedObjectName = key.split(' ').join();
            var sanatizedStageFields = {};

            for(var key2 in stageWiseFields[key]) {
                
                var sanatizedStageName = key2.split(' ').join('');
                var fields = stageWiseFields[key][key2]['fields'];
                fields = fields.map(v => v.toLowerCase());
                sanatizedStageFields[sanatizedStageName] = {
                    "fields": fields
                };
            }
            sanatizedObjectFields[sanatizedObjectName] = sanatizedStageFields;
        }
        leadershipMaster = leadershipMaster || [];

        for(var i=0; i < leadershipMaster.length; i += 1) {
            var stageName = leadershipMaster[i]["GP_Mandatory_for_stage__c"] || " ";
            stageName = stageName.split(' ').join('');
            var listOfFields = sanatizedObjectFields["Project"][stageName] || {"fields": []};
            listOfFields["fields"].push(leadershipMaster[i]["GP_Leadership_Role__c"]);

            sanatizedObjectFields["Project"][stageName] = listOfFields;
        }
        
        if(recordTypeName != 'Indirect PID') {
            var costChargingFields = sanatizedObjectFields["Project"]["CostCharging"] || {"fields": []};
            costChargingFields["fields"].push("Customer Name");  
            costChargingFields["fields"].push("Bill To Address");  
            costChargingFields["fields"].push("Ship To Address");   
            costChargingFields["fields"].push("Work Location");  
            costChargingFields["fields"].push("BCP flag");  
            costChargingFields["fields"].push("Primary location");  
            sanatizedObjectFields["Project"]["CostCharging"] = costChargingFields;   
        } else {
            var costChargingFields = sanatizedObjectFields["Project"]["ApprovedSupport"] || {"fields": []};
            costChargingFields["fields"].push("Work Location");  
            costChargingFields["fields"].push("BCP flag");  
            sanatizedObjectFields["Project"]["ApprovedSupport"] = costChargingFields;             
        }
        
        return sanatizedObjectFields;
    }
})