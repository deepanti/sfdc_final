({
    "defaultTemplate": {
        "listOfConfiguration": [{
                "objectName": "Project",
                "objectLabel": "Project",
                "listOfSection": [{
                        "SectionName": "Project_Info",
                        "SectionLabel": "Project Info",
                        "isExpanded": true,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "Name",
                                "Label": "Project Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": "" 
                            },
                            {
                                "ApiName": "GP_Project_Long_Name__c",
                                "Label": "Project Long Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Start_Date__c",
                                "Label": "Start Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_End_Date__c",
                                "Label": "End Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_SOW_Start_Date__c",
                                "Label": "SOW Start Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_SOW_End_Date__c",
                                "Label": "SOW End Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Customer_SOW_number__c",
                                "Label": "Customer SOW number",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Project_Currency__c",
                                "Label": "Project Currency",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Billing_Currency__c",
                                "Label": "Billing Currency",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Project_type__c",
                                "Label": "Project Type",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Project_Category__c",
                                "Label": "Project Category",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
							// START: Gainshare Change
                            {
                                "ApiName": "GP_Pure_Gainshare__c",
                                "Label": "Pure Gainshare",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Parent_PID_For_Gainshare__c",
                                "Label": "Parent PID for Gainshare",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            // END: Gainshare Change
                            {
                                "ApiName": "GP_TCV__c",
                                "Label": "TCV",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Bill_Rate_Type__c",
                                "Label": "Bill Rate Type",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Project_Organization__c",
                                "Label": "Project Owning Organization",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Primary_SDO__c",
                                "Label": "SDO",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Operating_Unit__c",
                                "Label": "Operating Unit",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_OMS_Deal_ID__c",
                                "Label": "OMS Deal Id",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Remittance_Mode__c",
                                "Label": "Remittance Mode",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CrossSellCharge__c",
                                "Label": "Cross Sell Charge",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Project_Description__c",
                                "Label": "Project Description",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_PBB_Flag__c",
                                "Label": "PBB Flag",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CRN_Number__c",
                                "Label": "CRN Number",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Contract_Start_Date__c",
                                "Label": "Contract Start Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Contract_End_Date__c",
                                "Label": "Contract End Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CRN_Status__c",
                                "Label": "CRN Status",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_T_M_With_Cap__c",
                                "Label": "T & M with Cap",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_GOL__c",
                                "Label": "GOL",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_GRM__c",
                                "Label": "GRM",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Collector__c",
                                "Label": "Collector",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Project_Info",
                                "stageName": ""
                            }
                        ]
                    },
                    // {
                    //     "SectionName": "Bill_To_And_Ship_To",
                    //     "SectionLabel": "Bill To and Ship To",
                    //     "isExpanded": false,
                    //     "isVisible": true,
                    //     "listOfFields": [{
                    //         "ApiName": "Bill_To_And_Ship_To_Name",
                    //         "Label": "Name",
                    //         "isVisible": true,
                    //         "isRequired": true,
                    //         "isReadOnly": false,
                    //         "isreadOnlyAfterApproval": true,
                    //         "sectionName": "Bill_To_And_Ship_To",
                    //         "stageName": ""
                    //     }]
                    // },
                    // {
                    //     "SectionName": "Project_Leadership",
                    //     "SectionLabel": "Project Leadership",
                    //     "isExpanded": false,
                    //     "isVisible": true,
                    //     "listOfFields": [{
                    //         "ApiName": "Project_Leadership_Name",
                    //         "Label": "Name",
                    //         "isVisible": true,
                    //         "isRequired": true,
                    //         "isReadOnly": false,
                    //         "isreadOnlyAfterApproval": true,
                    //         "sectionName": "Project_Leadership",
                    //         "stageName": ""
                    //     }]
                    // },
                    {
                        "SectionName": "CMITS_Fields",
                        "SectionLabel": "CMITS Fields",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "GP_HSL__c",
                                "Label": "HSL",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_VSL__c",
                                "Label": "VSL",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Parent_Customer_Sub_Division__c",
                                "Label": "Customer Sub Division",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Revenue_Description__c",
                                "Label": "Revenue Description",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_SILO_Names__c",
                                "Label": "SILO Names",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Project_Profile__c",
                                "Label": "Project Profile",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "CMITS_Fields",
                                "stageName": ""
                            }
                        ]
                    },
                    {
                        "SectionName": "Bussiness_Hierarchy",
                        "SectionLabel": "Bussiness Hierarchy",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "GP_Customer_Hierarchy_L4__c",
                                "Label": "Customer Hierarchy (L4)",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Business_Group_L1__c",
                                "Label": "Business Group(L1)",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Business_Segment_L2__c",
                                "Label": "Business Segment(L2)",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Sub_Business_L3__c",
                                "Label": "Sub Business (L3)",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Deal__c",
                                "Label": "DEAL",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Vertical__c",
                                "Label": "Vertical",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Sub_Vertical__c",
                                "Label": "Sub Vertical",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Service_Line__c",
                                "Label": "Service Line",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Nature_of_Work__c",
                                "Label": "Nature of Work",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Delivery_Org__c",
                                "Label": "Delivery Org",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Sub_Delivery_Org__c",
                                "Label": "Sub Delivery Org",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Product__c",
                                "Label": "Product",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Business_Name__c",
                                "Label": "Business Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Business_Type__c",
                                "Label": "Business Type",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_OLI_SFDC_Id__c",
                                "Label": "OLI SFDC Id",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Sales_Opportunity_Id__c",
                                "Label": "Opportunity SFDC Id",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Opportunity_Name__c",
                                "Label": "Opportunity Name",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": true,
                                "isreadOnlyAfterApproval": true,
                                "sectionName": "Bussiness_Hierarchy",
                                "stageName": ""
                            }
                        ]
                    },
                    {
                        "SectionName": "GEProjectDetails",
                        "SectionLabel": "GE Project Details",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "GP_Stakeholder_Name__c",
                                "Label": "Stakeholder Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "GEProjectDetails",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Stakeholder_Email_ID__c",
                                "Label": "Stakeholder Email",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "GEProjectDetails",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Contractual_Reporting_Requirement__c",
                                "Label": "Contractual Reporting Requirement",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "GEProjectDetails",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_BCP_Requirement__c",
                                "Label": "BCP Requirement",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "GEProjectDetails",
                                "stageName": ""
                            }
                        ]
                    },
                    {
                        "SectionName": "DispatchInfo",
                        "SectionLabel": "Dispatch Info",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "GP_MOD__c",
                                "Label": "MOD",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Portal_Name__c",
                                "Label": "Portal Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Attn_To_Name__c",
                                "Label": "Attn To Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Attn_To_Email__c",
                                "Label": "Attn To Email",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Customer_SPOC_Name__c",
                                "Label": "Customer SPOC Name",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Customer_SPOC_Email__c",
                                "Label": "Customer SPOC Email",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CC1__c",
                                "Label": "CC1",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CC2__c",
                                "Label": "CC2",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_CC3__c",
                                "Label": "CC3",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "type": "email",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Customer_Contact_Number__c",
                                "Label": "Customer Contact Number",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_FAX_Number__c",
                                "Label": "FAX Number",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "DispatchInfo",
                                "stageName": ""
                            }
                        ]
                    },
                    {
                        "SectionName": "OtherClassification",
                        "SectionLabel": "Other Classification",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                                "ApiName": "GP_SEZ__c",
                                "Label": "SEZ",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Is_PO_Required__c",
                                "Label": "Is PO Required",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                             {
                                "ApiName": "GP_Is_Customer_PO_Available__c",
                                "Label": "Is Customer PO Available",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Revenue_Sharing_Basis_Resource_HSL__c",
                                "Label": "Revenue Sharing Basis Resource HSL",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Invoice_Reminder_To_be_Sent__c",
                                "Label": "Invoice Reminder To be Sent",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Shift_Allowed__c",
                                "Label": "Shift Allowed",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Shift_Start_Date__c",
                                "Label": "Shift Start Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Overtime_Billable__c",
                                "Label": "Overtime Billable",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Holiday_Billable__c",
                                "Label": "Holiday Billable",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Invoice_at_Person_Level__c",
                                "Label": "Invoice At Person Level",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Transition_Allowed__c",
                                "Label": "Transition Allowed",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Transition_End_Date__c",
                                "Label": "Transition End Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Straight_Line_Method__c",
                                "Label": "Straight Line Method",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Grouped_Invoice__c",
                                "Label": "Grouped Invoice",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Next_Finance_Email_Date__c",
                                "Label": "Next Finance Email Date",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "type": "date",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Last_Update_Time_of_Child_Records__c",
                                "Label": "Last Update Time Of Child Records",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Timesheet_Requirement__c",
                                "Label": "Timesheet Requirement",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_OMS_Status__c",
                                "Label": "OMS Status",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Invoice_Format__c",
                                "Label": "Invoice Format",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Daily_Normal_Hours__c",
                                "Label": "Daily Normal Hours",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Parent_Project_for_Group_Invoice__c",
                                "Label": "Parent Project For Group Invoice",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Market_Serviced__c",
                                "Label": "Market Serviced",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },
                            {
                                "ApiName": "GP_Invoice_Frequency__c",
                                "Label": "Invoice Frequency",
                                "isVisible": true,
                                "isRequired": true,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Service_Deliver_in_US_Canada__c",
                                "Label": "Service Delivery in US/Canada",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, 
							   {
                                "ApiName": "GP_Country__c",
                                "Label": "Country",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },{
                                "ApiName": "GP_State__c",
                                "Label": "State",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Pincode__c",
                                "Label": "Pin Code",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_City__c",
                                "Label": "City",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            },   {
                                "ApiName": "GP_US_Address__c",
                                "Label": "Address",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Services_To_Be_Delivered_From__c",
                                "Label": "Services To Be Delivered From",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_HSN_Category_Id__c",
                                "Label": "HSN Id",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_HSN_Category_Name__c",
                                "Label": "HSN Name",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Sub_Category_Of_Services__c",
                                "Label": "Sub category of services",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, 
							 
									  {
							"ApiName": "GP_Divine__c",
							"Label": "GE Classification",
							"isVisible": true,
							"isRequired": false,
							"isReadOnly": false,
							"isreadOnlyAfterApproval": false,
							"sectionName": "OtherClassification",
							"stageName": ""
						},
							{
                                "ApiName": "GP_Tax_Exemption_Certificate_Available__c",
                                "Label": "Tax Exemption certificate available",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_Valid_Till__c",
                                "Label": "Valid Till",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }, {
                                "ApiName": "GP_TAX_Code__c",
                                "Label": "TAX Code",
                                "isVisible": true,
                                "isRequired": false,
                                "isReadOnly": false,
                                "isreadOnlyAfterApproval": false,
                                "sectionName": "OtherClassification",
                                "stageName": ""
                            }
                        ]
                    }
                ]
            },
            {
                "objectName": "ProjectLeadership",
                "numberOfDefaultRowToDisplay": 3,
                "objectLabel": "Project Leadership",
                "listOfSection": [{
                        "SectionName": "MandatoryKeyMembersLeadership",
                        "SectionLabel": "MandatoryKeyMembers Leadership",
                        "isExpanded": true,
                        "isVisible": true,
                        "listOfFields": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "MandatoryKeyMembersLeadership",
                            "stageName": ""
                        }]
                    },
                    {
                        "SectionName": "RegionalLeadership",
                        "SectionLabel": "Regional Leadership",
                        "isExpanded": true,
                        "isVisible": true,
                        "listOfFields": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "MandatoryKeyMembersLeadership",
                            "stageName": ""
                        }]
                    },
                    {
                        "SectionName": "AccountLeadership",
                        "SectionLabel": "Account Leadership",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "MandatoryKeyMembersLeadership",
                            "stageName": ""
                        }]
                    },
                    {
                        "SectionName": "HSLLeadership",
                        "SectionLabel": "HSL Leadership",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfFields": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "MandatoryKeyMembersLeadership",
                            "stageName": ""
                        }]
                    },
                    {
                        "SectionName": "AdditionalAccessLeadership",
                        "SectionLabel": "Additional Access Leadership",
                        "isExpanded": false,
                        "isVisible": true,
                        "listOfSection": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "MandatoryKeyMembersLeadership",
                            "stageName": ""
                        }]
                    }
                ]
            },
            {
                "objectName": "GP_Billing_Milestone__c",
                "objectLabel": "Billing Milestone",
                "numberOfDefaultRowToDisplay": 3,
                "listOfSection": [{
                    "SectionName": "All",
                    "SectionLabel": "Billing Milestone",
                    "isExpanded": true,
                    "isVisible": true,
                    "listOfFields": [{
                            "ApiName": "Name",
                            "Label": "Name",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Date__c",
                            "Label": "Date",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Milestone_start_date__c",
                            "Label": "MileStone Start Date",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Milestone_end_date__c",
                            "Label": "MileStone End Date",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Work_Location__c",
                            "Label": "Work location",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Entry_type__c",
                            "Label": "Entry type",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Value__c",
                            "Label": "Value",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_Amount__c",
                            "Label": "Amount",
                            "isVisible": true,
                            "isRequired": true,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "All",
                            "stageName": ""
                        }
                    ]
                }]
            },
            {
                "objectName": "ProjectResourceAllocation",
                "objectLabel": "Project Resource Allocation",
                "numberOfDefaultRowToDisplay": 3,
                "listOfSection": [{
                    "SectionName": "Default",
                    "SectionLabel": "Default",
                    "isExpanded": true,
                    "isVisible": true,
                    "listOfFields": [{
                            "ApiName": "GP_MS_Attention_To__c",
                            "Label": "Attention To (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_BusinessUnit_Manager__c",
                            "Label": "BusinessUnit Manager (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_MER_No__c",
                            "Label": "MER No (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_Address__c",
                            "Label": "Address (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_Email_Id__c",
                            "Label": "Email Id (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_ID__c",
                            "Label": "MS ID (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_MANAGER__c",
                            "Label": "MANAGER (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_MS_Payment_Mode__c",
                            "Label": "Payment Mode (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_COST_CENTER__c",
                            "Label": "Cost Center (MS)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_CM_Address__c",
                            "Label": "Address (CVS Health)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        },
                        {
                            "ApiName": "GP_CM_Id__c",
                            "Label": "Z Id (CVS Health)",
                            "isVisible": false,
                            "isRequired": false,
                            "isReadOnly": false,
                            "isreadOnlyAfterApproval": true,
                            "sectionName": "Default",
                            "stageName": ""
                        }
                    ]
                }]
            },
            {
                "objectName": "ProjectWorkLocationAndSDO",
                "numberOfDefaultRowToDisplay": 3,
                "objectLabel": "Project Work Location and SDO",
                "listOfSection": []
            }
        ]
    }
})