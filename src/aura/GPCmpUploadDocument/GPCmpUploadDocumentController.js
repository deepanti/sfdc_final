({
    handleFilesChange: function(component, event, helper) {
        var fileName = helper.FILE_NOT_SELECTED_ERROR_LABEL;
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }

        component.set("v.fileName", fileName);

        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            alert(helper.INVALID_FILE_ERROR_LABEL);
        }
    }
})