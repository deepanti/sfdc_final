({
    INVALID_FILE_ERROR_LABEL: 'Please Select a Valid File',
    INVALID_FILE_FORMAT_ERROR_LABEL: 'Invalid Format',
    FILE_NOT_SELECTED_ERROR_LABEL: 'No File Selected.',
    SELECTED_FILE_SIZE_LABEL: ' Selected file size: ',
    MAX_FILE_SIZE: 20000000, //Max file size 5 MB 
    CHUNK_SIZE: 2000000, //Chunk Max size 750Kb 
    MAP_OF_FILE_FORMAT: {
        "jpg": true, 
        "jpeg": true, 
        "jpe": true, 
        "email": true, 
        "excel": true,
        "xlsx": true,
        "pptx": true,
        "txt": true,
        "docx": true,
        "docm": true,
        "doc": true,
        "dotx": true,
        "dotm": true,
        "dot": true,
        "pdf": true, 
        "xps": true,
        "mht": true,
        "mhtml": true,
        "html": true,
        "htm": true,
        "rtf": true,
        "txt": true,
        "xml": true,
        "odt": true,
        "ods": true,
        "msg": true,
        "eml": true,
        "xlsb": true,
        "zip": true,
        "rar": true,
        "7z": true,
        "csv": true,
        "docx": true,
        "prn": true,
        "xla": true,
        "xps": true,
        "xlam": true,
        "slx": true,
        "dif": true,
        "xlt": true,
        "xltx": true,
        "xltm": true,
        "xml": true,
        "xls": true,
        "mht": true
    },
    uploadHelper: function(component, event) {
        component.set("v.showLoadingSpinner", true);

        var fileInput = component.find("fileId").get("v.files");
        var file = fileInput[0];
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", 'Alert : File size cannot exceed ' +
                self.MAX_FILE_SIZE + ' bytes.\n' +
                self.SELECTED_FILE_SIZE_LABEL + file.size);
            return;
        }

        var objFileReader = new FileReader();
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            self.uploadProcess(component, file, fileContents);
        });

        objFileReader.readAsDataURL(file);

    },

    uploadProcess: function(component, file, fileContents) {
        var startPosition = 0;
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        var filename = file.name;

        if(this.isValidFormat(filename)) {
            this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
            this.showToast('info', 'info', 'Document will not be uploaded unless save button is clicked.');
        } else {
            alert(this.INVALID_FILE_FORMAT_ERROR_LABEL);
            component.set("v.showLoadingSpinner", false);
            var indexvar = component.get("v.indexofDoc");
            var listofDocumentFile = component.get("v.listofDocumentFile");
            
            listofDocumentFile[indexvar]["filedata"] = null;
            listofDocumentFile[indexvar]["filename"] = null;

            component.set("v.listofDocumentFile", listofDocumentFile);
        }
    },
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        var getchunk = fileContents.substring(startPosition, endPosition);
        var documentFile = component.get("v.listofDocumentFile");
        var filename = component.get("v.fileName");
        var indexvar = component.get("v.indexofDoc");
        documentFile[indexvar]["filedata"] = getchunk;
        documentFile[indexvar]["filename"] = filename;

        component.set("v.listofDocumentFile", documentFile);
        component.set("v.showLoadingSpinner", false);
    },
    isValidFormat: function(fileName) {
        var parts = fileName.split('.');
        var fileExtension = parts[parts.length - 1]; 
        return this.MAP_OF_FILE_FORMAT[fileExtension]
    }
})