({
    getProjectStatus: function(component) {
        this.showSpinner(component);
        var approvalType = component.get("v.approvalType");
        var getProjectStatus;
        if (approvalType === 'Approval')
            getProjectStatus = component.get("c.getProjectStatus");
        else if (approvalType === 'Closure')
            getProjectStatus = component.get("c.getProjectStatusForClosure");
        getProjectStatus.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectStatus.setCallback(this, function(response) {
            this.getProjectStatusHandler(response, component);
        });

        $A.enqueueAction(getProjectStatus);
    },
    getProjectStatusHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        component.set("v.approvalRecieved", true);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectStatus(component, responseData);
        } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error",
                "type": "error",
                "message": responseData.message
            });
            toastEvent.fire();

        }
    },
    setProjectStatus: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        var isElligibleForApproval = responseJson.isElligibleForApproval;
        if (!isElligibleForApproval || responseJson["errorMessage"]) {
            var SerializedErrorMessage = responseJson["errorMessage"];
            var errorMessage;

            try {
                errorMessage = JSON.parse(SerializedErrorMessage);
            } catch(e) {
                component.set("v.additionalErrorMessage", SerializedErrorMessage);
                return;
            }

            var listOfFormattedError = this.getFormattedListOfError(errorMessage);
            component.set("v.errorMessage", listOfFormattedError);
        }

        this.fireApprovalValidation(responseJson.errorMessage);
        component.set("v.isElligibleForApproval", isElligibleForApproval);
        // component.set("v.errorMessage", responseJson.errorMessage);
        component.set("v.strGSTCode", responseJson.GSTCode);
        component.set("v.isAllFieldSeleted", responseJson.isAllFieldSeleted);
        component.set("v.strNullFields", responseJson.strNullFields);
        component.set("v.StageValues", responseJson.userApprovdProjectStage);
        component.set("v.lstChinaBandApprovers", responseJson.lstchinabandapprover);
        component.set("v.additionalApprovalreq", responseJson.additionalApprovalreq);
        
        var stageName = responseJson.userApprovdProjectStage;

        if (stageName && stageName.indexOf(',') >= 0) {
            stageName = stageName.split(',')[0];
        }
        component.set("v.userApprovdProjectStage", stageName);
    },
    fireApprovalValidation: function(message) {
        var validationUpdateEvent = $A.get("e.c:GPEvntApprovalValidationUpdate");
        validationUpdateEvent.setParams({
            "validationMessage": message
        });
        validationUpdateEvent.fire();
    },
    submitForApproval: function(component, event) {
        var approvalType = component.get("v.approvalType");
        var submitAction;
        if (approvalType === 'Approval')
            submitAction = component.get("c.submitforApproval");
        else if (approvalType === 'Closure')
            submitAction = component.get("c.submitforClosure");
        submitAction.setParams({
            "projectId": component.get("v.recordId"),
            "strStageValues": component.get("v.StageValues"),
            "userComments" : component.get("v.strComments"),
            "bandApprover" : component.get("v.selectedbandApprover")
        });
        submitAction.setCallback(this, function(response) {
            var responseData = response.getReturnValue();
            var toastEvent;

            if (responseData.isSuccess) {
                toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "type": "success",
                    "message": responseData.message
                });
                toastEvent.fire();

                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                });
                navEvt.fire();
            } else {
                toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "type": "error",
                    "message": responseData.message
                });
                toastEvent.fire();

                $A.get("e.force:closeQuickAction").fire();
            }
        });

        $A.enqueueAction(submitAction);
    }
})