({
    doInit: function(component, event, helper) {
        //component.set("v.isLoading", false);
         helper.getProjectStatus(component);
    },
    submitForApproval: function(component, event, helper) {
        var strComments = component.get("v.strComments");
        var commentDom = component.find("comment");
        var sizeError = component.find("sizeError");
        
        if ($A.util.isEmpty(strComments)) {
            $A.util.removeClass(commentDom, 'slds-hide');
            return;
        } else {
            $A.util.addClass(commentDom, 'slds-hide');
            // 28-12-18: DS-the comment length can't be more than 240 chars as 
            // GP_Approval_Requester_Comments__c field has db size limit.
            if(strComments.length > 240) {
                component.set("v.strCommentsLength", strComments.length);
                $A.util.removeClass(sizeError, 'slds-hide');
                return;
            } else {
                component.set("v.strCommentsLength", 0);
                $A.util.addClass(sizeError, 'slds-hide');
            }
        }
        
        var isaddtional = component.get("v.selectedbandApprover");
        var isaddtionalreq = component.get("v.additionalApprovalreq");
        var bandApproverDom = component.find("bandApprover");
        if (isaddtionalreq  && $A.util.isEmpty(isaddtional)) 
        {
            $A.util.removeClass(bandApproverDom, 'slds-hide');
            return;
        } 
        	else 
        {
            $A.util.addClass(bandApproverDom, 'slds-hide');
        }
        
        helper.submitForApproval(component, event);
    },
    checkForProjectStatus: function(component, event, helper) {
        helper.getProjectStatus(component);
    },
    closePopup: function(component, event, helper) {
        component.set('v.isSubmitforapproval', false);
        $A.get("e.force:closeQuickAction").fire();
    },
    hideSpinner: function(component) {
        component.set("v.isLoading", false);
    }
})