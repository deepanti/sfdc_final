({
	 init: function (component, event, helper) 
    {   
        var OpportunityList = component.get("v.OpportunityList");       
       	var Teststr = component.get("v.Teststr");
        var recordId = component.get("v.recordId"); 
        var conid = component.get("v.conid");  
        var OppWrapperList1 = [];
            OppWrapperList1 = component.get("v.OppWrapperList");  
       // alert('OppWrapperList-===11=in=='+OppWrapperList1.length);
        var OppWrapperListJSON = new Array();
		OppWrapperListJSON = JSON.stringify(component.get("v.OppWrapperList"));
       // alert('Loss survey ======OpportunityList======'+OpportunityList);
        //alert('Loss survey ======OppWrapperListJSON======'+OppWrapperListJSON.length);
      // var jsonRec =JSON.parse(JSON.stringify(OppWrapperList));
      //  alert('jsonRec-='+jsonRec[0]);
		var test=OppWrapperList1[0];     
        	var Opp_Name;
            var Opp_Source;
       		var OppSource;
            var Opp_DealType;
        	var Comp_Presence;
			var Opp_TCV;
			var Opp_NatureOfWork;
        for(var i=0;i<OppWrapperList1.length;i=i+1)
        {             
            var test1=OppWrapperList1[i];
            //alert('test======1111======'+JSON.stringify(test1));
            var jsnParse= JSON.parse(JSON.stringify(test1));
			//alert('-=-=-=-'+jsnParse.OpportunityDataList[0].Name);
            Opp_Name= jsnParse.OpportunityDataList[0].Name;
            Opp_Source= jsnParse.OpportunityDataList[0].Target_Source__c;  // old
            OppSource = jsnParse.OpportunityDataList[0].Opportunity_Source__c;  //new 
            Opp_DealType= jsnParse.OpportunityDataList[0].Annuity_Project__c;
            Comp_Presence=jsnParse.OpportunityDataList[0].Deal_Type__c;
			Opp_TCV = jsnParse.OpportunityDataList[0].TCV1__c;
			Opp_NatureOfWork = jsnParse.OpportunityDataList[0].Nature_Of_Work_OLI__c;
			//alert('Opp_NatureOfWork============'+Opp_NatureOfWork);
            //alert('Opp_TCV======1======'+Opp_TCV);          
        }
        
        component.set("v.Opp_Source",Opp_Source);
        component.set("v.OppSource",OppSource);
        component.set("v.Opp_DealType",Opp_DealType);
        component.set("v.Opp_Name",Opp_Name);
        component.set("v.Comp_Presence",Comp_Presence);
		component.set("v.Opp_TCV",Opp_TCV);
		component.set("v.Opp_NatureOfWork",Opp_NatureOfWork);
        helper.get_q3q4q5_val(component);
        helper.getClosestCompetitor(component);  
		helper.getClosestCompetitor1(component); 
		helper.getLossDealStage(component); 
		helper.getDealWinner(component); 
        
        
		//alert('=====Opp_NatureOfWork==='+ Opp_NatureOfWork);
		//alert('========Opp_TCV========='+Opp_TCV);       
		//alert('Comp_Presence===========1==============='+Comp_Presence);
		//alert('=============before conditions=====');
			  
			
			if(Comp_Presence == 'Sole Sourced')
			{	
				if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 && Opp_NatureOfWork )
				{	
					//alert('=======if Comp_Presence =====Sole Sourced=========');
					//if( (Opp_NatureOfWork.includes('Delivery') && Opp_TCV > 1000000 ) || ( Opp_NatureOfWork.includes('Delivery') && Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 1000000 )  )
					if(Opp_TCV >= 1000000 )
                    {
						//alert('======sole sourced====in===1===survey 6====');				
						component.set("v.IsSurvey6",true);
					}
					//else if(Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 100000 )
					else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services')  && Opp_TCV >= 100000 )
					{
						// alert('======sole sourced====in==else======1====survey 6===');			
						component.set("v.IsSurvey6",true);							
					}
					else
					{
						//alert('========sole sourced=======else=====survey 5a=');
						
						component.set("v.IsSurvey7",false);
						component.set("v.IsSurvey6",false);
						component.set("v.IsSurvey5a",true);
						component.set("v.IsSurvey5b",false);
					}
				}
				else
				{
					//alert('========sole sourced====final===else=====survey 5a=');
					component.set("v.IsSurvey7",false);
					component.set("v.IsSurvey6",false);
					component.set("v.IsSurvey5a",true);
					component.set("v.IsSurvey5b",false);
				}
			}
			else if(Comp_Presence == 'Competitive')
			{	
				//alert('======Competitive=========1=======');
				if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 && Opp_NatureOfWork)
				{
					//if( (Opp_NatureOfWork.includes('Delivery') && Opp_TCV > 1000000 ) || ( Opp_NatureOfWork.includes('Delivery') && Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 1000000 ) )
					if(Opp_TCV >= 1000000 )
                    {
						//alert('======Competitive====if=====1===survey 7===='+Opp_TCV);
						component.set("v.IsSurvey7",true);				
					}
					//else if(Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 100000)
					else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000)
                    {
						//alert('======Competitive======else=====1====survey 7===');				
						component.set("v.IsSurvey7",true);				
					}	
					else
					{
						//alert('========competitive=======else==survey 5b=');
						component.set("v.IsSurvey7",false);
						component.set("v.IsSurvey6",false);
						component.set("v.IsSurvey5a",false);
						component.set("v.IsSurvey5b",true);
						
					}	
				}
				else
				{
					//alert('========competitive=====final==else==survey 5b=');
					component.set("v.IsSurvey7",false);
					component.set("v.IsSurvey6",false);
					component.set("v.IsSurvey5a",false);
					component.set("v.IsSurvey5b",true);
				}
			}
			else
			{
				//alert('========else========survey 5a====');				
				component.set("v.IsSurvey7",false);
				component.set("v.IsSurvey6",false);
				component.set("v.IsSurvey5b",false);
				component.set("v.IsSurvey5a",true);
			}  
				
		/*else	
		{
			alert('===else tcv and nature of work not have values======');
			component.set("v.IsSurvey5a",false);
			component.set("v.IsSurvey5b",true);
			component.set("v.IsSurvey6",false);
			component.set("v.IsSurvey7",false);
		}	*/	
    },
        
     onGroup: function(cmp, evt) 
	 {
		var selected = evt.getSource().get("v.label");
		var selectedval= evt.getSource().get("v.value");
        var locid = evt.getSource().getLocalId();
		cmp.set("v.IsValidate",false);
         cmp.set("v.IsAllowed",true);
		var collection= cmp.get("v.SelectedOpt");
		collection.push(selectedval); 
        cmp.set("v.SelectedOpt",collection);
		
		var idcollection=cmp.get("v.selectedcmp");
		idcollection.push(locid);
        cmp.set("v.selectedcmp",idcollection);
		
		var Map = cmp.get("v.sectionLabels");
         Map[selected] = selectedval;
         cmp.set("v.sectionLabels",Map);
        
        
        
        var map=cmp.get("v.sectionLabels");    
        var count;
        var chkthrshold=0;
        var chknull=false; 
		for(var key in map)
        {	count=0;
         	if(map[key]!='')
            {
                chkthrshold++;
            }
            for(var key2 in map)
			{	
				if(map[key2]!='' || map[key]!='')
				{
					if(map[key2]==map[key])
					{	count++;
						
					}
				}
			}
           //alert(map[key]);  
		  // alert('=====count====='+count);
            if(count>1)
				cmp.set("v.IsValidate",true);
        
			if( map[key]=='')
				chknull=true;	
        }
		
		var size_sel=cmp.get("v.sectionLabels");
		//alert('=====size_sel.length====='+size_sel.length);
		//alert('=====chkthrshold========'+chkthrshold);
        if(chkthrshold>2)
        {	
			//alert('===========1=================');
            for(var i=0;i<21;i++)
                {	var c= 	cmp.find("selectItem"+i);
                     //alert(typeof c);
                     if(typeof c ==='undefined')
                     {
                         
                     }
                     else
                     {
                           cmp.find("selectItem"+i).set("v.disabled", true);
                           
                     }
                }            
        }
		//alert('======chkthrshold======'+chkthrshold);
        //alert( '==========isallowed ========= '+cmp.get("v.IsAllowed"));
		if(chkthrshold>0)
		{
			//alert('===========2==================');
			var c = cmp.get("v.IsValidate");
            if(c == false )
				cmp.set("v.IsAllowed",false);
		}
        var idcol=cmp.get("v.selectedcmp");
        //alert('=======idcol========='+idcol);
			for(var i in idcol )
				{
					cmp.find(idcol[i]).set("v.disabled", false);
				}       
	},
    
    handleClick:function(cmp,evt)
    {       
		//alert('Handle click============');
		cmp.set("v.isSuccess", true);
        var LongArea = cmp.find("comments");
     	var LongAreaVal = LongArea.get("v.value");
     //	alert('================='+LongAreaVal);
     	//alert('map========'+map)
        var map=cmp.get("v.sectionLabels");
        var custs = [];
        var count;
        for(var key in map)
        {	count=0;
            for(var key2 in map)
			{	
				if(map[key2]==map[key])
                {	count++;
                	//alert(map[key]);
                }
			}
           
            if(count>1)
            cmp.set("v.IsValidate",true);
        
        }   
        
		var Opp_DealOutCome = cmp.get("v.OpportunityList");
		//alert('=========1====Opp_DealOutCome======'+Opp_DealOutCome);
		var check_IsSurvey5a = cmp.get("v.IsSurvey5a");		
        var check_IsSurvey5b = cmp.get("v.IsSurvey5b");	
        var check_IsSurvey6 = cmp.get("v.IsSurvey6");		
        var check_IsSurvey7 = cmp.get("v.IsSurvey7");	
		//alert('=============1===========');
		//var in_ot = cmp.find("Incumbent_other").get("v.value");
		//alert('in_ot============'+in_ot);
		
	
		
		//alert('check_IsSurvey5a======1======'+ check_IsSurvey5a);
		//alert('check_IsSurvey5b======1======'+ check_IsSurvey5b);
		//alert('check_IsSurvey6======1======'+ check_IsSurvey6);
		//alert('check_IsSurvey7======1======'+ check_IsSurvey7);
		//alert('=======q5===ClosestCompetitorOther====='+cmp.get("v.selectedClosestCompetitorOthers"));
		//alert('=======q5===ClosestCompetitorOther_Percent====='+cmp.find("ClosestCompetitorOther_Percent").get("v.value"));
		
        if(check_IsSurvey5a != false)
		{	
			//alert('================in for survey 5a==============');
			if(LongAreaVal != 'undefined')
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);  // q2 
			}
			else
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", ' ');  // q2 
			}	
			cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 
			
			var UpdatewithWLR = cmp.get("c.saveWLR");
            UpdatewithWLR.setParams({
                             
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey5a",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
            });
			//alert('UpdatewithWLR======='+UpdatewithWLR);
        
            // Configure the response handler for the action
            UpdatewithWLR.setCallback(this, function(response) 
             {
                var state = response.getState();
                 //alert('state======1===survey 5a='+state);
                if(cmp.isValid() && state === "SUCCESS") 
                {
                   // alert('====save success========survey 5a');
				   cmp.set("v.isSuccess", false);
						 var recordId = cmp.get("v.recordId");                   
        				//sforce.one.navigateToSObject(recordId,"Opportunity");    
						function isLightningExperienceOrSalesforce1() 
						{
							return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
						} 
						if(isLightningExperienceOrSalesforce1()) 
						{                       
							sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
						}
						else
						{
							// window.location = '/'+recordId;   //'/006/o'
							//window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                           //window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
							window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                        }
                }
                else if (state === "ERROR") {
                    console.log('Problem saving WLR, response state: ' + state);
                     // alert('====error ========survey 5a');
                 
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                   // alert('in save');
                }
            });
			$A.enqueueAction(UpdatewithWLR);
			//cmp.set("v.isLoading", false);
			
		}
		else if(check_IsSurvey5b != false)
		{
			//alert('================in for survey 5b==============');
			var checkVal_DealWinner_Others = cmp.find("DealWinner_others").get("v.value");
					var DealWinnerOthersQ5 = cmp.get("v.selectedDealWinner");	//selectedDealWinner	
				var check_If_Deal_Winner_other = cmp.get("v.If_Deal_Winner_other");
				//alert('DealWinnerOthersQ5========'+DealWinnerOthersQ5);
				//alert('=============checkVal_DealWinner_Others============'+checkVal_DealWinner_Others);				
				//alert('====2=check_If_Deal_Winner_other====='+check_If_Deal_Winner_other); 
				
			if(cmp.get("v.selectedDealWinner") == '' || ((DealWinnerOthersQ5 == 'Others' || DealWinnerOthersQ5.includes('Others') ) &&  checkVal_DealWinner_Others == null) )			
			{
				 cmp.set("v.isSuccess", false);
				alert('Please fill all the mandatory questions 1,3 for survey');				
			}
			else
			{
                
				if(LongAreaVal != 'undefined')
				{
					cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);   // q2 
				}
				else
				{
					cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", ' ');  // q2 
				}
				
				cmp.set("v.newOppSurveyRecords.Deal_Winner__c", cmp.get("v.selectedDealWinner")); //q3	
				cmp.set("v.newOppSurveyRecords.Deal_Winner_Others__c", cmp.find("DealWinner_others").get("v.value"));				
				
				cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 
			
				var UpdatewithWLR = cmp.get("c.saveWLR");
				UpdatewithWLR.setParams({
				  					
					"oppId": cmp.get("v.recordId"),
					"conid": cmp.get("v.conid"),
                    "SurveyNumber": "Survey5b",
					"maps": cmp.get("v.sectionLabels"),
					"OppSurvey": cmp.get("v.newOppSurveyRecords")
				});
							
				// Configure the response handler for the action
				UpdatewithWLR.setCallback(this, function(response) 
				 {
					var state = response.getState();
					 //alert('state======1===survey 5b==='+state);
					if(cmp.isValid() && state === "SUCCESS") 
					{
						//alert('====save success====survey 5b=');
						cmp.set("v.isSuccess", false);
							 var recordId = cmp.get("v.recordId");                   
							//sforce.one.navigateToSObject(recordId,"Opportunity");   
							function isLightningExperienceOrSalesforce1() 
							{
								return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
							} 
							if(isLightningExperienceOrSalesforce1()) 
							{      
                                
								sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
							}
							else
							{
                                
								// window.location = '/'+recordId;   //'/006/o'
                               // window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                              // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
							window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                            } 
					}
					else if (state === "ERROR") {
						console.log('Problem saving WLR, response state: ' + state);
                          //alert('====error ========survey 5b');
					 
					}
					else {
						console.log('Unknown problem, response state: ' + state);
					   // alert('in save');
					}
				});
				$A.enqueueAction(UpdatewithWLR);
				//cmp.set("v.isLoading", false);				
			}			
		}
		else if(check_IsSurvey6 != false)
		{
			//alert('================in for survey 6===============')
			//alert('=====LongAreaVal===q2=====survy 6======='+LongAreaVal);
			//alert('=====selectedLossDealStage===q3=====survy 6======='+cmp.get("v.selectedLossDealStage"));
			//alert('=======cmp.get("v.selectedIncumbent")======q4=====survy 6==========='+cmp.get("v.selectedIncumbent"));
			//alert('====cmp.get("v.selectedQ4_G_invlvmnt")====q5=====survy 6==========='+cmp.get("v.selectedQ4_G_invlvmnt"));
			//alert('====cmp.get("v.selectedQ5_Interaction_w_Prospct")===q6=====survy 6==========='+cmp.get("v.selectedQ5_Interaction_w_Prospct"));
			
			//====cmp.get("v.selectedQ4_G_invlvmnt")====q4=====survy 3===========--None--
			//====cmp.get("v.selectedQ5_Interaction_w_Prospct")===q5=====survy 3===========
		
					var checkIncumbentQ4 = cmp.get("v.selectedIncumbent");	//selectedIncumbent	
					var checkVal_Incumbent_other = cmp.find("Incumbent_other").get("v.value");
			
				//alert('checkIncumbentQ4========'+checkIncumbentQ4);
				//alert('=========checkVal_Incumbent_other============'+checkVal_Incumbent_other);				
				
			//alert('======before man validation=====LongAreaVal=='+LongAreaVal);
			if( ( (LongAreaVal == 'undefined' || LongAreaVal == null )|| cmp.get("v.selectedLossDealStage") == '' || cmp.get("v.selectedIncumbent") == '' || cmp.get("v.selectedQ4_G_invlvmnt") == '' || cmp.get("v.selectedQ5_Interaction_w_Prospct") == '') || (checkIncumbentQ4 == 'Others'  &&  (checkVal_Incumbent_other == 'undefined' || checkVal_Incumbent_other == null ) ) )			
			{
				cmp.set("v.isSuccess", false);
				alert('Please fill all the mandatory questions 1 to 6 for survey');				
			}
			else
			{
				//alert('survey 6 ---------else with save========');
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);   // q2  
				cmp.set("v.newOppSurveyRecords.Loss_Deal_Stage__c", cmp.get("v.selectedLossDealStage"));   // q3
				cmp.set("v.newOppSurveyRecords.Incumbent__c", cmp.get("v.selectedIncumbent"));  // q4
				cmp.set("v.newOppSurveyRecords.Incumbent_Others__c", cmp.find("Incumbent_other").get("v.value"));
				
				cmp.set("v.newOppSurveyRecords.Genpact_Involvement_in_deal_with_prospec__c", cmp.get("v.selectedQ4_G_invlvmnt"));     // q5      
				cmp.set("v.newOppSurveyRecords.interactions_did_we_have_with_the_prospe__c", cmp.get("v.selectedQ5_Interaction_w_Prospct") );  // q6
				cmp.set("v.newOppSurveyRecords.If_advisor_was_involved__c", cmp.find("Q6_Advisor").get("v.value"));	 // q7
				cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome);  // WLD
				//UpdateOppRecords(cmp,evt);
				// ======For Update==================
				// alert('v.recordId=====Survey 3=='+cmp.get("v.recordId")); // opp id
				//alert('v.sectionLabels====Survey 3==='+JSON.stringify(cmp.get("v.sectionLabels")));  // ranking answer    
				//alert('v.newOppSurveyRecords====Survey 3==='+ JSON.stringify(cmp.get("v.newOppSurveyRecords")) );  // all others fields
				
				var UpdatewithWLR = cmp.get("c.saveWLR");
				UpdatewithWLR.setParams({
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey6",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
				});
			      
				// Configure the response handler for the action
				UpdatewithWLR.setCallback(this, function(response) 
				 {
					var state = response.getState();
					// alert('state======1===='+state);
					if(cmp.isValid() && state === "SUCCESS") 
					{
						//alert('====save success==');
						cmp.set("v.isSuccess", false);
							 var recordId = cmp.get("v.recordId");                   
							//sforce.one.navigateToSObject(recordId,"Opportunity");   
							function isLightningExperienceOrSalesforce1() 
							{
								return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
							} 
							if(isLightningExperienceOrSalesforce1()) 
							{                       
								sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
							}
							else
							{
								// window.location = '/'+recordId;   //'/006/o'
                               // window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                             // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
							window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                            } 
                        
					}
					else if (state === "ERROR") {
						console.log('Problem saving WLR, response state: ' + state);
                       // alert('====error ========survey 6');
					 
					}
					else {
						console.log('Unknown problem, response state: ' + state);
					   // alert('in save');
					}
				});
				$A.enqueueAction(UpdatewithWLR);
				//cmp.set("v.isLoading", false);
			}	
		}
		else if(check_IsSurvey7 != false)
		{	            
			//alert('================in for survey 7===============')
				
			var CheckVal_GenpactProposedPriceDifferPercentage = cmp.find("ClosestCompetitorOther_Percent").get("v.value");
			var checkVal_ClosestCompetitor2 = cmp.find("ClosestCompetitor2").get("v.value"); 	
			var checkVal_Competitor_Strength1 = cmp.find("Competitor_Strength1").get("v.value"); //q7
			var checkVal_Competitor_weakness1 = cmp.find("Competitor_weakness1").get("v.value");
			var checkVal_Competitor_Strength2 = cmp.find("Competitor_Strength2").get("v.value");
			var checkVal_Competitor_weakness2 = cmp.find("Competitor_weakness2").get("v.value");

			//alert('====ClosestCompetitor1====q7=====survey 7====='+cmp.get("v.selectedClosestCompetitor1"));
		//alert('====ClosestCompetitor2")===q7=====survey 7====='+cmp.find("ClosestCompetitor2").get("v.value"));			
			//alert('=======checkVal_Competitor_Strength1 ==== q7=== survey 7======='+checkVal_Competitor_Strength1);
			//alert('=======checkVal_Competitor_weakness1 ==== q7=== survey 7======='+checkVal_Competitor_weakness1);
			//alert('=======checkVal_Competitor_Strength2 ==== q7=== survey 7======='+checkVal_Competitor_Strength2);
			//alert('=======checkVal_Competitor_weakness2 ==== q7=== survey 7======='+checkVal_Competitor_weakness2);
			
			var checkIncumbentQ4 = cmp.get("v.selectedIncumbent");	
			var checkVal_Incumbent_other = cmp.find("Incumbent_other").get("v.value");
			var checkDealWinnerQ5 = cmp.get("v.selectedDealWinner");	
			var checkVal_DealWinner_others = cmp.find("DealWinner_others").get("v.value");
			var checkClosestCompetitorPerQ5 = cmp.get("v.selectedClosestCompetitorOthers"); 
			//alert('checkClosestCompetitorPerQ5========'+checkClosestCompetitorPerQ5);
	//alert('======CheckVal_GenpactProposedPriceDifferPercentage====survey 7==='+CheckVal_GenpactProposedPriceDifferPercentage);
	
						
			if( ( (LongAreaVal == 'undefined' || LongAreaVal == null ) || cmp.get("v.selectedLossDealStage") == ''  || cmp.get("v.selectedIncumbent") == '' || cmp.get("v.selectedDealWinner") == '' || cmp.get("v.selectedClosestCompetitorOthers") == '' || cmp.get("v.selectedClosestCompetitor1") == '' || checkVal_ClosestCompetitor2 == 'undefined' || checkVal_Competitor_Strength1 == 'undefined' || checkVal_Competitor_Strength1 == null  || checkVal_Competitor_weakness1 == 'undefined' || checkVal_Competitor_weakness1 == null  || cmp.get("v.selectedQ4_G_invlvmnt") == '' || cmp.get("v.selectedQ5_Interaction_w_Prospct") == '' ) || (checkIncumbentQ4 == 'Others'  &&  (checkVal_Incumbent_other == 'undefined' || checkVal_Incumbent_other == null ) ) || (checkDealWinnerQ5 == 'Others'  &&  (checkVal_DealWinner_others == 'undefined' || checkVal_DealWinner_others == null ) ) || ( (checkClosestCompetitorPerQ5 == 'Genpact’s price was higher than closest competitor' || checkClosestCompetitorPerQ5 == 'Genpact’s price was lower than closest competitor')  &&  CheckVal_GenpactProposedPriceDifferPercentage == null ) )			
			{
				cmp.set("v.isSuccess", false);
				alert('Please fill all the mandatory questions 1 to 9 for survey');
				
			}
			else
			{	
               //alert('survey 7 ---------else with save========');
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal); // q2  
				cmp.set("v.newOppSurveyRecords.Loss_Deal_Stage__c", cmp.get("v.selectedLossDealStage"));   // q3
				cmp.set("v.newOppSurveyRecords.Incumbent__c", cmp.get("v.selectedIncumbent")); //q4
				cmp.set("v.newOppSurveyRecords.Incumbent_Others__c", cmp.find("Incumbent_other").get("v.value"));			
				cmp.set("v.newOppSurveyRecords.Deal_Winner__c", cmp.get("v.selectedDealWinner")); //q5	
				cmp.set("v.newOppSurveyRecords.Deal_Winner_Others__c", cmp.find("DealWinner_others").get("v.value"));
				
				cmp.set("v.newOppSurveyRecords.Genpact_s_proposed_price_differ__c", cmp.get("v.selectedClosestCompetitorOthers")); //q5
				cmp.set("v.newOppSurveyRecords.Genpact_s_proposed_price_differ_Percent__c", CheckVal_GenpactProposedPriceDifferPercentage); 
				
				
				//alert('------survey 7 ---closest competitor 1----'+cmp.get("v.selectedClosestCompetitor1"));
				//alert('------survey 7 ---closest competitor 1----'+cmp.get("v.selectedClosestCompetitors1"));
				
				
						
				cmp.set("v.newOppSurveyRecords.Closest_Competitor_1__c", cmp.get("v.selectedClosestCompetitors1"));  //q7
				cmp.set("v.newOppSurveyRecords.Closest_Competitor_2__c", cmp.find("ClosestCompetitor2").get("v.value"));	 			
				
				if(checkVal_Competitor_Strength1 ==='undefined' || checkVal_Competitor_Strength1 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_strengths1__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_strengths1__c", cmp.find("Competitor_Strength1").get("v.value"));
				}
				if(checkVal_Competitor_weakness1 ==='undefined' || checkVal_Competitor_weakness1 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses1__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses1__c", cmp.find("Competitor_weakness1").get("v.value"));
				}
				if(checkVal_Competitor_Strength2 ==='undefined' || checkVal_Competitor_Strength2 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_strengths2__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_strengths2__c", cmp.find("Competitor_Strength2").get("v.value"));
				}
				if(checkVal_Competitor_weakness2 ==='undefined' || checkVal_Competitor_weakness2 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses2__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses2__c", cmp.find("Competitor_weakness2").get("v.value"));
				}					
							
				cmp.set("v.newOppSurveyRecords.Genpact_Involvement_in_deal_with_prospec__c", cmp.get("v.selectedQ4_G_invlvmnt"));  // q8     
				cmp.set("v.newOppSurveyRecords.interactions_did_we_have_with_the_prospe__c", cmp.get("v.selectedQ5_Interaction_w_Prospct") ); //q9
				cmp.set("v.newOppSurveyRecords.If_advisor_was_involved__c", cmp.find("Q6_Advisor").get("v.value")); //q10	
				cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome);   //WLD  

				// ====== For Update ==================
				// alert('v.recordId=====Survey 7=='+cmp.get("v.recordId")); // opp id
				// alert('v.sectionLabels====Survey 7==='+JSON.stringify(cmp.get("v.sectionLabels")));  // ranking answer    
				// alert('v.newOppSurveyRecords====Survey 7==='+ JSON.stringify(cmp.get("v.newOppSurveyRecords")) );  // all others fields
				
				var UpdatewithWLR = cmp.get("c.saveWLR");
				UpdatewithWLR.setParams({
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey7",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
				});
			      
				// Configure the response handler for the action
				UpdatewithWLR.setCallback(this, function(response) 
				 {
					var state = response.getState();
					// alert('state======1==survey 7=='+state);
					if(cmp.isValid() && state === "SUCCESS") 
					{
						//alert('====save success=====survey 7=');
						cmp.set("v.isSuccess", false);
							 var recordId = cmp.get("v.recordId");                   
							//sforce.one.navigateToSObject(recordId,"Opportunity");     
							function isLightningExperienceOrSalesforce1() 
							{
								return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
							} 
							if(isLightningExperienceOrSalesforce1()) 
							{                       
								sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
							}
							else
							{
								//window.location = '/'+recordId;   //'/006/o'
                               // window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                             // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
							
                            window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';} 
					}
					else if (state === "ERROR") {
						console.log('Problem saving WLR, response state: ' + state);
                       // alert('====error ========survey 7');
					 
					}
					else {
						console.log('Unknown problem, response state: ' + state);
					   // alert('in save');
					}
				});
				$A.enqueueAction(UpdatewithWLR);
				//cmp.set("v.isLoading", false);			
			}			
		}
    },
    
    handleCancel: function(component, event, helper) 
    {      
         	var recordId = component.get("v.recordId");  
    		//sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
			//sforce.one.back(true);
			function isLightningExperienceOrSalesforce1() 
			 {
				return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
			} 
			if(isLightningExperienceOrSalesforce1()) 
			{   
				//sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
				sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 
				sforce.one.back(true);
			}
			else
			{
				window.location = '/'+recordId;   //'/006/o'
			} 
    },
    
    reset: function(cmp,evt)
	{	
        cmp.set("v.IsValidate",false);
        for(var i=0;i<21;i++)
                {	var c= 	cmp.find("selectItem"+i);
                 //alert(typeof c);
                 if(typeof c ==='undefined')
                 {
                     
                 }
                 else 
                    {
                        cmp.find("selectItem"+i).set("v.disabled", false);
                      	cmp.find("selectItem"+i).set("v.value", null);
                    }
                    //alert("selectItem"+i);
                }
				cmp.set("v.IsAllowed",true);
                var Map = cmp.get("v.sectionLabels");
                Map={};
                
        		cmp.set("v.sectionLabels",Map);
        		var lst= cmp.get("v.selectedcmp");
        		lst=[];
        		cmp.set("v.selectedcmp",lst);
				
				
				var checkReset_IsSurvey5a = cmp.get("v.IsSurvey5a");		
				var checkReset_IsSurvey5b = cmp.get("v.IsSurvey5b");	
				var checkReset_IsSurvey6 = cmp.get("v.IsSurvey6");		
				var checkReset_IsSurvey7 = cmp.get("v.IsSurvey7");
				
				if(checkReset_IsSurvey5a == true)
				{
					cmp.find("comments").set("v.value", "");	
				}
				else if(checkReset_IsSurvey5b == true)
				{
					cmp.find("comments").set("v.value", "");
					cmp.find("DealWinner").set("v.value", null); 	
					cmp.find("DealWinner_others").set("v.value", "");	
				}
				else if(checkReset_IsSurvey6 == true)
				{
					//alert('=====3=======');
					cmp.find("comments").set("v.value", "");				
					cmp.find("Incumbent_other").set("v.value", "");
					cmp.set("v.If_Incumbent_other",true);
					cmp.find("Q6_Advisor").set("v.value", "");					
					cmp.find("Incumbent").set("v.value", null);
					cmp.find("Q4_G_invlvmnt").set("v.value", null);
					cmp.find("Q5_Interaction_w_Prospct").set("v.value", null);
				}
				else if(checkReset_IsSurvey7 == true)
				{
					//alert('=====survey 7 call reset=======');
					cmp.find("comments").set("v.value", "");								
					cmp.find("Incumbent_other").set("v.value", "");						
					cmp.set("v.If_Incumbent_other",true);				
					cmp.find("Q6_Advisor").set("v.value", "");											
					cmp.find("Incumbent").set("v.value", null);						
					cmp.find("Q4_G_invlvmnt").set("v.value", null);						
					cmp.find("Q5_Interaction_w_Prospct").set("v.value", null);				
					cmp.find("DealWinner").set("v.value", null); 					
					cmp.find("ClosestCompetitor1").set("v.value", null); 					
					cmp.set("v.selectedClosestCompetitors1",null); 					
					cmp.find("ClosestCompetitorOther").set("v.value", null);				
					cmp.find("ClosestCompetitorOther_Percent").set("v.value", "");					
					cmp.find("DealWinner_others").set("v.value", "");						
					cmp.set("v.If_Closest_Competitors_other",true); 					
					cmp.set("v.If_Deal_Winner_other",true); 
					cmp.set("v.If_ClosestCompetitor_other",true); 					
					cmp.find("Competitor_Strength1").set("v.value", "");						
					cmp.find("Competitor_weakness1").set("v.value", "");											
					cmp.find("ClosestCompetitor1").set("v.value", null);						
					cmp.find("ClosestCompetitor2").set("v.value", null);						
					cmp.find("Competitor_Strength2").set("v.value", "");					
					cmp.find("Competitor_weakness2").set("v.value", "");										
				}		
				//alert('=======reset done======') ;
			 
    },	

    enabled_Incumbnt_other:function(cmp,evt)
    {
    	var incumbnt = cmp.get("v.selectedIncumbent");
		if(incumbnt == 'Others' || incumbnt.includes('Others') )
			cmp.set("v.If_Incumbent_other",false);
		else
			cmp.set("v.If_Incumbent_other",true);
    
	},
    enabled_ClosestCompititor_other:function(cmp,evt) //for percentage q5
    {
    	var ClosestCompetitorOthers = cmp.get("v.selectedClosestCompetitorOthers");
		//alert('ClosestCompetitorOthers=====000000======'+ClosestCompetitorOthers);
		if(ClosestCompetitorOthers == 'Genpact’s price was higher than closest competitor' || ClosestCompetitorOthers == 'Genpact’s price was lower than closest competitor')
			cmp.set("v.If_ClosestCompetitor_other",false);
		else
			cmp.set("v.If_ClosestCompetitor_other",true); 
    
	},
	enabled_Closest_CompititorQ4_other:function(cmp,evt) //for closest competitor q4
    {
		//alert('======deal winner=====');
		//alert('IsClosestCompetitorFlag======strt==================='+ cmp.get("v.IsClosestCompetitorFlag"));
    	var ClosestCompetitorOthersQ4 = cmp.get("v.selectedClosestCompetitor");
		var DealWinnerOthersQ5 = cmp.get("v.selectedDealWinner");	//selectedDealWinner	
		//alert('deal winner selected=====1======'+DealWinnerOthersQ5 );
		if(DealWinnerOthersQ5 == 'Others' || DealWinnerOthersQ5.includes('Others') )
		{
			//alert('=======if=======');
			cmp.set("v.If_Closest_Competitors_other",false);
			cmp.set("v.If_Deal_Winner_other",false);
			cmp.set("v.IsClosestCompetitorFlag",false);				
		}		
		else
		{
			//alert('=======else=======');
			cmp.set("v.If_Closest_Competitors_other",true); 
			cmp.set("v.If_Deal_Winner_other",true);
			cmp.set("v.IsClosestCompetitorFlag",true);				
			cmp.set("v.selectedClosestCompetitors1",DealWinnerOthersQ5); 	
		}   
		//alert('=====select==final====='+ cmp.get("v.selectedClosestCompetitors1"));		
	},
	handleBlur_ClosestCompetitorsOthers : function(cmp, event)
	{	
		cmp.set("v.selectedClosestCompetitors1",cmp.find("DealWinner_others").get("v.value")); 
	} 
})