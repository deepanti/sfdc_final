({
	get_q3q4q5_val : function(component)
   	 {
        var action = component.get("c.getIncumbent");
        action.setCallback(this, function(response)
         {            
            component.set("v.Incumbent", response.getReturnValue());           
         });
        $A.enqueueAction(action);
		
		var action1 = component.get("c.getGenpact_Involvement_in_deal_with_prospec");
        action1.setCallback(this, function(response)
         {            
            component.set("v.Q4_G_invlvmnt", response.getReturnValue());           
         });
        $A.enqueueAction(action1);
		
		var action2 = component.get("c.getinteractions_did_we_have_with_the_prospe");
        action2.setCallback(this, function(response)
         {            
            component.set("v.Q5_Interaction_w_Prospct", response.getReturnValue());           
         });
        $A.enqueueAction(action2);
    } ,
     getClosestCompetitor : function(component)
   	 {
        var action = component.get("c.getClosestCompetitor");
        action.setCallback(this, function(response)
         {            
            component.set("v.ClosestCompetitor", response.getReturnValue());           
         });
        $A.enqueueAction(action);
    },
    getClosestCompetitor1 : function(component)
   	 {
        var action = component.get("c.getClosestCompetitor1");
        action.setCallback(this, function(response)
         {            
            component.set("v.ClosestCompetitor1", response.getReturnValue());  
            component.set("v.ClosestCompetitor2", response.getReturnValue());
         });
        $A.enqueueAction(action);
    },
    getLossDealStage : function(component)
   	 {
        var action = component.get("c.getLossDealStage");
        action.setCallback(this, function(response)
         {            
            component.set("v.LossDealStage", response.getReturnValue());              
         });
        $A.enqueueAction(action);
    },
    
    getDealWinner : function(component)
   	 {
        var action = component.get("c.getDealWinner");
        action.setCallback(this, function(response)
         {            
            component.set("v.DealWinner", response.getReturnValue());              
         });
        $A.enqueueAction(action);
    }
    
    
})