({
	 handleActiveFromParent : function(component, event, helper) {
        component.set("v.isEventFired",true);
        component.set("v.TabIdFromEvent",event.getParams("tabNo").arguments.tabNo);
    },
})