({
    
    //to check all Campaign Members on select All
    checkAllCampaignMembers: function(component, event, helper){
        if(component.get("v.assignAll"))
        {
            component.set("v.campMemberWrap.isSelected",true);
        }
        else
        {
            if(!component.get("v.autoAssignAllRemoval"))
                component.set("v.campMemberWrap.isSelected",false);
        }
        
    },
    //to assign Campaign Members
    assignCampaignMember: function(component, event, helper) {
        var wrapperId = component.get("v.campMemberWrap.wrapperId");
        component.set("v.campMemberWrap.isSelected",event.getSource().get("v.checked"));
        var isSelected = event.getSource().get("v.checked");
        var appEvent = $A.get("e.c:InsideSalesItemEvent");        
        appEvent.setParams({
            "wrapperId" : wrapperId,
            "isSelected" : isSelected,
        });
        appEvent.fire(); 
    },
    //to go To Account Detail on clicking Company name
    goToAccountDetail:function(component, event, helper){
        var sObjectEvent = $A.get("e.force:navigateToSObject");
        sObjectEvent.setParams({
            "recordId":  component.get("v.campMemberWrap.accountId"),
            "slideDevName": 'related'
        })
        sObjectEvent.fire();
       
    }
    
})