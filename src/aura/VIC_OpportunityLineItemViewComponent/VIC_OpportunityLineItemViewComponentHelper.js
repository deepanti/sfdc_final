({
	saveHelper : function(component,event) {
		 var action = component.get("c.setOLI");
        
        action.setParams({
            "cpqDealStatus": component.find("cpqDealStatusSelected").get("v.value"),
            "tcvEbitCon" : component.find("tcvEbitConSelected").get("v.value"),
            "user3Id": component.get("v.selItem1.val"),
            "user4Id": component.get("v.selItem2.val"),
            "TvAcV": component.get("v.OLIRecord.vic_TCV_Accelerator_Variable_Credit__c"),
            "oliToUpdate" : component.get("v.OLIRecord")
            
        });
         action.setCallback(this, function(response){
            var state = response.getState();
             alert('state');
            if (state === "SUCCESS") {
             alert('SUCCESS'); 
               
            }
             else if(state === "ERRROR"){
                  alert('error');
             }  
        });
        $A.enqueueAction(action);
    },
    doInitHelper:function(component,event){
        var action = component.get("c.getOLI");
        component.set("v.recordId",component.get("v.recordId"));
        action.setParams({
            "oliId": component.get("v.recordId")
        });
         action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('==='+JSON.stringify(response.getReturnValue()));
                
                component.set("v.OLIRecord",response.getReturnValue().oli);
                component.set("v.user3Id",response.getReturnValue().user3Id);
                component.set("v.user4Id",response.getReturnValue().user4Id);
   
                if(response.getReturnValue().user3Id != undefined &&
                   response.getReturnValue().user3Id != 'undefined')
                	component.set("v.user3",response.getReturnValue().user3);
                if(response.getReturnValue().user4Id != undefined &&
                   response.getReturnValue().user4Id != 'undefined')
                	component.set("v.user4",response.getReturnValue().user4);               
                if(response.getReturnValue().user3Id != undefined && 
                    response.getReturnValue().user3Id != 'undefined' ){
                    var jsonUser = '{\"val\":\"'+response.getReturnValue().user3Id+'\",\"text\":\"'+response.getReturnValue().oli.vic_VIC_User_3__r.Name+'\",\"objName\":\"User\"}';
                    component.set("v.selItem1",JSON.parse(jsonUser));
                }
           
                if(response.getReturnValue().user4Id != undefined && 
                    response.getReturnValue().user4Id != 'undefined' ){
                    var jsonUser = '{\"val\":\"'+response.getReturnValue().user4Id+'\",\"text\":\"'+response.getReturnValue().oli.vic_VIC_User_4__r.Name+'\",\"objName\":\"User\"}';
                    component.set("v.selItem2",JSON.parse(jsonUser));
                }
                component.set("v.cpqDealStatusValues",response.getReturnValue().cpqDealStatusValues);
                component.set("v.tcvEbitConValues",response.getReturnValue().tcvEbitConValues);
                component.set("v.ContTerm",response.getReturnValue().ContTerm);
                component.set("v.SalesrepApproValValues",response.getReturnValue().SalesrepApproValValues);
                
               
            }
        });
        $A.enqueueAction(action);
	}
})