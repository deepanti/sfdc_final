({
	toggleOpportunities : function(component, event, helper) {
		var isOppsOpen = component.get('v.isOppsOpen');
        component.set('v.isOppsOpen', !isOppsOpen);
	},
    checkSelectedOPPStatus : function(component, event, helper){	//Aura Event
        //alert('parent');   
        var oppCount = 0;
        var oppCheckedCount = 0;
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            oppCount++;
            if(objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked){
                oppCheckedCount++;
            }
        }
        if(oppCount == oppCheckedCount){
        	component.set("v.objUserDataWrapLine.isUserChecked",true);
        }else{
            component.set("v.objUserDataWrapLine.isUserChecked",false);
        }
    },
    selectNUnselectAll : function(component, event, helper){ 
        var oppCount = 0;
        var statusOfSelectAll = event.getSource().get("v.value");
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
            for(var eachOLIData in objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
            }
        }
        component.set("v.objUserDataWrapLine",objUserDataWrap);
    }
})