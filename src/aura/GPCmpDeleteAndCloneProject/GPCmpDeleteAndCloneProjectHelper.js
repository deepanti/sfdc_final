({
	deleteAndCloneProject: function(component) {
        var cloneAndDeleteProjectService = component.get("c.cloneAndDeleteProjectService");
        cloneAndDeleteProjectService.setParams({
            "strProjectId": component.get("v.recordId") 
        });
        this.showSpinner(component);
        cloneAndDeleteProjectService.setCallback(this, function(response) {
            this.cloneAndDeleteProjectServiceHandler(component, response);            
        });
        
        $A.enqueueAction(cloneAndDeleteProjectService);
    }, 
    cloneAndDeleteProjectServiceHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('success', 'success', 'Record unlocked and status changed to draft successfully');
            this.redirectToSobject(responseData.response, "related");
            this.fireProjectSaveEvent();

        } else {
            this.handleFailedCallback(component, responseData);
        }
    }
})