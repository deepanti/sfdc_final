({
	deleteAndCloneProject: function(component, event, helper) {
        helper.deleteAndCloneProject(component); 
    },
    closePopup : function(component, event, helper) {
    	component.set('v.isCloneAndDelete',false);
    }
})