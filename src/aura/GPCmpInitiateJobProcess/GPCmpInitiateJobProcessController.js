({
    doInit: function(component, event, helper) {
        helper.queueJob(component);
    },
    closeWindow: function(component, event, helper) {
        helper.closeView(component);
    }
})