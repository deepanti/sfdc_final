({
    queueJob: function(component) {
        var queueJobService = component.get("c.queueJob");
        queueJobService.setParams({
            "jobId": component.get("v.recordId")
        });
        queueJobService.setCallback(this, function(response) {
            this.queueJobServiceHandler(response, component);
        });

        $A.enqueueAction(queueJobService);
    },
    queueJobServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.responseCodeWithMessage", JSON.parse(responseData.response).responseCodeWithMessage)
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    closeView: function(response, component) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get('e.force:refreshView').fire();
    }
})