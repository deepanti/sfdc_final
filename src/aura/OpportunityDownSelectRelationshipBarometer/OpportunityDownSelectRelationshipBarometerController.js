({
    recordUpdated : function(component, event, helper){
        var changeType = event.getParams().changeType;
        if (changeType === "LOADED") { 
            /* handle record load */						
            var MaxSelectedTabId = component.get("v.OppRecord").LatestTabId__c;
           // alert('recload '+MaxSelectedTabId)
            //Update the tab selected on record load
            component.set("v.MaxSelectedTabId", MaxSelectedTabId);
        }
    },
	doInit:function(component, event, helper) {
        // to show spinner on click of component & before load of component 
        helper.showSpinner(component, event, helper);
        helper.createGCICompononent(component, event, helper,$A.get("$Label.c.OpportunityRelationshipBarometer"));
        helper.hideSpinner(component, event, helper);
    },
    save : function(component, event, helper) {
        component.set("v.Spinner", true);
        component.find("recordViewForm").submit();
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully');
        component.set("v.Spinner", false);
        localStorage.setItem('LatestTabId','15');
    },
    saveAndNext : function(component, event, helper) {
        	component.set("v.Spinner", true);
        if(localStorage.getItem('MaxTabId')<16){
            	
            	localStorage.setItem('MaxTabId',16);
                component.set("v.MaxSelectedTabId", "16");
        }
            component.find("recordViewForm").submit();
        	localStorage.setItem('LatestTabId','16');
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "16" });
            compEvents.fire();
            helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully');
            component.set("v.Spinner", false);
        	window.scrollTo(0, 0);
    },
    back : function(component, event, helper) {
        	localStorage.setItem('LatestTabId',14);
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "14"  });
            compEvents.fire();
        	window.scrollTo(0, 0);
    },
})