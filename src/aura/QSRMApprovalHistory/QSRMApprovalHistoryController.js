({
	doInit : function(component, event, helper) {
        var recID = component.get("v.recordId");
        var action = component.get("c.getProcessInstancesmethod");
    	action.setParams({ "QsrmID" : recID});
		var self = this;
		action.setCallback(this, function(actionResult) {
    		component.set('v.QSRMHistoryList', actionResult.getReturnValue());
		});
		$A.enqueueAction(action);
     }
})