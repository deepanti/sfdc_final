({
    doInit: function(component, event, helper) {
        component.set("v.massResourceAllocationObject", {});
        component.set("v.searchResourceAllocationObject", {});

        if ($A.get("$Label.c.GP_No_of_rows_display_in_Resource") != null)
            component.set("v.noOfRecordsToDisplayInAView", Number($A.get("$Label.c.GP_No_of_rows_display_in_Resource")));

        helper.getListOfResourceAllocation(component);
    },
    addResourceAllocation: function(component, event, helper) {
        helper.addToResourceAllocation(component);
    },
    editCurrentResourceAllocation: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var currentResourceAllocationIndex = Number(event.currentTarget.id);
        var currentResourceAllocation = listOfResourceAllocation[currentResourceAllocationIndex];

        component.set("v.activeResourceAllocation", currentResourceAllocation);
        component.set("v.activeResourceAllocationIndex", currentResourceAllocationIndex);
        component.set("v.isEditModeActive", true);
    },
    removeResourceAllocation: function(component, event, helper) {
        var approvalStatus = component.get("v.target.fields.GP_Approval_Status__c.value");

        if (approvalStatus == 'Approved' || approvalStatus == 'Pending For Approval')
            return;
        if (!confirm("Are you sure you want to delete?")) {
            return;
        }

        var currentResourceAllocationIndex = Number(event.currentTarget.id);
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");
        var resourceAllocationToBeDeleted = listOfResourceAllocationPagination[currentResourceAllocationIndex];

        listOfResourceAllocationPagination.splice(currentResourceAllocationIndex, 1);
        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocationPagination);

        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var indexOfMasterlistOfResourceAllocation = initialIndexDisplayed + currentResourceAllocationIndex;
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");

        listOfResourceAllocation.splice(indexOfMasterlistOfResourceAllocation, 1);
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);

        if (resourceAllocationToBeDeleted["Id"]) {
            helper.deleteResourceAllocation(component, resourceAllocationToBeDeleted["Id"]);
        }

        helper.refreshPaginatedView(component);
    },
    profileBillRateChange: function(component, event, helper) {
        var label = event.getSource().get("v.label");
        var splitted = label.split('@@');
        var resourceAllocationIndex = Number(splitted[1]);
        var resourceAllocationField = splitted[0];
        var value = event.getSource().get("v.value");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");

        helper.profileBillRateValueChange(component, value, resourceAllocationIndex, listOfResourceAllocationPagination);
        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);
    },
    saveResourceAllocation: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");

        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);

        if (helper.validateResourceAllocationRecord(component, listOfResourceAllocationPagination))
            helper.saveResourceAllocation(component, listOfResourceAllocationPagination, initialIndexDisplayed);

        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);
    },
    employeeChangeHandler: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");
        var employeeName = event.getParam("recordName");
        var selectedEmployeeId = event.getParam("sObjectId");
        var index = event.getParam("externalParameter");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");

        if (selectedEmployeeId != null) {
            helper.employeeChangeHandlerSDOReference(component, selectedEmployeeId, index, listOfResourceAllocationPagination);
        } else {
            listOfResourceAllocation[index + initialIndexDisplayed].employee = null;
        }
        listOfResourceAllocation[index + initialIndexDisplayed]["GP_Employee__r"] = {
            "Name": employeeName,
            "Id": selectedEmployeeId
        };

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);
    },
    editAllRows: function(component, event, helper) {
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");

        if (!listOfResourceAllocationPagination || listOfResourceAllocationPagination.length === 0) {
            return;
        }

        for (var i = 0; i < listOfResourceAllocationPagination.length; i += 1) {
            listOfResourceAllocationPagination[i]["isEditable"] = true;
        }

        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocationPagination);
        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);
        helper.refreshPaginatedView(component);
    },
    editSelectedRows: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
         var projectRecord = component.get("v.targetProject");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");
        var atleastOneSelected = false;
        for (var i = 0; i < listOfResourceAllocationPagination.length; i += 1) {

            if (listOfResourceAllocationPagination[i]["isSelected"] && listOfResourceAllocationPagination[i]["GP_Employee__r"]["GP_isActive__c"]) {
                atleastOneSelected = true;
                break;
            }
        }
        if (atleastOneSelected && !confirm("Are you sure you want to edit selected rows?"))
            return;

        for (var i = 0; i < listOfResourceAllocationPagination.length; i += 1) {

            if (listOfResourceAllocationPagination[i]["isSelected"] && listOfResourceAllocationPagination[i]["GP_Employee__r"]["GP_isActive__c"]) {
                listOfResourceAllocationPagination[i]["isEditable"] = true;
                if (projectRecord.RecordType.Name != 'CMITS') {
                    listOfResourceAllocationPagination[i].allocationDisabled = true;
                }
            }
        }

        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocationPagination);

        helper.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocationPagination, initialIndexDisplayed);
        helper.refreshPaginatedView(component);
    },
    closeDivision: function(component, event, helper) {
        component.set("v.processingStarted", false);
    },
    massUpdate: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var massResourceAllocationObject = component.get("v.massResourceAllocationObject");

        for (var key in listOfResourceAllocation) {
            if (listOfResourceAllocation[key]["isSelected"] && listOfResourceAllocation[key]["isEditable"]) {
                if (!$A.util.isEmpty(massResourceAllocationObject.massAllocationType) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Allocation_Type__c"] = massResourceAllocationObject.massAllocationType;
                if (!$A.util.isEmpty(massResourceAllocationObject.massProfile) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Profile__c"] = massResourceAllocationObject.massProfile;
                if (!$A.util.isEmpty(massResourceAllocationObject.massBillRate) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Bill_Rate__c"] = massResourceAllocationObject.massBillRate;
                if (!$A.util.isEmpty(massResourceAllocationObject.massStartDate) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Start_Date__c"] = massResourceAllocationObject.massStartDate;
                if (!$A.util.isEmpty(massResourceAllocationObject.massEndDate))
                    listOfResourceAllocation[key]["GP_End_Date__c"] = massResourceAllocationObject.massEndDate;
                if (!$A.util.isEmpty(massResourceAllocationObject.massWorkLocation) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Work_Location__c"] = massResourceAllocationObject.massWorkLocation;
                if (!$A.util.isEmpty(massResourceAllocationObject.massLendingSDO) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_SDO__c"] = massResourceAllocationObject.massLendingSDO;
                if (!$A.util.isEmpty(massResourceAllocationObject.massPercentageResourceAllocation) &&
                    $A.util.isEmpty(listOfResourceAllocation[key]["GP_Parent_Resource_Allocation__c"]))
                    listOfResourceAllocation[key]["GP_Percentage_allocation__c"] = massResourceAllocationObject.massPercentageResourceAllocation;

            }
        }

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        helper.refreshPaginatedView(component);
    },
    selectAllEditModeRecords: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var massResourceAllocationObject = component.get("v.massResourceAllocationObject");

        for (var key in listOfResourceAllocation) {
            if ( massResourceAllocationObject["isSelected"] && 
                ((listOfResourceAllocation[key]["GP_Employee__r"] && listOfResourceAllocation[key]["GP_Employee__r"]["GP_isActive__c"]) ||
                    (!listOfResourceAllocation[key]["GP_Employee__r"]))) {
                listOfResourceAllocation[key]["isSelected"] = true;
            } else if (!massResourceAllocationObject["isSelected"])
                listOfResourceAllocation[key]["isSelected"] = false;
        }

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        helper.refreshPaginatedView(component);
    },
     // Avinash - Interviewed By Client
     onSelectAllChange:function(component, event, helper) {      
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        for(var key in listOfResourceAllocation)
        {
             if(listOfResourceAllocation[key].isEditable)
            {
            listOfResourceAllocation[key].GP_Interviewed_by_Customer__c=component.get('v.isAllSelected');
            }
        }
        component.set("v.listOfResourceAllocation",listOfResourceAllocation);
        helper.refreshPaginatedView(component);
        
    },
    deleteErrorRecords: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        component.set("v.listOfResourceAllocation", helper.getErrorRemovedRecordList(listOfResourceAllocation));

        helper.refreshPaginatedView(component);
    },
    saveErrorRecords: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var listOfResourceAllocationErrorRecords = helper.getErrorRecordList(listOfResourceAllocation);

        if (helper.validateResourceAllocationRecord(component, listOfResourceAllocationErrorRecords))
            helper.saveResourceAllocation(component, listOfResourceAllocationErrorRecords);

        helper.refreshPaginatedView(component);
    },
    cloneSelectedRecords: function(component, event, helper) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");

        for (var key in listOfResourceAllocation) {
            if (listOfResourceAllocation[key]["isSelected"]) {
                var resourceAllocationRecord = helper.clone(listOfResourceAllocation[key]);
                resourceAllocationRecord.Id = null;
                resourceAllocationRecord.isEditable = true;
                resourceAllocationRecord.GP_Oracle_Id__c = null;
                resourceAllocationRecord.GP_Oracle_Status__c = null;
                resourceAllocationRecord.GP_Parent_Resource_Allocation__c = null;
                resourceAllocationRecord.GP_Operation_Mode__c='CREATE';
                listOfResourceAllocation.push(resourceAllocationRecord);
            }
        }

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var indexOfRecordInLastView = parseInt((listOfResourceAllocation.length - 1) / noOfRecordsToDisplayInAView) * noOfRecordsToDisplayInAView;

        helper.setListOfResourceAllocationPagination(component, listOfResourceAllocation, indexOfRecordInLastView, noOfRecordsToDisplayInAView);
        component.set("v.initialIndexDisplayed", indexOfRecordInLastView);
    },
    previousView: function(component, event, helper) {
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");

        helper.setListOfResourceAllocationPagination(component, listOfResourceAllocation,
            initialIndexDisplayed - noOfRecordsToDisplayInAView, noOfRecordsToDisplayInAView);
        component.set("v.initialIndexDisplayed", initialIndexDisplayed - noOfRecordsToDisplayInAView);
    },
    nextView: function(component, event, helper) {
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");

        helper.setListOfResourceAllocationPagination(component, listOfResourceAllocation,
            initialIndexDisplayed + noOfRecordsToDisplayInAView, noOfRecordsToDisplayInAView);
        component.set("v.initialIndexDisplayed", initialIndexDisplayed + noOfRecordsToDisplayInAView);
    },
    massProfileBillRateChange: function(component, event, helper) {
        var massResourceAllocationObject = component.get("v.massResourceAllocationObject");
        var mapOfProfileBillRateRecords = component.get("v.mapOfProfileBillRateRecords");
        var value = massResourceAllocationObject.massProfile;

        if (value)
            massResourceAllocationObject.massBillRate = mapOfProfileBillRateRecords[value].GP_Bill_Rate__c;
        else
            massResourceAllocationObject.massBillRate = 0;

        component.set("v.massResourceAllocationObject", massResourceAllocationObject);
    },
    extendToAnotherComponent: function(component, event, helper) {
        component.set("v.openMassUpload", true);
    },
    onChangelistOfMassResourceAllocationService: function(component, event, helper) {
        var listOfMassResourceAllocation = component.get("v.listOfMassResourceAllocation");
        listOfMassResourceAllocation = helper.setObjectApiNamesToColumn(listOfMassResourceAllocation);

        helper.processMassList(component, listOfMassResourceAllocation);
    },
    clearEmployee: function(component, event, helper) {
        var externalParameter = event.getParam("externalParameter");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var listOfResourceAllocationPagination = component.get("v.listOfResourceAllocationPagination");

        listOfResourceAllocationPagination[externalParameter]["GP_Employee__c"] = null;
        listOfResourceAllocationPagination[externalParameter].employee = null;
        listOfResourceAllocation[externalParameter]["GP_Employee__c"] = null;
        listOfResourceAllocation[externalParameter].employee = null;

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocationPagination);

    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },
    filterList: function(component, event, helper) {
        var label = event.getSource().get("v.label");
        var value = event.getSource().get("v.value");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation") || [];
        var filteredListOfResourceAllocation = JSON.parse(JSON.stringify(listOfResourceAllocation));

        var listOfResourceAllocationMatchingSearchString = [];
        var searchResourceAllocationObject = component.get("v.searchResourceAllocationObject");

        /*listOfResourceAllocationMatchingSearchString = filteredListOfResourceAllocation.filter(function(resourceAllocation) {
            return  resourceAllocation["GP_Employee__r"]["Name"].toLowerCase().includes(value.toLowerCase());
        });*/

        for (var index = 0; index < listOfResourceAllocation.length; index++) {
            if (listOfResourceAllocation[index]["GP_Employee__r"] &&
                listOfResourceAllocation[index]["GP_Employee__r"]["Name"] &&
                listOfResourceAllocation[index]["GP_Employee__r"]["Name"].toLowerCase().includes(value.toLowerCase())) {
                var filteredEmployee = listOfResourceAllocation[index];
                listOfResourceAllocation.splice(index, 1);
                listOfResourceAllocation.unshift(filteredEmployee);
                //listOfResourceAllocationMatchingSearchString.push(listOfResourceAllocation[index]);
            }
        }

        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var indexOfRecordInLastView = parseInt((listOfResourceAllocation.length - 1) / noOfRecordsToDisplayInAView) * noOfRecordsToDisplayInAView;
		helper.firstScreenView(component, listOfResourceAllocation);
        //helper.setListOfResourceAllocationPagination(component, listOfResourceAllocation, indexOfRecordInLastView, noOfRecordsToDisplayInAView);
        //component.set("v.initialIndexDisplayed", indexOfRecordInLastView);
        //component.set("v.listOfResourceAllocationPagination",listOfResourceAllocationMatchingSearchString);
    },
    showFilter: function(component, event, helper) {
        var searchResourceAllocationObject = component.get("v.searchResourceAllocationObject");
        searchResourceAllocationObject.isEmployeeFilterActive = !searchResourceAllocationObject.isEmployeeFilterActive;

        component.set("v.searchResourceAllocationObject", searchResourceAllocationObject);
    },
    profileBillRateChangeHandler: function(component, event, helper) {
        var lstOfProfileBillRateRecords = event.getParam("lstOfProfileBillRateRecords") || [];
        lstOfProfileBillRateRecords.unshift('--NONE--');

        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
     
    downloadAllocation:function(component, event, helper){
        
        var baseURL = '#/sObject/00O90000009eNn6/view?fv0='+component.get("v.recordId"); //= $A.get("$Label.c.GP_PE_PRF_VF_PAGE_BASE_URL") ;
       	var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
          "url":baseURL
        });
        eUrl.fire(); 
        },
})