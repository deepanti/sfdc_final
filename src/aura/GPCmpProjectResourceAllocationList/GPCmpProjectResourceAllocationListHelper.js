({
    TERMINATION_DATE_ERROR_LABEL: 'End Date should not be greater than resource actual termination Date.',
    LEGAL_ENTITY_ERROR_LABEL: 'Legal Entity of Employee is not same \n Work Location Legal Entity!',
    START_END_DATE__WRT_PROJECT_ERROR_LABEL: 'Start/End Date should lie between project duration.',
    BILL_RATE_MANDATORY_ERROR_LABEL: 'For FTE or T&M type of project bill rate is mandatory.',
    HIRE_DATE_ERROR_LABEL: 'Start Date should not be less than resource Hire Date.',
    START_END_DATE_ERROR_LABEL: 'Start Date should be smaller than End Date!',
    PERCENTAGE_ALLOCATION_ERROR_LABEL: 'Pecentage should lie between 0-100!',
    RESOURCE_LENDING_SDO_ERROR_LABEL: 'Resouce Lending SDO incorrect!',
    REQUIRED_FIELDS_ERROR_LABEL: 'Please Enter Required Fields!',
    MAX_END_DATE_ERROR_LABEL: 'End Date should be less than ',
    NONE_ENTRY_LABEL: 'null',
    LABEL_FIELD_TO_APINAME_MAP: {
        "percentage allocation": "GP_Percentage_allocation__c",
        "allocation type": "GP_Allocation_Type__c",
        "allocation status": "GP_Committed_SOW__c",
        "work location code": "GP_Work_Location__c",
        "start date": "GP_Start_Date__c",
        "bill rate": "GP_Bill_Rate__c",
        "end date": "GP_End_Date__c",
        "employee number": "GP_Employee__c",
        "profile name": "GP_Profile__c",
        //"lending sdo code": "GP_SDO__c",
        "attention to (ms)": "GP_MS_Attention_To__c",
        "businessunit manager (ms)": "GP_MS_BusinessUnit_Manager__c",
        "mer no (ms)": "GP_MS_MER_No__c",
        "address (ms)": "GP_MS_Address__c",
        "email id (ms)": "GP_MS_Email_Id__c",
        "ms id (ms)": "GP_MS_ID__c",
        "manager (ms)": "GP_MS_MANAGER__c",
        "payment mode (ms)": "GP_MS_Payment_Mode__c",
        "z id(cm)": "GP_CM_Id__c",
        "address (cm)": "GP_CM_Address__c",
        "oracle id" : "GP_Oracle_Id__c"

    },
    DEFAULT_VALUES_HEADER_ROW: [
        "EMPLOYEE NUMBER",
        "START DATE",
        "END DATE",
        "WORK LOCATION CODE",
        //"LENDING SDO CODE",
        //"PROFILE NAME",
        "BILL RATE",
        //"ALLOCATION STATUS",
        //"ALLOCATION TYPE",
        "PERCENTAGE ALLOCATION",
        "ORACLE ID"

    ],

    getListOfResourceAllocation: function(component) {
        var getResourceAllocationDataService = component.get("c.getResourceAllocationData");
        getResourceAllocationDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        getResourceAllocationDataService.setCallback(this, function(response) {
            this.resourceAllocationDataServiceHandler(response, component);
        });
        $A.enqueueAction(getResourceAllocationDataService);
    },
    resourceAllocationDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setResourceTemplate(component, responseData);
            this.setResourceAllocation(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setResourceTemplate: function(component, responseData) {
        var resourceAllocationTemplate = JSON.parse(responseData.response).mapOfFieldNameToTemplate;
        component.set("v.resourceAllocationTemplate", resourceAllocationTemplate);
    },
    addDefaultRows: function(component, projectRecord) {
        var projectId = component.get("v.recordId");
        //var projectRecord = component.get("v.target.fields");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation") || [];
        var noOfRowsToBeDisplayed = component.get("v.noOfRowsToBeDisplayed");
        if (listOfResourceAllocation.length == 0) {
            for (var rowCount = 0; rowCount < noOfRowsToBeDisplayed; rowCount++) {
                this.newRowAdd(projectId, projectRecord, listOfResourceAllocation);
            }
        }
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        this.firstScreenView(component, listOfResourceAllocation);
    },
    setResourceAllocation: function(component, responseData) {
        var response = responseData.response ? JSON.parse(responseData.response) || [] : [];

        if (!response.isResourceAllocationDefinable)
            component.set("v.isResourceAllocationDefinable", false);
        else {
            this.addSelectedCheckBoxToLst(component, response.lstresourceAllocation);
            component.set("v.noOfRowsToBeDisplayed", response.noOfRowsToBeDisplayed);
            component.set("v.mapOfProfileBillRateRecords", response.profileBillRate);
            component.set("v.mapOfWorkLocation", response.mapOfWorkLocation);
            component.set("v.currencyISOCode", response.currencyISOCode);
            component.set("v.lstOfWorkLocations", response.workLocation);
            component.set("v.lstOfLendingSDOs", response.lendingSDO);
            component.set("v.billRateType", response.billRateType);
            component.set("v.isPIDCreated", response.isPIDCreated);
            component.set("v.maxEndDate", response.maxEndDate);
            component.set("v.project", response.project);

            this.lstOfProfileBillRateRecords(component, response.profileBillRate);
            this.setheaderRow(component, response.listOfVisibleFields);
            this.addDefaultRows(component, response.project);
        }
    },
    deleteResourceAllocation: function(component, resourceAllocationId) {
        this.showSpinner(component);
        var deleteResourceAllocationData = component.get("c.deleteResourceAllocationData");
        deleteResourceAllocationData.setParams({
            "projectResourceAllocationId": resourceAllocationId
        });
        deleteResourceAllocationData.setCallback(this, function(response) {
            this.deleteResourceAllocationDataHandler(response, component);
        });
        $A.enqueueAction(deleteResourceAllocationData);
    },
    deleteResourceAllocationDataHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.fireProjectSaveEvent(true);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    addToResourceAllocation: function(component) {
        var projectId = component.get("v.recordId");
        var projectRecord = component.get("v.project"); //component.get("v.target.fields");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation") || [];

        this.newRowAdd(projectId, projectRecord, listOfResourceAllocation);
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);

        this.firstScreenView(component, listOfResourceAllocation);
    },
    newRowAdd: function(projectId, projectRecord, listOfResourceAllocation) {
        var resourceAllocation = {
            'isEditable': true,
            'GP_Project__c': projectId,
            'GP_Project__r': {
                'Name': projectRecord.Name.value
            },
            'GP_Start_Date__c': projectRecord.GP_Start_Date__c ? projectRecord.GP_Start_Date__c : null,
            'GP_End_Date__c': projectRecord.GP_End_Date__c ? projectRecord.GP_End_Date__c : null,
            'allocationDisabled': (projectRecord.RecordType.Name == 'CMITS' && (projectRecord.GP_Project_type__c == 'T&M' || projectRecord.GP_Project_type__c == 'FTE')) ? false : true,
            'GP_Committed_SOW__c': 'Committed'

        };

        this.addToListAtParticularIndex(0, listOfResourceAllocation, resourceAllocation);
    },
    firstScreenView: function(component, listOfResourceAllocation) {
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");

        this.setListOfResourceAllocationPagination(component, listOfResourceAllocation, 0, noOfRecordsToDisplayInAView);
        component.set("v.initialIndexDisplayed", 0);
    },
    profileBillRateValueChange: function(component, value, resourceAllocationIndex, listOfResourceAllocation) {
        var mapOfProfileBillRateRecords = component.get("v.mapOfProfileBillRateRecords");

        if (value)
            listOfResourceAllocation[resourceAllocationIndex].GP_Bill_Rate__c = mapOfProfileBillRateRecords[value];
        else
            listOfResourceAllocation[resourceAllocationIndex].GP_Bill_Rate__c = 0;

        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocation);

    },
    validateResourceAllocationRecord: function(component, listOfResourceAllocation) {
        var mapOfWorkLocation = component.get("v.mapOfWorkLocation");
        var resourceLendingSDO = component.get("v.resourceLendingSDO");
        var maxEndDate = new Date(component.get("v.maxEndDate"));
        var project = component.get("v.project");
        var isValid = true;

        for (var i = 0; i < listOfResourceAllocation.length; i += 1) {
            if (listOfResourceAllocation[i]["isSelected"] && listOfResourceAllocation[i]["isEditable"]) {
                var startDateValue = new Date(listOfResourceAllocation[i].GP_Start_Date__c);
                var endDateValue = new Date(listOfResourceAllocation[i].GP_End_Date__c);
                var rowDom = document.getElementById('project-resource-Allocation-row' + i);
                listOfResourceAllocation[i].GP_Oracle_Status__c = '';
                if (this.checkEmptyValuesOfFields(listOfResourceAllocation[i]) || !this.checkEmptyValueAgainstTemplate(component, listOfResourceAllocation[i])) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.REQUIRED_FIELDS_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (startDateValue > endDateValue) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.START_END_DATE_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (new Date(project.GP_Start_Date__c) > startDateValue || new Date(project.GP_End_Date__c) < endDateValue) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.START_END_DATE__WRT_PROJECT_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (listOfResourceAllocation[i]["GP_Bill_Rate__c"] == 0) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = 'Bill Rate can\'t be zero';
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (($A.util.isEmpty(listOfResourceAllocation[i]["GP_Shadow_type__c"]) || listOfResourceAllocation[i]["GP_Shadow_type__c"] == '--NONE--') &&
                    (project.GP_Project_type__c == 'FTE' || project.GP_Project_type__c == 'T&M') &&
                    (($A.util.isEmpty(listOfResourceAllocation[i]["GP_Bill_Rate__c"]) || listOfResourceAllocation[i]["GP_Bill_Rate__c"] == 0))) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.BILL_RATE_MANDATORY_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (maxEndDate && maxEndDate < endDateValue) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.MAX_END_DATE_ERROR_LABEL + component.get("v.maxEndDate");
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                }
                /*else if (!$A.util.isEmpty(listOfResourceAllocation[i].GP_SDO__c) &&
                                   !$A.util.isEmpty(listOfResourceAllocation[i]) &&
                                   !$A.util.isEmpty(listOfResourceAllocation[i].resourceLendingSDO) &&
                                   listOfResourceAllocation[i].GP_SDO__c != listOfResourceAllocation[i].resourceLendingSDO) {
                                   isValid = false;
                                   rowDom.className = 'invalid-row';
                                   listOfResourceAllocation[i]["errorMessage"] = this.RESOURCE_LENDING_SDO_ERROR_LABEL;
                                   listOfResourceAllocation[i]["iconName"] = "utility:error";
                               } */
                else if ((listOfResourceAllocation[i].GP_Percentage_allocation__c <= 0 ||
                        listOfResourceAllocation[i].GP_Percentage_allocation__c > 100)) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.PERCENTAGE_ALLOCATION_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (project && project.GP_Operating_Unit__c && !project.GP_Operating_Unit__r.Name.indexOf('headstrong') >= 0 &&
                    project.GP_Project_type__c != 'T&M' && listOfResourceAllocation[i]["GP_Committed_SOW__c"] == 'Missing Paper Work') {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = 'Missing Paper work can\'t be selected';
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (listOfResourceAllocation[i].objEmployee && listOfResourceAllocation[i].objEmployee.GP_HIRE_Date__c &&
                    startDateValue < new Date(listOfResourceAllocation[i].objEmployee.GP_HIRE_Date__c)) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.HIRE_DATE_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (listOfResourceAllocation[i].objEmployee && listOfResourceAllocation[i].objEmployee.GP_ACTUAL_TERMINATION_Date__c &&
                    endDateValue > new Date(listOfResourceAllocation[i].objEmployee.GP_ACTUAL_TERMINATION_Date__c)) {
                    isValid = false;
                    rowDom.className = 'invalid-row';
                    listOfResourceAllocation[i]["errorMessage"] = this.TERMINATION_DATE_ERROR_LABEL;
                    listOfResourceAllocation[i]["iconName"] = "utility:error";
                    listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                } else if (project.RecordType.Name != 'Indirect PID' && listOfResourceAllocation[i].GP_Work_Location__c && mapOfWorkLocation &&
                    ((listOfResourceAllocation[i].objEmployee != null && mapOfWorkLocation[listOfResourceAllocation[i].GP_Work_Location__c] !=
                            listOfResourceAllocation[i].objEmployee.GP_Legal_Entity_Code__c) ||
                        ((listOfResourceAllocation[i].objEmployee == null && listOfResourceAllocation[i].GP_Employee__r != null &&
                            mapOfWorkLocation[listOfResourceAllocation[i].GP_Work_Location__c] !=
                            listOfResourceAllocation[i].GP_Employee__r.GP_Legal_Entity_Code__c)))) {
                   
                    debugger;
                    if(endDateValue  >=  new Date()) {
                        isValid = false;
                        rowDom.className = 'invalid-row';
                        listOfResourceAllocation[i]["errorMessage"] = this.LEGAL_ENTITY_ERROR_LABEL;
                        listOfResourceAllocation[i]["iconName"] = "utility:error";
                        listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
                    }
                }
            }
        }

        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocation);
        return isValid;
    },
    checkEmptyValuesOfFields: function(resourceAllocationRecord) {
        return ($A.util.isEmpty(resourceAllocationRecord["GP_Employee__c"]) ||
            $A.util.isEmpty(resourceAllocationRecord["GP_Percentage_allocation__c"]) ||
            //$A.util.isEmpty(resourceAllocationRecord["GP_Bill_Rate__c"]) ||
            //resourceAllocationRecord["GP_Bill_Rate__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(resourceAllocationRecord["GP_SDO__c"]) ||
            resourceAllocationRecord["GP_SDO__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(resourceAllocationRecord["GP_End_Date__c"]) ||
            $A.util.isEmpty(resourceAllocationRecord["GP_Employee__c"]) ||
            // $A.util.isEmpty(resourceAllocationRecord["GP_Allocation_Type__c"]) ||
            // resourceAllocationRecord["GP_Allocation_Type__c"] == '' ||
            // resourceAllocationRecord["GP_Allocation_Type__c"] == 'None' || //commented on 28-03-2018 with respect to issue number 140318-0014
            $A.util.isEmpty(resourceAllocationRecord["GP_Start_Date__c"]) ||
            $A.util.isEmpty(resourceAllocationRecord["GP_Work_Location__c"]) ||
            resourceAllocationRecord["GP_Work_Location__c"] == this.NONE_ENTRY_LABEL //||
            //$A.util.isEmpty(resourceAllocationRecord["GP_Committed_SOW__c"]) ||
            //resourceAllocationRecord["GP_Committed_SOW__c"] == 'None' ||
            //resourceAllocationRecord["GP_Committed_SOW__c"] == ''
        );
    },
    checkEmptyValueAgainstTemplate: function(component, resourceAllocationRecord) {
        var isValid = true;
        var mapOfFieldNameToTemplate = component.get("v.resourceAllocationTemplate");

        for (var key in mapOfFieldNameToTemplate) {
            var fieldName = key.split("___")[2];
            if (mapOfFieldNameToTemplate[key]["isRequired"] && $A.util.isEmpty(resourceAllocationRecord[fieldName])) {
                isValid = false;
            }
        }

        return isValid;
    },
    getSelectedRecordCount: function(listOfResourceAllocation) {
        var count = 0;

        for (var i = 0; i < listOfResourceAllocation.length; i += 1) {
            var resourceAllocation = listOfResourceAllocation[i];
            if (resourceAllocation["isEditable"] && resourceAllocation["isSelected"]) {
                count++;
            }
        }

        return count;
    },
    saveResourceAllocation: function(component, listOfResourceAllocation) {
        component.set("v.showLoadingSpinner", true);
        var selectedRecordCount = this.getSelectedRecordCount(listOfResourceAllocation);

        if (selectedRecordCount == 0) {
            component.set("v.recordProcessedString", 'Please select at least one row.');
            component.set("v.processingStarted", true);
            component.set("v.showLoadingSpinner", false);
            component.set("v.disableClose", false);
            return;
        }

        component.set("v.recordProcessedString", '0 of ' + selectedRecordCount + ' processed!');
        var countOfRecordProcessed = 1;
        var i = 0;
        //for (var i = 0; i < listOfResourceAllocation.length; i += 1) {
        // var resourceAllocation = listOfResourceAllocation[i];
        // if (resourceAllocation["isEditable"] && resourceAllocation["isSelected"]) {
        component.set("v.processingStarted", true);
        component.set("v.disableClose", true);
        if (listOfResourceAllocation)
            this.saveEachResourceAllocation(component, listOfResourceAllocation, i, selectedRecordCount, countOfRecordProcessed);
        // countOfRecordProcessed++;
        //}
        // }

    },
    saveEachResourceAllocation: function(component, listOfResourceAllocation, i, selectedRecordCount, countOfRecordProcessed) {
        var resourceAllocation = listOfResourceAllocation[i];
        if (!resourceAllocation || !listOfResourceAllocation || (i > listOfResourceAllocation.length)) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.disableClose", false);
            //this.fireProjectSaveEvent(true);
            return;
        }
        if (!(resourceAllocation["isEditable"] && resourceAllocation["isSelected"])) {
            i++;
            this.saveEachResourceAllocation(component, listOfResourceAllocation, i, selectedRecordCount, countOfRecordProcessed);
            return;
        }

        var sanatizedResourceAllocation = this.clearRelatedRecords(JSON.parse(JSON.stringify(resourceAllocation)));
        if (!resourceAllocation) {
            this.showToast('Warning', 'warning', 'Please add a resource.');
            return;
        }

        var saveResourceAllocationService = component.get("c.saveResourceAllocationData");
        saveResourceAllocationService.setParams({
            "strResourceAllocation": JSON.stringify(sanatizedResourceAllocation)
        });
        saveResourceAllocationService.setCallback(this, function(response) {
            component.set("v.recordProcessedString", countOfRecordProcessed + ' of ' + selectedRecordCount + ' processed!');
            this.saveResourceAllocationServiceHandler(response, component, i, listOfResourceAllocation, selectedRecordCount, countOfRecordProcessed);
        });

        $A.enqueueAction(saveResourceAllocationService);
    },
    saveResourceAllocationServiceHandler: function(response, component, i, listOfResourceAllocation, selectedRecordCount, countOfRecordProcessed) {
        var responseData = response.getReturnValue() || {};
        //component.set("v.recordProcessedString", i + ' of ' + selectedRecordCount + ' processed!');
        if (component.isValid() && response.getState() === "SUCCESS" &&
            responseData.isSuccess && responseData.message !== 'WARNING') {
            listOfResourceAllocation[i].Id = JSON.parse(responseData.response).Id;
            listOfResourceAllocation[i] = JSON.parse(responseData.response);
            listOfResourceAllocation[i]["employee"] = this.createEmployeeHoverString(listOfResourceAllocation[i]["GP_Employee__r"]);
            listOfResourceAllocation[i]["iconName"] = "utility:success";
            listOfResourceAllocation[i]["isEditable"] = false;
            listOfResourceAllocation[i]["errorMessage"] = "Saved Successfully";
            listOfResourceAllocation[i]["isSelected"] = false;
            listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-success";
        } else if (component.isValid() && response.getState() === "SUCCESS" &&
            responseData.isSuccess && responseData.message === 'WARNING') {
            listOfResourceAllocation[i].Id = JSON.parse(responseData.response).resourceAllocation.Id;
            listOfResourceAllocation[i] = JSON.parse(responseData.response).resourceAllocation;
            listOfResourceAllocation[i]["employee"] = this.createEmployeeHoverString(listOfResourceAllocation[i]["GP_Employee__r"]);
            listOfResourceAllocation[i]["iconName"] = "utility:warning";
            listOfResourceAllocation[i]["isEditable"] = false;
            listOfResourceAllocation[i]["errorMessage"] = 'Records saved successfully \n But was Also Allocated on Other Project.Total Allocation : ' + JSON.parse(responseData.response).percentage + '%';
            listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-warning";

        } else if (component.isValid() && response.getState() === "SUCCESS" &&
            !responseData.isSuccess) {
            listOfResourceAllocation[i]["iconName"] = "utility:error";
            listOfResourceAllocation[i]["errorMessage"] = responseData.message;
            listOfResourceAllocation[i]["isEditable"] = true;
            listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
        } else {
            listOfResourceAllocation[i]["iconName"] = "utility:error";
            listOfResourceAllocation[i]["errorMessage"] = responseData.message;
            listOfResourceAllocation[i]["utilityClassName"] = "slds-show slds-icon-text-error";
        }

        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocation);
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        this.setlstOfResourceAllocationFromPaginatedList(component, listOfResourceAllocation, initialIndexDisplayed);

        countOfRecordProcessed++;
        i++;

        if (listOfResourceAllocation && (i < listOfResourceAllocation.length)) {
            this.saveEachResourceAllocation(component, listOfResourceAllocation, i, selectedRecordCount, countOfRecordProcessed);
            return;
        } else {
            component.set("v.processingStarted", false);
            this.fireRelatedRecordSaveEvent('Resource Allocation');
            return;
        }


    },
    lstOfProfileBillRateRecords: function(component, mapOfProfileBillRateRecords) {
        var nullRecord = '--NONE--';
        var lstOfProfileBillRateRecords = [];
        lstOfProfileBillRateRecords.push(nullRecord);
        for (var key in mapOfProfileBillRateRecords) {
            lstOfProfileBillRateRecords.push(key);
        }

        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
    clearRelatedRecords: function(resourceAllocation) {
        resourceAllocation["GP_Employee__r"] = null;
        resourceAllocation["objEmployee"] = null;
        resourceAllocation["GP_SDO__r"] = null;
        return resourceAllocation;
    },
    employeeChangeHandlerSDOReference: function(component, selectedEmployeeId, index, listOfResourceAllocation) {
        var projectRecordId = component.get("v.target").fields.Id.value;
        if ($A.util.isEmpty(selectedEmployeeId))
            return;
        var getLendingSDO = component.get("c.getLendingSDO");
        getLendingSDO.setParams({
            "employeeId": selectedEmployeeId,
            "projectId": projectRecordId
        });
        this.showSpinner(component);
        getLendingSDO.setCallback(this, function(response) {
            this.getLendingSDOHandler(response, component, index, listOfResourceAllocation);
        });
        $A.enqueueAction(getLendingSDO);
    },
    getLendingSDOHandler: function(response, component, index, listOfResourceAllocation) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" &&
            responseData.isSuccess && responseData.response) {
            var response = JSON.parse(responseData.response) || [];
            listOfResourceAllocation[index].resourceLendingSDO = response.resourceLendingSDO;
            listOfResourceAllocation[index].resourceLendingSDOName = response.resourceLendingSDOName;
            listOfResourceAllocation[index].GP_SDO__c = response.resourceLendingSDO;
            listOfResourceAllocation[index].GP_SDO__r = {
                'Name': response.resourceLendingSDOName,
                'GP_Oracle_Id__c': response.resourceLendingSDOOracleCode
            };
            listOfResourceAllocation[index]["GP_Employee__r"] = response.objEmployeeMaster;
            listOfResourceAllocation[index].employee = this.createEmployeeHoverString(response.objEmployeeMaster);
            listOfResourceAllocation[index].objEmployee = response.objEmployeeMaster;
        } else {
            this.handleFailedCallback(component, responseData);
        }
        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocation);
    },
    createEmployeeHoverString: function(objEmployeeMaster) {
        var employeeDetailString = '';
        /*if (!$A.util.isEmpty(objEmployeeMaster) && objEmployeeMaster.GP_EMPLOYEE_TYPE__c)
            employeeDetailString += ' Employee Type : ' + objEmployeeMaster.GP_EMPLOYEE_TYPE__c;*/

        if (!$A.util.isEmpty(objEmployeeMaster)) {
            employeeDetailString += '\n OHR No : ' + (objEmployeeMaster.GP_Final_OHR__c ? objEmployeeMaster.GP_Final_OHR__c : '');
            employeeDetailString += '\n Legal Entity Code : ' + (objEmployeeMaster.GP_Legal_Entity_Code__c ? objEmployeeMaster.GP_Legal_Entity_Code__c : '');
            employeeDetailString += '\n SDO : ' + (objEmployeeMaster.GP_SDO__c ? objEmployeeMaster.GP_SDO__r.Name : '');
            employeeDetailString += '\n Employee Type : ' + (objEmployeeMaster.GP_EMPLOYEE_TYPE__c ? objEmployeeMaster.GP_EMPLOYEE_TYPE__c : '');
            employeeDetailString += '\n Business Group Name : ' + (objEmployeeMaster.GP_Business_Group_NAME__c ? objEmployeeMaster.GP_Business_Group_NAME__c : '');
            employeeDetailString += '\n Location : ' + (objEmployeeMaster.GP_LOCATION__c ? objEmployeeMaster.GP_LOCATION__c : '');
            employeeDetailString += '\n Band : ' + (objEmployeeMaster.GP_BAND__c ? objEmployeeMaster.GP_BAND__c : '');
            employeeDetailString += '\n Hire Date : ' + (objEmployeeMaster.GP_HIRE_Date__c ? objEmployeeMaster.GP_HIRE_Date__c : '');
            employeeDetailString += '\n Termination Date : ' + (objEmployeeMaster.GP_ACTUAL_TERMINATION_Date__c ? objEmployeeMaster.GP_ACTUAL_TERMINATION_Date__c : '');
            employeeDetailString += '\n Payroll Country : ' + (objEmployeeMaster.GP_Payroll_Country__c ? objEmployeeMaster.GP_Payroll_Country__c : '');
            //employeeDetailString += '\n Organization : ' + (objEmployeeMaster.GP_ORGANIZATION__c ? objEmployeeMaster.GP_ORGANIZATION__c : '');
            //employeeDetailString += '\n WorkLocation : ' + (objEmployeeMaster.GP_PHYSICAL_WORK_LOCATION__c ? objEmployeeMaster.GP_PHYSICAL_WORK_LOCATION__c : '');
            //employeeDetailString += '\n Email Address : ' + (objEmployeeMaster.GP_OFFICIAL_EMAIL_ADDRESS__c ? objEmployeeMaster.GP_OFFICIAL_EMAIL_ADDRESS__c : '');
            //employeeDetailString += '\n Level : ' + (objEmployeeMaster.GP_BAND__c ? objEmployeeMaster.GP_BAND__c : '');
        }

        return employeeDetailString;
    },
    addSelectedCheckBoxToLst: function(component, listOfResourceAllocation) {
        for (var index in listOfResourceAllocation) {
            listOfResourceAllocation[index]["isSelected"] = false;
            listOfResourceAllocation[index]["employee"] = this.createEmployeeHoverString(listOfResourceAllocation[index]["GP_Employee__r"]);
        }
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        this.setListOfResourceAllocationPagination(component, listOfResourceAllocation,
            0, noOfRecordsToDisplayInAView);

    },
    getErrorRemovedRecordList: function(listOfResourceAllocation) {
        var returnlistOfResourceAllocation = listOfResourceAllocation;
        for (var index = listOfResourceAllocation.length - 1; index >= 0; index--) {
            if (listOfResourceAllocation[index]["iconName"] == "utility:error" && !listOfResourceAllocation[index]["Id"]) {
                returnlistOfResourceAllocation.splice(index, 1);
            }
        }
        return returnlistOfResourceAllocation;
    },
    setListOfResourceAllocationPagination: function(component, listOfResourceAllocation, initialIndex, noOfRecordsToDisplay) {
        var totalNoOfPages = listOfResourceAllocation.length > 0 ? Math.ceil(listOfResourceAllocation.length / noOfRecordsToDisplay) : 1;
        component.set("v.totalNoOfPages", totalNoOfPages);
        var pageNumber = listOfResourceAllocation.length > 0 ? ((initialIndex + noOfRecordsToDisplay) / noOfRecordsToDisplay) : 1;
        component.set("v.pageNumber", pageNumber);
        if ((initialIndex + noOfRecordsToDisplay) >= listOfResourceAllocation.length)
            component.set("v.disableNext", true);
        else
            component.set("v.disableNext", false);
        component.set("v.listOfResourceAllocationPagination", []);
        var listOfResourceAllocationPagination = [];
        for (var index = initialIndex; index - initialIndex < noOfRecordsToDisplay && listOfResourceAllocation[index]; index++) {
            listOfResourceAllocationPagination.push(listOfResourceAllocation[index]);
        }
        component.set("v.listOfResourceAllocationPagination", listOfResourceAllocationPagination);
    },
    addToListAtParticularIndex: function(index, lstOfRecords, recordToBePushed) {
        for (var i = lstOfRecords.length - 1; i >= index; i--) {
            var resource = lstOfRecords[i] || {};
            if (resource.GP_Employee__r && resource.GP_Employee__r.Name && resource.GP_Employee__r.Name.indexOf('--') >= 0) {
                resource.GP_Employee__r = {
                    "Name": resource.GP_Employee__r.Name.split('--')[1],
                    "GP_Employee_Unique_Name__c": resource.GP_Employee__r.Name.split('--')[0],
                    "Id": resource.GP_Employee__c
                }
            }
            lstOfRecords[i + 1] = resource;
        }
        lstOfRecords[index] = recordToBePushed;
        return lstOfRecords;
    },
    refreshPaginatedView: function(component) {
        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        var initialIndexDisplayed = component.get("v.initialIndexDisplayed");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        this.setListOfResourceAllocationPagination(component, listOfResourceAllocation,
            initialIndexDisplayed, noOfRecordsToDisplayInAView);
    },
    getErrorRecordList: function(listOfResourceAllocation) {
        var errorRecordList = [];
        for (var key in listOfResourceAllocation) {
            if (listOfResourceAllocation[key]["iconName"] == "utility:error") {
                errorRecordList.push(listOfResourceAllocation[key]);
            }
        }
        return errorRecordList;
    },
    setlstOfResourceAllocationFromPaginatedList: function(component, listOfResourceAllocationPagination, indexDisplayed) {
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        for (var index = 0; index < listOfResourceAllocationPagination.length; index++) {
            listOfResourceAllocation[index + indexDisplayed] = listOfResourceAllocationPagination[index];
        }
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);
    },
    clone: function(object) {
        return JSON.parse(JSON.stringify(object));
    },
    setheaderRow: function(component, listOfVisibleFields) {
        component.set("v.headerRow", []);
        var headerRowChild = this.DEFAULT_VALUES_HEADER_ROW;
        var project = component.get("v.project");
        if (listOfVisibleFields) {
            for (var index = 0; index < listOfVisibleFields.length; index++) {
                if (listOfVisibleFields[index].Label != 'Amount')
                    headerRowChild.push(listOfVisibleFields[index].Label.toUpperCase());
            }
        }
        if (project && project.RecordType && project.RecordType.Name != 'Indirect PID') {
            headerRowChild.push("PROFILE NAME");
            headerRowChild.push("ALLOCATION STATUS");
            headerRowChild.push("ALLOCATION TYPE");
        }
        var headerRowWrapper = [];
        headerRowWrapper.push(headerRowChild);
        component.set("v.headerRow", headerRowWrapper);
    },
    processMassList: function(component, listOfMassResourceAllocation) {
        var getProcessedCSVRecordsService = component.get("c.getProcessedCSVRecordsService");
        getProcessedCSVRecordsService.setParams({
            "listOfCSVRecordsUploaded": JSON.stringify(listOfMassResourceAllocation),
            "projectId": component.get("v.recordId")
        });
        this.showSpinner(component);
        getProcessedCSVRecordsService.setCallback(this, function(response) {
            this.getProcessedCSVRecordsServiceHandler(response, component);
        });

        $A.enqueueAction(getProcessedCSVRecordsService);
    },
    getProcessedCSVRecordsServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var processedData = JSON.parse(responseData.response);
            component.set("v.mapOfEmployeeVsLendingSDO", processedData.mapOfEmployeeVsLendingSDO);
            this.mergeResourceAllocationLists(component, processedData.CSVData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    mergeResourceAllocationLists: function(component, processedRecords) {
        var mapOfEmployeeVsLendingSDO = component.get("v.mapOfEmployeeVsLendingSDO");
        var listOfResourceAllocation = component.get("v.listOfResourceAllocation");
        var projectId = component.get("v.recordId");

        for (var index = 0; index < processedRecords.length; index++) {
            processedRecords[index]["isEditable"] = true;
            processedRecords[index]["isSelected"] = true;
            processedRecords[index]["GP_Project__c"] = projectId;
            var empId = processedRecords[index]["GP_Employee__c"];
            if (mapOfEmployeeVsLendingSDO && mapOfEmployeeVsLendingSDO[empId]) {
                processedRecords[index].resourceLendingSDO = mapOfEmployeeVsLendingSDO[empId][0];
                processedRecords[index].resourceLendingSDOName = mapOfEmployeeVsLendingSDO[empId][1];
                processedRecords[index].GP_SDO__c = mapOfEmployeeVsLendingSDO[empId][0];
                processedRecords[index].GP_SDO__r = {
                    'Name': mapOfEmployeeVsLendingSDO[empId][1],
                    'GP_Oracle_Id__c': mapOfEmployeeVsLendingSDO[empId][2]
                };
            }
            processedRecords[index].employee = this.createEmployeeHoverString(processedRecords[index]["GP_Employee__r"]);
            processedRecords[index].objEmployee = processedRecords[index]["GP_Employee__r"];
            this.addToListAtParticularIndex(0, listOfResourceAllocation, processedRecords[index]);
        }

        this.removeRowsWithoutEmployee(listOfResourceAllocation);
        component.set("v.listOfResourceAllocation", listOfResourceAllocation);

        var noOfRecordsToDisplayInAView = component.get("v.noOfRecordsToDisplayInAView");
        component.set("v.initialIndexDisplayed", 0);
        this.setListOfResourceAllocationPagination(component, listOfResourceAllocation, 0, noOfRecordsToDisplayInAView);

    },
    removeRowsWithoutEmployee: function(listOfResourceAllocation) {
        for (var index = listOfResourceAllocation.length - 1; index >= 0; index--) {
            if (!listOfResourceAllocation[index]["GP_Employee__c"]) {
                listOfResourceAllocation.splice(index, 1);
            }
        }
    },
    setObjectApiNamesToColumn: function(listOfMassResourceAllocation) {
        var parsedListOfResourceAllocation = [];

        for (var index = 0; index < listOfMassResourceAllocation.length; index++) {
            var parsedResourceAllocationRecord = {};
            var csvRecord = listOfMassResourceAllocation[index];
            for (var key in Object.keys(csvRecord)) {
                var csvFieldLabel = Object.keys(csvRecord)[key];
                var fieldApiName = this.LABEL_FIELD_TO_APINAME_MAP[csvFieldLabel.toLowerCase()];
                var value = csvRecord[csvFieldLabel];
                parsedResourceAllocationRecord[fieldApiName] = value;
            }
            parsedListOfResourceAllocation.push(parsedResourceAllocationRecord);
        }

        listOfMassResourceAllocation = parsedListOfResourceAllocation;
        return listOfMassResourceAllocation;
    }, 
    getConcatenatedTemplate: function(resourceAllocationTemplate) {
        var finalTemplate = '';
        if (resourceAllocationTemplate && resourceAllocationTemplate.GP_Final_JSON_1__c)
            finalTemplate += resourceAllocationTemplate.GP_Final_JSON_1__c;
        if (resourceAllocationTemplate && resourceAllocationTemplate.GP_Final_JSON_2__c)
            finalTemplate += resourceAllocationTemplate.GP_Final_JSON_2__c;
        if (resourceAllocationTemplate && resourceAllocationTemplate.GP_Final_JSON_3__c)
            finalTemplate += resourceAllocationTemplate.GP_Final_JSON_3__c;
        return JSON.parse(finalTemplate);
    }
})