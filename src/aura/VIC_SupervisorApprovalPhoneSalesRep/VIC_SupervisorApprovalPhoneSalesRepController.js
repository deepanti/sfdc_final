({
    
    resetUserDataWrap : function(component, event, helper){ 
        var userOppDataWrap = component.get("v.lstUserDataWrapLine");
        for(var eachUser in userOppDataWrap){
            if(isNaN(userOppDataWrap[eachUser]) && userOppDataWrap[eachUser].objUser.Id == event.currentTarget.id){
                userOppDataWrap[eachUser].isUserSelected = true;
                component.set("v.objParentUserDataWrapLine",userOppDataWrap[eachUser]);
                break;
            }
        }
        
        var lstUserOppDataWrap = component.get("v.lstUserDataWrapLine");
        for(var objuserdata in lstUserOppDataWrap){
            if(isNaN(lstUserOppDataWrap[objuserdata])){
                if(lstUserOppDataWrap[objuserdata].objUser.Id == event.currentTarget.id){
                    lstUserOppDataWrap[objuserdata].isUserSelected = true;
                }else{
                    lstUserOppDataWrap[objuserdata].isUserSelected = false;
                } 
            }
        }
        /* Here we are setting checked checkbos if all user is checked */
        var userSelectionCounter = 0;
        var lstUserOppDataWrap = component.get("v.lstUserDataWrapLine");
        for(var objuserdata in lstUserOppDataWrap){
            if(isNaN(lstUserOppDataWrap[objuserdata])){
                if(lstUserOppDataWrap[objuserdata].isUserChecked){
                	userSelectionCounter++;    
                } 
            }
        }
        //alert(userSelectionCounter);
        if(userSelectionCounter == lstUserOppDataWrap.length){
            component.set("v.checkAllUser",true);
        }else{
            component.set("v.checkAllUser",false);
        }
        
        component.set("v.lstUserDataWrapLine", lstUserOppDataWrap);
	},
    checkedAllUserLevel : function(component, event, helper) {
        var userSelectionCounter = 0;
        var lstUserOppDataWrap = component.get("v.lstUserDataWrapLine");
        for(var objuserdata in lstUserOppDataWrap){
            if(isNaN(lstUserOppDataWrap[objuserdata])){
                if(lstUserOppDataWrap[objuserdata].isUserChecked){
                	userSelectionCounter++;    
                } 
            }
        }
        alert(userSelectionCounter);
        if(userSelectionCounter == lstUserOppDataWrap.length){
            component.set("v.checkAllUser",true);
        }else{
            component.set("v.checkAllUser",false);
        }
    }
})