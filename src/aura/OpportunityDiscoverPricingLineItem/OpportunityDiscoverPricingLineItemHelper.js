({
    convertTCV : function(component, TCV, ISOCode){
        
        var ConversionRates = component.get("v.ConversionRates");
        var rate=1;
        for(var i=0;i<ConversionRates.length;i++)
        {
            if(ConversionRates[i].IsoCode==ISOCode)
            {
                rate = ConversionRates[i].ConversionRate;
                component.set("v.ExchangeRate",rate);
                break;
            }
        }
        TCV = TCV?TCV:0;
        var originalValue,convertedValue;
        if(TCV>=1000000000)
        {
            originalValue=ISOCode?(TCV/1000000000).toFixed(2) +"B " + ISOCode + "=":"";
        }
        else if(TCV>=1000000)
        {
            originalValue=ISOCode?(TCV/1000000).toFixed(2)+"M " + ISOCode + "=":"";
        }
            else if(TCV>=1000)
            {
                originalValue=ISOCode?(TCV/1000).toFixed(2)+"K " + ISOCode + "=":"";
            }
                else
                {
                    originalValue=ISOCode?TCV + " " + ISOCode + "=":"";
                }
        var convertedVal = (TCV/rate);    
        component.set("v.convertedVal",convertedVal);
        if((TCV/rate)>=1000000000)
        {
            convertedValue=((TCV/rate)/1000000000).toFixed(2)+" B USD" ;
        }
        else if((TCV/rate)>=1000000)
        {
            convertedValue=((TCV/rate)/1000000).toFixed(2)+" M USD" ;
        }
            else if((TCV/rate)>=1000)
            {
                convertedValue=((TCV/rate)/1000).toFixed(2)+"K USD" ;
            }
                else
                {
                    convertedValue=(TCV/rate).toFixed(2)+" USD" ;
                }
        
        component.set("v.convertedValue", originalValue + convertedValue);
    },
    validateAll : function(component, event, helper) {
        var hasError=false;
        var errorMessage='';
       // hasError=hasError||helper.validateMandate(component.find("Type_of_Deal__c"));
        hasError=hasError||helper.validateMandate(component.find("Revenue_Start_Date__c"));
        hasError=hasError||helper.validateMandate(component.find("Contract_Term__c"));
        hasError=hasError||helper.validateMandate(component.find("currencyISO"));
        hasError=hasError||helper.validateMandate(component.find("TCV"));
        hasError=hasError||helper.validateMandate(component.find("Delivering_Organisation__c"));
        hasError=hasError||helper.validateMandate(component.find("Sub_Delivering_Organisation__c"));
        hasError=hasError||helper.validateMandate(component.find("Delivery_Location__c"));
        hasError=hasError||helper.validateMandate(component.find("Product_BD_Rep__c"));
       
        if(hasError)
        {
            debugger
            errorMessage = 'Required fields are missing.<br/>';
        }
        
         var Revenue_Start_Date__c = component.find("Revenue_Start_Date__c");
        var Revenue_Start_Date__cName = Revenue_Start_Date__c.get("v.value");
        
         if($A.util.isUndefinedOrNull(Revenue_Start_Date__cName) || Revenue_Start_Date__cName == '')
        {
            $A.util.addClass(Revenue_Start_Date__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Revenue_Start_Date__c,'slds-has-error');
        }
        
        
         var Delivering_Organisation__c = component.find("Delivering_Organisation__c");
        var Delivering_Organisation__cName = Delivering_Organisation__c.get("v.value");
        
         if($A.util.isUndefinedOrNull(Delivering_Organisation__cName) || Delivering_Organisation__cName == '')
        {
            $A.util.addClass(Delivering_Organisation__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Delivering_Organisation__c,'slds-has-error');
        }
        
        
         var Sub_Delivering_Organisation__c = component.find("Sub_Delivering_Organisation__c");
        var Sub_Delivering_Organisation__cName = Sub_Delivering_Organisation__c.get("v.value");
         if(Delivering_Organisation__cName != ''){
         if($A.util.isUndefinedOrNull(Sub_Delivering_Organisation__cName) || Sub_Delivering_Organisation__cName == '')
        {
            $A.util.addClass(Sub_Delivering_Organisation__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Sub_Delivering_Organisation__c,'slds-has-error');
        }
         }
        
		 var Product_BD_Rep__c = component.find("Product_BD_Rep__c");
        var Product_BD_Rep__cName = Product_BD_Rep__c.get("v.value");
        
         if($A.util.isUndefinedOrNull(Product_BD_Rep__cName) || Product_BD_Rep__cName == '')
        {
            $A.util.addClass(Product_BD_Rep__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Product_BD_Rep__c,'slds-has-error');
        }
                
        
         var currencyISO = component.find("currencyISO");
        var currencyISOName = currencyISO.get("v.value");
        
         if($A.util.isUndefinedOrNull(currencyISOName) || currencyISOName == '')
        {
            $A.util.addClass(currencyISO,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(currencyISO,'slds-has-error');
        }
        // To keep Type of Deal as mandatory field
         /* var Type_of_Deal__c = component.find("Type_of_Deal__c");
        var Type_of_Deal__cName = Type_of_Deal__c.get("v.value");
        
         if($A.util.isUndefinedOrNull(Type_of_Deal__cName) || Type_of_Deal__cName == '')
        {
            $A.util.addClass(Type_of_Deal__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Type_of_Deal__c,'slds-has-error');
        }
        
        */
          var Delivery_Location__c = component.find("Delivery_Location__c");
        var Delivery_Location__cName = Delivery_Location__c.get("v.value");
        
         if($A.util.isUndefinedOrNull(Delivery_Location__cName) || Delivery_Location__cName == '')
        {
            $A.util.addClass(Delivery_Location__c,'slds-has-error');
              hasError = true;
        }
        else
        {
            $A.util.removeClass(Delivery_Location__c,'slds-has-error');
        }
        
        if(component.find("Contract_Term__c").get("v.value")<=0){     
            $A.util.addClass(component.find("Contract_Term__c"),'slds-has-error');
            errorMessage += 'Contract term cannot be zero or less than zero<br/>';
            hasError=true;
        }
        if(component.find("TCV").get("v.value") <=0){
            $A.util.addClass(component.find("TCV"),'slds-has-error');
            errorMessage += 'Total TCV cannot be zero or less than zero<br/>';
            hasError=true;
        }
        if(component.find("Revenue_Start_Date__c").get("v.value") < component.find("CloseDate").get("v.value"))
        {
            $A.util.addClass(component.find("Revenue_Start_Date__c"),'slds-has-error');
            errorMessage += 'Revenue start date cannot be earlier than MSA/SOW closure date<br/>';
            hasError=true;
        } 
       if(component.find("FTE__c").get("v.value") !== null && component.find("FTE__c").get("v.value") !== ''){    
            if(component.find("FTE__c").get("v.value") <0 || component.find("FTE__c").get("v.value") >999999){
                $A.util.addClass(component.find("FTE__c"),'slds-has-error');
                errorMessage += 'Please enter a valid FTE number between 1- 999999<br/>';
                hasError=true;
            }
        }
        if(component.get("v.OLI").Opportunity.QSRM_Status__c == 'Your QSRM is submitted for Approval'){
            errorMessage += "QSRM has been submitted and is currently in the approval phase. Once approved, you may edit your opportunity."; 
            hasError=true;
        }
        component.set("v.errorMessage",errorMessage);
        if(!hasError)
        {
            var OLIToUpdate = component.get("v.OLIToUpdate");
            OLIToUpdate.Type_of_Deal__c=component.find("Type_of_Deal__c").get("v.value");
            OLIToUpdate.Revenue_Start_Date__c=component.find("Revenue_Start_Date__c").get("v.value");
            OLIToUpdate.Local_Currency1__c=component.find("currencyISO").get("v.value");
            OLIToUpdate.Contract_Term__c=component.find("Contract_Term__c").get("v.value");
            OLIToUpdate.Local_Currency__c=component.find("currencyISO").get("v.value");
            OLIToUpdate.TCV__c=component.find("TCV").get("v.value");
            OLIToUpdate.Delivering_Organisation__c=component.find("Delivering_Organisation__c").get("v.value");
            OLIToUpdate.Sub_Delivering_Organisation__c=component.find("Sub_Delivering_Organisation__c").get("v.value");
            OLIToUpdate.Delivery_Location__c=component.find("Delivery_Location__c").get("v.value");
            OLIToUpdate.Product_BD_Rep__c=component.find("Product_BD_Rep__c").get("v.value");
            OLIToUpdate.Revenue_Exchange_Rate__c=component.get("v.ExchangeRate");
            OLIToUpdate.FTE__c=component.find("FTE__c").get("v.value");
            OLIToUpdate.Resell__c=component.find("Resell__c").get("v.value");
            OLIToUpdate.Id=component.get("v.OLI.Id");
            return OLIToUpdate;
        }
    },
    validateMandate : function(inputField){
        var hasError=false;
        if($A.util.isUndefinedOrNull(inputField.get("v.value")) || inputField.get("v.value") == '')
        {
            $A.util.addClass(inputField,'slds-has-error');
            hasError = true;
        }
        else
        {
            $A.util.removeClass(inputField,'slds-has-error');
        }
        return hasError;
    },
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})