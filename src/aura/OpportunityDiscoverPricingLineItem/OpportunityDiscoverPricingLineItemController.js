({  
    doInit : function(component, event, helper){
        
    },
    onFocusOutTCV : function(component, event, helper){
        var Contract_Term = component.find("Contract_Term__c");
        var Local_Currency = component.find("currencyISO");
        var Delivering_Organisation = component.find("Delivering_Organisation__c");
        var Sub_Delivering_Organisation = component.find("Sub_Delivering_Organisation__c");
        var Delivery_Location = component.find("Delivery_Location__c");
        var FTE = component.find("FTE__c");
        var Resell = component.find("Resell__c"); //Added By Neha
        var Product_BD_Rep = component.find("Product_BD_Rep__c");
        var ContractTermVal = Contract_Term.get("v.value");
        var LocalCurrencyVal = Local_Currency.get("v.value");
        var DeliveringOrganisationVal = Delivering_Organisation.get("v.value");
        var SubDeliveringOrganisationVal =  Sub_Delivering_Organisation.get("v.value");
        var DeliveryLocationVal = Delivery_Location.get("v.value");
        var  FTEVal = FTE.get("v.value");
        var  ProductBDRepVal = Product_BD_Rep.get("v.value");
        var ReselVal;
        if(Resell != null ){
        ReselVal=Resell.get("v.value");
        }
        
        var confirmation = false;
        if(component.find("Type_of_Deal__c").get("v.value")=='Renewal')
        {
            if(ContractTermVal == '' || LocalCurrencyVal == '' || DeliveringOrganisationVal == '' || SubDeliveringOrganisationVal == '' || DeliveryLocationVal == '' || FTEVal == '' || ProductBDRepVal == '' || ReselVal=='' )
            {
                component.set("v.showTCV",'true');
                 
            }
        }
        var Opportunity_Source__c = component.get("v.OLI.Opportunity.Opportunity_Source__c");
        var Type_of_Deal__c = component.find("Type_of_Deal__c").get("v.value");
        var OriginalTCV = component.get("v.OriginalTCV");
        var TCVTest = component.find("TCV").get("v.value");
        if(component.get("v.OLI.Opportunity.Opportunity_Source__c")=="Renewal" && component.find("Type_of_Deal__c").get("v.value")=='Renewal' && component.get("v.OriginalTCV") < parseFloat(component.find("TCV").get("v.value")))
        {
            confirmation = confirm('Changing the TCV for the product will result in addition of new product with the differential TCV as new booking. Do you want to continue?');
            if(confirmation){
                component.set("v.Spinner",true);
                var TCVValue = (component.find("TCV").get("v.value") - component.get("v.OriginalTCV"));
                var FTE = component.find("FTE__c");
                
                var NewOLI =[];
                var OLI={'sobjectType':'OpportunityLineItem',
                         'Product2Id':component.get("v.OLI.Product2Id"),
                         'TCV__c' : TCVValue,
                         'OpportunityId' : component.get("v.OLI.OpportunityId"),
                         'Type_of_Deal__c' : 'Booking', 
                         'Contract_Term__c' : Contract_Term.get("v.value"),
                         'Local_Currency__c': Local_Currency.get("v.value"),
                         'Delivering_Organisation__c' : Delivering_Organisation.get("v.value"),
                         'Sub_Delivering_Organisation__c' : Sub_Delivering_Organisation.get("v.value"),
                         'Delivery_Location__c': Delivery_Location.get("v.value"), 
                         'FTE__c': FTE.get("v.value"),
                         'Product_BD_Rep__c': Product_BD_Rep.get("v.value"),
                         'Resell__c':Resell.get("v.value")
                        };
                var action = component.get("c.insertOLI");
                action.setParams({
                    OLIStr: JSON.stringify(OLI),
                    OLIIdToUpdate: component.get("v.OLI.Id")
                });
                action.setCallback(this,function(response){
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        //document.location.reload(true);
                        var compEvent = component.getEvent("reloadEvent");
                        compEvent.fire();
                    }
                       else if (state === "INCOMPLETE") {
           					component.set("v.errMessage",'Connection Lost');
            			}
                    else{
                        /*console.log("Exception caught successfully");
                        console.log("Error object", response);
                        console.log("Error Message", response.getError()[0]);
                        console.log("Error Message", response.getError()[0].message);
                        console.log("Error Message", response.getState());
                        console.log("Error object", JSON.stringify(response));*/
                        component.set("v.errMessage", response.getError()[0].message);
                        //console.log('TODO : Add products and refresh')
                        
                    }
                });
                $A.enqueueAction(action);
            }
            else
            {
                event.preventDefault(); component.find("TCV").set("v.value",component.find("TCV").get("v.value"));
                helper.convertTCV(component,component.get("v.OriginalTCV"), component.find("currencyISO").get("v.value"));
               
            }
        }
    },
    handleConversion :function(component, event, helper)
    {
        component.set("v.CurrencyISO", component.find("currencyISO").get("v.value"));
        var TCV = parseFloat(component.find("TCV").get("v.value"));
        helper.convertTCV(component,TCV, component.find("currencyISO").get("v.value"));
    },
    saveRecords :function(component, event, helper)
    {
        return helper.validateAll(component, event, helper);
    },
    handleLoad : function(component, event, helper){
        debugger;
        var isLoad = component.get("v.preventOnload");        
        var closeDate = new Date(component.get("v.Opportunity.CloseDate"));
        closeDate.setDate(closeDate.getDate() + 1);        
        
        var TCV__c = event.getParams("record").recordUi.record.fields['TCV__c'];   
        if(!$A.util.isUndefinedOrNull(TCV__c) && TCV__c != '' && !$A.util.isUndefinedOrNull(TCV__c.value) && TCV__c.value !='')
        	component.set("v.OriginalTCV",TCV__c.value);
        
        var val = component.get("v.inputTCV");
        if($A.util.isUndefinedOrNull(val) || val == 0)
            component.set("v.inputTCV",TCV__c.value);        
        
        var revenueStartDate = event.getParams("record").recordUi.record.fields['Revenue_Start_Date__c'];
        if($A.util.isUndefinedOrNull(revenueStartDate) || $A.util.isUndefinedOrNull(revenueStartDate.value) || revenueStartDate.value == ''){
            component.set("v.RevenueStartDate",closeDate.toISOString());
        }
        else
            component.set("v.RevenueStartDate",revenueStartDate.value);
        
        var productBDRep = event.getParams("record").recordUi.record.fields['Product_BD_Rep__c'];
        if($A.util.isUndefinedOrNull(productBDRep) || $A.util.isUndefinedOrNull(productBDRep.value) || productBDRep.value == '')
             component.set("v.UserId",$A.get('$SObjectType.CurrentUser.Id'));
        else
             component.set("v.UserId",productBDRep.value);
        
        var Contract_Term__c = event.getParams("record").recordUi.record.fields['Contract_Term__c'];
        var contractVal = component.get("v.contractTerm");
        if(($A.util.isUndefinedOrNull(contractVal) || contractVal == '') && (!$A.util.isUndefinedOrNull(Contract_Term__c) || Contract_Term__c.value != ''))
            component.find("Contract_Term__c").set("v.value",Contract_Term__c.value);
		
        var localCurrency = event.getParams("record").recordUi.record.fields['Local_Currency__c'];
        if(!$A.util.isUndefinedOrNull(localCurrency) && !$A.util.isUndefinedOrNull(localCurrency.value))
        	helper.convertTCV(component,component.get("v.inputTCV"),localCurrency.value);
        
        component.set("v.preventOnload",false);
        component.set("v.Spinner",false);
    }
})