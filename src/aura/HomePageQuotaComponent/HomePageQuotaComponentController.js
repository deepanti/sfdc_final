({
    doInit : function(component, event, helper) {
        //Fetch report data using apex
        var action = component.get("c.getReportData");
        action.setParams({
            reportId : component.get("v.ReportId"),
            showGrandSummaryOnly : component.get("v.showGrandSummaryOnly")
        });
        action.setCallback(this, function(data) {
            if(data.getState() === 'SUCCESS')
            {
               debugger;
                //If successful, show numbers
                var reportDataList = data.getReturnValue();
                var groupings = [];
                for(var i=0;i<reportDataList.length;i++)
                {
                    //process server data
                    if(reportDataList[i].Label=='T!T')
                    {
                        component.set("v.GrandNumberToDisplay", reportDataList[i].Value);
                        //break if only grand summary to display
                        if(component.get("v.showGrandSummaryOnly"))
                        break;
                    }
                    else if(!component.get("v.showGrandSummaryOnly"))
                        groupings.push(reportDataList[i]);
                }
                component.set("v.groupings",groupings);
                component.set("v.isLoading",false);
            }
            else
            {
                //Check if error has been returned
                helper.showToast('error',data.getError()[0].message);
                component.set("v.isLoading",false);
            }
        });
        $A.enqueueAction(action);
    },
    redirectToReport : function(component, event, helper) {
        //Redirect to the report details
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": component.get("v.url")
        });
        navEvt.fire();
    }
})