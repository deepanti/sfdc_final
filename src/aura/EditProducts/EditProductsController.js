({
	init : function(component, event, helper) {
        
		helper.getOpportunityProducts(component);
		helper.getLocalCurrency(component);
       	helper.getDeliveringOrganization(component); 
       	helper.getDeliveryLocation(component);   
        //helper.getOpportunityclosedDate(component);
       	helper.getQSRMDetails(component);
		//alert('=============777777777777=========='+component.get("v.opportunityProductList"));
	},
    
     getSubDeliveringOprganization :function(component, event, helper)
	 {
        var selectedItem = event.currentTarget; // Get the target object
     	var index =  event.currentTarget.getAttribute('data-record');  
        var OLIList = component.get("v.opportunityProductList");
        //alert('======index=======getSubDeliveringOprganization=========='+index);
		// alert('======OLIList=======getSubDeliveringOprganization=========='+JSON.stringify(OLIList));
        var selectedDeliveringOrganization = OLIList[index].DeliveringOrganisation;
        var action = component.get("c.getFieldDependencies");
        
        action.setParams({
            	"controllingPickListValue" : selectedDeliveringOrganization
        });
         action.setCallback(this, function(response){            
             OLIList[index].subOrganizationFlag = false;
             OLIList[index].SubDeliveringOrganizationList = [];
             
             Array.prototype.push.apply(OLIList[index].SubDeliveringOrganizationList,  response.getReturnValue());
             component.set("v.opportunityProductList", OLIList); 
         });  
        $A.enqueueAction(action);
    },
    
    saveOpportunityProductList : function(component, event, helper){  
        component.set("v.isLoading", true);
        var opportunityProductList = component.get("v.opportunityProductList");
        var index;
        var bdRepFlag = true;
        var contractTermFlag = true;
        var closedDateFlag = true;
        var parts;
        var revenueStartDate; 
       	var closedDateParts = component.get("v.closedDate").split('-');
        var closedDate = new Date(closedDateParts[0], closedDateParts[1]-1,  closedDateParts[2], 0,0,0,0);
        var today = new Date();
        var subDeliveryOrganizationFlag = true;
        var subDeliveryOrg;
       var loggedInUser = component.get("v.loggedInUser");
         var fteFlag = true;
        var tcvFlag = true;
        
            for(index in opportunityProductList){
                if(opportunityProductList[index].RevenueStartDate.indexOf('/') !== -1){
                    parts =opportunityProductList[index].RevenueStartDate.split('/');
                    revenueStartDate = new Date( parts[2], parts[0]-1,  parts[1], 0,0,0,0);
                }
                else{
                    parts =opportunityProductList[index].RevenueStartDate.split('-');
                    revenueStartDate = new Date( parts[1], parts[0]-1,  parts[2], 0,0,0,0);
                }
                
                if(isNaN(opportunityProductList[index].ContractTerm)){
                    contractTermFlag = false;     
                }
                else{
                    var contractTerm = Number(opportunityProductList[index].ContractTerm);
                }
                if(opportunityProductList[index].ProductBDRep){
                    bdRepFlag = true;
                }
                else{
                    bdRepFlag = false;   
                    break;
                }
                if(contractTerm >0 && contractTerm % 1 === 0){
                    contractTermFlag = true; 
                    
                }            
                else{
                    contractTermFlag = false;
                    break; 
                }
                if( revenueStartDate.getTime() >= closedDate.getTime()){
                    closedDateFlag = true;
                    
                }
                else{
                    closedDateFlag = false;
                    break;
                }
                if(!isNaN(opportunityProductList[index].FTE)){
                    if(opportunityProductList[index].FTE % 1 === 0){
                        fteFlag = true;
                    }
                    else{
                        fteFlag = false;
                        break;
                    }
                }
                else if(!opportunityProductList[index].FTE || opportunityProductList[index].FTE == 0){
                    fteFlag = true;
                }
                else{
                    fteFlag = false;
                    break;     
                }
                
                if(!isNaN(opportunityProductList[index].TotalPrice) || (!opportunityProductList[index].TotalPrice 
                          || opportunityProductList[index].TotalPrice == 0)){
                    tcvFlag = true;
                }
                else{
                    tcvFlag = false;
                    break;     
                }
                
                if(opportunityProductList[index].SubDeliveringOrganizationList.length > 0 && 
                   opportunityProductList[index].SubDeliveringOrganisation){
                     subDeliveryOrganizationFlag = true;
                }
                else if(opportunityProductList[index].SubDeliveringOrganizationList.length == 0){
                    subDeliveryOrganizationFlag = true;
                    opportunityProductList[index].SubDeliveringOrganisation = subDeliveryOrg;
                }
                else if(opportunityProductList[index].SubDeliveringOrganizationList.length > 0 &&
                       !opportunityProductList[index].SubDeliveringOrganisation){
                    subDeliveryOrganizationFlag = false;
                    break;
                }
            }
        if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
        	closedDateFlag = true;	    
        }
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
            allValid = true;
        }
            if (allValid && contractTermFlag && closedDateFlag && bdRepFlag && subDeliveryOrganizationFlag && fteFlag && tcvFlag) {
                if(component.get("v.QSRMStatus") == 'Your QSRM is submitted for Approval' && loggedInUser.Profile.Name != 'Genpact Super Admin'){
                    component.set("v.errorMessage", "While QSRM is in approval status, the deals cannot be edited.");
                    component.set("v.isError", true);
                    component.set("v.isLoading", false);
                    window.setTimeout(
                        $A.getCallback(function() {
                            if(component.isValid()){
                                component.set("v.isError", false);    
                            }    
                        }),2000
                    );
                }
                else{
                    var recordId = component.get("v.recordId");
                    var action  = component.get("c.saveOpportunityProducts");
                    
                    
                    function isLightningExperienceOrSalesforce1() {
                        return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
                    }         
                    
                    action.setParams({
                        "opportunityProductList" : JSON.stringify(opportunityProductList),
                        "closedDate" : component.get("v.closedDate")
                    });
                    
                    action.setCallback(this, function(response){ 
                        if(response.getReturnValue()){
                            component.set("v.isSuccess", true);
                            if(isLightningExperienceOrSalesforce1()) {
                                //sforce.one.navigateToSObject(recordId,"OPPORTUNITY");
                                sforce.one.back(true);
                            }
                            else{
                                window.location = '/'+recordId;
                            }
                        }
                        else{
                            component.set("v.errorMessage", "Please Enter the correct values and try again.");
                            component.set("v.isError", true);
                            component.set("v.isLoading", false);
                            window.setTimeout(
                                $A.getCallback(function() {
                                    if(component.isValid()){
                                        component.set("v.isError", false);    
                                    }    
                                }),2000
                            );
                        }
                        
                    });
                    $A.enqueueAction(action);    
                }
            }
            else{
                if(!contractTermFlag){
                    component.set("v.errorMessage", 'Please enter contract term greater than 0');   
                }
                else if(!closedDateFlag){
                    component.set("v.errorMessage", 'Revenue Start Date should be greater than MSA /SOW Closure Date');    
                }
                else if(!bdRepFlag){
                	component.set("v.errorMessage", 'Please enter Product BD Rep');
                }
                else if(!subDeliveryOrganizationFlag){
                	component.set("v.errorMessage", "Please enter Sub delivery Organization");
                }
                else if(!fteFlag){
                     component.set("v.errorMessage", "Make Sure there is no decimal value / special character like comma / space in FTE.");   
                }
                else if(!tcvFlag){
                      component.set("v.errorMessage", "Make Sure there is no decimal value / special character like comma / space in TCV.");   
                }
                else if(!allValid){
                	component.set("v.errorMessage", 'Please fill all fileds');    
                }
                component.set("v.isLoading", false);
                component.set("v.isError", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),2000
                );
            }
	},
    
    handleClickCancel : function(component, event, helper){
        var recordId = component.get("v.recordId");
    	function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }   
         if(isLightningExperienceOrSalesforce1()) {
        	sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
         }
        else{
           window.location = '/'+recordId; 
        }
    },
    
    closeSuccessToast : function(component, event, helper)
	{
       component.set("v.isSuccess", false);
    },
    
    closeErrorToast : function(component, event, helper){
    	component.set("v.isError", false);
	},
	
	getEditProductDetails : function(component, event, helper)
    {            
		//alert('====CallSearchProductForDeleteProduct=====1======start==='); 
		var recordId = component.get("v.recordId"); 
		var appEvent = $A.get("e.c:NavigateToEditProdDetails"); // event    
		//alert('=======1========='); 
           appEvent.setParams({               
                "Teststr": 'neha',                         
                "recordId" : component.get("v.recordId")               
            }); 
		//alert('=========2========='); 
		appEvent.fire();
		//alert('====CallSearchProductForDeleteProduct=====1===recordId===end==='+recordId);			
    },
	
	openModel: function(component, event, helper) 
    {
        //alert('=====openModel=========');
		
		var selectedItem = event.currentTarget; // Get the target object
     	var index =  event.currentTarget.getAttribute('data-record');  
        var OLIList = component.get("v.opportunityProductList");
		var selectedOLI = OLIList[index];
		
		/*component.set("v.filterList", selectedOLI);	
		component.set("v.isOpen", true);*/
		debugger;
		 $A.createComponent(
        "c:EditProducts_DetailPage",   //"c:SearchProducts",
        
         {
             "serchFilterList" : selectedOLI,
             "closedDate" : component.get("v.closedDate")
         },
         function(newCmp){
             //alert('component.isValid()=='+component.isValid());
            if (component.isValid()) {
              component.set("v.body", newCmp);
            }
         }
      );	
			  
   },
 
    closeModel: function(component, event, helper) 
    {
       //  alert('=====closeModel=========');
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
	 // alert('=============='+component.get("v.isOpen"));
   },
 
   likenClose: function(component, event, helper) 
    {
         //alert('=====likenClose=========');
      // Display alert message on the click on the "Like and Close" button from Model Footer 
      // and set set the "isOpen" attribute to "False for close the model Box.
      //alert('thanks for like Us :)');
      component.set("v.isOpen", false);
   }
	
	
    /*deleteOpportunityProduct : function(component, event, helper){
    	
     	var index =  event.currentTarget.getAttribute('data-record');  
        var OLIList = component.get("v.opportunityProductList");
       	var selectedOpportunityProductID = OLIList[index].Id;  
        var action  = component.get("c.deleteOpportunityProducts");  
        
        action.setParams({
            "selectedOpportunityProductID" : selectedOpportunityProductID 
        });
        
        action.setCallback(this, function(response){
            alert('Opportunity Product has been deleted successfully!!');
            if(OLIList.length == 1){
              component.set("v.opportunityProductList", []);
            }
            else{
                var index = OLIList.findIndex(function(item) {return item.Id == selectedOpportunityProductID})
            	OLIList.splice(index, 1)
             	component.set("v.opportunityProductList", OLIList);   
            }            
        });
        $A.enqueueAction(action);
    } */
    
})