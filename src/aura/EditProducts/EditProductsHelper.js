({
	getOpportunityProducts : function(component) {
		var recordId = component.get("v.recordId");
        var action = component.get("c.getOpportunityProducts");
       var opportunityProductList = component.get("v.opportunityProductList");
        
        action.setParams({
            "recordId" : recordId 
        });
        
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var index;
                var OLIList = response.getReturnValue();
                var SubDeliveringOrganizationList;
            	for(index in OLIList){
                    var OLI = OLIList[index];
                    
                    if(OLI.Sub_Delivering_Organisation__c){
                    	SubDeliveringOrganizationList = [OLI.Sub_Delivering_Organisation__c];	    
                    }
                    else{
                        SubDeliveringOrganizationList = [];
                    }
                    var opportunityProduct = {
                        "Id" : OLI.Id,
                        "Product2Id"  :  OLI.Product2Id,
                        "ProductName" : OLI.Product2.Name,
                        "OpportunityId" : OLI.OpportunityId,
                        "ContractTerm" : OLI.Contract_Term__c,
                        "LocalCurrency" : OLI.Local_Currency__c,
                        "TotalPrice" : OLI.UnitPrice,
                        "RevenueStartDate" : OLI.Revenue_Start_Date__c,
                        "DeliveringOrganisation" : OLI.Delivering_Organisation__c,
                        "SubDeliveringOrganisation" : OLI.Sub_Delivering_Organisation__c,
                        "ProductBDRep" : OLI.Product_BD_Rep__c,
                        "DeliveryLocation" : OLI.Delivery_Location__c,
                        "FTE" : OLI.FTE__c,
                        "SubDeliveringOrganizationList" : SubDeliveringOrganizationList,
                        "RevenueExchangeRate" : OLI.Revenue_Exchange_Rate__c
                    }
                    opportunityProductList.push(opportunityProduct);
                }
            component.set("v.opportunityProductList", opportunityProductList);    
            }  
        });
        $A.enqueueAction(action);
	},
    
    getLocalCurrency : function(component){
        var action = component.get("c.getLocalCurrency");
        action.setCallback(this, function(response){            
            component.set("v.LocalCurrency", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
     getDeliveringOrganization : function(component){
        var action = component.get("c.getDeliveringOrganization");
        action.setCallback(this, function(response){            
            component.set("v.DeliveringOrganization", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
        
     getDeliveryLocation : function(component){
        var action = component.get("c.getDeliveryLocation");
        action.setCallback(this, function(response){            
            component.set("v.DeliveryLocation", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    /*getOpportunityclosedDate : function(component){
    	var action = component.get("c.getOpportunityClosedDate");
        var recordId = component.get("v.recordId");
        action.setParams({
            "opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){     
            var date1 = new Date(response.getReturnValue().closedDate);
            //alert(date1);
            component.set("v.closedDate", date1);
        });
         $A.enqueueAction(action);
    },*/
    
    getQSRMDetails : function(component, event, helper){
        var action = component.get("c.getQSRMDetails");
        action.setParams({
            "opportunityID" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response){
            var returnedValue = response.getReturnValue();
            //alert( returnedValue.oppClosedDate);
            component.set("v.closedDate", returnedValue.oppClosedDate);
            component.set("v.endDate", returnedValue.endDate);
            component.set("v.QSRMStatus", returnedValue.QSRMStatus);
           	component.set("v.loggedInUser", returnedValue.loggedInUser);
            var oppstage = returnedValue.StageName;
            var loginuser = returnedValue.loggedInUser;
            var QSRMstatus = returnedValue.QSRMStatus;
            
            var loggedInUser = component.get("v.loggedInUser");
            
            //alert(':--->'+oppstage);
            if((oppstage == '5. Confirmed' || oppstage == '6. Signed Deal' || QSRMstatus == 'Your QSRM is submitted for Approval') && loggedInUser.Profile.Name != 'Genpact Super Admin') {
                component.set("v.iseditable", false);
            }
            //if(component.get("v.QSRMStatus") == 'Your QSRM is submitted for Approval' && loggedInUser.Profile.Name != 'Genpact Super Admin'){
            var closedDateParts = returnedValue.oppClosedDate.split('-');
            var closedDate = new Date(closedDateParts[0], closedDateParts[1]-1,  closedDateParts[2], 0,0,0,0);
            var todayDate = new Date();
           	
            
            if(closedDate.getTime() < todayDate.getTime() && loggedInUser.Profile.Name != 'Genpact Super Admin'){
               component.set("v.errorMessage", "Please update MSA/SOW date of Opportunity later than today.");
                component.set("v.isError", true);
                component.set("v.isDisabled", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),5000
                );
            } 
        });
        $A.enqueueAction(action);
    }
})