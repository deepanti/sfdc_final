({
    doInit:function(component, event, helper) {
        // to show spinner on click of component & before load of component
        helper.showSpinner(component, event, helper);
        helper.createGCICompononent(component, event, helper,$A.get("$Label.c.OpportunityPursuitProfiler"));
        helper.hideSpinner(component, event, helper);
        
        
        
    },
    saveAndNext : function(component, event, helper) {
        //  debugger
        var IsRSparameterapprove = component.find("IsRSparameterapprove").get("v.value");
        var Amount = component.find("Amount").get("v.value");
        var oppSubSource = component.find("oppSubSource").get("v.value");
        var submitStatus = component.find("submitStatus").get("v.value");
        
        if(IsRSparameterapprove == false  && submitStatus==true){
            helper.fireToastEvent(component, event, helper, 'This opportunity is already awaiting for approval. The deal will move to next tab once approved.');
            
        }
        else if(IsRSparameterapprove == false && Amount>=10000000 && oppSubSource!=='Renewal'){
            helper.fireToastEvent(component, event, helper, 'GCI Tools are mandatory and must be submitted for approval.');
            
        }
            else 
            {
                
                component.set("v.Spinner", false);
                if(localStorage.getItem('MaxTabId')<11){
                    localStorage.setItem('MaxTabId',11);
                    component.set("v.MaxSelectedTabId", "11");
                    component.find("recordViewForm").submit();
                } 
                var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
                appEvent.setParams({"selectedTabId" : "11"});
                appEvent.fire();
                localStorage.setItem('LatestTabId',"11");
                window.scrollTo(0, 0);
            }
    },
    back : function(component, event, helper) {
        component.set("v.MaxSelectedTabId", "9");
        localStorage.setItem('LatestTabId','9');
        var MaxSelectedTabId = component.get("v.MaxSelectedTabId");
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : MaxSelectedTabId });
        compEvents.fire();
        window.scrollTo(0, 0);
    },
    showSpinner : function(component, event, helper){
        component.set("v.Spinner", true);  
    },
    showToast : function(component, event, helper) {
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastEvent(component, event, helper, errorMsg);
    },
    fireEvent: function(component, event, helper) {
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully in Define stage.');
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "9"});
        compEvents.fire();
    },
    hideSpinner : function(component,event,helper){
        
        // make Spinner attribute to false for hide loading spinner    
        helper.hideSpinner(component, event, helper);
    },
    openModal : function(component, event, helper){
        component.set("v.isVisible", true);  
    },
    closeModal : function(component, event, helper){
        component.set("v.isVisible", false);  
    },
     submitForApproval : function(component, event, helper){
       //var allValid=helper.validation(component, event, helper);
       // if(allValid)
        //{
            var oppId=component.get("v.recordId");
            helper.submitForApproval(component, event, helper,oppId);
        /*}
        else
        {
            helper.fireToastError('Required fields are missing'); 
        } */ 
    }
    
})