({
	createGCICompononent:function(component, event, helper,pageName){
        // to create respective component with the VF page for relationbarometer & Pursuit Profiler
        $A.createComponent(
            "c:OpportunityVFPageRerender",
            { 
                "pagename": pageName,
                "recordId":  component.get("v.recordId"),
                "height" : '300px'
            },
            function(newComponent){
                if (component.isValid()) { 
                    component.set("v.body",newComponent);
                    helper.hideSpinner(component, event, helper);
                }
                else {
                    throw new Error(error);
                }
            }
        );
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner      
        component.set("v.Spinner", true); 
    }, 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastEvent : function(component, event, helper, errorMsg) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "Error",
            "message": errorMsg,
            "type": "error"
        });
        toastEvent.fire();
    },
    
    submitForApproval:function(component, event, helper) { 
        component.set("v.Spinner",true);
        var action = component.get("c.submitForApprovalMethod");
        action.setParams(
            {

                comments:component.get("v.comments"),
                oppID:component.get("v.recordId")
                
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.fireToastSuccess(component, event, helper,'GCI tools for the opportunity have been submitted to Client Partner for approval. Once approved, the deal can be moved to On-Bid stage.');
                 component.set("v.comments",'');
                 component.set("v.isVisible",false);
                component.set("v.Spinner",false);
               }     
            else
            {
                 component.set("v.comments",'');
                 component.set("v.isVisible",false);
                component.set("v.Spinner",false);
                 this.fireToastEvent(component, event, helper,response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
	
    }
})