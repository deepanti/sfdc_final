({
    fetchData: function (component, event,helper) {        
        var action = component.get("c.fetchACRPendingForApporval");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                component.set('v.acrlist',result);
                component.set('v.count',result.length);
            }
            else{                
                helper.showToast(response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    pendingApprovalRecords : function(component,event,helper){   
        component.set("v.loaded",true);
        var action = component.get("c.approvePendingItems");
        action.setParams({
            approvalPendingItemId : component.get("v.recId"),
            comments : component.get("v.comments"),
            action : component.get("v.action")
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
         
            if (state === "SUCCESS") {
                var result = response.getReturnValue();   
               
                if(result)
                {
                    var acrLst = component.get("v.acrlist");
                    var approvalId = component.get("v.recId");
                    var count = 0;                    
                    for(var i=0;i<acrLst.length;i++)
                    {
                        if(acrLst[i].approvalItemId == approvalId)
                        {    
                            count = i;
                        }
                    }                    
                    acrLst.splice(count,1);
                    component.set("v.acrlist",acrLst);
                    component.set("v.count",acrLst.length);
                    component.set("v.loaded",false);
                    component.set("v.comments",'');
                    if(component.get("v.action")=='Approve')
                    	helper.showToast("success", "Account Creation Request has been Approved successfully");
                    else
                        helper.showToast("success", "Account Creation Request has been Rejected successfully");
                }
                else
                {
                    helper.showToast("error", "Unable to Approve Account Creation Request. Contact System admin.");
                    component.set("v.loaded",false);
                    component.set("v.comments",'');
                }
            }
            else{
                helper.showToast("error",response.getError()[0].message);
                component.set("v.loaded",false);
                component.set("v.comments",'');
            }
        });
        $A.enqueueAction(action);
    },
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})