({
    init: function (component, event, helper) {
        helper.fetchData(component,event,helper);
    },
    handleApprovalEvent:function (component, event, helper) {
        var recId = event.getParam("approvalRecordId");
        var action = event.getParam("actionToPerform");

        if(!$A.util.isUndefinedOrNull(recId))
        {
            component.set("v.recId",recId);
        }
        if(!$A.util.isUndefinedOrNull(action))
        {
            component.set("v.action",action);
        }
        component.set("v.modal",true);
    },
    closeModal:function (component, event, helper) {
        component.set("v.modal",false);
        component.set("v.comments",'');
    },
    approveRecord : function(component,event,helper){
        
        var comments = component.get("v.comments");
        var comm = component.find("comments");
        if($A.util.isUndefinedOrNull(comments) || comments == '')
        {
            $A.util.addClass(comm,'slds-has-error');
            helper.showToast("error", "Please Provide Appropriate Comments.");
        }
        else
        {
            $A.util.removeClass(comm,'slds-has-error');
            component.set("v.modal",false);
            helper.pendingApprovalRecords(component,event,helper);   
        }
    }
})