({
	// Your renderer method overrides go here
    render :function(component,helper)
    {
        //Get svn component value
        var classname=component.get("v.class");
        var ariahidden=component.get("v.aria-hidden");
        var xlinkhref=component.get("v.xlinkhref");
        //Fetch salforce svg attribute
        var svg=document.createElementNS("http://www.w3.org/2000/svg", "svg");
        svg.setAttribute('class',classname);
        svg.setAttribute('aria-hidden',ariahidden);
        svg.innerHTML='<use xlink:href="'+xlinkhref+'"></use>';
        return svg;
    }
})