({
    doInit : function(component, event, helper) {
        var action = component.get("c.getLatestTabId");
        action.setParams({
            oppId : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var tabId,maxTabId;
            
            if(response.getState()=='SUCCESS')
            {
                var opp = response.getReturnValue();
                if(opp.IsClosed)
                {
                    tabId='1';
                    maxTabId = opp.LatestTabId__c;
                }
                else
                {
                	tabId = maxTabId = opp.LatestTabId__c;
                }
            }
            else
                tabId = maxTabId = '0';
			localStorage.setItem('MaxTabId',maxTabId);
			localStorage.setItem('LatestTabId',parseInt(tabId));
            localStorage.setItem('StageName',opp.StageName);
            $A.get('e.force:refreshView').fire();
        });
        $A.enqueueAction(action);
    }
})