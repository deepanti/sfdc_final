({
    navigateToProjectDetail: function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef: "c:GPCmpProjectEdit",//"c:GPCmpProjectDetails",
            componentAttributes: {
                dealData: component.get("v.simpleDeal")
            }
        });
        evt.fire();
    }
})