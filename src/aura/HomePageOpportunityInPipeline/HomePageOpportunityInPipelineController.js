({
    doInit : function(component, event, helper) {
        helper.getOpportunityPipeline(component, event, helper);
    },  
    fullScreen : function(component) {
        component.set("v.fullScreen", true);
    },
    closeDialog : function(component) {
        component.set("v.fullScreen", false);
    }
   
})