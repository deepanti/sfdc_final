({
    getOpportunityPipeline:function(component, event, helper)
    {
        component.set("v.Spinner",true);
        var isSeller;
        var user=component.get("v.user");
        if(user=='Sales Rep')
        {
            isSeller=true;
        }
        else
            isSeller=false;
        
        var action = component.get("c.getOpportunityPipeline");
        action.setStorable();
         action.setParams(
            {
                'isSeller':isSeller
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.oppListFull",response.getReturnValue());
                var opplist=component.get("v.oppListFull");
                var i;
                var newlst =[];
                for (i = 0; i < component.get("v.pageSize") && i<opplist.length; i++) { 
                    newlst.push(opplist[i]);
                }
                component.set("v.oppList",newlst);
                component.set("v.Spinner",false);
            }     
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    }
})