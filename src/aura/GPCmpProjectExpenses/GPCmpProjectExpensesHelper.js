({
    COMBINATION_REPEAT: "Exp Category-Exp Type-Location combination data already entered.",
    AMOUNT_NEGATIVE_LABEL: "Amount cannot be less than equal to zero.",
    REQUIRED_FIELD_ERROR: 'Fill Required Fields',
    NONE_ENTRY_LABEL: "--NONE--",

    setProjectExpense: function(component) {
        var getProjectExpenseDataService = component.get("c.getProjectExpenseData");
        getProjectExpenseDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectExpenseDataService.setCallback(this, function(response) {
            this.getProjectExpenseDataServiceHandler(response, component);
        });

        $A.enqueueAction(getProjectExpenseDataService);
    },
    getProjectExpenseDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectExpenseData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setOMSProjectExpense: function(component) {
        var getProjectExpenseDataService = component.get("c.getOMSProjectExpenseData");
        getProjectExpenseDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectExpenseDataService.setCallback(this, function(response) {
            this.getOMSProjectExpenseDataServiceHandler(response, component);
        });

        $A.enqueueAction(getProjectExpenseDataService);
    },
    getOMSProjectExpenseDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setOMSProjectExpenseData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProjectExpenseData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        component.set("v.listOfExpenseCategory", responseJson.listOfExpenseCategory);
        component.set("v.listOfExpenditureType", responseJson.listOfExpenditureType);
        component.set("v.listOfLocation", responseJson.listOfLocation);
        component.set("v.billingCurrency", responseJson.billingCurrency);

        this.setCommonFieldsOfProjectExpense(component, responseJson);
    },
    setOMSProjectExpenseData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);

        this.setCommonFieldsOfProjectExpense(component, responseJson);
    },
    setCommonFieldsOfProjectExpense: function(component, responseJson) {
        component.set("v.listOfProjectExpense", responseJson.listOfProjectExpense);
        component.set("v.isExpenseNativelyAvailable", responseJson.isExpenseNativelyAvailable);
        component.set("v.isUpdatable", responseJson.isUpdatable);
        component.set("v.isOMSType", responseJson.isOMSType);
        component.set("v.project", responseJson.project);
    },
    addToProjectExpense: function(component) {
        var listOfProjectExpense = component.get("v.listOfProjectExpense") || [];
        var projectId = component.get("v.recordId");
        var project = component.get("v.project") || {};
        var nextExpenseCounter = project["GP_Next_Expense_Counter__c"] || 0;
        var newProjectExpense = {
            "isEditable": true,
            "GP_Project__c": projectId,
            "new_Record": true,
            "GP_Data_Source__c": "Pinnacle",
            "CurrencyIsoCode": component.get("v.billingCurrency"),
            "GP_Unique_Key__c": nextExpenseCounter + 1
        };

        project["GP_Next_Expense_Counter__c"] = nextExpenseCounter + 1;

        listOfProjectExpense.push(newProjectExpense);

        component.set("v.project", project);
        component.set("v.listOfProjectExpense", listOfProjectExpense);
    },
    saveProjectExpense: function(component) {
        var listOfProjectExpense = component.get("v.listOfProjectExpense");
        if (!listOfProjectExpense || listOfProjectExpense.length === 0 || !this.validateProjectExpense(component, listOfProjectExpense)) {
            return;
        }
        this.updateCurrencyOnAllRecords(component, listOfProjectExpense);
        var saveProjectExpenseService = component.get("c.saveProjectExpenseService");
        this.showSpinner(component);
        var project = component.get("v.project");

        saveProjectExpenseService.setParams({
            "serializedListOfProjectExpense": JSON.stringify(listOfProjectExpense),
            "nextExpenseCounter": project["GP_Next_Expense_Counter__c"],
            "projectId": project["Id"]
        });
        saveProjectExpenseService.setCallback(this, function(response) {
            this.saveProjectExpenseServiceHandler(response, component);
        });

        $A.enqueueAction(saveProjectExpenseService);
    },
    saveProjectExpenseServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        this.setIsEditableToFalse(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.listOfProjectExpense", JSON.parse(responseData.response));
            this.showToast('SUCCESS', 'success', 'Project expense saved successfully');
            this.fireRelatedRecordSaveEvent("expense");
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    toggelIsEditableForRow: function(component, event) {

        var listOfProjectExpense = component.get("v.listOfProjectExpense");
        var expenseIndex = Number(event.currentTarget.id);
        var isEditable = listOfProjectExpense[expenseIndex]["isEditable"]
        listOfProjectExpense[expenseIndex]["isEditable"] = !isEditable;
        component.set("v.listOfProjectExpense", listOfProjectExpense);
    },
    removeProjectExpense: function(component, event) {
        var expenseIndex = Number(event.currentTarget.id);
        var listOfProjectExpense = component.get("v.listOfProjectExpense");
        var projectExpenseTobeDeleted = listOfProjectExpense[expenseIndex];
        var isUpdatable = component.get("v.isUpdatable");

        if (!projectExpenseTobeDeleted.new_Record &&
            (//(projectExpenseTobeDeleted && projectExpenseTobeDeleted.GP_Data_Source__c != 'Pinnacle') ||
                !isUpdatable || projectExpenseTobeDeleted.GP_Parent_Project_Expense__c))
            return;

        if (!confirm("Are you sure you want to delete?")) {
            return;
        }

        listOfProjectExpense.splice(expenseIndex, 1);
        component.set("v.listOfProjectExpense", listOfProjectExpense);

        if (projectExpenseTobeDeleted["Id"]) {
            this.deleteProjectExpense(component, projectExpenseTobeDeleted["Id"])
        }
    },
    deleteProjectExpense: function(component, projectExpenseTobeDeletedId) {

        this.showSpinner(component);

        var deleteProjectExpenseService = component.get("c.deleteProjectExpense");

        deleteProjectExpenseService.setParams({
            "projectExpenseId": projectExpenseTobeDeletedId
        });

        deleteProjectExpenseService.setCallback(this, function(response) {
            this.deleteProjectExpenseServiceHandler(component, response);
        });

        $A.enqueueAction(deleteProjectExpenseService);
    },
    deleteProjectExpenseServiceHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Project expense deleted successfully');
            this.fireRelatedRecordSaveEvent("expense");
            this.fireProjectSaveEvent();
        } else {
            this.handleFailedCallback(component, responseData);

        }
    },
    setIsEditableToFalse: function(component) {
        var listOfProjectExpense = component.get("v.listOfProjectExpense");

        for (var i = 0; i < listOfProjectExpense.length; i += 1) {
            listOfProjectExpense[i]["isEditable"] = false;
        }

        component.set("v.listOfProjectExpense", listOfProjectExpense);
    },
    onChangelistOfProjectExpense: function(component) {
        var listOfProjectExpense = component.get("v.listOfProjectExpense");
        var totalAmount = 0;

        if (listOfProjectExpense) {
            for (var i = 0; i < listOfProjectExpense.length; i += 1) {
                if (listOfProjectExpense[i]["GP_Amount__c"])
                    totalAmount += listOfProjectExpense[i]["GP_Amount__c"];
            }
            component.set("v.totalAmount", totalAmount);
        }
    },
    validateProjectExpense: function(component, listOfProjectExpense) {
        var projectExpenseObjectMap = {};
        var isValid = true;
        var key, rowDom;

        for (var i = 0; i < listOfProjectExpense.length; i += 1) {
            key = listOfProjectExpense[i]["GP_Expense_Category__c"];
            key += '@@' + listOfProjectExpense[i]["GP_Expenditure_Type__c"];
            key += '@@' + listOfProjectExpense[i]["GP_Location__c"];

            rowDom = document.getElementById('project-expense-row' + i);

            if (this.checkEmptyValuesOfFields(listOfProjectExpense[i])) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfProjectExpense[i]["errorMessage"] = this.REQUIRED_FIELD_ERROR;
            } /*else if (projectExpenseObjectMap[key]) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfProjectExpense[i]["errorMessage"] = this.COMBINATION_REPEAT;
            } */else if (listOfProjectExpense[i].GP_Data_Source__c == 'Pinnacle' && !$A.util.isEmpty(listOfProjectExpense[i]["GP_Amount__c"]) &&
                listOfProjectExpense[i]["GP_Amount__c"] <= 0) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfProjectExpense[i]["errorMessage"] = this.AMOUNT_NEGATIVE_LABEL;
            } else {
                listOfProjectExpense[i]["errorMessage"] = null;
                projectExpenseObjectMap[key] = listOfProjectExpense[i];
            }
        }

        component.set("v.listOfProjectExpense", listOfProjectExpense);
        return isValid;
    },
    checkEmptyValuesOfFields: function(expenseRecord) {
        return ($A.util.isEmpty(expenseRecord["GP_Expense_Category__c"]) ||
            expenseRecord["GP_Expense_Category__c"] === this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(expenseRecord["GP_Expenditure_Type__c"]) ||
            expenseRecord["GP_Expenditure_Type__c"] === this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(expenseRecord["GP_Location__c"]) ||
            expenseRecord["GP_Location__c"] === this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(expenseRecord["GP_Amount__c"]));
    },
    updateCurrencyOnAllRecords: function(component, listOfProjectExpense) {
        for (var i = 0; i < listOfProjectExpense.length; i += 1) {
            listOfProjectExpense[i]["CurrencyISOCode"] = component.get("v.billingCurrency");
        }

        component.set("v.listOfProjectExpense", listOfProjectExpense);
    }
})