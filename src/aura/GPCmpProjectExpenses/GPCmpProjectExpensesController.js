({
    doInit: function(component, event, helper) {
        helper.setProjectExpense(component);
    },
    addToProjectExpense: function(component, event, helper) {
        helper.addToProjectExpense(component);
    },
    saveProjectExpense: function(component, event, helper) {
        helper.saveProjectExpense(component);
    },
    toggelIsEditableForRow: function(component, event, helper) {
        helper.toggelIsEditableForRow(component, event);
    },
    removeProjectExpense: function(component, event, helper) {
        helper.removeProjectExpense(component, event);
    },
    getOMSExpense: function(component, event, helper) {
        helper.setOMSProjectExpense(component);
    },
    onChangelistOfProjectExpenseService: function(component, event, helper) {
        helper.onChangelistOfProjectExpense(component);
    },
    editAllRows: function(component, event, helper) {
        var listOfProjectExpense = component.get("v.listOfProjectExpense");

        if (!listOfProjectExpense || listOfProjectExpense.length === 0) {
            return;
        }

        for (var i = 0; i < listOfProjectExpense.length; i += 1) {
            //if (listOfProjectExpense[i]["GP_Data_Source__c"] == 'Pinnacle')
                listOfProjectExpense[i]["isEditable"] = true;
        }
        
        component.set("v.listOfProjectExpense", listOfProjectExpense);
    }
})