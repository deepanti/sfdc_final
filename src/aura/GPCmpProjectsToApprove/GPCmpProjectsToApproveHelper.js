({
    fetchFieldSet: function(cmp) {
        var action = cmp.get("c.getPendingApprovals");
        action.setParams({
            strObjectName: cmp.get("v.relatedTo"),
            fieldSetName: cmp.get("v.fieldSetToDisplay")
           
        });
        $A.enqueueAction(action);
        action.setCallback(this, function(response) {
            var state = response.getState();
            var responseData = response.getReturnValue();
            if (state === "SUCCESS" && responseData.isSuccess) {
                var JSONResponse = JSON.parse(responseData.response);
                if (JSONResponse.length > 0) {
                    debugger;
                    cmp.set("v.lstFieldSetvalues", JSON.parse(JSONResponse[1]["Field_Set"]));
                    cmp.set("v.lstPendingPIWI", JSON.parse(JSONResponse[0]["Pending_WI"]));
                    cmp.set("v.lstAllPendingPIWI", JSON.parse(JSONResponse[0]["Pending_WI"]));
                }
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {}
                } else {}
            }
            this.hideSpinner(cmp);
        });

    },
    approveItems: function(cmp, lstApprovedItems, lstNotSelectedPendingItems) {
        this.showSpinner(cmp);
        var action = cmp.get("c.approvePendingItems");

        action.setParams({
            strPendingWorkItems: JSON.stringify(lstApprovedItems)
        });
        
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            var responseData = response.getReturnValue();
            if (state === "SUCCESS" && responseData.isSuccess) {
                
                this.showToast('SUCCESS', 'SUCCESS', 'Records are succesfully approved.');
                cmp.set("v.lstPendingPIWI", lstNotSelectedPendingItems);
                this.assignRemainingRecords(cmp, lstApprovedItems);
            } else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {}
                } else {}
            }
        });

        $A.enqueueAction(action);
        this.hideSpinner(cmp);
    },
    rejectItems: function(cmp, lstApprovedItems, lstNotSelectedPendingItems) {
        this.showSpinner(cmp);
        var action = cmp.get("c.rejectPendingItems");

        action.setParams({
            strPendingWorkItems: JSON.stringify(lstApprovedItems)
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            var responseData = response.getReturnValue();
            if (state === "SUCCESS" && responseData.isSuccess) {
                cmp.set("v.lstPendingPIWI", lstNotSelectedPendingItems);
                this.assignRemainingRecords(cmp, lstApprovedItems);
                this.showToast('SUCCESS', 'SUCCESS', 'Records are succesfully rejected.');

            } else if(state === "SUCCESS" && !responseData.isSuccess){
            	
            	 this.showToast('Error', 'Error', responseData.message);
            }
            else if (state === "INCOMPLETE") {
                // do something
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {}
                } else {}
            }
        });

        $A.enqueueAction(action);
        this.hideSpinner(cmp);
    },

    showErrorBlock: function(component, ErrorString) {
        component.set("v.errorString", ErrorString);
        component.set("v.showError", true);

    },
    hideErrorBlock: function(component) {
        component.set("v.errorString", '');
        component.set("v.showError", false);
    },

    assignRemainingRecords: function(component, lstSelectedItems) {
        var mapOfSelectedRecords = this.getMapOfIdToRecord(lstSelectedItems);
        var lstAllPendingPIWI = component.get("v.lstAllPendingPIWI");
        debugger;
        for (var i = 0; i < lstAllPendingPIWI.length; i += 1) {
            if (mapOfSelectedRecords[lstAllPendingPIWI[i].objPIWI.Id]) {
                lstAllPendingPIWI.splice(i, 1);
            }
        }
        component.set("v.lstAllPendingPIWI", lstAllPendingPIWI);
    },
    getMapOfIdToRecord: function(list) {
        var mapOfIdToRecord = {};

        list = list || [];

        list.forEach(function(record) {
            mapOfIdToRecord[record.objPIWI.Id] = record;
        });

        return mapOfIdToRecord;
    },
    filterRecord: function(component) {
        var varSeletedView = component.get("v.strSelectedView") || null;
        var lstPendingItems = component.get("v.lstAllPendingPIWI") || [];
        var lstFilterdItems = [];
        console.log(varSeletedView);
        if (varSeletedView && varSeletedView == 'All Projects') {
            component.set("v.lstPendingPIWI", lstPendingItems);
        } else if (varSeletedView && varSeletedView == 'Newly Created Projects') {
            lstPendingItems.forEach(function(pendingItem) {
                if (pendingItem.objRelatedto.GP_Oracle_PID__c=='NA') {
                    lstFilterdItems.push(pendingItem);
                }
            });
            component.set("v.lstPendingPIWI", lstFilterdItems);
        } else {
            lstPendingItems.forEach(function(pendingItem) {
                if (pendingItem.objRelatedto.GP_Oracle_PID__c !='NA') {
                    lstFilterdItems.push(pendingItem);
                }
            });
            component.set("v.lstPendingPIWI", lstFilterdItems);
        }
    }
})