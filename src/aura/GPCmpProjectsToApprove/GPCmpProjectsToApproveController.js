({

    doInit: function(component, event, helper) {
        helper.fetchFieldSet(component);
    },
    approveRecords: function(component, event, helper) {
        var lstPendingItems = component.get("v.lstPendingPIWI") || [];
        var lstNotSelectedPendingItems = [],
            lstApprovedItems = [];
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var hasError = false;

        lstPendingItems.forEach(function(pendingItem) {
            debugger;
            if(!pendingItem.objRelatedto.GP_PID_Approver_User__c){
            	 helper.showToast('Error', 'Error', 'PID Approver is not defined for the project.');
            	 return;
            	 hasError = true;
            }
            if (pendingItem.isSelected) {
                if (pendingItem.objRelatedto && pendingItem.objRelatedto.GP_PID_Approver_User__c && 
                    pendingItem.objRelatedto.GP_PID_Approver_User__c.slice(0, pendingItem.objRelatedto.GP_PID_Approver_User__c.length - 3) === userId &&
                    pendingItem.objRelatedto.GP_Oracle_PID__c=='NA') {
                    helper.showToast('Error', 'Error', 'You cannot approve the record from here. Please approve the record from project detail page. ');
                    hasError = true;
                    return;
                }
                lstApprovedItems.push(pendingItem);
            } else {
                lstNotSelectedPendingItems.push(pendingItem);
            }
        });
        if (hasError) {
            return;
        }

        if (lstApprovedItems && lstApprovedItems.length > 0) {
            helper.approveItems(component, lstApprovedItems, lstNotSelectedPendingItems);

            helper.filterRecord(component);
        } else {
            helper.showToast('Error', 'Error', 'Please selecte items to approve.');
        }
    },
    filterRecord: function(component, event, helper) {

        helper.filterRecord(component);
    },
    rejectRecords: function(component, event, helper) {
        helper.hideErrorBlock(component);
        var lstPendingItems = component.get("v.lstPendingPIWI") || [];
        var lstNotSelectedPendingItems = [];
        var lstApprovedItems = [];
        var hasError = false;
        lstPendingItems.forEach(function(pendingItem, index) {
            if (pendingItem.isSelected) {
                if (!pendingItem.strRemarks) {
                    var varRemarkID = "comments-" + index + '-' + pendingItem.objPIWI.Id;
                    var rowDom = document.getElementById(varRemarkID);
                    $A.util.addClass(rowDom, 'invalid-row');
                    helper.showErrorBlock(component, 'Please fill the approvers comments.');
                    helper.showToast('Error', 'Error', 'Please fill the approvers comments.');
                    hasError = true;
                } else {
                    lstApprovedItems.push(pendingItem);
                }
            } else {
                lstNotSelectedPendingItems.push(pendingItem);
            }

        });


        if (lstApprovedItems && lstApprovedItems.length > 0) {
            helper.rejectItems(component, lstApprovedItems,lstNotSelectedPendingItems);
        } else if (!hasError) {
            helper.showToast('Error', 'Error', 'Please selecte items to reject.');
        }
    },
    
    redirect: function(component, event, helper){
    	 var recordId = event.target.id;
    	 helper.redirectToSobject(recordId);
    }

})