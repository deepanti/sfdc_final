({
	
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true to display loading spinner 
        component.set("v.Spinner", true); 
    },
  
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false to hide loading spinner    
        component.set("v.Spinner", false);
    },
    //validation
    validateAll : function(component, event, helper) {
    
        //validating account id
        var proceedToSave = true;
    	var acc = component.find("accId");
        var accountName = acc.get("v.value");
        if($A.util.isUndefinedOrNull(accountName) || accountName == '')
        {
            $A.util.addClass(acc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(acc,'slds-has-error');
        }
        //validating target source picklist
        var tarSource = component.find("tarSource");
        var tarSourceValue = tarSource.get("v.value");
      
        if($A.util.isUndefinedOrNull(tarSourceValue) || tarSourceValue == '')
        {
          
            $A.util.addClass(tarSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(tarSource,'slds-has-error');
        }
       
        //validating opportunity source picklist
        var oppSource = component.find("oppSource");
        var oppSourceValue = oppSource.get("v.value");
      
        if($A.util.isUndefinedOrNull(oppSourceValue) || oppSourceValue == '')
        {
          
            $A.util.addClass(oppSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSource,'slds-has-error');
        }
        
        //validating sales region picklist
        var salesRegion = component.find("salesRegion");
        var salesRegionValue = salesRegion.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesRegionValue) || salesRegionValue == '')
        {
          
            $A.util.addClass(salesRegion,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesRegion,'slds-has-error');
        }
         
        
         //validating sales country picklist
        var salesCountry = component.find("salesCountry");
        var salesCountryValue = salesCountry.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesCountryValue) || salesCountryValue == '')
        {
          
            $A.util.addClass(salesCountry,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesCountry,'slds-has-error');
        }
        
        //validating description
       /* var sumOpp = component.find("sumOpp");
        var sumOppValue = sumOpp.get("v.value");
      
        if($A.util.isUndefinedOrNull(sumOppValue) || sumOppValue == '')
        {
          
            $A.util.addClass(sumOpp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(sumOpp,'slds-has-error');
        }
        */
        //validating opportunity origination
        var oppOrigin = component.find("oppOrigin");
        var oppOriginValue = oppOrigin.get("v.value");
      
        if($A.util.isUndefinedOrNull(oppOriginValue) || oppOriginValue == '')
        {
          
            $A.util.addClass(oppOrigin,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppOrigin,'slds-has-error');
        }
		
        //validating deal type 
        var dealType = component.find("dealType");
        var dealTypeValue = dealType.get("v.value");
      
        if($A.util.isUndefinedOrNull(dealTypeValue) || dealTypeValue == '')
        {
          
            $A.util.addClass(dealType,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(dealType,'slds-has-error');
        }
        //validating closest competitor
       /* var closeCom = component.find("closeCom");
        var closeComValue = closeCom.get("v.value");
      
        if($A.util.isUndefinedOrNull(closeComValue) || closeComValue == '')
        {
          
            $A.util.addClass(closeCom,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(closeCom,'slds-has-error');
        }*/
        
        var dealType = component.find("dealType");
        var dealTypeVal = dealType.get("v.value");
    	var CloseComp = component.find("ClosestCompetitor");
        var CloseCompotitor = CloseComp.get("v.value");
       /* if(($A.util.isUndefinedOrNull(CloseCompotitor) || CloseCompotitor == '' || CloseCompotitor == 'Undefined') && dealTypeVal =='Competitive')
        {
            $A.util.addClass(CloseComp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(CloseComp,'slds-has-error');
        }*/
        
        //validating nextstep
        var nextStep = component.find("nextStep");
        var nextStepValue = nextStep.get("v.value");
      
        if($A.util.isUndefinedOrNull(nextStepValue) || nextStepValue == '')
        {
            $A.util.addClass(nextStep,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(nextStep,'slds-has-error');
        }
        
        //validating contact
        var cont = component.find("cont");
        var contValue = cont.get("v.value");
      
        if($A.util.isUndefinedOrNull(contValue) || contValue == '')
        {
          
            $A.util.addClass(cont,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(cont,'slds-has-error');
        }
        
        //validating role
        var role = component.find("role");
        var roleValue = role.get("v.value");
      
        if($A.util.isUndefinedOrNull(roleValue) || roleValue == '')
        {
          
            $A.util.addClass(role,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(role,'slds-has-error');
        }
         
        return proceedToSave;
    }
    
})