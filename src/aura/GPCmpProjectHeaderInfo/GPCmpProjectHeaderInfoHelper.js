({
	getProjectData: function(component) {
		var projectId = component.get("v.recordId");
		var listOfFieldToBeDisplayed = component.get("v.listOfFieldToBeDisplayed");
        var getProjectDetails = component.get("c.getProjectDetails");
        
        getProjectDetails.setParams({
           "projectId": projectId
        });
        
        getProjectDetails.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var responseJson = JSON.parse(responseData.response);
				component.set("v.projectData", responseJson.project);
                console.log(responseJson);
            } else {
				//this.handleFailedCallback(component, responseData);                
            }
        });
        
        $A.enqueueAction(getProjectDetails);
	}
})