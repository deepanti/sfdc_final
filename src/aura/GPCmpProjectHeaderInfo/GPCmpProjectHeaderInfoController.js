({
    doInit: function(component, event, helper) {
        helper.getProjectData(component);
    },
    approvalValidationUpdateService: function(component, event, helper) {
        var validationMessage = event.getParam("validationMessage");
        var projectData = component.get("v.projectData") || {};
        projectData.GP_Approval_Error__c = validationMessage;

        component.set("v.projectData", projectData);
    }
})