({
    fetchProjDetails: function(component, event) {
        var getProject = component.get("c.getProjectDetail");

        getProject.setParams({
            "projectId": component.get("v.recordId")
        });

        getProject.setCallback(this, function(response) {
            this.hideSpinner(component);
            var responseData = response.getReturnValue() || {};

            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {

                var responseJson = JSON.parse(responseData.response);

                component.set("v.isRevenueStageValid", responseJson.isRevenueStageValid);
                component.set("v.approvalType", responseJson.approvalType);
                component.set("v.taxCodeName", responseJson.taxCodeName);
                component.set("v.projectObj", responseJson.projectObj);
                component.set("v.isSuccess", responseData.isSuccess);
                component.set("v.isPIDUser", responseJson.isPIDUser);
                component.set("v.taxCodeId", responseJson.taxCodeId);
                component.set("v.reports", responseJson.reports);
				// PC Change
				component.set("v.listOfProductCodes", responseJson.listOfProductCodes);
                component.set("v.productCodeField", responseJson.projectObj.GP_Product_Code__c);

                if (responseJson.approvalType == 'Approval')
                    this.setValuesForProjectApproval(component, responseJson);
                else if (responseJson.approvalType == 'Closure')
                    this.setValuesForProjectClosure(component, responseJson);
            } else {
                var errorLog = {
                    "mapOfSectionToSubSectionNameToListOfErrors": JSON.parse(responseData.message)
                };
                var formattederror = this.getFormattedListOfError(errorLog);
                component.set("v.isSuccess", responseData.isSuccess);
                component.set("v.errorMessage", formattederror);
            }
			 this.CheckIfAvalaraEnabled(component,responseData);
        });

        $A.enqueueAction(getProject);
    },
    setValuesForProjectApproval: function(component, responseJson) {
        var serializedMapOfStage = $A.get("$Label.c.GP_Final_Project_Stage_Values");
        var lstOfAllProjectStage = JSON.parse(serializedMapOfStage) || [];
        var lstProjectStage = [];
        var finalProjectStage;
        var isIndirectProject = responseJson.isIndirectProject;
        if (isIndirectProject) {

            lstOfAllProjectStage.forEach(function(stage) {
                //if the contract is not signed thend don't include approved in list of stages.
                if (stage.label === "Approved Support") {
                    lstProjectStage.push(stage);
                    finalProjectStage = stage.text;
                }
            });
        } else if (responseJson.approvalType == "Closure") {

            lstOfAllProjectStage.forEach(function(stage) {
                //if the contract is not signed thend don't include approved in list of stages.
                if (stage.label === "Closed") {
                    lstProjectStage.push(stage);
                    finalProjectStage = stage.text;
                }
            });

        } else {

            lstOfAllProjectStage.forEach(function(stage) {
                //if the contract is not signed thend don't include approved in list of stages.
                if (!(!responseJson.isRevenueStageValid && stage.label === 'Approved')) {
                    lstProjectStage.push(stage);
                }

                if (stage.label === responseJson.strStageValue) {
                    finalProjectStage = stage.text;
                }
            });

            var stageIndex = 0;

            if (responseJson.strStageValue === "Cost Charging") {
                stageIndex = 0;
            } else if (responseJson.strStageValue === "Billing Cost Charging") {
                stageIndex = 1;
            } else if (responseJson.strStageValue === 'Revenue Cost Charging') {
                stageIndex = 2;
            } else if (responseJson.strStageValue === 'Approved') {
                stageIndex = 3;
            }

            lstProjectStage = lstProjectStage.splice(0, stageIndex + 1);

        }

        component.set("v.lstProjectStage", lstProjectStage);
        component.set("v.finalProjectStage", finalProjectStage);
        component.set("v.strProjectStage", responseJson.strStageValue);

    },
    setValuesForProjectClosure: function(component, responseJson) {},
    ProjectApprovedHelper: function(component, event) {
        var validationResponse = this.validateProject(component);

        if (validationResponse.isValid) {

            component.set("v.strError", "");
            var ProjectApprove = component.get("c.ProjectStatusToApprove");
			// PC Change : Added productCode param.
            ProjectApprove.setParams({
                "projectId": component.get("v.recordId"),
                "strSelectedStage": component.get("v.finalProjectStage"),
                "strComment": component.get("v.strComments"),
                "taxCode": component.get("v.taxCodeName"),
                "reports": component.get("v.reports"),
                "productCode" : component.get("v.productCodeField")
            });
            ProjectApprove.setCallback(this, function(response) {
                this.handleCallBackForApprovalOrReject(component, response, 'approve');
            });
            $A.enqueueAction(ProjectApprove);
        } else {
            // component.set("v.strError", validationResponse.message);
        }
    },
    validateProject: function(component) {debugger;
        var isValid = true;
        var message = '';

        var strComment = component.get("v.strComments");
        var taxCodeName = component.get("v.taxCodeName");
        var isPIDUser = component.get("v.isPIDUser");
        var projectObj = component.get("v.projectObj");

        var taxCodeDom = component.find("taxCode");
        var commentDom = component.find("comment");
        var reportsDom = component.find("reports");
		// PC Change
        var productCodeDom = component.find("productCode");
        /*if ($A.util.isEmpty(strComment)) {
            isValid = false;
            message = "Please enter the comment to proceed.";

            $A.util.removeClass(commentDom, 'slds-hide');
        } else {
            $A.util.addClass(commentDom, 'slds-hide');
        }*/
		//bypass tax code validation for Service Delivery in US/Canada - Avalara
        var isAvalaraEnab = component.get("v.isAvalaraEnableApproval");	
        if(projectObj.GP_Service_Deliver_in_US_Canada__c && isAvalaraEnab){	
            console.log("Hide Tax code for GP_Service_Deliver_in_US_Canada__c");	
        }
        else
        {
            if (isPIDUser && projectObj.RecordType.Name != 'Indirect PID' && !projectObj.GP_Operating_Unit__r.GP_Is_GST_Enabled__c && $A.util.isEmpty(taxCodeName)) {
                isValid = false;
                message += "Please enter TAX Code to proceed";
                
                $A.util.removeClass(taxCodeDom, 'slds-hide');
            } else {
                $A.util.addClass(taxCodeDom, 'slds-hide');
            }
        }

       /* if (isPIDUser && projectObj.RecordType.Name != 'Indirect PID' && !projectObj.GP_Operating_Unit__r.GP_Is_GST_Enabled__c && $A.util.isEmpty(taxCodeName)) {
            isValid = false;
            message += "Please enter TAX Code to proceed";

            $A.util.removeClass(taxCodeDom, 'slds-hide');
        } else {
            $A.util.addClass(taxCodeDom, 'slds-hide');
        }*/
        var reports = component.get("v.reports");
        var finalProjectStage = component.get("v.finalProjectStage");
        if (isPIDUser && projectObj.RecordType.Name != 'Indirect PID' &&
            finalProjectStage !='1010' && 
            (($A.util.isEmpty(reports)) || reports == "--NONE--")) {
            isValid = false;
            message += "Please fill reports to proceed";

            $A.util.removeClass(reportsDom, 'slds-hide');
        } else {
            $A.util.addClass(reportsDom, 'slds-hide');
        }
		
		// PC Change
        if(component.get("v.isPIDUser") && projectObj.GP_Business_Group_L1__c === 'GE' 
           && projectObj.RecordType.Name !== 'Indirect PID'
           && !component.get("v.productCodeField")) {
			isValid = false;
            message += "Please select product code for GE PIDs.";
            
            $A.util.removeClass(productCodeDom, 'slds-hide');
        } else {
            $A.util.addClass(productCodeDom, 'slds-hide');
        }

        return {
            "isValid": isValid,
            "message": message
        };
    },
    handleCallBackForApprovalOrReject: function(component, response, source) {
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS") {
            if (responseData.isSuccess) {
                this.showToast("Success", "Success", responseData.message);
                /*if (source === 'reject') {
                    this.navigaettoURL('/lightning/page/home');
                } else {
                    this.redirectToSobject(component.get("v.recordId"));
                }*/
                //06-12-18: User to land to home page after either approving or rejecting the PID.
                this.navigaettoURL('/lightning/page/home');
                //this.closeModal();
            } else {
                this.showToast("Error", "error", responseData.message);
            }
        } else {
            this.redirectToSobject(component.get("v.recordId"));
        }
    },
    ProjectRejectHelper: function(component, event) {
        component.set("v.strError", "");
        var strComments = component.get("v.strComments");
        if (!strComments) {
            component.set("v.strError", "Please enter the comment to proceed.");
        } else {
            var ProjectReject = component.get("c.ProjectStatusToReject");
			// PC Change : Added productCode param.
            ProjectReject.setParams({
                "projectId": component.get("v.recordId"),
                "strSelectedStage": component.get("v.strProjectStage"),
                "strComment": component.get("v.strComments"),
                "productCode" : component.get("v.productCodeField")
            });
            ProjectReject.setCallback(this, function(response) {
                this.handleCallBackForApprovalOrReject(component, response, 'reject');


            });
            $A.enqueueAction(ProjectReject);
        }
    },
    ProjectReAssignHelper: function(component, event) {
        component.set("v.strError", "");
        if (component.get("v.strUserId") !== null && component.get("v.strUserId") !== '') {
            var ProjectAssgn = component.get("c.ProjectReassignToUser");
            ProjectAssgn.setParams({
                "projectId": component.get("v.recordId"),
                "strUserId": component.get("v.strUserId"),
                "strComment": component.get("v.strComments")
            });

            ProjectAssgn.setCallback(this, function(response) {
                this.handleCallBackForApprovalOrReject(component, response, 'reassign');
            });
            $A.enqueueAction(ProjectAssgn);
        } else {
            component.set("v.strError", "Please enter the re assign user.");
        }
    },
    ProjectClosureRejectHelper: function(component, event) {
        component.set("v.strError", "");
        var strComments = component.get("v.strComments");
        if (!strComments)
            component.set("v.strError", "Please enter the comment to proceed.");
        else {
            var ProjectStatusToRejectForClosure = component.get("c.ProjectStatusToRejectForClosure");
			// PC Change : Added productCode param.
            ProjectStatusToRejectForClosure.setParams({
                "projectId": component.get("v.recordId"),
                "strComment": component.get("v.strComments"),
                "productCode" : component.get("v.productCodeField")
            });
            ProjectStatusToRejectForClosure.setCallback(this, function(response) {
                this.handleCallBackForApprovalOrReject(component, response, 'reject');
            });
            $A.enqueueAction(ProjectStatusToRejectForClosure);
        }
    },
    ProjectClosureApprovedHelper: function(component, event) {
        var ProjectStatusToApproveForClosure = component.get("c.ProjectStatusToApproveForClosure");
		// PC Change : Added productCode param.
        ProjectStatusToApproveForClosure.setParams({
            "projectId": component.get("v.recordId"),
            "strComment": component.get("v.strComments"),
            "productCode" : component.get("v.productCodeField")
        });
        ProjectStatusToApproveForClosure.setCallback(this, function(response) {
            this.handleCallBackForApprovalOrReject(component, response, 'approve');
        });
        $A.enqueueAction(ProjectStatusToApproveForClosure);

    },
	//Avalara
    CheckIfAvalaraEnabled:function(component,responseData) {
		
		var responseJson = JSON.parse(responseData.response);  
		var projectRecord = responseJson.projectObj;
		var originalProjectRecord = JSON.parse(JSON.stringify(projectRecord));
		//var LeCode2 = projectRecord.GP_Operating_Unit__r["GP_LE_Code__c"];
		var LeCode = projectRecord.GP_Operating_Unit__r.GP_LE_Code__c;
		
		console.log('==LeCode=='+LeCode);
		if(!$A.util.isUndefinedOrNull(LeCode)){
			var action = component.get("c.CheckAvalaraLECodeExist");
			action.setParams({
				"lecode":LeCode
			});
			action.setCallback(this, function(response) {
				var res=response.getReturnValue();
				console.log('===Avalara Enable Flag ==='+res);
				if(res==='Y'){ 
					component.set("v.isAvalaraEnableApproval",true);
					//component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);					
				}
			});
			$A.enqueueAction(action);
		}
	}
})