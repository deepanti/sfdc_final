({
    doInit: function(component, event, helper) {
        helper.fetchProjDetails(component, event);
    },
    ProjectApproved: function(component, event, helper) {
        var approvalType = component.get("v.approvalType");
        var strComments = component.get("v.strComments");
        if (!strComments) {
            component.set("v.strError", "Required");
            return;
        }
        if (approvalType == 'Approval')
            helper.ProjectApprovedHelper(component, event);
        else if (approvalType == 'Closure')
            helper.ProjectClosureApprovedHelper(component, event);

    },
    ProjectReject: function(component, event, helper) {
        var approvalType = component.get("v.approvalType");
        var strComments = component.get("v.strComments");
        if (!strComments) {
            component.set("v.strError", "Required");
            return;
        }
        if (approvalType == 'Approval')
            helper.ProjectRejectHelper(component, event);
        else if (approvalType == 'Closure')
            helper.ProjectClosureRejectHelper(component, event);
    },
    clearUserDetail: function(component, event, helper) {
        component.set("v.strUserName", "");
        component.set("v.strUserId", "");
    },
    ProjectReAssign: function(component, event, helper) {
        helper.ProjectReAssignHelper(component, event);
    },
    clearTaxCode: function(component) {
        component.set("v.taxCodeId", null);
        component.set("v.taxCodeName", null);
    },
	  finalProjectStageChange: function(component, event, helper) {
        var finalStageVal = component.get("v.finalProjectStage");
        var lstProjectStage = component.get("v.lstProjectStage");
        
        for(var i=0; i<lstProjectStage.length; i++) {
            if(lstProjectStage[i].text === finalStageVal) {
                component.set("v.tempStrProjectStage", lstProjectStage[i].label)
            }
        }
    }
})