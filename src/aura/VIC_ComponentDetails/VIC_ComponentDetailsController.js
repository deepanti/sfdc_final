({
	 doInit: function(component, event, helper) {
        // Fetch the component detail list from the Apex controller
        helper.getComponentDetailList(component);
      },
})