({
    // Fetch the Component Detail List from the Apex controller
    getComponentDetailList: function(component) {
        var action = component.get('c.getComponentDetails');
        action.setParams({  userId : userId  });
        action.setCallback(this, function(actionResult) {
            component.set('v.componentDetails', actionResult.getReturnValue());
        });
        $A.enqueueAction(action);
    }
})