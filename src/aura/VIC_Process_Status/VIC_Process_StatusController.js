({
	changesJobStatus : function(component, event, helper) {
        helper.changesJobStatusHelper(component,event);       
	},
    doInit : function(component,event,helper){
        helper.doInitHelper(component,event); 
        helper.fetchDeaDate(component, event);
        helper.fetchJobDate(component, event);
        helper.fetchJobtime(component, event);
    },
    runUfrontBatch : function(component, event, helper){
        helper.runUpFrontBatchHelper(component);
    },
    runTCVBatch : function(component, event, helper){
        helper.runUpTCVBatchHelper(component);
    },
    
    runScoreCardBatch : function(component, event, helper){
        helper.runUpScoreCardBatchHelper(component);
    },
    
     runTCVAcceleratorIOTSBatch : function(component, event, helper){
        helper.runTCVAcceleratorIOTSBatchHelper(component);
    },
    runCalculatedGuaranteedAmountJob : function(component, event, helper){
        helper.runCalculatedGuaranteedAmountJobHelper(component);
    },
    
     runCurrConAnnualBatchHelper : function(component, event, helper){
        helper.runCurrConvAnnualBatchHelper(component);
    },
     runCurrConUpfrontBatchHelper : function(component, event, helper){
        helper.runCurrConvUpfrontBatchHelper(component);
    },
    
    runUfrontBatchAnnual : function(component, event, helper){
        helper.runUpFrontBatchHelperAnnual(component);
    },
    EnableVICCalcPicklist: function(component, event, helper){        
        helper.enableVICPicklist(component);
    },
    EnableAnnCalcPicklist: function(component, event, helper){        
        helper.enableAnnualPicklist(component);
    },
    
    EnableForHRPicklist: function(component, event, helper){        
        helper.enableHRPicklist(component);
    },
    
    EnableForSLPicklist: function(component, event, helper){        
        helper.enableSLPicklist(component);
    },
    
    updateVICClaculationYear: function(component, event, helper){        
        if(confirm('Are you sure? This will impact on VIC calculations.')){
            helper.updateCalculationYear(component);
        }
    },
    updateAnClaculationYear: function(component, event, helper){        
        if(confirm('Are you sure? This will impact on Annual calculations.')){
            helper.updateAnnualCalculationYear(component);
        }
    },
    EnableFrequencyPicklist: function(component, event, helper){        
        helper.enableFrequencySection(component);
    },
    UpdateReminderNotificationFrequency: function(component, event, helper){        
        helper.reminderNotificationFrequenct(component);
         
    },
    openNotificationModel: function(component, event, helper) {
        if(component.get("v.isNotificationEnabled")){            
            component.set("v.isNotificationModelOpen", true);
        }
        else{
            //TODO: DELETE NOTIFICATION JOB
            helper.changesSendNotHelper(component,event); 
             
        }        
   }, 
   closeNotificationModel: function(component, event, helper) {      
      component.set("v.isNotificationModelOpen", false);
      component.set("v.isNotificationEnabled", false); 
   }, 
   EnableNotificationJob: function(component, event, helper) {      
       ///TODO: CREATE NOTIFICATION JOB
      component.set("v.isNotificationModelOpen", false);
      component.set("v.isNotificationEnabled", true);
      helper.changesSendNotHelper(component,event); 
   },
    updateHRday: function(component, event, helper){        
        helper.updateHRdayhelper(component);
    },
    
    updateSLday: function(component, event, helper){        
        helper.updateSLdayhelper(component);
    },
    
    
})