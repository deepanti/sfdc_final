({
    updateHRdayhelper : function(component, event, helper) {
        var action = component.get("c.saveHRDay");
       var assessments = component.find("selectIdHR").get("v.value");
        action.setParams({"Dday": assessments});
        component.set("v.setHRDay", assessments);
       
         action.setCallback(this, function(actionResult){
              if(actionResult.getState()==='SUCCESS') { 
              component.set("v.isDisabledForHR", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Record has been updated."
                });
                toastEvent.fire();
       			}
             
    		 });        
        $A.enqueueAction(action);   
    },  

    
    updateSLdayhelper : function(component, event, helper) {
        var action = component.get("c.saveSLDay");
       var assessments = component.find("selectIdSL").get("v.value");
        action.setParams({"slDay": assessments});
        component.set("v.setSLDay", assessments);
       
         action.setCallback(this, function(actionResult){
              if(actionResult.getState()==='SUCCESS') { 
              component.set("v.isDisabledForSL", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Record has been updated."
                });
                toastEvent.fire();
       			}
             
    		 });        
        $A.enqueueAction(action);   
    }, 
    
    
    
	helperMethod : function(component, event) {
		 var action = component.get("c.startBatchJob");
         var checkboxLabel = event.target.Label;
         
         action.setParams({
           
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // if state of server response is comes "SUCCESS",
                // display the success message box by set mailStatus attribute to true
            }
 
        });
        $A.enqueueAction(action);
    },
    changesJobStatusHelper : function(component, event) {
        var action = component.get("c.enaqueSchedulerJob");
        
        action.setParams({
           "jobName": component.find("vicJobStatus").get("v.name"),
           "status": component.find("vicJobStatus").get("v.checked")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Job status updated."
                });
                toastEvent.fire();
               $A.get("e.force:refreshView").fire();
            }
            else if(state == "ERROR"){
                    alert('Error in calling server side action');
              
             }
        });
        $A.enqueueAction(action);
    },
    changesSendNotHelper : function(component, event) {
        var action = component.get("c.enaqueSendSchedulerJob");
         
        action.setParams({
           "jobName": component.find("enableNotification").get("v.name"),
           "status": component.find("enableNotification").get("v.checked"),
           "day":  component.get("v.frequencyMWeekDay"),
           "tme": component.get("v.frequencyMTime")
           
        });
       
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                   
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Job status updated."
                });
                toastEvent.fire();
                 $A.get("e.force:refreshView").fire();
            }
            else{
                
                var toastEvent = $A.get("e.force:showToast");
                
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    doInitHelper : function(component, event){
        var action = component.get("c.getRunningStatusOfJob");
        action.setParams({
            "jobName": component.find("vicJobStatus").get("v.name")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
               var status =  component.find("vicJobStatus");
                status.set("v.checked",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
       
       
        
        
        var action = component.get("c.getRunningStatusOfJob");
        action.setParams({
            "jobName": component.find("enableNotification").get("v.name"),
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
               var status =  component.find("enableNotification");
                status.set("v.checked",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
        
        var action1 = component.get("c.getFinancialYear");
        action1.setCallback(this, function(response) {
            var getFinancialYearState = response.getState();
            var valuegetFinancialYear = response.getReturnValue();
            if (getFinancialYearState === "SUCCESS") {
                component.set("v.financialYearValueList", valuegetFinancialYear);
            }
        });
        $A.enqueueAction(action1);
        
        var action2 = component.get("c.getCurrentVICYear");
        action2.setCallback(this, function(response) {            
            var currState = response.getState();
            var currValue = response.getReturnValue();
            if (currState === "SUCCESS") {                
                component.set("v.defaultYear", currValue);
                component.set("v.isDisabled", true);
            }
        });
        $A.enqueueAction(action2);
        
        
        
        var action5 = component.get("c.getCurrentAnnualYear");
        action5.setCallback(this, function(response) {            
            var currState = response.getState();
            var currValue = response.getReturnValue();
            if (currState === "SUCCESS") {                
                component.set("v.defaultAYear", currValue);
                component.set("v.isADisabled", true);
            }
        });
        $A.enqueueAction(action5);
        
        
	},
    
        
        
        
	
    runUpFrontBatchHelper : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runUpfrontBatchServerController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Upfront Component caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    runUpTCVBatchHelper	: function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runUpTCVBatchServerController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Upfront Component caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    runUpScoreCardBatchHelper	: function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runUpScoreCardBatchServerController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for ScoreCard Component caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
      },
 
    runTCVAcceleratorIOTSBatchHelper : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runUpTCVAcceleratorIOTSBatchServerController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for TCV Accelerator IO/TS Component caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    runCalculatedGuaranteedAmountJobHelper : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runCalculateGuaranteedAmountController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Calculation of guaranteed amount."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    runCurrConvUpfrontBatchHelper : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runCurrConvUpfrController");
       
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Currency Conversion Upfront caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
runCurrConvAnnualBatchHelper : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runCurrConvAnnualController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Currency Conversion Annual caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    
    runUpFrontBatchHelperAnnual : function(component){
        component.set('v.isSpinnerVisible', true);
        var action = component.get("c.runUpfrontBatchAnnualServerController");
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Batch Job queued successfully."
                });
                toastEvent.fire();
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch for Upfront Component caluculation."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },enableVICPicklist : function(component){
        component.set("v.isDisabled", false);
        /*$A.util.addClass(component.find("v.btnEdit"), "slds-hide");
        $A.util.removeClass(component.find("v.btnSave"), "slds-hide");*/
    },
    enableAnnualPicklist : function(component){
        component.set("v.isADisabled", false);
        /*$A.util.addClass(component.find("v.btnEdit"), "slds-hide");
        $A.util.removeClass(component.find("v.btnSave"), "slds-hide");*/
    },
    
    enableHRPicklist : function(component){
        component.set("v.isDisabledForHR", false);
        
    },
    enableSLPicklist : function(component){
        component.set("v.isDisabledForSL", false);
        
    },
    
    updateCalculationYear : function(component){        
        component.set('v.isSpinnerVisible', true);
        var defaultYear = component.get("v.defaultYear");
        
        var action = component.get("c.setVICProcessYear");
        action.setParams({
            "dYear": defaultYear
        });        
        
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                component.set("v.isDisabled", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "VIC calculation year updated."
                });
                toastEvent.fire();
            }else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	toastEvent.setParams({
                            "title": "Error!",
                            "type": "error",
                            "message": errors[0].message
                        });
                    }
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Unknown server error."
                    });
                }
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    updateAnnualCalculationYear : function(component){        
        component.set('v.isSpinnerVisible', true);
        var defaultAYear = component.get("v.defaultAYear");
        
        var action = component.get("c.setAnnualProcessYear");
        action.setParams({
            "dYear": defaultAYear
        });        
        
        action.setCallback(this, function(response){
            component.set('v.isSpinnerVisible', false);
            if(response.getState() == 'SUCCESS'){
                component.set("v.isADisabled", true);
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Annual calculation year updated."
                });
                toastEvent.fire();
            }else if (state === "ERROR") {
                var toastEvent = $A.get("e.force:showToast");
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                    	toastEvent.setParams({
                            "title": "Error!",
                            "type": "error",
                            "message": errors[0].message
                        });
                    }
                }else{
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Unknown server error."
                    });
                }
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
 
    enableFrequencySection : function(component){
        component.set("v.isFrequencyDisabled", false);
    },reminderNotificationFrequenct : function(component){
        component.set("v.isFrequencyDisabled", true);
        var action = component.get("c.enaqueSendSchedulerJob");
       action.setParams({
           "jobName": component.find("enableNotification").get("v.name"),
           "status": component.find("enableNotification").get("v.checked"),
           "day":  component.get("v.frequencyWeekDay"),
           "tme": component.get("v.frequencyTime")
          
        });
       
        action.setCallback(this, function(response){
            var state = response.getState();
            
            if (state === "SUCCESS") {
                  
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "type": "success",
                    "message": "Job status updated."
                });
                toastEvent.fire();
            }
            else{
                
                var toastEvent = $A.get("e.force:showToast");
                
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Problem in running batch"
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
        
        
    },
    fetchDeaDate: function(component, event, helper){
        var action = component.get("c.getlastdeactivatedOfJob");
        action.setParams({
            "jobName": component.find("vicJobStatus").get("v.name")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
              
                component.set("v.DeDate",response.getReturnValue());
            }
        });
        $A.enqueueAction(action); 
    },
    fetchJobDate: function(component, event, helper){
        var action = component.get("c.getJobDate");
       action.setParams({
            "jobName": component.find("enableNotification").get("v.name"),
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
          
            if (state === "SUCCESS") {
              
                component.set("v.frequencyWeekDay",response.getReturnValue());
                component.set("v.frequencyMWeekDay",response.getReturnValue());
                
            }
        });
        $A.enqueueAction(action); 
    },
    fetchJobtime: function(component, event, helper){
        var action = component.get("c.getJobTime");
       action.setParams({
            "jobName": component.find("enableNotification").get("v.name"),
        });
        
        action.setCallback(this, function(response){
            var state = response.getState().toString();
           
            if (state === "SUCCESS") {
               var result= response.getReturnValue();
                if(result!=null){
                 var time=result.toString();
                }
                
                component.set("v.frequencyTime",time );
                component.set("v.frequencyMTime", time);   
       
            }
        });
        $A.enqueueAction(action); 
    }
    

})