({
    doInit: function(component, event, helper) {
        helper.getProjectBudget(component);
    },
    addToProjectBudget: function(component, event, helper) {
        helper.addToProjectBudget(component);
    },
    budgetChangeHandler: function(component, event, helper) {
        var label = event.getSource().get("v.label");
        var splitted = label.split('@@');
        var budgetIndex = Number(splitted[1]);
        var budgetField = splitted[0];
        var value = event.getSource().get("v.value");

        helper.setTCVAmount(component, budgetIndex, budgetField, value);
    },
    budgetChangeHandlerText: function(component, event, helper) {
        var budgetIndex = event.target.getAttribute("data-budget-index");
        var budgetField = event.target.getAttribute("data-attribute-name");
        var value = event.currentTarget.value;

        helper.setTCVAmount(component, budgetIndex, budgetField, value);
    },
    toggelIsEditableRow: function(component, event, helper) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var projectbudgetIndex = Number(event.currentTarget.id);
        var isEditable = listOfProjectbudget[projectbudgetIndex]["isEditable"];

        listOfProjectbudget[projectbudgetIndex]["isEditable"] = !isEditable;
        component.set("v.listOfProjectbudget", listOfProjectbudget);
    },
    removeProjectBudget: function(component, event, helper) {
        var projectBudgetIndex = Number(event.currentTarget.id);
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var projectbudgetToBeDeleted = listOfProjectbudget[projectBudgetIndex];
        var isOMSType = component.get("v.isOMSType");
        var isUpdatable = component.get("v.isUpdatable");

        if (isOMSType || projectbudgetToBeDeleted.GP_Data_Source__c != 'Pinnacle' ||
            !isUpdatable)
            return;

        if (!confirm("Are you sure you want to delete?")) {
            return;
        }

        helper.removeProjectbudget(component, projectBudgetIndex);
    },
    saveProjectBudget: function(component, event, helper) {
        helper.saveProjectbudget(component);
    },
    getPricingView: function(component, event, helper) {
        helper.getOMSProjectBudget(component);
    },
    onChangelistOfProjectBudgetService: function(component, event, helper) {
        helper.onChangelistOfProjectBudget(component);
    },
    editAllRows: function(component, event, helper) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");

        if (!listOfProjectbudget || listOfProjectbudget.length === 0) {
            return;
        }
        
        for (var i = 0; i < listOfProjectbudget.length; i += 1) {
            if (listOfProjectbudget[i]["GP_Data_Source__c"] == 'Pinnacle')
                listOfProjectbudget[i]["isEditable"] = true;
        }
        component.set("v.listOfProjectbudget", listOfProjectbudget);
    }
})