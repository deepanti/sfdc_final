({
    COMBINATION_REPEAT: "Band-Country-Product combination data already entered.",
    COMBINATION_VALUE_ERROR: "Combination value doesn’t exist in the database",
    BUDGET_DELETED_LABEL:'Project budget deleted successfully.',
    PROJECT_BUDGET: "Project Budget",
    PRICING_VIEW: "Pricing View",
    NONE_ENTRY_LABEL: "null",

    getProjectBudget: function(component) {
        var getProjectBudgetDataService = component.get("c.getProjectBudgetData");
        getProjectBudgetDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectBudgetDataService.setCallback(this, function(response) {
            this.getProjectBudgetDataServiceHandler(response, component);
        });
        $A.enqueueAction(getProjectBudgetDataService);
    },
    getProjectBudgetDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectBudgetData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    getOMSProjectBudget: function(component) {
        var getProjectBudgetDataService = component.get("c.getOMSProjectBudgetData");
        getProjectBudgetDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectBudgetDataService.setCallback(this, function(response) {
            this.getOMSProjectBudgetDataServiceHandler(response, component);
        });
        $A.enqueueAction(getProjectBudgetDataService);
    },
    getOMSProjectBudgetDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setOMSProjectBudgetData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProjectBudgetData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);

        component.set("v.mapOfBudgetRecord", responseJson.mapOfBudgetRecord);
        component.set("v.listOfCountries", this.sortAndAddDefault(responseJson.listOfCountries));
        component.set("v.listOfProducts", this.sortAndAddDefault(responseJson.listOfProducts));
        component.set("v.listOfBands", this.sortAndAddDefault(responseJson.listOfBands));

        if (!responseJson.isOMSType)
            component.set("v.cardTitle", this.PROJECT_BUDGET);
        else
            component.set("v.cardTitle", this.PRICING_VIEW);
        this.setCommonFieldsOfProjectBudget(component, responseJson);
    },
    setOMSProjectBudgetData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        this.setCommonFieldsOfProjectBudget(component, responseJson);
    },
    setCommonFieldsOfProjectBudget: function(component, responseJson) {
        component.set("v.listOfProjectbudget", responseJson.listOfProjectbudget);
        component.set("v.isBudgetNativelyAvailable", responseJson.isBudgetNativelyAvailable);
        component.set("v.isUpdatable", responseJson.isUpdatable);
        component.set("v.isOMSType", responseJson.isOMSType);
        component.set("v.currencyISOCode", responseJson.currencyISOCode);
    },
    addToProjectBudget: function(component) {
        var projectId = component.get("v.recordId");

        var projectBudget = {
            "isEditable": true,
            "GP_Project__c": projectId,
            "GP_Data_Source__c": "Pinnacle"
        };
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        listOfProjectbudget.push(projectBudget);

        component.set("v.listOfProjectbudget", listOfProjectbudget);
    },
    removeProjectbudget: function(component, projectBudgetIndex) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var projectbudgetToBeDeleted = listOfProjectbudget[projectBudgetIndex];

        listOfProjectbudget.splice(projectBudgetIndex, 1);
        component.set("v.listOfProjectbudget", listOfProjectbudget);
        if(projectbudgetToBeDeleted["Id"]){
        	var listOfProjectbudgetToDelete = component.get("v.listOfProjectbudgetToDelete") || [];
            listOfProjectbudgetToDelete.push(projectbudgetToBeDeleted);
            component.set("v.listOfProjectbudgetToDelete",listOfProjectbudgetToDelete);
        }
        //this.deleteProjectbudgetData(component, projectbudgetToBeDeleted["Id"]);
    },
    deleteProjectbudgetData: function(component, projectbudgetId) {
        if (!projectbudgetId)
            return;

        var deleteProjectbudgetDataService = component.get("c.deleteProjectbudgetData");
        deleteProjectbudgetDataService.setParams({
            "projectbudgetId": projectbudgetId
        });
        this.showSpinner(component);
        deleteProjectbudgetDataService.setCallback(this, function(response) {
            this.deleteProjectbudgetDataServiceHandler(response, component);
        });
        $A.enqueueAction(deleteProjectbudgetDataService);
    },
    deleteProjectbudgetDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.fireProjectSaveEvent();
            this.showToast('SUCCESS', 'success', this.BUDGET_DELETED_LABEL);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    saveProjectbudget: function(component) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");

        if (!listOfProjectbudget || listOfProjectbudget.length === 0 ||
            !this.validateProjectBudget(component, listOfProjectbudget)) {
            return;
        }

        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var currencyISOCode = component.get("v.currencyISOCode");

        this.updateCurrencyOnAllRecords(component, listOfProjectbudget, currencyISOCode);
        
        var listOfProjectbudgetToDelete = component.get("v.listOfProjectbudgetToDelete") || [];

        var saveProjectbudgetDataService = component.get("c.saveProjectbudgetData");
        saveProjectbudgetDataService.setParams({
            "strListOfProjectbudget": JSON.stringify(listOfProjectbudget),
            "strListOfProjectBudgetToDelete" : JSON.stringify(listOfProjectbudgetToDelete)
        });

        this.showSpinner(component);

        saveProjectbudgetDataService.setCallback(this, function(response) {
            this.saveProjectbudgetDataServiceHandler(response, component);
        });
        $A.enqueueAction(saveProjectbudgetDataService);
    },
    saveProjectbudgetDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setEditableToFalse(component);
            component.set("v.listOfProjectbudget", JSON.parse(responseData.response));
            this.fireProjectSaveEvent();
            this.showToast('SUCCESS', 'success', 'Records saved successfully.');
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setEditableToFalse: function(component) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");

        for (var i = 0; i < listOfProjectbudget.length; i += 1) {
            listOfProjectbudget[i]["isEditable"] = false;
        }

        component.set("v.listOfProjectbudget", listOfProjectbudget);
    },
    validateProjectBudget: function(component, listOfProjectbudget) {
        var isValid = true;
        var projectBudgetObjectMap = {};
        var key,rowDom;
        for (var i = 0; i < listOfProjectbudget.length; i += 1) {
            key = listOfProjectbudget[i]["GP_Band__c"] + '@@';
            key += listOfProjectbudget[i]["GP_Country__c"];
            key += '@@' + listOfProjectbudget[i]["GP_Product__c"];

            rowDom = document.getElementById('project-budget-row' + i);

            if (this.checkEmptyValuesOfFields(listOfProjectbudget[i])) {
                isValid = false;
                rowDom.className = 'invalid-row';
            } else {
                rowDom.className = '';
            }

            if (!listOfProjectbudget[i]["GP_TCV__c"])
                listOfProjectbudget[i]["errorMessage"] = this.COMBINATION_VALUE_ERROR;
            else if (projectBudgetObjectMap[key]) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfProjectbudget[i]["errorMessage"] = this.COMBINATION_REPEAT;
            } else {
                listOfProjectbudget[i]["errorMessage"] = null;
                projectBudgetObjectMap[key] = listOfProjectbudget[i];
            }
        }
        component.set("v.listOfProjectbudget", listOfProjectbudget);
        return isValid;
    },
    onChangelistOfProjectBudget: function(component) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var isOMSType = component.get("v.isOMSType");
        var totalEfforts = 0;
        var totalTCV = 0;

        if (listOfProjectbudget) {
            for (var i = 0; i < listOfProjectbudget.length; i += 1) {
                //if (!isOMSType) {
                    if (listOfProjectbudget[i]["GP_Effort_Hours__c"])
                        totalEfforts += Number(listOfProjectbudget[i]["GP_Effort_Hours__c"]);
               // } else {
                //    if (listOfProjectbudget[i]["GP_Man_Days__c"])
                 //       totalEfforts += Number(listOfProjectbudget[i]["GP_Man_Days__c"]);
                //}
                if (listOfProjectbudget[i]["GP_TCV__c"])
                    totalTCV += Number(listOfProjectbudget[i]["GP_TCV__c"]);
            }
            component.set("v.totalEfforts", totalEfforts.toFixed(2));
            component.set("v.totalTCV", totalTCV.toFixed(2));
        }
    },
    setTCVAmount: function(component, budgetIndex, budgetField, value) {
        var listOfProjectbudget = component.get("v.listOfProjectbudget");
        var currentProjectBudget = listOfProjectbudget[budgetIndex];
        var mapOfBudgetRecord = component.get("v.mapOfBudgetRecord");

        currentProjectBudget[budgetField] = value;

        var key = currentProjectBudget["GP_Band__c"] + '___';
        key += currentProjectBudget["GP_Country__c"] + '___';
        key += currentProjectBudget["GP_Product__c"];

        var budget = mapOfBudgetRecord[key];

        if (!$A.util.isEmpty(budget) && !$A.util.isEmpty(currentProjectBudget["GP_Effort_Hours__c"])) {
            var tcvEffort = budget["GP_Cost_Per_Hour__c"] * currentProjectBudget["GP_Effort_Hours__c"];
            currentProjectBudget["GP_TCV__c"] = tcvEffort.toFixed(2);
            currentProjectBudget["GP_Cost_Rate__c"] = budget["GP_Cost_Per_Hour__c"];
        } else {
            currentProjectBudget["GP_TCV__c"] = null;
        }

        listOfProjectbudget[budgetIndex] = currentProjectBudget;
        component.set("v.listOfProjectbudget", listOfProjectbudget);
    },
    checkEmptyValuesOfFields: function(budgetRecord) {
        return ($A.util.isEmpty(budgetRecord["GP_Band__c"]) ||
            budgetRecord["GP_Band__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(budgetRecord["GP_Country__c"]) ||
            budgetRecord["GP_Country__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(budgetRecord["GP_Product__c"]) ||
            budgetRecord["GP_Product__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(budgetRecord["GP_Effort_Hours__c"]) ||
            $A.util.isEmpty(budgetRecord["GP_TCV__c"]));
    },
    updateCurrencyOnAllRecords: function(component, listOfProjectbudget, currencyISOCode) {
        for (var i = 0; i < listOfProjectbudget.length; i += 1) {
            listOfProjectbudget[i]["CurrencyISOCode"] = "USD";
        }
        component.set("v.listOfProjectbudget", listOfProjectbudget);
    }
})