({
	
		doInit : function(component, event, helper) {
        
        var action=component.get("c.getQsrm");
        var recordId;
        action.setStorable();
        action.setParams({"oppId": component.get("v.recordId")});
         
            action.setCallback(this, function(response) {
            var state = response.getState();
                
            if(state === "SUCCESS") {
            
                component.set("v.OppQsrm", response.getReturnValue());
                component.set("v.OppListSize", (component.get("v.OppQsrm")).length);
                console.log('Length of array'+component.get("v.OppQsrm").length);
               console.log("product value:  "+JSON.stringify(component.get("v.OppQsrm")));
                console.log("----"+JSON.stringify(recordId));
            } else {
                console.log('Problem getting OppQsrm, response state: ' + state);
            }
                
        });
        $A.enqueueAction(action);
	},
    gotoQsrmRecod : function (component, event) {
        var qsrmArr = component.get("v.OppQsrm");
        var testArray = [];
        testArray = JSON.parse(JSON.stringify(qsrmArr));
        console.log(testArray[0].qsrmId);
        var qsrmID = testArray[0].qsrmId;
        var urlEvent = $A.get("e.force:navigateToSObject");
       	urlEvent.setParams({
           "recordId":qsrmID
        });
        urlEvent.fire();
    },
   
    closeQuickAction: function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

	
})