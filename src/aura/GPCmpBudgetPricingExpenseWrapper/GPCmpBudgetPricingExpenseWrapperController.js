({
    activateBudget: function(component, event, helper) {
        var budgetSideBar = component.find("budgetAndPricing_sidebar");
        var expenseSideBar = component.find("Expense_sidebar");

        var budgetContent = component.find("budgetAndPricing");
        var expenseContent = component.find("expense");

        //$A.util.addClass(budgetSideBar, 'active');
       // $A.util.removeClass(expenseSideBar, 'active');


        //$A.util.addClass(expenseContent, 'slds-hide');
        //$A.util.removeClass(budgetContent, 'slds-hide');

        component.set("v.showBudget",true);
    },
    activateExpense: function(component, event, helper) {
        var budgetSideBar = component.find("budgetAndPricing_sidebar");
        var expenseSideBar = component.find("Expense_sidebar");

        var budgetContent = component.find("budgetAndPricing");
        var expenseContent = component.find("expense");

       // $A.util.removeClass(budgetSideBar, 'active');
       // $A.util.addClass(expenseSideBar, 'active');


        //$A.util.removeClass(expenseContent, 'slds-hide');
       // $A.util.addClass(budgetContent, 'slds-hide');

        component.set("v.showBudget",false);
    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    }
})