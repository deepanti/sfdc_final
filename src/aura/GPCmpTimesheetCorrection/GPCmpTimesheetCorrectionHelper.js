({
    monthList: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    Error_No_Repeated_Combination: "Repetition of Project-Task-Expenditure Type not allowed!",
    Error_Resource_Not_Booked_For_Duration: "Employee not booked for this Date -  ",
    Error_Project_Associated_Entries: "Resource Not Booked On Any Project!",
    Error_Not_Booked_For_Project: "Employee not booked for this Project!",
    Error_In_Hour_Of_Records: "Enter Hours Correctly for Date - ",
    Error_Time_Sheet_Entries: "Time Sheet Entries not found!",
    Error_Not_A_Valid_User: "Invalid User to serve request!",
    Record_Updated_Message: "Records Updated Succesfully!",
    Valid_Input_Message: "Please Enter Valid Inputs!",
    Record_Save_Message: "Record Saved Succesfully!",
    Error_On_Save: "Error Occurred while save!",
    mapOfMonthNameToNumber: {
        "Jan": 1,
        "Feb": 2,
        "Mar": 3,
        "Apr": 4,
        "May": 5,
        "Jun": 6,
        "Jul": 7,
        "Aug": 8,
        "Sep": 9,
        "Oct": 10,
        "Nov": 11,
        "Dec": 12
    },
    mapOfShortMonthNameToLongName: {
        "jan": "january",
        "feb": "february",
        "mar": "march",
        "apr": "april",
        "may": "may",
        "jun": "june",
        "jul": "july",
        "aug": "august",
        "sep": "september",
        "oct": "october",
        "nov": "november",
        "dec": "december"
    },

    validateLoggedInUser: function(component) {
        var getIsValidUserValue = component.get("c.getIsValidUser");

        getIsValidUserValue.setParams({
            "timesheetTransactionId": component.get("v.recordId")
        });

        getIsValidUserValue.setCallback(this, function(response) {
            this.validateLoggedInUserHandler(component, response);
        });

        $A.enqueueAction(getIsValidUserValue);

    },
    validateLoggedInUserHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJson = JSON.parse(responseData.response);
            component.set("v.isValidUser", responseJson.isValidUser);
            var isValidUser = component.get("v.isValidUser");

            if (isValidUser) {
                var recordId = component.get("v.recordId");
                component.set("v.monthYearDuration", responseJson.monthYearDuration);
                component.set("v.financialMonth", responseJson.financialMonth);
                this.setlstOfmonthYear(component);
                if (!recordId) {
                    this.setTimeSheetTranctionRecord(component);
                } else {
                    var todayYear = new Date().getFullYear();
                    todayYear = (todayYear.toString().substr(todayYear.length - 2)).substring(0, 2);
                    responseJson.timesheetRecord.GP_Month_Year__c = responseJson.timesheetRecord.GP_Month_Year__c.substring(0, responseJson.timesheetRecord.GP_Month_Year__c.indexOf("-")) + '-' + todayYear + responseJson.timesheetRecord.GP_Month_Year__c.substring(responseJson.timesheetRecord.GP_Month_Year__c.indexOf("-") + 1, responseJson.timesheetRecord.GP_Month_Year__c.length);
                    component.set("v.timeSheetTranctionRecord", responseJson.timesheetRecord);
                    var employeeDom = component.find("employee");
                    employeeDom.initMethod();
                    this.getTimeSheetTransactions(component, event, this, true);
                }
            } else {
                this.showToast('Error', 'Error', this.Error_Not_A_Valid_User);
                this.showErrorBlock(component, this.Error_Not_A_Valid_User);
                this.hideSpinner(component);
            }
        } else {
            this.handleFailedCallback(component, responseData);
            this.clearInputFields(component);
            this.showErrorBlock(component);
        }

        this.hideSpinner(component);
    },
    getTimeSheetTransactions: function(component, event, helper, skipSelectedCheck) {
        this.showSpinner(component);

        var isValid = this.ValidateInputParams(component, skipSelectedCheck);

        if (isValid) {
            this.resetAllData(component);
            component.set("v.listOfTimesheetTransactionInSystem", null);
            var timeSheetTransactionRecord = JSON.parse(JSON.stringify(component.get("v.timeSheetTranctionRecord")));
            var inputMonthYear = timeSheetTransactionRecord["GP_Month_Year__c"];
            var monthYear = inputMonthYear.trim().split("-");
            timeSheetTransactionRecord["GP_Month_Year__c"] = monthYear[0] + "-" + monthYear[1].substr(monthYear[1].length - 2);
            //this.mapOfShortMonthNameToLongName[monthYear[0].toLowerCase()] + "-" + monthYear[1];
            var objTimeSheet = JSON.stringify(timeSheetTransactionRecord);
            var monthNumber = this.mapOfMonthNameToNumber[monthYear[0]] >= 10 ?
                String(this.mapOfMonthNameToNumber[monthYear[0]]) : '0' + String(this.mapOfMonthNameToNumber[monthYear[0]]);
            var date = monthYear[1] + '-' + monthNumber + '-01';
            component.set("v.monthStartDate", date);
            var getTimesheetTransactions = component.get("c.getTimesheetTransactions");
            getTimesheetTransactions.setParams({
                "objJSONTimeSheetTransaction": objTimeSheet
            });

            getTimesheetTransactions.setCallback(this, function(response) {
                this.getTimesheetTransactionsHandler(component, response);
            });

            $A.enqueueAction(getTimesheetTransactions);
        } else {
            this.showToast('Error', 'Error', this.Valid_Input_Message);
            this.hideSpinner(component);
        }
    },
    getTimesheetTransactionsHandler: function(component, response) {

        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.showError", false);
            var responseJson = JSON.parse(responseData.response);
            if (responseJson.listOfTimesheetTransactionInSystem && responseJson.listOfTimesheetTransactionInSystem.length > 0) {
                this.setIsNewCreatable(component, responseJson.listOfTimesheetTransactionInSystem,responseJson.pinnoraSyncPending);
                component.set("v.listOfTimesheetTransactionInSystem", responseJson.listOfTimesheetTransactionInSystem);
            	component.set("v.validateHoursCheck",responseJson.validateHoursCheck)
            } else {
                component.set("v.isCreationPossible", true);
            }
            var recordId = component.get("v.recordId");
            if (recordId)
                this.selectTimesheetRecordAndFetchEntries(component, recordId, responseJson.listOfTimesheetTransactionInSystem);
        } else {

            this.handleFailedCallback(component, responseData);
            //this.clearInputFields(component);
            this.showErrorBlock(component);

        }

        this.hideSpinner(component);
    },
    selectTimesheetRecordAndFetchEntries: function(component, recordId, listOfTimesheetTransactionInSystem) {
        for (var index = 0; index < listOfTimesheetTransactionInSystem.length; index++) {
            if (recordId == listOfTimesheetTransactionInSystem[index]["Id"]) {
                listOfTimesheetTransactionInSystem[index]["isSelected"] = true;
                break;
            }
        }
        component.set("v.listOfTimesheetTransactionInSystem", listOfTimesheetTransactionInSystem);
        this.getTimeSheetEntries(component, event, this, true, false);
    },
    setIsNewCreatable: function(component, listOfTimesheetTransactionInSystem, pinnoraSyncPending) {
        component.set("v.isCreationPossible", true);
        var draftCount = 0,
            oraclePendingCount = 0,
            pendingForApprovalCount = 0,
            approvedSuccessCount = 0,
            oracleErrorCount = 0,
            oracleProcessCompletionCount = 0;
        for (var index = 0; index < listOfTimesheetTransactionInSystem.length; index++) {
            listOfTimesheetTransactionInSystem[index]["isSelected"] = false;
            listOfTimesheetTransactionInSystem[index]["CreatedDate"] = listOfTimesheetTransactionInSystem[index]["CreatedDate"].substring(0, listOfTimesheetTransactionInSystem[index]["CreatedDate"].indexOf("T"));
            listOfTimesheetTransactionInSystem[index]["LastModifiedDate"] = listOfTimesheetTransactionInSystem[index]["LastModifiedDate"].substring(0, listOfTimesheetTransactionInSystem[index]["LastModifiedDate"].indexOf("T"));
            if (listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Draft')
                draftCount++;
            else if (listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Pending')
                pendingForApprovalCount++;
            else if (listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Approved' 
                     && (listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c != 'S' && 
                        listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c != 'E' &&
                        listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c != 'R'))
                oraclePendingCount++;
            else if(listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Approved' 
                     && (listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c == 'E' || 
                   		listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c == 'R'))
                oracleErrorCount++;
            else if(listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Approved' 
                     && listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c == 'S')
                oracleProcessCompletionCount++;
            /*else if (listOfTimesheetTransactionInSystem[index].GP_Approval_Status__c == 'Approved' && listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c && listOfTimesheetTransactionInSystem[index].GP_Oracle_Status__c == 'E')
                oracleRejectedCount++;*/
        }
        if (draftCount > 0) {
            component.set("v.isCreationPossible", false);
        } else if (pendingForApprovalCount > 0) {
            component.set("v.isCreationPossible", false);
        } else if (oraclePendingCount > 0) {
            component.set("v.isCreationPossible", false);
        } else if(oracleProcessCompletionCount > 0 && pinnoraSyncPending){
            component.set("v.isCreationPossible", false);
        } else if(oracleErrorCount > 0 && oracleProcessCompletionCount == 0){
            component.set("v.isCreationPossible", true);
        } 
        /*else if (oracleRejectedCount > 0) {
                   component.set("v.isCreationPossible", false);
               }*/
    },
    resetAllData: function(component) {
        component.set("v.mapOfRowTotal", null);
        component.set("v.mapOfColumnTotal", null);
        component.set("v.wrapListOfTimeSheetEntries", null);
        component.set("v.wrapListOfTimeSheetEntriesEdit", null);
        component.set("v.lstOfDates", null);
        component.set("v.setRowColTotal", null);
    },
    getTimeSheetEntries: function(component, event, helper, skipSelectedCheck, isNew) {
        this.showSpinner(component);
        var objTimeSheet, inputMonthYear, timeSheetTransactionRecord, monthYear;
        var isValid = this.ValidateInputParams(component, skipSelectedCheck);

        if (isValid) {
            this.resetAllData(component);
            if (isNew)
                component.set("v.isCreationPossible", false);
            timeSheetTransactionRecord = component.get("v.timeSheetTranctionRecord");
            inputMonthYear = timeSheetTransactionRecord["GP_Month_Year__c"];
            monthYear = inputMonthYear.trim().split("-");
            timeSheetTransactionRecord["GP_Month_Year__c"] = monthYear[0] + "-" + monthYear[1].substr(monthYear[1].length - 2);
            objTimeSheet = component.get("v.selectedTimesheetTransactionRecord") || timeSheetTransactionRecord;
            if ((objTimeSheet.Id == null) ||
                (objTimeSheet.Id != null && objTimeSheet.OwnerId.indexOf($A.get("$SObjectType.CurrentUser.Id")) >= 0 &&
                    (objTimeSheet.GP_Approval_Status__c == 'Draft')))
                component.set("v.isEditable", true);
            else
                component.set("v.isEditable", false);
            //this.mapOfShortMonthNameToLongName[monthYear[0].toLowerCase()] + "-" + monthYear[1];


            var monthNumber = this.mapOfMonthNameToNumber[monthYear[0]] >= 10 ?
                String(this.mapOfMonthNameToNumber[monthYear[0]]) : '0' + String(this.mapOfMonthNameToNumber[monthYear[0]]);
            var date = monthYear[1] + '-' + monthNumber + '-01';
            component.set("v.monthStartDate", date);
            var getTimeSheetEntries = component.get("c.getTimeSheetEntries");
            getTimeSheetEntries.setParams({
                "objJSONTimeSheetTransaction": JSON.stringify(objTimeSheet)
            });

            getTimeSheetEntries.setCallback(this, function(response) {
                this.getTimeSheetEntriesHandler(component, response);
            });

            $A.enqueueAction(getTimeSheetEntries);
        } else {
            this.showToast('Error', 'Error', this.Valid_Input_Message);
            this.hideSpinner(component);
        }
    },
    getTimeSheetEntriesHandler: function(component, response) {

        var responseData = response.getReturnValue() || {};
        var forApproval = component.get("v.forApproval");
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            if (!forApproval) {
                this.setEmployeeProjectAllocationData(component, responseData);
                this.setIsApprovalAvailable(component);
            } else {
                component.set("v.isEditable", false);
                component.set("v.isCreationPossible", false);
                component.set("v.forApproval", false);
                component.set("v.listOfTimesheetTransactionInSystem", []);
            }

        } else {

            this.handleFailedCallback(component, responseData);
            //this.clearInputFields(component);
            this.showErrorBlock(component);

        }

        this.hideSpinner(component);
    },
    setIsApprovalAvailable: function(component) {
        var selectedTimesheetTransactionRecord = component.get("v.selectedTimesheetTransactionRecord");
        if (selectedTimesheetTransactionRecord && selectedTimesheetTransactionRecord.GP_Approval_Status__c == 'Pending' && selectedTimesheetTransactionRecord.GP_Approver__c.indexOf($A.get("$SObjectType.CurrentUser.Id")) >= 0) {
            component.set("v.isApprovalAvailable", true);
        } else {
            component.set("v.isApprovalAvailable", false);
        }
        if (selectedTimesheetTransactionRecord && selectedTimesheetTransactionRecord.GP_Approval_Status__c == 'Pending' && selectedTimesheetTransactionRecord.OwnerId.indexOf($A.get("$SObjectType.CurrentUser.Id")) >= 0) {
            component.set("v.isWithdrawAvailable", true);
        } else {
            component.set("v.isWithdrawAvailable", false);
        }
    },
    setEmployeeProjectAllocationData: function(component, responseData) {
        component.set("v.employeeProjectAllocationData", JSON.parse(responseData.response));

        var empProjctAllocation = component.get("v.employeeProjectAllocationData");
        component.set("v.listOfEmployeeHRInactiveRecords", empProjctAllocation.listOfEmployeeHRInactiveRecords);
        component.set("v.timeSheetTransactionId", empProjctAllocation.timeSheetTransactionId);
        component.set("v.isCompeletlyDisabled", empProjctAllocation.isCompeletlyDisabled);
        component.set("v.monthYearDuration", empProjctAllocation.monthYearDuration);
        component.set("v.maxDateIndex", empProjctAllocation.maxDateIndex);
        component.set("v.employeeId", empProjctAllocation.employeeId);
        this.setLstOfDates(component);
        this.setprojectIdsOfEmployeeSet(component, empProjctAllocation);
        this.setwrapListOfTimeSheetEntries(component, empProjctAllocation);
    },
    setwrapListOfTimeSheetEntries: function(component, empProjctAllocation) {
        if ($A.util.isUndefined(empProjctAllocation.wrapperTimeSheetEntries))
            this.showToast('Error', 'Error', this.Error_Time_Sheet_Entries);
        else {
            var wrapperTimeSheetEntries = this.setIsEditableIndexOnEntryRecords(component, empProjctAllocation.wrapperTimeSheetEntries,
                empProjctAllocation.isCompeletlyDisabled,
                empProjctAllocation.maxDateIndex,
                empProjctAllocation.listOfEmployeeHRInactiveRecords);

            component.set("v.wrapListOfTimeSheetEntries", empProjctAllocation.wrapperTimeSheetEntries);
            component.set("v.wrapListOfTimeSheetEntriesEdit", empProjctAllocation.wrapperTimeSheetEntries);

            this.setMinAndMaxHoursOfEmployee(component, empProjctAllocation);
        }
    },
    setmapOfProjectIdVsResourceAllocationEntry: function(component, empProjctAllocation) {
        if ($A.util.isUndefined(empProjctAllocation.mapOfwrapperResourceAllocation)) {
            if (component.get("v.isEditable"))
                this.showToast('Error', 'Error', this.Error_Project_Associated_Entries);
            //component.set("v.projectLookupFilterCondition", 'GP_Approval_Status__c = \'Approved\' and GP_Is_Closed__c = false and (RecordType.Name = \'Indirect PID\' OR RecordType.Name = \'BPM\')');
            component.set("v.projectLookupFilterCondition", 'GP_Approval_Status__c = \'Approved\' and GP_Is_Closed__c = false and id=\'\'');
            //component.set("v.setOfProjectAllocationIds", setOfProjectAllocationIds);
        } else {
            component.set("v.mapOfProjectIdVsResourceAllocationEntry", empProjctAllocation.mapOfwrapperResourceAllocation);
            component.set("v.mapOfProjectAndAssociatedTasks", empProjctAllocation.mapOfProjectAndAssociatedTasks);
            this.createProjectTaskMap(component, empProjctAllocation.mapOfProjectAndAssociatedTasks);
            this.createSetOfProjectAllocationIds(component, empProjctAllocation.mapOfwrapperResourceAllocation);
            this.createListOfProjectIds(component, empProjctAllocation.listOfEmployeeAssociatedProjects);
            //this.setwrapListOfTimeSheetEntries(component, empProjctAllocation);
        }
    },
    createProjectTaskMap: function(component, mapOfProjectAndAssociatedTasks) {
        var mapOfProjectTasks = {};
        for (var projectId in Object.keys(mapOfProjectAndAssociatedTasks)) {
            for (var index in mapOfProjectAndAssociatedTasks[Object.keys(mapOfProjectAndAssociatedTasks)[projectId]]) {
                var taskRecord = mapOfProjectAndAssociatedTasks[Object.keys(mapOfProjectAndAssociatedTasks)[projectId]][index];
                if (taskRecord)
                    mapOfProjectTasks[taskRecord.GP_Task_Number__c] = taskRecord;
            }
        }
        component.set("v.mapOfProjectTasks", mapOfProjectTasks);
    },
    createListOfProjectIds: function(component, listOfEmployeeAssociatedProjects) {
        var listOfProjects = [];
        var listOfProjectAssociatedTasks = [];
        var noneOption = {
            "text": null,
            "label": "--NONE--"
        };
        listOfProjects.push(noneOption);
        listOfProjectAssociatedTasks.push(noneOption);
        for (var key = 0; key < listOfEmployeeAssociatedProjects.length; key++) {
            var project = {
                "text": listOfEmployeeAssociatedProjects[key].GP_Oracle_PID__c,
                "label": listOfEmployeeAssociatedProjects[key].GP_Oracle_PID__c + '--' + listOfEmployeeAssociatedProjects[key].Name
            };
            listOfProjects.push(project);
        }
        component.set("v.listOfProjects", listOfProjects);
        component.set("v.listOfAllocatedProjectData", listOfEmployeeAssociatedProjects);
        component.set("v.listOfProjectAssociatedTasks", listOfProjectAssociatedTasks);
    },
    createSetOfProjectAllocationIds: function(component, mapOfProjectAndResource) {
        var setOfProjectAllocationIds = '(';
        for (var projectId in Object.keys(mapOfProjectAndResource)) {
            setOfProjectAllocationIds += '\'' + Object.keys(mapOfProjectAndResource)[projectId] + '\',';
            //setOfProjectAllocationIds.push(Object.keys(mapOfProjectAndResource)[projectId]);
        }
        setOfProjectAllocationIds = setOfProjectAllocationIds.slice(0, -1);
        setOfProjectAllocationIds += ')';
        component.set("v.setOfProjectAllocationIds", setOfProjectAllocationIds);

        //var queryFilter = 'GP_Is_Closed__c = false and (RecordType.Name = \'Indirect PID\' OR RecordType.Name = \'BPM\' OR id in ' + setOfProjectAllocationIds + ')';
        var queryFilter = 'GP_Is_Closed__c = false and GP_Approval_Status__c =\'Approved\' and id in ' + setOfProjectAllocationIds;
        component.set("v.projectLookupFilterCondition", queryFilter);
    },
    setprojectIdsOfEmployeeSet: function(component, empProjctAllocation) {
        if ($A.util.isUndefined(empProjctAllocation.setOfEmployeeAssociatedProjectId)) {
            if (component.get("v.isEditable"))
                this.showToast('Error', 'Error', this.Error_Project_Associated_Entries);
        } else {
            component.set("v.setOfProjectIdsOfEmployee", empProjctAllocation.setOfEmployeeAssociatedProjectId);
            this.setmapOfProjectIdVsResourceAllocationEntry(component, empProjctAllocation);
        }
    },
    setMinAndMaxHoursOfEmployee: function(component, empProjctAllocation) {
        if ($A.util.isUndefined(empProjctAllocation.minHours) || $A.util.isUndefined(empProjctAllocation.maxHours))
            this.showToast('Error', 'Error', this.Error_Hour_Range);
        else {
            component.set("v.minHours", empProjctAllocation.minHours);
            component.set("v.maxHours", empProjctAllocation.maxHours);
        }
    },
    setLstOfDates: function(component) {

        var inputMonthYear = (component.get("v.timeSheetTranctionRecord.GP_Month_Year__c"));
        var numberOfDays = this.getNoOfDatesInMonth(inputMonthYear);
        var lstOfDates = [];
        var mapOfDateVsDay = component.get("v.mapOfDateVsDay") || {};
        var mapOfDateVsClassName = component.get("v.mapOfDateVsClassName") || {};
        var monthYear = inputMonthYear.trim().split("-");
        var date = new Date();

        for (var i = 0; i < numberOfDays; i++) {
            date = new Date(String((new Date()).getFullYear()).substr(0, 2) + monthYear[1], this.mapOfMonthNameToNumber[monthYear[0]] - 1, i + 1);
            //lstOfDates.push(i);
            var prop = date.toString().split(' ')[0] == "Sat" ||
                date.toString().split(' ')[0] == "Sun" ? date.toString().split(' ')[0] : i + 1;
            mapOfDateVsDay[i] = prop;
            lstOfDates.push(prop);
            mapOfDateVsClassName[i] = prop == "Sat" || prop == "Sun" ? '' : '';
        }

        component.set("v.lstOfDates", lstOfDates);
        component.set("v.mapOfDateVsDay", mapOfDateVsDay);
        component.set("v.mapOfDateVsClassName", mapOfDateVsClassName);
        this.hideErrorBlock(component);
    },
    getNoOfDatesInMonth: function(inputMonthYear) {

        var monthYear = inputMonthYear.trim().split("-");
        return new Date(monthYear[1], this.mapOfMonthNameToNumber[monthYear[0]], 0).getDate();

    },
    showErrorBlock: function(component, ErrorString) {

        component.set("v.errorString", ErrorString);
        component.set("v.showError", true);

    },
    hideErrorBlock: function(component) {

        component.set("v.errorString", '');
        component.set("v.showError", false);
    },
    setTimeSheetTranctionRecord: function(component) {

        component.set("v.timeSheetTranctionRecord", {});
        this.hideSpinner(component);

    },
    saveModifiedRecordList: function(component) {
        this.showSpinner(component);
        var modifiedList = component.get("v.wrapListOfTimeSheetEntriesEdit");
        var reurnValueForUniqueValidation = this.validateRowsUniqueness(component, modifiedList);

        if (reurnValueForUniqueValidation['isValid']) {
            var returnValue = this.ValidateModifiedList(component);
            if (returnValue['isValid']) {
                var timeSheetEntriesModified = JSON.stringify(modifiedList);
                var saveTimeSheetEntries = component.get("c.saveTimeSheetEntries");
                var timeSheetTransactionRecord = component.get("v.timeSheetTranctionRecord");
                var inputMonthYear = timeSheetTransactionRecord["GP_Month_Year__c"];
                var timeSheetTransactionId = component.get("v.timeSheetTransactionId");
                var employeeId = component.get("v.employeeId");
                var forApproval = component.get("v.forApproval");
                saveTimeSheetEntries.setParams({
                    "lstJSONTimeSheetEntries": timeSheetEntriesModified,
                    "isForApproval": forApproval,
                    "monthYear": inputMonthYear,
                    "timeSheetTransactionId": timeSheetTransactionId,
                    "employeeId": employeeId

                });

                saveTimeSheetEntries.setCallback(this, function(response) {
                    this.saveModifiedRecordListHandler(component, response);
                });

                $A.enqueueAction(saveTimeSheetEntries);
            } else {
                this.showToast('Error', 'Error', returnValue['errorString']);
                component.set("v.forApproval", true);
                this.hideSpinner(component);
            }
        } else {
            this.showToast('Error', 'Error', reurnValueForUniqueValidation['errorString']);
            this.hideSpinner(component);
        }
        //component.set("v.forApproval", false);

    },
    saveModifiedRecordListHandler: function(component, response) {
		component.set("v.forApproval", false);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {

            this.showToast('Success', 'Success', this.Record_Save_Message);
            this.getTimeSheetEntriesHandler(component, response);
            //this.setTimeSheetTranctionRecord(component);
            //this.showErrorBlock(component);
            //this.clearInputFields(component);

        } else {

            this.handleFailedCallback(component, responseData);
            this.clearInputFields(component);
            this.showErrorBlock(component);
        }

        this.hideSpinner(component);
    },
    clearInputFields: function(component) {
        var employeeLookupComponent = component.find("employee")
        employeeLookupComponent.ClearLookupControl();
        component.set("v.timeSheetTranctionRecord", {});
        //component.set("v.timeSheetTranctionRecord.GP_Month_Year__c", '--None--');
    },
    AddDummyRow: function(component, cloneIndex, copyModifiedHours, copyActualHours) {
        //dummy JSON create attach to the list
        var lstOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntriesEdit");
        var dummyRecord = this.clone(lstOfTimeSheetEntries[cloneIndex]);
        var timeSheetTransactionId = lstOfTimeSheetEntries[cloneIndex].GP_Timesheet_Transaction__c;
        var timeSheetTransactionRecord = component.get("v.timeSheetTranctionRecord");
        dummyRecord.exType = 'Billable';
        dummyRecord.projectName = null;
        dummyRecord.isSelected = false;
        dummyRecord.isDefault = false;
        dummyRecord.newRecord = true;
        dummyRecord.projectId = null;
        dummyRecord.taskName = null;
        dummyRecord.taskId = null;

        dummyRecord.lstOfTimeSheetEntries.forEach(function(entry) {

            entry.GP_Modified_Hours__c = copyModifiedHours ? entry.GP_Modified_Hours__c : 0;
            entry.GP_Actual_Hours__c = copyActualHours ? entry.GP_Actual_Hours__c : 0;
            entry.Id = null;
            entry.GP_Timesheet_Transaction__c = dummyRecord.timeSheetTransactionId;

        });

        lstOfTimeSheetEntries.push(dummyRecord);
        component.set("v.wrapListOfTimeSheetEntriesEdit", lstOfTimeSheetEntries);
        var mapOfRowTotal = component.get("v.mapOfRowTotal");
        var lstOfDates = component.get("v.lstOfDates");

        mapOfRowTotal[lstOfTimeSheetEntries.length - 1] = 0;
        component.set("v.mapOfRowTotal", mapOfRowTotal);
    },
    clone: function(object) {
        return JSON.parse(JSON.stringify(object));
    },
    ValidateInputParams: function(component, skipSelectedCheck) {
        var inputEmployee = component.get("v.timeSheetTranctionRecord.GP_Employee__c");
        var inputMonthYear = component.get("v.timeSheetTranctionRecord.GP_Month_Year__c");
        var listOfTimesheetTransactionInSystem = component.get("v.listOfTimesheetTransactionInSystem") || [];
        var timesheetSelected = this.getSelectedTimesheetRecord(component, listOfTimesheetTransactionInSystem);
        component.set("v.selectedTimesheetTransactionRecord", timesheetSelected);
        if (!skipSelectedCheck && listOfTimesheetTransactionInSystem.length > 0 && !timesheetSelected)
            return false;

        if (!inputEmployee) {
            return false;
        }
        if (!inputMonthYear || inputMonthYear == '' || inputMonthYear == null || inputMonthYear == '--None--') {
            return false;
        }

        var monthYear = inputMonthYear.trim().split("-");

        if ($A.util.isUndefined(inputEmployee))
            return false;
        if ($A.util.isUndefined(inputMonthYear) && inputMonthYear.indexOf('-') < 0)
            return false;
        // if (!$A.util.isUndefined(monthYear) && (monthYear[0].length != 3 || monthYear[1].length != 4))
        //     return false;
        return true;
    },
    getSelectedTimesheetRecord: function(component, listOfTimesheetTransactionInSystem) {
        for (var index = 0; index < listOfTimesheetTransactionInSystem.length; index++) {
            if (listOfTimesheetTransactionInSystem[index].isSelected)
                return listOfTimesheetTransactionInSystem[index];
        }
    },
    ValidateModifiedList: function(component) {
        var dateWiseHours = {};
        var returnValue = { 'isValid': true, 'errorString': '' };
        var minHours = component.get("v.minHours");
        var maxHours = component.get("v.maxHours");
        var isValidEntries = true;
        var modifiedList = component.get("v.wrapListOfTimeSheetEntriesEdit");
        var mapOfProjectVsResource = component.get("v.mapOfProjectIdVsResourceAllocationEntry") || {};
        var inputMonthYear = (component.get("v.timeSheetTranctionRecord.GP_Month_Year__c"));
        var monthYear = inputMonthYear.trim().split("-");
        var year = String((new Date()).getFullYear()).substr(0, 2) + monthYear[1];
        var month = this.mapOfMonthNameToNumber[monthYear[0]];

        for (var wrapperTimeSheetEntry in modifiedList) {
            var projctId = modifiedList[wrapperTimeSheetEntry].projectId;
            var taskId = modifiedList[wrapperTimeSheetEntry].taskId;
            if ($A.util.isUndefined(projctId) || projctId == null || projctId == '') {
                returnValue['isValid'] = false;
                returnValue['errorString'] = 'Please Select Project!';
                isValidEntries = false;
                break;
            }
            if ($A.util.isUndefined(taskId) || taskId == null || taskId == '') {
                returnValue['isValid'] = false;
                returnValue['errorString'] = 'Please Select Task!';
                isValidEntries = false;
                break;
            }

            for (var day = 0; day < modifiedList[wrapperTimeSheetEntry].lstOfTimeSheetEntries.length; day++) {
                var entry = modifiedList[wrapperTimeSheetEntry].lstOfTimeSheetEntries[day];
                var resourceAllocationEntries = mapOfProjectVsResource[projctId];
                if ( //(modifiedList[wrapperTimeSheetEntry].projectRecordTypeName == 'CMITS' ||
                    //  modifiedList[wrapperTimeSheetEntry].projectRecordTypeName == 'Other PBB') &&
                    $A.util.isUndefined(resourceAllocationEntries)) {
                    returnValue['isValid'] = false;
                    returnValue['errorString'] = this.Error_Not_Booked_For_Project;
                    isValidEntries = false;
                    break;
                }
                if (entry['GP_Modified_Hours__c'] > 0) {
                    var inputDate = new Date(year, month - 1, day + 1);
                    if (dateWiseHours[inputDate])
                        dateWiseHours[inputDate] += entry.GP_Modified_Hours__c;
                    else
                        dateWiseHours[inputDate] = entry.GP_Modified_Hours__c;
                    var isValid = this.validateDateRange(inputDate, resourceAllocationEntries); // (modifiedList[wrapperTimeSheetEntry].projectRecordTypeName == 'CMITS' ||
                    //modifiedList[wrapperTimeSheetEntry].projectRecordTypeName == 'Other PBB') ? this.validateDateRange(inputDate, resourceAllocationEntries) : true;
                    if (!isValid) {
                        returnValue['isValid'] = false;
                        returnValue['errorString'] = this.Error_Resource_Not_Booked_For_Duration + (month) + "-" + (day + 1) + "-" + year;
                        isValidEntries = false;
                        break;
                    }
                    var mapOfProjectTasks = component.get("v.mapOfProjectTasks");
                    var projectTaskRecord = mapOfProjectTasks[modifiedList[wrapperTimeSheetEntry].taskId];
                    if (projectTaskRecord && new Date(projectTaskRecord.GP_Start_Date__c).setHours(0, 0, 0, 0) > inputDate &&
                        (projectTaskRecord.GP_End_Date__c ||
                            (projectTaskRecord.GP_End_Date__c &&
                                new Date(projectTaskRecord.GP_End_Date__c).setHours(0, 0, 0, 0) < inputDate))) {
                        returnValue['isValid'] = false;
                        returnValue['errorString'] = 'Task is Active from : ' + projectTaskRecord.GP_Start_Date__c;
                        returnValue['errorString'] += projectTaskRecord.GP_End_Date__c ? ' to ' + projectTaskRecord.GP_End_Date__c : '';
                        isValidEntries = false;
                        break;
                    }

                } else if (entry['GP_Modified_Hours__c'] < 0) {
                    returnValue['isValid'] = false;
                    returnValue['errorString'] = 'Hours cannot be less than zero ,please enter hours correctly for ' + (month) + "-" + (day + 1) + "-" + year;
                    isValidEntries = false;
                    break;
                }

            }

        }
		var validateHoursCheck = component.get("v.validateHoursCheck");
        if(validateHoursCheck){
        for (var key in Object.keys(dateWiseHours)) {
            if (!(dateWiseHours[Object.keys(dateWiseHours)[key]] >= 8 &&
                    dateWiseHours[Object.keys(dateWiseHours)[key]] <= 24)) {
                var inputDate = new Date(Object.keys(dateWiseHours)[key]);
                var year = inputDate.getFullYear() + "";
                var month = (inputDate.getMonth() + 1) + "";
                var day = inputDate.getDate() + "";
                var dateFormat = month + "-" + day + "-" + year;
                returnValue['isValid'] = false;
                returnValue['errorString'] = this.Error_In_Hour_Of_Records + "  " + dateFormat + ' Range (' + minHours + 'hrs-' + maxHours + 'hrs)';
                isValidEntries = false;
                break;
            }
        }
        }

        if (!isValidEntries)
            return returnValue;

        return returnValue;
    },
    validateDateRange: function(inputDate, listOfAllocationRecords) {
        var isValid = false;

        for (var index = 0; index < listOfAllocationRecords.length; index++) {
            var startDate = new Date(listOfAllocationRecords[index].GP_Start_Date__c).setHours(0, 0, 0, 0);
            var endDate = new Date(listOfAllocationRecords[index].GP_End_Date__c).setHours(0, 0, 0, 0);
            var parsedInputDate = new Date(inputDate).setHours(0, 0, 0, 0);
            if ((parsedInputDate >= startDate &&
                    parsedInputDate <= endDate)) {
                isValid = true;
                break;
            }

        }

        if (!isValid)
            return false;
        return true;
    },
    validateRowsUniqueness: function(component, listOfModifiedRecords) {
        var projectTaskExTypeUniqueList = {};
        var returnValue = { 'isValid': true, 'errorString': '' };

        for (var index = 0; index < listOfModifiedRecords.length; index++) {
            var key = listOfModifiedRecords[index].projectId +
                listOfModifiedRecords[index].taskId +
                listOfModifiedRecords[index].exType;
            if (projectTaskExTypeUniqueList[key]) {
                returnValue['isValid'] = false;
                returnValue['errorString'] = this.Error_No_Repeated_Combination;
                return returnValue;
            } else {
                projectTaskExTypeUniqueList[key] = listOfModifiedRecords[index];
            }
        }
        return returnValue;
    },
    setlstOfmonthYear: function(component) {
        var lstOfmonthYear = [];
        var listOfYearBand = [];
        var noneString = '--None--';
        var todayMonth = new Date().getMonth();
        var monthYearDuration = component.get("v.monthYearDuration");
        var todayYear = new Date().getFullYear();
        var count = 0;
        lstOfmonthYear.push(noneString);

        for (var i = todayMonth; count <= monthYearDuration; i--) {
            lstOfmonthYear.push(this.monthList[i] + '-' + todayYear.toString().substr(todayYear.length - 2));
            if (i == 0) {
                todayYear = todayYear - 1;
                i = 12;
            }
            count++;

        }

        component.set("v.lstOfmonthYear", lstOfmonthYear);
    },
    setRowColTotal: function(component, wrapperTimeSheetEntries) {
        var mapOfRowTotal = component.get("v.mapOfRowTotal");
        var mapOfColumnTotal = component.get("v.mapOfColumnTotal");
        mapOfColumnTotal = {};
        mapOfRowTotal = {};
        var rowsum = {};

        for (var wrapTimeSheetTransactionIndex in wrapperTimeSheetEntries) {
            var colsum = 0;
            for (var timeSheetEntryIndex in wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"]) {
                colsum += wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["GP_Modified_Hours__c"];
                if (!mapOfColumnTotal[timeSheetEntryIndex])
                    mapOfColumnTotal[timeSheetEntryIndex] = 0;

                mapOfColumnTotal[timeSheetEntryIndex] += wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["GP_Modified_Hours__c"];

            }
            mapOfRowTotal[wrapTimeSheetTransactionIndex] = colsum;
        }
        component.set("v.mapOfRowTotal", mapOfRowTotal);
        component.set("v.mapOfColumnTotal", mapOfColumnTotal);
    },
    setIsEditableIndexOnEntryRecords: function(component, wrapperTimeSheetEntries, isCompeletlyDisabled, maxDateIndex, listOfEmployeeHRInactiveRecords) {

        var lstOfDates = component.get("v.lstOfDates");

        var inputMonthYear = (component.get("v.timeSheetTranctionRecord.GP_Month_Year__c"));
        var monthYear = inputMonthYear.trim().split("-");
        var year = monthYear[1];
        var month = this.mapOfMonthNameToNumber[monthYear[0]];

        this.setRowColTotal(component, wrapperTimeSheetEntries);

        for (var wrapTimeSheetTransactionIndex in wrapperTimeSheetEntries) {
            wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["isSelected"] = false;
            for (var timeSheetEntryIndex in wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"]) {
                if (!wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["isDisabled"]) {
                    for (var employeeHRInactiveRecord in listOfEmployeeHRInactiveRecords) {
                        var inputDate = new Date(year, month - 1, Number(timeSheetEntryIndex) + 1).setHours(0, 0, 0, 0);;
                        var startDate = new Date(listOfEmployeeHRInactiveRecords[employeeHRInactiveRecord].GP_ASGN_EFFECTIVE_START__c).setHours(0, 0, 0, 0);
                        var endDate = new Date(listOfEmployeeHRInactiveRecords[employeeHRInactiveRecord].GP_ASGN_EFFECTIVE_END__c).setHours(0, 0, 0, 0);
                        var parsedInputDate = new Date(inputDate).setHours(0, 0, 0, 0);
                        if (inputDate >= startDate &&
                            inputDate <= endDate)
                            wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["isDisabled"] = true;
                    }
                }
                if (lstOfDates[timeSheetEntryIndex] == 'Sat' || lstOfDates[timeSheetEntryIndex] == 'Sun')
                    wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["className"] = 'weekEndBGColor hours';
                else
                    wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["className"] = 'hours';

                if (!isCompeletlyDisabled && maxDateIndex == -2)
                    wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["isDisabled"] = false;
                else if (isCompeletlyDisabled || maxDateIndex == 0 || Number(timeSheetEntryIndex) + 1 > maxDateIndex)
                    wrapperTimeSheetEntries[wrapTimeSheetTransactionIndex]["lstOfTimeSheetEntries"][timeSheetEntryIndex]["isDisabled"] = true;
            }
        }
        return wrapperTimeSheetEntries;
    },
    refreshAllContent: function(component) {
        component.set("v.wrapListOfTimeSheetEntries", null);
        component.set("v.wrapListOfTimeSheetEntriesEdit", null);
        component.set("v.lstOfDates", null);
        this.clearInputFields(component);
    },
    getJobRecordAndRedirect: function(component) {
        var getJobId = component.get("c.getJobId");

        getJobId.setCallback(this, function(response) {
            this.getJobIdHandler(component, response);
        });

        $A.enqueueAction(getJobId);
    },
    getJobIdHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS") {
            var responseJson = JSON.parse(responseData.response);
            if (responseJson.Id) {
                this.redirectToSobject(responseJson.Id, "detail");
            } else
                component.set("v.showMassUploadPopup", false);
        } else {
            this.handleFailedCallback(component, responseData);
            component.set("v.showMassUploadPopup", false);
        }

        this.hideSpinner(component);
    },
    processApproval: function(component, event, helper, action) {
        var timesheetStatusToApproveOrReject = component.get("c.timesheetStatusToApproveOrReject");
        var selectedTimesheetTransactionRecord = component.get("v.selectedTimesheetTransactionRecord") || {};
        if (selectedTimesheetTransactionRecord && selectedTimesheetTransactionRecord.Id) {
            timesheetStatusToApproveOrReject.setParams({
                "timesheetTransactionId": selectedTimesheetTransactionRecord.Id,
                "strComment": '',
                "action": action

            });

            timesheetStatusToApproveOrReject.setCallback(this, function(response) {
                this.processApprovalHandler(component, response);
            });

            $A.enqueueAction(timesheetStatusToApproveOrReject);
        }
    },
    processApprovalHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJson = JSON.parse(responseData.message);
            if (responseJson.Id)
                this.redirectToSobject(responseJson.Id, "detail");
        } else {
            this.handleFailedCallback(component, responseData);
            this.clearInputFields(component);
            this.showErrorBlock(component);
        }
        this.hideSpinner(component);
    },
    withdrawRecord: function(component, response) {
        var recallRequest = component.get("c.recallRequest");
        var selectedTimesheetTransactionRecord = component.get("v.selectedTimesheetTransactionRecord") || {};
        if(!(selectedTimesheetTransactionRecord && selectedTimesheetTransactionRecord.OwnerId.indexOf($A.get("$SObjectType.CurrentUser.Id")) >= 0)){
            this.showToast('Error', 'Error', 'Not the record submitter');
            return;
        }
        if (selectedTimesheetTransactionRecord && selectedTimesheetTransactionRecord.Id) {
            recallRequest.setParams({
                "timesheetTransactionId": selectedTimesheetTransactionRecord.Id
            });

            recallRequest.setCallback(this, function(response) {
                this.processApprovalHandler(component, response);
            });

            $A.enqueueAction(recallRequest);
        }
    }
})