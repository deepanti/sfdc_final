({
    doInit: function(component, event, helper) {
        helper.validateLoggedInUser(component);
    },
    clearEmployeeRecord: function(component, event, helper) {
        var timeSheetTranctionRecord = component.get("v.timeSheetTranctionRecord") || {};
        timeSheetTranctionRecord["GP_Employee__c"] = null;
        component.set("v.timeSheetTranctionRecord", timeSheetTranctionRecord);
    },
    getTimeSheetTransactionRecords: function(component, event, helper) {
        helper.getTimeSheetTransactions(component, event, helper, true);
    },
    createNewRecord: function(component, event, helper) {
        component.set("v.listOfTimesheetTransactionInSystem", null);
        helper.getTimeSheetEntries(component, event, helper, false, true);
    },
    viewTimesheetRecord: function(component, event, helper) {
        var selectedTimesheetTransactionRecord = component.get("v.selectedTimesheetTransactionRecord");
        if (!selectedTimesheetTransactionRecord) {
            helper.showToast('Error', 'Error', 'Please select a record');
            return;
        }
        helper.getTimeSheetEntries(component, event, helper, false, false);
    },
    getTimeSheetEntry: function(component, event, helper) {
        helper.getTimeSheetEntries(component, event, helper, true, false);
    },
    saveRecords: function(component, event, helper) {
        helper.saveModifiedRecordList(component);
    },
    saveAndSubmitRecords: function(component, event, helper) {
        component.set("v.forApproval", true);
        helper.saveModifiedRecordList(component);
    },
    AddRow: function(component, event, helper) {
        helper.AddDummyRow(component, 0, false, false);
    },
    cloneSelectedRow: function(component, event, helper) {
        var indexSelected;// = 0;
        var wrapListOfTimeSheetEntriesEdit = component.get("v.wrapListOfTimeSheetEntriesEdit") || [];
        for (var wrapTimeSheetTransactionIndex in wrapListOfTimeSheetEntriesEdit) {
            if (wrapListOfTimeSheetEntriesEdit[wrapTimeSheetTransactionIndex]["isSelected"]) {
                indexSelected = wrapTimeSheetTransactionIndex;
                break;
            }
        }
        if(!indexSelected)
        {
            helper.showToast('Error', 'Error', 'Please select a record');
            return;
        }
        helper.AddDummyRow(component, indexSelected, true, false);
    },
    refreshData: function(component, event, helper) {
        helper.refreshAllContent(component);
    },
    toggleMassUpload: function(component, event, helper) {
        component.set("v.showMassUploadPopup", !component.get("v.showMassUploadPopup"));
    },
    redirectToJobRecord: function(component, event, helper) {
        helper.getJobRecordAndRedirect(component);
    },
    setRowColTotal: function(component, event, helper) {
        var wrapperTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        helper.setRowColTotal(component, wrapperTimeSheetEntries);
    },
    approveTimesheetRecord: function(component, event, helper) {
        helper.processApproval(component, event, helper,'Approve');
    },
    rejectTimesheetRecord: function(component, event, helper) {
        helper.processApproval(component, event, helper,'Reject');
    },
    toggleSelection: function(component, event, helper) {
        var timesheetTransactionIndex = event.getSource().get("v.name");
        var listOfTimesheetTransactionInSystem = component.get("v.listOfTimesheetTransactionInSystem") || [];
        for (var index = 0; index < listOfTimesheetTransactionInSystem.length; index++) {
            if (index == timesheetTransactionIndex){
                listOfTimesheetTransactionInSystem[index].isSelected = listOfTimesheetTransactionInSystem[index].isSelected ? true : false;
                component.set("v.selectedTimesheetTransactionRecord",listOfTimesheetTransactionInSystem[index].isSelected ? listOfTimesheetTransactionInSystem[index] : null);
            }
            else
                listOfTimesheetTransactionInSystem[index].isSelected = false;
        }
        component.set("v.listOfTimesheetTransactionInSystem", listOfTimesheetTransactionInSystem);
    },
    withdrawSubmission: function(component, event, helper) {
        var selectedTimesheetTransactionRecord = component.get("v.selectedTimesheetTransactionRecord");
        if (!selectedTimesheetTransactionRecord) {
            helper.showToast('Error', 'Error', 'Please select a record');
            return;
        }
        helper.withdrawRecord(component, event, helper);
    }
})