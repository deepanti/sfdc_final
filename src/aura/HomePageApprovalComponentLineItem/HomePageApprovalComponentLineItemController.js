({
    init: function (cmp, event, helper) {
        var product = cmp.get("v.wrapper.prodList");
        var prodLst = product.split(',');
        cmp.set("v.prodList",prodLst);
        cmp.set("v.TCV",cmp.get("v.wrapper.TCV"));
        
    },
	openOpportunity : function(cmp, event, helper) {
		var recordId = cmp.get("v.wrapper.recId");
        helper.navigateToRecordDetails(cmp,recordId);
	},
    openAccount : function(cmp, event, helper) {
		var recordId = cmp.get("v.wrapper.accountId");
        helper.navigateToRecordDetails(cmp,recordId);
	},
    openQsrm:function(cmp,event,helper)
    {
       var recordId = cmp.get("v.wrapper.qsrmId");
        helper.navigateToRecordDetails(cmp,recordId); 
    },
    approve : function(cmp, event, helper) {
        var cmpEvent = cmp.getEvent("cmpEvent");
        cmpEvent.setParams({
            "approvalRecordId" : cmp.get("v.wrapper.approvalItemId"),
            "approvalType" : cmp.get("v.wrapper.approvalType"),
            "OpportunityName" : cmp.get("v.wrapper.oppName"),
            "QsrmName" : cmp.get("v.wrapper.qsrmName"),
            "actionToPerform" : 'Approve'
        });
        cmpEvent.fire();
	},
    reject : function(cmp, event, helper) {
        var cmpEvent = cmp.getEvent("cmpEvent");
        cmpEvent.setParams({
            "approvalRecordId" : cmp.get("v.wrapper.approvalItemId"),
            "approvalType" : cmp.get("v.wrapper.approvalType"),
            "OpportunityName" : cmp.get("v.wrapper.oppName"),
            "QsrmName" : cmp.get("v.wrapper.qsrmName"),
            "actionToPerform" : 'Reject'
        });
        cmpEvent.fire(); 
	},
    rework : function(cmp, event, helper) {
        var cmpEvent = cmp.getEvent("cmpEvent");
        cmpEvent.setParams({
            "approvalRecordId" : cmp.get("v.wrapper.approvalItemId"),
            "approvalType" : cmp.get("v.wrapper.approvalType"),
            "OpportunityName" : cmp.get("v.wrapper.oppName"),
            "QsrmName" : cmp.get("v.wrapper.qsrmName"),
            "actionToPerform" : 'Rework'
        });
        cmpEvent.fire();
	},
     ReworkQsrm : function(cmp,event,helper){debugger;
         var recordId = cmp.get("v.wrapper.qsrmId");
         function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
         if(!isLightningExperienceOrSalesforce1()) {
              //sforce.one.editRecord('/'+recordId,'_blank');
             window.open('/'+recordId,'_blank');
         }
         else{
             
             window.location = '/'+recordId;
             
         }
        // alert('rework resubmit'+recordId);
     }
})