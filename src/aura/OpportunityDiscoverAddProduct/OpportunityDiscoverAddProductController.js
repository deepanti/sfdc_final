({
    
    closeModal:function(component,event,helper){    
        component.set("v.isSearchOpen", false);
    },
    
    openmodal:function(component,event,helper) {
        component.set("v.isOpen", true);
    },
    
    init: function (component, event, helper)
    {   
        helper.showSpinner(component, event, helper);
        component.find("reset").set("v.disabled", true);
        component.find("resetPopup").set("v.disabled", true);
        var recordId = component.get("v.recordId");  
        helper.getProducts(component); 
        helper.getOpportunityClosedDate(component);
        helper.getOpportunityLineItems(component, event, helper);
        helper.getProducts_Popup(component); 
        
        var action = component.get("c.getOpportunityClosedDate");
        var recordId = component.get("v.recordId");
        
        
        action.setParams({
            "opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){    
            
            var loggedInUser = response.getReturnValue().loggedInUser.Id;
            var ownerId =response.getReturnValue().ownerId;
            if(loggedInUser != ownerId){
                component.set("v.isloggedInUser", false);
            }
            else{
                component.set("v.isloggedInUser", true);
            }
            
        });
        $A.enqueueAction(action);   
    },
    hideSpinner: function(component, event, helper) {       
        helper.hideSpinner(component, event, helper);
    },
    
    SearchProducts : function(component, event, helper)
    {
        //component.set("v.isSearching", true);
        component.set("v.Spinner", true); 
        component.set("v.isSearchOpen", true);
        component.set("v.Spinner", false);
        var productList = component.get("v.productWrapperList");
        
        component.set("v.selectedProductList", []);
        
        var page = page || 1;
        var recordId = component.get("v.recordId");    
        var ServiceLine_Value = component.get("v.selectedServiceLine");
        
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
        
        var ProductName_Value = component.get("v.selectedProductName");
        
        if(ServiceLine_Value != '--None--' || NatureOfWork_Value != '--None--' || ProductName_Value != '--None--')
        {
            component.set("v.isLoading", true);
            var action = component.get("c.getSearchedProduct");
            action.setParams({
                "opportunityID" : recordId,               
                "ServiceLine" : ServiceLine_Value,
                "NatureOfWork" : NatureOfWork_Value,
                "ProductName" : ProductName_Value,
                "isSearching" : true
            });
            action.setCallback(this, function(response){ 
                
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var result = response.getReturnValue();
                    
                    component.set("v.productWrapperList",result);
                    helper.setProductInWrapperList(component, result, page);
                    
                    component.set("v.selectedProductName", "--None--");
                    component.set("v.selectedNatureOfWork","--None--");
                    component.set("v.selectedServiceLine", "--None--");
                    
                    component.set("v.OnChangeServiceLine", false);
                    component.set("v.OnChangeNatureOfWork", false);
                    component.set("v.OnChangeProductName", false);
                    
                    component.find("ProductName").set("v.disabled", false);
                    component.find("ServiceLine").set("v.disabled", false); 
                    component.find("NatureOfWork").set("v.disabled", false); 
                    
                    
                    helper.getProducts(component, helper);
                }    
            });
            $A.enqueueAction(action);
        }         
        else
        {	
            component.set("v.isSearchOpen", false);
            if(ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--'){
                component.set("v.errorMessage", "Please select at least one search filter.");
                component.set("v.isError", true);
                
                
                component.set("v.isLoading", false);
                var final_OLI_List = component.get("v.final_OLI_List");
                if(final_OLI_List.length == 0){
                    component.set("v.isproductWrapperListEmpty", false);
                }
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);
                            
                        }    
                    }),2000
                );    
                
            }
        }       
    },
    clearallfilters :  function(component, event, helper)
    {
        
        component.set("v.selectedServiceLine", "--None--");
        component.set("v.selectedNatureOfWork", "--None--");
        component.set("v.selectedProductName", "--None--");
        
        component.find("ProductName").set("v.disabled", false);
        component.find("ServiceLine").set("v.disabled", false); 
        component.find("NatureOfWork").set("v.disabled", false);
        
        component.set("v.OnChangeServiceLine", false);
        component.set("v.OnChangeNatureOfWork", false);
        component.set("v.OnChangeProductName", false);
        
        component.find("reset").set("v.disabled", true);
        helper.getProducts(component);
        
    },
    
    clearallfiltersPopup :  function(component, event, helper)
    {
        
        component.set("v.selectedServiceLine_Popup", "--None--");
        component.set("v.selectedNatureOfWork_Popup", "--None--");
        component.set("v.selectedProductName_Popup", "--None--");
        
        component.find("ServiceLine_Popup").set("v.disabled", false);
        component.find("NatureOfWork_Popup").set("v.disabled", false); 
        component.find("ProductName_Popup").set("v.disabled", false);
        
        component.set("v.OnChangeServiceLine_Popup", false);
        component.set("v.OnChangeNatureOfWork_Popup", false);
        component.set("v.OnChangeProductName_Popup", false);
        
        component.find("resetPopup").set("v.disabled", false);
        helper.getProducts_Popup(component);
        
    },
    handleEdit: function(component, event, helper) {
        
       // $A.enqueueAction(component.get('c.clearallfiltersPopup'));
		component.set("v.OnChangeServiceLine_Popup", true);
        component.set("v.OnChangeNatureOfWork_Popup", true);    
        
        component.set("v.isOpen", true);
        component.find("resetPopup").set("v.disabled", false);
        var index = event.target.id;
        component.set("v.listIndex", index);
        var final_OLI_List = component.get("v.final_OLI_List");

        //anjali 11/19
        component.set("v.ServiceLineArrayList_Popup", component.get("v.ServiceLineArrayList_Popup_Original"));
        component.set("v.NatureOfWorkArrayList_Popup", component.get("v.NatureOfWorkArrayList_Popup_Original"));
        component.set("v.ProductNameArrayList_Popup", component.get("v.ProductNameArrayList_Popup_Original"));
        
        //FAKING 
        component.set("v.selectedServiceLine_Popup_Temp", "--None--");
        component.set("v.selectedNatureOfWork_Popup_Temp", "--None--");
        component.set("v.selectedProductName_Popup_Temp", "--None--");
        
        
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
        
        
        //reset
        component.set("v.selectedServiceLine_Popup", final_OLI_List[index].Product2.Service_Line__c);
        component.set("v.selectedNatureOfWork_Popup", final_OLI_List[index].Product2.Nature_of_Work__c);
        component.set("v.selectedProductName_Popup", final_OLI_List[index].Product2.Name);
        
        // Now set our preferred value
        component.find("ServiceLine_Popup").set("v.value", final_OLI_List[index].Product2.Service_Line__c);
        component.find("NatureOfWork_Popup").set("v.value", final_OLI_List[index].Product2.Nature_of_Work__c);
        component.find("ProductName_Popup").set("v.value", final_OLI_List[index].Product2.Name);
        
        
         var ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup");
        //Fake var ProductName_Value = component.get("v.selectedProductName_Popup");	
        var ProductName_Value = component.get("v.selectedProductName_Popup");
        
        
        
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
        var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");

        
        if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option'))
            {
                
                helper.getProducts_Popup(component);
                component.find("resetPopup").set("v.disabled", false);
            }
            if(ServiceLine_Value == 'Select an Option'){
                component.find("resetPopup").set("v.disabled", true);
            }
        
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
        //reset search component
        component.set("v.selectedProductName_Popup_Temp", "--None--");
        component.set("v.selectedNatureOfWork_Popup_Temp","--None--");
        component.set("v.selectedServiceLine_Popup_Temp", "--None--");
        
        component.set("v.selectedProductName_Popup", "--None--");
        component.set("v.selectedNatureOfWork_Popup","--None--");
        component.set("v.selectedServiceLine_Popup", "--None--");                
        
        component.set("v.OnChangeServiceLine_Popup", false);
        component.set("v.OnChangeNatureOfWork_Popup", false);
        component.set("v.OnChangeProductName_Popup", false);
        
        component.find("ProductName_Popup").set("v.disabled", false);
        component.find("ServiceLine_Popup").set("v.disabled", false); 
        component.find("NatureOfWork_Popup").set("v.disabled", false); 
        $A.enqueueAction(component.get('c.clearallfiltersPopup'));
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');		
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    
    onSavePopup : function(component, event, helper) {
        
        var index = component.get("v.listIndex");		
        var recordId = component.get("v.recordId");
        var ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup");
        var ProductName_Value = component.get("v.selectedProductName_Popup");
        
        if(ServiceLine_Value == '--None--' || ServiceLine_Value == 'Select an Option' || 
           NatureOfWork_Value == '--None--' || NatureOfWork_Value == 'Select an Option' || 
           ProductName_Value =='--None--' || ProductName_Value =='Select an Option')
        {
            helper.fireToastError(component, event, helper, 'Please fill all fields.');
            
        }
        
        //get the Product2 which matches these conditions
        else{
            var action = component.get("c.getSearchedProduct");
            action.setParams({
                "opportunityID" : recordId,   
                "ServiceLine" : ServiceLine_Value,
                "NatureOfWork" : NatureOfWork_Value,
                "ProductName" : ProductName_Value,
                "isSearching" : false
                
            });
            action.setCallback(this, function(response){ 
                
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    var result = response.getReturnValue();
                    //from the result 
                    var final_OLI_List = component.get("v.final_OLI_List");	
                    var oliid= final_OLI_List[index].Id;
                    //alert(oliid);
                    final_OLI_List[index] = result.oliList[0];	
                    final_OLI_List[index].Id=oliid;
                    component.set("v.final_OLI_List", final_OLI_List);
                    //alert(JSON.stringify(final_OLI_List));
                    component.set("v.isOpen",false); 
                    
                    //reset search component
                    component.set("v.selectedProductName_Popup_Temp", "--None--");
                    component.set("v.selectedNatureOfWork_Popup_Temp","--None--");
                    component.set("v.selectedServiceLine_Popup_Temp", "--None--");
                    
                    component.set("v.selectedProductName_Popup", "--None--");
                    component.set("v.selectedNatureOfWork_Popup","--None--");
                    component.set("v.selectedServiceLine_Popup", "--None--");                
                    
                    component.set("v.OnChangeServiceLine_Popup", false);
                    component.set("v.OnChangeNatureOfWork_Popup", false);
                    component.set("v.OnChangeProductName_Popup", false);
                    
                    component.find("ProductName_Popup").set("v.disabled", false);
                    component.find("ServiceLine_Popup").set("v.disabled", false); 
                    component.find("NatureOfWork_Popup").set("v.disabled", false); 
                    helper.getProducts(component, helper);
                    component.set("v.isLoading", false);
                    var cmpTarget = component.find('Modalbox');
                    var cmpBack = component.find('Modalbackdrop');		
                    $A.util.removeClass(cmpBack,'slds-backdrop--open');
                    $A.util.removeClass(cmpTarget, 'slds-fade-in-open');
                      $A.get('e.force:refreshView').fire();
                    
                }    
            });
            $A.enqueueAction(action);					
            
            
        }        
        
    },
    
    
    changeRowSelection : function(component, event, helper)
    {
        var productID = event.target.id;
        var checkBoxValue = document.getElementById(event.target.id).checked;
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        
        var productList = component.get("v.productWrapperList");
        
        if(checkBoxValue){
            for(index in productList){
                var tempOli=new Object;
                tempOli.Product2Id=productList[index].product.Id;
                tempOli.Product2=productList[index].product;
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    
                    productWrapper.isSelect = true;
                    selectedProductList.push(tempOli);
                }
            }
        }
        else{
            component.set("v.selectAllFlag", false);
            for(index in productList){
                var tempOli=new Object;
                tempOli.Product2Id=productList[index].product.Id;
                tempOli.Product2=productList[index].product;
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    productWrapper.isSelect = false;
                    selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id),1);
                }
            }
        }
        if(selectedProductList.length > 0){
            component.find("addToCart").set("v.disabled", false);
        }
        else{
            component.find("addToCart").set("v.disabled", true);
        }
        component.set("v.selectedProductList", selectedProductList);
    },
    
    
    
    OnChange_ServiceLine_Popup:function(component, event, helper) 
    {					
        
        component.set("v.OnChangeServiceLine_Popup", true);
        component.set("v.isFilterLoading_Popup", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList_Popup");
        
        //get the selected dropdown values - but we need to fake it via temp variables
        var ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        if(ServiceLine_Value =='Others')
            var  confirmation=confirm('You have selected product(s) with service line as ‘Others’. If this is erroneous, then please select the correct product – service line combination else click cancel to proceed.')
            
            if(confirmation)
            {
                var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup_Temp");
                var ProductName_Value = component.get("v.selectedProductName_Popup_Temp");
                
                var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
                var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
                var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");
                
                if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
                   || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
                   || (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option')
                   || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option'))
                {
                    
                    helper.getProducts_Popup(component);
                    component.find("resetPopup").set("v.disabled", false);
                }
                if(ServiceLine_Value == 'Select an Option'){
                    component.find("resetPopup").set("v.disabled", true);
                }
            }
        else{
            var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup_Temp");
            var ProductName_Value = component.get("v.selectedProductName_Popup_Temp");
            
            
            
            var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
            var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
            var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");
            
            if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option'))
            {
                
                helper.getProducts_Popup(component);
                component.find("resetPopup").set("v.disabled", false);
            }
            if(ServiceLine_Value == 'Select an Option'){
                component.find("resetPopup").set("v.disabled", true);
            }
        }
    },
    
    
    OnChange_NatureOfWork_Popup:function(component, event, helper) 
    {		
        
        component.set("v.OnChangeNatureOfWork_Popup", true);
        component.set("v.isFilterLoading_Popup", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList_Popup");
        
        //get the dropdown values - but we need to fake it via temp variables
        //Fake : var ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        var ServiceLine_Value = component.get("v.selectedServiceLine_Popup_Temp");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup");
        //Fake var ProductName_Value = component.get("v.selectedProductName_Popup");	
        var ProductName_Value = component.get("v.selectedProductName_Popup_Temp");
        
        
        
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
        var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");
        
        if( (ServiceLine_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option') 
           || (ServiceLine_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option')
           || (ServiceLine_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option')
           || (ServiceLine_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option'))
        {
            helper.getProducts_Popup(component); 		
            component.find("resetPopup").set("v.disabled", false);
        }   
        if(NatureOfWork_Value == 'Select an Option'){
            component.find("resetPopup").set("v.disabled", true);
        }
        
        
    },
    
    OnChange_ProductName_Popup:function(component, event, helper) 
    {	
        component.set("v.OnChangeProductName_Popup", true);
        component.set("v.isFilterLoading_Popup", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList_Popup");
        
        //fake it var ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        var ServiceLine_Value = component.get("v.selectedServiceLine_Popup_Temp");
        
        //fake it var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup_Temp");        
        var ProductName_Value = component.get("v.selectedProductName_Popup");	
        
        
        
        
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
        var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");
        
        if( (ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != 'Select an Option') 
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != 'Select an Option')
           || (ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != 'Select an Option')
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != 'Select an Option'))		
        {
            helper.getProducts_Popup(component); 		
            component.find("resetPopup").set("v.disabled", false);
        }
        if(ProductName_Value == 'Select an Option'){
            component.find("resetPopup").set("v.disabled", true);
        }
        
    },
    
    
    OnChange_ServiceLine:function(component, event, helper) 
    {		
        var confirmation=true;
        if(event.getSource().get("v.value")=='Others')
            confirmation=confirm('You have selected product(s) with service line as ‘Others’. If this is erroneous, then please select the correct product – service line combination else click OK to proceed.')
            if(!confirmation)
            {
                event.getSource().set("v.value","Select an Option");
                $A.enqueueAction(component.get('c.clearallfilters'));
                $A.get('e.force:refreshView').fire();
                
            }
        else{
            component.set("v.OnChangeServiceLine", true);
            component.set("v.isFilterLoading", true);
            
            
            var OppProduct2Data = [];				
            OppProduct2Data = component.get("v.productDataList");
            var ServiceLine_Value = component.get("v.selectedServiceLine");
            var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
            var ProductName_Value = component.get("v.selectedProductName");	
            var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
            var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
            var IsOnChangeProductName = component.get("v.OnChangeProductName");
            
            if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option')
               || (IsOnChangeServiceLine == true  && NatureOfWork_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && ServiceLine_Value != 'Select an Option'))
            {
                helper.getProducts(component);
                component.find("reset").set("v.disabled", false);
            }
            
            if(ServiceLine_Value == 'Select an Option'){
                component.find("reset").set("v.disabled", true);
            }
            //errorMessage
            if(ServiceLine_Value = 'Select an Option')
                document.getElementById("ServiceLineError").style.display = "none";
        }
    },
    
    OnChange_NatureOfWork:function(component, event, helper) 
    {		
        component.set("v.OnChangeNatureOfWork", true);
        component.set("v.isFilterLoading", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList");
        var ServiceLine_Value = component.get("v.selectedServiceLine");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
        var ProductName_Value = component.get("v.selectedProductName");	
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
        if( (ServiceLine_Value == 'Select an Option' && ProductName_Value == 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option') 
           || (ServiceLine_Value != 'Select an Option' && ProductName_Value == 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option')
           || (ServiceLine_Value == 'Select an Option' && ProductName_Value != 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option')
           || (ServiceLine_Value != 'Select an Option' && ProductName_Value != 'Select an Option' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != 'Select an Option'))
        {
            helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }  
        if(NatureOfWork_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        //errorMessage
        if(NatureOfWork_Value = 'Select an Option')
            document.getElementById("NatureOfWorkError").style.display = "none";
        
        
    },
    
    
    OnChange_ProductName:function(component, event, helper) 
    {
        
        component.set("v.OnChangeProductName", true);
        component.set("v.isFilterLoading", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList");
        var ServiceLine_Value = component.get("v.selectedServiceLine");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
        var ProductName_Value = component.get("v.selectedProductName");	
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
        
        
        
        if( (ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--') 
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--'))		
        {
            helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }
        if(ProductName_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        //errorMessage
        if(ProductName_Value = 'Select an Option')
            document.getElementById("ProductNameError").style.display = "none";
    },
    
    
    
    selectAllProducts : function(component, event, helper)
    {
        var selectallFlag = component.get("v.selectAllFlag");
        var productList = component.get("v.productWrapperList");
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        if(selectallFlag){
            for(index in productList){
                var tempOli=new Object;
                tempOli.Product2Id=productList[index].product.Id;
                tempOli.Product2=productList[index].product;
                var productWrapper = productList[index];
                
                productWrapper.isSelect = true;
                document.getElementById(productWrapper.product.Id).checked = true;
                if(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id) === -1) {
                    selectedProductList.push(tempOli);
                    
                }  
            }
        }
        else{
            for(index in productList){
                var tempOli=new Object;
                tempOli.Product2Id=productList[index].product.Id;
                tempOli.Product2=productList[index].product;
                var productWrapper = productList[index];
                
                document.getElementById(productWrapper.product.Id).checked = false;
                productWrapper.isSelect = false;
                selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id),1);
                
            }
        }
        if(selectedProductList.length > 0){
            component.find("addToCart").set("v.disabled", false);
        }
        else{
            component.find("addToCart").set("v.disabled", true);
        }
        component.set("v.selectedProductList", selectedProductList);
    },
    
    getFinalListOfSelectedProducts : function(component, event, helper){
        component.find("addToCart").set("v.disabled", true);
        var selectedProductList = component.get("v.selectedProductList");
        
        var productWrapperList = component.get("v.productWrapperList");
        
        var totalProductList = component.get("v.totalProductList");
        
        var index = 0;
        var final_OLI_List = component.get("v.final_OLI_List");
        
        
        var page = page || 1;
        
        final_OLI_List.push.apply(final_OLI_List,selectedProductList);  
        
        component.set("v.final_OLI_List", final_OLI_List);
        
        for(index in selectedProductList){
            var selectedProduct = selectedProductList[index];
            var ind = productWrapperList.findIndex(x => x.product.Id==selectedProduct.Id);
            if(ind != -1){
                productWrapperList.splice(ind,1);    
            }
            totalProductList.splice(totalProductList.findIndex(x => x.product.Id==selectedProduct.Product2Id),1);
        }
        component.set("v.totalProductList", totalProductList);
        component.set("v.selectedProductList",[]);
        
        helper.getProductFromWrapperList(component,page);
        component.set("v.isSearchOpen",false); 
        
    },
    
    deleteSelectedProduct : function(component, event, helper){
        
        //var productID = event.getSource().get("v.name");
        var index = event.getSource().get("v.name");
        
        var final_OLI_List = component.get("v.final_OLI_List");
        
        final_OLI_List.splice(index,1);
        
        
        component.set("v.final_OLI_List", final_OLI_List);
    },
    
    closeErrorToast : function(component, event, helper){
        component.set("v.isError", false);
    },
    
    addProductToCart : function(component, event, helper){
        var ServiceLine_Value = component.get("v.selectedServiceLine");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
        var ProductName_Value = component.get("v.selectedProductName");
        
        
        
        
        if(ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--') {
            
            document.getElementById("ServiceLineError").style.display = "block";
            document.getElementById("NatureOfWorkError").style.display = "block";
            document.getElementById("ProductNameError").style.display = "block";
            
        }
       
        if(ServiceLine_Value == '--None--' || ServiceLine_Value == 'Select an Option' || 
           NatureOfWork_Value == '--None--' || NatureOfWork_Value == 'Select an Option' || 
           ProductName_Value =='--None--' || ProductName_Value =='Select an Option')
        {
            helper.fireToastError(component, event, helper, 'Please fill all fields.');
            
        }
            else{
                component.set("v.isproductWrapperListEmpty", false);
                var page = page || 1;
                var recordId = component.get("v.recordId");    
                
                var final_OLI_List = component.get("v.final_OLI_List");
                
                var action = component.get("c.getSearchedProduct");
                action.setParams({
                    "opportunityID" : recordId,               
                    "ServiceLine" : ServiceLine_Value,
                    "NatureOfWork" : NatureOfWork_Value,
                    "ProductName" : ProductName_Value,
                    "isSearching" : false
                    
                });
                action.setCallback(this, function(response){ 
                    
                    var state = response.getState();
                    
                    if(state === 'SUCCESS')
                    {
                        var result = response.getReturnValue();
                        
                        if(result.total === 0) {
                            helper.fireToastError(component, event, helper, 'No Products with Matching Criteria Found');
                        }
                        else{
                            final_OLI_List.push.apply(final_OLI_List, result.oliList);
                            component.set("v.final_OLI_List", final_OLI_List);
                        }
                        //reset search component
                        component.set("v.selectedProductName", "--None--");
                        component.set("v.selectedNatureOfWork","--None--");
                        component.set("v.selectedServiceLine", "--None--");
                        
                        component.set("v.OnChangeServiceLine", false);
                        component.set("v.OnChangeNatureOfWork", false);
                        component.set("v.OnChangeProductName", false);
                        
                        component.find("ProductName").set("v.disabled", false);
                        component.find("ServiceLine").set("v.disabled", false); 
                        component.find("NatureOfWork").set("v.disabled", false); 
                        
                        helper.getProducts(component, helper);
                        component.set("v.isLoading", false);
                    }
                    else if (state === "ERROR") {
                        
                        helper.fireToastError(component, event, helper, response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
            }
    },
   
    handleSave :function(component, event, helper)
    {         
       component.set("v.IsSpinner",true);
        // helper.showSpinner(component, event, helper);
        helper.saveProducts(component, event, helper ,false);
        $A.enqueueAction(component.get('c.init'));
       
    },
    //vijendra added function for next
    saveAndNext : function(component, event, helper) {  
 component.set("v.IsSpinner",true);        
        //helper.showSpinner(component, event, helper);
        helper.saveProducts(component, event, helper ,true);
         $A.enqueueAction(component.get('c.init'));
          //$A.get('e.force:refreshView').fire();
    }, 
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',2);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "2" });
        compEvents.fire();
        window.scrollTo(0, 0);
        
    },
})