({	
    
    /** :::::: Logic Summary :::::: 
	1. Product Name
		- Only changing NatureOfWork and ServiceLineArrayList dropdown
		2. Service Line Change
			- Only changing NatureOfWork dropdown
		3. Nature of Work
			- Nothing
			
		2. Nature
			- Only changing ServiceLineArrayList dropdown
		3. Service
			- Nothing

	1. Nature of Work
		- Only Product List will change
		2. Product 
			- Only changing ServiceLineArrayList dropdown
		3. Service 
			- Nothing
		
		2. Service
			- Only changing ProductNameArrayList dropdown
		3. Product
			- Nothing

		
	1. Service
		- Only changing NatureOfWork and ProductNameArrayList dropdown
		2. Nature
			- Only changing ProductNameArrayList dropdown
		3. Product
			- Nothing
		
		2. Product
			- Only changing NatureOfWork dropdown
		3. Nature
			- Nothing
	
*******/
    getProducts_Popup : function(component, page)
    {
        
        var page = page || 1;
        var recordId = component.get("v.recordId");
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine_Popup");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork_Popup");
        var IsOnChangeProductName = component.get("v.OnChangeProductName_Popup");
        
        //Actual Values
        var ServiceLine_Value_Actual = component.get("v.selectedServiceLine_Popup");
        var NatureOfWork_Value_Actual = component.get("v.selectedNatureOfWork_Popup");
        var ProductName_Value_Actual = component.get("v.selectedProductName_Popup");
        
        //Since popup would be called by EDIT, we need to first set every value to None
        //Temp Values
        var ServiceLine_Value = component.get("v.selectedServiceLine_Popup_Temp");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup_Temp");
        var ProductName_Value = component.get("v.selectedProductName_Popup_Temp");
        
        
        
        
        //Now based on what dropdown was selcted, set that particular actual selected value
        if(IsOnChangeServiceLine) {
            ServiceLine_Value = component.get("v.selectedServiceLine_Popup");
        }
        if(IsOnChangeNatureOfWork) {
            NatureOfWork_Value = component.get("v.selectedNatureOfWork_Popup");
        }
        if(IsOnChangeProductName) {
            ProductName_Value = component.get("v.selectedProductName_Popup");
        }
        
        
        
        //which ever were changed, take ONLY those value and query. Dont query on previously selected values
        //and once the result is returned fill the dropdowns
        //there can be chances that dropdown gets populated with values which doesnt contain pre-selected value
        //in that case you need to set the temp value(?) to blank
        if(IsOnChangeServiceLine == true || IsOnChangeNatureOfWork == true || IsOnChangeProductName == true )		
        {            
            var action = component.get("c.getProductsOnChange");
            action.setParams({
                "opportunityID" : recordId,
                "SelectedServiceLine" : ServiceLine_Value,	
                "SelectedNatureOfWork" : NatureOfWork_Value,
                "SelectedProductName" : ProductName_Value,
            });
            action.setCallback(this, function(response) { 
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading_Popup", false);
                    var result = response.getReturnValue();
                    
                    var productList = result.productList;
                    
                    var OnChangeService_productDataList =[]; 
                    OnChangeService_productDataList.push(JSON.stringify(productList));
                    component.set("v.OnChangeService_productDataList_Popup", OnChangeService_productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.OnChangeService_productDataList_Popup");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        
                        ProductNameArrayList.push(product.Name);
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates	
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }	
                    
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    
                    
                    if(IsOnChangeServiceLine === true  && NatureOfWork_Value === '--None--' && ProductName_Value === '--None--') 
                    {	
                        
                        component.set("v.NatureOfWorkArrayList_Popup", unique_NatureOfWorkArrayList);  
                        component.set("v.ProductNameArrayList_Popup", unique_ProductNameArrayList); 
                        
                        //check if the pre-sel values exist in these new dropdown
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWork_Value_Actual) == -1) {
                            
                            component.set("v.selectedNatureOfWork_Popup", "--None--");        					
                        }
                        
                        //check if the pre-sel values exist in these new dropdown
                        if(unique_ProductNameArrayList.indexOf(ProductName_Value_Actual) == -1) {
                            
                            component.set("v.selectedProductName_Popup", "--None--");
                        }                        
                    }	
                    else if(IsOnChangeServiceLine === true  && NatureOfWork_Value !== '--None--' && ProductName_Value === '--None--') 
                    { 
                        
                        component.set("v.ProductNameArrayList_Popup", unique_ProductNameArrayList);                                                 
                        
                        //check if the pre-sel values exist in these new dropdown
                        if(unique_ProductNameArrayList.indexOf(ProductName_Value_Actual) == -1) {
                            
                            component.set("v.selectedProductName_Popup", "--None--");
                        }
                        
                    }
                        else if(IsOnChangeServiceLine === true  && NatureOfWork_Value === '--None--' && ProductName_Value !== '--None--') 
                        { 
                            
                            component.set("v.NatureOfWorkArrayList_Popup", unique_NatureOfWorkArrayList);  
                            
                            //check if the pre-sel values exist in these new dropdown
                            if(unique_NatureOfWorkArrayList.indexOf(NatureOfWork_Value_Actual) == -1) {
                                
                                component.set("v.selectedNatureOfWork_Popup", "--None--");        					
                            }
                        }
                            else if(IsOnChangeNatureOfWork === true  && ServiceLine_Value === '--None--' && ProductName_Value === '--None--') 
                            { 
                                
                                component.set("v.ProductNameArrayList_Popup", unique_ProductNameArrayList); 
                                
                                //check if the pre-sel values exist in these new dropdown
                                if(unique_ProductNameArrayList.indexOf(ProductName_Value_Actual) == -1) {
                                    
                                    component.set("v.selectedProductName_Popup", "--None--");
                                }
                                
                            }
                                else if(IsOnChangeNatureOfWork === true  && ServiceLine_Value !== '--None--' && ProductName_Value === '--None--') 
                                { 
                                    
                                    component.set("v.ProductNameArrayList_Popup", unique_ProductNameArrayList); 
                                    
                                    //check if the pre-sel values exist in these new dropdown
                                    if(unique_ProductNameArrayList.indexOf(ProductName_Value_Actual) == -1) {
                                        
                                        component.set("v.selectedProductName_Popup", "--None--");
                                    }
                                }
                                    else if(IsOnChangeNatureOfWork === true  && ServiceLine_Value === '--None--' && ProductName_Value !== '--None--') 
                                    { 
                                        
                                        component.set("v.ServiceLineArrayList_Popup", unique_ServiceLineArrayList);
                                        
                                        //check if the pre-sel values exist in these new dropdown
                                        if(unique_ServiceLineArrayList.indexOf(ServiceLine_Value_Actual) == -1) {
                                            
                                            component.set("v.selectedServiceLine_Popup", "--None--");
                                        }
                                    }
                                        else if(IsOnChangeProductName === true  && ServiceLine_Value === '--None--' && NatureOfWork_Value === '--None--') 
                                        { 
                                            
                                            component.set("v.ServiceLineArrayList_Popup", unique_ServiceLineArrayList);  
                                            component.set("v.NatureOfWorkArrayList_Popup", unique_NatureOfWorkArrayList); 
                                            
                                            //check if the pre-sel values exist in these new dropdown
                                            if(unique_NatureOfWorkArrayList.indexOf(NatureOfWork_Value_Actual) == -1) {
                                                
                                                component.set("v.selectedNatureOfWork_Popup", "--None--");
                                                
                                            }
                                            if(unique_ServiceLineArrayList.indexOf(ServiceLine_Value_Actual) == -1) {
                                                
                                                component.set("v.selectedServiceLine_Popup", "--None--");					        
                                            }
                                        }
                                            else if(IsOnChangeProductName === true  && ServiceLine_Value !== '--None--' && NatureOfWork_Value === '--None--') 
                                            {   
                                                
                                                component.set("v.NatureOfWorkArrayList_Popup", unique_NatureOfWorkArrayList); 
                                                
                                                if(unique_NatureOfWorkArrayList.indexOf(NatureOfWork_Value_Actual) == -1) {
                                                    
                                                    component.set("v.selectedNatureOfWork_Popup", "--None--");        					
                                                }
                                            }
                                                else if(IsOnChangeProductName === true  && ServiceLine_Value === '--None--' && NatureOfWork_Value !== '--None--') 
                                                { 
                                                    
                                                    component.set("v.ServiceLineArrayList_Popup", unique_ServiceLineArrayList);					 
                                                    
                                                    if(unique_ServiceLineArrayList.indexOf(ServiceLine_Value_Actual) == -1) {
                                                        
                                                        component.set("v.selectedServiceLine_Popup", "--None--");                              
                                                    }
                                                }
                    
                    
                    
                }			
            });
            
            
            if(IsOnChangeServiceLine && IsOnChangeNatureOfWork && ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'){
                
                
                component.find("ServiceLine_Popup").set("v.disabled", true);
                component.find("NatureOfWork_Popup").set("v.disabled", true);
                component.set("v.OnChangeServiceLine_Popup", false);
                component.set("v.OnChangeNatureOfWork_Popup", false);
                
                
            }
            if(IsOnChangeServiceLine && IsOnChangeProductName && ServiceLine_Value != '--None--' && ProductName_Value != '--None--'){
                
                component.find("ServiceLine_Popup").set("v.disabled", true);
                component.find("ProductName_Popup").set("v.disabled", true); 
                component.set("v.OnChangeServiceLine_Popup", false);
                component.set("v.OnChangeProductName_Popup", false);
            }
            
            if(IsOnChangeNatureOfWork && IsOnChangeProductName && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
                
                component.find("NatureOfWork_Popup").set("v.disabled", true);
                component.find("ProductName_Popup").set("v.disabled", true); 
                component.set("v.OnChangeNatureOfWork_Popup", false);
                component.set("v.OnChangeProductName_Popup", false);
            }
        }			
        else
        {	
            var action = component.get("c.getProducts");
            action.setParams({
                "opportunityID" : recordId           
            });
            
            action.setCallback(this, function(response){ 
                var state = response.getState();
                
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading_Popup", false);
                    
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var productDataList =[];
                    productDataList.push(JSON.stringify(productList));
                    component.set("v.productDataList_Popup", productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.productDataList_Popup");
                    for(index in productList)
                    {
                        
                        product = productList[index];
                        ProductNameArrayList.push(product.Name);
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates		
                    }	
                    
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    component.set("v.ProductNameArrayList_Popup", unique_ProductNameArrayList); // Product Name list
                    component.set("v.ServiceLineArrayList_Popup", unique_ServiceLineArrayList); // Service Line list
                    component.set("v.NatureOfWorkArrayList_Popup", unique_NatureOfWorkArrayList); 	
                    
                    //anjali 11/19
                    component.set("v.ProductNameArrayList_Popup_Original", unique_ProductNameArrayList); // Product Name list
                    component.set("v.ServiceLineArrayList_Popup_Original", unique_ServiceLineArrayList); // Service Line list
                    component.set("v.NatureOfWorkArrayList_Popup_Original", unique_NatureOfWorkArrayList); 	
                    
                    
                }  
                
            });
        }
        $A.enqueueAction(action); 
    },
    
    getProducts : function(component, page)
    {
        var page = page || 1;
        var recordId = component.get("v.recordId");
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        var ServiceLine_Value = component.get("v.selectedServiceLine");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWork");
        var ProductName_Value = component.get("v.selectedProductName");
        
        
        if(IsOnChangeServiceLine === true || IsOnChangeNatureOfWork === true || IsOnChangeProductName === true )		
        {            
            var action = component.get("c.getProductsOnChange");
            action.setParams({
                "opportunityID" : recordId,
                "SelectedServiceLine" : ServiceLine_Value,	
                "SelectedNatureOfWork" : NatureOfWork_Value,
                "SelectedProductName" : ProductName_Value,
            });
            action.setCallback(this, function(response) { 
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading", false);
                    var result = response.getReturnValue();
                    
                    
                    
                    var productList = result.productList;
                    var OnChangeService_productDataList =[]; 
                    OnChangeService_productDataList.push(JSON.stringify(productList));
                    component.set("v.OnChangeService_productDataList", OnChangeService_productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.OnChangeService_productDataList");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name);
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates	
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }	
                    
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    
                    
                    if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--') 
                    {	
                        component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList);  
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }	
                    else if(IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value == '--None--') 
                    { 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }
                        else if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value != '--None--') 
                        { 
                            component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList);  
                        }
                            else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value == '--None--') 
                            { 
                                component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                            }
                                else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value != '--None--' && ProductName_Value == '--None--') 
                                { 
                                    component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                                }
                                    else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value != '--None--') 
                                    { 
                                        component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);
                                    }
                                        else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--') 
                                        { 
                                            
                                            component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);  
                                            component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                                        }
                                            else if(IsOnChangeProductName == true  && ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--') 
                                            {   
                                                
                                                component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                                            }
                                                else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--') 
                                                { 
                                                    
                                                    component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);					 
                                                }
                    
                    
                }			
            });
            
            
            if(IsOnChangeServiceLine && IsOnChangeNatureOfWork && ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeNatureOfWork", false);
                
                
            }
            if(IsOnChangeServiceLine && IsOnChangeProductName && ServiceLine_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true); 
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeProductName", false);
            }
            
            if(IsOnChangeNatureOfWork && IsOnChangeProductName && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true); 
                component.set("v.OnChangeNatureOfWork", false);
                component.set("v.OnChangeProductName", false);
            }
        }			
        else
        {	
            var action = component.get("c.getProducts");
            action.setParams({
                "opportunityID" : recordId,            
            });
            
            action.setCallback(this, function(response){ 
                var state = response.getState();
                
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading", false);
                    
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var productDataList =[];
                    productDataList.push(JSON.stringify(productList));
                    component.set("v.productDataList", productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.productDataList");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name); // Product Name list
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates		
                    }	
                    
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    component.set("v.ProductNameArrayList", unique_ProductNameArrayList); // Product Name list
                    component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList); // Service Line list
                    component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 	
                    
                    
                }  
                
            });
        }
        $A.enqueueAction(action); 
    },	
    
    
    getOpportunityLineItems :function(component, event, helper) {
        var action = component.get("c.getAddedProducts_LineItems");
        
        var listOpptyLineItemsFromServer =[];
        
        action.setParams(
            {
                'oppId' :component.get("v.recordId")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (state === "SUCCESS") {
                listOpptyLineItemsFromServer=response.getReturnValue(); 
                
                component.set("v.final_OLI_List",listOpptyLineItemsFromServer); 
            }
            else
            {
                
            }
            
        });
        $A.enqueueAction(action);
        
    },
    
    setProductInWrapperList : function(component, result, page)
    {
        var productList = result.productList;
        var index;
        var selectedIndex;
        var product;
        var totalProductList = [];
        
        for(index in productList)
        {
            product = productList[index];
            var productWrapper ={
                "product" : product,                       
                "isSelect" : false
            } 
            totalProductList.push(productWrapper);
        }
        
        component.set("v.page",page);
        component.set("v.total",result.total);
        component.set("v.pages",Math.ceil(result.total/component.get("v.pageSize")));
        component.set('v.selectAllFlag', false);
        component.set("v.totalProductList", totalProductList);
        component.set("v.productWrapperList", totalProductList);
        
        if(totalProductList.length > 0)
        {
            component.set("v.isproductWrapperListEmpty", true);
        }
        else
        {
            component.set("v.isproductWrapperListEmpty", false);
        }
        component.set("v.isLoading", false);
    },
    
    getProductFromWrapperList : function(component, page)
    {
        var productWrapperList = [];
        var req_list = [];
        var pageSize = component.get("v.pageSize");
        var totalList = component.get("v.totalProductList");
        
        req_list=totalList;
        component.set("v.page",page);
        component.set("v.productWrapperList", req_list);
        component.set("v.pages",Math.ceil(totalList.length/component.get("v.pageSize")));
        
    },
    getOpportunityClosedDate : function(component, event, helper)
    {
        var action = component.get("c.getOpportunityClosedDate");
        var recordId = component.get("v.recordId");
        action.setParams({
            "opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){
            var closedDate = new Date(response.getReturnValue().closedDate);
            var todayDate = new Date();
            var loggedInUser = response.getReturnValue().loggedInUser;
            var QSRMStatus = response.getReturnValue().QSRMStatus;
             var state = response.getState();
            
            if(closedDate.getTime() < todayDate.getTime() && loggedInUser.Profile.Name != 'Genpact Super Admin')
            {
            
                component.set("v.errorMessage", "MSA/SOW closure date cannot be earlier than today.");
                component.set("v.isError", true);
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true);
                component.set("v.isQSRMApproved", false);
                window.setTimeout(
                    $A.getCallback(function() {
                        if((state === "SUCCESS")){
                            component.set("v.isError", false);    
                        }    
                    }),5000
                );
            } 
            else if(QSRMStatus=='Your QSRM is submitted for Approval'){
            
                component.set("v.errorMessage", "Since QSRM is in approval status, you cannot edit the deal.");
                component.set("v.isError", true);
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true);
                component.set("v.isQSRMApproved", false);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(state === "SUCCESS"){
                            component.set("v.isError", false);    
                        }    
                    }),5000
                );
                
            }            
            else{            
            	component.set("v.isQSRMApproved", true);
            }
            //Anjali-Enabling the dropdowns if closure date is greater than today
            if(closedDate.getTime() > todayDate.getTime() && loggedInUser.Profile.Name != 'Genpact Super Admin' && QSRMStatus !='Your QSRM is submitted for Approval')
            {             	
                component.find("ServiceLine").set("v.disabled", false);
                component.find("NatureOfWork").set("v.disabled", false);
                component.find("ProductName").set("v.disabled", false);                
            }
            
            
        });
        $A.enqueueAction(action); 
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message, 
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner         
        component.set("v.Spinner", true); 
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    saveProducts : function(component, event, helper, saveAndNext){
        var opportunityId=component.get("v.recordId");		
        var final_OLI_List=JSON.stringify(component.get("v.final_OLI_List"));
        var action = component.get("c.saveOppLineItems");
        action.setParams(
            {
                'final_OLI_List' :final_OLI_List,
                'oppId':opportunityId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(component.get("v.final_OLI_List").length>0)
                {
                    localStorage.setItem('LatestTabId',"3");
                    // added by aayush to get OLI in Add Pricing Comp                        
                    localStorage.setItem('stopRecursiveUpdate','0');
                    if(saveAndNext)
                    {
                        
                        if(localStorage.getItem('MaxTabId')<4)
                        { 
                            localStorage.setItem('MaxTabId',4);
                            component.set("v.MaxSelectedTabId", "4");
                            component.find("recordViewForm").submit();
                        }
                        localStorage.setItem('LatestTabId',"4");
                        var compEvents = component.getEvent("componentEventFired");
                        compEvents.setParams({ "SelectedTabId" : "4" });
                        compEvents.fire();
                        window.scrollTo(0, 0); 
                         component.set("v.IsSpinner",false);
                    }
                    $A.get('e.force:refreshView').fire();
                    helper.fireToastSuccess(component, event, helper, 'Data Saved Successfully');
                    
                      component.set("v.IsSpinner",false);
                }
                else
                {
                   component.set("v.IsSpinner",false);
                    //helper.hideSpinner(component, event, helper);
                    helper.fireToastError(component, event, helper,'Please add atleast one product to continue.');
                }
                
                
                
            }
            else if(state === "ERROR") { 
                var errors = response.getError();
                var message= errors[0].message;
                 component.set("v.IsSpinner",false);
               // helper.hideSpinner(component, event, helper);
                helper.fireToastError(component, event, helper, response.getError()[0].message);
                
            }
                else
                {        
                    
                    component.set("v.IsSpinner",false);
                    // helper.hideSpinner(component, event, helper);
                    helper.fireToastError(component, event, helper,'Error Contact Salesforce Administrator!');
                    
                }            
        } );
        $A.enqueueAction(action);
    }
    
})