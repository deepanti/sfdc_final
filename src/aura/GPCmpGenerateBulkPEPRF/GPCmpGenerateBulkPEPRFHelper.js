({
    getBusinessNamesHelper : function(component) {        
        var businessNames = component.get("c.getBusinessNames");
        businessNames.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            if(component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var businessNames = [];
                var data = JSON.parse(responseData.response);
                businessNames.push('All');
                
                for(var i=0; i<data.length; i++) {
                    if(data[i] != 'BPM') {
                        businessNames.push(data[i]);
                    }                    
                }
                businessNames.sort();
                component.set("v.listOfBusinessNames", businessNames);
                this.hideSpinner(component);
            }
        });
        $A.enqueueAction(businessNames);
    },
    getApprovedPinnaclePID: function(component, helper, startDate, endDate, selectedStatus, businessNameFilter) {
        this.showSpinner(component);
        
        var getApprovedPinnaclePIDForDuration = component.get("c.getApprovedPinnaclePIDForDuration");
        // DS-16-01-19: Added to refresh the PID lists and master checkbox.
        component.set("v.listOfApprovedPIDsForView", []);
        component.set("v.listOfApprovedPIDs", []);
        component.set("v.masterCheckbox", false);
        
        getApprovedPinnaclePIDForDuration.setParams({
            "startDate": startDate,
            "endDate": endDate,
            "selectedStatus" : selectedStatus,
            "businessName" : businessNameFilter
        });
        getApprovedPinnaclePIDForDuration.setCallback(this, function(response) {
            console.log('==response=='+response);
            this.getApprovedPinnaclePIDForDurationHandler(response, component);
        });
        $A.enqueueAction(getApprovedPinnaclePIDForDuration);
    },
    getApprovedPinnaclePIDForDurationHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setListOfApprovedPIDs(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setListOfApprovedPIDs: function(component, responseData) {
        var listOfApprovedPIDs = JSON.parse(responseData.response)['listOfApprovedProjects'];
        var rowCount = parseInt($A.get("$Label.c.GP_PRF_Report_Count"));
        var tPageCount = 1;
        var tempList = [];        
        
        if(listOfApprovedPIDs.length > 0) {
            var rCount = listOfApprovedPIDs.length > rowCount ? rowCount : listOfApprovedPIDs.length;
            
            for(var i=0; i<rCount; i++) {
                tempList.push(listOfApprovedPIDs[i]);
            }
            
            if(listOfApprovedPIDs.length > 0) {
                if(listOfApprovedPIDs.length > rowCount) {
                    tPageCount = parseInt(listOfApprovedPIDs.length/rowCount);
                    
                    if(tPageCount*rowCount < listOfApprovedPIDs.length) {
                        tPageCount++;
                    }
                }
            }
            
            if(tPageCount == 1) {
                component.set("v.disableNext", true);
            } else {
                component.set("v.disableNext", false);
            }
            
            component.set("v.listOfApprovedPIDsForView", tempList);
            component.set("v.pageNumber", 0);
            component.set("v.totalPageCount", tPageCount);
            component.set("v.listOfApprovedPIDs", listOfApprovedPIDs);
            this.hideSpinner(component);
        }
    },
    downloadPIDReportForSelectedRecords: function(component, listOfApprovedPIDs,selectedStatus) {
        var baseURL = $A.get("$Label.c.GP_PE_PRF_VF_PAGE_BASE_URL");//+ 'https://genpact--pinancle2--c.cs58.visual.force.com/apex/GPGenerateProjectFileEXL?id=';
        var downloadLink = $A.get("$Label.c.GP_Download_URL");
        
        var urlToOpen;
        for (var index = 0; index < listOfApprovedPIDs.length; index++) {
            if (listOfApprovedPIDs[index].isSelected) {
                urlToOpen = listOfApprovedPIDs[index].GP_Content_Version_Document_Id__c != null ? downloadLink + listOfApprovedPIDs[index].GP_Content_Version_Document_Id__c  + '?asPdf=false&operationContext=CHATTER' : baseURL + listOfApprovedPIDs[index].Id +'&selectedStatus='+selectedStatus;
                window.open(urlToOpen , '_blank');
            }
        }
    },
    
    refreshPIDListHelper: function(component, listOfApprovedPIDs) {
        for(var index = 0; index<listOfApprovedPIDs.length;index++) {
            listOfApprovedPIDs[index].isSelected = false;
        }
        component.set('v.listOfApprovedPIDs', listOfApprovedPIDs);
    }
})