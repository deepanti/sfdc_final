({
    doInit : function(component, event, helper) {
        helper.getBusinessNamesHelper(component);
    },
    fetchApprovedPinnaclePIDForDuration: function(component, event, helper) {
        var startDate = component.get("v.startDate");
        var endDate = component.get("v.endDate");
        var selectedStatus = component.get("v.selectedStatus");
        var businessNameFilter = component.get("v.businessNameFilter");
        if (!startDate || !endDate || !selectedStatus || !businessNameFilter) {
            helper.showToast('Error', 'Error', 'Please fill all the inputs.');
            return;
        } else
            helper.getApprovedPinnaclePID(component, helper, startDate, endDate, selectedStatus, businessNameFilter);
    },
    downloadPIDReport: function(component, event, helper) {
        
        // var listOfApprovedPIDs = component.get("v.listOfApprovedPIDs");
        var listOfApprovedPIDsForView = component.get('v.listOfApprovedPIDsForView') || [];
        var oneSelected = false;
        var selectedStatus = component.get("v.selectedStatus");
        
        for (var index = 0; index < listOfApprovedPIDsForView.length; index++) {
            if (listOfApprovedPIDsForView[index].isSelected) {
                oneSelected = true;
                break;
            }
        }

        if (oneSelected) {
            helper.downloadPIDReportForSelectedRecords(component, listOfApprovedPIDsForView, selectedStatus);
        } else {
            helper.showToast('Error', 'Error', 'Please select atleast one record.');
            return;
        }
    },
    selectAllRecords: function(component, event, helper) {
        var listOfApprovedPIDs = component.get('v.listOfApprovedPIDs') || [];
        var listOfApprovedPIDsForView = component.get('v.listOfApprovedPIDsForView') || [];
        var masterCheckbox = component.get("v.masterCheckbox");
        
        helper.refreshPIDListHelper(component, listOfApprovedPIDs);
        
        for(var index = 0; index< listOfApprovedPIDsForView.length;index++){
            listOfApprovedPIDsForView[index].isSelected = masterCheckbox;
        }
        component.set("v.listOfApprovedPIDsForView", listOfApprovedPIDsForView);
    },
    
    previousView : function(component, event, helper) {
        var listOfApprovedPIDs = component.get('v.listOfApprovedPIDs') || [];
        component.set("v.masterCheckbox", false);
        helper.refreshPIDListHelper(component, listOfApprovedPIDs);
        
        if(listOfApprovedPIDs.length > 0) {
            var pageNumber = component.get("v.pageNumber");
            var rowCount = parseInt($A.get("$Label.c.GP_PRF_Report_Count"));
            pageNumber -= 1;
            component.set("v.listOfApprovedPIDsForView",listOfApprovedPIDs.slice(pageNumber*rowCount, (pageNumber+1)*rowCount));
            component.set("v.pageNumber", pageNumber);
            component.set("v.disableNext", false);
        }
    },
    
    nextView : function(component, event, helper) {
        var listOfApprovedPIDs = component.get('v.listOfApprovedPIDs') || [];
        component.set("v.masterCheckbox", false);
        helper.refreshPIDListHelper(component, listOfApprovedPIDs);
        
        if(listOfApprovedPIDs.length > 0) {
            var pageNumber = component.get("v.pageNumber");
            var rowCount = parseInt($A.get("$Label.c.GP_PRF_Report_Count"));
            
            component.set("v.listOfApprovedPIDsForView",listOfApprovedPIDs.slice((pageNumber+1)*rowCount, (pageNumber+2)*rowCount));
            component.set("v.pageNumber", pageNumber+1);
            
            if(component.get("v.pageNumber")+1 == component.get('v.totalPageCount')) {
                component.set("v.disableNext", true);
            }
        }
    }
})