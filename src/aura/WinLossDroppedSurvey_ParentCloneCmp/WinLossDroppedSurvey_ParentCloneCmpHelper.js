({
    getWinSurvey1Values : function(component)
   	 {
       // alert('=====get("v.OpportunityList")====helper=='+component.get("v.OpportunityList"));  
        var action = component.get("c.getWinSurvey1");
        action.setParams({
            "oppList" : component.get("v.OpportunityList"),           
        });
        action.setCallback(this, function(response)
         {  
            // alert('=====load data==helper===getWinSurvey1Values==='+JSON.stringify(response.getReturnValue()));
             var check_IsSurvey89 = component.get("v.IsSurvey89");	
             component.set("v.reasonMap", response.getReturnValue());
            // alert('======1=======');
             var keyList = [];
           //  alert('======2=======');
             
             var reasonMap = response.getReturnValue();
            // alert('======3=======');
             //alert("response.getReturnValue()====3.1="+JSON.stringify(response.getReturnValue()));
             // alert('===reasonMap===='+reasonMap);
             for (var singlekey in reasonMap) 
              {
                 // alert(singlekey + '=====================:' +reasonMap[singlekey]);
                  var keyJson = {
                 	"label" : singlekey,
                      "ranking" : reasonMap[singlekey],
                      "IsSurvey89" : check_IsSurvey89,
                      "comment" : ""
            	 };
                  keyList.push(keyJson);
                  //alert('=====keyList======'+keyList);
                  
                  component.set("v.keyList",keyList);
                 // alert('=====component.get("v.keyList")======'+component.get("v.keyList"));
              }
         });       
        $A.enqueueAction(action);
     },
	get_q3q4q5_val : function(component)
   	 {
        var action = component.get("c.getIncumbent");
        action.setCallback(this, function(response)
         {            
            component.set("v.Incumbent", response.getReturnValue());           
         });
        $A.enqueueAction(action);
		
		var action1 = component.get("c.getGenpact_Involvement_in_deal_with_prospec");
        action1.setCallback(this, function(response)
         {            
            component.set("v.Q4_G_invlvmnt", response.getReturnValue());           
         });
        $A.enqueueAction(action1);
		
		var action2 = component.get("c.getinteractions_did_we_have_with_the_prospe");
        action2.setCallback(this, function(response)
         {            
            component.set("v.Q5_Interaction_w_Prospct", response.getReturnValue());           
         });
        $A.enqueueAction(action2);
    } ,
    getClosestCompetitor : function(component)
   	 {
        var action = component.get("c.getClosestCompetitor");
        action.setCallback(this, function(response)
         {            
            component.set("v.ClosestCompetitor", response.getReturnValue());           
         });
        $A.enqueueAction(action);
    },
    getClosestCompetitor1 : function(component)
   	 {
        var action = component.get("c.getClosestCompetitor1");
        action.setCallback(this, function(response)
         {            
            component.set("v.ClosestCompetitor1", response.getReturnValue());  
            component.set("v.ClosestCompetitor2", response.getReturnValue());
         });
        $A.enqueueAction(action);
    }
    
    
})