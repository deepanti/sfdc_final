({
	doInit : function(component,event,helper) 
    { 
        ('WinLossDroppedSurvey_ParentCloneCmp======load=====');
        var recordId = component.get("v.recordId"); 
        var action = component.get("c.getOpportunityData");
        var oppReason = component.find("oppReasons");
        var opts=[];
       action.setParams({
            "opportunityID" : recordId,           
        });
        action.setCallback(this, function(response){  
            
            if(response.getState() == 'SUCCESS'){                
                var oppResults = response.getReturnValue();
               // alert('======get old opp data====='+JSON.stringify(oppResults));
                component.set("v.OpportunityList",oppResults);
				
				var OppWrapperList11 = [];            
                OppWrapperList11 = component.get("v.OpportunityList");                  
            	var ClosestCometitor; 
                var KeyWinReasons_Learning;  
                 var SurveyNumber;
                    for(var i=0;i<OppWrapperList11.length;i=i+1)
                    {                          
                        var test1=OppWrapperList11[i];                      
                        var jsnParse= JSON.parse(JSON.stringify(test1));                        
                       // alert('=====3==='+JSON.stringify(test1));
                       ClosestCometitor= jsnParse.Closest_Competitor__c;
                       KeyWinReasons_Learning = jsnParse.key_win_reasons_and_any_key_learnings__c;
                       SurveyNumber = jsnParse.SurveyNumber__c;                    
                    }
                component.set("v.SClosestCometitor",ClosestCometitor);
                component.find("ClosestCompetitor").set("v.value",ClosestCometitor);
                component.find("comments").set("v.value",KeyWinReasons_Learning);
                component.set("v.KeyWinReasonLearning",KeyWinReasons_Learning);
               // alert('==========='+SurveyNumber);
                if(SurveyNumber == 'Survey8' || SurveyNumber == 'Survey9')
                {
                   // alert('=====If survey=====');
                    component.set("v.IsSurvey89",true);
                } 
                else
                {
                   // alert('=====else survey=====');
                    component.set("v.IsSurvey89",false);
                }                
                
                //alert('=====helper==getWinSurvey1Values==start===');
                helper.getWinSurvey1Values(component);
                //alert('=====helper==getWinSurvey1Values==end===');
                
                var Opp_NamedCompetitor;
                var Opp_Name;
                var Opp_Source;
                var OppSource;
                var Opp_DealType;
                var Comp_Presence;
                var Opp_TCV;
                var Opp_NatureOfWork;          
                var Reason_1;
                var Reason_2;
                var Reason_3;
                var selectedClosestCompetitor;
                
                Opp_NamedCompetitor= oppResults[0].Competitor__c;
                Opp_Name= oppResults[0].Name;
                Opp_Source= oppResults[0].Target_Source__c;  
                OppSource = oppResults[0].Opportunity_Source__c;  
                Opp_DealType= oppResults[0].Annuity_Project__c;
                Comp_Presence = oppResults[0].Deal_Type__c;
                Opp_TCV = oppResults[0].TCV1__c;
                Opp_NatureOfWork = oppResults[0].Nature_Of_Work_OLI__c ;
                Reason_1 = oppResults[0].Reason_1__c ;
                Reason_2 = oppResults[0].Reason_2__c;
                Reason_3 = oppResults[0].Reason_3__c;
                selectedClosestCompetitor = oppResults[0].Closest_Competitor__c;
                //alert('--1-- '+Reason_1 + ' --2-- ' +Reason_2 + ' --3-- '+Reason_3);
                
                component.set("v.Opp_Source",Opp_Source);
                component.set("v.OppSource",OppSource);
                component.set("v.Opp_DealType",Opp_DealType);
                component.set("v.Opp_Name",Opp_Name);
                component.set("v.Comp_Presence",Comp_Presence);
                component.set("v.Opp_TCV",Opp_TCV);
                component.set("v.Opp_NatureOfWork",Opp_NatureOfWork);
                
                helper.get_q3q4q5_val(component); 
                helper.getClosestCompetitor(component);  
				helper.getClosestCompetitor1(component); 
                component.set("v.selectedClosestCompetitor",selectedClosestCompetitor);
                
               // alert('=====selectedClosestCompetitor====='+selectedClosestCompetitor);
                if(OppSource == 'Renewal') 
                {
                    component.set("v.IsSurvey1",true);
                }        
                else if(OppSource == 'IT Non-Annuity Extensions' || OppSource == 'Ramp Up' || Opp_DealType == 'Staff Aug')
                {
                    component.set("v.IsSurvey2",true);
                }
                else if(Comp_Presence == 'Sole Sourced')
                {
                    if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 )
                    {
                        if(Opp_TCV >= 1000000 )
                        {
                            //alert('======sole sourced====in===1=======');				
                            component.set("v.IsSurvey3",true);
                        }
                        else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000)
                        {
                            //alert('======sole sourced====in==else======1=======');			
                            component.set("v.IsSurvey3",true);							
                        }
                        else
                        {
                            //alert('========sole sourced=======else===');
                            component.set("v.IsSurveyElseCon",true);
                            component.set("v.IsSurvey4",false);
                            component.set("v.IsSurvey3",false);
                            component.set("v.IsSurvey2",false);
                            component.set("v.IsSurvey1",false);
                        }
                    }
                    else
                    {
                        //alert('========sole sourced====final===else===');
                        component.set("v.IsSurveyElseCon",true);
                        component.set("v.IsSurvey4",false);
                        component.set("v.IsSurvey3",false);
                        component.set("v.IsSurvey2",false);
                        component.set("v.IsSurvey1",false);
                    }
                }
                else if(Comp_Presence == 'Competitive')
                {	
                    if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 )
                    {
                        
                        if(Opp_TCV >= 1000000 )
                        {
                            //alert('======Competitive====if=====1======='+Opp_TCV);
                            component.set("v.IsSurvey4",true);				
                        }
                        else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000 )
                        {
                            //alert('======Competitive======else=====1=======');				
                            component.set("v.IsSurvey4",true);				
                        }	
                        else
                        {
                            //alert('========competitive=======else===');
                            component.set("v.IsSurveyElseCon",true);
                            component.set("v.IsSurvey4",false);
                            component.set("v.IsSurvey3",false);
                            component.set("v.IsSurvey2",false);
                            component.set("v.IsSurvey1",false);
                        }
                    }
                    else
                    {
                        //alert('========competitive====final===else===');
                            component.set("v.IsSurveyElseCon",true);
                            component.set("v.IsSurvey4",false);
                            component.set("v.IsSurvey3",false);
                            component.set("v.IsSurvey2",false);
                            component.set("v.IsSurvey1",false);
                    }			
                }
                else
                {
                    //alert('=========final ======else======');
                    component.set("v.IsSurveyElseCon",true);
                    component.set("v.IsSurvey4",false);
                    component.set("v.IsSurvey3",false);
                    component.set("v.IsSurvey2",false);
                    component.set("v.IsSurvey1",false);
                }
                
            }
         })  
       
        $A.enqueueAction(action);
         
    },
	
	
	enabled_Closest_CompititorQ4_other:function(cmp,evt) //for closest competitor q2
    {	
		//alert('======closest competitor======');
    	var ClosestCompetitorOthersQ4 = cmp.get("v.selectedClosestCompetitor");
		//alert('ClosestCompetitorOthersQ4======'+ClosestCompetitorOthersQ4);
		if(ClosestCompetitorOthersQ4 == 'Others' || ClosestCompetitorOthersQ4.includes('Others') ) 
		{
			//alert('======if==onchange======');
			cmp.set("v.If_Closest_Competitors_other",false);
		//	alert(cmp.find("ClosestCompetitors_other").get("v.value") );
			cmp.set("v.IsClosestCompetitorFlag",false);			
		}
		else
		{
			//alert('======else==onchange======');
			cmp.find("ClosestCompetitors_other").set("v.value", "");
			cmp.set("v.If_Closest_Competitors_other",true); 
			cmp.set("v.IsClosestCompetitorFlag",true);
			cmp.set("v.selectedClosestCompetitors1",ClosestCompetitorOthersQ4); 
			//alert('=====selectedClosestCompetitors1===final==on change picklist==='+ cmp.get("v.selectedClosestCompetitors1"));
		} 
		//alert('IsClosestCompetitorFlag======end==================='+ cmp.get("v.IsClosestCompetitorFlag"));
	},
	handleBlur_ClosestCompetitorsOthers : function(cmp, event)
	{
		//alert('========handle blure=====');
		cmp.set("v.selectedClosestCompetitors1",cmp.find("ClosestCompetitors_other").get("v.value")); 		
	},
	 
    handleClick:function(cmp,evt)
    {
        //alert('Inside handleClick== '+JSON.stringify(cmp.get("v.keyList")));
		cmp.set("v.isSuccess", true);
        try
		{
			var surveyList = cmp.get("v.keyList");
			var isRankingMissing = false;
			var isRankingDuplicate = false;
			var UnickCoutn=[];
			var checkDublicate;
			for(var index in surveyList)
			{
				var survey = surveyList[index];
				//alert('survey=='+JSON.stringify(survey));
				console.log('====check=='+JSON.stringify(survey));
				var comment = survey.comment;
				//alert('=====comment====='+comment);
				var ranking = survey.ranking;
				//alert('=====ranking====='+ranking);
				if(ranking > 0)
				{
					  UnickCoutn.push(ranking);				 
				}
				else
				{
					//alert('====else====');
				   // UnickCoutn = ranking;
				   //UnickCoutn.push(ranking);
				}
				var rankstring = ranking.toString();   				
				
				if((!comment && ranking!='0') || (!comment && ranking=='0') || (comment && ranking!='0')){
					isRankingMissing = false;
				   // alert('isRankingMissing if=='+isRankingMissing);               
				}
				else if(comment && ranking =='0'){
					isRankingMissing = true;
					// alert('isRankingMissing else if=='+isRankingMissing);
					 break;
				}
			}
			var sorted_UnickCoutn = UnickCoutn.slice().sort(); 
			//alert('===sorted_UnickCoutn====='+sorted_UnickCoutn);
			var results = [];
				
			for (var i = 0; i < sorted_UnickCoutn.length - 1; i++) 
			{
				//alert('===sorted_UnickCoutn[i + 1]===='+sorted_UnickCoutn[i + 1] +'===sorted_UnickCoutn[i]==='+sorted_UnickCoutn[i]);
				if (sorted_UnickCoutn[i + 1] == sorted_UnickCoutn[i]) 
				{
					results.push(sorted_UnickCoutn[i]);
				}
			}
			//alert('=====results======'+results);
			if(results.length > 0)
			{		
				//alert('=====if duplicate');
				isRankingDuplicate = true;
				//break;
			}
			else
			{
				//alert('=====else duplicate');
				isRankingDuplicate = false;
			}
                      
		var checkClosestCompetitorQ2 = cmp.get("v.selectedClosestCompetitor");			
        var LongArea = cmp.find("comments");
     	var LongAreaVal = LongArea.get("v.value");
		var checkClosestCompetitorOthers = cmp.find("ClosestCompetitors_other");
		var checkClosestCompetitorOthersVal = checkClosestCompetitorOthers.get("v.value");
		//alert('=====LongAreaVal====='+LongAreaVal);
		var KeyWinReasonsComment;
		var ClosestCompetitorOthers;
		if(LongAreaVal == 'undefined' || LongAreaVal == null ||LongAreaVal == '' )
		{
			//alert('======if comments==');
			KeyWinReasonsComment = '';
		}
		else
		{
			//alert('======else comments==');
			KeyWinReasonsComment = LongAreaVal;
		}		
		if(checkClosestCompetitorOthersVal == 'undefined' || checkClosestCompetitorOthersVal == null ||checkClosestCompetitorOthersVal == '' )
		{
			//alert('======if compe==');
			ClosestCompetitorOthers = '';
		}
		else
		{
			//alert('======else compe==');
			ClosestCompetitorOthers = checkClosestCompetitorOthersVal;
		}		
		//alert('=====checkClosestCompetitorOthersVal====='+checkClosestCompetitorOthersVal);		
		
        if(!isRankingMissing && !isRankingDuplicate)
        {
           // alert('=====save start=======');
            var action = cmp.get("c.saveWLRClone");           
			action.setParams({                             
                "oppId": cmp.get("v.recordId"),	
				"ClosestCompetitor": cmp.get("v.selectedClosestCompetitor"),
				"ClosestCompetitorOthers": ClosestCompetitorOthers,
				"KeyWinReasons_Learning": KeyWinReasonsComment,				
                "OppSurvey": JSON.stringify(cmp.get("v.keyList"))
			});        
			action.setCallback(this, function(response) 
             {
                var state = response.getState();
                //alert('state======1===='+JSON.stringify(response)+"==returnedValue=="+response.getReturnValue()+"===state=="+state);
				if(cmp.isValid() && state === "SUCCESS") 
                {
                   // alert('====save success==');
					cmp.set("v.isSuccess", false);	
					var recordId = cmp.get("v.recordId");  
					function isLightningExperienceOrSalesforce1() 
					{
						//alert('======1=====');
						return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
					} 
					if(isLightningExperienceOrSalesforce1()) 
					{                       
						// alert('======2=====');
						sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
					}
					else
					{
						// alert('======3=====');
						window.location = '/'+recordId;   //'/006/o'
					}  
                }
                else if (state === "ERROR"){
                    console.log('Problem saving WLR, response state: ' + state);
                 	//alert('error ='+ state );
                }
                else{
                    console.log('Unknown problem, response state: ' + state);
                   // alert('in save');
                }         
          });           
			$A.enqueueAction(action);              
        }
        else{
            alert("Ranking is missing. or Ranking value is duplicate'");
            cmp.set("v.isSuccess", false);
        }
        }catch(err) {
            //alert(':===error msg===:'+err.name);
        }
    },
    handleCancel: function(component, event, helper) 
    {           	
         	var recordId = component.get("v.recordId");      
			function isLightningExperienceOrSalesforce1() 
			 {
				return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
			} 
			if(isLightningExperienceOrSalesforce1()) 
			{   
				sforce.one.navigateToSObject(recordId,"OPPORTUNITY");               
			}
			else
			{
				window.location = '/'+recordId;   //'/006/o'
			} 
    },
    
    reset: function(cmp,evt)
	{	
      
	}	
})