({
	trackHomePageVisit:function(component, event, helper)
    {
        var action = component.get("c.trackHomePage");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
             
            }
            else
            {
                 helper.showToast('error','Something went wrong, please try again.')
               // this.fireToastError(component, event, helper,response.getError()[0].message);
             
            }
            
        });
        $A.enqueueAction(action); 
    },
     showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})