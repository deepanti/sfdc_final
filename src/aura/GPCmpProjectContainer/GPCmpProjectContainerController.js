({
    
    /**
     * Handle Component Initialization event.
     * @param {Aura.Component} component - this component
     * @description Fetch the map of accessibility for project and its related records
     * along with validation error.
     */
     doInit: function(component, event, helper) {
         helper.displayApplication(component);
         var displayApplication = component.get("v.displayApplication");
         if(displayApplication){
             var validateApproval = event.getParam("validateApproval");
             
             helper.getProjectContainerData(component, validateApproval);
         }
    },
    /**
     * Handle Approval Validation Update Handler.
     * @param {Aura.Component} component - this component
     * @description Parse the validation error message and show warning message in corresponding sections.
     */
    approvalValidationUpdateHandler: function(component, event, helper) {
        var validationMessage = event.getParam("validationMessage");
        var projectData = component.get("v.projectData") || {};
        var errorLog;

        try {
            errorLog = JSON.parse(validationMessage) || {};
        } catch (error) {
            errorLog = {};
        }

        var listOfFormattedError = helper.getFormattedListOfError(errorLog);
        errorLog = helper.getSanatizedMapOfError(errorLog);

        component.set("v.mapOfErrorLog", errorLog);
        component.set("v.errorLog", listOfFormattedError);
    },
    setSelectedTabId: function(component,event,helper){
        var selectedTab = event.currentTarget.getAttribute("id");
        component.set("v.selectedTabId",selectedTab);
    }
})