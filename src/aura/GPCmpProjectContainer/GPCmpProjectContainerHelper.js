({
    getProjectContainerData: function(component, validateApproval) {
        //for now commented real time validation.
        this.showSpinner(component);
        validateApproval = false; //Boolean(validateApproval);
        if (validateApproval) {
            component.set("v.isValidating", true);
        }
        var projectData = component.get("v.projectData") || {};
        var recordId = projectData["Id"];

        if (!recordId) {
            recordId = component.get("v.recordId");
        }

        var getProjectContainerStatus = component.get("c.getProjectContainerStatus");
        getProjectContainerStatus.setParams({
            "projectId": recordId,
            "validateApproval": Boolean(validateApproval)
        });

        getProjectContainerStatus.setCallback(this, function(response) {
            this.getProjectContainerStatusHandler(response, component);
        });

        $A.enqueueAction(getProjectContainerStatus);
    },
    getProjectContainerStatusHandler: function(response, component) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectContainerData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProjectContainerData: function(component, responseData) {

        var responseJson = JSON.parse(responseData.response);
        var expenseStatus = responseJson.expenseStatus;
        var budgetStatus = responseJson.budgetStatus;
        var budgetPricingAndExpenseStatus;

        if (responseJson.projectStatus !== 'approved') {
            budgetPricingAndExpenseStatus = (expenseStatus === 'unTouched' ||
                budgetStatus === 'unTouched') ? 'unTouched' : 'dirty';
        } else {
            budgetPricingAndExpenseStatus = 'approved';
        }

        component.set("v.projectStatus", responseJson.projectStatus);
        component.set("v.workLocationStatus", responseJson.workLocationStatus);
        component.set("v.leadershipStatus", responseJson.leadershipStatus);
        component.set("v.billingMilestoneStatus", responseJson.billingMilestoneStatus);
        component.set("v.budgetPricingAndExpenseStatus", budgetPricingAndExpenseStatus);
        component.set("v.documentUploadStatus", responseJson.documentUploadStatus);
        component.set("v.resourceAllocationStatus", responseJson.resourceAllocationStatus);

        //Added
        component.set("v.budgetPricingStatus", budgetStatus);

        component.set("v.projectData", responseJson.project);
        var serializedErrorLog = responseJson.project.GP_Approval_Error__c;
        var errorLog;

        try {
            errorLog = JSON.parse(serializedErrorLog) || {};
        } catch (error) {
            errorLog = {};
        }

        var listOfFormattedError = this.getFormattedListOfError(errorLog);

        errorLog = this.getSanatizedMapOfError(errorLog);

        component.set("v.errorLog", listOfFormattedError);

        component.set("v.mapOfErrorLog", errorLog);

        component.set("v.isValidating", false);

        var mapOfRoleAccessibility = this.getSanatizedRoleAccessibility(responseJson.mapOfRoleAccessibility);

        console.log("mapOfRoleAccessibility: ", mapOfRoleAccessibility);

        component.set("v.mapOfRoleAccessibility", mapOfRoleAccessibility);
        this.hideSpinner(component);
    },
    getSanatizedMapOfError: function(errorLog) {
        var mapOfSectionToSubSectionNameToListOfErrors = errorLog.mapOfSectionToSubSectionNameToListOfErrors;
        var sanatizedMap = {};

        for (var sectionName in mapOfSectionToSubSectionNameToListOfErrors) {
            var sanatizedsectionName = this.replaceAll(sectionName, " ", "");
            var sanatizedMapOfSubSectionNameToSubSection = {};
            for (var subSectionName in mapOfSectionToSubSectionNameToListOfErrors[sectionName]) {
                var sanatizedSubSectionName = this.replaceAll(subSectionName, " ", "");
                sanatizedMapOfSubSectionNameToSubSection[sanatizedSubSectionName] = mapOfSectionToSubSectionNameToListOfErrors[sectionName][subSectionName];
            }
            sanatizedMap[sanatizedsectionName] = sanatizedMapOfSubSectionNameToSubSection;
        }
        return sanatizedMap;
    },
    getSanatizedRoleAccessibility: function(mapOfRoleAccessibility) {

        var sanatizedRoleAccessibility = {};
        for (var key in mapOfRoleAccessibility) {
            var sanatizedKey = this.replaceAll(key, " ", "");
            sanatizedRoleAccessibility[sanatizedKey] = mapOfRoleAccessibility[key];
        }
        return sanatizedRoleAccessibility;
    },
    replaceAll: function(string, search, replacement) {
        return string.replace(new RegExp(search, 'g'), replacement);
    }
})