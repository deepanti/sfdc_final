({
    
  /*  doInit: function(component, event, helper) { 
         //helper.OliRecord(component);
        //var oppId=component.get("v.recordId");
        var action = component.get('c.getOpportunityOLI');
        //action.setParams({"oppId":oppId});
        action.setCallback(this, function(actionResult) {
        console.log('Opportunity+++',actionResult.getReturnValue());
       if(actionResult.getState()=='SUCCESS') { 
        component.set('v.opportunityList', actionResult.getReturnValue());
       }
     });
        
        $A.enqueueAction(action);
    
    },
    
    */
    
    
	/*doInit: function(component, event, helper) { 
        //var oppId=component.get("v.recordId");
        var action = component.get('c.getOpportunityOLI');
        //action.setParams({"oppId":oppId});
        action.setCallback(this, function(actionResult) {
        console.log('state+++',actionResult.getReturnValue());
       if(actionResult.getState()=='SUCCESS') { 
        component.set('v.opportunityOLIList', actionResult.getReturnValue());
       }
     });
        
        $A.enqueueAction(action);
    
    },
    */
    
    
    
    
    toggleOppProdItems : function(component, event, helper) {
		var isOppProdOpen = component.get('v.isOppProdOpen');
        console.log('isOppProdOpen: ', isOppProdOpen);
        component.set('v.isOppProdOpen', !isOppProdOpen);
        
	},
    
    showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    selectAllOppLI : function(component, event, helper){
        var oppwrap = component.get("v.oppwrap");
        var checkBoxStatus = oppwrap.oppSelected;
        var opportunityLIWrapperObj = oppwrap.opportunityLIWrapperObj;
        
        for(var i=0; i < opportunityLIWrapperObj.length; i += 1) {
            opportunityLIWrapperObj[i].oppLISelected = checkBoxStatus;
        }
        
        oppwrap.opportunityLIWrapperObj = opportunityLIWrapperObj;
        component.set("v.oppwrap", oppwrap);
    }
    
})