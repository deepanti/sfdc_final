({
    redirectToURL : function(component, event, helper) {
        //Redirect to the report details
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": component.get("v.url")
        });
        navEvt.fire();
    }
})