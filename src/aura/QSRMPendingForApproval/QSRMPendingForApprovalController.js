({
    doInit : function(component, event, helper) {
        helper.getCurrentUser(component,event,helper);
        helper.getUserSpecificPendingsRecord(component,event,helper);
        helper.getProcessInstancePendingRecordsForSL(component,event,helper);
    },     
    
    navigateToApprove : function(component, event, helper){
        var recID = event.getSource().get("v.name");
        component.set("v.recordId",recID);
        var actionQSRM = component.get("c.getQSRMObjMethod");
        actionQSRM.setParams({
            "QsrmID" : recID
        });
        actionQSRM.setCallback(this, function(actionResult) {
            component.set('v.QSRMObject', actionResult.getReturnValue());
            console.log(actionResult.getReturnValue());
            //helper.goTop(component,event,helper);
            component.set("v.approvePopup",true);
        });
        $A.enqueueAction(actionQSRM);
        
    },
    navigateToReject : function(component, event, helper){
        var recID = event.getSource().get("v.name");
        component.set("v.recordId",recID);
        var actionQSRM = component.get("c.getQSRMObjMethod");
        actionQSRM.setParams({
            "QsrmID" : recID
        });
        actionQSRM.setCallback(this, function(actionResult) {
            component.set('v.QSRMObject', actionResult.getReturnValue());
            console.log(actionResult.getReturnValue());
            component.set("v.rejectPopup",true);
        });
        $A.enqueueAction(actionQSRM);
        
    },
    navigateToRework : function(component, event, helper){
        var recID = event.getSource().get("v.name");
        component.set("v.recordId",recID);
        var actionQSRM = component.get("c.getQSRMObjMethod");
        actionQSRM.setParams({
            "QsrmID" : recID
        });
        actionQSRM.setCallback(this, function(actionResult) {
            component.set('v.QSRMObject', actionResult.getReturnValue());
            console.log(actionResult.getReturnValue());
            component.set("v.reworkPopup",true);
        });
        $A.enqueueAction(actionQSRM);
    },
    navigateToViewProduct : function(component, event, helper){
        var recID = event.getSource().get("v.name");
        var action=component.get("c.getAllProductForQsrm");
        action.setParams({"qsrmId": recID});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.OppProduct", response.getReturnValue());
                component.set("v.OppListSize", (component.get("v.OppProduct")).length);
                var scrollOptions = {
                    left: 0,
                    top: 0,
                    behavior: 'smooth'
                }
                window.scrollTo(scrollOptions);
                helper.goTop(component,event,helper);
                component.set("v.productPopup",true);
                helper.goTop(component,event,helper);
            } else {
                console.log('Problem getting OppProduct, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    gotoQsrmRecod : function (component, event,helper) {
        var qsrmID = event.getSource().get("v.name");
        var urlEvent = $A.get("e.force:navigateToSObject");
        urlEvent.setParams({
            "recordId":qsrmID
        });
        urlEvent.fire();
    },
    
    QSRMEditRecord : function(component, event, helper){
        var qsrmID = event.getSource().get("v.name");
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": qsrmID
        });
        editRecordEvent.fire();
    }, 
    
    gotoOpptyRecord : function(component,event,helper){
        var opptyID = event.getSource().get("v.name");
        var urlEvent = $A.get("e.force:navigateToSObject");
        urlEvent.setParams({
            "recordId":opptyID
        });
        urlEvent.fire();
    },
    
    fullScreen : function(component,event,helper){
        component.set("v.fullScreen",false)
    },
    
    closePopup : function(component,event,helper){
        component.set("v.approvePopup",false);
        component.set("v.rejectPopup",false);
        component.set("v.productPopup",false);
        component.set("v.reworkPopup",false);
    },
    
    approveMethod: function(component,event,helper){
        var recID = component.get("v.recordId");
        var commentsEntered = component.find("commentsId").get("v.value");
        if(commentsEntered  != null && commentsEntered != '' && !commentsEntered.trim().length < 1){
            var action = component.get("c.approverActionMethod");
            action.setParams({ "QsrmID" : recID, "Action" : "Approve","commentsData" : commentsEntered});
            var self = this;
            action.setCallback(this, function(actionResult) {
                if(actionResult.getState()=="SUCCESS"){
                    $A.get('e.force:refreshView').fire();
                    component.set("v.approvePopup",false);
                    helper.showSuccessToast(component,event,helper);
                }
                else{
                    helper.showAdminToast(component,event,helper);
                    component.set("v.approvePopup",false);
                }
            });
            $A.enqueueAction(action);
        }else{
            helper.showCommentToast(component,event,helper);
        }
    },
    
    rejectMethod: function(component,event,helper){
        var recID = component.get("v.recordId");
        var commentsEntered = component.find("commentsId").get("v.value");
        if(commentsEntered  != null && commentsEntered != '' && !commentsEntered.trim().length < 1){
            var action = component.get("c.approverActionMethod");
            action.setParams({ "QsrmID" : recID, "Action" : "Reject","commentsData" : commentsEntered});
            action.setCallback(this, function(actionResult) {
                if(actionResult.getState()=="SUCCESS"){
                    alert('success');
                    $A.get('e.force:refreshView').fire();
                    component.set("v.rejectPopup",false);
                    helper.showSuccessToast(component,event,helper);
                }
                else{
                    helper.showAdminToast(component,event,helper);
                    component.set("v.rejectPopup",false);
                }
            });
            $A.enqueueAction(action);
        }
        else{
            helper.showCommentToast(component,event,helper);
        }
    },
    
    reviewMethod: function(component,event,helper){
        var recID = component.get("v.recordId");
        var commentsEntered = component.find("commentsId").get("v.value");
        if(commentsEntered  != null && commentsEntered != '' && !commentsEntered.trim().length < 1){
            var action = component.get("c.approverActionMethod");
            action.setParams({ "QsrmID" : recID, "Action" : "Removed","commentsData" : commentsEntered});
            action.setCallback(this, function(actionResult) {
                if(actionResult.getState()=="SUCCESS"){
                    $A.get('e.force:refreshView').fire();
                    component.set("v.reworkPopup",false);
                    helper.showSuccessToast(component,event,helper);
                }
                else{
                    helper.showAdminToast(component,event,helper);
                    component.set("v.reworkPopup",false);
                }
            });
            $A.enqueueAction(action);
            
        }else{
            helper.showCommentToast(component,event,helper);
        }
    },
    
})