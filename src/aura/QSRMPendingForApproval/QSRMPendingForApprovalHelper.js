({
    getProcessInstancePendingRecordsForSL : function(component,event,helper){
        var action=component.get('c.getProcessInstancePendingmethodHomePage');
        action.setParams({"checkForHomePage" : "Nothomapage"});
        action.setCallback(this, function(actionResult) {
            component.set('v.ProcessInstanceNodelist',actionResult.getReturnValue());    
            this.getProcessInstancePendingRecordsForSLHome(component,event,helper);
        });
        $A.enqueueAction(action);
    },
    
    getProcessInstancePendingRecordsForSLHome: function(component,event,helper){
        var action=component.get('c.getProcessInstancePendingmethodHomePage');
        action.setParams({"checkForHomePage" : "homepage"});
        action.setCallback(this, function(actionResult) {
            component.set('v.ProcessInstanceNodelistHomePage',actionResult.getReturnValue());   
        });
        $A.enqueueAction(action);
    },
    
    getUserSpecificPendingsRecord : function(component,event,helper){
        var action=component.get('c.getProcessInstanceReworkmethodHomePageSalesRep');
        action.setParams({"checkForHomePage" : "notHomePage"});
        action.setCallback(this, function(actionResult) {
            component.set('v.ProcessInstanceslistForReworkSalesRep',actionResult.getReturnValue());    
            this.getUserSpecificHomePendingsRecords(component,event,helper);
        });
        $A.enqueueAction(action);
    },
    
    getUserSpecificHomePendingsRecords : function(component,event,helper){
        var action=component.get('c.getProcessInstanceReworkmethodHomePageSalesRep');
        action.setParams({"checkForHomePage" : "homePage"});
        action.setCallback(this, function(actionResult) {
            component.set('v.ProcessInstanceslistForReworkHomeSalesRep',actionResult.getReturnValue());   
        });
        $A.enqueueAction(action);
    },
    
    getCurrentUser: function(component,event,helper){
        var action=component.get('c.getCurrentLogInUser');
        var self=this;
        action.setCallback(this,function(actionResult){
            component.set('v.userObject', actionResult.getReturnValue());
            var profileName= component.get('v.userObject.Profile.Name');
            component.set('v.UserType' , profileName);
        });
        $A.enqueueAction(action);
    },
    
    showCommentToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message:',
            message:'Please Enter Appropriate Comments',
            messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    showAdminToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message:',
            message:'Please Contact System Admin',
            messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
            duration:' 5000',
            key: 'info_alt',
            type: 'error',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    showSuccessToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Success Message',
            message: 'Record Updated Succesfully',
            messageTemplate: 'Mode is pester ,duration is 5sec and Message is overrriden',
            duration:' 5000',
            key: 'info_alt',
            type: 'success',
            mode: 'pester'
        });
        toastEvent.fire();
    },
    
    goTop : function(component,event,helper){
        document.body.scrollTop = 0;
  		document.documentElement.scrollTop = 0;
    }
})