({
    doInit: function(component, event, helper) {
        helper.getLeaderShipData(component);
    },
    addToRegionalProjectLeadership: function(component, event, helper) {
        helper.addToRegionalProjectLeadership(component);
        helper.updateLeadershipProgressStatus(component);
    },
    addToHSLProjectLeadership: function(component, event, helper) {
        helper.addToHSLProjectLeadership(component);
        helper.updateLeadershipProgressStatus(component);
    },
    addToAdditionalAccessProjectLeadership: function(component, event, helper) {
        helper.addToAdditionalAccessProjectLeadership(component);
        helper.updateLeadershipProgressStatus(component);
    },
    removeRegionalProjectLeadership: function(component, event, helper) {
        if (component.get("v.target.fields.GP_Approval_Status__c.value") === 'Approved' ||
            !confirm("Are you sure you want to delete?")) {
            return;
        }

        var regionalLeadershipIndex = Number(event.currentTarget.id);
        helper.removeRegionalProjectLeadership(component, regionalLeadershipIndex);
        helper.updateLeadershipProgressStatus(component);
    },
    removeAdditionalAccessProjectLeadership: function(component, event, helper) {

        if (component.get("v.target.fields.GP_Approval_Status__c.value") === 'Approved' ||
            !confirm("Are you sure you want to delete?")) {
            return;
        }

        var additionalAccessLeadershipIndex = Number(event.currentTarget.id);
        helper.removeAdditionalAccessProjectLeadership(component, additionalAccessLeadershipIndex);
        helper.updateLeadershipProgressStatus(component);
    },
    saveMandatoryKeyMembersProjectLeadership: function(component, event, helper) {
        var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");

        if (!listOfMandatoryKeyMembersLeadership || listOfMandatoryKeyMembersLeadership.length === 0) {
            return;
        }
        var project = component.get("v.project");
        var skipEndDateCheck = project["RecordType"]["Name"] === "Indirect PID" || project["RecordType"]["Name"] === "BPM";
        var validationResult = helper.validateListOfLeadership(component, listOfMandatoryKeyMembersLeadership, skipEndDateCheck);

        if (!validationResult["isValid"]) {
            component.set("v.listOfMandatoryKeyMembersLeadership", validationResult["listOfLeadership"]);
            return;
        }
        helper.saveProjectLeadership(listOfMandatoryKeyMembersLeadership, component, "MandatoryKeyMembers");
        component.set("v.isMandatoryKeyMembersLeadershipSaved", true);
        //fire event to navigate to next section (As per the left sidebar list items)
    },
    saveRegionalProjectLeadership: function(component, event, helper) {
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");

        if (!listOfRegionalLeadership || listOfRegionalLeadership.length === 0) {
            return;
        }
        

        var validationResult = helper.validateRegionalLeadrship(component, listOfRegionalLeadership);
        if (!validationResult["isValid"]) {
            component.set("v.listOfRegionalLeadership", validationResult["listOfLeadership"]);
            return;
        }

        var project = component.get("v.project");
        var skipEndDateCheck = true;//project["RecordType"]["Name"] === "Indirect PID";

        validationResult = helper.validateListOfLeadership(component, listOfRegionalLeadership, skipEndDateCheck);

        if (!validationResult["isValid"]) {
            component.set("v.listOfRegionalLeadership", validationResult["listOfLeadership"]);
            return;
        }

        helper.saveProjectLeadership(listOfRegionalLeadership, component, "Regional");
    },
    saveAccountProjectLeadership: function(component, event, helper) {
        var listOfAccountLeadership = component.get("v.listOfAccountLeadership");

        if (!listOfAccountLeadership || listOfAccountLeadership.length === 0) {
            return;
        }

        var project = component.get("v.project");
        var skipEndDateCheck = project["RecordType"]["Name"] === "Indirect PID";

        var validationResult = helper.validateListOfLeadership(component, listOfAccountLeadership, skipEndDateCheck);

        if (!validationResult["isValid"]) {
            component.set("v.listOfAccountLeadership", validationResult["listOfLeadership"]);
            return;
        }

        helper.saveProjectLeadership(listOfAccountLeadership, component, "Account");
        component.set("v.isAccountLeadershipSaved", true);
    },
    saveHSLProjectLeadership: function(component, event, helper) {
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");

        if (!listOfHSLLeadership || listOfHSLLeadership.length === 0) {
            return;
        }

        var project = component.get("v.project");
        var skipEndDateCheck = project["RecordType"]["Name"] === "Indirect PID";

        var validationResult = helper.validateListOfLeadership(component, listOfHSLLeadership, skipEndDateCheck);

        if (!validationResult["isValid"]) {
            component.set("v.listOfHSLLeadership", validationResult["listOfLeadership"]);
            return;
        }
        helper.saveProjectLeadership(listOfHSLLeadership, component, "HSL");
        component.set("v.isHSLLeadershipSaved", true);
    },
    saveAdditionalAccessProjectLeadership: function(component, event, helper) {
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");

        if (!listOfAdditionalAccessLeadership || listOfAdditionalAccessLeadership.length === 0) {
            return;
        }

        var project = component.get("v.project");
        var skipEndDateCheck = project["RecordType"]["Name"] === "Indirect PID";

        var validationResult = helper.validateListOfLeadership(component, listOfAdditionalAccessLeadership, skipEndDateCheck);

        if (!validationResult["isValid"]) {
            component.set("v.listOfAdditionalAccessLeadership", validationResult["listOfLeadership"]);
            return;
        }
        helper.saveProjectLeadership(listOfAdditionalAccessLeadership, component, "Additional");

    },
    toggelIsEditableForMandatoryKeyMembersLeadershipRow: function(component, event, helper) {
        var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
        var MandatoryKeyMembersLeadershipIndex = Number(event.currentTarget.id);
        var isEditable = listOfMandatoryKeyMembersLeadership[MandatoryKeyMembersLeadershipIndex]["isEditable"];

        listOfMandatoryKeyMembersLeadership[MandatoryKeyMembersLeadershipIndex]["isEditable"] = !isEditable;
        component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
    },
    toggelIsEditableForRegionalLeadershipRow: function(component, event, helper) {
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
        var regionalLeadershipIndex = Number(event.currentTarget.id);
        var isEditable = listOfRegionalLeadership[regionalLeadershipIndex]["isEditable"];

        listOfRegionalLeadership[regionalLeadershipIndex]["isEditable"] = !isEditable;
        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
    },
    toggelIsEditableForAccountLeadershipRow: function(component, event, helper) {
        var listOfAccountLeadership = component.get("v.listOfAccountLeadership");
        var accountLeadershipIndex = Number(event.currentTarget.id);
        var isEditable = listOfAccountLeadership[accountLeadershipIndex]["isEditable"];

        listOfAccountLeadership[accountLeadershipIndex]["isEditable"] = !isEditable;
        component.set("v.listOfAccountLeadership", listOfAccountLeadership);
    },
    toggelIsEditableForHSLLeadershipRow: function(component, event, helper) {
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
        var hslLeadershipIndex = Number(event.currentTarget.id);
        var isEditable = listOfHSLLeadership[hslLeadershipIndex]["isEditable"];

        listOfHSLLeadership[hslLeadershipIndex]["isEditable"] = !isEditable;
        component.set("v.listOfHSLLeadership", listOfHSLLeadership);
    },
    toggelIsEditableForAdditionalAccessLeadershipRow: function(component, event, helper) {
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
        var additionalAccessLeadershipIndex = Number(event.currentTarget.id);
        var isEditable = listOfAdditionalAccessLeadership[additionalAccessLeadershipIndex]["isEditable"];

        listOfAdditionalAccessLeadership[additionalAccessLeadershipIndex]["isEditable"] = !isEditable;
        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    MandatoryKeyMembersEmployeeChangeHandler: function(component, event, helper) {
        var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
        var mapOfEmployeeIdToEmployee = component.get("v.mapOfEmployeeIdToEmployee");

        for (var MandatoryKeyMembersEmployeeIndex = 0; MandatoryKeyMembersEmployeeIndex < listOfMandatoryKeyMembersLeadership.length; MandatoryKeyMembersEmployeeIndex += 1) {
            var employeeId = listOfMandatoryKeyMembersLeadership[MandatoryKeyMembersEmployeeIndex]["GP_Employee_ID__c"]
            var employeeName = mapOfEmployeeIdToEmployee[employeeId] ? mapOfEmployeeIdToEmployee[employeeId]["Name"] : "";

            listOfMandatoryKeyMembersLeadership[MandatoryKeyMembersEmployeeIndex]["GP_Employee_ID__r"] = {
                "Name": employeeName
            };
        }

        component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
    },
    accountEmployeeChangeHandler: function(component, event, helper) {
        var listOfAccountLeadership = component.get("v.listOfAccountLeadership");
        var mapOfEmployeeIdToEmployee = component.get("v.mapOfEmployeeIdToEmployee");

        for (var accountEmployeeIndex = 0; accountEmployeeIndex < listOfAccountLeadership.length; accountEmployeeIndex += 1) {
            var employeeId = listOfAccountLeadership[accountEmployeeIndex]["GP_Employee_ID__c"]
            var employeeName = mapOfEmployeeIdToEmployee[employeeId] ? mapOfEmployeeIdToEmployee[employeeId]["Name"] : "";

            listOfAccountLeadership[accountEmployeeIndex]["GP_Employee_ID__r"] = {
                "Name": employeeName
            };
        }

        component.set("v.listOfAccountLeadership", listOfAccountLeadership);
    },
    hslEmployeeChangeHandler: function(component, event, helper) {
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
        var mapOfEmployeeIdToEmployee = component.get("v.mapOfEmployeeIdToEmployee");

        for (var hslEmployeeIndex = 0; hslEmployeeIndex < listOfHSLLeadership.length; hslEmployeeIndex += 1) {
            var employeeId = listOfHSLLeadership[hslEmployeeIndex]["GP_Employee_ID__c"]
            var employeeName = mapOfEmployeeIdToEmployee[employeeId] ? mapOfEmployeeIdToEmployee[employeeId]["Name"] : "";

            listOfHSLLeadership[hslEmployeeIndex]["GP_Employee_ID__r"] = {
                "Name": employeeName
            };
        }

        component.set("v.listOfHSLLeadership", listOfHSLLeadership);
    },
    additionalAccessEmployeeChangeHandler: function(component, event, helper) {
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
        var mapOfEmployeeIdToEmployee = component.get("v.mapOfEmployeeIdToEmployee");

        for (var additionalAccessEmployeeIndex = 0; additionalAccessEmployeeIndex < listOfAdditionalAccessLeadership.length; additionalAccessEmployeeIndex += 1) {
            var employeeId = listOfAdditionalAccessLeadership[additionalAccessEmployeeIndex]["GP_Employee_ID__c"]
            var employeeName = mapOfEmployeeIdToEmployee[employeeId] ? mapOfEmployeeIdToEmployee[employeeId]["Name"] : "";

            listOfAdditionalAccessLeadership[additionalAccessEmployeeIndex]["GP_Employee_ID__r"] = {
                "Name": employeeName
            };
        }

        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    editMandatoryKeyMembersProjectLeadership: function(component, event, helper) {
        var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
        for (var i = 0; i < listOfMandatoryKeyMembersLeadership.length; i += 1) {
            listOfMandatoryKeyMembersLeadership[i]["isEditable"] = true;
        }

        component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
    },
    editAccountProjectLeadership: function(component, event, helper) {
        var listOfAccountLeadership = component.get("v.listOfAccountLeadership");
        for (var i = 0; i < listOfAccountLeadership.length; i += 1) {
            listOfAccountLeadership[i]["isEditable"] = true;
        }

        component.set("v.listOfAccountLeadership", listOfAccountLeadership);
    },
    editHSLProjectLeadership: function(component, event, helper) {
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
        for (var i = 0; i < listOfHSLLeadership.length; i += 1) {
            listOfHSLLeadership[i]["isEditable"] = true;
        }

        component.set("v.listOfHSLLeadership", listOfHSLLeadership);
    },
    editRegionalProjectLeadership: function(component, event, helper) {
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
        for (var i = 0; i < listOfRegionalLeadership.length; i += 1) {
            listOfRegionalLeadership[i]["isEditable"] = true;
        }

        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
    },
    editAdditionalAccessProjectLeadership: function(component, event, helper) {
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
        for (var i = 0; i < listOfAdditionalAccessLeadership.length; i += 1) {
            listOfAdditionalAccessLeadership[i]["isEditable"] = true;
        }

        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    toggelSection: function(component, event, helper) {
        var activeSidebar = event.currentTarget.id;

        var activeSectionName = activeSidebar.split('_')[0];
        var listOfSections = helper.LIST_OF_SECTIONS;

        for(var i = 0; listOfSections && i < listOfSections.length; i += 1) {
            var sectionName = listOfSections[i]
            var sideBarName = sectionName + '_sidebar';

            var sectionDom = component.find(sectionName);
            var sideBarDom = component.find(sideBarName);
            
            if(activeSectionName === sectionName) {
                $A.util.removeClass(sectionDom, 'slds-hide');
                $A.util.addClass(sideBarDom, 'active');
            } else {
                $A.util.addClass(sectionDom, 'slds-hide');
                $A.util.removeClass(sideBarDom, 'active');
            }
        }
    },
    toggleSidebar: function(component, event, helper){
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },
    clearRegionalEmployee: function(component, event, helper) {

        var externalParameter = event.getParam("externalParameter");
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");

        listOfRegionalLeadership[externalParameter]["GP_Employee_ID__c"] = null;
        listOfRegionalLeadership[externalParameter]["GP_Employee_ID__r"] = null;

        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
    },
    clearAdditionalAccessEmployee: function(component, event, helper) {

        var externalParameter = event.getParam("externalParameter");
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");

        listOfAdditionalAccessLeadership[externalParameter]["GP_Employee_ID__c"] = null;
        listOfAdditionalAccessLeadership[externalParameter]["GP_Employee_ID__r"] = null;

        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    clearMandatoryEmployee: function(component, event, helper) {

        var externalParameter = event.getParam("externalParameter");
        var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");

        listOfMandatoryKeyMembersLeadership[externalParameter]["GP_Employee_ID__c"] = null;
        listOfMandatoryKeyMembersLeadership[externalParameter]["GP_Employee_ID__r"] = null;

        component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
    }
})