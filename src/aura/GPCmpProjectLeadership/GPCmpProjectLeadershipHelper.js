({
    REGIONAL_LEADERSHIP_RECORDTYPE_NAME: "Regional Leadership",
    ACCOUNT_LEADERSHIP_RECORDTYPE_NAME: "Account Leadership",
    HSL_LEADERSHIP_RECORDTYPE_NAME: "HSL Leadership",
    ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME: "Additional Access",
    MandatoryKeyMembers_LEADERSHIP_RECORDTYPE_NAME: "Mandatory Key Members",
    START_DATE_EMPTY: "Leadership start date should not be empty",
    END_DATE_EMPTY: "Leadership end date should not be empty",
    START_DATE_GREATER_THAN_END_DATE: "Leadership start date should be less than end date",
    LEADERSHIP_DATE_NOT_IN_RANGE: "Leadership start and end dates should be in range of project start and end date",
    START_DATE_LESS_THAN_TODAY: "Start date can not be less than today",
    LIST_OF_SECTIONS: [
        "AccountLeadership",
        "HSLLeadership",
        "RegionalLeadership",
        "AdditionalAccessLeadership"
    ],
    getLeaderShipData: function(component) {
        var getLeadershipDataService = component.get("c.getLeadershipData");
        var recordId = component.get("v.recordId");

        if ($A.util.isEmpty(recordId)) {
            //invalid context.
            this.hideSpinner(component);
            return;
        }

        getLeadershipDataService.setParams({
            "projectId": recordId
        });
        getLeadershipDataService.setCallback(this, function(response) {
            this.leadershipDataServiceServiceHandler(response, component);
        });
        $A.enqueueAction(getLeadershipDataService);
    },
    leadershipDataServiceServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectLeadershipData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    saveProjectLeadership: function(listOfProjectLeadership, component, recordType) {
        var saveLeadershipDataService = component.get("c.saveLeadershipData");
        var projectId = component.get("v.recordId");

        saveLeadershipDataService.setParams({
            "strListOfProjectLeadership": this.getSanatizedProjectLeadership(listOfProjectLeadership,recordType),
            "projectId": projectId
        });
        this.showSpinner(component);
        saveLeadershipDataService.setCallback(this, function(response) {
            this.saveLeadershipDataServiceHandler(response, component, recordType);
        });
        $A.enqueueAction(saveLeadershipDataService);
    },
    getSanatizedProjectLeadership: function(listOfProjectLeadership,recordType) {
        listOfProjectLeadership = JSON.parse(JSON.stringify(listOfProjectLeadership));
        var listOfProjectLeadershipWithEmployee = [];

        for (var i = 0; i < listOfProjectLeadership.length; i += 1) {
            listOfProjectLeadership[i]["listOfEmployee"] = null;
            listOfProjectLeadership[i]["GP_Employee_ID__r"] = null;
            listOfProjectLeadership[i]["listOfRoles"] = null;

            if (listOfProjectLeadership[i]["GP_Employee_ID__c"] === '--Select--' ||
                listOfProjectLeadership[i]["GP_Employee_ID__c"] === '--NONE--') {
                listOfProjectLeadership[i]["GP_Employee_ID__c"] = null;
                if(recordType==="Account" || recordType ==="Regional" ||  recordType==="HSL")
                	listOfProjectLeadershipWithEmployee.push(listOfProjectLeadership[i]);
            } else if (listOfProjectLeadership[i]["GP_Employee_ID__c"]) {
                listOfProjectLeadershipWithEmployee.push(listOfProjectLeadership[i]);
            }
            listOfProjectLeadership[i]["GP_Active__c"] = true;
        }

        return JSON.stringify(listOfProjectLeadershipWithEmployee);
    },
    saveLeadershipDataServiceHandler: function(response, component, recordType) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var updatedLeadershipData = JSON.parse(responseData.response);
            this.updateId(component, updatedLeadershipData, recordType);
            this.setIsEditableToFalse(component, recordType);
            this.showToast('SUCCESS', 'success', 'Information is saved successfully.');
            this.updateLeadershipProgressStatus(component);
            this.fireProjectSaveEvent();
            if (recordType === "MandatoryKeyMembers") {
                this.fireRelatedRecordSaveEvent("mandatoryKeyMembers");
            }
        } else if (component.isValid() && response.getState() === "SUCCESS" && responseData.message == 'Validation Error') {
            this.updateErrorMessage(component, responseData.response, recordType);
        } else {
			
			//Avinash - PID Approver validation AANF Change : handle error messages.
			
            if(responseData.message.indexOf("FIELD_CUSTOM_VALIDATION_EXCEPTION") >= 0) {
                var errorList = responseData.message.split("FIELD_CUSTOM_VALIDATION_EXCEPTION");
                var messageStr = errorList[1].substring(errorList[1].indexOf(",") + 2);
                responseData.message = messageStr.split(":")[0];
            }
			
            this.handleFailedCallback(component, responseData);
        }
    },
    updateErrorMessage: function(component, responseData, recordType) {
        var listOfProjectLeadership;
        responseData = JSON.parse(responseData) || {};

        if (recordType === "MandatoryKeyMembers") {
            listOfProjectLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
        } else if (recordType === "Account") {
            listOfProjectLeadership = component.get("v.listOfAccountLeadership");
        } else if (recordType === "HSL") {
            listOfProjectLeadership = component.get("v.listOfHSLLeadership");
        } else if (recordType === "Regional") {
            listOfProjectLeadership = component.get("v.listOfRegionalLeadership");
        } else if (recordType === "Additional") {
            listOfProjectLeadership = component.get("v.listOfAdditionalAccessLeadership");
        }

        for (var i = 0; i < listOfProjectLeadership.length; i += 1) {
            var errorMessage = responseData[listOfProjectLeadership[i]["GP_Leadership_Role_Name__c"]];
            if (errorMessage) {
                listOfProjectLeadership[i]["errorMessage"] = errorMessage;
            }
        }

        if (recordType === "MandatoryKeyMembers") {
            component.set("v.listOfMandatoryKeyMembersLeadership", listOfProjectLeadership);
        } else if (recordType === "Account") {
            component.set("v.listOfAccountLeadership", listOfProjectLeadership);
        } else if (recordType === "HSL") {
            component.set("v.listOfHSLLeadership", listOfProjectLeadership);
        } else if (recordType === "Regional") {
            component.set("v.listOfRegionalLeadership", listOfProjectLeadership);
        } else if (recordType === "Additional") {
            component.set("v.listOfAdditionalAccessLeadership", listOfProjectLeadership);
        }
    },
    updateId: function(component, updatedLeadershipData, recordType) {
        var listOfProjectLeadership;

        if (recordType === "MandatoryKeyMembers") {
            listOfProjectLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
        } else if (recordType === "Account") {
            listOfProjectLeadership = component.get("v.listOfAccountLeadership");
        } else if (recordType === "HSL") {
            listOfProjectLeadership = component.get("v.listOfHSLLeadership");
        } else if (recordType === "Regional") {
            listOfProjectLeadership = component.get("v.listOfRegionalLeadership");
        } else if (recordType === "Additional") {
            listOfProjectLeadership = component.get("v.listOfAdditionalAccessLeadership");
        }

        for (var i = 0; i < listOfProjectLeadership.length; i += 1) {
            var projectLeadershipRole = listOfProjectLeadership[i]["GP_Leadership_Role_Name__c"];
            var updatedLeadership = updatedLeadershipData[projectLeadershipRole] || {};

            listOfProjectLeadership[i]["Id"] = updatedLeadership["Id"];
        }

        if (recordType === "MandatoryKeyMembers") {
            component.set("v.listOfMandatoryKeyMembersLeadership", listOfProjectLeadership);
        } else if (recordType === "Account") {
            component.set("v.listOfAccountLeadership", listOfProjectLeadership);
        } else if (recordType === "HSL") {
            component.set("v.listOfHSLLeadership", listOfProjectLeadership);
        } else if (recordType === "Regional") {
            component.set("v.listOfRegionalLeadership", listOfProjectLeadership);
        } else if (recordType === "Additional") {
            component.set("v.listOfAdditionalAccessLeadership", listOfProjectLeadership);
        }
    },
    setIsEditableToFalse: function(component, recordType) {
        if (recordType === "MandatoryKeyMembers") {
            var listOfMandatoryKeyMembersLeadership = component.get("v.listOfMandatoryKeyMembersLeadership");
            this.setMandatoryKeyMembersIsEditableToFalse(component, listOfMandatoryKeyMembersLeadership);
        } else if (recordType === "Account") {
            var listOfAccountLeadership = component.get("v.listOfAccountLeadership");
            this.setAccountIsEditableToFalse(component, listOfAccountLeadership);
        } else if (recordType === "HSL") {
            var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
            this.setHSLIsEditableToFalse(component, listOfHSLLeadership);
        } else if (recordType === "Regional") {
            var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
            this.setRegionalIsEditableToFalse(component, listOfRegionalLeadership);
        } else if (recordType === "Additional") {
            var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
            this.setAdditionalAccessIsEditableToFalse(component, listOfAdditionalAccessLeadership);
        }
    },
    setProjectLeadershipData: function(component, responseData) {

        var responseJson = JSON.parse(responseData.response);
        var projectData = responseJson.project;

        var employeeFilterCondition = 'GP_isActive__c = true';

        /*
         * Commented on 29-05-2018 as this validation needs to be shifted from
         *  project start and end date to employee start date and end date.
        if(projectData["GP_Start_Date__c"]) {
            employeeFilterCondition += ' and GP_HIRE_Date__c <= ' + projectData["GP_Start_Date__c"];
        }

        if(projectData["GP_End_Date__c"]) {
            employeeFilterCondition += ' and ( GP_ACTUAL_TERMINATION_Date__c = null or GP_ACTUAL_TERMINATION_Date__c >= ' + projectData["GP_End_Date__c"] + ' ) ';
        }*/

        component.set("v.employeeFilterCondition", employeeFilterCondition);
        component.set("v.mapOfLeaderShipRecordTypeNameToRecordType", responseJson.mapOfLeaderShipRecordTypeNameToRecordType);
        component.set("v.mapOfEmployeeIdToEmployee", responseJson.mapOfEmployeeIdToEmployee);
        component.set("v.mapOfRecordTypeVisibility", responseJson.mapOfRecordTypeVisibility);
        component.set("v.mapOfCategoryToListOfRoles", responseJson.mapOfCategoryToListOfRoles);
        component.set("v.project", responseJson.project);
        component.set("v.mapOfRoleToListOfMandatoryServiceLines", responseJson.mapOfRoleToListOfMandatoryServiceLines);
        component.set("v.mapOfCategoryNameToLeadership", responseJson.mapOfCategoryNameToLeadership);
        component.set("v.isHSLLeadershipEditable", responseJson.isHSLLeadershipEditable);
        component.set("v.mapOfRoleToIsMandatory", responseJson.mapOfRoleToIsMandatory);

        var isEditingAfterApproval = !$A.util.isEmpty(projectData.GP_Parent_Project__c) &&
            projectData.GP_Oracle_PID__c !== 'NA';

        component.set("v.isEditingAfterApproval", isEditingAfterApproval);
        this.setMandatoryKeyMembersLeadershipData(component, responseJson);
        this.setAccoutLeadershipData(component, responseJson);
        this.setHSLLeadershipData(component, responseJson);
        // this.setAdditionalAccessLeadershipData(component, responseJson);
        this.setRegionalLeadershipData(component, responseJson);

        this.setVisibility(component);
    },
    setVisibility: function(component) {
        var mapOfRecordTypeVisibility = component.get("v.mapOfRecordTypeVisibility") || {};

        // for(var key in this.LIST_OF_SECTIONS) {
        for (var i = 0; i < this.LIST_OF_SECTIONS.length; i += 1) {
            var key = this.LIST_OF_SECTIONS[i];

            if (key !== "MandatoryKeyMembersLeadership" && mapOfRecordTypeVisibility[key]) {
                var leadershipCategory = key;
                var sideBarName = leadershipCategory + "_sidebar";
                var contentName = leadershipCategory;

                var sideBarDom = component.find(sideBarName);
                var contentDom = component.find(contentName);

                $A.util.addClass(sideBarDom, "active");
                $A.util.removeClass(contentDom, 'slds-hide');
                break;
            }
        }
    },
    setRegionalLeadershipData: function(component, responseJson) {
        var listOfRegionalLeadership = responseJson.listOfRegionalLeadership || [];
        var mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
        var projectId = component.get("v.recordId");
        var project = component.get("v.project");

        if (!listOfRegionalLeadership ||
            listOfRegionalLeadership.length === 0) {
            listOfRegionalLeadership = [{
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.REGIONAL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }, {
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.REGIONAL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }, {
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.REGIONAL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }];
        }
        var listOfRegionalRoleMaster = responseJson.mapOfCategoryToListOfRoles["Regional"] || [];
        var mapOfRoleToRegionalLeadershipMaster = {};

        var listOfRegionalRoles = ["--NONE--"];

        for (var roleMasterCounter = 0; roleMasterCounter < listOfRegionalRoleMaster.length; roleMasterCounter += 1) {
            listOfRegionalRoles.push(listOfRegionalRoleMaster[roleMasterCounter]["GP_Leadership_Role__c"]);
            mapOfRoleToRegionalLeadershipMaster[listOfRegionalRoleMaster[roleMasterCounter]["GP_Leadership_Role__c"]] = listOfRegionalRoleMaster[roleMasterCounter];
        }

        for (var regionalLeadershipCounter = 0; regionalLeadershipCounter < listOfRegionalLeadership.length; regionalLeadershipCounter += 1) {
            listOfRegionalLeadership[regionalLeadershipCounter]["listOfRoles"] = this.sortAndAddDefault(listOfRegionalRoles);
        }

        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
        component.set("v.mapOfRoleToRegionalLeadershipMaster", mapOfRoleToRegionalLeadershipMaster);
    },
    sortAndAddDefault: function(listOfOptions) {
        listOfOptions = listOfOptions || [];
        listOfOptions.sort(function(option1, option2) {
            if (option1 && option2)
                return option1.toLowerCase().localeCompare(option2.toLowerCase());
            else
                return 0;
        });

        return listOfOptions;
    },
    setMandatoryKeyMembersLeadershipData: function(component, responseJson) {
        var MandatoryKeyMembersRoleKey,
            listOfEmployee,
            listOfMandatoryKeyMembersLeadership,
            numberOfValidEmployee,
            MandatoryKeyMembersRoles,
            mapOfRoleToListOfEmployee,
            mapOfLeaderShipRecordTypeNameToRecordType;

        var project = responseJson.project;
        //if the MandatoryKeyMembers leadership is already defined then auto populate it
        //otherwise create new list of MandatoryKeyMembers leadership based on roles.
        if (responseJson.listOfMandatoryKeyMembersLeadership && responseJson.listOfMandatoryKeyMembersLeadership.length > 0) {
            listOfMandatoryKeyMembersLeadership = responseJson.listOfMandatoryKeyMembersLeadership;
            MandatoryKeyMembersRoles = responseJson.mapOfCategoryToListOfRoles["MandatoryKeyMembers"] || [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfRoleToProjectLeadership = this.getMapOfRoleToProjectLeadership(listOfMandatoryKeyMembersLeadership);
            var listOfUpdatedMandatoryKeyMembersLeadership = [];
            var self = this;

            MandatoryKeyMembersRoles.forEach(function(MandatoryKeyMembersRole) {
                MandatoryKeyMembersRoleKey = "Mandatory Key Members___" + MandatoryKeyMembersRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey] || [];
                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;

                if (listOfEmployee && (listOfEmployee.length > 1) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                }

                var projectLeadership = mapOfRoleToProjectLeadership[MandatoryKeyMembersRole["GP_Type_of_Leadership__c"]] || {};

                if (MandatoryKeyMembersRole["GP_Employee_Field_Type__c"] !== 'Searchable Text' &&
                    listOfEmployee.length === 1) {
                    projectLeadership["GP_Employee_ID__c"] = listOfEmployee[0]["Id"]
                }

                listOfUpdatedMandatoryKeyMembersLeadership.push({
                    "GP_Project__c": project.Id,
                    "Id": projectLeadership["Id"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Pinnacle_End_Date__c": projectLeadership["GP_Pinnacle_End_Date__c"] ? projectLeadership["GP_Pinnacle_End_Date__c"] : project["GP_End_Date__c"],
                    "GP_Leadership_Master__c": MandatoryKeyMembersRole["Id"],
                    "GP_Start_Date__c": projectLeadership["GP_Start_Date__c"] ? projectLeadership["GP_Start_Date__c"] : project["GP_Start_Date__c"],
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    "GP_Employee_ID__c": (MandatoryKeyMembersRole["GP_Employee_Field_Type__c"] !== 'Searchable Text' && (!mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey] || mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey].length === 0)) ? projectLeadership["GP_Employee_ID__c"] : projectLeadership["GP_Employee_ID__c"],
                    "GP_Employee_ID__r": (MandatoryKeyMembersRole["GP_Employee_Field_Type__c"] !== 'Searchable Text' && (!mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey] || mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey].length === 0)) ? projectLeadership["GP_Employee_ID__r"] : projectLeadership["GP_Employee_ID__r"] ? projectLeadership["GP_Employee_ID__r"] : {},
                    "GP_Leadership_Role__c": MandatoryKeyMembersRole["GP_Oracle_Id__c"],
                    "GP_Leadership_Role_Name__c": MandatoryKeyMembersRole["GP_Leadership_Role__c"],
                    "GP_Employee_Field_Type__c": MandatoryKeyMembersRole["GP_Employee_Field_Type__c"],
                    "GP_Editable_after_PID_Creation__c": MandatoryKeyMembersRole["GP_Editable_after_PID_Creation__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.MandatoryKeyMembers_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Parent_Project_Leadership__c": projectLeadership["GP_Parent_Project_Leadership__c"]
                });
            });

            component.set("v.listOfMandatoryKeyMembersLeadership", listOfUpdatedMandatoryKeyMembersLeadership);
            component.set("v.isMandatoryKeyMembersLeadershipSaved", true);
        } else if (responseJson.project.GP_Approval_Status__c === 'Approved') {
            //Do Nothing  
            component.set("v.isMandatoryKeyMembersLeadershipSaved", true);
        } else {
            var self = this;
            listOfMandatoryKeyMembersLeadership = [];
            MandatoryKeyMembersRoles = responseJson.mapOfCategoryToListOfRoles["MandatoryKeyMembers"] || [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfCategoryNameToLeadership = responseJson.mapOfCategoryNameToLeadership;

            MandatoryKeyMembersRoles.forEach(function(MandatoryKeyMembersRole) {
                MandatoryKeyMembersRoleKey = "Mandatory Key Members___" + MandatoryKeyMembersRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[MandatoryKeyMembersRoleKey] || [];
                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;

                if (listOfEmployee && (listOfEmployee.length > 1) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                }

                listOfMandatoryKeyMembersLeadership.push({
                    "GP_Project__c": project.Id,
                    "GP_Leadership_Role_Name__c": MandatoryKeyMembersRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.MandatoryKeyMembers_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": project["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    "GP_Employee_ID__c": MandatoryKeyMembersRole["GP_Employee_Field_Type__c"] !== 'Searchable Text' && listOfEmployee && listOfEmployee.length > 0 ? listOfEmployee[0]["Id"] : null,
                    "GP_Employee_ID__r": {
                        "Name": MandatoryKeyMembersRole["GP_Employee_Field_Type__c"] !== 'Searchable Text' && listOfEmployee && listOfEmployee.length > 0 && listOfEmployee[0]["Id"] ? listOfEmployee[0]["Name"] : null,
                    },
                    // commented by amitppt//15032018
                    //"GP_Leadership_Master__c": mapOfCategoryNameToLeadership["MandatoryKeyMembers"],
                    //"GP_Leadership_Role__c": mapOfCategoryNameToLeadership["MandatoryKeyMembers"],
                    // changed by amitppt//15032018
                    "GP_Leadership_Master__c": MandatoryKeyMembersRole["Id"],
                    "GP_Leadership_Role__c": MandatoryKeyMembersRole["GP_Oracle_Id__c"],
                    "GP_Editable_after_PID_Creation__c": MandatoryKeyMembersRole["GP_Editable_after_PID_Creation__c"],
                    "GP_Employee_Field_Type__c": MandatoryKeyMembersRole["GP_Employee_Field_Type__c"]
                });
            });

            component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
            component.set("v.isMandatoryKeyMembersLeadershipSaved", false);
        }
    },
    getMapOfRoleToProjectLeadership: function(listOfMandatoryKeyMembersLeadership) {
        var mapOfRoleToProjectLeadership = {};

        listOfMandatoryKeyMembersLeadership.forEach(function(projectLeadership) {
            mapOfRoleToProjectLeadership[projectLeadership["GP_Type_of_Leadership__c"]] = projectLeadership;
            //mapOfRoleToProjectLeadership[projectLeadership["GP_Leadership_Role__c"]] = projectLeadership;            
        });

        return mapOfRoleToProjectLeadership;
    },
    setAccoutLeadershipData: function(component, responseJson) {
        //if the Account leadership is already defined then auto populate it
        //otherwise create new list of Account leadership based on roles.
        var accountRoleKey,
            listOfEmployee,
            numberOfValidEmployee,
            listOfAccountLeadership,
            mapOfRoleToListOfEmployee,
            hasMultipleEmployeerecords,
            mapOfLeaderShipRecordTypeNameToRecordType;

        var project = responseJson.project;

        if (responseJson.listOfAccountLeadership && responseJson.listOfAccountLeadership.length > 0) {
            var accountRoles = responseJson.mapOfCategoryToListOfRoles["Account"] || [];
            var self = this;

            listOfAccountLeadership = responseJson.listOfAccountLeadership;
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfRoleToProjectLeadership = this.getMapOfRoleToProjectLeadership(listOfAccountLeadership);
            var listOfUpdatedAccountLeadership = [];
			
            accountRoles.forEach(function(accountRole) {
                accountRoleKey = "Account___" + accountRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[accountRoleKey] || [];

                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;
				hasMultipleEmployeerecords = true;
                if (listOfEmployee && (listOfEmployee.length ) || listOfEmployee.length === 0) {
                    if(listOfEmployee.length === 0){
                        hasMultipleEmployeerecords = false;
                   }
                    
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                   
                }
                var projectLeadership = mapOfRoleToProjectLeadership[accountRole["GP_Type_of_Leadership__c"]] || {};

                if(!hasMultipleEmployeerecords && projectLeadership["GP_Employee_ID__r"]){
                   
                    listOfEmployee.push({
                        "Id":projectLeadership["GP_Employee_ID__r"].Id,
                        "Name":projectLeadership["GP_Employee_ID__r"].Name,
                        "GP_Employee_Unique_Name__c":projectLeadership["GP_Employee_ID__r"].GP_Employee_Unique_Name__c
                    });
                       
                }
                listOfUpdatedAccountLeadership.push({
                    "GP_Project__c": project.Id,
                    "Id": projectLeadership["Id"],
                    "GP_Leadership_Role_Name__c": accountRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.ACCOUNT_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": projectLeadership["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": projectLeadership["GP_Pinnacle_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    "GP_Employee_ID__c": (!mapOfRoleToListOfEmployee[accountRoleKey] || mapOfRoleToListOfEmployee[accountRoleKey].length === 0) ? projectLeadership["GP_Employee_ID__c"] : projectLeadership["GP_Employee_ID__c"],
                    "GP_Employee_ID__r": (!mapOfRoleToListOfEmployee[accountRoleKey] || mapOfRoleToListOfEmployee[accountRoleKey].length === 0) ? projectLeadership["GP_Employee_ID__r"] ? projectLeadership["GP_Employee_ID__r"]:{}: projectLeadership["GP_Employee_ID__r"],
                    "GP_Leadership_Master__c": accountRole["Id"],
                    "GP_Leadership_Role__c": accountRole["GP_Oracle_Id__c"],
                });
            });

            component.set("v.listOfAccountLeadership", listOfUpdatedAccountLeadership);
            component.set("v.isAccountLeadershipSaved", true);
        } else if (responseJson.project.GP_Approval_Status__c === 'Approved') {
            //Do Nothing  
            component.set("v.isAccountLeadershipSaved", true);
        } else {
            var accountRoles = responseJson.mapOfCategoryToListOfRoles["Account"] || [];
            var self = this;
            listOfAccountLeadership = [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfCategoryNameToLeadership = responseJson.mapOfCategoryNameToLeadership;

            accountRoles.forEach(function(accountRole) {
                accountRoleKey = "Account___" + accountRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[accountRoleKey] || [];

                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;

                if (listOfEmployee && (listOfEmployee.length) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                }

                listOfAccountLeadership.push({
                    "GP_Project__c": project.Id,
                    "GP_Leadership_Role_Name__c": accountRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.ACCOUNT_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": project["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    "GP_Employee_ID__c": listOfEmployee && listOfEmployee.length > 0 ? listOfEmployee[0]["Id"] : null,
                    "GP_Employee_ID__r": {
                        "Name": listOfEmployee && listOfEmployee.length > 0 && listOfEmployee[0]["Id"] ? listOfEmployee[0]["Name"] : null,
                    },
                    // commented by amitppt//15032018
                    //"GP_Leadership_Master__c": mapOfCategoryNameToLeadership["Account"],
                    //"GP_Leadership_Role__c": mapOfCategoryNameToLeadership["Account"],
                    // changed by amitppt//15032018
                    "GP_Leadership_Master__c": accountRole["Id"],
                    "GP_Leadership_Role__c": accountRole["GP_Oracle_Id__c"],
                });
            });

            component.set("v.listOfAccountLeadership", listOfAccountLeadership);
            component.set("v.isAccountLeadershipSaved", false);
        }
    },
    setHSLLeadershipData: function(component, responseJson) {
        var hslRoleKey,
            listOfHSLLeadership,
            listOfEmployee,
            numberOfValidEmployee,
            mapOfRoleToListOfEmployee,
            mapOfLeaderShipRecordTypeNameToRecordType;
        //if the Account leadership is already defined then auto populate it
        //otherwise create new list of Account leadership based on roles.
        if (responseJson.listOfHSLLeadership && responseJson.listOfHSLLeadership.length > 0) {
            listOfHSLLeadership = responseJson.listOfHSLLeadership;

            var mapOfRoleToProjectLeadership = this.getMapOfRoleToProjectLeadership(listOfHSLLeadership);
            var HSLRoles = responseJson.mapOfCategoryToListOfRoles["HSL"] || [];
            var project = responseJson.project;
            var self = this;
            var listOfUpdatedHSLLeadership = [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfCategoryNameToLeadership = responseJson.mapOfCategoryNameToLeadership;
			var hasMultipleEmployeerecords ;
            HSLRoles.forEach(function(HSLRole) {
                hslRoleKey = "HSL___" + HSLRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[hslRoleKey] || [];
                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;
				hasMultipleEmployeerecords = true ;
                if (listOfEmployee && (listOfEmployee.length ) || listOfEmployee.length === 0) {
                   
                    if(listOfEmployee.length === 0){
                        hasMultipleEmployeerecords = false;
                   }
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                    
                }
                var HSLLeadership = mapOfRoleToProjectLeadership[HSLRole["GP_Type_of_Leadership__c"]] || {};
				 if(!hasMultipleEmployeerecords && HSLLeadership["GP_Employee_ID__r"]){
                   
                    listOfEmployee.push({
                        "Id":HSLLeadership["GP_Employee_ID__r"].Id,
                        "Name":HSLLeadership["GP_Employee_ID__r"].Name,
                        "GP_Employee_Unique_Name__c":HSLLeadership["GP_Employee_ID__r"].GP_Employee_Unique_Name__c
                    });
                       
                }
                listOfUpdatedHSLLeadership.push({
                    "Id": HSLLeadership["Id"],
                    "GP_Project__c": project.Id,
                    "GP_Leadership_Role_Name__c": HSLRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.HSL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": HSLLeadership["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": HSLLeadership["GP_Pinnacle_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Employee_ID__c": (!mapOfRoleToListOfEmployee[hslRoleKey] || mapOfRoleToListOfEmployee[hslRoleKey].length === 0) ? HSLLeadership["GP_Employee_ID__c"] : HSLLeadership["GP_Employee_ID__c"],
                    "GP_Employee_ID__r": (!mapOfRoleToListOfEmployee[hslRoleKey] || mapOfRoleToListOfEmployee[hslRoleKey].length === 0) ? HSLLeadership["GP_Employee_ID__r"]?HSLLeadership["GP_Employee_ID__r"]:{} : HSLLeadership["GP_Employee_ID__r"],
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    // commented by amitppt//15032018
                    //"GP_Leadership_Master__c": mapOfCategoryNameToLeadership["HSL"],
                    //"GP_Leadership_Role__c": mapOfCategoryNameToLeadership["HSL"],
                    // changed by amitppt//15032018
                    "GP_Leadership_Master__c": HSLRole["Id"],
                    "GP_Leadership_Role__c": HSLRole["GP_Oracle_Id__c"],
                });
            });
            component.set("v.listOfHSLLeadership", listOfUpdatedHSLLeadership);
            component.set("v.isHSLLeadershipSaved", true);
        } else if (responseJson.project.GP_Approval_Status__c === 'Approved') {
            //Do Nothing  
            component.set("v.isHSLLeadershipSaved", true);
        } else {
            var HSLRoles = responseJson.mapOfCategoryToListOfRoles["HSL"] || [];
            var project = responseJson.project;
            var self = this;
            listOfHSLLeadership = [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfCategoryNameToLeadership = responseJson.mapOfCategoryNameToLeadership;

            HSLRoles.forEach(function(HSLRole) {
                hslRoleKey = "HSL___" + HSLRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[hslRoleKey] || [];
                if (listOfEmployee && (listOfEmployee.length ) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                }

                numberOfValidEmployee = listOfEmployee ? listOfEmployee.length : 0;
                listOfHSLLeadership.push({
                    "GP_Project__c": project.Id,
                    "GP_Leadership_Role_Name__c": HSLRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.HSL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": project["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Employee_ID__c": listOfEmployee && listOfEmployee.length > 0 ? listOfEmployee[0]["Id"] : null,
                    "GP_Employee_ID__r": {
                        "Name": listOfEmployee && listOfEmployee.length > 0 && listOfEmployee[0]["Id"] ? listOfEmployee[0]["Name"] : null,
                    },
                    "GP_Leadership_Available_Count__c": numberOfValidEmployee,
                    // commented by amitppt//15032018
                    //"GP_Leadership_Master__c": mapOfCategoryNameToLeadership["HSL"],
                    //"GP_Leadership_Role__c": mapOfCategoryNameToLeadership["HSL"],
                    // changed by amitppt//15032018
                    "GP_Leadership_Master__c": HSLRole["Id"],
                    "GP_Leadership_Role__c": HSLRole["GP_Oracle_Id__c"],
                });
            });

            component.set("v.listOfHSLLeadership", listOfHSLLeadership);
            component.set("v.isHSLLeadershipSaved", false);
        }
    },
    setAdditionalAccessLeadershipData: function(component, responseJson) {
        var listOfAdditionalAccessLeadership,
            mapOfRoleToListOfEmployee,
            AdditionalAccessRoles,
            mapOfLeaderShipRecordTypeNameToRecordType,
            listOfEmployee;

        var projectId = component.get("v.recordId");

        mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");

        //if the Account leadership is already defined then auto populate it
        //otherwise create new list of Account leadership based on roles.
        if (responseJson.listOfAdditionalAccessLeadership &&
            responseJson.listOfAdditionalAccessLeadership.length > 0) {

            listOfAdditionalAccessLeadership = responseJson.listOfAdditionalAccessLeadership;
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;

            for (var i = 0; i < listOfAdditionalAccessLeadership.length; i += 1) {

                var additionalAccessRoleKey = "AdditionalAccess___" + listOfAdditionalAccessLeadership[i]["GP_Leadership_Role_Name__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[additionalAccessRoleKey] || [];

                if (listOfEmployee && (listOfEmployee.length > 1) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                } else if (listOfEmployee.length === 1) {
                    listOfAccountLeadership[i]["GP_Employee_ID__c"] = listOfEmployee[0]["Id"];
                    listOfAccountLeadership[i]["GP_Employee_ID__r"] = {};
                    listOfAccountLeadership[i]["GP_Employee_ID__r"]["Name"] = listOfEmployee[0]["Name"];
                }

                listOfAdditionalAccessLeadership[i]["listOfEmployee"] = listOfEmployee;
            }
            component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
        } else if (responseJson.project.GP_Approval_Status__c === 'Approved') {
            //Do Nothing  
            component.set("v.isMandatoryKeyMembersLeadershipSaved", true);
        } else {
            var project = responseJson.project;
            var self = this;
            listOfAdditionalAccessLeadership = [{
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }, {
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }, {
                "isEditable": true,
                "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                "GP_Project__c": projectId,
                "GP_Start_Date__c": project["GP_Start_Date__c"],
                "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"]
            }];
            //listOfAdditionalAccessLeadership = [];

            AdditionalAccessRoles = responseJson.mapOfCategoryToListOfRoles["Additional Access"] || [];
            mapOfRoleToListOfEmployee = responseJson.mapOfRoleToListOfEmployee;
            mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
            var mapOfCategoryNameToLeadership = responseJson.mapOfCategoryNameToLeadership;

            AdditionalAccessRoles.forEach(function(AdditionalAccessRole) {
                var HSLRoleKey = "AdditionalAccess___" + AdditionalAccessRole["GP_Type_of_Leadership__c"];
                listOfEmployee = mapOfRoleToListOfEmployee[HSLRoleKey] || [];

                if (listOfEmployee && (listOfEmployee.length > 1) || listOfEmployee.length === 0) {
                    listOfEmployee.unshift({
                        "Name": "--NONE--"
                    });
                }

                listOfAdditionalAccessLeadership.push({
                    "GP_Project__c": project.Id,
                    "GP_Leadership_Role_Name__c": AdditionalAccessRole["GP_Leadership_Role__c"],
                    "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[self.ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME]["Id"],
                    "GP_Start_Date__c": project["GP_Start_Date__c"],
                    "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
                    "listOfEmployee": listOfEmployee,
                    "GP_Employee_ID__c": listOfEmployee && listOfEmployee.length > 0 ? listOfEmployee[0]["Id"] : null,
                    "GP_Employee_ID__r": {
                        "Name": listOfEmployee && listOfEmployee.length > 0 && listOfEmployee[0]["Id"] ? listOfEmployee[0]["Name"] : null,
                    },
                    // commented by amitppt//15032018
                    //"GP_Leadership_Master__c": mapOfCategoryNameToLeadership["AdditionalAccess"],
                    //"GP_Leadership_Role__c": mapOfCategoryNameToLeadership["AdditionalAccess"],
                    // changed by amitppt//15032018
                    "GP_Leadership_Master__c": AdditionalAccessRole["Id"],
                    "GP_Leadership_Role__c": AdditionalAccessRole["GP_Oracle_Id__c"],
                });
            });

            component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
        }
    },
    deleteLeadershipData: function(component, projectLeadershipId) {
        if (!projectLeadershipId) {
            return;
        }

        this.showSpinner(component);
        var deleteLeadershipDataService = component.get("c.deleteLeadershipData");
        deleteLeadershipDataService.setParams({
            "projectLeadershipId": projectLeadershipId,
            "projectId": component.get("v.recordId")
        });
        deleteLeadershipDataService.setCallback(this, function(response) {
            this.deleteLeadershipDataServiceHandler(response, component);
        });
        $A.enqueueAction(deleteLeadershipDataService);
    },
    deleteLeadershipDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Project leadership deleted successfully.');
            this.fireProjectSaveEvent(true);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    removeRegionalProjectLeadership: function(component, regionalLeadershipIndex) {
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
        var regionalLeadershipToBeDeleted = listOfRegionalLeadership[regionalLeadershipIndex];

        listOfRegionalLeadership.splice(regionalLeadershipIndex, 1);
        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
        this.deleteLeadershipData(component, regionalLeadershipToBeDeleted["Id"]);
    },
    removeAdditionalAccessProjectLeadership: function(component, additionalAccessLeadershipIndex) {
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
        var additionalAccessLeadershipToBeDeleted = listOfAdditionalAccessLeadership[additionalAccessLeadershipIndex];

        listOfAdditionalAccessLeadership.splice(additionalAccessLeadershipIndex, 1);
        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
        this.deleteLeadershipData(component, additionalAccessLeadershipToBeDeleted["Id"]);
    },
    setRegionalIsEditableToFalse: function(component, listOfRegionalLeadership) {
        for (var i = 0; i < listOfRegionalLeadership.length; i += 1) {
            listOfRegionalLeadership[i]["isEditable"] = false;
        }
        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
    },
    setAccountIsEditableToFalse: function(component, listOfAccountLeadership) {
        for (var i = 0; i < listOfAccountLeadership.length; i += 1) {
            listOfAccountLeadership[i]["isEditable"] = false;
        }
        component.set("v.listOfAccountLeadership", listOfAccountLeadership);
    },
    setHSLIsEditableToFalse: function(component, listOfHSLLeadership) {
        for (var i = 0; i < listOfHSLLeadership.length; i += 1) {
            listOfHSLLeadership[i]["isEditable"] = false;
        }
        component.set("v.listOfHSLLeadership", listOfHSLLeadership);
    },
    setAdditionalAccessIsEditableToFalse: function(component, listOfAdditionalAccessLeadership) {
        for (var i = 0; i < listOfAdditionalAccessLeadership.length; i += 1) {
            listOfAdditionalAccessLeadership[i]["isEditable"] = false;
        }
        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    setMandatoryKeyMembersIsEditableToFalse: function(component, listOfMandatoryKeyMembersLeadership) {

        for (var i = 0; i < listOfMandatoryKeyMembersLeadership.length; i += 1) {
            listOfMandatoryKeyMembersLeadership[i]["isEditable"] = false;
        }
        component.set("v.listOfMandatoryKeyMembersLeadership", listOfMandatoryKeyMembersLeadership);
    },
    addToRegionalProjectLeadership: function(component) {
        var mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
        var projectId = component.get("v.recordId");
        var mapOfCategoryToListOfRoles = component.get("v.mapOfCategoryToListOfRoles");
        var listOfRoleMaster = mapOfCategoryToListOfRoles["Regional"] || [];
        var listOfRoles = ['--NONE--'];

        for (var i = 0; i < listOfRoleMaster.length; i += 1) {
            listOfRoles.push(listOfRoleMaster[i]["GP_Leadership_Role__c"]);
        }

        var project = component.get("v.project");

        var regionalProjectLeadership = {
            "isEditable": true,
            "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.REGIONAL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
            "GP_Project__c": projectId,
            "listOfRoles": listOfRoles,
            "GP_Start_Date__c": project["GP_Start_Date__c"],
            "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
            "GP_Type_of_Leadership__c": "Regional Leadership"
        };

        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
        listOfRegionalLeadership.push(regionalProjectLeadership);

        component.set("v.listOfRegionalLeadership", listOfRegionalLeadership);
    },
    addToHSLProjectLeadership: function(component) {
        var mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
        var projectId = component.get("v.recordId");

        var hslProjectLeadership = {
            "isEditable": true,
            "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.HSL_LEADERSHIP_RECORDTYPE_NAME]["Id"],
            "GP_Project__c": projectId
        };
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
        listOfHSLLeadership.push(hslProjectLeadership);

        component.set("v.listOfHSLLeadership", listOfHSLLeadership);
    },
    addToAdditionalAccessProjectLeadership: function(component) {
        var mapOfLeaderShipRecordTypeNameToRecordType = component.get("v.mapOfLeaderShipRecordTypeNameToRecordType");
        var projectId = component.get("v.recordId");
        var project = component.get("v.project");

        var additionalAccessProjectLeadership = {
            "isEditable": true,
            "RecordTypeId": mapOfLeaderShipRecordTypeNameToRecordType[this.ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME]["Id"],
            "GP_Project__c": projectId,
            "GP_Start_Date__c": project["GP_Start_Date__c"],
            "GP_Pinnacle_End_Date__c": project["GP_End_Date__c"],
            "GP_Type_of_Leadership__c": "Additional Access"
        };
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");
        listOfAdditionalAccessLeadership.push(additionalAccessProjectLeadership);

        component.set("v.listOfAdditionalAccessLeadership", listOfAdditionalAccessLeadership);
    },
    updateLeadershipProgressStatus: function(component) {
        var listOfRegionalLeadership = component.get("v.listOfRegionalLeadership");
        var listOfAccountLeadership = component.get("v.listOfAccountLeadership");
        var listOfHSLLeadership = component.get("v.listOfHSLLeadership");
        var listOfAdditionalAccessLeadership = component.get("v.listOfAdditionalAccessLeadership");

        if ((listOfRegionalLeadership && listOfRegionalLeadership.length > 0) ||
            (listOfAccountLeadership && listOfAccountLeadership.length > 0) ||
            (listOfHSLLeadership && listOfHSLLeadership.length > 0) ||
            (listOfAdditionalAccessLeadership && listOfAdditionalAccessLeadership.length > 0)) {
            component.set("v.leadershipStatus", "dirty");
        } else {
            component.set("v.leadershipStatus", "unChanged");
        }
    },
    validateListOfLeadership: function(component, listOfLeadership, skipEndDateCheck) {
        var project = component.get("v.project");
        var isValid = true;
        var mapOfRoleToListOfMandatoryServiceLines = component.get("v.mapOfRoleToListOfMandatoryServiceLines");
        var today = new Date().toJSON().slice(0, 10);
        var mapOfCategoryNameToLeadership = component.get("v.mapOfCategoryNameToLeadership");
        var mapOfRoleToIsMandatory = component.get("v.mapOfRoleToIsMandatory");
        var mapOfUnique = {};
        var regionalMapOfUnique = {};
        var billingSPOCEmployee, billingApproverEmployee;
        var isEndDateMandatory = !skipEndDateCheck;

        for (var i = 0; i < listOfLeadership.length; i += 1) {
            listOfLeadership[i]["errorMessage"] = "";

            var currentRole = "Role Master___" + listOfLeadership[i]["GP_Leadership_Role_Name__c"];

            var listOfMandatoryServiceLines = mapOfRoleToListOfMandatoryServiceLines[currentRole];
            var mapOfMandatoryServiceLine = this.getMapOfSeviceLineForList(listOfMandatoryServiceLines);
            var uniqueKey = listOfLeadership[i]["GP_Leadership_Role_Name__c"];
            uniqueKey += listOfLeadership[i]["GP_Employee_ID__c"];
            uniqueKey += listOfLeadership[i]["GP_Start_Date__c"];
            uniqueKey += listOfLeadership[i]["GP_Pinnacle_End_Date__c"];

            listOfLeadership[i]["errorMessage"] = '';
            if (mapOfUnique[uniqueKey]) {
                listOfLeadership[i]["errorMessage"] = 'Duplicate Data';
                isValid = false;
            } else if (listOfLeadership[i]["GP_Employee_ID__c"]) {
                mapOfUnique[uniqueKey] = true;
            }
            //regional Leadership
            var uniqueKeyRegionalLeadership = listOfLeadership[i]["GP_Leadership_Role_Name__c"];

            listOfLeadership[i]["errorMessage"] = '';
            if (!mapOfRoleToIsMandatory[currentRole]) {
                if (regionalMapOfUnique[uniqueKeyRegionalLeadership]) {
                    listOfLeadership[i]["errorMessage"] = 'Duplicate Data';
                    isValid = false;
                } else if (listOfLeadership[i]["GP_Employee_ID__c"]) {
                    regionalMapOfUnique[uniqueKeyRegionalLeadership] = true;
                }
            }

            // Start date can not be empty
            if (listOfLeadership[i]["GP_Employee_ID__c"] &&
                listOfLeadership[i]["GP_Employee_ID__c"] != "--NONE--" &&
                !listOfLeadership[i]["GP_Start_Date__c"]) {
                listOfLeadership[i]["errorMessage"] = this.START_DATE_EMPTY;
                isValid = false;
            }

            listOfLeadership[i]["GP_Pinnacle_End_Date__c"] = listOfLeadership[i]["GP_Pinnacle_End_Date__c"] || null;

            // end date can not be empty
            if (isEndDateMandatory &&
                listOfLeadership[i]["GP_Employee_ID__c"] &&
                listOfLeadership[i]["GP_Employee_ID__c"] != "--NONE--" &&
                !listOfLeadership[i]["GP_Pinnacle_End_Date__c"]) {
                if (listOfLeadership[i]["errorMessage"]) {
                    listOfLeadership[i]["errorMessage"] += ' and ';
                }
                listOfLeadership[i]["errorMessage"] += this.END_DATE_EMPTY;
                isValid = false;
            }

            //Start date can not be greater than end date.
            if (listOfLeadership[i]["GP_Pinnacle_End_Date__c"] &&
                listOfLeadership[i]["GP_Start_Date__c"] > listOfLeadership[i]["GP_Pinnacle_End_Date__c"]) {
                if (listOfLeadership[i]["errorMessage"]) {
                    listOfLeadership[i]["errorMessage"] += ' and ';
                }
                listOfLeadership[i]["errorMessage"] += this.START_DATE_GREATER_THAN_END_DATE;
                isValid = false;
            }

            //Leadership Start Date should be equal to project Start Date
            if (!listOfLeadership[i]["GP_Parent_Project_Leadership__c"] &&
                listOfLeadership[i]["GP_Leadership_Role__c"] == "PROJECT MANAGER" &&
                listOfLeadership[i]["GP_Start_Date__c"] != project["GP_Start_Date__c"]) {
                if (listOfLeadership[i]["errorMessage"])
                    listOfLeadership[i]["errorMessage"] += ' and ';
                listOfLeadership[i]["errorMessage"] += 'Leadership Start Date should be equal to project Start Date (' + project["GP_Start_Date__c"] + ')';
                isValid = false;
            }

            //leadership start and end date should lie in between project start and end date
            if (listOfLeadership[i]["GP_Start_Date__c"] < project["GP_Start_Date__c"] ||
                (listOfLeadership[i]["GP_Pinnacle_End_Date__c"] &&
                    listOfLeadership[i]["GP_Pinnacle_End_Date__c"] > project["GP_End_Date__c"])) {
                if (listOfLeadership[i]["errorMessage"])
                    listOfLeadership[i]["errorMessage"] += ' and ';
                listOfLeadership[i]["errorMessage"] += this.LEADERSHIP_DATE_NOT_IN_RANGE + '( ' + project["GP_Start_Date__c"] + ' to ' + project["GP_End_Date__c"] + ' )';
                isValid = false;
            }

            //Validate Mandatory service line check.
            if (project["RecordType"]["Name"] === 'CMITS' &&
                project["GP_HSL__r"] &&
                mapOfMandatoryServiceLine[project["GP_HSL__r"]["Parent_HSL__c"]] &&
                (!listOfLeadership[i]["GP_Employee_ID__c"] ||
                    listOfLeadership[i]["GP_Employee_ID__c"] === '--Select--' ||
                    listOfLeadership[i]["GP_Employee_ID__c"] === '--NONE--')) {
                if (listOfLeadership[i]["errorMessage"])
                    listOfLeadership[i]["errorMessage"] += ' and ';
                listOfLeadership[i]["errorMessage"] += 'HSL Delivery Head should be mandatory';
                //listOfLeadership[i]["errorMessage"] += 'Employee is mandatory for ' + project["GP_HSL__r"]["Parent_HSL__c"] + ' service line';
                isValid = false;
            }
            //For MandatoryKeyMembers/Regional/Additional Access Leadership Employee is always mandatory.
            //listOfLeadership[i]["GP_Leadership_Master__c"] === mapOfCategoryNameToLeadership["MandatoryKeyMembers"] ||

            //Employee is mandatory for MandatoryKeyMembers, Regional & Additional Access Leadership
            if ((!listOfLeadership[i]["GP_Leadership_Master__c"]) &&
                (!listOfLeadership[i]["GP_Employee_ID__c"] ||
                    listOfLeadership[i]["GP_Employee_ID__c"] === '--Select--' ||
                    listOfLeadership[i]["GP_Employee_ID__c"] === '--NONE--')) {
                if (listOfLeadership[i]["errorMessage"])
                    listOfLeadership[i]["errorMessage"] += ' and ';
                listOfLeadership[i]["errorMessage"] += 'Employee is mandatory for Additional Access Leadership';
                isValid = false;
            }

            if (mapOfRoleToIsMandatory[currentRole] &&
                (!listOfLeadership[i]["GP_Employee_ID__c"] ||
                    listOfLeadership[i]["GP_Employee_ID__c"] === '--NONE--')) {
                if (listOfLeadership[i]["errorMessage"])
                    listOfLeadership[i]["errorMessage"] += ' and ';
                listOfLeadership[i]["errorMessage"] += 'Employee is mandatory for this role';
                isValid = false;
            }

            if (listOfLeadership[i]["GP_Leadership_Role_Name__c"] === 'Billing Spoc') {
                billingSPOCEmployee = listOfLeadership[i]["GP_Employee_ID__c"];
            } else if (listOfLeadership[i]["GP_Leadership_Role_Name__c"] === 'Billing Approver') {
                billingApproverEmployee = listOfLeadership[i]["GP_Employee_ID__c"];
            }
        }

        //Billing SPOC & Billing Approver can not be same.
        if ((!$A.util.isEmpty(billingSPOCEmployee) &&
                billingSPOCEmployee !== '--NONE--') &&
            (!$A.util.isEmpty(billingApproverEmployee) &&
                billingSPOCEmployee !== '--NONE--') &&
            billingApproverEmployee === billingSPOCEmployee) {
            isValid = false;
            this.showToast('error', 'error', 'Billing SPOC and Billing Approver cannot be same.');
        }

        return {
            "isValid": isValid,
            "listOfLeadership": listOfLeadership
        };
    },
    getMapOfSeviceLineForList: function(listOfMandatoryServiceLines) {
        listOfMandatoryServiceLines = listOfMandatoryServiceLines || [];
        var result = {};
        listOfMandatoryServiceLines.forEach(function(mandatoryServiceLine) {
            result[mandatoryServiceLine] = true;
        });

        return result;
    },
    validateRegionalLeadrship: function(component, listOfRegionalLeadership) {
        var mapOfRoleToRegionalLeadershipMaster = component.get("v.mapOfRoleToRegionalLeadershipMaster");
        var isValid = true;

        for (var i = 0; i < listOfRegionalLeadership.length; i += 1) {
            var isCurrentRecordValid = true;
            listOfRegionalLeadership[i]["errorMessage"] = "";

            if ($A.util.isEmpty(listOfRegionalLeadership[i]["GP_Leadership_Role_Name__c"]) ||
                listOfRegionalLeadership[i]["GP_Leadership_Role_Name__c"] === "--NONE--") {
                listOfRegionalLeadership[i]["errorMessage"] = "Please Choose a Role";
                isValid = false;
                isCurrentRecordValid = false;
            }

            if ($A.util.isEmpty(listOfRegionalLeadership[i]["GP_Employee_ID__c"])) {
                if (!isCurrentRecordValid) {
                    listOfRegionalLeadership[i]["errorMessage"] += ", ";
                }
                listOfRegionalLeadership[i]["errorMessage"] += "Please Choose an Employee";
                isValid = false;
                isCurrentRecordValid = false;
            }

            if ($A.util.isEmpty(listOfRegionalLeadership[i]["GP_Start_Date__c"])) {
                if (!isCurrentRecordValid) {
                    listOfRegionalLeadership[i]["errorMessage"] += ", ";
                }
                listOfRegionalLeadership[i]["errorMessage"] += "Please Choose start date";
                isValid = false;
                isCurrentRecordValid = false;
            }

            // if($A.util.isEmpty(listOfRegionalLeadership[i]["GP_Pinnacle_End_Date__c"])) {
            //     if(!isCurrentRecordValid) {
            //         listOfRegionalLeadership[i]["errorMessage"] += ", "; 
            //     }
            //     listOfRegionalLeadership[i]["errorMessage"] += "Please Choose end date";
            //     isValid = false;
            //     isCurrentRecordValid = false;
            // } 

            if (isCurrentRecordValid) {
                listOfRegionalLeadership[i]["errorMessage"] = null;
                var regionalLeadershipMaster = mapOfRoleToRegionalLeadershipMaster[listOfRegionalLeadership[i]["GP_Leadership_Role_Name__c"]];
                listOfRegionalLeadership[i]["GP_Leadership_Role__c"] = regionalLeadershipMaster["GP_Oracle_Id__c"];
                listOfRegionalLeadership[i]["GP_Leadership_Master__c"] = regionalLeadershipMaster["Id"];
            }
        }

        return {
            "isValid": isValid,
            "listOfLeadership": listOfRegionalLeadership
        };
    }
})