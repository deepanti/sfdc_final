({
    init : function(component, event, helper) 
    {  
        var Att_isopen = component.get("v.isOpen");
        var SelectId = component.get("v.selectedID");
        component.set('v.productDetailFlag', true);
        var selectedID = component.get("v.selectedID");
        var getOLI = component.get("c.getOppProductDetails");
        getOLI.setParams({
            "OLIID" : selectedID
        });
        getOLI.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
               
                var OppProductWrapperList = response.getReturnValue();
                component.set("v.recordId",OppProductWrapperList.OpportunityId);
                component.set("v.DefaultOliId",OppProductWrapperList.Id); //DefaultproductId
                component.set("v.DefaultproductId",OppProductWrapperList.Product2Id);
                component.set("v.selectedProductNameDefault",OppProductWrapperList.Product2.Name);
                component.set("v.contract",OppProductWrapperList. Contract_Term__c);
                component.set("v.Fte",OppProductWrapperList.FTE__c);
                component.set("v.localcurrancy",OppProductWrapperList.Local_Currency__c);
                component.set("v.TCBvalue",OppProductWrapperList.UnitPrice);
                component.set("v.Revenuestartdate",OppProductWrapperList.Revenue_Start_Date__c);
                component.set("v.DeliveringOrg",OppProductWrapperList.Delivering_Organisation__c);
                component.set("v.SubDelOrg",OppProductWrapperList.Sub_Delivering_Organisation__c);
                if(OppProductWrapperList.Sub_Delivering_Organisation__c != ''){
                    component.set("v.SubDeliveringOrganization", OppProductWrapperList.Sub_Delivering_Organisation__c);
                }
                
                component.set("v.ProductbdRep",OppProductWrapperList.Product_BD_Rep__c);//DeliveryLocation
                component.set("v.DeliveryLoc",OppProductWrapperList.Delivery_Location__c);
                helper.getProductDetails(component);
                helper.getProductsOnload(component);
            }
        }); 
        $A.enqueueAction(getOLI);
        helper.getLocalCurrency(component);
        helper.getDeliveringOrganization(component); 
        helper.getDeliveryLocation(component);
       
    },
    
    getSubDeliveringOprganization :function(component, event, helper)
    {   
        component.set("v.SubDeliveringOrganization",'');
        var selectedDeliveringOrganization = component.get("v.DeliveringOrg"); 
        component.set("v.SubDelOrg", ''); 
        var action = component.get("c.getFieldDependencies");
        
        action.setParams({
            "controllingPickListValue" : selectedDeliveringOrganization
        });
        action.setCallback(this, function(response)
                           {      
                               component.set("v.SubDeliveringOrganization", response.getReturnValue());
                           });  
        $A.enqueueAction(action);
    },
    
    Reset : function(component, event, helper)
    {
        component.set("v.selectedProductNameDefault",'--None--');
        component.set("v.selectedServiceLineDefault",'--None--');
        component.set("v.selectedNatureOfWorkDefault",'--None--');
        component.set("v.OnChangeServiceLine",false);
        component.set("v.OnChangeNatureOfWork",false);
        component.set("v.OnChangeProductName",false);
        component.find("ServiceLine").set("v.disabled", false); 
        component.find("NatureOfWork").set("v.disabled", false);
        component.find("ProductName").set("v.disabled", false);
        helper.getProducts(component); 		
    }, 	 
    
    OnChange_ServiceLine:function(component, event, helper) 
    {		
        
        component.set("v.OnChangeServiceLine", true);
        component.set("v.isFilterLoading", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");	
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
        if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value == '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value != '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--' && ServiceLine_Value != '--None--'))
        {    
            
            helper.getProducts(component);
            component.find("reset").set("v.disabled", false);
        }
        else
        {
            
            component.set("v.isFilterLoading", false);
        }
        if(ServiceLine_Value == '--None--'){
            
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        } 
    },
    
    OnChange_NatureOfWork:function(component, event, helper) 
    {		
        component.set("v.OnChangeNatureOfWork", true);
        component.set("v.isFilterLoading", true);
        // alert(':--natureofwork--:');
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");	
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
        if( (ServiceLine_Value == '--None--' && ProductName_Value == '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--') 
           || (ServiceLine_Value != '--None--' && ProductName_Value == '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--')
           || (ServiceLine_Value == '--None--' && ProductName_Value != '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--')
           || (ServiceLine_Value != '--None--' && ProductName_Value != '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--'))
        {
            helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }   
        
        else
        {
            component.set("v.isFilterLoading", false);
        }
        if(NatureOfWork_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        } 
    },
    
    OnChange_ProductName:function(component, event, helper) 
    {		
        component.set("v.OnChangeProductName", true);
        component.set("v.isFilterLoading", true);
        
        var OppProduct2Data = [];				
        OppProduct2Data = component.get("v.productDataList");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");	
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
        if( (ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--') 
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--'))		
        {
            helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }
        else
        {
            component.set("v.isFilterLoading", false);
        }	
        if(ProductName_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        } 
    },
    
    handleCancel : function(component, event, helper)
    {	
        component.destroy();			
    }, 
    
    closeSuccessToast : function(component, event, helper)
    {
        component.set("v.isSuccess", false);
    },
    
    closeErrorToast : function(component, event, helper){
        component.set("v.isError", false);
    },
    
    saveOli: function(component, event, helper){
        component.set("v.isLoading", true);
        var contract = component.get("v.contract");
        var Fte = component.get("v.Fte");
        var localcurrcy = component.get("v.localcurrancy");
        var TCBvalue = component.get("v.TCBvalue");
        var Revenuestartdate = component.get("v.Revenuestartdate");
        var DeliveringOrg = component.get("v.DeliveringOrg");
        var SubDelOrg = component.get("v.SubDelOrg");
        var ProductbdRep = component.get("v.ProductbdRep");
        var DeliveryLoc = component.get("v.DeliveryLoc");
        var OppId = component.get("v.recordId");
        var OppOliId = component.get("v.DefaultOliId");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");
        var product2Id = component.get("v.DefaultproductId");
        var oppdate = component.get("v.oppcloseDate");
         var SubDeliveringOrganization = component.get("v.SubDeliveringOrganization");
        if(SubDeliveringOrganization == null){
            SubDeliveringOrganization = [];
        }
       	
        var DeliveryLocFlag = true;
       	var bdRepFlag = false;
        var contractTermFlag = false;
        var closedDateFlag = false;
        var parts;
        var revenueStartDate; 
        var closedDateParts = component.get("v.oppcloseDate").split('-');
        var closedDate = new Date(closedDateParts[0], closedDateParts[1]-1,  closedDateParts[2], 0,0,0,0);
        var today = new Date();
        var subDeliveryOrganizationFlag = false;
        var loggedInUser = component.get('v.loggedInUser'); 
        var tcvFlag = false;
        var deliveryOrgFlag = false;
        var currencyFlag = false;
        
        function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
        if(Revenuestartdate.indexOf('/') !== -1){
            parts =Revenuestartdate.split('/');
            revenueStartDate = new Date( parts[2], parts[0]-1,  parts[1], 0,0,0,0);
        }
        else{
            parts =Revenuestartdate.split('-');
            
            revenueStartDate = new Date( parts[0], parts[1]-1,  parts[2], 0,0,0,0);
        }
	       var contractTerm;
        if(isNaN(contract)){
            contractTermFlag = false;     
        }
        else{
            contractTerm = Number(contract);
        }
        if(ProductbdRep){
            bdRepFlag = true;
        }
        else{
            bdRepFlag = false; 
        }
        if(localcurrcy){
            currencyFlag = true;
        }
        else{
            currencyFlag = false;
        }
        if(contractTerm >0 && contractTerm % 1 === 0){
            contractTermFlag = true; 
        }            
        else{
            contractTermFlag = false;
        }
        if( revenueStartDate.getTime() >= closedDate.getTime()){
          	closedDateFlag = true;
        }
        else{
            
            closedDateFlag = false;
        }
        if(!isNaN(TCBvalue) && TCBvalue != 0){
            tcvFlag = true;
        }
        else{
            tcvFlag = false; 
        }
        if(DeliveryLoc){
            DeliveryLocFlag = true;
        }
        else{
            DeliveryLocFlag = false;
        }
        if(DeliveringOrg){
            deliveryOrgFlag = true;
        }
        else{
            deliveryOrgFlag = false;
        }
        if(SubDeliveringOrganization.length > 0 && SubDelOrg){
            subDeliveryOrganizationFlag = true;
        }
        else if(SubDeliveringOrganization.length == 0 && !SubDelOrg){
            subDeliveryOrganizationFlag = true;
        }
            else if(SubDeliveringOrganization.length > 0 && !SubDelOrg){
                subDeliveryOrganizationFlag = false;
            }  
         if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
        	closedDateFlag = true;	    
        }
       	
        if(contractTermFlag && closedDateFlag && bdRepFlag && subDeliveryOrganizationFlag && tcvFlag && deliveryOrgFlag && subDeliveryOrganizationFlag && DeliveryLocFlag && currencyFlag){
            var action = component.get("c.SaveEditOliItem");
            if(Fte == ''){
                Fte = 0;
            }
            action.setParams({ 
                "ServiceLine" : ServiceLine_Value,
                "NatureOfWork" : NatureOfWork_Value,
                "ProductName" : ProductName_Value,
                "OldOliItmId" : OppOliId,
                "opprtyId" : OppId,
                "contrct" : contract,
                "fte" : Fte,
                "localcurrncy" : localcurrcy,
                "tcbval" : TCBvalue,
                "revenuedt" : Revenuestartdate,
                "deliveryorg" : DeliveringOrg,
                "subdelorg" : SubDelOrg,
                "prodtbdrp" : ProductbdRep,
                "delivryloc" : DeliveryLoc,
                "productId" : product2Id
            });
            action.setCallback(this, function(response) {
                if(response.getReturnValue()){
                    component.set("v.isSuccess", true);
                    var success = true;
                    if(isLightningExperienceOrSalesforce1()) {
                        var urlEvent = $A.get("e.force:navigateToURL");
                        if(urlEvent) {
                            urlEvent.setParams({
                                "url": "/apex/OpportunityProductDetailPage",
                                "isredirect": "true",
                                "oppID" : OppId
                            });
                            urlEvent.fire();
                        }
                        else{
                            window.location = '/apex/OpportunityProductDetailPage?recordID='+OppId;
                        }
                    }
                    else{
                        window.location = '/apex/OpportunityProductDetailPage?recordID='+OppId;
                    }
                }
                else{
                    component.set("v.errorMessage", "Please Enter the correct values and try again.");
                    component.set("v.isError", true);
                    component.set("v.isLoading", false);
                    window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.isError", false); 
                        }),3000
                    );
                }
            });
            $A.enqueueAction(action);
        }else {
           if(!contractTermFlag){
                component.set("v.errorMessage", "Please enter contract term greater than 0");
            }
            else if(!closedDateFlag){
                component.set("v.errorMessage", "Revenue Start Date should be greater than MSA /SOW Closure Date");
            }
            else if(!bdRepFlag){
            	component.set("v.errorMessage", "Please enter Product BD Rep");
            } 
            else if(!subDeliveryOrganizationFlag){
            	component.set("v.errorMessage", "Please enter Sub delivery Organization");
            }
            else if(!tcvFlag){
            	component.set("v.errorMessage", "Make Sure there is no decimal value / special character like comma / space in TCV.");   
            }
            else if(!deliveryOrgFlag){
                  component.set("v.errorMessage", "Please enter delivery Organization");
           }
            else if(!DeliveryLocFlag){
                  component.set("v.errorMessage", "Please enter delivery Location");
           }
            else if(!currencyFlag){
                    component.set("v.errorMessage", "Please enter Local Currency.");
                }
            
            
            component.set("v.isLoading", false);
            component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            );
        }
        
    },
    closeErrorToast : function(component, event, helper){
        component.set("v.isError", false);
    },
    
    closeModel: function(component, event, helper) 
    {
        component.set("v.isOpen", false);
    },
    
    closeSuccessToast :function(component, event, helper) 
    {
        component.set("v.isSuccess", false);
    },
})