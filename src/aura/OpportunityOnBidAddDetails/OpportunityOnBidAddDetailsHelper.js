({
    
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var acc = component.find("oppSource");
        var accountName = acc.get("v.value");
        if($A.util.isUndefinedOrNull(accountName) || accountName == '')
        {
            $A.util.addClass(acc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(acc,'slds-has-error');
        }
        
        var ProposedPricing = component.find("ProposedPricing");
        var ProposedPricingVal = ProposedPricing.get("v.value");
        if($A.util.isUndefinedOrNull(ProposedPricingVal) || ProposedPricingVal == '')
        {
            $A.util.addClass(ProposedPricing,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(ProposedPricing,'slds-has-error');
        }
        return proceedToSave;
    },
    fireToastEvent : function(component, event, helper, msg) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "Error",
            "message": msg,
            "type": "error"
        });
        toastEvent.fire();
        component.set("v.Spinner", false);
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
})