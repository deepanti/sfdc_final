({
    saveRecord : function(component, event, helper) {
        component.set("v.Spinner", true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            
            if(localStorage.getItem('StageName')=='2. Define')
            {
                component.set("v.StageName","3. On Bid");
                localStorage.setItem('StageName',"3. On Bid");
            }
            component.find("recordViewForm").submit();
            localStorage.setItem('MaxTabId',11);
        }
        else
        {
            helper.fireToastEvent(component, event, helper, "Required fields are missing");
            component.set("v.Spinner", false);
        }
    }, 
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    saveAndNext : function(component, event, helper) {
        component.set("v.Spinner", true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        
        if(check)
        {
            
             if(localStorage.getItem('MaxTabId')<12){
                localStorage.setItem('MaxTabId',12);
                component.set("v.MaxSelectedTabId", "12");
                 if(localStorage.getItem('StageName')=='2. Define')
                {
                    component.set("v.StageName","3. On Bid");
                    localStorage.setItem('StageName',"3. On Bid");
                }
            }
           
            component.find("recordViewForm").submit();
            
            localStorage.setItem('LatestTabId',"12");
            window.scrollTo(0, 0);
           
        }
        else
        {
            helper.fireToastEvent(component, event, helper, "Required fields are missing");
            component.set("v.Spinner", false);
        }
    },
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',10);
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "10"});
        appEvent.fire();
        window.scrollTo(0, 0);
    },
    onSuccess:function(component, event, helper){
        var appEvent = $A.get("e.c:SetSalesPath");
        appEvent.fire();
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully');
        component.set("v.IsSpinner", false);
        var compEvents = component.getEvent("componentEventFired");
            compEvents.fire();
    },
    showError : function(component, event, helper) {
        
        localStorage.setItem('MaxTabId',11);
        localStorage.setItem('LatestTabId',"11");//fix to change the stage
       	component.set("v.StageName","2. Define");//fix to change the stage 
        localStorage.setItem('StageName',"2. Define");//fix to change the stage
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastEvent(component, event, helper, errorMsg);
        
    },
})