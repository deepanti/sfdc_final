({
    openReport : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        //Navigate to the report details on click
        
        navEvt.setParams({
            "recordId": component.get("v.ReportId")
            
        });
        navEvt.fire();
    },
    validateInputs : function(component, event, helper) {
        try{
            var ReportId = component.get("v.ReportId");
            if(ReportId.length!=15 && ReportId.length!=18)
            {
                //Show error if the report Id is less than equals 15 or 18 char
                component.set("v.errorFlag",true);
            }
        }
        catch(exception){}
    }
})