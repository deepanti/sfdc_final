({
	getRecordTypes : function(component) {
		var recordTypeList = [];
        recordTypeList.push('Pre-discover Opportunity');
        recordTypeList.push('Discover Opportunity');
        component.set("v.recordTypeList", recordTypeList);
	}
})