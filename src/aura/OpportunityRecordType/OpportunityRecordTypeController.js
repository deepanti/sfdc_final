({
    doInit : function(component, event, helper) {
        helper.getRecordTypes(component);
        component.set("v.myURL", '/006/e?retURL=%2F006%2Fo&RecordType=0120k0000008vrHAAQ&ent=Opportunity');
    },
    
    validateRecordType : function(component, event, helper){
        var recordType = component.get("v.recordType");
        if(recordType == ""){
            component.set("v.disableFlag", true);      
        }
        else{
            component.set("v.disableFlag", false);     
        }    
    },
    
    openRecordTypeLayout : function(component, event, helper){
        var recordType = component.get("v.recordType");
        var action = component.get("c.getRecordTypeID");
        function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        } 
        
        action.setParams({
            "recordType" : recordType
        });
        
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                if(isLightningExperienceOrSalesforce1()) {
                    if(recordType == 'Pre-discover Opportunity'){
                        sforce.one.createRecord("Opportunity",response.getReturnValue(), {StageName : 'Prediscover'});    
                    }
                    else if(recordType == 'Discover Opportunity'){
                        sforce.one.createRecord("Opportunity",response.getReturnValue(), {StageName : '1. Discover'});      
                    }
                } 
                else {  
                    if(recordType == 'Pre-discover Opportunity'){
                    	window.location = '/006/e?RecordType=' + response.getReturnValue() + '&nooverride=1&opp11=Prediscover';
                    }
                    else if(recordType == 'Discover Opportunity'){
                    	window.location = '/006/e?RecordType=' + response.getReturnValue() + '&nooverride=1&opp11=1.+Discover';    
                    }
                }    
            }
        });
        $A.enqueueAction(action); 
    },
    
    backToOpportunityPage : function(component, event, helper){
    	 function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        } 
        if(isLightningExperienceOrSalesforce1()) {
        	sforce.one.navigateToURL('/006');    
        }
        else{
          window.location = '/006/o';  
        }
    }
})