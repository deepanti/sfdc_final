({
    handleActive: function (component, event, tabSelected) {
        var tab = component.find(tabSelected);
        if(component.get("v.isEventFired"))
        {
            //Set the previous tab
            component.set("v.selectedPrevTab", component.get("v.selectedTab"));
            localStorage.setItem('LatestTabId',tabSelected);
            /*
            switch (tab.get('v.id')) {
                case '1' :
                    this.injectComponent('c:OpportunityConfirmedAddDetails', tab, component.get("v.recordId"),'','');
                    break;
                case '2' :
                    this.injectComponent('c:OpportunityVFPageRerender', tab, component.get("v.recordId"),'/apex/Win_Loss_Dropped_Surveys','1000px');
                    break;
                case '2' :
                    this.injectComponent('c:OpportunityConfirmedAddContract', tab, component.get("v.recordId"),'','');
                    break;
            }
            */
        }
        else
        {
            //block tab change
            component.set("v.selectedTab", component.get("v.selectedPrevTab"));
        }
        component.set("v.isEventFired",false);
    },
    injectComponent: function (name, target, recId,page,height) {
        $A.createComponent(name, {
            pagename : page,
            recordId : recId,
            height:height
        }, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                throw new Error(error);
            }
        });
    },
    
    recordUpdated : function(component, event, helper, forceRecord){
            //Update the tab selected on record load
            component.set("v.isEventFired",true);
            component.set("v.selectedTab", forceRecord);
            component.set('v.selectedPrevTab',forceRecord); 
            helper.handleActive(component, event, forceRecord);
    }
})