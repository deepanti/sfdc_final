({
    getWorklocationData: function(component) {
        var worklocationDataService = component.get("c.getWorklocationData");
        worklocationDataService.setParams({
            "projectId": component.get("v.recordId")
        });
        worklocationDataService.setCallback(this, function(response) {
            this.worklocationDataServiceHandler(response, component);
        });
        $A.enqueueAction(worklocationDataService);
    },
    validateWorkLocation: function(component, listOfProjectWorklocation) {
        var numberOfPrimary = 0;
        var projectData = component.get("v.project");
        var mapOfLECODE = {};

        var validationResponse = {
            "isValid": true,
            "errorMessage": null
        };
        var mapOfUnique = {};
        var listOfWorkLocationToBeSaved = [];
        var listOfWorkLocationToBeDeleted = component.get("v.listOfProjectWorklocationToBeDeleted") || [];

        for (var index = 0, workLocationToBeSavedIndex = -1; listOfProjectWorklocation && index < listOfProjectWorklocation.length; index += 1) {
            var projectWorkLocation = listOfProjectWorklocation[index];
            projectWorkLocation["errorMessage"] = null;

            var key = projectWorkLocation.GP_Work_Location__c;
            var leCode = listOfProjectWorklocation[index]["GP_Work_Location__r"] ? listOfProjectWorklocation[index]["GP_Work_Location__r"]["GP_Legal_Entity_Code__c"] : "";
            
            if(!$A.util.isEmpty(leCode)) {
                mapOfLECODE[leCode] = true;
            }

            // listOfWorkLocationToBeSaved[workLocationToBeSavedIndex]["errorMessage"] = null;

            var fieldElement;
            if (projectWorkLocation.GP_Primary__c) {
                numberOfPrimary++;
            }
            

            if (projectWorkLocation.GP_BCP__c &&
                $A.util.isEmpty(projectWorkLocation.GP_Work_Location__c)) {
                validationResponse["isValid"] = false;
                projectWorkLocation["errorMessage"] = "Work Location is mandatory";
                listOfWorkLocationToBeSaved.push(projectWorkLocation);
                workLocationToBeSavedIndex++;
            } else if (!$A.util.isEmpty(projectWorkLocation.GP_Work_Location__c)) {
                listOfWorkLocationToBeSaved.push(projectWorkLocation);
                workLocationToBeSavedIndex++;
            } else {
                if(projectWorkLocation["Id"]) {
                    listOfWorkLocationToBeDeleted.push(projectWorkLocation);
                }
                continue;
            }

            if (mapOfUnique[key]) {
                validationResponse["isValid"] = false;
                listOfWorkLocationToBeSaved[workLocationToBeSavedIndex]["errorMessage"] = 'Duplicate Record.';
            } else if(key) {
                mapOfUnique[key] = true;
            }

            if ($A.util.isEmpty(projectWorkLocation.GP_BCP__c) ||
                projectWorkLocation.GP_BCP__c.toLowerCase() === '--Select--') {
                validationResponse["isValid"] = false;
                listOfWorkLocationToBeSaved[workLocationToBeSavedIndex]["errorMessage"] = "BCP is mandatory";
            }

            if (projectWorkLocation.GP_Primary__c && projectWorkLocation.GP_is_Active__c) {
                validationResponse["isValid"] = false;
                listOfWorkLocationToBeSaved[workLocationToBeSavedIndex]["errorMessage"] = "Primary Worklocation cannot be deactivated.";
            }
        }
        var project = component.get("v.project");

        //Primary Work Location is mandatory for Non BPM project and non support projets.
        if (numberOfPrimary === 0 && 
            projectData[0]["fields"]["GP_Deal_Category__c"]["value"] != 'Support' && 
            projectData[0]["recordTypeInfo"]["name"] !== "BPM") {
            validationResponse["isValid"] = false;
            validationResponse["errorMessage"] = "Primary Work Location is mandatory";
        } else if (numberOfPrimary > 1) {
            validationResponse["isValid"] = false;
            validationResponse["errorMessage"] = "There can be only one Primary Work Location under a project";
        }

        /*if (projectData[0]["recordTypeInfo"]["name"] === "BPM") {
            var numberOfLE = this.sizeOfMap(mapOfLECODE);
            if (numberOfLE > 1) {
                validationResponse["isValid"] = false;
                validationResponse["errorMessage"] = "Work Locations for BPM projects should be of same LE code.";
            }
        }*/

        component.set("v.listOfProjectWorklocation", listOfWorkLocationToBeSaved);
        component.set("v.listOfProjectWorklocationToBeDeleted", listOfWorkLocationToBeDeleted);

        validationResponse["listOfWorkLocationToBeSaved"] = listOfWorkLocationToBeSaved;
        validationResponse["listOfWorkLocationToBeDeleted"] = listOfWorkLocationToBeDeleted;

        return validationResponse;
    },
    sizeOfMap: function(associativeMap) {
        var sizeOfMap = 0;

        for (var key in associativeMap) {
            sizeOfMap++;
        }

        return sizeOfMap;
    },
    validateSDO: function(component, listOfSDOProjectWorklocation) {
        listOfSDOProjectWorklocation = listOfSDOProjectWorklocation || [];

        var projectData = component.get("v.project");
        var validationResponse = {
            "isValid": true,
            "errorMessage": null
        };
        var listOfSDOToBeSaved = [];
        var listOfProjectSDOToBeDeleted = component.get("v.listOfProjectSDOToBeDeleted") || [];

        var mapOfUnique = {};

        for (var index = 0, sdoToBeSavedIndex = -1; index < listOfSDOProjectWorklocation.length; index += 1) {
            var projectWorkLocation = listOfSDOProjectWorklocation[index];

            if (!$A.util.isEmpty(projectWorkLocation.GP_Work_Location__c)) {
                listOfSDOToBeSaved.push(projectWorkLocation);
                sdoToBeSavedIndex++;
            } else {
                if(projectWorkLocation["Id"]) {
                    listOfSDOToBeDeleted.push(projectWorkLocation);
                }
                continue;
            }

            var key = projectWorkLocation.GP_Work_Location__c;
            listOfSDOProjectWorklocation[index]["errorMessage"] = '';

            if (mapOfUnique[key]) {
                validationResponse["isValid"] = false;
                listOfSDOToBeSaved[sdoToBeSavedIndex]["errorMessage"] = 'Duplicate Record.';
            } else if(key) {
                mapOfUnique[key] = true;
            }
        }

        component.set("v.listOfProjectSDOToBeDeleted", listOfProjectSDOToBeDeleted);
        component.set("v.listOfSDOProjectWorklocation", listOfSDOToBeSaved);

        validationResponse["listOfProjectSDOToBeDeleted"] = listOfProjectSDOToBeDeleted;
        validationResponse["listOfSDOToBeSaved"] = listOfSDOToBeSaved;

        return validationResponse;
    },
    worklocationDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectWorklocationData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProjectWorklocationData: function(component, responseData) {

        var responseJson = JSON.parse(responseData.response);
        var listOfProjectWorklocation = responseJson.listOfProjectWorklocation;
        var listOfSDOProjectWorklocation = responseJson.listOfSDOProjectWorklocation;
        var projectWorkLocationRecordType = responseJson.projectWorkLocationRecordType;
        var masterWorkLocationRecordType = responseJson.masterWorkLocationRecordType;
        var mapOfMasterWorkLocationRecordType = responseJson.mapOfMasterWorkLocationRecordType;
        var sdoRecordTypeId = mapOfMasterWorkLocationRecordType['SDO']["Id"];
        var worklocationRecordTypeId = mapOfMasterWorkLocationRecordType['Work Location']["Id"];
        var directProjectAddress = responseJson.directProjectAddress || {};
        var isGSTEnabled = responseJson.isGSTEnabled;

        var project = responseJson.project;

        var projectId = component.get("v.recordId");
        var projectData = component.get("v.projectData");
        var bcpRequired;
        var workLocationFilterCondition, SDOFilterCondition;
        var isSetUsingOMS = responseJson.isSetUsingOMS;

        var isEditingAfterApproval = !$A.util.isEmpty(project.GP_Parent_Project__c) &&
            project.GP_Oracle_PID__c !== 'NA';

        component.set("v.isEditingAfterApproval", isEditingAfterApproval);
        component.set("v.isSetUsingOMS", isSetUsingOMS);

        if (project && project["RecordType"] && project["RecordType"]["Name"] === "BPM") {
            // bcpRequired = "No";
            workLocationFilterCondition = "GP_Status__c = 'Active and Visible' and RecordTypeId = '" + worklocationRecordTypeId + "'";

            if (!$A.util.isEmpty(project["GP_Start_Date__c"])) {
                workLocationFilterCondition += " and GP_Start_Date_Active__c <= " + project["GP_Start_Date__c"];
            }

            workLocationFilterCondition += " and (GP_End_Date_Active__c = null";

            if (!$A.util.isEmpty(project["GP_End_Date__c"])) {
                workLocationFilterCondition += " or GP_End_Date_Active__c > " + project["GP_End_Date__c"];
            }

            workLocationFilterCondition += ")";
            workLocationFilterCondition += " and GP_Billable__c = 'Yes' and GP_Hide_For_BPM__c = false";

            SDOFilterCondition = "GP_Status__c = 'Active and Visible' and RecordTypeId = '" + sdoRecordTypeId + "'";

            if (!$A.util.isEmpty(project["GP_Start_Date__c"])) {
                SDOFilterCondition += " and GP_Start_Date_Active__c <= " + project["GP_Start_Date__c"];
            }

            SDOFilterCondition += " and (GP_End_Date_Active__c = null";

            if (!$A.util.isEmpty(project["GP_End_Date__c"])) {
                SDOFilterCondition += " or GP_End_Date_Active__c > " + project["GP_End_Date__c"] + ")";
            }

            //SDOFilterCondition += " and GP_Billable__c = 'Yes' and GP_Hide_For_BPM__c = false";  //commented by amitppt//31032018
        } else {
            workLocationFilterCondition = "GP_Status__c = 'Active and Visible' and RecordTypeId = '" + worklocationRecordTypeId + "'";

            if (!$A.util.isEmpty(project["GP_Start_Date__c"])) {
                workLocationFilterCondition += " and GP_Start_Date_Active__c <= " + project["GP_Start_Date__c"];
            }

            workLocationFilterCondition += " and (GP_End_Date_Active__c = null";

            if (!$A.util.isEmpty(project["GP_End_Date__c"])) {
                workLocationFilterCondition += " or GP_End_Date_Active__c > " + project["GP_End_Date__c"];
            }

            workLocationFilterCondition += ")";
            workLocationFilterCondition += " and GP_Billable__c = 'Yes'";

            SDOFilterCondition = "GP_Status__c = 'Active and Visible' and RecordTypeId = '" + sdoRecordTypeId + "'";

            if (!$A.util.isEmpty(project["GP_Start_Date__c"])) {
                SDOFilterCondition += " and GP_Start_Date_Active__c <= " + project["GP_Start_Date__c"];
            }

            SDOFilterCondition += " and (GP_End_Date_Active__c = null";

            if (!$A.util.isEmpty(project["GP_End_Date__c"])) {
                SDOFilterCondition += " or GP_End_Date_Active__c > " + project["GP_End_Date__c"];
            }

            SDOFilterCondition += ")";
            //SDOFilterCondition += " and GP_Billable__c = 'Yes'"; //commented by amitppt//31032018
        }

        if (directProjectAddress && 
            (isGSTEnabled && 
             directProjectAddress["GP_Bill_To_Address__r"] && 
             (directProjectAddress["GP_Bill_To_Address__r"]["GP_Country__c"] === 'IN' || 
              directProjectAddress["GP_Bill_To_Address__r"]["GP_Country__c"] === 'India'))) {
            workLocationFilterCondition += " And GP_Country__c = 'India'";
            // SDOFilterCondition += " And GP_Country__c = 'India'";
        }

        component.set("v.workLocationFilterCondition", workLocationFilterCondition);
        component.set("v.SDOFilterCondition", SDOFilterCondition);

        if (!listOfProjectWorklocation ||
            listOfProjectWorklocation.length === 0) {
            listOfProjectWorklocation = [{
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[1]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }, {
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[1]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }, {
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[1]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }];
        }

        if (!listOfSDOProjectWorklocation ||
            listOfSDOProjectWorklocation.length === 0) {
            listOfSDOProjectWorklocation = [{
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[0]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }, {
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[0]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }, {
                "isEditable": true,
                "RecordTypeId": projectWorkLocationRecordType[0]["Id"],
                "GP_Project__c": projectId,
                "GP_BCP__c": bcpRequired
            }];
        }

        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
        component.set("v.masterWorkLocationRecordType", masterWorkLocationRecordType);
        component.set("v.projectWorkLocationRecordType", projectWorkLocationRecordType);
        component.set("v.mapOfMasterWorkLocationRecordType", mapOfMasterWorkLocationRecordType)
    },
    saveWorkLocation: function(component, listOfProjectWorklocationToBeSaved, listOfProjectWorklocationToBeDeleted, isSDO) {
        this.showSpinner(component);
        
        var saveWorklocationData;
        if(listOfProjectWorklocationToBeDeleted && listOfProjectWorklocationToBeDeleted.length > 0) {
            saveWorklocationData = component.get("c.updateWorklocationData");
            saveWorklocationData.setParams({
                "strListOfWorklocationToBeSaved": JSON.stringify(listOfProjectWorklocationToBeSaved),
                "strListOfWorklocationToBeDeleted": JSON.stringify(listOfProjectWorklocationToBeDeleted)
            });
        } else {
            //HSN Changes
            var validationResponse = this.validateIsHSNApplicable(component, listOfProjectWorklocationToBeSaved);
            if(validationResponse['isValid'] || isSDO) {
                saveWorklocationData = component.get("c.saveWorklocationData");
                var isHSNSelected = component.get("v.isHSNSelected");
                var hsnCatId = '';
                var hsnCatName = '';
                //HSN Changes
                var isHSNNeeded = validationResponse['isHSNNeeded'];
                
                if(isHSNSelected) {
                    hsnCatId = component.get("v.hsnCatId");
                    hsnCatName = component.get("v.hsnCatName");
                }
                
                saveWorklocationData.setParams({
                    "strListOfWorklocation": JSON.stringify(listOfProjectWorklocationToBeSaved),
                    "hsnCatId" : hsnCatId,
                    "hsnCatName" : hsnCatName,
                    "isHSNNeeded" : isHSNNeeded
                });
            } else {
                component.set("v.isWorkLocationModal",true);
                this.hideSpinner(component);
                return;
            }
        }
        
        saveWorklocationData.setCallback(this, function(response) {
            this.saveWorklocationServiceHandler(response, component, isSDO);
        });
        $A.enqueueAction(saveWorklocationData);
    },
    saveWorklocationServiceHandler: function(response, component, isSDO) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var updatedListOfWorkLocation = JSON.parse(responseData.response);
            var oldListOfWorkLocation, populatedListOfWorkLocation;
            if (isSDO) {
                oldListOfWorkLocation = component.get("v.listOfSDOProjectWorklocation");
                populatedListOfWorkLocation = this.populateId(oldListOfWorkLocation, updatedListOfWorkLocation);
                component.set("v.listOfSDOProjectWorklocation", populatedListOfWorkLocation);
                component.set("v.listOfProjectSDOToBeDeleted", null);
            } else {
                oldListOfWorkLocation = component.get("v.listOfProjectWorklocation");
                populatedListOfWorkLocation = this.populateId(oldListOfWorkLocation, updatedListOfWorkLocation);
                component.set("v.listOfProjectWorklocation", populatedListOfWorkLocation);
                component.set("v.listOfProjectWorklocationToBeDeleted", null);
            }
            this.setIsEditableToFalse(component, isSDO);
            this.setWorkLocationProgressStatus(component);
            this.fireProjectSaveEvent(true);
            this.showToast('SUCCESS', 'success', responseData.message);
        } else {
            this.handleWorkLocationFailedCallback(component, responseData);
        }
    },
    populateId: function(oldListOfWorkLocation, updatedListOfWorkLocation) {
        for (var i = 0; i < oldListOfWorkLocation.length; i += 1) {
            oldListOfWorkLocation[i]["Id"] = updatedListOfWorkLocation[i]["Id"];
        }
        return oldListOfWorkLocation;
    },
    setWorkLocationProgressStatus: function(component) {
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");

        if ((listOfProjectWorklocation && listOfProjectWorklocation.length > 0) ||
            (listOfSDOProjectWorklocation && listOfSDOProjectWorklocation.length > 0)) {
            component.set("v.workLocationStatus", 'dirty');
        }
    },
    setIsEditableToFalse: function(component, isSDO) {
        var projectWorkLocation;
        if (isSDO) {
            projectWorkLocation = component.get("v.listOfSDOProjectWorklocation");
        } else {
            projectWorkLocation = component.get("v.listOfProjectWorklocation");
        }

        for (var i = 0; i < projectWorkLocation.length; i += 1) {
            projectWorkLocation[i]["isEditable"] = false;
        }

        if (isSDO) {
            component.set("v.listOfSDOProjectWorklocation", projectWorkLocation);
        } else {
            component.set("v.listOfProjectWorklocation", projectWorkLocation);
        }

    },
    deleteProjectWorkLocationService: function(component, projectWorkLocationId, worklocationIndex, isSDO) {
        var deleteProjectWorkLocation = component.get("c.deleteProjectWorkLocation");
        var deleteHSNDetails = false;
        
        //HSN Changes
        if(!this.validateGSTApplicable(component, worklocationIndex, isSDO)) {
            deleteHSNDetails = true;
			// No HSN Changes.
            this.closeModelHelper(component);
        }
        
        this.showSpinner(component);
        
        deleteProjectWorkLocation.setParams({
            "projectWorkLocationId": projectWorkLocationId,
            "deleteHSNDetails" : deleteHSNDetails
        });
        
        deleteProjectWorkLocation.setCallback(this, function(response) {
            this.deleteProjectWorkLocationHandler(response, component, worklocationIndex, isSDO);
        });
        
        $A.enqueueAction(deleteProjectWorkLocation);
    },
    deleteProjectWorkLocationHandler: function(response, component, worklocationIndex, isSDO) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            //Splice the deleted work location from local list
            var listOfProjectWorklocation;
            if (isSDO) {
                listOfProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
                listOfProjectWorklocation.splice(worklocationIndex, 1);
                component.set("v.listOfSDOProjectWorklocation", listOfProjectWorklocation);
            } else {
                listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
                listOfProjectWorklocation.splice(worklocationIndex, 1);
                component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
            }

            this.fireProjectSaveEvent(true);
            this.showToast('SUCCESS', 'success', responseData.message);
        } else {
            this.handleWorkLocationFailedCallback(component, responseData);
        }
    },
    handleWorkLocationFailedCallback: function(component, responseData) {

        var errorMessage = responseData.message || this.DEFAULT_ERROR_MESSAGE;
        if (errorMessage.indexOf("FIELD_CUSTOM_VALIDATION_EXCEPTION") >= 0) {
            var errors = errorMessage.split(",");
            var customErrorMessage = errors[1];

            customErrorMessage = customErrorMessage.substring(0, customErrorMessage.indexOf(":"));

            this.showToast('Error', 'error', customErrorMessage);
        } else {
            this.showToast('Error', 'error', errorMessage);
        }
    },
    getRelatedDataFordditionalSDO: function(component, additionalSDOId) {
        var getRelatedDataForWorkLocation = component.get("c.getRelatedDataForWorkLocation");
        getRelatedDataForWorkLocation.setParams({
            "workLocationId": additionalSDOId
        });

        this.showSpinner(component);

        getRelatedDataForWorkLocation.setCallback(this, function(response) {
            this.getRelatedDataForWorkLocationHandler(component, response);
        });

        $A.enqueueAction(getRelatedDataForWorkLocation);
    },
    getRelatedDataForWorkLocationHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var additionalSDO = JSON.parse(responseData.response);
            var activeAdditionalSDOIndex = component.get("v.activeAdditionalSDOIndex");
            var isWorkLocationActive = component.get("v.isWorkLocationActive");
            var listOfSDOProjectWorklocation;

            if (isWorkLocationActive) {
                listOfSDOProjectWorklocation = component.get("v.listOfProjectWorklocation");
            } else {
                listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
            }

            listOfSDOProjectWorklocation[activeAdditionalSDOIndex]["GP_Work_Location__r"] = additionalSDO;

            if (isWorkLocationActive) {

                component.set("v.isWorkLocationActive", false);

                component.set("v.listOfProjectWorklocation", listOfSDOProjectWorklocation);
            } else {
                component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
            }
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    // HSN Changes
    validateIsHSNApplicable : function(component, listOfWLs) {
        debugger;
        var response = {
            isValid : true,
            isHSNNeeded : true
        };
        //var isValid = true;
        var hsnCatId = component.get("v.hsnCatId");
        // ECR-HSN Changes.
        var isGSTNeeded = component.get("v.hsnMapWL.isGSTNeeded");
        
        if(isGSTNeeded) {
            // Validations will be executed regardless of stage.        
            // OU has GST enabled but HSN not selected at project level.
            if(component.get("v.hsnMapWL.isGstApplicable") !== 'false'
               && component.get("v.selectedHSN") === 'undefined-undefined') {
                response['isValid'] = false;
            }
            
            // When GST WL is selected for non GST OU.
            if(component.get("v.hsnMapWL.isGstApplicable") === 'false' &&
               this.isIndianWorkLocation(component, listOfWLs) &&
               $A.util.isUndefinedOrNull(hsnCatId)) {
                response['isValid'] = false;
                response['isHSNNeeded'] = true;
            }
            
            if(component.get("v.hsnMapWL.isGstApplicable") === 'false' &&
               !this.isIndianWorkLocation(component, listOfWLs) &&
               $A.util.isUndefinedOrNull(hsnCatId)) {
                response['isValid'] = true;
                response['isHSNNeeded'] = false;
            }
            
            // User is on WL page for WL updates.
            if(component.get("v.hsnMapWL.isGstApplicable") === 'false' &&
               !this.isIndianWorkLocation(component, listOfWLs) &&
               !$A.util.isUndefinedOrNull(hsnCatId)) {
                response['isValid'] = true;
                response['isHSNNeeded'] = false;
            }
            
            // User is again coming from Project page to WL page.
            if(component.get("v.hsnMapWL.isGstApplicable") !== 'false' &&
               !this.isIndianWorkLocation(component, listOfWLs) &&
               component.get("v.hsnMapWL.isGstByOU") === 'false') {
                response['isValid'] = true;
                response['isHSNNeeded'] = false;
            }
            
            /*if(component.get("v.isGstApplicableWorkLoc") !== 'false' &&
              this.isIndianWorkLocation(component, listOfWLs) &&
              component.get("v.isGstByOU") === 'false') {
                response['isValid'] = true;
                response['isHSNNeeded'] = true;
            }*/
            //}
        } else {
            response['isValid'] = true;
            response['isHSNNeeded'] = false;
        }
        
        return response;
    },
    // HSN Changes
    isIndianWorkLocation : function(component, listOfWLs) {
        var isGSTLoc = false;
        
        var gstEnabledOUs = component.get("v.listOfGSTEnabledOUs");        
        
        if(gstEnabledOUs.length > 0) {
            for(var i=0; i<listOfWLs.length; i++) {
                var isWLGST = false;
                for(var j=0; j<gstEnabledOUs.length; j++) {
                    if(listOfWLs[i].GP_Work_Location__r.GP_Legal_Entity_Code__c === gstEnabledOUs[j]['externalParameter3']) {
                        isWLGST = true;
                        break;
                    }
                }
                
                if(isWLGST) {
                    isGSTLoc = true;
                    break;
                }
            }
        }
        
        return isGSTLoc;
    },
    // HSN Changes
    saveworkLocationProjectHelper : function(component) {
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        if (!listOfProjectWorklocation ||
            listOfProjectWorklocation.length === 0) {
            this.showToast("Info", "info", "Please add atleast one Project Work location");
            return;
        }
        var validationResponse = this.validateWorkLocation(component, listOfProjectWorklocation);
        if (validationResponse["isValid"] && 
            validationResponse["listOfWorkLocationToBeSaved"] && 
            validationResponse["listOfWorkLocationToBeSaved"].length > 0) {
            component.set("v.isSetUsingOMS", false);
            this.saveWorkLocation(component, validationResponse["listOfWorkLocationToBeSaved"], validationResponse["listOfWorkLocationToBeDeleted"], false);
        } else if (validationResponse["errorMessage"]) {
            this.showToast('Error', 'error', validationResponse["errorMessage"]);
        }
    },
    // HSN Changes
    validateGSTApplicable : function(component, worklocationIndex, isSDO) {
        debugger;
        var isValidForGST = true;
        var isGstByOU = component.get("v.hsnMapWL.isGstByOU");
        var isGSTNeeded = component.get("v.hsnMapWL.isGSTNeeded");
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        var tempList = [];
        
        if(!isSDO && isGSTNeeded && isGstByOU === 'false') {
            var projectWorklocationTobeDeleted = worklocationIndex != null ? listOfProjectWorklocation[worklocationIndex] : null;
            
            if(projectWorklocationTobeDeleted != null) {
                for(var i=0; i<listOfProjectWorklocation.length; i++) {
                    if(projectWorklocationTobeDeleted["Id"] !== listOfProjectWorklocation[i]["Id"]) {
                        tempList.push(listOfProjectWorklocation[i]);
                    }
                }
            } else {
                tempList = listOfProjectWorklocation;
            }
            
            isValidForGST = this.isIndianWorkLocation(component, tempList);
        } else if(!isGSTNeeded) {
            // ECR-HSN Changes.
            isValidForGST = false;
        }
        
        return isValidForGST;
    },
	// No HSN Changes
    closeModelHelper: function(component) {
    	component.set("v.isWorkLocationModal",false);
        component.set("v.hsnCatId", null);
        component.set("v.hsnCatName", null);
        component.set("v.selectedHSN", 'undefined-undefined');
        component.set("v.isHSNSelected", false);
    }  
})