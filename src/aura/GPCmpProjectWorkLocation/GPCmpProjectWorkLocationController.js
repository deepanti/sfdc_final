({
    doInit: function(component, event, helper) {
        helper.getWorklocationData(component);
    },
    addToWorkLocationProject: function(component, event, helper) {

        var projectWorkLocationRecordType = component.get("v.projectWorkLocationRecordType");
        var projectId = component.get("v.recordId");
        var projectData = component.get("v.project");
        var bcpRequired;
        // if (projectData && projectData.length > 0 && projectData[0]["recordTypeInfo"] 
        //     && projectData[0]["recordTypeInfo"]["name"] === "BPM") {
        //     bcpRequired = "No";
        // }
        var workLocationProject = {
            "isEditable": true,
            "RecordTypeId": projectWorkLocationRecordType[1]["Id"],
            "GP_Project__c": projectId,
            "GP_BCP__c": bcpRequired
        };
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        listOfProjectWorklocation.push(workLocationProject);

        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
    },
    addToSDOProject: function(component, event, helper) {
        var projectWorkLocationRecordType = component.get("v.projectWorkLocationRecordType");
        var projectId = component.get("v.recordId");
        var projectData = component.get("v.project");
        var bcpRequired;
        // if (projectData && projectData.length > 0 && projectData[0]["recordTypeInfo"] && projectData[0]["recordTypeInfo"]["name"] === "BPM") {
        //     bcpRequired = "No";
        // }
        var SDOProject = {
            "isEditable": true,
            "RecordTypeId": projectWorkLocationRecordType[0]["Id"],
            "GP_Project__c": projectId,
            "GP_BCP__c": bcpRequired
        };
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        listOfSDOProjectWorklocation.push(SDOProject);

        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);

    },
    // HSN Changes
    saveworkLocationProject: function(component, event, helper) {
        helper.saveworkLocationProjectHelper(component);
    },
    saveSDOProject: function(component, event, helper) {
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        if (!listOfSDOProjectWorklocation ||
            listOfSDOProjectWorklocation.length === 0) {
            helper.showToast("Info", "info", "Please add atleast one Additional SDO");
            return;
        }

        var validationResponse = helper.validateSDO(component, listOfSDOProjectWorklocation);

        if (validationResponse["isValid"] && 
            validationResponse["listOfSDOToBeSaved"] && 
            validationResponse["listOfSDOToBeSaved"].length > 0) {
            helper.saveWorkLocation(component, validationResponse["listOfSDOToBeSaved"], validationResponse["listOfSDOToBeDeleted"], true);
        } else if (validationResponse["errorMessage"]) {
            helper.showToast('Error', 'error', validationResponse["errorMessage"]);
        }
    },
    removeWorkLocation: function(component, event, helper) {

        if (component.get("v.project[0].fields.GP_Approval_Status__c.value") === 'Approved' ||
            component.get("v.project[0].fields.GP_Approval_Status__c.value") === 'Pending for Approval' ||
            !component.get("v.isEditable")) {
            return;
        }

        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        var worklocationIndex = Number(event.currentTarget.id);
        var projectWorklocationTobeDeleted = listOfProjectWorklocation[worklocationIndex];

        if(!projectWorklocationTobeDeleted["Id"]) {

            listOfProjectWorklocation.splice(worklocationIndex, 1);
            component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);

            if (listOfProjectWorklocation.length === 0) {
                component.set("v.workLocationStatus", 'unTouched');
            }
        } else if(confirm("Are you sure you want to delete?")) {
            helper.deleteProjectWorkLocationService(component, projectWorklocationTobeDeleted["Id"], worklocationIndex);
        }

    },
    removeSDO: function(component, event, helper) {

        if (component.get("v.project[0].fields.GP_Approval_Status__c.value") === 'Approved' ||
            component.get("v.project[0].fields.GP_Approval_Status__c.value") === 'Pending for Approval' ||
            !component.get("v.isEditable")) {
            return;
        }

        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        var sdoIndex = Number(event.currentTarget.id);
        var projectWorklocationTobeDeleted = listOfSDOProjectWorklocation[sdoIndex];
        
        if(!projectWorklocationTobeDeleted["Id"]) {
            listOfSDOProjectWorklocation.splice(sdoIndex, 1);
            component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
        } else if(confirm("Are you sure you want to delete?")) {
            helper.deleteProjectWorkLocationService(component, projectWorklocationTobeDeleted["Id"], sdoIndex, true);
        }
    },
    editWorkLocationRow: function(component, event, helper) {
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        var worklocationIndex = Number(event.currentTarget.id);

        listOfProjectWorklocation[worklocationIndex]["isEditable"] = true;
        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
    },
    editSDORow: function(component, event, helper) {
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        var sdoIndex = Number(event.currentTarget.id);

        listOfSDOProjectWorklocation[sdoIndex]["isEditable"] = true;
        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
    },
    updateWorkLocationRow: function(component, event, helper) {
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        var worklocationIndex = Number(event.currentTarget.id);

        listOfProjectWorklocation[worklocationIndex]["isEditable"] = false;
        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
    },
    updateSDORow: function(component, event, helper) {
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        var sdoIndex = Number(event.currentTarget.id);

        listOfSDOProjectWorklocation[sdoIndex]["isEditable"] = false;
        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
    },
    editWorkLocationProject: function(component, event, helper) {
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");
        for (var i = 0; listOfProjectWorklocation && i < listOfProjectWorklocation.length; i += 1) {
            listOfProjectWorklocation[i]["isEditable"] = true;
        }
        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);
    },
    editSdoWorkLocationProject: function(component, event, helper) {
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");
        for (var i = 0; listOfSDOProjectWorklocation && i < listOfSDOProjectWorklocation.length; i += 1) {
            listOfSDOProjectWorklocation[i]["isEditable"] = true;
        }
        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
    },
    clearWorkLocation: function(component, event, helper) {
        var externalParameter = event.getParam("externalParameter");
        var listOfProjectWorklocation = component.get("v.listOfProjectWorklocation");

        listOfProjectWorklocation[externalParameter]["GP_Work_Location__c"] = null;
        listOfProjectWorklocation[externalParameter]["GP_Work_Location__r"] = null;

        component.set("v.listOfProjectWorklocation", listOfProjectWorklocation);

    },
    clearSDO: function(component, event, helper) {
        var externalParameter = event.getParam("externalParameter");
        var listOfSDOProjectWorklocation = component.get("v.listOfSDOProjectWorklocation");

        listOfSDOProjectWorklocation[externalParameter]["GP_Work_Location__c"] = null;
        listOfSDOProjectWorklocation[externalParameter]["GP_Work_Location__r"] = null;

        component.set("v.listOfSDOProjectWorklocation", listOfSDOProjectWorklocation);
    },
    activateWorkLocation: function(component, event, helper) {
        component.set("v.isSDOActive", false);
        var workLocationSideBar = component.find("worklocation_sidebar");
        var sdoSideBar = component.find("SDO_sidebar");

        $A.util.addClass(workLocationSideBar, 'active');
        $A.util.removeClass(sdoSideBar, 'active');
    },
    activateSDO: function(component, event, helper) {
        component.set("v.isSDOActive", true);

        var workLocationSideBar = component.find("worklocation_sidebar");
        var sdoSideBar = component.find("SDO_sidebar");

        $A.util.removeClass(workLocationSideBar, 'active');
        $A.util.addClass(sdoSideBar, 'active');
    },
    updateAdditionalSDOlocation: function(component, event, helper) {
        var additionalSDOID = event.getParam("sObjectId");
        var activeAdditionalSDOIndex = event.getParam("externalParameter");

        component.set("v.activeAdditionalSDOIndex", activeAdditionalSDOIndex);

        helper.getRelatedDataFordditionalSDO(component, additionalSDOID);
    },
    updateWorkLocationlocation: function(component, event, helper) {
        var worklocationID = event.getParam("sObjectId");
        var activeWorklocationIndex = event.getParam("externalParameter");

        component.set("v.activeAdditionalSDOIndex", activeWorklocationIndex);
        component.set("v.isWorkLocationActive", true);
        helper.getRelatedDataFordditionalSDO(component, worklocationID);
    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },
    // ECR-HSN Changes
    closeModel: function(component, event, helper) {
    	helper.closeModelHelper(component);
    },
    // HSN Changes
    saveHSNandCloseModel: function(component, event, helper) {
        var fieldElement;
        var hsnid = component.get("v.hsnCatId");
        //var hsnname = component.get("v.hsnCatName");
        
        if(!$A.util.isUndefinedOrNull(hsnid)) {
            //component.set("v.HsnCatId", hsnid );
            //component.set("v.HsnCatName", hsnname );
            component.set("v.isWorkLocationModal",false);
            // Auto save the WLs
            helper.saveworkLocationProjectHelper(component);
        } else {
            fieldElement = component.find('GP_HSN_Category_Id__c');
            
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Required.';
            }
        }
    },
    // HSN changes
    hsnSelectHandler: function(component, event, helper) {
        
        var selectedHSN = event.getSource().get("v.text");
        var hsnId, hsnName;
        
        if ($A.util.isEmpty(selectedHSN)) {
            hsnId = null;
            hsnName = null;
        } else {
            hsnId = selectedHSN.split("-")[0];
            hsnName = selectedHSN.split("-")[1];
        }
        
        component.set("v.hsnCatId", hsnId);
        component.set("v.hsnCatName", hsnName);
        component.set("v.selectedHSN", selectedHSN);
        component.set("v.isHSNSelected", true);
        //component.set("v.isGstByWL", true);
    } 
})