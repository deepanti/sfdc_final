({	
    
   getProducts : function(component, page){
        var page = page || 1;
    	var recordId = component.get("v.recordId");
        var action = component.get("c.getProducts");
        action.setParams({
            "opportunityID" : recordId,            
        });
        
        action.setCallback(this, function(response){ 
            var state = response.getState();
            if(state === 'SUCCESS'){
               var result = response.getReturnValue();
               this.setProductInWrapperList(component, result, page);
             }    
        });
        $A.enqueueAction(action);    
    },
    
   /* getLoggedInUserDetail : function(component){
       var action = component.get("c.getLoggedInUserDetail");
        action.setCallback(this, function(response){
            component.set("v.loggedInUser", response.getReturnValue());
        });
        $A.enqueueAction(action);    
    },*/
    
    getOperators : function(component){
        component.set("v.operatorList", ["equals", "not equal to",
                                      	"less than", "greater than",
                                      	"less or equal", "greater or equal",
                                      	"contains", "does not contain",
                                      	"starts with"]);
    },
    
    getProductFields : function(component){
        var action = component.get("c.getProductFields");
        action.setCallback(this, function(response){
            component.set("v.productFields", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    getOpportunityClosedDate : function(component, event, helper){
       var action = component.get("c.getOpportunityClosedDate");
       var recordId = component.get("v.recordId");
        action.setParams({
            	"opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){     
            var closedDate = new Date(response.getReturnValue().closedDate);
            var todayDate = new Date();
            var loggedInUser = response.getReturnValue().loggedInUser;
            //var loggedInUser = component.get('v.loggedInUser');
            if(closedDate.getTime() < todayDate.getTime() && loggedInUser.Profile.Name != 'Genpact Super Admin'){
                component.set("v.errorMessage", "Please update MSA/SOW date of Opportunity later than today.");
                component.set("v.isError", true);
                component.set("v.isDisabled", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),5000
                );
            } 
           
        });
        $A.enqueueAction(action); 
    },
    
    setProductInWrapperList : function(component, result, page){
       // alert(JSON.stringify(result));
    	var productList = result.productList;
        var index;
        var product;
        var totalProductList = [];
        for(index in productList){
            product = productList[index];
            var productWrapper ={
                "product" : product,                       
                "isSelect" : false
            } 
            totalProductList.push(productWrapper);
        }
        component.set("v.page",page);
        component.set("v.total",result.total);
        //alert(component.get("v.total"));
        component.set("v.pages",Math.ceil(result.total/component.get("v.pageSize")));
        component.set('v.selectAllFlag', false);
        component.set("v.totalProductList", totalProductList);
        this.getProductFromWrapperList(component, page);   
        component.set("{!v.isLoading}", false);
    },
    
    getProductFromWrapperList : function(component, page){
        var productWrapperList = [];
        var req_list = [];
        var pageSize = component.get("v.pageSize");
        var totalList = component.get("v.totalProductList");
        if(page === 1){
        	req_list = totalList.slice(0, pageSize);    
        }
        else{
            var offset = (+page -1) * (+pageSize);
        	req_list = totalList.slice(offset , (+pageSize * +page));    
        }
        component.set("v.page",page);
       	component.set("v.productWrapperList", req_list);
    },
    
    
    
})