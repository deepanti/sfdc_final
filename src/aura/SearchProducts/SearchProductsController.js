({
  init: function (component, event, helper) { 
     // alert('on page load');
      helper.getProducts(component); 
      helper.getOperators(component);
      helper.getProductFields(component);
      helper.getOpportunityClosedDate(component);
     // helper.getLoggedInUserDetail(component);
    },
    
    SearchProducts : function(component, event, helper){
        var page = page || 1;
        var keyword = component.find("keyword").get("v.value");
        var filterList = component.get("v.filterList");
        var recordId = component.get("v.recordId");  
       	var filter = filterList[0];   
        
        if(keyword && (!filter.fieldName && !filter.operator)){
            var action = component.get("c.getSearchedProduct");
        	action.setParams({
            	"opportunityID" : recordId,
                "searchFilter" : "BY_KEYWORD",
            	"keyword" : keyword
        	});
             action.setCallback(this, function(response){ 
               
                var state = response.getState();
            	if(state === 'SUCCESS'){
               		var result = response.getReturnValue();
                    helper.setProductInWrapperList(component, result, page);
                }    
            });
        }        
        else if(filter.fieldName && filter.operator && !keyword){
            var action = component.get("c.getSearchedProduct");
           action.setParams({
            	"opportunityID" : recordId,
               	"searchFilter" : "BY_FILED_FILTER",
            	"keyword" : JSON.stringify(filterList)
        	});  
            action.setCallback(this, function(response){
                var state = response.getState();
            	if(state === 'SUCCESS'){
               		var result = response.getReturnValue();
               		helper.setProductInWrapperList(component, result, page);
                }  
            });
        }
        else if(filter.fieldName && filter.operator && keyword){
          var action = component.get("c.getSearchedProduct2");
          action.setParams({
            	"opportunityID" : recordId,
               	"searchFilter" : "BY_BOTH",
            	"keyword" : keyword,
              	"filterList" : JSON.stringify(filterList)
        	});  
            action.setCallback(this, function(response){
                var state = response.getState();
            	if(state === 'SUCCESS'){
               		var result = response.getReturnValue();
               		helper.setProductInWrapperList(component, result, page);
                }  
            });      
        }
        else{
              location.reload();
        }        
        $A.enqueueAction(action);
    },
    
    getSelectedProduct : function(component, helper){          
      
        var selectedProductList = component.get("v.selectedProductList");       
        if(selectedProductList.length == 0){
        	component.set("v.errorMessage", "Please select atleast one Product");
        	component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            );
        }
        else{
            var event = $A.get("e.c:NavigateToAddProduct");
        	event.setParams({
                "productList" : selectedProductList,
                "recordId" : component.get("v.recordId")  
     		});
             event.fire();
        }
    },
    
    changeRowSelection : function(component, event, helper){
        var productID = event.target.id;
        var checkBoxValue = document.getElementById(event.target.id).checked;
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        
        var productList = component.get("v.productWrapperList");
        
        if(checkBoxValue){
            for(index in productList){
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    productWrapper.isSelect = true;
                	selectedProductList.push(productWrapper.product);
                }
            }
            component.set("v.selectedProductList", selectedProductList);
        }
        else{
            component.set("v.selectAllFlag", false);
            for(index in productList){
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    productWrapper.isSelect = false;
                	selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productID),1);
                }
            }
            component.set("v.selectedProductList", selectedProductList);
        } 
	},
    
    handleClickCancel : function(component, event, helper){
    	var recordId = component.get("v.recordId");
        function isLightningExperienceOrSalesforce1() {
        	return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
        if(isLightningExperienceOrSalesforce1()) {
        	sforce.one.navigateToSObject(recordId,"OPPORTUNITY");
        }
        else{
            window.location = '/'+recordId;
        }
    },

    selectAllProducts : function(component, event, helper){
     	var selectallFlag = component.get("v.selectAllFlag");
        var productList = component.get("v.productWrapperList");
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        
        if(selectallFlag){
        	for(index in productList){
                var productWrapper = productList[index];
                productWrapper.isSelect = true;
            	document.getElementById(productWrapper.product.Id).checked = true;
                if(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id) === -1) {
            		selectedProductList.push(productWrapper.product);
                }    
           	}
        }
        else{
            for(index in productList){
                var productWrapper = productList[index];
            	document.getElementById(productWrapper.product.Id).checked = false;
            	productWrapper.isSelect = false;
                selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id),1);
        	}
         }
        component.set("v.selectedProductList", selectedProductList);
    },

    clearallfilters :  function(component, event, helper){
        component.find("keyword").set("v.value", "");
       	var childCmp = component.find("rowFilter")
 		childCmp.resetValues();
        helper.getProducts(component);
    },
    
    pageChange : function(component, event, helper) {
        var page = component.get("v.page") || 1;
        var pages = component.get("v.pages");
       	var direction = event.getParam("direction");
        
        if(direction === "previous"){
        	page =  page - 1;
            helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "next"){
        	page = page + 1;  
             helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "first"){
        	page = 1;    
             helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "last"){
            page = pages; 
             helper.getProductFromWrapperList(component,page);
        }       
       component.set("v.selectAllFlag", false);
    },
    
    goToProductPage : function(component,event, helper){
    	var productID = event.target.id; 
        function isLightningExperienceOrSalesforce1() {
        	return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
        if(isLightningExperienceOrSalesforce1()) {
        	sforce.one.navigateToSObject(productID,"PRODUCT2");
        }
        else{
            window.location = '/'+productID;
        } 
    },
    
    closeErrorToast : function(component, event, helper){
    	component.set("v.isError", false);
	}
})