({
    
    doInit : function(component, event, helper) {
       var opportunityListPending=component.get('v.opportunityListPending');
       component.set("v.oppid",opportunityListPending[0]);
        var oppid=component.get("v.oppid");
      //alert(oppid);
              
      
	},  
    
    getOppId : function(component, event, helper) {
        var oppId = event.target.id;
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "oppId" : oppId
        });
        cmpEvent.fire();
	},
    
    
    selectAllOppLI : function(component, event, helper){
        
           var selectopp=[];
           var selectoli=[];
        
        /* var oppwrap = component.get("v.oppwrap");
         
        var checkBoxStatus = oppwrap.oppSelected;
        var opportunityLIWrapperObj = oppwrap.opportunityLIWrapperObj;
       
       
        
        for(var i=0; i < opportunityLIWrapperObj.length; i++) {
            opportunityLIWrapperObj[i].oppLISelected = checkBoxStatus;
            
            
        }
        alert(opportunityLIWrapperObj.length);
        
        oppwrap.opportunityLIWrapperObj = opportunityLIWrapperObj;
       
        component.set("v.oppwrap", oppwrap);*/
        
        var masterCheckAll = component.get("v.masterCheck");
        var checkBoxStatus;
        var opportunityListPending=component.get('v.opportunityListPending');
        
        for(var i=0; i < opportunityListPending.length; i++) {
             checkBoxStatus=opportunityListPending[i].oppSelected;
           
            if(opportunityListPending[i].oppSelected==true)
            {
                selectopp.push(opportunityListPending[i]);
                
            }
            
            for( var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
                
                opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected = checkBoxStatus;
                selectoli.push(opportunityListPending[i].opportunityLIWrapperObj[j]);
                
            }
            
        }
        
        component.set("v.ListOfOpp",selectoli);
        component.set('v.opportunityListPending',opportunityListPending);
       
        if(selectopp.length==opportunityListPending.length){
            component.set("v.masterCheck",true); 
        }
        else{
            component.set("v.masterCheck",false); 
        }
       
      /*  if(masterCheckAll == true && oppwrap.oppSelected == false) {
            component.set("v.masterCheck",false);
        }
       */
        
      
    },
   
    
    UpdateColor : function(component, event, helper)
    {       
    	
        
       helper.getOppOLIData(component, event, helper);
       var rowId = event.currentTarget.id;
       var x = document.getElementsByClassName("selectcolor");
       
       if(x.length > 0){
           for (var i = 0; i < x.length; i++) {
              x[i].classList.remove("selectcolor");
            }	
         }
        var Row = document.getElementById(rowId);
        Row.classList.add("selectcolor");
        
    
    },
    toggleventresult : function(component, event, helper){
        
      var oppOLIData = event.getParam("oliid");
      component.set('v.opportunityListPending', oppOLIData);
      
      
    }, 
        
        
    
   
    
    
})