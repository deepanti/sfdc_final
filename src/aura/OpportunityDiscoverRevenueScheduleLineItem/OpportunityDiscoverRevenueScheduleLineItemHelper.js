({
	updateList : function(component, event, helper) {
        var OLISList = component.get('v.OLISList');
        var wrapperIndex = component.get('v.wrapperIndex');
        var appEvent = $A.get("e.c:OpportunityDiscoverOLISEvent"); 
        //Set event attribute value 
        appEvent.setParams({
            "OLISList":OLISList, 
            "wrapperIndex":wrapperIndex
        }); 
        appEvent.fire();
    }
})