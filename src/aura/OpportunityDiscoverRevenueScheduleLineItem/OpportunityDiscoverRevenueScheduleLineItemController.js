({
    updateList : function(component, event, helper) {
        var revenueval = component.get("v.OLISList");
        var inputComp = component.find("ind");
        var isCorrect = true;
        for(var i=0;i<revenueval.length;i++)
        {
            if(revenueval[i].Revenue == '')
            {
                $A.util.addClass(inputComp[i],'slds-has-error');
                isCorrect = false;
            }
            else
            {
                $A.util.removeClass(inputComp[i],'slds-has-error');
            }
        }	
        if(isCorrect)
        {
            helper.updateList(component, event, helper);    
        }
        else
        {
            var toastEvent = $A.get("e.force:showToast");  
            toastEvent.setParams({  
                "title": "Error!",  
                "message": 'Monthly revenue cannot be blank or less than zero',  
                "type": "ERROR"  
            });  
            toastEvent.fire();
        }
    },
    doInit : function(component, event, helper) {
        var unitTCV = component.get("v.TCV");
        if(unitTCV>=1000000000)
        {
            component.set("v.TCV",(unitTCV/1000000000).toFixed(2) +"B");
        }
        else if(unitTCV>=1000000)
        {
            component.set("v.TCV",(unitTCV/1000000).toFixed(2) +"M");
        }
            else if(unitTCV>=1000)
            {
                component.set("v.TCV",(unitTCV/1000).toFixed(2) +"K");
            }
                else
                    component.set("v.TCV",unitTCV);
        
        var OLISList = component.get("v.OLISList");
        var newList = [];
        var lowest;
        for(var i=0;i<OLISList.length;i++)
        {
            lowest = OLISList[i];
            for(var j=i;j< OLISList.length;j++)
            {                
            	if(lowest.ScheduleDate > OLISList[j].ScheduleDate)
                {
             		lowest = OLISList[j];   
                    OLISList[j] = OLISList[i];
                    OLISList[i] = lowest;
                }
            }
        }
        component.set("v.OLISList",OLISList);
    }
    
})