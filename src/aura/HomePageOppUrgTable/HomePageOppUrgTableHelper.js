({
    fireToastSuccess : function(message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
    handleConfirmationForPrediscover:function(component,event,helper,oppId){
        var action = component.get("c.updateStageToPrediscover");
        component.set("v.spinner",true);
        action.setParams(
            {
                OppId:oppId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.spinner",false);
                helper.fireToastSuccess("Stage changed to prediscover"); 
                var compEvent = component.getEvent("RefreshEvent");
                compEvent.fire();
            }
            else
            {
                component.set("v.spinner",false);
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    }
})