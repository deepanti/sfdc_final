({
    handleStageChange:function(component, event, helper) {
        var oppLst = component.get("v.oppList");  
        var oppId = oppLst[event.getSource().get("v.name")].opp.Id;
        if(event.getParam("value")=='Prediscover')
        {
            if(confirm('This opportunity will be moved to Prediscover Stage. Are you sure?'))
                helper.handleConfirmationForPrediscover(component, event, helper,oppId);
        }
        else
        {
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": oppId
            });
            navEvt.fire();            
        }
    },
    closeModal : function(component,event,helper)
    {
        component.set("v.showConfirmAction",false);
        component.find("ConfirmationModal").set("v.recordId",null);
    },
    dateChange: function(component, event, helper)
    {
        var oppLst = component.get("v.oppList");
        var action = component.get("c.updateClosureDate");
        if(Date.parse(oppLst[event.target.id].opp.CloseDate)<=new Date())
            helper.fireToastError('The date selected must be later than today');
        else
        {
            component.set("v.spinner",true);
            action.setParams(
                {
                    closedDate :oppLst[event.target.id].opp.CloseDate,
                    oppID:oppLst[event.target.id].opp.Id
                });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    //TODO: remove this item from the list
                    //$A.get('e.force:refreshView').fire();
                    component.set("v.spinner",false);
                    var compEvent = component.getEvent("RefreshEvent");
                    compEvent.setParams({"isRefresh" :true});
                    compEvent.fire();
                    helper.fireToastSuccess("MSA/SOW Closure date is updated"); 
                }
                else
                {
                    component.set("v.spinner",false);
                    helper.fireToastError(response.getError()[0].message); 
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    changeStage: function(component, event, helper)
    {
        var oppLst = component.get("v.oppList");
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": oppLst[event.target.id].opp.Id
        });
        editRecordEvent.fire();
    },
    openOpportunity : function(component, event, helper)
    {
        
        var oppLst = component.get("v.oppList");
        var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
            "recordId": oppLst[event.getSource().get("v.label")].opp.Id
        });
        navEvt.fire();
    },
    followUpReminderQSRM:function(component, event, helper)
    {
        var oppLst = component.get("v.oppList");
        var action = component.get("c.sendFollowUpEmail");
        component.set("v.spinner",true);
        action.setParams(
            {
                userList :oppLst[event.target.id].reminderUserList,
                orgWideEmail :component.get("v.orgWideEmail") ,
                templateId:component.get("v.emailTemplateQSRM"),
                recordId:oppLst[event.target.id].qsrmId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                component.set("v.spinner",false);
                helper.fireToastSuccess("Follow Up Reminder Sent"); 
            }
            else
            {
                component.set("v.spinner",false);
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },
    followUpReminderContract:function(component,event,helper)
    {
        var oppLst = component.get("v.oppList");
        var action = component.get("c.sendFollowUpEmail");
        component.set("v.spinner",true);
        action.setParams(
            {
                userList :oppLst[event.target.id].reminderUserListContract,
                orgWideEmail :component.get("v.orgWideEmail") ,
                templateId:component.get("v.emailTemplateContract"),
                recordId:oppLst[event.target.id].contractId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {    
                component.set("v.spinner",false);
                helper.fireToastSuccess("Follow Up Reminder Sent"); 
            }
            else
            {
                component.set("v.spinner",false);
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
        
    },
    followUpReminderMSA:function(component, event, helper)
    {
        
        var oppLst = component.get("v.oppList");
        var action = component.get("c.sendFollowUpEmail");
        var msaTemplate;
        component.set("v.spinner",true);
        if(oppLst[event.target.id].status.includes('past'))
        {
            msaTemplate=component.get("v.emailTemplateMSAPast");
        }
        else
        {
            msaTemplate=component.get("v.emailTemplateMSA");
        }
        action.setParams(
            {
                userList :oppLst[event.target.id].followUpRemainderList,
                orgWideEmail :component.get("v.orgWideEmail") ,
                templateId:msaTemplate,
                recordId:oppLst[event.target.id].opp.Id
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
				component.set("v.spinner",false);                
                helper.fireToastSuccess("Follow Up Reminder Sent"); 
            }
            else
            {
                component.set("v.spinner",false);
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
        
    },
    followUpReminderStage:function(component, event, helper)
    {
        var oppLst = component.get("v.oppList");
        var action = component.get("c.sendFollowUpEmail");
        component.set("v.spinner",true);
        action.setParams(
            {
                userList :oppLst[event.target.id].followUpRemainderList,
                orgWideEmail :component.get("v.orgWideEmail") ,
                templateId:component.get("v.emailTemplateStage"),
                recordId:oppLst[event.target.id].opp.Id
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {     
                component.set("v.spinner",false);
                helper.fireToastSuccess("Follow Up Reminder Sent"); 
            }
            else
            {
                component.set("v.spinner",false);
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    }
    
})