({
    doInit:function(component, event, helper) {
        // to show spinner on click of component & before load of component 
        helper.showSpinner(component, event, helper);
        helper.createGCICompononent(component, event, helper,$A.get("$Label.c.OpportunityRelationshipBarometer"));
        helper.hideSpinner(component, event, helper);
    },
    
    showTabItem: function(component, event, helper) {
        // displays respective items on click of tabs
        var tab = event.getParam("id");
        //if(tab="2")
           // component.set("v.secondTab", true);
        
        helper.showTabItem(component, event, helper,tab);
    },
    
    save : function(component, event, helper) {
        component.find("recordViewForm").submit();
    },
    hideSpinner : function(component,event,helper){
        helper.hideSpinner(component, event, helper);
    },
    saveAndNext : function(component, event, helper) {
        component.set("v.Spinner", true);
        var secondTab = component.get("v.secondTab");
        
        if(secondTab === false){
            component.set("v.SelectedTab", "2");
            helper.showTabItem(component, event, helper,"2");
            component.set("v.secondTab", true);
        }else{
            if(localStorage.getItem('MaxTabId')<7)
			{
                        component.set("v.MaxSelectedTabId", "7");
                        localStorage.setItem('MaxTabId',7);
                        component.find("recordViewForm").submit();
            }
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "7" });
			localStorage.setItem('LatestTabId',"7");
            compEvents.fire();
         
            window.scrollTo(0, 0);
            $A.get('e.force:refreshView').fire();//fix for QSRM component to refresh
        }
        component.set("v.Spinner", false);
    },
    back : function(component, event, helper) {
        var secondTab = component.get("v.secondTab");
        
        if(secondTab === true){
            component.set("v.SelectedTab", "1");
            helper.showTabItem(component, event, helper,"1");
            component.set("v.Spinner", false);
            component.set("v.secondTab", false);
        }else{
            localStorage.setItem('LatestTabId',5);
            component.set("v.SelectedTabId", "5");
            var SelectedTabId = component.get("v.SelectedTabId");
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "5" });
            compEvents.fire();
            window.scrollTo(0, 0);
        }
        
    }
})