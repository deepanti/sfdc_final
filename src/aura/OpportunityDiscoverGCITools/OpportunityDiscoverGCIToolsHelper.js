({
    createGCICompononent:function(component, event, helper,pageName){
        // to create respective component with the VF page for relationbarometer & Pursuit Profiler
        $A.createComponent(
            "c:OpportunityVFPageRerender",
            { 
                "pagename": pageName,
                "recordId":  component.get("v.recordId"),
                "height" : '450px'
            },
            function(newComponent){
                if (component.isValid()) { 
                    component.set("v.body",newComponent);
                    helper.hideSpinner(component, event, helper);
                }
                else {
                    throw new Error(error);
                }
            }
        );
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner      
        component.set("v.Spinner", true); 
    }, 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    showTabItem :function(component,event,helper,tab){
        switch (tab) {
            case '1' :
                helper.createGCICompononent(component,event,helper,$A.get("$Label.c.OpportunityRelationshipBarometer"));
                break;
            case '2' :
                helper.createGCICompononent(component,event,helper,$A.get("$Label.c.OpportunityPursuitProfiler"));
                break;
        }
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    }
    
})