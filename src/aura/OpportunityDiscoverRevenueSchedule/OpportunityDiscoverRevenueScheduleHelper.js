({
    
    doInit: function(component, event, helper) {
        component.set("v.wrapperIndex",0);
        var action = component.get("c.getProductWrapperList");
        action.setParams({
            OppId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();		
            if(state === "SUCCESS"){
                component.set("v.wrapperList",response.getReturnValue());
                if(component.get("v.wrapperList").length >0){
                    component.set("v.productName",response.getReturnValue()[component.get('v.wrapperIndex')].productName);
                    component.set("v.unitPrice",response.getReturnValue()[component.get('v.wrapperIndex')].unitPrice);
                    component.set("v.natureOfWork",response.getReturnValue()[component.get('v.wrapperIndex')].natureOfWork);
                    component.set("v.serviceLine",response.getReturnValue()[component.get('v.wrapperIndex')].serviceLine);
                    component.set("v.OLIList",response.getReturnValue()[component.get('v.wrapperIndex')].opportunityLineItemSchedules);
                    component.set("v.productId", response.getReturnValue()[component.get('v.wrapperIndex')].productId);
                }  
            }
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(action);        
    },
    handleSave : function(component, event, helper, saveAndNext) {
        component.set("v.Spinner", true);
        var wrapperList = component.get("v.wrapperList");
        var OLISRevenue =[];
        for(var i=0;i<wrapperList.length;i++)
        {
            OLISRevenue = OLISRevenue.concat((wrapperList[i].opportunityLineItemSchedules));
        }
        
        component.set("v.OLIList1",OLISRevenue);
        var OLIList =  component.get('v.OLIList1');
        var action = component.get("c.saveProductRevenueSchedule");     
        action.setParams({
            "oLIList" : JSON.stringify(OLIList)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                localStorage.setItem('stopRecursiveUpdate','0');
                //set tabs
                var countOfUnitPrice=0;  
                for(var i=0;i<wrapperList.length;i++)
                {
                    for(var j=0;j<(wrapperList[i].opportunityLineItemSchedules).length;j++)
                    {
                        countOfUnitPrice += parseFloat(wrapperList[i].opportunityLineItemSchedules[j].Revenue);   
                    }
                    wrapperList[i].unitPrice = countOfUnitPrice.toFixed(2);
                    countOfUnitPrice=0;
                }
                component.set("v.wrapperList",wrapperList);
                localStorage.setItem('LatestTabId',"5");
                if(saveAndNext)
                {
                    if(localStorage.getItem('MaxTabId')<6)
                    {
                        component.set("v.MaxSelectedTabId", "6");
                        localStorage.setItem('MaxTabId',6);
                        component.find("recordViewForm").submit();
                    }
                    localStorage.setItem('LatestTabId',"6");
                    var SelectedTabId = component.get("v.SelectedTabId");
                    var compEvents = component.getEvent("componentEventFired");
                    compEvents.setParams({ "SelectedTabId" : "6" });
                    compEvents.fire();
                    window.scrollTo(0, 0);
                }
                else
                {
                    component.set("v.isRefresh",true);
                    $A.get("e.force:refreshView").fire();
                }
                component.set("v.Spinner", false);
                this.fireToastSuccess(component, event, helper,'Revenue Schedule Updated Successfully.');
            }
            else{
                this.fireToastError(component, event, helper, response.getError()[0].message);
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);        
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    }, 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    }
    
})