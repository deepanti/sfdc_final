({
    recordUpdated : function(component, event, helper){
        var changeType = event.getParams().changeType;
        if (changeType === "LOADED") { 
            /* handle record load */						
            var MaxSelectedTabId = component.get("v.OppRecord").LatestTabId__c;
            component.set("v.MaxSelectedTabId", MaxSelectedTabId);
        }else if(changeType === "CHANGED"){
            component.set("v.Spinner", true);
            helper.doInit(component, event, helper);
            var prodindex = document.getElementById('0_prodindex__item');
            if(prodindex != null && prodindex != 'undefined')
            	document.getElementById('0_prodindex__item').click();                     
        }
    },
    doInit : function(component, event, helper) {
        var isRefresh = component.get("v.isRefresh");
        if(!isRefresh)
        {
            helper.doInit(component, event, helper);
        }        
    }, 
    handleSave: function(component, event, helper){
        helper.handleSave(component, event, helper, false);
    },
    changeOLIRevenueList: function(component, event, helper){
        var OLIList = event.getParam("OLISList"); 
        var wrapperIndex = event.getParam("wrapperIndex");
        component.set('v.OLIList',OLIList); 
        component.set('v.wrapperIndex',wrapperIndex); 
        var wrapperList = component.get("v.wrapperList");
        wrapperList[component.get('v.wrapperIndex')].opportunityLineItemSchedules = component.get('v.OLIList');
       
    },
    
    saveAndNext : function(component, event, helper) {
        helper.handleSave(component, event, helper, true);        
    },
    back : function(component, event, helper) {
        localStorage.setItem('stopRecursiveUpdate','0');
        component.set("v.isRefresh",false);
        localStorage.setItem('LatestTabId',4);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "4" });
        compEvents.fire();
        $A.get('e.force:refreshView').fire();
        window.scrollTo(0, 0);
    },
     hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        helper.hideSpinner(component, event, helper);        
    }
    
    
})