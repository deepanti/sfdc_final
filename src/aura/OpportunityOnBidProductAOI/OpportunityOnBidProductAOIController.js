({
    doInit : function(component, event, helper) {
    	helper.showSpinner(component, event, helper);
        helper.fetchOppLineItems(component, event, helper); 
    },
    saveOLI : function(component, event, helper) {
        //debugger;
        var OLILst = component.find("AOIVal");
        var validate = true;
        for(var i=0; i<OLILst.length; i++)
        {
            console.log(OLILst[i].get("v.value"));
            if($A.util.isUndefinedOrNull(OLILst[i].get("v.value")) || OLILst[i].get("v.value") == '')
            {
                $A.util.addClass(OLILst[i],'slds-has-error');
                validate = false;
            }
            else
            {
                $A.util.removeClass(OLILst[i],'slds-has-error');
            }
        }
        if(validate)
        {
            helper.showSpinner(component, event, helper);
			helper.saveOppLineItems(component, event, helper);
             component.set("v.SelectedTabId", "4");
            component.find("recordViewForm").submit();
        }
        else
        {
            helper.showToast("error", "Please fill all required Field.");
        }
        	
	},
    
    saveAndNext : function(component, event, helper) {
        debugger;
        var OLILst = component.find("AOIVal");
        var validate = true;
        for(var i=0; i<OLILst.length; i++)
        {
            console.log(OLILst[i].get("v.value"));
            if($A.util.isUndefinedOrNull(OLILst[i].get("v.value")) || OLILst[i].get("v.value") == '')
            {
                $A.util.addClass(OLILst[i],'slds-has-error');
                validate = false;
            }
            else
            {
                $A.util.removeClass(OLILst[i],'slds-has-error');
            }
        }
        if(validate)
        {
            helper.showSpinner(component, event, helper);
			helper.saveOppLineItems(component, event, helper);
            component.set("v.SelectedTabId", "1");
            component.find("recordViewForm").submit();
            var SelectedTabId = component.get("v.SelectedTabId");
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : SelectedTabId });
            compEvents.fire();
        }
        else
        {
            helper.showToast("error", "Please fill all required Field.");
        }
        
        
    },
     back : function(component, event, helper) {
            component.set("v.SelectedTabId", "3");
            var SelectedTabId = component.get("v.SelectedTabId");
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : SelectedTabId });
            compEvents.fire();
        
    },

})