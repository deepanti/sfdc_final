({
	showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    fetchOppLineItems : function(component,event,helper){
        var action = component.get("c.fetchOpportunityLineItems");
        action.setParams({ 
            oppId : component.get("v.recordId") 
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.OLIList",response.getReturnValue());
                helper.hideSpinner(component,event,helper);
            }
            else{
                   helper.hideSpinner(component,event,helper);
                   helper.showToast("error", "Unable to fetch the Product list.");
                }
        });
        $A.enqueueAction(action);
    },
    saveOppLineItems : function(component,event,helper){
        var action = component.get("c.updateOpportunityLineItems");
        action.setParams({ 
            oppLineItems : component.get("v.OLIList") 
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component,event,helper);
                helper.showToast("success", "AOI value updated Successfully.");
            }
            else{
                    helper.hideSpinner(component,event,helper);
                	helper.showToast("error", "Unable to update AOI value.");
                }
        });
        $A.enqueueAction(action);
    },
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})