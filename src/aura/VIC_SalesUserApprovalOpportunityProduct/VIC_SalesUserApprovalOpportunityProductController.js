({    
    resetRadioGroup : function(component, event, helper){
        component.set('v.value', '');
    },
    showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    onHoldStatus : function(component, event, helper){
        //To show generic alert modal
        //var alertCmp = component.find("confirmAlertId");
        //alertCmp.set("v.showModal",true);
       
        var commentBoxId = component.find("comments");
        var isCommentted = helper.CommentsMandate(component, event, helper);
        if(isCommentted === true) {
            var idToUpdate = event.getSource().get("v.value");
            var comment = component.get("v.strInnerDescription");
            $A.util.removeClass(commentBoxId, 'comment_required');
            var btnClickEvent = component.getEvent("buttonClickEvent");
            let button = btnClickEvent.getSource();
            button.set('v.disabled',true);
            btnClickEvent.setParams({
                "Value" : idToUpdate,
                "Description" : comment,
                "Operation": "onHoldSelect",
                "Source":component
            });
           btnClickEvent.fire();
            
           // alert(component.get("v.oppOLIWrapper.oppLIObj.vic_Comment__c"));
            
            //alert(event.getSource().get("v.value"));
            /*if(confirm('Are you sure?')){
                if(!$A.util.isUndefinedOrNull(comment)){                
                    var idToUpdate = event.getSource().get("v.value");                
                    var action = component.get("c.updateOLIStatus");
                    
                    action.setParams({
                        "strRecordId": idToUpdate ,
                        "strDescription" : comment
                    });
                    action.setCallback(this, function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                          $A.get('e.force:refreshView').fire();
                                    
                          var sMsg = 'On-Hold Status Has been Updated';
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                mode: 'dismissible',
                                duration:' 500',
                                message: sMsg,
                                type : 'success'
                            });
                           toastEvent.fire();
                           
                        }else{
                            alert('ERROR----');
                        }
                    });
                    $A.enqueueAction(action);
                }else{                
                     component.set("v.isOnHold", false);
                    // this is toast message while approve opportunity  		
                    var sMsg = 'Please Insert Comments before On-Hold';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 4000',
                        message: sMsg,
                        type : 'error'
                    });
                    toastEvent.fire();                
                }
            }else{
                component.set("v.isOnHold", false);
            }*/
        } else {
            $A.util.addClass(commentBoxId, 'comment_required');
		}
    },
    
    updatePreStatus : function(component, event, helper){
        var idToUpdate = event.getSource().get("v.value"); 
         var isonhold=component.get("v.isOnHold");
        if(isonhold==true)
        {
         var action = component.get("c.updatePreOLIStatus");
       // alert(idToUpdate);
        action.setParams({
            "lstRecordIdpen": idToUpdate
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                $A.get('e.force:refreshView').fire();
        /* this is toast message while approve opportunity*/   		
           		  var sMsg = 'Pending Status Has been Updated';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 500',
                        message: sMsg,
                        type : 'success'
                    });
                   toastEvent.fire();
                
            }
        });
        $A.enqueueAction(action);
        }
        else{
           component.set("v.strInnerDescription", ''); 
       }
    },
    
   
    
    doInit: function(component, event, helper) { 
        var strComment = component.get("v.strDescription");
        
       
        
        
        var objProduct = component.get("v.oppOLIWrapper");
        var strCurrLoggedInUser = component.get("v.currentUserID");
        if(objProduct.oppLIObj.Opportunity.OwnerId == strCurrLoggedInUser && objProduct.oppLIObj.vic_Sales_Rep_Approval_Status__c=='On-Hold'){
            component.set("v.isOnHold", true);
        }else if(objProduct.oppLIObj.Product_BD_Rep__c == strCurrLoggedInUser  && objProduct.oppLIObj.vic_Product_BD_Rep_Approval_Status__c =='On-Hold'){
            component.set("v.isOnHold", true);
        }
        var isonhold=component.get("v.isOnHold");
        if(isonhold==true){
            component.set("v.strInnerDescription", strComment);
        }
        else{
             component.set("v.strInnerDescription", '');
        }
    },
    handleResetOnHold : function(component, event, helper) {
        debugger;
        var selectedId = event.getParam("Value");
        if(event.getParam("Operation") === "resetHoldValue") {
            if(selectedId == component.get("v.oppOLIWrapper.oppLIObj.Id")) {
                component.set("v.isOnHold",false);
            }
        }
    }
    
    /*   
     // For count the selected checkboxes. 
 checkboxSelect: function(component, event, helper) {
  // get the selected checkbox value  
  var selectedRec = event.getSource().get("v.value");
  // get the selectedCount attrbute value(default is 0) for add/less numbers. 
  var getSelectedNumber = component.get("v.selectedCount");
  // check, if selected checkbox value is true then increment getSelectedNumber with 1 
  // else Decrement the getSelectedNumber with 1     
  if (selectedRec == true) {
   getSelectedNumber++;
  } else {
   getSelectedNumber--;
  }
  // set the actual value on selectedCount attribute to show on header part. 
  component.set("v.selectedCount", getSelectedNumber);
 },
 
 // For select all Checkboxes 
 selectAll: function(component, event, helper) {
  //get the header checkbox value  
  var selectedHeaderCheck = event.getSource().get("v.value");
  // get all checkbox on table with "boxPack" aura id (all iterate value have same Id)
  // return the List of all checkboxs element 
  var getAllId = component.find("boxPack");
  // If the local ID is unique[in single record case], find() returns the component. not array   
     if(! Array.isArray(getAllId)){
       if(selectedHeaderCheck == true){ 
          component.find("boxPack").set("v.value", true);
          component.set("v.selectedCount", 1);
       }else{
           component.find("boxPack").set("v.value", false);
           component.set("v.selectedCount", 0);
       }
     }else{
       // check if select all (header checkbox) is true then true all checkboxes on table in a for loop  
       // and set the all selected checkbox length in selectedCount attribute.
       // if value is false then make all checkboxes false in else part with play for loop 
       // and select count as 0 
        if (selectedHeaderCheck == true) {
        for (var i = 0; i < getAllId.length; i++) {
  		  component.find("boxPack")[i].set("v.value", true);
   		 component.set("v.selectedCount", getAllId.length);
        }
        } else {
          for (var i = 0; i < getAllId.length; i++) {
    		component.find("boxPack")[i].set("v.value", false);
   			 component.set("v.selectedCount", 0);
  	    }
       } 
     }  
 
 },
    
 deleteSelected: function(component, event, helper) {
 
  var delId = []; 
  var getAllId = component.find("boxPack");
     if(! Array.isArray(getAllId)){
         if (getAllId.get("v.value") == true) {
           delId.push(getAllId.get("v.text"));
         }
     }else{
    
     for (var i = 0; i < getAllId.length; i++) {
       if (getAllId[i].get("v.value") == true) {
         delId.push(getAllId[i].get("v.text"));
       }
      }
     } 
   
      helper.deleteSelectedHelper(component, event, delId);
        
 },
   */ 
    
    
})