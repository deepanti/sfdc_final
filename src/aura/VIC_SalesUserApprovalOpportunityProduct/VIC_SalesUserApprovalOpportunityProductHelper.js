({
    /*deleteSelectedHelper: function(component, event, deleteRecordsIds) {
        var action = component.get('c.deleteRecords');
        action.setParams({
            "lstRecordId": deleteRecordsIds
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                
                this.onLoad(component, event);
            }
        });
        $A.enqueueAction(action);
    },
    */
    
    CommentsMandate : function(component, event,helper){
        var isCommentted;
        //var  radioGrp = component.find("radio").get("v.value");
        var  radioGrp =  event.getSource().get("v.checked");
        var strCmnt = component.get("v.strInnerDescription");
        
        if ((strCmnt==null || strCmnt=='') && radioGrp!=null ){
            isCommentted = false;
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Comments are required."
            });
            
            toastEvent.fire();
            component.set('v.isOnHold', false);
        } 
        else {
          
            isCommentted = true;
        }
        return isCommentted;
    }
})