({
	showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    resetRadioGroup : function(component, event, helper){
        component.set('v.oliDataWrap.strActionStatus', '');
        var objOLIDataWrap = component.get("v.oliDataWrap");
        objOLIDataWrap.strComment = '';
        objOLIDataWrap.isCommentRequired = false;
        component.set("v.oliDataWrap",objOLIDataWrap);
    },
    checkSelectedAllOLIStatus : function(component, event, helper){	
        var compEvent = component.getEvent("OpportunityComponentEvent");
        compEvent.fire();	//Call with registeration
    },
    checkRequiredData : function(component, event, helper){
        var objOLIDataWrap = component.get("v.oliDataWrap");
        if((objOLIDataWrap.strComment == '' || objOLIDataWrap.strComment == null) && (objOLIDataWrap.strActionStatus == 'Supervisor - OnHold' || 
                                               objOLIDataWrap.strActionStatus == 'Supervisor - Rejected')){
			objOLIDataWrap.isCommentRequired = true;    
            //alert('Comment is Required for pending and hold status');
        }else{
            objOLIDataWrap.isCommentRequired = false;  
        }
        component.set("v.oliDataWrap",objOLIDataWrap);
    },
    CommentsMandate: function(component, event,helper){						// v.oliDataWrap.strActionStatus
        var  radioGrp = component.find("radio").get("v.value");				// v.oliDataWrap.strComment
        var oliDataWrap = component.get("v.oliDataWrap");
        //alert(oliDataWrap.strActionStatus);
        if (oliDataWrap.strActionStatus == 'Supervisor - OnHold' && (oliDataWrap.strComment == null || oliDataWrap.strComment == '') && radioGrp!=null ){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Comments are required."
            });
            toastEvent.fire();
        }  
        if(oliDataWrap.strActionStatus == 'Supervisor - Rejected'){
            oliDataWrap.strComment = '';
        }
        component.set("v.oliDataWrap",oliDataWrap);
 	}
})