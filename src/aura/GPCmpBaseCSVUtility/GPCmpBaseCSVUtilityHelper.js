({
    MAX_FILE_SIZE: 5000000, //Max file size 5 MB 
    CHUNK_SIZE: 750000, //Chunk Max size 750Kb 
    FILE_NOT_SELECTED_ERROR_LABEL: 'No File Selected.',
    INVALID_FILE_SELECTED_ERROR_LABEL: 'Please Select a Valid File',
    FILE_SIZE_ERROR_LABEL: 'Alert : File size cannot exceed',
    UNSUPPORTED_FILE_FORMAT_ERROR_LABEL: 'Alert : Unsupported File Format',
    uploadHelper: function(component, event) {
        component.set("v.showLoadingSpinner", true);
        var fileInput = component.find("fileId").get("v.files");
        var file = fileInput[0];
        var fileName = file.name;
        var fileType = file.type;

        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {
            var errorMessage = self.FILE_SIZE_ERROR_LABEL;
            errorMessage += self.MAX_FILE_SIZE + ' bytes.\n';
            errorMessage += ' Selected file size: ' + file.size;

            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", errorMessage);
            return;
        }

        if (fileType !== "application/vnd.ms-excel" && fileType !== 'text/csv') {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", self.UNSUPPORTED_FILE_FORMAT_ERROR_LABEL);
            return;
        }

        this.parseCSVFile(component, fileName, file);
    },
    uploadProcess: function(component, file, fileContents) {
        var startPosition = 0;
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);

        this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
        component.set("v.showLoadingSpinner", false);
    },
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        var getchunk = fileContents.substring(startPosition, endPosition);
        var filename = component.get("v.fileName");
        var contentOfFile = getchunk;
    },
    parseCSVFile: function(component, fileName, file) {
        var reader = new FileReader();
        var parsedContent = [];
        var self = this;
        reader.onload = function(e) {
            var csv = e.target.result;
            // Parse local CSV file
            Papa.parse(csv, {
                complete: function(results) {
                    var validationResult = self.validateJsonToBeUploaded(results.data);
                    if (!validationResult.isValid) {
                        self.showToast(validationResult.type, "error", validationResult.message);
                        component.find('fileId').getElement().files = null;
                        component.set("v.listOfParsedRecords", null);
                    } else {
                        component.set("v.fileName", fileName);
                        results.data.splice(results.data.length - 1, 1);
                        component.set("v.parsedContent", results.data);
                        component.set("v.showLoadingSpinner", false);
                    }
                }
            });
        };
        
        reader.readAsText(file);
    },
    validateJsonToBeUploaded: function(data) {
        var validationMessage = {};

        /* for(var i = 1; i < data.length - 1; i += 1) {
             if($A.util.isEmpty(data[i][0])) {
                 return {
                     "isValid": false,
                     "type": "invalid data",
                     "message": "Field is empty on row" + i
                 };
             } else if($A.util.isEmpty(data[i][1])) {
                 return {
                     "isValid": false,
                     "type": "invalid data",
                     "message": "Field is empty on row" + i
                 };                
             } */
        return {
            "isValid": true,
            "type": "success",
            "message": "Success"
        };
    },
    createObjectList: function(component, csvrecords) {
        var listOfParsedRecords = [];
        for (var row = 1; row < csvrecords.length; row++) {
            var parseRecord = {};
            for (var col = 0; col < csvrecords[row].length; col++) {
                
                if($A.util.isEmpty(csvrecords[row][col]))
                    csvrecords[row][col] = null;

                if (csvrecords[row][col] === "TRUE" || csvrecords[row][col] === "true") {
                    csvrecords[row][col] = true;
                } else if (csvrecords[row][col] === "FALSE" || csvrecords[row][col] === "false") {
                    csvrecords[row][col] = false;
                }
                parseRecord[csvrecords[0][col]] = csvrecords[row][col];
            }
            listOfParsedRecords.push(parseRecord);
        }
        component.set("v.listOfParsedRecords", listOfParsedRecords);
    },
    downloadTemplate: function(component) {
        var headerRow = component.get("v.headerRow");
        var csv = Papa.unparse(headerRow);
        var fileName = component.get("v.exportfileName");
        var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
        if (navigator.msSaveBlob) {
            // IE 10+
            var blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
            navigator.msSaveBlob(blob, fileName);
        } else {
            var link = document.createElement("a");
            link.setAttribute('download', fileName);
            //To set the content of the file
            link.href = uri;
            link.style = "visibility:hidden";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
})