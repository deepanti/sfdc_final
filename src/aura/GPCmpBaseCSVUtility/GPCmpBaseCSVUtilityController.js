({
    handleFilesChange: function(component, event, helper) {
        var fileName = helper.FILE_NOT_SELECTED_ERROR_LABEL;

        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
        if (component.find("fileId").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            helper.showToast('Error', 'error', helper.INVALID_FILE_SELECTED_ERROR_LABEL);
        }
    },
    closeCmpBaseUtiltyPopup: function(component, event, helper) {
        component.set("v.showPopUp", false);
    },
    parseToListOfObjects: function(component, event, helper) {
        var parsedContent = component.get("v.parsedContent");
        if (parsedContent && parsedContent.length === 0) {
            return;
        }
        helper.createObjectList(component, parsedContent);
        component.set("v.showPopUp", false);
    },
    downloadTemplate: function(component, event, helper) {
        helper.downloadTemplate(component);
    }
})