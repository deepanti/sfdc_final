({
	toggleOpportunity : function(component, event, helper) {
		var isOppOpen = component.get('v.isOppOpen');
        component.set('v.isOppOpen', !isOppOpen);
	},
    showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    selectNUnselectAll : function(component, event, helper) {
        var statusOfSelectAll = event.getSource().get("v.value");
        var objOppDataWrap = component.get("v.oppDataWrap");
        for(var eachOLIData in objOppDataWrap.lstOLIDataWrap){
            objOppDataWrap.lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
        }
        component.set("v.oppDataWrap",objOppDataWrap);
        var compEvent = component.getEvent("UserComponentEvent");
        compEvent.fire();	//Call with registration
    },
    checkSelectedOLIStatus : function(component, event, helper){	//Aura event call
        var countOLI = 0;
        var objOppDataWrap = component.get("v.oppDataWrap");
        for(var eachOLIData in objOppDataWrap.lstOLIDataWrap){
            if(objOppDataWrap.lstOLIDataWrap[eachOLIData].isOLIChecked){
                countOLI++;
            }
        }
        if(objOppDataWrap.lstOLIDataWrap.length != countOLI){
            component.find("selectAllOLIID").set("v.value",false);
            var compEvent = component.getEvent("UserComponentEvent");
        	compEvent.fire();	//Call with registration
        }else{
            component.find("selectAllOLIID").set("v.value",true);
            var compEvent = component.getEvent("UserComponentEvent");
        	compEvent.fire();	//Call with registration
        }
    }
})