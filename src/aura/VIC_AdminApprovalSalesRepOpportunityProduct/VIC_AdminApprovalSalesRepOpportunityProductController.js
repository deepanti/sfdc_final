({
	showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    createDiscretionary : function(component, event, helper){
        //alert('call');
        component.set('v.isDiscretionaryVisible', true);
    },
    resetRadioGroup : function(component, event, helper){
        component.set('v.oliDataWrap.strActionStatus', '');
        var objOLIDataWrap = component.get("v.oliDataWrap");
        objOLIDataWrap.strComment = '';
        objOLIDataWrap.isCommentRequired = false;
        component.set("v.oliDataWrap",objOLIDataWrap);
    },
    checkSelectedAllOLIStatus : function(component, event, helper){	
        var compEvent = component.getEvent("AdminOppCmpEvent");
        compEvent.fire();	//Call with registeration
    },
    checkRequiredData : function(component, event, helper){
        var objOLIDataWrap = component.get("v.oliDataWrap");
        if(objOLIDataWrap.strComment == '' && (objOLIDataWrap.strActionStatus == 'VIC Team - OnHold')){
			objOLIDataWrap.isCommentRequired = true;    
            //alert('Comment is Required for pending and hold status');
        }else{
            objOLIDataWrap.isCommentRequired = false;  
        }
        component.set("v.oliDataWrap",objOLIDataWrap);
    },
    
    CommentsMandate: function(component, event,helper){
            var  radioGrp = component.find("radio").get("v.value");
            var strCmnt=component.get("v.oliDataWrap.strComment");
			if ((strCmnt==null || strCmnt=='') && radioGrp!=null ){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Comments are required."
                    });
                    toastEvent.fire();
            }
                 
 }
})