({
	deleteProject: function(component) {
        var deleteProject = component.get("c.deleteProjectRecord");
        deleteProject.setParams({
            "projectId": component.get("v.recordId") 
        });
        this.showSpinner(component);
        deleteProject.setCallback(this, function(response) {
            this.deleteProjectHandler(component, response);            
        });
        
        $A.enqueueAction(deleteProject);
    },
    deleteProjectHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('success', 'success', 'Record deleted successfully');
            this.redirectToListView('GP_Project__c');
            
            // var urlEvent = $A.get("e.force:navigateToURL");
            // urlEvent.setParams({
            //   "url": "/lightning/o/GP_Project__c/list"
            // });
            // urlEvent.fire();

        } else {
            this.handleFailedCallback(component, responseData);
        }
    }
})