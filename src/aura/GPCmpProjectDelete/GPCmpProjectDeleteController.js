({
	deleteProject: function(component, event, helper) {
        helper.deleteProject(component);
    },
    closePopup : function(component, event, helper) {
    	component.set('v.isDelete',false);
    }
})