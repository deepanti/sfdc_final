({
	showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    },
    resetRadioGroup : function(component, event, helper){
        component.set('v.value', '');
    }
})