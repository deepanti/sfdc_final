({ 
    recordUpdated : function(component, event, helper) {   
        //actions on record update
        alert('Change Form');
        var changeType = event.getParams().changeType;
        if (changeType === "CHANGED" || changeType === "LOADED"){
            helper.getStage(component, event, helper);            
        }
        else if (changeType === "ERROR") { 
            component.set("v.opportunityError",'Unknown problem, state: ' + saveResult.state + ', error: ' + JSON.stringify(saveResult.error));
            var opportunityError = component.get("v.opportunityError");
            console.log(opportunityError+'opportunityError');
        }
    },
    
    openPrediscover: function(component, event, helper)
    {
        helper.navigateToStage("0");
    },
    openDiscover: function(component, event, helper)
    {
        
        if(localStorage.getItem('MaxTabId')>=1)
            helper.navigateToStage("1");
        else
        { 
            var previousStage = component.get("v.pathOpportunity.Previous_Stage__c");
            if(previousStage=='Prediscover' || previousStage=='1. Discover' || previousStage=='2. Define'|| previousStage=='3. On Bid' ||previousStage=='4. Down Select' || previousStage=='5. Confirmed')
            {
                var stageName = component.get("v.pathOpportunity.StageName"); 
                if((stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped') )
                {
                    helper.navigateToStage("1");
                }	
            }
        }
        
        
    },
    openDefine: function(component, event, helper)
    {
        
        if(localStorage.getItem('MaxTabId')>=8)
            helper.navigateToStage("8");
        else
        {
            var previousStage = component.get("v.pathOpportunity.Previous_Stage__c");
            if(previousStage=='Prediscover' || previousStage=='1. Discover' || previousStage=='2. Define'|| previousStage=='3. On Bid' ||previousStage=='4. Down Select' || previousStage=='5. Confirmed')
            {
                var stageName = component.get("v.pathOpportunity.StageName"); 
                if(stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped')
                {
                    helper.navigateToStage("8");
                } 
            }
        }
        
    },
    openOnBid: function(component, event, helper)
    {
        
        if(localStorage.getItem('MaxTabId')>=11)
            helper.navigateToStage("11");
        else
        {
            var previousStage = component.get("v.pathOpportunity.Previous_Stage__c");
            if(previousStage=='Prediscover' || previousStage=='1. Discover' || previousStage=='2. Define'|| previousStage=='3. On Bid' ||previousStage=='4. Down Select' || previousStage=='5. Confirmed')
            {
                var stageName = component.get("v.pathOpportunity.StageName"); 
                if(stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped')
                {
                    helper.navigateToStage("11");
                } 
            }
        }
    },
    openDownSelect: function(component, event, helper)
    {
        
        if(localStorage.getItem('MaxTabId')>=14)
            helper.navigateToStage("14");
        else
        {
            var previousStage = component.get("v.pathOpportunity.Previous_Stage__c");
            if(previousStage=='Prediscover' || previousStage=='1. Discover' || previousStage=='2. Define'|| previousStage=='3. On Bid' ||previousStage=='4. Down Select' || previousStage=='5. Confirmed')
            {
                var stageName = component.get("v.pathOpportunity.StageName"); 
                if(stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped')
                {
                    helper.navigateToStage("14");
                }
            }
        }
    },   
    openConfirmed: function(component, event, helper)
    {
        
        if(localStorage.getItem('MaxTabId')>=17)
            helper.navigateToStage("17");
        
        else
        {
            var previousStage = component.get("v.pathOpportunity.Previous_Stage__c");
            if(previousStage=='Prediscover' || previousStage=='1. Discover' || previousStage=='2. Define'|| previousStage=='3. On Bid' ||previousStage=='4. Down Select' || previousStage=='5. Confirmed')
            {
                var stageName = component.get("v.pathOpportunity.StageName"); 
                if(stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped')
                {
                    helper.navigateToStage("17");
                }
            }
        }
        
    },
 initRecord: function(component, event, helper){
     	//alert('initRecord');
        var action = component.get("c.fetchRecord");
        action.setParams({
            'oppId' : component.get('v.recordId')
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var StoreResponse = response.getReturnValue();
                component.set('v.pathOpportunity',StoreResponse);
                 helper.getStage(component, event, helper);
                //alert('initRecord2');
            }
             else{
                alert('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
        
    }
    
})