({
    getStage: function(component, event, helper){
        //to fetch opportunity stage
        
        var stageName = component.get("v.pathOpportunity.StageName"); 
        var previousStageName = component.get("v.pathOpportunity.Previous_Stage__c");
        // alert('-previousStageName-'+previousStageName);
        if (stageName == 'Prediscover')
            var listItemId = component.find("discover");
        else if(stageName == '1. Discover')
            var listItemId = component.find("define");
            else if (stageName == '2. Define')
                var listItemId = component.find("onBid");
                else if (stageName == '3. On Bid')
                    var listItemId = component.find("downSelect");
                    else if (stageName == '4. Down Select')
                        var listItemId = component.find("confirmed");
                        else if (stageName == '5. Confirmed')
                            var listItemId = component.find("closed");
                            else if (stageName == '6. Signed Deal' || stageName == '7. Lost' || stageName == '8. Dropped')
                            {
                                //var listItemId = component.find("closed");  
                                if(stageName == '6. Signed Deal')
                                    component.set("v.closedStatusLabel",'Signed Deal');   
                                else  if(stageName == '7. Lost')
                                    component.set("v.closedStatusLabel",'Lost');   
                                    else if(stageName == '8. Dropped')
                                        component.set("v.closedStatusLabel",'Dropped');                                     
                            }
        
        this.setActiveClass(component, event, helper,listItemId,stageName,previousStageName);
    },
    setActiveClass : function(component, event, helper,listItemId,stageName,previousStageName) {
        //to apply active class foe the active opportunity stage
        if(listItemId != null && listItemId != 'undefined')
        {
            var lis = document.getElementsByClassName("pathActive");
            for (var i = 0; i < lis.length; i++) {  
                lis[i].classList.add("slds-is-incomplete");
            }
               $A.util.addClass(listItemId, 'slds-is-active slds-is-current');
          //  $A.util.addClass(listItemId, 'slds-is-incomplete');
            component.set("v.isActive",true);
        }
        this.addCompleteClass(component,event,helper,stageName,previousStageName);
        
    },
    addCompleteClass:function(component, event, helper,stageName,previousStageName) {
        // adds complete stage css to the completed stages
        if(stageName == 'Prediscover') {    
            $A.util.addClass(component.find("prediscover"), 'slds-is-complete');            
            $A.util.removeClass(component.find("prediscover"), 'slds-is-active slds-is-current');            component.set("v.isCompletePrediscover",true);
            //  component.set("v.isActiveDiscover",true);         
        }
        else if(stageName == '1. Discover') {
            $A.util.addClass(component.find("prediscover"), 'slds-is-complete'); 
            component.set("v.isCompletePrediscover",true);
            
            $A.util.removeClass(component.find("discover"), 'slds-is-active slds-is-current');
            $A.util.addClass(component.find("discover"), 'slds-is-complete');
            component.set("v.isCompleteDiscover",true);
            // component.set("v.isActiveDefine",true);
        }
            else if(stageName == '2. Define'){
                $A.util.addClass(component.find("prediscover"), 'slds-is-complete');
                component.set("v.isCompletePrediscover",true);
                $A.util.addClass(component.find("discover"), 'slds-is-complete');
                component.set("v.isCompleteDiscover",true);
                $A.util.removeClass(component.find("define"), 'slds-is-active slds-is-current');
                $A.util.addClass(component.find("define"), 'slds-is-complete');                
                component.set('v.isCompleteDefine',true);
                //    component.set('v.isActiveOnBid',true);
            }
                else if(stageName == '3. On Bid'){
                    $A.util.addClass(component.find("prediscover"), 'slds-is-complete'); 
                    component.set('v.isCompletePrediscover',true);
                    $A.util.addClass(component.find("discover"), 'slds-is-complete');
                    component.set('v.isCompleteDiscover',true);
                    $A.util.addClass(component.find("define"), 'slds-is-complete');                
                    component.set('v.isCompleteDefine',true);
                    $A.util.removeClass(component.find("onBid"), 'slds-is-active slds-is-current');
                    $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                    component.set('v.isCompleteOnBid',true);
                    //    component.set('v.isActiveDownSelect',true);
                    
                }
                    else if(stageName == '4. Down Select'){
                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete');  
                        component.set('v.isCompletePrediscover',true);
                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                        component.set('v.isCompleteDiscover',true);
                        $A.util.addClass(component.find("define"), 'slds-is-complete');
                        component.set('v.isCompleteDefine',true);
                        $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                        component.set('v.isCompleteOnBid',true);
                        $A.util.removeClass(component.find("downSelect"), 'slds-is-active slds-is-current');
                        $A.util.addClass(component.find("downSelect"), 'slds-is-complete');
                        component.set('v.isCompleteDownSelect',true);
                        //    component.set('v.isActiveConfirmed',true);
                    }
                        else if(stageName == '5. Confirmed'){
                            $A.util.addClass(component.find("prediscover"), 'slds-is-complete');  
                            component.set('v.isCompletePrediscover',true);
                            $A.util.addClass(component.find("discover"), 'slds-is-complete');
                            component.set('v.isCompleteDiscover',true);
                            $A.util.addClass(component.find("define"), 'slds-is-complete');
                            component.set('v.isCompleteDefine',true);
                            $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                            component.set('v.isCompleteOnBid',true);
                            $A.util.addClass(component.find("downSelect"), 'slds-is-complete');
                            component.set('v.isCompleteDownSelect',true);
                            $A.util.removeClass(component.find("confirmed"), 'slds-is-active slds-is-current');
                            $A.util.addClass(component.find("confirmed"), 'slds-is-complete');
                            component.set('v.isCompleteConfirmed',true);
                            //      component.set('v.isActiveClosed',true);
                        }
                            else if(stageName == '6. Signed Deal'){
                                $A.util.addClass(component.find("prediscover"), 'slds-is-complete');  
                                component.set('v.isCompletePrediscover',true);
                                $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                component.set('v.isCompleteDiscover',true);
                                $A.util.addClass(component.find("define"), 'slds-is-complete');
                                component.set('v.isCompleteDefine',true);
                                $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                                component.set('v.isCompleteOnBid',true);
                                $A.util.addClass(component.find("downSelect"), 'slds-is-complete');
                                component.set('v.isCompleteDownSelect',true);
                                $A.util.addClass(component.find("confirmed"), 'slds-is-complete');
                                component.set('v.isCompleteConfirmed',true);
                                component.set('v.isCompleteClosed',true);
                                $A.util.removeClass(component.find("closed"), 'slds-is-active slds-is-current');
                                //  $A.util.addClass(component.find("closed"), 'slds-is-lost');
                                $A.util.addClass(component.find("closed"), 'slds-is-complete');
                            } 
                                else if(stageName == '7. Lost' || stageName == '8. Dropped'){
                                    $A.util.removeClass(component.find("closed"), 'slds-is-active slds-is-current');
                                    $A.util.addClass(component.find("closed"), 'slds-is-lost');
                                   
                                    if(previousStageName == 'Prediscover'){
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete');            
                                        $A.util.removeClass(component.find("prediscover"), 'slds-is-active slds-is-current'); 
                                        $A.util.removeClass(component.find("discover"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("discover"), 'slds-is-incomplete');
                                        $A.util.removeClass(component.find("define"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("define"), 'slds-is-incomplete');                
                                        $A.util.removeClass(component.find("onBid"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("onBid"), 'slds-is-incomplete');
                                        $A.util.removeClass(component.find("downSelect"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-incomplete');
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-incomplete');
                                        
                                    } else if(previousStageName == '1. Discover'){
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete'); 
                                        component.set("v.isCompletePrediscover",true);
                                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                        $A.util.addClass(component.find("define"), 'slds-is-incomplete');                
                                        $A.util.addClass(component.find("onBid"), 'slds-is-incomplete');
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-incomplete');
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-incomplete');
                                    
                                    } else if(previousStageName == '2. Define'){
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete');
                                        component.set("v.isCompletePrediscover",true);
                                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                        component.set("v.isCompleteDiscover",true);
                                        $A.util.removeClass(component.find("define"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("define"), 'slds-is-complete');                
                                        $A.util.removeClass(component.find("onBid"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("onBid"), 'slds-is-incomplete');
                                        $A.util.removeClass(component.find("downSelect"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-incomplete');
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-incomplete');
                                     
                                    } else if(previousStageName == '3. On Bid'){
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete'); 
                                        component.set('v.isCompletePrediscover',true);
                                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                        component.set('v.isCompleteDiscover',true);
                                        $A.util.addClass(component.find("define"), 'slds-is-complete');                
                                        component.set('v.isCompleteDefine',true);
                                        $A.util.removeClass(component.find("onBid"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                                        $A.util.removeClass(component.find("downSelect"), 'slds-is-active slds-is-current');
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-incomplete');
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-incomplete');
                                    
                                    } else if(previousStageName == '4. Down Select'){
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete');  
                                        component.set('v.isCompletePrediscover',true);
                                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                        component.set('v.isCompleteDiscover',true);
                                        $A.util.addClass(component.find("define"), 'slds-is-complete');
                                        component.set('v.isCompleteDefine',true);
                                        $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                                        component.set('v.isCompleteOnBid',true);
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-complete');
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-incomplete');
                                    
                                    } else if(previousStageName == '5. Confirmed'){
                                        
                                        $A.util.addClass(component.find("prediscover"), 'slds-is-complete');  
                                        component.set('v.isCompletePrediscover',true);
                                        $A.util.addClass(component.find("discover"), 'slds-is-complete');
                                        component.set('v.isCompleteDiscover',true);
                                        $A.util.addClass(component.find("define"), 'slds-is-complete');
                                        component.set('v.isCompleteDefine',true);
                                        $A.util.addClass(component.find("onBid"), 'slds-is-complete');
                                        component.set('v.isCompleteOnBid',true);
                                        $A.util.addClass(component.find("downSelect"), 'slds-is-complete');
                                        component.set('v.isCompleteDownSelect',true);
                                        $A.util.addClass(component.find("confirmed"), 'slds-is-complete');
                                    } 
                                } 
        
    },
    navigateToStage : function(tabNo){
        localStorage.setItem('LatestTabId',tabNo);
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : tabNo});
        appEvent.fire();
        
    }
})