({
    doInit: function(component, event, helper) { 
        
         helper.opportunityPending(component);
         helper.opportunityApproved(component);
         helper.getFinancialYear(component, event, helper);
        
    },
    updateStatus: function(component, event, helper) { 
       
        var confirmModalCmp = component.find("confirmModalId");
        let button = confirmModalCmp.find('saveBtnId');
        button.set('v.disabled',false);
        var opportunityListPending = component.get("v.opportunityListPending");
        var selectedOpportunityLI = [];
        for(var i=0; i < opportunityListPending.length; i++){
            for(var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
                if(opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected){
                    selectedOpportunityLI.push(opportunityListPending[i].opportunityLIWrapperObj[j].oppLIObj);
                }
            }
        }
        
        
       confirmModalCmp.set("v.confirmMessage","Are you sure you want to continue?");
       confirmModalCmp.set("v.showModal",true);
       for(var i=0; i < selectedOpportunityLI.length; i++){
            if(selectedOpportunityLI[i].vic_Sales_Rep_Approval_Status__c==='On-Hold'){
                
                confirmModalCmp.set("v.confirmMessage","You are approving Inquery records also. Are you sure you want to continue?");   
            
            }
            
            
        }
        confirmModalCmp.set("v.holdValue","fromApproveBtn");
        
    },
    
    selectAllOppAndLI: function(component, event, helper) {
  	    var selectedHeaderCheck = event.getSource().get("v.value");
        var opportunityListPending = component.get("v.opportunityListPending");
        for(var i=0; i < opportunityListPending.length; i++){
            opportunityListPending[i].oppSelected = selectedHeaderCheck;
            for(var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
               opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected = selectedHeaderCheck;
            }
        }
        //opportunityListPending = opportunityListPending;
        component.set("v.opportunityListPending", opportunityListPending);
    },
    
/*
   selectAll: function(component, event, helper) {
     
  	var selectedHeaderCheck = event.getSource().get("v.value");
 	 var getAllId = component.find("boxPack");
  
     if(! Array.isArray(getAllId)){
       if(selectedHeaderCheck == true){ 
          component.find("boxPack").set("v.value", true);
          //component.set("v.selectedCount", 1);
       }else{
           component.find("boxPack").set("v.value", false);
           //component.set("v.selectedCount", 0);
       }
     }
     */
    
    /* else{
       
        if (selectedHeaderCheck == true) {
        for (var i = 0; i < getAllId.length; i++) {
  		  component.find("boxPack")[i].set("v.value", true);
   		 component.set("v.selectedCount", getAllId.length);
        }
        } else {
          for (var i = 0; i < getAllId.length; i++) {
    		component.find("boxPack")[i].set("v.value", false);
   			 component.set("v.selectedCount", 0);
  	    }
       } 
     }*/  
 
   
    
 /*   checkAllCheckboxes : function(component, event, helper) {
       
        var isBoolean = component.find("selectAllId").get("v.checked");
        component.set("v.selectAll", isBoolean);
        
    },
   */ 
    
    
 /*    statusUpdate : function(component, event, helper){
        alert('sss');
        var idToUpdate = event.getSource().get("v.value"); 
        var action = component.get("c.updateOpportunityStatus");
        action.setParams({
            "lstRecordId": idToUpdate
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            //alert('Status Update to On-Hold');
            if (state === "SUCCESS") {
             alert('Selected OLI Status has been Updated');   
               
            }
        });
        $A.enqueueAction(action);
    }
    */
    
    /*
		@author: Karthik Chekkilla
		@company: Saasfocus
		@description: This method is used to handle events.
	*/
	onBtnClickHandler : function(component, event, helper) {
        debugger;
    	var Operation = event.getParam("Operation");
        var comment = event.getParam("Description");
        var idToUpdate = event.getParam("Value");
        var childCmp = event.getParam("Source");
        if(Operation === "handleSaveBtn") {
            if(idToUpdate === "fromApproveBtn") {
            	helper.updateStatus(component, event);
            }else { 
            if(!$A.util.isUndefinedOrNull(comment) && !$A.util.isEmpty(comment)) {                              
                var action = component.get("c.updateOLIStatus");
                action.setParams({
                    "strRecordId": idToUpdate ,
                    "strDescription" : comment
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                      $A.get('e.force:refreshView').fire();     		
                      var sMsg = 'Records have been processed successfully.';
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            duration:' 500',
                            message: sMsg,
                            type : 'success'
                        });
                       toastEvent.fire();
                       
                    }else{
                        //alert('ERROR----');
                    }
                    var tabComp = component.find("confirmModalId");
                    tabComp.set("v.showModal",false);
                });
                $A.enqueueAction(action);
            }else {
                childCmp.set("v.isOnHold",false);
                 var tabComp = component.find("confirmModalId");
                  tabComp.set("v.showModal",false);
                  var sMsg = 'Comments are required.';
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            mode: 'dismissible',
                            duration:' 500',
                            message: sMsg,
                            type : 'error'
                        });
                       toastEvent.fire();
            }
        }
		}
        if(Operation === "onHoldSelect") {
            var confirmCmp = component.find("confirmModalId");
            confirmCmp.set("v.showModal",true);
            confirmCmp.set("v.holdValue",idToUpdate);
            confirmCmp.set("v.holdDescription",comment);
            confirmCmp.set("v.Source",childCmp);
            
           // component.set("v.isOnHold", false);
            
        }
        if(Operation === "handleCancelBtn") {
            debugger;
           // var radioBtn = component.find(idToUpdate);
            if(idToUpdate === "fromApproveBtn") {
                //approve button 
            }else{
                childCmp.set("v.isOnHold",false);
            }  
		}
        if(Operation === "handleSaveBtn"){
            var confirmCmp = component.find("confirmModalId");
            let button = confirmCmp.find('saveBtnId');
            button.set('v.disabled',true);
        }
    },
    
    generateLetter: function(component, event, helper){ 
        component.set("v.showGenerateStmt", true);
    },
    
    cancelBtnId: function(component, event, helper){ 
        component.set("v.showGenerateStmt", false);
    },
    
    submitLetter: function(component, event, helper){ 
        var userId = $A.get("$SObjectType.CurrentUser.Id");
  		var strFY = component.get("v.strFinancialYear");  
        var selectedYear = parseInt(strFY);
        //alert(userId);
        helper.submittingBonusLetter(component, event, helper);
        component.set("v.showGenerateStmt", false);
    }
    
})