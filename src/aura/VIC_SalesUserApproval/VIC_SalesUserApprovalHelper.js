({
	opportunityPending : function(component) {		
        var action = component.get('c.getOpportunityPending');
        action.setCallback(this, function(actionResult) {
        console.log('Opportunity+++',actionResult.getReturnValue());
       if(actionResult.getState()=='SUCCESS') { 
        component.set('v.opportunityListPending', actionResult.getReturnValue());
        component.set("v.listSizePending",actionResult.getReturnValue().length); 
  	 }
            
     });
        $A.enqueueAction(action);
    },
    
    updateStatus : function(component, event) {	
        var opportunityListPending = component.get("v.opportunityListPending");
        var selectedOpportunityLI = [];
        for(var i=0; i < opportunityListPending.length; i++){
            for(var j=0; j < opportunityListPending[i].opportunityLIWrapperObj.length; j++){
                if(opportunityListPending[i].opportunityLIWrapperObj[j].oppLISelected){
                      selectedOpportunityLI.push(opportunityListPending[i].opportunityLIWrapperObj[j].oppLIObj);
                }
            }
        }
         
        if($A.util.isEmpty(selectedOpportunityLI) || $A.util.isUndefined(selectedOpportunityLI)) {  		
            var sMsg = "Please select atleast one opportunity line item.";
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                mode: 'dismissible',
                duration:' 500',
                message: sMsg,
                type : 'error'
            });
            toastEvent.fire();
            var tabComp = component.find("confirmModalId");
                 tabComp.set("v.showModal",false);  
        }else{

        var action = component.get('c.getupdateStatus');
        action.setParams({"selectedOpportunityLI":selectedOpportunityLI});
      
       		 action.setCallback(this, function(actionResult) {
       	if(actionResult.getState()==='SUCCESS') { 
               	 $A.get('e.force:refreshView').fire();
      	  /* this is toast message while approve opportunity*/   		
           		var sMsg = 'Records have been processed successfully.';  
            	//var sMsg = 'Selected Opportunity has been Approved Successfully';
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 500',
                        message: sMsg,
                        type : 'success'
                    });
                   toastEvent.fire();
       		}
                 else {
                     
                	var errors = actionResult.getError();
                   // console.log('++error',errors[0].message);
                    $A.get('e.force:refreshView').fire();
      	  			/* this is toast message while Error on opportunity*/   		
           		    var sMsg = errors[0].message;
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode: 'dismissible',
                        duration:' 500',
                        message: sMsg,
                        type : 'error'
                    });
                   toastEvent.fire();
            }
                 var tabComp = component.find("confirmModalId");
                 tabComp.set("v.showModal",false);           
     });
        
        $A.enqueueAction(action);
    }
    },

 
    opportunityApproved : function(component) {		
        var action = component.get('c.getOpportunityApproved');
        action.setCallback(this, function(actionResult) {
        console.log('Opportunity+ Arproveddd++',actionResult.getReturnValue());
       if(actionResult.getState()=='SUCCESS') { 
        component.set('v.opportunityListApproved', actionResult.getReturnValue());
        component.set("v.listSizeApproved",actionResult.getReturnValue().length);    
       }
     });
        
        $A.enqueueAction(action);
    
    },
    getFinancialYear : function(component,event,helper) {
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstYears",allValues);
                component.set("v.strFinancialYear",allValues[0]);
            }
        });
        $A.enqueueAction(action);
    },
    
    submittingBonusLetter : function(component,event,helper) {
        var action = component.get("c.submitBonusLetter");
        action.setParams({
            "strYear" : component.get("v.strFinancialYear")
        });
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var urlValues = response.getReturnValue();
                window.open(urlValues);
            }
        });
        $A.enqueueAction(action);
    }
	
})