({
	 getDealOutcome : function(component)
   	 {
        var action = component.get("c.getDealOutcome");
        action.setCallback(this, function(response)
         {            
            component.set("v.DealOutcome", response.getReturnValue());           
         });
        $A.enqueueAction(action);
    },
     getOpportunityData : function(component, page)
    {
        var recordId = component.get("v.recordId");
        var action = component.get("c.getOpportunityData");
        action.setParams({
            "opportunityID" : recordId,           
        });
        
        action.setCallback(this, function(response) { 
            var state = response.getState();
            if(state === 'SUCCESS'){
               var result = response.getReturnValue();
               // alert('get opp data result========'+JSON.stringify(result));    
                component.set("v.OppWrapperList", response.getReturnValue());
              	var OppWrapperList11 = [];
                OppWrapperList11 = component.get("v.OppWrapperList");  
            	var WLD;
                var Stage_Name;
              //  alert(OppWrapperList11.length);
                    for(var i=0;i<OppWrapperList11.length;i=i+1)
                    {   
                        var test1=OppWrapperList11[i];
                        var jsnParse= JSON.parse(JSON.stringify(test1));         
                        WLD= jsnParse.OpportunityDataList[0].W_L_D__c;
                        Stage_Name= jsnParse.OpportunityDataList[0].StageName;
                        component.set("v.Stage_Name",Stage_Name);
                       // alert('=======Stage_Name===helper===='+Stage_Name);
                        //alert('======stage Name=====111===='+component.get("v.Stage_Name")); 
                    }
                //alert('====WLD====helper===='+WLD);
					 if(WLD == 'undefined' || WLD == null )
					 {
						component.set("v.IsSurveyFilled",false);
					 }
					 else
					 {
						component.set("v.IsSurveyFilled",true);
					 }
                
                component.set("v.WLD",WLD);
                component.find("DealOutcome").set("v.value",WLD);
                //alert('=====v.IsSurveyFilled====='+ component.get("v.IsSurveyFilled"));
                
             }    
        });
        $A.enqueueAction(action); 
        
        
    }
    
})