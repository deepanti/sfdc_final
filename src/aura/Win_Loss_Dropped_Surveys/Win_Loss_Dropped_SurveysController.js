({
	 init: function (component, event, helper) 
    {    
        helper.getDealOutcome(component);  
        helper.getOpportunityData(component);         
        //component.set("v.selectedDealOutcome","v.WLD"); 
        
    },    
    OnSelectChange: function(component, event, helper)
    {     
        var selected = component.find("DealOutcome").get("v.value");
        
    },   
    
    getNext : function(component, event, helper)
    {     
       // alert('next page=====');          
       // alert(JSON.stringify(component.get("v.OppWrapperList"))); 
        var SelectedDealOutcomeList = component.find("DealOutcome").get("v.value"); 
        var OppList = component.get("v.OppWrapperList");
        
        console.log('=====SelectedDealOutcomeList====='+SelectedDealOutcomeList);     
        console.log('=======OppList========'+JSON.stringify(OppList));  
       // alert('======OppList====main=='+JSON.stringify(OppList));
       // alert('=======OppList===111====='+JSON.stringify(OppList));  
        if(SelectedDealOutcomeList == '' || SelectedDealOutcomeList == '--None--' )
        {
            alert('Please select at least one Deal Outcome.');
        }
        else
        {     
            var DealOutcome1 = component.get("v.selectedDealOutcome");
            var Opp_StageName = component.get("v.Stage_Name")
            //alert('======deal outcome========'+DealOutcome1);
            //alert('======Opp_StageName===='+Opp_StageName);
            
          /*  if(DealOutcome1 == 'Signed')
            {
                if(Opp_StageName == '5. Confirmed')
                {
                     var event = $A.get("e.c:NavigateToSurveys");              
                    	event.setParams({
                        "OpportunityList": component.get("v.selectedDealOutcome"),
                        "OppWrapperList":OppList,
                        "conid": component.get("v.conid"),                         
                        "recordId" : component.get("v.recordId")
                        
                    });
                	 event.fire();     
                }
                else
                {
                	alert('Please select the deal outcome as either Loss or Dropped.');
                }                
            }
            else{   */       
                //alert('=======4=====');
                var event = $A.get("e.c:NavigateToSurveys");
                //event.setParams({
                //    "OpportunityList" : SelectedDealOutcomeList, 
                //     "OpportunityDataList" : OppList,                
                //    "recordId" : component.get("v.recordId")  
                //});
                event.setParams({
                    "OpportunityList": component.get("v.selectedDealOutcome"),
                    "OppWrapperList":OppList,
                    "conid": component.get("v.conid"),                         
                    "recordId" : component.get("v.recordId")
                    
                });
                 event.fire();         
        	//}
        }
    }
    
    ,   
    
     getSurveyDetails : function(component, event, helper)
    {     
       //alert('====getSurveyDetails=== page=====');          
       // alert(JSON.stringify(component.get("v.OppWrapperList"))); 
        var SelectedDealOutcomeList = component.find("DealOutcome").get("v.value"); 
        var OppList = component.get("v.OppWrapperList");
        
        console.log('=====SelectedDealOutcomeList====='+SelectedDealOutcomeList);     
        console.log('=======OppList========'+JSON.stringify(OppList));  
        //alert('==getSurveyDetails======selectedDealOutcome==111====='+component.get("v.selectedDealOutcome")); 
        // alert('==getSurveyDetails=====OppList==111==='+JSON.stringify(OppList)); 
        // alert('==getSurveyDetails=====recordId===111===='+component.get("v.recordId")); 
        
         var event = $A.get("e.c:NavigateToSurveyDetails");        	
           event.setParams({
                "OpportunityList": component.get("v.selectedDealOutcome"),
                "OppWrapperList":OppList,
                "conid": component.get("v.conid"),                         
                "recordId" : component.get("v.recordId")
                
            });
             event.fire();   
        //alert('=====NavigateToSurveyDetails=====end===');
    },
    
     handleClickCancel : function(component, event, helper)
	 {
    	var recordId = component.get("v.recordId");
        //sforce.one.navigateToSObject(recordId,"OPPORTUNITY");
		function isLightningExperienceOrSalesforce1() 
		 {
			return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
		} 
		if(isLightningExperienceOrSalesforce1()) 
		{   
			//sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
			sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 				
		}
		else
		{
			window.location = '/'+recordId;   //'/006/o'
		}		
     }        
})