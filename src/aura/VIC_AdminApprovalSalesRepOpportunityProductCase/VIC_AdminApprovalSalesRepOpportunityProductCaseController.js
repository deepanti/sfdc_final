({
	showPopover : function(component, event, helper){
        var caseId = event.target.id;
        var objOLIDataWrap = component.get('v.oliDataWrap');
        for(var key in objOLIDataWrap.lstCaseDataWrap){
            if(objOLIDataWrap.lstCaseDataWrap[key].strCaseId == caseId){
                objOLIDataWrap.lstCaseDataWrap[key].isPopoverVisible = true;
                break;
            }
        }
        component.set('v.oliDataWrap',objOLIDataWrap);
    },
    hidePopover : function(component, event, helper){
        var objOLIDataWrap = component.get('v.oliDataWrap');
        for(var key in objOLIDataWrap.lstCaseDataWrap){
            objOLIDataWrap.lstCaseDataWrap[key].isPopoverVisible = false;
        }
        component.set('v.oliDataWrap',objOLIDataWrap);
    },
    createDiscretionary : function(component, event, helper){
        //alert('call');
        component.set('v.isDiscretionaryVisible', true);
    },
    resetRadioGroup : function(component, event, helper){
        component.set('v.oliDataWrap.strActionStatus', '');
    },
    checkRequiredData : function(component, event, helper){
        var objOLIDataWrap = component.get("v.oliDataWrap");
        if(objOLIDataWrap.strComment == '' && (objOLIDataWrap.strActionStatus == 'VIC Team - OnHold')){
			objOLIDataWrap.isCommentRequired = true;    
            //alert('Comment is Required for pending and hold status');
        }else{
            objOLIDataWrap.isCommentRequired = false;  
        }
        component.set("v.oliDataWrap",objOLIDataWrap);
    },
    doInit : function(component, event, helper) {
		var custs = [];
        var mapvalue = component.get("v.mapCaseIdToOLIWrap");
        var lstOLIDataWrap = component.get("v.lstOLIDataWrap");
        //alert('mapcall======='+lstOLIDataWrap);
        for(var keyval in mapvalue){
			//alert('===KEY==='+keyval);
            custs.push({value:mapvalue[keyval],key:keyval}); 
            //alert('=========================='+mapvalue[keyval].strITKicker);
        }
        component.set("v.lstOLIDataWrap",custs);
        //alert('lstOLIDataWrap======'+custs['5000l000002ZuMsAAK']);
	}
})