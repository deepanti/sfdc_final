({
    doInit : function(component, event, helper) {
        helper.getFinancialYear(component, event, helper);
    },
    //to get selected Target Records for Selected Financial Year
    getTargetRecord : function(component, event, helper) {
        helper.getTargetRecord(component, event, helper);
    },
    
    checkboxSelect : function(component, event, helper) {
        helper.checkboxSelect(component, event, helper);
    },
    
    selectAll : function(component, event, helper) {
        helper.selectAll(component, event, helper);
    },
    
    congaEmails: function(component, event, helper){
        helper.congaEmails(component, event, helper);
    },    
    
    getCurrentYearVicRole : function(component,event,helper){
        helper.currentYearVicRole(component,event,helper);
    }
})