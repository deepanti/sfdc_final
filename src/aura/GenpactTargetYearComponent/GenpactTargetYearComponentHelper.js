({
    getFinancialYear : function(component,event,helper) {
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) { 
            
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstYears",allValues);
                component.set("v.strFinancialYear",allValues[1]);
                this.getRoles(component,event,helper);
            }
        });
        $A.enqueueAction(action);
    },
       
    getRoles : function(component,event,helper) {
		var dt = new Date();
  		var currentFY = dt.getFullYear(); 
        
        var action = component.get("c.getVicRole");
        action.setParams({"currentFinancialYear": currentFY});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstVicRole",allValues);
                component.set("v.VicRole",allValues[0]);
             
            }
        });
        $A.enqueueAction(action);
    },
    
    currentYearVicRole : function(component,event,helper){
      var selectedYear = component.find("Year").get("v.value");  
      
        var action = component.get("c.getVicRole");
        action.setParams({"currentFinancialYear": selectedYear});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstVicRole",allValues);
                component.set("v.VicRole",allValues[0]);
                
            }
        });
        $A.enqueueAction(action);
    },
    
    getTargetRecord : function(component,event,helper) {
       debugger;
        var status = component.get("v.strStatus");
        component.set("v.generateTargetLetterStatus", status);
        var vicRole=component.get("v.VicRole");
        debugger;
        var role=component.find("Role");
        if(vicRole == null || vicRole == undefined || vicRole ==""){
            role.set("v.errors", [{message:"Please Select VIC Role."}]);
            component.set("v.statusShowHide",false);
            return null;
        }
        else{
            role.set("v.errors",null);
            var financialYear=component.get("v.strFinancialYear");
            var action = component.get("c.getTargetRecords");
            action.setParams({"status": status,"vicRole": vicRole,"financialYear": financialYear});
            action.setCallback(this, function(response){
                
                if (response.getState() == "SUCCESS") {
                    var allValues = response.getReturnValue();
                    if(allValues.length>0){
                        component.set("v.errorMessage", "");
                        component.set("v.targetRecords",allValues);
                        component.set("v.statusShowHide",true);
                        component.find("box3").set("v.value", false);
                        component.set("v.selectedCount",0);
                    }
                    else{
                        component.set("v.errorMessage", "No Records Found");
                        component.set("v.selectedCount",0);
                        component.set("v.statusShowHide",false);
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    checkboxSelect: function(component, event, helper) {
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        
        component.set("v.selectedCount", getSelectedNumber);
        var status = component.get("v.strStatus");
        var vicRole=component.get("v.VicRole");
        var financialYear=component.get("v.strFinancialYear");
        var action = component.get("c.getTargetRecords");
        action.setParams({"status": status,"vicRole": vicRole,"financialYear": financialYear});
        action.setCallback(this, function(response){
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                
                
                if(allValues.length==getSelectedNumber){
                    component.find("box3").set("v.value", true);
                }
                else
                    component.find("box3").set("v.value", false);
                
            }
        });
        
        $A.enqueueAction(action);
    },
    
    selectAll: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var getAllId = component.find("boxPack");
        
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);
                component.set("v.selectedCount", 1);
            }else{
                component.find("boxPack").set("v.value", false);
                component.set("v.selectedCount", 0);
            }
        }
        else{
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } 
            else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            } 
        }  
        
    },
    
    congaEmails : function(component, event, helper){
        var sendId = [];
        var getAllId = component.find("boxPack");
        if(getAllId.length > 0){
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    sendId.push(getAllId[i].get("v.text"));
                }
            }
        }
        else{
            if(component.find("boxPack").get("v.value")==true){
                sendId.push(component.find("boxPack").get("v.text"));
            }
        }
        if(sendId == null || sendId == 'undefined' || sendId ==""){
            component.set("v.errorMessage", "Please select atleast one User");
        }
        else{
            debugger;
             var generateTargetLetterStatus = component.get("v.generateTargetLetterStatus");
            var action=component.get("c.setUpdateTargetRecords");
            action.setParams({
                "targetIds": sendId,
                "strStatus": generateTargetLetterStatus
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() == "SUCCESS") {
                    var strResponse = response.getReturnValue();
                    if(strResponse =='SUCCESS'){
                        component.set("v.errorMessage","");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Letter Sent Successfully.",
                            "type" : "success"
                        });
                        
                        toastEvent.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else{
                        
                        component.set("v.errorMessage","Error !! "+strResponse);
                    } 
                }
            });
            $A.enqueueAction(action);
        }
    }
    
})