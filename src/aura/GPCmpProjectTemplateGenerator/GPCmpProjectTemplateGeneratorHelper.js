({
    getProjectTemplateService: function(component) {
        var recordId = component.get("v.recordId");

        if (recordId) {
            var getDefaultProjectTemplate = component.get("c.getProjectTemplate");

            getDefaultProjectTemplate.setParams({
                "projectTemplateId": component.get("v.recordId")
            });

            getDefaultProjectTemplate.setCallback(this, function(response) {
                this.getDefaultTemplateHandler(response, component);
            });

            $A.enqueueAction(getDefaultProjectTemplate);
        } else {
            var parsedDefaultJson = this.defaultTemplate;
            this.setProjectTemplate(component, parsedDefaultJson, parsedDefaultJson);
            this.hideSpinner(component);
        }
    },
    getDefaultTemplateHandler: function(response, component) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setTemplateData(component, responseData.response);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    createAssesmentTemplateService: function(component) {
        if (component.get("v.templateActivated") &&
            !confirm("Once a template is Activated you can not modify it, are you sure you want to continue?")) {
            return;
        }

        this.showSpinner(component);
        var createProjectTemplate = component.get("c.createProjectTemplate");

        createProjectTemplate.setParams({
            "defaultTemplateId": component.get("v.defaultTemplateId"),
            "serializedTemplateRecord": this.getTemplateRecord(component)
        });

        createProjectTemplate.setCallback(this, function(response) {
            this.createProjectTemplateHandler(response, component);
        });

        $A.enqueueAction(createProjectTemplate);
    },
    getTemplateRecord: function(component) {
        var defaultTemplate = component.get("v.defaultTemplate");
        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var flatDefaultTemplate = this.getFlatJson(defaultTemplate);
        var flatAssesmentTemplate = this.getFlatJson(assesmentTemplateData);

        var deltaJSON = {};

        for (var key in flatDefaultTemplate) {
            for (var attribute in flatDefaultTemplate[key]) {
                if (flatDefaultTemplate[key][attribute] !== flatAssesmentTemplate[key][attribute]) {
                    deltaJSON[key] = flatAssesmentTemplate[key];
                }
            }
        }

        var stageWiseFieldName = this.getStageWiseField(flatAssesmentTemplate);

        var finalJSON = JSON.stringify(flatAssesmentTemplate);
        var textAreaOffset = 131072;

        var finalJSON1 = finalJSON.length > textAreaOffset ? finalJSON.substring(0, textAreaOffset) : finalJSON;
        var finalJSON2 = finalJSON.length > textAreaOffset * 2 ?
            finalJSON.substring(textAreaOffset, textAreaOffset * 2) :
            (finalJSON.length > textAreaOffset ? finalJSON.substring(textAreaOffset) : '');
        var finalJSON3 = finalJSON.length > textAreaOffset * 3 ?
            finalJSON.substring(textAreaOffset * 2, textAreaOffset * 3) :
            (finalJSON.length > textAreaOffset * 2 ? finalJSON.substring(textAreaOffset * 2) : '');

        var templateData = {
            "defaultJSON1": JSON.stringify(defaultTemplate),
            "deltaJSON1": JSON.stringify(deltaJSON),
            "finalJSON1": finalJSON1,
            "finalJSON2": finalJSON2,
            "finalJSON3": finalJSON3,
            "templateName": component.get("v.templateName"),
            "businessType": component.get("v.businessType"),
            "businessName": component.get("v.businessName"),
            "assesmentTemplateId": component.get("v.recordId"),
            "isActive": component.get("v.templateActivated"),
            "isWithoutSFDC": Boolean(component.get("v.isWithoutSFDC")),
            "businessGroupL1": component.get("v.businessGroupL1"),
            "businessSegmentL2": component.get("v.businessSegmentL2"),
            "stageWiseFieldJson": stageWiseFieldName
        };
        return JSON.stringify(templateData);
    },
    getFlatJson: function(templateJson) {
        var flatJson = {};
        if (templateJson.listOfConfiguration)
            templateJson.listOfConfiguration.forEach(function(objectConfiguration) {
                if (objectConfiguration.listOfSection)
                    objectConfiguration.listOfSection.forEach(function(section) {
                        if (section.listOfFields)
                            section.listOfFields.forEach(function(field) {
                                var key = objectConfiguration["objectName"] + '___' + section["SectionName"] + '___' + field.ApiName;

                                field["sectionName"] = section["SectionName"];
                                field["isSectionVisible"] = section["isVisible"];
                                field["numberOfDefaultRowToDisplay"] = objectConfiguration["numberOfDefaultRowToDisplay"];
                                flatJson[key] = field;
                            });
                    });
            });
        return flatJson;
    },
    createProjectTemplateHandler: function(response, component) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        var templateId = component.get("v.recordId");
        var successMessage;
        if (templateId) {
            successMessage = 'Project template successfully updated';
        } else {
            successMessage = 'Project template successfully created';
        }
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', successMessage);
            this.redirectToSobject(responseData.response, 'related');
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setTemplateData: function(component, responseData) {
        var returnedResponse = JSON.parse(responseData) || {};
        var projectTemplateObj = returnedResponse["assesmentTemplate"];
        var numberOfProjectsUnderTemplate = returnedResponse["numberOfProjectsUnderTemplate"];
        
		var templateAlreadyActivated = returnedResponse["isSystemAdmin"] ? false : (numberOfProjectsUnderTemplate > 0 && projectTemplateObj["GP_Active__c"]);
		//var templateAlreadyActivated = numberOfProjectsUnderTemplate > 0 && projectTemplateObj["GP_Active__c"];

        var recordId = component.get("v.recordId");
        var parsedDefaultJson = JSON.parse(JSON.stringify(this.defaultTemplate));
        var serializedDeltaJSON = this.getConcatenatedDeltaJSON(projectTemplateObj);
        var parsedDeltaJson = serializedDeltaJSON ? JSON.parse(serializedDeltaJSON) : {};
        var finalTemplate = this.getMergedTemplate(parsedDeltaJson, parsedDefaultJson);

        parsedDefaultJson = JSON.parse(JSON.stringify(this.defaultTemplate));

        component.set("v.templateName", projectTemplateObj["Name"]);
        component.set("v.businessType", projectTemplateObj["GP_Business_Type__c"]);
        component.set("v.businessName", projectTemplateObj["GP_Business_Name__c"]);
        component.set("v.templateActivated", projectTemplateObj["GP_Active__c"]);
        component.set("v.templateAlreadyActivated", templateAlreadyActivated);
        component.set("v.businessGroupL1", projectTemplateObj["GP_Business_Group_L1__c"]);
        component.set("v.businessSegmentL2", projectTemplateObj["GP_Business_Segment_L2__c"]);
        component.set("v.defaultTemplateId", projectTemplateObj["Id"]);
        component.set("v.isWithoutSFDC", projectTemplateObj["GP_Without_SFDC__c"]);

        this.setProjectTemplate(component, parsedDefaultJson, finalTemplate);
    },
    setProjectTemplate: function(component, defaultTemplate, finalTemplate) {
        defaultTemplate = this.getClonedTemplate(defaultTemplate);
        finalTemplate = this.getClonedTemplate(finalTemplate);
        var activeTab = finalTemplate["listOfConfiguration"][0]["objectName"];

        component.set("v.defaultTemplate", defaultTemplate);
        component.set("v.assesmentTemplateData", finalTemplate);
        component.set("v.activeTab", activeTab);
    },
    getConcatenatedDeltaJSON: function(projectTemplateObj) {
        var serializedDeltaJSON = '';

        if (!$A.util.isEmpty(projectTemplateObj.GP_Delta_JSON_1__c))
            serializedDeltaJSON += projectTemplateObj.GP_Delta_JSON_1__c;
        if (!$A.util.isEmpty(projectTemplateObj.GP_Delta_JSON_2__c))
            serializedDeltaJSON += projectTemplateObj.GP_Delta_JSON_2__c;
        if (!$A.util.isEmpty(projectTemplateObj.GP_Delta_JSON_3__c))
            serializedDeltaJSON += projectTemplateObj.GP_Delta_JSON_3__c;

        return serializedDeltaJSON;
    },
    handleFailedCallback: function(component, responseData) {
        var errorMessage = responseData.message || 'Something has went wrong!';
        component.set("v.errorMessage", errorMessage);
        //this.showToast('Error', 'error', errorMessage);
    },
    hasValidName: function(component) {
        var templateName = component.get("v.templateName");
        var businessType = component.get("v.businessType");
        var businessName = component.get("v.businessName");

        return ((!$A.util.isEmpty(templateName)) &&
            (!$A.util.isEmpty(businessType) &&
                businessType !== "--NONE--"));
    },
    getMergedTemplate: function(deltaTemplate, templateJson) {
        for (var i = 0; templateJson.listOfConfiguration && i < templateJson.listOfConfiguration.length; i += 1) {
            var objectConfiguration = templateJson.listOfConfiguration[i];
            for (var j = 0; objectConfiguration.listOfSection && j < objectConfiguration.listOfSection.length; j += 1) {
                var section = objectConfiguration.listOfSection[j];
                for (var k = 0; section.listOfFields && k < section.listOfFields.length; k += 1) {
                    var field = section.listOfFields[k];
                    var key = objectConfiguration["objectName"] + '___' + section["SectionName"] + '___' + field.ApiName;
                    if (deltaTemplate[key]) {
                        section.listOfFields[k] = deltaTemplate[key];
                        section["isVisible"] = deltaTemplate[key]["isSectionVisible"];
                        objectConfiguration.listOfSection[j] = section;
                        templateJson.listOfConfiguration[i] = objectConfiguration;
                    }
                }
            }
        }
        return templateJson;
    },
    getClonedTemplate: function(obj) {
        return JSON.parse(JSON.stringify(obj));
    },
    getIsChanged: function(currentField, currentDefaultField) {
        return currentField.Label !== currentDefaultField.Label ||
            currentField.isVisible !== currentDefaultField.isVisible ||
            currentField.isRequired !== currentDefaultField.isRequired ||
            currentField.isReadOnly !== currentDefaultField.isReadOnly ||
            currentField.isreadOnlyAfterApproval !== currentDefaultField.isreadOnlyAfterApproval;
    },
    getStageWiseField: function(finalJSON) {
        var stageWiseFieldJson = {};
        var mapOfStageNameToIndex = {
            "Cost Charging": 1,
            "Billing & Cost Charging": 2,
            "Revenue & Cost Charging": 3,
            "Approved": 4
        };

        for (var key in finalJSON) {
            var keyList = key.split("___");
            var objectName = keyList[0];
            var mapOfObjectWiseStage = stageWiseFieldJson[objectName] || {};
            var stageName = finalJSON[key]["stageName"];

            if ($A.util.isEmpty(stageName)) {
                continue;
            }

            var mapOfStageWiseFields = mapOfObjectWiseStage[stageName] || {};
            var listOfFields = mapOfStageWiseFields["fields"] || [];

            listOfFields.push(finalJSON[key]["ApiName"]);
            mapOfStageWiseFields["fields"] = listOfFields;
            mapOfStageWiseFields["index"] = mapOfStageNameToIndex[stageName];

            mapOfObjectWiseStage[stageName] = mapOfStageWiseFields;

            stageWiseFieldJson[objectName] = mapOfObjectWiseStage;
        }

        return JSON.stringify(stageWiseFieldJson);
    },
    validateStageWiseFields: function(component) {
        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var isValid = true;

        if (assesmentTemplateData.listOfConfiguration) {
            for (var i = 0; i < assesmentTemplateData.listOfConfiguration.length; i += 1) {
                var objectConfiguration = assesmentTemplateData.listOfConfiguration[i];
                objectConfiguration['hasError'] = false;
                if (objectConfiguration.listOfSection) {
                    for (var j = 0; j < objectConfiguration.listOfSection.length; j += 1) {

                        var section = objectConfiguration.listOfSection[j];

                        if (section.listOfFields) {
                            for (var k = 0; k < section.listOfFields.length; k += 1) {
                                var field = section.listOfFields[k];
                                var stageName = field['stageName'];
                                if((stageName === 'Cost Charging' || 
                                    stageName === 'Approved Support') && 
                                    !field['isRequired']) {
                                    debugger;
                                    field['errorMessage'] = 'This field can not be in '+ stageName + ' stage';
                                    objectConfiguration['hasError'] = true;
                                    isValid = false;
                                } else {
                                    field['errorMessage'] = null;
                                }
                                section.listOfFields[k] = field;
                            }
                        }
                        objectConfiguration.listOfSection[j] = section;
                    }
                }
                assesmentTemplateData.listOfConfiguration[i] = objectConfiguration;
            }
        }

        component.set("v.assesmentTemplateData", assesmentTemplateData);

        return isValid;
    }
})