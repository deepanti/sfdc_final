({
    doInit: function(component, event, helper) {
        helper.getProjectTemplateService(component);
    },
    activateTab: function(component, event, helper) {
        var activeTab = event.currentTarget.id;
        component.set("v.activeTab", activeTab);
    },
    saveAssesmentTemplate: function(component, event, helper) {
        if(!helper.hasValidName(component)) {
            helper.showToast('Warning', 'warning', 'Please enter a valid template name & Type');
            return;
        }

        if (helper.validateStageWiseFields(component)) {
            helper.createAssesmentTemplateService(component);
        }
    },
    cancel: function(component, event, helper) {
        helper.redirectToListView("GP_Project_Template__c");
    },
    refreshAllRow: function(component, event, helper) {

        var indexes = event.currentTarget.id ? event.currentTarget.id.split("-") : [0, 0];
        var assesmentTemplateData = component.get("v.assesmentTemplateData");

        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][indexes[0]]["listOfSection"][indexes[1]];
        var currentSection = JSON.parse(JSON.stringify(currentDefaultSection));

        assesmentTemplateData["listOfConfiguration"][indexes[0]]["listOfSection"][indexes[1]] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    refreshRow: function(component, event, helper) {
        var indexes = event.currentTarget.id ? event.currentTarget.id.split("-") : [0, 0, 0];
        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var currentSection = assesmentTemplateData["listOfConfiguration"][indexes[0]]["listOfSection"][indexes[1]];
        var currentField = currentSection["listOfFields"][indexes[2]];

        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][indexes[0]]["listOfSection"][indexes[1]];
        var currentDefaultField = currentDefaultSection["listOfFields"][indexes[2]];
        currentSection["listOfFields"][indexes[2]] = JSON.parse(JSON.stringify(currentDefaultField));
        assesmentTemplateData["listOfConfiguration"][indexes[0]]["listOfSection"][indexes[1]] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    sectionVisiblilityChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");

        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex]["isVisible"] = event.currentTarget.checked;
        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    expandCurrentSection: function(component, event, helper) {
        var configurationIndex = Number(event.currentTarget.id.split("-")[0]);
        var sectionIndex = Number(event.currentTarget.id.split("-")[1]);

        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var currentObjectConfiguration = assesmentTemplateData["listOfConfiguration"][configurationIndex];
        var defaultTemplate = component.get("v.defaultTemplate");
        var defaultObjectConfiguration = defaultTemplate["listOfConfiguration"][configurationIndex];

        for (var i = 0; i < currentObjectConfiguration["listOfSection"].length; i += 1) {
            if (i === sectionIndex) {
                currentObjectConfiguration["listOfSection"][i]["isExpanded"] = true;
                defaultObjectConfiguration["listOfSection"][i]["isExpanded"] = true;
            } else {
                currentObjectConfiguration["listOfSection"][i]["isExpanded"] = false;
                defaultObjectConfiguration["listOfSection"][i]["isExpanded"] = false;
            }
        }
        assesmentTemplateData["listOfConfiguration"][configurationIndex] = currentObjectConfiguration;
        defaultTemplate["listOfConfiguration"][configurationIndex] = defaultObjectConfiguration;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
        component.set("v.defaultTemplate", defaultTemplate);
    },
    labelChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");
        var fieldIndex = event.target.getAttribute("data-field-index");

        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var currentSection = assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentField = currentSection["listOfFields"][fieldIndex];

        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentDefaultField = currentDefaultSection["listOfFields"][fieldIndex];

        currentField["Label"] = event.currentTarget.value;
        var isChanged = helper.getIsChanged(currentField, currentDefaultField);

        currentField["isChanged"] = isChanged;
        currentSection["listOfFields"][fieldIndex] = currentField;
        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex] = currentSection;
        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    visibleChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");
        var fieldIndex = event.target.getAttribute("data-field-index");
        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentDefaultField = currentDefaultSection["listOfFields"][fieldIndex];

        var assesmentTemplateData = component.get("v.assesmentTemplateData");

        var currentSection = assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentField = currentSection["listOfFields"][fieldIndex];
        currentField["isVisible"] = event.currentTarget.checked;

        var isChanged = helper.getIsChanged(currentField, currentDefaultField);
        currentField["isChanged"] = isChanged;
        currentSection["listOfFields"][fieldIndex] = currentField;

        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    mandatoryChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");
        var fieldIndex = event.target.getAttribute("data-field-index");
        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentDefaultField = currentDefaultSection["listOfFields"][fieldIndex];

        var assesmentTemplateData = component.get("v.assesmentTemplateData");

        var currentSection = assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentField = currentSection["listOfFields"][fieldIndex];

        currentField["isRequired"] = event.currentTarget.checked;

        var isChanged = helper.getIsChanged(currentField, currentDefaultField);
        currentField["isChanged"] = isChanged;

        currentSection["listOfFields"][fieldIndex] = currentField;
        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    readOnlyChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");
        var fieldIndex = event.target.getAttribute("data-field-index");

        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentDefaultField = currentDefaultSection["listOfFields"][fieldIndex];
        var assesmentTemplateData = component.get("v.assesmentTemplateData");

        var currentSection = assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentField = currentSection["listOfFields"][fieldIndex];

        currentField["isReadOnly"] = event.currentTarget.checked;

        var isChanged = helper.getIsChanged(currentField, currentDefaultField);
        currentField["isChanged"] = isChanged;

        currentSection["listOfFields"][fieldIndex] = currentField
        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    },
    readOnlyAfterApprovalChangeHandler: function(component, event, helper) {
        var configurationIndex = event.target.getAttribute("data-configuration-index");
        var sectionIndex = event.target.getAttribute("data-section-index");
        var fieldIndex = event.target.getAttribute("data-field-index");

        var defaultTemplate = component.get("v.defaultTemplate");
        var currentDefaultSection = defaultTemplate["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentDefaultField = currentDefaultSection["listOfFields"][fieldIndex];

        var assesmentTemplateData = component.get("v.assesmentTemplateData");
        var currentSection = assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex];
        var currentField = currentSection["listOfFields"][fieldIndex];

        currentField["isreadOnlyAfterApproval"] = event.currentTarget.checked;

        var isChanged = helper.getIsChanged(currentField, currentDefaultField);
        currentField["isChanged"] = isChanged;

        currentSection["listOfFields"][fieldIndex] = currentField;

        assesmentTemplateData["listOfConfiguration"][configurationIndex]["listOfSection"][sectionIndex] = currentSection;

        component.set("v.assesmentTemplateData", assesmentTemplateData);
    }
})