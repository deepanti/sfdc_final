({
    doInit:function(component, event, helper)
    {
        helper.flagSet(component, event, helper);
    },
   
    handleSuccess : function(component, event, helper) {
        helper.fireToastSuccess(component, event, helper,'Thanks for giving the feedback');
        helper.setSurvey(component, event, helper);
    },
    handleSubmit:function(component, event, helper) {
        component.set("v.isVisible",true);
        event.preventDefault();
        var check= helper.validateAll(component, event, helper);
        if(check)
        {
            var rateUI = component.find("rateUI");
        	var rateUIValue = rateUI.get("v.value");
            var comments = component.find("comments");
            var commentsValue = comments.get("v.value");
            
            if(rateUIValue<=3)
            {
                
                if($A.util.isUndefinedOrNull(commentsValue) || commentsValue == '')
                {
                    $A.util.addClass(comments,'slds-has-error');
                   	helper.fireToastError(component, event, helper,'Tell us how can we improve your experience');
                }
                else
                {
                    $A.util.removeClass(comments,'slds-has-error');
                    component.find("submitForm").submit();
                }
                
            }
            else
            {
                if($A.util.isUndefinedOrNull(commentsValue) || commentsValue == '')
                {
                    $A.util.addClass(comments,'slds-has-error');
                   	helper.fireToastError(component, event, helper,'Tell us what was perfect in mySales');
                }
                else
                {
                    $A.util.removeClass(comments,'slds-has-error');
                    component.find("submitForm").submit();
                }
                
            }
        }
        
        else
            helper.fireToastError(component, event, helper,'Error something went wrong!');
    },
    cancelSurvey:function(component, event, helper) {
        component.set("v.showModal",false);
        component.set("v.isVisible",false);
    },
    showError : function(component, event, helper) {
        
        var error = event.getParams();
        //alert(JSON.stringify(error))
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastError(component, event, helper, errorMsg);
        
    },
    //adding stars
    afterScriptsLoaded : function(component, event, helper) {
        var domEl = component.find("ratingArea").getElement();
        var currentRating = component.get('v.value');
        var readOnly = component.get('v.readonly');
        var maxRating = 5;
        var callback = function(rating) {
            component.set('v.value',rating);
        }
        component.ratingObj = rating(domEl,currentRating,maxRating,callback,readOnly);
    },

    onValueChange: function(component,event,helper) {
        if (component.ratingObj) {
            var value = component.get('v.value');
            component.ratingObj.setRating(value,false);
            component.set("v.valueInPicklist",value.toString());
        }
    }
    //adding stars
})