({
    flagSet:function(component, event, helper)
    {
        var action = component.get("c.userflagSet");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set("v.UserFlags",response.getReturnValue());
            }
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
                            
            }
            
        });
        $A.enqueueAction(action); 
    },
    setSurvey:function(component, event, helper)
    {
        
        component.set("v.isVisible",true);
        var action = component.get("c.setField");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.isVisible",false);
                component.set("v.showModal",false);
            }
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
                component.set("v.isVisible",false);
                component.set("v.showModal",false);                
            }
            
        });
        $A.enqueueAction(action); 
    },
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var rateUI = component.find("rateUI");
        var rateUIValue = rateUI.get("v.value");
        if($A.util.isUndefinedOrNull(rateUIValue) || rateUIValue == '')
        {
            $A.util.addClass(rateUI,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(rateUI,'slds-has-error');
        }
       
        return proceedToSave;
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    }
})