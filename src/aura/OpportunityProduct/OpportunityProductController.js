({
   doInit : function(component, event, helper) {
       console.log(component.get("v.recordId"));
      $A.createComponent(
        "c:OpportunitySearchProducts",   //"c:SearchProducts",
        
         {
             "recordId" : component.get("v.recordId")
         },
         function(newCmp){
            if (component.isValid()) {
               component.set("v.body", newCmp);
            }
         }
      );
   },
    
   NavigateComponent : function(component,event,helper){ 
      //alert(JSON.stringify(event.getParam("productList")));
      $A.createComponent(
         "c:AddProducts",
         {
           "productList" : event.getParam("productList"),
             "recordId" : event.getParam("recordId")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );
   },
    NavigateToSearchComponent : function(component,event,helper){ 
      //alert(JSON.stringify(event.getParam("productList")));
      $A.createComponent(
         "c:OpportunitySearchProducts",
         {
           "finalSelectedProductList" : event.getParam("selectedProductList"),
             "recordId" : event.getParam("recordId")
         },
         function(newCmp){
            if (component.isValid()) {
                component.set("v.body", newCmp);
            }
         }
      );
   }
})