({
    handleActive: function (component, event, tabSelected) {
        var tab = component.find(tabSelected);
        if(component.get("v.isEventFired"))
        {
            //Set the previous tab
            component.set("v.selectedPrevTab", component.get("v.selectedTab"));
            localStorage.setItem('LatestTabId',tabSelected);
        }
        else
        {
            //block tab change
            component.set("v.selectedTab", component.get("v.selectedPrevTab"));
        }
        component.set("v.isEventFired",false);
    },
    injectComponent: function (name, target, page, recId, showButton) {
        $A.createComponent(name, {
            pagename : page,
            recordId : recId,
            showButton : showButton,
        }, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                throw new Error(error);
            }
        });
    },
    
    recordUpdated : function(component, event, helper, forceRecord){
            //Update the tab selected on record load
            component.set("v.isEventFired",true);
            component.set("v.selectedTab", forceRecord);
            component.set('v.selectedPrevTab',forceRecord); 
            helper.handleActive(component, event, forceRecord);
    }
})