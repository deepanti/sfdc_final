({
	recallApprovalRequest : function(component) {
		 var  submitAction = component.get("c.recallRequest");
		   submitAction.setParams({
            "projectId": component.get("v.recordId"),
        });
        
        submitAction.setCallback(this, function(response) {
            var responseData = response.getReturnValue();
            var toastEvent;

            if (responseData.isSuccess) {
                toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success",
                    "type": "success",
                    "message": responseData.message
                });
                toastEvent.fire();

                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": component.get("v.recordId"),
                    "slideDevName": "related"
                }); 
                navEvt.fire();
            } else {
                toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error",
                    "type": "error",
                    "message": responseData.message
                });
                toastEvent.fire();

                $A.get("e.force:closeQuickAction").fire();
            }
        });
	
	 $A.enqueueAction(submitAction);
	}
})