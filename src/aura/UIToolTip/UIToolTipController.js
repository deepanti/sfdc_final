({
    init: function(component, event, helper) {
        helper.calculateNubbinPlacement(component);
    },
    updateNubbinPosition: function(component, event, helper) {
        helper.calculateNubbinPlacement(component);
    },
    showTooltip: function(component, event, helper) {
        var tooltipStyleOptions = helper.calculateTooltipPosition(component);
        var tooltipStyle = 'position: absolute; width: ';
        tooltipStyle += tooltipStyleOptions.width + 'px;left: ';
        tooltipStyle += tooltipStyleOptions.tooltipXPos + 'px; top: ';
        tooltipStyle += tooltipStyleOptions.tooltipYPos + 'px; z-index: 10000;';

        component.set('v.tooltipStyle', tooltipStyle);
        component.set('v.showTooltip', true);
    },
    hideTooltip: function(component, event, helper) {
        var tooltipStyle = component.find('tooltipStyle').getElement();
        tooltipStyle.innerHTML = '';

        component.set('v.showTooltip', false);
        component.set('v.tooltipStyle', '');
    }
})