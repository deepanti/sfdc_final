({
    /**
     * Handle Component Initialization event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    doInit: function(component, event, helper) {
        helper.doInitHelper(component);
    },
    projectInitializedHandler: function(component, event, helper) {
        var projectRecord = event.getParam("projectRecord");
        var projectTemplate = event.getParam("projectTemplate");

        component.set("v.projectRecord", projectRecord);
        component.set("v.projectTemplate", projectTemplate);
    }
})