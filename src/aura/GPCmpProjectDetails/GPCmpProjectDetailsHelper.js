({
    /**
     * Handle Component Initialization event.
     * @param {Aura.Component} component - this component
     * @description when the component is opened in new mode then fetch record types
     * other wise if the component is opened in edit mode then fetch all the project data along with its template 
     * and picklist values.
     */
    doInitHelper: function(component) {
        var recordId = component.get("v.recordId");

        if (recordId) {
            this.getProjectData(component, recordId);
        } 
    },
    /**
     * Fetches the project data for given projectId.
     * @param {Aura.Component} component - this component
     * @param {Id} SFDC Project Id - SFDC ID of Project
     * @description Fetches the project data for given projectId.
     */
    getProjectData: function(component, projectId) {
        var getProjectData = component.get("c.getProjectData");

        getProjectData.setParams({
            "projectId": projectId
        });

        getProjectData.setCallback(this, function(response) {
            this.getProjectDataHandler(component, response);
        });

        $A.enqueueAction(getProjectData);
    },
    /**
     * Handle getProjectData proimse.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Handle getProjectData proimse.
     */
    getProjectDataHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectData(component, responseData);
        } else {
            component.set("v.errorMessage", responseData.message);
        }
    },
    /**
     * Sets Project Data is the Aura response has no error.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Sets Project Data is the Aura response has no error.
     */
    setProjectData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        var projectTemplate = this.getConcatenatedTemplate(responseJson.ProjectTemplate);
        var projectRecord = responseJson.Project;

        component.set("v.projectRecord", projectRecord);
        component.set("v.projectTemplate", projectTemplate);
    },
    
    /**
     * Returns Concatenated ProjectTemplateData.
     * @param {SObject} projectTemplate SObject
     * @description Returns Concatenated ProjectTemplateData.
     */
    getConcatenatedTemplate: function(projectTemplate) {
        var finalTemplate = '';
        if (projectTemplate && projectTemplate.GP_Final_JSON_1__c)
            finalTemplate += projectTemplate.GP_Final_JSON_1__c;
        if (projectTemplate && projectTemplate.GP_Final_JSON_2__c)
            finalTemplate += projectTemplate.GP_Final_JSON_2__c;
        if (projectTemplate && projectTemplate.GP_Final_JSON_3__c)
            finalTemplate += projectTemplate.GP_Final_JSON_3__c;
        return JSON.parse(finalTemplate);
    }
})