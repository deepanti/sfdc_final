({
    doInit : function(component, event, helper) {
        var action = component.get("c.dealAgeing");
        action.setParams({
            parentId : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            var currentDate;
            if (a.getState() === "SUCCESS") {
                var opp = a.getReturnValue();
                component.set("v.opp", opp);
              //  var colsedate = opp[0].CloseDate;
                var Insight = opp[0].Insight__c;
                var nonage  = opp[0].Transformation_deal_ageing_for_non_ts__c;
                var Aeging = Insight + nonage;
                component.set("v.Aeging",Aeging) 
               /* var dateFormatTotime = new Date(colsedate);
                var increasedDate = new Date(dateFormatTotime.getTime() +(Insight *86400000));
                var dd = increasedDate.getDate();
                var mm = increasedDate.getMonth() + 1;
                var y = increasedDate.getFullYear();
                var someFormattedDate = dd + '/'+ mm + '/'+ y;
                console.log(':---someFormattedDate---: '+someFormattedDate);
                component.set("v.RecomdedDate",someFormattedDate)  */
            } else if (a.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);  
    
    }
    
    
})