({
   
    saveRecord : function(component, event, helper) {
       
        component.set("v.IsSpinner",true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            
            if(component.get("v.OppRecord.StageNameCustom__c") =='1. Discover')
                    {
                        component.set("v.OppRecord.StageNameCustom__c","2. Define");
                        localStorage.setItem('StageNameCustom__c',"2. Define");
                        component.set("v.StageName1",'2. Define');
                    }
            
            if(localStorage.getItem('StageName')=='1. Discover')
            {
                component.set("v.StageName","2. Define");
                localStorage.setItem('StageName',"2. Define");
            }
            component.find("recordEditForm").submit(); 
            localStorage.setItem('LatestTabId','8');
        }
        else
        {
            helper.fireToastEvent(component, event, helper, 'Required fields are missing');
            component.set("v.IsSpinner", false);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    saveAndNext : function(component, event, helper) {
        component.set("v.IsSpinner", true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            if(localStorage.getItem('MaxTabId')<9)
            {
                component.set("v.MaxSelectedTabId", "9");
                localStorage.setItem('MaxTabId',9);
                 if(component.get("v.OppRecord.StageNameCustom__c") =='1. Discover')
                    {
                        component.set("v.OppRecord.StageNameCustom__c","2. Define");
                        localStorage.setItem('StageNameCustom__c',"2. Define");
                        component.set("v.StageName1",'2. Define');
                    }
                if(localStorage.getItem('StageName')=='1. Discover')
                {
                    component.set("v.StageName","2. Define");
                    localStorage.setItem('StageName',"2. Define");
                }
            }
            component.find("recordEditForm").submit();
            localStorage.setItem('LatestTabId','9');
            window.scrollTo(0, 0);
        }
        else
        {
            debugger;
            helper.fireToastEvent(component, event, helper,'Required fields are missing');
            component.set("v.IsSpinner", false);
        }
    },
    
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',7);
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "7"});
        appEvent.fire();
        window.scrollTo(0, 0);
    },
    
    onSuccess:function(component, event, helper){
         var appEvent = $A.get("e.c:SetSalesPath");
        appEvent.fire();
        helper.fireToastSuccess(component, event, helper, 'Opportunity Saved Successfully');
        component.set("v.IsSpinner", false);
        var compEvents = component.getEvent("componentEventFired");
           // compEvents.setParams({ "SelectedTabId" : "9"});
            compEvents.fire();
    },
    showError : function(component, event, helper) {
        localStorage.setItem('MaxTabId',8);
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastEvent(component, event, helper, errorMsg);
        component.set("v.IsSpinner", false);
    },
    
    
})