({
    
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var dealAdmin = component.find("DealAdministrator");
        var dealAdminName = dealAdmin.get("v.value");
        if($A.util.isUndefinedOrNull(dealAdminName) || dealAdminName == '')
        {
            $A.util.addClass(dealAdmin,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(dealAdmin,'slds-has-error');
        }
       var AnnuityProject = component.find("AnnuityProject");
        var AnnuityProjectVar = AnnuityProject.get("v.value");
        if($A.util.isUndefinedOrNull(AnnuityProjectVar) || AnnuityProjectVar == '')
        {
            $A.util.addClass(AnnuityProject,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(AnnuityProject,'slds-has-error');
        } 
        
        return proceedToSave;
    },
    fireToastEvent : function(component, event, helper, message) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "error",
            "message": message,
            "type": "error"
        });
        toastEvent.fire();
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
})