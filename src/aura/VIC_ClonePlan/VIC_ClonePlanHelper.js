({
    doInitHelper : function(component, event){
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) {
            var getFinancialYearState = response.getState();
            var valuegetFinancialYear = response.getReturnValue();
            if (getFinancialYearState === "SUCCESS") {
                component.set("v.financialYearValueList", valuegetFinancialYear);
            }
        });
        $A.enqueueAction(action);
 	},
    checkyear : function(component, event){
        var action = component.get("c.getYearValid");
        action.setParams({
		   "recrdId": component.get("v.recordId"),
		   "strYear": component.get("v.defaultYear")
		 
		 });
        
        action.setCallback(this, function(response) {
            var getFinancialYearState = response.getState();
            var valueIsValid = response.getReturnValue();
            
            if (getFinancialYearState === "SUCCESS" ){
                    if(valueIsValid===true){
                    var Isvalid= component.set("v.isDisabled", valueIsValid);
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Plan for Same or previous Year cannot be created."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    toastEvent.fire();
                  
                   }
                  else{
                       this.saveData(component, event);                
                  }
           }
      });
        $A.enqueueAction(action);
  },
    saveData :  function(component, event){ 
      var action = component.get("c.savePlan");
      action.setParams({
		   "recrdId": component.get("v.recordId"),
		   "strYear": component.get("v.defaultYear")
		 
		 });
        action.setCallback(this, function(response){
          var saveState = response.getState(); 
            if(saveState ==="SUCCESS"){
              var cloneId = response.getReturnValue(); 
              
               var sObectEvent = $A.get("e.force:navigateToSObject");
                sObectEvent .setParams({
                "recordId": cloneId
                
               });
            sObectEvent.fire(); 
         
           }
     });
     $A.enqueueAction(action);                      
    
    }
 
})