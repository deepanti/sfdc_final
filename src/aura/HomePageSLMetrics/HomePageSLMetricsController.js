({
    doInit : function(component, event, helper) {
        var action = component.get("c.getMetricValues");
        action.setStorable();
        action.setCallback(this,function(response){
            if(response.getState()==='SUCCESS'){
                var data = response.getReturnValue();
                for(var i=0;i<data.length;i++)
                {
                    if(data[i].Total<0)
                    {
                        data[i].Color='red';
                        data[i].Total *= -1;
                    }
                    data[i].url = component.get("v.URL"+(i+1));
                    if(i==5)//Exception for VIC approvals being hard coded in the component
                        data[i].url = component.get("v.URL"+(i+2));
                }
                component.set("v.Metrics",data);
            }
            else
            {
                helper.showToast('error',response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }
 })