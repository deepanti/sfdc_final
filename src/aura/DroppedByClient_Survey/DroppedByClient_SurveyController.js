({
	init: function (component, event, helper) 
    {   
        var OpportunityList = component.get("v.OpportunityList");       
       	var Teststr = component.get("v.Teststr");
        var recordId = component.get("v.recordId");  
		var conid = component.get("v.conid"); 		
        var OppWrapperList1 = [];
            OppWrapperList1 = component.get("v.OppWrapperList");  
       // alert('OppWrapperList-===11=in=='+OppWrapperList1.length);
        var OppWrapperListJSON = new Array();
            OppWrapperListJSON = JSON.stringify(component.get("v.OppWrapperList"));
       //  alert('win survey ======OpportunityList======'+OpportunityList);
       // alert('win survey ======OppWrapperListJSON======'+OppWrapperListJSON.length);
      // var jsonRec =JSON.parse(JSON.stringify(OppWrapperList));
      //  alert('jsonRec-='+jsonRec[0]);
       var test=OppWrapperList1[0];     
        	var Opp_Name;
            var Opp_Source;
            var Opp_DealType;
        	var Comp_Presence;
			var Opp_TCV;
			var Opp_NatureOfWork;
        	var Opp_Stage;  
        
        for(var i=0;i<OppWrapperList1.length;i=i+1)
        {             
            var test1=OppWrapperList1[i];
           // alert('test======1111======'+JSON.stringify(test1));
            var jsnParse= JSON.parse(JSON.stringify(test1));
          //  alert('-=-=-=-'+jsnParse.OpportunityDataList[0].Name);
            Opp_Name= jsnParse.OpportunityDataList[0].Name;
            Opp_Source= jsnParse.OpportunityDataList[0].Target_Source__c;
            Opp_DealType= jsnParse.OpportunityDataList[0].Annuity_Project__c;
            Comp_Presence=jsnParse.OpportunityDataList[0].Deal_Type__c;
			Opp_TCV = jsnParse.OpportunityDataList[0].TCV1__c;
			Opp_NatureOfWork = jsnParse.OpportunityDataList[0].Nature_Of_Work_OLI__c ;
             Opp_Stage = jsnParse.OpportunityDataList[0].StageName ;
           //alert('Opp_NatureOfWork======1======'+Opp_NatureOfWork);
           //alert('Opp_TCV=====1======'+Opp_TCV);
          
        }
        
        component.set("v.Opp_Source",Opp_Source);
        component.set("v.Opp_DealType",Opp_DealType);
        component.set("v.Opp_Name",Opp_Name);
        component.set("v.Comp_Presence",Comp_Presence);
		component.set("v.Opp_TCV",Opp_TCV);
		component.set("v.Opp_NatureOfWork",Opp_NatureOfWork);
         component.set("v.Opp_Stage",Opp_Stage);
         
		//alert('=============before conditions=====');
		if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 && Opp_NatureOfWork )
		{
			//if( (Opp_NatureOfWork.includes('Delivery') && Opp_TCV > 1000000 ) || ( Opp_NatureOfWork.includes('Delivery') && Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 1000000 )  )
			if(Opp_TCV >= 1000000  && !Opp_Stage.includes('Prediscover') && !Opp_Stage.includes('1. Discover') )
            {               				
				component.set("v.IsSurvey9",true);
			}
			//else if(Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 100000 )
			else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000 && !Opp_Stage.includes('Prediscover') && !Opp_Stage.includes('1. Discover')  )
			{			
				component.set("v.IsSurvey9",true);							
			}
			else
			{			
				component.set("v.IsSurvey9",false);				
			}
		}
		else
		{
			//alert('=========else  blank condition======');
			component.set("v.IsSurvey9",false);	
		}
    },
        
     onGroup: function(cmp, evt) 
	 {
		var selected = evt.getSource().get("v.label");
		var selectedval= evt.getSource().get("v.value");
        var locid = evt.getSource().getLocalId();
		cmp.set("v.IsValidate",false);
         cmp.set("v.IsAllowed",true);
		var collection= cmp.get("v.SelectedOpt");
		collection.push(selectedval); 
        cmp.set("v.SelectedOpt",collection);
		
		var idcollection=cmp.get("v.selectedcmp");
		idcollection.push(locid);
        cmp.set("v.selectedcmp",idcollection);
		
		var Map = cmp.get("v.sectionLabels");
         Map[selected] = selectedval;
         cmp.set("v.sectionLabels",Map);
        
        
        
        var map=cmp.get("v.sectionLabels");    
        var count;
        var chkthrshold=0;
        var chknull=false; 
		for(var key in map)
        {	count=0;
         	if(map[key]!='')
            {
                chkthrshold++;
            }
            for(var key2 in map)
			{	
				if(map[key2]!='' || map[key]!='')
				{
					if(map[key2]==map[key])
					{	count++;
						
					}
				}
			}
           //alert(map[key]);  
		  // alert('=====count====='+count);
            if(count>1)
				cmp.set("v.IsValidate",true);
        
			if( map[key]=='')
				chknull=true;	
        }
		
		var size_sel=cmp.get("v.sectionLabels");
		//alert('=====size_sel.length====='+size_sel.length);
		//alert('=====chkthrshold========'+chkthrshold);
        if(chkthrshold>1)
        {	
            for(var i=0;i<18;i++)
                {	var c= 	cmp.find("selectItem"+i);
                     //alert(typeof c);
                     if(typeof c ==='undefined')
                     {
                         
                     }
                     else
                     {
                           cmp.find("selectItem"+i).set("v.disabled", true);
                           
                     }
                }            
        }
		if(chkthrshold>0)
		{
			
			var c = cmp.get("v.IsValidate");
            if(c == false )
				cmp.set("v.IsAllowed",false);
		}
        var idcol=cmp.get("v.selectedcmp");
        //alert('=======idcol========='+idcol);
			for(var i in idcol )
				{
					cmp.find(idcol[i]).set("v.disabled", false);
				}       
	},
    
    handleClick:function(cmp,evt)
    {         
		cmp.set("v.isSuccess", true);
        var map=cmp.get("v.sectionLabels");
        var custs = [];
        var count;
        for(var key in map)
        {	count=0;
            for(var key2 in map)
			{	
				if(map[key2]==map[key])
                {	count++;
                	//alert(map[key]);
                }
			}
           
            if(count>1)
            cmp.set("v.IsValidate",true);
        
        }     	
        
       //alert(map);
        
		var Opp_DealOutCome = cmp.get("v.OpportunityList");
		//var check_IsSurvey1 = cmp.get("v.IsSurvey1");
		
		var check_IsSurvey9 = cmp.get("v.IsSurvey9");
				
		if(check_IsSurvey9 != false)
		{				
			var LongArea = cmp.find("comments");
			var LongAreaVal = LongArea.get("v.value");			
			cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);  // q2   
			cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 		       
			var UpdatewithWLR = cmp.get("c.saveWLR");
            UpdatewithWLR.setParams({
                           
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey9",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
            });
			        
            // Configure the response handler for the action
            UpdatewithWLR.setCallback(this, function(response) 
             {
                var state = response.getState();
                if(cmp.isValid() && state === "SUCCESS") 
                {         
					cmp.set("v.isSuccess", false);
					 var recordId = cmp.get("v.recordId"); 					
					//sforce.one.navigateToSObject(recordId,"Opportunity");  
					function isLightningExperienceOrSalesforce1() 
                    {
                        return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
                    } 
                   if(isLightningExperienceOrSalesforce1()) 
                    {                       
                        sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
                    }
                    else
                    {
                       // window.location = '/'+recordId;   //'/006/o'
                       // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                    	window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                    } 
                   /*   var compEvents = component.getEvent("componentEventFired");
                    compEvents.setParams({ "SelectedTabId" : '19'});
                    compEvents.fire();
                    window.scrollTo(0, 0); */
                }
                else if (state === "ERROR") {
                    console.log('Problem saving WLR, response state: ' + state);
                   
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                 
                }
            });
			$A.enqueueAction(UpdatewithWLR);
		}
		else
		{
			//alert('=====else==survey 9=====');
			//cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);  // q2   
			cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 

		       
			var UpdatewithWLR = cmp.get("c.saveWLR");
            UpdatewithWLR.setParams({
                           
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey9",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
            });
			        
            // Configure the response handler for the action
            UpdatewithWLR.setCallback(this, function(response) 
             {
                var state = response.getState();
                if(cmp.isValid() && state === "SUCCESS") 
                {                   
					 var recordId = cmp.get("v.recordId");  
					 cmp.set("v.isSuccess", false);
					//sforce.one.navigateToSObject(recordId,"Opportunity");
					function isLightningExperienceOrSalesforce1() 
                    {
                        return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
                    } 
                    if(isLightningExperienceOrSalesforce1()) 
                    {                       
                        sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
                    }
                    else
                    {
                       // window.location = '/'+recordId;   //'/006/o'
                       // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                   
                    window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';}  
                    /* var compEvents = component.getEvent("componentEventFired");
                    compEvents.setParams({ "SelectedTabId" : '19'});
                    compEvents.fire();
                    window.scrollTo(0, 0); */
                }
                else if (state === "ERROR") {
                    console.log('Problem saving WLR, response state: ' + state);
                   
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                 
                }
            });
			$A.enqueueAction(UpdatewithWLR);
			
		}	
    },
    
     handleCancel: function(component, event, helper) 
    {      
         	var recordId = component.get("v.recordId");  
    		//sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
			//sforce.one.back(true);
			function isLightningExperienceOrSalesforce1() 
			{
				return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
			} 
			if(isLightningExperienceOrSalesforce1()) 
			{   
				//sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
				sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 
				sforce.one.back(true);
			}
			else
			{
				window.location = '/'+recordId;   //'/006/o'
			}    
    },
    
    reset: function(cmp,evt) 
    {	 
        cmp.set("v.IsValidate",false);
        for(var i=0;i<18;i++)
                {	var c= 	cmp.find("selectItem"+i);
                 //alert(typeof c);
                 if(typeof c ==='undefined')
                 {
                     
                 }
                 else 
                    {
                        cmp.find("selectItem"+i).set("v.disabled", false);
                      	cmp.find("selectItem"+i).set("v.value", null);
                    }
                    //alert("selectItem"+i);
                }
				cmp.set("v.IsAllowed",true);
                var Map = cmp.get("v.sectionLabels");
                Map={};
                
        		cmp.set("v.sectionLabels",Map);
        		var lst= cmp.get("v.selectedcmp");
        		lst=[];
        		cmp.set("v.selectedcmp",lst);				
				var checkReset_IsSurvey9 = cmp.get("v.IsSurvey9");		
							
				if(checkReset_IsSurvey9 == true)
				{
					cmp.find("comments").set("v.value", "");	
				}
    }
	
    
})