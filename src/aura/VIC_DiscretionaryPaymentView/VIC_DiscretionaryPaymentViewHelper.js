({
	getDiscretionaryPayments : function(component, event, helper) {
        //alert('===child call==');
		var screenName = component.get("v.screenName");
        var userId = component.get("v.userId");
        var action = component.get("c.getIncentiveDetails");
        action.setParams({ strUserId:userId, strScreenName : screenName });
        action.setCallback(this, function(response) {
        	var state = response.getState();            
            if (state === "SUCCESS") {             
                //component.set("v.isOpen", true);
                component.set('v.lstDiscretionaryPayments',response.getReturnValue());
            }else if (state === "ERROR") {
            	var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
	}
})