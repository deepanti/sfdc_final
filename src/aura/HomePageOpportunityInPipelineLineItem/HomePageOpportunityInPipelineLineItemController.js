({
    //function which opens the opportunity detail page
	openOpportunity : function(component, event, helper)
    { 
        var oppLst = component.get("v.oppList");
        var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
            "recordId": component.get("v.oppRecord.Id")
        });
        navEvt.fire();
    }
})