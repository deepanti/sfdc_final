({
    getFinancialYear : function(component,event,helper) {
        debugger;
        //var year=component.find("v.Year").get("v.value"); ;
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstYears",allValues);
                //component.set("v.Year", '2018'); 
                component.set("v.strFinancialYear",allValues[1]);
            }
        });
        $A.enqueueAction(action);
    },
    
    getRoles : function(component,event,helper) {
        var dt = new Date();
  		var currentFY = dt.getFullYear();        
        var action = component.get("c.getVicRole");
        action.setParams({"currentFinancialYear": currentFY});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstVicRole",allValues);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    currentYearVICRoles : function(component,event,helper){
        var selectedYear = component.find("Year").get("v.value");
        var action = component.get("c.getVicRole");
        action.setParams({"currentFinancialYear": selectedYear});
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues !=''){
                     component.set("v.lstVicRole",allValues);
                     component.find("Role").set("v.disabled",false);
                }else{
                    component.find("Role").set("v.disabled",true);
                }
               
            }
        });
        $A.enqueueAction(action);
    },
    
    getUsers : function(component,event,helper) {
        debugger;
        var selectedYear = component.get("v.strFinancialYear");
        var selectedRole = component.get("v.VicRole");
        var userrecord = component.get("v.userRecordsNumber");
        //userrecord=true;
        
        var role=component.find("Role");
        if(selectedRole == null || selectedRole == 'undefined' || selectedRole ==""){
            role.set("v.errors", [{message:"Please Select VIC Role."}]);
            component.set("v.statusShowHide",false);
            return null;
        }
        else{
            role.set("v.errors",null);
            var action = component.get("c.getAllUsers");
            action.setParams({"getVicRole": selectedRole,"financialYear": selectedYear});
            
            action.setCallback(this, function(response){
                console.log(response.getState()); 
                if (response.getState() == "SUCCESS") {
                    var allValues = response.getReturnValue();
                    if(allValues.length>0){
                        
                        component.set("v.userRecords",allValues);
                        component.set("v.statusShowHide",true);
                        component.find("box3").set("v.value", false);
                        component.set("v.selectedCount",0);
                        component.set("v.gridShowHide",false);
                        component.set("v.selectedCount",0);
                        component.set("v.errorMessage","");
                    }
                    else{
                        
                        component.set("v.selectedCount",0);
                        component.set("v.errorMessage","No Records Found");
                        component.set("v.statusShowHide",false);
                        component.set("v.statusShowHide",false);
                        component.set("v.gridShowHide",false);
                        
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },
    
    sendEmail : function(component, event, helper){
        var sendId = [];
        var getAllId = component.find("boxPack");
        if(! Array.isArray(getAllId)){
            if (getAllId.get("v.value") == true) {
                sendId.push(getAllId.get("v.text"));
                
            }
        }
        else{
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    sendId.push(getAllId[i].get("v.text"));
                    component.set("v.lstUserSelectedId",sendId);
                }
            }
        }
    },
    
    checkboxSelect: function(component, event, helper) {
        var selectedRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.selectedCount");
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
        component.set("v.selectedCount", getSelectedNumber);
        
        var selectedYear = component.get("v.strFinancialYear");
        var selectedRole = component.get("v.VicRole");
        var action = component.get("c.getAllUsers");
        action.setParams({"getVicRole": selectedRole,"financialYear": selectedYear});
        
        action.setCallback(this, function(response){
            console.log(response.getState()); 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                if(allValues.length==getSelectedNumber){
                    component.find("box3").set("v.value", true);
                }
                else
                    component.find("box3").set("v.value", false);
            }
        }); 
        $A.enqueueAction(action);
    },
    
    getPlans : function(component,event,helper) {
        var sendId = [];
        var getAllId = component.find("boxPack");
        if(getAllId.length > 0){
            for (var i = 0; i < getAllId.length; i++) {
                if (getAllId[i].get("v.value") == true) {
                    sendId.push(getAllId[i].get("v.text"));
                    
                }
            }
        }
        else{
            if(component.find("boxPack").get("v.value")==true){
                sendId.push(component.find("boxPack").get("v.text"));
            }
        }
        debugger;
        if(sendId == null || sendId == 'undefined' || sendId ==""){
            
            component.set("v.errorMessage", "Please select atleast one User");
            
        }
        else{
            debugger;
            component.set("v.errorMessage", "");
            var selectedRole=component.get("v.VicRole");
            var selectedUsers = component.get("v.lstUserSelectedId");
            var selectedFY = component.get("v.strFinancialYear")
            var lstWrapper = [];
            for(var i in sendId){
                var options = {
                    "userId" : sendId[i].split('-')[0],
                    "userName" : sendId[i].split('-')[1],
                    
                    "targetPercent" : "",
                    "targetCurrency" : ""
                };
                lstWrapper.push(options);
            } 
            component.set("v.lstWrapper",lstWrapper); 
            var action1 = component.get("c.getPlanName");
            action1.setParams({
                "vicRole": selectedRole,
                "lstUserDetails" : sendId,
                "financialYear" : selectedFY
            });
            action1.setCallback(this, function(response){
                
                if (response.getState() == "SUCCESS") {
                    component.set("v.errorMessage","");
                    var allValues = response.getReturnValue();
                    if(allValues != null && allValues != undefined){
                        //component.set("v.planComponent1",allValues[0].Plan_Components__r);
                        component.set("v.planComponent",allValues.lstPlan);
                        component.set("v.lstMasterCmpName",allValues.lstMasterCmpName);
                        component.set("v.lstWrapperRows",allValues.lstWrapperRows);
                        component.set("v.gridShowHide",true);
                        component.set("v.selectedCount",0);
                        component.set("v.statusShowHide", false);
                        
                    }
                    else{
                        alert('No Records Found');
                        component.set("v.selectedCount",0);
                        component.set("v.gridShowHide",false);
                    }
                }else if(response.getState() === 'ERROR'){
                    var errors = action1.getError();
                    //var errors = response.getError();
                    if (errors) {    
                        if (errors[0] && errors[0].message) {  
                            component.set("v.errorMessage",errors[0].message);
                        }    
                    }
                }
            });
            $A.enqueueAction(action1);
        }
        
        this.getCurrencyIsoCode(component, event, helper);
        
    },
    
    insertData: function(component, event, helper) {
        //debugger;
        //  if(!this.validateResult(component,event,helper)){
        //    return false;
        // }
        
        var Year=component.get("v.strFinancialYear");
        var currency=component.get("v.strCurrency");
        var selectedYear = component.get("v.lstWrapperRows");
        var planCmp = component.get("v.planComponent");
        var action = component.get("c.insertTarget");
        action.setParams({
            "planId" : planCmp[0].Id,
            "strWrapperRows": JSON.stringify(selectedYear),
            "financialYear" : Year,
            "selectCurrency" :currency
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var strResponse = response.getReturnValue();
                if(strResponse =='SUCCESS'){
                    component.set("v.errorMessage","");
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "message": "Record Saved Successfully.",
                        "type" : "success"
                    });
                    
                    toastEvent.fire();
                    $A.get('e.force:refreshView').fire();
                }
                else{
                    
                    component.set("v.errorMessage","Error !! "+strResponse);
                } 
            }
        });
        $A.enqueueAction(action);
        
    },
    
    selectAll: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var getAllId = component.find("boxPack");
        
        if(! Array.isArray(getAllId)){
            if(selectedHeaderCheck == true){ 
                component.find("boxPack").set("v.value", true);
                component.set("v.selectedCount", 1);
            }else{
                component.find("boxPack").set("v.value", false);
                component.set("v.selectedCount", 0);
            }
        }
        else{
            if (selectedHeaderCheck == true) {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", true);
                    component.set("v.selectedCount", getAllId.length);
                }
            } 
            else {
                for (var i = 0; i < getAllId.length; i++) {
                    component.find("boxPack")[i].set("v.value", false);
                    component.set("v.selectedCount", 0);
                }
            } 
        }  
        
    },
    
    validateResult : function(component,event,helper){
        var lstResult=component.get("v.lstWrapperRows");
        for(var i in lstResult){
            var lstMaster=lstResult[i].lstMasterCmpName;
            for(var k in lstMaster){
                if(lstMaster[k].strMasterType=='Currency' && (lstMaster[k].targetCurrency==null || lstMaster[k].targetPercent=='' || lstMaster[k].targetCurrency==undefined )){
                    component.set("v.errorMessage", "Please fill all the currency.");
                    return false;
                }
                else if(lstMaster[k].strMasterType!='Currency' && (lstMaster[k].targetPercent==null || lstMaster[k].targetPercent=='' || lstMaster[k].targetPercent==undefined )){
                    component.set("v.errorMessage", "Please fill all the %.");
                    return false;
                } 
                    else
                    {
                        component.set("v.errorMessage", "");
                        
                    }
            }
        }
        return true;
    },
    
    getCurrencyIsoCode : function(component,event,helper){
        debugger;
        var action = component.get("c.getEmptyTargetObject");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                debugger;
                component.set("v.DummyTarget",allValues);
               // for(var k in allValues){
                 //   if(allValues[k].IsoCode=='USD' ){
                        component.set("v.strCurrency","USD");
                
                //alert(test);
                   // }
                //}
            }
        });
        $A.enqueueAction(action);
    }
    
    
    
    
    
    
    
})