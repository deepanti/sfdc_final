({
	doInit : function(component, event, helper) {
		helper.getFinancialYear(component, event, helper);
        helper.getRoles(component, event, helper);
        
	},
    
    getUsers : function(component, event, helper) {
		helper.getUsers(component, event, helper);
       // helper.getBooleanChk(component, event, helper);
        
	},
    
    showSelected: function(component, event, helper) {
       helper.sendEmail(component, event, helper); 
    },
    
    checkboxSelect : function(component, event, helper) {
		helper.checkboxSelect(component, event, helper);
	},
    
    getPlans : function(component, event, helper) {
		helper.getPlans(component, event, helper);
        //helper.getCurrencyIsoCode(component, event, helper);
	},
    
    insertData : function(component, event, helper) {
		helper.insertData(component, event, helper);
	},
     
    selectAll : function(component, event, helper) {
		helper.selectAll(component, event, helper);
	},
    
    setLength : function(component, event, helper) {
		helper.validateLength(component, event, helper);
	},
    
    getCurrentYearVicRole : function(component,event,helper){
        helper.currentYearVICRoles(component,event,helper);
    }
})