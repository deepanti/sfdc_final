({
	 init: function (component, event, helper) 
    {   
        var OpportunityList = component.get("v.OpportunityList");       
       	var Teststr = component.get("v.Teststr");
        var recordId = component.get("v.recordId");
        var conid = component.get("v.conid");   
        var OppWrapperList1 = [];
            OppWrapperList1 = component.get("v.OppWrapperList");  
       // alert('OppWrapperList-===11=in=='+OppWrapperList1.length);
        var OppWrapperListJSON = new Array();
            OppWrapperListJSON = JSON.stringify(component.get("v.OppWrapperList"));
       //  alert('win survey ======OpportunityList======'+OpportunityList);
       // alert('win survey ======OppWrapperListJSON======'+OppWrapperListJSON.length);
      // var jsonRec =JSON.parse(JSON.stringify(OppWrapperList));
      //  alert('jsonRec-='+jsonRec[0]);
       var test=OppWrapperList1[0];     
        	var Opp_Name;
            var Opp_Source;
        	var OppSource;
            var Opp_DealType;
        	var Comp_Presence;
			var Opp_TCV;
			var Opp_NatureOfWork;
        for(var i=0;i<OppWrapperList1.length;i=i+1)
        {             
            var test1=OppWrapperList1[i];
           // alert('test======1111======'+JSON.stringify(test1));
            var jsnParse= JSON.parse(JSON.stringify(test1));
          // alert('-=-=3333333333333-=-'+jsnParse.OpportunityDataList[0].Target_Source__c);
           // alert('-=-11111=-=-'+jsnParse.OpportunityDataList[0].Opportunity_Source__c);
            Opp_Name= jsnParse.OpportunityDataList[0].Name;
            Opp_Source= jsnParse.OpportunityDataList[0].Target_Source__c;  // old 
            OppSource = jsnParse.OpportunityDataList[0].Opportunity_Source__c;  //new 
            Opp_DealType= jsnParse.OpportunityDataList[0].Annuity_Project__c;
            Comp_Presence=jsnParse.OpportunityDataList[0].Deal_Type__c;
			Opp_TCV = jsnParse.OpportunityDataList[0].TCV1__c;
			Opp_NatureOfWork = jsnParse.OpportunityDataList[0].Nature_Of_Work_OLI__c ;
           //alert('Opp_Source====111111111========'+Opp_Source);
            //alert('OppSource=====222222222222======'+OppSource);
            //alert('test.Target_Source__c======1======'+Opp_DealType);
        }
        
        component.set("v.Opp_Source",Opp_Source);
        component.set("v.OppSource",OppSource);
        component.set("v.Opp_DealType",Opp_DealType);
        component.set("v.Opp_Name",Opp_Name);
        component.set("v.Comp_Presence",Comp_Presence);
		component.set("v.Opp_TCV",Opp_TCV);
		component.set("v.Opp_NatureOfWork",Opp_NatureOfWork);
        helper.get_q3q4q5_val(component);
        helper.getClosestCompetitor(component);  
		helper.getClosestCompetitor1(component); 
        
      //  alert('================Opp_Source======'+ component.get("v.Opp_Source"));
		//alert('================Opp_TCV======'+ component.get("v.Opp_TCV"));
		//alert('===============Opp_NatureOfWork====='+component.get("v.Opp_NatureOfWork"));
		//alert('========Comp_Presence===='+ component.get("v.Comp_Presence"));
       
		if(OppSource == 'Renewal') //if(Opp_Source == 'Renewal')
        {
            component.set("v.IsSurvey1",true);
        }        
		else if(OppSource == 'IT Non-Annuity Extensions' || OppSource == 'Ramp Up' || Opp_DealType == 'Staff Aug')
        {
            component.set("v.IsSurvey2",true);
        } 
		else if(Comp_Presence == 'Sole Sourced')
        {
            //alert('======Opp_NatureOfWork==='+Opp_NatureOfWork)
			if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 && Opp_NatureOfWork  )
			{
				//if( (Opp_NatureOfWork.includes('Delivery') && Opp_TCV > 1000000 ) || ( Opp_NatureOfWork.includes('Delivery') && Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 1000000 )  )
				if(Opp_TCV >= 1000000 )
                {
					//alert('======sole sourced====in===1=======');				
					component.set("v.IsSurvey3",true);
				}
				//else if(Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 100000 )
				else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000)
                {
					 //alert('======sole sourced====in==else======1=======');			
					component.set("v.IsSurvey3",true);							
				}
				else
				{
					//alert('========sole sourced=======else===');
					component.set("v.IsSurveyElseCon",true);
					component.set("v.IsSurvey4",false);
					component.set("v.IsSurvey3",false);
					component.set("v.IsSurvey2",false);
					component.set("v.IsSurvey1",false);
				}
			}
			else
			{
				//alert('========sole sourced====final===else===');
					component.set("v.IsSurveyElseCon",true);
					component.set("v.IsSurvey4",false);
					component.set("v.IsSurvey3",false);
					component.set("v.IsSurvey2",false);
					component.set("v.IsSurvey1",false);
			}
        }
		else if(Comp_Presence == 'Competitive')
        {	
            //alert('======Competitive=========1=======');	
		
			if(Opp_NatureOfWork != 'undefined' && Opp_TCV !=0 && Opp_NatureOfWork)
			{
				//if( (Opp_NatureOfWork.includes('Delivery') && Opp_TCV > 1000000 ) || ( Opp_NatureOfWork.includes('Delivery') && Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 1000000 ) )
				if(Opp_TCV >= 1000000 )
                {
					//alert('======Competitive====if=====1======='+Opp_TCV);
					component.set("v.IsSurvey4",true);				
				}
				//else if(Opp_NatureOfWork.includes('Consulting') && Opp_TCV > 100000)
				else if(!Opp_NatureOfWork.includes('Managed Services') && !Opp_NatureOfWork.includes('Analytics') && !Opp_NatureOfWork.includes('Support') && !Opp_NatureOfWork.includes('IT Services') && Opp_TCV >= 100000 )
				{
					//alert('======Competitive======else=====1=======');				
					component.set("v.IsSurvey4",true);				
				}	
				else
				{
					//alert('========competitive=======else===');
					component.set("v.IsSurveyElseCon",true);
					component.set("v.IsSurvey4",false);
					component.set("v.IsSurvey3",false);
					component.set("v.IsSurvey2",false);
					component.set("v.IsSurvey1",false);
				}
			}
			else
			{
				//alert('========competitive====final===else===');
					component.set("v.IsSurveyElseCon",true);
					component.set("v.IsSurvey4",false);
					component.set("v.IsSurvey3",false);
					component.set("v.IsSurvey2",false);
					component.set("v.IsSurvey1",false);
			}			
        }
        else
        {
			//alert('=========final ======else======');
			component.set("v.IsSurveyElseCon",true);
			component.set("v.IsSurvey4",false);
			component.set("v.IsSurvey3",false);
			component.set("v.IsSurvey2",false);
			component.set("v.IsSurvey1",false);
        }       
		
    },
   
     onGroup: function(cmp, evt) 
	 {
		var selected = evt.getSource().get("v.label");
		var selectedval= evt.getSource().get("v.value");
        var locid = evt.getSource().getLocalId();
		cmp.set("v.IsValidate",false);
         cmp.set("v.IsAllowed",true);
		var collection= cmp.get("v.SelectedOpt");
		collection.push(selectedval); 
        cmp.set("v.SelectedOpt",collection);
		
		var idcollection=cmp.get("v.selectedcmp");
		idcollection.push(locid);
        cmp.set("v.selectedcmp",idcollection);
		
		var Map = cmp.get("v.sectionLabels");
         Map[selected] = selectedval;
         cmp.set("v.sectionLabels",Map);
        
        
        
        var map=cmp.get("v.sectionLabels");    
        var count;
        var chkthrshold=0;
        var chknull=false; 
		for(var key in map)
        {	count=0;
         	if(map[key]!='')
            {
                chkthrshold++;
            }
            for(var key2 in map)
			{	
				if(map[key2]!='' || map[key]!='')
				{
					if(map[key2]==map[key])
					{	count++;
						
					}
				}
			}
           //alert(map[key]);  
		  // alert('=====count====='+count);
            if(count>1)
				cmp.set("v.IsValidate",true);
        
			if( map[key]=='')
				chknull=true;	
        }
		
		var size_sel=cmp.get("v.sectionLabels");
		//alert('=====size_sel.length====='+size_sel.length);
		//alert('=====chkthrshold========'+chkthrshold);
        if(chkthrshold>2)
        {	
			//alert('===========1=================');
            for(var i=0;i<18;i++)
                {	var c= 	cmp.find("selectItem"+i);
                     //alert(typeof c);
                     if(typeof c ==='undefined')
                     {
                         
                     }
                     else
                     {
                           cmp.find("selectItem"+i).set("v.disabled", true);
                           
                     }
                }            
        }
		//alert('======chkthrshold======'+chkthrshold);
        //alert( '==========isallowed ========= '+cmp.get("v.IsAllowed"));
		if(chkthrshold>0)
		{
			//alert('===========2==================');
			var c = cmp.get("v.IsValidate");
            if(c == false )
				cmp.set("v.IsAllowed",false);
		}
        var idcol=cmp.get("v.selectedcmp");
        //alert('=======idcol========='+idcol);
			for(var i in idcol )
				{
					cmp.find(idcol[i]).set("v.disabled", false);
				}       
	},    
    
    handleClick:function(cmp,evt)
    {       
       // alert('Handle click============');
	   cmp.set("v.isSuccess", true);
        var LongArea = cmp.find("comments");
     	var LongAreaVal = LongArea.get("v.value");
     	//alert('================='+LongAreaVal);
     	//alert('map========'+map)
        var map=cmp.get("v.sectionLabels");
        var custs = [];
        var count;
        for(var key in map)
        {	count=0;
            for(var key2 in map)
			{	
				if(map[key2]==map[key])
                {	count++;
                	//alert(map[key]);
                }
			}
           
            //if(count>1)
            //	cmp.set("v.IsValidate",true);
        
        }   
        
		var Opp_DealOutCome = cmp.get("v.OpportunityList");
		//alert('=========1====Opp_DealOutCome======'+Opp_DealOutCome);
		var check_IsSurvey1 = cmp.get("v.IsSurvey1");		
        var check_IsSurvey2 = cmp.get("v.IsSurvey2");	
        var check_IsSurvey3 = cmp.get("v.IsSurvey3");		
        var check_IsSurvey4 = cmp.get("v.IsSurvey4");	
		var check_IsSurvey1_ElseCon = cmp.get("v.IsSurveyElseCon");
		//alert('=============1===========');
		//var in_ot = cmp.find("Incumbent_other").get("v.value");
		//alert('in_ot============'+in_ot);
		
	
		
		//alert('check_IsSurvey1======1======'+ check_IsSurvey1);
		//alert('check_IsSurvey2======1======'+ check_IsSurvey2);
		//alert('check_IsSurvey3======1======'+ check_IsSurvey3);
		//alert('check_IsSurvey4======1======'+ check_IsSurvey4);
		//alert('check_IsSurvey1_ElseCon======1======'+ check_IsSurvey1_ElseCon);
		//alert('=======q5===ClosestCompetitorOther====='+cmp.get("v.selectedClosestCompetitorOthers"));
		//alert('=======q5===ClosestCompetitorOther_Percent====='+cmp.find("ClosestCompetitorOther_Percent").get("v.value"));
		
        if(check_IsSurvey1 != false || check_IsSurvey1_ElseCon != false)
		{		
           // alert('=====if condition=====');
			//alert('check_IsSurvey1=========in===='+cmp.get("v.conid") + cmp.get("v.recordId"));
			if(LongAreaVal != 'undefined')
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);  // q2 
			}
			else
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", ' ');  // q2 
			}	
			cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 
			
			var UpdatewithWLR = cmp.get("c.saveWLR");
            UpdatewithWLR.setParams({
                             
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey1",
                "maps": cmp.get("v.sectionLabels"),                
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
            });
			//alert('UpdatewithWLR======='+UpdatewithWLR+ cmp.get("v.recordId")+cmp.get("v.conid"));
        
            // Configure the response handler for the action
            UpdatewithWLR.setCallback(this, function(response) 
             {
                var state = response.getState();
                // alert('state======1===='+state);
                if(cmp.isValid() && state === "SUCCESS") 
                {
                    //alert('====save success==');
                    cmp.set("v.isSuccess", false);	
						 var recordId = cmp.get("v.recordId");                   
        				//sforce.one.navigateToSObject(recordId,"Opportunity");          
						function isLightningExperienceOrSalesforce1() 
						 {
							return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
						} 
						if(isLightningExperienceOrSalesforce1()) 
						{                       
							sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
						}
						else
						{
                          //   alert('-------check else--------');
							// window.location = '/'+recordId;   //'/006/o'                           
                          // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
						window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                           
                        }  
                   // alert('-------check 1--------');
                   //
                    /*
                    debugger;
                    var compEvents = component.getEvent("componentEventFired");
                    compEvents.setParams({ "SelectedTabId" : '19'});
                    compEvents.fire();
                    window.scrollTo(0, 0);
                    */
                    // alert('-------check 11--------');
                }
                else if (state === "ERROR") {
                    console.log('Problem saving WLR, response state: ' + state);
                 	//alert('error ='+ state );
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                   // alert('in save');
                }
            });
			$A.enqueueAction(UpdatewithWLR);
					
		}
		else if(check_IsSurvey2 != false)
		{
            //alert('===========survey 2=======');
			if(LongAreaVal != 'undefined')
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);   // q2 
			}
			else
			{
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", ' ');  // q2 
			}
			cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome); 
			
			var UpdatewithWLR = cmp.get("c.saveWLR");
            UpdatewithWLR.setParams({
                            
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey2",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
            });
			
            // Configure the response handler for the action
            UpdatewithWLR.setCallback(this, function(response) 
             {
                var state = response.getState();
                // alert('state======1===='+state);
                if(cmp.isValid() && state === "SUCCESS") 
                {
                    //alert('====save success==');
                    cmp.set("v.isSuccess", false);	
						 var recordId = cmp.get("v.recordId");                   
        				//sforce.one.navigateToSObject(recordId,"Opportunity");  
						function isLightningExperienceOrSalesforce1() 
						 {
							return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
						} 
						if(isLightningExperienceOrSalesforce1()) 
						{                       
							sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
						}
						else
						{
							// window.location = '/'+recordId;   //'/006/o'
                           // window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                         // window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
						window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                        }  
                    // alert('-------check 2--------');
                   
                   // alert('-------check 22--------');
                }
                else if (state === "ERROR") {
                    console.log('Problem saving WLR, response state: ' + state);
                 
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                   // alert('in save');
                }
            });
			$A.enqueueAction(UpdatewithWLR);	
			//cmp.set("v.isSuccess", false);
		}
		else if(check_IsSurvey3 != false)
		{
			//alert('================in for survey 3===============')		
			//alert('====cmp.get("v.selectedIncumbent")======q3=====survy 3======'+cmp.get("v.selectedIncumbent"));		
			
			var checkIncumbentQ3 = cmp.get("v.selectedIncumbent");	//selectedIncumbent	
			var checkVal_Incumbent_other = cmp.find("Incumbent_other").get("v.value");
			
			//alert('checkIncumbentQ3========'+checkIncumbentQ3);
			//alert('=========checkVal_Incumbent_other============'+checkVal_Incumbent_other);							
				
			if( ( (LongAreaVal == 'undefined' || LongAreaVal == null ) || cmp.get("v.selectedIncumbent") == '' || cmp.get("v.selectedQ4_G_invlvmnt") == '' || cmp.get("v.selectedQ5_Interaction_w_Prospct") == '' ) || (checkIncumbentQ3 == 'Others'  &&  (checkVal_Incumbent_other == 'undefined' || checkVal_Incumbent_other == null ) ) )			
			{
				alert('Please fill all the mandatory questions 1,2,3,4,5 for survey');
				cmp.set("v.isSuccess", false);
			}
			else
			{
				//alert('survey 3 ---------else with save========');
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal);   // q2  
								
				cmp.set("v.newOppSurveyRecords.Incumbent__c", cmp.get("v.selectedIncumbent"));
				
				cmp.set("v.newOppSurveyRecords.Incumbent_Others__c", cmp.find("Incumbent_other").get("v.value"));
					
				cmp.set("v.newOppSurveyRecords.Genpact_Involvement_in_deal_with_prospec__c", cmp.get("v.selectedQ4_G_invlvmnt"));   
									
				cmp.set("v.newOppSurveyRecords.interactions_did_we_have_with_the_prospe__c", cmp.get("v.selectedQ5_Interaction_w_Prospct") );
					
				cmp.set("v.newOppSurveyRecords.If_advisor_was_involved__c", cmp.find("Q6_Advisor").get("v.value"));	
			
				cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome);  
			
				//UpdateOppRecords(cmp,evt);
				// ======For Update==================
				// alert('v.recordId=====Survey 3=='+cmp.get("v.recordId")); // opp id
				//alert('v.sectionLabels====Survey 3==='+JSON.stringify(cmp.get("v.sectionLabels")));  // ranking answer    
				//alert('v.newOppSurveyRecords====Survey 3==='+ JSON.stringify(cmp.get("v.newOppSurveyRecords")) );  // all others fields
				
				var UpdatewithWLR = cmp.get("c.saveWLR");
				UpdatewithWLR.setParams({
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey3",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
				});
			      //alert('survey 3 ---------8=======');	
				// Configure the response handler for the action
				UpdatewithWLR.setCallback(this, function(response) 
				 {
					var state = response.getState();
					// alert('state======1===='+state);
					if(cmp.isValid() && state === "SUCCESS") 
					{
						//alert('====save success==');
						cmp.set("v.isSuccess", false);	
							 var recordId = cmp.get("v.recordId");                   
							//sforce.one.navigateToSObject(recordId,"Opportunity");  
						function isLightningExperienceOrSalesforce1() 
						{
							return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
						} 
						if(isLightningExperienceOrSalesforce1()) 
						{                       
							sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
						}
						else
						{                            
							//window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                       		// window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                       		 window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                        }                    
					}
					else if (state === "ERROR") {
					//alert('====error========');
						console.log('Problem saving WLR, response state: ' + state);
					 
					}
					else {
						console.log('Unknown problem, response state: ' + state);
					   // alert('in save');
					}
				});
				$A.enqueueAction(UpdatewithWLR);
				//cmp.set("v.isSuccess", false);
			}	
		}
		else if(check_IsSurvey4 != false)
		{	
           //alert('==========in for survey 4=============================');
			//alert('=======q5===ClosestCompetitorOther====='+cmp.get("v.selectedClosestCompetitorOthers"));
			//alert('=======q5===ClosestCompetitorOther_Percent====='+cmp.find("ClosestCompetitorOther_Percent").get("v.value"));
			var CheckVal_GenpactProposedPriceDifferPercentage = cmp.find("ClosestCompetitorOther_Percent").get("v.value");
			var checkVal_Competitor_Strength1 = cmp.find("Competitor_Strength1").get("v.value"); //q6
			var checkVal_Competitor_weakness1 = cmp.find("Competitor_weakness1").get("v.value");
			var checkVal_Competitor_Strength2 = cmp.find("Competitor_Strength2").get("v.value");
			var checkVal_Competitor_weakness2 = cmp.find("Competitor_weakness2").get("v.value");
			//alert('======CheckVal_GenpactProposedPriceDifferPercentage====survey 4===='+CheckVal_GenpactProposedPriceDifferPercentage);
			
			var checkIncumbentQ4 = cmp.get("v.selectedIncumbent");	
			var checkVal_Incumbent_other = cmp.find("Incumbent_other").get("v.value");
			var checkClosestCompetitorQ5 = cmp.get("v.selectedClosestCompetitor");	
			var checkVal_ClosestCompetitors_other = cmp.find("ClosestCompetitors_other").get("v.value");
			var checkClosestCompetitorPerQ5 = cmp.get("v.selectedClosestCompetitorOthers"); 
			var CheckVal_GenpactProposedPriceDifferPercentage = cmp.find("ClosestCompetitorOther_Percent").get("v.value");
			//alert('checkClosestCompetitorPerQ5========'+checkClosestCompetitorPerQ5);
			//alert('======CheckVal_GenpactProposedPriceDifferPercentage====survey 7==='+CheckVal_GenpactProposedPriceDifferPercentage);
				
			if( ( (LongAreaVal == 'undefined' || LongAreaVal == null ) || cmp.get("v.selectedIncumbent") == '' || cmp.get("v.selectedClosestCompetitor") == '' || cmp.get("v.selectedClosestCompetitor1") == '' || cmp.get("v.selectedClosestCompetitor2") == '' || checkVal_Competitor_Strength1 == 'undefined' || checkVal_Competitor_weakness1 == 'undefined' || checkVal_Competitor_Strength2 == 'undefined' || checkVal_Competitor_weakness2 == 'undefined' || cmp.get("v.selectedQ4_G_invlvmnt") == '' || cmp.get("v.selectedQ5_Interaction_w_Prospct") == '') || (checkIncumbentQ4 == 'Others'  &&  (checkVal_Incumbent_other == 'undefined' || checkVal_Incumbent_other == null ) ) || (checkClosestCompetitorQ5 == 'Others'  &&  (checkVal_ClosestCompetitors_other == 'undefined' || checkVal_ClosestCompetitors_other == null ) ) || ( (checkClosestCompetitorPerQ5 == 'Genpact’s price was higher than closest competitor' || checkClosestCompetitorPerQ5 == 'Genpact’s price was lower than closest competitor')  &&  CheckVal_GenpactProposedPriceDifferPercentage == null )  )			
			{
				component.set("v.isSuccess", false);
				alert('Please fill all the mandatory questions 1 to 8 for survey');				
			}
			else
			{	
                //alert('============else=   === save==== survey 4======');
				cmp.set("v.newOppSurveyRecords.key_win_reasons_and_any_key_learnings__c", LongAreaVal); // q2   		
				cmp.set("v.newOppSurveyRecords.Incumbent__c", cmp.get("v.selectedIncumbent")); //q3
				cmp.set("v.newOppSurveyRecords.Incumbent_Others__c", cmp.find("Incumbent_other").get("v.value"));			
				cmp.set("v.newOppSurveyRecords.Closest_Competitor__c", cmp.get("v.selectedClosestCompetitor")); //q4
				cmp.set("v.newOppSurveyRecords.Genpact_s_proposed_price_differ__c", cmp.get("v.selectedClosestCompetitorOthers")); //q5
				cmp.set("v.newOppSurveyRecords.Genpact_s_proposed_price_differ_Percent__c", CheckVal_GenpactProposedPriceDifferPercentage); 						
				
				cmp.set("v.newOppSurveyRecords.Closest_Competitor_Others__c", cmp.find("ClosestCompetitors_other").get("v.value"));		
				
				
				cmp.set("v.newOppSurveyRecords.Closest_Competitor_1__c", cmp.get("v.selectedClosestCompetitors1")); 
				cmp.set("v.newOppSurveyRecords.Closest_Competitor_2__c", cmp.find("ClosestCompetitor2").get("v.value"));
				
				if(checkVal_Competitor_Strength1 ==='undefined' || checkVal_Competitor_Strength1 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_strengths1__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_strengths1__c", cmp.find("Competitor_Strength1").get("v.value"));
				}
				if(checkVal_Competitor_weakness1 ==='undefined' || checkVal_Competitor_weakness1 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses1__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses1__c", cmp.find("Competitor_weakness1").get("v.value"));
				}
				if(checkVal_Competitor_Strength2 ==='undefined' || checkVal_Competitor_Strength2 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_strengths2__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_strengths2__c", cmp.find("Competitor_Strength2").get("v.value"));
				}
				if(checkVal_Competitor_weakness2 ==='undefined' || checkVal_Competitor_weakness2 =='' )
				{
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses2__c", ' ');
				}
				else
				{			
					cmp.set("v.newOppSurveyRecords.Competitor_weaknesses2__c", cmp.find("Competitor_weakness2").get("v.value"));
				}					
							
				cmp.set("v.newOppSurveyRecords.Genpact_Involvement_in_deal_with_prospec__c", cmp.get("v.selectedQ4_G_invlvmnt"));  // q7       
				cmp.set("v.newOppSurveyRecords.interactions_did_we_have_with_the_prospe__c", cmp.get("v.selectedQ5_Interaction_w_Prospct") ); //q8
				cmp.set("v.newOppSurveyRecords.If_advisor_was_involved__c", cmp.find("Q6_Advisor").get("v.value")); //q9	
				cmp.set("v.newOppSurveyRecords.W_L_D__c", Opp_DealOutCome);    

				// ====== For Update ==================
				// alert('v.recordId=====Survey 4=='+cmp.get("v.recordId")); // opp id
				// alert('v.sectionLabels====Survey 4==='+JSON.stringify(cmp.get("v.sectionLabels")));  // ranking answer    
				// alert('v.newOppSurveyRecords====Survey 4==='+ JSON.stringify(cmp.get("v.newOppSurveyRecords")) );  // all others fields
				
				var UpdatewithWLR = cmp.get("c.saveWLR");
				UpdatewithWLR.setParams({
                "oppId": cmp.get("v.recordId"),
				"conid": cmp.get("v.conid"),
                "SurveyNumber": "Survey4",
                "maps": cmp.get("v.sectionLabels"),
                "OppSurvey": cmp.get("v.newOppSurveyRecords")
				});
			      
				// Configure the response handler for the action
				UpdatewithWLR.setCallback(this, function(response) 
				 {
					var state = response.getState();
					 //alert('state======1===='+state);
					if(cmp.isValid() && state === "SUCCESS") 
					{
                        cmp.set("v.isSuccess", false);	
						//alert('====save success==survey 4=====');
							 var recordId = cmp.get("v.recordId");                   
							//sforce.one.navigateToSObject(recordId,"Opportunity");  
						function isLightningExperienceOrSalesforce1() 
						{
							return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
						} 
						if(isLightningExperienceOrSalesforce1()) 
						{                       
							sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
						}
						else
						{                            
							//window.location = 'https://genpact--revampqa.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                       		//window.location = 'https://genpact--preprod.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                       		window.location = 'https://genpact.lightning.force.com/lightning/r/Opportunity/'+recordId+'/view';
                        }                    
					}
					else if (state === "ERROR") 
					{
						//alert('====error ==survey 4=====');
						console.log('Problem saving WLR, response state: ' + state);
					 
					}
					else {
						console.log('Unknown problem, response state: ' + state);
					   // alert('in save');
					}
				});
				$A.enqueueAction(UpdatewithWLR);			
				//cmp.set("v.isSuccess", false);
			}			
		}
    },
    
     handleCancel: function(component, event, helper) 
    {      
         	var recordId = component.get("v.recordId");  
    		//sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
			//sforce.one.back(true);
			function isLightningExperienceOrSalesforce1() 
			 {
				return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
			} 
			if(isLightningExperienceOrSalesforce1()) 
			{   
				//sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
				sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 
				sforce.one.back(true);
			}
			else
			{
				window.location = '/'+recordId;   //'/006/o'
			}  
			
    },
    
    reset: function(cmp,evt)
	{	 
        cmp.set("v.IsValidate",false);
        for(var i=0;i<18;i++)
                {	var c= 	cmp.find("selectItem"+i);
                 //alert(typeof c);
                 if(typeof c ==='undefined')
                 {
                     
                 }
                 else 
                    {
                        cmp.find("selectItem"+i).set("v.disabled", false);
                      	cmp.find("selectItem"+i).set("v.value", null);
                    }
                    //alert("selectItem"+i);
                }
				cmp.set("v.IsAllowed",true);
                var Map = cmp.get("v.sectionLabels");
                Map={};
                
        		cmp.set("v.sectionLabels",Map);
        		var lst= cmp.get("v.selectedcmp");
        		lst=[];
        		cmp.set("v.selectedcmp",lst);
				
				
				var checkReset_IsSurvey1 = cmp.get("v.IsSurvey1");		
				var checkReset_IsSurvey2 = cmp.get("v.IsSurvey2");	
				var checkReset_IsSurvey3 = cmp.get("v.IsSurvey3");		
				var checkReset_IsSurvey4 = cmp.get("v.IsSurvey4");
				
				if(checkReset_IsSurvey1 == true)
				{
					cmp.find("comments").set("v.value", "");	
				}
				else if(checkReset_IsSurvey2 == true)
				{
					cmp.find("comments").set("v.value", "");	
				}
				else if(checkReset_IsSurvey3 == true)
				{
					//alert('=====3=======');
					cmp.find("comments").set("v.value", "");				
					cmp.find("Incumbent_other").set("v.value", "");
					cmp.set("v.If_Incumbent_other",true);
					cmp.find("Q6_Advisor").set("v.value", "");					
					cmp.find("Incumbent").set("v.value", null);
					cmp.find("Q4_G_invlvmnt").set("v.value", null);
					cmp.find("Q5_Interaction_w_Prospct").set("v.value", null);
				}
				else if(checkReset_IsSurvey4 == true)
				{
					//alert('=====survey 4 reset call=======');
					cmp.find("comments").set("v.value", "");										
					cmp.find("Incumbent_other").set("v.value", "");					
					cmp.set("v.If_Incumbent_other",true);					
					cmp.find("Q6_Advisor").set("v.value", "");										
					cmp.find("Incumbent").set("v.value", null);					
					cmp.find("Q4_G_invlvmnt").set("v.value", null);					
					cmp.find("Q5_Interaction_w_Prospct").set("v.value", null);					
					cmp.find("ClosestCompetitor").set("v.value", null);					
					cmp.find("ClosestCompetitorOther").set("v.value", null);					
					cmp.find("ClosestCompetitorOther_Percent").set("v.value", "");					
					cmp.find("ClosestCompetitors_other").set("v.value", "");					
					cmp.set("v.If_Closest_Competitors_other",true); 					
					cmp.set("v.If_ClosestCompetitor_other",true); 									
					cmp.find("Competitor_Strength1").set("v.value", "");					
					cmp.find("Competitor_weakness1").set("v.value", "");									
					cmp.find("ClosestCompetitor1").set("v.value", null);					
						cmp.set("v.selectedClosestCompetitors1",null); 					
					cmp.find("ClosestCompetitor2").set("v.value", null);					
					cmp.find("Competitor_Strength2").set("v.value", "");					
					cmp.find("Competitor_weakness2").set("v.value", "");						
				}		
			//	alert('=======reset done======') ;
			 
    },	

    enabled_Incumbnt_other:function(cmp,evt)
    {
    	var incumbnt = cmp.get("v.selectedIncumbent");
		if(incumbnt == 'Others' || incumbnt.includes('Others') )
			cmp.set("v.If_Incumbent_other",false);
		else
			cmp.set("v.If_Incumbent_other",true);
    
	},
    enabled_ClosestCompititor_other:function(cmp,evt) //for percentage q5
    {
    	var ClosestCompetitorOthers = cmp.get("v.selectedClosestCompetitorOthers");
		//alert('ClosestCompetitorOthers=====000000======'+ClosestCompetitorOthers);
		if(ClosestCompetitorOthers == 'Genpact’s price was higher than closest competitor' || ClosestCompetitorOthers == 'Genpact’s price was lower than closest competitor')
			cmp.set("v.If_ClosestCompetitor_other",false);
		else
			cmp.set("v.If_ClosestCompetitor_other",true); 
    
	},
	enabled_Closest_CompititorQ4_other:function(cmp,evt) //for closest competitor q4
    {	
    	var ClosestCompetitorOthersQ4 = cmp.get("v.selectedClosestCompetitor");
		//alert('ClosestCompetitorOthersQ4=====1111======'+ClosestCompetitorOthersQ4);
		//alert('IsClosestCompetitorFlag======strt==================='+ cmp.get("v.IsClosestCompetitorFlag"));
		
		if(ClosestCompetitorOthersQ4 == 'Others' || ClosestCompetitorOthersQ4.includes('Others') ) 
		{
			//alert('======if==onchange======');
			cmp.set("v.If_Closest_Competitors_other",false);
		//	alert(cmp.find("ClosestCompetitors_other").get("v.value") );
			cmp.set("v.IsClosestCompetitorFlag",false);			
		}
		else
		{
			//alert('======else==onchange======');
			cmp.find("ClosestCompetitors_other").set("v.value", "");
			cmp.set("v.If_Closest_Competitors_other",true); 
			cmp.set("v.IsClosestCompetitorFlag",true);
			cmp.set("v.selectedClosestCompetitors1",ClosestCompetitorOthersQ4); 
			//alert('=====selectedClosestCompetitors1===final==on change picklist==='+ cmp.get("v.selectedClosestCompetitors1"));
		} 
		//alert('IsClosestCompetitorFlag======end==================='+ cmp.get("v.IsClosestCompetitorFlag"));
	},
	handleBlur_ClosestCompetitorsOthers : function(cmp, event)
	{
		cmp.set("v.selectedClosestCompetitors1",cmp.find("ClosestCompetitors_other").get("v.value")); 		
	}	
})