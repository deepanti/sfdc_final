({
   doInit : function(component, event, helper) {
       console.log(component.get("v.recordId"));      
      $A.createComponent(
         "c:Win_Loss_Dropped_Surveys",
         {
             "recordId" : component.get("v.recordId"),
             "conid" : component.get("v.conid")           
         },
         function(newCmp){
            if (component.isValid()) {
               component.set("v.body", newCmp);
            }
         }
      );
   },
    
    NavigateComponent : function(component,event,helper)
    {         
        //alert('=============11111111============'+event.getParam("OpportunityList"));
       // alert(JSON.stringify(event.getParam("OppWrapperList")));
        
       // console.log(JSON.stringify(event.getParam("OpportunityList")));      
     
     if(event.getParam("OpportunityList") =='Signed') 
        {
            //alert('====sign===');
            $A.createComponent(
                 "c:Win_Survey",
                 {
                   "OpportunityList" : event.getParam("OpportunityList"),    
                    "OppWrapperList" : event.getParam("OppWrapperList"), 
                     "Teststr" : "neha",
                     "conid" : event.getParam("conid"),
                     "recordId" : event.getParam("recordId")
                 },
                 function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                 }
                
              );
        }
     else if(event.getParam("OpportunityList") =='Lost') 
     {   
          $A.createComponent(
                 "c:Loss_Survey",
                 {
                   "OpportunityList" : event.getParam("OpportunityList"),
                     "OppWrapperList" : event.getParam("OppWrapperList"),  
                      "conid" : event.getParam("conid"),
                     "recordId" : event.getParam("recordId")
                 },
                 function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                 }
              );
     }
      else if(event.getParam("OpportunityList") =='Dropped by Genpact') 
     {
          $A.createComponent(
                 "c:DroppedByGenpact_Survey",
                 {
                   "OpportunityList" : event.getParam("OpportunityList"),
                     "OppWrapperList" : event.getParam("OppWrapperList"),
                     "conid" : event.getParam("conid"),
                     "recordId" : event.getParam("recordId")
                 },
                 function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                 }
              );
     }
      else if(event.getParam("OpportunityList") =='Dropped by Client') 
     {   
          $A.createComponent(
                 "c:DroppedByClient_Survey",
                 {
                   "OpportunityList" : event.getParam("OpportunityList"),
                     "OppWrapperList" : event.getParam("OppWrapperList"),  
                      "conid" : event.getParam("conid"),
                     "recordId" : event.getParam("recordId")
                 },
                 function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                 }
              );
     }   
   },
    
      NavigateSurveyDetailsComponent : function(component,event,helper)
    {   
            $A.createComponent(
                 "c:SurveyDetails",
                 {
                   "OpportunityList" : event.getParam("OpportunityList"),    
                    "OppWrapperList" : event.getParam("OppWrapperList"), 
                     "Teststr" : "neha",
                     "conid" : event.getParam("conid"),
                     "recordId" : event.getParam("recordId")
                 },
                 function(newCmp){
                    if (component.isValid()) {
                        component.set("v.body", newCmp);
                    }
                 }
                
              ); 
    }
    
})