({
    doInit : function(component, event, helper) {
		component.set("v.Isallowed", false);
        
        //Set picklist values
        var action = component.get("c.getpickval");
        var inputsel = component.find("InputSelectDynamic");
        var opts=[];
        action.setCallback(this, function(a) {
            for(var i=0;i< a.getReturnValue().length;i++){
                opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
            }
            inputsel.set("v.options", opts);

        });
        $A.enqueueAction(action); 
		
		
		// Prepare the action to load account record
        var action1 = component.get("c.getAccount");
        action1.setParams({"accountId": component.get("v.recordId")});

        // Configure response handler
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.account", response.getReturnValue());
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(action1);
		
		var allow = component.get("c.get_profile");
        allow.setParams({"accountId": component.get("v.recordId")});
        
        // Configure response handler
        allow.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                component.set("v.Isallowed", response.getReturnValue());
            } else {
                console.log('Problem getting account, response state: ' + state);
            }
        });
        $A.enqueueAction(allow);
        
        
        
    },

    handleSaveContact: function(component, event, helper) {
        //if(helper.validateContactForm(component)) {
            var picklistval= component.find("InputSelectDynamic").get("v.value");
        	component.set("v.hasErrors", false);
			component.set("v.newAccteam.Account__c", component.get("v.recordId"));
			component.set("v.newAccteam.Archetype__c", component.get("v.account.Archetype__c"));
        	component.set("v.newAccteam.User__c", component.get("v.userid"));
        	component.set("v.newAccteam.Team_Member_Role__c", picklistval);
            // Prepare the action to create the new contact
            var saveAccountTeamAction = component.get("c.saveAccountTeam");
            saveAccountTeamAction.setParams({
                "AccTeam": component.get("v.newAccteam"),
                "accountId": component.get("v.recordId")
            });

            // Configure the response handler for the action
            saveAccountTeamAction.setCallback(this, function(response) {
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS") {

                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "title": "Account Team",
                        "message": "Team member has been added."
                    });

                    // Update the UI: close panel, show toast, refresh account page
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    console.log('Problem saving contact, response state: ' + state);
                    component.set("v.hasErrors", true);
                }
                else {
                    console.log('Unknown problem, response state: ' + state);
                }
            });

            // Send the request to create the new contact
            
            $A.enqueueAction(saveAccountTeamAction);
        //}
        //else {
            // New contact form failed validation, show a message to review errors
            //component.set("v.hasErrors", true);
        //}
    },

	handleCancel: function(component, event, helper) {
	    $A.get("e.force:closeQuickAction").fire();
    }
})