({
    doInit : function(component, event, helper) {
        var action = component.get("c.getQuotaInformation");
        action.setParams({
        });
        action.setCallback(this,function(response){
            if(response.getState()==='SUCCESS')
            {
                var data = response.getReturnValue();
                console.log(data);
                if(data.Total<0)
                {
                    data.Color='red';
                    data.Total *= -1;
                }
                component.set("v.Metrics",data);
            }
            else if(response.getState()==='ERROR')
            {
                component.set("v.error",response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    }
})