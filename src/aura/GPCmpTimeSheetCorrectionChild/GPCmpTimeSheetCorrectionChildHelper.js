({
    removeTimeSheetRow: function(component, timeSheetEntryRowIndex) {
        var listOfWrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var timeSheetEntryToBeDeleted = listOfWrapListOfTimeSheetEntries[timeSheetEntryRowIndex];

        listOfWrapListOfTimeSheetEntries.splice(timeSheetEntryRowIndex, 1);

        component.set("v.wrapListOfTimeSheetEntries", listOfWrapListOfTimeSheetEntries);
        /*var deleteTimeSheetRow = component.getEvent("DeleteTimeSheetRow");

        deleteTimeSheetRow.fire();*/
    },
    projectChangeHandler: function(component, selectedProjectId, index, wrapListOfTimeSheetEntries) {
        if ($A.util.isEmpty(wrapListOfTimeSheetEntries))
            return;
        var getProjectRecord = component.get("c.getProjectRecord");
        getProjectRecord.setParams({
            "projectId": selectedProjectId
        });
        this.showSpinner(component);
        getProjectRecord.setCallback(this, function(response) {
            this.getProjectRecordHandler(response, component, index, wrapListOfTimeSheetEntries);
        });
        $A.enqueueAction(getProjectRecord);
    },
    getProjectRecordHandler: function(response, component, index, wrapListOfTimeSheetEntries) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" &&
            responseData.isSuccess && responseData.response) {
            var response = JSON.parse(responseData.response) || [];
            wrapListOfTimeSheetEntries[index].projectRecordTypeName = response.RecordType.Name;
        } else {
            this.handleFailedCallback(component, responseData);
        }
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    },
    projectTaskChangeHandler: function(component, selectedProjectTaskId, index, wrapListOfTimeSheetEntries) {
        if ($A.util.isEmpty(wrapListOfTimeSheetEntries))
            return;
        var getProjectTaskRecord = component.get("c.getProjectTaskRecord");
        getProjectTaskRecord.setParams({
            "projectTaskId": selectedProjectTaskId
        });
        this.showSpinner(component);
        getProjectTaskRecord.setCallback(this, function(response) {
            this.getProjectTaskRecordHandler(response, component, index, wrapListOfTimeSheetEntries);
        });
        $A.enqueueAction(getProjectTaskRecord);
    },
    getProjectTaskRecordHandler: function(response, component, index, wrapListOfTimeSheetEntries) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" &&
            responseData.isSuccess && responseData.response) {
            var response = JSON.parse(responseData.response) || [];
            wrapListOfTimeSheetEntries[index].projectTask = response.projectTask;
        } else {
            this.handleFailedCallback(component, responseData);
        }
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    }
})