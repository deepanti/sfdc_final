({
    clearTaskRecord: function(component, event, helper) {},
    clearProjectRecord: function(component, event, helper) {
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var index = event.getParam("externalParameter");

        wrapListOfTimeSheetEntries[index]["projectId"] = null;
        wrapListOfTimeSheetEntries[index]["projectName"] = null;

        wrapListOfTimeSheetEntries[index]["taskId"] = null;
        wrapListOfTimeSheetEntries[index]["taskName"] = null;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    },
    removeTimeSheetRowAdded: function(component, event, helper) {

        if (!confirm("Are you sure you want to delete?"))
            return;

        var timeSheetEntryRowIndex = Number(event.currentTarget.id);
        helper.removeTimeSheetRow(component, timeSheetEntryRowIndex);
    },
    changeOfValue: function(component, event, helper) {
        var rowIndex = event.target.getAttribute("data-row-index");
        var colIndex = event.target.getAttribute("data-col-index");
        var value = Number(event.currentTarget.value);
        var mapOfRowTotal = component.get("v.mapOfRowTotal");
        var mapOfColumnTotal = component.get("v.mapOfColumnTotal");
        var wrapperTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var oldValue = wrapperTimeSheetEntries[rowIndex]["lstOfTimeSheetEntries"][colIndex]["GP_Modified_Hours__c"];

        mapOfColumnTotal[colIndex] = mapOfColumnTotal[colIndex] - oldValue + value;
        mapOfRowTotal[rowIndex] = mapOfRowTotal[rowIndex] - oldValue + value;
        wrapperTimeSheetEntries[rowIndex]["lstOfTimeSheetEntries"][colIndex]["GP_Modified_Hours__c"] = value;

        component.set("v.mapOfRowTotal", mapOfRowTotal);
        component.set("v.mapOfColumnTotal", mapOfColumnTotal);
        component.set("v.wrapListOfTimeSheetEntries", wrapperTimeSheetEntries);
    },
    projectChangeHandler: function(component, event, helper) {
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var projectName = event.getParam("recordName");
        var selectedProjectId = event.getParam("sObjectId");
        var index = event.getParam("externalParameter");
        if (selectedProjectId != null) {
            helper.projectChangeHandler(component, selectedProjectId, index, wrapListOfTimeSheetEntries);
        } else {
            wrapListOfTimeSheetEntries[index].projectRecordTypeId = null;
        }
        wrapListOfTimeSheetEntries[index]["projectId"] = selectedProjectId;
        wrapListOfTimeSheetEntries[index]["projectName"] = projectName;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    },
    projectTaskChangeHandler: function(component, event, helper) {
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var projectTaskName = event.getParam("recordName");
        var selectedProjectTaskId = event.getParam("sObjectId");
        var index = event.getParam("externalParameter");
        if (selectedProjectTaskId != null) {
            helper.projectTaskChangeHandler(component, selectedProjectTaskId, index, wrapListOfTimeSheetEntries);
        } else {
            wrapListOfTimeSheetEntries[index].projectRecordTypeId = null;
        }
        wrapListOfTimeSheetEntries[index]["taskId"] = selectedProjectTaskId;
        wrapListOfTimeSheetEntries[index]["taskName"] = projectTaskName;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    }
})