({
	myAction : function(component, event, helper) {
		
	},
    toggleOpportunities : function(component, event, helper) {
		var isOppsOpen = component.get('v.isOppsOpen');
        component.set('v.isOppsOpen', !isOppsOpen);
	}
})