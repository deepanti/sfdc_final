({
    handleSave : function(component, event, helper, SaveAndNext){
        //handle Save & Save & next
        var OLIToUpdate=[];
        var oliNew = [];
        if(SaveAndNext)
        {
            var oli = component.get("v.OLIList");
            for(var i=0;i<oli.length; i++)
            {
                var oliVar = component.get("v.oliRecord");                
                //if(oli[i].TCV__c > 0)
                //{                    
                    oliVar.FTE__c = oli[i].FTE__c;
                    oliVar.Id=oli[i].Id;
                    oliNew.push(oliVar);
                //}                
            }            
        }
        
        var OLIIds=[];
        var childCmp = component.find("child");
        if($A.util.isUndefinedOrNull(childCmp.length))
        {
            var val = childCmp.saveRecords();
            if(val)
            {
                if(Array.isArray(val.Product_BD_Rep__c)){
                    val.Product_BD_Rep__c = val.Product_BD_Rep__c[0];
                }
                oliNew[0] = val;
                OLIToUpdate.push(val); 
                OLIIds.push(val.Id);
            }
            else
            {
                oliNew.splice(0,1);
            }
        }// call the aura:method in the child component
        else
        {
            for(var i=0; i<childCmp.length; i++)
            {
                var val =  childCmp[i].saveRecords();
               
                if(val)
                {
                    if(Array.isArray(val.Product_BD_Rep__c)){
                        val.Product_BD_Rep__c = val.Product_BD_Rep__c[0];
                    }
                    OLIToUpdate.push(val); 
                    OLIIds.push(val.Id);
                }
                else
                {
                    oliNew.splice(0,1);
                }
            }
        }
        //Star mark for errored products
        var OLIList=component.get("v.OLIList");
        
        var action = component.get("c.saveOLI");
        action.setParams({
            OLIList: OLIToUpdate
        });
        console.log(OLIToUpdate +'OLI to update');
        action.setCallback(this,function(response){
            
            if(response.getState()=='SUCCESS')
            {
                localStorage.setItem('LatestTabId',"4");
                if(SaveAndNext)
                {
                    localStorage.setItem('stopRecursiveUpdate','1');
                    
                    //Handle next logic
                    helper.showToast('Success','Products Saved Successfully');
                    if(localStorage.getItem('MaxTabId')<5)
                    {
                        component.set("v.MaxSelectedTabId", "5");
                        localStorage.setItem('MaxTabId',5);
                        component.find("recordViewForm").submit();
                    }
                    component.set('v.isRefresh',false);
                    component.set("v.Spinner",false);
                    var compEvents = component.getEvent("componentEventFired");
                    compEvents.setParams({ "SelectedTabId" : "5" });
                    localStorage.setItem('LatestTabId',"5");
                    compEvents.fire();    
                    $A.get('e.force:refreshView').fire();
                    window.scrollTo(0, 0);
                }
                else if(OLIToUpdate.length==component.get("v.OLIList").length){
                    localStorage.setItem('stopRecursiveUpdate','1');
                    component.set("v.Spinner",false);
                    helper.hideSpinner(component, event, helper);
                    helper.showToast('Success','Products Saved Successfully');
            		$A.get('e.force:refreshView').fire();                    
                }  
                else if(OLIToUpdate.length!=0){
                    localStorage.setItem('stopRecursiveUpdate','1');
                    component.set("v.Spinner",false);
                    helper.hideSpinner(component, event, helper);
                    helper.showToast('Warning','Some products saved sucessfully. Please fill in all product details and save.');
                    component.set('v.isRefresh',true);
            		$A.get('e.force:refreshView').fire();
                }
                else if(OLIToUpdate.length==0){
                    component.set("v.Spinner",false);
                    helper.hideSpinner(component, event, helper);
                    helper.showToast('error','Please resolve all errors to continue.');
                }                
            }
             else if (state === "INCOMPLETE") {
                helper.hideSpinner(component, event, helper);
                helper.showToast('error','Connection lost'); 
            }
            else if (state === "ERROR") {
              helper.hideSpinner(component, event, helper);
              helper.showToast('error',response.getError()[0].message); 
            }
            else
            {
                helper.hideSpinner(component, event, helper);
                helper.showToast('error','Some error has occured while saving product. Contact System admin');               
                component.set("v.IsSpinner",false);
            }
            
        });
        if(SaveAndNext)
        { 
            
            
            if(OLIToUpdate.length==component.get("v.OLIList").length){
                $A.enqueueAction(action);
                component.set("v.IsSpinner",false);
            }
            else if((oliNew.length == component.get("v.OLIList").length) && (oliNew.length != OLIToUpdate.length)){
                $A.enqueueAction(action);
                component.set("v.IsSpinner",false);
            }
            else{
                helper.hideSpinner(component, event, helper);
                helper.showToast('error','Please resolve all errors to continue.');
                component.set("v.IsSpinner",false);
            }
        }
        else{
            $A.enqueueAction(action);
            component.set("v.IsSpinner",false);
        }
        
    },
    fetchLineItems: function(component, event, helper) {  
         //fetch lineItems on doInit
        var action = component.get("c.getOLISMap");
        action.setParams({
            OppId : component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();		
            if(state === "SUCCESS"){		
                var OLIList = response.getReturnValue();
                debugger;
                //Activate first tab
                helper.fetchRates(component, event, helper, OLIList);
            }
            else if (state === "INCOMPLETE") {
                helper.hideSpinner(component, event, helper);
                helper.showToast('error','Connection lost'); 
            }
            else if (state === "ERROR") {
             helper.hideSpinner(component, event, helper);
             helper.showToast('error',response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },    
    fetchRates: function(component, event, helper, OLIList) {
        // fetch rates for conversion of currency
        var action = component.get("c.getConversionRates");
        action.setCallback(this, function(response) {
            var state = response.getState();		
            if(state === "SUCCESS"){		
                var rates = response.getReturnValue();
                component.set("v.OLIList",OLIList);
                component.set("v.ConversionRates",rates);
            }
            else if (state === "INCOMPLETE") {
                helper.hideSpinner(component, event, helper);
                helper.showToast('error','Connection lost'); 
            }
            else if (state === "ERROR") {
              helper.hideSpinner(component, event, helper);
              helper.showToast('error',response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(type, message) {
        // make toast message appear
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    }, 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
     hideChildComponents:function(component, event, helper, tabSelected){
        var OLIList = component.get("v.OLIList");
        for(var i=0;i<OLIList.length;i++)
        {
            if(OLIList[i].Id==tabSelected)
            {
                $A.util.addClass(document.getElementById(OLIList[i].Id + 'child'),'slds-show');
                $A.util.removeClass(document.getElementById(OLIList[i].Id + 'child'),'slds-hide');
            }
            else
            {
                $A.util.addClass(document.getElementById(OLIList[i].Id + 'child'),'slds-hide');
                $A.util.removeClass(document.getElementById(OLIList[i].Id + 'child'),'slds-show');
            }
            
        }
    }
    
   
    
})