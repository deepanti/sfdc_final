({
    
    doInit: function(component, event, helper) {
        //fetch Line Items on init
        var isRefresh = component.get('v.isRefresh');
        localStorage.setItem('stopRecursiveUpdate','1');
        if(!isRefresh)
        {
            helper.fetchLineItems(component, event, helper);             
        }		        
    },
    handleSave: function(component, event, helper){
        //handle save logic
		helper.showSpinner(component, event, helper);
        helper.handleSave(component, event, helper, false);
    },   
    
    saveAndNext : function(component, event, helper) {
        //handle save & next logic & updation of MSA Closure Date
        var Insight = component.get("v.OppRecord.Insight__c");
        var nonage  = component.get("v.OppRecord.Transformation_deal_ageing_for_non_ts__c");
        var Ageing = Insight + nonage;            
        var typeOfOpp = component.get("v.OppRecord.Type_Of_Opportunity__c");
        var oppSource = component.get("v.OppRecord.Opportunity_Source__c");
        if((typeOfOpp === 'Non Ts') && (oppSource !== 'Ramp Up') && (oppSource !== 'Renewal') && (Insight>0)){
            component.set("v.Ageing",Ageing); 
            component.set("v.isModalOpen",true);
        }
        else{
            helper.showSpinner(component, event, helper);
            helper.handleSave(component, event, helper, true);
        }
        
    },
    back : function(component, event, helper) {
        //back button click logic
        component.set('v.isRefresh',false);
        localStorage.setItem('LatestTabId',3);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "3"});
        compEvents.fire();
        window.scrollTo(0, 0);
    },
    handleActive : function(component, event, helper) {
       // alert('hi');
        //var tabSelected=component.get('v.OLIList[0].id');
      //  alert(tabSelected);
        //Found definition blank/commented hence commeting this - Dildar
        //helper.hideChildComponents(component, event, helper, tabSelected);
    },
    hideSpinner : function(component, event, helper) {
        helper.hideSpinner(component, event, helper);
        var tabSelected=component.get('v.OLIList[0].id');
      // alert(tabSelected);
        helper.hideChildComponents(component, event, helper, tabSelected);
    },
    handleActiveFromParent : function(component, event, helper) {
        debugger;
        helper.fetchLineItems(component, event, helper);
    },
    closeModal:function(component, event, helper) {
        component.set("v.isModalOpen",false);
    },
    updateMSAClosureDate:function(component, event, helper) {
        component.set("v.isModalOpen",false);
        localStorage.setItem('LatestTabId',1);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "1" });
        compEvents.fire();
        window.scrollTo(0, 0);
    },
    cancelAndGoToRevenueSchedule:function(component, event, helper){
        component.set("v.isModalOpen",false);
        component.set("v.IsSpinner",true);
        helper.showSpinner(component, event, helper);
        helper.handleSave(component, event, helper, true);
    },
    reloadCmp : function(component, event, helper){
        helper.fetchLineItems(component, event, helper);
    },
    recordUpdated: function(component, event, helper) {
        var eventParams = event.getParams();
        var stopRecursiveUpdate = localStorage.getItem('stopRecursiveUpdate');
        if(eventParams.changeType === "CHANGED" && stopRecursiveUpdate=='0') {
            helper.fetchLineItems(component, event, helper);
            localStorage.setItem('stopRecursiveUpdate','1');
        } else if(eventParams.changeType === "LOADED" && stopRecursiveUpdate=='0') {
            helper.fetchLineItems(component, event, helper);
            localStorage.setItem('stopRecursiveUpdate','1');
        }
    }
})