({
    doInit: function(component) {
        this.getOppProjectRecord(component);
    },
    getOppProjectRecord: function(component) {
        var getOppProjectData = component.get("c.getOppProjectData");
        
        getOppProjectData.setParams({
            "oppProjectId": component.get("v.recordId") 
        });
		
		this.showSpinner(component);
        
        getOppProjectData.setCallback(this, function(response) {
            this.getOppProjectDataHandler(component, response);
        });
        
        $A.enqueueAction(getOppProjectData);
    },
    getOppProjectDataHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJSON = JSON.parse(responseData.response) || [];
            
            component.set("v.setOfServiceLine", responseJSON.setOfServiceLine);
            component.set("v.mapOfServiceLineToNatureOfWork", responseJSON.mapOfServiceLineToNatureOfWork);
            component.set("v.mapOfNatureOfWorkToProduct", responseJSON.mapOfNatureOfWorkToProduct);
            component.set("v.oppProject", responseJSON.oppProject);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    validateOppProject: function(component) {
        var isValid = true;
        
        var oppProject = component.get("v.oppProject");
        var serviceLineDom = component.find("GP_Service_Line__c");
        var natureOfWorkDom = component.find("GP_Nature_of_Work__c");
        var productDom = component.find("GP_Product__c");
        
        if($A.util.isEmpty(oppProject.GP_Service_Line__c)) {
            isValid = false
            $A.util.removeClass(serviceLineDom, 'slds-hide');
        } else {
            $A.util.addClass(serviceLineDom, 'slds-hide');            
        }
        
        if($A.util.isEmpty(oppProject.GP_Nature_of_Work__c)) {
            isValid = false
            $A.util.removeClass(natureOfWorkDom, 'slds-hide');
        } else {
            $A.util.addClass(natureOfWorkDom, 'slds-hide');            
        }
        
        if($A.util.isEmpty(oppProject.GP_Product_Id__c )) {
            isValid = false
            $A.util.removeClass(productDom, 'slds-hide');
        } else {
            $A.util.addClass(productDom, 'slds-hide');            
        }
        
        return isValid;
    },
    saveOppProject: function(component) {
        var saveOppProject = component.get("c.saveOppProject");
        var oppProject= component.get("v.oppProject");
        
        saveOppProject.setParams({
            "serializedOppProject": JSON.stringify(oppProject) 
        });
        this.showSpinner(component);
        saveOppProject.setCallback(this, function(response) {
            this.saveOppProjectHandler(component, response);            
        });
        
        $A.enqueueAction(saveOppProject);
    },
    saveOppProjectHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('success', 'success', 'Record has been successfully saved');
            this.redirectToSobject(component.get("v.recordId"))
        } else {
            this.handleFailedCallback(component, responseData);
        }
    }
})