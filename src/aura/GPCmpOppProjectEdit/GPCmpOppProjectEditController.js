({
    doInit: function(component, event, helper) {
        helper.doInit(component);
    },
    serviceLineChangeHandler: function(component, event, helper) {
        var isServiceLineInitialized = component.get("v.isServiceLineInitialized");
        var selectedServiceLine = component.get("v.oppProject.GP_Service_Line__c");
        var mapOfServiceLineToNatureOfWork = component.get("v.mapOfServiceLineToNatureOfWork") || {};
        
        var setOfNatureOfWork = component.get("v.setOfNatureOfWork");
        
        if (!isServiceLineInitialized && setOfNatureOfWork && setOfNatureOfWork.length > 0) {
            //reset child records
            component.set("v.setOfProduct", []);
            component.set("v.oppProject.GP_Nature_of_Work__c", null);

            component.set("v.isServiceLineInitialized", true);
        }
        
        setOfNatureOfWork = mapOfServiceLineToNatureOfWork[selectedServiceLine] || [];
        component.set("v.setOfNatureOfWork", helper.sort(setOfNatureOfWork, true));
    },
    natureOfWorkChangeHandler: function(component, event, helper) {
        var isNatureOfWorkInitialized = component.get("v.isNatureOfWorkInitialized");
        debugger;
        //populate list of service line on the basis of selected family
        var selectedNatureOfWork = component.get("v.oppProject.GP_Nature_of_Work__c");
        var mapOfNatureOfWorkToProduct = component.get("v.mapOfNatureOfWorkToProduct") || {};

        var setOfProduct = component.get("v.setOfProduct");

        if (!isNatureOfWorkInitialized && setOfProduct && setOfProduct.length > 0) {
            //reset child records
            component.set("v.oppProject.GP_Product__c", null);
            component.set("v.isNatureOfWorkInitialized", true);
        }

        setOfProduct = mapOfNatureOfWorkToProduct[selectedNatureOfWork] || [];
        component.set("v.setOfProduct", helper.sort(setOfProduct, false));

    },
    saveRecord: function(component, event, helper) {
        if(helper.validateOppProject(component)) {
            helper.saveOppProject(component);
        }
    }
})