({
    ERROR_DOCUMENT_NAME_EMPTY_LABEL: 'Project Document Name cannot be empty.',
    ERROR_DOCUMENT_EMPTY_LABEL: 'Project File data cannot be empty.',
    DOUMENT_DELETED_LABEL: 'Project Document deleted successfully.',
    DOUMENT_SAVE_LABEL: 'Project Document Saved successfully.',
    ERROR_PO_NUMBER_EMPTY_LABEL: 'PO Number cannot be empty.',
    ERROR_NO_DOCUMENT_LABEL: 'No Project Dcuments Found!',
    CHUNK_SIZE : 75000,
    

    getRelatedProjectDocuments: function(component) {
        var projectId = component.get("v.recordId");
        var getProjectDocuments = component.get("c.getProjectDocuments");
        getProjectDocuments.setParams({
            "projectId": projectId
        });
        getProjectDocuments.setCallback(this, function(response) {
            this.setProjectDocumentsHandler(component, response);
        });

        $A.enqueueAction(getProjectDocuments);
    },

    validateProjectDocument: function(component, listOfProjectDocument) {
        var isPoDocument = component.get("v.isPODocument");
        var isOtherDocument = component.get("v.isOtherDocument");

        var validationResponse = {
            "isValid": true,
            "errorMessage": null
        };
        var uniqueId = "{!v.isPoDocument}" ? "poDocument" : "otherDocument";
        var projectDocument, rowDomPO, rowDomOther;

        for (var index = 0; listOfProjectDocument && index < listOfProjectDocument.length; index += 1) {
            projectDocument = listOfProjectDocument[index];
            listOfProjectDocument[index]["errorMessage"] = '';
            rowDomPO = document.getElementById('project-PO-Document-row' + index) || {};
            rowDomOther = document.getElementById('project-Other-Document-row' + index) || {};

            if (isPoDocument && $A.util.isEmpty(projectDocument.GP_PO_Number__c)) {
                rowDomPO.className = 'invalid-row';
                validationResponse["isValid"] = false;
                listOfProjectDocument[index]["errorMessage"] = this.ERROR_PO_NUMBER_EMPTY_LABEL;
            }

            if (isOtherDocument && $A.util.isEmpty(projectDocument.Name)) {
                rowDomOther.className = 'invalid-row';
                validationResponse["isValid"] = false;
                listOfProjectDocument[index]["errorMessage"] = this.ERROR_DOCUMENT_NAME_EMPTY_LABEL;
            }

            if (isPoDocument && $A.util.isEmpty(projectDocument.filedata) && $A.util.isEmpty(projectDocument.Id)) {
                rowDomPO.className = 'invalid-row';
                validationResponse["isValid"] = false;
                listOfProjectDocument[index]["errorMessage"] = this.ERROR_DOCUMENT_EMPTY_LABEL;
            }

            if (isOtherDocument && $A.util.isEmpty(projectDocument.filedata) && $A.util.isEmpty(projectDocument.Id)) {
                rowDomOther.className = 'invalid-row';
                validationResponse["isValid"] = false;
                listOfProjectDocument[index]["errorMessage"] = this.ERROR_DOCUMENT_EMPTY_LABEL;
            }

        }

        if (isPoDocument)
            component.set("v.listOfPOProjectDocument", listOfProjectDocument);
        else
            component.set("v.listOfOtherProjectDocument", listOfProjectDocument);

        return validationResponse;
    },

    setProjectDocumentsHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJson = JSON.parse(responseData.response);
            this.setAllAttributes(component, responseJson);
            //this.fireProjectSaveEvent();
        } else {

            this.handleFailedCallback(component, responseData);

        }

        this.hideSpinner(component);
    },

    setAllAttributes: function(component, responseJson) {
        if (!$A.util.isEmpty(responseJson.otherDocuments)) {
            component.set("v.listOfOtherProjectDocument", responseJson.otherDocuments);
            component.set("v.isOtherProjectDocumentExist", true);

        }
        if (!$A.util.isEmpty(responseJson.PODocuments)) {
            component.set("v.listOfPOProjectDocument", responseJson.PODocuments);
            component.set("v.isPOProjectDocumentExist", true);

        }
        if (!$A.util.isEmpty(responseJson.OtherDocumentRecordTypeId)) {
            component.set("v.OtherDocumentRecordTypeId", responseJson.OtherDocumentRecordTypeId);
        }
        if (!$A.util.isEmpty(responseJson.PORecordTypeId)) {
            component.set("v.PODocumentRecordTypeId", responseJson.PORecordTypeId);
        }
        if (!$A.util.isEmpty(responseJson.objProject)) {
            var objProject = responseJson.objProject;
            if (objProject.RecordType.Name == 'BPM') {
                component.set("v.isBPM", true);
                //component.set("v.isOtherDocument", true);
                //component.set("v.isPODocument", false);
            }
            if (responseJson.showPODocument) {
                component.set("v.isOtherDocument", false);
                component.set("v.isPODocument", true);
            } else {
                component.set("v.isOtherDocument", true);
                component.set("v.isPODocument", false);
            }

            if (objProject.GP_Project_Status__c === 'Approved') {
                component.set("v.isProjectApproved", true);
            }
        }

        component.set("v.showPODocument",responseJson.showPODocument);

    },
    removeProjectDocumentRecord: function(component, projectDocumentToBeDeleted) {
        this.deleteProjectDocumentData(component, projectDocumentToBeDeleted["Id"], projectDocumentToBeDeleted["GP_Content_Version_Id__c"]);
    },

    deleteProjectDocumentData: function(component, projectDocumentId, contentVersionId) {

        if (!projectDocumentId) {
            return;
        }

        this.showSpinner(component);
        var deleteProjectDocumentService = component.get("c.deleteProjectDocument");
        deleteProjectDocumentService.setParams({
            "projectDocumentId": projectDocumentId,
            "contentVersionId": contentVersionId
        });
        this.showSpinner(component);
        deleteProjectDocumentService.setCallback(this, function(response) {
            this.deleteProjectDocumentHandler(response, component);
        });
        $A.enqueueAction(deleteProjectDocumentService);
    },

    deleteProjectDocumentHandler: function(response, component) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {

            this.showToast('SUCCESS', 'success', this.DOUMENT_DELETED_LABEL);
            this.fireProjectSaveEvent();
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },

    saveDocument: function(component, listofProjectDocument, recordTypeName) {
        this.showSpinner(component);
        var projectId = component.get("v.recordId");
        var saveProjectDocumentService = component.get("c.saveProjectDocument");
        var wraplistofProjectDocument = [];

        for (var index in listofProjectDocument) {
            var firstfilechunk;
            if(listofProjectDocument[index]["filedata"]){
                firstfilechunk =listofProjectDocument[index]["filedata"].substring(0, this.CHUNK_SIZE);
                console.log('firstfilechunk:'+firstfilechunk);
                console.log('firstfilechunk:'+listofProjectDocument[index]["filedata"]);
            }
            var objWrap = {
                "objProjectDocument": listofProjectDocument[index],
                "filedata": listofProjectDocument[index]["filedata"],
                "filename": listofProjectDocument[index]["filename"]
            };
            wraplistofProjectDocument.push(objWrap);
           // mapOfIdToRecord[index] = listofProjectDocument[index]["filedata"];
        }
        saveProjectDocumentService.setParams({
            "serializedProjectDocument": JSON.stringify(wraplistofProjectDocument),
            "projectId": projectId,
            "recordTypeName": recordTypeName
        });

        saveProjectDocumentService.setCallback(this, function(response) {
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS") {
                this.showToast('SUCCESS', 'success', this.DOUMENT_SAVE_LABEL);
                this.uploadfiles(component,response);
                this.setProjectDocumentsHandler(component, response);
                this.fireProjectSaveEvent();
            }
        });

        $A.enqueueAction(saveProjectDocumentService);
    },
                
    uploadfiles :function(component,response){
        var responseData = response.getReturnValue() || {};
        var responseJson = JSON.parse(responseData.response);
        console.log("responseJson"+responseJson);
    },
    uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
        var getchunk = fileContents.substring(startPosition, endPosition);
        var documentFile = component.get("v.listofDocumentFile");
        var filename = component.get("v.fileName");
        var indexvar = component.get("v.indexofDoc");
        var saveProjectDocumentChunk = component.get("c.saveProjectDocumentChunk");
        saveProjectDocumentChunk.setParams({
            "fileId":attachId,
            "base64Data":getchunk
        })

        saveProjectDocumentChunk.setCallback(this,function(response){
            if (component.isValid() && response.getState() === "SUCCESS") {
                startPosition = endPosition;
                endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
               
                if (startPosition < endPosition) {
                    this.uploadInChunk(component, file, fileContents, startPosition, endPosition, attachId);
                }
            }

        })
    },

    editAll: function(component) {

        var listOfProjectDocument;

        if (component.get("v.isOtherDocument"))
            listOfProjectDocument = component.get("v.listOfOtherProjectDocument");
        if (component.get("v.isPODocument"))
            listOfProjectDocument = component.get("v.listOfPOProjectDocument");

        if (!listOfProjectDocument || listOfProjectDocument.length === 0) {
            return;
        }

        for (var i = 0; i < listOfProjectDocument.length; i += 1) {
            listOfProjectDocument[i]["isEditable"] = true;
        }

        if (component.get("v.isOtherDocument"))
            component.set("v.listOfOtherProjectDocument", listOfProjectDocument);
        if (component.get("v.isPODocument"))
            component.set("v.listOfPOProjectDocument", listOfProjectDocument);
    }
})