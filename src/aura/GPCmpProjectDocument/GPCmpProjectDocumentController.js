({
    doInit: function(component, event, helper) {
        helper.getRelatedProjectDocuments(component);
    },
    documentPageRefreshHandler: function(component, event, helper) {
        var listOfOtherProjectDocument = component.get("v.listOfOtherProjectDocument");
        var listOfPOProjectDocument = component.get("v.listOfPOProjectDocument");

        for (var i = 0; i < listOfOtherProjectDocument.length; i += 1) {
            listOfOtherProjectDocument[i]["isEditable"] = false;
        }


        for (var i = 0; i < listOfPOProjectDocument.length; i += 1) {
            listOfPOProjectDocument[i]["isEditable"] = false;
        }

        component.set("v.listOfOtherProjectDocument", listOfOtherProjectDocument);
        component.set("v.listOfPOProjectDocument", listOfPOProjectDocument);

    },
    removeOtherProjectDocument: function(component, event, helper) {

        if (!confirm("Are you sure you want to delete?"))
            return;

        var projectDocumentIndex = Number(event.currentTarget.id);
        var listOfProjectDocument = component.get("v.listOfOtherProjectDocument");
        var projectDocumentToBeDeleted = listOfProjectDocument[projectDocumentIndex];

        listOfProjectDocument.splice(projectDocumentIndex, 1);
        
        component.set("v.listOfOtherProjectDocument", listOfProjectDocument);
        
        if ($A.util.isEmpty(listOfProjectDocument)) {
            component.set("v.isOtherProjectDocumentExist", false);
        }
        
        helper.removeProjectDocumentRecord(component, projectDocumentToBeDeleted);
    },

    removePOProjectDocument: function(component, event, helper) {
        if (!confirm("Are you sure you want to delete?"))
            return;

        var projectDocumentIndex = Number(event.currentTarget.id);
        var listOfProjectDocument = component.get("v.listOfPOProjectDocument");
        var projectDocumentToBeDeleted = listOfProjectDocument[projectDocumentIndex];

        listOfProjectDocument.splice(projectDocumentIndex, 1);
        component.set("v.listOfPOProjectDocument", listOfProjectDocument);

        if ($A.util.isEmpty(listOfProjectDocument)) {
            component.set("v.isPOProjectDocumentExist", false);
        }
        helper.removeProjectDocumentRecord(component, projectDocumentToBeDeleted);
    },

    submitPO: function(component, event, helper) {
        var recordTypeName = "PODocument";
        var listofProjectDocument = component.get("v.listOfPOProjectDocument");
        var validationResponse = helper.validateProjectDocument(component, listofProjectDocument);

        if (!validationResponse.isValid)
            return;

        helper.saveDocument(component, listofProjectDocument, recordTypeName);
    },

    submitOtherDoc: function(component, event, helper) {
        var recordTypeName = "OtherDocument";
        var listofProjectDocument = component.get("v.listOfOtherProjectDocument");
        var validationResponse = helper.validateProjectDocument(component, listofProjectDocument);

        if (!validationResponse.isValid)
            return;

        helper.saveDocument(component, listofProjectDocument, recordTypeName);
    },

    addaPODocumentRow: function(component, event, helper) {
        var listofDoc = [];
        var listOfPOProjectDocument = component.get("v.listOfPOProjectDocument");

        if (!$A.util.isEmpty(listOfPOProjectDocument))
            listofDoc = listOfPOProjectDocument;

        var recordId = component.get("v.recordId");
        var PODocumentRecordTypeId = component.get("v.PODocumentRecordTypeId");
        var len = listofDoc.length;

        listofDoc.push({
            'GP_Project__c': recordId,
            'RecordTypeId': PODocumentRecordTypeId,
            "editMode": true
        });

        component.set("v.listOfPOProjectDocument", listofDoc);
    },

    addaOtherDocumentRow: function(component, event, helper) {
        var listofDoc = [];
        var listOfOtherProjectDocument = component.get("v.listOfOtherProjectDocument");

        if (!$A.util.isEmpty(listOfOtherProjectDocument))
            listofDoc = listOfOtherProjectDocument;

        var recordId = component.get("v.recordId");
        var OtherDocumentRecordTypeId = component.get("v.OtherDocumentRecordTypeId");
        var len = listofDoc.length;

        listofDoc.push({
            'GP_Project__c': recordId,
            'RecordTypeId': OtherDocumentRecordTypeId,
            "editMode": true
        });
        
        component.set("v.listOfOtherProjectDocument", listofDoc);
    },

    toggelIsEditableRow: function(component, event, helper) {
        helper.editAll(component);
    },
    showPODocument: function(component, event, helper) {
        var otherDocument = document.getElementById("otherDocument");
        var poDocument = document.getElementById("poDocument");

        $A.util.removeClass(otherDocument, 'active');
        $A.util.addClass(poDocument, 'active');

        component.set("v.isOtherDocument", false);
        component.set("v.isPODocument", true);
    },
    showOtherDocument: function(component, event, helper) {
        var otherDocument = document.getElementById("otherDocument");
        var poDocument = document.getElementById("poDocument");

        $A.util.removeClass(poDocument, 'active');
        $A.util.addClass(otherDocument, 'active');

        component.set("v.isPODocument", false);
        component.set("v.isOtherDocument", true);
    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },
    handleUploadFinished: function (cmp, event) {
        // Get the list of uploaded files
       
        var uploadedFiles = event.getParam("files");
       // alert("Files uploaded : " + uploadedFiles[0].documentId);
        var clickedRow = event.getSource().get('v.name'); 
        var listofDocumentFile ;
        if(clickedRow){
            var strRow =  clickedRow.split("-");
            if(strRow && strRow[0]=="PO"){
                listofDocumentFile= cmp.get("v.listOfPOProjectDocument");
                listofDocumentFile[strRow[1]]["filedata"] = uploadedFiles[0].documentId; //getchunk;
                listofDocumentFile[strRow[1]]["GP_File_Name__c"] = uploadedFiles[0].name;
                listofDocumentFile[strRow[1]]["GP_Content_Version_Id__c"] =uploadedFiles[0].documentId;
            }
            else if (strRow && strRow[0]=="OT" ){
              listofDocumentFile= cmp.get("v.listOfOtherProjectDocument");
                listofDocumentFile[strRow[1]]["filedata"] = uploadedFiles[0].documentId; //getchunk;
                listofDocumentFile[strRow[1]]["GP_File_Name__c"] = uploadedFiles[0].name;
                listofDocumentFile[strRow[1]]["GP_Content_Version_Id__c"] =uploadedFiles[0].documentId;
            }
         }
      
    }
    
    
   
})