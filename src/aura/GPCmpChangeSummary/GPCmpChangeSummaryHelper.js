({
    //Actual fields to be compared will be based on the Project template.
    mapOfProjectFieldsToBeCompared: {
        "name": true,
        "gp_ep_project_number__c": true,
        "gp_project_status__c": true,
        "gp_oracle_status__c": true
    },
    mapOfProjectFieldsToBeIgnored: {
        "gp_last_update_time_of_child_records__c": true
    },
    mapOfWorkLoationFieldsToBeCompared: {
        "gp_work_location__c": true,
        "gp_primary__c": true,
        "gp_bcp__c": true,
        "gp_bcp_flag__c": true
    },
    mapOfAdditionalSDOFieldsToBeCompared: {
        "gp_work_location__c": true
    },
    mapOfProjectDocumentFieldsToBeCompared: {
        "name": true,
        "gp_source__c": true,
        "gp_type__c": true,
        "gp_po_start_date__c": true,
        "gp_po_end_date__c": true,
        "gp_po_amount__c": true,
        "gp_payment_terms__c": true,
        "gp_po_necessary_on_invoice__c": true,
        "gp_po_remarks__c": true
    },
    mapOfLeadershipFieldsToBeCompared: {
        "gp_employee_id__c": true,
        "gp_leadership_role_name__c": true,
        "gp_start_date__c": true,
        "gp_pinnacle_end_date__c": true
    },
    mapOfBudgetFieldsToBeCompared: {
        "gp_band__c": true,
        "gp_country__c": true,
        "gp_effort_hours__c": true,
        "gp_tcv__c ": true
    },
    mapOfBillingMilestoneFieldsToBeCompared: {
        "name": true,
        "gp_billing_status__c": true,
        "gp_amount__c": true,
        "gp_date__c": true,
        "gp_milestone_description__c": true,
        "gp_milestone_end_date__c": true,
        "gp_entry_type__c": true,
        "gp_milestone_start_date__c": true,
        "gp_project__c": true,
        "gp_trigger__c": true,
        "gp_value__c": true,
        "gp_work_location__c": true
    },
    mapOfExpenseFieldsToBeCompared: {
        "gp_expense_category__c": true,
        "gp_expenditure_type__c": true,
        "gp_location__c": true,
        "gp_amount__c": true,
        "gp_remark__c": true,
        "gp_data_source__c": true
    },
    mapOfAddressFieldsToBeCompared: {
        "gp_relationship__c": true,
        "gp_customer_name__c": true,
        "gp_bill_to_address__c": true,
        "gp_ship_to_address__c": true,
        "gp_bill_to_address_formula__c" :true,
        "gp_ship_to_address_formula__c" : true
    },
    mapOfResourceAllocationFieldsToBeCompared: {
        "gp_allocation_type__c": true,
        "gp_ms_attention_to__c": true,
        "gp_bill_rate__c": true,
        "gp_ms_businessunit_manager__c": true,
        "gp_cm_address__c": true,
        "gp_committed_sow__c": true,
        "gp_cost_center__c": true,
        "gp_cs_value_1__c": true,
        "gp_cs_value_2__c": true,
        "gp_cs_value_3__c": true,
        "gp_cs_value_4__c": true,
        "gp_cs_value_5__c": true,
        "gp_cs_value_6__c": true,
        "gp_cs_value_7__c": true,
        "gp_email_address__c": true,
        "gp_employee__c": true,
        "gp_employee_level__c": true,
        "gp_end_date__c": true,
        "gp_interviewed_by_customer__c": true,
        "gp_sdo__c": true,
        "gp_ms_mer_no__c": true,
        "gp_morgan_stanley_manager__c": true,
        "gp_ms_address__c": true,
        "gp_ms_email_id__c": true,
        "gp_ms_id__c": true,
        "gp_ohr__c": true,
        "gp_ms_payment_mode__c": true,
        "gp_payroll_location__c": true,
        "gp_percentage_allocation__c": true,
        "gp_project_profile__c": true,
        "gp_shadow_type__c": true,
        "gp_standard_cost__c": true,
        "gp_start_date__c": true,
        "gp_total_efforts__c": true,
        "gp_work_location__c": true,
        "gp_cm_id__c": true
    },
    mapOfPretifiedProjectFeild: {
        "gp_project_organization__r.Name": "gp_project_organization_Name"
    },
    mapOfPretifiedLeadershipFeild: {
        'gp_employee_id__r.name': 'gp_employee_name'
    },
    mapOfPretifiedWorkLocationFeild: {
        'gp_work_location__r.name': 'gp_work_location_name'
    },
    mapOfPretifiedBillingMilestoneField: {
        'gp_work_location__r.name': 'gp_work_location_name'
    },
    diffpatcher: null,
    getProjectChangeSummary: function(component) {
        debugger;
        var getProjectVersions = component.get("c.getProjectVersions");
        getProjectVersions.setParams({
            "projectId": component.get("v.recordId")
        });

        getProjectVersions.setCallback(this, function(response) {
            this.getProjectVersionsHandler(response, component);
        });

        $A.enqueueAction(getProjectVersions);
    },
    getProjectVersionsHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectVersions(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProjectVersions: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response) || {};
        component.set("v.listOfProjectVersions", responseJson.listOfProjectVersions);
        component.set("v.mapOfFieldNameToFieldWrapper", responseJson.mapOfFieldNameToFieldWrapper);

        if (responseJson.project)
            this.setProjectMap(responseJson.project.GP_Project_Template__r);
        this.hideSpinner(component);

        this.setLatestProjectVersionToList(component, responseJson.listOfProjectVersions);
        this.setDiffJson(component);
    },
    setLatestProjectVersionToList: function(component, listOfProjectVersions) {
        if (listOfProjectVersions && listOfProjectVersions.length >= 2) {
            component.set("v.selectedSourceVersionId", listOfProjectVersions[0].Id);
            component.set("v.selectedDestinationVersionId", listOfProjectVersions[1].Id);
        } else if (listOfProjectVersions && listOfProjectVersions.length == 1) {
            component.set("v.selectedSourceVersionId", listOfProjectVersions[0].Id);
            component.set("v.selectedDestinationVersionId", listOfProjectVersions[0].Id);
        }

    },
    setProjectMap: function(projectTemplate) {
        var strFinalTemplate = '';

        if (projectTemplate.GP_Final_JSON_1__c)
            strFinalTemplate += projectTemplate.GP_Final_JSON_1__c;
        if (projectTemplate.GP_Final_JSON_2__c)
            strFinalTemplate += projectTemplate.GP_Final_JSON_2__c;
        if (projectTemplate.GP_Final_JSON_3__c)
            strFinalTemplate += projectTemplate.GP_Final_JSON_3__c;

        var finalJSON = JSON.parse(strFinalTemplate) || {};

        for (var key in finalJSON) {
            var listOfKey = key.split("___");
            if (listOfKey[0] === 'Project' && finalJSON[key]["isreadOnlyAfterApproval"] === false) {
                var fieldName = listOfKey[2].toLocaleLowerCase();
                this.mapOfProjectFieldsToBeCompared[fieldName] = true;
            }
        }

        for (var fieldApiName in this.mapOfProjectFieldsToBeIgnored) {
            this.mapOfProjectFieldsToBeCompared[fieldApiName] = false;
        }
    },
    setDiffJson: function(component) {
        var selectedProjectVersions = this.getSelectedProjectVersion(component);
        // create a configured instance, match objects by name

        this.setDiffPatcher();
        if (!selectedProjectVersions["selectedSourceVersion"] ||
            !selectedProjectVersions["selectedDestinationVersion"]) {
            component.set("v.projectDifference", null);
            component.set("v.message", "Please select versions");
            this.resetDiffJson(component);
            return;
        }

        var mapOfFieldNameToFieldWrapper = component.get("v.mapOfFieldNameToFieldWrapper");
        var projectDifference = this.setProjectDifferenceJSON(selectedProjectVersions, mapOfFieldNameToFieldWrapper);
        component.set("v.projectDifference", projectDifference);

        var projectAddressDifference = this.setProjectAddressDifferenceJSON(selectedProjectVersions, mapOfFieldNameToFieldWrapper);
        component.set("v.projectAddressDifference", projectAddressDifference);

        var projectWorkLocationDifference = this.setProjectWorkLocationDifferenceJSON(selectedProjectVersions, mapOfFieldNameToFieldWrapper);

        component.set("v.projectWorkLocationDifference", projectWorkLocationDifference.listOfDifferentWorkLocation);
        component.set("v.listOfDifferentprojectWorkLocationFields", projectWorkLocationDifference.listOfDifferentFields);


        var projectAdditionalSDODifference = this.setProjectAdditionalSDODifferenceJSON(selectedProjectVersions);
        component.set("v.projectAdditionalSDODifference", projectAdditionalSDODifference);

        var projectLeadershipDifference = this.setProjectLeadershipDifferenceJSON(selectedProjectVersions);
        component.set("v.projectLeadershipDifference", projectLeadershipDifference);

        var projectBudgetDifference = this.setProjectBudgetDifferenceJSON(selectedProjectVersions);
        component.set("v.projectBudgetDifference", projectBudgetDifference);

        var projectBillingMilestoneDifference = this.setProjectBillingMilestoneDifferenceJSON(selectedProjectVersions);
        component.set("v.projectBillingMilestoneDifference", projectBillingMilestoneDifference);

        var projectExpenseDifference = this.setProjectExpenseDifferenceJSON(selectedProjectVersions);
        component.set("v.projectExpenseDifference", projectExpenseDifference);

        var projectResourceAllocationDifference = this.setProjectResourceAllocationDifferenceJSON(selectedProjectVersions, mapOfFieldNameToFieldWrapper);

        component.set("v.projectResourceAllocationDifference", projectResourceAllocationDifference.listOfDifferentResourceAllocation);
        component.set("v.listOfDifferentResourceAllocationFields", projectResourceAllocationDifference.listOfDifferentResourceAllocationFields);

        component.set("v.listOfNewVersion1ResourceAllocationFields", projectResourceAllocationDifference.listOfNewVersion1ResourceAllocationFields);
        component.set("v.listOfNewVersion2ResourceAllocationFields", projectResourceAllocationDifference.listOfNewVersion2ResourceAllocationFields);

        var sanatizedNewVersion1Records, sanatizedNewVersion2Records;
        if (projectResourceAllocationDifference.listOfNewVersion1ResourceAllocation &&
            projectResourceAllocationDifference.listOfNewVersion1ResourceAllocation.length > 0) {
            sanatizedNewVersion1Records = this.getSanatizedNewRecords(projectResourceAllocationDifference.listOfNewVersion1ResourceAllocation,
                mapOfFieldNameToFieldWrapper);
            component.set("v.listOfNewVersion1ResourceAllocation", sanatizedNewVersion1Records.listOfSanatizedRecords);
            component.set("v.listOfNewVersion1ResourceAllocationFields", sanatizedNewVersion1Records.listOfNewVersion1ResourceAllocationFields);
        }

        if (projectResourceAllocationDifference.listOfNewVersion2ResourceAllocation &&
            projectResourceAllocationDifference.listOfNewVersion2ResourceAllocation.length > 0) {
            sanatizedNewVersion2Records = this.getSanatizedNewRecords(projectResourceAllocationDifference.listOfNewVersion2ResourceAllocation,
                mapOfFieldNameToFieldWrapper);
            component.set("v.listOfNewVersion2ResourceAllocation", sanatizedNewVersion2Records.listOfSanatizedRecords);
            component.set("v.listOfNewVersion2ResourceAllocationFields", sanatizedNewVersion2Records.listOfNewVersion2ResourceAllocationFields);
        }

        var projectDocumentDifference = this.setProjectDocumentDifferenceJSON(selectedProjectVersions);
        component.set("v.projectDocumentDifference", projectDocumentDifference);

        if (projectDifference ||
            (projectWorkLocationDifference && projectWorkLocationDifference.length > 0) ||
            (projectAdditionalSDODifference && projectAdditionalSDODifference.length > 0) ||
            (projectLeadershipDifference && projectLeadershipDifference.length > 0) ||
            (projectBudgetDifference && projectBudgetDifference.length > 0) ||
            (projectBillingMilestoneDifference && projectBillingMilestoneDifference.length > 0) ||
            (projectExpenseDifference && projectExpenseDifference.length > 0) ||
            (projectResourceAllocationDifference && projectResourceAllocationDifference.length > 0) ||
            (projectDocumentDifference && projectDocumentDifference.length > 0)
        ) {
            component.set("v.message", null);
        } else {
            component.set("v.message", "Selected Project versions are identical");
        }
        var projectSectionDOM = document.getElementById('project');

        $A.util.removeClass(projectSectionDOM, 'slds-hide');

        this.toggleActiveSection(component,'project_sidebar');
    },
    setProjectAddressDifferenceJSON: function(selectedProjectVersions, mapOfFieldNameToFieldWrapper) {
        var self = this;
        var sourceAddressJSON, destinationAddressJSON;

        if (selectedProjectVersions["selectedSourceVersion"]["GP_Address_JSON__c"]) {
            sourceAddressJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Address_JSON__c"])["Project Address"] || [];
        } else {
            sourceAddressJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"]["GP_Address_JSON__c"]) {
            destinationAddressJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Address_JSON__c"])["Project Address"] || [];
        } else {
            destinationAddressJSON = [];
        }

        var mapOfRelationShipToSourceAddress = {},
            mapOfRelationShipToDestinationAddress = {};

        var listOfNewSourceAddress = [],
            listOfNewDestinationAddress = [];

        //create map of source leadership to track if a new leadership is added.
        sourceAddressJSON.forEach(function(leadership) {
            mapOfRelationShipToSourceAddress[leadership.gp_relationship__c] = true;
        });

        //create map of destination leadership to track if a new leadership is added.
        destinationAddressJSON.forEach(function(leadership) {
            mapOfRelationShipToDestinationAddress[leadership.gp_relationship__c] = true;
        });

        this.spliceRecordsToNewArray(sourceAddressJSON, mapOfRelationShipToDestinationAddress, listOfNewSourceAddress, 'gp_relationship__c');
        this.spliceRecordsToNewArray(destinationAddressJSON, mapOfRelationShipToSourceAddress, listOfNewDestinationAddress, 'gp_relationship__c');


        sourceAddressJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_relationship__c > leadership2.gp_relationship__c;
        });

        destinationAddressJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_relationship__c > leadership2.gp_relationship__c;
        });

        var listOfDifferentAddress = [];
        var maxLength = Math.max(sourceAddressJSON.length, destinationAddressJSON.length);


        for (var i = 0; i < sourceAddressJSON.length; i += 1) {
            var currentLeadershipSource = sourceAddressJSON[i];
            var currentLeadershipDestination = destinationAddressJSON[i];

            var modifiedSourceLeadership = this.ignoreFields(currentLeadershipSource,
                this.mapOfAddressFieldsToBeCompared, this.mapOfPretifiedLeadershipFeild);

            var modifiedDestinationLeadership = this.ignoreFields(currentLeadershipDestination,
                this.mapOfAddressFieldsToBeCompared, this.mapOfPretifiedLeadershipFeild);

            modifiedSourceLeadership = this.sanatizeObject(modifiedSourceLeadership, true);
            modifiedDestinationLeadership = this.sanatizeObject(modifiedDestinationLeadership, true);
            var difference = jsondiffpatch.diff(modifiedSourceLeadership, modifiedDestinationLeadership);

            if (difference) {
                listOfDifferentAddress.push({
                    "source": sourceAddressJSON[i],
                    "destintion": destinationAddressJSON[i]
                });
            }
        }

        listOfNewSourceAddress.forEach(function(address) {

            address = self.ignoreFields(address,
                self.mapOfAddressFieldsToBeCompared,
                self.mapOfPretifiedLeadershipFeild);
            listOfDifferentAddress.push({
                "source": address,
                "destintion": null
            });
        });

        listOfNewDestinationAddress.forEach(function(address) {

            address = self.ignoreFields(address,
                self.mapOfAddressFieldsToBeCompared,
                self.mapOfPretifiedLeadershipFeild);

            listOfDifferentAddress.push({
                "source": null,
                "destintion": address
            });
        });

        return listOfDifferentAddress;
    },
    getSanatizedNewRecords: function(listOfNewRecords, mapOfFieldNameToFieldWrapper) {
        var listOfSanatizedRecords = [],
            listOfNewVersion1ResourceAllocationFields = [],
            listOfNewVersion2ResourceAllocationFields = [];
        var mapOfNewVersion1Fields = {};

        for (var i = 0; i < listOfNewRecords.length; i += 1) {
            var sanatizedRecord = {};
            for (var key in listOfNewRecords[i]) {
                if (listOfNewRecords[i][key]) {
                    var fieldName;
                    if (mapOfFieldNameToFieldWrapper["GP_Resource_Allocation__c"][key]) {
                        fieldName = mapOfFieldNameToFieldWrapper["GP_Resource_Allocation__c"][key]["label"];
                    } else {
                        var dirtyName = key.split(".")[0];
                        fieldName = dirtyName.replace(/GP|__r|_/gi, "") + "Name";
                    }

                    sanatizedRecord[key] = listOfNewRecords[i][key];
                    mapOfNewVersion1Fields[fieldName] = key;
                }
            }
            listOfSanatizedRecords.push(sanatizedRecord);
        }

        for (var field in mapOfNewVersion1Fields) {
            listOfNewVersion1ResourceAllocationFields.push({
                "apiName": mapOfNewVersion1Fields[field],
                "label": field
            });
        }

        return {
            "listOfSanatizedRecords": listOfSanatizedRecords,
            "listOfNewVersion1ResourceAllocationFields": listOfNewVersion1ResourceAllocationFields
        };
    },
    resetDiffJson: function(component) {
        component.set("v.projectDifference", null);
        component.set("v.projectWorkLocationDifference", null);
        component.set("v.projectAdditionalSDODifference", null);
        component.set("v.projectLeadershipDifference", null);
        component.set("v.projectBudgetDifference", null);
        component.set("v.projectBillingMilestoneDifference", null);
        component.set("v.projectExpenseDifference", null);
        component.set("v.projectResourceAllocationDifference", null);
        component.set("v.projectDocumentDifference", null);
    },
    setProjectDifferenceJSON: function(selectedProjectVersions, mapOfFieldNameToFieldWrapper) {
        if (!selectedProjectVersions["selectedSourceVersion"]["GP_Project_JSON__c"] ||
            !selectedProjectVersions["selectedDestinationVersion"]["GP_Project_JSON__c"]) {
            return null;
        }

        var sourceProjectJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Project_JSON__c"]) || {};
        var destinationProjectJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Project_JSON__c"]) || {};

        sourceProjectJSON = this.ignoreFields(sourceProjectJSON,
            this.mapOfProjectFieldsToBeCompared,
            this.mapOfPretifiedProjectFeild);

        destinationProjectJSON = this.ignoreFields(destinationProjectJSON,
            this.mapOfProjectFieldsToBeCompared,
            this.mapOfPretifiedProjectFeild);

        // sourceProjectJSON = this.sanatizeObject(sourceProjectJSON, true);
        // destinationProjectJSON = this.sanatizeObject(destinationProjectJSON, true);
        var difference = jsondiffpatch.diff(sourceProjectJSON, destinationProjectJSON);

        return this.getSanitatedDifference(difference, mapOfFieldNameToFieldWrapper);
    },
    setProjectWorkLocationDifferenceJSON: function(selectedProjectVersions, mapOfFieldNameToFieldWrapper) {
        var self = this;

        var sourceWorkLocationJSON,
            destinationWorkLocationJSON,
            mapOfDifferentFields = {},
            maxLength,
            difference,
            listOfDifferentWorkLocation = [];

        if (selectedProjectVersions["selectedSourceVersion"] &&
            selectedProjectVersions["selectedSourceVersion"]["GP_Work_Location_JSON__c"]) {
            sourceWorkLocationJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Work_Location_JSON__c"])["Work Location"] || [];
        } else {
            sourceWorkLocationJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"] &&
            selectedProjectVersions["selectedDestinationVersion"]["GP_Work_Location_JSON__c"]) {
            destinationWorkLocationJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Work_Location_JSON__c"])["Work Location"] || [];
        } else {
            destinationWorkLocationJSON = [];
        }


        var mapOfWorkLocationNameToSourceWorkLocation = {},
            mapOfWorkLocationNameToDestinationWorkLocation = {};

        var listOfNewSourceWorkLocation = [],
            listOfNewDestinationWorkLocation = [];

        //create map of source workLocation to track if a new workLocation is added.
        sourceWorkLocationJSON.forEach(function(workLocation) {
            mapOfWorkLocationNameToSourceWorkLocation[workLocation['gp_work_location__r.name']] = true;
        });

        //create map of destination workLocation to track if a new workLocation is added.
        destinationWorkLocationJSON.forEach(function(workLocation) {
            mapOfWorkLocationNameToDestinationWorkLocation[workLocation['gp_work_location__r.name']] = true;
        });

        this.spliceRecordsToNewArray(sourceWorkLocationJSON,
            mapOfWorkLocationNameToDestinationWorkLocation,
            listOfNewSourceWorkLocation,
            'gp_work_location__r.name');
        this.spliceRecordsToNewArray(destinationWorkLocationJSON,
            mapOfWorkLocationNameToSourceWorkLocation,
            listOfNewDestinationWorkLocation,
            'gp_work_location__r.name');

        sourceWorkLocationJSON.sort(function(workLocation1, workLocation2) {
            return workLocation1['gp_work_location__r.name'] > workLocation2['gp_work_location__r.name'];
        });

        destinationWorkLocationJSON.sort(function(workLocation1, workLocation2) {
            return workLocation1['gp_work_location__r.name'] > workLocation2['gp_work_location__r.name'];
        });

        for (var i = 0; i < sourceWorkLocationJSON.length; i += 1) {

            var currentSourceWorkLocation = sourceWorkLocationJSON[i];
            var currentDestinationWorkLocation = destinationWorkLocationJSON[i];

            var modifiedSourceWorkLocation = this.ignoreFields(currentSourceWorkLocation,
                this.mapOfWorkLoationFieldsToBeCompared, this.mapOfPretifiedWorkLocationFeild);
            var modifiedDestinationWorkLocation = this.ignoreFields(currentDestinationWorkLocation,
                this.mapOfWorkLoationFieldsToBeCompared, this.mapOfPretifiedWorkLocationFeild);

            // modifiedSourceWorkLocation = this.sanatizeObject(modifiedSourceWorkLocation, true);
            // modifiedDestinationWorkLocation = this.sanatizeObject(modifiedDestinationWorkLocation, true);

            difference = jsondiffpatch.diff(modifiedSourceWorkLocation, modifiedDestinationWorkLocation);

            if (difference) {
                //update map of different fields
                for (var key in difference) {
                    mapOfDifferentFields[key] = true;
                }

                listOfDifferentWorkLocation.push({
                    "source": sourceWorkLocationJSON[i],
                    "destintion": destinationWorkLocationJSON[i]
                });
            }
        }

        listOfNewSourceWorkLocation.forEach(function(workLocation) {

            workLocation = self.ignoreFields(workLocation,
                self.mapOfWorkLoationFieldsToBeCompared,
                self.mapOfPretifiedWorkLocationFeild);
            listOfDifferentWorkLocation.push({
                "source": workLocation,
                "destintion": null
            });
        });

        listOfNewDestinationWorkLocation.forEach(function(workLocation) {

            workLocation = self.ignoreFields(workLocation,
                self.mapOfWorkLoationFieldsToBeCompared,
                self.mapOfPretifiedWorkLocationFeild);

            listOfDifferentWorkLocation.push({
                "source": null,
                "destintion": workLocation
            });
        });

        //convert map of different fields to list of different fields
        var listOfDifferentFields = [];
        for (var fieldName in mapOfDifferentFields) {
            listOfDifferentFields.push({
                "apiName": fieldName,
                "label": mapOfFieldNameToFieldWrapper["GP_Project_Work_Location_SDO__c"][fieldName]
            });
        }

        return {
            "listOfDifferentWorkLocation": listOfDifferentWorkLocation,
            "listOfDifferentFields": listOfDifferentFields
        };
    },
    setProjectLeadershipDifferenceJSON: function(selectedProjectVersions) {
        var self = this;
        var sourceLeadershipJSON, destinationLeadershipJSON;

        if (selectedProjectVersions["selectedSourceVersion"]["GP_Leadership_JSON__c"]) {
            sourceLeadershipJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Leadership_JSON__c"])["Leadership"] || [];
        } else {
            sourceLeadershipJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"]["GP_Leadership_JSON__c"]) {
            destinationLeadershipJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Leadership_JSON__c"])["Leadership"] || [];
        } else {
            destinationLeadershipJSON = [];
        }

        var mapOfRoleToSourceLeadership = {},
            mapOfRoleToDestinationLeadership = {};

        var listOfNewSourceLeadership = [],
            listOfNewDestinationLeadership = [];

        //create map of source leadership to track if a new leadership is added.
        sourceLeadershipJSON.forEach(function(leadership) {
            mapOfRoleToSourceLeadership[leadership.gp_leadership_role_name__c] = true;
        });

        //create map of destination leadership to track if a new leadership is added.
        destinationLeadershipJSON.forEach(function(leadership) {
            mapOfRoleToDestinationLeadership[leadership.gp_leadership_role_name__c] = true;
        });

        this.spliceRecordsToNewArray(sourceLeadershipJSON, mapOfRoleToDestinationLeadership, listOfNewSourceLeadership, 'gp_leadership_role_name__c');
        this.spliceRecordsToNewArray(destinationLeadershipJSON, mapOfRoleToSourceLeadership, listOfNewDestinationLeadership, 'gp_leadership_role_name__c');


        sourceLeadershipJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_leadership_role_name__c > leadership2.gp_leadership_role_name__c;
        });

        destinationLeadershipJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_leadership_role_name__c > leadership2.gp_leadership_role_name__c;
        });

        var listOfDifferentLeadership = [];
        var maxLength = Math.max(sourceLeadershipJSON.length, destinationLeadershipJSON.length);


        for (var i = 0; i < sourceLeadershipJSON.length; i += 1) {
            var currentLeadershipSource = sourceLeadershipJSON[i];
            var currentLeadershipDestination = destinationLeadershipJSON[i];

            var modifiedSourceLeadership = this.ignoreFields(currentLeadershipSource,
                this.mapOfLeadershipFieldsToBeCompared, this.mapOfPretifiedLeadershipFeild);

            var modifiedDestinationLeadership = this.ignoreFields(currentLeadershipDestination,
                this.mapOfLeadershipFieldsToBeCompared, this.mapOfPretifiedLeadershipFeild);

            modifiedSourceLeadership = this.sanatizeObject(modifiedSourceLeadership, true);
            modifiedDestinationLeadership = this.sanatizeObject(modifiedDestinationLeadership, true);
            var difference = jsondiffpatch.diff(modifiedSourceLeadership, modifiedDestinationLeadership);

            if (difference) {
                listOfDifferentLeadership.push({
                    "source": sourceLeadershipJSON[i],
                    "destintion": destinationLeadershipJSON[i]
                });
            }
        }

        listOfNewSourceLeadership.forEach(function(leadership) {

            leadership = self.ignoreFields(leadership,
                self.mapOfLeadershipFieldsToBeCompared,
                self.mapOfPretifiedLeadershipFeild);
            listOfDifferentLeadership.push({
                "source": leadership,
                "destintion": null
            });
        });

        listOfNewDestinationLeadership.forEach(function(leadership) {

            leadership = self.ignoreFields(leadership,
                self.mapOfLeadershipFieldsToBeCompared,
                self.mapOfPretifiedLeadershipFeild);

            listOfDifferentLeadership.push({
                "source": null,
                "destintion": leadership
            });
        });

        return listOfDifferentLeadership;
    },
    setProjectBillingMilestoneDifferenceJSON: function(selectedProjectVersions) {
        var self = this;

        var sourceBillingMilestoneJSON,
            destinationBillingMilestoneJSON,
            maxLength,
            difference,
            listOfDifferentBillingMilestone = [];
        if (selectedProjectVersions["selectedSourceVersion"] &&
            selectedProjectVersions["selectedSourceVersion"]["GP_Billing_Milestones_JSON__c"]) {
            sourceBillingMilestoneJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Billing_Milestones_JSON__c"])["Billing Milestone"] || [];
        } else {
            sourceBillingMilestoneJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"] &&
            selectedProjectVersions["selectedDestinationVersion"]["GP_Billing_Milestones_JSON__c"]) {
            destinationBillingMilestoneJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Billing_Milestones_JSON__c"])["Billing Milestone"] || [];
        } else {
            destinationBillingMilestoneJSON = [];
        }

        var mapOfWorkLocationToSourceLeadership = {},
            mapOfWorkLocationToDestinationLeadership = {};

        var listOfNewSourceBillingMilestone = [],
            listOfNewDestinationBillingMilestone = [];

        //create map of source leadership to track if a new leadership is added.
        sourceBillingMilestoneJSON.forEach(function(leadership) {
            mapOfWorkLocationToSourceLeadership[leadership.gp_work_location__c] = true;
        });

        //create map of destination leadership to track if a new leadership is added.
        destinationBillingMilestoneJSON.forEach(function(leadership) {
            mapOfWorkLocationToDestinationLeadership[leadership.gp_work_location__c] = true;
        });

        this.spliceRecordsToNewArray(sourceBillingMilestoneJSON, mapOfWorkLocationToDestinationLeadership, listOfNewSourceBillingMilestone, 'gp_work_location__c');
        this.spliceRecordsToNewArray(destinationBillingMilestoneJSON, mapOfWorkLocationToSourceLeadership, listOfNewDestinationBillingMilestone, 'gp_work_location__c');


        sourceBillingMilestoneJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_work_location__c > leadership2.gp_work_location__c;
        });

        destinationBillingMilestoneJSON.sort(function(leadership1, leadership2) {
            return leadership1.gp_work_location__c > leadership2.gp_work_location__c;
        });

        maxLength = Math.max(sourceBillingMilestoneJSON.length, destinationBillingMilestoneJSON.length);

        for (var i = 0; i < maxLength; i += 1) {
            if (sourceBillingMilestoneJSON.length < i) {
                listOfDifferentBillingMilestone.push({
                    "source": null,
                    "destintion": destinationBillingMilestoneJSON[i]
                });
            } else if (destinationBillingMilestoneJSON.length < i) {
                listOfDifferentBillingMilestone.push({
                    "source": sourceBillingMilestoneJSON[i],
                    "destintion": null
                });
            } else {
                var currentSourceBillingMilestone = sourceBillingMilestoneJSON[i];
                var currentDestinationBillingMilestone = destinationBillingMilestoneJSON[i];

                var modifiedSourceBillingMilestone = this.ignoreFields(currentSourceBillingMilestone,
                    this.mapOfBillingMilestoneFieldsToBeCompared);
                var modifiedDestinationBillingMilestone = this.ignoreFields(currentDestinationBillingMilestone,
                    this.mapOfBillingMilestoneFieldsToBeCompared);
                modifiedSourceBillingMilestone = this.sanatizeObject(modifiedSourceBillingMilestone, true);
                modifiedDestinationBillingMilestone = this.sanatizeObject(modifiedDestinationBillingMilestone, true);

                difference = jsondiffpatch.diff(modifiedSourceBillingMilestone, modifiedDestinationBillingMilestone);
                if (difference) {
                    listOfDifferentBillingMilestone.push({
                        "source": sourceBillingMilestoneJSON[i],
                        "destintion": destinationBillingMilestoneJSON[i]
                    });
                }
            }
        }

        listOfNewSourceBillingMilestone.forEach(function(billingMilestone) {

            billingMilestone = self.ignoreFields(billingMilestone,
                self.mapOfBillingMilestoneFieldsToBeCompared,
                self.mapOfPretifiedBillingMilestoneField);
            listOfDifferentBillingMilestone.push({
                "source": billingMilestone,
                "destintion": null
            });
        });

        listOfNewDestinationBillingMilestone.forEach(function(billingMilestone) {

            billingMilestone = self.ignoreFields(billingMilestone,
                self.mapOfBillingMilestoneFieldsToBeCompared,
                self.mapOfPretifiedBillingMilestoneField);

            listOfDifferentBillingMilestone.push({
                "source": null,
                "destintion": billingMilestone
            });
        });
        return listOfDifferentBillingMilestone;
    },
    setProjectBudgetDifferenceJSON: function(selectedProjectVersions) {
        var self = this;

        var listOfDifferentBudget = [],
            destinationBudgetJSON,
            sourceBudgetJSON,
            maxLength,
            difference,
            i;

        if (selectedProjectVersions["selectedSourceVersion"] &&
            selectedProjectVersions["selectedSourceVersion"]["GP_Budget_JSON__c"]) {
            sourceBudgetJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Budget_JSON__c"])["Project Budget"] || [];
        } else {
            sourceBudgetJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"] &&
            selectedProjectVersions["selectedDestinationVersion"]["GP_Budget_JSON__c"]) {
            destinationBudgetJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Budget_JSON__c"])["Project Budget"] || [];
        } else {
            destinationBudgetJSON = [];
        }

        var mapOfUniqueToSourceBudget = {},
            mapOfUniqueToDestinationBudget = {};

        var listOfNewSourceBudget = [],
            listOfNewDestinationBudget = [];

        var uniqueKey, budget;

        //create map of source leadership to track if a new leadership is added.
        for (i = 0; i < sourceBudgetJSON.length; i += 1) {
            budget = sourceBudgetJSON[i];
            uniqueKey = budget["gp_band__c"] + '_' + budget["gp_country__c"] + '_' + budget["gp_product__c"];
            mapOfUniqueToSourceBudget[uniqueKey] = true;
            sourceBudgetJSON[i]["uniqueKey"] = uniqueKey;
        }

        for (i = 0; i < destinationBudgetJSON.length; i += 1) {
            budget = destinationBudgetJSON[i];
            uniqueKey = budget["gp_band__c"] + '_' + budget["gp_country__c"] + '_' + budget["gp_product__c"];
            mapOfUniqueToDestinationBudget[uniqueKey] = true;
            destinationBudgetJSON[i]["uniqueKey"] = uniqueKey;
        }

        this.spliceRecordsToNewArray(sourceBudgetJSON, mapOfUniqueToSourceBudget, listOfNewSourceBudget, 'uniqueKey');
        this.spliceRecordsToNewArray(destinationBudgetJSON, mapOfUniqueToDestinationBudget, listOfNewDestinationBudget, 'uniqueKey');


        sourceBudgetJSON.sort(function(budget1, budget2) {
            return budget1.uniqueKey > budget2.uniqueKey;
        });

        destinationBudgetJSON.sort(function(budget1, budget2) {
            return budget1.uniqueKey > budget2.uniqueKey;
        });

        maxLength = Math.max(sourceBudgetJSON.length, destinationBudgetJSON.length);

        for (i = 0; i < maxLength; i += 1) {
            if (sourceBudgetJSON.length < i) {
                listOfDifferentBudget.push({
                    "source": null,
                    "destintion": destinationBudgetJSON[i]
                });
            } else if (destinationBudgetJSON.length < i) {
                listOfDifferentBudget.push({
                    "source": sourceBudgetJSON[i],
                    "destintion": null
                });
            } else {
                var currentSourceBudget = sourceBudgetJSON[i];
                var currentDestinationBudget = destinationBudgetJSON[i];

                var modifiedSourceBudget = this.ignoreFields(currentSourceBudget,
                    this.mapOfBudgetFieldsToBeCompared);
                var modifiedDestinationBudget = this.ignoreFields(currentDestinationBudget,
                    this.mapOfBudgetFieldsToBeCompared);
                modifiedSourceBudget = this.sanatizeObject(modifiedSourceBudget, true);
                modifiedDestinationBudget = this.sanatizeObject(modifiedDestinationBudget, true);

                difference = jsondiffpatch.diff(modifiedSourceBudget, modifiedDestinationBudget);

                if (difference) {
                    listOfDifferentBudget.push({
                        "source": sourceBudgetJSON[i],
                        "destintion": destinationBudgetJSON[i]
                    });
                }
            }
        }

        listOfNewSourceBudget.forEach(function(newSourceBudget) {

            newSourceBudget = self.ignoreFields(newSourceBudget, self.mapOfBudgetFieldsToBeCompared, {});
            listOfDifferentBudget.push({
                "source": newSourceBudget,
                "destintion": null
            });
        });

        listOfNewDestinationBudget.forEach(function(newDestinationBudget) {

            newDestinationBudget = self.ignoreFields(newDestinationBudget, self.mapOfBudgetFieldsToBeCompared, {});

            listOfDifferentBudget.push({
                "source": null,
                "destintion": newDestinationBudget
            });
        });

        return listOfDifferentBudget;
    },
    setProjectExpenseDifferenceJSON: function(selectedProjectVersions) {

        var sourceExpenseJSON,
            destinationExpenseJSON,
            maxLength,
            difference,
            listOfDifferentExpense = [];

        if (selectedProjectVersions["selectedSourceVersion"] &&
            selectedProjectVersions["selectedSourceVersion"]["GP_Expenses_JSON__c"]) {
            sourceExpenseJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Expenses_JSON__c"])["Project Expense"] || [];
        } else {
            sourceExpenseJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"] &&
            selectedProjectVersions["selectedDestinationVersion"]["GP_Expenses_JSON__c"]) {
            destinationExpenseJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Expenses_JSON__c"])["Project Expense"] || [];
        } else {
            destinationExpenseJSON = [];
        }

        sourceExpenseJSON.sort(function(expense1, expense2) {
            return expense1['gp_unique_key__c'] > expense2['gp_unique_key__c'];
        });

        destinationExpenseJSON.sort(function(expense1, expense2) {
            return expense1['gp_unique_key__c'] > expense2['gp_unique_key__c'];
        });

        maxLength = Math.max(sourceExpenseJSON.length, destinationExpenseJSON.length);

        for (var i = 0; i < maxLength; i += 1) {
            if (sourceExpenseJSON.length < i) {
                listOfDifferentExpense.push({
                    "source": null,
                    "destintion": destinationExpenseJSON[i]
                });
            } else if (destinationExpenseJSON.length < i) {
                listOfDifferentExpense.push({
                    "source": sourceExpenseJSON[i],
                    "destintion": null
                });
            } else {

                var currentSourceExpense = sourceExpenseJSON[i];
                var currentDestinationExpense = destinationExpenseJSON[i];

                var modifiedSourceExpense = this.ignoreFields(currentSourceExpense,
                    this.mapOfExpenseFieldsToBeCompared);
                var modifiedDestinationExpense = this.ignoreFields(currentDestinationExpense,
                    this.mapOfExpenseFieldsToBeCompared);
                modifiedSourceExpense = this.sanatizeObject(modifiedSourceExpense, true);
                modifiedDestinationExpense = this.sanatizeObject(modifiedDestinationExpense, true);

                difference = jsondiffpatch.diff(modifiedSourceExpense, modifiedDestinationExpense);
                if (difference) {
                    listOfDifferentExpense.push({
                        "source": sourceExpenseJSON[i],
                        "destintion": destinationExpenseJSON[i]
                    });
                }
            }
        }
        return listOfDifferentExpense;
    },
    setProjectAdditionalSDODifferenceJSON: function(selectedProjectVersions) {
        var self = this;
        var sourceAdditionalSDOJSON,
            destinationAdditionalSDOJSON,
            maxLength,
            difference,
            listOfDifferentAdditionalSDO = [];

        if (selectedProjectVersions["selectedSourceVersion"] &&
            selectedProjectVersions["selectedSourceVersion"]["GP_Additional_SDO_JSON__c"]) {
            sourceAdditionalSDOJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Additional_SDO_JSON__c"])["Additional SDO"] || [];
        } else {
            sourceAdditionalSDOJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"] &&
            selectedProjectVersions["selectedDestinationVersion"]["GP_Additional_SDO_JSON__c"]) {
            destinationAdditionalSDOJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Additional_SDO_JSON__c"])["Additional SDO"] || [];
        } else {
            destinationAdditionalSDOJSON = [];
        }

        var mapOfWorkLocationNameToSourceWorkLocation = {},
            mapOfWorkLocationNameToDestinationWorkLocation = {};

        var listOfNewSourceWorkLocation = [],
            listOfNewDestinationWorkLocation = [];

        //create map of source workLocation to track if a new workLocation is added.
        sourceAdditionalSDOJSON.forEach(function(workLocation) {
            mapOfWorkLocationNameToSourceWorkLocation[workLocation['gp_work_location__r.name']] = true;
        });

        //create map of destination workLocation to track if a new workLocation is added.
        destinationAdditionalSDOJSON.forEach(function(workLocation) {
            mapOfWorkLocationNameToDestinationWorkLocation[workLocation['gp_work_location__r.name']] = true;
        });

        this.spliceRecordsToNewArray(sourceAdditionalSDOJSON,
            mapOfWorkLocationNameToDestinationWorkLocation,
            listOfNewSourceWorkLocation,
            'gp_work_location__r.name');
        this.spliceRecordsToNewArray(destinationAdditionalSDOJSON,
            mapOfWorkLocationNameToSourceWorkLocation,
            listOfNewDestinationWorkLocation,
            'gp_work_location__r.name');

        sourceAdditionalSDOJSON.sort(function(workLocation1, workLocation2) {
            return workLocation1['gp_work_location__r.name'] > workLocation2['gp_work_location__r.name'];
        });

        destinationAdditionalSDOJSON.sort(function(workLocation1, workLocation2) {
            return workLocation1['gp_work_location__r.name'] > workLocation2['gp_work_location__r.name'];
        });

        maxLength = Math.max(sourceAdditionalSDOJSON.length, destinationAdditionalSDOJSON.length);

        for (var i = 0; i < maxLength; i += 1) {
            var currentSourceSDO = sourceAdditionalSDOJSON[i];
            var currentDestinationSDO = destinationAdditionalSDOJSON[i];

            var modifiedSourceSDO = this.ignoreFields(currentSourceSDO,
                this.mapOfAdditionalSDOFieldsToBeCompared,
                this.mapOfPretifiedWorkLocationFeild);
            var modifiedDestinationSDO = this.ignoreFields(currentDestinationSDO,
                this.mapOfAdditionalSDOFieldsToBeCompared,
                this.mapOfPretifiedWorkLocationFeild);

            modifiedSourceSDO = this.sanatizeObject(modifiedSourceSDO, true);
            modifiedDestinationSDO = this.sanatizeObject(modifiedDestinationSDO, true);

            difference = jsondiffpatch.diff(modifiedSourceSDO, modifiedDestinationSDO);
            if (difference) {
                listOfDifferentAdditionalSDO.push({
                    "source": sourceAdditionalSDOJSON[i],
                    "destintion": destinationAdditionalSDOJSON[i]
                });
            }
        }


        listOfNewSourceWorkLocation.forEach(function(workLocation) {

            workLocation = self.ignoreFields(workLocation,
                self.mapOfWorkLoationFieldsToBeCompared,
                self.mapOfPretifiedWorkLocationFeild);
            listOfDifferentAdditionalSDO.push({
                "source": workLocation,
                "destintion": null
            });
        });

        listOfNewDestinationWorkLocation.forEach(function(workLocation) {

            workLocation = self.ignoreFields(workLocation,
                self.mapOfWorkLoationFieldsToBeCompared,
                self.mapOfPretifiedWorkLocationFeild);

            listOfDifferentAdditionalSDO.push({
                "source": null,
                "destintion": workLocation
            });
        });

        return listOfDifferentAdditionalSDO;
    },
    setProjectResourceAllocationDifferenceJSON: function(selectedProjectVersions, mapOfFieldNameToFieldWrapper) {

        var sourceResourceAllocationJSON, destinationResourceAllocationJSON;

        if (!selectedProjectVersions["selectedSourceVersion"]["GP_Resource_Allocation_JSON__c"]) {
            sourceResourceAllocationJSON = [];
        } else {
            sourceResourceAllocationJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Resource_Allocation_JSON__c"])["Resource Allocation"] || [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"]["GP_Resource_Allocation_JSON__c"]) {
            destinationResourceAllocationJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Resource_Allocation_JSON__c"])["Resource Allocation"] || [];
        } else {
            destinationResourceAllocationJSON = [];
        }

        var listOfDifferentResourceAllocation = [],
            listOfNewVersion1ResourceAllocation = [],
            listOfNewVersion2ResourceAllocation = [];
        var mapOfDifferentFields = {};
        var maxLength = Math.max(sourceResourceAllocationJSON.length, destinationResourceAllocationJSON.length);
        var mapOfNewVersion1Fields = {},
            mapOfNewVersion2Fields = {};
        var currentSourceResourceAllocation,
            modifiedSourceResourceAllocation,
            currentDestinationResourceAllocation,
            modifiedDestinationResourceAllocation;

        for (var i = 0; i < maxLength; i += 1) {
            if (sourceResourceAllocationJSON.length <= i) {
                currentDestinationResourceAllocation = destinationResourceAllocationJSON[i];
                modifiedDestinationResourceAllocation = this.ignoreFields(currentDestinationResourceAllocation,
                    this.mapOfResourceAllocationFieldsToBeCompared);
                listOfNewVersion1ResourceAllocation.push(modifiedDestinationResourceAllocation);
            } else if (destinationResourceAllocationJSON.length <= i) {
                currentSourceResourceAllocation = sourceResourceAllocationJSON[i];
                modifiedSourceResourceAllocation = this.ignoreFields(currentSourceResourceAllocation,
                    this.mapOfResourceAllocationFieldsToBeCompared);
                listOfNewVersion2ResourceAllocation.push(modifiedSourceResourceAllocation);
            } else {
                currentSourceResourceAllocation = sourceResourceAllocationJSON[i];
                currentDestinationResourceAllocation = destinationResourceAllocationJSON[i];

                modifiedSourceResourceAllocation = this.ignoreFields(currentSourceResourceAllocation,
                    this.mapOfResourceAllocationFieldsToBeCompared);
                modifiedDestinationResourceAllocation = this.ignoreFields(currentDestinationResourceAllocation,
                    this.mapOfResourceAllocationFieldsToBeCompared);
                // modifiedSourceResourceAllocation = this.sanatizeObject(modifiedSourceResourceAllocation, true);
                // modifiedDestinationResourceAllocation = this.sanatizeObject(modifiedDestinationResourceAllocation, true);

                var difference = jsondiffpatch.diff(modifiedSourceResourceAllocation, modifiedDestinationResourceAllocation);

                if (difference) {
                    for (var field in difference) {
                        mapOfDifferentFields[field] = true;
                    }
                    listOfDifferentResourceAllocation.push({
                        "source": sourceResourceAllocationJSON[i],
                        "destintion": destinationResourceAllocationJSON[i]
                    });
                }
            }
        }
        var listOfDifferentResourceAllocationFields = [];

        for (var apiName in mapOfDifferentFields) {
            if (apiName.indexOf("__r") >= 0) {
                apiName = apiName.replace("__r.name", "__c");
            }
            listOfDifferentResourceAllocationFields.push({
                "apiName": apiName,
                "label": mapOfFieldNameToFieldWrapper["GP_Resource_Allocation__c"][apiName]["label"]
            });
        }
        return {
            "listOfDifferentResourceAllocation": listOfDifferentResourceAllocation,
            "listOfDifferentResourceAllocationFields": listOfDifferentResourceAllocationFields,
            "listOfNewVersion1ResourceAllocation": listOfNewVersion1ResourceAllocation,
            "listOfNewVersion2ResourceAllocation": listOfNewVersion2ResourceAllocation
        };
    },
    updateNewVersionFields: function(modifiedDestinationResourceAllocation, mapOfNewVersion1Fields) {
        for (var key in modifiedDestinationResourceAllocation) {
            mapOfNewVersion1Fields[key] = true;
        }
    },
    setProjectDocumentDifferenceJSON: function(selectedProjectVersions) {
        var sourceProjectDocumentJSON,
            destinationProjectDocumentJSON;

        if (selectedProjectVersions["selectedSourceVersion"]["GP_Project_Document_JSON__c"]) {
            sourceProjectDocumentJSON = JSON.parse(selectedProjectVersions["selectedSourceVersion"]["GP_Project_Document_JSON__c"])["Project Document"] || [];
        } else {
            sourceProjectDocumentJSON = [];
        }

        if (selectedProjectVersions["selectedDestinationVersion"]["GP_Project_Document_JSON__c"]) {
            destinationProjectDocumentJSON = JSON.parse(selectedProjectVersions["selectedDestinationVersion"]["GP_Project_Document_JSON__c"])["Project Document"] || [];
        } else {
            destinationProjectDocumentJSON = [];
        }

        var listOfDifferentProjectDocument = [];
        var maxLength = Math.max(sourceProjectDocumentJSON.length, destinationProjectDocumentJSON.length);

        for (var i = 0; i < maxLength; i += 1) {
            if (sourceProjectDocumentJSON.length < i) {
                listOfDifferentProjectDocument.push({
                    "source": null,
                    "destintion": destinationProjectDocumentJSON[i]
                });
            } else if (destinationProjectDocumentJSON.length < i) {
                listOfDifferentProjectDocument.push({
                    "source": sourceProjectDocumentJSON[i],
                    "destintion": null
                });
            } else {
                var currentSourceProjectDocument = sourceProjectDocumentJSON[i];
                var currentDestinationProjectDocument = destinationProjectDocumentJSON[i];

                var modifiedSourceProjectDocument = this.ignoreFields(currentSourceProjectDocument,
                    this.mapOfProjectDocumentFieldsToBeCompared);
                var modifiedDestinationProjectDocument = this.ignoreFields(currentDestinationProjectDocument,
                    this.mapOfProjectDocumentFieldsToBeCompared);
                modifiedSourceProjectDocument = this.sanatizeObject(modifiedSourceProjectDocument, true);
                modifiedDestinationProjectDocument = this.sanatizeObject(modifiedDestinationProjectDocument, true);

                var difference = jsondiffpatch.diff(modifiedSourceProjectDocument, modifiedDestinationProjectDocument);

                if (difference) {
                    listOfDifferentProjectDocument.push({
                        "source": sourceProjectDocumentJSON[i],
                        "destintion": destinationProjectDocumentJSON[i]
                    });
                }
            }
        }
        return listOfDifferentProjectDocument;
    },
    spliceRecordsToNewArray: function(leadershipJSON, mapOfRoleToLeadership, listOfNewLeadership, uniqueFieldName) {
        for (var i = leadershipJSON.length - 1; i >= 0; i -= 1) {
            var leadership = leadershipJSON[i];

            if (!mapOfRoleToLeadership[leadership[uniqueFieldName]]) {
                listOfNewLeadership.push(leadershipJSON[i]);
                leadershipJSON.splice(i, 1);
            }
        }
    },
    getSanitatedDifference: function(difference, mapOfFieldNameToFieldWrapper) {
        if (difference) {
            var result = [];
            var isReference = false;
            for (var fieldName in difference) {
                result.push({
                    "fieldName": fieldName,
                    "source": difference[fieldName] ? difference[fieldName][0] : "Not Available",
                    "destination": difference[fieldName] ? difference[fieldName][1] : "Not Available",
                    "label": mapOfFieldNameToFieldWrapper["GP_Project__c"][fieldName] ? mapOfFieldNameToFieldWrapper["GP_Project__c"][fieldName]["label"] : "Not Available"
                });
            }
            return result;
        } else {
            return null;
        }
    },
    setDiffPatcher: function() {
        this.diffpatcher = jsondiffpatch.create({
            objectHash: function(obj) {
                return obj.Name;
            }
        });
    },
    getSelectedProjectVersion: function(component) {
        var selectedSourceVersionId = component.get("v.selectedSourceVersionId");
        var selectedDestinationVersionId = component.get("v.selectedDestinationVersionId");
        var listOfProjectVersions = component.get("v.listOfProjectVersions");
        var selectedSourceVersion, selectedDestinationVersion;

        for (var i = 0; i < listOfProjectVersions.length; i += 1) {
            if (listOfProjectVersions[i]["Id"] === selectedSourceVersionId) {
                selectedSourceVersion = listOfProjectVersions[i];
            }

            if (listOfProjectVersions[i]["Id"] === selectedDestinationVersionId) {
                selectedDestinationVersion = listOfProjectVersions[i];
            }

            if (selectedSourceVersion && selectedDestinationVersion) {
                break;
            }
        }

        return {
            "selectedSourceVersion": selectedSourceVersion,
            "selectedDestinationVersion": selectedDestinationVersion
        };
    },
    ignoreFields: function(obj, mapOfFieldsToBeCompared, prettifiedMap) {
        var modifiedObject = {};

        for (var key in obj) {
            if (key.indexOf("__r") >= 0) {
                var newKey = key.replace("__r.name", "__c");
                obj[newKey] = obj[key];
            }
        }


        for (var key in obj) {
            if (mapOfFieldsToBeCompared[key]) {
                modifiedObject[key] = obj[key];
            }
        }
        return modifiedObject;
    },
    sanatizeObject: function(obj, ignoreId) {
        for (var key in obj) {
            if (ignoreId && key === "Id") {
                delete obj[key];
            } else if (this.isArray(obj[key])) {

                obj[key] = this.getSortedArray(obj[key], ignoreId);
                for (var i = 0; i < obj[key].length; i += 1) {
                    obj[key][i] = this.sanatizeObject(obj[key][i], ignoreId);
                }

            } else if (this.isObject(obj[key])) {
                for (var key2 in obj[key]) {
                    obj[key][key2] = this.sanatizeObject(obj[key][key2], ignoreId);
                }
            }
        }
        return obj;
    },
    isObject: function(obj) {
        return (!!obj) && (obj.constructor === Object);
    },
    isArray: function(obj) {
        return (!!obj) && (obj.constructor === Array);
    },
    getSortedArray: function(arrayObj, ignoreId) {
        /*arrayObj.sort(function(a,b) {
            return (a.Name > b.Name) ? 1 : ((b.Name > a.Name) ? -1 : 0);
        });*/
        if (!arrayObj || arrayObj.length === 0) {
            return [];
        }

        var props = [];
        var idPattern = new RegExp("Id");

        for (var key in arrayObj[0]) {
            if (!idPattern.test(key)) {
                props.push(key);
            } else if (!ignoreId) {
                props.push(key);
            }
        }

        arrayObj.sort(this.dynamicSortMultiple(props));
        return arrayObj;
    },
    dynamicSortMultiple: function(props) {
        //var props = arguments;
        return function(obj1, obj2) {
            var i = 0,
                result = 0,
                numberOfProperties = props.length;
            while (result === 0 && i < numberOfProperties) {
                result = obj1[props[i]] < obj2[props[i]] ? -1 : obj1[props[i]] > obj2[props[i]] ? 1 : 0;
                i++;
            }
            return result;
        }
    },
    resetOldFilters: function(component) {
		debugger;
        component.set("v.selectedSourceVersionId", null);
        component.set("v.selectedDestinationVersionId", null);

        component.set("v.projectDifference", null);
        component.set("v.projectWorkLocationDifference", null);
        component.set("v.projectAdditionalSDODifference", null);
        component.set("v.projectLeadershipDifference", null);
        component.set("v.projectBillingMilestoneDifference", null);
        component.set("v.projectBudgetDifference", null);
        component.set("v.projectExpenseDifference", null);
        component.set("v.projectResourceAllocationDifference", null);
        component.set("v.listOfNewVersion1ResourceAllocation", null);
        component.set("v.listOfNewVersion2ResourceAllocation", null);
        component.set("v.projectDocumentDifference", null);
    },
    toggleActiveSection : function(component ,activeSideBarName){
        var listOfSectionSidebars = [
            "project_sidebar",
            "projectWorklocation_sidebar",
            "projectAddress_sidebar",
            "projectLeadership_sidebar",
            "projectAdditionalSDOWorklocation_sidebar",
            "projectBillingMilestone_sidebar",
            "projectBudget_sidebar",
            "projectExpense_sidebar",
            "projectResourceAllocation_sidebar",
            "projectDocument_sidebar"
        ];

        listOfSectionSidebars.forEach(function(sideBarName) {
            var sectionName = sideBarName.split("_")[0];

            var sideBarDom = document.getElementById(sideBarName); //component.find(sideBarName);
            var sectionDom = component.find(sectionName); //document.getElementById(sectionName); //

            if (sideBarName === activeSideBarName) {
                $A.util.addClass(sideBarDom, 'active');
                $A.util.removeClass(sectionDom, 'slds-hide');
            } else {
                $A.util.removeClass(sideBarDom, 'active');
                $A.util.addClass(sectionDom, 'slds-hide');
            }
        });
    }
})