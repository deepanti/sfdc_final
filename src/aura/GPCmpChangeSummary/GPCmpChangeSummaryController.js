({
    doInit: function(component, event, helper) {
        helper.getProjectChangeSummary(component);
        helper.resetOldFilters(component);
    },
    sourceVersionSelectHandler: function(component, event, helper) {
        helper.setDiffJson(component);
    },
    destinationVersionSelectHandler: function(component, event, helper) {

        helper.setDiffJson(component);
    },
    toggelSection: function(component, event, helper) {
        var activeSideBarName = event.currentTarget.id;
        helper.toggleActiveSection(component,activeSideBarName);
    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    }
})