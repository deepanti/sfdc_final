({
	fetchField : function(component,event,helper){
		var BoadExLink;
        var action=component.get("c.getAccountField");
        var accountId=component.get("v.recordId");
        action.setParams({
            lstAccIds:accountId
        });
        action.setCallback(this, function (response){
            var state=response.getState();
            if(state=="SUCCESS")
            {
                var accList=response.getReturnValue();
                component.set("v.AccountList",accList);
                BoadExLink = accList[0].BoardEx_Link__c;
                if(BoadExLink != 'BX ID is NULL'){
                var div = document.createElement('div');
                div.innerHTML = BoadExLink;
                var hrefLink = div.firstChild.getAttribute('href');
                component.set("v.hrefLink", hrefLink);
                component.set("v.accLinkflag", true);
                    component.set("v.accnullLinkflag", false);
                }else{
                    component.set("v.accLinkflag", false);
                    component.set("v.accnullLinkflag", true);
                }
            }
            else
            {
                alert('Error in getting data');
            }
        });
        $A.enqueueAction(action);
	}
})