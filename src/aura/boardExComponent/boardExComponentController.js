({
    getAccField : function(component, event, helper) {
        helper.fetchField(component,event,helper);
    },
    
    clickCount : function(component,event,helper){
        var accountId=component.get("v.recordId");
        var Link=component.get("v.hrefLink");
        var action=component.get("c.saveClickCount");
        action.setParams({
            lstAccIds :accountId,
                      
          });
        action.setCallback(this, function (response){
            var state=response.getState();
            if(state=="SUCCESS") {
               window.open(Link,'_blank');
            } else{
                alert('Error in getting data');
            }
        });
        $A.enqueueAction(action);
      }
})