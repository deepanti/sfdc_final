({
    /**
     * Handle Component Initialization event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    doInit: function(component, event, helper) {
        helper.displayApplication(component);
        var displayApplication = component.get("v.displayApplication");
         if(displayApplication){
            helper.doInitHelper(component);
         }else{
              helper.hideSpinner(component);
         }
		//Avinash - Avalara changes   
        // var OUID=component.get("v.projectRecord.GP_Operating_Unit__c");
        // helper.CheckIfAvalaraEnabled(component,OUID);
        if($A.util.isUndefinedOrNull(component.get("v.projectRecord.GP_Country__c")))
        {
            // get the fields API name and pass it to helper function  
            var controllingFieldAPI = component.get("v.controllingFieldAPI");
            var dependingFieldAPI = component.get("v.dependingFieldAPI");
            var objDetails = component.get("v.objDetail");
            // call the helper function
            helper.fetchPicklistValues(component,objDetails,controllingFieldAPI, dependingFieldAPI);
        }
        
        //Avalara Changes End Here
        //
    },
    /**
     * Handle Sid bar click event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
	 //Avalara - changes
    handleServiceDeliveryFlag:function(component, event, helper)
    {
        
        var serDelFlag=event.getParam("ServiceDeliveryFlag"); 
        var isAvalaraLeEnable=component.get("v.isAvalaraEnable");
        console.log("===serDelFlag==="+serDelFlag);
        console.log("===isAvalaraLeEnable==="+isAvalaraLeEnable);
        component.set("v.CopyBillToAddrFlag",false);
        component.set("v.projectRecord.GP_US_Address__c","");   
        component.set("v.projectRecord.GP_City__c","");
        component.set("v.projectRecord.GP_City__r.Name","");
        component.set("v.projectRecord.GP_Pincode__c",""); 
        component.set("v.projectRecord.GP_State__c",""); 
        component.set("v.projectRecord.GP_Country__c",""); 
        component.set("v.readonlyAddress",true); //isEditable
        //component.set("v.isEditable",false); //isEditable
        if(serDelFlag && isAvalaraLeEnable){        
            component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
            //helper.SetServiceDeliveryInCanadaFlag(component,true);
        }
        else
        {
            component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",false);
             component.set("v.projectRecord.GP_Sub_Category_Of_Services__c",null);
             component.set("v.projectRecord.GP_Tax_Exemption_Certificate_Available__c",false);
             component.set("v.projectRecord.GP_Valid_Till__c",null);
            //helper.SetServiceDeliveryInCanadaFlag(component,false);
        }
        
    },
    toggelSection: function(component, event, helper) {
		       //Avinash - Avalara
        //helper.CheckIfAvalaraEnabled(component);
        var activeSection = event.currentTarget.id;
        var activeSectionName = activeSection.substring(activeSection.indexOf("_") + 1, activeSection.indexOf("_sidebar"));
        
        if(activeSectionName==="OtherClassification")
        {
            var usBillToAddressFlag = component.get("v.isUsCanadaAddr");
            var serDeliveryUSCanada=component.get("v.projectRecord.GP_Service_Deliver_in_US_Canada__c");
            if(usBillToAddressFlag && serDeliveryUSCanada){
                component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
                component.set("v.projectRecord.GP_TAX_Code__c","");
            }
        }
        helper.toggelSection(component, event);
		if(activeSectionName==="OtherClassification")
        {
            var stateVal=component.get("v.projectRecord.GP_State__c");
            if(!$A.util.isUndefinedOrNull(stateVal) && !$A.util.isEmpty(stateVal))
            {
                $A.enqueueAction(component.get("c.onControllerFieldChange"));
            }
        }
    },
    /**
     * Handle Toggel of project type selection i.e. Support or indirect selection while creating new project.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    toggelProjectTypeSelection: function(component, event, helper) {
        var eventSource = event.getSource().get("v.name");

        if (eventSource === "legacyProject") {
            component.set("v.createSupportProject", false);
            component.set("v.createInterCOEProject", false);
        } else if(eventSource === "supportProject"){
            component.set("v.createWithoutSFDCProject", false);
            component.set("v.createInterCOEProject", false);
        }
        else {
            component.set("v.createWithoutSFDCProject", false);
            component.set("v.createSupportProject", false);
        }
        component.set("v.projectRecord.GP_Primary_SDO__c", null);
        // When the selected project type is Without SFDC then only SDO with active legacy expiration date will be fetched
        //Other wise all active sdo Under user role will be fetched.
        helper.getFilteredSOD(component);
    },
    /**
     * Handles Previous button click event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Previous button Press
     */
    navigateToStage1: function(component, event, helper) {
        component.set("v.stage", 1);
    },
    /**
     * Handle Next button Click event from stage 0 and stage 1.
     * if the project is created from deal then 
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Next button Press
     */
    goToNextStage: function(component, event, helper) {
        var currentStage = component.get("v.stage");

        if (currentStage === 1) {
            helper.navigateToStage2WithoutDeal(component);
        } else if (helper.isValidSDOSelected(component)) {
            helper.navigateToStage2WithDeal(component);
        } else {
            return;
        }

        helper.getFilteredSOD(component);
        helper.getSDOdata(component);
        helper.setPbbFlag(component);
    },
    /**
     * Handle Operating unit selection event.
     * Based on Operating unit selected following operations are performed:
     * 
     * 1> Populate currency
     * 2> Update list of parent project for group invoice
     * 3> Update list of Tax Code
     * 4> Conditionally show HSN Category selection option is the selected operating unit is applicable for GST
     * 
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    operatingUnitChangeHandler: function(component, event, helper) {
        helper.operatingUnitChangeHandler(component);
    },
    /**
     * Handle Customer Hierarchy selection event.
     * get filtered list of vertical and sub vertical.
     * @param {Aura.Component} component - this component
     * @param {Event} event - LookupComponent.updateLookupIdEvent
     */
    updateCustomerHierarchy: function(component, event, helper) {
        var accountID = event.getParam("sObjectId");
        var projectRecord = component.get("v.projectRecord");
        helper.getverticalSubVertical(component, accountID);
        // Gainshare Change
        helper.gainshareFieldsHandler(component, projectRecord);
        // Update CRN on Account chnage for M&A PIDs
        /*if(accountID && (component.get("v.createWithoutSFDCProject") || projectRecord.GP_Deal_Category__c === 'Without SFDC')) {
            helper.getCRNDataOnAccountChangeHandler(component, accountID);
        }*/
		 // Avinash: Valueshare Change: here we are considering Intercoe too.
        if(accountID && ((component.get("v.createWithoutSFDCProject") || component.get("v.createInterCOEProject")) || projectRecord.GP_Deal_Category__c !== 'Sales SFDC')) {
            helper.getCRNDataOnAccountChangeHandler(component, accountID);
        }
    },
    /**
     * Handle  Customer Hierarchy clear event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - LookupComponent.clearLookupIdEvent
     */
    clearCustomerHierarchy: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord");
        helper.clearBusinessHierarchy(component);
        // Gainshare Change
        helper.gainshareFieldsHandler(component, projectRecord);
    },
    /**
     * Handle Operating Unit clear event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - LookupComponent.clearLookupIdEvent
     */
    clearOperatingUnit: function(component, event, helper) {
        component.set("v.projectRecord.GP_Operating_Unit__c", null);
        helper.updateListOfParentProjectForInvoice(component);
    },
    /**
     * Handle cancel button Click event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for cancel button Press
     */
    cancel: function(component, event, helper) {
        var dealData = component.get("v.dealData");
        var recordId = component.get("v.recordId");

        if (recordId) {
            helper.redirectToSobject(recordId);
        } else if (dealData && dealData.Id) {
            helper.redirectToSobject(dealData.Id);
        } else {
            helper.redirectToListView("GP_Project__c");
        }
    },
    /**
     * Handle MOD change event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    modChangeHandler: function(component, event, helper) {
        var selectedMOD = component.get("v.projectRecord.GP_MOD__c");

        if (selectedMOD === 'Out of Scope') {
            helper.showToast('warning', 'warning', 'Invoice Dispatch will not happen for this Project from GDS [Global Dispatch System]');
        } else if (selectedMOD === 'Autopay') {
            helper.showToast('warning', 'warning', 'Invoice will not be dispatched to client &  will tag as Auto Pay');
        }
		
		//var modRecordType=component.get("v.MODRecordType");
         var projectRecord = component.get("v.projectRecord");
       //Fixed monthly,Fixed Price,FTE,T&M,Transactions,Gainshare       
       var billableProjectTypeLabel = $A.get("$Label.c.GP_Billable_Project_Type_Values");

        if ((selectedMOD === 'Upload to Portal' ||
            selectedMOD === 'Email and Upload to Portal' ||
            selectedMOD === 'Hard Copy and Upload to Portal' ||
            selectedMOD === 'Fax and Upload to Portal') && 
            (projectRecord.GP_Project_type__c !== null && billableProjectTypeLabel.indexOf(projectRecord.GP_Project_type__c) != -1)) {
            // (projectRecord.RecordType !==null && projectRecord.RecordType.Name !== 'Indirect PID') ) {              
            var POFlag = component.get("v.projectRecord.GP_Is_PO_Required__c");
            if(!POFlag)
            {
                component.set("v.projectRecord.GP_Is_PO_Required__c",true);                  
                helper.SetPOFlag(component,true);
                
            }
            helper.showToast('warning', 'warning', 'PO Flag is Auto Turn ON');
        }
        
        if ((selectedMOD === 'Email' ||
             selectedMOD === 'Email and Fax' ||
             selectedMOD === 'Email and Upload to Portal' ||
             selectedMOD === 'Email, Hard Copy, FAX and Upload to Portal' ||
             selectedMOD === 'Email and Hard Copy' ||
             selectedMOD === 'Email, Hard Copy and Upload to Portal') && 
            (projectRecord.GP_Project_type__c !== null 
             && billableProjectTypeLabel.indexOf(projectRecord.GP_Project_type__c) != -1)) {
            var attnToEmail = component.get("v.projectRecord.GP_Attn_To_Email__c");
            if(!$A.util.isEmpty(attnToEmail)){
                var elementIndex = attnToEmail.indexOf('@genpact.com');
                if(elementIndex != -1)
                {
                    helper.showToast('', 'Error', 'Only Input Customer’s Email ID who must receive the Invoices billed on this PID for timely & accurate Invoice Dispatch');            
                }
            }
            
        }
		
        helper.resetRelatedRecordForMod(component);
    },
	IsPORequiredChangeHandler: function(component, event, helper) {
        var poflg = component.get("v.projectRecord.GP_Is_PO_Required__c");
         helper.SetPOFlag(component,poflg);
    },
    /**
     * Handle Project type change event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    projectTypeChangeHandler: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord");
        helper.updateProjectCategoryOnProjectTypeChange(component);
        helper.changeOfBillRate(component);
        helper.updateWorkingHour(component);
        helper.resetDependentFieldsForProjectType(component);

        var listOfRecordType = component.get("v.listOfRecordType");
        var bpmRecordType = helper.getSelectedRecordByName(listOfRecordType, 'BPM') || {};

        var projectTemplate = component.get("v.projectTemplate");
        var isTCVRequired = helper.isTCVMandatory(projectRecord, bpmRecordType["Id"]);
        projectTemplate.Project___Project_Info___GP_TCV__c.isRequired = isTCVRequired;
        component.set("v.projectTemplate", projectTemplate);
		
		// Gainshare Change
        helper.resetGainshareFieldsHandler(component, ['GP_Pure_Gainshare__c', 'GP_Parent_PID_For_Gainshare__c']);
        //component.set("v.projectRecord.GP_Pure_Gainshare__c", false);
        //component.set("v.projectRecord.GP_Parent_PID_For_Gainshare__c", null);
        helper.getParentPIDFilterAndInvoiceFrequencyList(component, projectRecord);
    },
	Parentgainsharepid : function(component,event,helper) {
        // Gainshare Change
        helper.getParentPIDFilterAndInvoiceFrequencyList(component, component.get("v.projectRecord"));        
    },
    billingCurrencyChange: function(component, event, helper) {
        var project = component.get("v.projectRecord") || {};
        if(project && project.Id && project.GP_Parent_Project__c){
            helper.showToast('warning', 'warning', 'Please review resource assignment bill rate and billing Milestone');
        }
    },
    billRateTypeChangeHandler: function(component, event, helper) {
        helper.updateWorkingHour(component);
    },
    businessGroupChangeHandler: function(component, event, helper) {
        // helper.setIsPORequiredValue(component);
    },
    /**
     * Handle CRN number change event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    crnNumberChangeHandler: function(component, event, helper) {
        var CRNNumber = component.get("v.projectRecord.GP_CRN_Number__c");
        var projectRecord = component.get("v.projectRecord");
        helper.getRelatedCRNData(component, CRNNumber);
        // Gainshare Change
        helper.gainshareFieldsHandler(component, projectRecord);
    },
	updateParentProjectPID: function(component,event,helper) {          
        // Gainshare Change
        component.set("v.projectRecord.GP_Parent_PID_For_Gainshare__c", event.getParam("recordName"));
        
        var element = document.getElementById('GP_Parent_PID_For_Gainshare__c');
        $A.util.addClass(element, "slds-hide");
        $A.util.removeClass(element, "errorText");
        element.innerHTML = '';
    },    
    clearGainshareParentPID : function(component, event, helper) {
        // Gainshare Change
        helper.resetGainshareFieldsHandler(component, ['GP_Parent_PID_For_Gainshare__c']);
    },
    /**
     * Handle Product hierarchy save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveProductHierarchy: function(component, event, helper) {
        helper.saveProjectService(component, 'Product_Hierarchy');
    },
    /**
     * Handle Business hierarchy save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveBusinessHierarchy: function(component, event, helper) {

        var projectRecord = component.get("v.projectRecord");
        var createWithoutSFDCProject = component.get("v.createWithoutSFDCProject");
		var createInterCOEProject = component.get("v.createInterCOEProject");//Avinash - PO flag
        var createSupportProject = component.get("v.createSupportProject");
        var projectTemplate = component.get("v.projectTemplate");
        var activeSectionName = "Bussiness_Hierarchy";
		  //Avinash - handle PO flag for M&A and L1=GE
        if(projectRecord.RecordType !== null){
            if(projectRecord.RecordType.Name !=='Indirect PID' &&
               projectRecord.GP_Business_Group_L1__c ==='GE' 
               && (createWithoutSFDCProject === true ||
                   createInterCOEProject === true)
              )
            {            
                component.set("v.projectRecord.GP_Is_PO_Required__c",true);           
               
            }
        }
        var validationResponse = helper.validateProjectData(component, projectRecord, projectTemplate, activeSectionName);

        if (!validationResponse.isValid) {
            return;
        }

        if ($A.util.isEmpty(projectRecord["Id"])) {
            var message = "Since this is a ";
            message += createWithoutSFDCProject ? "Without SFDC" : "Support";
            message += " project record will not be saved unless you fill data in next section";
            alert(message);
            component.set("v.activeSectionName", activeSectionName);
            helper.navigateToNextSection(component);
        } else {
            helper.saveProjectService(component, 'Bussiness_Hierarchy');
        }
    },
    /**
     * Handle Project info save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveProjectInfo: function(component, event, helper) {
        helper.saveProjectService(component, 'Project_Info');
    },
    /**
     * Handle CMITS section save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveCMITSField: function(component, event, helper) {
        helper.saveProjectService(component, 'CMITS_Fields');
    },
    /**
     * Handle Other classification save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveOtherClassification: function(component, event, helper) {
        helper.saveProjectService(component, 'OtherClassification');
    },
    /**
     * Handle Dispatch info save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveDispatchInfo: function(component, event, helper) {
        helper.saveProjectService(component, 'DispatchInfo');
    },
    /**
     * Handle GE Project detail save event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    saveGEProjectDetail: function(component, event, helper) {
        helper.saveProjectService(component, 'GEProjectDetails');
    },
    /**
     * Handle Service line change event.
     * on the basis of selected service line it filters the list of nature of work.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    serviceLineChangeHandler: function(component, event, helper) {        
        //populate list of service line on the basis of selected family
        var selectedServiceLine = component.get("v.projectRecord.GP_Service_Line__c");
        
        var mapOfServiceLineToNatureOfWork = component.get("v.mapOfServiceLineToNatureOfWork") || {};
        
        var listOfAvailableNatureOfWork = component.get("v.listOfAvailableNatureOfWork");

        if (component.get("v.createSupportProject")) {
            //reset child records
            component.set("v.listOfAvailableProducts", []);
            component.set("v.projectRecord.GP_Nature_of_Work__c", null);
        } else {
            //reset the product and product id also.            
            helper.natureOfWorkChangeHelper(component);
        }

        listOfAvailableNatureOfWork = mapOfServiceLineToNatureOfWork[selectedServiceLine] || [];
        component.set("v.listOfAvailableNatureOfWork", helper.sort(listOfAvailableNatureOfWork, true));

        helper.updateListOfSubCategory(component, selectedServiceLine);
    },
    /**
     * Handle Nature of work change event.
     * On the basis of selected Nature of Work the list of filtered products are populated.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    natureOfWorkChangeHandler: function(component, event, helper) {
        /*var selectedNatureOfWork = component.get("v.projectRecord.GP_Nature_of_Work__c");
        var mapOfNatureOfWorkToProduct = component.get("v.mapOfNatureOfWorkToProduct") || {};

        //Added
        var selectedServiceLine = component.get("v.projectRecord.GP_Service_Line__c");

        var listOfAvailableProducts = component.get("v.listOfAvailableProducts");

        if (listOfAvailableProducts && listOfAvailableProducts.length > 0) {
            //reset child records
            component.set("v.projectRecord.GP_Product__c", null);
            component.set("v.projectRecord.GP_Product_Id__c", null);
        }

        listOfAvailableProducts = mapOfNatureOfWorkToProduct[selectedServiceLine + '--' + selectedNatureOfWork] || [];
        component.set("v.listOfAvailableProducts", helper.sort(listOfAvailableProducts));*/
        
        helper.natureOfWorkChangeHelper(component);
    },
    /**
     * Handle Delivery Org change event.
     * On the basis of selected Delivery ORG list of Sub Delivery ORG is Populated
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    deliveryOrgChangeHandler: function(component, event, helper) {
        var selectedDeliveryORG = component.get("v.projectRecord.GP_Delivery_Org__c");
        var mapOfDeliveryToSubDelivery = component.get("v.mapOfDeliveryToSubDelivery") || {};

        var listOfAvailableSubDelivery = component.get("v.listOfAvailableSubDelivery");

        if (listOfAvailableSubDelivery && listOfAvailableSubDelivery.length > 0) {
            //reset child records
            component.set("v.projectRecord.GP_Sub_Delivery_Org__c", null);
        }

        listOfAvailableSubDelivery = mapOfDeliveryToSubDelivery[selectedDeliveryORG] || [];
        component.set("v.listOfAvailableSubDelivery", helper.sort(listOfAvailableSubDelivery, true));
    },
    /**
     * Handle Project address change event event.
     * Updates parent project for Invoice list
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    projectAddressChangeHandler: function(component, event, helper) {
        helper.updateParentProjectInvoiceList(component);
        component.set('v.activeSectionName', 'Bill_To_And_Ship_To');
        helper.navigateToNextSection(component);
    },
    /**
     * Handle Side bar minimization and Expansion event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },
    /**
     * Handle Product Change event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    productChangeHandler: function(component, event, helper) {
        helper.setProductId(component);
        // helper.populateDefaultHSL(component);
    },
    /**
     * Handle GOL Clear event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    clearGOL: function(component, event, helper) {
        component.set("v.projectRecord.GP_GOL__c", null);
    },
    /**
     * Handle GRM Clear event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    clearGRM: function(component, event, helper) {
        component.set("v.projectRecord.GP_GRM__c", null);
    },
    /**
     * Handle Collector Clear event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    clearCollector: function(component, event, helper) {
        component.set("v.projectRecord.GP_Collector__c", null);
    },
    /**
     * Handle HSN Selection event.
     * on the basis of selected HSN populate the HSN Oracle Id and HSN Name
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     */
    hsnSelectHandler: function(component, event, helper) {
        // var selectedHSN = component.get("v.selectedHSN");
        var selectedHSN = event.getSource().get("v.text");
        var hsnId, hsnName;

        if ($A.util.isEmpty(selectedHSN)) {
            hsnId = null;
            hsnName = null;
        } else {
            hsnId = selectedHSN.split("-")[0];
            hsnName = selectedHSN.split("-")[1];
        }

        var projectRecord = component.get("v.projectRecord");

        projectRecord["GP_HSN_Category_Id__c"] = hsnId;
        projectRecord["GP_HSN_Category_Name__c"] = hsnName;

        component.set("v.projectRecord", projectRecord);
        component.set("v.selectedHSN", selectedHSN);
    },
    shiftAllowedChangeHandler: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord") || {};

        if (!projectRecord.GP_Shift_Allowed__c) {
            projectRecord.GP_Shift_Start_Date__c = null;
            component.set("v.projectRecord", projectRecord);
        }
    },
    transitionAllowedChangeHandler: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord") || {};

        if (!projectRecord.GP_Transition_Allowed__c) {
            projectRecord.GP_Transition_End_Date__c = null;
            component.set("v.projectRecord", projectRecord);
        }
    },
    parentProjectForGroupInvoiceChangeHandler: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord") || {};

        if (!projectRecord.GP_Parent_Project_for_Group_Invoice__c) {
            projectRecord.GP_Grouped_Invoice__c = null;
            component.set("v.projectRecord", projectRecord);
        }
    },
    serviceDeliverUSCanadaChangeHandler: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord") || {};
		
        var isUSCanadaAddress=component.get("v.isUsCanadaAddr");
       var isAvalara = component.get("v.isAvalaraEnable");
         if(isUSCanadaAddress && !projectRecord.GP_Service_Deliver_in_US_Canada__c && isAvalara)
        {
            helper.showToast('warning', 'warning', 'Note : Bill To/Ship To Address has US/Canada, pls. ensure to update fields related to “Is Service Delivery in US/Canada for apt TAX Calculation');
            //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
            //return;
        }
		component.set("v.CopyBillToAddrFlag",false);
        component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",projectRecord.GP_Service_Deliver_in_US_Canada__c);
        helper.SetServiceDeliveryInCanadaFlag(component,projectRecord.GP_Service_Deliver_in_US_Canada__c)
         if (!projectRecord.GP_Service_Deliver_in_US_Canada__c) {
			component.set("v.projectRecord.GP_Sub_Category_Of_Services__c",null);
             component.set("v.projectRecord.GP_Tax_Exemption_Certificate_Available__c",false);
             component.set("v.projectRecord.GP_Valid_Till__c",null);
			 
			component.set("v.projectRecord.GP_US_Address__c","");   
            component.set("v.projectRecord.GP_City__c","");
            component.set("v.projectRecord.GP_City__r.Name","");
            component.set("v.projectRecord.GP_Pincode__c",""); 
            component.set("v.projectRecord.GP_State__c",""); 
            component.set("v.projectRecord.GP_Country__c","");
		 }
            
		
    },
    projectLeadershipSaveHandler: function(component, event, helper) {
        var nameOfRelatedRecord = event.getParam("nameOfRelatedRecord");
        if (nameOfRelatedRecord === "mandatoryKeyMembers") {
            helper.projectLeadershipSaveHandler(component);
        }
    },
    sdoChangeHandler: function(component, event, helper) {
        helper.setPbbFlag(component);
    },
    clearCity: function(component, event, helper) {
        var projectRecord = component.get("v.projectRecord");
        
        projectRecord.GP_City__c = null;
        projectRecord.GP_City__r = {};

        component.set("v.projectRecord", projectRecord);
    },
	//Avinash - Avalara - changes
    CopyBillToAddSetDeliveryAdd :function(component,event,helper)
    {  
        if(component.get("v.CopyBillToAddrFlag"))
        {
            var depnedentFieldMap = component.get("v.depnedentFieldMap");
            helper.getBillToAddress(component,event,depnedentFieldMap); 
            // $A.enqueueAction(component.get("c.onControllerFieldChange"));
            
        }
        else
        {
            component.set("v.projectRecord.GP_US_Address__c","");   
            component.set("v.projectRecord.GP_City__c","");
            component.set("v.projectRecord.GP_City__r.Name","");
            component.set("v.projectRecord.GP_Pincode__c",""); 
            component.set("v.projectRecord.GP_State__c",""); 
            component.set("v.projectRecord.GP_Country__c",""); 
            component.set("v.readonlyAddress",true); //isEditable
            //component.set("v.isEditable",false); //isEditable
        }
    },
    onControllerFieldChange: function(component, event, helper) { 
        debugger;
        var controllerValueKey;
        var stVal=component.get("v.projectRecord.GP_Country__c");
        if(!$A.util.isUndefinedOrNull(stVal))
        {
            controllerValueKey=stVal;
        }
        else
        {
            controllerValueKey = event.getSource().get("v.value"); // get selected controller field value
        }
        var depnedentFieldMap = component.get("v.depnedentFieldMap");
        
        
        
        if (controllerValueKey != '--None--') {
            var ListOfDependentFields = depnedentFieldMap[controllerValueKey];
            
            if(ListOfDependentFields.length > 0){
                component.set("v.bDisabledDependentFld" , false);  
                helper.fetchDepValues(component, ListOfDependentFields);    
            }else{
                component.set("v.bDisabledDependentFld" , true); 
                component.set("v.listDependingValues", ['--None--']);
            }  
            
        } else {
            component.set("v.listDependingValues", ['--None--']);
            component.set("v.bDisabledDependentFld" , true);
        }
    }
})