({
    listOfSectionName: [],
    mapOfSectionNameToFieldName: {},
    mapOfFieldNameToSectionName: {},
    mapOfSectionNameToErrorCounter: {},
    DEFAULT_EMAIL: "receivables.helpdesk@genpact.com",
    listOfMandatoryFieldsForDealForSupportProject: [
        "GP_Business_Group_L1__c",
        "GP_Business_Segment_L2__c",
        "GP_Sub_Business_L3__c",
        "GP_Customer_Hierarchy_L4__c", //
        "GP_Vertical__c",
        "GP_Sub_Vertical__c",
        "GP_Nature_of_Work__c",
        "GP_Service_Line__c",
        "GP_Product__c"
    ],
    listOfMandatoryFieldsForDealForWithoutSFDCProject: [
        "GP_Business_Group_L1__c",
        "GP_Business_Segment_L2__c",
        "GP_Sub_Business_L3__c",
        "GP_Customer_Hierarchy_L4__c", //
        "GP_Vertical__c",
        "GP_Sub_Vertical__c",
        "GP_Service_Line__c",
        "GP_Nature_of_Work__c",
        "GP_Product__c",
        "GP_Business_Name__c",
        "GP_Business_Type__c",
        "GP_Sub_Delivery_Org__c",
        "GP_TCV__c"

    ],
    LIST_OF_DAILY_NORMAL_HOUR: [],
    LIST_OF_WEEKLY_NORMAL_HOUR: [],
    L4_FILTER_CONDITION: "(Not Name LIKE '%unidentified%')",
    /**
     * Handle Component Initialization event.
     * @param {Aura.Component} component - this component
     * @description when the component is opened in new mode then fetch record types
     * other wise if the component is opened in edit mode then fetch all the project data along with its template 
     * and picklist values.
     */
    doInitHelper: function(component) {
        var recordId = component.get("v.recordId");

        this.setDailyAndWeeklyNormalHours(component);
        this.getProjectRecordTypeService(component);

        if (recordId) {
            this.getDependentPicklistData(component);
            this.getProjectData(component, recordId);
            component.set("v.stage", 2);
        } else {
            var dealData = this.isDealDataAvailable(component);
            var projectRecord = component.get("v.projectRecord") || {};
            
            if(!$A.util.isUndefinedOrNull(dealData) && !$A.util.isUndefinedOrNull(dealData.GP_Business_Name__c)) {
                var cloneProjectFilterCondition = 'GP_Approval_Status__c = \'Approved\' AND GP_Oracle_Status__c = \'S\'';
                if(dealData.GP_Business_Name__c === 'CMITS') {
                    cloneProjectFilterCondition += ' AND Recordtype.DeveloperName = \'CMITS\'';
                } else if(dealData.GP_Business_Name__c === 'BPM') {
                    cloneProjectFilterCondition += ' AND Recordtype.DeveloperName = \'BPM\'';
                } else if(dealData.GP_Business_Name__c === 'Other PBB' 
                          || dealData.GP_Business_Name__c === 'Digital'
                          || dealData.GP_Business_Name__c === 'Consulting') {
                    cloneProjectFilterCondition += ' AND Recordtype.DeveloperName = \'OTHER_PBB\'';
                }
                component.set("v.cloneProjectFilterCondition", cloneProjectFilterCondition);
            } 
            
            if (!dealData) {
                this.getIsAvailableForLegacyProjectCreation(component);
            } else {
                //this.getRelatedProjectsUnderDeal(component, dealData);
                if(dealData != null && dealData.GP_Business_Name__c !== null) {
                    component.set("v.allowProjectCreationUnderDeal", true);
                }
                component.set("v.stage", 0);
            }

            this.getProjectTypePicklist(component);
            this.getFilteredSOD(component);
        }
    },
    setProjectDataFromDeal: function(component) {
        var dealData = this.isDealDataAvailable(component);
        var projectRecord = component.get("v.projectRecord") || {};
        if (dealData) {
            projectRecord.GP_TCV__c = dealData.GP_OMS_TCV__c ? dealData.GP_OMS_TCV__c : dealData.GP_TCV__c;
            projectRecord.GP_Product__c = dealData.GP_Product__c;
            projectRecord.GP_Product_Id__c = dealData.GP_Product_Id__c;
            projectRecord.GP_OMS_Deal_ID__c = dealData.GP_OMS_Deal_ID__c;
            projectRecord.GP_Delivery_Org__c = dealData.GP_Delivery_Org__c;
            projectRecord.GP_Service_Line__c = dealData.GP_Service_Line__c;
            projectRecord.GP_Nature_of_Work__c = dealData.GP_Nature_of_Work__c;
            projectRecord.GP_Sub_Delivery_Org__c = dealData.GP_Sub_Delivery_Org__c;
            projectRecord.GP_Revenue_Description__c = dealData.GP_Revenue_Description__c;

            projectRecord.GP_CC1__c = dealData.GP_CC1__c;
            projectRecord.GP_CC2__c = dealData.GP_CC2__c;
            projectRecord.GP_CC3__c = dealData.GP_CC3__c;
            projectRecord.GP_Fax_Number__c = dealData.GP_Fax_Number__c;
            //projectRecord.GP_Portal_Name__c = dealData.GP_Portal_Name__c;
            projectRecord.GP_Attn_To_Name__c = dealData.GP_Attn_To_Name__c;
            projectRecord.GP_Attn_To_Email__c = dealData.GP_Attn_To_Email__c;
            projectRecord.GP_Customer_SPOC_Name__c = dealData.GP_Customer_SPOC_Name__c;
            projectRecord.GP_Mode_of_dispatch_Id__c = dealData.GP_Mode_of_dispatch_Id__c;
            projectRecord.GP_Customer_SPOC_Email__c = dealData.GP_Customer_SPOC_Email__c;
            projectRecord.GP_Customer_Contact_Number__c = dealData.GP_Customer_Contact_Number__c;
            projectRecord.GP_Pricing_Status__c = dealData.GP_Pricing_Approval_Status__c;

            projectRecord.GP_Sales_Opportunity_Id__c = dealData.GP_Sales_Opportunity_Id__c;
            projectRecord.GP_Opportunity_Name__c = dealData.GP_Opportunity_Name__c;
            projectRecord.GP_Opportunity_Project__c = dealData.GP_Opportunity_Project__c;
            projectRecord.GP_OLI_SFDC_Id__c = dealData.GP_OLI_SFDC_Id__c;
            projectRecord.GP_Is_PO_Required__c = dealData.GP_Business_Group_L1__c == 'GE';
            projectRecord.GP_Project_type__c = dealData.GP_Project_Type__c == 'Fixed Monthly' ? 'Fixed monthly' : dealData.GP_Project_Type__c;

            //Added 
            if ($A.util.isEmpty(projectRecord["Id"])) {
                projectRecord["GP_Approval_Status__c"] = "Draft";
            }

            component.set("v.projectRecord", projectRecord);
            component.set("v.stage", 0);
        }
    },
    projectTypeChangeHandlerDefault: function(component) {
        var projectRecord = component.get("v.projectRecord");
        this.updateProjectCategoryOnProjectTypeChange(component);
        this.changeOfBillRate(component);
        this.updateWorkingHour(component);
        this.resetDependentFieldsForProjectType(component);

        var listOfRecordType = component.get("v.listOfRecordType");
        var bpmRecordType = this.getSelectedRecordByName(listOfRecordType, 'BPM') || {};

        var projectTemplate = component.get("v.projectTemplate");
        var isTCVRequired = this.isTCVMandatory(projectRecord, bpmRecordType["Id"]);
        projectTemplate.Project___Project_Info___GP_TCV__c.isRequired = isTCVRequired;
        component.set("v.projectTemplate", projectTemplate);
		
		 // Avinash : Valueshare changes
        this.handleValueshare(component, undefined, 'NA', undefined);
    },
    setDailyAndWeeklyNormalHours: function(component) {
        this.LIST_OF_DAILY_NORMAL_HOUR = this.getArrayForRange(8, 10);
        this.LIST_OF_WEEKLY_NORMAL_HOUR = this.getArrayForRange(20, 60);
    },
    getArrayForRange: function(startRange, endRange) {
        var list = [{
            "label": '--NONE--'
        }];

        for (var i = startRange; i <= endRange; i += 1) {
            list.push({
                "text": i,
                "label": i
            });
        }

        return list;
    },
    setPbbFlag: function(component) {
        var createSupportProject = component.get("v.createSupportProject");
        var pbbFlag;

        /*if (createSupportProject) {
            pbbFlag = "Yes";
        } else {
            var selectedSDO = this.getSelectedSDORecord(component) || {};
            pbbFlag = selectedSDO["externalParameter"] === "PBB" ? "Yes" : "No";
        }*/

        var selectedSDO = this.getSelectedSDORecord(component) || {};
        pbbFlag = selectedSDO["externalParameter"] === "PBB" ? "Yes" : "No";
        component.set("v.projectRecord.GP_PBB_Flag__c", pbbFlag);
        component.set("v.projectRecord.GP_Primary_SDO__r.Name", selectedSDO["label"]);
    },
    getSelectedSDORecord: function(component) {
        var selectedSDO = component.get("v.projectRecord.GP_Primary_SDO__c");
        var listOfSDO = component.get("v.listOfFilteredSDO") || [];

        for (var i = 0; i < listOfSDO.length; i += 1) {
            if (listOfSDO[i]["text"] === selectedSDO) {
                return listOfSDO[i];
            }
        }

        return null;
    },
    /**
     * Handle Component Side navigation cick event.
     * @param {Aura.Component} component - this component
     * @param {Event} event - HTML DOM Event for Component Initialization
     * @description Toggel the visibility of sections when clicked on navigation side bar
     */
    toggelSection: function(component, event) {
        var activeSection = event.currentTarget.id;
        var activeSectionName = activeSection.substring(activeSection.indexOf("_") + 1, activeSection.indexOf("_sidebar"));
        var recordId = component.get("v.recordId");

        component.set("v.activeSectionName", activeSectionName);

        var listOfSectionName = [
            "Bussiness_Hierarchy",
            "Project_Info",
            "Project_Leadership",
            "Bill_To_And_Ship_To",
            "CMITS_Fields",
            "OtherClassification",
            "DispatchInfo",
            "GEProjectDetails",
            "OracleStatus",
            "Dashboard"
        ];

        for (var i = 0; i < listOfSectionName.length; i += 1) {
            var currentSideBarName = listOfSectionName[i] + '_sidebar';
            var currentSectionName = listOfSectionName[i];

            if (recordId) {
                currentSideBarName = recordId + '_' + currentSideBarName;
            } else {
                currentSideBarName = '_' + currentSideBarName;
            }

            var sideBar = document.getElementById(currentSideBarName);
            var sectionContent = component.find(currentSectionName);

            if (currentSectionName !== activeSectionName) {
                $A.util.removeClass(sideBar, 'active');
                $A.util.removeClass(sectionContent, 'slds-is-open');
            } else {
                $A.util.addClass(sideBar, 'active');
                $A.util.addClass(sectionContent, 'slds-is-open');
            }
        }
    },
    /**
     * fetches the boolean if logged in user can create legacy project
     * @param {Aura.Component} component - this component
     * @description fetches the boolean if logged in user can create legacy project
     */
    getIsAvailableForLegacyProjectCreation: function(component) {
        var getIsAvailableForLegacyProjectCreation = component.get("c.getIsAvailableForLegacyProjectCreation");
        getIsAvailableForLegacyProjectCreation.setCallback(this, function(response) {
            this.setAvailableForLegacyProjectHandler(component, response);
        });

        $A.enqueueAction(getIsAvailableForLegacyProjectCreation);
    },
    /**
     * handles the getIsAvailableForLegacyProjectCreation callback
     * @param {Aura.Component} component - this component
     * @param {response} Object
     * @description handles the getIsAvailableForLegacyProjectCreation callback
     */
    setAvailableForLegacyProjectHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var isAvailableForLegacyProjectCreation = JSON.parse(responseData.response);
            component.set("v.isAvailableForLegacyProjectCreation", isAvailableForLegacyProjectCreation)
        } else {
            // component.set("v.errorMessage", responseData.message);
        }
    },
    /**
     * Fetches the project data for given projectId.
     * @param {Aura.Component} component - this component
     * @param {Id} SFDC Project Id - SFDC ID of Project
     * @description Fetches the project data for given projectId.
     */
    getProjectData: function(component, projectId) {debugger;
        var getProjectData = component.get("c.getProjectData");

        getProjectData.setParams({
            "projectId": projectId
        });

        getProjectData.setCallback(this, function(response) {
            this.getProjectDataHandler(component, response);
        });

        $A.enqueueAction(getProjectData);
    },
    /**
     * Handle getProjectData proimse.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Handle getProjectData proimse.
     */
    getProjectDataHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectData(component, responseData);
            this.getFilteredSOD(component);

            this.updateWorkingHour(component);
            this.changeOfBillRate(component, true);
            this.updateListOfParentProjectForInvoice(component);
            this.updateIsGSTApplicable(component);
            this.updateProjectCategoryOnProjectTypeChange(component);
            var projectRecord = component.get("v.projectRecord") || {};
            if(projectRecord.GP_Customer_Hierarchy_L4__c)
            	this.getverticalSubVertical(component, projectRecord.GP_Customer_Hierarchy_L4__c);

            var mapOfServiceLineToNatureOfWork = component.get("v.mapOfServiceLineToNatureOfWork") || {};
            var mapOfNatureOfWorkToProduct = component.get("v.mapOfNatureOfWorkToProduct") || {};
            var selectedServiceLine = component.get("v.projectRecord.GP_Service_Line__c");
            var listOfAvailableNatureOfWork = mapOfServiceLineToNatureOfWork[selectedServiceLine] || [];

            var selectedNatureOfWork = component.get("v.projectRecord.GP_Nature_of_Work__c");
            var listOfAvailableProducts = mapOfNatureOfWorkToProduct[selectedServiceLine + '--' + selectedNatureOfWork] || [];

            component.set("v.listOfAvailableNatureOfWork", this.sort(listOfAvailableNatureOfWork, true));

            component.set("v.listOfAvailableProducts", this.sort(listOfAvailableProducts));
			
			  //Avalara Changes
            var isServiceDelFlagOn = component.get("v.projectRecord.GP_Service_Deliver_in_US_Canada__c");
           /* if(!isServiceDelFlagOn)
            {
             this.CheckIfAvalaraEnabled(component,responseData);
            }*/
			
			this.CheckIfAvalaraEnabled(component,responseData);
            //-----//

            this.getProjectTypePicklist(component);
			
			// Gainshare Change
            this.getParentPIDFilterAndInvoiceFrequencyList(component, projectRecord);
			
			// Avinash : Valueshare changes.
            this.handleValueshare(component, projectRecord.GP_Project_type__c, projectRecord.GP_Oracle_PID__c, projectRecord.GP_Deal_Category__c);
      
        } else {
            component.set("v.errorMessage", responseData.message);
        }
    },
    /**
     * Sets Project Data is the Aura response has no error.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Sets Project Data is the Aura response has no error.
     */
    setProjectData: function(component, responseData) {
		debugger;
        var responseJson = JSON.parse(responseData.response);
        var projectTemplate = this.getConcatenatedTemplate(responseJson.ProjectTemplate);
        var projectRecord = responseJson.Project;
        var originalProjectRecord = JSON.parse(JSON.stringify(projectRecord));
        var selectedCity = projectRecord.GP_City__c;
        projectRecord["GP_City__r"] = {
            "Name": selectedCity
        };
        component.set("v.projectRecord", projectRecord);
        var isEditable;
        var mapOfRoleAccessibility = component.get("v.mapOfRoleAccessibility") || {};

        if (mapOfRoleAccessibility.projectcreation == 'read') {
            isEditable = false;
        } else {
            isEditable = projectRecord.GP_Approval_Status__c === "Draft" ||
                projectRecord.GP_Approval_Status__c === "Rejected";
        }

        component.set("v.isEditable", isEditable);
        component.set("v.originalProjectRecord", originalProjectRecord);
        var mapOfFieldNameToListOfOptions = responseJson.mapOfFieldNameToListOfOptions;
        var mapOfServiceLineToListOfSubCategory = responseJson.mapOfServiceLineToListOfSubCategory;

        for (var key in mapOfFieldNameToListOfOptions) {

            var listOfOptions = mapOfFieldNameToListOfOptions[key];
            if (key === "listOfHSN") {
                this.sort(listOfOptions);
            } else {
                this.sortAndAddDefault(listOfOptions);
            }
            mapOfFieldNameToListOfOptions[key] = listOfOptions;
        }

        var serviceLine = projectRecord.GP_Service_Line__c;
        var listOfSubCategoryOptions = mapOfServiceLineToListOfSubCategory[serviceLine];

        this.sortAndAddDefault(listOfSubCategoryOptions);
        mapOfFieldNameToListOfOptions["listOfSubCategoryOfServices"] = listOfSubCategoryOptions;

        var isEditingAfterApproval = !$A.util.isEmpty(projectRecord.GP_Parent_Project__c) &&
            projectRecord.GP_Oracle_PID__c !== 'NA';

        var hsnCategoryId = projectRecord.GP_HSN_Category_Id__c;
        var hsnCategoryName = projectRecord.GP_HSN_Category_Name__c;

        var selectedHSN = hsnCategoryId + "-" + hsnCategoryName;
        component.set("v.selectedHSN", selectedHSN);

        component.set("v.mapOfPicklistValues", mapOfFieldNameToListOfOptions);
        component.set("v.mapOfServiceLineToListOfSubCategory", mapOfServiceLineToListOfSubCategory);
        component.set("v.projectTemplate", projectTemplate);
        component.set("v.isEditingAfterApproval", isEditingAfterApproval);

        //update Account filter condition
        if (projectRecord.RecordType.Name !== 'Indirect PID') {
            component.set("v.l4FilterCondition", this.L4_FILTER_CONDITION);
        }
        
        // Set GST enabled OUs and HSNs.
        // ECR-HSN Changes.
		this.setListOfGSTOUsAndHSUs(component);
        this.fireProjectInitializedEvent(projectRecord, projectTemplate);
    },
    /**
     * Validates data entered in Stage 1 Screen.
     * @param {Aura.Component} component - this component
     * @param {Object} projectRecord
     * @description Validates data entered in Stage 1 Screen.
     */
    validateStage1: function(component, projectRecord) {
        var validationMessage = {
            "isSuccess": true,
            "message": "SUCCESS"
        };
        var sdoWithoutSfdcDom;
        var createSupportProject = component.get("v.createSupportProject");
        var createWithoutSFDCProject = component.get("v.createWithoutSFDCProject");
        var createInterCOEProject = component.get("v.createInterCOEProject");

        if (!createSupportProject && !createWithoutSFDCProject && !createInterCOEProject) {
            validationMessage["isSuccess"] = false;
            validationMessage["message"] = "Please select type of project.";
        } else if (createWithoutSFDCProject && $A.util.isEmpty(component.get("v.projectRecord.GP_Primary_SDO__c"))) {
            validationMessage["isSuccess"] = false;
            validationMessage["message"] = "Please select SDO";

            sdoWithoutSfdcDom = component.find("PrimarySDO_without_SFDC_required");
            $A.util.removeClass(sdoWithoutSfdcDom, 'slds-hide');
        } else if (createInterCOEProject && $A.util.isEmpty(component.get("v.projectRecord.GP_Primary_SDO__c"))) {
            validationMessage["isSuccess"] = false;
            validationMessage["message"] = "Please select SDO";

            sdoWithoutSfdcDom = component.find("PrimarySDO_without_SFDC_required");
            $A.util.removeClass(sdoWithoutSfdcDom, 'slds-hide');
        } else {
            sdoWithoutSfdcDom = component.find("PrimarySDO_without_SFDC_required");
            $A.util.addClass(sdoWithoutSfdcDom, 'slds-hide');
        }

        return validationMessage;

    },
    /**
     * Fetches Project template data for a given projectTemplateId.
     * @param {Aura.Component} component - this component
     * @description Fetches Project template data for a given projectTemplateId.
     */
    getProjectTemplate: function(component, SdoId, isIndirect) {
        this.showSpinner(component);
        var getProjectTemplate = component.get("c.getProjectTemplate");
        var dealData = component.get("v.dealData");
        var isWithoutSFDC = component.get("v.createWithoutSFDCProject");
        var isInterCOEProject = component.get("v.createInterCOEProject");

        var businessGroupL1, businessSegmentL2;

        if (dealData) {
            businessGroupL1 = dealData.GP_Business_Group_L1__c;
            businessSegmentL2 = dealData.GP_Business_Segment_L2__c;
        }

        getProjectTemplate.setParams({
            "sdoId": SdoId,
            "businessGroupL1": businessGroupL1,
            "businessSegmentL2": businessSegmentL2,
            "isIndirect": isIndirect,
            "isWithoutSFDC": isWithoutSFDC,
            "isInterCOEProject": isInterCOEProject
        });

        getProjectTemplate.setCallback(this, function(response) {
            this.getProjectTemplateHandler(component, response);
        });

        $A.enqueueAction(getProjectTemplate);
    },
    /**
     * Handles getProjectTemplate Promise.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Handles getProjectTemplate Promise.
     */
    getProjectTemplateHandler: function(component, response) {

        this.hideSpinner(component);

        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProjectTemplateData(component, responseData);
            this.getDefaultOpportunityData(component);
            this.updateWorkingHour(component);
            this.changeOfBillRate(component);
            this.projectTypeChangeHandlerDefault(component);
        } else {
            component.set("v.errorMessage", responseData.message);
        }
    },
    /**
     * Sets project template data when there is no error in getProjectTemplate Promise.
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} AuraResponseWrapper
     * @description Sets project template data when there is no error in getProjectTemplate Promise.
     */
    setProjectTemplateData: function(component, responseData) {
        var projectTemplate = JSON.parse(responseData.response);
        var finalJSON = this.getConcatenatedTemplate(projectTemplate);
        var projectRecord = component.get("v.projectRecord") || {};

        projectRecord.GP_Project_Template__c = projectTemplate.Id;
        this.setProjectRecordType(component, projectRecord, projectTemplate);
        component.set("v.projectTemplate", finalJSON);
        component.set("v.projectRecord", projectRecord);
    },
    setProjectRecordType: function(component, projectRecord, projectTemplate) {
        var listOfRecordType = component.get("v.listOfRecordType");
        var recordTypeName;

        if (projectTemplate.GP_Business_Type__c === "Indirect") {
            recordTypeName = "Indirect PID";
        } else if (projectTemplate.GP_Business_Type__c === "BPM") {
            recordTypeName = "BPM";
        } else if (projectTemplate.GP_Business_Type__c === "PBB" && projectTemplate.GP_Business_Name__c === "CMITS") {
            recordTypeName = "CMITS";
        } else if (projectTemplate.GP_Business_Type__c === "PBB" && projectTemplate.GP_Business_Name__c !== "CMITS") {
            recordTypeName = "OTHER PBB";
        }

        var recordType = this.getSelectedRecordByName(listOfRecordType, recordTypeName);
        projectRecord.RecordTypeId = recordType.Id;
        projectRecord.RecordType = {
            "Name": recordTypeName
        };
    },
    /**
     * Returns Concatenated ProjectTemplateData.
     * @param {SObject} projectTemplate SObject
     * @description Returns Concatenated ProjectTemplateData.
     */
    getConcatenatedTemplate: function(projectTemplate) {
        var finalTemplate = '';
        if (projectTemplate && projectTemplate.GP_Final_JSON_1__c) {
            finalTemplate += projectTemplate.GP_Final_JSON_1__c;
        }
        if (projectTemplate && projectTemplate.GP_Final_JSON_2__c) {
            finalTemplate += projectTemplate.GP_Final_JSON_2__c;
        }
        if (projectTemplate && projectTemplate.GP_Final_JSON_3__c) {
            finalTemplate += projectTemplate.GP_Final_JSON_3__c;
        }
        return JSON.parse(finalTemplate);
    },
    /**
     * Saves Project Templated in SFDC After validating it against Project Template.
     * @param {Aura.Component} component - this component
     * @description Saves Project Templated in SFDC After validating it against Project Template.
     */
    saveProjectService: function(component, activeSectionName) {
        var projectRecord = component.get("v.projectRecord");
        var createWithoutSFDCProject = component.get("v.createWithoutSFDCProject");
        var createSupportProject = component.get("v.createSupportProject");
        var saveProject, validationResponse, dealData;
        var projectTemplate = component.get("v.projectTemplate");
        var createInterCOEProject = component.get("v.createInterCOEProject");

        if ($A.util.isEmpty(projectRecord["Id"]) &&
            (createSupportProject || createWithoutSFDCProject || createInterCOEProject)) {
            projectRecord["GP_Approval_Status__c"] = "Draft";
            validationResponse = this.validateProjectData(component, projectRecord, projectTemplate, 'Bussiness_Hierarchy');
            if (!validationResponse.isValid) {
                this.redirectToSubSection(component, 'Bussiness_Hierarchy');
                return;
            }
        }
		
		// Gainshare Changes : M&A Module
        if(projectRecord["GP_Deal_Category__c"] && projectRecord["GP_Deal_Category__c"] === 'Without SFDC') {
            validationResponse = this.validateProjectData(component, projectRecord, projectTemplate, 'Bussiness_Hierarchy');
            if (!validationResponse.isValid) {
                this.redirectToSubSection(component, 'Bussiness_Hierarchy');
                return;
            }
        }

        validationResponse = this.validateProjectData(component, projectRecord, projectTemplate, activeSectionName);
        if (!validationResponse.isValid) {
            return;
        }

        var originalProjectRecord = component.get("v.originalProjectRecord");
        var mergedProjectRecord;

        if (!projectRecord["Id"]) {
            mergedProjectRecord = projectRecord;
        } else {
            mergedProjectRecord = this.getMergedRecord(originalProjectRecord, projectRecord, projectTemplate, activeSectionName);
        }

        mergedProjectRecord = this.getSanatizedData(mergedProjectRecord);

        if ($A.util.isEmpty(mergedProjectRecord["GP_End_Date__c"])) {
            mergedProjectRecord["GP_End_Date__c"] = null;
        }
        if ($A.util.isEmpty(mergedProjectRecord["GP_SOW_Start_Date__c"])) {
            mergedProjectRecord["GP_SOW_Start_Date__c"] = null;
        }
        if ($A.util.isEmpty(mergedProjectRecord["GP_SOW_End_Date__c"])) {
            mergedProjectRecord["GP_SOW_End_Date__c"] = null;
        }

        if (!mergedProjectRecord.Id) {
            var recordTypeId = this.getRecordTypeId(component);
            mergedProjectRecord["RecordTypeId"] = recordTypeId;
        }

        if (createWithoutSFDCProject || createSupportProject || createInterCOEProject) {

            if (!mergedProjectRecord["Name"]) {
                this.showToast("error", "error", "Please enter project name in project Info section");
                return;
            }

            dealData = component.get("v.dealData") || {};
            //code block to copy data from project to deal.
            var listOfMandatoryFieldsForDeal;
            if (createSupportProject) {
                listOfMandatoryFieldsForDeal = this.listOfMandatoryFieldsForDealForSupportProject;
            } else {
                listOfMandatoryFieldsForDeal = this.listOfMandatoryFieldsForDealForWithoutSFDCProject;
            }

            for (var i = 0; i < listOfMandatoryFieldsForDeal.length; i += 1) {
                var currentField = listOfMandatoryFieldsForDeal[i];
                dealData[currentField] = projectRecord[currentField];
            }

            dealData = this.getSanatizedData(dealData);
            dealData["Name"] = mergedProjectRecord["Name"];

            if (createWithoutSFDCProject) {
                dealData["GP_Deal_Category__c"] = "Without SFDC";
				 if(mergedProjectRecord["RecordTypeId"]=="CMITS")	
                {	
                     dealData["GP_No_Pricing_in_OMS__c"] = true;	
                }
            } else if (createInterCOEProject) {
                dealData["GP_Deal_Category__c"] = "Inter COE";
                dealData["GP_No_Pricing_in_OMS__c"] = true;
            } else {
                dealData["GP_Deal_Category__c"] = "Support";
            }

            dealData["GP_Account_Name_L4__c"] = (createWithoutSFDCProject || createInterCOEProject) ? mergedProjectRecord["GP_Customer_Hierarchy_L4__c"] : dealData["GP_Account_Name_L4__c"];

            saveProject = component.get("c.saveLegacyProject");
            saveProject.setParams({
                "strProjectRecord": JSON.stringify(mergedProjectRecord),
                "strDealRecord": JSON.stringify(dealData)
            });
        } else {
            saveProject = component.get("c.saveProject");
            saveProject.setParams({
                "strProjectRecord": JSON.stringify(mergedProjectRecord)
            });
        }
        this.showSpinner(component);
        saveProject.setCallback(this, function(response) {
            this.saveProjectHandler(component, response);
        });

        $A.enqueueAction(saveProject);
    },
    setSFDCIdToFieldsUsingOracleIds: function(component) {
        var dealData = this.isDealDataAvailable(component);
        var project = component.get("v.projectRecord") || {};
        if (dealData.GP_Portal_Name__c != null || dealData.GP_Portal_Name__c != '') {
            var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
            var listOfPortalOptions = mapOfPicklistValues.PortalMaster || [];
            for (var index = 0; index < listOfPortalOptions.length; index++) {
                if (listOfPortalOptions[index].externalParameter == dealData.GP_Portal_Name__c) {
                    project.GP_Portal_Name__c = listOfPortalOptions[index].text;
                    break;
                }
            }
            component.set("v.projectRecord", project);
        }
    },
    redirectToSubSection: function(component, activeSectionName) {
        var recordId = component.get("v.recordId");

        component.set("v.activeSectionName", activeSectionName);

        var listOfSectionName = [
            "Bussiness_Hierarchy",
            "Project_Info",
            "Project_Leadership",
            "Bill_To_And_Ship_To",
            "CMITS_Fields",
            "OtherClassification",
            "DispatchInfo",
            "GEProjectDetails",
            "OracleStatus",
            "Dashboard"
        ];

        for (var i = 0; i < listOfSectionName.length; i += 1) {
            var currentSideBarName = listOfSectionName[i] + '_sidebar';
            var currentSectionName = listOfSectionName[i];

            if (recordId) {
                currentSideBarName = recordId + '_' + currentSideBarName;
            } else {
                currentSideBarName = '_' + currentSideBarName;
            }

            var sideBar = document.getElementById(currentSideBarName);
            var sectionContent = component.find(currentSectionName);

            if (currentSectionName !== activeSectionName) {
                $A.util.removeClass(sideBar, 'active');
                $A.util.removeClass(sectionContent, 'slds-is-open');
            } else {
                $A.util.addClass(sideBar, 'active');
                $A.util.addClass(sectionContent, 'slds-is-open');
            }
        }
    },
    validateLegacyProject: function(component, projectRecord) {
        var isValid = true;
        var dealData = {};

        var listOfMandatoryFieldsForDeal;
        if (component.get("v.createSupportProject")) {
            listOfMandatoryFieldsForDeal = this.listOfMandatoryFieldsForDealForSupportProject;
        } else {
            listOfMandatoryFieldsForDeal = this.listOfMandatoryFieldsForDealForWithoutSFDCProject;
        }

        for (var i = 0; i < listOfMandatoryFieldsForDeal.length; i += 1) {
            var currentField = listOfMandatoryFieldsForDeal[i];
            var fieldElement = component.find(currentField);

            if (Object.prototype.toString.call(fieldElement) === '[object Array]') {
                fieldElement = fieldElement[0];
            }

            if ($A.util.isEmpty(projectRecord[currentField])) {
                isValid = false;

                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                if (fieldElement && fieldElement.getElement()) {
                    fieldElement.getElement().innerHTML = "Required";
                }
            } else {

                dealData[currentField] = projectRecord[currentField];

                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
                if (fieldElement && fieldElement.getElement()) {
                    fieldElement.getElement().innerHTML = "Required";
                }
            }
        }

        return {
            "isValid": isValid,
            "message": isValid ? 'SUCCESS' : 'Please fill the required fields in SFDC & Product Hierarchy section',
            "data": dealData
        }
    },
    getMergedRecord: function(originalProjectRecord, projectRecord, projectTemplate, activeSectionName) {

        var mergedProjectRecord = originalProjectRecord || {};
        for (var key in projectTemplate) {
            var fieldNameTree = key.split("___") || [];
            var fieldName = fieldNameTree[2];
            var sectionName = fieldNameTree[1];

            if (fieldNameTree[0] !== "Project" || sectionName !== activeSectionName) {
                continue;
            }

            mergedProjectRecord[fieldName] = projectRecord[fieldName];
        }
        if (activeSectionName === "Project_Info") {
            var listOfFieldsToBeCopied = [
                "GP_Timesheet_Requirement__c",
                "GP_Overtime_Billable__c",
                "GP_Holiday_Billable__c",
                "GP_T_M_With_Cap__c"
            ];
            listOfFieldsToBeCopied.forEach(function(fieldName) {
                mergedProjectRecord[fieldName] = projectRecord[fieldName];
            });
        } else if (activeSectionName === "Bussiness_Hierarchy") {
            mergedProjectRecord["GP_Product_Id__c"] = projectRecord["GP_Product_Id__c"];
            mergedProjectRecord["GP_Sub_Business_L3_Id__c"] = projectRecord["GP_Sub_Business_L3_Id__c"];
            mergedProjectRecord["GP_Business_Segment_L2_Id__c"] = projectRecord["GP_Business_Segment_L2_Id__c"];
        }
        return mergedProjectRecord;
    },
    getSanatizedData: function(data) {
        var sanatizedDealData = {};
        for (var key in data) {
            if (data.hasOwnProperty(key) &&
                key.indexOf('__r') === -1) {
                sanatizedDealData[key] = data[key];
            }

            var value = data[key];

            if (value && typeof value === 'string' &&
                (value.toLowerCase() === '--none--' ||
                    value.toLowerCase() === '--select--')) {
                sanatizedDealData[key] = '';
            }
        }
        return sanatizedDealData;
    },
    saveProjectHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var projectId = JSON.parse(responseData.response);
            this.showToast('Success', 'success', 'Information is successfully saved');
            if (!component.get("v.recordId")) {
                this.redirectToSobject(projectId);
            }
			var isServiceDelFlagOn = component.get("v.projectRecord.GP_Service_Deliver_in_US_Canada__c");
            var gpOraclePID = component.get("v.projectRecord.GP_Oracle_PID__c");
			
			this.GetLECodeAfterSaveProject(component,responseData);
			
           /* if(!isServiceDelFlagOn || gpOraclePID =='NA')//T7 Changes
            {
                this.GetLECodeAfterSaveProject(component,responseData);
            }
            else
            {
              component.set("v.isAvalaraEnable",true);
            }*/
            this.fireProjectSaveEvent(true);
            this.navigateToNextSection(component);
        } else {
            responseData = responseData || {};
            var responseMessage = responseData.message;
            if (responseMessage.indexOf("DUPLICATE_VALUE") >= 0) {
                var mapOfPossibleDuplicateFields = {
                    "GP_Unique_Project_Short_Name__c": "Name",
                    "GP_Unique_Project_Long_Name__c": "GP_Project_Long_Name__c",
                    "GP_Unique_Project_Description__c": "GP_Project_Description__c"
                };

                for (var uniqueFieldName in mapOfPossibleDuplicateFields) {
                    if (responseMessage.indexOf(uniqueFieldName) >= 0) {
                        var fieldName = mapOfPossibleDuplicateFields[uniqueFieldName];
                        var fieldDom = component.find(fieldName);

                        $A.util.removeClass(fieldDom, 'slds-hide');
                        $A.util.addClass(fieldDom, 'errorText');

                        fieldDom.getElement().innerHTML = 'Duplicate value';
                    }
                }

            } else {
                this.handleFailedCallbackOnRecordSave(component, responseMessage);
            }
        }
    },
    handleFailedCallbackOnRecordSave: function(component, responseMessage) {
        if (responseMessage.indexOf("FIELD_CUSTOM_VALIDATION_EXCEPTION") >= 0) {
            var errorList = responseMessage.split(":");
            var fieldName = errorList[errorList.length - 1].replace(/\W+/g, "");
            var message = errorList[1].substring(errorList[1].indexOf(",") + 2);

            var fieldElement = component.find(fieldName);
            if (fieldElement) {
                $A.util.removeClass(fieldElement, 'slds-hide');
                $A.util.addClass(fieldElement, 'errorText');
                fieldElement.getElement().innerHTML = message;
            } else {
                this.showToast('error', 'Error', message);
            }
        } else {
            var errorList = responseMessage.split(":");
            var fieldName = errorList[errorList.length - 1].replace(/\W+/g, "");
            var message = errorList[2];

            var fieldElement = component.find(fieldName);
            if (fieldElement) {
                $A.util.removeClass(fieldElement, 'slds-hide');
                $A.util.addClass(fieldElement, 'errorText');
                fieldElement.getElement().innerHTML = message;
            } else {
                this.showToast('error', 'Error', message);
            }
        }
    },
    /**
     * Validates Project Record against Project Template.
     * @param {Object} GP_Project__c - SFDC Project Record. 
     * @param {Object} GP_Project_Template__c - SFDC Project Template Record.
     * @description Validates Project Record against Project Template.
     */
    validateProjectData: function(component, projectRecord, projectTemplate, activeSectionName) {
        var response = {
            "isValid": true,
            "message": "SUCCESS"
        };

        this.mapOfSectionNameToFieldName = {};
        this.mapOfSectionNameToErrorCounter = {};
        this.mapOfFieldNameToSectionName = {};
        this.resetPicklistOptions(projectRecord);
        var isDummyCRN = this.isDummyCRNSelected(component, projectRecord);

        for (var key in projectTemplate) {
            var fieldNameTree = key.split("___") || [];
            var fieldElement;

            if (fieldNameTree[1] === "DispatchInfo" ||
                fieldNameTree[0] !== "Project") {
                continue;
            }

            this.mapOfSectionNameToFieldName[fieldNameTree[1]] = fieldNameTree[2];
            this.mapOfFieldNameToSectionName[fieldNameTree[2]] = fieldNameTree[1];

            var fieldName = fieldNameTree[2];
            var sectionName = fieldNameTree[1];
            if (activeSectionName === sectionName) {
                this.validateProjectField(component, projectRecord, fieldName, response, projectTemplate[key], sectionName, activeSectionName, isDummyCRN);
            }
        }
        this.customValidate(component, projectRecord, response, activeSectionName);

        return response;
    },
    isDummyCRNSelected: function(component, projectRecord) {
        var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
        var CRNNumber = mapOfPicklistValues.CRNNumber || [];
        for (var index = 0; index < CRNNumber.length; index++) {
            if (projectRecord.GP_CRN_Number__c == CRNNumber[index].text) {
                if (CRNNumber[index].externalParameter == 'Dummy')
                    return true;
                else
                    return false;
            }
        }
        if (projectRecord && projectRecord.GP_CRN_Number__r && projectRecord.GP_CRN_Number__r.GP_Type__c == 'Dummy') {
            return true;
        }
        return false;
    },
    customValidate: function(component, projectRecord, response, activeSectionName) {
        var fieldElement, sectionName, errorCounter;

        sectionName = this.mapOfFieldNameToSectionName['GP_T_M_With_Cap__c'];

        // projectRecord["GP_Project_type__c"] === 'Fixed monthly' ||
        //        projectRecord["GP_Project_type__c"] === 'Fixed Price' ||
        //        projectRecord["GP_Project_type__c"] === 'Transactions' ||
        //        (projectRecord["GP_Project_type__c"] === 'T&M' &&
        //         projectRecord["GP_T_M_With_Cap__c"]); 
        // then TCV is mandatory
        var listOfRecordType = component.get("v.listOfRecordType");
        var bpmRecordType = this.getSelectedRecordByName(listOfRecordType, 'BPM') || {};
        sectionName = this.mapOfFieldNameToSectionName['GP_TCV__c'];
        if (sectionName === activeSectionName &&
            this.isTCVMandatory(projectRecord, bpmRecordType["Id"]) &&
            (!projectRecord['GP_TCV__c'] || projectRecord['GP_TCV__c'] == 0)) {
            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_TCV__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = "Required.";
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['Name'];
        //Project Name length cannot be greater than 30 character.
        if (sectionName === activeSectionName &&
            (projectRecord['Name'] && projectRecord['Name'].length > 30)) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('Name');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                //this.showToast('Error', 'error', 'Project Name can not be greater than 30 characters.');
                fieldElement.getElement().innerHTML = 'Project Name can not be greater than 30 characters.';
            }
        } else if (projectRecord['Name'] &&
            projectRecord['Name'].indexOf($A.get("$Label.c.GP_BPM_Clone_Project_Name_Prefix")) >= 0) {

            response["isValid"] = false;
            fieldElement = component.find('Name');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = 'Please change Project Name.';
            }
        } else if (projectRecord['Name']) {
            fieldElement = component.find('Name');

            $A.util.removeClass(fieldElement, "errorText");
            $A.util.addClass(fieldElement, "slds-hide");

            fieldElement.getElement().innerHTML = "required";
        }

        // Project Long name length can not be greater than 240 character.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Project_Long_Name__c'] &&
            projectRecord['GP_Project_Long_Name__c'].length > 240) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Project_Long_Name__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = 'Project Long Name can not be greater than 240 characters.';
            }
        } else if (projectRecord['GP_Project_Long_Name__c'] &&
            projectRecord['GP_Project_Long_Name__c'].indexOf($A.get("$Label.c.GP_BPM_Clone_Project_Name_Prefix")) >= 0) {

            response["isValid"] = false;
            fieldElement = component.find('GP_Project_Long_Name__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = 'Please change Project Long Name.';
            }
        } else if (projectRecord['GP_Project_Long_Name__c']) {
            fieldElement = component.find('GP_Project_Long_Name__c');

            $A.util.removeClass(fieldElement, "errorText");
            $A.util.addClass(fieldElement, "slds-hide");

            fieldElement.getElement().innerHTML = "required";
        }

        // Project description length can not be greater than 250 character.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Project_Description__c'] &&
            projectRecord['GP_Project_Description__c'].length > 250) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Project_Description__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = 'Project Description can not be greater than 250 characters.';
            }
        } else if (projectRecord['GP_Project_Description__c']) {
            fieldElement = component.find('GP_Project_Description__c');

            $A.util.removeClass(fieldElement, "errorText");
            $A.util.addClass(fieldElement, "slds-hide");

            fieldElement.getElement().innerHTML = "required";
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_Overtime_Billable__c'];
        // Project GP_Project_type__c !=  'T&M' 
        // and GP_Project_type__c !=  'FTE' 
        // then GP_Overtime_Billable__c is not applicable.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Project_type__c'] !== 'T&M' &&
            projectRecord['GP_Project_type__c'] !== 'FTE' &&
            projectRecord['GP_Overtime_Billable__c']) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Overtime_Billable__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Overtime billable is only applicable for T&M and FTE projects.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_Timesheet_Requirement__c'];
        //Project GP_Project_type__c =  'T&M' then GP_Timesheet_Requirement__c is not applicable.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Project_type__c'] !== 'T&M' &&
            projectRecord['GP_Project_type__c'] !== 'FTE' &&
            Boolean(projectRecord['GP_Timesheet_Requirement__c']) &&
            projectRecord['GP_Timesheet_Requirement__c'] !== 'null') {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Timesheet_Requirement__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Timesheet requirement is only applicable for T&M and FTE projects.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_Bill_Rate_Type__c'];
        //Project GP_Project_type__c =  'T&M' then GP_Bill_Rate_Type__c is mandatory.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Project_type__c'] === 'T&M' &&
            (projectRecord['GP_Bill_Rate_Type__c'] === '--NONE--' ||
                projectRecord['GP_Bill_Rate_Type__c'] === 'null')) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Bill_Rate_Type__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Bill rate type is mandatory for T&M projects.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_GPM_Start_Date__c'];
        //GPM start date should be between project start and end date
        if (sectionName === activeSectionName &&
            projectRecord['GP_GPM_Start_Date__c'] &&
            !this.validDate(projectRecord['GP_GPM_Start_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_GPM_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (sectionName === activeSectionName &&
            projectRecord['GP_GPM_Start_Date__c'] &&
            !(projectRecord['GP_GPM_Start_Date__c'] >= projectRecord['GP_Start_Date__c'] &&
                projectRecord['GP_GPM_Start_Date__c'] <= projectRecord['GP_End_Date__c'])) {

            response["isValid"] = false;
            fieldElement = component.find('GP_GPM_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'GPM Allocation should be between project start and end date.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_End_Date__c'];
        //project start date should be less than end date
        if (sectionName === activeSectionName &&
            projectRecord['GP_End_Date__c'] &&
            !this.validDate(projectRecord['GP_End_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (sectionName === activeSectionName &&
            projectRecord['GP_Start_Date__c'] &&
            projectRecord['GP_End_Date__c'] &&
            projectRecord['GP_Start_Date__c'] > projectRecord['GP_End_Date__c']) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Project end date should be greater than start date.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_SOW_End_Date__c'];
        //SOW start date should be less than end date
        if (sectionName === activeSectionName &&
            projectRecord['GP_SOW_End_Date__c'] &&
            !this.validDate(projectRecord['GP_SOW_End_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_SOW_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (sectionName === activeSectionName &&
            projectRecord['GP_SOW_Start_Date__c'] &&
            projectRecord['GP_SOW_End_Date__c'] &&
            projectRecord['GP_SOW_Start_Date__c'] > projectRecord['GP_SOW_End_Date__c']) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_SOW_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'SOW End Date should be greater than SOW Start Date.';
            }
        }
		 //Krishna Kumar Validate SOW End Date should be equal to/less than Project End Date.
		 else if(projectRecord['GP_End_Date__c'] 
	         && projectRecord['GP_SOW_End_Date__c']
	         && projectRecord['GP_SOW_End_Date__c'] > projectRecord['GP_End_Date__c']) {
				
				errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
				errorCounter++;
				
				this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;
				
				response["isValid"] = false;
				fieldElement = component.find('GP_SOW_End_Date__c');
				if (fieldElement) {
				 $A.util.addClass(fieldElement, "errorText");
				 $A.util.removeClass(fieldElement, "slds-hide");
				 fieldElement.getElement().innerHTML = 'SOW End Date should be equal to/less than Project End Date.';
			}
		}
		
		// Avinash : Valueshare Changes Value Share -Outcome,Value Share-SLA OD
		// Gainshare Change : Validation
        //  START: Added Gainshare validation.       
        sectionName = this.mapOfFieldNameToSectionName['GP_Parent_PID_For_Gainshare__c'];
        if(sectionName === activeSectionName &&
           projectRecord['GP_Project_type__c'] &&
           (projectRecord['GP_Project_type__c'] === 'Gainshare'
           || projectRecord['GP_Project_type__c'] === 'Value Share -Outcome' 
        	|| projectRecord['GP_Project_type__c'] === 'Value Share-SLA OD') && 
           projectRecord['GP_Pure_Gainshare__c'] &&
           $A.util.isUndefinedOrNull(projectRecord['GP_Parent_PID_For_Gainshare__c'])) {
            
            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            var element = document.getElementById('GP_Parent_PID_For_Gainshare__c');
            //fieldElement = component.find('GP_Parent_PID_For_Gainshare__c');
            if (element) {
                $A.util.addClass(element, "errorText");
                $A.util.removeClass(element, "slds-hide");
                element.innerHTML = 'Parent PID is required for Gainshare/Valueshare project type.';
                //fieldElement.getElement().innerHTML = 'Parent PID is required for Gainshare project type.';
            }
        }
        // END:DS:04-03-19 :- Added Gainshare validation.
		
        sectionName = this.mapOfFieldNameToSectionName['GP_SOW_Start_Date__c'];
        if (sectionName === activeSectionName &&
            projectRecord['GP_SOW_Start_Date__c'] &&
            !this.validDate(projectRecord['GP_SOW_Start_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_SOW_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_Contract_End_Date__c'];
        //Contract start date should be less than end date
        if (sectionName === activeSectionName &&
            projectRecord['GP_Contract_End_Date__c'] &&
            !this.validDate(projectRecord['GP_Contract_End_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Contract_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (sectionName === activeSectionName &&
            projectRecord['GP_Contract_Start_Date__c'] &&
            projectRecord['GP_Contract_End_Date__c'] &&
            projectRecord['GP_Contract_Start_Date__c'] > projectRecord['GP_Contract_End_Date__c']) {

            errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Contract_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Contract end date should be greater than start date.';
            }
        }

        sectionName = this.mapOfFieldNameToSectionName['GP_Start_Date__c'];
        //validate contract start date format.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Contract_Start_Date__c'] &&
            !this.validDate(projectRecord['GP_Contract_Start_Date__c'])) {

            response["isValid"] = false;
            fieldElement = component.find('GP_Contract_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        }

        //validate contract end date format.
        if (sectionName === activeSectionName &&
            projectRecord['GP_Contract_End_Date__c'] &&
            !this.validDate(projectRecord['GP_Contract_End_Date__c'])) {

            response["isValid"] = false;
            fieldElement = component.find('GP_Contract_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        }

        var isGstApplicable = component.get("v.isGstApplicable");

        // if GST is applicable then HSN is a mandatory field
        if (activeSectionName === "OtherClassification" &&
            isGstApplicable && isGstApplicable !== "false" &&
            $A.util.isEmpty(projectRecord["GP_HSN_Category_Name__c"])) {
            response["isValid"] = false;

            fieldElement = component.find('GP_HSN_Category_Id__c');

            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Required.';
            }
        }

        // shift start date must be required if isShiftAllowed is true
        if (activeSectionName === "OtherClassification" &&
            projectRecord['GP_Shift_Allowed__c'] &&
            !projectRecord['GP_Shift_Start_Date__c']) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Shift_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Required';
            }
        }

        // Transition start date must be required if Transition Allowed is true
        if (activeSectionName === "OtherClassification" &&
            projectRecord['GP_Transition_Allowed__c'] &&
            !projectRecord['GP_Transition_End_Date__c']) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Transition_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Required';
            }
        }

        // shift start date must lie in between project start date and project end date
        if (activeSectionName === "OtherClassification" &&
            projectRecord['GP_Shift_Start_Date__c'] &&
            !this.validDate(projectRecord['GP_Shift_Start_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Shift_Start_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (activeSectionName === "OtherClassification" && projectRecord["GP_Shift_Start_Date__c"] &&
            projectRecord["GP_Shift_Allowed__c"] &&
            (projectRecord["GP_Shift_Start_Date__c"] < projectRecord["GP_Start_Date__c"] ||
                projectRecord["GP_Shift_Start_Date__c"] > projectRecord["GP_End_Date__c"])) {
            response["isValid"] = false;

            fieldElement = component.find('GP_Shift_Start_Date__c');

            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Shift Start Date must lie in between project duration.';
            }
        } else if (!projectRecord["GP_Shift_Allowed__c"]) {
            projectRecord["GP_Shift_Start_Date__c"] = null;
        }

        // transition end date must lie in between project start date and project end date
        if (activeSectionName === "OtherClassification" &&
            projectRecord['GP_Transition_End_Date__c'] &&
            !this.validDate(projectRecord['GP_Transition_End_Date__c'])) {

            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

            response["isValid"] = false;
            fieldElement = component.find('GP_Transition_End_Date__c');
            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Invalid date.';
            }
        } else if (activeSectionName === "OtherClassification" && projectRecord["GP_Transition_End_Date__c"] &&
            projectRecord["GP_Transition_Allowed__c"] &&
            (projectRecord["GP_Transition_End_Date__c"] < projectRecord["GP_Start_Date__c"] ||
                projectRecord["GP_Transition_End_Date__c"] > projectRecord["GP_End_Date__c"])) {
            response["isValid"] = false;

            fieldElement = component.find('GP_Transition_End_Date__c');

            if (fieldElement) {
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Transition End Date must lie in between project duration.';
            }
        } else if (!projectRecord["GP_Transition_Allowed__c"]) {
            projectRecord["GP_Transition_End_Date__c"] = null;
        }

        //validate if Us tax is true then all the fields under it are mandatory
        if (activeSectionName === "OtherClassification" &&
            projectRecord["GP_Service_Deliver_in_US_Canada__c"]) {
            var listOfFieldUnderUsTax = [
                "GP_State__c",
				"GP_Country__c",
                "GP_US_Address__c",
                "GP_City__c",
                "GP_Sub_Category_Of_Services__c",
                "GP_Tax_Exemption_Certificate_Available__c",
                "GP_Pincode__c"
            ];

            for (var i = 0; i < listOfFieldUnderUsTax.length; i += 1) {
                var fieldName = listOfFieldUnderUsTax[i];

                sectionName = this.mapOfFieldNameToSectionName[fieldName];

                if (sectionName === activeSectionName &&
                    $A.util.isEmpty(projectRecord[fieldName]) ||
                    projectRecord[fieldName] === '--NONE--' ||
                    projectRecord[fieldName] === 'null') {
                    errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
                    errorCounter++;

                    this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

                    response["isValid"] = false;
                    fieldElement = component.find(fieldName);
                    if (fieldElement) {
                        $A.util.addClass(fieldElement, "errorText");
                        $A.util.removeClass(fieldElement, "slds-hide");
                        fieldElement.getElement().innerHTML = 'Required';
                    }
                } else if (fieldName === "GP_City__c" && projectRecord["GP_City__r"] && projectRecord["GP_City__r"]["Name"]) {
                    projectRecord["GP_City__c"] = projectRecord["GP_City__r"]["Name"];
                    projectRecord["GP_City__r"] = {};
                }
            }


            if (projectRecord["GP_Tax_Exemption_Certificate_Available__c"] && $A.util.isEmpty(projectRecord["GP_Valid_Till__c"])) {

                response["isValid"] = false;
                fieldElement = component.find("GP_Valid_Till__c");
                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else if (!projectRecord["GP_Tax_Exemption_Certificate_Available__c"]) {
                projectRecord["GP_Valid_Till__c"] = null;
            }
        }
		
		 // <!--Avinash: GE Devine code changes -->
        if (activeSectionName === "OtherClassification" &&
            projectRecord["GP_Business_Group_L1__c"]=='GE' )     
        {
            
            var gpDev=component.find("GPDivine").get("v.value");
            var gpDevVal=component.get("v.projectRecord.GP_Divine__c");
            fieldElement = component.find("GP_Divine__c");
            if (fieldElement && component.find("GPDivine").get("v.value") =='') {
                response["isValid"] = false;    
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");
                fieldElement.getElement().innerHTML = 'Required';
            }
            else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }
        }

        //validate dispatch info
        if (activeSectionName === "DispatchInfo") {

            fieldElement = component.find("GP_Portal_Name__c");
            //validate if GP_MOD__c is Portal then Portal Name is mandatory
            if ((projectRecord["GP_MOD__c"] === 'Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy, FAX and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Hard Copy and Upload to Portal') &&
                (!projectRecord["GP_Portal_Name__c"] || projectRecord["GP_Portal_Name__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            //validate if GP_MOD__c is not null then Attn to name is mandatory
            fieldElement = component.find("GP_Attn_To_Name__c");
            if (projectRecord["GP_MOD__c"] &&
                (!projectRecord["GP_Attn_To_Name__c"] ||
                    projectRecord["GP_Attn_To_Name__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            fieldElement = component.find("GP_Attn_To_Email__c");

            //validate if GP_MOD__c is Email then Attn to email is mandatory
            if (projectRecord["GP_MOD__c"] &&
                (!projectRecord["GP_Attn_To_Email__c"] ||
                    projectRecord["GP_Attn_To_Email__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else if (projectRecord["GP_Attn_To_Email__c"] && projectRecord["GP_Attn_To_Email__c"].length > 250) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Attn to email cannot be greater than 250 character.";
                }
            } else if (projectRecord["GP_Attn_To_Email__c"] && !this.isValidEmails(projectRecord["GP_Attn_To_Email__c"])) {
                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Invalid Attn to email.";
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            fieldElement = component.find("GP_Customer_SPOC_Name__c");

            //validate if GP_MOD__c is Email then Attn to email is mandatory
            if (projectRecord["GP_MOD__c"] &&
                (!projectRecord["GP_Customer_SPOC_Name__c"] ||
                    projectRecord["GP_Customer_SPOC_Name__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            fieldElement = component.find("GP_Customer_SPOC_Email__c");
            //validate if GP_MOD__c is Email then Attn to email is mandatory

            if (projectRecord["GP_MOD__c"] &&
                (!projectRecord["GP_Customer_SPOC_Email__c"] ||
                    projectRecord["GP_Customer_SPOC_Email__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else if (projectRecord["GP_Customer_SPOC_Email__c"] &&
                !this.validateEmail(projectRecord["GP_Customer_SPOC_Email__c"])) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Invalid Email";
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            //validate if GP_MOD__c is Email then Attn to email is mandatory
            fieldElement = component.find('GP_Customer_Contact_Number__c');
            if ((projectRecord["GP_MOD__c"] === 'Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Email and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Hard Copy and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy, FAX and Upload to Portal') &&
                (!projectRecord["GP_Customer_Contact_Number__c"] ||
                    projectRecord["GP_Customer_Contact_Number__c"] === '--None--')) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else if ((projectRecord["GP_MOD__c"] === 'Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Email and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Hard Copy and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy, FAX and Upload to Portal') &&
                (projectRecord["GP_Customer_Contact_Number__c"] &&
                    !this.validateContact(projectRecord['GP_Customer_Contact_Number__c']))) {

                response["isValid"] = false;
                fieldElement = component.find('GP_Customer_Contact_Number__c');
                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Contact Number should be minimum 10 digit.';
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            fieldElement = component.find('GP_FAX_Number__c');
            //validate if GP_MOD__c is Email then Attn to email is mandatory
            if ((projectRecord["GP_MOD__c"] === 'Email and Fax' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy, FAX and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Fax' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Upload to Portal') &&
                (!projectRecord["GP_FAX_Number__c"] ||
                    projectRecord["GP_FAX_Number__c"] === '--None--')) {


                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Required';
                }
            } else if ((projectRecord["GP_MOD__c"] === 'Email and Fax' ||
                    projectRecord["GP_MOD__c"] === 'Email, Hard Copy, FAX and Upload to Portal' ||
                    projectRecord["GP_MOD__c"] === 'Fax' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Hard Copy' ||
                    projectRecord["GP_MOD__c"] === 'Fax and Upload to Portal') &&
                (projectRecord['GP_FAX_Number__c'] &&
                    !this.validateContact(projectRecord['GP_FAX_Number__c']))) {

                errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
                errorCounter++;

                this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;

                response["isValid"] = false;
                fieldElement = component.find('GP_FAX_Number__c');
                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");
                    fieldElement.getElement().innerHTML = 'Fax Number should be minimum 10 digit.';
                }
            } else {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }

            fieldElement = component.find('GP_CC1__c');
            if (projectRecord["GP_CC1__c"] && projectRecord["GP_CC1__c"].length > 150) {
                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Max Length is 150 character(" + projectRecord["GP_CC1__c"].length + ")";
                }
            } else if (projectRecord["GP_CC1__c"] && !this.isValidEmails(projectRecord["GP_CC1__c"])) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Invalid Email";
                }
            } else if (fieldElement) {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "";
            }

            fieldElement = component.find('GP_CC2__c');

            if (projectRecord["GP_CC2__c"] && projectRecord["GP_CC2__c"].length > 150) {
                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Max Length is 150 character(" + projectRecord["GP_CC2__c"].length + ")";
                }
            } else if (projectRecord["GP_CC2__c"] && !this.isValidEmails(projectRecord["GP_CC2__c"])) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Invalid Email";
                }
            } else if (fieldElement) {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "";
            }

            fieldElement = component.find('GP_CC3__c');

            if (projectRecord["GP_CC3__c"] && projectRecord["GP_CC3__c"].length > 150) {
                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Max Length is 150 character(" + projectRecord["GP_CC3__c"].length + ")";
                }
            } else if (projectRecord["GP_CC3__c"] && !this.isValidEmails(projectRecord["GP_CC3__c"])) {

                response["isValid"] = false;

                if (fieldElement) {
                    $A.util.addClass(fieldElement, "errorText");
                    $A.util.removeClass(fieldElement, "slds-hide");

                    fieldElement.getElement().innerHTML = "Invalid Email";
                }
            } else if (fieldElement) {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "";
            }
        }
    },
	// Gainshare change
    isTCVMandatory: function(projectRecord, bpmRecordTypeId) {
        return projectRecord["RecordTypeId"] != bpmRecordTypeId &&
            (projectRecord["GP_Project_type__c"] === 'Fixed monthly' ||
                projectRecord["GP_Project_type__c"] === 'Fixed Price' ||
                projectRecord["GP_Project_type__c"] === 'Transactions' ||
				projectRecord["GP_Project_type__c"] === 'Gainshare' ||
                (projectRecord["GP_Project_type__c"] === 'T&M' &&
                    projectRecord["GP_T_M_With_Cap__c"]));

    },
    isValidEmails: function(concatenatedEmails) {
        var listOfEmails = concatenatedEmails.split(",");
        for (var i = 0; i < listOfEmails.length; i++) {
            var email = listOfEmails[i] || "";
            email = email.trim();
            if (email && !this.validateEmail(email)) {
                return false;
            }
        }
        return true;
    },
    validateProjectField: function(component, projectRecord, fieldName, response, template, sectionName, activeSectionName, isDummyCRN) {

        var fieldElement;

        if (String(projectRecord[fieldName]).toLowerCase() === 'null') {
            projectRecord[fieldName] = null;
        }

        if (template.isRequired && (((fieldName == 'GP_Contract_Start_Date__c' || fieldName == 'GP_Contract_End_Date__c') && !isDummyCRN) ||
                (fieldName != 'GP_Contract_Start_Date__c' || fieldName != 'GP_Contract_End_Date__c')) &&
            ($A.util.isEmpty(projectRecord[fieldName]) ||
                String(projectRecord[fieldName]).toLowerCase() === '--select--' ||
                String(projectRecord[fieldName]).toLowerCase() === '--none--')) {
            var errorCounter = this.mapOfSectionNameToErrorCounter[sectionName] || 0;
            errorCounter++;
            this.mapOfSectionNameToErrorCounter[sectionName] = errorCounter;
            fieldElement = component.find(fieldName);

            if (fieldElement) {

                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "Required";
            }
            response["isValid"] = false;
            response["message"] += fieldName + ", ";
        } else {
            fieldElement = component.find(fieldName);

            if (fieldElement) {
                $A.util.removeClass(fieldElement, "errorText");
                $A.util.addClass(fieldElement, "slds-hide");
            }
        }

        if (template.type === "email" && projectRecord[fieldName] && !this.validateEmail(projectRecord[fieldName])) {

            fieldElement = component.find(fieldName);


            if (fieldElement) {

                response["isValid"] = false;
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "Invalid Email";
            }
        } else if (template.type === "date" && projectRecord[fieldName] && !this.validDate(projectRecord[fieldName])) {

            fieldElement = component.find(fieldName);


            if (fieldElement) {

                response["isValid"] = false;
                $A.util.addClass(fieldElement, "errorText");
                $A.util.removeClass(fieldElement, "slds-hide");

                fieldElement.getElement().innerHTML = "Invalid Date format";
            }
        }
    },
    resetPicklistOptions: function(projectRecord) {
        projectRecord.RecordType = null;
        projectRecord.GP_Deal__r = null;
        projectRecord.GP_Project_Template__r = null;
        for (var key in projectRecord) {
            if (projectRecord[key] === '--Select--' ||
                projectRecord[key] === '--None--' ||
                projectRecord[key] === 'null') {
                projectRecord[key] = null;
            }
        }
    },
    /**
     * Fetches Project Record Types.
     * @param {Aura.Component} component - this component
     * @description Fetches Project Record Types.
     */
    getProjectRecordTypeService: function(component) {
        var getRecordType = component.get("c.getRecordType");
        getRecordType.setParams({
            "sObjectName": "GP_Project__c"
        });

        getRecordType.setCallback(this, function(response) {

            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var listOfRecordType = JSON.parse(responseData.response);
                component.set("v.listOfRecordType", listOfRecordType);
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(getRecordType);
    },
    isDealDataAvailable: function(component) {
        var dealData = component.get("v.dealData");
        if (dealData) {
            return dealData;
        }
        return false;
    },
    /**
     * Filters A Record type for given record type name.
     * @param {Array} listOfRecordType
     * @param {String} selectedRecordTypeName
     * @description Filters A Record type for given record type name.
     */
    getSelectedRecordByName: function(listOfRecordType, selectedRecordTypeName) {
        for (var i = 0; i < listOfRecordType.length; i += 1) {
            if (listOfRecordType[i]["Name"] === selectedRecordTypeName) {
                return listOfRecordType[i];
            }
        }
    },
    /**
     * Filters A Record type for given record type Id.
     * @param {Array} listOfRecordType
     * @param {Id} selectedRecordTypeId
     * @description Filters A Record type for given record type Id.
     */
    getSelectedRecordById: function(listOfRecordType, selectedRecordTypeId) {
        for (var i = 0; i < listOfRecordType.length; i += 1) {
            if (listOfRecordType[i]["Id"] === selectedRecordTypeId) {
                return listOfRecordType[i];
            }
        }
    },
    /**
     * Fetches Default Project Data to b Auto populated from its parent deal.
     * @param {Aura.Component} component - this component
     * @description Fetches Default Project Data to b Auto populated from its parent deal.
     */
    getDefaultOpportunityData: function(component) {
        var projectRecord = component.get("v.projectRecord");
        var dealData = component.get("v.dealData");

        if ($A.util.isEmpty(dealData) || $A.util.isEmpty(dealData.Id)) {
            return;
        }

        var getDefaultOpportunityData = component.get("c.getDefaultProjectData");

        getDefaultOpportunityData.setParams({
            "dealId": dealData["Id"]
        });

        getDefaultOpportunityData.setCallback(this, function(response) {

            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.setDefaultProjectData(component, responseData);
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(getDefaultOpportunityData);
    },
    /**
     * Auto-Populate Data on project record based on parent deal record.
     * @param {Aura.Component} component - this component
     * @return {AuraResponse} Response From  getDefaultProjectData Promise
     * @description Auto-Populate Data on project record based on parent deal record.
     */
    setDefaultProjectData: function(component, responseData) {
		
        var dealData = JSON.parse(responseData.response) || {};
        var projectRecord = component.get("v.projectRecord") || {};

        var opportunityName = dealData && dealData.GP_Opportunity_ID__r ? dealData.GP_Opportunity_ID__r.Name : '';
        //set default project data.
        projectRecord.Name = opportunityName;

        //Added by Amit PPT on 11-11-17 for displaying the Opp Name on Project
        projectRecord.GP_Deal__c = dealData.Id;
        projectRecord.GP_Opportunity_Name__c = dealData.GP_Opportunity_Name__c;
        projectRecord.GP_Customer_Hierarchy_L4__c = dealData.GP_Account_Name_L4__c;
        projectRecord.GP_Customer_Hierarchy_L4__r = dealData.GP_Account_Name_L4__c != null ? dealData.GP_Account_Name_L4__r : '';

        projectRecord.GP_Business_Group_L1__c = dealData.GP_Business_Group_L1__c;
        projectRecord.GP_Business_Segment_L2__c = dealData.GP_Business_Segment_L2__c;
        projectRecord.GP_Sub_Business_L3__c = dealData.GP_Sub_Business_L3__c;

        projectRecord.GP_Delivery_Org__c = dealData.GP_Delivery_Org__c;
        projectRecord.GP_Sub_Delivery_Org__c = dealData.GP_Sub_Delivery_Org__c;

        projectRecord.GP_Vertical__c = dealData.GP_Vertical__c;
        projectRecord.GP_Sub_Vertical__c = dealData.GP_Sub_Vertical__c;
        projectRecord.GP_Product_Family__c = dealData.GP_Product_Family__c;
        projectRecord.GP_Nature_of_Work__c = dealData.GP_Nature_of_Work__c;

        projectRecord.GP_Revenue_Description__c = dealData.GP_Revenue_Description__c;
        projectRecord.GP_OMS_Deal_ID__c = dealData.GP_OMS_Deal_ID__c;

        projectRecord.GP_Project_Long_Name__c = opportunityName;
        projectRecord.GP_Project_Description__c = opportunityName;
        projectRecord.GP_Start_Date__c = dealData.GP_Start_Date__c;
        projectRecord.GP_End_Date__c = dealData.GP_End_Date__c;

        if (!projectRecord.GP_CC1__c) {
            projectRecord.GP_CC1__c = this.DEFAULT_EMAIL;
        }

        component.set("v.projectRecord", projectRecord);

        this.getverticalSubVertical(component, projectRecord.GP_Customer_Hierarchy_L4__c);
    },
    /**
     * Fetches Account Record for a given Account Id
     * @param {Aura.Component} component - this component
     * @param {Id} Id - Account Id
     * @description Fetches Account Record for a given Account Id.
     */
    getBusinessHierarchyData: function(component, accountID) {
        var getAccountRecord = component.get("c.getAccountRecord");
        getAccountRecord.setParams({
            "accountID": accountID
        });

        getAccountRecord.setCallback(this, function(response) {

            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.updateBusinessHierarchy(component, responseData);
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(getAccountRecord);
    },
    /**
     * updates Business Hierarchy Field for the Selected Account
     * @param {Aura.Component} component - this component
     * @param {GPAuraResponse} GPAuraResponse - Account Record
     * @description updates Business Hierarchy Field for the Selected Account.
     */
    updateBusinessHierarchy: function(component, responseData) {
        var businessHierarchyData = JSON.parse(responseData.response);
        var projectRecord = component.get("v.projectRecord");

        projectRecord.GP_Business_Group_L1__c = businessHierarchyData.Business_Group__c;
        projectRecord.GP_Business_Segment_L2__c = businessHierarchyData.Business_Segment__r ? businessHierarchyData.Business_Segment__r.Name : '';
        projectRecord.GP_Sub_Business_L3__c = businessHierarchyData.Sub_Business__r ? businessHierarchyData.Sub_Business__r.Name : '';
        projectRecord.GP_Customer_Hierarchy_L4__r = businessHierarchyData;

        component.set("v.projectRecord", projectRecord);
    },
    /**
     * Clears Business Hierarchy Field When BusinessHeirarchy is unselected
     * @param {Aura.Component} component - this component
     * @description Clears Business Hierarchy Field When BusinessHeirarchy is unselected.
     */
    clearBusinessHierarchy: function(component) {
        var projectRecord = component.get("v.projectRecord");

        projectRecord.GP_Business_Group_L1__c = null;
        projectRecord.GP_Business_Segment_L2__c = null;
        projectRecord.GP_Sub_Business_L3__c = null;
        // Gainshare change: M&A PID changes.
        //projectRecord.GP_Account_Name_L4__c = null;        
        projectRecord.GP_Customer_Hierarchy_L4__c = null;

        projectRecord.GP_Vertical__c = null;
        projectRecord.GP_Sub_Vertical__c = null;
        projectRecord.GP_Service_Line__c = null;
        projectRecord.GP_Nature_of_Work__c = null;
        projectRecord.GP_Product_Id__c = null;
        projectRecord.GP_Product__c = null;

        component.set("v.listOfAvailableServiceLine", null);
        component.set("v.projectRecord", projectRecord);
    },
    populateCurrency: function(component, operatingUnitID) {
        var getOUCurrency = component.get("c.getOUCurrency");
        getOUCurrency.setParams({
            "operatingUnitID": operatingUnitID
        });

        getOUCurrency.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.updateCurrency(component, responseData);
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(getOUCurrency);
    },
    updateCurrency: function(component, responseData) {
        var operatingUnit = JSON.parse(responseData.response);

        component.set("v.projectRecord.GP_Project_Currency__c", operatingUnit.GP_Currency__c);
        component.set("v.projectRecord.GP_Billing_Currency__c", operatingUnit.GP_Currency__c);
    },
    getRelatedProjectsUnderDeal: function(component, dealData) {
        if ($A.util.isEmpty(dealData) || (dealData.GP_Business_Name__c !== null && dealData.GP_Business_Name__c !== 'BPM')) {
            component.set("v.allowProjectCreationUnderDeal", true);
            return;
        }

        var getProjectsForDeal = component.get("c.getProjectsForDeal");
        getProjectsForDeal.setParams({
            "dealId": dealData.Id
        });
        this.showSpinner(component);
        getProjectsForDeal.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var listOfProjectsUnderDeal = JSON.parse(responseData.response);
                
                if (listOfProjectsUnderDeal && listOfProjectsUnderDeal.length > 0) {

                    listOfProjectsUnderDeal.unshift({
                        "Name": "Select an approved PID",
                        "Id": null
                    });

                    component.set("v.listOfProjectsUnderDeal", listOfProjectsUnderDeal);
                    component.set("v.allowProjectCreationUnderDeal", true);
                }
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });
        $A.enqueueAction(getProjectsForDeal);
    },
    updateListOfParentProjectForInvoice: function(component) {
        var projectRecord = component.get("v.projectRecord");

        component.set("v.projectRecord.GP_Parent_Project_for_Group_Invoice__c", null);
        this.getParentProjectForInvoice(component, projectRecord);
    },
    resetParentProjectForInvoice: function(component) {
        var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
        mapOfPicklistValues.listOfParentProjectForGroupInvoiceOptions = [{
            "label": "--NONE--"
        }];

        component.set("v.mapOfPicklistValues", mapOfPicklistValues);
        //this.getProjectTypePicklist(component);
    },
    getParentProjectForInvoice: function(component, projectRecord) {

        var getParentProjectForGroupInvoice = component.get("c.getParentProjectForGroupInvoice");
        getParentProjectForGroupInvoice.setParams({
            "serializedProject": JSON.stringify(projectRecord)
        });
        this.showSpinner(component);

        getParentProjectForGroupInvoice.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);

            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var listOfParentProjectForGroupInvoiceOptions = JSON.parse(responseData.response) || [];

                listOfParentProjectForGroupInvoiceOptions.unshift({
                    "label": "--NONE--"
                });
                var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
                mapOfPicklistValues.listOfParentProjectForGroupInvoiceOptions = listOfParentProjectForGroupInvoiceOptions;

                component.set("v.mapOfPicklistValues", mapOfPicklistValues);
                //this.getProjectTypePicklist(component);
            } else {
                // this.handleFailedCallback(component, responseData);
            }
        });
        $A.enqueueAction(getParentProjectForGroupInvoice);
    },
    resetProjectUnderDeal: function(component) {
        component.set("v.listOfProjectsUnderDeal", null);
        component.set("v.createNewProjectUnderDeal", false);
        component.set("v.allowProjectCreationUnderDeal", true);
        component.set("v.selectedProjectCreationOptionUnderDeal", null);
    },
    cloneProject: function(component, selectedProjectCreationOptionUnderDeal) {
        var dealData = component.get("v.dealData") || {};
        var cloneBPMProject = component.get("c.cloneBPMProject");
        cloneBPMProject.setParams({
            "projectId": selectedProjectCreationOptionUnderDeal,
            "dealId" : dealData.Id
        });
        var cloneLoaderDom = component.find("cloneLoader");

        $A.util.removeClass(cloneLoaderDom, "slds-hide");

        cloneBPMProject.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            $A.util.addClass(cloneLoaderDom, "slds-hide");
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.redirectToSobject(responseData.response, "detail");
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });
        $A.enqueueAction(cloneBPMProject);
    },
    getRelatedCRNData: function(component, CRNNumber) {
        if (CRNNumber && CRNNumber !== 'null') {
            var getCRNData = component.get("c.getCRNData");

            getCRNData.setParams({
                "CRNNumberId": CRNNumber
            });
            getCRNData.setCallback(this, function(response) {
                var responseData = response.getReturnValue() || {};
                this.hideSpinner(component);
                if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                    var CRNData = JSON.parse(responseData.response) || {};
                    component.set("v.projectRecord.GP_Contract_Start_Date__c", CRNData.GP_START_DATE__c);
                    component.set("v.projectRecord.GP_CRN_Status__c", CRNData.GP_Status__c);
                    component.set("v.projectRecord.GP_Contract_End_Date__c", CRNData.GP_END_DATE__c);
                } else {
                    this.handleFailedCallback(component, responseData);
                }
            });
            $A.enqueueAction(getCRNData);
        } else {
            component.set("v.projectRecord.GP_Contract_Start_Date__c", null);
            component.set("v.projectRecord.GP_CRN_Status__c", null);
            component.set("v.projectRecord.GP_Contract_End_Date__c", null);
        }
    },
    getverticalSubVertical: function(component, accountId) {
        var getVerticalSubVerticalForAccount = component.get("c.getVerticalSubVerticalForAccount");
        this.showSpinner(component);

        getVerticalSubVerticalForAccount.setParams({
            "accountID": accountId
        });

        getVerticalSubVerticalForAccount.setCallback(this, function(response) {
            this.verticalSubVerticalChangeHandler(component, response);
        });

        $A.enqueueAction(getVerticalSubVerticalForAccount);
    },
    verticalSubVerticalChangeHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setverticalSubVerticalData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setverticalSubVerticalData: function(component, responseData) {
        debugger;
        var relatedAccountData = JSON.parse(responseData.response) || {};

        component.set("v.projectRecord.GP_Business_Group_L1__c", relatedAccountData.accountData.Business_Group__c);
        component.set("v.projectRecord.GP_Business_Segment_L2__c", relatedAccountData.accountData.Business_Segment__r.Name);
        component.set("v.projectRecord.GP_Business_Segment_L2_Id__c", relatedAccountData.accountData.Business_Segment__c);

        component.set("v.projectRecord.GP_Sub_Business_L3__c", relatedAccountData.accountData.Sub_Business__r.Name);
        component.set("v.projectRecord.GP_Sub_Business_L3_Id__c", relatedAccountData.accountData.Sub_Business__c);

        component.set("v.projectRecord.GP_Vertical__c", relatedAccountData.accountData.Industry_Vertical__c);
        component.set("v.projectRecord.GP_Sub_Vertical__c", relatedAccountData.accountData.Sub_Industry_Vertical__c);
        component.set("v.listOfAvailableServiceLine", relatedAccountData.setOfServiceLine);
        component.set("v.mapOfServiceLineToNatureOfWork", relatedAccountData.mapOfServiceLineToNatureOfWork);
        component.set("v.mapOfNatureOfWorkToProduct", relatedAccountData.mapOfNatureOfWorkToProduct);

        var selectedServiceLine = component.get("v.projectRecord.GP_Service_Line__c");
        var listOfAvailableNatureOfWork = relatedAccountData.mapOfServiceLineToNatureOfWork[selectedServiceLine] || [];
        component.set("v.listOfAvailableNatureOfWork", this.sort(listOfAvailableNatureOfWork, true));



        var selectedNatureOfWork = component.get("v.projectRecord.GP_Nature_of_Work__c");
        var listOfAvailableProducts = relatedAccountData.mapOfNatureOfWorkToProduct[selectedServiceLine + '--' + selectedNatureOfWork] || [];
        component.set("v.listOfAvailableProducts", this.sort(listOfAvailableProducts));
    },
    getDependentPicklistData: function(component) {
        var getDealPicklist = component.get("c.getDealPicklist");

        this.showSpinner(component);

        getDealPicklist.setCallback(this, function(response) {
            this.getPicklistHandler(component, response);
        });
        $A.enqueueAction(getDealPicklist);
    },
    getPicklistHandler: function(component, response) {

        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setPicklistData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setPicklistData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response) || {};
        component.set("v.listOfAvailableServiceLine", this.sort(responseJson.setOfServiceLine, true));
        component.set("v.mapOfServiceLineToNatureOfWork", responseJson.mapOfServiceLineToNatureOfWork);
        component.set("v.mapOfNatureOfWorkToProduct", responseJson.mapOfNatureOfWorkToProduct);
        component.set("v.mapOfBusinessTypeToBusinessName", responseJson.mapOfBusinessTypeToBusinessName);
        component.set("v.listOfAvailableBusinessType", this.sort(responseJson.setOfBusinessType, true));

        var mapOfDeliveryToSubDelivery;

        if (responseJson.deliverySubDeliveryMapping && responseJson.deliverySubDeliveryMapping.GP_Delivery_Sub_Delivery_Mapping__c) {
            mapOfDeliveryToSubDelivery = JSON.parse(responseJson.deliverySubDeliveryMapping.GP_Delivery_Sub_Delivery_Mapping__c)
        } else {
            mapOfDeliveryToSubDelivery = {};
        }

        var listOfAvailableDelivery = [];
        for (var key in mapOfDeliveryToSubDelivery) {
            listOfAvailableDelivery.push(key);
        }
        component.set("v.mapOfDeliveryToSubDelivery", mapOfDeliveryToSubDelivery);
        component.set("v.listOfAvailableDelivery", listOfAvailableDelivery);
    },
    getFilteredSOD: function(component) {
        var isLegacyProject = component.get("v.createWithoutSFDCProject");
        var projectRecord = component.get("v.projectRecord") || {};
        var isInterCOEProject = component.get("v.createInterCOEProject");
        var recordType;

        if (projectRecord.RecordType) {
            recordType = projectRecord.RecordType.Name;
        }

        /*if (!isLegacyProject && recordType === 'Indirect PID') {
            isLegacyProject = true;
        }*/

        this.showSpinner(component);

        var getFilteredSOD = component.get("c.getFilteredSOD");
        getFilteredSOD.setParams({
            "isLegacyProject": isLegacyProject,
            "isInterCOEProject": isInterCOEProject
        });

        getFilteredSOD.setCallback(this, function(response) {
            this.getFilteredSODHandler(component, response);
        });

        $A.enqueueAction(getFilteredSOD);
    },
    getFilteredSODHandler: function(component, response) {

        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var listOfFilteredSDO = JSON.parse(responseData.response) || [];

            component.set("v.listOfFilteredSDO", this.sortAndAddDefault(listOfFilteredSDO));
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    navigateToNextSection: function(component) {
        var project = component.get("v.project") || [];
        var recordTypeName;

        if (project.length > 0 && project[0] && project[0]["recordTypeInfo"] && project[0]["recordTypeInfo"]["name"]) {
            recordTypeName = project[0]["recordTypeInfo"]["name"];
        }

        var listOfSectionName = [
            "Bussiness_Hierarchy",
            "Project_Info",
            "Bill_To_And_Ship_To",
            "Project_Leadership",
            "CMITS_Fields",
            "OtherClassification",
            "DispatchInfo",
            "Dashboard"
        ];

        var projectRecordTypeName = component.get("v.projectRecord.RecordType.Name");
        var recordId = component.get("v.recordId");
        var activeSectionName = component.get("v.activeSectionName") || "Bussiness_Hierarchy";
        var currentSectionIndex = -2;
        var sideBar;

        for (var i = 0; i < listOfSectionName.length; i += 1) {
            if (activeSectionName === listOfSectionName[i]) {
                currentSectionIndex = i;
            }

            if (recordId) {
                sideBar = document.getElementById(recordId + '_' + listOfSectionName[i] + '_sidebar'); //component.find(currentSideBarName); //
            } else {
                sideBar = document.getElementById('_' + listOfSectionName[i] + '_sidebar'); //component.find(currentSideBarName); //
            }

            if (!sideBar) {
                currentSectionIndex++;
                continue;
            }

            var sectionContent = component.find(listOfSectionName[i]);

            if (i === currentSectionIndex + 1) {
                activeSectionName = listOfSectionName[i];
                component.set("v.activeSectionName", activeSectionName);
                $A.util.addClass(sideBar, 'active');
                $A.util.addClass(sectionContent, 'slds-is-open');
            } else {
                $A.util.removeClass(sideBar, 'active');
                $A.util.removeClass(sectionContent, 'slds-is-open');
            }
        }
    },
    updateParentProjectInvoiceList: function(component) {
        var projectRecord = component.get("v.projectRecord");

        var getParentProjectInvoice = component.get("c.getParentProjectInvoice");

        var project = {
            "GP_Operating_Unit__c": projectRecord.GP_Operating_Unit__c,
            "Id": projectRecord.Id
        };

        getParentProjectInvoice.setParams({
            "serializedProject": JSON.stringify(project)
        });
        getParentProjectInvoice.setCallback(this, function(response) {
            this.getParentProjectInvoiceHandler(component, response);
        });

        $A.enqueueAction(getParentProjectInvoice);
    },
    getParentProjectInvoiceHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var listOfParentProjectForGroupInvoiceOptions = JSON.parse(responseData.response) || [];
            listOfParentProjectForGroupInvoiceOptions = this.sortAndAddDefault(listOfParentProjectForGroupInvoiceOptions);
            var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
            mapOfPicklistValues.listOfParentProjectForGroupInvoiceOptions = listOfParentProjectForGroupInvoiceOptions;

            component.set("v.mapOfPicklistValues", mapOfPicklistValues);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    getOperatingUnitRecord: function(operatingUnitID, listOfOperatingUnit) {
        var operatingUnit;

        for (var i = 0; i < listOfOperatingUnit.length; i += 1) {
            if (listOfOperatingUnit[i]["text"] === operatingUnitID) {
                operatingUnit = listOfOperatingUnit[i];
                break;
            }
        }

        return operatingUnit;
    },
    getRecordTypeId: function(component) {
        var listOfRecordType = component.get("v.listOfRecordType");
        var recordTypeName = this.getRecordTypeName(component);
        var recordType = this.getSelectedRecordByName(listOfRecordType, recordTypeName);

        return recordType.Id;
    },
    getRecordTypeName: function(component) {
        var businessType = component.get("v.projectRecord.GP_Business_Type__c");
        var businessName = component.get("v.projectRecord.GP_Business_Name__c");
        var createSupportProject = component.get("v.createSupportProject");
        var recordTypeName;

        if (createSupportProject) {
            recordTypeName = "Indirect PID";
        } else {
            if (businessType === 'PBB') {
                if (businessName === 'CMITS') {
                    recordTypeName = 'CMITS';
                } else {
                    recordTypeName = "OTHER PBB";
                }
            } else if (businessType === 'BPM') {
                recordTypeName = 'BPM';
            }
        }

        return recordTypeName;
    },
    getPickList: function(component) {
        var projectRecord = component.get("v.projectRecord");
        var dealData = component.get("v.dealData");

        var getPickList = component.get("c.getPickList");

        getPickList.setParams({
            "serializedProjectData": JSON.stringify(projectRecord),
            "serializedDealData": JSON.stringify(dealData)
        });
        this.showSpinner(component);

        getPickList.setCallback(this, function(response) {
            this.getPickListHandler(component, response);
        });
        $A.enqueueAction(getPickList);
    },
    getPickListHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJSON = JSON.parse(responseData.response) || {};
            var mapOfNewPicklistValues = responseJSON.mapOfFieldNameToListOfOptions;
            var mapOfServiceLineToListOfSubCategory = responseJSON.mapOfServiceLineToListOfSubCategory;

            var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
            for (var key in mapOfNewPicklistValues) {
                var listOfOptions = mapOfNewPicklistValues[key];

                this.sortAndAddDefault(listOfOptions);
                mapOfPicklistValues[key] = listOfOptions;
            }

            component.set("v.mapOfPicklistValues", mapOfPicklistValues);
            component.set("v.mapOfServiceLineToListOfSubCategory", mapOfServiceLineToListOfSubCategory);

            if (responseJSON.mapOfMappingForDealData != null) {
                if (responseJSON.mapOfMappingForDealData.golEmployee) {
                    component.set("v.projectRecord.GP_GOL__c", responseJSON.mapOfMappingForDealData.golEmployee.Id);
                    component.set("v.projectRecord.GP_GOL__r", responseJSON.mapOfMappingForDealData.golEmployee);
                    var golDom = component.find("GOL");
                    if(golDom)
                    	golDom.initMethod();
                }
                if (responseJSON.mapOfMappingForDealData.grmEmployee) {
                    component.set("v.projectRecord.GP_GRM__c", responseJSON.mapOfMappingForDealData.grmEmployee.Id);
                    component.set("v.projectRecord.GP_GRM__r", responseJSON.mapOfMappingForDealData.grmEmployee);
                    var grmDom = component.find("GRM");
                    if(grmDom)
                    	grmDom.initMethod();
                }
                if (responseJSON.mapOfMappingForDealData.pinnacleMaster) {
                    component.set("v.projectRecord.GP_MOD__c", responseJSON.mapOfMappingForDealData.pinnacleMaster.Name);
                }
            }

            var projectRecord = component.get("v.projectRecord") || {};

            var selectedServiceLine = projectRecord.GP_Service_Line__c;

            if (selectedServiceLine) {
                this.updateListOfSubCategory(component, selectedServiceLine);
            }

            if (!projectRecord.GP_Portal_Name__c) {
                this.setSFDCIdToFieldsUsingOracleIds(component);
            }
            //this.getProjectTypePicklist(component);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    navigateToStage2WithoutDeal: function(component) {
        var validationMessage = this.validateStage1(component);

        if (!validationMessage.isSuccess) {
            this.showToast('Warning', 'warning', validationMessage.message);
            return;
        }

        var projectRecord = component.get("v.projectRecord") || {};

        if (!projectRecord.GP_CC1__c) {
            projectRecord.GP_CC1__c = this.DEFAULT_EMAIL;
        }

        component.set("v.projectRecord", projectRecord);

        var selectedSDO = component.get("v.projectRecord.GP_Primary_SDO__c");
        var createSupportProject = component.get("v.createSupportProject");

        if (!createSupportProject) {
            component.set("v.l4FilterCondition", this.L4_FILTER_CONDITION);
        }

        this.getDependentPicklistData(component);
        this.getPickList(component);
        this.getProjectTemplate(component, selectedSDO, createSupportProject);
        this.resetCustomer(component);
        this.getProjectTypePicklist(component);

        if (createSupportProject) {
            this.setDefaultOUForIndirectProject(component);
        }

        component.set("v.stage", 2);
    },
    resetCustomer: function(component) {
        var accountLookup = component.find("CustomerHierarchy");
        if (!$A.util.isEmpty(accountLookup)) {
            accountLookup.ClearLookupControl();
        }
    },
    navigateToStage2WithDeal: function(component) {
        var selectedProjectCreationOptionUnderDeal = component.get("v.selectedProjectCreationOptionUnderDeal");
        var selectedSDO = component.get("v.projectRecord.GP_Primary_SDO__c");
        var primarySDODom = component.find("PrimarySDO_required");

        if (selectedProjectCreationOptionUnderDeal) {
            this.cloneProject(component, selectedProjectCreationOptionUnderDeal);
        } else if ($A.util.isEmpty(selectedSDO)) {
            $A.util.removeClass(primarySDODom, "slds-hide");
            return;
        } else {
            $A.util.addClass(primarySDODom, "slds-hide");
            this.setProjectDataFromDeal(component);
            this.getProjectTemplate(component, selectedSDO, false);
            this.getPickList(component);

            component.set("v.stage", 2);
        }
    },
    getProjectTypePicklist: function(component) {
        var supportValues = $A.get("$Label.c.GP_Support_Project_Type_Values");
        var billableValues = $A.get("$Label.c.GP_Billable_Project_Type_Values");
        var support_array = [{
            "label": '--None--'
        }];
        supportValues.split(',').forEach(function(value) {
            var support = {
                "text": value,
                "label": value
            }
            support_array.push(support);
        });
        var billable_array = [{
            "label": '--None--'
        }];
        billableValues.split(',').forEach(function(value) {
            var billable = {
                "text": value,
                "label": value
            }
            billable_array.push(billable);
        });
        var createSupportProject = component.get("v.createSupportProject");
        var projectRecord = component.get("v.projectRecord");

        if (projectRecord && projectRecord.RecordType && projectRecord.RecordType.Name === 'Indirect PID') {
            component.set("v.listOfProjectTypes", support_array);
        } else if (projectRecord && projectRecord.RecordType && !createSupportProject) {
            component.set("v.listOfProjectTypes", billable_array);
        } else if (createSupportProject) {
            component.set("v.listOfProjectTypes", support_array);
        } else {
            component.set("v.listOfProjectTypes", billable_array);
        }
    },
    changeOfBillRate: function(component, isInitialized) {
        var projectType = component.get("v.projectRecord.GP_Project_type__c");
        if (!(projectType === 'FTE' || projectType === 'T&M')) {
            if (!isInitialized) {
                component.set("v.projectRecord.GP_Bill_Rate_Type__c", 'Hourly');
            }
            component.set("v.projectTemplate.Project___Project_Info___GP_Bill_Rate_Type__c.isReadOnly", true);
        } else {
            if (!isInitialized) {
                component.set("v.projectRecord.GP_Bill_Rate_Type__c", '');
            }
            component.set("v.projectTemplate.Project___Project_Info___GP_Bill_Rate_Type__c.isReadOnly", false);
        }
    },
    updateWorkingHour: function(component) {
        var projectType = component.get("v.projectRecord.GP_Project_type__c");
        var billRateType = component.get("v.projectRecord.GP_Bill_Rate_Type__c");

        if ((projectType === 'FTE' || projectType === 'T&M') && billRateType === "Daily") {
            component.set("v.projectTemplate.Project___OtherClassification___GP_Daily_Normal_Hours__c.isVisible", true);
            component.set("v.projectTemplate.Project___OtherClassification___GP_Daily_Normal_Hours__c.label", "Daily Normal Hour");
            component.set("v.mapOfPicklistValues.listOfDailyNormalHoursOptions", this.LIST_OF_DAILY_NORMAL_HOUR);
			 //Commented for hours selection issue 
            //component.set("v.projectRecord.GP_Daily_Normal_Hours__c", 8);
        } else if ((projectType === 'FTE' || projectType === 'T&M') && billRateType === "Weekly") {
            component.set("v.projectTemplate.Project___OtherClassification___GP_Daily_Normal_Hours__c.isVisible", true);
            component.set("v.projectTemplate.Project___OtherClassification___GP_Daily_Normal_Hours__c.label", "Weekly Normal Hour");
            component.set("v.mapOfPicklistValues.listOfDailyNormalHoursOptions", this.LIST_OF_WEEKLY_NORMAL_HOUR);
			 //Commented for hours selection issue 
            //component.set("v.projectRecord.GP_Daily_Normal_Hours__c", 40);
        } else {
            component.set("v.projectTemplate.Project___OtherClassification___GP_Daily_Normal_Hours__c.isVisible", false);
            component.set("v.projectRecord.GP_Daily_Normal_Hours__c", null);
        }
    },
    operatingUnitChangeHandler: function(component) {
        var projectRecord = component.get("v.projectRecord") || {};
        var operatingUnitID = projectRecord.GP_Operating_Unit__c;
        var listOfOperatingUnit = component.get("v.mapOfPicklistValues.listOfOperatingUnit") || [];

        if (!$A.util.isEmpty(operatingUnitID)) {
            this.populateCurrency(component, operatingUnitID);
            this.updateListOfParentProjectForInvoice(component);
        } else {
            component.set("v.projectRecord.GP_Project_Currency__c", null);
            component.set("v.projectRecord.GP_Billing_Currency__c", null);
        }

        this.updateIsGSTApplicable(component);

        var hsnCategoryId = projectRecord.GP_HSN_Category_Id__c;
        var hsnCategoryName = projectRecord.GP_HSN_Category_Name__c;
        var selectedHSN = hsnCategoryId + "-" + hsnCategoryName;

        component.set("v.selectedHSN", selectedHSN);
    },
    // ECR-HSN Changes
    updateIsGSTApplicable: function(component) {
    	debugger;
        var projectRecord = component.get("v.projectRecord") || {};
        var operatingUnitID = projectRecord.GP_Operating_Unit__c;
        var listOfOperatingUnit = component.get("v.mapOfPicklistValues.listOfOperatingUnit") || [];
		var operatingUnit = this.getOperatingUnitRecord(operatingUnitID, listOfOperatingUnit) || {};

        var operatingUnitOracleID = operatingUnit["externalParameter"];
        var isGstApplicable = operatingUnit["externalParameter2"];
		if(operatingUnit["externalParameter2"]==='false'){
        component.set("v.isGstApplicableTaxCodeVisible",true);
        }
        
        // DS-02-03-19.
        // If HSN category is defined, then GST has to be enabled,
        // even if OU is non Indian because tagged Work location
        // can be of India location.
        
        component.set("v.hsnMapPE.isGstByOU", isGstApplicable);
        // Is GST applicable or not.
        if(projectRecord.GP_HSN_Category_Id__c) {
            component.set("v.isGstApplicable", true);
            component.set("v.hsnMapPE.isGstApplicable", true);
        } else {
            component.set("v.isGstApplicable", isGstApplicable);
            component.set("v.hsnMapPE.isGstApplicable", isGstApplicable);
        }
    },
    getSDOdata: function(component) {
        var selectedSDO = this.getSelectedSDORecord(component) || {};

        if (selectedSDO && selectedSDO["text"]) {

            var getSDOData = component.get("c.getSDOData");

            this.showSpinner(component);

            getSDOData.setParams({
                "sdoId": selectedSDO["text"]
            });

            getSDOData.setCallback(this, function(response) {
                this.getSDOdataHandler(component, response);
            });

            $A.enqueueAction(getSDOData);
        } else {
            var projectRecord = component.get("v.projectRecord") || {};

            projectRecord.GP_Business_Type__c = 'Indirect';
            projectRecord.GP_Business_Name__c = 'Indirect';

            component.set("v.projectRecord", projectRecord);
        }
    },
    getSDOdataHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var sdo = JSON.parse(responseData.response);
            var projectRecord = component.get("v.projectRecord") || {};

            projectRecord.GP_Business_Type__c = sdo.GP_Business_Type__c;
            projectRecord.GP_Business_Name__c = sdo.GP_Business_Name__c;

            component.set("v.projectRecord", projectRecord);
        }
    },
    resetDependentFieldsForProjectType: function(component) {
        var projectRecord = component.get("v.projectRecord") || {};

        if (projectRecord.GP_Project_type__c !== "T&M" &&
            projectRecord.GP_Project_type__c !== "FTE") {
            projectRecord.GP_Timesheet_Requirement__c = null;
        } else if (projectRecord.GP_Project_type__c !== "T&M") {
            projectRecord.GP_T_M_With_Cap__c = false;
            projectRecord.GP_Overtime_Billable__c = false;
            projectRecord.GP_Holiday_Billable__c = false;

            component.set("v.projectRecord", projectRecord);
        }
    },
    resetRelatedRecordForMod: function(component) {
        var projectRecord = component.get("v.projectRecord") || {};

        if (projectRecord.GP_MOD__c !== "Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Email and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Email, Hard Copy and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Email, Hard Copy, FAX and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Fax and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Hard Copy and Upload to Portal") {
            projectRecord.GP_Portal_Name__c = null;
        }


        if (projectRecord.GP_MOD__c !== "Hard Copy" &&
            projectRecord.GP_MOD__c !== "Email and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Email, Hard Copy and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Email, Hard Copy, FAX and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Fax and Hard Copy" &&
            projectRecord.GP_MOD__c !== "Hard Copy and Upload to Portal") {
            projectRecord.GP_Customer_Contact_Number__c = null;
        }

        if (projectRecord.GP_MOD__c !== "Email and Fax" &&
            projectRecord.GP_MOD__c !== "Email, Hard Copy, FAX and Upload to Portal" &&
            projectRecord.GP_MOD__c !== "Fax" &&
            projectRecord.GP_MOD__c !== "Fax and Hard Copy" &&
            projectRecord.GP_MOD__c !== "Fax and Upload to Portal") {
            projectRecord.GP_FAX_Number__c = null;
        }

        component.set("v.projectRecord", projectRecord);
    },
    setProductId: function(component) {
        var selectedProductName = component.get("v.projectRecord.GP_Product__c");
        var listOfProductOptions = component.get("v.listOfAvailableProducts") || [];
        var selectedProductId = "";

        for (var i = 0; i < listOfProductOptions.length; i += 1) {
            if (listOfProductOptions[i]["label"] === selectedProductName) {
                selectedProductId = listOfProductOptions[i]["text"];
                break;
            }
        }

        component.set("v.projectRecord.GP_Product_Id__c", selectedProductId);
    },
    projectLeadershipSaveHandler: function(component) {
        component.set('v.activeSectionName', 'Project_Leadership');
        this.navigateToNextSection(component);
    },
    fireProjectInitializedEvent: function(projectRecord, projectTemplate) {
        var projectInitializedEvent = $A.get("e.c:GPEvntProjectInitialized");
        projectInitializedEvent.fire({
            "projectRecord": projectRecord,
            "projectTemplate": projectTemplate
        });
    },
    isValidSDOSelected: function(component) {
        var cloneFromExistingProject = component.get("v.cloneFromExistingProject");
        var selectedProjectCreationOptionUnderDeal = component.get("v.selectedProjectCreationOptionUnderDeal");
        var dealBusinessName = component.get("v.dealData.GP_Business_Name__c");
        var selectedSDO = this.getSelectedSDORecord(component);

        var isValid, message;
        var requiredDOM;

        if (cloneFromExistingProject) {
            requiredDOM = component.find("ParentProject_required");
            isValid = Boolean(selectedProjectCreationOptionUnderDeal);
            if (!isValid) {
                message = "Please select an approved PID";
            }
        } else {
            requiredDOM = component.find("PrimarySDO_required");
            isValid = (selectedSDO &&
                (dealBusinessName !== 'BPM' ||
                    selectedSDO.externalParameter === 'BPM'));
            if (!isValid) {
                message = selectedSDO ? "Please select a BPM SDO" : 'Required';
            }
        }

        if (!isValid) {
            $A.util.addClass(requiredDOM, 'errorText');
            $A.util.removeClass(requiredDOM, 'slds-hide');

            requiredDOM.getElement().innerHTML = message;
        } else {
            $A.util.removeClass(requiredDOM, 'errorText');
            $A.util.addClass(requiredDOM, 'slds-hide');
        }
        return isValid;
    },
    setDefaultOUForIndirectProject: function(component) {
        var getDefaultOUForIndirectProject = component.get("c.getDefaultOUForIndirectProject");

        this.showSpinner(component);

        getDefaultOUForIndirectProject.setCallback(this, function(response) {
            this.getDefaultOUForIndirectProjectHandler(component, response);
        });

        $A.enqueueAction(getDefaultOUForIndirectProject);
    },
    getDefaultOUForIndirectProjectHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var defaultOUForIndirectProject = JSON.parse(responseData.response);
            component.set("v.projectRecord.GP_Operating_Unit__c", defaultOUForIndirectProject.Id);
            this.operatingUnitChangeHandler(component);
        }
    },
    updateProjectCategoryOnProjectTypeChange: function(component) {
        var projectRecord = component.get("v.projectRecord");
        var projectType = component.get("v.projectRecord.GP_Project_type__c");

        var baseOption = {
            "text": "BASE",
            "label": "BASE",
            "externalParameter2": null,
            "externalParameter": null
        };
        var listOfProjectCategory = component.get("v.mapOfPicklistValues.listOfProjectCategory") || [];
        if (listOfProjectCategory && listOfProjectCategory.length > 0) {
            if (projectType === 'TTITO') {
                listOfProjectCategory.push(baseOption);
                component.set("v.mapOfPicklistValues.listOfProjectCategory", listOfProjectCategory);

                component.set("v.projectRecord.GP_Project_Category__c", "BASE");
                component.set("v.projectTemplate.Project___Project_Info___GP_Project_Category__c.isReadOnly", true);
            } else {
                for (var key = 0; key < listOfProjectCategory.length; key++) {
                    if (listOfProjectCategory[key]["text"] == "BASE")
                        listOfProjectCategory.splice(key, 1);
                }
                component.set("v.mapOfPicklistValues.listOfProjectCategory", listOfProjectCategory);
                component.set("v.projectTemplate.Project___Project_Info___GP_Project_Category__c.isReadOnly", false);
            }
        }
    },
    updateListOfSubCategory: function(component, selectedServiceLine) {

        var mapOfServiceLineToListOfSubCategory = component.get("v.mapOfServiceLineToListOfSubCategory") || {};
        var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};

        var listOfSubCategoryOptions = mapOfServiceLineToListOfSubCategory[selectedServiceLine];

        this.sortAndAddDefault(listOfSubCategoryOptions);
        mapOfPicklistValues["listOfSubCategoryOfServices"] = listOfSubCategoryOptions;

        component.set("v.mapOfPicklistValues", mapOfPicklistValues);
    },
    
    natureOfWorkChangeHelper : function(component) {
        var selectedNatureOfWork = component.get("v.projectRecord.GP_Nature_of_Work__c");
        var mapOfNatureOfWorkToProduct = component.get("v.mapOfNatureOfWorkToProduct") || {};

        //Added
        var selectedServiceLine = component.get("v.projectRecord.GP_Service_Line__c");

        var listOfAvailableProducts = component.get("v.listOfAvailableProducts");

        if (listOfAvailableProducts && listOfAvailableProducts.length > 0) {
            //reset child records
            component.set("v.projectRecord.GP_Product__c", null);
            component.set("v.projectRecord.GP_Product_Id__c", null);
        }

        listOfAvailableProducts = mapOfNatureOfWorkToProduct[selectedServiceLine + '--' + selectedNatureOfWork] || [];
        component.set("v.listOfAvailableProducts", this.sort(listOfAvailableProducts));
    },
    // ECR-HSN Changes
    setListOfGSTOUsAndHSUs : function(component) {
        
    	debugger;
        var listOfOperatingUnits = component.get("v.mapOfPicklistValues.listOfOperatingUnit") || [];
        var projectRecord = component.get("v.projectRecord") || {};
        var listOfHSNs = component.get("v.mapOfPicklistValues.listOfHSN") || [];
        var tempOUList = [];
        var billableValues = $A.get("$Label.c.GP_Billable_Project_Type_Values");
        
        for(var i=0; i<listOfOperatingUnits.length; i++) {
            if(listOfOperatingUnits[i]['externalParameter2'] === 'true') {
                tempOUList.push(listOfOperatingUnits[i]);
            }            
        }
        
        // ECR-HSN Changes
        // HSN is only for new and non support PIDs.
       if(projectRecord && ((projectRecord.GP_Project_Type__c &&
           billableValues.indexOf(projectRecord.GP_Project_Type__c) != -1) ||
           projectRecord.RecordType.Name !== 'Indirect PID')) {
             
            component.set("v.hsnMapPE.listOfHSNs", listOfHSNs);
            component.set("v.hsnMapPE.isGSTNeeded", true);
        } else {
            component.set("v.hsnMapPE.listOfHSNs", null);
            component.set("v.hsnMapPE.isGSTNeeded", false);
        }
        
        component.set("v.listOfGSTEnabledOUs", tempOUList);
    },
	//Avinash - Avalara
    getBillToAddress : function(component,event,depnedentFieldMapHelp)
    {
       
         var billToAddr = component.get("c.getBillToAddressForProject");    
			var pid=component.get("v.recordId");
            this.showSpinner(component);
            billToAddr.setParams({
                "pId": pid
            });
            billToAddr.setCallback(this, function(response) {
                var responseData = response.getReturnValue() || {};
                this.hideSpinner(component);
               
                if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                    var responseJSON = JSON.parse(responseData.response) || {};
                    var listOfProjectAddress = responseJSON.listOfProjectAddress || [];         
                   
                    //console.log('BilltoAddre'+listOfProjectAddress[0].GP_Bill_To_Address__r.GP_Address__c);   
                    var addressLast = listOfProjectAddress.length-1;
                    if(!($A.util.isEmpty(listOfProjectAddress) 
                         && (listOfProjectAddress.length> 0 )
                         && !($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c)
                             && (listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c ==="US" 
                              || listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c ==="CA"
                              || listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c ==="US" 
                              || listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c ==="CA" 
                                )
                            )
                        ))
                    {
                        var gpUSAddress="";
                        if( (listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c ==="US" 
                              || listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c ==="CA")) 
                        {  
                        
                        
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_1__c))){
                            gpUSAddress = listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_1__c;}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_2__c))){
                            gpUSAddress = gpUSAddress +" " + listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_2__c;}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_3__c))){
                            gpUSAddress = gpUSAddress +" " + listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Address_Line_3__c;}
                        
                        component.set("v.projectRecord.GP_US_Address__c",gpUSAddress); 
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_City__c))){
                            component.set("v.projectRecord.GP_City__c",listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_City__c);
                            component.set("v.projectRecord.GP_City__r.Name",listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_City__c);}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Postal_Code__c))){                     
                            component.set("v.projectRecord.GP_Pincode__c",listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Postal_Code__c);}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_State__c))){
                            component.set("v.projectRecord.GP_State__c",listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_State__c); }
                            if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c))){
                                if(listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c=='CA')
                                {
                                    component.set("v.projectRecord.GP_Country__c",'Canada'); 
                                    var canadaCountryMap=component.get("v.CanadaCountry");
                                    var cndState = canadaCountryMap[listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_State__c];
                                    component.set("v.projectRecord.GP_State__c",cndState); 
                                }
                                else{
                                    component.set("v.projectRecord.GP_Country__c",listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_Country__c); 
                                }
                            }
                     //component.set("v.readonlyAddress",false);
                     //
                     //
                        }
                        else
                        {
                            if( (listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c ==="US" 
                              || listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c ==="CA")) 
                        {  
                        
                        
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_1__c))){
                            gpUSAddress = listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_1__c;}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_2__c))){
                            gpUSAddress = gpUSAddress +" " + listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_2__c;}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_3__c))){
                            gpUSAddress = gpUSAddress +" " + listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Address_Line_3__c;}
                        
                        component.set("v.projectRecord.GP_US_Address__c",gpUSAddress); 
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_City__c))){
                            component.set("v.projectRecord.GP_City__c",listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_City__c);
                            component.set("v.projectRecord.GP_City__r.Name",listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_City__c);}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Postal_Code__c))){                     
                            component.set("v.projectRecord.GP_Pincode__c",listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Postal_Code__c);}
                        if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_State__c))){
                            component.set("v.projectRecord.GP_State__c",listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_State__c); }
                            if(!($A.util.isEmpty(listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c))){
                                
                                if(listOfProjectAddress[0].GP_Ship_To_Address__r.GP_Country__c=='CA')
                                {
                                    component.set("v.projectRecord.GP_Country__c",'Canada');
                                    var canadaCountryMap=component.get("v.CanadaCountry");
                                    var cndState = canadaCountryMap[listOfProjectAddress[addressLast].GP_Bill_To_Address__r.GP_State__c];
                                    component.set("v.projectRecord.GP_State__c",cndState); 
                   
                                }
                                else{
                                    component.set("v.projectRecord.GP_Country__c",listOfProjectAddress[addressLast].GP_Ship_To_Address__r.GP_Country__c); 
                                }
                            }
                     //component.set("v.readonlyAddress",false);
                     //
                     //
                        }
                        }
                        
                        var controllerValueKeyHelp;
                        var stValHelp=component.get("v.projectRecord.GP_Country__c");
                        if(!$A.util.isUndefinedOrNull(stValHelp))
                        {
                            if(stValHelp=='CA')
                            {
                                stValHelp='Canada';
                            }
                            controllerValueKeyHelp=stValHelp;
                        }
                        if (controllerValueKeyHelp != '--None--') {
                            var ListOfDependentFields = depnedentFieldMapHelp[controllerValueKeyHelp];
                            
                            if( (!$A.util.isUndefinedOrNull(ListOfDependentFields))&& (ListOfDependentFields.length > 0)){                               
                                this.fetchDepValues(component, ListOfDependentFields);    
                            }
                            
                        }
                        
                    }
                }
            });
        
          $A.enqueueAction(billToAddr);
            
    },
    fetchPicklistValues: function(component,objDetails,controllerField, dependentField) {
        //console.log('===controllerField==='+controllerField);
        // call the server side function  
        var action = component.get("c.getDependentMap");
        // pass paramerters [object definition , contrller field name ,dependent field name] -
        // to server side function 
        action.setParams({
            'objDetail' : objDetails,
            'contrfieldApiName': controllerField,
            'depfieldApiName': dependentField 
        });
        //set callback   
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                //store the return response from server (map<string,List<string>>)  
                var StoreResponse = response.getReturnValue();
                
                // once set #StoreResponse to depnedentFieldMap attribute 
                component.set("v.depnedentFieldMap",StoreResponse);
                
                // create a empty array for store map keys(@@--->which is controller picklist values) 
                var listOfkeys = []; // for store all map keys (controller picklist values)
                var ControllerField = []; // for store controller picklist value to set on lightning:select. 
                
                // play a for loop on Return map 
                // and fill the all map key on listOfkeys variable.
                for (var singlekey in StoreResponse) {
                    listOfkeys.push(singlekey);
                }
                
                //set the controller field value for lightning:select
                if (listOfkeys != undefined && listOfkeys.length > 0) {
                    ControllerField.push('--None--');
                }
                
                for (var i = 0; i < listOfkeys.length; i++) {
                    ControllerField.push(listOfkeys[i]);
                }  
                // set the ControllerField variable values to country(controller picklist field)
                component.set("v.listControllingValues", ControllerField);
            }else{
                alert('Something went wrong..');
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchDepValues: function(component, ListOfDependentFields) {
        // create a empty array var for store dependent picklist values for controller field  
        
        var dependentFields = [];
        dependentFields.push({value:'--None--',key:'None'});
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            var stateValue= ListOfDependentFields[i];
            var stateAPI = stateValue.match(/\((.*)\)/);
            if(!$A.util.isUndefinedOrNull(stateAPI)){
            dependentFields.push({value:stateValue, key:stateAPI[1]});
            }
            //console.log("ListOfDependentFields=="+ListOfDependentFields[i]);
        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        component.set("v.listDependingValues", dependentFields);
        /* var dependentFields = new Map();
        dependentFields.set('none','--None--');
        for (var i = 0; i < ListOfDependentFields.length; i++) {
            var stateValue= ListOfDependentFields[i];
            var stateAPI = stateValue.match(/\((.*)\)/);
            if(!$A.util.isUndefinedOrNull(stateAPI)){
            dependentFields.set(stateAPI[1],stateValue);
            }
            
        }
        // set the dependentFields variable values to store(dependent picklist field) on lightning:select
        console.log("ListOfDependentFields=="+dependentFields);
        component.set("v.listDependingValues", dependentFields);
        */
        
    },
    //Avinash - Avalara
    GetLECodeAfterSaveProject:function(component,responseData)
    {     
            var responseJson='';
            if(!$A.util.isUndefinedOrNull(responseData.response))
            {
                responseJson = JSON.parse(responseData.response);
            }
            else
            {
                responseJson=responseData;           
            }
          var getLECodeAction=component.get("c.getOperatingUnitLECode");
            getLECodeAction.setParams({
                "pId":responseJson
            });
            getLECodeAction.setCallback(this,function(response){
                 var res=response.getReturnValue();
                    console.log('===LE Code After Save Project ==='+res);
                  this.CheckIfAvalaraEnabled(component,res);
            });
        $A.enqueueAction(getLECodeAction);           
    },
    
    
    //Avinash - Avalara
    CheckIfAvalaraEnabled:function(component,responseData)
    {        
          
        var LeCode='';
        if(!$A.util.isUndefinedOrNull(responseData.response))
        {
            var responseJson = JSON.parse(responseData.response);
            var projectRecord = responseJson.Project;
            var originalProjectRecord = JSON.parse(JSON.stringify(projectRecord));
            //var LeCode2 = projectRecord.GP_Operating_Unit__r["GP_LE_Code__c"];
            LeCode = projectRecord.GP_Operating_Unit__r.GP_LE_Code__c; 
        }
        else
        {
            LeCode=responseData;
        }
        
        console.log('==LeCode=='+LeCode);
        if(!$A.util.isUndefinedOrNull(LeCode)){
            var action = component.get("c.CheckAvalaraLECodeExists");
            action.setParams({
                "lecode":LeCode
            });
            action.setCallback(this, function(response) {
                var res=response.getReturnValue();
                console.log('===Avalara Enable Flag ==='+res);
                if(res==='Y'){ 
                    component.set("v.isAvalaraEnable",true);
                    //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);                 
                    
                }
                else
                {
                    
                   // component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",false);
                  //------------------------------------------------------------ 
                   /* component.set("v.projectRecord.GP_US_Address__c","");   
                    component.set("v.projectRecord.GP_City__c","");
                    component.set("v.projectRecord.GP_City__r.Name","");
                    component.set("v.projectRecord.GP_Pincode__c",""); 
                    component.set("v.projectRecord.GP_State__c",""); 
                    component.set("v.projectRecord.GP_Country__c",""); */
                    //this.SetServiceDeliveryInCanadaFlag(component,false);
                    
                    //16 May deployment changes
                    var isSerDel=component.get("v.projectRecord.GP_Service_Deliver_in_US_Canada__c");
                    if(!isSerDel)
                    {
                     this.SetServiceDeliveryInCanadaFlag(component,false);
                    }
				/*	if(component.get("v.isEditable")){
                    this.SetServiceDeliveryInCanadaFlag(component,false);
                    } */
                }
            });
            $A.enqueueAction(action);
        }
    },
    SetServiceDeliveryInCanadaFlag : function(component, serviceDeliveryFlag)
    {
        		var recordId = component.get("v.recordId");
         		var action = component.get("c.SetServiceDeliveryInCanadaFlag");
                action.setParams({
                    "ProjectId":recordId,
                    "serviceDeliveryFlag":serviceDeliveryFlag
                });                
               
                $A.enqueueAction(action);
    },
	SetPOFlag : function(component, POFlag) {
        var recordId = component.get("v.recordId");
        var action = component.get("c.SetPOFlag");
        action.setParams({
            "ProjectId":recordId,
            "POFlag":POFlag
        });        
        $A.enqueueAction(action);
    },
    // Gainshare Change
    getParentPIDFilterAndInvoiceFrequencyList : function(component, projectRecord) {
        debugger;
        
        var salesOppId = component.get("v.projectRecord.GP_Sales_Opportunity_Id__c");
        var customerHierarchyL4 = component.get("v.projectRecord.GP_Customer_Hierarchy_L4__c");
        var crnNumberICON = component.get("v.projectRecord.GP_CRN_Number__c");
        var getInvoiceFrequencyOptions = component.get("v.mapOfPicklistValues.listOfInvoiceFrequencyOptions") || [];
        var gainshareProjectFilterCondition = '';
        var finalInvoiceFrequencyList = [];
        
       // Avinash : Valueshare Changes.
        if(projectRecord["GP_Project_type__c"] === 'Gainshare' 
        || projectRecord["GP_Project_type__c"] === 'Value Share -Outcome' 
        || projectRecord["GP_Project_type__c"] === 'Value Share-SLA OD') {
            
             // Avinash : Valueshare Changes.
            gainshareProjectFilterCondition += ' GP_Project_type__c != \'Gainshare\' AND GP_Project_type__c != \'Value Share -Outcome\' AND GP_Project_type__c != \'Value Share-SLA OD\'';
            gainshareProjectFilterCondition += ' AND GP_Approval_Status__c = \'Approved\' AND GP_Oracle_Status__c = \'S\' AND Recordtype.Name != \'Indirect PID\'';
            // Gainshare Changes: M&A PID changes. CRN not to be Genpact Dummy.
            if(salesOppId) {
                gainshareProjectFilterCondition += ' AND GP_Sales_Opportunity_Id__c = \'' +salesOppId+ '\'';
            } else if(crnNumberICON && crnNumberICON !== $A.get("$Label.c.GP_Genpact_Dummy_CRN_For_M_A_Module")) {
                gainshareProjectFilterCondition += ' AND GP_CRN_Number__c = \'' +crnNumberICON+ '\'';
            } else if(customerHierarchyL4) {
                gainshareProjectFilterCondition += ' AND GP_Customer_Hierarchy_L4__c = \'' +customerHierarchyL4+ '\'';
            }
            
            for(var i=0; i<getInvoiceFrequencyOptions.length; i++) {
                if(getInvoiceFrequencyOptions[i].text === 'Bi-Monthly') {
                    continue;
                }
                finalInvoiceFrequencyList.push(getInvoiceFrequencyOptions[i]);
            }
        }
        if(finalInvoiceFrequencyList.length > 0) {
        	component.set('v.getInvoiceFrequencyOptions', finalInvoiceFrequencyList);
            // If Project Type = Gainshare then Invoice Frequency can't be Bi-Monthly
            if(projectRecord["GP_Invoice_Frequency__c"] === 'Bi-Monthly') {
                component.set("v.projectRecord.GP_Invoice_Frequency__c", null);
            }
        } else {
            component.set('v.getInvoiceFrequencyOptions', getInvoiceFrequencyOptions);
        }
        component.set("v.gainshareProjectFilterCondition",gainshareProjectFilterCondition);
    },
    // Gainshare Change
    gainshareFieldsHandler : function(component, projectRecord) {
        // Gainshare Change
        if((component.get("v.createWithoutSFDCProject") || projectRecord.GP_Deal_Category__c === 'Without SFDC')
            //(projectRecord.GP_Oracle_PID__c === 'NA' && projectRecord.GP_Deal_Category__c === 'Without SFDC'))
           && projectRecord.GP_Pure_Gainshare__c) {
            this.resetGainshareFieldsHandler(component, ['GP_Pure_Gainshare__c', 'GP_Parent_PID_For_Gainshare__c']);
            this.getParentPIDFilterAndInvoiceFrequencyList(component, projectRecord);
            component.set("v.projectRecord.GP_Pure_Gainshare__c", true);
        }
    }, 
    // Gainshare Change
    // Reset the Gainshare fields
    resetGainshareFieldsHandler : function(component, fieldNames) {
        for(var field in fieldNames) {
            if(fieldNames[field] === 'GP_Pure_Gainshare__c') {
                component.set("v.projectRecord.GP_Pure_Gainshare__c", false);
            }
            if(fieldNames[field] === 'GP_Parent_PID_For_Gainshare__c') {
                component.set("v.projectRecord.GP_Parent_PID_For_Gainshare__c", null);
            }            
        }
    },
    // Gainshare Change
    getCRNDataOnAccountChangeHandler : function(component, accountId) {
        var getIconData = component.get("c.getCRNDataPerCustomerHierarachy");
        getIconData.setParams({
            "accountId": accountId
        });
        getIconData.setCallback(this, function(response) {
            this.hideSpinner(component);
            var responseData = response.getReturnValue() || {};
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var mapOfPicklistValues = component.get("v.mapOfPicklistValues") || {};
                mapOfPicklistValues["CRNNumber"] = JSON.parse(responseData.response);
                component.set("v.mapOfPicklistValues", mapOfPicklistValues);
                component.set("v.projectRecord.GP_CRN_Number__c", null);
                component.set("v.projectRecord.GP_CRN_Status__c", null);
                component.set("v.projectRecord.GP_Contract_Start_Date__c", null);
                component.set("v.projectRecord.GP_Contract_End_Date__c", null);
            } else {
                component.set("v.errorMessage", responseData.message);
            }
        });
        $A.enqueueAction(getIconData);
    },
	// Avinash : Valueshare changes. Value Share -Outcome,Value Share-SLA OD
    // If Sales SFDC then project type readonly for valueshare.
    // NOT NEEDED: If M&A PID and Oracle PID exist then project type readonly for valueshare.
    handleValueshare : function(component, projectType, OraclePID, dealCategory) {
        var dealData = component.get("v.dealData");
        if(dealData && !projectType) {
            projectType = dealData.GP_Project_Type__c;
        }
        if(dealData && !dealCategory) {
            dealCategory = dealData.GP_Deal_Category__c;
        }
        // Actually Project Type gets readonly after approval.
        // (dealCategory === 'Sales SFDC' || (OraclePID !== 'NA' && dealCategory === 'Without SFDC'))
        if(dealCategory === 'Sales SFDC'
        && (projectType === 'Gainshare' 
        || projectType === 'Value Share -Outcome' 
        || projectType === 'Value Share-SLA OD')) {
            component.set("v.projectRecord.GP_Project_Type__c", projectType);
            //var projectTemplate = component.get("v.projectTemplate");
            component.set('v.projectTemplate.Project___Project_Info___GP_Project_type__c.isReadOnly',true);
            //component.set("v.projectTemplate",projectTemplate);
        } else if(dealCategory === 'Sales SFDC') {
            var listOfProjectTypes = component.get("v.listOfProjectTypes") || [];
            var tempList = [];
            for(var i=0; i < listOfProjectTypes.length; i++) {
                if(listOfProjectTypes[i]['label'] !== 'Value Share -Outcome' 
                   && listOfProjectTypes[i]['label'] !== 'Value Share-SLA OD'
				    && listOfProjectTypes[i]['label'] !== 'Gainshare') {
                    tempList.push(listOfProjectTypes[i]);
                }
            }
            component.set("v.listOfProjectTypes", tempList);
        }
    }
})