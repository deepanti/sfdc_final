({
	 getOperators : function(component){
        component.set("v.operatorList", ["equals", "not equal to",
                                      	"less than", "greater than",
                                      	"less or equal", "greater or equal",
                                      	"contains", "does not contain",
                                      	"starts with"]);
    },
    
    getProductFields : function(component){
        var action = component.get("c.getProductFields");
        action.setCallback(this, function(response){
            component.set("v.productFields", response.getReturnValue());
        });
        $A.enqueueAction(action);
    }

})