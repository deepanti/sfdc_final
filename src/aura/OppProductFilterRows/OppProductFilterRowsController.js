({
	init : function(component, event, helper) {
        var filter = 
          {
            "fieldName": "",
            "operator": "",
            "value":""
          }
        helper.getOperators(component);
      	helper.getProductFields(component);
	},
    AddNewRow : function(component, event, helper){
       // fire the AddNewRowEvt Lightning Event 
        component.getEvent("AddRowEvent").fire();     
    },
    
    removeRow : function(component, event, helper){
     // fire the DeleteRowEvt Lightning Event and pass the deleted Row Index to Event parameter/attribute
       component.getEvent("DeleteRowEvent").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    },   
 
})