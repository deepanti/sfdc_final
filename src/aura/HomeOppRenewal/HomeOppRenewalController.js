({
    doInit : function(component, event, helper) {
        // create a one-time use instance of the serverEcho action
        // in the server-side controller
        
        console.log("Do init is called");
        var action = component.get("c.fetchListOpportunity");
         action.setParams({
            'renewalDuration' :component.get("v.RenewalDuration")
        });
        action.setCallback(this, function(response) {
            //debugger;
            var state = response.getState();
            
            console.log("state"+state);
            if (state === "SUCCESS") {
                var itemList=[];
                var result = response.getReturnValue();
                var maxNoOfRecords = parseInt(component.get("v.maxNoOfRecords"));
                if(result!=null)
                {
                    component.set("v.oppFullList",result);
                    component.set("v.TotalOppCount",result.length);
                    if(maxNoOfRecords>=result.length  )
                        maxNoOfRecords = result.length;
                    for(var i=0;i<maxNoOfRecords;i++)
                    {
                        itemList.push(result[i]);
                    }
                    component.set("v.displaySpecificRecords",maxNoOfRecords);
                    component.set("v.opplst",itemList);
                   
                }
            }
            else if (state === "INCOMPLETE") {
                // do something
                console.log("Incomplete");
            }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            component.set("v.isLoading", false);
        });
        
        // optionally set storable, abortable, background flag here
        
        // A client-side action could cause multiple events, 
        // which could trigger other events and 
        // other server-side action calls.
        // $A.enqueueAction adds the server-side action to the queue.
        $A.enqueueAction(action);
    },
    fullScreen:function(component, event, helper) {
        component.set("v.fullScreen", true); 
    },
    closeDialog:function(component, event, helper) {
        component.set("v.fullScreen", false);       
    },
    setRenewalDuration:function(component, event, helper) {
        
        component.set("v.RenewalDuration", event.getParam("value")); 
    }
    
})