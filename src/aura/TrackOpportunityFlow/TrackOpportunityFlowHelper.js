({
	trackOpportunityVisit:function(component, event, helper)
    {
        var action = component.get("c.trackOpportunityFlow");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
             
            }
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
             
            }
            
        });
        $A.enqueueAction(action); 
    }
})