({
	doInit : function(component, event, helper) {
        
		var action = component.get("c.createDeepCloneOpportunity");
        action.setParams({ 
            "oppId" : component.get("v.recordId"),
            "isRenewable" : false,
            "stage" : '1. Discover'
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var oppId = response.getReturnValue();
                helper.showToast("success", "Opportunity Deep Clone Successfully.");
                helper.navigateToOpp(oppId);
            }
            else
            {
               	helper.showToast("error", "Deep Clone failed.");
            }
        });
        $A.enqueueAction(action);
	}
})