({
	showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    },
    navigateToOpp : function (oppId) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": oppId
        });
        navEvt.fire();
    }
})