({
    doInit : function(component, event, helper) {
		helper.initAllDataWrap(component,event,helper);
        helper.initHoldDataWrap(component,event,helper);
        helper.initHRApprovedDataWrap(component,event,helper);
        helper.getFinancialYear(component,event,helper);
        helper.fetchUser(component,event,helper);
	},
	myAction : function(component, event, helper) {
		
	},
	selectNUnselectAll : function(component, event, helper) {
		var statusOfSelectAll = component.find("selectAllID").get("v.value");
        var userDataWrap = component.get("v.lstUserDataWrapLine");
        for(var eachUser in userDataWrap){
            userDataWrap[eachUser].isUserChecked = statusOfSelectAll;
            for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                userDataWrap[eachUser].lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
                for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
                }
            } 
        }
        component.set("v.lstUserDataWrapLine",userDataWrap);
	},
    selectAllOnHold : function(component, event, helper) {
		var statusOfSelectAll = component.find("selectOnHoldAllID").get("v.value");
        var userDataWrap = component.get("v.lstUserOnHoldDataWrapLine");
        for(var eachUser in userDataWrap){
            userDataWrap[eachUser].isUserChecked = statusOfSelectAll;
            for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                userDataWrap[eachUser].lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
                for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
                }
            } 
        }
        component.set("v.lstUserOnHoldDataWrapLine",userDataWrap);
	},
	approveNSubmit : function(component, event, helper) {
        
        component.set("v.showModal",true);
    },   
        
    onButtonClickHandler : function(component, event, helper) {
        var checkOliCount = 0;
        var reqCommCountErrCount = 0;
        var reqOliCheckErrCount = 0;
        var userDataWrap = component.get("v.lstUserDataWrapLine");
        for(var eachUser in userDataWrap){
            for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    if( ($A.util.isUndefinedOrNull(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment) 
                         || userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment == '') && 
                      (userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - OnHold')){
                        //|| userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - Rejected' 
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = true;
                    	//alert(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus);
                        	reqCommCountErrCount++;
                    }else{
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = false;
                    }
                    if(!$A.util.isUndefinedOrNull(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment) && 
                      	(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - OnHold' || 
                      	userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - Rejected') && 
                      	!userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                        	reqOliCheckErrCount++;  
                    }
                    if(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                        checkOliCount++;
                    }
                }
            } 
            if(userDataWrap[eachUser].isUserChecked){
                checkOliCount++;
            }
        }
        component.set("v.lstUserDataWrapLine",userDataWrap);
        if(reqCommCountErrCount != 0){
            var strMSG = 'Comments are required for pending and hold status';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(reqOliCheckErrCount != 0){
            var strMSG = 'Opportunity Product Need To Be Checked';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount == 0){
            var strMSG = 'At Least One Opportunity Product Need To Be Selected';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount != 0){
            helper.approveNSubmitHelper(component,event,helper);
            helper.initAllDataWrap(component,event,helper);
            //helper.showToastMsg(component,event,helper);
        }
        component.set("v.showModal",false);
	},
        
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
    }, 
    approveOnHold : function(component, event, helper) {
        var checkOliCount = 0;
        var reqCommCountErrCount = 0;
        var reqOliCheckErrCount = 0;
        var userDataWrap = component.get("v.lstUserOnHoldDataWrapLine");
        for(var eachUser in userDataWrap){
            for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    if($A.util.isUndefinedOrNull(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment) && 
                      (userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - OnHold' || 
                      	userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - Rejected')){
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = true;
                    	//alert(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus);
                        	reqCommCountErrCount++;
                    }else{
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = false;
                    }
                    if(!$A.util.isUndefinedOrNull(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment) && 
                      	(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - OnHold' || 
                      	userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'Supervisor - Rejected') && 
                      	!userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                        	reqOliCheckErrCount++;  
                    }
                    if(userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                        checkOliCount++;
                    }
                } 
            } 
            if(userDataWrap[eachUser].isUserChecked){
                checkOliCount++;
            }
        }
        component.set("v.lstUserOnHoldDataWrapLine",userDataWrap);
        if(reqCommCountErrCount != 0){
			//alert('Comments are required for pending and hold status');
			var strMSG = 'Comments are required for pending and hold status';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(reqOliCheckErrCount != 0){
            //alert('Opportunity Product Need To Be Checked');
            var strMSG = 'Opportunity Product Need To Be Checked';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount == 0){
            //alert('At Least One Opportunity Product Need To Be Checked');
            var strMSG = 'At Least One Opportunity Product Need To Be Selected';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount != 0){
            helper.approveOnHoldHelper(component,event,helper);
            helper.initAllDataWrap(component,event,helper);
            //helper.showToastMsg(component,event,helper);
        }
	},
    
    generateLetter: function(component, event, helper){ 
        component.set("v.showGenerateStmt", true);
    },
    
    cancelBtnId: function(component, event, helper){ 
        component.set("v.showGenerateStmt", false);
    },
    
    submitLetter: function(component, event, helper){ 
        //var userId = $A.get("$SObjectType.CurrentUser.Id");
  		//var strFY = component.get("v.strFinancialYear");  
        //var selectedYear = parseInt(strFY);
        //alert(userId);
        helper.submittingBonusLetter(component, event, helper);
        component.set("v.showGenerateStmt", false);
    }
})