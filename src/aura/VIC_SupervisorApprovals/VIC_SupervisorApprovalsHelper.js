({
    initAllDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");	
        cmp.set("v.isSpinnerVisible",true);
        action.setParams({
            "strVicTeamStatus" : 'Supervisor Pending'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstUserDataWrapLine",resp.getReturnValue());
            }
            cmp.set("v.isSpinnerVisible",false);
        });
        $A.enqueueAction(action);
	},
    initHoldDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");	
        action.setParams({
            "strVicTeamStatus" : 'Supervisor - OnHold'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstUserOnHoldDataWrapLine",resp.getReturnValue()); 
            }else{
               
            }
        });
        $A.enqueueAction(action);
	},
    initHRApprovedDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");	
        action.setParams({
            "strVicTeamStatus" : 'HR - Approved'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstUserHrApprovedDataWrapLine",resp.getReturnValue()); 
            }else{
               
            }
        });
        $A.enqueueAction(action);
	},
	approveNSubmitHelper : function(component, event, helper) {
        component.set("v.showModal",false);
        var lstUserOppWrapData = component.get("v.lstUserDataWrapLine");
		var action = component.get("c.approvSubmitIncentive");
        action.setParams({
            "strUserOppDataWrpa" : JSON.stringify(lstUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                /* this is toast message while approve opportunity*/     
                var sMsg = 'Records have been processed successfully.';
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    duration:' 500',
                    message: sMsg,
                    type : 'success'
                });
                toastEvent.fire();
            }else{
                //alert(state);
                var sMsg = 'Records have not been processed successfully.'
                this.showToastMsg(component.event, helper, sMsg, 'error');
            }
        });
        $A.enqueueAction(action);
	},
    approveOnHoldHelper : function(component, event, helper) {
        component.set("v.showModal",false);
        var lstUserOppWrapData = component.get("v.lstUserOnHoldDataWrapLine");
		var action = component.get("c.approvSubmitIncentive");
        action.setParams({
            "strUserOppDataWrpa" : JSON.stringify(lstUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                /* this is toast message while approve opportunity*/     
                var sMsg = 'Records have been processed successfully.';
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    duration:' 500',
                    message: sMsg,
                    type : 'success'
                });
                toastEvent.fire();
            }else{
               //alert(state);
                var sMsg = 'Records have not been processed successfully.'
                this.showToastMsg(component.event, helper, sMsg, 'error');
            }
        });
        $A.enqueueAction(action);
	},
    showToastMsg : function(component, event, helper, strMSG, strType){  
        var sMsg = strMSG;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            duration:' 500',
            message: sMsg,
            type : strType
        });
        toastEvent.fire();
	},
    
    getFinancialYear : function(component,event,helper) {
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstYears",allValues);
                component.set("v.strFinancialYear",allValues[0]);
            }
        });
        $A.enqueueAction(action);
    },
    
    fetchUser : function(component,event,helper) {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstUser",allValues);
            }
        });
        $A.enqueueAction(action);
    },
    
    submittingBonusLetter : function(component,event,helper) {
        component.set("v.isSpinnerVisible",true);
        var action = component.get("c.submitBonusLetter");
        action.setParams({
            "strYear" : component.get("v.strFinancialYear"),
            "userId" : component.get("v.strUserId")
        });
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var urlValues = response.getReturnValue();
                window.open(urlValues);
            }
            component.set("v.isSpinnerVisible",false);
        });
        $A.enqueueAction(action);
    }
    
})