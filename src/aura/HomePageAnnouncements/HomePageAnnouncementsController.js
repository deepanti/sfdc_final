({
    doInit : function(component, event, helper) {
        var MaxNoOfAnnouncements = parseInt(component.get("v.MaxNoOfAnnouncements"));
        var itemList = [];
        var action = component.get("c.getActiveAnnouncements");
        action.setCallback(this, function(response){
            if(response.getState()==='SUCCESS')
            {
                var announcements = response.getReturnValue();
                if(announcements!=null)
                {
                    //Set maximum no of announcement to fetched announcement length or MaxNoOfAnnouncements attribute
                    //Whichever is less
                    if(MaxNoOfAnnouncements>=announcements.length)
                        MaxNoOfAnnouncements = announcements.length;
                    for(var i=0;i<MaxNoOfAnnouncements;i++)
                    {
                        itemList.push(announcements[i]);
                    }
                    component.set("v.AnnouncementList",itemList);
                }
            }
        });
        $A.enqueueAction(action);
    }
})