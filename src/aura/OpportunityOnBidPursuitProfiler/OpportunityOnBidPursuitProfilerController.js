({
    doInit:function(component, event, helper) {
        // to show spinner on click of component & before load of component 
        helper.showSpinner(component, event, helper);
        helper.createGCICompononent(component, event, helper,$A.get("$Label.c.OpportunityPursuitProfiler"));
        helper.hideSpinner(component, event, helper);
    },
    save : function(component, event, helper) {
        component.set("v.Spinner", true);
        component.find("recordViewForm").submit();
        localStorage.setItem('LatestTabId','13');
    },
    saveAndNext : function(component, event, helper) {
        component.set("v.Spinner", false);
        if(localStorage.getItem('MaxTabId')<14){
            localStorage.setItem('MaxTabId',14);
            component.set("v.MaxSelectedTabId", "14");
            component.find("recordViewForm").submit();
        } 
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "14"});
        appEvent.fire();
        localStorage.setItem('LatestTabId',"14");
        window.scrollTo(0, 0);
        
    },
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',"12");
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "12"});
        compEvents.fire();
        window.scrollTo(0, 0);
    },
    showSpinner : function(component, event, helper){
        component.set("v.Spinner", true);  
    },
    showToast : function(component, event, helper) {
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
            	if(!$A.util.isUndefinedOrNull(msg.message))
                        errorMsg +=msg.message;
            }
        );
        
        // top level error messages
         Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                        errorMsg +=msg.message + ', ';
					 }
                )
            });
        helper.fireToastEvent(component, event, helper, errorMsg);
    },
    fireEvent: function(component, event, helper) {
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully in OnBid stage.');
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "12"});
        compEvents.fire();
    },
})