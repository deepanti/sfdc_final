({
    doInit : function(component, event, helper) {
		helper.showSpinner(component, event, helper);
	},
    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
	handleLoad : function(component, event, helper) {
		helper.hideSpinner(component, event, helper);
	},
    handleSubmit: function(component, event, helper) 
    {
        component.set("v.Spinner", true);
        component.set("v.showToastAnnouncement",false);
        event.preventDefault();
         var check = helper.validateAll(component, event, helper);
        if(check)
        {
           var fields = event.getParam("fields");
        	//
            
            fields["StageName"]='Prediscover';
        	component.find("form").submit(fields);
             
        }
        else
        {
             component.set("v.Spinner", false);
            component.set("v.toastAnnouncementText",'Required fields are missing');
            component.set("v.showToastAnnouncement",true);
        }
		
	},
    //For Spinner
     handleError: function(component, event, helper) {
      component.set("v.Spinner", false);
	},
    
    handleSuccess: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": event.getParams().response.id
                });
                navEvt.fire();
	},
    closeModal:function(component, event, helper) {
        window.history.back();
    } 
})