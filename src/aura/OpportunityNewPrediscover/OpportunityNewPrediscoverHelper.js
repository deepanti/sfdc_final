({
	
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
   
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
     validateAll : function(component, event, helper) {
        //validating account Id
        var proceedToSave = true;
    	var acc = component.find("accId");
        var accountName = acc.get("v.value");
        if($A.util.isUndefinedOrNull(accountName) || accountName == '')
        {
            $A.util.addClass(acc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(acc,'slds-has-error');
        }
        
        //Validating target Source Picklist
        var tarSource = component.find("tarSource");
        var tarSourceValue = tarSource.get("v.value");
      
        if($A.util.isUndefinedOrNull(tarSourceValue) || tarSourceValue == '')
        {
          
            $A.util.addClass(tarSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(tarSource,'slds-has-error');
        }
       
        //Validating Opportunity Source picklist
        var oppSource = component.find("oppSource");
        var oppSourceValue = oppSource.get("v.value");
         if(tarSourceValue !='') {
        if($A.util.isUndefinedOrNull(oppSourceValue) || oppSourceValue == '')
        {
          
            $A.util.addClass(oppSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSource,'slds-has-error');
        }
     }
        //Validating Opportunity Close Date 
       /* var closeDate = component.find("oppCloseDate");
        var closeDateValue = closeDate.get("v.value");
      
        if($A.util.isUndefinedOrNull(closeDateValue) || closeDateValue == '')
        {
          
            $A.util.addClass(closeDate,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(closeDate,'slds-has-error');
        }/*
         //Validating Sales Region picklist
       /* var salesRegion = component.find("salesRegion");
        var salesRegionValue = salesRegion.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesRegionValue) || salesRegionValue == '')
        {
          
            $A.util.addClass(salesRegion,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesRegion,'slds-has-error');
        }
         //Validating sales Country picklist
        var salesCountry = component.find("salesCountry");
        var salesCountryValue = salesCountry.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesCountryValue) || salesCountryValue == '')
        {
          
            $A.util.addClass(salesCountry,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesCountry,'slds-has-error');
        }
        */
        //validating Description
        /*var sumOpp = component.find("sumOpp");
        var sumOppValue = sumOpp.get("v.value");
      
        if($A.util.isUndefinedOrNull(sumOppValue) || sumOppValue == '')
        {
          
            $A.util.addClass(sumOpp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(sumOpp,'slds-has-error');
        }*/
        return proceedToSave;
    }
})