({
    scriptsLoaded : function(component, event, helper){       
        //helper.getAccts(component,event, helper);     
        var action = component.get("c.autoCompleteCon");
        action.setCallback(this, function(response){
            var state = response.getState();            
            component.set("v.accounts", response.getReturnValue());                       
            var j$ = jQuery.noConflict();            
            var AccountList = component.get("v.accounts");                        
            j$(document).ready(function(){
                j$("#test1").autocomplete({
                    source :AccountList
                });
            });               
        });
        //$A.enqueueAction(action);     
    },
    doInit: function(component, event, helper) {         
    },
    handleInputKeyUp : function(component, event, helper){       
        //helper.getAccts(component,event, helper);     
        var action = component.get("c.autoCompleteCon");
        var sb = component.find('sb').getElement().value;
        //alert(sb);
        action.setParams({ sb : component.find('sb').getElement().value });
        action.setCallback(this, function(response){
            var state = response.getState();            
            component.set("v.accounts", response.getReturnValue());                       
            var j$ = jQuery.noConflict();            
            var AccountList = component.get("v.accounts");                        
            j$(document).ready(function(){
                j$("#test1").autocomplete({
                    source :AccountList
                });
            });               
        });
        $A.enqueueAction(action);    
                component.set("v.Spinner", true);
    },
    handleClick : function(component, event, helper){ 
        var action = component.get("c.searchString");
        //alert(sb);
        action.setParams({ sb : component.find('sb').getElement().value });
        action.setCallback(this, function(response){
            var state = response.getState();            
            if (state === "SUCCESS") {                
                var responsearray = [];
                var keyMap = response.getReturnValue();
                for ( var key in keyMap ) {
                    var url = "/one/one.app?source=aloha#/sObject/" + key + "/view";
                    responsearray.push({value:keyMap[key], key:url});
                }
                component.set("v.responsearray", responsearray);
                if(responsearray.length>0)
                {
                	component.set("v.showBool", true);
                    component.set("v.noData", false);
                }
                else
                {
                    component.set("v.showBool", true);
                    component.set("v.noData", true);
                }
            }                
        });
        $A.enqueueAction(action);     
         
    },
     // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    }
 
})