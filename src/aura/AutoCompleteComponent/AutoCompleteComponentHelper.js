({
    getAccts: function(component,event, helper){
        var action = component.get("c.autoCompleteCon");        
        action.setCallback(this, function(response){
            var state = response.getState();            
            component.set("v.accounts", response.getReturnValue());                       
            var j$ = jQuery.noConflict();            
            var AccountList = component.get("v.accounts");                        
            j$(document).ready(function(){
                j$("#testId").autocomplete({
                    source :AccountList
                });
            });               
        });
        $A.enqueueAction(action);	
    }
})