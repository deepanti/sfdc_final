({
    onSelectChange : function(component, event, helper) {
    var selected = component.find("define_gci").get("v.value");
        alert(selected);
    //do something else
},
    
    doInit : function(component, event, helper) {
        var action = component.get("c.chkStage");
        action.setParams({
            parentId : component.get("v.recordId")
        });
        
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                var opp = a.getReturnValue();
                component.set("v.opp", opp);
            } else if (a.getState() === "ERROR") {
                console.log("oof");
            }
        });
        $A.enqueueAction(action);  
    },
    
    
    getInput : function(component, event) {
        var opp=component.get("v.opp");
        var gci;
        var solution;
        var pricing;
        
       // gci = component.find("define_gci");
        solution = component.find("define_solution");
        pricing = component.find("define_pricing");
        if((solution.get("v.value") == "") || (pricing.get("v.value") == ""))
        {
            alert('Competitive Win Plan/Solution and Pricing review need to be completed before moving Deal to next stage');
        }
        else
        {
            var saveValue = component.get("c.savefields");
            saveValue.setParams({
                
                solutionval: solution.get("v.value"),
                pricingval: pricing.get("v.value"),
                parentId : component.get("v.recordId")
            });
            
            saveValue.setCallback(this, function(response) {
                var state = response.getState();
                if(component.isValid() && state === "SUCCESS") {
                    
                    // Prepare a toast UI message
                    var resultsToast = $A.get("e.force:showToast");
                    var res = saveValue.getReturnValue();
                    resultsToast.setParams({
                        "title": "Deal governance",
                        "message": "Deal governance checklist updated successfully"
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                else if (state === "ERROR") {
                    var resultsToast = $A.get("e.force:showToast");
                    var res = saveValue.getReturnValue();
                    resultsToast.setParams({
                        "title": "Deal governance",
                        "message": "There is some error with the Opoortunity. Please try again."
                    });
                    
                    // Update the UI: close panel, show toast, refresh account page
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    $A.get("e.force:refreshView").fire();
                }
                    else {
                        console.log('Unknown problem, response state: ' + state);
                    }
            });
            $A.enqueueAction(saveValue);
        }
        
        
        
    }
})