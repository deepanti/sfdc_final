({
 getAccounts: function(component, event, helper) {
  //call apex class method
  var action = component.get('c.fetchAccount');
     action.setParams({"OppId": component.get("v.recordId")});
  action.setCallback(this, function(response) {
   //store state of response
   var state = response.getState();
   if (state === "SUCCESS") {
    //set response value in ListOfAccount attribute on component.
    component.set('v.ListOfAccount', response.getReturnValue());
   }
  });
  $A.enqueueAction(action);
 },
getAccountLeads: function(component, event, helper) {
  //call apex class method
  var action = component.get('c.fetchAccountLead');
     action.setParams({"OppId": component.get("v.recordId")});
  action.setCallback(this, function(response) {
   //store state of response
   var state = response.getState();
   if (state === "SUCCESS") {
    //set response value in ListOfAccount attribute on component.
    component.set('v.ListOfAccountLeads', response.getReturnValue());
   }
  });
  $A.enqueueAction(action);
 }, 

mailToString: function(component, event, helper) {
  //call apex class method
  var action = component.get('c.mailToString');
     action.setParams({"OppId": component.get("v.recordId")});
  action.setCallback(this, function(response) {
   //store state of response
   var state = response.getState();
   if (state === "SUCCESS") {
    //set response value in ListOfAccount attribute on component.
    component.set('v.accountName', response.getReturnValue());
   }
  });
  $A.enqueueAction(action);
 }
    
    
    
    
})