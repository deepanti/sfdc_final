({
    SearchHelper: function(component, event) {
        // show spinner message
         component.find("Id_spinner").set("v.class" , 'slds-show');
        var action = component.get("c.fetchOpportunities");
       
        action.setParams({
            'searchKeyWord': component.get("v.searchKeyword"),
            'searchKeyWordOLI': component.get("v.searchKeywordOLI")
        });
        action.setCallback(this, function(response) {
           // hide spinner when response coming from server 
            component.find("Id_spinner").set("v.class" , 'slds-hide');
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                // if storeResponse size is 0 ,display no record found message on screen.
                if (storeResponse.length == 0) {
                    component.set("v.Message", true);
                } else {
                    component.set("v.Message", false);
                }
                
                // set numberOfRecord attribute value with length of return value from server
                component.set("v.TotalNumberOfRecord", storeResponse.length);
                
                // set searchResult list with return value from server.
                component.set("v.searchResult", storeResponse); 
                
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
            }else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
    },
    ViewHelper:function(component, event) {
          component.set("v.recordId", event.getSource().get("v.name"));
  		  component.set("v.isVisble", true);
          component.set("v.isVisbleView", true);
    },
    closePopUpHelper:function(component, event){
        
          if(component.get("v.isview"))
  		 	 component.set("v.isVisble", false);
          else{
             component.set("v.isVisble", false);
             component.set("v.isVisbleEdit", false);
             component.set("v.isedit", false);
             component.set("v.isview", true);
          }
    },
    EditHelper:function(component, event){
          var saveClicked = false;
          if(component.get("v.isVisbleEdit")){ 
          	 var childCmp = component.find("editComp");
 			 childCmp.saveOLI();
             component.set("v.isVisble", false);
             component.set("v.isVisbleEdit", false);
             component.set("v.isedit", false);
             component.set("v.isview", true);
             saveClicked = true;
            
             var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                  "title": "Success!",
                  "type": "Success",
                
                "message": "OLI has been updated successfully."
            });
            toastEvent.fire(); 
              
          }
          if(!saveClicked){
  		  	component.set("v.isVisble", true);
            component.set("v.isedit", true);
            component.set("v.isVisbleEdit", true);
            component.set("v.isview", false);
            event.getSource().set('v.label','Save');
          }
    },
    CancelHelper:function(component, event){
         if(component.get("v.isview"))
  		  	 component.set("v.isVisble", false);
         else{
            component.set("v.isview", true);
            component.set("v.isedit", false);
            component.set("v.isVisbleEdit", false);
            component.find("saveButtonId").set('v.label','Edit');

         }
    }
    
})