({
    Search: function(component, event, helper) {
        component.set("v.showHeader",true);
        var searchField = component.find('searchField');
        // if value is missing show error message and focus on field
        if(false) {
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
        }else{
          // else call helper function 
            helper.SearchHelper(component, event);
        }
    },
    View:function(component,event,helper){
          helper.ViewHelper(component, event);
        /*  component.set("v.recordId", event.getSource().get("v.name"));
  		  component.set("v.isVisble", true);
          component.set("v.isVisbleView", true);*/
    },
    closePopUp:function(component,event,helper){
          helper.closePopUpHelper(component,event);
         /* if(component.get("v.isview"))
  		 	component.set("v.isVisble", false);
          else{
             component.set("v.isVisble", false);
             component.set("v.isVisbleEdit", false);
             component.set("v.isedit", false);
             component.set("v.isview", true);
          }*/
    },
    Edit:function(component,event,helper){
          helper.EditHelper(component,event);
          /*var saveClicked = false;
          if(component.get("v.isVisbleEdit")){ 
          	 var childCmp = component.find("editComp");
 			 childCmp.saveOLI();
             component.set("v.isVisble", false);
             component.set("v.isVisbleEdit", false);
             component.set("v.isedit", false);
             component.set("v.isview", true);
             saveClicked = true;
          }
          if(!saveClicked){
  		  	component.set("v.isVisble", true);
            component.set("v.isedit", true);
            component.set("v.isVisbleEdit", true);
            component.set("v.isview", false);
            event.getSource().set('v.label','Save');
          }*/
    },
    Cancel:function(component,event,helper){
         
         helper.CancelHelper(component,event);
         /* if(component.get("v.isview"))
  		  	 component.set("v.isVisble", false);
         else{
            component.set("v.isview", true);
            component.set("v.isedit", false);
            component.set("v.isVisbleEdit", false);
            component.find("saveButtonId").set('v.label','Edit');

         }*/
    },
})