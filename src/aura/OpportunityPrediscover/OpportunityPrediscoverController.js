({
    doInit : function(component, event, helper) {
        console.log(component.get("v.recordId"));
        helper.showSpinner(component, event, helper);
    },
    saveRecord : function(component, event, helper) {
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            
        	component.set("v.IsSpinner",false);
            component.find("recordViewForm").submit();
        }
        else
        {
            component.set("v.IsSpinner",false);
            helper.fireToastEvent(component, event, helper);
        }
    },
    showSuccessMessage : function(component, event, helper) {
        helper.showToast('success','Opportunity updated successfully')
    },
    SaveAndNext : function(component, event, helper) {
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            component.set("v.IsSpinner",false);
            if(localStorage.getItem('MaxTabId')<1)
            {
                component.set("v.MaxSelectedTabId", "1");
                localStorage.setItem('MaxTabId','1');
            }
            component.find("recordViewForm").submit();
            localStorage.setItem('LatestTabId','1');
            var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
            appEvent.setParams({"selectedTabId" : "1"});
            appEvent.fire();
        }
        else
        {
            component.set("v.IsSpinner",false);
            helper.fireToastEvent(component, event, helper);
        }
    },
    hideSpinner : function(component, event, helper) {
        helper.hideSpinner(component, event, helper);
    }
})