({   
    validateAll : function(component, event, helper) {
        component.set("v.IsSpinner",true);
        var proceedToSave = true;
    	var acc = component.find("account");
        var accountName = acc.get("v.value");
        if($A.util.isUndefinedOrNull(accountName) || accountName == '')
        {
            $A.util.addClass(acc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(acc,'slds-has-error');
        }
        var oppSrc = component.find("oppSource");
        var oppSrcVal = oppSrc.get("v.value");
        if($A.util.isUndefinedOrNull(oppSrcVal) || oppSrcVal == '')
        {
            $A.util.addClass(oppSrc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSrc,'slds-has-error');
        }
        var oppSubSource = component.find("oppSubSource");
        var oppSubSourceVal = oppSubSource.get("v.value");
        if($A.util.isUndefinedOrNull(oppSubSourceVal) || oppSubSourceVal == '')
        {
            $A.util.addClass(oppSubSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSubSource,'slds-has-error');
        }
          
        
       /* var summary = component.find("summary");
        var summaryVal = summary.get("v.value");
        if($A.util.isUndefinedOrNull(summaryVal) || summaryVal == '')
        {
            $A.util.addClass(summary,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(summary,'slds-has-error');
        }*/
          //Validating Sales Region picklist
    /*    var salesRegion = component.find("salesRegion");
        var salesRegionValue = salesRegion.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesRegionValue) || salesRegionValue == '')
        {
          
            $A.util.addClass(salesRegion,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesRegion,'slds-has-error');
        }
         //Validating sales Country picklist
        var salesCountry = component.find("salesCountry");
        var salesCountryValue = salesCountry.get("v.value");
      
        if($A.util.isUndefinedOrNull(salesCountryValue) || salesCountryValue == '')
        {
          
            $A.util.addClass(salesCountry,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesCountry,'slds-has-error');
        }*/
        return proceedToSave;
    },
    fireToastEvent : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "Required Fields Missing",
            "message": "Please fill all required fields.",
            "type": "error"
        });
        toastEvent.fire();
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message,
            "mode" : "sticky"
        });
        toastEvent.fire();
    },  
})