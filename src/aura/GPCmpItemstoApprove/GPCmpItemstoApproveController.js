({
	doInit: function(component, event, helper) {
        helper.displayApplication(component);
        helper.hideSpinner(component);
    }
})