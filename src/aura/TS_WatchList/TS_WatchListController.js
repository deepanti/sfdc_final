({
   navChatter : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Chatter"
        });
        homeEvent.fire();
    },
    
    isStale : function (component,event,helper){
        var chkSF1 = component.get("c.chkSF1");
        chkSF1.setCallback(this,function(response){
            var state = response.getState();
            if (state == 'SUCCESS') {
                if(response.getReturnValue() == true){
                    component.set("v.chkSF1",true);
                }
            }
        });
        
        var chkProfile = component.get("c.chkProfile");
        chkProfile.setCallback(this,function(response){
            var state = response.getState();
            var dealsWrapTs;
            var dealsWrapNonTs;
            
            if (state == 'SUCCESS') {                
                if(response.getReturnValue() == true){
                    component.set("v.chkProfile",true);
                }
                var getTsDealsCount = component.get("c.Fetch_TS_Ageing_Opps");
                getTsDealsCount.setCallback(this, function(response){
                    var state = response.getState();
                    if(state == 'SUCCESS'){
                        dealsWrapTs = response.getReturnValue();
                         var profileCheck = component.get("v.chkProfile");
                         if(profileCheck == true){
                            component.set("v.stalecount", dealsWrapTs.staleDealsCount); 
                    		component.set("v.stallcount", dealsWrapTs.stalledDealsCount);
                        }
                        else{
                            component.set("v.staledealcount", dealsWrapTs.staleDealsCount);
                    		component.set("v.stalldealcount", dealsWrapTs.stalledDealsCount);
                        }
                        component.set("v.displaytext",true);
                    }
                });
                
                var getNonTsDealsCount = component.get("c.Fetch_Non_TS_Ageing_Opps");
                getNonTsDealsCount.setCallback(this, function(response){
                    var state = response.getState();
                    if(state == 'SUCCESS'){
                        dealsWrapNonTs = response.getReturnValue();
                         var profileCheck = component.get("v.chkProfile");
                         if(profileCheck == true){
                         	component.set("v.nonTsStaleCount", dealsWrapNonTs.staleDealsCount_nonTs);
                    		component.set("v.nonTsStallCount", dealsWrapNonTs.stalledDealsCount_nonTs);    
                         }
                        else{
                            component.set("v.nonTsStaleDealCount", dealsWrapNonTs.staleDealsCount_nonTs);
                    		component.set("v.nonTsStallDealCount", dealsWrapNonTs.stalledDealsCount_nonTs);
                        }
                        component.set("v.displaytext",true);
                    }
                });
                
                $A.enqueueAction(getTsDealsCount);
                $A.enqueueAction(getNonTsDealsCount);
                
            }
        });	
	
        $A.enqueueAction(chkSF1); 
        $A.enqueueAction(chkProfile);
    },
    gotoURL : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/apex/Stale_deal_v2"
        });
        urlEvent.fire();
    },
    
    gotomobURL : function (component, event, helper) {
        var moburlEvent = $A.get("e.force:navigateToURL");
        moburlEvent.setParams({
            "url": "/apex/Stale_deal_Mobile"
        });
        moburlEvent.fire();
    },
    
    navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:OppMainComponent",
        });
        evt.fire();
    }
        
})