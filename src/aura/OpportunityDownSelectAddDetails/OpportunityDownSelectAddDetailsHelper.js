({
    
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var dealType = component.find("dealType");
        var dealTypeVal = dealType.get("v.value");
    	var CloseComp = component.find("ClosestCompetitor");
        var CloseCompotitor = CloseComp.get("v.value");
        /*if(($A.util.isUndefinedOrNull(CloseCompotitor) || CloseCompotitor == '' || CloseCompotitor == 'Undefined') && dealTypeVal =='Competitive')
        {
            $A.util.addClass(CloseComp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(CloseComp,'slds-has-error');
        }*/
       
        return proceedToSave;
    },
    fireToastEvent : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "Error!",
            "message": "Required fields are missing",
            "type": "error"
        });
        toastEvent.fire();
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
})