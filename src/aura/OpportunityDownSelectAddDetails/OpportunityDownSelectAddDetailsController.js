({
    saveRecord : function(component, event, helper) {
        event.preventDefault();
        component.set("v.Spinner", true);
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            if(localStorage.getItem('StageName')=='3. On Bid')
            {
                component.set("v.StageName","4. Down Select");
                component.set("v.MaxSelectedTabId","14");
                localStorage.setItem('StageName',"4. Down Select");
            }
            component.find("recordViewForm").submit();
            localStorage.setItem('LatestTabId','14');
            component.set("v.Spinner", false);
        }
        else
        {
            helper.fireToastEvent(component, event, helper);
            component.set("v.Spinner", false);
        }
    },
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    saveAndNext : function(component, event, helper) {
        component.set("v.Spinner", true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            if(localStorage.getItem('MaxTabId')<15){
                localStorage.setItem('MaxTabId',15);
                component.set("v.MaxSelectedTabId", "15");
                if(localStorage.getItem('StageName')=='3. On Bid')
                {
                    component.set("v.StageName","4. Down Select");
                    localStorage.setItem('StageName',"4. Down Select");
                }
            }
            component.find("recordViewForm").submit();
            localStorage.setItem('LatestTabId','15');
            window.scrollTo(0, 0);
        }
        else
        {
            helper.fireToastEvent(component, event, helper);
            component.set("v.Spinner", false);
        }
    },
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',13);
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "13"});
        appEvent.fire();
        window.scrollTo(0, 0);
    },
    onSuccess:function(component, event, helper){
        var appEvent = $A.get("e.c:SetSalesPath");
        appEvent.fire();
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully');
        component.set("v.IsSpinner", false);
        var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "11"});
            compEvents.fire();
    },
    showError : function(component, event, helper) {
        localStorage.setItem('MaxTabId',14);
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastEvent(component, event, helper, errorMsg);
        component.set("v.IsSpinner", false);
    },
})