({
    openModel: function(component, event, helper) {
        var recID = component.get("v.recordId");
        var actionQSRM = component.get("c.getQSRMObjMethod");
        actionQSRM.setParams({
            "QsrmID" : recID
        });
        var self = this;
        actionQSRM.setCallback(this, function(actionResult) {
            component.set('v.QSRMObject', actionResult.getReturnValue());
            console.log(actionResult.getReturnValue());
        });
        $A.enqueueAction(actionQSRM);
    },
    cancelBtn : function(component, event, helper) { 
        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
        dismissActionPanel.fire(); 
    } ,
    
    ReworkMethod: function(component,event,helper){
        try{
            debugger;
            component.set("v.spinner",true);
            var recID = component.get("v.recordId");
            var commentsEntered = component.find("commentsId").get("v.value");
            var comm = component.find("comments");
            if($A.util.isUndefinedOrNull(commentsEntered) || commentsEntered == ''){ 
                $A.util.addClass(comm,'slds-has-error');
                component.set("v.isErrorMsg","Please Provide Appropriate Comments.");
                component.set("v.isError", true); 
                window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            );
                component.set("v.spinner",false);
            } else {
                
                $A.util.removeClass(comm,'slds-has-error');
                var action = component.get("c.approverActionMethod");
                action.setParams({ "QsrmID" : recID, "Action" : "Removed","commentsData" : commentsEntered});
                action.setCallback(this, function(actionResult) {
                    if(actionResult.getState()=="SUCCESS"){
                        component.set('v.ProcessInstanceslist', actionResult.getReturnValue());
                        component.set("v.spinner",false);
                        helper.showToast(component,"Success", "Rework request has been submitted to Opportunity owner successfully.");
                        var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
                        dismissActionPanel.fire(); 
                }
                else{
                    helper.showToast(component,"error",actionResult.getError()[0].message);
                    component.set("v.isErrorMsg","Some thingh wrong contact your Admin");
                    component.set("v.isError", true); 
                    component.set("v.spinner",false);
                    var dismissActionPanel = $A.get("e.force:closeQuickAction"); 
                    dismissActionPanel.fire(); 
                    window.setTimeout(
                        $A.getCallback(function() {
                            if(component.isValid()){
                                component.set("v.isError", false);    
                            }    
                        }),2000
                    );
                 }
            });
            $A.enqueueAction(action);
        } 
         }catch(err) {
             alert(':===== error message====:'+err.message+':===error line====:'+err.line);
         }
    },
    
    closeToast :function(component, event, helper){
        component.set("v.isError", false);
        component.set("v.isSuccess", false);
    }
})