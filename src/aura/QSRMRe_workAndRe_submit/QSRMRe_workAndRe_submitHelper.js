({
    showToast : function(component,type, message) {
        component.set("v.spinner",false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})