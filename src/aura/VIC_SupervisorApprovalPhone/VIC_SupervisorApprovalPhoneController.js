({
    doInit : function(component, event, helper) {
        component.set("v.showModal", false);
		helper.initAllDataWrap(component,event,helper);
        var userDataWrap = component.get("v.lstUserDataWrapLine");
    },
    
    navigateToOpportunities : function(component, event, helper) {
        var objUserDataWrap = component.get("v.objEachUseDataWrapLine")
        if(!$A.util.isEmpty(objUserDataWrap.lstOppDataWrap)){
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:VIC_SupervisorApprovalPhoneSalesRepOpportunities",
                componentAttributes: {
                    objUserDataWrapLine: objUserDataWrap
                }
            });
            evt.fire();
        }else{
            var strMSG = 'NO INCENTIVE EXIST';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }
    },
    
    initAllDataWrap : function(cmp,evt,help) {		//@Vikas Rajput: Potentially no use code, remove latter 
		var action = cmp.get("c.getInitLoadPageData");
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstUserDataWrapLine",resp.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
	selectNUnselectAll : function(component, event, helper){
        var statusOfSelectAll = component.find("selectAllID").get("v.value");
        var userDataWrap = component.get("v.lstUserDataWrapLine");
        for(var eachUser in userDataWrap){
            userDataWrap[eachUser].isUserChecked = statusOfSelectAll;
            if(isNaN(userDataWrap[eachUser].lstOppDataWrap)){
                for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                    userDataWrap[eachUser].lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
                    for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
                    }
                } 
            }
        }
        component.set("v.lstUserDataWrapLine",userDataWrap);
	},
    checkedUser : function(component, event, helper){
        var userChecked = component.get("v.checkAllUser");;
        var lstuserDataWrapVal = component.get("v.lstUserDataWrapLine");
        for(var objUser in lstuserDataWrapVal){
            if(isNaN(lstuserDataWrapVal[objUser])){
            	lstuserDataWrapVal[objUser].isUserChecked = userChecked;
            }
        }
        component.set("v.lstUserDataWrapLine",lstuserDataWrapVal);
    },
    approveIncentive : function(component, event, helper) {
        var oliCheckedcount = 0;
        var userDataWrap = component.get("v.lstUserDataWrapLine");
        for(var eachUser in userDataWrap){
            //userDataWrap[eachUser].isUserChecked = statusOfSelectAll;
            if(userDataWrap[eachUser].isUserChecked && isNaN(userDataWrap[eachUser].lstOppDataWrap)){
                for(var eachOppData in userDataWrap[eachUser].lstOppDataWrap){
                    userDataWrap[eachUser].lstOppDataWrap[eachOppData].isOppChecked = userDataWrap[eachUser].isUserChecked;
                    for(var eachOLIData in userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                        userDataWrap[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = userDataWrap[eachUser].isUserChecked;
                    	oliCheckedcount++;
                    }
                } 
            }
        }
        if(oliCheckedcount == 0){
            var strMSG = 'NO INCENTIVE CHECKED TO APPROVE';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else{
            helper.approveNSubmitHelper(component,event,helper);
        }
	},
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
    },
    showConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", true);
    }
})