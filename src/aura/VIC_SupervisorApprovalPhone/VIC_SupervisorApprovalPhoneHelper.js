({
	initAllDataWrap : function(component, event, helper) {
		var action = component.get("c.getInitLoadPageData");
        action.setParams({
            "strVicTeamStatus" : 'Supervisor Pending'
        });
        
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                component.set("v.lstUserDataWrapLine",resp.getReturnValue());
                
                var userDataWrap = component.get("v.lstUserDataWrapLine");
                //changes done by Dheeraj Chawla on 04-09-2018.
                //to check the data is available for "Supervisor Pending".
                if($A.util.isUndefinedOrNull(userDataWrap) || userDataWrap.length == 0){
                    var strMSG = 'No Records Available for approval';
                    var strType = 'Error';
                    var strMode = 'sticky';
                    this.showToastMsg(component, event, helper, strMSG, strType, strMode);
                    return;
        		}
        		var userOppDataWrap;
                for(var eachUser in userDataWrap){
                    userOppDataWrap = userDataWrap[eachUser];
                    userDataWrap[eachUser].isUserSelected = true;
                    break;
                }
                component.set("v.objEachUseDataWrapLine",userOppDataWrap);
                component.set("v.lstUserDataWrapLine",userDataWrap);
            }
        });
        $A.enqueueAction(action);
        //component.set("v.objEachUseDataWrapLine",userOppDataWrap); 
        
	},
    approveNSubmitHelper : function(component, event, helper) {
        		var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    duration:' 5000',
                    message: 'RECORD SUBMITTED',
                    type : 'Success'
                });
                toastEvent.fire();
        var lstUserOppWrapData = component.get("v.lstUserDataWrapLine");
		var action = component.get("c.approvSubmitIncentive");
        action.setParams({
            "strUserOppDataWrpa" : JSON.stringify(lstUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
            	$A.get('e.force:refreshView').fire();    
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    duration:' 5000',
                    message: 'Error',
                    type : 'Error'
                });
                toastEvent.fire();
            }
            
        });
        $A.enqueueAction(action);
	},
    showToastMsg : function(component, event, helper, strMSG, strType, strMode){  
        var sMsg = strMSG;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: strMode,
            duration:' 5000',
            message: sMsg,
            type : strType
        });
        toastEvent.fire();
	}
})