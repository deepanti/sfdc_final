({
	myAction : function(component, event, helper) {
		
	},
    toggleOpportunity : function(component, event, helper) {
		var isOppOpen = component.get('v.isOppOpen');
        component.set('v.isOppOpen', !isOppOpen);
	},
    showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    }
})