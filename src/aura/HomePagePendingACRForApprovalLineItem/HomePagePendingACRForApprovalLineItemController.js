({
    openACR : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.wrapper.ACRRecord.Id")
        });
        navEvt.fire();
    },
    approve: function(component, event, helper)
    {
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "approvalRecordId" : component.get("v.wrapper.approvalItemId"),
            "actionToPerform" : 'Approve'
        });
        cmpEvent.fire();
    },
    reject:function(component, event, helper)
    {
        var cmpEvent = component.getEvent("cmpEvent");
        cmpEvent.setParams({
            "approvalRecordId" : component.get("v.wrapper.approvalItemId"),
            "actionToPerform" : 'Reject'
        });
        cmpEvent.fire();
    }
})