({
	navigateToPanel : function(component,event,url) {
     var urlEvent = $A.get("e.force:navigateToURL");
      urlEvent.setParams({
        'url': url
      });
      urlEvent.fire();
    },
    
    showInfoToast : function(component,event,helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: 'You do not have Privilege to access this VIC Panel.',
            duration:' 5000',
            key: 'info_alt',
            type: 'info',
            mode: 'dismissible'
        });
        toastEvent.fire();
    }
})