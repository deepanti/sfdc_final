({
	redirect : function(component, event, helper) {
        var url = '';
        console.log('Calling Controller');
		var action = component.get("c.redirectToUserPanel");
        action.setCallback(this,function(response){
            if(response.getState() == "SUCCESS"){
                var result = response.getReturnValue();
                console.log(result);
                if(result === 'Sales rep'){
                    console.log('Inside Sales Rep');
                    url = '/lightning/n/VIC_Sales_Rep_Approval';
                    helper.navigateToPanel(component,event,url);
                }else if(result == 'Supervisor'){
                    url = '/lightning/n/VIC_Supervisor_Approvals';
                    helper.navigateToPanel(component,event,url);
                }else if(result == 'No Privilege'){
                    helper.showInfoToast(component,event,helper);
                }
            }
        });
        $A.enqueueAction(action)
	}
})