({
    REQUIRED_FIELD_ERROR: "Required Fields Missing",

    getProfileBillRateData: function(component) {
        var getProfileBillRateService = component.get("c.getProjectProfileBillRateData");
        getProfileBillRateService.setParams({
            "projectId": component.get("v.recordId")
        });
        getProfileBillRateService.setCallback(this, function(response) {
            this.getProfileBillRateServiceHandler(response, component);
        });

        $A.enqueueAction(getProfileBillRateService);
    },
    getProfileBillRateServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setProfileBillRateAndRelatedData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setProfileBillRateAndRelatedData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        component.set("v.lstOfProfileBillRateRecords", responseJson.listOfProfileBillRates);
        component.set("v.projectBillingCurrency", responseJson.projectBillingCurrency);
        this.updateProfileBillRateStatus(component);
    },
    addRowToProfileBillRate: function(component) {
        var projectId = component.get("v.projectId");
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords") || [];
        var projectId = component.get("v.recordId");
        var newProfileBillRate = {
            "isEditable": true,
            "GP_Bill_Rate_Type__c": "",
            "new_Record": true,
            "Name": "",
            "GP_Project__c": projectId
        };

        lstOfProfileBillRateRecords.push(newProfileBillRate);
        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
    removeProfileBillRateService: function(component, event, helper) {
        if (!confirm("Are you sure you want to delete?"))
            return;

        var listOfProfileBillRate = component.get("v.lstOfProfileBillRateRecords");
        var profileIndex = Number(event.currentTarget.id);
        var profileBillRateToBeDeleted = listOfProfileBillRate[profileIndex];

        if (profileBillRateToBeDeleted["Id"]) {
            this.deleteProfileBillRate(component, profileBillRateToBeDeleted["Id"])
        }
        listOfProfileBillRate.splice(profileIndex, 1);
        component.set("v.lstOfProfileBillRateRecords", listOfProfileBillRate);
    },

    deleteProfileBillRate: function(component, profileBillrateToBeDeletedId) {

        this.showSpinner(component);

        var deleteProfileBillRateService = component.get("c.deleteProfileBillRate");

        deleteProfileBillRateService.setParams({
            "profileBillRate": profileBillrateToBeDeletedId
        });

        deleteProfileBillRateService.setCallback(this, function(response) {
            this.deleteProfileBillRateServiceHandler(component, response);
        });

        $A.enqueueAction(deleteProfileBillRateService);
    },
    deleteProfileBillRateServiceHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Project profile Bill Rate deleted successfully');
            this.fireProjectSaveEvent();
        } else {
            this.handleFailedCallback(component, responseData);

        }
    },
    saveProfileBillRateRecord: function(component) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords");

        if (!this.validateProfileBillRate(!lstOfProfileBillRateRecords || component, lstOfProfileBillRateRecords)) {
            return;
        }

        var saveProfileBillRateService = component.get("c.saveProfileBillRate");
        saveProfileBillRateService.setParams({
            "serializedListOfProfileBillRate": JSON.stringify(lstOfProfileBillRateRecords)
        });
        saveProfileBillRateService.setCallback(this, function(response) {
            this.saveProfileBillRateServiceHandler(response, component);
        });

        $A.enqueueAction(saveProfileBillRateService);

    },
    saveProfileBillRateServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.lstOfProfileBillRateRecords", JSON.parse(responseData.response));

            this.setIsEditableToFalse(component);
            this.showToast('SUCCESS', 'success', 'Project Profile Bill rate saved successfully');
            this.updateProfileBillRateStatus(component);
            this.fireProjectSaveEvent(true);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    toggelIsEditableForRow: function(component, event) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords");
        var profileIndex = Number(event.currentTarget.id);
        var isEditable = lstOfProfileBillRateRecords[profileIndex]["isEditable"];

        lstOfProfileBillRateRecords[profileIndex]["isEditable"] = !isEditable;
        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
    validateProfileBillRate: function(component, lstOfProfileBillRateRecords) {
        var profileBillRateObjectMap = {};
        var isValid = true;
        
        for (var i = 0; i < lstOfProfileBillRateRecords.length; i += 1) {
            var key = lstOfProfileBillRateRecords[i]["Name"] + '@@' + lstOfProfileBillRateRecords[i]["GP_Bill_Rate__c"] +
                '@@' + lstOfProfileBillRateRecords[i]["GP_Profile__c"];
            var rowDom = document.getElementById('profile-bill-rate-row' + i);

            if (this.checkEmptyValuesOfFields(lstOfProfileBillRateRecords[i])) {
                isValid = false;
                rowDom.className = 'invalid-row';
                lstOfProfileBillRateRecords[i]["errorMessage"] = this.REQUIRED_FIELD_ERROR;
            } else {
                lstOfProfileBillRateRecords[i]["errorMessage"] = null;
                profileBillRateObjectMap[key] = lstOfProfileBillRateRecords[i];
            }
        }
        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
        return isValid;
    },
    checkEmptyValuesOfFields: function(profileBillRateRecord) {
        return ($A.util.isEmpty(profileBillRateRecord["GP_Bill_Rate__c"]) ||
            profileBillRateRecord["GP_Bill_Rate__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(profileBillRateRecord["GP_Profile__c"]) ||
            profileBillRateRecord["GP_Profile__c"] == this.NONE_ENTRY_LABEL);

    },
    setIsEditableToFalse: function(component) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords");

        for (var i = 0; i < lstOfProfileBillRateRecords.length; i += 1) {
            lstOfProfileBillRateRecords[i]["isEditable"] = false;
        }
        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
    editProfileBillRateRecord: function(component) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords");
        if (!lstOfProfileBillRateRecords || lstOfProfileBillRateRecords.length === 0) {
            return;
        }

        for (var i = 0; i < lstOfProfileBillRateRecords.length; i += 1) {
            lstOfProfileBillRateRecords[i]["isEditable"] = true;
        }

        component.set("v.lstOfProfileBillRateRecords", lstOfProfileBillRateRecords);
    },
    updateProfileBillRateStatus: function(component) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords");

        if (lstOfProfileBillRateRecords && lstOfProfileBillRateRecords.length > 0) {
            component.set("v.billRateStatus", "dirty");
        } else {
            component.set("v.billRateStatus", "unTouched");
        }
    },

    fireApplicationEvent: function(component, event) {
        var appEvent = $A.get("e.c:GPEvntProfileBillRateChange");
        var lstOfProfileBillRateRecords = this.getListOfProfileBillRateRecords(component);

        appEvent.fire({
            "lstOfProfileBillRateRecords": lstOfProfileBillRateRecords
        });
    },
    getListOfProfileBillRateRecords: function(component) {
        var lstOfProfileBillRateRecords = component.get("v.lstOfProfileBillRateRecords") || [];
        var listOfProfileBillRateName = [];

        lstOfProfileBillRateRecords.forEach(function(profileBillRate) {
            listOfProfileBillRateName.push(profileBillRate.GP_Profile__c);
        });

        return listOfProfileBillRateName;
    }
})