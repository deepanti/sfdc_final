({
    doInit: function(component, event, helper) {
        var recordId = component.get("v.recordId");

        if (recordId) {
            helper.getProfileBillRateData(component);
        }
    },
    addToProfileBillRate: function(component, event, helper) {
        helper.addRowToProfileBillRate(component);
    },
    removeProfileBillRate: function(component, event, helper) {
        var approvalStatus = component.get("v.target.fields.GP_Approval_Status__c.value");
        
        if (approvalStatus == 'Approved' || approvalStatus == 'Pending For Approval')
            return;

        helper.removeProfileBillRateService(component, event, helper);
        helper.fireApplicationEvent(component, event);
    },
    saveRecords: function(component, event, helper) {
        helper.saveProfileBillRateRecord(component);
        /*var recordId = component.get("v.recordId");
        if(recordId) {
            helper.getProfileBillRateData(component);            
        }*/
        helper.fireApplicationEvent(component, event);
    },
    toggelIsEditableForRow: function(component, event, helper) {
        helper.toggelIsEditableForRow(component, event);
    },
    editAllRecords: function(component, event, helper) {
        helper.editProfileBillRateRecord(component);
    },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    },

})