({
    openMSA : function(component, event, helper) {
        component.set("v.IsSpinner",true);
        localStorage.setItem('LatestTabId','1');
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({ "stageName" : "Prediscover",
                            "selectedTabId" : "1"
                           });
        appEvent.fire();
        component.set("v.IsSpinner",false);
    },
    
    openContacts : function(component, event, helper) {
        component.set("v.IsSpinner",true);
        localStorage.setItem('LatestTabId','2');
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({ "stageName" : "Prediscover",
                            "selectedTabId" : "2"
                           });
        appEvent.fire();
        component.set("v.IsSpinner",false);
    },
    
    openProducts : function(component, event, helper) {
        component.set("v.IsSpinner",true);
        localStorage.setItem('LatestTabId','3');
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({ "stageName" : "Prediscover",
                            "selectedTabId" : "3"
                           });
        appEvent.fire();
        component.set("v.IsSpinner",false);
    },
    
    openPricing : function(component, event, helper) {
        component.set("v.IsSpinner",true);
        localStorage.setItem('LatestTabId','4');
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({ "stageName" : "Prediscover",
                            "selectedTabId" : "4"
                           });
        appEvent.fire();
        component.set("v.IsSpinner",false);
    },
})