({
	myAction : function(component, event, helper) {
		
	},
    cancel : function(component, event, helper) {
		component.set('v.isVisible', false);
	},
    save : function(component, event, helper) {
        var objIncentive = component.get('v.popoverIncentiveData');
        if(objIncentive.Target_Component__c == null){
         // alert("No Discretionary Componenet Attached With User");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "No Discretionary Componenet Attached With User."
            });
            toastEvent.fire();
        }else if(objIncentive.Bonus_Amount__c == null){
         // alert("Amount Required");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Amount Required."
            });
            toastEvent.fire();
        }else if(objIncentive.VIC_Description__c == null || objIncentive.VIC_Description__c == ''){
         // alert("Comments Required");
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Comments Required."
            });
            toastEvent.fire();
        }else{
            var objOLIDataWrap = component.get('v.oliDataWrap');
            var discretionarySum = (parseInt(objOLIDataWrap.strDiscretionary) + parseInt(objIncentive.Bonus_Amount__c));
            objOLIDataWrap.strTempDiscretionary = ''+discretionarySum;
            component.set('v.oliDataWrap',objOLIDataWrap);
            component.set('v.isVisible', false);
        }
	}
})