({
    
    handleApplicationEvent: function(component, event, helper) {
        helper.handleApplicationEvent(component, event, helper);
    },
    
    doInit : function(component, event, helper) {
        event.preventDefault();
        component.set("v.selectedTabId", localStorage.getItem('LatestTabId'));
        component.set("v.intSelectedTabId", parseInt(localStorage.getItem('LatestTabId')));  
    },
})