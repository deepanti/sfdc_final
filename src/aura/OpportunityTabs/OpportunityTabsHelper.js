({
	handleApplicationEvent : function(component, event, helper) {
        
		var selectedTabId = event.getParam("selectedTabId");
        
        component.set("v.selectedTabId", selectedTabId);
        component.set("v.intSelectedTabId", parseInt(selectedTabId));
        var selectIntval = component.get("v.intSelectedTabId");
        if(selectIntval < 1){
            var callAuraMethod = component.find('PrediscoverTabs');
        }
        else if(selectIntval < 8){
            var callAuraMethod = component.find('DiscoverTabs');
            callAuraMethod.setTabId(selectedTabId);
        }
            else if(selectIntval < 11){
                var callAuraMethod = component.find('DefineTabs');
                callAuraMethod.setTabId(selectedTabId);
            }
                else if(selectIntval < 14){
                    var callAuraMethod = component.find('OnBidTabs');
                    callAuraMethod.setTabId(selectedTabId);
                }
                    else if(selectIntval < 17){
                        var callAuraMethod = component.find('DownSelectTabs');
                        callAuraMethod.setTabId(selectedTabId);
                    }
                        else if(selectIntval < 20){
                            var callAuraMethod = component.find('ConfirmedTabs');
                            callAuraMethod.setTabId(selectedTabId);
                        }
                            else {
                                var callAuraMethod = component.find('ClosedTabs');
                                
                                callAuraMethod.setTabId(selectedTabId);
                            }
	}
})