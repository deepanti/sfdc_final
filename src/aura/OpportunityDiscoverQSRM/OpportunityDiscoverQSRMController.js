({
    doInit : function(component, event, helper) {
        $A.get('e.force:refreshView').fire();  
        helper.doInit(component, event, helper);
        helper.OppDetails(component, event, helper);
        if(component.get("v.oppDetails.QSRM_Required__c")){
            helper.showSpinner(component, event, helper);            
        }
       
    },
     handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    formHandleOnSubmit : function(component, event, helper) {
        component.set("v.Spinner", true);
        component.set("v.isSubmitted", true);
        
        event.preventDefault();
        var fields = event.getParam("fields");
        console.log(component.get("v.recordId"));
        component.find("form").submit(fields);
    },
    handleOnSubmit : function(component, event, helper) {
        component.set("v.Spinner", true);
         component.set("v.isSubmitted", true);
        event.preventDefault();
        var fields = event.getParam("fields");
        console.log(component.get("v.recordId"));
        component.find("recordViewForm").submit(fields);
    },
    handleSuccess : function(component, event, helper) {
        var action1 = component.get("c.fetchQSRMOwnerName");
        action1.setParams({
            "qsrmId" :component.get("v.QSRMId")
        });
        action1.setCallback(this, function(response) {
            var state = response.getState();		
            if(component.isValid() && state === "SUCCESS"){
                component.set("v.QSRMUsers",response.getReturnValue().toString());
                component.set("v.Spinner", false);                
 				helper.showToast("success", "QSRM Record is Created Successfully"); 
                $A.get('e.force:refreshView').fire();                
            }  
            
        });
        $A.enqueueAction(action1);
       
       
    },
    handleError : function(component, event, helper) {
        component.set("v.Spinner", false);
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        
        // top level error messages
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        helper.showToast("error", errorMsg);
    },
    hideSpinner : function(component, event, helper) {
         var eventParams = event.getParams();
        if(eventParams.changeType === "CHANGED") {
            // get the fields that are changed for this record
            var changedFields = eventParams.changedFields;
            if(changedFields.hasOwnProperty('QSRM_Required__c')){
                component.set("v.oppDetails.QSRM_Required__c",changedFields.QSRM_Required__c.value);              
            }else{
                component.set("v.showHideQSRM", true);
            }
            if(changedFields.hasOwnProperty('QSRM_Type__c')){
               component.set("v.oppDetails.QSRM_Type__c",changedFields.QSRM_Type__c.value); 
                $A.get('e.force:refreshView').fire(); 
            }
          //  $A.get('e.force:refreshView').fire(); 
        }
        helper.hideSpinner(component, event, helper);
        
    },
    //For Ts
    saveRecordTS : function(component, event, helper) {
        component.set("v.Spinner", true);
        event.preventDefault();
      	 var checkTS = helper.validateTS(component, event, helper);
        if(checkTS)
        {	
            component.set("v.isSubmitted", true);
            component.find("form").submit();
        }
        else
       {
           component.set("v.Spinner", false);
           helper.fireToastEvent(component, event, helper, 'Required fields are missing');
        }
        
    },
    saveRecord : function(component, event, helper) {
       component.set("v.Spinner", true);
        event.preventDefault();
      	 var check = helper.validateNonTs(component, event, helper);
        if(check)
        {
            component.set("v.isSubmitted", true);
            component.find("recordViewForm").submit();
        }
        else
       {
           component.set("v.Spinner", false);
            helper.fireToastEvent(component, event, helper, 'Required fields are missing');
        }
        
    },
    markStageAsComplete : function(component, event, helper) {
        component.set("v.Spinner", true); 
        if(localStorage.getItem('MaxTabId')<8)
        { 
            localStorage.setItem('MaxTabId',8);
            component.set('v.oppDetails.LatestTabId__c','8');
        }
        localStorage.setItem('LatestTabId',8);
        component.find("oppRecord").saveRecord($A.getCallback(function(saveResult) {
            if (saveResult.state === "SUCCESS" || saveResult.state === "DRAFT") {
                helper.showToast("success", "Opportunity Updated Successfully");
                component.set("v.Spinner", false);
            } else if (saveResult.state === "INCOMPLETE") {
                component.set("v.Spinner", false);
            } else if (saveResult.state === "ERROR") {
                component.set("v.Spinner", false);
            } else {
                component.set("v.Spinner", false);
            }
        }));
    },
    markStageAsCompleteWithoutSave: function(component, event, helper) {
        component.set("v.Spinner", true); 
        component.set('v.oppDetails.LatestTabId__c','8');
        
    },
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',6);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "6"});
        compEvents.fire();
        window.scrollTo(0, 0);
    },
    
    NonTsQSRMSubmitForA : function(component, event, helper) { 
        component.set("v.IsSpinner",true);
        event.preventDefault();
        var check = helper.validateNonTs(component, event, helper);
        if(check)
        {
        }
        else
        {
            helper.fireToastEvent(component, event, helper, 'Required fields are missing');
            component.set("v.IsSpinner", false);
        }
    },
    OnOtherValue : function(component, event, helper) { 
        
        var Named_Advisor = component.find("In_case_of_non_named_account__c");
        if(Named_Advisor != null && Named_Advisor != ''){
            var Named_AdvisorName = Named_Advisor.get("v.value");
            if(Named_AdvisorName == 'Other'){
                component.set("v.OnOtherValueFlag",true);
            }else{
                component.set("v.OnOtherValueFlag",false);
            }
        }
        
    }
})