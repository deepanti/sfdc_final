({
    doInit : function(component, event, helper) {
      
        var action1 = component.get("c.fetchApprovedOrInApprovalQSRM");
        action1.setParams({
            "oppId" :component.get("v.recordId")
        });
        action1.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === "SUCCESS"){
                var submitQSRM=component.get("v.isSubmitted");
                if(submitQSRM==true){
                    
                    helper.showToast("success", "QSRM form is submitted for approval successfully."); 
                    $A.get('e.force:refreshView').fire(); 
                }
                component.set("v.QSRMId",response.getReturnValue()); 
                  component.set("v.Spinner", false);
              $A.get('e.force:refreshView').fire();
            }            
        });
        $A.enqueueAction(action1);
        
    },
    
    OppDetails : function(component, event, helper){
        var action1 = component.get("c.fetchOppDetail");
        action1.setParams({
            "oppId" :component.get("v.recordId")
        });
        action1.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state === "SUCCESS"){
                var opplist = response.getReturnValue();
                var archtype = opplist.Archtype;
                var service = opplist.ServiceLine;
                if(archtype =='Non Named' || service == 'Others'){
                    component.set("v.TestFieldFlag",true);
                }
                console.log("Hello world!"+component.get("v.TestFieldFlag"));
               // alert(':===service===:'+service+':===archtype==:'+archtype);
            }            
        });
        $A.enqueueAction(action1);
        
    },
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message,
            "duration" :'3000',
            
        });
        toastEvent.fire();
    },  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    
    validateTS : function(component, event, helper) {
        
        var proceedToSave = true;
        var Deal_Administrator = component.find("Deal_Administrator");
        var Deal_AdministratorName = Deal_Administrator.get("v.value");
        if($A.util.isUndefinedOrNull(Deal_AdministratorName) || Deal_AdministratorName == '')
        {
            $A.util.addClass(Deal_Administrator,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Deal_Administrator,'slds-has-error');
        }
        var Named_Advisor = component.find("Named_Advisor");
        var Named_AdvisorName = Named_Advisor.get("v.value");
        if(Deal_AdministratorName != '' && Deal_AdministratorName != 'In-house'){
            if($A.util.isUndefinedOrNull(Named_AdvisorName) || Named_AdvisorName == '')
            {
                $A.util.addClass(Named_Advisor,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Named_Advisor,'slds-has-error');
            }
        }
        
        var Deal_Type = component.find("Deal_Type");
        var Deal_TypeName = Deal_Type.get("v.value");
        if($A.util.isUndefinedOrNull(Deal_TypeName) || Deal_TypeName == '')
        {
            $A.util.addClass(Deal_Type,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Deal_Type,'slds-has-error');
        }
        var Named_Analyst = component.find("Named_Analyst");
        var Named_AnalystName = Named_Analyst.get("v.value");
        if(Named_AdvisorName != ''){
            if($A.util.isUndefinedOrNull(Named_AnalystName) || Named_AnalystName == '')
            {
                $A.util.addClass(Named_Analyst,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Named_Analyst,'slds-has-error');
            }
        }
        /*
        var Bid_pro_support_needed = component.find("Bid_pro_support_needed");
        var Bid_pro_supportName = Bid_pro_support_needed.get("v.value");
        if($A.util.isUndefinedOrNull(Bid_pro_supportName) || Bid_pro_supportName == '')
        {
            $A.util.addClass(Bid_pro_support_needed,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Bid_pro_support_needed,'slds-has-error');
        }
        */
        var Analyst_If_Others = component.find("Analyst_If_Others");
        var Analyst_Name_If_Others = Analyst_If_Others.get("v.value");
        if(Named_AnalystName != '' && Named_AnalystName =='Other' ){
            if($A.util.isUndefinedOrNull(Analyst_Name_If_Others) || Analyst_Name_If_Others == '')
            {
                $A.util.addClass(Analyst_If_Others,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Analyst_If_Others,'slds-has-error');
            }
        }
       /*  var Is_this_is_a_RPA_deal = component.find("Is_this_is_a_RPA_deal");
        var Is_this_is_a_RPA_dealName = Is_this_is_a_RPA_deal.get("v.value");
        if($A.util.isUndefinedOrNull(Is_this_is_a_RPA_dealName) || Is_this_is_a_RPA_dealName == '')
        {
            $A.util.addClass(Is_this_is_a_RPA_deal,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Is_this_is_a_RPA_deal,'slds-has-error');
        }
        /* Alignment to Priorities (at the time of deal creation)  start */
        var Does_the_client_have_a_budget_for_Opp = component.find("What_is_our_track_record_at_this_client");
        var Does_the_client_have_a_budget_for_OppName = Does_the_client_have_a_budget_for_Opp.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_a_budget_for_OppName) || Does_the_client_have_a_budget_for_OppName == '')
        {
            $A.util.addClass(Does_the_client_have_a_budget_for_Opp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Does_the_client_have_a_budget_for_Opp,'slds-has-error');
        }
        var What_is_likely_decision_date = component.find("Does_this_project_align_with_the_client");
        var What_is_likely_decision_dateName = What_is_likely_decision_date.get("v.value");
        if($A.util.isUndefinedOrNull(What_is_likely_decision_dateName) || What_is_likely_decision_dateName == '')
        {
            $A.util.addClass(What_is_likely_decision_date,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_is_likely_decision_date,'slds-has-error');
        }
        
        var Does_the_client_have_compelling_need = component.find("Does_the_client_rate_card_or_proposed_c");
        var Does_the_client_have_compelling_needName = Does_the_client_have_compelling_need.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_compelling_needName) || Does_the_client_have_compelling_needName == '')
        {
            $A.util.addClass(Does_the_client_have_compelling_need,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Does_the_client_have_compelling_need,'slds-has-error');
        }
        
        
        
        /* Alignment to Priorities (at the time of deal creation)  End */
        
        /* Intent to Buy start*/
        
        var Do_we_have_client_references = component.find("Does_the_client_have_an_approved_budget");
        var Do_we_have_client_referencesName = Do_we_have_client_references.get("v.value");
        if($A.util.isUndefinedOrNull(Do_we_have_client_referencesName) || Do_we_have_client_referencesName == '')
        {
            $A.util.addClass(Do_we_have_client_references,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Do_we_have_client_references,'slds-has-error');
        }
        
        var Do_we_have_connect_with_decision_maker = component.find("Is_there_a_clear_client_sponsor_decision");
        var Do_we_have_connect_with_decision_makerName = Do_we_have_connect_with_decision_maker.get("v.value");
        if($A.util.isUndefinedOrNull(Do_we_have_connect_with_decision_makerName) || Do_we_have_connect_with_decision_makerName == '')
        {
            $A.util.addClass(Do_we_have_connect_with_decision_maker,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Do_we_have_connect_with_decision_maker,'slds-has-error');
        }
        
        var Who_is_the_competition_on_this_deal = component.find("Has_the_client_sponsor_decision_maker");
        var Who_is_the_competition_on_this_dealName = Who_is_the_competition_on_this_deal.get("v.value");
        if($A.util.isUndefinedOrNull(Who_is_the_competition_on_this_dealName) || Who_is_the_competition_on_this_dealName == '')
        {
            $A.util.addClass(Who_is_the_competition_on_this_deal,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Who_is_the_competition_on_this_deal,'slds-has-error');
        }
        
        var What_is_the_type_of_Project = component.find("Do_we_understand_the_client");
        var What_is_the_type_of_ProjectName = What_is_the_type_of_Project.get("v.value");
        if($A.util.isUndefinedOrNull(What_is_the_type_of_ProjectName) || What_is_the_type_of_ProjectName == '')
        {
            $A.util.addClass(What_is_the_type_of_Project,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_is_the_type_of_Project,'slds-has-error');
        }
        /*  Intent to Buy End */
        /* Potential to Win start  */
        var What_is_the_nature_of_work = component.find("How_strong_is_our_value_propostion_and_w");
        var What_is_the_nature_of_workName = What_is_the_nature_of_work.get("v.value");
        if($A.util.isUndefinedOrNull(What_is_the_nature_of_workName) || What_is_the_nature_of_workName == '')
        {
            $A.util.addClass(What_is_the_nature_of_work,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_is_the_nature_of_work,'slds-has-error');
        }
        
        var Do_we_have_the_right_relationship_streng = component.find("Do_we_have_the_right_relationship_streng");
        var Do_we_have_the_right_relationship_strengName = Do_we_have_the_right_relationship_streng.get("v.value");
        if($A.util.isUndefinedOrNull(Do_we_have_the_right_relationship_strengName) || Do_we_have_the_right_relationship_strengName == '')
        {
            $A.util.addClass(What_is_the_nature_of_work,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_is_the_nature_of_work,'slds-has-error');
        }
       /* debugger;
        var Do_we_have_the_streng = component.find("In_case_of_non_named_account__c");
        var Do_we_have_the_strengName = Do_we_have_the_streng.get("v.value");
        if($A.util.isUndefinedOrNull(Do_we_have_the_strengName) || Do_we_have_the_strengName == '')
        {
            $A.util.addClass(Do_we_have_the_strengName,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_is_the_nature_of_work,'slds-has-error');
        }
        
       /* var Do_we_have_the_right_domain_knowledge = component.find("In_case_of_non_named_account");
        var Does_the_client_have_compelling_needName = Do_we_have_the_right_domain_knowledge.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_compelling_needName) || Does_the_client_have_compelling_needName == '')
        {
            $A.util.addClass(Do_we_have_the_right_domain_knowledge,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Do_we_have_the_right_domain_knowledge,'slds-has-error');
        }
       
        /* Potential to Win End  */
        
        return proceedToSave;
    },
    
    sendReworkEmailTemplate : function(component,event,helper) {
   
		var action  = component.get('c.sendReworkQSRMEmailTemplate');
        action.setCallback(this,function(response){
            if(response.getState() == "SUCCESS"){
                alert('Mail send ');
            }else{
                alert('Error');
            }
        });
        $A.enqueueAction(action);
	},
    
    validateNonTs : function(component, event, helper) {
        //For Non TS
        var proceedToSave = true;
        var Deal_AdministratorNonTs = component.find("Deal_AdministratorNonTs");
        var Deal_AdministratorNonTsName = Deal_AdministratorNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Deal_AdministratorNonTsName) || Deal_AdministratorNonTsName == '')
        {
            $A.util.addClass(Deal_AdministratorNonTs,'slds-has-error');
            proceedToSave = false;
        }
        
        else
        {
            $A.util.removeClass(Deal_AdministratorNonTs,'slds-has-error');
        }
        
        var Named_AdvisorNonTs = component.find("Named_AdvisorNonTs");
        var Named_AdvisorNonTsName = Named_AdvisorNonTs.get("v.value");
        if(Deal_AdministratorNonTsName != '' && Deal_AdministratorNonTsName != 'In-house'){
            if($A.util.isUndefinedOrNull(Named_AdvisorNonTsName) || Named_AdvisorNonTsName == '')
            {
                $A.util.addClass(Named_AdvisorNonTs,'slds-has-error');
                proceedToSave = false;
            }        
            else
            {
                $A.util.removeClass(Named_AdvisorNonTs,'slds-has-error');
            }
        }
        var Named_AnalystNonTs = component.find("Named_AnalystNonTs");
        var Named_AnalystNonTsName = Named_AnalystNonTs.get("v.value");
        if(Named_AdvisorNonTsName != ''  ){
            if($A.util.isUndefinedOrNull(Named_AnalystNonTsName) || Named_AnalystNonTsName == '')
            {
                $A.util.addClass(Named_AnalystNonTs,'slds-has-error');
                proceedToSave = false;
            }
            
            else
            {
                $A.util.removeClass(Named_AnalystNonTs,'slds-has-error');
            }
        }
        
        var Deal_TypeNonTs = component.find("Deal_TypeNonTs");
        var Deal_TypeNonTsName = Deal_TypeNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Deal_TypeNonTsName) || Deal_TypeNonTsName == '')
        {
            $A.util.addClass(Deal_TypeNonTs,'slds-has-error');
            proceedToSave = false;
        }
        
        else
        {
            $A.util.removeClass(Deal_TypeNonTs,'slds-has-error');
        }
        var Analyst_If_OthersNonTs = component.find("Analyst_If_OthersNonTs");
        var Analyst_If_OthersNonTsName = Analyst_If_OthersNonTs.get("v.value");
        if(Named_AnalystNonTsName != '' && Named_AnalystNonTsName =='Other' ){
            if($A.util.isUndefinedOrNull(Analyst_If_OthersNonTsName) || Analyst_If_OthersNonTsName == '')
            {
                $A.util.addClass(Analyst_If_OthersNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Analyst_If_OthersNonTs,'slds-has-error');
            }
        }
        
       /* var If_not_expected_date_of_NDA_signatureNonTs = component.find("If_not_expected_date_of_NDA_signatureNonTs");
        var If_not_expected_date_of_NDA_signatureNonTsName = If_not_expected_date_of_NDA_signatureNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(If_not_expected_date_of_NDA_signatureNonTsName) || If_not_expected_date_of_NDA_signatureNonTsName == '')
        {
            $A.util.addClass(If_not_expected_date_of_NDA_signatureNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(If_not_expected_date_of_NDA_signatureNonTs,'slds-has-error');
        }
		*/        
        
       /* var CommentsNonTs = component.find("CommentsNonTs");
        var CommentsNonTsName = CommentsNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(CommentsNonTsName) || CommentsNonTsName == '')
        {
            $A.util.addClass(CommentsNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(CommentsNonTs,'slds-has-error');
        }
        */
        var OpportunityNonTs = component.find("OpportunityNonTs");
        var OpportunityNonTsName = OpportunityNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(OpportunityNonTsName) || OpportunityNonTsName == '')
        {
            $A.util.addClass(OpportunityNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(OpportunityNonTs,'slds-has-error');
        }
        
        var Is_NDA_signedNonTs = component.find("Is_NDA_signedNonTs");
        var Is_NDA_signedNonTsName = Is_NDA_signedNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Is_NDA_signedNonTsName) || Is_NDA_signedNonTsName == '')
        {
            $A.util.addClass(Is_NDA_signedNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Is_NDA_signedNonTs,'slds-has-error');
        }
        
        var Functional_SPOCNonTs = component.find("Functional_SPOCNonTs");
        var Functional_SPOCNonTsName = Functional_SPOCNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Functional_SPOCNonTsName) || Functional_SPOCNonTsName == '')
        {
            $A.util.addClass(Functional_SPOCNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Functional_SPOCNonTs,'slds-has-error');
        }
        
        
        
       /* var IncumbentNonTs = component.find("IncumbentNonTs");
        var IncumbentNonTsName = IncumbentNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(IncumbentNonTsName) || IncumbentNonTsName == '')
        {
            $A.util.addClass(IncumbentNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(IncumbentNonTs,'slds-has-error');
        }
        
        var OthersNonTs = component.find("OthersNonTs");
        var OthersNonTsName = OthersNonTs.get("v.value");
        if(IncumbentNonTsName != '' && IncumbentNonTsName =='Others' ){
            if($A.util.isUndefinedOrNull(OthersNonTsName) || OthersNonTsName == '')
            {
                $A.util.addClass(OthersNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(OthersNonTs,'slds-has-error');
            }
        }
        
        var Named_CompetitorsNonTs = component.find("Named_CompetitorsNonTs");
        var Named_CompetitorsNonTsName = Named_CompetitorsNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Named_CompetitorsNonTsName) || Named_CompetitorsNonTsName == '')
        {
            $A.util.addClass(Named_CompetitorsNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Named_CompetitorsNonTs,'slds-has-error');
        }
        var Other_CompetitorNonTs = component.find("Other_CompetitorNonTs");
        var Other_CompetitorNonTsName = Other_CompetitorNonTs.get("v.value");
            if(Named_CompetitorsNonTsName != '' && $A.util.isUndefinedOrNull(Named_CompetitorsNonTsName) == false && Named_CompetitorsNonTsName.indexOf('Others')>-1){
                if($A.util.isUndefinedOrNull(Other_CompetitorNonTsName) || Other_CompetitorNonTsName == '')
                {
                    $A.util.addClass(Other_CompetitorNonTs,'slds-has-error');
                    proceedToSave = false;
                }
                else
                {
                    $A.util.removeClass(Other_CompetitorNonTs,'slds-has-error');
                }
            }*/
        var Due_date_for_bid_submissionNonTs = component.find("Due_date_for_bid_submissionNonTs");
        var Due_date_for_bid_submissionNonTsName = Due_date_for_bid_submissionNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Due_date_for_bid_submissionNonTsName) || Due_date_for_bid_submissionNonTsName == '')
        {
            $A.util.addClass(Due_date_for_bid_submissionNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Due_date_for_bid_submissionNonTs,'slds-has-error');
        }
        
        /*var Date_on_which_RFP_received_by_GenpactNonTs = component.find("Date_on_which_RFP_received_by_GenpactNonTs");
        var Date_on_which_RFP_received_by_GenpactNonTsName = Date_on_which_RFP_received_by_GenpactNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Date_on_which_RFP_received_by_GenpactNonTsName) || Date_on_which_RFP_received_by_GenpactNonTsName == '')
        {
            $A.util.addClass(Date_on_which_RFP_received_by_GenpactNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Date_on_which_RFP_received_by_GenpactNonTs,'slds-has-error');
        }
        */
        var Does_the_client_have_a_compelling_needNonTs = component.find("Does_the_client_have_a_compelling_needNonTs");
        var Does_the_client_have_a_compelling_needNonTsName = Does_the_client_have_a_compelling_needNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_a_compelling_needNonTsName) || Does_the_client_have_a_compelling_needNonTsName == '')
        {
            $A.util.addClass(Does_the_client_have_a_compelling_needNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Does_the_client_have_a_compelling_needNonTs,'slds-has-error');
        }
        
        var Does_the_client_have_a_compelling_needNonTs = component.find("Does_the_client_have_a_compelling_needNonTs");
        var Does_the_client_have_a_compelling_needNonTsName = Does_the_client_have_a_compelling_needNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_a_compelling_needNonTsName) || Does_the_client_have_a_compelling_needNonTsName == '')
        {
            $A.util.addClass(Does_the_client_have_a_compelling_needNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Does_the_client_have_a_compelling_needNonTs,'slds-has-error');
        }
        
        var Does_the_client_have_a_budgetNonTs = component.find("Does_the_client_have_a_budgetNonTs");
        var Does_the_client_have_a_budgetNonTsName = Does_the_client_have_a_budgetNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Does_the_client_have_a_budgetNonTsName) || Does_the_client_have_a_budgetNonTsName == '')
        {
            $A.util.addClass(Does_the_client_have_a_budgetNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Does_the_client_have_a_budgetNonTs,'slds-has-error');
        }
        
        var Is_there_a_client_sponsor_for_the_dealNonTs = component.find("Is_there_a_client_sponsor_for_the_dealNonTs");
        var Is_there_a_client_sponsor_for_the_dealNonTsName = Is_there_a_client_sponsor_for_the_dealNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Is_there_a_client_sponsor_for_the_dealNonTsName) || Is_there_a_client_sponsor_for_the_dealNonTsName == '')
        {
            $A.util.addClass(Is_there_a_client_sponsor_for_the_dealNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Is_there_a_client_sponsor_for_the_dealNonTs,'slds-has-error');
        }
        
        var Decision_timeframe_to_award_the_dealNonTs = component.find("Decision_timeframe_to_award_the_dealNonTs");
        var Decision_timeframe_to_award_the_dealNonTsName = Decision_timeframe_to_award_the_dealNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Decision_timeframe_to_award_the_dealNonTsName) || Decision_timeframe_to_award_the_dealNonTsName == '')
        {
            $A.util.addClass(Decision_timeframe_to_award_the_dealNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Decision_timeframe_to_award_the_dealNonTs,'slds-has-error');
        }
        
        var Strong_bus_case_oppNonTs = component.find("Strong_bus_case_oppNonTs");
        var Strong_bus_case_oppNonTsName = Strong_bus_case_oppNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Strong_bus_case_oppNonTsName) || Strong_bus_case_oppNonTsName == '')
        {
            $A.util.addClass(Strong_bus_case_oppNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Strong_bus_case_oppNonTs,'slds-has-error');
        }
        
        var Is_there_a_match_on_solution_approachNonTs = component.find("Is_there_a_match_on_solution_approachNonTs");
        var Is_there_a_match_on_solution_approachNonTsName = Is_there_a_match_on_solution_approachNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Is_there_a_match_on_solution_approachNonTsName) || Is_there_a_match_on_solution_approachNonTsName == '')
        {
            $A.util.addClass(Is_there_a_match_on_solution_approachNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Is_there_a_match_on_solution_approachNonTs,'slds-has-error');
        }
        
        var What_margin_potential_dealNonTs = component.find("What_margin_potential_dealNonTs");
        var What_margin_potential_dealNonTsName = What_margin_potential_dealNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(What_margin_potential_dealNonTsName) || What_margin_potential_dealNonTsName == '')
        {
            $A.util.addClass(What_margin_potential_dealNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_margin_potential_dealNonTs,'slds-has-error');
        }
        
        var Potential_significant_relationshipNonTs = component.find("Potential_significant_relationshipNonTs");
        var Potential_significant_relationshipNonTsName = Potential_significant_relationshipNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Potential_significant_relationshipNonTsName) || Potential_significant_relationshipNonTsName == '')
        {
            $A.util.addClass(Potential_significant_relationshipNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Potential_significant_relationshipNonTs,'slds-has-error');
        } 
        
        var What_degree_risk_associatedNonTs = component.find("What_degree_risk_associatedNonTs");
        var What_degree_risk_associatedNonTsName = What_degree_risk_associatedNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(What_degree_risk_associatedNonTsName) || What_degree_risk_associatedNonTsName == '')
        {
            $A.util.addClass(What_degree_risk_associatedNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(What_degree_risk_associatedNonTs,'slds-has-error');
        } 
        
        var Industry_expertiseNonTs = component.find("Industry_expertiseNonTs");
        var Industry_expertiseNonTsName = Industry_expertiseNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Industry_expertiseNonTsName) || Industry_expertiseNonTsName == '')
        {
            $A.util.addClass(Industry_expertiseNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Industry_expertiseNonTs,'slds-has-error');
        } 
        var Cap_Do_we_have_references_for_similarNonTs = component.find("Cap_Do_we_have_references_for_similarNonTs");
        var Cap_Do_we_have_references_for_similarNonTsName = Cap_Do_we_have_references_for_similarNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Cap_Do_we_have_references_for_similarNonTsName) || Cap_Do_we_have_references_for_similarNonTsName == '')
        {
            $A.util.addClass(Cap_Do_we_have_references_for_similarNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Cap_Do_we_have_references_for_similarNonTs,'slds-has-error');
        } 
        var Service_line_expertiseNonTs = component.find("Service_line_expertiseNonTs");
        var Service_line_expertiseNonTsName = Service_line_expertiseNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Service_line_expertiseNonTsName) || Service_line_expertiseNonTsName == '')
        {
            $A.util.addClass(Service_line_expertiseNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Service_line_expertiseNonTs,'slds-has-error');
        } 
        var Is_this_a_RFP_or_Sole_sourced_dealNonTs = component.find("Is_this_a_RFP_or_Sole_sourced_dealNonTs");
        var Is_this_a_RFP_or_Sole_sourced_dealNonTsName = Is_this_a_RFP_or_Sole_sourced_dealNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Is_this_a_RFP_or_Sole_sourced_dealNonTsName) || Is_this_a_RFP_or_Sole_sourced_dealNonTsName == '')
        {
            $A.util.addClass(Is_this_a_RFP_or_Sole_sourced_dealNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Is_this_a_RFP_or_Sole_sourced_dealNonTs,'slds-has-error');
        } 
        
        var Who_influenced_the_RfP_requirementsNonTs = component.find("Who_influenced_the_RfP_requirementsNonTs");
        var Who_influenced_the_RfP_requirementsNonTsName = Who_influenced_the_RfP_requirementsNonTs.get("v.value");
        if(Is_this_a_RFP_or_Sole_sourced_dealNonTsName != ''){
            if($A.util.isUndefinedOrNull(Who_influenced_the_RfP_requirementsNonTsName) || Who_influenced_the_RfP_requirementsNonTsName == '')
            {
                $A.util.addClass(Who_influenced_the_RfP_requirementsNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Who_influenced_the_RfP_requirementsNonTs,'slds-has-error');
            } 
        }
        var Knowledge_on_decision_making_processNonTs = component.find("Knowledge_on_decision_making_processNonTs");
        var Knowledge_on_decision_making_processNonTsName = Knowledge_on_decision_making_processNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Knowledge_on_decision_making_processNonTsName) || Knowledge_on_decision_making_processNonTsName == '')
        {
            $A.util.addClass(Knowledge_on_decision_making_processNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Knowledge_on_decision_making_processNonTs,'slds-has-error');
        } 
        
        var Hunting_Mining_picklistNonTs = component.find("Hunting_Mining_picklistNonTs");
        var Hunting_Mining_picklistNonTsName = Hunting_Mining_picklistNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Hunting_Mining_picklistNonTsName) || Hunting_Mining_picklistNonTsName == '')
        {
            $A.util.addClass(Hunting_Mining_picklistNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Hunting_Mining_picklistNonTs,'slds-has-error');
        } 
        var Relationship_with_decision_makersNonTs = component.find("Relationship_with_decision_makersNonTs");
        var Relationship_with_decision_makersNonTsName = Relationship_with_decision_makersNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Relationship_with_decision_makersNonTsName) || Relationship_with_decision_makersNonTsName == '')
        {
            $A.util.addClass(Relationship_with_decision_makersNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Relationship_with_decision_makersNonTs,'slds-has-error');
        } 
        
        var Our_previous_track_record_with_prospectNonTs = component.find("Our_previous_track_record_with_prospectNonTs");
        var Our_previous_track_record_with_prospectNonTsName = Our_previous_track_record_with_prospectNonTs.get("v.value");
        if(Hunting_Mining_picklistNonTsName != '' && Hunting_Mining_picklistNonTsName != 'Mining'){
            if($A.util.isUndefinedOrNull(Our_previous_track_record_with_prospectNonTsName) || Our_previous_track_record_with_prospectNonTsName == '')
            {
                $A.util.addClass(Our_previous_track_record_with_prospectNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Our_previous_track_record_with_prospectNonTs,'slds-has-error');
            } 
        }
        
        var Our_reputation_with_clientNonTs = component.find("Our_reputation_with_clientNonTs");
        var Our_reputation_with_clientNonTsNonTsName = Our_reputation_with_clientNonTs.get("v.value");
        if(Hunting_Mining_picklistNonTsName != '' && Hunting_Mining_picklistNonTsName != 'Hunting'){
            if($A.util.isUndefinedOrNull(Our_reputation_with_clientNonTsNonTsName) || Our_reputation_with_clientNonTsNonTsName == '')
            {
                $A.util.addClass(Our_reputation_with_clientNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Our_reputation_with_clientNonTs,'slds-has-error');
            } 
        }
        
        var Relationship_with_third_party_advisorsNonTs = component.find("Relationship_with_third_party_advisorsNonTs");
        var Relationship_with_third_party_advisorsNonTsName = Relationship_with_third_party_advisorsNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Relationship_with_third_party_advisorsNonTsName) || Relationship_with_third_party_advisorsNonTsName == '')
        {
            $A.util.addClass(Relationship_with_third_party_advisorsNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Relationship_with_third_party_advisorsNonTs,'slds-has-error');
        } 
        
        var Incumbent_strengthNonTs = component.find("Incumbent_strengthNonTs");
        var Incumbent_strengthNonTsName = Incumbent_strengthNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Incumbent_strengthNonTsName) || Incumbent_strengthNonTsName == '')
        {
            $A.util.addClass(Incumbent_strengthNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Incumbent_strengthNonTs,'slds-has-error');
        } 
        var Can_we_build_a_differentiated_solutionNonTs = component.find("Can_we_build_a_differentiated_solutionNonTs");
        var Can_we_build_a_differentiated_solutionNonTsName = Can_we_build_a_differentiated_solutionNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Can_we_build_a_differentiated_solutionNonTsName) || Can_we_build_a_differentiated_solutionNonTsName == '')
        {
            $A.util.addClass(Can_we_build_a_differentiated_solutionNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Can_we_build_a_differentiated_solutionNonTs,'slds-has-error');
        } 
        
        var Client_involved_in_value_discoveryNonTs = component.find("Client_involved_in_value_discoveryNonTs");
        var Client_involved_in_value_discoveryNonTsName = Client_involved_in_value_discoveryNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Client_involved_in_value_discoveryNonTsName) || Client_involved_in_value_discoveryNonTsName == '')
        {
            $A.util.addClass(Client_involved_in_value_discoveryNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Client_involved_in_value_discoveryNonTs,'slds-has-error');
        } 
        
        var Rebadge_DealNonTs = component.find("Rebadge_DealNonTs");
        var Rebadge_DealNonTsName = Rebadge_DealNonTs.get("v.value");
        if($A.util.isUndefinedOrNull(Rebadge_DealNonTsName) || Rebadge_DealNonTsName == '')
        {
            $A.util.addClass(Rebadge_DealNonTs,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(Rebadge_DealNonTs,'slds-has-error');
        } 
        
        var Rebadge_TriggerNonTs = component.find("Rebadge_TriggerNonTs");
        var Rebadge_TriggerNonTsName = Rebadge_TriggerNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Rebadge_TriggerNonTsName) || Rebadge_TriggerNonTsName == '')
            {
                $A.util.addClass(Rebadge_TriggerNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Rebadge_TriggerNonTs,'slds-has-error');
            } 
        }
        
        var Existing_forums_of_employee_representatiNonTs = component.find("Existing_forums_of_employee_representatiNonTs");
        var Existing_forums_of_employee_representatiNonTsName = Existing_forums_of_employee_representatiNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Existing_forums_of_employee_representatiNonTsName) || Existing_forums_of_employee_representatiNonTsName == '')
            {
                $A.util.addClass(Existing_forums_of_employee_representatiNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Existing_forums_of_employee_representatiNonTs,'slds-has-error');
            } 
        }
        
        var Cost_per_FTENonTs = component.find("Cost_per_FTENonTs");
        var Cost_per_FTENonTsName = Cost_per_FTENonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Cost_per_FTENonTsName) || Cost_per_FTENonTsName == '')
            {
                $A.util.addClass(Cost_per_FTENonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Cost_per_FTENonTs,'slds-has-error');
            } 
        }
        var What_are_facility_Options_post_RebadgeNonTs = component.find("What_are_facility_Options_post_RebadgeNonTs");
        var What_are_facility_Options_post_RebadgeNonTsName = What_are_facility_Options_post_RebadgeNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(What_are_facility_Options_post_RebadgeNonTsName) || What_are_facility_Options_post_RebadgeNonTsName == '')
            {
                $A.util.addClass(What_are_facility_Options_post_RebadgeNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(What_are_facility_Options_post_RebadgeNonTs,'slds-has-error');
            } 
        }
        var Client_Fully_Loaded_Costs_in_KNonTs = component.find("Client_Fully_Loaded_Costs_in_KNonTs");
        var Client_Fully_Loaded_Costs_in_KNonTsName = Client_Fully_Loaded_Costs_in_KNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Client_Fully_Loaded_Costs_in_KNonTsName) || Client_Fully_Loaded_Costs_in_KNonTsName == '')
            {
                $A.util.addClass(Client_Fully_Loaded_Costs_in_KNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Client_Fully_Loaded_Costs_in_KNonTs,'slds-has-error');
            } 
        }
        var Existing_Genpact_site_in_the_countryNonTs = component.find("Existing_Genpact_site_in_the_countryNonTs");
        var Existing_Genpact_site_in_the_countryNonTsName = Existing_Genpact_site_in_the_countryNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Existing_Genpact_site_in_the_countryNonTsName) || Existing_Genpact_site_in_the_countryNonTsName == '')
            {
                $A.util.addClass(Existing_Genpact_site_in_the_countryNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Existing_Genpact_site_in_the_countryNonTs,'slds-has-error');
            } 
        }
        var What_is_G_value_Proposition_on_PursuingNonTs = component.find("What_is_G_value_Proposition_on_PursuingNonTs");
        var What_is_G_value_Proposition_on_PursuingNonTsName = What_is_G_value_Proposition_on_PursuingNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(What_is_G_value_Proposition_on_PursuingNonTsName) || What_is_G_value_Proposition_on_PursuingNonTsName == '')
            {
                $A.util.addClass(What_is_G_value_Proposition_on_PursuingNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(What_is_G_value_Proposition_on_PursuingNonTs,'slds-has-error');
            } 
        }
        
        var Can_Premise_be_shared_to_other_customersNonTs = component.find("Can_Premise_be_shared_to_other_customersNonTs");
        var Can_Premise_be_shared_to_other_customersNonTsName = Can_Premise_be_shared_to_other_customersNonTs.get("v.value");
        if(Rebadge_DealNonTsName != '' && Rebadge_DealNonTsName != 'No'){
            if($A.util.isUndefinedOrNull(Can_Premise_be_shared_to_other_customersNonTsName) || Can_Premise_be_shared_to_other_customersNonTsName == '')
            {
                $A.util.addClass(Can_Premise_be_shared_to_other_customersNonTs,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(Can_Premise_be_shared_to_other_customersNonTs,'slds-has-error');
            } 
        }
        
        return proceedToSave;
    },
    
    fireToastEvent : function(component, event, helper, message) {
        var toastEvent = $A.get("e.force:showToast");    
        toastEvent.setParams({
            "title": "error",
            "message": message,
            "type": "error"
        });
        toastEvent.fire();
    },
})