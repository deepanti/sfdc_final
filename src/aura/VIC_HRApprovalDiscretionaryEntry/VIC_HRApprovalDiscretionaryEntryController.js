({
	save : function(component, event, helper) {
        component.set("v.isOpenDiscPopup",false);
        var mapOfNewDiscretionaryRecord = component.get("v.mapOfNewDiscretionaryRecord") || {};
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper");
        var selectedIncentiveRecordIndex = component.get("v.selectedIncentiveRecordIndex");
        var incentiveRecord = JSON.parse(JSON.stringify(component.get("v.incentiveRecord")));
        if(mapOfNewDiscretionaryRecord[selectedIncentiveRecordIndex] && lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount != incentiveRecord.DiscretionaryAmount) {
            lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount = lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount - mapOfNewDiscretionaryRecord[selectedIncentiveRecordIndex].DiscretionaryAmount + incentiveRecord.DiscretionaryAmount ;
            lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].Comments = incentiveRecord.Comments;
            mapOfNewDiscretionaryRecord[selectedIncentiveRecordIndex] = incentiveRecord;
        } else if(lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount != incentiveRecord.DiscretionaryAmount) {
            lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount = lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].DiscretionaryAmount + incentiveRecord.DiscretionaryAmount ;
            lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].Comments = incentiveRecord.Comments;
            mapOfNewDiscretionaryRecord[selectedIncentiveRecordIndex] = incentiveRecord;
        }
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
		component.set("v.mapOfNewDiscretionaryRecord", mapOfNewDiscretionaryRecord);
        
    },
    closeDiscModel:function(component, event, helper) {
        component.set("v.isOpenDiscPopup", false);
    }
})