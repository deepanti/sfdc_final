({
	myAction : function(component, event, helper) {
		
	},
    doInit : function(component, event, helper) {
		helper.initAllDataWrap(component,event,helper);
        helper.initCaseDataWrap(component,event,helper);
        helper.initHrApprovedDataWrap(component,event,helper);
	},
    selectNUnselectAll : function(component, event, helper) {
		var statusOfSelectAll = event.getSource().get("v.value");
        var evtName = event.getSource().get("v.name");
        var userDmclDataWrap = component.get("v.lstDomicileUserDataWrapLine");
        for(var objDomicile in userDmclDataWrap){
            if(evtName == userDmclDataWrap[objDomicile].strDomicile){
                for(var eachUser in userDmclDataWrap[objDomicile].lstUserOppData){
                    userDmclDataWrap[objDomicile].lstUserOppData[eachUser].isUserChecked = statusOfSelectAll;
                    for(var eachOppData in userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap){
                        userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
                        for(var eachOLIData in userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                            userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
                        }
                    } 
                }
            }
        }
        component.set("v.lstDomicileUserDataWrapLine",userDmclDataWrap);
	},
    approveNSubmit : function(component, event, helper) {
        component.set("v.showModal",true);
    },   
     
    onButtonClickHandler : function(component, event, helper) {
		
        var checkOliCount = 0;
        var reqOliCheckErrCount = 0;
        var reqCommCountErrCount = 0;
        var userDmclDataWrap = component.get("v.lstDomicileUserDataWrapLine");
        for(var objDomicile in userDmclDataWrap){
            for(var eachUser in userDmclDataWrap[objDomicile].lstUserOppData){
                for(var eachOppData in userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap){
                    for(var eachOLIData in userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap){
                        if(userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'VIC Team - Rejected' && 
                           (userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment == '' || 
                            userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment == null)){
                            userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = true;
                            //reqCommCountErrCount++; @Commented because now comments are not required for rejecting OLI
                        }else{
                            userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isCommentRequired = false;
                        }
                        if(userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strActionStatus == 'VIC Team - Rejected' && 
                           (userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment != '' || 
                            userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment != null) && 
                          	!userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                            reqOliCheckErrCount++;
                        }
                        if(userDmclDataWrap[objDomicile].lstUserOppData[eachUser].lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                            checkOliCount++;
                        }
                    }
                } 
                if(userDmclDataWrap[objDomicile].lstUserOppData[eachUser].isUserChecked){
                    checkOliCount++;
                }
            }
        }
        component.set("v.lstDomicileUserDataWrapLine",userDmclDataWrap);
        if(reqCommCountErrCount != 0){
            var strMSG = 'Comment Required For ON Hold Incentive';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(reqOliCheckErrCount != 0){
            var strMSG = 'Opportunity Product Need To Be Checked';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount == 0){
            var strMSG = 'At Least One Opportunity Product Need To Be Checked';
            var strType = 'Error';
            helper.showToastMsg(component,event,helper,strMSG,strType);
        }else if(checkOliCount != 0){
            helper.approveNSubmitHelper(component,event,helper);
        }
    },
    
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
    },
    
    searchNOrder : function(component, event, helper) {
		var searchObjUser = component.get("v.searchUser");
        var userDmclDataWrap = component.get("v.lstDomicileUserDataWrapLine");
        for(var objDomicile in userDmclDataWrap){
            for (var index = 0; index < userDmclDataWrap[objDomicile].lstUserOppData.length; index++){
                 if (userDmclDataWrap[objDomicile].lstUserOppData[index].objUser.Name && searchObjUser && 
                    userDmclDataWrap[objDomicile].lstUserOppData[index].objUser.Name.toLowerCase().includes(searchObjUser.toLowerCase())) {
                    var filteredIncentiveRecord = userDmclDataWrap[objDomicile].lstUserOppData[index];
                    userDmclDataWrap[objDomicile].lstUserOppData.splice(index, 1);
                    userDmclDataWrap[objDomicile].lstUserOppData.unshift(filteredIncentiveRecord);  
                }
            }
        }
        component.set("v.lstDomicileUserDataWrapLine",userDmclDataWrap);
	},
    searchCaseUserNOrder : function(component, event, helper) {
		var searchObjUser = component.get("v.searchOnHoldUser");
        var userDmclDataWrap = component.get("v.lstDomicileUserDataWrapCaseLine");
        for(var objDomicile in userDmclDataWrap){
            for (var index = 0; index < userDmclDataWrap[objDomicile].lstUserOppData.length; index++){
                 if (userDmclDataWrap[objDomicile].lstUserOppData[index].objUser.Name && searchObjUser && 
                    userDmclDataWrap[objDomicile].lstUserOppData[index].objUser.Name.toLowerCase().includes(searchObjUser.toLowerCase())) {
                    var filteredIncentiveRecord = userDmclDataWrap[objDomicile].lstUserOppData[index];
                    userDmclDataWrap[objDomicile].lstUserOppData.splice(index, 1);
                    userDmclDataWrap[objDomicile].lstUserOppData.unshift(filteredIncentiveRecord);  
                }
            }
        }
        component.set("v.lstDomicileUserDataWrapCaseLine",userDmclDataWrap);
	}
    
})