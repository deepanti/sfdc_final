({
	helperMethod : function() {
		
	},
    initAllDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");
        cmp.set("v.isSpinnerVisible",true);
        action.setParams({
            "casePrint" : 'false',
            "strVicTeamStatus" : 'VIC Team - Pending'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstDomicileUserDataWrapLine",resp.getReturnValue());
            }
            cmp.set("v.isSpinnerVisible",false);
        });
        $A.enqueueAction(action);
	},
    initCaseDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");
        action.setParams({
            "casePrint" : 'true',
            "strVicTeamStatus" : 'VIC Team - Pending'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstDomicileUserDataWrapCaseLine",resp.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);
	},
    initHrApprovedDataWrap : function(cmp,evt,help) {
		var action = cmp.get("c.getInitLoadPageData");
        action.setParams({
            "casePrint" : 'false',
            "strVicTeamStatus" : 'HR - Approved'
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                cmp.set("v.lstDomicileHRAppUserDataWrapLine",resp.getReturnValue());
            }
            
        });
        $A.enqueueAction(action);
	},
    approveNSubmitHelper : function(component, event, helper) {
        var lstUserOppWrapData = component.get("v.lstDomicileUserDataWrapLine");
		var action = component.get("c.approvSubmitIncentive");
        component.set("v.showModal",false);
        action.setParams({
            "strUserOppDataWrpa" : JSON.stringify(lstUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                $A.get('e.force:refreshView').fire();
                /* this is toast message while approve opportunity*/     
                var sMsg = 'Records have been processed successfully.';
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: 'dismissible',
                    duration:' 500',
                    message: sMsg,
                    type : 'success'
                });
                toastEvent.fire();
            }else{
                //alert(state);
                var sMsg = 'Records have not been processed successfully.'
                this.showToastMsg(component.event, helper, sMsg, 'error');
            }
        });
        $A.enqueueAction(action);
	},
    showToastMsg : function(component, event, helper, strMSG, strType) {
		component.set("v.showModal",false);
        var sMsg = strMSG;
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: 'dismissible',
            duration:' 500',
            message: sMsg,
            type : strType
        });
        toastEvent.fire();
	}
})