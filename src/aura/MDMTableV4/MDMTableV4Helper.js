({
    fetchField : function(component, event, helper) {
        try{
            var totalRecords ;
            var totalContacts = 0;
            var totalLeads = 0;
            
            var action = component.get("c.getData");
            action.setCallback(this, function(response){  
                var index;
                if(response.getState() == 'SUCCESS'){ 
                    var retLst=response.getReturnValue();
                    component.set("v.dataList",retLst);
                    
                    for(index in retLst){
                        var lst = retLst[index];
                        if(lst.RecordTypeId=="Contact"){
                            totalContacts = totalContacts + 1;
                             component.set("v.TotalContactVal", totalContacts);
                        }else{
                            totalLeads = totalLeads + 1;
                            component.set("v.TotalLeadVal", totalLeads);
                        }
                    }
                    
                    if(retLst != null){
                    totalRecords = retLst.length;
                    }
                    component.set("v.TotalRecordVal", totalRecords);
                    
                }else{
                    alert('Error In Getting Data');
                }
            });
            $A.enqueueAction(action);
        }
        catch(err) {
            alert(err.message);
        }
    },
    
    getopportunityFromWrapperList : function(component, page)
    {
        var productWrapperList = [];
        var req_list = [];
        var pageSize = component.get("v.pageSize");
        var totalList = component.get("v.queryList");
       if(page === 1){
            req_list = totalList.slice(0, pageSize); 
        }
        else{
            var offset = (+page -1) * (+pageSize);
            req_list = totalList.slice(offset , (+pageSize * +page));    
        }
        component.set("v.page",page);
        component.set("v.dataList", req_list);
        component.set("v.pages",Math.ceil(totalList.length/component.get("v.pageSize")));
       
     },
    
})