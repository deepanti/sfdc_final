({
    doInit : function(component, event, helper) {
        document.body.setAttribute('style', 'overflow-y: hidden !important; overflow-x: hidden;');
        
        
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var URLfields = sPageURL.split('?');
        var field1=URLfields[0];
        var field2=URLfields[1];
        var field3=URLfields[2];
        var field4=URLfields[3];
        var field5=URLfields[4];
        var field6=URLfields[5];
        var field7=URLfields[6];
        var field8=URLfields[7];
        var field9=URLfields[8];
        
        var field1Flag = false;
        var field2Flag = false;
        var field3Flag = false;
        var field4Flag = false;
        var field5Flag = false;
        var field6Flag = false;
        var field7Flag = false;
        var field8Flag = false;
        var field9Flag = false;
        if(URLfields.length > 0 && field1 != null || field1 != ''  || field2 != null || field2 != '' || field3 != null || field3 != '' || field4 != null || field4 != '' || field5 != null || field5 != '' || field6 != null || field6 != '' || field7 != null || field7 != '' || field8 != null || field8 != '' || field9 != null || field9 != ''){
            
            if(typeof(field1) !== 'undefined' && field1 != ''){
                component.set("v.levelVal",field1);
                field1Flag = true;     
                var LevelSpan = component.find('level-span');
                $A.util.removeClass(LevelSpan, 'beforeSelectionColour');
                component.set("v.levelText",field1.toString()); 
            }
            if(typeof(field2) !== 'undefined'  && field2 != ''){
                component.set("v.industryVal",field2);
                field2Flag = true;   
                var IndustrySpan = component.find('Industry-span');
                $A.util.removeClass(IndustrySpan, 'beforeSelectionColour');
                component.set("v.industryText",field2.toString()); 
            }
            if(typeof(field3) !== 'undefined' && field3 != ''){
                component.set("v.statusVal",field3);
                field3Flag = true;
                var StatusSpan = component.find('status-span');
                $A.util.removeClass(StatusSpan, 'beforeSelectionColour');
                component.set("v.statusText",field3.toString());
            }
            if(typeof(field4) !== 'undefined' && field4 != ''){
                component.set("v.buyVal",field4);
                field4Flag = true;
                var BuySpan = component.find('buy-span');
                $A.util.removeClass(BuySpan, 'beforeSelectionColour');
                component.set("v.buyText",field4.toString());    
            }
            if(typeof(field5) !== 'undefined' && field5 != ''){
                component.set("v.archetypeVal",field5);
                field5Flag = true;
                var ArchSpan = component.find('arch-span');
                $A.util.removeClass(ArchSpan, 'beforeSelectionColour');
                component.set("v.archetypeText",field5.toString()); 
            }
            if(typeof(field6) !== 'undefined' && field6 != ''){
                component.set("v.RecordTypeValue",field6);
                field6Flag = true;
                var RecordSpan = component.find('Record-span');
                $A.util.removeClass(RecordSpan, 'beforeSelectionColour');
                component.set("v.recordText",field6.toString()); 
            }
            if(typeof(field7) !== 'undefined' && field7 != ''){
                component.set("v.scoreValue",field7);
                field7Flag = true;
                var ScoreSpan = component.find('score-span');
                $A.util.removeClass(ScoreSpan, 'beforeSelectionColour');
                component.set("v.scoreText",field7.toString()); 
                
            }
            if(typeof(field8) !== 'undefined' && field8 != ''){
                component.set("v.countryVal",field8); 
                field8Flag = true;
                var CountrySpan = component.find('country-span');
                $A.util.removeClass(CountrySpan, 'beforeSelectionColour');
                component.set("v.countryText",field8.toString()); 
            }
            if(typeof(field9) !== 'undefined' && field9 != ''){
                component.set("v.titleVal",field9);
                field9Flag = true;
            }
            if(field1Flag || field2Flag || field3Flag || field4Flag || field5Flag || field6Flag || field7Flag || field8Flag || field9Flag){
                var a = component.get('c.queryData');
                $A.enqueueAction(a);
            }
        }
        
        var action = component.get("c.getFieldValues");
        action.setCallback(this, function(response){  
            if(response.getState() == 'SUCCESS'){ 
                
                var wraperList = response.getReturnValue();
                component.set("v.industrywrapper", wraperList.IndustryVerticalList);
                component.set("v.countrywrapper", wraperList.CountryList);
                component.set("v.levelwrapper", wraperList.LevelOfContactList);
                component.set("v.archetypewrapper", wraperList.ArchetypeList);
                component.set("v.buyingCenterwrapper", wraperList.BuyingCenterList);
                component.set("v.statuswrapper", wraperList.wStatusList);
                component.set("v.CurrentUserName", wraperList.CurrentUserName);
            }else{
                
            }
            
        });
        $A.enqueueAction(action); 
    },
    
    ExportData : function(component, event,helper){
        var titleselect;
        var countryselect = component.get("v.countryVal").toString();
        var industryselect = component.get("v.industryVal").toString();
        var levelselect = component.get("v.levelVal").toString();
        var statusselect = component.get("v.statusVal").toString();
        var buyselect = component.get("v.buyVal").toString();
        var archselect = component.get("v.archetypeVal").toString();
        var recordselect = component.get("v.RecordTypeValue").toString();
        var scoreselect = component.get("v.scoreValue").toString();
        var queryList = component.get("v.dataList");
        var titleselectvlue = component.get("v.titleVal");
        if( titleselectvlue != null && titleselectvlue !='' && typeof(titleselectvlue) !== 'undefined' && titleselectvlue !== 'undefined'){
            titleselect = titleselectvlue;
        }
        
        var action1 = component.get("c.DataExport");
        action1.setParams({ 
            Industry : industryselect,
            Level : levelselect,
            Status : statusselect,
            Buying : buyselect,
            Archetype : archselect,
            RecordType : recordselect,
            LeadScore : scoreselect,
            Title : titleselect,
            Country : countryselect
            
        });
        action1.setCallback(this, function(response){
        });
        $A.enqueueAction(action1); 
    },
    
    queryData : function(component, event,helper){
        
        var page = page || 1;
        var totalRecords ;
        var totalContacts=0;
        var totalLeads=0;
        
        var titleselect;
        var countryselect = component.get("v.countryVal").toString();
        var industryselect = component.get("v.industryVal").toString();
        var levelselect = component.get("v.levelVal").toString();
        var statusselect = component.get("v.statusVal").toString();
        var buyselect = component.get("v.buyVal").toString();
        var archselect = component.get("v.archetypeVal").toString();
        var recordselect = component.get("v.RecordTypeValue").toString();
        var scoreselect = component.get("v.scoreValue").toString();
        var queryList = component.get("v.dataList");
        var titleselectvlue = component.get("v.titleVal");
        if( titleselectvlue != null && titleselectvlue !='' && typeof(titleselectvlue) !== 'undefined' && titleselectvlue !== 'undefined'){
            titleselect = titleselectvlue;
        }
        
        //UserId For Excel Export
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var UserIdLabel = $A.get("$Label.c.MDMTableUserForExport");
        var ReportLinkLabel = $A.get("$Label.c.MDM_Report_LInk");
        var UserIdLabelString = (JSON.stringify(UserIdLabel)).includes(userId, 1); 
        if(UserIdLabelString){
            component.set("v.ShowButtonFlag", true);
        }else{
            component.set("v.ShowButtonFlag", false);
        }
        //user Id Ends
        
        
        
        component.set("v.TotalContactVal", 0);
        component.set("v.TotalLeadVal", 0);
        component.set("v.TotalRecordVal", 0);
        component.set("v.TotalDisplayRecordVal", 0);
        
        ///URL For Mail
        component.set("v.ShowMailLink",true);
        if( typeof(titleselect) !== 'undefined'){
            var UrlParams = ReportLinkLabel+"?"+levelselect+"?"+industryselect+"?"+statusselect+"?"+buyselect+"?"+archselect+"?"+recordselect+"?"+scoreselect+"?"+countryselect+"?"+titleselect;
        }else{
            var UrlParams = ReportLinkLabel+"?"+levelselect+"?"+industryselect+"?"+statusselect+"?"+buyselect+"?"+archselect+"?"+recordselect+"?"+scoreselect+"?"+countryselect;
        }
        component.set("v.URLParams",UrlParams);
        
        var ReportForwardEmailLabel = $A.get("$Label.c.MDMTableReportForwardEmail");
        var ReportForwardEmailccLabel = $A.get("$Label.c.MDMTableReportForwardEmailcc");
        var mailbodyContent1="Hi,%0D%0A%0D%0APlease find the MDM Report link below.";
        var Currentusername = component.get("v.CurrentUserName");
        var mailbodyContent2="%0D%0A%0D%0A%0D%0AThanks%0D"+Currentusername;
        var hrefval = "mailto:"+ReportForwardEmailLabel+"?Subject=MDM Report Link&cc="+ReportForwardEmailccLabel+"&body="+mailbodyContent1+"%0D%0A%0D%0A!Important : you must be already logged into salesforce before using the below URL.%0DLink : "+"\""+UrlParams+"\""+mailbodyContent2;
        component.set("v.hrefval",hrefval);
        ///URL
        
        var action1 = component.get("c.getDataCount");
        action1.setParams({ 
            Industry : industryselect,
            Level : levelselect,
            Status : statusselect,
            Buying : buyselect,
            Archetype : archselect,
            RecordType : recordselect,
            LeadScore : scoreselect,
            Title : titleselect,
            Country : countryselect
            
        });
        action1.setCallback(this, function(response){
            var index1;
            if(response.getState() == 'SUCCESS'){  
                var CountLst=response.getReturnValue();
                
                
                for(index1 in CountLst){
                    var lst1 = CountLst[index1];
                    var TotalContactsCount = lst1.TotalContactsCount;
                    component.set("v.TotalContactVal", TotalContactsCount);
                    
                    var TotalLeadsCount = lst1.TotalLeadsCount;
                    component.set("v.TotalLeadVal", TotalLeadsCount);
                    
                    var TotalRecordsCount = lst1.TotalRecordsCount;
                    component.set("v.TotalRecordVal", TotalRecordsCount);
                    if(TotalRecordsCount != 0){
                        component.set("v.ShowNote",true);
                    }
                    
                }              
                
                
            }else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert('==================Error=================='+ errors[0].message);
                        
                    }
                } else {
                    alert('Unknown error');
                }
                
            }
        });
        $A.enqueueAction(action1); 
        
        
        var action = component.get("c.getData");
        action.setParams({ 
            Industry : industryselect,
            Level : levelselect,
            Status : statusselect,
            Buying : buyselect,
            Archetype : archselect,
            RecordType : recordselect,
            LeadScore : scoreselect,
            Title : titleselect,
            Country : countryselect
            
        });
        action.setCallback(this, function(response){  
            var index;
            if(response.getState() == 'SUCCESS'){  
                var retLst=response.getReturnValue();
                component.set("v.page",page);
                component.set("v.total",retLst.length);
                component.set("v.pages",Math.ceil(retLst.length/component.get("v.pageSize")));
                component.set("v.queryList",retLst);
                helper.getopportunityFromWrapperList(component, page);
                if(retLst.length == 0){
                    component.set("v.ListLength",true);
                } else{
                    component.set("v.ListLength",false);
                }
                for(index in retLst){
                    var lst = retLst[index];
                    if(lst.RecordTypeId=="Contact"){
                        totalContacts = totalContacts + 1;
                        
                    }else{
                        totalLeads = totalLeads + 1;
                        
                    }
                }
                if(retLst != null){
                    totalRecords = retLst.length;
                    component.set("v.TotalDisplayRecordVal", totalRecords);
                }else{
                    totalRecords = 0;
                    component.set("v.TotalDisplayRecordVal", totalRecords);
                    
                }
            }else{
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert('==================Error=================='+ errors[0].message);
                        
                    }
                } else {
                    alert('Unknown error');
                }
                
            }
        });
        $A.enqueueAction(action); 
    },
    
    clearFilter : function(component, event, helper) {
        location.reload();
    },
    
    industryhandleClick: function(component, event, helper) {
        var industryDiv = component.find('industry-div');
        $A.util.addClass(industryDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    industryhandleMouseLeave : function(component, event, helper) {
        var industryDiv = component.find('industry-div');
        $A.util.removeClass(industryDiv, 'slds-is-open');
        var selectValue = component.get("v.industryVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var IndustrySpan = component.find('Industry-span');
            $A.util.removeClass(IndustrySpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var IndustrySpan = component.find('Industry-span');
            $A.util.addClass(IndustrySpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.industryText",val); 
    },
    levelhandleClick: function(component, event, helper) {
        var levelDiv = component.find('level-div');
        $A.util.addClass(levelDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    levelhandleMouseLeave : function(component, event, helper) {
        var levelDiv = component.find('level-div');
        $A.util.removeClass(levelDiv, 'slds-is-open');
        var selectValue = component.get("v.levelVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var LevelSpan = component.find('level-span');
            $A.util.removeClass(LevelSpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var LevelSpan = component.find('level-span');
            $A.util.addClass(LevelSpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.levelText",val); 
    },
    statushandleClick: function(component, event, helper) {
        var statusDiv = component.find('status-div');
        $A.util.addClass(statusDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    statushandleMouseLeave : function(component, event, helper) {
        var statusDiv = component.find('status-div');
        $A.util.removeClass(statusDiv, 'slds-is-open');
        var selectValue = component.get("v.statusVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var StatusSpan = component.find('status-span');
            $A.util.removeClass(StatusSpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var StatusSpan = component.find('status-span');
            $A.util.addClass(StatusSpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.statusText",val); 
    },
    buyhandleClick: function(component, event, helper) {
        var buyDiv = component.find('buy-div');
        $A.util.addClass(buyDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    buyhandleMouseLeave : function(component, event, helper) {
        var buyDiv = component.find('buy-div');
        $A.util.removeClass(buyDiv, 'slds-is-open');
        var selectValue = component.get("v.buyVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var BuySpan = component.find('buy-span');
            $A.util.removeClass(BuySpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var BuySpan = component.find('buy-span');
            $A.util.addClass(BuySpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.buyText",val); 
    },
    archhandleClick: function(component, event, helper) {
        var archDiv = component.find('arch-div');
        $A.util.addClass(archDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    archhandleMouseLeave : function(component, event, helper) {
        var archDiv = component.find('arch-div');
        $A.util.removeClass(archDiv, 'slds-is-open');
        var selectValue = component.get("v.archetypeVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var ArchSpan = component.find('arch-span');
            $A.util.removeClass(ArchSpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var ArchSpan = component.find('arch-span');
            $A.util.addClass(ArchSpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.archetypeText",val); 
    },
    
    countryhandleClick: function(component, event, helper) {
        var cntryDiv = component.find('country-div');
        $A.util.addClass(cntryDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    countryhandleMouseLeave : function(component, event, helper) {
        var cntryDiv = component.find('country-div');
        $A.util.removeClass(cntryDiv, 'slds-is-open');
        var selectValue = component.get("v.countryVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var CountrySpan = component.find('country-span');
            $A.util.removeClass(CountrySpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var CountrySpan = component.find('country-span');
            $A.util.addClass(CountrySpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.countryText",val); 
    },
    
    recordhandleClick: function(component, event, helper) {
        var recordDiv = component.find('record-div');
        $A.util.addClass(recordDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    recordhandleMouseLeave : function(component, event, helper) {
        var recordDiv = component.find('record-div');
        $A.util.removeClass(recordDiv, 'slds-is-open');
        var selectValue = component.get("v.RecordTypeValue")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var RecordSpan = component.find('Record-span');
            $A.util.removeClass(RecordSpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var RecordSpan = component.find('Record-span');
            $A.util.addClass(RecordSpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.recordText",val); 
    },
    scorehandleClick: function(component, event, helper) {
        var scoreDiv = component.find('score-div');
        $A.util.addClass(scoreDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    scorehandleMouseLeave : function(component, event, helper) {
        var scoreDiv = component.find('score-div');
        $A.util.removeClass(scoreDiv, 'slds-is-open');
        var selectValue = component.get("v.scoreValue")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var ScoreSpan = component.find('score-span');
            $A.util.removeClass(ScoreSpan, 'beforeSelectionColour');
        }else {
            val = 'All';
            var ScoreSpan = component.find('score-span');
            $A.util.addClass(ScoreSpan, 'beforeSelectionColour');
            event.preventDefault();
        }
        component.set("v.scoreText",val); 
    }, 
    
    LabelAntiEnter: function(component, event, helper) {
        if(event.which == 13) {
            var industryDiv = component.find('industry-div');
            $A.util.removeClass(industryDiv, 'slds-is-open');
            event.preventDefault();
            return false;
        } 
    },
    
    RecordPerpageClick: function(component, event, helper) {
        var RecordPerpageDiv = component.find('RecordPerpage-div');
        $A.util.addClass(RecordPerpageDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    RecordPerpageMouseLeave : function(component, event, helper) {
        var RecordPerpageDiv = component.find('RecordPerpage-div');
        $A.util.removeClass(RecordPerpageDiv, 'slds-is-open');
        
    }, 
    RecordPerpageMouseLeavedropdown : function(component, event, helper) {
        var selectValue = component.get("v.recordsPerpageValue")
        var val;
        if(selectValue != null && selectValue != ''){
            var page = page || 1;
            val = selectValue.toString();
            component.set("v.pageSize",val);
            helper.getopportunityFromWrapperList(component,page);
            var a = component.get('c.pageChange');
            $A.enqueueAction(a);
        }else {
            
            val = '500';
        }
        component.set("v.recordsPerpageText",val); 
    },
    pageChange : function(component, event, helper) 
    {   
        
        var page = component.get("v.page") || 1;
        var pages = component.get("v.pages");
        var direction = event.getParam("direction");
        
        if(direction === "previous"){
            page =  page - 1;
            helper.getopportunityFromWrapperList(component,page);
        }
        else if(direction === "next"){
            page = page + 1;  
            helper.getopportunityFromWrapperList(component,page);
        }
            else if(direction === "first"){
                page = 1;    
                helper.getopportunityFromWrapperList(component,page);
            }
                else if(direction === "last"){
                    page = pages; 
                    helper.getopportunityFromWrapperList(component,page);
                }       
        component.set("v.selectAllFlag", false);
    },
    
    AllRecordtypeChange: function(component, event, helper) {
        var allrec = component.get("v.RecordTypeValue").toString();
        if(allrec == 'All'){
            
        }
    },
     // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // making Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
    
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // making Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    }
})