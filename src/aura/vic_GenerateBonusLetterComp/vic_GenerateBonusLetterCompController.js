({
	doInit : function(component, event, helper) {
		helper.getFinancialYear(component);
        helper.getYear(component);
		        
	},
    onVICRoleSelect : function(component, event, helper) {
    	console.log(component.get("v.VicRole"));        
	},
    getBonusData : function(component, event, helper) {
		helper.getBonusData(component);
	},
    
    checkboxSelect : function(component, event, helper) {
		helper.checkboxSelect(component, event);
	},
    
    selectAll : function(component, event, helper) {
		helper.selectAll(component, event);
	},
     
    congaEmails: function(component, event, helper) {
       helper.congaEmails(component); 
    },
    
    selectFrequency: function(component, event, helper) {
       helper.selectFrequency(component); 
    }
    
    
    
})