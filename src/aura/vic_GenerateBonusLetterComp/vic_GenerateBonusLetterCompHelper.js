({
    getYear : function(component, event, helper) {
        
        var action = component.get("c.getYearForDisplay");
        debugger;
        action.setCallback(this, function(response) {
            var getFinancialYearState = response.getState();
            var valuegetFinancialYear = response.getReturnValue();
            if (getFinancialYearState === "SUCCESS") {
                component.set("v.financialYearValueList", valuegetFinancialYear);
            }
        });
        $A.enqueueAction(action); 
        
        var action1 = component.get("c.getCurrentYear");
        debugger;
        action1.setCallback(this, function(response) {
            var getFinancialYearState1 = response.getState();
            var valuegetFinancialYear1 = response.getReturnValue();
            if (getFinancialYearState1 === "SUCCESS") {
                component.set("v.defaultYear", valuegetFinancialYear1);
            }
        });
        $A.enqueueAction(action1);  
    },
    
    selectFrequency : function(component,event,helper) {
        var frequency = component.find("frequency").get("v.value");
        var frequencyCmp=component.find("frequency");
        if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'Monthly YTD'){
            component.set("v.monthYTD", true);
            component.set("v.QTR", false);
            component.set("v.year", true);
            frequencyCmp.set("v.errors", null);
        }
        else if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'QTR'){
            component.set("v.monthYTD", false);
            component.set("v.QTR", true);
            component.set("v.year", true);
            frequencyCmp.set("v.errors", null);
        }
            else if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'Yearly'){
                component.set("v.monthYTD", false);
                component.set("v.QTR", false);
                component.set("v.year", true);
                frequencyCmp.set("v.errors", null);
            }
                else{
                    component.set("v.monthYTD", false);
                    component.set("v.QTR", false);
                    component.set("v.year", false);
                }
        
    },
    
    getFinancialYear : function(component,event,helper) {
        var action = component.get("c.getFinancialYear");
        action.setCallback(this, function(response) { 
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstYears",allValues);
                component.set("v.strFinancialYear",allValues[0]);
                this.getRoles(component,event,helper);
            }
        });
        $A.enqueueAction(action);
    },
    
    getRoles : function(component,event,helper) {
        var action = component.get("c.getVicRole");
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.lstVicRole",allValues);
            }
        });
        $A.enqueueAction(action);
        
    },
    
    getBonusData : function(component) {
        var selectedRole = component.get("v.VicRole");
        var role=component.find("Role");
        var frequency = component.find("frequency").get("v.value");
        var frequencyCmp=component.find("frequency");
        if(selectedRole === null || selectedRole === 'undefined' || selectedRole ===''){
            role.set("v.errors", [{message:"Please Select VIC Role."}]);
            component.set("v.isShowHide",false);
            return null;
        }
        else if($A.util.isUndefinedOrNull(frequency) || frequency === ""){
            frequencyCmp.set("v.errors", [{message:"Please Select Frequency."}]);
            component.set("v.isShowHide",false);
            return null;
        }
            else{
                role.set("v.errors",null);
                var ytdYear=component.get("v.defaultYear");
                var domicile = component.get("v.user.Domicile__c");
                var selectedYear = component.get("v.strFinancialYear");
                var month = null;
                var qtr = null;
                var bonusFilterStartDate = null;
                var bonusFilterEndDate = null;
                if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'Monthly YTD'){
                    month = component.find("month").get("v.value");
                    bonusFilterStartDate = selectedYear+'-1-1';
                    bonusFilterEndDate = ytdYear+'-'+month;
                }
                else if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'QTR'){
                    qtr = component.find("qtr").get("v.value");
                    if(qtr==='Q1'){
                        bonusFilterStartDate = selectedYear+'-1-1';
                        bonusFilterEndDate = selectedYear+'-3-31'; 
                    }
                    if(qtr==='Q2'){
                        bonusFilterStartDate = selectedYear+'-4-1';
                        bonusFilterEndDate = selectedYear+'-6-30'; 
                    }
                    if(qtr==='Q3'){
                        bonusFilterStartDate = selectedYear+'-7-1';
                        bonusFilterEndDate = selectedYear+'-9-30'; 
                    }
                    if(qtr==='Q4'){
                        bonusFilterStartDate = selectedYear+'-10-1';
                        bonusFilterEndDate = selectedYear+'-12-31'; 
                    }
                }
                    else if(!$A.util.isUndefinedOrNull(frequency) && frequency === 'Yearly'){
                        bonusFilterStartDate = selectedYear+'-1-1';
                        bonusFilterEndDate = (parseInt(selectedYear)+2)+'-12-31'; 
                    }
                component.set("v.bonusFilterStartDate", bonusFilterStartDate);
                component.set("v.bonusFilterEndDate", bonusFilterEndDate);
                var action = component.get("c.getSearchItems");
                action.setParams({
                    "vicRole": selectedRole,
                    "selectedYear": selectedYear,
                    "domicile": domicile,
                    "frequency": frequency,
                    "month": month,
                    "qtr": qtr,
                    "ytdYear": ytdYear
                });
                console.log(selectedRole + ' | ' + selectedYear + ' | ' + domicile + ' | ' + frequency + ' | ' + month + ' | ' + qtr + ' | ' + ytdYear);
                action.setCallback(this, function(response){
                    if (response.getState() === "SUCCESS") {
                        //component.set("v.searchRecords",null);
                        var allValues = JSON.parse(response.getReturnValue());
                        if(allValues.length>0){
                            component.set("v.errorMessage", "");
                            component.set("v.searchRecords",allValues);
                            component.set("v.isShowHide",true);
                            component.find("box3").set("v.value", false);
                            component.set("v.selectedCount",0);
                        }
                        else{
                            component.set("v.errorMessage", "No Record Found");
                            component.set("v.selectedCount",0);
                            component.set("v.isShowHide",false);
                        }
                    }else{
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title: 'Error',
                             type: 'error',
                             message: response.getError()[0].message
                        });
                        toastEvent.fire();
                    }
                });
                $A.enqueueAction(action);
            }
    },
    
    checkboxSelect: function(component, event) {
        var searchRecords = component.get("v.searchRecords") || [];
        var checked = event.getSource().get("v.checked");
        var record = searchRecords[event.getSource().get("v.label")];
        record["isSelected"] = checked;
        if(checked){
            var targetcounter=0;
            for(var i=0;i<searchRecords.length;i++){
                if(searchRecords[i].isSelected){
                    targetcounter++; 
                }
            }
            if(searchRecords.length == targetcounter){
                component.set('v.isSelected',true);
            }
        }else{
            component.set('v.isSelected',false);
        }
    },
    
    selectAll: function(component, event, helper) {
        var selectedHeaderCheck = event.getSource().get("v.value");
        var searchRecords = component.get("v.searchRecords") || [];
        for(var i=0;i<searchRecords.length;i++){
            searchRecords[i].isSelected = selectedHeaderCheck;
        }
        component.set("v.searchRecords", searchRecords);        
    },
    congaEmails : function(component){
        var searchRecords = component.get("v.searchRecords") || [];
        var lstOfSelectedRecords=[];
        for(var i=0;i<searchRecords.length;i++){
            if(searchRecords[i].isSelected){
                lstOfSelectedRecords.push(searchRecords[i]);
            }
            
        }
        
        
        if(lstOfSelectedRecords==null || lstOfSelectedRecords.length==0) {
            var toast = $A.get("e.force:showToast");
                    if(toast){
                        toast.setParams({
                            "title": "Error",
                            "message": "Please select atleast one User.",
                            "type" : "error"
                        });
                    }
                    toast.fire();
			           
        }
        else{
            var bonusFilterStartDate = component.get("v.bonusFilterStartDate");
            var bonusFilterEndDate = component.get("v.bonusFilterEndDate");
            var vicrole=component.get("v.VicRole");
            
            var action=component.get("c.getCongaUrlForBonusLetter"); 
            action.setParams({
                
                "bonusFilterStartDate"	:bonusFilterStartDate,
                "bonusFilterEndDate"	:bonusFilterEndDate,
                "strOfSelectedRecords" 	:JSON.stringify(lstOfSelectedRecords),
                "vicRole" 				:component.get("v.VicRole"),
                "year"					:component.get("v.defaultYear")
            });
            
            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS") {
                    var strResponse = response.getReturnValue();
                    if(strResponse ==='SUCCESS'){
                        component.set("v.errorMessage","");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Letter Sent Successfully.",
                            "type" : "success"
                        });
                        toastEvent.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else if(strResponse ==='Queued'){
                        component.set("v.errorMessage","");
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": "Success!",
                            "message": "Batch Processed",
                            "type" : "success"
                        });
                        toastEvent.fire();
                        $A.get('e.force:refreshView').fire();
                    }
                    else{
                        
                        component.set("v.errorMessage","Error !! "+strResponse);
                    } 
                }else if(response.getState() === "ERROR"){
                    var toast = $A.get("e.force:showToast");
                    if(toast){
                        toast.setParams({
                            "title": "Error",
                            "message": response.getError()[0].message,
                            "type" : "error"
                        });
                    }
                    toast.fire();
                }
            });
           $A.enqueueAction(action);
        }
    }
    
})