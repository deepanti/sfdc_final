({
    // get List of All Campaign Members of the Related Campaign
    doInit: function(component, event, helper) {
        var profileIds = $A.get("$Label.c.GenpactInsideSalesProfileId");
       	var tempArray = [];
        tempArray = profileIds.split(',');
        console.log(tempArray.length+'tempArrLen');
         var newList = '';
        if(tempArray.length>0){
             for(var i=0;i<tempArray.length;i++)
                    {
                        newList = newList  + '\'' + tempArray[i] + '\',';
                    }
              newList = newList.substr(0, newList.length-1);
            
        }
        component.set("v.GenpactInsideSalesProfileId","AND ProfileId in (" + newList + ")"); 
        helper.doInit(component, event, helper);
        var campId =component.get("v.campId"); 
        campId=campId.substring(1,campId.length -1);
        var action = component.get("c.getCampaignMembers");
        action.setParams({
            "campaignId":campId
        });
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                component.set("v.wrapperList",result.getReturnValue());   
            }
        });
        helper.getCallersToSkip(component, event, helper);
        $A.enqueueAction(action);
    },
    //to assign Callers to Campaign
    assignCaller:function(component, event, helper) {
        var caller = component.get('v.selectInsideSalesUser');
        var wrapperList = component.get("v.wrapperList");
        var wrapperListSize = component.get("v.wrapperList").length;
        var countOfUnchecked =0;
        var countOfChecked = 0;          
        for(var i=0;i<wrapperList.length;i++)
        {
            if(wrapperList[i].isSelected == true){
                countOfChecked++;             
                wrapperList[i].assignedTo = caller.text;
                wrapperList[i].assignedToId = caller.val;
                wrapperList[i].isSelected = false;
                for(var j=0;j<wrapperList.length;j++)
                {
                    if(wrapperList[i].companyName==wrapperList[j].companyName &&  i!==j)
                    {
                        wrapperList[j].assignedTo = caller.text;
                        wrapperList[j].assignedToId = caller.val;
                        wrapperList[j].isSelected = false;
                        break;
                    }
                }
            }
            else{
                countOfUnchecked++;
            }
        }
        component.set("v.CountOfAssignedMembers",countOfChecked);
        if(countOfUnchecked==wrapperListSize){
            alert('Please select campaign members to assign caller.');
        } 
        else{      
             component.set("v.wrapperList",wrapperList);
             component.set("v.autoAssignAllRemoval",false);
             component.set("v.assignAll",false);
            }
        component.set("v.selectInsideSalesUser",null);
        
    },
     //to close Modal on saving assignment to callers
    closeModal:function(component, event, helper) {
        component.set("v.isOpenModal", false);
        window.history.back();
    },
     //to assign Callers to all Campaign Members on select All
    assignAllCampaignMembers:function(component, event, helper) {
        
        var wrapperList = component.get("v.wrapperList");
        var caimpaingnMemberIds = [];
        for(var i=0;i<wrapperList.length;i++)
        {
            caimpaingnMemberIds = caimpaingnMemberIds.concat(wrapperList[i].campMembersIds);
        }
        component.set("v.campMemIds",caimpaingnMemberIds); 
        component.set("v.autoAssignAllRemoval",false); 
        console.log(component.get("v.assignAll"));
    },
    
    //to save assigned Callers to Campaign Members
    saveCaller:function(component, event, helper) {
        
        helper.saveCaller(component, event, helper);
    },
    //to assign Callers individually
    assignCampMember:function(component, event, helper){
        var wrapperId = event.getParam("wrapperId");
        var isSelected = event.getParam("isSelected");
        var wrapperList = component.get("v.wrapperList");
        var campaignMemberIds = [];
        if(isSelected == false)
        {
            component.set("v.autoAssignAllRemoval",true);
            component.set("v.assignAll",false);
        }
        
        for(var i=0;i<wrapperList.length;i++)
        {
            if(wrapperList[i].wrapperId == wrapperId){
                campaignMemberIds = campaignMemberIds.concat(wrapperList[i].campMembersIds);
                break;
            }
        }
        component.set("v.campMemIds",campaignMemberIds);   
        
    }
})