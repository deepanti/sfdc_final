({
    // get Related Campaign details
    doInit: function(component, event, helper) {
        var campId =component.get("v.campId"); 
        campId=campId.substring(1,campId.length -1);
        var action = component.get("c.fetchCampaign");
        action.setParams(	{                            
            "campaignId":campId
        }
                        );
        action.setCallback(this, function(response) {		
            var state = response.getState();		
            if(component.isValid() && state === "SUCCESS"){		
                component.set("v.relatedCampaign",response.getReturnValue());  
            }
        });		
        $A.enqueueAction(action);		
        
    },
    //to save assigned Callers to Campaign Members
    saveCaller:function(component, event, helper) {
        console.log('being saved')
        component.set("v.InPlayCampaignListViewID",$A.get("$Label.c.InPlayCampaignListViewID"));
        var inPlayCampaignListViewID = component.get('v.InPlayCampaignListViewID');
        var isAllSelected= component.get('v.isAllSelected');
        var wrapperList = component.get('v.wrapperList');
        var action = component.get("c.saveCallersToCampaignMembers");
        var countOfChecked = component.get("v.CountOfAssignedMembers");   
        var wrapperListSize = component.get("v.wrapperList").length;
        
        action.setParams({
            "wrapperListStr" :JSON.stringify(wrapperList)
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(countOfChecked == wrapperListSize){
                    var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": inPlayCampaignListViewID,
                        "scope": "Campaign"
                    });
                    navEvent.fire();
                }
                else{
                    window.history.back();
                }
            }
        });
        $A.enqueueAction(action);
        component.set("v.isOpenModal", false);
        console.log('countOfChecked'+countOfChecked);
    },
    // to get List of Callers to skip
    getCallersToSkip:function(component, event, helper) {
        var action = component.get("c.getCallersToSkipInPlay");
        action.setCallback(this, function(result){
            var state = result.getState();
            if (component.isValid() && state === "SUCCESS"){
                var list = result.getReturnValue();
                var newList = '';
                if(list.length>0)
                {
                    for(var i=0;i<list.length;i++)
                    {
                        newList = newList  + '\'' + list[i] + '\',';
                    }
                    newList = newList.substr(0, newList.length-1);
                    // Modified by Amit Bhardwaj
                 // As discussed with Pooja gupta single tele caller can be assigned more than one campaign
                 //   component.set("v.UsersToSkipForLookupClause","AND Id not in (" + newList + ")");   
                }
            }
        });
        $A.enqueueAction(action);
    }
    
})