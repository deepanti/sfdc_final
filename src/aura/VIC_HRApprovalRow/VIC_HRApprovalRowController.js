({
    resetRadioGroup: function(component, event, helper) {
        var incentiveStatus = component.get("v.incentiveStatus");
        var targetcmp=component.find("comments");
        if (incentiveStatus == 'HR - OnHold' || incentiveStatus == "VIC Team - Pending") {
            
           component.set("v.showModal", true);
           
        } else {
            component.set("v.incentiveRecord.IsOnHold", false);
            component.set("v.incentiveRecord.IsRejected", false);
            component.set("v.incentiveRecord.Comments",'');
            component.set("v.isSelected",false);
            /*if(targetcmp)
            	$A.util.removeClass(targetcmp, 'comment_required');*/

           
        }
    },
    updateincentives: function(component, event, helper) {
      component.set("v.showModal", false);
      helper.updateIncentives(component, event, helper); 
        
    },
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
    },
    toggleOtherOption: function(component, event, helper) {
        
       
        var value= event.getSource().get("v.value");
        var label= event.getSource().get("v.label");
        
        if (value == "Query"){
            component.set("v.incentiveRecord.IsOnHold", true);
            component.set("v.incentiveRecord.IsRejected", false);
        } else if (value == "Reject") {
            component.set("v.incentiveRecord.IsOnHold", false);
            component.set("v.incentiveRecord.IsRejected", true);
            component.set("v.incentiveRecord.Comments",'');
        }
        
        //var  radioGrp = component.find("radio").get("v.value");
        var radioGrp = event.getSource().get("v.value");
        var strCmnt = component.get("v.incentiveRecord.Comments");
        var commentBoxId = component.find("comments");
        if ((strCmnt == null || strCmnt == '') && value == "Query" ){
            //$A.util.addClass(commentBoxId, 'comment_required');
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error!",
                "type": "error",
                "message": "Comments are required."
            });
            
            toastEvent.fire();
        }
        /*else {
            $A.util.removeClass(commentBoxId, 'comment_required');
        }*/
    },
    onIncentiveRecordSelection: function(component, event, helper) {
        var incentiveRecord = JSON.parse(JSON.stringify(component.get("v.incentiveRecord")));
        component.set("v.selectedIncentiveRecordIndex",component.get("v.incentiveRecordIndex"));
        component.set("v.selectedIncentiveRecord" , incentiveRecord);
    },
    fetchIncentiveDetails: function(component, event, helper){
        //alert('call');
        helper.fetchIncentiveDetailsinApprovals(component,event, helper);
    },
    CommentsMandate: function(component, event,helper){
  
   
            var  radioGrp = component.get("v.incentiveRecord.IsRejected");
           
            var strCmnt=component.get("v.incentiveRecord.Comments");
            
			if ((strCmnt==null || strCmnt=='') && radioGrp!=null ){
                   
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": "Comments are required."
                    });
                   
                    toastEvent.fire();
                  
                   }
                 
 }
    
    
})