({
    updateIncentives: function(component, event, helper) {
        var incentiveRecord = component.get("v.incentiveRecord");
        var screenName = component.get("v.screenName");
        var updateToPendingDataOfIncentiveRecord = component.get("c.updateToPendingDataOfIncentiveRecord");
        var mapOfNewDiscretionaryRecord = component.get("v.mapOfNewDiscretionaryRecord") || {};
        var incentiveRecordIndex = component.get("v.incentiveRecordIndex");
        var discretionaryRecord = mapOfNewDiscretionaryRecord[incentiveRecordIndex] || {};
        updateToPendingDataOfIncentiveRecord.setParams({
            "wrapperRecord": JSON.stringify(incentiveRecord),
            "screenName":screenName,
            "discretionaryRecordJson" : JSON.stringify(discretionaryRecord)
        });
        updateToPendingDataOfIncentiveRecord.setCallback(this, function(response) {
            this.updateIncentivesHandler(response, component);
        });
        $A.enqueueAction(updateToPendingDataOfIncentiveRecord);
    },
    updateIncentivesHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('Success', 'Success', 'Record Saved Succesfully');
            //$A.get('e.force:refreshView').fire(); 
           	component.set("v.showModal",false);
            var refreshView = component.getEvent("refreshView");
            refreshView.fire();
            
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    hideSpinner: function(component) {
        component.set("v.isLoading", false);
    },
    showSpinner: function(component) {
        component.set("v.isLoading", true);
    },
    handleFailedCallback: function(component, responseData) {
        var errorMessage = responseData.message || this.DEFAULT_ERROR_MESSAGE;
        this.showToast('Error', 'error', errorMessage);
    },
    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.fire({
                "title": title,
                "type": type,
                "message": message
            });
        } else {
            //to avoid exception when opened in a standalone app.
            //alert(message);
        }
    },
    fetchIncentiveDetailsinApprovals: function(component, event, helper){
        var incentiveStatus = component.get("v.incentiveStatus");
        //alert('Test');
        var userId = event.target.name;
        //alert('userId'+userId);
        var action = component.get("c.getIncentiveDetails");
        action.setParams({ 
            "strUserId" : userId,
            "strIncentiveStatus" : incentiveStatus
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                //alert('success'+resp.getReturnValue());
                
                component.set("v.lstDiscretionaryPayments", resp.getReturnValue());
                //alert('V'+component.get("v.lstDiscretionaryPayments"));
                component.set("v.isOpenDiscPopup", true);
                //alert('V'+component.get("v.isOpenDiscPopup"));
            }else{
                this.showToast('Error', 'error', errorMessage);
            }
        });
        $A.enqueueAction(action);
        
    }
})