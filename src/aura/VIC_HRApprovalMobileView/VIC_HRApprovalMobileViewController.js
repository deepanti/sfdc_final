({
    onSelectionOfDomicile: function(component, event, helper) {
        var activeDomicile = event.target.dataset.menuItemId;
        component.set("v.activeDomicile",activeDomicile);
        component.set("v.showNavBar",false);
    },
    toggleNavBar: function(component, event, helper) {
        var showNavBar = component.get("v.showNavBar");
        component.set("v.showNavBar",!showNavBar);
    }
})