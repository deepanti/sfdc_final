({
    //validating fields 
    validateAll : function(component, event, helper) {
        
        var proceedToSave = true;
        var conLang = component.find("conLang");
        var conLangvalue = conLang.get("v.value");
        //validating contract language 
        if($A.util.isUndefinedOrNull(conLangvalue) || conLangvalue == '')
        {
            $A.util.addClass(conLang,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(conLang,'slds-has-error');
        }
        
        if(conLangvalue=='Non - English')
        {
            var genpactLegal = component.find("genpactLegal");
            var genpactLegalvalue = genpactLegal.get("v.value");
            
            if($A.util.isUndefinedOrNull(genpactLegalvalue) || genpactLegalvalue == '')
            {
                $A.util.addClass(genpactLegal,'slds-has-error');
                proceedToSave = false;
                
            }
            else
            {
                $A.util.removeClass(genpactLegal,'slds-has-error');
                
            }
            
            
            var customerLegal = component.find("customerLegal");
            var customerLegalvalue = customerLegal.get("v.value");
            
            if($A.util.isUndefinedOrNull(customerLegalvalue) || customerLegalvalue == '')
            {
                $A.util.addClass(customerLegal,'slds-has-error');
                proceedToSave = false;
            }
            else
            {
                $A.util.removeClass(customerLegal,'slds-has-error');
                
            }
            
        }
        //effectiveDate validation
        var effectiveDate = component.find("effectiveDate");
        var effectiveDatevalue = effectiveDate.get("v.value");
        //validating contract language 
        if($A.util.isUndefinedOrNull(effectiveDatevalue) || effectiveDatevalue == '')
        {
            $A.util.addClass(effectiveDate,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(effectiveDate,'slds-has-error');
        }
        
        
        var endDate = component.find("endDate");
        var endDatevalue = endDate.get("v.value");
        //validating end date
        if($A.util.isUndefinedOrNull(endDatevalue) || endDatevalue == '')
        {
            $A.util.addClass(endDate,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(endDate,'slds-has-error');
        }
        
        //validating signature 
        
        var signature = component.find("signature");
        var signaturevalue = signature.get("v.value");
        if($A.util.isUndefinedOrNull(signaturevalue) || signaturevalue == '')
        {
            $A.util.addClass(signature,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(signature,'slds-has-error');
        } 
        
        
        //validating signed date 
        var signedDate = component.find("signedDate");
        var signedDatevalue = signedDate.get("v.value");
        if($A.util.isUndefinedOrNull(signedDatevalue) || signedDatevalue == '')
        {
            $A.util.addClass(signedDate,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(signedDate,'slds-has-error');
        }
        
        //validating signed By        
        var signedBy = component.find("signedBy");
        var signedByvalue = signedBy.get("v.value");
        if($A.util.isUndefinedOrNull(signedByvalue) || signedByvalue == '')
        {
            $A.util.addClass(signedBy,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(signedBy,'slds-has-error');
        }
        
        
        var CustomerSignedDate = component.find("Customer");
        var CustomerSignedDatevalue = CustomerSignedDate.get("v.value");
        if($A.util.isUndefinedOrNull(CustomerSignedDatevalue) || CustomerSignedDatevalue == '')
        {
            $A.util.addClass(CustomerSignedDate,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(CustomerSignedDate,'slds-has-error');
        }
        return proceedToSave;
    },
    
    
    //convert To Attachment
    convertToAttachment:function(component, event, helper,contractId) {
       
        component.set("v.Spinner",true);
        var action = component.get("c.convertToAttachment");
        action.setParams(
            {
                'opportunityId' :component.get("v.opportunityId"),
                'contractId':contractId,
                'fileId':component.get("v.fileId")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                this.fireToastSuccess(component, event, helper,'Contract is saved successfully. As a next step submit the contract for approval to ICON team.');
               
         
                this.getCountOfContractsInDraft(component, event, helper);
                  var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
            appEvent.setParams({"selectedTabId" : "19"});
            appEvent.fire();

            localStorage.setItem('LatestTabId',"19");
            window.scrollTo(0, 0);
                var files = [];
                component.set("v.fileId",files);
                component.set("v.setFlag",false);
                component.set("v.fileLength","0");
                component.set('v.contractId',"");
                $A.get('e.force:refreshView').fire();
            }
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
            }
            //setting file length to 0 since file is stored in the contract
            component.set("v.fileLength",0);
            
            component.set("v.Spinner",false);
        });
        $A.enqueueAction(action); 
    },
    //submiting for approval
    handleSubmitApproval:function(component, event, helper)
    {
        component.set("v.Spinner",true);
        var action = component.get("c.submitForApproval");
        action.setParams(
            {
                'opportunityId' :component.get("v.opportunityId")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                this.fireToastSuccess(component, event, helper,'Contracts are submited for approval!');
                component.set("v.Spinner",false);
                if(localStorage.getItem('MaxTabId')<20){
                    localStorage.setItem('MaxTabId',20);
                    component.set("v.MaxSelectedTabId", "20");
                    
                } 
                
                var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
                appEvent.setParams({"selectedTabId" : "20"});
                appEvent.fire();
                localStorage.setItem('LatestTabId',"20");
                window.scrollTo(0, 0);
                
            }
            else
            {
                
                this.fireToastError(component, event, helper,response.getError()[0].message);
                component.set("v.Spinner",false);
            }
            
        });
        $A.enqueueAction(action); 
        
    },
    setContractFile:function(component, event, helper,fileId)
    {
        component.set("v.Spinner",true);
        var action = component.get("c.setContractFile");
        action.setParams(
            {
                'opportunityId' :component.get("v.opportunityId"),
                'fileId':fileId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Spinner",false);
                
            }
            else
            {
                component.set("v.Spinner",false);
                this.fireToastError(component, event, helper,response.getError()[0].message);
                
            }
            
        });
        $A.enqueueAction(action); 
    },
    getCountOfContractsInDraft:function(component, event, helper)
    {
        component.set("v.Spinner",true);
        var action = component.get("c.getContractInDraft");
        action.setParams(
            {
                'opportunityId' :component.get("v.opportunityId")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.count",response.getReturnValue());
                component.set("v.Spinner",false);
            }
            else
            {
                
                this.fireToastError(component, event, helper,response.getError()[0].message);
                component.set("v.Spinner",false);
            }
            
        });
        $A.enqueueAction(action); 
    },
    //error toast
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
    //success toast 
    fireToastSuccess : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "SUCCESS"  
        });  
        toastEvent.fire();  
    }
})