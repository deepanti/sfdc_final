({
    doInit:function(component, event, helper) {
        helper.getCountOfContractsInDraft(component, event, helper);
    },
    handleSubmit : function(component, event, helper) {
      
        event.preventDefault();
        //validating the inserted data
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            if(component.get("v.fileLength")==0)
            {
                helper.fireToastError(component, event, helper,'Please Upload Genpact Contract');
                
            }
            else
            {
                
                component.set("v.Spinner",true);
                //inserting the contract data
                var fields = event.getParam("fields");
                fields["Opportunity_tagged__c"]=component.get("v.opportunityId");
                fields["AccountId"]=component.get("v.oppRecord.AccountId");
                fields["Status"]='Draft';
                component.find("form").submit(fields);
                
            }
        }
        else
        {
            helper.fireToastError(component, event, helper,'Required fields are missing');
        }
        
    },
    
    handleSuccess:function(component, event, helper) {
      
        component.set("v.Spinner",false);
        var contractId=event.getParams().response.id;
        component.set('v.contractId',contractId);
        helper.convertToAttachment(component, event, helper,contractId);
        
        
    },
    //sending contract for approval
    handleSubmitApproval :function(component, event, helper) {
    
        helper.handleSubmitApproval(component, event, helper);
    },
    //uploading file 
    handleUploadFinished: function (component, event,helper) {

        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        var files = [];
        component.set("v.fileLength",uploadedFiles.length);
        for(var i=0;i<uploadedFiles.length;i++)
        {
            files[i]=uploadedFiles[i].documentId;
        }
        component.set("v.fileId",files);
        helper.setContractFile(component, event, helper,files);
        component.set("v.setFlag",true);
    },
    handleError:function (component, event,helper) {
        
        component.set("v.Spinner",false);
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        error.output.errors.forEach(
            function(msg) { 
                if(!$A.util.isUndefinedOrNull(msg.message))
                    errorMsg +=msg.message;
            }
        );
        Object.keys(error.output.fieldErrors).forEach(
            function(field) { 
                error.output.fieldErrors[field].forEach(
                    function(msg) { 
                        if(!$A.util.isUndefinedOrNull(msg.message))
                            errorMsg +=msg.message + ', ';
                    }
                )
            });
        
        helper.fireToastError(component, event, helper, errorMsg);
    }
})