({
    handleActive: function (component, event, helper) {
        component.set("v.selectedPrevTab",localStorage.getItem('LatestTabId'));
         //get the maximum tab id
       // localStorage.setItem('LatestTabId',event.getParam("id"));
        var latestTab  = localStorage.getItem('MaxTabId');
        //selected tab id
        var tabSelected = event.getParam("id");
        //Previous tabs are allowed
        if(parseInt(tabSelected)<=latestTab)
            component.set("v.isEventFired",true);
        helper.handleActive(component, event, event.getParam("id"));
    },
     handleActiveFromParent : function(component, event, helper) {
        component.set("v.isEventFired",true);
        component.set("v.TabIdFromEvent",event.getParams("tabNo").arguments.tabNo);
    },
    handleComponentEventFired : function(component, event, helper) {
        var context = localStorage.getItem('LatestTabId');
        component.set("v.isEventFired",true);
        component.set("v.selectedTab", context);
        helper.handleActive(component, event, context);
    }
})