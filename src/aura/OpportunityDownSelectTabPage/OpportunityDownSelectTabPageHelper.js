({
    handleActive: function (component, event, tabSelected) {
        var tab = component.find(tabSelected);
        if(component.get("v.isEventFired"))
        {
            //Set the previous tab
            component.set("v.selectedPrevTab", component.get("v.selectedTab"));
            localStorage.setItem('LatestTabId',tabSelected);
            /*
            switch (tab.get('v.id')) {
                case '1' :
                    this.injectComponent('c:OpportunityDownSelectAddDetails', tab, '', component.get('v.recordId'));
                    break;
                case '2' :
                    this.injectComponent('c:OpportunityDownSelectRelationshipBarometer', tab, $A.get("$Label.c.OpportunityRelationshipBarometer"), component.get('v.recordId'));
                    break;
                case '3':
                    this.injectComponent('c:OpportunityDownSelectPursuitProfiler', tab, $A.get("$Label.c.OpportunityPursuitProfiler"), component.get('v.recordId'));
                    break;
            }
            */
        }
        else
        {
            //block tab change
            component.set("v.selectedTab", component.get("v.selectedPrevTab"));
        }
        component.set("v.isEventFired",false);
    },
    injectComponent: function (name, target, page, recId) {
        $A.createComponent(name, {
            pagename : page,
            recordId : recId,
        }, function (contentComponent, status, error) {
            if (status === "SUCCESS") {
                target.set('v.body', contentComponent);
            } else {
                throw new Error(error);
            }
        });
    }
})