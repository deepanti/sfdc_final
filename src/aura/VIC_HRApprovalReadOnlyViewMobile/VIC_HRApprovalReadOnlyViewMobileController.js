({
	bindcommentToRecordIndex : function(component, event, helper) {
		var selectedIncentiveRecordIndex = component.get("v.selectedIncentiveRecordIndex");
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        var incentiveRecord = component.get("v.incentiveRecord");
        lstOfIncentiveRecordWrapper[selectedIncentiveRecordIndex].Comments = incentiveRecord.Comments;
        component.set("v.lstOfIncentiveRecordWrapper",lstOfIncentiveRecordWrapper);
	}
})