({
    // function to get TS opportunities 
	getOpportunityTS:function(component, event, helper)
    {
       
        var action = component.get("c.createOpportunityWrapperList");
        action.setParams(
            {

                'TSservicesOnly':true,
                
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opplistTS",response.getReturnValue());
                var opplist=component.get("v.opplistTS");
              
                console.log(opplist);
                var i;
                var newlst =[];
                // copying opportunities those need to be displayed on Home Screen Without clicking on View All button
                for (i = 0; i < component.get("v.pageSize") && i<opplist.length; i++) { 
                    newlst.push(opplist[i]);
                }
                component.set("v.userSelectedOppListTS",newlst);
               }     
            else
            {
                 helper.fireToastError(response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
	},
    // function to get IO(Non TS) opportunities 
    getOpportunityNonTS:function(component, event, helper)
    {
               
       
     	var action = component.get("c.createOpportunityWrapperList");
         action.setParams(
            {
              
                TSservicesOnly:false
              
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
          
            if (state === "SUCCESS") { 
                var result = response.getReturnValue();
                component.set("v.opplistNonTS",result);
                var opplist = component.get("v.opplistNonTS");
                
                var newlst =[];
                for (var i = 0; i < component.get("v.pageSize") && i<opplist.length; i++) { 
                    newlst.push(opplist[i]);
                }
               
                component.set("v.userSelectedOppListNonTS",newlst);
               }     
            else
            {
              helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
	},
    //function to change the profile of the user 
     changeProfile:function(component, event, helper)
    {
        component.set("v.spinner",true);
    
     	var action = component.get("c.toggleProfile");

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
               // alert('response.getReturnValue()'+response.getReturnValue())
                component.set("v.spinner",false);
                 /*if(response.getReturnValue()==200)
                 {*/
                   
                     document.location.reload(true); 
                // }
             		
                /*else
                 helper.fireToastError('Something went wrong ,Please contact System Admin'); */
               }     
            else
            {
              component.set("v.spinner",false);
              helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
	},
    fireToastError : function(message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    }
})