({
	doInit : function(component, event, helper) {
        console.log('init called');
        /*var oppProds = component.get('v.oppProds');
        console.log('oppProds: ', oppProds);
        helper.moveToOppProd(component);*/
       
        
	},
    onRender : function(component, event, helper){
        var sliderId = component.getGlobalId() + '_slides';
        var slides = document.getElementById(sliderId);
        console.log('slides: ', slides);
        var parentOfSlider = slides.parentElement;
        console.log('parentOfSlider: ', parentOfSlider);
        var parentWidth = parentOfSlider.offsetWidth;
        console.log('parentOfSlider width: ', parentOfSlider.offsetWidth);
        var marginBetweenSlides=component.get("v.marginBetweenSlides");
       
        component.set('v.sliderWidth', parentWidth);
        
    },
    moveToPrev : function(component, event, helper) {
        var currentOppProdIndex = component.get('v.currentOppProdIndex');
        if(currentOppProdIndex > 0){
            component.set('v.currentOppProdIndex', currentOppProdIndex - 1);
        }
        //helper.moveToOppProd(component);
	},
 	moveToNext : function(component, event, helper) {
        var currentOppProdIndex = component.get('v.currentOppProdIndex');
        var oppProds = component.get('v.oppwrapss');
        if(currentOppProdIndex < oppProds.length - 1){
            component.set('v.currentOppProdIndex', currentOppProdIndex + 1);
        }
        //helper.moveToOppProd(component);
	},
    
    
    selectAllOppLI : function(component, event, helper){
        var oppwrap = component.get("v.oppwrapss");
        var checkBoxStatus = oppwrap.oppSelected;
        var opportunityLIWrapperObj = oppwrap.opportunityLIWrapperObj;
        
        for(var i=0; i < opportunityLIWrapperObj.length; i += 1) {
            opportunityLIWrapperObj[i].oppLISelected = checkBoxStatus;
        }
        
        oppwrap.opportunityLIWrapperObj = opportunityLIWrapperObj;
        component.set("v.oppwrapss", oppwrap);
    },
    
   
})