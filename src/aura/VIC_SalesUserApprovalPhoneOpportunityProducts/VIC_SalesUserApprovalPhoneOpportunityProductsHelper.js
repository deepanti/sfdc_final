({
    moveToOppProd : function(component) {
        var currentIndex = component.get('v.currentOppProdIndex');
        console.log('currentIndex: ', currentIndex);
        var oppProds = component.get('v.oppProds');
        if(oppProds.length > 0){
            component.set('v.currOppProd', oppProds[currentIndex]);
            if(currentIndex - 1 >= 0){
                component.set('v.prevOppProd', oppProds[currentIndex - 1]);
            }
            else{
                component.set('v.prevOppProd', null);
            }
            console.log('test: ', (oppProds.length > 1 && (currentIndex + 1 <= oppProds.length - 1)));
            if(oppProds.length > 1 && (currentIndex + 1 <= oppProds.length - 1)){
                component.set('v.nextOppProd', oppProds[currentIndex + 1]);
            }
            else{
                component.set('v.nextOppProd', null);
            }
        }
    }
})