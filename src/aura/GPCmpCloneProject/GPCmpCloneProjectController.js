({
    doInit: function(component, event, helper) {        
        helper.getChildProjectList(component);
    },
    CloneProject: function(component, event, helper) {
        component.set("v.showLoadingSpinner", true);
        var ProjId = component.get("v.recordId");
        var action = component.get("c.cloneProject");
        action.setParams({ "strProjectId": ProjId });
        action.setCallback(this, function(response) {
            if (response.getState() == 'SUCCESS') {
                if (response.getReturnValue().indexOf("Error") >= 0) {
                    helper.showToast('Error', 'error', response.getReturnValue());
                } else {
                    helper.showToast('Success', 'Success', "Successfully created.");
                    helper.redirectToSobject(response.getReturnValue(), "related");
                }
            } else {
                helper.showToast('Error', 'error', "Oops, Something is wrong.");
            }
            component.set("v.showLoadingSpinner", false);
        });
        $A.enqueueAction(action);
        
    },
    ClosePopup: function(component, event, helper) {
        component.set("v.isClone",false);
        $A.get("e.force:closeQuickAction").fire();
    }
})