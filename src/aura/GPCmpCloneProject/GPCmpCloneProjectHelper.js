({
    getChildProjectList: function(component) {
        var getChildProjectRecord = component.get("c.getChildProjectRecord");
        getChildProjectRecord.setParams({
            "strProjectId": component.get("v.recordId")
        });
        getChildProjectRecord.setCallback(this, function(response) {
            this.getChildProjectRecordHandler(response, component);
        });
        $A.enqueueAction(getChildProjectRecord);
    },
    getChildProjectRecordHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJson = JSON.parse(responseData.response);
            if(responseJson)
            component.set("v.childProject",responseJson);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    }
})