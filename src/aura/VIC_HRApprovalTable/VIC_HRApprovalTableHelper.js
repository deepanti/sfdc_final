({
    DEFAULT_ERROR_MESSAGE: "Something went wrong!",
    getPendingForApprovalIncentivesService: function(component) {
        var incentiveStatus = component.get("v.incentiveStatus");
        var leaderName = component.get("v.leaderName");
        var screenName = component.get("v.screenName");
        var getPendingForApprovalIncentives = component.get("c.getPendingForApprovalIncentives");
        getPendingForApprovalIncentives.setParams({
            "incentiveStatus": incentiveStatus,
            "leaderName": leaderName,
            "screenName" : screenName
        });
        getPendingForApprovalIncentives.setCallback(this, function(response) {
            this.getPendingForApprovalIncentivesHandler(response, component);
        });
        $A.enqueueAction(getPendingForApprovalIncentives);
    },
    getPendingForApprovalIncentivesHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setPendingWrapperIncentives(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setPendingWrapperIncentives: function(component, responseData) {
        var mapofDomicileVslstOfIncentiveRecordWrapper = JSON.parse(responseData.response).mapofDomicileVslstOfIncentiveRecordWrapper;
        component.set("v.mapofDomicileVslstOfIncentiveRecordWrapper", mapofDomicileVslstOfIncentiveRecordWrapper);
        var mapOfUserVsSetOfIncentiveIds = JSON.parse(responseData.response).mapOfUserVsSetOfIncentiveIds;
        component.set("v.mapOfUserVsSetOfIncentiveIds", mapOfUserVsSetOfIncentiveIds);
        var activeDomicile = "US";
        this.setActiveDomicileIncentiveList(component, mapofDomicileVslstOfIncentiveRecordWrapper, activeDomicile);
        this.setDomicileVsIncentiveCount(component, mapofDomicileVslstOfIncentiveRecordWrapper);
    },
    setDomicileVsIncentiveCount: function(component, mapofDomicileVslstOfIncentiveRecordWrapper) {
        var mapofDomicileVsCountOfIncentives = {};
        for (var key in mapofDomicileVslstOfIncentiveRecordWrapper) {
            mapofDomicileVsCountOfIncentives[key] = mapofDomicileVslstOfIncentiveRecordWrapper[key] ? mapofDomicileVslstOfIncentiveRecordWrapper[key].length : 0;
        }
        component.set("v.mapofDomicileVsCountOfIncentives", mapofDomicileVsCountOfIncentives);
    },
    setActiveDomicileIncentiveList: function(component, mapofDomicileVslstOfIncentiveRecordWrapper, domicile) {
        var lstOfIncentiveRecordWrapper = mapofDomicileVslstOfIncentiveRecordWrapper[domicile] || [];
        var activeSection = component.get("v.activeSection");
        //this.trimInvalidRows(component,lstOfIncentiveRecordWrapper,activeSection);
        if(lstOfIncentiveRecordWrapper.length > 0)
        	component.set("v.selectedIncentiveRecord",lstOfIncentiveRecordWrapper[0]);
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
    },
    trimInvalidRows:function(component,lstOfIncentiveRecordWrapper,activeSection){
        for(var index = 0;index< lstOfIncentiveRecordWrapper.length;index++){
            if((activeSection == 'ScoreCard' && 
                (lstOfIncentiveRecordWrapper[index].TotalScoreCard - lstOfIncentiveRecordWrapper[index].DiscretionaryAmount) == 0) 
                || (activeSection == 'Upfront' && lstOfIncentiveRecordWrapper[index].TotalPayoutAmount == 0))
                lstOfIncentiveRecordWrapper.splice(index,1);
        }
    },
    hideSpinner: function(component) {
        component.set("v.isLoading", false);
    },
    showSpinner: function(component) {
        component.set("v.isLoading", true);
    },
    handleFailedCallback: function(component, responseData) {
        var errorMessage = responseData.message || this.DEFAULT_ERROR_MESSAGE;
        this.showToast('Error', 'error', errorMessage);
    },
    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.fire({
                "title": title,
                "type": type,
                "message": message
            });
        } else {
            //to avoid exception when opened in a standalone app.
            //alert(message);
        }
    },
    validateRows: function(component, helper, lstOfIncentiveRecordWrapper) {
        var isValid = true;
        var oneRecordSelected = false;
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            if (lstOfIncentiveRecordWrapper[index].IsSelected)
                oneRecordSelected = true;
        }
        if(!oneRecordSelected){
            this.showToast('Error', 'error', 'Please select atleast one record!');
            return false;
        }
        //|| lstOfIncentiveRecordWrapper[index].IsRejected
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            if (lstOfIncentiveRecordWrapper[index].IsSelected &&
                (lstOfIncentiveRecordWrapper[index].IsOnHold) &&
                (!lstOfIncentiveRecordWrapper[index].Comments || lstOfIncentiveRecordWrapper[index].Comments == '')) {
                isValid = false;
                //lstOfIncentiveRecordWrapper[index].className = 'slds-error';
                this.showToast('Error', 'error', 'Comments are mandatory for Query records.');
                break;
            }
        }
        
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
        return isValid;
    },
    saveRecords: function(component, helper, lstOfIncentiveRecordWrapper) {
        var listOfSelectedIncentiveRecords = [];
        var mapOfNewDiscretionaryRecord = component.get("v.mapOfNewDiscretionaryRecord");
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            if (lstOfIncentiveRecordWrapper[index].IsSelected)
                listOfSelectedIncentiveRecords.push(lstOfIncentiveRecordWrapper[index]);
        }
        if (!listOfSelectedIncentiveRecords) {
            return;
        }
        var updateDataOfIncentiveRecords = component.get("c.updateDataOfIncentiveRecords");
        updateDataOfIncentiveRecords.setParams({
            "incentiveJson": JSON.stringify(listOfSelectedIncentiveRecords),
            "screenName": component.get("v.screenName"),
            "discretionaryRecordJson" : mapOfNewDiscretionaryRecord ? JSON.stringify(mapOfNewDiscretionaryRecord) : null
        });
        updateDataOfIncentiveRecords.setCallback(this, function(response) {
            this.updateDataOfIncentiveRecordsHandler(response, component);
        });
        $A.enqueueAction(updateDataOfIncentiveRecords);
    },
    updateDataOfIncentiveRecordsHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('Success', 'Success', 'Records have been processed successfully.');
            $A.get('e.force:refreshView').fire(); 
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
})