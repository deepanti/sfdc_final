({
    doInit: function(component, event, helper) {
        helper.getPendingForApprovalIncentivesService(component);
    },
    changeOfMasterCheckbox: function(component, event, helper) {
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        var masterCheckBox = component.get("v.masterCheckBox");
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            lstOfIncentiveRecordWrapper[index].IsSelected = masterCheckBox;
        }
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
    },
    handleTabSelection: function(component, event, helper) {
        var tab = event.getSource();
        var tabId = tab.get('v.id');
        component.set("v.activeDomicile",tabId);
    },
    activeDomicileChangeHandler: function(component, event, helper) {
        var activeDomicile = component.get("v.activeDomicile");
        var mapofDomicileVslstOfIncentiveRecordWrapper = component.get("v.mapofDomicileVslstOfIncentiveRecordWrapper") || {};
        helper.setActiveDomicileIncentiveList(component, mapofDomicileVslstOfIncentiveRecordWrapper, activeDomicile);
    },
    validateAndSubmitRecords: function(component, event, helper) {
         var confirmModalCmp = component.find("confirmModalId");
         confirmModalCmp.set("v.showModal",true);
        /*var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        if (helper.validateRows(component, helper, lstOfIncentiveRecordWrapper))
            helper.saveRecords(component, helper, lstOfIncentiveRecordWrapper);*/
    },
    validateAndSubmitRecordsForApproval: function(component, event, helper) {
        component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        for(var index = 0 ;index < lstOfIncentiveRecordWrapper.length; index++){
            if(lstOfIncentiveRecordWrapper[index].IsSelected){
                lstOfIncentiveRecordWrapper[index].IsOnHold = false;
                lstOfIncentiveRecordWrapper[index].IsRejected = false;
            }
        }
        component.set("v.lstOfIncentiveRecordWrapper",lstOfIncentiveRecordWrapper);
        if (helper.validateRows(component, helper, lstOfIncentiveRecordWrapper))
            helper.saveRecords(component, helper, lstOfIncentiveRecordWrapper);
        else
            helper.showToast('Error', 'error', 'Please Fill Comments');
    },
    validateAndSubmitRecordsForRejection: function(component, event, helper) {
        component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        for(var index =0 ;index < lstOfIncentiveRecordWrapper.length; index++){
            if(lstOfIncentiveRecordWrapper[index].IsSelected){
                lstOfIncentiveRecordWrapper[index].IsOnHold = false;
                lstOfIncentiveRecordWrapper[index].IsRejected = true;
            }
        }
        component.set("v.lstOfIncentiveRecordWrapper",lstOfIncentiveRecordWrapper);
        if (helper.validateRows(component, helper, lstOfIncentiveRecordWrapper))
            helper.saveRecords(component, helper, lstOfIncentiveRecordWrapper);
         else
            helper.showToast('Error', 'error', 'Please Fill Comments');
    },
    validateAndSubmitRecordsForHold: function(component, event, helper) {
        component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        for(var index =0 ;index < lstOfIncentiveRecordWrapper.length; index++){
            if(lstOfIncentiveRecordWrapper[index].IsSelected){
                lstOfIncentiveRecordWrapper[index].IsOnHold = true;
                lstOfIncentiveRecordWrapper[index].IsRejected = false;
            }
        }
        component.set("v.lstOfIncentiveRecordWrapper",lstOfIncentiveRecordWrapper);
        if (helper.validateRows(component, helper, lstOfIncentiveRecordWrapper))
            helper.saveRecords(component, helper, lstOfIncentiveRecordWrapper);
         else
            helper.showToast('Error', 'error', 'Please Fill Comments');
    },
    filterList: function(component, event, helper) {
        var searchText = component.get("v.searchText");
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper");
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
             if (lstOfIncentiveRecordWrapper[index]["UserName"] && searchText &&
                lstOfIncentiveRecordWrapper[index]["UserName"].toLowerCase().includes(searchText.toLowerCase())) {
                var filteredIncentiveRecord = lstOfIncentiveRecordWrapper[index];
                lstOfIncentiveRecordWrapper.splice(index, 1);
                lstOfIncentiveRecordWrapper.unshift(filteredIncentiveRecord);
            }
        }
        component.set("v.lstOfIncentiveRecordWrapper",lstOfIncentiveRecordWrapper);
    },
    /*
		@author: Karthik Chekkilla
		@company: Saasfocus
		@description: This method is used to handle events.
	*/
	onBtnClickHandler : function(component, event, helper) {
        debugger;
    	var Operation = event.getParam("Operation");
        /*var comment = event.getParam("Description");
        var idToUpdate = event.getParam("Value");
        var childCmp = event.getParam("Source");*/
        if(Operation === "handleSaveBtn") {
            var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
                if (helper.validateRows(component, helper, lstOfIncentiveRecordWrapper)) {
                    helper.saveRecords(component, helper, lstOfIncentiveRecordWrapper);             
                } 
            	var confirmModalCmp = component.find("confirmModalId");
            	confirmModalCmp.set("v.showModal",false);
            }
   	},
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
         component.set("v.actionStatus", "");
    },
    approveConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","approve");
         component.set("v.showModal", true);
    },
    queryConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","query");
         component.set("v.showModal", true);
    },
    rejectConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","reject");
         component.set("v.showModal", true);
    }
})