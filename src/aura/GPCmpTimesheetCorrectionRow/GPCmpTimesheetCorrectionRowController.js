({
    clearTaskRecord: function(component, event, helper) {},
    clearProjectRecord: function(component, event, helper) {
        debugger;
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var index = event.getParam("externalParameter");

        wrapListOfTimeSheetEntries[index]["projectId"] = null;
        wrapListOfTimeSheetEntries[index]["projectName"] = null;

        wrapListOfTimeSheetEntries[index]["taskId"] = null;
        wrapListOfTimeSheetEntries[index]["taskName"] = null;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);

        var projectTaskLookupDom = component.find("project_task_lookup") || {};
        projectTaskLookupDom.ClearLookupControl();
        //projectTaskLookupDom.getEvent("clearLookupIdEvent").fire();
    },
    removeTimeSheetRowAdded: function(component, event, helper) {

        if (!confirm("Are you sure you want to delete?"))
            return;

        var timeSheetEntryRowIndex = Number(event.currentTarget.id);
        helper.removeTimeSheetRow(component, timeSheetEntryRowIndex);
    },
    changeOfValue: function(component, event, helper) {
        var rowIndex = event.target.getAttribute("data-row-index");
        var colIndex = event.target.getAttribute("data-col-index");
        var value = Number(event.currentTarget.value);
        var mapOfRowTotal = component.get("v.mapOfRowTotal");
        var mapOfColumnTotal = component.get("v.mapOfColumnTotal");
        var wrapperTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var oldValue = wrapperTimeSheetEntries[rowIndex]["lstOfTimeSheetEntries"][colIndex]["GP_Modified_Hours__c"];

        mapOfColumnTotal[colIndex] = mapOfColumnTotal[colIndex] - oldValue + value;
        mapOfRowTotal[rowIndex] = mapOfRowTotal[rowIndex] - oldValue + value;
        wrapperTimeSheetEntries[rowIndex]["lstOfTimeSheetEntries"][colIndex]["GP_Modified_Hours__c"] = value;

        component.set("v.mapOfRowTotal", mapOfRowTotal);
        component.set("v.mapOfColumnTotal", mapOfColumnTotal);
        component.set("v.wrapListOfTimeSheetEntries", wrapperTimeSheetEntries);
    },
    projectChangeHandler: function(component, event, helper) {
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var projectName = event.getParam("recordName");
        var selectedProjectId = event.getParam("sObjectId");
        var index = event.getParam("externalParameter");
        if (selectedProjectId != null) {
            helper.projectChangeHandler(component, selectedProjectId, index, wrapListOfTimeSheetEntries);
        } else {
            wrapListOfTimeSheetEntries[index].projectRecordTypeId = null;
        }
        wrapListOfTimeSheetEntries[index]["projectId"] = selectedProjectId;
        wrapListOfTimeSheetEntries[index]["projectName"] = projectName;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    },
    projectOptionChangeHandler: function(component, event, helper) {
        var mapOfProjectAndAssociatedTasks = component.get("v.mapOfProjectAndAssociatedTasks") || {};
        var wrapTimeSheetEntryIndex = component.get("v.wrapTimeSheetEntryIndex");
        var label = event.getSource().get("v.label");
        var value = event.getSource().get("v.value");
        var noneOption = {
            "text": null,
            "label": "--NONE--"
        };
        var listOfProjectAssociatedTasks = [];
        listOfProjectAssociatedTasks.push(noneOption);
        if (mapOfProjectAndAssociatedTasks[value]) {
            for (var projectId in Object.keys(mapOfProjectAndAssociatedTasks[value])) {
                var taskRecord = mapOfProjectAndAssociatedTasks[value][Object.keys(mapOfProjectAndAssociatedTasks[value])[projectId]];
                var projectTaskOption = {
                    "text": taskRecord.GP_Task_Number__c,
                    "label": taskRecord.GP_Task_Number__c + '--' + taskRecord.Name
                };
                listOfProjectAssociatedTasks.push(projectTaskOption);
            }
        }
        component.set("v.wrapTimeSheetEntry.listOfProjectAssociatedTasks", helper.sort(listOfProjectAssociatedTasks));
    },
    projectTaskChangeHandler: function(component, event, helper) {
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries");
        var projectTaskName = event.getParam("recordName");
        var selectedProjectTaskId = event.getParam("sObjectId");
        var index = event.getParam("externalParameter");
        if (selectedProjectTaskId != null) {
            helper.projectTaskChangeHandler(component, selectedProjectTaskId, index, wrapListOfTimeSheetEntries);
        } else {
            wrapListOfTimeSheetEntries[index].projectRecordTypeId = null;
        }
        wrapListOfTimeSheetEntries[index]["taskId"] = selectedProjectTaskId;
        wrapListOfTimeSheetEntries[index]["taskName"] = projectTaskName;
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    },
    toggleTimesheetEntrySelection: function(component, event, helper) {
        var timesheetTransactionIndex = event.getSource().get("v.name");
        var wrapListOfTimeSheetEntries = component.get("v.wrapListOfTimeSheetEntries") || [];
        for (var index = 0; index < wrapListOfTimeSheetEntries.length; index++) {
            if (index == timesheetTransactionIndex)
                wrapListOfTimeSheetEntries[index].isSelected = wrapListOfTimeSheetEntries[index].isSelected ? true : false;
            else
                wrapListOfTimeSheetEntries[index].isSelected = false;
        }
        component.set("v.wrapListOfTimeSheetEntries", wrapListOfTimeSheetEntries);
    }
})