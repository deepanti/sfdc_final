({
	
		doInit : function(component, event, helper) {
        
        var action=component.get("c.getAllProductForQsrm");
            //action.setStorable();

        action.setParams({"qsrmId": component.get("v.recordId")});
         
            action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                component.set("v.OppProduct", response.getReturnValue());
                component.set("v.OppListSize", (component.get("v.OppProduct")).length);
               console.log("product value:  "+JSON.stringify(component.get("v.OppProduct")));
            } else {
                console.log('Problem getting OppProduct, response state: ' + state);
            }
        });
        $A.enqueueAction(action);
	}
	
})