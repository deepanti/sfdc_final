({
    doInitCall : function(component) {
        var action = component.get("c.getFinancialYear");
        debugger;
        action.setCallback(this, function(response) {
            var getFinancialYearState = response.getState();
            var valuegetFinancialYear = response.getReturnValue();
            if (getFinancialYearState === "SUCCESS") {
                component.set("v.financialYearValueList", valuegetFinancialYear);
            }
        });
        $A.enqueueAction(action); 
        
        var action1 = component.get("c.getCurrentYear");
        debugger;
        action1.setCallback(this, function(response) {
            var getFinancialYearState1 = response.getState();
            var valuegetFinancialYear1 = response.getReturnValue();
            if (getFinancialYearState1 === "SUCCESS") {
                component.set("v.defaultYear", valuegetFinancialYear1);
            }
        });
        $A.enqueueAction(action1);  
    },
    
    targetComponentUpdate : function(component) {
        debugger;
        var userTargetObj = component.get("v.userTargetObj");
        var targetComponent = component.get("v.targetComponent");
        
        var isValid = true;
        for (var i = 0; i < targetComponent.length; i++) {
            targetComponent[i]["error"] = null;
            if(targetComponent[i].Target_Percent__c >100 || targetComponent[i].Target_Percent__c< 0){
                targetComponent[i]["error"] = "Please fill value from 0 to 100";
                isValid = false;
            }
        }
        component.set("v.targetComponent", targetComponent);
        if(!isValid) {
            return;
        }
        var action = component.get("c.updatingTargetComponent");
        action.setParams({
            "userTargetObj": userTargetObj,
            "targetComponent": targetComponent
        });
        
        action.setCallback(this, function(response) {
            var updatingTargetComponentState = response.getState();
            if (updatingTargetComponentState === "SUCCESS" && !$A.util.isUndefinedOrNull(response.getReturnValue())) {
                $A.get('e.force:refreshView').fire();
                /*var homeEvent = $A.get("e.force:navigateToSObject");
                homeEvent.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "detail"
                });
                homeEvent.fire();*/
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            }
        });
        $A.enqueueAction(action);  
    },
    
    saveTarget : function(component){
        //debugger;
        component.set("v.isError",false);
        var userId=component.get("v.userTarget.User__c");
        var fYear=component.get("v.defaultYear");
        var curr =component.get("v.userTarget.CurrencyIsoCode");
        var action1 =component.get("c.checkUserStatus");
        action1.setParams({"usrId" : userId, "financialYear" : fYear});
        
        action1.setCallback(this, function(response1) {
            var res=response1.getReturnValue();
            
            if(res.checkUserStatus==='SUCCESS' && res.checkTrgtStatus==='SUCCESS'){
                var userTarget = component.get("v.userTarget");
                var financialYear = component.get("v.defaultYear");
                var action = component.get("c.saveTargetUser");
                action.setParams({
                    "userTarget": userTarget,
                    "financialYear": financialYear
                });
                action.setCallback(this, function(response) {
                    component.set("v.errorMessage",null);
                    var saveTargetUserState = response.getState();
                    var saveTargetUserValue = response.getReturnValue();
                    if (saveTargetUserState === "SUCCESS" && saveTargetUserValue !== null) {
                        var planComponent = saveTargetUserValue.lstPlanComponent;
                        if(!$A.util.isUndefinedOrNull(planComponent) && planComponent.length > 0 && $A.util.isUndefinedOrNull(saveTargetUserValue.errorMessage) && saveTargetUserValue.isErrorMessage === false){
                            component.set("v.createTargetmodal", false);
                            component.set("v.TargetComponentmodal", true);
                            component.set("v.targetComponent", planComponent);
                            var financialDate = planComponent[0].Target__r.Start_date__c;
                            if(!$A.util.isUndefinedOrNull(financialDate) && financialDate !== ''){
                                var Financialyear = financialDate.substring(0, 4);
                                component.set("v.financeYear", Financialyear);
                            }
                        }
                        else if($A.util.isUndefinedOrNull(planComponent) && !$A.util.isUndefinedOrNull(saveTargetUserValue.errorMessage) && saveTargetUserValue.isErrorMessage === true){
                            component.set("v.errorMessage", saveTargetUserValue.errorMessage);
                            component.set("v.isError", saveTargetUserValue.isErrorMessage);
                        }
                    }
                });
                $A.enqueueAction(action);
            }
            else{
                if(res.checkUserStatus==='SUCCESS' && res.checkTrgtStatus==='FAIL'){
                component.set("v.isError",true);
                component.set("v.errorMessage", 'Target is already defined for this User.');
                }
                else{
                    component.set("v.isError",true);
                	component.set("v.errorMessage", 'User is inactive or not applicable for VIC.');
                }
            }
        });
        $A.enqueueAction(action1);
    },
    
    saveComponentTarget : function(component) {
        debugger;
        var targetPercent= component.find("targetPercent");
        var targetComponent = component.get("v.targetComponent");
        var isValid = true;
        
        for (var i = 0; i < targetComponent.length; i++) {
            targetComponent[i]["error"] = null;
            if(targetComponent[i].Target_Percent__c >100 || targetComponent[i].Target_Percent__c< 0){
                targetComponent[i]["error"] = "Please fill value from 0 to 100";
                isValid = false;
            }
        }
        
        component.set("v.targetComponent", targetComponent);
        
        if(!isValid) {
            return;
        }
        var action = component.get("c.saveingTargetComponent");
        action.setParams({
            "targetComponent": targetComponent
        });
        action.setCallback(this, function(response) {
            var targetComponentState = response.getState();
            if (targetComponentState === "SUCCESS" && !$A.util.isUndefinedOrNull(response.getReturnValue())) {
                var homeEvent = $A.get("e.force:navigateToSObject");
                homeEvent.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "detail"
                });
                homeEvent.fire();
            }
        });
        $A.enqueueAction(action);  
    },
    
    getTargetUserDetails : function(component, recordId) {
        debugger;
        var action = component.get("c.fetchTargetUserDetails");
        action.setParams({
            "recordId": recordId
        });
        debugger;
        action.setCallback(this, function(response) {
            var fetchTargetUserDetailsState = response.getState();
            var fetchTargetUserDetailsValue = response.getReturnValue();
            if (fetchTargetUserDetailsState === "SUCCESS" && !$A.util.isUndefinedOrNull(fetchTargetUserDetailsValue)) {
                if(!$A.util.isUndefinedOrNull(fetchTargetUserDetailsValue.lstPlanComponent) && fetchTargetUserDetailsValue.lstPlanComponent.length > 0){
                    component.set("v.targetComponent", fetchTargetUserDetailsValue.lstPlanComponent);
                }
                component.set("v.userTargetObj", fetchTargetUserDetailsValue.usertargetObj);
                if(!$A.util.isUndefinedOrNull(fetchTargetUserDetailsValue.financeialYear)){
                    component.set("v.financeialYear", fetchTargetUserDetailsValue.financeialYear); 
                }
                component.set("v.TargetComponentmodal", true);
            }
        });
        $A.enqueueAction(action); 
    },
    
    showErroToast : function(component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : 'Error Message',
            message: 'Weightage percent cannot be greater than 100.',
            duration:' 3000',
            key: 'info_alt',
            type: 'error',
            mode: 'dismissible'
        });
        toastEvent.fire();
    }
})