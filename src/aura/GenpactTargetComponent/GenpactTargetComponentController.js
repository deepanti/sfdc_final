({
    doInit : function(component, event, helper) {
        var recordId = component.get("V.recordId"); 
        if(!$A.util.isUndefinedOrNull(recordId) && recordId !== ''){
           component.set("v.createTargetmodal", false);
           component.set("v.updateTargetModal", true); 
           helper.getTargetUserDetails(component, recordId);  
        }
       
        else if($A.util.isUndefinedOrNull(recordId) || recordId === ''){
             helper.doInitCall(component);
        }
    },
    
    cancelTargetComponent : function(component, event, helper) {
        var recordId = component.get("V.recordId");
         var homeEvent = $A.get("e.force:navigateToSObject");
                homeEvent.setParams({
                   "recordId": recordId,
                   "slideDevName": "detail"
                });
                homeEvent.fire();    
    },
    
    saveUserTarget : function(component, event, helper) {
        helper.saveTarget(component);
    },
    
    saveTargetComponent : function(component, event, helper) {
        var isSuccess = 0;
        var weightageList = component.find('inputWtgId');
        if($A.util.isUndefined(weightageList)){
            helper.saveComponentTarget(component, event, helper);
        }else{
            if($A.util.isUndefined(weightageList.length)){
                if(weightageList.get("v.value")>100){
					helper.showErroToast(component, event, helper);
                }else{
                 	helper.saveComponentTarget(component, event, helper);   
                }
            }else{
                for(var i=0;i<weightageList.length;i++){
                    if(weightageList[i].get("v.value")>100){
                        isSuccess = 1;
                    }
                }
                if(isSuccess ==0){
                    helper.saveComponentTarget(component, event, helper);
                }else{
                    helper.showErroToast(component, event, helper);
                }
            }
        }
    },
    
     updateTargetComponent : function(component, event, helper) {
        helper.targetComponentUpdate(component);
    },
    
    cancleUserTarget : function(component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Target__c"
        });
        homeEvent.fire();
    }
})