({
    INSTRUCTION_MAP: {
        'Update Project': '1. Date format should be of the format yyyy-mm-dd. <br/> 2. Product SFDC ID should be filled in Product column. <br/> 3. Oracle Project Number should be filled in Project column. <br/>4. SDO Oracle Id should be filled for Primary SDO column. <br/>5. Start & End Date should lie between contract start & end date. <br/>6. HSL should be filled with HSL Unique Number. <br/>7. Email address should be of the format ‘abc@xyz.com’. (Domain name should be valid)',
        'Update Project Leadership': '1. Date format should be of the format yyyy-mm-dd. <br/>2. Oracle Project Number should be filled in Project column. <br/>3. Employee Number(OHR) should be filled in Employee Number column. <br/>4. Start & End Date should lie between project start & end date. <br/>5. Leadership Role should be filled with active Leadership Name for the project.(Billing Approver, Billing Spoc, FP&A, Global Project Manager, PID Approver Finance)',
        'Update Project Work Location & SDOs': '1. Work Location Oracle Id should be filled in Work Location column. <br/>2. Primary will have a value as ‘TRUE’ or ’FALSE’. <br/> 3. Oracle Project Number should be filled in Project column.<br/>4. BCP Flag will have a value as ‘Yes’ or ’No’. <br/>',
        'Upload Time Sheet Entries': '1. Date format should be of the format yyyy-mm-dd. <br/>2. Month-year should be of the format mmm-yy. <br/>3. Oracle Project Number should be filled in Project column. <br/>4. Employee Number(OHR) should be filled in Employee Number column. <br/>5. Modified hours should be number only. <br/>6. Project Task should be filled with Task Oracle Number.'
    },
    getBulkUploadMetadata: function(component) {
        var getJobConfigurationDataService = component.get("c.getJobConfigurationDataService");
        getJobConfigurationDataService.setParams({
            "jobId": component.get("v.recordId")
        });
        getJobConfigurationDataService.setCallback(this, function(response) {
            this.getJobConfigurationDataServiceHandler(response, component);
        });

        $A.enqueueAction(getJobConfigurationDataService);
    },
    getJobConfigurationDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        var responseJson = JSON.parse(responseData.response);
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.exportfileName", responseJson.exportfileName);
            component.set("v.parseToObjectLabel", responseJson.parseToObjectLabel);
            component.set("v.instructions", responseJson.instructions);

            this.setHeaderRow(component, responseJson.headerRow);
            this.setInstructions(component, responseJson);
            this.setlabelFieldToApiName(component, responseJson.mapOfColumnNameToFieldAPIName);
            this.hideSpinner(component);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setHeaderRow: function(component, headerRowChild) {
        //var headerRowChild = this.DEFAULT_VALUES_HEADER_ROW;
        var headerRowWrapper = [];
        headerRowWrapper.push(headerRowChild);
        component.set("v.headerRow", headerRowWrapper);
    },
    setInstructions: function(component, responseJson) {
        var jobType = responseJson.jobType;
        var instructions = this.INSTRUCTION_MAP[jobType];
        component.set("v.instructions", instructions);
    },
    setlabelFieldToApiName: function(component, labelFieldToApiName) {
        component.set("v.labelFieldToApiName", labelFieldToApiName);
    },
    setObjectApiNamesToColumn: function(component, listOfMassRecords) {
        var labelFieldToApiName = component.get("v.labelFieldToApiName");
        var jobId = component.get("v.recordId");
        var parsedListOfRecords = [];

        for (var index = 0; index < listOfMassRecords.length; index++) {
            var parsedTemporaryRecord = {
                "GP_Job_Id__c": jobId
            };
            var csvRecord = listOfMassRecords[index];
            for (var key in Object.keys(csvRecord)) {
                var csvFieldLabel = Object.keys(csvRecord)[key];
                var fieldApiName = labelFieldToApiName[csvFieldLabel.toLowerCase()];
                var value = csvRecord[csvFieldLabel];
                parsedTemporaryRecord[fieldApiName] = value;
            }
            parsedListOfRecords.push(parsedTemporaryRecord);
        }

        listOfMassRecords = parsedListOfRecords;
        return listOfMassRecords;
    },
    insertTemporaryRecords: function(component, listOfMassRecords) {
        var insertTemporaryRecordsService = component.get("c.insertTemporaryRecordsService");
        var parseToObjectLabel = component.get("v.parseToObjectLabel");
        
        insertTemporaryRecordsService.setParams({
            "serializedTemporaryData": JSON.stringify(listOfMassRecords),
            "JobType": parseToObjectLabel,
            "fileName": component.get("v.fileName"),
            "jobId": component.get("v.recordId")
        });
        insertTemporaryRecordsService.setCallback(this, function(response) {
            this.insertTemporaryRecordsServiceHandler(response, component);
        });

        $A.enqueueAction(insertTemporaryRecordsService);
    },
    insertTemporaryRecordsServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        var parseToObjectLabel = component.get("v.parseToObjectLabel");
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Records Uploaded Successfully for Job Type : ' + parseToObjectLabel + '!.');
        } else {
            this.handleFailedCallback(component, responseData);
        }
        this.closeModal();
    }
})