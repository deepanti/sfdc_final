({
    doInit: function(component, event, helper) {
        component.set("v.openMassUpload", true);
        helper.getBulkUploadMetadata(component);
    },
    onChangelistOfMassRecordsService: function(component, event, helper) {
        var listOfMassRecords = component.get("v.listOfMassRecords");
        listOfMassRecords = helper.setObjectApiNamesToColumn(component, listOfMassRecords);

        //helper.processMassList(component,listOfMassBillingMilestone);
        
        helper.insertTemporaryRecords(component, listOfMassRecords);
    }
})