({
    /*
		@author: Karthik Chekkilla
		@company: Saasfocus
		@description: This method is used to fire button click events.
	*/
     onButtonClickHandler : function(component, event, helper) {
		var cmpId = event.getSource().getLocalId();
        if(cmpId === "cancelBtnId") {
            component.set("v.showModal",false); 
           // component.set("v.holdValue","");
            component.set("v.holdDescription","");
            var btnClickEvent = component.getEvent("buttonClickEvent");
            btnClickEvent.setParams({
                "Value" : component.get("v.holdValue"),
                "Description" : "",
                "Source" : component.get("v.Source"),
                "Operation": "handleCancelBtn"       
            });
            btnClickEvent.fire();
        }else if(cmpId === "saveBtnId") {
            var btnClickEvent = component.getEvent("buttonClickEvent");
            btnClickEvent.setParams({
                "Value" : component.get("v.holdValue"),
                "Description" : component.get("v.holdDescription"),
                "Source" : component.get("v.Source"),
                "Operation": "handleSaveBtn"       
            });
            btnClickEvent.fire();
        }
	}
})