({
    doInit : function(component, event, helper) {
        var recID = component.get("v.recordId");
        var checkReturn = component.get("v.checkStatusApproval");
        var action = component.get("c.getProcessInstancesmethod");
        action.setParams({ "QsrmID" : recID, "statusCheck" : checkReturn });
        var self = this;
        action.setCallback(this, function(actionResult) {
            component.set('v.ProcessInstanceslist', actionResult.getReturnValue());
         });
        $A.enqueueAction(action);
        
        var recID = component.get("v.recordId");
        var actionQSRM = component.get("c.getQSRMObjMethod");
        actionQSRM.setParams({
            "QsrmID" : recID
        });
        var self = this;
        actionQSRM.setCallback(this, function(actionResult) {
            component.set('v.QSRMObject', actionResult.getReturnValue());
           });
        $A.enqueueAction(actionQSRM);
    },
    
    approveMethod: function(component,event,helper){
       helper.approverAction(component,helper,"Approve");
    },
    
    rejectMethod: function(component,event,helper){
        helper.approverAction(component,helper,"Reject");
    },
    
    reviewMethod: function(component,event,helper){
       helper.approverAction(component,helper,"Removed");
    },
    
    openModel: function(component, event, helper) {
        component.set("v.ReworkOpenpopup", true);
    },
    
    approveOpenModel: function(component, event, helper) {
        component.set("v.approveOpenPopup", true);
    },
    
    rejectOpenModel: function(component, event, helper) {
        component.set("v.rejectOpenPopup", true);
    },
    
    closeModel: function(component, event, helper) {
        component.set("v.approveOpenPopup", false);
        component.set("v.rejectOpenPopup", false);
         component.set("v.ReworkOpenpopup", false);
    },
    
})