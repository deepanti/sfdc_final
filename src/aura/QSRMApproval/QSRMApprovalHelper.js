({  
    approverAction : function(component,helper,approvedAction){
        component.set("v.spinner",true);
        var recID = component.get("v.recordId");
        var commentsEntered = component.find("commentsId").get("v.value");
        var comm = component.find("comments");
       // alert('approvedAction helper method=='+approvedAction);
        if($A.util.isUndefinedOrNull(commentsEntered) || commentsEntered == '')
        { 
           // alert('approvedAction helper method==if');
            $A.util.addClass(comm,'slds-has-error');
            helper.showToast("error", "Please Provide Appropriate Comments.");
            component.set("v.spinner",false);
        }
        else
        {
           // alert('approvedAction helper method==else');
            $A.util.removeClass(comm,'slds-has-error');
            var successMsg ='';
            if(approvedAction =='Approve'){
                successMsg ='QSRM is approved successfully.';
            }else if(approvedAction =='Reject'){
                successMsg ='QSRM is rejected and the opportunity will be marked as Dropped.';
            }else if(approvedAction =='Removed'){
                successMsg ='Rework request has been submitted to Opportunity owner successfully.';
            }
            var action = component.get("c.approverActionMethod");
            action.setParams({ "QsrmID" : recID, "Action" : approvedAction,"commentsData" : commentsEntered});
            action.setCallback(this, function(actionResult) {
                if(actionResult.getState()=="SUCCESS"){
                    component.set('v.ProcessInstanceslist', actionResult.getReturnValue());
                    component.set("v.approveOpenPopup", false);
                    component.set("v.rejectOpenPopup", false);
                    component.set("v.ReworkOpenpopup", false);
                    component.set("v.spinner",false);
                    helper.showToast("Success", successMsg);
                }
                else{
                    helper.showToast("error", actionResult.getError()[0].message);
                    component.set("v.spinner",false);
                    component.set("v.approveOpenPopup", false);
                    component.set("v.rejectOpenPopup", false);
                    component.set("v.ReworkOpenpopup", false);
                }
                
            });
            $A.enqueueAction(action);
    } 
    },
	showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    }
})