({
	IndustryVertical : function (component){
        var AccountName = component.get("v.AccountSelectedLookUpRecord");
        var action = component.get("c.IndustryVertical");
        ction.setParams({ 
                   AccName:AccountName
                });
        action.setCallback(this, function(response){            
            component.set("v.IndustryVertical", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    
    createcaseRecord : function(component){
      alert('test create case');
        var action = component.get("c.CreateCaseRecord");
        action.setParams({ firstName : 'test name' });
        action.setCallback(this, function(response) 
          {
            var state = response.getState();
                alert('======state======'+state);
       });
        $A.enqueueAction(action);
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    }
})