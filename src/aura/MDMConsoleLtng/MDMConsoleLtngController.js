({
    doInit : function(component, event, helper) {
        var options = [];
        
        var action = component.get("c.RequestReasonList");
        action.setCallback(this, function(response){  
            if(response.getState() == 'SUCCESS'){
                var wraperList = response.getReturnValue();
                console.log(wraperList.requestResponseList);
                component.set("v.listOfItems", wraperList.requestResponseList);
                console.log(wraperList.Industrylist);
                component.set("v.industry", wraperList.Industrylist);
                console.log(wraperList.titleList);
                component.set("v.titlewrapper", wraperList.titleList);
                console.log(wraperList.buyinghList);
                component.set("v.buyingCentre", wraperList.buyinghList);
                console.log(wraperList.Levellist);
                component.set("v.contractwrapper", wraperList.Levellist);
                console.log(wraperList.Countrylist);
                component.set("v.countrywrapper", wraperList.Countrylist);
                console.log(wraperList.Accountlist);
                component.set("v.accountrywrapper", wraperList.Accountlist);
                console.log(wraperList.ArchetypeList);
                component.set("v.archetypewrapper", wraperList.ArchetypeList);
            }else{
                // alert('test fail');
            }
            
        });
        $A.enqueueAction(action);
        
        
    },
    createCase : function(component, event, helper){
        component.set("v.newCaseWin", true);
    },
    
    cancelCase : function(component, event, helper){
        component.set("v.newCaseWin", false);
    },
    
    selectoptionvalue :function(component, event, helper) {
    },
    selectAll:function(component, event, helper) {
        alert(component.get("v.selected"));
    },
    
    onCheck: function(cmp, evt) {
        try{
            var checkCmp = cmp.get("v.OtherEnabler");
            var buttonEnable = cmp.get("v.ButtonEnabler");
            var val = cmp.get("v.selected");
            if(checkCmp){
                cmp.set("v.OtherEnabler",false);   
                cmp.set("v.ButtonEnabler",false);
            }else{
              cmp.set("v.othertextBox",'');
                if(val != null && val != ''){
                    cmp.set("v.ButtonEnabler",false);  
                }else{
                    cmp.set("v.ButtonEnabler",true);
                }
                cmp.set("v.OtherEnabler",true);
            }
        }catch(err) {
            alert(':===== error message====:'+err.message+':===error line====:'+err.line);
        }
    },
    handleChange: function(cmpp, evt) {
        var checkCmp = cmpp.get("v.OtherEnabler");
        var val = cmpp.get("v.selected");
        if((val != null && val != '') || !checkCmp){
            cmpp.set("v.ButtonEnabler",false);  
        }else{
            cmpp.set("v.ButtonEnabler",true);
        }
    },
    createMDM : function(component, event) {
        var SelectedValue;
        var textvaluem;
        var completevalue;
        var selectBox = false;
        var checkboxValue = component.get("v.selected");
        var textvalue = component.find("otherTextbox").get("v.value");
        var textFlag = true;
        var checkCmp = component.get("v.OtherEnabler");
        
        if(!checkCmp){
            textFlag = false; 
            selectBox = true;
        }
        
        if(textvalue != null && textvalue != '' && !textvalue.trim().length < 1){
            textFlag = true;
        }
        var cmval;
        var UrlLabel = $A.get("$Label.c.MDM_Report_LInk");
      // var UrlLabel ="https://genpact--preprod--c.cs72.visual.force.com/apex/MDMTableVFP";
        if(checkboxValue != null){
            cmval = checkboxValue.toString();
        }
        if(cmval != null && cmval != ''){
            selectBox = true;
        }
         if(selectBox &&  textFlag){
            var action = component.get("c.CreateRecord");
            action.setParams({  
                selectValue : cmval,
                othersVal : textvalue
            });
            action.setCallback(this, function(response){  
                if(response.getState() == 'SUCCESS'){
                    var response = response.getReturnValue();
                    component.set("v.isSuccess",true);
                     window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.isSuccess", false); 
                        }),3000
                    );
                }else{
                    component.set("v.isError",true);
                    window.setTimeout(
                        $A.getCallback(function() {
                            component.set("v.isError", false); 
                        }),3000
                    );
                }
            });
            $A.enqueueAction(action);
            window.setTimeout(
                $A.getCallback(function() {
                    window.open(UrlLabel,'_blank');
                }),2000
            );
        }else{
            component.set("v.isError", true);
             window.setTimeout(
                $A.getCallback(function() {
                    component.set("v.isError", false);
                }),3000
            );
         }
    },
    
    closeSuccessToast : function(component,event){
        component.set("v.isSuccess",false);
    },
    
    closeErrorToast : function(component,event){
        component.set("v.isError", false); 
    },
    
    
    
    OnSelect : function(component,event){
        var sId = component.find("checkbox1").get("v.value");
        alert("OnSelect"+component.get("v.Selectvalue"));
    },
    handleClick: function(component, event, helper) {
        var mainDiv = component.find('main-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    handleMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('main-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.industryVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var IndustrySpan = component.find('Industry-span');
            $A.util.removeClass(IndustrySpan, 'beforeSelectionColour');
        }else {
            var IndustrySpan = component.find('Industry-span');
            $A.util.addClass(IndustrySpan, 'beforeSelectionColour');
           event.preventDefault();
            val = 'Select Industry Verticals';
        }
        component.set("v.industryText",val); 
    },
      
    BuyClick: function(component, event, helper) {
        var mainDiv = component.find('Buy-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    BuyMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('Buy-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.buyingCentreVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var BuySpan = component.find('Buy-span');
            $A.util.removeClass(BuySpan, 'beforeSelectionColour');
        }else {
            var BuySpan = component.find('Buy-span');
            $A.util.addClass(BuySpan, 'beforeSelectionColour');
            event.preventDefault();
            val = 'Select Buying Centre';
        }
        component.set("v.buyingCentreText",val); 
    },
    LevelClick: function(component, event, helper) {
        var mainDiv = component.find('Level-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    LevelMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('Level-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.levelOfContactVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var LevelSpan = component.find('Level-span');
            $A.util.removeClass(LevelSpan, 'beforeSelectionColour');
        }else {
            var LevelSpan = component.find('Level-span');
            $A.util.addClass(LevelSpan, 'beforeSelectionColour');
            event.preventDefault();
            val = 'Select Level Of Contacts';
        }
        component.set("v.levelofcontactText",val); 
    },
    contryClick: function(component, event, helper) {
        var mainDiv = component.find('contry-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    
    contryMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('contry-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.countryval")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var CountrySpan = component.find('Country-span');
            $A.util.removeClass(CountrySpan, 'beforeSelectionColour');
        }else {
             var CountrySpan = component.find('Country-span');
            $A.util.addClass(CountrySpan, 'beforeSelectionColour');
            event.preventDefault();
            val = 'Select Country Of Residence';
        }
        component.set("v.countrytypeText",val); 
    },
    AccountClick: function(component, event, helper) {
        var mainDiv = component.find('Account-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    AccountMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('Account-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.AccountVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
        }else {
            val = 'Select The Account ';
        }
        component.set("v.AccountText",val); 
    },
    
    ArchClick: function(component, event, helper) {
        var mainDiv = component.find('Arch-div');
        $A.util.addClass(mainDiv, 'slds-is-open');
        event.preventDefault();
        return false;
    },
    
    
    
    ArchMouseLeave : function(component, event, helper) {
        var mainDiv = component.find('Arch-div');
        $A.util.removeClass(mainDiv, 'slds-is-open');
        var selectValue = component.get("v.archetypeVal")
        var val;
        if(selectValue != null && selectValue != ''){
            val = selectValue.toString();
            var ArchSpan = component.find('Arch-span');
            $A.util.removeClass(ArchSpan, 'beforeSelectionColour');
        }else {
            var ArchSpan = component.find('Arch-span');
            $A.util.addClass(ArchSpan, 'beforeSelectionColour');
            event.preventDefault();
            val = 'Select Archetype';
        }
        component.set("v.archetypeText",val); 
    },
    
    ResetCase : function(component, event, helper) {
        component.set("v.industryVal","");
        var val = 'Select Industry Verticals';
        component.set("v.industryText",val); 
        var IndustrySpan = component.find('Industry-span');
            $A.util.addClass(IndustrySpan, 'beforeSelectionColour');
           
        
        component.set("v.titleVal","");
        
        component.set("v.buyingCentreVal","");
        var val1 = 'Select Buying Centre';
        component.set("v.buyingCentreText",val1); 
         var BuySpan = component.find('Buy-span');
            $A.util.addClass(BuySpan, 'beforeSelectionColour');
        
        component.set("v.levelOfContactVal","");
        var val2 = 'Select Level Of Contacts';
        component.set("v.levelofcontactText",val2); 
        var LevelSpan = component.find('Level-span');
            $A.util.addClass(LevelSpan, 'beforeSelectionColour');
            
        
        component.set("v.countryval","");
        var val3 = 'Select Country Of Residence';
        component.set("v.countrytypeText",val3);
         var CountrySpan = component.find('Country-span');
            $A.util.addClass(CountrySpan, 'beforeSelectionColour');
        
        component.set("v.archetypeVal","");
        var val4 = 'Select Archetype';
        component.set("v.archetypeText",val4); 
        var ArchSpan = component.find('Arch-span');
            $A.util.addClass(ArchSpan, 'beforeSelectionColour');
    
        component.set("v.AccountSelectedLookUpRecord","");
        component.set("v.ValidReason","");
        component.set("v.campaignName","");
    },
    
    createdummyCase : function(component, event,helper){
        try {
            var industryselect = component.get("v.industryVal");
            var title = component.get("v.titleVal");
            var buyingcenter = component.get("v.buyingCentreVal");
            var levelcontract = component.get("v.levelOfContactVal");
            var countrySelect = component.get("v.countryval");
            var accountArctye = component.get("v.archetypeVal");
            var AccIDval = component.get("v.AccountSelectedLookUpRecord");
            var validreason = component.get("v.ValidReason");
            var campaignName = component.get("v.campaignName");
            var indusflag,titleflag,buyingflag,levelflag,countryflag,accLookupflag,archtypeflag,validreasonflag,campaignflag;
            
            if(AccIDval != null && AccIDval != ''){
            var accLookup = AccIDval.Id;
            }else{
                accLookupflag = false;
            }
            
            indusflag = titleflag = buyingflag = levelflag =countryflag = archtypeflag = validreasonflag = accLookupflag = campaignflag  = false;
            var industryselectval;
            var titleval;
            var buyingcenterval;
            var levelcontractval;
            var countrySelectval;
            var accLookupval;
            var accountArctyeval;
            var validreasonval;
            var campaignval;
            if(industryselect != null && industryselect != ''){
                var industryselectval = industryselect.toString(); 
                indusflag = true;
            }
            if(title  != null && title != '' && !title.trim().length < 1){
                titleval = title.toString();
                titleflag = true;
            }
            if(buyingcenter  != null && buyingcenter != ''){
                buyingcenterval = buyingcenter.toString();
                buyingflag = true;
            }
            if(levelcontract  != null && levelcontract != ''){
                levelcontractval = levelcontract.toString();
                levelflag = true;
            }
            if(countrySelect  != null && countrySelect != ''){
                countrySelectval = countrySelect.toString();
                countryflag = true;
            }
            if(accLookup  != null && accLookup != ''){
                accLookupval = accLookup.toString();
                accLookupflag = true;
            }
            if(accountArctye  != null && accountArctye != ''){
                accountArctyeval = accountArctye.toString();
                archtypeflag = true;
            }
            if(validreason  != null && validreason != '' && !validreason.trim().length < 1){
                validreasonval = validreason.toString();
                validreasonflag = true;
            }
            if(campaignName  != null && campaignName != '' && !campaignName.trim().length < 1){
                campaignval = campaignName.toString();
                campaignflag = true;
            }
                
          
           
            
            if(indusflag && titleflag && buyingflag && levelflag &&countryflag && archtypeflag  && validreasonflag && accLookupflag && campaignflag){ 
                
                var action = component.get("c.CreateCaseRecord");
                action.setParams({ 
                    IndustryVertical : industryselectval,
                    title: titleval,
                    buyingcenter : buyingcenterval,
                    levelcontract:levelcontractval,
                    countrySelect:countrySelectval,
                    accountArctye:accountArctyeval,
                    validreason:validreasonval,
                    accLookupvalue:accLookupval,
                    campaignName:campaignval
                });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        //alert('test success');
                        component.set("v.newCaseWin", false);
                        component.set("v.isSuccess",true);
                        window.setTimeout(
                            $A.getCallback(function() {
                                component.set("v.isSuccess", false); 
                            }),3000
                        );
                        
                    }
                    else if (state === "INCOMPLETE") {
                        alert('test incomplete');
                    }  else{
                        alert('serversice falil');
                    }
                });
                
                $A.enqueueAction(action); 
              }else{
                var allMandatory;
                if(!indusflag){
                    allMandatory = 'Please Select Industry Vertical \n';
                }
                
                   if(!levelflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Select Level Of Contact \n';
                    }else{
                        allMandatory = 'Please Select Level Of Contact \n';
                    }
                }
                  if(!countryflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Select Country Of Residence \n';
                    }else{
                        allMandatory = 'Please Select Country Of Residence \n';
                    }
                }
                if(!buyingflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Select Buying Centre \n';
                    }else{
                        allMandatory = 'Please Select Buying Centre \n';
                    }
                }
                if(!archtypeflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Select Account Archetype \n';
                    }else{
                        allMandatory = 'Please Select Account Archetype \n';
                    }
                }
                if(!titleflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Fill Title \n';
                    }else{
                        allMandatory = 'Please Fill Title \n';
                    }
                }
                
                if(!accLookupflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Fill Account \n';
                    }else{
                        allMandatory = 'Please Fill Account \n';
                    }
                }
               
                if(!validreasonflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Please Provide Valid Reason For A New List Build Request \n';
                    }else{
                        allMandatory = 'Please Provide Valid Reason For A New List Build Request \n';
                    }
                }
                if(!campaignflag){
                    if(allMandatory != null && allMandatory != ''){
                        allMandatory = allMandatory + 'Provide campaign name in case data is requested for a campaign \n';
                    }else{
                        allMandatory = 'Provide campaign name in case data is requested for a campaign \n';
                    }
                }
                  helper.fireToastError(component, event,helper,allMandatory);
                //alert(allMandatory);
              } 
        }
        catch(err) {
            alert(':===== error message====:'+err.message+':===error line====:'+err.line);
        }
    }
    
});