({
	init : function(component, event, helper) {
        var recordId = component.get("v.recordId");
        console.log('Inside');
        console.log(recordId);
        component.set("v.OliRecordId" ,recordId);
        var action = component.get("c.executeBatchApexMethod");
        action.setParams({
            "OliId" : recordId 
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=="SUCCESS"){
                component.set("v.successMessage" , true);
            }
        });
        $A.enqueueAction(action);
	},
    
    closeModel : function(component,event,helper){
        var oliRecordId = component.get("v.OliRecordId");
        component.set("v.successMessage" , false);
    }   
     
})