({
    DEFAULT_ERROR_LABEL: 'Something has went wrong!',
    getPickListOptions: function(component) {
        var getPicklistOptionsService = component.get("c.getPicklistOptions");

        getPicklistOptionsService.setParams({
            "sObjectName": component.get("v.objectAPIName"),
            "sObjectFieldName": component.get("v.fieldAPIName")
        });

        getPicklistOptionsService.setCallback(this, function(response) {
            this.getOptionHandler(response, component)
        });

        $A.enqueueAction(getPicklistOptionsService);
    },
    getOptionHandler: function(response, component) {
        var responseData = response.getReturnValue() || [];
        if (component.isValid() && response.getState() === "SUCCESS") {
            this.setPicklistOptions(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setPicklistOptions: function(component, listOfOptions) {
        var addNoneAsDefault = component.get("v.addNoneAsDefault");

        if(addNoneAsDefault) {
            listOfOptions.unshift({
                "label": "--None--"
            });
        }
        component.set("v.listOfOptions", listOfOptions);
    },
    handleFailedCallback: function(component, responseData) {
        var errorMessage = responseData.message || this.DEFAULT_ERROR_LABEL;
        this.showToast('Error', 'error', errorMessage);
    },
    showToast: function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.setParams({
                "title": title,
                "type": type,
                "message": message
            });
            toastEvent.fire();
        } else {
            alert(message);
        }
    }
})