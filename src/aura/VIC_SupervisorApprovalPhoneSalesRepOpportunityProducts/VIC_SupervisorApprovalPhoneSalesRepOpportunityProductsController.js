({
	doInit : function(component, event, helper) {
        console.log('init called');
	},
    onRender : function(component, event, helper){
        var sliderId = component.getGlobalId() + '_slides';
        var slides = document.getElementById(sliderId);
        console.log('slides: ', slides);
        var parentOfSlider = slides.parentElement;
        console.log('parentOfSlider: ', parentOfSlider);
        var parentWidth = parentOfSlider.offsetWidth;
        console.log('parentOfSlider width: ', parentOfSlider.offsetWidth);
        component.set('v.sliderWidth', parentWidth);
        
    },
    moveToPrev : function(component, event, helper) {
        var currentOppProdIndex = component.get('v.currentOppProdIndex');
        if(currentOppProdIndex > 0){
            component.set('v.currentOppProdIndex', currentOppProdIndex - 1);
        }
        //helper.moveToOppProd(component);
	},
 	moveToNext : function(component, event, helper) {
        var currentOppProdIndex = component.get('v.currentOppProdIndex');
        var oppProds = component.get('v.lstOLIDataWrapLine');
        if(currentOppProdIndex < oppProds.length - 1){
            component.set('v.currentOppProdIndex', currentOppProdIndex + 1);
        }
        //helper.moveToOppProd(component);
	}
})