({
    doInit: function(component, event, helper) {
        helper.displayApplication(component);
        var displayApplication = component.get("v.displayApplication");
        if(displayApplication){
            helper.fetchProjectData(component);
        }else{
            helper.hideSpinner(component);
        }
        
    },
    
    cloneProject: function(component, event, helper) {
        helper.CheckResourceSync(component);
       
    },
    submitforApproval: function(component, event, helper) {
        component.set("v.isSubmitforapproval", true)
    },
    projectAssignment: function(component, event, helper) {
        component.set("v.isProjectAssignment", true)
    },
    approveReject: function(component, event, helper) {
        component.set("v.isApproveReject", true)
    },
    submitforClosure: function(component, event, helper) {
        component.set("v.isSubmitforClosure", true)
    },
    deleteProject: function(component, event, helper) {
        component.set("v.isDelete", true)
    },
    cloneAndDeleteProject: function(component, event, helper) {
        component.set("v.isCloneAndDelete", true)
    },
    recallApproval: function(component, event, helper) {
        component.set("v.isrecall", true)
    },
    closeModel: function(component, event, helper) {
        component.set("v.isClone", false);
        component.set("v.isSubmitforapproval", false);
        component.set("v.isProjectAssignment", false);
        component.set("v.isApproveReject", false);
        component.set("v.isSubmitforClosure", false);
        component.set("v.isDelete", false);
        component.set("v.isrecall", false);
        component.set("v.isCloneAndDelete",false);
        
    },
})