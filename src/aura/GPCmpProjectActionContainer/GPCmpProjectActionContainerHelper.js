({
	fetchProjectData : function(cmp) {
		
        var action = cmp.get("c.getUserAccess");
        action.setParams({
            strRecordId: cmp.get("v.recordId")
        });
        $A.enqueueAction(action);
        action.setCallback(this, function(response) {
            var state = response.getState();
            var responseData = response.getReturnValue();
            if (state === "SUCCESS" && responseData.isSuccess) {
                var JSONResponse = JSON.parse(responseData.response);
                debugger;
                console.log('JSONResponse',JSONResponse);
                cmp.set( "v.UserAccess",JSONResponse);
            } else if (state === "INCOMPLETE") {
            
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {}
                } else {}
            }
            this.hideSpinner(cmp);
        });

    
    },
    
    CheckResourceSync : function(cmp)
    {
         var IsResourceSync=false;
         var recordId= cmp.get("v.recordId");
         var action = cmp.get("c.isResourceSync");
         action.setParams({
            strRecordId: recordId
        });
       
        action.setCallback(this, function(response) {
            var state = response.getState();
            var responseData = response.getReturnValue();
            if (state == "SUCCESS" && responseData =='Y') {
                 IsResourceSync=true; 
                  
                 cmp.set("v.isClone", true);
            } 
            else{
                   cmp.set("v.isClone", false);
                 this.showToast('ERROR', 'ERROR', ' Resource data still not synced with Oracle.Please modify PID later.');
            }
            
            
        });
         $A.enqueueAction(action);
       
    } 

})