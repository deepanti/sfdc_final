({
	showAnnouncementDetails : function(component, event, helper) {
        //check if the file is present on annoucement or not
		if(component.get("v.fileId")!='' && component.get("v.fileId")!=null && component.get("v.fileId")!=undefined)
           helper.openFile(component);	//if yes, open file
        else
           helper.redirectToRecord(component); //if no, open announcement
            
	},
    hideAnnouncement :function(component, event, helper) {
        //Hide announcement on close
        component.set("v.isVisible",false);
        helper.updatePreference(component,helper);
    }
})