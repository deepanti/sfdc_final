({
	updatePreference : function(component,helper) {
        //Store the user preference for closed announcement
		var action = component.get("c.updateUserPreference");
        action.setParams({
            AnnouncementId : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            if(response.getState()!='SUCCESS')
            {
                component.set("v.isVisible",true);
                helper.showToast('error','Something went wrong, please try again.')
            }
        });
        $A.enqueueAction(action);
	},
    redirectToRecord : function(component)    {
        //Redirect to announcement record
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.recordId")
        });
        navEvt.fire();
    },
    openFile: function(component) {
        //Open the file preview
		$A.get('e.lightning:openFiles').fire({
		    recordIds: [component.get("v.fileId")]
		});
	},
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    },
})