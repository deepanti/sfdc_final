({
	doInit: function(component, event, helper) {
        helper.doInitHelper(component);
	},
    editAddress: function(component, event, helper) {
        helper.editAddress(component);
    },
    saveAddress: function(component, event, helper) {
        helper.saveAddress(component);
    },
    addToAddress: function(component, event, helper) {
        helper.addToAddress(component);
    },
    removeAddress: function(component, event, helper) {
        var addressIndex = Number(event.currentTarget.id);
        var listOfProjectAddress = component.get("v.listOfProjectAddress");
        
        if(addressIndex === 0) {
            return;
        }
        
        if (!confirm("Are you sure you want to delete?")) {
            return;
        }
        helper.removeAddress(component, addressIndex);
    },
    clearCustomerName: function(component, event, helper) {

        var externalParameter = event.getParam("externalParameter");
        var listOfProjectAddress = component.get("v.listOfProjectAddress");

        listOfProjectAddress[externalParameter]["GP_Customer_Name__c"] = null;
        listOfProjectAddress[externalParameter]["GP_Customer_Name__r"] = null;
        
        listOfProjectAddress[externalParameter]["listOfBillToAddress"] = [helper.DEFAULT_ADDRESS];
        listOfProjectAddress[externalParameter]["GP_Bill_To_Address__c"] = null;
        listOfProjectAddress[externalParameter]["GP_Bill_To_Address__r"] = null;
        
        listOfProjectAddress[externalParameter]["listOfShipToAddress"] = [helper.DEFAULT_ADDRESS];
        listOfProjectAddress[externalParameter]["GP_Ship_To_Address__c"] = null;
        listOfProjectAddress[externalParameter]["GP_Ship_To_Address__r"] = null;

        component.set("v.listOfProjectAddress", listOfProjectAddress);
    },
    getListOfAddress: function(component, event, helper) {  
        helper.updateListOfAddressOption(component);
    },
    customerMasterUpdateHandler: function(component, event, helper) {
        var addressIndex = event.getParam('externalParameter');
        var listOfProjectAddress = component.get("v.listOfProjectAddress");
        var billingEntityID = component.get("v.billingEntityID");
        var customerId = listOfProjectAddress[addressIndex]["GP_Customer_Name__c"];  //event.getParam('sObjectId');

        component.set("v.activeAddressIndex", addressIndex);


        helper.getListOfAddress(component, [customerId], billingEntityID);
    },
    billToAddressChangeHandler: function(component, event, helper) {
        // var addressIndex = 0; //event.currentTarget.get("v.label");

        // var listOfProjectAddress = component.get("v.listOfProjectAddress");

        // var billToAddressId = listOfProjectAddress[addressIndex]["GP_Bill_To_Address__c"];
        // var listOfBillToAddress = listOfProjectAddress[addressIndex]["listOfBillToAddress"];
        // var addressLabel = '';

        // listOfBillToAddress.forEach(function(addressOption) {
        //     if(addressOption.text === billToAddressId) {
        //         addressLabel = addressOption.label;
        //         return;
        //     }
        // });

        // listOfProjectAddress[addressIndex]["GP_Bill_To_Address__r"] = {
        //     "GP_Address__c": addressLabel
        // };
        // component.set("v.listOfProjectAddress", listOfProjectAddress);       
    },
    shipToAddressChangeHandler: function(component, event, helper) {
        // var addressIndex = 0; //event.currentTarget.get("v.label");

        // var listOfProjectAddress = component.get("v.listOfProjectAddress");

        // var shipToAddressId = listOfProjectAddress[addressIndex]["GP_Ship_To_Address__c"];
        // var listOfShipToAddress = listOfProjectAddress[addressIndex]["listOfShipToAddress"];
        // var addressLabel = '';

        // listOfShipToAddress.forEach(function(addressOption) {
        //     if(addressOption.text === shipToAddressId) {
        //         addressLabel = addressOption.label;
        //         return;
        //     }
        // });

        // listOfProjectAddress[addressIndex]["GP_Ship_To_Address__r"] = {
        //     "GP_Address__c": addressLabel
        // };
        // component.set("v.listOfProjectAddress", listOfProjectAddress);       
    }
})