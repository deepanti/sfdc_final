({
    PROJECT_ADDRESS_RELATIONSHIP_INDEX: [
        "DIRECT",
        "SECOND",
        "THIRD",
        "FOURTH",
        "FIFTH",
        "SIX",
        "SEVEN",
        "EITHT",
        "NINE",
        "TEN"
    ],
    DEFAULT_ADDRESS: {
        "label": "--NONE--"    
    },
    doInitHelper: function(component) {
        var projectId = component.get("v.recordId");
        var getProjectAddress = component.get("c.getProjectAddress");
        this.showSpinner(component);
        getProjectAddress.setParams({
            "projectId": component.get("v.recordId")
        });
        getProjectAddress.setCallback(this, function(response) {
            this.getProjectAddressHandler(component, response);
        });

        $A.enqueueAction(getProjectAddress);
    },
    getProjectAddressHandler: function(component, response) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var responseJSON = JSON.parse(responseData.response) || {};
            var listOfProjectAddress = responseJSON.listOfProjectAddress || [];

            if (listOfProjectAddress.length === 0) {
                listOfProjectAddress.push({
                    "GP_Relationship__c": this.PROJECT_ADDRESS_RELATIONSHIP_INDEX[0],
                    "GP_Sequence_Number__c": 0,
                    "GP_Project__c": component.get("v.recordId")
                });
            }

            component.set("v.listOfProjectAddress", listOfProjectAddress);
            this.updateListOfAddressOption(component, true);//Avinash - Avalara changes.//
			//console.log("===listOfProjectAddress.length==="+listOfProjectAddress.length);            
            //for(var i=0;i<listOfProjectAddress.length;i++){               
            if(listOfProjectAddress.length > 0){   
                var AddressLast = listOfProjectAddress.length-1;
               /* if(!($A.util.isEmpty(listOfProjectAddress[i].GP_Bill_To_Address__r)) &&  
                   !($A.util.isEmpty(listOfProjectAddress[i].GP_Bill_To_Address__r.GP_Country__c)) &&             
                   (listOfProjectAddress[i].GP_Bill_To_Address__r.GP_Country__c === "US" || listOfProjectAddress[i].GP_Bill_To_Address__r.GP_Country__c === "CA"))*/
                if((!($A.util.isEmpty(listOfProjectAddress[AddressLast].GP_Bill_To_Address__r)) &&  
                   !($A.util.isEmpty(listOfProjectAddress[AddressLast].GP_Bill_To_Address__r.GP_Country__c)) &&             
                   (listOfProjectAddress[AddressLast].GP_Bill_To_Address__r.GP_Country__c === "US" || listOfProjectAddress[AddressLast].GP_Bill_To_Address__r.GP_Country__c === "CA")	   
				   ) 
				   || 				   
				   (!($A.util.isEmpty(listOfProjectAddress[AddressLast].GP_Ship_To_Address__r)) &&  
                   !($A.util.isEmpty(listOfProjectAddress[AddressLast].GP_Ship_To_Address__r.GP_Country__c)) &&             
                   (listOfProjectAddress[AddressLast].GP_Ship_To_Address__r.GP_Country__c === "US" || listOfProjectAddress[AddressLast].GP_Ship_To_Address__r.GP_Country__c === "CA")	
				   )				   
				   )
                {
                    component.set("v.isUsCanadaAddr",true);                     
                    //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
                    this.SetServiceDelFlag(component);//Avalara SetServiceDelFlag event fire  
                    //break;           
                }
                else
                {
                    component.set("v.isUsCanadaAddr",false);  
                    //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
                    this.SetServiceDelFlag(component);//Avalara SetServiceDelFlag event fire  
                    //break;
                }
            }
           // }
                     
            
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    editAddress: function(component) {
        var listOfProjectAddress = component.get("v.listOfProjectAddress");

        for (var i = 0; listOfProjectAddress && i < listOfProjectAddress.length; i += 1) {
            listOfProjectAddress[i]["isEditable"] = true;
        }

        component.set("v.listOfProjectAddress", listOfProjectAddress);
    },
    saveAddress: function(component) {
        var listOfProjectAddress = component.get("v.listOfProjectAddress");
        var validationResponse = this.validateProjectAddress(component, listOfProjectAddress);

        if (validationResponse.isSuccess) {
            this.saveProjectAddress(component, validationResponse.data);
        }
    },
    saveProjectAddress: function(component, listOfProjectAddress) {
        this.showSpinner(component);
        listOfProjectAddress = this.getSanatizedListOfProjectAddress(listOfProjectAddress);
        var saveProjectAddress = component.get("c.saveProjectAddress");
        saveProjectAddress.setParams({
            "serializedListOfProjectAddress": JSON.stringify(listOfProjectAddress)
        });

        saveProjectAddress.setCallback(this, function(response) {
            this.saveProjectAddressHandler(component, response);
        });

        $A.enqueueAction(saveProjectAddress);
    },
    getSanatizedListOfProjectAddress: function(listOfProjectAddress) {
        for(var i = 0; i < listOfProjectAddress.length; i += 1) {
            listOfProjectAddress[i]["listOfBillToAddress"] = null;
            listOfProjectAddress[i]["listOfShipToAddress"] = null;
            listOfProjectAddress[i]["errorMessage"] = null;
            listOfProjectAddress[i]["isEditable"] = null;
        }

        return listOfProjectAddress;
    },
    saveProjectAddressHandler: function(component, response) {
        this.hideSpinner(component);
        this.fireProjectAddressSaveEvent();
        this.showToast("success", "success", "Project Address has been saved successfully");
        
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var listOfProjectAddress = component.get("v.listOfProjectAddress");
            var updatedListOfProjectAddress = JSON.parse(responseData.response) || {};

            for (var i = 0; i < listOfProjectAddress.length; i += 1) {
                listOfProjectAddress[i]["Id"] = updatedListOfProjectAddress[i]["Id"];
                listOfProjectAddress[i]["isEditable"] = false;
            }
            component.set("v.listOfProjectAddress", listOfProjectAddress);

            this.fireProjectSaveEvent();
			this.getProjectAddressAfterDML(component);//Avalara		
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    validateProjectAddress: function(component, listOfProjectAddress) {
        var isValid = true;
        var sanatizedListOfProjectAddress = JSON.parse(JSON.stringify(listOfProjectAddress));
        for (var i = 0; i < listOfProjectAddress.length; i += 1) {
            listOfProjectAddress[i]["errorMessage"] = '';

            if ($A.util.isEmpty(listOfProjectAddress[i]["GP_Relationship__c"])) {
                listOfProjectAddress[i]["errorMessage"] += 'Relationship is mandatory.';
                isValid = false;
            }

            if ($A.util.isEmpty(listOfProjectAddress[i]["GP_Customer_Name__c"])) {
                listOfProjectAddress[i]["errorMessage"] += ' Customer name is mandatory.';
                isValid = false;
            }

            if ($A.util.isEmpty(listOfProjectAddress[i]["GP_Bill_To_Address__c"]) ||
                listOfProjectAddress[i]["GP_Bill_To_Address__c"] === "--NONE--") {
                listOfProjectAddress[i]["errorMessage"] += ' Bill To Address is mandatory.';
                isValid = false;
            }

            if ($A.util.isEmpty(listOfProjectAddress[i]["GP_Ship_To_Address__c"]) ||
                listOfProjectAddress[i]["GP_Ship_To_Address__c"] === "--NONE--") {
                listOfProjectAddress[i]["errorMessage"] += ' Ship To Address is mandatory.';
                isValid = false;
            }

            sanatizedListOfProjectAddress[i]["listOfBillToAddress"] = [this.DEFAULT_ADDRESS];
            sanatizedListOfProjectAddress[i]["listOfShipToAddress"] = [this.DEFAULT_ADDRESS];
            sanatizedListOfProjectAddress[i]["GP_Bill_To_Address__r"] = null;
            sanatizedListOfProjectAddress[i]["GP_Ship_To_Address__r"] = null;
        }

        component.set("v.listOfProjectAddress", listOfProjectAddress);

        return {
            "isSuccess": isValid,
            "data": sanatizedListOfProjectAddress
        };
    },
    addToAddress: function(component) {
        var listOfProjectAddress = component.get("v.listOfProjectAddress") || [];
        if (listOfProjectAddress.length >= 10) {
            this.showToast('warning', 'warning', 'Address can not be more than 10');
            return;
        }
        listOfProjectAddress.push({
            "GP_Relationship__c": this.PROJECT_ADDRESS_RELATIONSHIP_INDEX[listOfProjectAddress.length],
            "GP_Sequence_Number__c": listOfProjectAddress.length,
            "GP_Project__c": component.get("v.recordId"),
            "isEditable": true
        });
        component.set("v.listOfProjectAddress", listOfProjectAddress);
    },
    removeAddress: function(component, addressIndex) {

        var listOfProjectAddress = component.get("v.listOfProjectAddress") || [];
        var removedProjectAddress = listOfProjectAddress.splice(addressIndex, 1);
        component.set("v.listOfProjectAddress", listOfProjectAddress);

        this.deletedProjectAddressFromServer(component, removedProjectAddress[0]["Id"]);
    },
    deletedProjectAddressFromServer: function(component, projectAddressId) {
        if(!projectAddressId) {
            return;
        }

        this.showSpinner(component);
        var deleteProjectAddress = component.get("c.deleteProjectAddress");
        deleteProjectAddress.setParams({
            "projectAddressId": projectAddressId
        });
        deleteProjectAddress.setCallback(this, function(response) {
            this.deleteProjectAddressHandler(component, response);
        });

        $A.enqueueAction(deleteProjectAddress);
    },
    deleteProjectAddressHandler: function(component, response) {
        this.hideSpinner(component);
        this.fireProjectSaveEvent();
		this.getProjectAddressAfterDML(component);//Avalara changes
        //this.showToast(response.getState());
    },
    getListOfAddress: function(component, lsitOfCustomerId, billingEntityId) {

        if (!lsitOfCustomerId ||
            lsitOfCustomerId.length === 0 ||
            !billingEntityId) {
            return;
        }

        var getFilteredListOfAddress = component.get("c.getFilteredListOfAddress");

        getFilteredListOfAddress.setParams({
            "serializedListOfCustomerId": JSON.stringify(lsitOfCustomerId),
            "billingEntityID": billingEntityId
        });

        this.showSpinner(component);

        getFilteredListOfAddress.setCallback(this, function(response) {
            this.getFilteredListOfAddressHandler(component, response);
        });

        $A.enqueueAction(getFilteredListOfAddress);
    },
    getFilteredListOfAddressHandler: function(component, response) {
        this.hideSpinner(component);
        var responseData = response.getReturnValue() || {};
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var mapOfCustomerIdToAddress = JSON.parse(responseData.response) || {};
            var listOfProjectAddress = component.get("v.listOfProjectAddress");
            var isInitalized = component.get("v.isInitialized");
            var activeAddressIndex = component.get("v.activeAddressIndex");

            if (!$A.util.isEmpty(activeAddressIndex) && activeAddressIndex >= 0) {
                var customerId = listOfProjectAddress[activeAddressIndex]["GP_Customer_Name__c"];
                var mapOfAddress = mapOfCustomerIdToAddress[customerId] || {};
                var listOfBillToAddress = mapOfAddress["listOfBillToAddress"] || [];
                var listOfShipToAddress = mapOfAddress["listOfShipToAddress"] || [];
                
                listOfBillToAddress.unshift(this.DEFAULT_ADDRESS);
                listOfShipToAddress.unshift(this.DEFAULT_ADDRESS);

                listOfProjectAddress[activeAddressIndex]["listOfBillToAddress"] = listOfBillToAddress
                listOfProjectAddress[activeAddressIndex]["listOfShipToAddress"] = listOfShipToAddress;


                if (listOfBillToAddress && listOfBillToAddress.length > 0) {
                    listOfProjectAddress[activeAddressIndex]["GP_Bill_To_Address__c"] = listOfBillToAddress[0]["text"];
                    listOfProjectAddress[activeAddressIndex]["GP_Bill_To_Address__r"] = {
                        "Name": listOfBillToAddress[0]["label"]
                    };
                } else {
                    listOfProjectAddress[activeAddressIndex]["GP_Bill_To_Address__c"] = null;
                }

                if (listOfShipToAddress && listOfShipToAddress.length > 0) {
                    listOfProjectAddress[activeAddressIndex]["GP_Ship_To_Address__c"] = listOfShipToAddress[0]["text"];
                    listOfProjectAddress[activeAddressIndex]["GP_Ship_To_Address__r"] = {
                        "Name": listOfShipToAddress[0]["label"]
                    };
                } else {
                    listOfProjectAddress[activeAddressIndex]["GP_Ship_To_Address__c"] = null;
                }
            } else {
                for (var i = 0; i < listOfProjectAddress.length; i += 1) {
                    var customerId = listOfProjectAddress[i]["GP_Customer_Name__c"];
                    //Object.keys(mapOfCustomerIdToAddress)[0]; //mapOfCustomerIdToAddress[listOfProjectAddress[i]["GP_Customer_Name__c"]];

                    var mapOfAddress = mapOfCustomerIdToAddress[customerId] || {};
                    var listOfBillToAddress = mapOfAddress["listOfBillToAddress"] || [];
                    var listOfShipToAddress = mapOfAddress["listOfShipToAddress"] || [];

                    listOfBillToAddress.unshift(this.DEFAULT_ADDRESS);
                    listOfShipToAddress.unshift(this.DEFAULT_ADDRESS);

                    listOfProjectAddress[i]["listOfBillToAddress"] = listOfBillToAddress
                    listOfProjectAddress[i]["listOfShipToAddress"] = listOfShipToAddress;

                    if (!isInitalized ||
                        !listOfProjectAddress[i]["GP_Bill_To_Address__c"]) {
                        if (listOfBillToAddress && listOfBillToAddress.length > 0) {
                            listOfProjectAddress[i]["GP_Bill_To_Address__c"] = listOfBillToAddress[0]["text"];
                            listOfProjectAddress[i]["GP_Bill_To_Address__r"] = {
                                "Name": listOfBillToAddress[0]["label"]
                            };
                        } else {
                            listOfProjectAddress[i]["GP_Bill_To_Address__c"] = null;
                        }

                        if (listOfShipToAddress && listOfShipToAddress.length > 0) {
                            listOfProjectAddress[i]["GP_Ship_To_Address__c"] = listOfShipToAddress[0]["text"];
                            listOfProjectAddress[i]["GP_Ship_To_Address__r"] = {
                                "Name": listOfShipToAddress[0]["label"]
                            };
                        } else {
                            listOfProjectAddress[i]["GP_Ship_To_Address__c"] = null;
                        }
                    } else {
                        component.set("v.isInitialized", false);
                    }
                }
            }

            component.set("v.listOfProjectAddress", listOfProjectAddress);
            component.set("v.activeAddressIndex", null);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    updateListOfAddressOption: function(component) {
        var listOfProjectAddress = component.get("v.listOfProjectAddress");
        var billingEntityID = component.get("v.billingEntityID");
        var listOfCustomerId = this.getListOfCustomerId(listOfProjectAddress);

        this.getListOfAddress(component, listOfCustomerId, billingEntityID);
    },
    getListOfCustomerId: function(listOfProjectAddress) {
        var listOfCustomerId = [];

        for (var i = 0; i < listOfProjectAddress.length; i += 1) {
            if (listOfProjectAddress[i]["GP_Customer_Name__c"]) {
                listOfCustomerId.push(listOfProjectAddress[i]["GP_Customer_Name__c"]);
            }
        }

        return listOfCustomerId;
    },
    fireProjectAddressSaveEvent: function(component, event, helper) {
        $A.get("e.c:GPEvntProjectAddressChange").fire();
    },
	 //Avalara - changes
    SetServiceDelFlag: function(component)
    {
        var isUSCanada = component.get("v.isUsCanadaAddr");
    	 var serCmpEvent=component.getEvent("serviceDeliveryFlagEvent");
            serCmpEvent.setParams({
                "ServiceDeliveryFlag":isUSCanada			            
            });
        serCmpEvent.fire();
	},
    //Avalara - changes
    getProjectAddressAfterDML:function(component)
    {
         var getProjectAddressAfterEdit = component.get("c.getProjectAddress");
            this.showSpinner(component);
            getProjectAddressAfterEdit.setParams({
                "projectId": component.get("v.recordId")
            });
            
            getProjectAddressAfterEdit.setCallback(this, function(response) {
                
                var responseData = response.getReturnValue() || {};
                this.hideSpinner(component);
                if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                    var responseJSON = JSON.parse(responseData.response) || {};
                    var listOfProjectAddressEdit = responseJSON.listOfProjectAddress || [];
                    
                    if (listOfProjectAddressEdit.length === 0) {
                        listOfProjectAddressEdit.push({
                            "GP_Relationship__c": this.PROJECT_ADDRESS_RELATIONSHIP_INDEX[0],
                            "GP_Sequence_Number__c": 0,
                            "GP_Project__c": component.get("v.recordId")
                        });
                    }
                    //for(var i=0;i<listOfProjectAddressEdit.length;i++){   
                    if(listOfProjectAddressEdit.length > 0){  
                        var addrLast=listOfProjectAddressEdit.length-1;
                        if((!($A.util.isEmpty(listOfProjectAddressEdit[addrLast].GP_Bill_To_Address__r)) &&  
                            !($A.util.isEmpty(listOfProjectAddressEdit[addrLast].GP_Bill_To_Address__r.GP_Country__c)) &&             
                            (listOfProjectAddressEdit[addrLast].GP_Bill_To_Address__r.GP_Country__c === "US" || listOfProjectAddressEdit[addrLast].GP_Bill_To_Address__r.GP_Country__c === "CA")	   
                           ) 
                           || 				   
                           (!($A.util.isEmpty(listOfProjectAddressEdit[addrLast].GP_Ship_To_Address__r)) &&  
                            !($A.util.isEmpty(listOfProjectAddressEdit[addrLast].GP_Ship_To_Address__r.GP_Country__c)) &&             
                            (listOfProjectAddressEdit[addrLast].GP_Ship_To_Address__r.GP_Country__c === "US" || listOfProjectAddressEdit[addrLast].GP_Ship_To_Address__r.GP_Country__c === "CA")	
                           )				   
                          )
                        {
                            component.set("v.isUsCanadaAddr",true);                     
                            //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
                            this.SetServiceDelFlag(component);//Avalara SetServiceDelFlag event fire 
                            
                            //break;           
                        }
                        else
                        {
                            component.set("v.isUsCanadaAddr",false);  
                            //component.set("v.projectRecord.GP_Service_Deliver_in_US_Canada__c",true);
                            this.SetServiceDelFlag(component);//Avalara SetServiceDelFlag event fire  
                            //break;
                        }
                    } 
                    
                }
            });            
        	$A.enqueueAction(getProjectAddressAfterEdit);    
			
          
    }
   
})