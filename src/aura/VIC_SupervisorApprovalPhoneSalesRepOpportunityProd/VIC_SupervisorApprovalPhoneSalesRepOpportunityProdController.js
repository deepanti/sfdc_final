({
	toggleSelection : function(component, event, helper){
        var oliDataWrap = component.get('v.oliDataWrap');
        oliDataWrap.isOLIChecked = !oliDataWrap.isOLIChecked;
        component.set('v.oliDataWrap', oliDataWrap);
        var oppDataWrap = component.get('v.objOppDataWrapLine');
        var countOLIChecked = 0;
        for(var eachOLIWrap in oppDataWrap.lstOLIDataWrap){
            if(isNaN(oppDataWrap.lstOLIDataWrap[eachOLIWrap])){
                if(oppDataWrap.lstOLIDataWrap[eachOLIWrap].isOLIChecked){
                    countOLIChecked++;
                }
            }
        }
        if(countOLIChecked == 0){
            oppDataWrap.isOppChecked = false;
            component.set('v.objOppDataWrapLine',oppDataWrap);
        }
        var compEvent = component.getEvent("OLIToOpptEvent");
        compEvent.fire();	//Call with registration
	}
})