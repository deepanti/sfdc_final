({
	handleClick : function(component, event, helper) {
		var isEditable = component.get('v.isEditable');
        if(!isEditable){
            var value = component.get('v.value');
        	component.set('v.prevValue', value);
        }
        component.set('v.isEditable', !isEditable);
	},
    cancel : function(component, event, helper) {
        var prevValue = component.get('v.prevValue');
        component.set('v.value',prevValue);
        component.set('v.isEditable', false);
    }
})