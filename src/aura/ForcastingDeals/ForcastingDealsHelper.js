({  
    setOppotuniryInWrapperList : function(component, result, page)
    {
      
        var index;
        var selectedIndex;
        var product;
        var opportunityProductList = component.get("v.opportunityList");
        for(index in result){
                    debugger;
                    var opp = OLIList[index];
                    var Foreopportunity = {
                        "Id" : opp.Id,
                        "OPPId" : opp.Opportunity_ID__c,
                        "OppName"  :  opp.Name,
                        "OppOwner" : opp.Owner.Name,
                        "CloseDate" : opp.CloseDate,
                        "StageName" : opp.StageName,
                        "TCV" : opp.TCV1__c,
                        "ForeCastingCategoryName" : opp.ForecastCategoryName,
						"Actualstage" : opp.StageName,
						"ForeCastingTCV" : opp.Forecasted_TCV__c,
                        "GroundLevelForecast" : opp.Ground_Level_Forecast__c,
                    }
                    opportunityProductList.push(Foreopportunity);
                }
                component.set("v.opportunityList", opportunityProductList);  
                component.set("v.page",page);
                component.set("v.total",OLIList.length);
                component.set("v.pages",Math.ceil(OLIList.length/component.get("v.pageSize")));
               component.set('v.selectAllFlag', false);
        
        component.set("v.page",page);
        component.set("v.total",result.total);
        component.set("v.pages",Math.ceil(result.total/component.get("v.pageSize")));
        component.set('v.selectAllFlag', false);
        component.set("v.totalProductList", totalProductList);
        
        this.getopportunityFromWrapperList(component, page);  
       /* if(totalProductList.length > 0)
        {
            component.set("v.isproductWrapperListEmpty", true);
        }
        else
        {
            component.set("v.isproductWrapperListEmpty", false);
        }
        component.set("v.isLoading", false);*/
    },
    
	getopportunityFromWrapperList : function(component, page)
    {
        //debugger;
        var productWrapperList = [];
        var req_list = [];
        var pageSize = component.get("v.pageSize");
        var totalList = component.get("v.opportunityList");
       if(page === 1){
            req_list = totalList.slice(0, pageSize); 
        }
        else{
            var offset = (+page -1) * (+pageSize);
            req_list = totalList.slice(offset , (+pageSize * +page));    
        }
        component.set("v.page",page);
        component.set("v.opportunityWrapperList", req_list);
        component.set("v.pages",Math.ceil(totalList.length/component.get("v.pageSize")));
         component.set("v.isFilterLoading", false);
       // console.log(JSON.stringify(req_list)); getOpportunitycloseMonth forecastgoryStage
     },
    getverticalpickList : function(component) {
        var action = component.get("c.getVerticalPicklist");
        action.setCallback(this, function(response){            
            component.set("v.verticalpicklist", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    getForecastCategorycustomsetting : function (component){
        var action = component.get("c.getForecastCategoryCustomSetting");
        action.setCallback(this, function(response){            
            component.set("v.forcastcustumsettingvalue", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    getforcastStage : function(component) {
        var action = component.get("c.getForcastStage");
        action.setCallback(this, function(response){            
            component.set("v.forecastgoryStage", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    getOpportunitycloseMonth : function(component) { 
      
        var action = component.get("c.getOpportunityCloseMonth");
        action.setCallback(this, function(response){   
           // console.log(':--close month --:'+response.getReturnValue());
            component.set("v.OppCloseMonth", response.getReturnValue());
        });
        $A.enqueueAction(action);
        
    },
    getcloseDateMonth : function(component) {
        
    },
    getopportunityforecastValue : function(component,updateforcastvalue,groundlevelvalue,updateforcastOppId) {
        
    },
    getforcastpicklist : function(component) {
        var action = component.get("c.getForcastPicklist");
        action.setCallback(this, function(response){            
            component.set("v.forcastpicklist", response.getReturnValue());
        });
        $A.enqueueAction(action);   
    },
    sortBy: function(component, field) {
        var page = page || 1;
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.opportunityList");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.opportunityList", records);
      //  console.log(':--records--:'+JSON.stringify(records));
       
        this.renderPage(component);
        this.getopportunityFromWrapperList(component, page);
    },
    sortByDate : function(component, field) {
        var page = page || 1;
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.opportunityList");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var da = new Date(a[field]).getTime() == new Date(b[field]).getTime(),
			db = (new Date(!a[field]).getTime() && new Date(b[field]).getTime()) || new Date(a[field]).getTime() < new Date(b[field]).getTime();
           return da? 0: (sortAsc?-1:1)*(db?1:-1);
            
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.opportunityList", records);
      //  console.log(':--records--:'+JSON.stringify(records));
        this.renderPage(component);
        this.getopportunityFromWrapperList(component, page);
       
    },
    renderPage: function(component) {
		var records = component.get("v.opportunityList"),
            pageNumber = component.get("v.pageSize");
         //   pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
       // component.set("v.opportunityWrapperList", pageRecords);
      //  console.log(':--pageRecords--:'+pageRecords);
	}
})