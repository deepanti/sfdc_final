({
	doInit : function(component, event, helper) 
    { 
         component.set("v.isFilterLoading", true);
        component.set("v.ForeCastingCategorytemp", []);
        component.set("v.Selectedvalue",[]);
        var page = page || 1;
        var totalTcv = 0;
        var totalForcastingTcv = 0;
        var action = component.get("c.getOpportunities");
        var opportunityProductList =  [];
        const monthNames = ["January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"
                           ];
        action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var index;
                var date1;
                var groundLevelProb;
                var OLIList = response.getReturnValue();
                
                 for(index in OLIList){
                    var opp = OLIList[index]; 
                     totalTcv += opp.TCV1__c;//totalTcv totalForcastingTcv
                     if(typeof opp.Forecasted_TCV__c === 'number'){
                         totalForcastingTcv += opp.Forecasted_TCV__c;
                     }
                     
                     //console.log(typeof totalForcastingTcv , typeof opp.Forecasted_TCV__c);
                     
                    var d = new Date(opp.CloseDate);
                    var oppcloseMonth = opp.Opportunity_Close_Date_Month__c;
                     if(oppcloseMonth != null && oppcloseMonth != ''){
                     date1 = oppcloseMonth;
                     }else {
                     date1 = monthNames[d.getMonth()];    
                     }
                     var grprab = opp.Ground_Level_Probabilty_forcast_page__c;
                     if(grprab != null && grprab != ''){
                       groundLevelProb = grprab;
                     } else {
                         groundLevelProb = opp.Ground_Level_Forecast__c;
                     }
                     var forecastStage = opp.Forecats_Stage__c;
                     var forecastStageValue;
                     if(forecastStage != null && forecastStage != ''){
                         forecastStageValue = forecastStage;
                     }else{
                         forecastStageValue = opp.StageName;
                     }
                     var curr_date = d.getDate();
                     var curr_month = d.getMonth()+1; 
                     var curr_year = d.getFullYear();
                     
                     var requiredDate = curr_month + "/"+ curr_date + "/" +curr_year; 
                     var Foreopportunity = {
                        "Id" : opp.Id,
                        "OPPId" : opp.Opportunity_ID__c,
                        "OppName"  :  opp.Name,
                        "OppOwner" : opp.Owner.Name,
                        "CloseDate" : requiredDate,
                        "TCV" : opp.TCV1__c,
                        "ForeCastingCategoryName" : opp.ForecastCategoryName,
						"Actualstage" : opp.StageName,
						"ForeCastingTCV" : opp.Forecasted_TCV__c, 
                        "GroundLevelForecast" : groundLevelProb,
                        "oppCloseDateMonth" : date1,
                        "foreCastStage" : forecastStageValue,
                    }
                  //   console.log(Foreopportunity.length);
                     if(Foreopportunity.length > 0 ){
                        console.log('test'); 
                     }
                     
                    opportunityProductList.push(Foreopportunity);
                    
                 }
              //  console.log(JSON.stringify(opportunityProductList));
               // console.log(':--tcv---:'+totalTcv+':---forcating--:'+totalForcastingTcv);
                component.set("v.opportunityList", opportunityProductList);  
                component.set("v.page",page);
                component.set("v.totalTcv",parseFloat(totalTcv).toFixed(2));
                component.set("v.forecastingTcv",parseFloat(totalForcastingTcv).toFixed(2));
                component.set("v.total",OLIList.length);
                component.set("v.pages",Math.ceil(OLIList.length/component.get("v.pageSize")));
                component.set('v.selectAllFlag', false);
               helper.getopportunityFromWrapperList(component, page);
               helper.getverticalpickList(component);
               helper.getforcastpicklist(component); 
               helper.getOpportunitycloseMonth(component);  //getforcastStage totalTcv,forecastingTcv
               helper.getcloseDateMonth(component);
               helper.getforcastStage(component);
               helper.getForecastCategorycustomsetting(component); 
            } else {
                console.log('call back fail');
                component.set("v.isFilterLoading", false);
            }
            component.set("v.isFilterLoading", false);
        });
        $A.enqueueAction(action);
         
	},
    
    updatedeals:function(component, event, helper) {
        try {
        var opportunityList = component.get("v.opportunityList");
       // console.log(JSON.stringify(opportunityList));
        var UpdateoppList = component.get("v.SelectopportunityList");
        var changeforecast = component.get("v.changeGroundForecast");
      //  console.log(JSON.stringify(UpdateoppList));
      //  console.log(':--ground--:'+changeforecast.length);
        if(UpdateoppList.length > 0  || changeforecast.length > 0){
            component.set("v.isFilterLoading", true);
            var index;
            var updateopptList =  [];
            for(index in UpdateoppList){
                var opp = UpdateoppList[index];
                var Foreopportunity = {
                    "Id" : opp.Id,
                    "ForeCastingCategoryName" : opp.ForeCastingCategoryName,
                    "closedateMonth": opp.oppCloseDateMonth,
                    "groundLevelProb" : opp.GroundLevelForecast,
                    "forecastStage"   :opp.foreCastStage
                } 
                updateopptList.push(Foreopportunity); 
            }
       //     console.log(JSON.stringify(updateopptList));
             var action  = component.get("c.updateOpportunity");
            action.setParams({
                "updateOppList" : JSON.stringify(updateopptList)
            });
            action.setCallback(this, function(response){            
                if(response.getState() == 'SUCCESS'){
                    component.set("v.successs", true);
                    component.set("v.SuccessMessage", "Deal Updated Successfully");
                    //this.doInit(component, event, helper);  
                    window.setTimeout(
                        $A.getCallback(function() {
                            if(component.isValid()){
                                component.set("v.successs", false); 
                                component.set("v.isFilterLoading", false);
                                //  location.reload();
                                var vertical = $('.select2-selection__choice').text();
                                var quater = component.get("v.Quater");
                                var supersl = component.get("v.SuperSL");
                                var sl = component.get("v.SL");
                                var dealValue = component.get("v.dealValue");
                                
                                component.set("v.SelectopportunityList",[]);
                                //   console.log(':--ver--'+vertical+':---qua--:'+quater+':---sup--'+supersl+':--sl--'+sl);
                                if((vertical != 'None' && vertical != '') || (quater != 'None' && quater != '') || 
                                   supersl != undefined ||  sl != undefined || (dealValue != 'None' && dealValue != '')){
                                    var a = component.get('c.filterOpportunity');
                                    $A.enqueueAction(a);
                                }else{
                                    var a = component.get('c.doInit');
                                    $A.enqueueAction(a);
                                }
                              
                            }    
                        }),2000
                    ); 
                   
                }
              
            });
            $A.enqueueAction(action); 
           // component.set("v.isFilterLoading", false);
        } else{
            component.set("v.errorMessage", []);
            component.set("v.isError", true);
            component.set("v.errorMessage", "Please update a deal");
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            ); 
        } 
        }
        catch(err) {
        //    console.log(':--try catch error--:'+err.message);
        }
        
    },
    
    filterOpportunity : function(component, event, helper) {
        var vertical = $('.select2-selection__choice').text();
     //   var vertic = component.get("v.vertical");
        var quater = component.get("v.Quater");
        var supersl = component.get("v.SuperSL");
        var sl = component.get("v.SL");
        var dealValue = component.get("v.dealValue");
        var closeDateYear = component.get("v.closeDateYear");
        
       
      //  console.log(':--ver--'+vertical+':---qua--:'+closeDateYear+':---sup--'+supersl+':--sl--'+sl);
        if((vertical != 'None' && vertical != '') || (quater != 'None' && quater != '') || (closeDateYear != 'None' && closeDateYear != '') || 
           supersl != undefined ||  sl != undefined || (dealValue != 'None' && dealValue != '')){
            component.set("v.isFilterLoading", true);
           // var vertical = $('.select2-selection__choice').text();
            const monthNames = ["January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"
                           ];
            var page = page || 1;
            component.set("v.opportunityWrapperList",[]);
            component.set("v.opportunityList", []); 
            var opportunityProductList =  [];
            var action = component.get("c.getFilterOpportunity");
            action.setParams({  
                "dealValue" : dealValue,
                "vertical" : vertical,
                "quater" : quater,
                "closeDateYear" : closeDateYear,
                "supersl" : supersl,
                "sl" : sl  
            });
            action.setCallback(this, function(response){
            if(response.getState() == 'SUCCESS'){
                var index;
                var date1;
                var groundLevelProb;
                var totalTcv = 0;
                var totalForcastingTcv = 0;
                component.set("v.totalTcv",'');
                component.set("v.forecastingTcv", '');
               
                var opplist = response.getReturnValue();
           //  console.log(opplist.length);
                if(opplist.length == 0 ){
                 //   console.log('test'); 
                    component.set("v.errorMessage", "No Opportunity found");
                    component.set("v.isError", true);
                    window.setTimeout(
                        $A.getCallback(function() {
                            if(component.isValid()){
                                component.set("v.isError", false);    
                            }    
                        }),2000
                    ); 
                    }
             // console.log(JSON.stringify(opplist));
                for(index in opplist){
                    var opp = opplist[index];
                    var d = new Date(opp.CloseDate);
                    var oppcloseMonth = opp.Opportunity_Close_Date_Month__c;
                    totalTcv +=opp.TCV1__c;
                   if(typeof opp.Forecasted_TCV__c === 'number'){
                         totalForcastingTcv += opp.Forecasted_TCV__c;
                     } if(oppcloseMonth != null && oppcloseMonth != ''){
                     date1 = oppcloseMonth;
                     }else {
                     date1 = monthNames[d.getMonth()];    
                     }
                     var grprab = opp.Ground_Level_Probabilty_forcast_page__c;
                     if(grprab != null && grprab != ''){
                       groundLevelProb = grprab;
                     } else {
                         groundLevelProb = opp.Ground_Level_Forecast__c;
                     }
                    var forecaststageValue;
                    var forecaststage = opp.Forecats_Stage__c
                    if(forecaststage != null && forecaststage != ''){
                        forecaststageValue = forecaststage;
                    }else {
                        forecaststageValue = opp.StageName;
                    }
                    
                     var curr_date = d.getDate();
                     var curr_month = d.getMonth()+1; 
                     var curr_year = d.getFullYear();
                     
                     var requiredDate = curr_month + "/"+ curr_date + "/" +curr_year; 
                    
                    var Foreopportunity = {
                        "Id" : opp.Id,
                        "OPPId" : opp.Opportunity_ID__c,
                        "OppName"  :  opp.Name,
                        "OppOwner" : opp.Owner.Name,
                        "CloseDate" : requiredDate,
                        "TCV" : opp.TCV1__c,
                        "ForeCastingCategoryName" : opp.ForecastCategoryName,
						"Actualstage" : opp.StageName,
						"ForeCastingTCV" : opp.Forecasted_TCV__c,
                        "GroundLevelForecast" : groundLevelProb,
                        "oppCloseDateMonth" : date1,
                        "foreCastStage" : forecaststageValue,
                    }
                  opportunityProductList.push(Foreopportunity);
                }
             //   console.log(':--tcv---:'+totalTcv+':---forcating--:'+totalForcastingTcv);
                component.set("v.opportunityList", opportunityProductList);  
                component.set("v.page",page);
               component.set("v.totalTcv",parseFloat(totalTcv).toFixed(2));
                component.set("v.forecastingTcv",parseFloat(totalForcastingTcv).toFixed(2));

                component.set("v.total",opplist.length);
                component.set("v.pages",Math.ceil(opplist.length/component.get("v.pageSize")));
                component.set('v.selectAllFlag', false);
               // component.set("v.Quater",'None');
              //  component.set("v.vertical",'None');
               helper.getopportunityFromWrapperList(component, page);
               helper.getverticalpickList(component); 
               helper.getforcastpicklist(component); 
               helper.getforcastStage(component);
               component.set("v.isFilterLoading", false);
              // this.scriptsLoaded(component, event, helper);
            } else{
                //component.set("v.errorMessage", "There are some problem");
                //component.set("v.isError", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),2000
                ); 
                
            } 
            });
        $A.enqueueAction(action);
        } else{
            component.set("v.errorMessage", "Please select a filter criteria");
            component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            ); 
        } 
    },
    pageChange : function(component, event, helper) 
	{
        var page = component.get("v.page") || 1;
        var pages = component.get("v.pages");
       	var direction = event.getParam("direction");
        
        if(direction === "previous"){
        	page =  page - 1;
            helper.getopportunityFromWrapperList(component,page);
        }
        else if(direction === "next"){
        	page = page + 1;  
             helper.getopportunityFromWrapperList(component,page);
        }
        else if(direction === "first"){
        	page = 1;    
             helper.getopportunityFromWrapperList(component,page);
        }
        else if(direction === "last"){
            page = pages; 
             helper.getopportunityFromWrapperList(component,page);
        }       
       component.set("v.selectAllFlag", false);
    },
        closeError : function(component, event, helper) {
            component.set("v.isError", false);
            component.set("v.successs", false);
           
        },
    ChangeforcastCategory : function(component, event, helper) {
       // debugger;
        var changevalue = component.get("v.ForeCastingCategory");
        var opportunityList = component.get("v.opportunityWrapperList");
        var changelist = component.get("v.SelectopportunityList");
        var forecastvalue = component.get("v.forcastcustumsettingvalue");
       //updateforcastvalue,groundlevelvalue,updateforcastOppId
        var index;
        var updatevalue = changevalue.split(",");
        var updateforcastvalue = updatevalue[0];
        var groundlevelvalue;
        for(index in forecastvalue){
            var forcastvalue = forecastvalue[index];
            if(forcastvalue.Name == updateforcastvalue){
                groundlevelvalue = forcastvalue.forecast_value__c;
            }
        } 
      //  console.log(':--forecastvalue--:'+groundlevelvalue);
      /*  if(updateforcastvalue == 'Solid'){
          groundlevelvalue = 85; 
        }else if(updateforcastvalue == 'At Risk'){
           groundlevelvalue = 25;
        }else{
            groundlevelvalue = 0;
        } */
        var updateforcastOppId = updatevalue[1];
        var updatewrapperList = [];
      //  console.log(changelist.length);
      //  console.log(':--wrapper---:'+JSON.stringify(opportunityList));
        if(changelist.length > 0 ){
            var listIndex = changelist.findIndex(x => x.Id==updateforcastOppId);
            if(listIndex != -1){
                //changelist.splice(listIndex,1);
                for(index in changelist){
                    var productWrapper = changelist[index];
                    if(changelist.Id == updateforcastOppId){
                        productWrapper.ForeCastingCategoryName = updateforcastvalue;
                        productWrapper.GroundLevelForecast = groundlevelvalue;
                        changelist.push(productWrapper);
                    }
                }  
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updateforcastOppId){
                        productWrapper.ForeCastingCategoryName = updateforcastvalue;
                        productWrapper.GroundLevelForecast = groundlevelvalue;
                    }
                    updatewrapperList.push(productWrapper);
                }
            }else{
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updateforcastOppId){
                        productWrapper.ForeCastingCategoryName = updateforcastvalue;
                        productWrapper.GroundLevelForecast = groundlevelvalue;
                        changelist.push(productWrapper);
                    }
                    updatewrapperList.push(productWrapper);
                }
            }
            
        } else{
           // this.getopportunityforecastValue(component,updateforcastvalue);
           helper.getopportunityforecastValue(component,updateforcastvalue,groundlevelvalue,updateforcastOppId);
            for(index in opportunityList){
                var productWrapper1 = opportunityList[index];
                if(productWrapper1.Id == updateforcastOppId){
                    productWrapper1.ForeCastingCategoryName = updateforcastvalue;
                    productWrapper1.GroundLevelForecast = groundlevelvalue;
                    changelist.push(productWrapper1);
                }
                updatewrapperList.push(productWrapper1);
            }
           }
        
        
        component.set("v.SelectopportunityList", changelist);
        component.set("v.opportunityWrapperList", updatewrapperList);
        
       // console.log(changelist.length);
       // console.log(':--change--:'+JSON.stringify(updatewrapperList));
     //   console.log(':--change--:'+JSON.stringify(changelist));
     },
    getopportunityforecastValue : function (component,updateforcastvalue){
       // console.log(updateforcastvalue+ groundlevelvalue+updateforcastOppId);
    },
    SelectVertical : function(component, event, helper) {
        var verlenght = component.get("v.Selectedvalue");
     //   console.log(verlenght.length);
      var vertical = component.get("v.vertical");
        var VerticalValue ;
       var tempver = component.get("v.ForeCastingCategorytemp");
       
        if(tempver != undefined && tempver != ''){
            VerticalValue = tempver + ','+ vertical;
        } else{
            VerticalValue = vertical;
        }
        var changelist = component.get("v.verticalpicklist");
       
        var listIndex = changelist.findIndex(x => x==vertical);
        if(listIndex != -1){
            changelist.splice(listIndex,1); //icon
        }
        
        component.set("v.verticalpicklist",changelist);
        component.set("v.ForeCastingCategorytemp", VerticalValue);
        var tempfore = component.get("v.ForeCastingCategorytemp");
        if(tempfore != ''){
            component.set("v.icon",true);
        }
        component.set("v.Selectedvalue",VerticalValue);
    },
    RemoveSelected : function(component, event, helper) {
        component.set("v.ForeCastingCategorytemp",[]);
        component.set("v.Selectedvalue",[]);
        component.set("v.verticalpicklist",[]);
        component.set("v.icon",false);
        helper.getverticalpickList(component); 
    },
    ChangeGroundForecastvalue : function(component, event, helper) {
        var target = event.getSource();  
        var ChangeGroundForecastvalue = target.get("v.value");
        var changeGroundForecastId = event.getSource().get("v.name");
        var opportunityList = component.get("v.opportunityWrapperList");
        var index;
         var updatewrapperList = [];
       // console.log('Selected Value is auraid'+changeGroundForecastId);
        var changelist = component.get("v.SelectopportunityList");
        if(changelist.length > 0 ){
            var listIndex = changelist.findIndex(x => x.Id==changeGroundForecastId);
            if(listIndex != -1){
                //   changelist.splice(listIndex,1);
                for(index in changelist){
                    var change = changelist[index];
                    if(changelist.Id == changeGroundForecastId){
                        productWrapper.GroundLevelForecast = ChangeGroundForecastvalue;
                        changelist.push(productWrapper);
                    }
                }
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == changeGroundForecastId){
                        productWrapper.GroundLevelForecast = ChangeGroundForecastvalue;
                    }
                    updatewrapperList.push(productWrapper);
                }
             //   console.log(':--change list-if -:'+JSON.stringify(changelist));
            }else{
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == changeGroundForecastId){
                        productWrapper.GroundLevelForecast = ChangeGroundForecastvalue;
                        changelist.push(productWrapper);
                    }
                    updatewrapperList.push(productWrapper);
                }
            //    console.log(':--change list- if-else -:'+JSON.stringify(changelist));
            }
            
        } else{
            for(index in opportunityList){
                var productWrapper = opportunityList[index];
                if(productWrapper.Id == changeGroundForecastId){
                    productWrapper.GroundLevelForecast = ChangeGroundForecastvalue;
                    changelist.push(productWrapper);
                }
                updatewrapperList.push(productWrapper);
            }
         //   console.log(':--change list-else -:'+JSON.stringify(changelist));
        }
        //console.log(':--change list--:'+JSON.stringify(changelist));
        component.set("v.SelectopportunityList", changelist);
        component.set("v.opportunityWrapperList", updatewrapperList);
    }, //ChangeforcastStage
    ChangeforcastStage : function(component, event, helper) {
       var changestagevalue = component.get("v.forecastgoryStageValue");
      //  console.log(':--change month value--:'+changemonthvalue);
        var opportunityList = component.get("v.opportunityWrapperList");
        var changelist = component.get("v.SelectopportunityList");
        var index;
        var updatevalue = changestagevalue.split(",");
        var updatestagevalue = updatevalue[0];
        var updatestageOppId = updatevalue[1];
        var updatewrapperList = [];
       if(changelist.length > 0 ){
            var listIndex = changelist.findIndex(x => x.Id==updatestageOppId);
            if(listIndex != -1){
                // changelist.splice(listIndex,1);
                for(index in changelist){
                    var change = changelist[index];
                    if(changelist.Id == updatestageOppId){
                        productWrapper.foreCastStage = updatestagevalue;
                        changelist.push(productWrapper);
                    }
                }
                
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updatestageOppId){
                        productWrapper.foreCastStage = updatestagevalue;
                    }
                    updatewrapperList.push(productWrapper);
                }
            }else{
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updatestageOppId){
                        productWrapper.foreCastStage = updatestagevalue;
                        changelist.push(productWrapper);
                     }
                    updatewrapperList.push(productWrapper);
                }

            }
            
        } else {
            for(index in opportunityList){
                var productWrapper = opportunityList[index];
                if(productWrapper.Id == updatestageOppId){
                    productWrapper.foreCastStage = updatestagevalue;
                    changelist.push(productWrapper);
                }
                updatewrapperList.push(productWrapper);
            }
        }
        component.set("v.SelectopportunityList", changelist);
        component.set("v.opportunityWrapperList", updatewrapperList);
        //console.log(changelist.length);
      //  console.log(JSON.stringify(changelist));
    },
    ChangeCloseDateMonth : function(component, event, helper) {
       var changemonthvalue = component.get("v.oppcloseDateMonth");
      //  console.log(':--change month value--:'+changemonthvalue);
        var opportunityList = component.get("v.opportunityWrapperList");
        var changelist = component.get("v.SelectopportunityList");
        var index;
        var updatevalue = changemonthvalue.split(",");
        var updateforcastvalue = updatevalue[0];
        var updateforcastOppId = updatevalue[1];
        var updatewrapperList = [];
       if(changelist.length > 0 ){
            var listIndex = changelist.findIndex(x => x.Id==updateforcastOppId);
            if(listIndex != -1){
                // changelist.splice(listIndex,1);
                for(index in changelist){
                    var change = changelist[index];
                    if(changelist.Id == updateforcastOppId){
                        productWrapper.oppCloseDateMonth = updateforcastvalue;
                        changelist.push(productWrapper);
                    }
                }
                
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updateforcastOppId){
                        productWrapper.oppCloseDateMonth = updateforcastvalue;
                    }
                    updatewrapperList.push(productWrapper);
                }
            }else{
                for(index in opportunityList){
                    var productWrapper = opportunityList[index];
                    if(productWrapper.Id == updateforcastOppId){
                        productWrapper.oppCloseDateMonth = updateforcastvalue;
                        changelist.push(productWrapper);
                     }
                    updatewrapperList.push(productWrapper);
                }

            }
            
        } else {
            for(index in opportunityList){
                var productWrapper = opportunityList[index];
                if(productWrapper.Id == updateforcastOppId){
                    productWrapper.oppCloseDateMonth = updateforcastvalue;
                    changelist.push(productWrapper);
                }
                updatewrapperList.push(productWrapper);
            }
        }
        component.set("v.SelectopportunityList", changelist);
        component.set("v.opportunityWrapperList", updatewrapperList);
        //console.log(changelist.length);
      //  console.log(JSON.stringify(changelist));
    },
    scriptsLoaded : function(component, event, helper) {
		//console.log('load successfully');
       
       // active/call select2 plugin function after load jQuery and select2 plugin successfully    
       $(".select2Class").select2({
           placeholder: "Select Multiple values"
       });
	},
    clearFilter : function(component, event, helper) {
      location.reload();
    },
    exportdeals : function(component, event, helper) {
        var deals = component.get("v.opportunityList");
        // console.log(JSON.stringify(deals));
       // var PositionTitle = 'Selected Position';
        var data = [];
        var headerArray = [];
        var csvContentArray = [];
        //Provide the title 
      //  var CSV = 'rnSelected Positionrn';
        var CSV = '';
        headerArray.push('Id');
        headerArray.push('Opportunity ID');
        headerArray.push('Opportunity Name');
        headerArray.push('Opportunity Owner');
        headerArray.push('MSA/SOW Closure Date');
        headerArray.push('Stage');
        headerArray.push('TCV (USD)');
        headerArray.push('ForeCast Category');
        headerArray.push('Actual Stage');
        headerArray.push('Ground Level Probability');
        headerArray.push('Forecasted TCV (USD)');
        headerArray.push('Closure Month');
        data.push(headerArray);
        var sno = 0;
        for(var i=0;i<deals.length;i++){
            var tempArray = [];
            sno = parseInt(sno) + parseInt(1);
           // tempArray.push('"'+sno+'"');
            tempArray.push('"'+deals[i].Id+'"');
            tempArray.push('"'+deals[i].OPPId+'"');
            tempArray.push('"'+deals[i].OppName+'"');
            tempArray.push('"'+deals[i].OppOwner+'"');
            tempArray.push('"'+deals[i].CloseDate+'"');
            tempArray.push('"'+deals[i].Actualstage+'"');
            tempArray.push('"'+deals[i].TCV+'"');
            tempArray.push('"'+deals[i].ForeCastingCategoryName+'"');
            tempArray.push('"'+deals[i].foreCastStage+'"');
            tempArray.push('"'+deals[i].GroundLevelForecast+'"');
            tempArray.push('"'+deals[i].ForeCastingTCV+'"');
            tempArray.push('"'+deals[i].oppCloseDateMonth+'"');
            data.push(tempArray);
        }
        for(var j=0;j<data.length;j++){
            var dataString = data[j].join(",");
            csvContentArray.push(dataString);
        }
      //  var csvContent = csvContentArray;
        var csvContent = CSV + csvContentArray.join("\r\n");
        
        //Generate a file name
        var fileName = "Forecast Deals";
        //this will remove the blank-spaces from the title and replace it with an underscore
      //  fileName += PositionTitle.replace(/ /g,"_");   
        fileName += ".csv";
        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csvContent);
        
        if (navigator.msSaveBlob) { // IE 10+
     //       console.log('----------------if-----------');
            var blob = new Blob([csvContent],{type: "text/csv;charset=utf-8;"});
       //     console.log('----------------if-----------'+blob);
            navigator.msSaveBlob(blob, fileName);
        }
        else{
            var link = document.createElement("a");
            link.setAttribute('download',fileName);
            link.href = uri;
            link.style = "visibility:hidden";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }  
    },
    sortByCloserDate: function(component, event, helper) {
        component.set("v.isFilterLoading", true);
        helper.sortByDate(component, "CloseDate");
      //  component.set("v.isFilterLoading", false);
       // helper.sortBy(component, "TCV"); 
    },
    sortByTCV: function(component, event, helper) {
        helper.sortBy(component, "TCV");
    },
    sortByStage : function(component, event, helper) {
        helper.sortBy(component, "Actualstage");
    },
     goToOppportunity : function(component, event, helper) {
       /* var target = event.getSource();  
   	    var ChangeGroundForecastvalue = target.get("v.value");
        var whichOne;// = event.getSource().getLocalId();
         console.log('test'+whichOne+ChangeGroundForecastvalue); */
    }
})