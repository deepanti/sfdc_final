({
    
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var opp = component.find("oppName");
        var oppName = opp.get("v.value");
        if($A.util.isUndefinedOrNull(oppName) || oppName == '')
        {
            $A.util.addClass(opp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(opp,'slds-has-error');
        }
        var acc = component.find("account");
        var accountName = acc.get("v.value");
        if($A.util.isUndefinedOrNull(accountName) || accountName == '')
        {
            $A.util.addClass(acc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(acc,'slds-has-error');
        }
        var oppSrc = component.find("oppSource");
        var oppSrcVal = oppSrc.get("v.value");
        if($A.util.isUndefinedOrNull(oppSrcVal) || oppSrcVal == '')
        {
            $A.util.addClass(oppSrc,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSrc,'slds-has-error');
        }
         var nextStep = component.find("nextStep");
        var nextStepVal = nextStep.get("v.value");
        if($A.util.isUndefinedOrNull(nextStepVal) || nextStepVal == '')
        {
            $A.util.addClass(nextStep,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(nextStep,'slds-has-error');
        }
        
        var oppSubSource = component.find("oppSubSource");
        var oppSubSourceVal = oppSubSource.get("v.value");
        if($A.util.isUndefinedOrNull(oppSubSourceVal) || oppSubSourceVal == '')
        {
            $A.util.addClass(oppSubSource,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(oppSubSource,'slds-has-error');
        }
        
        
        
        var dealType = component.find("dealType");
        var dealTypeVar = dealType.get("v.value");
        var closeComp = component.find("closeComp");
        var closeCompVar = closeComp.get("v.value");
      /*  if(($A.util.isUndefinedOrNull(closeCompVar) || closeCompVar == '' || closeCompVar == undefined) && dealTypeVar =='Competitive')
        {
            
            $A.util.addClass(closeComp,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(closeComp,'slds-has-error');
        }
        */
        var salesRegion = component.find("salesRegion");
        var salesRegionVar = salesRegion.get("v.value");
        if($A.util.isUndefinedOrNull(salesRegionVar) || salesRegionVar == '')
        {
            $A.util.addClass(salesRegion,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesRegion,'slds-has-error');
        }
        var dealType = component.find("dealType");
        var dealTypeVar = dealType.get("v.value");
        if($A.util.isUndefinedOrNull(dealTypeVar) || dealTypeVar == '')
        {
            $A.util.addClass(dealType,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(dealType,'slds-has-error');
        }
        var salesCountry = component.find("salesCountry");
        var salesCountryVal = salesCountry.get("v.value");
        if($A.util.isUndefinedOrNull(salesCountryVal) || salesCountryVal == '')
        {
            $A.util.addClass(salesCountry,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(salesCountry,'slds-has-error');
        }
       /* var summary = component.find("summary");
        var summaryVal = summary.get("v.value");
        if($A.util.isUndefinedOrNull(summaryVal) || summaryVal == '')
        {
            $A.util.addClass(summary,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(summary,'slds-has-error');
        }*/
        var primaryContact = component.find("primaryContact");
        var contactVal = primaryContact.get("v.value");
        if($A.util.isUndefinedOrNull(contactVal) || contactVal == '')
        {
            $A.util.addClass(primaryContact,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(primaryContact,'slds-has-error');
        }
        var contactRole = component.find("contactRole");
        var contactRoleVal = contactRole.get("v.value");
        if($A.util.isUndefinedOrNull(contactRoleVal) || contactRoleVal == '')
        {
            $A.util.addClass(contactRole,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(contactRole,'slds-has-error');
        }
        var opportunityOrigination = component.find("opportunityOrigination");
        var opportunityOriginationVal = opportunityOrigination.get("v.value");
        if($A.util.isUndefinedOrNull(opportunityOriginationVal) || opportunityOriginationVal == '')
        {
            $A.util.addClass(opportunityOrigination,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(opportunityOrigination,'slds-has-error');
        }
        //component.set("v.IsSpinner",false);
        return proceedToSave;
    }, 
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    }
})