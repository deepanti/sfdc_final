({
    doInit : function(component, event, helper) {
        component.set("v.IsSpinner",true);
    },
    hideSpinner1 : function(component, event, helper) {
        component.set("v.IsSpinner",false);        
    },
    handleSectionToggle: function (component, event) {
        var openSections = event.getParam('openSections');
        
        if (openSections.length === 0) {
            component.set('v.activeSectionsMessage', "All sections are closed");
        } else {
            component.set('v.activeSectionsMessage', "Open sections: " + openSections.join(', '));
        }
    },
    saveRecord : function(component, event, helper){
       
        component.set("v.IsSpinner",true);
        
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
           
            //component.set("v.IsSpinner",false);
            var today = new Date();
            var closeDate = component.find("closeDate");
            var closeDateVal = closeDate.get("v.value");
            var tempDate = new Date(closeDateVal);
            //today = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();
            if($A.util.isUndefinedOrNull(closeDateVal) || tempDate < today)
            {
                $A.util.addClass(closeDate,'slds-has-error');
                component.set("v.IsSpinner",false);
                helper.fireToastError(component, event, helper, 'MSA/SOW closure date cannot be earlier than today');
            }
            else
            {
                $A.util.removeClass(closeDate,'slds-has-error');
                if(localStorage.getItem('MaxTabId')<2)
                {
                     if(localStorage.getItem('StageName')=='Prediscover')
                    {
                     	component.set("v.StageName","1. Discover");
                        localStorage.setItem('StageName',"1. Discover");
                    }
                   
                }

                component.find("recordViewForm").submit();
                localStorage.setItem('LatestTabId','1');
            }
            
            
        }
        else
        {
            
            helper.fireToastError(component, event, helper, 'Required fields are missing');
            component.set("v.IsSpinner",false);
        }
        
        
        
    },
    showSuccessMessage : function(component, event, helper) {
        component.set("v.IsSpinner",false);
        var appEvent = $A.get("e.c:SetSalesPath");
        appEvent.fire();
        //if(localStorage.getItem('StageName') == '1. Discover')       
        var check = component.get("v.navigateToNext");
        if(check)
        {
            var compEvents = component.getEvent("componentEventFired");
            compEvents.setParams({ "SelectedTabId" : "2"});
            compEvents.fire();
            component.set("v.IsSpinner",false);
            window.scrollTo(0, 0);
        }
        helper.fireToastSuccess(component, event, helper, 'Opportunity Saved Successfully');
    },
    saveAndNext : function(component, event, helper) { 
        
        component.set("v.IsSpinner",true);
        component.set("v.navigateToNext",true);
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            
            //component.set("v.IsSpinner",false);
            var today = new Date();
            var closeDate = component.find("closeDate");
            var closeDateVal = closeDate.get("v.value");
            var tempDate = new Date(closeDateVal);
            //today = today.getFullYear() + '-' + (today.getMonth()+1) + '-' + today.getDate();
            if($A.util.isUndefinedOrNull(closeDateVal) || tempDate < today)
            {
                $A.util.addClass(closeDate,'slds-has-error');
                component.set("v.IsSpinner",false);
                helper.fireToastError(component, event, helper, 'MSA/SOW closure date cannot be earlier than today');
            }
            else
            {
                
                //component.set("v.IsSpinner",true);//fix added to refresh simultanously 
                $A.util.removeClass(closeDate,'slds-has-error');
                
                if(localStorage.getItem('MaxTabId')<2)
                {
                    component.set("v.MaxSelectedTabId", "2");
                    localStorage.setItem('MaxTabId',2);
                    if(localStorage.getItem('StageName')=='Prediscover')
                    {
                        component.set("v.StageName","1. Discover");                        
                        localStorage.setItem('StageName',"1. Discover");
                    }
                    
                }
                localStorage.setItem('LatestTabId',"2");
                component.find("recordViewForm").submit();                                
            }
        }
        else
        {
            component.set("v.IsSpinner",false);
            helper.fireToastError(component, event, helper, 'Required fields are missing');
        }
    },
    
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',"0");
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "0"});
        appEvent.fire();
        window.scrollTo(0, 0);
    },
    showError: function(component, event, helper) {
        helper.fireToastError(component, event, helper, 'Required fields are missing');
    },
    
    showError1 : function(component, event, helper) {
        
        var error = event.getParams();
        
        var errorMsg='';
        // top level error messages
        if( !$A.util.isUndefinedOrNull(error.output.errors))
        {
            
            error.output.errors.forEach(
                function(msg) { 
                    if(!$A.util.isUndefinedOrNull(msg.message))
                        errorMsg +=msg.message;
                }
            );
            
            // top level error messages
            Object.keys(error.output.fieldErrors).forEach(
                function(field) { 
                    error.output.fieldErrors[field].forEach(
                        function(msg) { 
                            if(!$A.util.isUndefinedOrNull(msg.message))
                                errorMsg +=msg.message + ', ';
                        }
                    )
                });
        }
        helper.fireToastError(component, event, helper, errorMsg);
        component.set("v.IsSpinner",false);
    }
})