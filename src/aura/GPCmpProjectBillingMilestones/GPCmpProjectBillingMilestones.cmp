<aura:component controller="GPCmpServiceProjectBillingMilestones"
                implements="flexipage:availableForAllPageTypes,force:hasRecordId"
                extends="c:GPCmpBaseUtility"
                description="Component to manage Project billing milestone under a project in single view.">
    
    <!-- public attributes -->
    <aura:attribute name="recordId"
                    type="Id"
                    description="The record id of project in context of which this component is opened."/>
    
    <aura:attribute name="billingMilestoneStatus"
                    type="string"
                    description="Maintains the state of project(unTouched, dirty, approved)"/>
    
    <!-- private attribute -->
    <aura:attribute name="listOfBillingMilestone"
                    type="Object[]"
                    access="private"
                    description="list of billing milestone under the given project."/>
    
    <aura:attribute name="listOfMassBillingMilestone"
                    type="Object[]"
                    description="list of parsed records from CSV ."/>
    
    <aura:attribute name="lstOfWorkLocations" 
                    type="String[]" 
                    description="List Of Related Work Locations"/>
    
    <aura:attribute name="listOfMilestoneTypeOptions"
                    type="Object[]"
                    access="private"
                    description="list of billing milestone type."/>
    
    <aura:attribute name="mapOfFieldNameToTemplate"
                    type="Object"
                    access="private"
                    description="Map of field name to its template."/>
    
    <aura:attribute name="listOfVisibleFields"
                    type="Object[]"
                    access="private"
                    description="List of visible fields in project template."/>
    
    <aura:attribute name="projectTCVAmount"
                    type="Double"
                    access="private"
                    description="Project TCV Amount!"
                    default="0"/>
    
    <aura:attribute name="totalOfBillingMilestones"
                    type="Double"
                    access="private"
                    description="Total Billing Milestone."
                    default="0"/>
    
    <aura:attribute name="projectStartDate"
                    type="Date"
                    access="private"
                    description="Project Start Date!"/>
    
    <aura:attribute name="projectEndDate"
                    type="Date"
                    access="private"
                    description="Project End Date!"/>
    
    <aura:attribute name="errorString"
                    type="String"
                    access="private"
                    description="Error Of Component!"/>
    
    <aura:attribute name="isUpdatable"
                    type="Boolean"
                    access="private"
                    description="A variable to keep track whether the record is updatable."
                    default="true"/>
    
    <aura:attribute name="isITOType"
                    type="Boolean"
                    access="private"
                    description="A variable to identify ITO type of Project."
                    default="false"/>
    
    <aura:attribute name="isMileStoneDefinable"
                    type="Boolean"
                    access="private"
                    description="A variable to identify Milestone can be defined for project or not."
                    default="false"/>
    
    <aura:attribute name="currencyISOCode"
                    type="String"
                    access="private"
                    description="Project Currency ISO Code."/>
    
    <aura:attribute name="dateToday"
                    type="Date"
                    access="private"
                    description="A variable to keep todays Date."/>
    
    <aura:attribute name="showError"
                    type="Boolean"
                    access="private"
                    description="Show Error Block!"/>
    
    <aura:attribute name="noOfRowsToBeDisplayed"
                    type="Integer"
                    access="private"
                    description="No of Default Rows!"/>
    
    <aura:attribute name="openMassUpload"
                    type="Boolean"
                    default="false"
                    description="Show Error Block!"/>
    
    <aura:attribute name="headerRow"
                    type="Object[]"
                    description="Header row for Mass Upload!"/>
    
    <aura:attribute name="mapOfWorkLocation"
                    type="Object"
                    description="Oracle Id to WorkLocation map."/>
    
    <aura:attribute name="selectAll"
                    type="Boolean"
                    description="Manage Select All."/>
    <aura:attribute name="isEditable"
                    type="Boolean"
                    description="isEditable"/>
    
    <!--aura:attribute name="instructions"
                    type="String"
                    default="1. Date format should be of the form yyyy-mm-dd.
                             &lt;br/> 2. Work Location Oracle Id will be filled for Work Location column."/-->
    <aura:attribute name="instructions"
                    type="String"
                    default="1. Date format should be of the format yyyy-mm-dd. 
                             &lt;br/>2. Date should lie between project start and end date.
                             &lt;br/>3. Work Location Oracle Id will be filled for Work Location column. 
                             &lt;br/>4. Entry Type can be either 'Percent' or 'Amount'.
                             &lt;br/>5. Value can be a number only."/>
    
    
    <!-- Handler -->
    <aura:handler name="init"
                  value="{!this}"
                  action="{!c.doInit}"
                  description=""/>
    
    <aura:handler name="change" 
                  value="{!v.listOfBillingMilestone}" 
                  action="{!c.onChangelistOfProjectBillingMilestoneService}"/>
    
    <aura:handler name="change" 
                  value="{!v.listOfMassBillingMilestone}" 
                  action="{!c.onChangelistOfMassProjectBillingMilestoneService}"/>
    
    <aura:handler event="c:GPEvntProjectSave" action="{!c.doInit}"/>
    
    <!-- Markup -->
    <lightning:spinner size="medium"
                       variant="brand"
                       alternativeText="Loading..."
                       class="{!if(v.isLoading, '', 'slds-hide')}"/>
    
    <aura:if isTrue="{!v.openMassUpload}">
        <c:GPCmpBaseCSVUtility showPopUp="{!v.openMassUpload}"
                               listOfParsedRecords="{!v.listOfMassBillingMilestone}"
                               headerRow="{!v.headerRow}"
                               parseToObjectLabel="Billing MileStone"
                               exportfileName="uploadTemplateBillingMileStone.csv"
                               basicInfoRegardingDataUpload="{!v.instructions}"/>
    </aura:if>
    
    <div class="modal-body slds-scrollable_none slds-modal__content slds-p-around--medium slds-grid slds-wrap cstm-lr-panel">
        
        <div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--1-of-1 slds-large-size--1-of-5 csleft-panel">
            <em class="sidePanel_collapse" onclick="{!c.toggleSidebar}"></em>
            <ul>
                <li class="active"
                    title="Billing Milestone">
                    <a href="#">
                        <i class="fa fa-file" aria-hidden="true"></i>
                        Billing Milestone
                    </a>
                </li>
            </ul>
        </div>
        
        <div class="csrightpanel hide-slds-header">
            
            <aura:if isTrue="{!or(v.showError,!v.isMileStoneDefinable)}">
                <div class="slds-grid slds-text-align_center slds-text-align--center backgroundHeader">
                    <div class="slds-grid">
                        <br/>&nbsp;{!v.errorString}
                    </div>
                </div>
                <br/>
            </aura:if>
            
            <aura:if isTrue="{!v.isMileStoneDefinable}">
                <div class="slds-grid full forcePageBlockSectionRow errorRow">
                    <c:GPCMPOutputApprovalLog mapOfError="{!v.errorLog}"
                                              activeSectionName="Billing Milestone"
                                              isValidating="{!v.isValidating}"
                                              activeSubSectionName="Billing Milestone"/>
                </div>
                <lightning:card title="Billing Milestone">
                    <div class="slds-grid row slds-scrollable slds-p-bottom_small">
                        <table class="slds-table slds-table_bordered slds-table_striped slds-no-row-hover slds-max-medium-table_stacked-horizontal cstmtable unsettable">
                            
                            <thead>
                                <tr class="slds-text-title_caps">
                                    <th scope="col" style="width:3.25rem;">
                                        <ui:inputCheckbox value="{!v.selectAll}"
                                                          change="{!c.selectAllRecords}"
                                                          disabled="{!!v.isUpdatable}"/>
                                    </th>
                                    <th data-label="Milestone Name">
                                        Milestone Name
                                    </th>  
                                    
                                    <th data-label="Milestone Date">
                                        Milestone Date
                                    </th>
                                    <th data-label="Milestone Start Date">
                                        Milestone Start Date
                                    </th>
                                    <th data-label="Milestone End Date">
                                        Milestone End Date
                                    </th>
                                    <th data-label="Work Location"
                                        class="slds-truncate">
                                        Work Location
                                    </th>
                                    
                                    <th data-label="Entry Type">
                                        Entry Type
                                        
                                    </th>
                                    
                                    <th data-label="Value">
                                        Value
                                    </th>
                                    
                                    <th data-label="Amount">
                                        Amount({!v.currencyISOCode})
                                    </th>
                                    
                                </tr>
                            </thead>
                            
                            <tbody>
                                <aura:iteration items="{!v.listOfBillingMilestone}" 
                                                var="billingMilestone"
                                                indexVar="billingMilestoneIndex">
                                    <tr id="{!'project-milestone-row' + billingMilestoneIndex}">
                                        <td data-label="Action">
                                            <c:UIToolTip class="{!if(billingMilestone.errorMessage, 
                                                                'slds-show slds-icon-text-error', 
                                                                'slds-hide')}"
                                                         text="{!billingMilestone.errorMessage}"
                                                         iconName="utility:warning"
                                                         variant="error"
                                                         size="small"
                                                         placement="right"/>
                                            <!--a class="{!if(and(v.isUpdatable,
                                                  billingMilestone.GP_Billing_Status__c != 'Not Billed'),
                                                  'slds-truncate','disable-anchor')}"
                                           title="{!if(billingMilestone.isEditable, '', 'Edit')}"
                                           id="{!billingMilestoneIndex}"
                                           onclick="{!c.toggelIsEditableForBillingMilestoneRow}">
                                            {!if(billingMilestone.isEditable, '', 'Edit ')}
                                        </a> 
                                        <aura:if isTrue="{!!billingMilestone.isEditable}">
                                            |
                                        </aura:if>
                                        <a class="{!if(and(v.isUpdatable,
                                                  !billingMilestone.GP_Parent_Billing_Milestone__c ,
                                                  billingMilestone.GP_Billing_Status__c != 'Not Billed'),'slds-truncate','disable-anchor')}"
                                           title="Delete"
                                           id="{!billingMilestoneIndex}"
                                           onclick="{!c.removeBillingMilestone}">Delete</a-->
                                            <ui:inputCheckbox disabled="{!or(!billingMilestone.isEditable ,!v.isUpdatable)}"
                                                              value="{!billingMilestone.isSelected}"
                                                              class="slds-truncate"/>
                                        </td>
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___Name.isVisible}"-->
                                        <td data-label="Name">
                                            <div class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                 title="{!billingMilestone.Name}">
                                                {!billingMilestone.Name}
                                            </div>
                                            <ui:inputText value="{!billingMilestone.Name}"
                                                          class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}"/>
                                        </td>                            
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!and(!v.isITOType,v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Date__c.isVisible)}"-->
                                        <td data-label="Date">
                                            <ui:outputDate class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                           value="{!billingMilestone.GP_Date__c}"/> 
                                            <div class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}">
                                                <ui:inputDate value="{!billingMilestone.GP_Date__c}"
                                                              displayDatePicker="true"/>
                                            </div>
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.isITOType}">
                                            
                                            <aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Milestone_start_date__c.isVisible}"-->
                                        <td data-label="Start Date">
                                            <ui:outputDate class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                           value="{!billingMilestone.GP_Milestone_start_date__c}"/> 
                                            <div class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}">
                                                <ui:inputDate value="{!billingMilestone.GP_Milestone_start_date__c}"
                                                              displayDatePicker="true"/>
                                            </div>
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Milestone_end_date__c.isVisible}"-->
                                        <td data-label="End Date">
                                            <ui:outputDate class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                           value="{!billingMilestone.GP_Milestone_end_date__c}"/> 
                                            <div class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}">
                                                <ui:inputDate value="{!billingMilestone.GP_Milestone_end_date__c}"
                                                              displayDatePicker="true"/>
                                            </div>
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Work_Location__c.isVisible}"-->
                                        <td data-label="Work Location"
                                            class="slds-truncate">
                                            <ui:outputText class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                           value="{!billingMilestone.GP_Work_Location__r.Name}"
                                                           title="{!billingMilestone.GP_Work_Location__r.Name}"/>
                                            
                                            <aura:if isTrue="{!billingMilestone.isEditable}">
                                                <lightning:select aura:id="workLocations"
                                                                  name="Work Location" 
                                                                  variant="label-hidden"
                                                                  value="{!billingMilestone.GP_Work_Location__c}"
                                                                  disabled="{!!billingMilestone.isEditable}">
                                                    <aura:iteration items="{!v.lstOfWorkLocations}" 
                                                                    var="workLocation">
                                                        <option value="{!workLocation.text}"
                                                                selected="{billingMilestone.GP_Work_Location__c == workLocation.text}">
                                                            {!workLocation.label}
                                                        </option>
                                                    </aura:iteration>
                                                </lightning:select>
                                            </aura:if>
                                            
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Entry_type__c.isVisible}"-->
                                        <td data-label="Entry Type">
                                            <div class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                 title="{!billingMilestone.GP_Entry_type__c}">
                                                {!billingMilestone.GP_Entry_type__c}
                                            </div>
                                            
                                            <aura:if isTrue="{!billingMilestone.isEditable}">
                                                <lightning:select name="Entry Type" 
                                                                  class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}"
                                                                  value="{!billingMilestone.GP_Entry_type__c}"
                                                                  label="{!'GP_Entry_type__c@@'+billingMilestoneIndex}" 
                                                                  onchange="{!c.entryTypeChangeHandler}"
                                                                  variant="label-hidden">
                                                    <aura:iteration items="{!v.listOfMilestoneTypeOptions}"
                                                                    var="milestoneType">
                                                        <option value="{!milestoneType.text}"
                                                             selected="{billingMilestone.GP_Entry_type__c == milestoneType.text}">
                                                                {!milestoneType.label}
                                                            </option>
                                                    </aura:iteration>
                                                </lightning:select>
                                            </aura:if>
                                            
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Value__c.isVisible}"-->
                                        <td data-label="Value">
                                            <ui:outputNumber class="{!if(billingMilestone.isEditable, 'slds-hide', 'slds-truncate')}"
                                                             value="{!billingMilestone.GP_Value__c}"/> 
                                            <div class="{!if(billingMilestone.isEditable, 'slds-truncate', 'slds-hide')}">
                                                <input type="number"
                                                       min="0"
                                                       class="input uiInput--input"
                                                       value="{!billingMilestone.GP_Value__c}"
                                                       data-budget-index="{!billingMilestoneIndex}"
                                                       data-attribute-name="GP_Value__c"
                                                       onchange="{!c.valueChangeHandler}"/>
                                            </div>
                                        </td>
                                        <!--/aura:if-->
                                        
                                        <!--aura:if isTrue="{!v.mapOfFieldNameToTemplate.GP_Billing_Milestone__c___All___GP_Amount__c.isVisible}"-->
                                        <td data-label="Amount">
                                            <ui:outputCurrency class="slds-truncate" 
                                                               value="{!billingMilestone.GP_Amount__c}"
                                                               currencySymbol="{!v.currencyISOCode + ' '}"/>
                                        </td>
                                        <!--/aura:if-->
                                        
                                    </tr>
                                </aura:iteration>
                                
                                <tr>
                                    <td colspan="8" 
                                        style="text-align-last:right"
                                        class="slds-show_medium">
                                        <b>Total:</b>
                                    </td>
                                    <td  data-label="Total Amount">
                                        <ui:outputCurrency class="slds-truncate" 
                                                           value="{!v.totalOfBillingMilestones}"
                                                           currencySymbol="{!v.currencyISOCode + ' '}"/>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="8" 
                                        style="text-align-last:right"
                                        class="slds-show_medium">
                                        <b> Project TCV Amount: </b>
                                    </td>
                                    <td data-label="Project TCV Amount">
                                        <ui:outputCurrency class="slds-truncate" 
                                                           value="{!v.projectTCVAmount}"
                                                           currencySymbol="{!v.currencyISOCode + ' '}"/>
                                    </td>
                                </tr>
                                
                            </tbody>
                            
                        </table>
                    </div> 
                    <br/>
                    <lightning:buttonGroup class="slds-float--right">
                        <lightning:button label="Download in Excel"
                                          class="slds-button slds-button--neutral cstm-btn "
                                           onclick="{!c.downloadAllocation}"/>
                    </lightning:buttonGroup>
                    <lightning:buttonGroup class="{!if(v.isUpdatable, 'slds-float--right', 'slds-hide')}">
                        
                         
                        <lightning:button label="Mass Upload"
                                          variant="neutral"
                                          class="{!if(v.isEditable, 'slds-button slds-button--neutral cstm-btn massUpload-btn', 'slds-hide')}"
                                          onclick="{!c.extendToAnotherComponent}"
                                          disabled="{!!v.isUpdatable}"/>
                        <lightning:button label="Add" 
                                          variant="neutral"
                                          iconName="utility:add"
                                          iconPosition="left"
                                          class="{!if(v.isEditable, 'slds-button slds-button--neutral cstm-btn topbtm-radius', 'slds-hide')}"
                                          onclick="{!c.addToBillingMilestone}"
                                          disabled="{!!v.isUpdatable}"/>
                        <lightning:button label="Edit"
                                          variant="neutral"
                                          iconPosition="left"
                                          class="{!if(v.isEditable, 'slds-button slds-button--neutral cstm-btn', 'slds-hide')}"
                                          onclick="{!c.editAllRows}"
                                          disabled="{!!v.isUpdatable}"/>
                        <lightning:button label="Save"
                                          variant="neutral"
                                          iconPosition="left"
                                          class="{!if(v.isEditable, 'slds-button slds-button--neutral cstm-btn', 'slds-hide')}"
                                          onclick="{!c.saveBillingMilestone}"
                                          disabled="{!!v.isUpdatable}"/>
                        
                        <lightning:button label="Delete Selected"
                                          variant="neutral"
                                          iconPosition="left"
                                          class="{!if(v.isEditable, 'slds-button slds-button--neutral cstm-btn', 'slds-hide')}"
                                          onclick="{!c.removeSelectedRecords}"
                                          disabled="{!!v.isUpdatable}"/>
                        
                    </lightning:buttonGroup>
                    
                    <br/> <br/>
                    
                </lightning:card>
            </aura:if>
            
        </div>
    </div>
</aura:component>