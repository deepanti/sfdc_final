({
    MILESTONE_NOT_DEFINED_ERROR_LABEL: "Milestones cannot be defined for this type of Project.",
    MIX_ENTRIES_ERROR_LABEL: "Either Select Percent or amount,mix entries aren't allowed!",
    PERCENTAGE_ERROR_LABEL: "Value cannot be greater than 100 and less than 0.",
    DATE_RANGE_ERROR_LABEL: "Milestone Date should be within project duration.",
    AMOUNT_NEGATIVE_LABEL: "Amount cannot be less than or equal to zero.",
    COMBINATION_REPEAT: "Repeated Combination.",
    AMOUNT_ERROR_LABEL: "Value cannot be 0.",
    NONE_ENTRY_LABEL: "--Select--",
    LABEL_FIELD_TO_APINAME_MAP: {
        "name": "Name",
        "date": "GP_Date__c",
        "work location": "GP_Work_Location__c",
        "entry type": "GP_Entry_type__c",
        "value": "GP_Value__c",
        "milestone start date": "GP_Milestone_start_date__c",
        "milestone end date": "GP_Milestone_end_date__c",
    },

    getBillingMilestones: function(component) {
        var getBillingMilestoneService = component.get("c.getBillingMilestone");
        getBillingMilestoneService.setParams({
            "projectId": component.get("v.recordId")
        });
        getBillingMilestoneService.setCallback(this, function(response) {
            this.getBillingMilestoneServiceHandler(response, component);
        });

        $A.enqueueAction(getBillingMilestoneService);
    },
    getBillingMilestoneServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.setBillingMilestoneData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setBillingMilestoneData: function(component, responseData) {
        var responseJson = JSON.parse(responseData.response);
        console.log('==MY responseJson==',responseJson);
        component.set("v.isMileStoneDefinable", responseJson.isMileStoneDefinable);
        if (responseJson.isMileStoneDefinable) {
            this.setAllAttributes(component, responseJson);
        } else {
            component.set("v.showError", true);
            component.set("v.errorString", this.MILESTONE_NOT_DEFINED_ERROR_LABEL);
        }
    },
    setAllAttributes: function(component, responseJson) {
        component.set("v.listOfMilestoneTypeOptions", responseJson.listOfMilestoneTypeOptions);
        component.set("v.mapOfFieldNameToTemplate", responseJson.mapOfFieldNameToTemplate);
        component.set("v.listOfBillingMilestone", responseJson.listOfBillingMilestone);
        component.set("v.noOfRowsToBeDisplayed", responseJson.noOfRowsToBeDisplayed);
        component.set("v.mapOfWorkLocation", responseJson.mapOfWorkLocation);
        component.set("v.projectTCVAmount", responseJson.projectTCVAmount);
        component.set("v.projectStartDate", responseJson.projectStartDate);
        component.set("v.currencyISOCode", responseJson.currencyISOCode);
        component.set("v.lstOfWorkLocations", responseJson.workLocation);
        component.set("v.projectEndDate", responseJson.projectEnDate);
        component.set("v.isUpdatable", responseJson.isUpdatable);
        component.set("v.isITOType", responseJson.isITOType);

        this.setVisibleFieldsFromTemplate(component, responseJson.mapOfFieldNameToTemplate);
    },
    setVisibleFieldsFromTemplate: function(component, mapOfFieldNameToTemplate) {
        var isITOType = component.get("v.isITOType");
        var listOfVisibleFields = [];

        for (var key in mapOfFieldNameToTemplate) {
            if (mapOfFieldNameToTemplate[key]["isVisible"]) {
                /*if (isITOType && (key != 'GP_Billing_Milestone__c___All___GP_Date__c'))
                    listOfVisibleFields.push(mapOfFieldNameToTemplate[key]);
                else if (!isITOType && (key != 'GP_Billing_Milestone__c___All___GP_Milestone_start_date__c' &&
                        key != 'GP_Billing_Milestone__c___All___GP_Milestone_end_date__c'))*/
                listOfVisibleFields.push(mapOfFieldNameToTemplate[key]);
            }
        }
        component.set("v.listOfVisibleFields", listOfVisibleFields);

        this.setheaderRow(component, listOfVisibleFields);
        this.addDefaultRows(component);
    },
    addDefaultRows: function(component) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone") || [];
        var noOfRowsToBeDisplayed = component.get("v.noOfRowsToBeDisplayed");
        var projectId = component.get("v.recordId");
        if (listOfBillingMilestone.length == 0) {
            for (var rowCount = 0; rowCount < noOfRowsToBeDisplayed; rowCount++) {
                this.newRowAdd(projectId, listOfBillingMilestone);
            }
        }
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);

    },
    setheaderRow: function(component, listOfVisibleFields) {
        var headerRowChild = [];
        for (var index = 0; index < listOfVisibleFields.length; index++) {
            if (listOfVisibleFields[index].Label != 'Amount')
                headerRowChild.push(listOfVisibleFields[index].Label);
        }

        var headerRowWrapper = [];
        headerRowWrapper.push(headerRowChild);
        component.set("v.headerRow", headerRowWrapper);
    },
    addToBillingMilestone: function(component) {
        var projectId = component.get("v.recordId");
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone") || [];

        this.newRowAdd(projectId, listOfBillingMilestone);
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    newRowAdd: function(projectId, listOfBillingMilestone) {
        var billingMilestone = {
            "isEditable": true,
            "GP_Project__c": projectId,
            "GP_Billing_Status__c": "Not Billed"
        };
        listOfBillingMilestone.push(billingMilestone);
    },
    removeBillingMilestone: function(component, billingMilestoneIndex) {

        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var billingMilestoneToBeDeleted = listOfBillingMilestone[billingMilestoneIndex];

        listOfBillingMilestone.splice(billingMilestoneIndex, 1);

        component.set("v.listOfBillingMilestone", listOfBillingMilestone);

        this.deleteBillingMilestoneData(component, billingMilestoneToBeDeleted["Id"]);
    },
    saveBillingMilestone: function(listOfBillingMilestone, component) {
        var currencyISOCode = component.get("v.currencyISOCode");
        this.updateCurrencyOnAllRecords(component, listOfBillingMilestone, currencyISOCode);
        var saveBillingMilestonesService = component.get("c.saveBillingMilestones");
        saveBillingMilestonesService.setParams({
            "serializedBillingMilestones": JSON.stringify(listOfBillingMilestone)
        });
        this.showSpinner(component);
        saveBillingMilestonesService.setCallback(this, function(response) {
            this.saveBillingMilestonesServiceHandler(response, component);
        });

        $A.enqueueAction(saveBillingMilestonesService);
    },
    saveBillingMilestonesServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            component.set("v.listOfBillingMilestone", JSON.parse(responseData.response));
            this.updateBillingMilestoneProgressStatus(component);
            this.showToast('SUCCESS', 'success', 'Billing milestone Saved successfully.');
            this.fireProjectSaveEvent();
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    setMilestoneIsEditableToFalse: function(component, listOfBillingMilestone) {
        for (var i = 0; i < listOfBillingMilestone.length; i += 1) {
            listOfBillingMilestone[i]["isEditable"] = false;
        }
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    deleteBillingMilestoneData: function(component, billingMilestoneId) {

        if (!billingMilestoneId)
            return;

        var deleteBillingMilestoneService = component.get("c.deleteBillingMilestone");
        deleteBillingMilestoneService.setParams({
            "billingMilestoneId": billingMilestoneId
        });
        this.showSpinner(component);
        deleteBillingMilestoneService.setCallback(this, function(response) {
            this.deleteBillingMileStoneDataServiceHandler(response, component);
        });
        $A.enqueueAction(deleteBillingMilestoneService);
    },
    deleteBillingMileStoneDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Billing milestone deleted successfully.');
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    updateBillingMilestoneProgressStatus: function(component) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");

        if (listOfBillingMilestone && listOfBillingMilestone.length > 0) {
            component.set("v.billingMilestoneStatus", "dirty");
        } else {
            component.set("v.billingMilestoneStatus", "unChanged");
        }
    },
    validateInput: function(component) {
        return true;
        component.set("v.showError", false);
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var projectTCVAmount = component.get("v.projectTCVAmount");
        var billingMileStoneAmount = 0;
        listOfBillingMilestone.forEach(function(billingMileStone) {
            billingMileStoneAmount += Number(billingMileStone.GP_Amount__c);
        });
        if (projectTCVAmount < billingMileStoneAmount)
            return false;
        return true;
    },
    showErrorBlock: function(component) {
        var projectTCVAmount = component.get("v.projectTCVAmount") ? component.get("v.projectTCVAmount") : 0;
        component.set("v.errorString", "Sum of Billing Milestones Amount should not be more than " + projectTCVAmount);
        component.set("v.showError", true);
    },
    validateProjectMileStone: function(component, listOfBillingMilestone) {
        var key, rowDom;
        var isValid = true;
        var projectMilestoneObjectMap = {};
        var entryType = listOfBillingMilestone[0]["GP_Entry_type__c"];
        var projectStartDate = component.get("v.projectStartDate");

        if (projectStartDate)
            projectStartDate = projectStartDate.toLocaleString();

        var projectEndDate = component.get("v.projectEndDate");

        if (projectEndDate)
            projectEndDate = projectEndDate.toLocaleString();

        var isITOType = component.get("v.isITOType");

        for (var i = 0; i < listOfBillingMilestone.length; i += 1) {
            key = listOfBillingMilestone[i]["GP_Work_Location__c"] + '@@';
            key += listOfBillingMilestone[i]["GP_Entry_type__c"];
            rowDom = document.getElementById('project-milestone-row' + i);

            if ($A.util.isEmpty(projectStartDate)) {
                component.set("v.errorString", "Project Start Date is not defined");
                component.set("v.showError", true);
            }

            if (this.checkEmptyValuesOfFields(listOfBillingMilestone[i], isITOType)) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = "Please fill Required Fields!";
            } else if (entryType != listOfBillingMilestone[i]["GP_Entry_type__c"]) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = this.MIX_ENTRIES_ERROR_LABEL;
            } /*else if (!$A.util.isEmpty(listOfBillingMilestone[i]["GP_Amount__c"]) &&
                listOfBillingMilestone[i]["GP_Amount__c"] <= 0) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = this.AMOUNT_NEGATIVE_LABEL;
            } */else if (new Date(listOfBillingMilestone[i]["GP_Milestone_start_date__c"]) >
                new Date(listOfBillingMilestone[i]["GP_Milestone_end_date__c"])) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = "Start Date Cannot be greater than End Date";
            }
            /*else if (!(!isITOType && (listOfBillingMilestone[i]["GP_Date__c"] &&
                               listOfBillingMilestone[i]["GP_Date__c"].toLocaleString() <= projectEndDate &&
                               listOfBillingMilestone[i]["GP_Date__c"].toLocaleString() >= projectStartDate))) {
                           isValid = false;
                           rowDom.className = 'invalid-row';
                           listOfBillingMilestone[i]["errorMessage"] = this.DATE_RANGE_ERROR_LABEL;
                       } else if (isITOType && this.checkDateRangeForITOTypeProject(projectStartDate, projectEndDate,
                               listOfBillingMilestone[i]["GP_Milestone_start_date__c"],
                               listOfBillingMilestone[i]["GP_Milestone_end_date__c"])) {
                           isValid = false;
                           rowDom.className = 'invalid-row';
                           listOfBillingMilestone[i]["errorMessage"] = this.DATE_RANGE_ERROR_LABEL;
                       }*/
            else if (!$A.util.isEmpty(listOfBillingMilestone[i]["GP_Entry_type__c"]) &&
                listOfBillingMilestone[i]["GP_Entry_type__c"] == 'Percent' &&
                !(listOfBillingMilestone[i]["GP_Value__c"] > 0 && listOfBillingMilestone[i]["GP_Value__c"] <= 100)) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = this.PERCENTAGE_ERROR_LABEL;
            } else if (!$A.util.isEmpty(listOfBillingMilestone[i]["GP_Entry_type__c"]) &&
                listOfBillingMilestone[i]["GP_Entry_type__c"] == 'Amount' &&
                listOfBillingMilestone[i]["GP_Value__c"] == 0) {
                isValid = false;
                rowDom.className = 'invalid-row';
                listOfBillingMilestone[i]["errorMessage"] = this.AMOUNT_ERROR_LABEL;
            } else {
                listOfBillingMilestone[i]["errorMessage"] = null;
                projectMilestoneObjectMap[key] = listOfBillingMilestone[i];
            }
        }

        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
        return isValid;
    },
    checkEmptyValuesOfFields: function(mileStoneRecord, isITOType) {
        return ($A.util.isEmpty(mileStoneRecord["Name"]) ||
            $A.util.isEmpty(mileStoneRecord["GP_Work_Location__c"]) ||
            $A.util.isEmpty(mileStoneRecord["GP_Entry_type__c"]) ||
            mileStoneRecord["GP_Entry_type__c"] == this.NONE_ENTRY_LABEL ||
            $A.util.isEmpty(mileStoneRecord["GP_Date__c"]) || $A.util.isEmpty(mileStoneRecord["GP_Milestone_start_date__c"]) ||
            $A.util.isEmpty(mileStoneRecord["GP_Milestone_end_date__c"]));
        // (!isITOType && ($A.util.isEmpty(mileStoneRecord["GP_Date__c"]))) ||
        // (isITOType && ($A.util.isEmpty(mileStoneRecord["GP_Milestone_start_date__c"]) ||
        //   $A.util.isEmpty(mileStoneRecord["GP_Milestone_end_date__c"]))));
    },
    setAmount: function(component, billingMilestoneIndex, billingMilestoneField, value) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var currentProjectBillingMileStone = listOfBillingMilestone[billingMilestoneIndex];

        currentProjectBillingMileStone[billingMilestoneField] = value;

        var entryType = currentProjectBillingMileStone["GP_Entry_type__c"];
        var enteredValue = Number(currentProjectBillingMileStone["GP_Value__c"]);

        var projectTCVAmount = component.get("v.projectTCVAmount");
        var rowDom = document.getElementById('project-milestone-row' + billingMilestoneIndex);

        var amount = 0;

        if (!$A.util.isEmpty(entryType) &&
            entryType == 'Percent' &&
            !(enteredValue > 0 && enteredValue <= 100)) {
            rowDom.className = 'invalid-row';
            listOfBillingMilestone[billingMilestoneIndex]["errorMessage"] = this.PERCENTAGE_ERROR_LABEL;
        } else {
            rowDom.className = '';
            listOfBillingMilestone[billingMilestoneIndex]["errorMessage"] = null;
        }

        if (!$A.util.isEmpty(entryType) &&
            !$A.util.isEmpty(enteredValue) &&
            (!isNaN(enteredValue))) {

            if (entryType == "Percent")
                amount = ((enteredValue * projectTCVAmount) / 100).toFixed(2);
            else
                amount = enteredValue;

            currentProjectBillingMileStone["GP_Amount__c"] = amount;
        } else {
            currentProjectBillingMileStone["GP_Amount__c"] = 0;
        }

        listOfBillingMilestone[billingMilestoneIndex] = currentProjectBillingMileStone;
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    isDeletable: function(component, billingMilestoneIndex, listOfBillingMilestone) {
        var billingMilestoneToBeDeleted = listOfBillingMilestone[billingMilestoneIndex];
        var Id = billingMilestoneToBeDeleted["Id"];
        var dateToday = new Date(component.get("v.dateToday")).toLocaleString();

        if (!$A.util.isEmpty(Id) && !$A.util.isEmpty(billingMilestoneToBeDeleted["GP_Date__c"]) &&
            (new Date(billingMilestoneToBeDeleted["GP_Date__c"]).toLocaleString() < dateToday))
            return false;

        return true;
    },
    onChangelistOfProjectBillingMilestone: function(component) {
        var totalOfBillingMilestones = 0;
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");

        if (listOfBillingMilestone) {
            for (var i = 0; i < listOfBillingMilestone.length; i += 1) {
                if (listOfBillingMilestone[i]["GP_Amount__c"])
                    totalOfBillingMilestones += Number(listOfBillingMilestone[i]["GP_Amount__c"]);
            }
            component.set("v.totalOfBillingMilestones", totalOfBillingMilestones);
        }
    },
    updateCurrencyOnAllRecords: function(component, listOfBillingMilestone, currencyISOCode) {
        for (var i = 0; i < listOfBillingMilestone.length; i += 1) {
            listOfBillingMilestone[i]["CurrencyISOCode"] = currencyISOCode;
        }
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    checkDateRangeForITOTypeProject: function(projectStartDate, projectEndDate, mileStoneStartDate, mileStoneEndDate) {
        return (!((mileStoneStartDate.toLocaleString() < projectEndDate &&
                mileStoneStartDate.toLocaleString() >= projectStartDate) &&
            (mileStoneEndDate.toLocaleString() <= projectEndDate &&
                mileStoneEndDate.toLocaleString() > projectStartDate) &&
            (mileStoneStartDate.toLocaleString() < mileStoneEndDate.toLocaleString())));
    },
    processMassList: function(component, listOfMassBillingMilestone) {
        var getProcessedCSVRecordsService = component.get("c.getProcessedCSVRecordsService");
        getProcessedCSVRecordsService.setParams({
            "listOfCSVRecordsUploaded": JSON.stringify(listOfMassBillingMilestone)
        });
        this.showSpinner(component);
        getProcessedCSVRecordsService.setCallback(this, function(response) {
            this.getProcessedCSVRecordsServiceHandler(response, component);
        });

        $A.enqueueAction(getProcessedCSVRecordsService);
    },
    getProcessedCSVRecordsServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            var processedRecords = JSON.parse(responseData.response);
            this.mergeBillingMilestoneLists(component, processedRecords);
        } else {
            this.handleFailedCallback(component, responseData);
        }
    },
    mergeBillingMilestoneLists: function(component, processedRecords) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var mapOfWorkLocation = component.get("v.mapOfWorkLocation");
        var projectTCVAmount = component.get("v.projectTCVAmount");
        var projectId = component.get("v.recordId");

        for (var index = 0; index < processedRecords.length; index++) {
            processedRecords[index]["isEditable"] = true;
            processedRecords[index]["GP_Project__c"] = projectId;
            if (mapOfWorkLocation && mapOfWorkLocation[processedRecords[index]["GP_Work_Location__c"]])
                processedRecords[index]["GP_Work_Location__c"] = mapOfWorkLocation[processedRecords[index]["GP_Work_Location__c"]];
            else
                processedRecords[index]["GP_Work_Location__c"] = null;
            var amount = processedRecords[index]["GP_Value__c"];
            if (processedRecords[index]["GP_Entry_type__c"] == "Percent")
                amount = ((processedRecords[index]["GP_Value__c"] * projectTCVAmount) / 100).toFixed(2)
            processedRecords[index]["GP_Amount__c"] = amount;
            listOfBillingMilestone.push(processedRecords[index]);
        }
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    setObjectApiNamesToColumn: function(listOfMassBillingMilestone) {
        var parsedListOfBillingMileStone = [];

        for (var index = 0; index < listOfMassBillingMilestone.length; index++) {
            var parsedBillingMilestoneRecord = {};
            var csvRecord = listOfMassBillingMilestone[index];
            if(csvRecord.Name != null){
            for (var key in Object.keys(csvRecord)) {
                var csvFieldLabel = Object.keys(csvRecord)[key];
                var fieldApiName = this.LABEL_FIELD_TO_APINAME_MAP[csvFieldLabel.toLowerCase()];
                var value = csvRecord[csvFieldLabel];
                if (fieldApiName === 'GP_Value__c' && value !=null)
                    value = value.replace(/,/g, '');
                parsedBillingMilestoneRecord[fieldApiName] = value;
            }
            parsedBillingMilestoneRecord["isSelected"] = true;
            parsedListOfBillingMileStone.push(parsedBillingMilestoneRecord);
            }
        }
        listOfMassBillingMilestone = parsedListOfBillingMileStone;
        return listOfMassBillingMilestone;
    },
    deleteInBulk: function(component, recordIdsToDelete) {
        var deleteBillingMilestoneBulkService = component.get("c.deleteBillingMilestoneBulk");
        deleteBillingMilestoneBulkService.setParams({
            "billingMilestoneIds": recordIdsToDelete
        });
        this.showSpinner(component);
        deleteBillingMilestoneBulkService.setCallback(this, function(response) {
            this.deleteBillingMilestoneBulkServiceHandler(response, component);
        });
        $A.enqueueAction(deleteBillingMilestoneBulkService);

    },
    deleteBillingMilestoneBulkServiceHandler: function(response, component) {
        var responseData = response.getReturnValue() || {};
        this.hideSpinner(component);

        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            this.showToast('SUCCESS', 'success', 'Billing milestone deleted successfully.');
        } else {
            this.handleFailedCallback(component, responseData);
        }
    }
})