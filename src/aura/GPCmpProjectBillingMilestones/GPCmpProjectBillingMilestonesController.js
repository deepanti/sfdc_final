({
    doInit: function(component, event, helper) {
        component.set("v.dateToday", new Date());
        helper.getBillingMilestones(component);
    },
    //Add row to table
    addToBillingMilestone: function(component, event, helper) {
        helper.addToBillingMilestone(component);
    },
    //Remove single record from table
    removeBillingMilestone: function(component, event, helper) {

        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        if (listOfBillingMilestone &&
            listOfBillingMilestone.length > 0 &&
            !confirm("Are you sure you want to delete?")) {
            return;
        }

        var billingMilestoneIndex = Number(event.currentTarget.id);

        //if (helper.isDeletable(component, billingMilestoneIndex, listOfBillingMilestone)) {
            helper.removeBillingMilestone(component, billingMilestoneIndex);
            helper.updateBillingMilestoneProgressStatus(component);
        //}
    },
    //Toggle IsEditable property of row
    toggelIsEditableForBillingMilestoneRow: function(component, event, helper) {

        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var billingMilestoneIndex = Number(event.currentTarget.id);
        var isEditable = listOfBillingMilestone[billingMilestoneIndex]["isEditable"];

        listOfBillingMilestone[billingMilestoneIndex]["isEditable"] = !isEditable;
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    //Save all the records
    saveBillingMilestone: function(component, event, helper) {
        component.set("v.showError", false);
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");

        if (!listOfBillingMilestone || listOfBillingMilestone.length === 0)
            return;

        if (helper.validateProjectMileStone(component, listOfBillingMilestone)) {
            if (helper.validateInput(component)) {
                helper.saveBillingMilestone(listOfBillingMilestone, component);
                helper.setMilestoneIsEditableToFalse(component, listOfBillingMilestone);
            } else
                helper.showErrorBlock(component);
        }
    },
    //Entry Type value change
    changeEntryTypeOrValue: function(component, event, helper) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var billingMilestoneIndex = Number(event.currentTarget.id);
    },
    //Change billing type
    entryTypeChangeHandler: function(component, event, helper) {
        var label = event.getSource().get("v.label");
        var splitted = label.split('@@');
        var billingMilestoneIndex = Number(splitted[1]);
        var billingMilestoneField = splitted[0];
        var value = event.getSource().get("v.value");

        helper.setAmount(component, billingMilestoneIndex, billingMilestoneField, value);
    },
    //Change value of billing milestonealueChangeHandler
    valueChangeHandler: function(component, event, helper) {
        var billingMilestoneIndex = event.target.getAttribute("data-budget-index");
        var billingMilestoneField = event.target.getAttribute("data-attribute-name");
        var value = event.currentTarget.value;

        helper.setAmount(component, billingMilestoneIndex, billingMilestoneField, value);
    },
    //Mass upload billing milestone list change
    onChangelistOfProjectBillingMilestoneService: function(component, event, helper) {
        helper.onChangelistOfProjectBillingMilestone(component);
    },
    //Open Mass upload popup
    extendToAnotherComponent: function(component, event, helper) {
        component.set("v.openMassUpload", true);
    },
    //Append Mass Upload List to screen List
    onChangelistOfMassProjectBillingMilestoneService: function(component, event, helper) {
        var listOfMassBillingMilestone = component.get("v.listOfMassBillingMilestone");
        listOfMassBillingMilestone = helper.setObjectApiNamesToColumn(listOfMassBillingMilestone);

        helper.mergeBillingMilestoneLists(component, listOfMassBillingMilestone);
    },
    //Check/Uncheck all records
    selectAllRecords: function(component, event, helper) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var selectAll = component.get("v.selectAll");
        var isUpdatable = component.get("v.isUpdatable");

        for (var key in listOfBillingMilestone) {
            if (listOfBillingMilestone[key]["isEditable"] && !(isUpdatable && listOfBillingMilestone[key].GP_Parent_Billing_Milestone__c &&
                    listOfBillingMilestone[key].GP_Billing_Status__c != 'Not Billed'))
                listOfBillingMilestone[key]["isSelected"] = selectAll;
        }

        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    //Remove records in bulk(Selected)
    removeSelectedRecords: function(component, event, helper) {

        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        var selected = false;
        for (var key = 0; key <= listOfBillingMilestone.length - 1; key++) {
            if (listOfBillingMilestone[key]["isSelected"]) {
                selected = true;
                break;
            }
        }

        if (listOfBillingMilestone &&
            listOfBillingMilestone.length > 0 && selected &&
            !confirm("Are you sure you want to delete?")) {
            return;
        }

        var modifiedListOfBillingMilestones = [];
        modifiedListOfBillingMilestones = JSON.parse(JSON.stringify(listOfBillingMilestone));
        var amountToBeReduced = 0;
        var recordIdsToDelete = [];
        var projectTCVAmount = component.get("v.projectTCVAmount");
        var totalOfBillingMilestones = component.get("v.totalOfBillingMilestones");

        for (var key = listOfBillingMilestone.length - 1; key >= 0; key--) {
            if (listOfBillingMilestone[key]["isSelected"]){ //&& helper.isDeletable(component, key, listOfBillingMilestone)) {

                if (listOfBillingMilestone[key]["Id"])
                    recordIdsToDelete.push(listOfBillingMilestone[key]["Id"]);

                //amountToBeReduced += Number(listOfBillingMilestone[key]["GP_Amount__c"]);
                //if(!listOfBillingMilestone[key]["GP_Amount__c"])
                //  modifiedListOfBillingMilestones.splice(key, 1);
                //else
                listOfBillingMilestone.splice(key, 1);
            }
        }
        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
        // component.set("v.listOfBillingMilestone", modifiedListOfBillingMilestones);

        // if(projectTCVAmount != totalOfBillingMilestones - amountToBeReduced){
        //     component.set("v.showError", true);
        //     component.set("v.errorString", "Sum of Billing Milestones Amount should be equal to " + projectTCVAmount);
        //     return;
        // }else{
        //component.set("v.showError", false);
        //component.set("v.errorString",  "");
        //component.set("v.listOfBillingMilestone", listOfBillingMilestone);
        //}

        if (recordIdsToDelete.length != 0)
            helper.deleteInBulk(component, recordIdsToDelete);
        if (listOfBillingMilestone.length == 0)
            component.set("v.selectAll", false);
    },
    //Edit all rows of table
    editAllRows: function(component, event, helper) {
        var listOfBillingMilestone = component.get("v.listOfBillingMilestone");
        if (!listOfBillingMilestone || listOfBillingMilestone.length === 0) {
            return;
        }
        for (var i = 0; i < listOfBillingMilestone.length; i += 1) {
            if (listOfBillingMilestone[i]["GP_Billing_Status__c"] == 'Not Billed')
                listOfBillingMilestone[i]["isEditable"] = true;
        }

        component.set("v.listOfBillingMilestone", listOfBillingMilestone);
    },
    downloadAllocation:function(component, event, helper){
        
        var baseURL = '#/sObject/00O90000009eNnG/view?fv0='+component.get("v.recordId"); //= $A.get("$Label.c.GP_PE_PRF_VF_PAGE_BASE_URL") ;
       	var eUrl= $A.get("e.force:navigateToURL");
        eUrl.setParams({
          "url":baseURL
        });
        eUrl.fire(); 
        },
    toggleSidebar: function(component, event, helper) {
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    }
})