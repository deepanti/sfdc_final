({
    doInit: function(component, event, helper) {
        component.set("v.isLoading", false);
    },
    changeOfMasterCheckbox: function(component, event, helper) {
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        var masterCheckBox = component.get("v.masterCheckBox");
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            lstOfIncentiveRecordWrapper[index].IsSelected = masterCheckBox;
        }
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
    },
    closeDiscModel:function(component, event, helper) {
        component.set("v.isOpenDiscPopup", false);
    },
    selectedIncentiveRecordChange:function(component, event, helper) {
        var lstOfIncentiveRecordWrapper = component.get("v.lstOfIncentiveRecordWrapper") || [];
        var selectedIncentiveRecordIndex = component.get("v.selectedIncentiveRecordIndex");
        var isMobileView = component.get("v.isMobileView");
        for (var index = 0; index < lstOfIncentiveRecordWrapper.length; index++) {
            if(selectedIncentiveRecordIndex == index && isMobileView)
            	lstOfIncentiveRecordWrapper[index].className = 'slds-mobile-select';
            else
                lstOfIncentiveRecordWrapper[index].className = '';
            
            /*if(selectedIncentiveRecordIndex == index && !isMobileView && lstOfIncentiveRecordWrapper[index].IsOnHold)
            	lstOfIncentiveRecordWrapper[index].IsOnHold = false;
            else if(selectedIncentiveRecordIndex == index && !isMobileView && lstOfIncentiveRecordWrapper[index].IsRejected)
            	lstOfIncentiveRecordWrapper[index].IsOnHold = false;*/
            
        }
        component.set("v.lstOfIncentiveRecordWrapper", lstOfIncentiveRecordWrapper);
    }

})