({
    ERROR_HEADER: '<h1>We have discovered that the project has some validation error, you need to fix these errors before submitting for approval.</h1>',
    SUCCESS_HEADER: '<h1>All the records are valid and project is available for approval.</h1>',
	getFormattedError: function(component) {
        var isValid = false;
        var serializedMapOfError = component.get("v.serializedMapOfError");
        var errorLogger;

        if(!serializedMapOfError) {
            return;
        }
        
		var mapOfSectionToSubSectionNameToListOfErrors;
        try {
            errorLogger = JSON.parse(serializedMapOfError);
        } catch(exception) {
            return serializedMapOfError;
        }
        mapOfSectionToSubSectionNameToListOfErrors  = errorLogger.mapOfSectionToSubSectionNameToListOfErrors;

        var activeSectionName = component.get("v.activeSectionName");
        var activeSubSectionName = component.get("v.activeSubSectionName");

        var mapOfSubSectionNameToListOfError = {};
        var formatedErrorMessage = '<span class="approvalLog"> ';

        if(errorLogger.isValid) {
            formatedErrorMessage += this.SUCCESS_HEADER;
            return formatedErrorMessage;
        }
        
        formatedErrorMessage += this.ERROR_HEADER;
                
        for(var sectionName in mapOfSectionToSubSectionNameToListOfErrors) {
            if(activeSectionName && activeSectionName != sectionName) {
                continue;
            }
            mapOfSubSectionNameToListOfError = mapOfSectionToSubSectionNameToListOfErrors[sectionName];
            formatedErrorMessage += '<h2>' + sectionName +'</h2>';
            for(var subSectionName in mapOfSubSectionNameToListOfError) {
                if(activeSubSectionName && activeSubSectionName != subSectionName) {
                    continue;
                }
                formatedErrorMessage += '<h3>' + subSectionName +'</h3>';
                formatedErrorMessage += '<ol>';
                var listOfErrors = mapOfSubSectionNameToListOfError[subSectionName] || [];

                listOfErrors.forEach(function(error){
                    formatedErrorMessage += '<li>';
                    formatedErrorMessage += error;
                    formatedErrorMessage += '</li>';
                });
                
                formatedErrorMessage += '</ol>';
            }
        }
        formatedErrorMessage += '</span>';
        
        return formatedErrorMessage;
	}
})