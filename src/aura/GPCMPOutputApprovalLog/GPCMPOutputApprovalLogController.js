({
	updateFormattedError: function(component, event, helper) {
		var formatedErrorMessage = helper.getFormattedError(component);
        component.set("v.formattedErrorLog", formatedErrorMessage);
	}
})