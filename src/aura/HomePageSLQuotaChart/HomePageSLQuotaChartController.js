({
	doInit : function(component, event, helper) {
		var action = component.get("c.fetchChartDataWrapper");
		action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var result = response.getReturnValue();
                //console.log(result);
                component.set("v.title",result.chartTitle);
                component.set("v.data",result.dataWrapper);
                component.set("v.leftSubtitle",'View Report');                
                var month_names =["Jan","Feb","Mar",
                      			  "Apr","May","Jun",
                      			  "Jul","Aug","Sep",
                      			  "Oct","Nov","Dec"];
                var today = new Date();
                var label = today.getDate()+' '+month_names[today.getMonth()]+' '+today.getFullYear();
                component.set("v.rightSubtitle",label);
                helper.stackedBarGraph(component, event, helper);
            }
            else
            {
               console.log("Unknown error");
            }
        });
        $A.enqueueAction(action);        
	}
})