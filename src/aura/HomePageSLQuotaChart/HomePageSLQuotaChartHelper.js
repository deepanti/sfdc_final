({
	stackedBarGraph : function(component,event, helper) 
    {
        var data = component.get("v.data");
        console.log(data);
        var key = [ "TCV","Quota"];
        helper.drawChart(component,data,key);            
    },
    drawChart : function(component,data,key) 
    {
        var me = this,
            domEle = 'stacked-bar',
            stackKey = key,
            data = data,
            margin = {top: 20, right: 20, bottom: 30, left: 80},
            width = 480 - margin.left - margin.right,
            height = 300 - margin.top - margin.bottom,
            xScale = d3.scaleLinear().rangeRound([0, width]),
            yScale = d3.scaleBand().rangeRound([height, 0]).padding(0.5),            
            xAxis = d3.axisBottom(xScale).ticks(5).tickFormat(d3.format(".2s")),
            yAxis =  d3.axisLeft(yScale),
            svg = d3.select(component.find('chart').getElement()).append("svg")
        .attr("width", 400)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
        //'#76ded9'
        var color = d3.scaleOrdinal().range(['#00a4e3', '#16325c']);
        
        var stack = d3.stack()
        .keys(stackKey)
        /*.order(d3.stackOrder)*/
        .offset(d3.stackOffsetNone);
        
        var layers= stack(data);
        data.sort(function(a, b) { return b.total - a.total; });
        yScale.domain(data.map(function(d) { return d.name; }));
        xScale.domain([0, d3.max(layers[layers.length - 1], function(d) { return  d[0] + d[1]; }) ]).nice();
        
        var layer = svg.selectAll(".layer")
        .data(layers)
        .enter().append("g")
        .attr("class", "layer")
        .style("fill", 
                function(d) { return color(d.key); }
            );
        
        layer.selectAll("rect")
        .data(function(d) { return d; })
        .enter().append("rect")
        .attr("y", function(d) { return yScale(d.data.name); })
        .attr("x", function(d) { return xScale(d[0]); })
        .attr("height", yScale.bandwidth())
        .attr("width", function(d) { return xScale(d[1]) - xScale(d[0]) });
        
        
        svg.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + (height+5) + ")")
        .call(xAxis);
        
        svg.append("g")
        .attr("class", "axis axis--y")
        .attr("transform", "translate(0,0)")
        .call(yAxis)
        .selectAll("text")	
        .style("text-anchor", "end")
        .attr("dx", "1.5em")
        .attr("dy", "-2em")
        .attr("transform", "rotate(-35)");
        
        
        
        var legend = svg.append("g")
        .attr("font-family", "sans-serif")
        .attr("font-size", 10)
        .attr("text-anchor", "start")
        .selectAll("g")
        .data(key.slice().reverse())
        .enter().append("g")
        //.attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });
        .attr("transform", function(d, i) { return "translate(-50," + (200 + i * 20) + ")"; });
        
        legend.append("rect")
        .attr("x", width - 60)
        
        .attr("width", 19)
        .attr("height", 19)
        .attr("fill", color);
        
        legend.append("text")
        .attr("x", width - 40)
        .attr("y", 10)
        .attr("dy", "0.32em")
        .text(function(d) { return d; }); 
    }
})