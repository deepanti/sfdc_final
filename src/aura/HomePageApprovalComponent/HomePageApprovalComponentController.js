({
	init: function (cmp, event, helper) {
      
        var userType = cmp.get("v.UserType");
         
        if(userType == 'SalesLeader')
        {
            helper.fetchData(cmp,event,helper);
        }
        else if(userType == 'SelesRep')
        {
            helper.fetchQSRMPendingOpportunity(cmp,event,helper);
        }
        
    },
    /******************************************************
    	Event Handler Method which fetches data from which 
        Child component event is called
    ********************************************************/
    handleApprovalEvent : function (cmp, event, helper) {
        var recId = event.getParam("approvalRecordId");
        var action = event.getParam("actionToPerform");
        var OppName = event.getParam("OpportunityName");
        var QsrmName = event.getParam("QsrmName");
         
        if(!$A.util.isUndefinedOrNull(recId))
        {
            cmp.set("v.recId",recId);
        }
        if(!$A.util.isUndefinedOrNull(action))
        {  
             cmp.set("v.action",action);  
        }
         if(!$A.util.isUndefinedOrNull(OppName))
        {
            cmp.set("v.OpportunityName",OppName);
        }
        if(!$A.util.isUndefinedOrNull(QsrmName))
        {  
             cmp.set("v.QsrmName",QsrmName);  
        }
        helper.openModal(cmp, event, helper);
    }, 
    closeModal : function(cmp,event,helper){
        cmp.set("v.modal",false);
        cmp.set("v.comments",'');
    },
    /******************************************************
    	Method is executed whech user either approve or reject
        any item in the list
    ********************************************************/
    approveRecord : function(cmp,event,helper){
        
		var comments = cmp.get("v.comments");
        var comm = cmp.find("comments");
        if($A.util.isUndefinedOrNull(comments) || comments == '')
        {
            $A.util.addClass(comm,'slds-has-error');
            helper.showToast("error", "Please Provide Appropriate Comments.");
        }
        else
        {
            $A.util.removeClass(comm,'slds-has-error');
            cmp.set("v.modal",false);
            cmp.set("v.loaded",true);
         	helper.pendingApprovalRecords(cmp,event,helper);   
        }
    },
    ReworkQsrm : function(cmp,event,helper){
        alert('rework resubmit');
    }
})