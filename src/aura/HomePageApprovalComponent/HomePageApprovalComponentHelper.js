({
	/******************************************************
    	doinit Method which fetch data while loading component
    ********************************************************/
	fetchData: function (cmp, event,helper) {        
        var action = cmp.get("c.fetchOpportunityPendingForApporval");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                cmp.set('v.opplist',result);
                cmp.set('v.count',result.length);
                //alert('approval result '+result.length);
            }
            else{                
                helper.showToast("error", "Unable to fetch the list. Contact System admin.");
            }
        });
        $A.enqueueAction(action);
    },
    
    showToast : function(type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": type + "!",
            "type": type,
            "message": message
            
        });
        toastEvent.fire();
    },
    openModal : function(cmp,event,helper){
        cmp.set("v.modal",true);
    },
    /******************************************************
    	this method does a backend call to server for 
        approving or rejecting any approval record 
    ********************************************************/
    pendingApprovalRecords : function(cmp,event,helper){        
        var action = cmp.get("c.approvePendingItems");
        var succesMsg = '';
        var approvedAction = cmp.get("v.action");
         var successMsg ='';
            if(approvedAction =='Approve'){
                successMsg ='QSRM is approved successfully.';
            }else if(approvedAction =='Reject'){
                successMsg ='QSRM is rejected and the opportunity will be marked as Dropped.';
            }else if(approvedAction =='Rework'){
                successMsg ='Rework request has been submitted to Opportunity owner successfully.';
            }
        action.setParams({
            approvalPendingItemId : cmp.get("v.recId"),
            comments : cmp.get("v.comments"),
            action : cmp.get("v.action")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	var result = response.getReturnValue();                
                if(result)
                {
                    var oppLst = cmp.get("v.opplist");
                    var approvalId = cmp.get("v.recId");
                    var count = 0;                    
                    for(var i=0;i<oppLst.length;i++)
                    {
                        if(oppLst[i].approvalItemId == approvalId)
                        {    
                            count = i;
                        }
                    }                    
                    oppLst.splice(count,1);
                    cmp.set("v.opplist",oppLst);
                    cmp.set("v.count",oppLst.length);
                    cmp.set("v.loaded",false);
                    cmp.set("v.comments",'');//fix to reset comments
                    helper.showToast("Success", successMsg);
                }
                else
                {
                    helper.showToast("error", "Unable to Approve the Opportunity. Contact System admin.");
                    cmp.set("v.loaded",false);
                }
            }
            else{
                helper.showToast("error", "Unable to Approve the Opportunity. Contact System admin.");
                cmp.set("v.loaded",false);
            }
        });
        $A.enqueueAction(action);
    },
    /******************************************************************************************
        Seller Home fetch Opportunity pending for QSRM approval
    ******************************************************************************************/
    
    fetchQSRMPendingOpportunity : function(cmp,event,helper){
        var action = cmp.get("c.fetchOpportunityPendingForQSRMApproval");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
            	var result = response.getReturnValue();
                cmp.set('v.opplist',result);
                cmp.set('v.count',result.length);
                var oppLst = cmp.get("v.opplist");
                for(var i=0;i<oppLst.length;i++) {
                    if(oppLst[i].status == 'Rework'){
                       cmp.set("v.ReworkAction",true);
                        break;
                    } 
                } 
                
            } else{
                helper.showToast("error", "Unable to fetch the list. Contact System admin.");
            }
        });
        $A.enqueueAction(action);
    }
})