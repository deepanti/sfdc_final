({
    fireToastSuccess : function(message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
    //update stage to prediscover 
    handleConfirmationForPrediscover:function(component,event,helper,oppId){
        var action = component.get("c.updateStageToPrediscover");
        action.setParams(
            {
                OppId:oppId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.fireToastSuccess("Stage changed to prediscover"); 
                var compEvent = component.getEvent("RefreshEvent");
                compEvent.fire();
            }
            else
            {
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },
    raiseAnException:function(component,event,helper,oppId){
        var action = component.get("c.raiseAnExceptionTS");
        component.set("v.spinner",true);
         			
        action.setParams(
            {
                oppID:oppId,
                count:component.get("v.exceptionDays"),
                comments:component.get("v.comments")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                if(response.getReturnValue()==false)
                {
                    component.set("v.spinner",false);
                    helper.fireToastSuccess("Exception has been raised for the opportunity"); 
                    component.set("v.comments","");
                    component.set("v.exceptionDays","");
                    //component.set("v.opportunityID","");
                    var compEvent = component.getEvent("RefreshEvent");
                    compEvent.fire();
                    component.set("v.isVisible",false);
                }
                else
                {
                    component.set("v.spinner",false);
                    component.set("v.comments","");
                    component.set("v.exceptionDays","");
                    //component.set("v.opportunityID","");
                    helper.fireToastError('You have exceeded the allowed number of exception. Please update the Stage of the opportunity'); 
                }
                
            }
            else
            {
                component.set("v.spinner",false);
                  component.set("v.comments","");
                    component.set("v.exceptionDays","");
                   // component.set("v.opportunityID","");
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },
    //Raising an Exception for Non Ts MSA/Sow closure date criteria 
    raiseAnExceptionClosureDate:function(component,event,helper,oppId){
        var action = component.get("c.raiseAnExceptionNONTS");
        component.set("v.spinner",true);
        action.setParams(
            {
                oppID:oppId,
                comments:component.get("v.comments")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.spinner",false);
                component.set("v.comments","");
                //component.set("v.opportunityID","");
                component.set("v.isVisibleExceptionNonTS",false);
                helper.fireToastSuccess("Exception has been raised for the opportunity"); 
                var compEvent = component.getEvent("RefreshEvent");
                compEvent.fire();
            }
            else
            {
                component.set("v.spinner",false);
                component.set("v.comments","");
                component.set("v.isVisibleExceptionNonTS",false);
                //component.set("v.opportunityID","");
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    },
    validationTs:function(component, event, helper) { 
        var allValid = component.find('validateTS').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        return allValid; 
    },
    validationNonTs:function(component, event, helper) { 
  		var temp=[];
        temp.push(component.find('validateNonTS'));
        var allValid = temp.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);
        
        return allValid; 
    }
    
})