({
    //function which takes ageing actions  
    handleStageChange:function(component, event, helper) {
        var oppLst = component.get("v.oppList");  
        var oppId = oppLst[event.getSource().get("v.name")].opp.Id;
        var stageName=oppLst[event.getSource().get("v.name")].opp.StageName;
        //move to prediscover only available for Ts Discover Stage 
        if(event.getParam("value")=='Prediscover')
        {
            if(confirm('This opportunity will be moved to Prediscover Stage. Are you sure?'))
                helper.handleConfirmationForPrediscover(component, event, helper,oppId);
        }
        else
            if(event.getParam("value")=='Exception')
            {
                
                component.set("v.isVisible",true);
                component.set("v.opportunityID",oppId);
                component.set("v.StageName",stageName);
            }
        //for Drop a deal and change stage
            else
            {
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": oppId
                });
                navEvt.fire();            
            }
    },
    closeModal : function(component,event,helper)
    {
        component.set("v.showConfirmAction",false);
        component.find("ConfirmationModal").set("v.recordId",null);
    },
    closeExceptionModal:function(component,event,helper)
    {
        component.set("v.isVisible",false);
        component.set("v.opportunityID","");
        component.set("v.comments","");
        component.set("v.exceptionDays","");
    },
    //function to update msa sow closure date 
    dateChange: function(component, event, helper)
    {
        var oppLst = component.get("v.oppList");
        var action = component.get("c.updateClosureDate");
        // for non ts msa/sow closure date 
        if((Date.parse(oppLst[event.target.id].opp.CloseDate)<=Date.parse(oppLst[event.target.id].datePro)) && oppLst[event.target.id].isNonTs)
            helper.fireToastError('MSA/SOW closure Date cannot be earlier than Proposed MSA/SOW closure date ' +'('+oppLst[event.target.id].datePro+'). For more queries please contact SFDCHelpdesk.');
		else if(Date.parse(oppLst[event.target.id].opp.CloseDate)<=new Date()) //for Ts Msa/Sow Closure date
            helper.fireToastError('The date selected must be later than today');
        else
        {
            action.setParams(
                {
                    closedDate :oppLst[event.target.id].opp.CloseDate,
                    oppID:oppLst[event.target.id].opp.Id
                });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var compEvent = component.getEvent("RefreshEvent");
                    compEvent.setParams({"isRefresh" :true});
                    compEvent.fire();
                    helper.fireToastSuccess("MSA/SOW Closure date is updated"); 
                }
                else
                {
                    helper.fireToastError(response.getError()[0].message); 
                }
            });
            
            $A.enqueueAction(action);
        }
    },
    openOpportunity : function(component, event, helper)
    {
        
        var oppLst = component.get("v.oppList");
        var navEvt = $A.get("e.force:navigateToSObject");
        
        navEvt.setParams({
            "recordId": oppLst[event.getSource().get("v.label")].opp.Id
        });
        navEvt.fire();
    },
    //raising an exception for deal ageing for TS deals only
    raiseAnException:function(component, event, helper)
    {
        
        var allValid=helper.validationTs(component, event, helper);
        if(allValid)
        {
            var oppId=component.get("v.opportunityID");
            helper.raiseAnException(component, event, helper,oppId);
        }
        else
        {
            helper.fireToastError('Required fields are missing'); 
        }
    },
    //Raising an Exception for Non TS Msa/Sow closure date criteria 
    raiseAnExceptionClosureDate:function(component,event,helper)
    {
        var allValid=helper.validationNonTs(component, event, helper);
        if(allValid)
        {
            var oppId=component.get("v.opportunityID");
            helper.raiseAnExceptionClosureDate(component,event,helper,oppId);
        }
        else
        {
            helper.fireToastError('Required fields are missing'); 
        }
    },
    raiseAnExceptionClosureDateOpenModal:function(component,event,helper)
    {
        var oppLst = component.get("v.oppList");
        var oppId=oppLst[event.target.id].opp.Id;
        component.set("v.isVisibleExceptionNonTS",true);
        component.set("v.opportunityID",oppId);
    },
    closeRaiseAnExceptionClosureDateModal:function(component,event,helper)
    {
        component.set("v.isVisibleExceptionNonTS",false);
        component.set("v.opportunityID","");
        component.set("v.comments","");
    }
})