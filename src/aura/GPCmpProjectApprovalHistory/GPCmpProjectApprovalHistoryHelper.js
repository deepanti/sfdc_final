({
	getProjectApprovalhistory : function(component) {
        var action = component.get("c.getApprovalsHistory");
        action.setParams({
            "objectId": component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            this.hideSpinner(component);
            var responseData = response.getReturnValue() || {};

            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            		
                var responseJson = JSON.parse(responseData.response);
                	component.set("v.aSW", responseJson);
                	debugger;
                	console.log('AH',responseJson )
            }
        });

        $A.enqueueAction(action);
	}
})