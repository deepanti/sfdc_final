({
	toggleOppProdItems : function(component, event, helper) {
		var isOppProdOpen = component.get('v.isOppProdOpen');
        console.log('isOppProdOpen: ', isOppProdOpen);
        component.set('v.isOppProdOpen', !isOppProdOpen);
	},
    showPopover : function(component, event, helper){
        component.set('v.isPopoverVisible', true);
    },
    hidePopover : function(component, event, helper){
        component.set('v.isPopoverVisible', false);
    }
})