({
    createObjectData: function(component, event) {
        // get the contactList from component and add(push) New Object to List 
       
        var RowItemList = component.get("v.serchFilterList");
        var filterMap = new Object(); // or var map = {};
		filterMap["fieldName"] ="";
        filterMap["operator"] ="";
        filterMap["value"] ="";

        RowItemList.push(filterMap); 
            
        // set the updated list to attribute (contactList) again    
        component.set("v.serchFilterList", RowItemList);
    },
    
    clearValues : function(component, event) {
        component.set("v.serchFilterList",[]);
        this.createObjectData(component, event);
    }
    
    
})