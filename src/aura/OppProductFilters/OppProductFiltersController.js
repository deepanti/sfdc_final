({
 
    doInit: function(component, event, helper) {
        
        helper.createObjectData(component, event);
        
    },
 
    // function for create new object Row in Contact List 
    addNewRow: function(component, event, helper) {        
        helper.createObjectData(component, event);
    },
 
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {        
        var index = event.getParam("indexVar");       
        var AllRowsList = component.get("v.serchFilterList");
        AllRowsList.splice(index, 1);        
        component.set("v.serchFilterList", AllRowsList);
    },
    
    clear : function(component, event, helper) {
        helper.clearValues(component, event);
    }
})