({
    doInit : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
    },
    
    hideSpinner : function(component, event, helper) {
        helper.hideSpinner(component, event, helper);
    },
    saveRecord  : function(component, event, helper) {
        helper.saveData(component, event, helper, false);
    },
    saveAndNext  : function(component, event, helper) {
        helper.saveData(component, event, helper, true);
    },
    back : function(component, event, helper) {
        var appEvent = $A.get("e.c:OpportunityTabsApplicationEvent");
        appEvent.setParams({"selectedTabId" : "16"});
        appEvent.fire();
        localStorage.setItem('LatestTabId',"16");
        window.scrollTo(0, 0);
    },
    onSuccess:function(component, event, helper){
        var appEvent = $A.get("e.c:SetSalesPath");
        appEvent.fire();
        helper.fireToastSuccess(component, event, helper,'Opportunity Saved Successfully');
        component.set("v.IsSpinner", false);
    },
    showError : function(component, event, helper) {
       localStorage.setItem('MaxTabId',17);
        var error = event.getParams();
        var errorMsg='';
        // top level error messages
        if(!$A.util.isUndefinedOrNull(error.output.errors))
        {
            error.output.errors.forEach(
                function(msg) { 
                    if(!$A.util.isUndefinedOrNull(msg.message))
                        errorMsg +=msg.message;
                }
            );
            
            // top level error messages
            Object.keys(error.output.fieldErrors).forEach(
                function(field) { 
                    error.output.fieldErrors[field].forEach(
                        function(msg) { 
                            if(!$A.util.isUndefinedOrNull(msg.message))
                                errorMsg +=msg.message + ', ';
                        }
                    )
                });
        }
        helper.fireToastError(component, event, helper, errorMsg);
        component.set("v.IsSpinner", false);
    },
})