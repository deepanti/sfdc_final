({
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    validateAll : function(component, event, helper) {
        var proceedToSave = true;
        var pricer = component.find("pricer");
        var pricerValue = pricer.get("v.value");
        if($A.util.isUndefinedOrNull(pricerValue) || pricerValue == '')
        {
            $A.util.addClass(pricer,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(pricer,'slds-has-error');
        }
        
        //Validating target Source Picklist
        var type = component.find("type");
        var typeValue = type.get("v.value");
        
        if($A.util.isUndefinedOrNull(typeValue) || typeValue == '')
        {
            
            $A.util.addClass(type,'slds-has-error');
            proceedToSave = false;
        }
        else
        {
            $A.util.removeClass(type,'slds-has-error');
        }
        return proceedToSave;
    },
    
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "error"  
        });  
        toastEvent.fire();  
    },
    saveData : function(component, event, helper, saveAndNext){ 
        //validating the inserted data
        event.preventDefault();
        var check = helper.validateAll(component, event, helper);
        if(check)
        {
            if(saveAndNext)
            {
                if(localStorage.getItem('MaxTabId')<18){
                    localStorage.setItem('MaxTabId',18);
                    component.set("v.MaxSelectedTabId", "18");
                    if(localStorage.getItem('StageName')=='4. Down Select')
                    {
                        component.set("v.StageName","5. Confirmed");
                        localStorage.setItem('StageName',"5. Confirmed");
                    }
                }
                component.find("recordViewForm").submit();
                localStorage.setItem('LatestTabId','18');
                var compEvents = component.getEvent("componentEventFired");
                compEvents.fire();
                window.scrollTo(0, 0);
            }
            else
            {
                if(localStorage.getItem('StageName')=='4. Down Select')
                {
                    component.set("v.StageName","5. Confirmed");
                    localStorage.setItem('StageName',"5. Confirmed");
                }
                component.find("recordViewForm").submit();
                localStorage.setItem('LatestTabId',17);
            }
        } 
        else {
            helper.fireToastError(component, event, helper, 'Required fields are missing');
        }
    }
})