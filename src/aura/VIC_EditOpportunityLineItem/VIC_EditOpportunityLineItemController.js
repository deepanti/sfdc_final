({
     Cancel:function(component,event,helper){
        var evt = $A.get("e.force:navigateToComponent");
		var recordIdOLI = component.get("v.recordIdOLI");
        evt.setParams({
            componentDef: "c:VIC_OpportunityLineItemViewComponent",
            componentAttributes: {
                recordId : recordIdOLI,
			}
            
        });
       
    evt.fire();
    },
    save:function(component,event,helper){
        var action = component.get("c.setOLI");
        alert(component.get("v.primarySplit")+'='+component.get("v.productBDUserSplit")+'='+component.get("v.user3Split")+'='+component.get("v.user4Split"));
        alert(component.get("v.selItem.val")+'='+component.get("v.selItem1.val")+'='+component.get("v.selItem2.val"));
        action.setParams({
            "primarySplit": component.get("v.primarySplit"),
            "productBDUserSplit": component.get("v.productBDUserSplit"),
            "user3Split": component.get("v.user3Split"),
            "user4Split": component.get("v.user4Split"),
            "userBDId": component.get("v.selItem.val"),
            "user3Id": component.get("v.selItem1.val"),
            "user4Id": component.get("v.selItem2.val"),
            "oliId": component.get("v.recordIdOLI"),
            "tcv1" : component.get("v.tcv1"),
            "ebit1": component.get("v.ebit1"),
            "tcv2" : component.get("v.tcv2"),
            "ebit2": component.get("v.ebit2"),
            "tcv3" : component.get("v.tcv3"),
            "ebit3": component.get("v.ebit3"),
            "tcv4" : component.get("v.tcv4"),
            "ebit4": component.get("v.ebit4"),
            "tcv5" : component.get("v.tcv5"),
            "ebit5": component.get("v.ebit5"),
            "tcv6" : component.get("v.tcv6"),
            "ebit6": component.get("v.ebit6"),
            "tcv7" : component.get("v.tcv7"),
            "ebit7": component.get("v.ebit7"),
            "tcv8" : component.get("v.tcv8"),
            "ebit8": component.get("v.ebit8"),
            "tcv9" : component.get("v.tcv9"),
            "ebit9": component.get("v.ebit9"),
            "tcv10" : component.get("v.tcv10"),
            "ebit10": component.get("v.ebit10")
            
        });
         action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('ok');
            }
        });
        $A.enqueueAction(action);
    },
	doInit:function(component,event,helper){
        var action = component.get("c.getOLI");
        action.setParams({
            "oliId": component.get("v.recordIdOLI")
        });
         action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opportunityid",response.getReturnValue().opportunityId);
                component.set("v.primaryUserId",response.getReturnValue().primaryUserIds);
                component.set("v.opportunityname",response.getReturnValue().opportunityName);
                component.set("v.primaryUser",response.getReturnValue().primaryUser);
                component.set("v.primarySplit",response.getReturnValue().primaryUserSplit);
               
                if(response.getReturnValue().productBDId != undefined && 
                    response.getReturnValue().productBDId != 'undefined' ){
                    var jsonUser = '{\"val\":\"'+response.getReturnValue().productBDId+'\",\"text\":\"'+response.getReturnValue().productBD+'\",\"objName\":\"User\"}';
                    component.set("v.selItem",JSON.parse(jsonUser));
                }
                component.set("v.productBDUserSplit",response.getReturnValue().productBDSplit);
                if(response.getReturnValue().user3Id != undefined && 
                    response.getReturnValue().user3Id != 'undefined' ){
                    var jsonUser = '{\"val\":\"'+response.getReturnValue().user3Id+'\",\"text\":\"'+response.getReturnValue().user3+'\",\"objName\":\"User\"}';
                    component.set("v.selItem1",JSON.parse(jsonUser));
                }
                component.set("v.user3Split",response.getReturnValue().user3Split);
                if(response.getReturnValue().user4Id != undefined && 
                    response.getReturnValue().user4Id != 'undefined' ){
                    var jsonUser = '{\"val\":\"'+response.getReturnValue().user4Id+'\",\"text\":\"'+response.getReturnValue().user4+'\",\"objName\":\"User\"}';
                    component.set("v.selItem2",JSON.parse(jsonUser));
                }
                component.set("v.user4Split",response.getReturnValue().user4Split);
                component.set("v.tcv1",response.getReturnValue().tcv1);
                component.set("v.ebit1",response.getReturnValue().ebit1);
                component.set("v.tcv2",response.getReturnValue().tcv2);
                component.set("v.ebit2",response.getReturnValue().ebit2);
                component.set("v.tcv3",response.getReturnValue().tcv3);
                component.set("v.ebit3",response.getReturnValue().ebit3);
                component.set("v.tcv4",response.getReturnValue().tcv4);
                component.set("v.ebit4",response.getReturnValue().ebit4);
                component.set("v.tcv5",response.getReturnValue().tcv5);
                component.set("v.ebit5",response.getReturnValue().ebit5);
                component.set("v.tcv6",response.getReturnValue().tcv6);
                component.set("v.ebit6",response.getReturnValue().ebit6);
                component.set("v.tcv7",response.getReturnValue().tcv7);
                component.set("v.ebit7",response.getReturnValue().ebit7);
                component.set("v.tcv8",response.getReturnValue().tcv8);
                component.set("v.ebit8",response.getReturnValue().ebit8);
                component.set("v.tcv9",response.getReturnValue().tcv9);
                component.set("v.ebit9",response.getReturnValue().ebit9);
                component.set("v.tcv10",response.getReturnValue().tcv10);
                component.set("v.ebit10",response.getReturnValue().ebit10);        
            }
        });
        $A.enqueueAction(action);
    }
})