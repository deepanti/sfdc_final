({
    doInit: function(component, event, helper) {
      
       var products=component.get("v.wrapper.opportunityDetails.OpportunityLineItems");
       var pro='';
     
        for(var i=0;i<products.length;i++)
        {
            
            if(i!=(products.length-1))
              pro=pro+products[i].PricebookEntry.Name+'; ';
            else
               pro=pro+products[i].PricebookEntry.Name;
        }
		component.set("v.product",pro);
    },
    deleteItem : function(component, event, helper) {
        var oppWrap=component.get("v.wrapper");
        var action = component.get("c.updateHideForRenewal");
        if(confirm('This opportunity will no longer be displayed on home page for renewal. Are you sure?'))
        {
            component.set("v.isVisible",false);
            action.setParams(
                {
                    oppid :oppWrap.opportunityDetails.Id
                });
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    helper.showToast("success", "This opportunity will no longer be displayed for renewal.");
                    var compEvent = component.getEvent("RefreshEvent");
                    compEvent.fire();
                }
                else
                {
                    helper.showToast("error", "Unable to update the preference. Contact System admin.");
                    component.set("v.isVisible",true);
                }
                
            });
            $A.enqueueAction(action);
        }
    },
    redirect: function(component, event, helper) 
    {
        component.set("v.spinner",true);
        var action = component.get("c.createDeepCloneOpportunity");
        action.setParams({
            'oppId' :component.get("v.wrapper.opportunityDetails.Id"),
            'isRenewable':true,
            'stage':'Prediscover'
            
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.spinner",false);
                console.log("Success in updation");
                var navEvt = $A.get("e.force:navigateToSObject");
                navEvt.setParams({
                    "recordId": response.getReturnValue()
                });
                navEvt.fire();
            }
            else
            {
                component.set("v.spinner",false);
                helper.showToast("error", "Unable to create renewal opportunity, contact system admin.");                
            }
            
        });
        $A.enqueueAction(action);
    },
    openOpportunity : function(component, event, helper)
    {
        
        helper.redirectToRecord(component.get("v.wrapper.opportunityDetails.Id"));
    },
    openAccount : function(component, event, helper)
    {
        helper.redirectToRecord(component.get("v.wrapper.opportunityDetails.Account.Id"));
    },
    openProduct : function(component, event, helper)
    {
        helper.redirectToRecord(event.getSource().get("v.label"));
    },
    openModal : function(component, event, helper) {
        component.set("v.showConfirmAction",true);
    },
    closeModal : function(component, event, helper) {
        component.set("v.showConfirmAction",false);
    },
})