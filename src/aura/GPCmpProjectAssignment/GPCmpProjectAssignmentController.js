({
	loadUserList: function(component, event, helper) {
		helper.getUserList(component);
	},
    assignProject: function(component, event, helper) {
    	helper.assignProjectToUser(component);
    },
    assignToCreator: function(component, event, helper) {
    	helper.assignProjectToOwner(component);
    }
})