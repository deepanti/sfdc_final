({
    getUserList: function(component) {
        var getUserlist = component.get("c.getUserlist");
        getUserlist.setParams({
            "strProjectID": component.get("v.recordId")
        });

        this.showSpinner(component);

        getUserlist.setCallback(this, function(response) {

            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() &&
                response.getState() === "SUCCESS" &&
                responseData.isSuccess &&
                responseData.message === 'AssignToCreator') {
                component.set("v.AssignToCreator", true);
            } else if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                var responseData = JSON.parse(responseData.response) || [];

                var listOfUsers = responseData.listOfUserRole;
                listOfUsers.unshift({
                    "GP_User__r": {
                        "Name": "--NONE--"
                    }
                });

                component.set("v.userList", listOfUsers);
                component.set("v.projectData", responseData.projectData);
                component.set("v.isAdminUser", responseData.isAdminUser);
                component.set("v.changeOwner", responseData.isAdminUser);

                // component.set("v.selectedUser", responseData.projectData.GP_Current_Working_User__c);
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });
        $A.enqueueAction(getUserlist);
    },
    assignProjectToUser: function(component) {
        var updateCurrentWorkingUser = component.get("c.updateCurrentWorkingUser");
        var selectedUserId = component.get("v.selectedUser");
        var selectedUserIdDOM = component.find("selectedUserId");
        var changeOwner = component.get("v.changeOwner");

        if ($A.util.isEmpty(selectedUserId) || selectedUserId === "None") {
            $A.util.removeClass(selectedUserIdDOM, 'slds-hide');
            $A.util.addClass(selectedUserIdDOM, 'errorText');
            return;
        } else {
            $A.util.addClass(selectedUserIdDOM, 'slds-hide');
        }

        updateCurrentWorkingUser.setParams({
            "strProjectId": component.get("v.recordId"),
            "strUserID": selectedUserId,
            "changeOwner": changeOwner
        });
        
        this.showSpinner(component);

        updateCurrentWorkingUser.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.showToast("Success", "success", "Project successfully assigned");
                component.set("v.isProjectAssignment", false);
                this.redirectToSobject(component.get("v.recordId"), "detail");
                // this.closeModal();
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(updateCurrentWorkingUser);
    },
    assignProjectToOwner: function(component) {
        var assignToOwner = component.get("c.assignToOwner");

        assignToOwner.setParams({
            "strProjectId": component.get("v.recordId"),
        });
        this.showSpinner(component);

        assignToOwner.setCallback(this, function(response) {
            var responseData = response.getReturnValue() || {};
            this.hideSpinner(component);
            if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
                this.showToast("Success", "success", "Project successfully assigned");
                this.redirectToListView("GP_Project__c");
            } else {
                this.handleFailedCallback(component, responseData);
            }
        });

        $A.enqueueAction(assignToOwner);
    },
    handleFailedCallback: function(component, responseData) {
        var errorMessage = responseData.message;
        component.set("v.errorMessage", errorMessage);
    }
})