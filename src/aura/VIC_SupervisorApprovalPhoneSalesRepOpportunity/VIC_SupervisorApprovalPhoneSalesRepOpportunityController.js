({
    
    reselectOpportunity : function(component, event, helper) {
        var selectedOppDataWrapLine = component.get("v.selectedOppDataWrapLine");
        if(selectedOppDataWrapLine != null && selectedOppDataWrapLine.objOpp.Id != event.currentTarget.id){	
            component.set("v.cardlength",0);
        }
        var userOppDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOpp in userOppDataWrap.lstOppDataWrap){
            if(isNaN(userOppDataWrap.lstOppDataWrap[eachOpp]) && 
               !$A.util.isEmpty(userOppDataWrap.lstOppDataWrap[eachOpp].lstOLIDataWrap) && 
                userOppDataWrap.lstOppDataWrap[eachOpp].objOpp.Id == event.currentTarget.id){
                component.set("v.selectedOppDataWrapLine",userOppDataWrap.lstOppDataWrap[eachOpp]);
                for(var eachOLI in userOppDataWrap.lstOppDataWrap[eachOpp].lstOLIDataWrap){
                    userOppDataWrap.lstOppDataWrap[eachOpp].lstOLIDataWrap[eachOLI].isOLIChecked = userOppDataWrap.lstOppDataWrap[eachOpp].isOppChecked;
                }
                component.set("v.lstOLIDataWrapLine",userOppDataWrap.lstOppDataWrap[eachOpp].lstOLIDataWrap);
            }
        }
        for(var eachOpp in userOppDataWrap.lstOppDataWrap){
            if(isNaN(userOppDataWrap.lstOppDataWrap[eachOpp])){
                if(userOppDataWrap.lstOppDataWrap[eachOpp].objOpp.Id == event.currentTarget.id){
                    userOppDataWrap.lstOppDataWrap[eachOpp].isOppSelect = true;
                }else{
                    userOppDataWrap.lstOppDataWrap[eachOpp].isOppSelect = false;
                }
            }
        }
        component.set("v.objUserDataWrapLine",userOppDataWrap);
	},
    selectNUnselectOppAll : function(component, event, helper){ 
        var oppCountForOli = 0;
        var oppCheckedCount = 0;
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            if(isNaN(objUserDataWrap.lstOppDataWrap[eachOppData]) && 
               	isNaN(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap)){
                if(!$A.util.isEmpty(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap) ){
                    oppCountForOli++;
                }
                if(!$A.util.isEmpty(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap) && 
                        objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked){
                    oppCheckedCount++;
                }
            }
        }
        if(oppCheckedCount != oppCountForOli){
            objUserDataWrap.isUserChecked = false;
        }else{
            objUserDataWrap.isUserChecked = true;
        }
        var lstOLIDataWrapLine = component.get("v.lstOLIDataWrapLine");
        var eachOppSelect = event.getSource().get("v.value");
        if(isNaN(lstOLIDataWrapLine)){
            for(var eachOLI in lstOLIDataWrapLine){
                lstOLIDataWrapLine[eachOLI].isOLIChecked = eachOppSelect;
            }
        }
        component.set("v.lstOLIDataWrapLine", lstOLIDataWrapLine);
        component.set("v.objUserDataWrapLine",objUserDataWrap);  
        
    },
    selectCheckBox : function(component, event, helper){ 
        var oppCheckCounterVal = 0;
        var objUserDataValueWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataValueWrap.lstOppDataWrap){
            if(objUserDataValueWrap.lstOppDataWrap[eachOppData].isOppChecked){
                oppCheckCounterVal++;
            }
        }
        if(objUserDataValueWrap.lstOppDataWrap.length == oppCheckCounterVal){
            objUserDataValueWrap.isUserChecked = true;
        }else{
            objUserDataValueWrap.isUserChecked = false;
        }
        component.set("v.objUserDataWrapLine",objUserDataValueWrap);  
    }
})