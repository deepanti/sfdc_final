({
	doInit : function(component, event, helper) {
		var salesRepId = component.get('v.salesRepId');
        //console.log('salesRepId: ', salesRepId);
        helper.initOppOliDataWrap(component,event,helper);
        component.set("v.showModal",false);
	},
    selectNUnselectAll : function(component, event, helper){ 
        var oppCount = 0;
        var statusOfSelectAll = event.getSource().get("v.value");
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            if(isNaN(objUserDataWrap.lstOppDataWrap[eachOppData])){
                objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked = statusOfSelectAll;
                for(var eachOLIData in objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked = statusOfSelectAll;
                }
            }
        }
        
        //var objOppDataWrapLine = component.get("v.objOppDataWrapLine");
        var lstOLIDataWrapLine = component.get("v.lstOLIDataWrapLine");
        
        component.set("v.objUserDataWrapLine",objUserDataWrap);
        component.set("v.lstOLIDataWrapLine",lstOLIDataWrapLine);
    },
    checkOliStatus : function(component, event, helper) {
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            if(isNaN(objUserDataWrap.lstOppDataWrap[eachOppData])){
                var oliCheckCount = 0;
                for(var eachOLIData in objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    if(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                        oliCheckCount++;
                    }
                }           
                if(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap.length == oliCheckCount){
                    objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked = true; 
                }else{
                    objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked = false; 
                }
            }
        }
        component.set("v.objUserDataWrapLine",objUserDataWrap);
        var oppSelectedCheckCount = 0;
        var oppCheckCount = 0;
        var objUserDataTEMPWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataTEMPWrap.lstOppDataWrap){
            if(isNaN(objUserDataTEMPWrap.lstOppDataWrap[eachOppData])){
                if(objUserDataTEMPWrap.lstOppDataWrap[eachOppData].isOppChecked && 
                   (!$A.util.isEmpty(objUserDataTEMPWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap)) ){
                    oppCheckCount++;
                }
                if(!$A.util.isEmpty(objUserDataTEMPWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap)){
                    oppSelectedCheckCount++;
                }
            }
        }
        if(oppSelectedCheckCount == oppCheckCount){
            objUserDataTEMPWrap.isUserChecked = true;
        }else{
            objUserDataTEMPWrap.isUserChecked = false;
        }
        component.set("v.objUserDataWrapLine",objUserDataTEMPWrap);
	},
    approve : function(component, event, helper){
        component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        helper.validateDataSelection(component,event,helper);
        var oppChecked = component.get("v.selectedRecCount");
        if(oppChecked == 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "type": "error",
                "message": "NO INCENTIVE CARD IS CHECKED TO APPROVE."
            });
            toastEvent.fire();
        }else{
            helper.approveHelper(component,event,helper); 
            helper.refreshPage(component,event,helper);
        }
        
	},
    onHold : function(component, event, helper) {
		component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        helper.validateDataSelection(component,event,helper);
        var oppChecked = component.get("v.selectedRecCount");
        var oliCommentCount = 0;
        var selectedOppDataWrapLine = component.get("v.selectedOppDataWrapLine");
        var objUserDataWrapLine = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrapLine.lstOppDataWrap){
            if(isNaN(objUserDataWrapLine.lstOppDataWrap[eachOppData])){
                for(var eachOLIData in objUserDataWrapLine.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                    if(objUserDataWrapLine.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked && 
                       (objUserDataWrapLine.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment == '' || 
                          objUserDataWrapLine.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].strComment == null)){
                        oliCommentCount++;
                    }
                }
            }
        }
        if(oppChecked == 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "type": "error",
                "message": "NO INCENTIVE CARD IS CHECKED TO QUERY."
            });
            toastEvent.fire();
        }else if(oliCommentCount != 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "type": "error",
                "message": "COMMENT IS REQUIRED TO QUERY."
            });
            toastEvent.fire();
        }else{
            helper.onHoldHelper(component,event,helper);
            helper.refreshPage(component,event,helper);
        }
        
	},
    reject : function(component, event, helper) {
        component.set("v.showModal", false);
        component.set("v.actionStatus", "");
        helper.validateDataSelection(component,event,helper);
        var oppChecked = component.get("v.selectedRecCount");
        if(oppChecked == 0){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "ERROR!",
                "type": "error",
                "message": "NO INCENTIVE CARD IS CHECKED TO REJECT."
            });
            toastEvent.fire();
        }else{
            helper.rejectHelper(component,event,helper);
            helper.refreshPage(component,event,helper);
        }
        
	},
    hideConfirmBox : function (component, event, helper) { 
         component.set("v.showModal", false);
         component.set("v.actionStatus", "");
    },
    
    approveConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","approve");
         component.set("v.showModal", true);
    },
    queryConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","query");
         component.set("v.showModal", true);
    },
    rejectConfirmBox : function (component, event, helper) { 
         component.set("v.actionStatus","reject");
         component.set("v.showModal", true);
    }
    
})