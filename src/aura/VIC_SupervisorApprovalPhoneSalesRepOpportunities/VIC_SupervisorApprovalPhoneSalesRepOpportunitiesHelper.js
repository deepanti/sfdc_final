({
	helperMethod : function() {
		
	},
    initOppOliDataWrap : function(component, event, helper) {
		var userDataWrap = component.get("v.objUserDataWrapLine");
        var oppDataWrap;        
        if(!$A.util.isUndefinedOrNull(userDataWrap.lstOppDataWrap)){ 
            for(var eachOppData in userDataWrap.lstOppDataWrap){
                if(!$A.util.isEmpty(userDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap)){
                    oppDataWrap = userDataWrap.lstOppDataWrap[eachOppData];
                    userDataWrap.lstOppDataWrap[eachOppData].isOppSelect = true;
                    break;
                }
            }
        }
        if(!$A.util.isUndefinedOrNull(userDataWrap.lstOppDataWrap)){ 
            for(var eachOppData in userDataWrap.lstOppDataWrap){
                if(!$A.util.isEmpty(userDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap)){
                    userDataWrap.lstOppDataWrap[eachOppData].isOppChecked = userDataWrap.isUserChecked;
                    for(var eachOLI in userDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                        userDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLI].isOLIChecked = userDataWrap.isUserChecked;
                    }
                }
            }
        }
        //alert(oppDataWrap);
        component.set("v.objOppDataWrapLine",oppDataWrap);
        component.set("v.selectedOppDataWrapLine",oppDataWrap);
        var lstOLI = [];
        for(var eachOLI in oppDataWrap.lstOLIDataWrap){
            lstOLI.push(oppDataWrap.lstOLIDataWrap[eachOLI]);
        }
        component.set("v.lstOLIDataWrapLine",oppDataWrap.lstOLIDataWrap);
	},
    approveHelper : function(component, event, helper) {
		var objUserOppWrapData = component.get("v.objUserDataWrapLine");
		var action = component.get("c.approvOLIIncentive");
        action.setParams({
            "strObjUserOppDataWrpa" : JSON.stringify(objUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "SUCCESS!",
                    "type": "success",
                    "message": "RECORD SUBMITTED."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Error."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    onHoldHelper : function(component, event, helper) {
		var objUserOppWrapData = component.get("v.objUserDataWrapLine");
		var action = component.get("c.pendingOLIIncentive");
        action.setParams({
            "strObjUserOppDataWrpa" : JSON.stringify(objUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "SUCCESS!",
                    "type": "success",
                    "message": "Case created successfully. Check the email to track case number."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Error."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    rejectHelper : function(component, event, helper) {
		var objUserOppWrapData = component.get("v.objUserDataWrapLine");
		var action = component.get("c.rejectdOLIIncentive");
        action.setParams({
            "strObjUserOppDataWrpa" : JSON.stringify(objUserOppWrapData)
        });
        action.setCallback(this,function(resp) {
        	var state = resp.getState();
            if(state === "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "SUCCESS!",
                    "type": "success",
                    "message": "RECORD SUBMITTED."
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Error!",
                    "type": "error",
                    "message": "Error."
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    validateDataSelection : function(component, event, helper) {
		var oppChecked = 0;
        var objUserDataWrap = component.get("v.objUserDataWrapLine");
        for(var eachOppData in objUserDataWrap.lstOppDataWrap){
            if(objUserDataWrap.lstOppDataWrap[eachOppData].isOppChecked){
                oppChecked++;
            }
            for(var eachOLIData in objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap){
                if(objUserDataWrap.lstOppDataWrap[eachOppData].lstOLIDataWrap[eachOLIData].isOLIChecked){
                	oppChecked++;
                }
            }
        }
        component.set("v.selectedRecCount",oppChecked);
	},
    refreshPage : function(component, event, helper) {
		var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:VIC_SupervisorApprovalPhone"
        });
        evt.fire();
	}
    
})