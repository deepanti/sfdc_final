({
     // Variable use to Calculate number of Total Opportunity
    opplistTS:0,
    opplistnonTS:0,
    // function to get TS opportunities 
	getOpportunityTS:function(component, event, helper)
    {
        var isSeller;
        var user=component.get("v.user");
        if(user=='Sales Rep')
        {
            isSeller=true;
        }
        else
            isSeller=false;
       
        var action = component.get("c.createOpportunityWrapperList");
        action.setParams(
            {
                'isSeller':isSeller,
                'TSservicesOnly':true,
                'approvalCriteriaInDays':component.get("v.approvalCriteriaInDays"),
                'contractapprovalCriteriaInDays':component.get("v.contractapprovalCriteriaInDays")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.opplistTS",response.getReturnValue());
                var opplist=component.get("v.opplistTS");
                 this.setTotalOpp(component,'opplistTS',component.get("v.opplistTS").length);
                console.log(opplist);
                var i;
                var newlst =[];
                // copying opportunities those need to be displayed on Home Screen Without clicking on View All button
                for (i = 0; i < component.get("v.pageSize") && i<opplist.length; i++) { 
                    newlst.push(opplist[i]);
                }
                component.set("v.userSelectedOppListTS",newlst);
               }     
            else
            {
                 helper.fireToastError(response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
	},
    // function to get IO(Non TS) opportunities 
    getOpportunityNonTS:function(component, event, helper)
    {
        var isSeller;
        var user=component.get("v.user");
        if(user=='Sales Rep')
        {
            isSeller=true;
        }
        else
            isSeller=false;
        
       
     	var action = component.get("c.createOpportunityWrapperList");
         action.setParams(
            {
                isSeller:isSeller,
                TSservicesOnly:false,
                approvalCriteriaInDays:component.get("v.approvalCriteriaInDaysIO"),
                contractapprovalCriteriaInDays:component.get("v.contractapprovalCriteriaInDaysIO")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
          
            if (state === "SUCCESS") { 
                var result = response.getReturnValue();
                component.set("v.opplistNonTS",result);
                var opplist = component.get("v.opplistNonTS");
                this.setTotalOpp(component,'opplistNonTS',component.get("v.opplistNonTS").length);
                var newlst =[];
                for (var i = 0; i < component.get("v.pageSize") && i<opplist.length; i++) { 
                    newlst.push(opplist[i]);
                }
               
                component.set("v.userSelectedOppListNonTS",newlst);
               }     
            else
            {
              helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
	},
    fireToastError : function(message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
     // Method used to calculate sum of all Opportunity
  setTotalOpp:function(component,oppType,oppLength){
      if(oppType=='opplistTS')
      {this.opplistTS =oppLength;
         console.log('opp Lst TC'+this.opplistTS);}
          
      else{
          this.opplistnonTS =oppLength;
          console.log('opp Lst NonTs'+this.opplistnonTS);
          }
          
      var sum= this.opplistTS+ this.opplistnonTS;  
      console.log('value of sum'+sum);
      component.set("v.totalOpp",sum);
      
  }
})