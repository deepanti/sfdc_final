({ 
    // function which initialises the component 
    doInit : function(component, event, helper) {
		
         //fetches all the TS and IO opportunities 
         helper.getOpportunityTS(component, event, helper);
       	 helper.getOpportunityNonTS(component, event, helper);
       
    },
    fullScreen : function(component) {
        component.set("v.fullScreen", true);
	},
    closeDialog : function(component) {
        component.set("v.fullScreen", false);
	}
    
})