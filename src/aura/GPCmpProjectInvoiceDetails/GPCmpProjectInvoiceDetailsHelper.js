({
    initJSONData: function(component, event, helper) {
        component.set("v.isHiddenonLoad", true);
        component.set("v.listOfProjectInvoices", helper.getSampleJSON());


    },
    doInit: function(component, event, helper) {
        this.hideSpinner(component);
    },
    pullProjectInvoiceDetails: function(component, event, helper) {
        this.showSpinner(component);
        var pullProjectInvoiceDetails = component.get("c.getProjectInvoiceDetails");
        pullProjectInvoiceDetails.setStorable();
        pullProjectInvoiceDetails.setParams({
            "projectId": component.get("v.recordId")
        });
        pullProjectInvoiceDetails.setCallback(this, function(response) {
            this.getProjectInvoicesDataServiceHandler(response, component);
        });
        $A.enqueueAction(pullProjectInvoiceDetails);
    },
    getProjectInvoicesDataServiceHandler: function(response, component) {
        var responseData = response.getReturnValue();
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess) {
            
            this.setProjectInvoicesData(component, responseData);
        } else {
            this.handleFailedCallback(component, responseData)
        }
    },
    setProjectInvoicesData: function(component, responseData) {
        
        var responseJson = JSON.parse(responseData.response);
        console.log('responseData'+responseData);
        //if(responseJson.isSuccess)
        {
        	console.log('responseJson: ', responseJson["PinnacleInvoiceDetail"]);
        	var array = [];
             component.set("v.isHiddenonLoad", true);
       	 	component.set("v.listOfProjectInvoices", responseJson["PinnacleInvoiceDetail"]);
        }
        /*else
        {
             component.set("v.isHiddenonLoad", true);
            this.showToast('Error', 'Error', responseJson.message);
        }*/
    },
    sendEmailRequest: function(component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var eventSource = event.target;
        var projectId = component.get("v.recordId");
        var TRXID = eventSource.parentNode.parentNode.parentNode.cells[4].textContent;
        if (TRXID == '') {
            this.showToast('Error', 'error', 'TRXID not generated.You cannot place request.');
            return;
        }
        this.showSpinner(component);
        var listofEmailNotification = [];
        var newEmailNotification = {
            "GP_Oracle_Invoice_ID__c": TRXID,
            "GP_Status__c": "Not Initiated",
            "GP_Project__c": projectId,
            "GP_User__c": userId
        };
        listofEmailNotification.push(newEmailNotification);
        var saveEmailRequestService = component.get("c.saveEmailInvoiceRequest");
        saveEmailRequestService.setParams({
            "serializedListOfEmailInvoiceRequest": JSON.stringify(listofEmailNotification)
        });
        saveEmailRequestService.setCallback(this, function(response) {
            this.saveEmailRequestServiceHandler(response, component);
        });

        $A.enqueueAction(saveEmailRequestService);
    },
    saveEmailRequestServiceHandler: function(response, component) {
        var responseData = response.getReturnValue();
        this.hideSpinner(component);
        if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess && responseData.response.toString() === "SUCCESS") {

            this.showToast('SUCCESS', 'success', 'Email request placed sucessfully.');
        } else if (component.isValid() && response.getState() === "SUCCESS" && responseData.isSuccess && responseData.response.toString() === "DUPLICATE") {
            this.showToast('Error', 'Error', 'Email request already exists.');
        } else {
            this.showToast('Error', 'Error', 'Some error Occured.Please try later.');
        }
    },

    getSampleJSON: function() {
        return [{
                "serviceDeliveryStartDate": "23-Jun-2017",
                "serviceDeliveryEndDate": "23-Jun-2017",
                "dueDate": "27-Jun-2017",
                "TRXID": "1234",
                "invoiceDate": "21-Jun-2017 ",
                "invoiceNumber": "12346",
                "PO": "PO12345",
                "oracleInvoiceNumber": "INV234518",
                "amount": "10000"

            },
            {
                "serviceDeliveryStartDate": "25-Jul-2017",
                "serviceDeliveryEndDate": "25-Jul-2017",
                "dueDate": "27-Jul-2017",
                "TRXID": "1235",
                "invoiceDate": "21-Jul-2017 ",
                "invoiceNumber": "12346",
                "PO": "PO1234533",
                "oracleInvoiceNumber": "INV234519",
                "amount": "11000"

            },
            {
                "serviceDeliveryStartDate": "25-Aug-2017",
                "serviceDeliveryEndDate": "25-Aug-2017",
                "dueDate": "27-Aug-2017",

                "invoiceDate": "21-Aug-2017 ",
                "invoiceNumber": "12346",
                "PO": "PO14533",
                "oracleInvoiceNumber": "INV24519",
                "amount": "12000"

            }


        ];
    }
})