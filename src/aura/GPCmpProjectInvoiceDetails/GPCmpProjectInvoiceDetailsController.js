({
	
    doInit: function(component, event, helper) {
		helper.doInit(component,event,helper);
	},
    pullProjectInvoiceDetails : function(component, event, helper) {
		helper.pullProjectInvoiceDetails(component, event, helper);        
    },
    
    sendEmailRequest : function(component, event, helper) {
		helper.sendEmailRequest(component, event, helper);        
    
    },
    toggleSidebar: function(component, event, helper){
        var elem = event.target.parentNode;
        $A.util.toggleClass(elem, 'collapsed');
    }
    
    
})