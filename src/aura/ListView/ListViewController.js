({
    doInit : function (component, event, helper) {
        
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "listViewId": component.get("v.listviewId"),
            "listViewName":component.get("v.listviewName"),
            "scope": component.get("v.objectName")
        });
        navEvent.fire();   
    },
    
    navChatter : function (component, event, helper) {
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Chatter"
        });
        homeEvent.fire();
    },
    
    isStale : function (component,event,helper){
        var action = component.get("c.getStaleUsers");
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state == 'SUCCESS') {
                if(response.getReturnValue()>0){
                    component.set("v.staledealcount", response.getReturnValue());
                    component.set("v.displaytext",true);
                }
            }
        });
       	var chkSF1 = component.get("c.chkSF1");
        chkSF1.setCallback(this,function(response){
            var state = response.getState();
            if (state == 'SUCCESS') {
                if(response.getReturnValue() == true){
                    //component.set("v.staledealcount", response.getReturnValue());
                    component.set("v.chkSF1",true);
                }
            }
        });
        
        var chkProfile = component.get("c.chkProfile");
        chkProfile.setCallback(this,function(response){
            var state = response.getState();
            if (state == 'SUCCESS') {
                //alert(response.getReturnValue());
                if(response.getReturnValue() == true){
                    //component.set("v.staledealcount", response.getReturnValue());
                    component.set("v.chkProfile",true);
                    
                }
            }
        });
        
        $A.enqueueAction(action); 
        $A.enqueueAction(chkSF1); 
        $A.enqueueAction(chkProfile); 
    },
    gotoURL : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/one/one.app#/chatter"
        });
        urlEvent.fire();
    },
    navigateToMyComponent : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:OppMainComponent",
        });
        evt.fire();
    },
        navigateToMyList : function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/one/one.app#/n/Update_Stale_Deals"
        });
        urlEvent.fire();
    }
})