({
  init: function (component, event, helper)
    { 
		helper.getProducts(component); 
		helper.getOpportunityClosedDate(component);	
        component.find("reset").set("v.disabled", true);
    },
	
	SearchProducts : function(component, event, helper)
	{   
         component.set("v.selectedProductList", []);
        
        var page = page || 1;
		var recordId = component.get("v.recordId");    
		
		var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
		
		var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
		
		var ProductName_Value = component.get("v.selectedProductNameDefault");
	
        if(ServiceLine_Value != '--None--' || NatureOfWork_Value != '--None--' || ProductName_Value != '--None--')
		{   
            
			component.set("v.isLoading", true);
            var action = component.get("c.getSearchedProduct");
        	action.setParams({
            	"opportunityID" : recordId,               
				"ServiceLine" : ServiceLine_Value,
				"NatureOfWork" : NatureOfWork_Value,
				"ProductName" : ProductName_Value,
				
        	});
           
             action.setCallback(this, function(response){ 
               
                var state = response.getState();
				if(state === 'SUCCESS')
				{
               		var result = response.getReturnValue();
					helper.setProductInWrapperList(component, result, page);
	
                    component.set("v.selectedProductNameDefault", "--None--");
                    component.set("v.selectedNatureOfWorkDefault","--None--");
                    component.set("v.selectedServiceLineDefault", "--None--");
                   
                    component.set("v.OnChangeServiceLine", false);
                    component.set("v.OnChangeNatureOfWork", false);
                    component.set("v.OnChangeProductName", false);
                    
                    component.find("ProductName").set("v.disabled", false);
                    component.find("ServiceLine").set("v.disabled", false); 
                    component.find("NatureOfWork").set("v.disabled", false); 
                    
                   	 /* var isProductListEmpty = component.get("v.isproductWrapperListEmpty");
                    
                  if(isProductListEmpty){
                        alert('in call back2==');
                        component.find("addToCart").set("v.disabled", true);
                    }*/
                    helper.getProducts(component, helper);
                }    
            });
			 $A.enqueueAction(action);
        }         
        else
		{	
            if(ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--'){
                component.set("v.errorMessage", "Please select at least one Search filter.");
                component.set("v.isError", true);
                component.set("v.isLoading", false);
                var finalSelectedProductList = component.get("v.finalSelectedProductList");
                if(finalSelectedProductList.length == 0){
                    component.set("v.isproductWrapperListEmpty", false);
                }
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),2000
                );    
               
            }
		}  
     
    },
	
	getSelectedProduct : function(component, helper)
	{     		 
        var selectedProductList = component.get("v.finalSelectedProductList");    
		if(selectedProductList.length == 0)
		{
        	component.set("v.errorMessage", "Please select atleast one Product");
        	component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            );
        }
        else
		{
            var event = $A.get("e.c:NavigateToAddProduct");
        	event.setParams({
                "productList" : selectedProductList,
                "recordId" : component.get("v.recordId")  
     		});
             event.fire();
        }
        
    },
	
	changeRowSelection : function(component, event, helper)
	{
        var productID = event.target.id;
        var checkBoxValue = document.getElementById(event.target.id).checked;
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        
        var productList = component.get("v.productWrapperList");
        
        if(checkBoxValue){
            for(index in productList){
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    productWrapper.isSelect = true;
                	selectedProductList.push(productWrapper.product);
                }
            }
        }
        else{
            component.set("v.selectAllFlag", false);
            for(index in productList){
                var productWrapper = productList[index];
                if(productWrapper.product.Id == productID){
                    productWrapper.isSelect = false;
                	selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productID),1);
                }
            }
        }
        if(selectedProductList.length > 0){
            component.find("addToCart").set("v.disabled", false);
        }
        else{
            component.find("addToCart").set("v.disabled", true);
        }
        component.set("v.selectedProductList", selectedProductList);
	},
	
	clearallfilters :  function(component, event, helper)
	{
		
        component.set("v.selectedServiceLineDefault", "--None--");
        component.set("v.selectedNatureOfWorkDefault", "--None--");
        component.set("v.selectedProductNameDefault", "--None--");
        
        component.find("ProductName").set("v.disabled", false);
        component.find("ServiceLine").set("v.disabled", false); 
        component.find("NatureOfWork").set("v.disabled", false);
        
        component.set("v.OnChangeServiceLine", false);
        component.set("v.OnChangeNatureOfWork", false);
        component.set("v.OnChangeProductName", false);
        
        component.find("reset").set("v.disabled", true);
        helper.getProducts(component);
        
    },
	handleClickCancel : function(component, event, helper)
	{
    	var recordId = component.get("v.recordId");
        function isLightningExperienceOrSalesforce1() 
		{
        	return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }
        if(isLightningExperienceOrSalesforce1()){
        	sforce.one.navigateToSObject(recordId,"OPPORTUNITY");
        }
        else{
            window.location = '/'+recordId;
        }
    },
	OnChange_ServiceLine:function(component, event, helper) 
    {		
		component.set("v.OnChangeServiceLine", true);
        component.set("v.isFilterLoading", true);
        
		var OppProduct2Data = [];				
		OppProduct2Data = component.get("v.productDataList");
		var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
		var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
		var ProductName_Value = component.get("v.selectedProductNameDefault");	
		var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
		if( (IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value == '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value != '--None--' && ServiceLine_Value != '--None--')
           || (IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--' && ServiceLine_Value != '--None--'))
		{
			helper.getProducts(component);
            component.find("reset").set("v.disabled", false);
		}
		else
		{
             component.set("v.isFilterLoading", false);
		}
        if(ServiceLine_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        }
	},
	OnChange_NatureOfWork:function(component, event, helper) 
    {		
		component.set("v.OnChangeNatureOfWork", true);
        component.set("v.isFilterLoading", true);
        
		var OppProduct2Data = [];				
		OppProduct2Data = component.get("v.productDataList");
		var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
		var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
		var ProductName_Value = component.get("v.selectedProductNameDefault");	
		var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
			
		if( (ServiceLine_Value == '--None--' && ProductName_Value == '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--') 
           || (ServiceLine_Value != '--None--' && ProductName_Value == '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--')
           || (ServiceLine_Value == '--None--' && ProductName_Value != '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--')
           || (ServiceLine_Value != '--None--' && ProductName_Value != '--None--' && IsOnChangeNatureOfWork == true && NatureOfWork_Value != '--None--'))
		{
			helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }   
		
		else
		{
            component.set("v.isFilterLoading", false);
		}
        if(NatureOfWork_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        }
	},
	OnChange_ProductName:function(component, event, helper) 
    {		
		component.set("v.OnChangeProductName", true);
        component.set("v.isFilterLoading", true);
        
		var OppProduct2Data = [];				
		OppProduct2Data = component.get("v.productDataList");
		var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
		var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
		var ProductName_Value = component.get("v.selectedProductNameDefault");	
		var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        
		if( (ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--') 
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--')
           || (ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'  && IsOnChangeProductName == true && ProductName_Value != '--None--'))		
		{
           helper.getProducts(component); 		
            component.find("reset").set("v.disabled", false);
        }
		else
		{
            component.set("v.isFilterLoading", false);
		}	
       	if(ProductName_Value == '--None--'){
            component.find("reset").set("v.disabled", true);
        }
        if(ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
            component.set("v.disableAddToCart", true);
        }
        else{
            component.set("v.disableAddToCart", false);
        }
	},
	pageChange : function(component, event, helper) 
	{
        var page = component.get("v.page") || 1;
        var pages = component.get("v.pages");
       	var direction = event.getParam("direction");
        
        if(direction === "previous"){
        	page =  page - 1;
            helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "next"){
        	page = page + 1;  
             helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "first"){
        	page = 1;    
             helper.getProductFromWrapperList(component,page);
        }
        else if(direction === "last"){
            page = pages; 
             helper.getProductFromWrapperList(component,page);
        }       
       component.set("v.selectAllFlag", false);
    },
	 selectAllProducts : function(component, event, helper)
	{
     	var selectallFlag = component.get("v.selectAllFlag");
        var productList = component.get("v.productWrapperList");
        var selectedProductList = component.get("v.selectedProductList");
        var index;
        
        if(selectallFlag){
        	for(index in productList){
                var productWrapper = productList[index];
                productWrapper.isSelect = true;
            	document.getElementById(productWrapper.product.Id).checked = true;
                if(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id) === -1) {
            		selectedProductList.push(productWrapper.product);
                }    
           	}
        }
        else{
            for(index in productList){
                var productWrapper = productList[index];
            	document.getElementById(productWrapper.product.Id).checked = false;
            	productWrapper.isSelect = false;
                selectedProductList.splice(selectedProductList.findIndex(x => x.Id==productWrapper.product.Id),1);
        	}
         }
        if(selectedProductList.length > 0){
            component.find("addToCart").set("v.disabled", false);
        }
        else{
            component.find("addToCart").set("v.disabled", true);
        }
        component.set("v.selectedProductList", selectedProductList);
    },
    
    getFinalListOfSelectedProducts : function(component, event, helper){
        component.find("addToCart").set("v.disabled", true);
        var selectedProductList = component.get("v.selectedProductList");
        var productWrapperList = component.get("v.productWrapperList");
        var totalProductList = component.get("v.totalProductList");
        var index = 0;
        var finalProductList = component.get("v.finalSelectedProductList");
        var page = page || 1;
        finalProductList.push.apply(finalProductList,selectedProductList);    
        
        component.set("v.finalSelectedProductList", finalProductList);
        var serviceline = [];
        for(index in selectedProductList){
            var selectedProduct = selectedProductList[index];
            serviceline.push(selectedProductList[index].Service_Line__c);
            var ind = productWrapperList.findIndex(x => x.product.Id==selectedProduct.Id);
            if(ind != -1){
                productWrapperList.splice(ind,1);    
            }
            totalProductList.splice(totalProductList.findIndex(x => x.product.Id==selectedProduct.Id),1);
        }
        console.log(':----serviceLine------'+serviceline);
        if(serviceline.includes("Others")){
            component.set("v.errorMessageserviceLine", "1.	You have selected product(s) with service line as ‘Others’. If this is erroneous, then please select the correct product – service line combination else proceed to next step.");
                component.set("v.isErrorserviceline", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isErrorserviceline", false);    
                        }    
                    }),6000
                );     
        }
        component.set("v.totalProductList", totalProductList);
        component.set("v.selectedProductList",[]);
        
        helper.getProductFromWrapperList(component,page);
        
    },
    
    deleteSelectedProduct : function(component, event, helper){
    	var productID = event.currentTarget.id;
        var index = 0;
        var finalSelectedProductList = component.get("v.finalSelectedProductList");
        var selectedProductList = component.get("v.selectedProductList");
       
        for(index in finalSelectedProductList){
            var selectedProduct = finalSelectedProductList[index];
            var ind = finalSelectedProductList.findIndex(x => x.Id==productID);
            if(ind != -1){
                finalSelectedProductList.splice(ind,1);
            }
        }
        var i = 0;
        
        for(i in selectedProductList){
            var selectedProduct = selectedProductList[i];
           	var ind = selectedProductList.findIndex(x => x.Id==productID);
            if(ind != -1){
            	selectedProductList.splice(ind,1);    
            }
        }
        component.set("v.selectedProductList", selectedProductList);
        component.set("v.finalSelectedProductList", finalSelectedProductList);
    },
    
     closeErrorToast : function(component, event, helper){
    	component.set("v.isError", false);
	},
    
	 closeErrorToastServiceLine : function(component, event, helper){
    	component.set("v.isErrorserviceline", false);
	},
    
    addProductToCart : function(component, event, helper){
        component.set("v.isproductWrapperListEmpty", false);
        var page = page || 1;
        var recordId = component.get("v.recordId");    
        
        var finalProductList = component.get("v.finalSelectedProductList");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");
        if(ServiceLine_Value == 'Others'){
            component.set("v.errorMessageserviceLine", "1.	You have selected product(s) with service line as ‘Others’. If this is erroneous, then please select the correct product – service line combination else proceed to next step.");
            component.set("v.isErrorserviceline", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isErrorserviceline", false);    
                    }    
                }),6000
            );    
            
        }
        component.set("v.isLoading", true);
        var action = component.get("c.getSearchedProduct");
        action.setParams({
            "opportunityID" : recordId,               
            "ServiceLine" : ServiceLine_Value,
            "NatureOfWork" : NatureOfWork_Value,
            "ProductName" : ProductName_Value,
            
        });
        action.setCallback(this, function(response){ 
            
            var state = response.getState();
            if(state === 'SUCCESS')
            {
                var result = response.getReturnValue();
                finalProductList.push.apply(finalProductList, result.productList);
                component.set("v.finalSelectedProductList", finalProductList);
                
                component.set("v.selectedProductNameDefault", "--None--");
                component.set("v.selectedNatureOfWorkDefault","--None--");
                component.set("v.selectedServiceLineDefault", "--None--");
                
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeNatureOfWork", false);
                component.set("v.OnChangeProductName", false);
                
                component.find("ProductName").set("v.disabled", false);
                component.find("ServiceLine").set("v.disabled", false); 
                component.find("NatureOfWork").set("v.disabled", false); 
                
                component.set("v.disableAddToCart", false);
                   
                helper.getProducts(component, helper);
                component.set("v.isLoading", false);
            }    
        });
        $A.enqueueAction(action);
    }
	
})