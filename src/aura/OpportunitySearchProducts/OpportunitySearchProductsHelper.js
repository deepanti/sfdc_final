({	
    getProducts : function(component, page)
    {
        var page = page || 1;
        var recordId = component.get("v.recordId");
        var IsOnChangeServiceLine = component.get("v.OnChangeServiceLine");
        var IsOnChangeNatureOfWork = component.get("v.OnChangeNatureOfWork");
        var IsOnChangeProductName = component.get("v.OnChangeProductName");
        var ServiceLine_Value = component.get("v.selectedServiceLineDefault");
        var NatureOfWork_Value = component.get("v.selectedNatureOfWorkDefault");
        var ProductName_Value = component.get("v.selectedProductNameDefault");
        
        if(IsOnChangeServiceLine == true || IsOnChangeNatureOfWork == true || IsOnChangeProductName == true )		
        {
            var action = component.get("c.getProductsOnChange");
            action.setParams({
                "opportunityID" : recordId,
                "SelectedServiceLine" : ServiceLine_Value,	
                "SelectedNatureOfWork" : NatureOfWork_Value,
                "SelectedProductName" : ProductName_Value,
            });
            action.setCallback(this, function(response) { 
                var state = response.getState();
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading", false);
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var OnChangeService_productDataList =[]; 
                    OnChangeService_productDataList.push(JSON.stringify(productList));
                    component.set("v.OnChangeService_productDataList", OnChangeService_productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.OnChangeService_productDataList");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name);  // Product Name list
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates	
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }	
                    
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value == '--None--') 
                    {	
                        component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList);  
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }	
                    else if(IsOnChangeServiceLine == true  && NatureOfWork_Value != '--None--' && ProductName_Value == '--None--') 
                    { 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }
                    else if(IsOnChangeServiceLine == true  && NatureOfWork_Value == '--None--' && ProductName_Value != '--None--') 
                    { 
                        component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList);  
                    }
                    else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value == '--None--') 
                    { 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }
                    else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value != '--None--' && ProductName_Value == '--None--') 
                    { 
                        component.set("v.ProductNameArrayList", unique_ProductNameArrayList); 
                    }
                    else if(IsOnChangeNatureOfWork == true  && ServiceLine_Value == '--None--' && ProductName_Value != '--None--') 
                    { 
                        component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);
                    }
                    else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value == '--None--') 
                    { 
                        component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);  
                        component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                    }
                    else if(IsOnChangeProductName == true  && ServiceLine_Value != '--None--' && NatureOfWork_Value == '--None--') 
                    {   
                         component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 
                    }
                    else if(IsOnChangeProductName == true  && ServiceLine_Value == '--None--' && NatureOfWork_Value != '--None--') 
                    { 
                          component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList);					 
                    }
                }			
            });
            
            
            if(IsOnChangeServiceLine && IsOnChangeNatureOfWork && ServiceLine_Value != '--None--' && NatureOfWork_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeNatureOfWork", false);
                
                
            }
            if(IsOnChangeServiceLine && IsOnChangeProductName && ServiceLine_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("ServiceLine").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true); 
                component.set("v.OnChangeServiceLine", false);
                component.set("v.OnChangeProductName", false);
            }
            
            if(IsOnChangeNatureOfWork && IsOnChangeProductName && NatureOfWork_Value != '--None--' && ProductName_Value != '--None--'){
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true); 
                component.set("v.OnChangeNatureOfWork", false);
                component.set("v.OnChangeProductName", false);
            }
         }			
        else
        {	
            var action = component.get("c.getProducts");
            action.setParams({
                "opportunityID" : recordId,            
            });
            
            action.setCallback(this, function(response){ 
                var state = response.getState();
                
                if(state === 'SUCCESS')
                {
                    component.set("v.isFilterLoading", false);
                    
                    var result = response.getReturnValue();
                    var productList = result.productList;
                    var productDataList =[];
                    productDataList.push(JSON.stringify(productList));
                    component.set("v.productDataList", productDataList); // Complete Product2 data initially
                    var index;
                    var product;
                    var ProductNameArrayList = [];
                    var ServiceLineArrayList = [];
                    var NatureOfWorkArrayList = [];
                    var OppProduct2Data = [];				
                    OppProduct2Data = component.get("v.productDataList");
                    
                    for(index in productList)
                    {
                        product = productList[index];
                        ProductNameArrayList.push(product.Name);  // Product Name list
                        ServiceLineArrayList.push(product.Service_Line__c); // Service Line list with duplicates
                        NatureOfWorkArrayList.push(product.Nature_of_Work__c); // Nature of work list with duplicates		
                    }	
                    
                    
                    var unique_ProductNameArrayList = []
                    for(var i = 0;i < ProductNameArrayList.length; i++)
                    {
                        if(unique_ProductNameArrayList.indexOf(ProductNameArrayList[i]) == -1){
                            unique_ProductNameArrayList.push(ProductNameArrayList[i])
                        }
                    }
                    
                    var unique_ServiceLineArrayList = []
                    for(var i = 0;i < ServiceLineArrayList.length; i++)
                    {
                        if(unique_ServiceLineArrayList.indexOf(ServiceLineArrayList[i]) == -1){
                            unique_ServiceLineArrayList.push(ServiceLineArrayList[i])
                        }
                    }
                    
                    var unique_NatureOfWorkArrayList = []
                    for(var i = 0;i < NatureOfWorkArrayList.length; i++)
                    {
                        if(unique_NatureOfWorkArrayList.indexOf(NatureOfWorkArrayList[i]) == -1){
                            unique_NatureOfWorkArrayList.push(NatureOfWorkArrayList[i])
                        }
                    }
                    unique_ProductNameArrayList.sort();
                    unique_NatureOfWorkArrayList.sort();
                    unique_ServiceLineArrayList.sort();
                    
                    component.set("v.ProductNameArrayList", unique_ProductNameArrayList); // Product Name list
                    component.set("v.ServiceLineArrayList", unique_ServiceLineArrayList); // Service Line list
                    component.set("v.NatureOfWorkArrayList", unique_NatureOfWorkArrayList); 	
                    
                    
                }  
                
            });
        }
        $A.enqueueAction(action); 
    },	
    
    setProductInWrapperList : function(component, result, page)
    {
        var productList = result.productList;
        var index;
        var selectedIndex;
        var product;
        var totalProductList = [];
       
        for(index in productList)
        {
            product = productList[index];
            var productWrapper ={
                "product" : product,                       
                "isSelect" : false
            } 
            totalProductList.push(productWrapper);
        }
        
        component.set("v.page",page);
        component.set("v.total",result.total);
        component.set("v.pages",Math.ceil(result.total/component.get("v.pageSize")));
        component.set('v.selectAllFlag', false);
        component.set("v.totalProductList", totalProductList);
        
        this.getProductFromWrapperList(component, page);  
        if(totalProductList.length > 0)
        {
            component.set("v.isproductWrapperListEmpty", true);
        }
        else
        {
            component.set("v.isproductWrapperListEmpty", false);
        }
        component.set("v.isLoading", false);
    },
    
    getProductFromWrapperList : function(component, page)
    {
        var productWrapperList = [];
        var req_list = [];
        var pageSize = component.get("v.pageSize");
        var totalList = component.get("v.totalProductList");
        if(page === 1){
            req_list = totalList.slice(0, pageSize);    
        }
        else{
            var offset = (+page -1) * (+pageSize);
            req_list = totalList.slice(offset , (+pageSize * +page));    
        }
        component.set("v.page",page);
        component.set("v.productWrapperList", req_list);
        component.set("v.pages",Math.ceil(totalList.length/component.get("v.pageSize")));
        
    },
    getOpportunityClosedDate : function(component, event, helper)
    {
        var action = component.get("c.getOpportunityClosedDate");
        var recordId = component.get("v.recordId");
        action.setParams({
            "opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){     
            var closedDate = new Date(response.getReturnValue().closedDate);
            var todayDate = new Date();
            var loggedInUser = response.getReturnValue().loggedInUser;
            if(closedDate.getTime() < todayDate.getTime() && loggedInUser.Profile.Name != 'Genpact Super Admin')
            {
                component.set("v.errorMessage", "Please update MSA/SOW date of Opportunity later than today.");
                component.set("v.isError", true);
                component.find("ServiceLine").set("v.disabled", true);
                component.find("NatureOfWork").set("v.disabled", true);
                component.find("ProductName").set("v.disabled", true);
                component.find("searchProduct").set("v.disabled", true);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),5000
                );
            } 
            
        });
        $A.enqueueAction(action); 
    },
    
   /* getAllServiceLine : function(cmp){
        var action = cmp.get('c.getAllServiceLine');
        
        action.setParams({
            oppID : cmp.get('v.recordId')
        });
        action.setCallback(this,function(resp){
            var unique_ServiceLineArrayList = [];
            var duplicateserviceLineList = resp.getReturnValue();
            for(var i = 0;i < duplicateserviceLineList.length; i++)
            {
                if(unique_ServiceLineArrayList.indexOf(duplicateserviceLineList[i]) == -1){
                    unique_ServiceLineArrayList.push(duplicateserviceLineList[i])
                }
            }
            cmp.set("v.serviceLineList", unique_ServiceLineArrayList);
        });
        $A.enqueueAction(action);
    },
    
    getAllProductName : function(cmp){
        var action = cmp.get('c.getAllProductName');
        
        action.setParams({
            oppID : cmp.get('v.recordId')
        });
        action.setCallback(this,function(resp){
            var unique_ProductNameArrayList = [];
            var duplicateProductList = resp.getReturnValue();
            for(var i = 0;i < duplicateProductList.length; i++)
            {
                if(unique_ProductNameArrayList.indexOf(duplicateProductList[i]) == -1){
                    unique_ProductNameArrayList.push(duplicateProductList[i])
                }
            }
            cmp.set("v.productNameList", unique_ProductNameArrayList);
        });
        $A.enqueueAction(action);
    },
    
    getAllNatureOfWork : function(cmp){
        
        var action = cmp.get('c.getAllNatureOfWork');
        action.setParams({
            oppID : cmp.get('v.recordId')
        });
        action.setCallback(this,function(resp){
            var unique_natureOfWorkArrayList = [];
            var duplicateNatureOfWorkList = resp.getReturnValue();
            for(var i = 0;i < duplicateNatureOfWorkList.length; i++)
            {
                if(unique_natureOfWorkArrayList.indexOf(duplicateNatureOfWorkList[i]) == -1){
                    unique_natureOfWorkArrayList.push(duplicateNatureOfWorkList[i])
                }
            }
            cmp.set("v.natureOfWorkList", unique_natureOfWorkArrayList);                   
        });
        $A.enqueueAction(action);
    }*/
})