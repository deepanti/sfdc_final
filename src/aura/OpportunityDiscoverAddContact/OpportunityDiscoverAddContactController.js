({
    doInit : function(component, event, helper) {  
        //Initializing buying role picklist
     
        helper.buyingRolePicklist(component, event, helper);
        //fetching contact Role data from the server 
        helper.getContactRole(component, event, helper);
    },
    
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
          helper.showSpinner(component, event, helper); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner  
        helper.hideSpinner(component, event, helper); 
       
    },
    
    handleAdd: function(component, event, helper) { 
        helper.handleAdd(component, event, helper);
    },
    handleContact:function(component, event, helper) {
        //creating new contact by opening standard contact New page 
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Contact",
            "defaultFieldValues": 
            {
                'AccountId' : component.get("v.simpleRecord.AccountId")
            }
        });
        createRecordEvent.fire();
    },
    //function to delete item from the Contact Role List 
    deleteItemFromList:function(component, event, helper) {
        var contId=event.getParam("contactId");
        
        var contRoleList=component.get("v.ContactRoleList"); 
        var delItem=component.get("v.deleteItems");
        var getIndex=-1;
        //getting the index of the item to be deleted
        for(var i=0;i<contRoleList.length;i++)
        {
            if(contRoleList[i].Contact__c==contId)
            {
                getIndex=i;
                break;
            }
        }
        
        if(getIndex!=-1)
        {
          
            if(!$A.util.isUndefinedOrNull(contRoleList[getIndex].Id))
            {
                var item=contRoleList[getIndex];
                item.IsPrimary__c='false';//making it False since primary role cannot be deleted when it is True
                delItem.push(item);
                component.set("v.deleteItems",delItem);
            }
            
            contRoleList.splice(getIndex, 1);
            component.set("v.ContactRoleList",contRoleList);
        }
        
    },
    closeDialog: function(component, event, helper) {
        component.set("v.isVisible",false);
    },
    
    handleSaveModal:function(component, event, helper)
    {
        
        var ContactRoleList = component.get("v.ContactRoleList");
        var contactId=component.get("v.contactId");
        var buyingRole=component.get("v.buyingRole");
        var isPrimary=component.get("v.isPrimary");
        for(var i=0;i<ContactRoleList.length;i++)
        {
            if(ContactRoleList[i].Contact__c==contactId)
            {
                if(buyingRole=='' ||$A.util.isUndefinedOrNull(buyingRole))
                {
                    helper.fireToastError(component, event, helper,'Role could not be empty!');
                }
                else
                {
                    ContactRoleList[i].Role__c=buyingRole;
                    component.set("v.buyingRole",null);
                }
                
                ContactRoleList[i].IsPrimary__c=isPrimary;
                
                if(ContactRoleList[i].IsPrimary__c==true)
                {
                    for(var j=0;j<ContactRoleList.length;j++)
                    {
                        if(ContactRoleList[j].Contact__c!=contactId)
                        {
                            ContactRoleList[j].IsPrimary__c=false;
                        }
                    }
                }
                break;
            }
        }
        component.set("v.ContactRoleList",ContactRoleList);
        component.set("v.isVisible",false);
    },
    handleSave:function(component, event, helper)
    {
        helper.showSpinner(component, event, helper); 
        //saving the result
        var contRoleList1=component.get("v.contact");
        helper.handleSave(component, event, helper, false);
    },
    hideShowComponent:function(component, event, helper)
    {   
     component.set("v.isVisible",event.getParam("isVisible"));
     component.set("v.isPrimary",event.getParam("isPrimary"));
     component.set("v.contact",event.getParam("contact"));
     component.set("v.buyingRole",event.getParam("buyingRole"));
     component.set("v.contactId",event.getParam("contactId"))
    },
    
    //save and next button
    saveAndNext : function(component, event, helper) {
         helper.showSpinner(component, event, helper); 
        var contRoleList1=component.get("v.ContactRoleList");
        helper.handleSave(component, event, helper, true);
       
    },
    back : function(component, event, helper) {
        localStorage.setItem('LatestTabId',1);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "1"});
        compEvents.fire();
        window.scrollTo(0, 0);
       $A.get('e.force:refreshView').fire();//added to reflect primary contact on Discover
    },
    navigate:function(component, event, helper)
    {

        localStorage.setItem('LatestTabId',3);
        var compEvents = component.getEvent("componentEventFired");
        compEvents.setParams({ "SelectedTabId" : "3" });
        compEvents.fire();
        window.scrollTo(0, 0);
        
    }
})