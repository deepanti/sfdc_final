({
    //fetching the picklist from the server 
    buyingRolePicklist : function(component, event, helper) {
    
        var opts = [];
        var action = component.get("c.getContactRolePicklist");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var allValues = response.getReturnValue(); 
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "Select Role",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.buyingRolePicklist",opts);
                component.set("v.Spinner",false);
            }
            else
            {
               this.fireToastError(component, event, helper,response.getError()[0].message); 
            }
            
        });
        $A.enqueueAction(action);
        
    },
    //initialising the data from the server 
    getContactRole:function(component, event, helper) {
       
        component.set("v.Spinner",true);
        component.set("v.buyingRole",null);
        //clearing the list since  existing data may present  
        var clearList=component.get("v.ContactRoleList");
        clearList.splice(0,clearList.length);
        component.set("v.ContactRoleList",clearList);
        
        //clearing the list since  existing data may present 
        var cleardeleteItems=component.get("v.deleteItems");
        cleardeleteItems.splice(0,cleardeleteItems.length);
        component.set("v.deleteItems",cleardeleteItems);
        
        var action = component.get("c.getContactRoleRecords");
        var listContactRoleFromServer =[];
        
        action.setParams(
            {
                'oppId' :component.get("v.opportunityId")
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                listContactRoleFromServer=response.getReturnValue(); 
                var contRoleList=component.get("v.ContactRoleList"); 
                
                for(var i=0;i<listContactRoleFromServer.length;i++)
                {
                    var singleObject = {};
                    singleObject = { 
                        'Contact__c': listContactRoleFromServer[i].Contact__c,
                        'Role__c':listContactRoleFromServer[i].Role__c,
                        'IsPrimary__c':listContactRoleFromServer[i].IsPrimary__c,
                        'ContactName':listContactRoleFromServer[i].Contact__r.Name,
                        'Id':listContactRoleFromServer[i].Id
                    };
                    contRoleList.push(singleObject);
                }
                component.set("v.ContactRoleList",contRoleList);
                component.set("v.Spinner",false);
            }
            else
            {
                this.fireToastError(component, event, helper,response.getError()[0].message);
                $A.get('e.force:refreshView').fire();
            }
            
        });
        $A.enqueueAction(action);
        
    },
    //adding the contact and role picklist 
    handleAdd: function(component, event, helper) {
        
        var buyingRoleValue=component.get("v.buyingRole");
        var contRoleList=component.get("v.ContactRoleList"); 
        //validating contact 
        if(component.get("v.selItem.val")==''||$A.util.isUndefinedOrNull(component.get("v.selItem.val")))
        {
            this.fireToastError(component, event, helper,'Atleast one contact needs to be tagged to an opportunity. Please select contact and contact role');
        }
        //validating buying role
        else if(buyingRoleValue=='' ||$A.util.isUndefinedOrNull(buyingRoleValue))
        {
            this.fireToastError(component, event, helper,'Atleast one contact needs to be tagged to an opportunity. Please select contact and contact role');
        }
            else
            {
                var singleObject = {};
                singleObject = { 
                    'Contact__c': component.get("v.selItem.val"),
                    'Role__c':buyingRoleValue,
                    'IsPrimary__c':component.get("v.DefaultRole"),
                    'ContactName':component.get("v.selItem.text")
                    
                };
                
                //validating  the values of the picklist 
                if(singleObject.Contact__c!=null &&
                   (singleObject.Role__c!=null && singleObject.Role__c!=""))
                {
                    var flag=0;
                    for(var i=0;i<contRoleList.length;i++)
                    {
                        if(contRoleList[i].Contact__c==component.get("v.selItem.val"))//check if element is present in the list
                            flag=1;
                    }
                    if(flag==0)
                    {
                        contRoleList.push(singleObject);
                        component.set("v.ContactRoleList",contRoleList);
                    }
                    else
                    {
                        this.fireToastError(component, event, helper,'Contact is already added!');
                    }
                }
                component.set("v.selItem",null);
                component.set("v.buyingRole",null);
            }
        
    },
    handleSave:function(component, event, helper, saveAndNext) 
    {
        var listRoleRecords = [];//list of Sobject to be passed to the server
        var opportunityId=component.get("v.opportunityId");
        var contRoleList=component.get("v.ContactRoleList"); 
        var delItem=component.get("v.deleteItems");//list of Sobject that needs to be deleted 
        var primaryCheckFlag=0;
        if(contRoleList.length>0)
        {
            for(var i=0;i<contRoleList.length;i++)
            {
                var singleObject = {};
                singleObject = {
                    'sobjectType': 'Contact_Role_Rv__c',
                    'Contact__c': contRoleList[i].Contact__c,
                    'Role__c':contRoleList[i].Role__c,
                    'IsPrimary__c':contRoleList[i].IsPrimary__c,
                    'Opportunity__c':opportunityId,
                    'Id':contRoleList[i].Id
                };
                //check for primary contact ,whether it is added or not 
                if(singleObject.IsPrimary__c==true)
                    primaryCheckFlag++;
                listRoleRecords.push(singleObject);
                
            }
            if(primaryCheckFlag==0)
            {
                helper.hideSpinner(component, event, helper); 
                this.fireToastError(component, event, helper,'Primary contact is not added!');
            }
            else
            {
                var seriallistRoleRecords=JSON.stringify(listRoleRecords);
                var serialdelItem=JSON.stringify(delItem);
                var action = component.get("c.saveContactRole");
                
                action.setParams(
                    {
                        'listOfContactRole' :seriallistRoleRecords,
                        'oppId':opportunityId,
                        'deleteItemList':serialdelItem
                    });
                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        var message=response.getReturnValue();
                        if(message=='Data Saved Successfully')
                        {
                            helper.hideSpinner(component, event, helper); 
                            this.fireToastSuccess(component, event, helper,'Contacts Saved Successfully');
                            this.getContactRole(component, event, helper);
                            if(saveAndNext)
                            {
                                
                                var SelectedTabId = "3";
                                if(localStorage.getItem('MaxTabId')<3)
                                { 
                                    localStorage.setItem('MaxTabId',3);
                                    component.set("v.MaxSelectedTabId", SelectedTabId);
                                    component.find("recordViewForm").submit();
                                    localStorage.setItem('LatestTabId',3);
                                    var compEvents = component.getEvent("componentEventFired");
                                    compEvents.setParams({ "SelectedTabId" : "3" });
                                    compEvents.fire();
                                    window.scrollTo(0, 0);
                                }
                                else
                                {
                                    localStorage.setItem('LatestTabId',3);
                                    var compEvents = component.getEvent("componentEventFired");
                                    compEvents.setParams({ "SelectedTabId" : "3" });
                                    compEvents.fire();
                                    window.scrollTo(0, 0);
                                }
                                
                                
                                //Fixed By Dildar - not changing tab to Add Products
                                $A.get('e.force:refreshView').fire();
                               
                            }
                            //$A.get('e.force:refreshView').fire();
                            localStorage.setItem('LatestTabId',SelectedTabId);
                        }
                        else
                        {
                            helper.hideSpinner(component, event, helper); 
                            this.fireToastError(component, event, helper,message);
                            this.getContactRole(component, event, helper);
                        }
                         //$A.get('e.force:refreshView').fire();
                    }
                    else
                    {    helper.hideSpinner(component, event, helper);     
                         this.fireToastError(component, event, helper,response.getError()[0].message);
                    	 this.getContactRole(component, event, helper);
                    }
                    
                });
                $A.enqueueAction(action);
            }
        }
        else
        {
            helper.hideSpinner(component, event, helper); 
            this.fireToastError(component, event, helper,'At least one primary contact should be present');
            this.getContactRole(component, event, helper);
        }
    },
    fireToastSuccess : function(component, event, helper,message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(component, event, helper,message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner
        component.set("v.Spinner", false);
    }
    
})