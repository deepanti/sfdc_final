({
	init: function (component, event, helper) 
    {        
        var OpportunityList = component.get("v.OpportunityList");       
       
        var recordId = component.get("v.recordId");     
        var OppWrapperList1 = [];
            OppWrapperList1 = component.get("v.OppWrapperList");  
		 
       
		var test=OppWrapperList1[0]; 	
		var Opp_Reason_1;
		var Opp_Reason_2;
		var Opp_Reason_3;
		var Opp_key_win_reasons_and_any_key_learnings;
		var Opp_Incumbent;
		var Opp_Incumbent_Others;
		var Opp_Genpact_s_proposed_price_differ;
		var Opp_Genpact_s_proposed_price_differ_Percent;
		var Opp_Closest_Competitor_Others;
		var Opp_Closest_Competitor_1;
		var Opp_Closest_Competitor_2;
		var Opp_Competitor_strengths1;
		var Opp_Competitor_weaknesses1;
		var Opp_Competitor_strengths2;
		var Opp_Competitor_weaknesses2;
		var Opp_Genpact_Involvement_in_deal_with_prospec;
		var Opp_interactions_did_we_have_with_the_prospe;
		var Opp_If_advisor_was_involved;
		var Opp_Loss_Deal_Stage;
		var Opp_Deal_Winner;
		var Opp_Deal_Winner_Others;		
		var Opp_W_L_D__c;
						
        for(var i=0;i<OppWrapperList1.length;i=i+1)
        {             
            var test1=OppWrapperList1[i];
           // alert('=======Data====='+JSON.stringify(test1));
            var jsnParse= JSON.parse(JSON.stringify(test1));
          	//alert('=====jsnParse======'+jsnParse);
			
			Opp_Reason_1= jsnParse.OpportunityDataList[0].Reason_1__c;
			Opp_Reason_2= jsnParse.OpportunityDataList[0].Reason_2__c;
			Opp_Reason_3= jsnParse.OpportunityDataList[0].Reason_3__c;         
            Opp_key_win_reasons_and_any_key_learnings= jsnParse.OpportunityDataList[0].key_win_reasons_and_any_key_learnings__c;
            Opp_Incumbent= jsnParse.OpportunityDataList[0].Incumbent__c;  
            Opp_Incumbent_Others = jsnParse.OpportunityDataList[0].Incumbent_Others__c;  
            Opp_Genpact_s_proposed_price_differ= jsnParse.OpportunityDataList[0].Genpact_s_proposed_price_differ__c;
            Opp_Genpact_s_proposed_price_differ_Percent=jsnParse.OpportunityDataList[0].Genpact_s_proposed_price_differ_Percent__c;
			Opp_Closest_Competitor_Others = jsnParse.OpportunityDataList[0].Closest_Competitor_Others__c;
			Opp_Closest_Competitor_1 = jsnParse.OpportunityDataList[0].Closest_Competitor_1__c ;  
			Opp_Closest_Competitor_2= jsnParse.OpportunityDataList[0].Closest_Competitor_2__c;  
			Opp_Competitor_strengths1= jsnParse.OpportunityDataList[0].Competitor_strengths1__c;  
			Opp_Competitor_weaknesses1= jsnParse.OpportunityDataList[0].Competitor_weaknesses1__c;  
			Opp_Competitor_strengths2= jsnParse.OpportunityDataList[0].Competitor_strengths2__c;  
			Opp_Competitor_weaknesses2= jsnParse.OpportunityDataList[0].Competitor_weaknesses2__c;  
			Opp_Genpact_Involvement_in_deal_with_prospec= jsnParse.OpportunityDataList[0].Genpact_Involvement_in_deal_with_prospec__c; 
			Opp_interactions_did_we_have_with_the_prospe= jsnParse.OpportunityDataList[0].interactions_did_we_have_with_the_prospe__c;
			Opp_If_advisor_was_involved= jsnParse.OpportunityDataList[0].If_advisor_was_involved__c;
			Opp_Loss_Deal_Stage= jsnParse.OpportunityDataList[0].Loss_Deal_Stage__c;
			Opp_Deal_Winner= jsnParse.OpportunityDataList[0].Deal_Winner__c;
			Opp_Deal_Winner_Others= jsnParse.OpportunityDataList[0].Deal_Winner_Others__c;			
			Opp_W_L_D__c= jsnParse.OpportunityDataList[0].W_L_D__c;			
			         
        }
		
		component.set("v.Opp_Reason_1",Opp_Reason_1);
		component.set("v.Opp_Reason_2",Opp_Reason_2);
		component.set("v.Opp_Reason_3",Opp_Reason_3);
		component.set("v.Opp_key_win_reasons_and_any_key_learnings",Opp_key_win_reasons_and_any_key_learnings);
		component.set("v.Opp_Incumbent",Opp_Incumbent);
		component.set("v.Opp_Incumbent_Others",Opp_Incumbent_Others);
		component.set("v.Opp_Genpact_s_proposed_price_differ",Opp_Genpact_s_proposed_price_differ);
		component.set("v.Opp_Genpact_s_proposed_price_differ_Percent",Opp_Genpact_s_proposed_price_differ_Percent);
		component.set("v.Opp_Closest_Competitor_Others",Opp_Closest_Competitor_Others);
		component.set("v.Opp_Closest_Competitor_1",Opp_Closest_Competitor_1);
		component.set("v.Opp_Closest_Competitor_2",Opp_Closest_Competitor_2);
		component.set("v.Opp_Competitor_strengths1",Opp_Competitor_strengths1);
		component.set("v.Opp_Competitor_weaknesses1",Opp_Competitor_weaknesses1);
		component.set("v.Opp_Competitor_strengths2",Opp_Competitor_strengths2);
		component.set("v.Opp_Competitor_weaknesses2",Opp_Competitor_weaknesses2);
		component.set("v.Opp_Genpact_Involvement_in_deal_with_prospec",Opp_Genpact_Involvement_in_deal_with_prospec);
		component.set("v.Opp_interactions_did_we_have_with_the_prospe",Opp_interactions_did_we_have_with_the_prospe);
		component.set("v.Opp_If_advisor_was_involved",Opp_If_advisor_was_involved);
		component.set("v.Opp_Loss_Deal_Stage",Opp_Loss_Deal_Stage);
		component.set("v.Opp_Deal_Winner",Opp_Deal_Winner);
		component.set("v.Opp_Deal_Winner_Others",Opp_Deal_Winner_Others);
		component.set("v.Opp_W_L_D__c",Opp_W_L_D__c);					
 
    },
	
	handleCancel: function(component, event, helper) 
    {      
			var recordId = component.get("v.recordId"); 			
    		//sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
			//sforce.one.back(true);
			function isLightningExperienceOrSalesforce1() 
			{
				return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
			} 
			if(isLightningExperienceOrSalesforce1()) 
			{   
				//sforce.one.navigateToURL('/'+recordId); //'/'+recordId  //'/006'
				sforce.one.navigateToSObject(recordId,"OPPORTUNITY"); 
				sforce.one.back(true);
			}
			else
			{
				window.location = '/'+recordId;   //'/006/o'
			} 
    }
})