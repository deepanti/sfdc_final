({
    fireToastSuccess : function(message) {
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Success!",  
            "message": message,  
            "type": "success"  
        });  
        toastEvent.fire();  
    },
    fireToastError : function(message) {  
        var toastEvent = $A.get("e.force:showToast");  
        toastEvent.setParams({  
            "title": "Error!",  
            "message": message,  
            "type": "ERROR"  
        });  
        toastEvent.fire();  
    },
    handleConfirmationForPrediscover:function(component,event,helper,oppId){
        var action = component.get("c.updateStageToPrediscover");
        action.setParams(
            {
                OppId:oppId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.fireToastSuccess("Stage changed to prediscover"); 
                var compEvent = component.getEvent("RefreshEvent");
                compEvent.fire();
            }
            else
            {
                helper.fireToastError(response.getError()[0].message); 
            }
        });
        $A.enqueueAction(action);
    }
})