({
    init: function (component, event, helper) 
    {   
        var productList = component.get("v.productList");
        var opportunityProductList = component.get("v.opportunityProductList");
        var index;
        
        
        var action = component.get("c.getOpportunityClosedDate");
        var recordId = component.get("v.recordId");
        
        action.setParams({
            "opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){    
            
            var date1 = new Date(response.getReturnValue().closedDate);
            var loggedInUser = response.getReturnValue().loggedInUser;
            date1.setDate(date1.getDate() + 1);
            var curr_date = date1.getDate();
            var curr_month = date1.getMonth()+1; 
            var curr_year = date1.getFullYear();
            
            var requiredDate = curr_month + "/"+ curr_date + "/" +curr_year;
            component.set('v.loggedInUser', loggedInUser);
            
            for(index in productList){
                var product = productList[index];
                var opportunity = {
                    "Product2Id"  :  product.Id,
                    "ProductName" : product.Name,
                    "OpportunityId" : recordId,
                    "ContractTerm" : '',
                    "LocalCurrency" : 'USD',
                    "TotalPrice" : '0.00',
                    "RevenueStartDate" : requiredDate,
                    "DeliveringOrganisation" : '',
                    "SubDeliveringOrganisation" : '',
                    "ProductBDRep" :loggedInUser.Id,
                    "DeliveryLocation" : '',
                    "FTE" : '',
                    "SubDeliveringOrganizationList" : [],
                    "subOrganizationFlag" : true,
                    "RevenueExchangeRate" : '0.00'
                }
                opportunityProductList.push(opportunity);
            }
            component.set("v.opportunityProductList", opportunityProductList);
        });
        $A.enqueueAction(action);         
        
        helper.getLocalCurrency(component);
        helper.getDeliveringOrganization(component); 
        helper.getDeliveryLocation(component);   
        helper.getQSRMDetails(component);
    },
    
    getSubDeliveringOprganization :function(component, event, helper){
        var index =  event.currentTarget.getAttribute('data-record');  
        var OLIList = component.get("v.opportunityProductList");
        
        var selectedDeliveringOrganization = OLIList[index].DeliveringOrganisation;
        var action = component.get("c.getFieldDependencies");
        
        action.setParams({
            "controllingPickListValue" : selectedDeliveringOrganization
        });
        action.setCallback(this, function(response){            
            OLIList[index].subOrganizationFlag = false;
            OLIList[index].SubDeliveringOrganizationList = [];
            
            Array.prototype.push.apply(OLIList[index].SubDeliveringOrganizationList,  response.getReturnValue());
            component.set("v.opportunityProductList", OLIList); 
            
        });  
        $A.enqueueAction(action);
    },
    
    saveOpportunityProductList : function(component, event, helper){ 
        component.set("v.isLoading", true);
        var opportunityProductList = component.get("v.opportunityProductList");
        var index;
        var bdRepFlag = true;
        var contractTermFlag = true;
        var closedDateFlag = true;
        var parts;
        var revenueStartDate; 
        var closedDateParts = component.get("v.closedDate").split('-');
        var closedDate = new Date(closedDateParts[0], closedDateParts[1]-1,  closedDateParts[2], 0,0,0,0);
        var today = new Date();
        var subDeliveryOrganizationFlag = true;
        var loggedInUser = component.get('v.loggedInUser'); 
         var fteFlag = true;
        var tcvFlag = true;
        
        for(index in opportunityProductList){
            if(opportunityProductList[index].RevenueStartDate.indexOf('/') !== -1){
                parts =opportunityProductList[index].RevenueStartDate.split('/');
                revenueStartDate = new Date( parts[2], parts[0]-1,  parts[1], 0,0,0,0);
            }
            else{
                parts =opportunityProductList[index].RevenueStartDate.split('-');
                revenueStartDate = new Date( parts[1], parts[0]-1,  parts[2], 0,0,0,0);
            }
            if(isNaN(opportunityProductList[index].ContractTerm)){
                contractTermFlag = false;     
            }
            else{
                var contractTerm = Number(opportunityProductList[index].ContractTerm);
            }
            if(opportunityProductList[index].ProductBDRep){
                bdRepFlag = true;
            }
            else{
                bdRepFlag = false;   
                break;
            }
            if(contractTerm >0 && contractTerm % 1 === 0){
                contractTermFlag = true; 
                
            }            
            else{
                contractTermFlag = false;
                break; 
            }
            if( revenueStartDate.getTime() >= closedDate.getTime()){
                closedDateFlag = true;
                
            }
            else{
                closedDateFlag = false;
                break;
            }
            if(!isNaN(opportunityProductList[index].FTE)){
                    if(opportunityProductList[index].FTE % 1 === 0){
                        fteFlag = true;
                    }
                    else{
                        fteFlag = false;
                        break;
                    }
                }
                else if(!opportunityProductList[index].FTE || opportunityProductList[index].FTE == 0){
                    fteFlag = true;
                }
                else{
                    fteFlag = false;
                    break;     
                }
                
                if(!isNaN(opportunityProductList[index].TotalPrice) || (!opportunityProductList[index].TotalPrice 
                          || opportunityProductList[index].TotalPrice == 0)){
                    tcvFlag = true;
                }
                else{
                    tcvFlag = false;
                    break;     
                }
            if(opportunityProductList[index].SubDeliveringOrganizationList.length > 0 && 
               opportunityProductList[index].SubDeliveringOrganisation){
                subDeliveryOrganizationFlag = true;
            }
            else if(opportunityProductList[index].SubDeliveringOrganizationList.length == 0 &&
                    !opportunityProductList[index].SubDeliveringOrganisation){
                subDeliveryOrganizationFlag = true;
            }
                else if(opportunityProductList[index].SubDeliveringOrganizationList.length > 0 &&
                        !opportunityProductList[index].SubDeliveringOrganisation){
                    subDeliveryOrganizationFlag = false;
                    break;
                }  
        }
        if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
        	closedDateFlag = true;	    
        }
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        
        if (allValid && contractTermFlag && closedDateFlag && bdRepFlag && subDeliveryOrganizationFlag && fteFlag && tcvFlag) {
            if(component.get("v.QSRMStatus") == 'Your QSRM is submitted for Approval' && loggedInUser.Profile.Name != 'Genpact Super Admin'){
                component.set("v.errorMessage", "While QSRM is in approval status, the deals cannot be edited.");
                component.set("v.isError", true);
                component.set("v.isLoading", false);
                window.setTimeout(
                    $A.getCallback(function() {
                        if(component.isValid()){
                            component.set("v.isError", false);    
                        }    
                    }),2000
                );
            }
            else{
                var recordId = component.get("v.recordId");
                var action  = component.get("c.saveOpportunityProducts");
                
                
                function isLightningExperienceOrSalesforce1() {
                    return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
                }         
                
                action.setParams({
                    "opportunityProductList" : JSON.stringify(opportunityProductList),
                    "closedDate" : component.get("v.closedDate")
                });
                
                action.setCallback(this, function(response){ 
                    if(response.getReturnValue()){
                        component.set("v.isSuccess", true);
                        if(isLightningExperienceOrSalesforce1()) {
                            var urlEvent = $A.get("e.force:navigateToURL");
                            if(urlEvent) {
                                urlEvent.setParams({
                                    "url": "/apex/OpportunityProductDetailPage",
                                    "isredirect": "true",
                                    "oppID" : recordId
                                });
                                urlEvent.fire();
                            }
                            else{
                                window.location = '/apex/OpportunityProductDetailPage?recordID='+recordId;
                            }
                        }
                        else{
                            window.location = '/apex/OpportunityProductDetailPage?recordID='+recordId;
                        }
                    }
                    else{
                        component.set("v.errorMessage", "Please Enter the correct values and try again.");
                        component.set("v.isError", true);
                        component.set("v.isLoading", false);
                        window.setTimeout(
                            $A.getCallback(function() {
                                if(component.isValid()){
                                    component.set("v.isError", false);    
                                }    
                            }),2000
                        );
                    }
                    
                });
                $A.enqueueAction(action);    
            }
        } 
        else{
            if(!contractTermFlag){
                component.set("v.errorMessage", "Please enter contract term greater than 0");
            }
            else if(!closedDateFlag){
                component.set("v.errorMessage", "Revenue Start Date should be greater than MSA /SOW Closure Date");
            }
            else if(!bdRepFlag){
            	component.set("v.errorMessage", "Please enter Product BD Rep");
            } 
            else if(!subDeliveryOrganizationFlag){
            	component.set("v.errorMessage", "Please enter Sub delivery Organization");
            }
            else if(!fteFlag){
            	component.set("v.errorMessage", "Make Sure there is no decimal value / special character like comma / space in FTE.");   
            }
            else if(!tcvFlag){
            	component.set("v.errorMessage", "Make Sure there is no decimal value / special character like comma / space in TCV.");   
            }
            else if(!allValid){
            	component.set("v.errorMessage", "Please fill all fileds");
            }
            component.set("v.isLoading", false);
            component.set("v.isError", true);
            window.setTimeout(
                $A.getCallback(function() {
                    if(component.isValid()){
                        component.set("v.isError", false);    
                    }    
                }),2000
            );
        }
        
    },
    
    handleClickCancel : function(component, event, helper){
        //for redirecting to opportunityPage
       /* var recordId = component.get("v.recordId");
         function isLightningExperienceOrSalesforce1() {
            return((typeof sforce != 'undefined') && sforce && (!!sforce.one));
        }   
         if(isLightningExperienceOrSalesforce1()) {
        	sforce.one.navigateToSObject(recordId,"OPPORTUNITY");  
         }
        else{
           window.location = '/'+recordId; 
        }*/
        var event = $A.get("e.c:NavigateToSearchProduct");
        event.setParams({
            "selectedProductList" : component.get("v.productList"),
            "recordId" : component.get("v.recordId")  
        });
        event.fire();
    },
    
    closeSuccessToast : function(component, event, helper){
       component.set("v.isSuccess", false);
    },
    
    closeErrorToast : function(component, event, helper){
    	component.set("v.isError", false);
	}
})