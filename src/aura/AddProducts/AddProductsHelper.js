({
	 getLocalCurrency : function(component){
        var action = component.get("c.getLocalCurrency");
        action.setCallback(this, function(response){            
            component.set("v.LocalCurrency", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
     getDeliveringOrganization : function(component){
        var action = component.get("c.getDeliveringOrganization");
        action.setCallback(this, function(response){            
            component.set("v.DeliveringOrganization", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
        
     getDeliveryLocation : function(component){
        var action = component.get("c.getDeliveryLocation");
        action.setCallback(this, function(response){            
            component.set("v.DeliveryLocation", response.getReturnValue());
        });
        $A.enqueueAction(action);
    },
    
    getOpportunityClosedDate : function(component, event, helper){
       var action = component.get("c.getOpportunityClosedDate");
       var recordId = component.get("v.recordId");
        action.setParams({
            	"opportunityID" : recordId
        });
        
        action.setCallback(this, function(response){     
            var date1 = new Date(response.getReturnValue());
            
            var curr_date = date1.getDate() + 1;
            var curr_month = date1.getMonth() ; 
            var curr_year = date1.getFullYear();
            
            var requiredDate = curr_date + "/"+ curr_month + "/" +curr_year;
        });
        $A.enqueueAction(action); 
    },
    
    getQSRMDetails : function(component, event, helper){
        var action = component.get("c.getQSRMDetails");
        action.setParams({
            "opportunityID" : component.get("v.recordId")
        });
        
        action.setCallback(this, function(response){
            var returnedValue = response.getReturnValue();
            
            component.set("v.closedDate", returnedValue.oppClosedDate);
            component.set("v.endDate", returnedValue.endDate);
            component.set("v.QSRMStatus", returnedValue.QSRMStatus);
        });
        $A.enqueueAction(action);
    }
    
})