<apex:component controller="ApprovalQsrmController" access="global" >
    <apex:attribute name="QsrmId" type="Id" assignTo="{!qId}" description="This is the Id of the qsrm" access="global" />
    <style>
        td, th {border : 1px solid #ececec; padding: 0; }
        th {background : #dcdaff;}
    </style>
    {!QSRMObjList[0].Name} created on &nbsp; <apex:outputText value="{0,date,MM/dd/yy}"> <apex:param value="{!QSRMObjList[0].CreatedDate}" /></apex:outputText> has been submitted for your approval.This QSRM has scored RED/AMBER in “Ability to Deliver” section. Your response is mandatory for next steps.
    <br/>
    If you record <b>‘Approve’</b>, the QSRM will be presented to the vertical TS leader for final approval.<br/>
    If you record <b>‘Reject’</b>, the deal will be dropped from SFDC. <br/>
    If you record <b>‘Rework’</b>, the QSRM will be sent back to the opportunity owner for further action.<br/>   
    Please find the details below 
    <br/><br/>
    <b>Account Name:</b> {!QSRMObjList[0].Account_Name__c}<br/>
    <b>Account Archetype:</b> {!QSRMObjList[0].Account_Priority_from_Account__c}<br/>
    <br/>
    <table border = "1" width="50%">
        <tr>
            <td width="50%"> Reason for pursuing Opportunity</td>
            <td width="50%"> {!QSRMObjList[0].In_case_of_non_named_account__c} </td>
        </tr>
        <tr>
            <td> Comments</td>
            <td>  </td>
        </tr>
    </table>
    <br/>
    <b>Opportunity Name:</b> {!OpptyList[0].Name}<br/>
    <b>Opportunity Details:</b><br/><br/>
    
    <table border = "1" width="50%">
        <tr>
            <td width="50%"> Summary of the Opportunity</td>
            <td width="50%"> {!OpptyList[0].Description} </td>
        </tr>
        <tr>
            <td> Client Situation</td>
            <td> {!OpptyList[0].Summary_of_opportunity__c} </td>
        </tr>
        <tr>
            <td> Client Challenge</td>
            <td> {!OpptyList[0].Current_challenges_client_facing__c} </td>
        </tr>
        <tr>
            <td> Genpact Value Proposition</td>
            <td> {!OpptyList[0].End_objective_the_client_trying_to_meet__c} </td>
        </tr>
        <tr>
            <td> Win Theme</td>
            <td> {!OpptyList[0].Win_Theme__c} </td>
        </tr>      
    </table>
    <br/>
    <b>Product Details:</b> <br/><br/>
    <table border = "1" width="50%" >
        <tr>
            <th > Nature of Work</th>
            <th > Service Line</th>
            <th > Product Name</th>
            <th > TCV</th>               
        </tr>
        <apex:repeat value="{!oliList}" var="oli">
            <tr>
                <td> {!oli.Nature_of_Work__c} </td>
                <td> {!oli.Service_Line__c} </td>
                <td> {!oli.Product_Name__c} </td>
                <td> {!oli.TCV__c} </td>            
            </tr>
        </apex:repeat>        
    </table> <br/>
    <b>QSRM Form:</b> <br/><br/>
    <table border = "1" width="50%">
        <tr>
            <td width="50%"><b> Alignment to our Priorities</b></td>
            <td width="50%" > </td>
        </tr>
        <tr>
            <td> What is our track record at this client</td>
            <td> {!QSRMObjList[0].What_is_our_track_record_at_this_client__c} </td>
        </tr>
        <tr>
            <td> Alignment with Client Business Objective</td>
            <td> {!QSRMObjList[0].Does_this_project_align_with_the_client__c} </td>
        </tr>
        <tr>
            <td> Ability to achieve target margin</td>
            <td> {!QSRMObjList[0].Does_the_client_rate_card_or_proposed_c__c} </td>
        </tr>
        <tr>
            <td> Reason for Non-Named account/ Others</td>
            <td> {!QSRMObjList[0].In_case_of_non_named_account__c} </td>
        </tr>
        <tr>
            <td> Alignment to Priorities Score</td>
            <td> {!QSRMObjList[0].Alignment_to_Priorities_Score__c} </td>
        </tr>
        <tr>
            <td>Alignment to Priorities Rating</td>
            <td> {!QSRMObjList[0].Alignment_to_Priorities_Color_Formula__c}</td>
        </tr>
    </table>
    <br/>
    <table border = "1" width="50%">
        <tr>
            <th width="50%"><b> Intent to Buy</b></th>
            <th width="50%" > </th>
        </tr>
        <tr>
            <td> Approved budget</td>
            <td> {!QSRMObjList[0].Does_the_client_have_an_approved_budget__c} </td>
        </tr>
        <tr>
            <td> Clear client sponsor/Decision maker</td>
            <td> {!QSRMObjList[0].Is_there_a_clear_client_sponsor_decision__c} </td>
        </tr>
        <tr>
            <td> Was client bought in</td>
            <td> {!QSRMObjList[0].Has_the_client_sponsor_decision_maker__c} </td>
        </tr>
        <tr>
            <td> Do we understand client buying process</td>
            <td> {!QSRMObjList[0].Do_we_understand_the_client__c} </td>
        </tr>
        <tr>
            <td>Intent to Buy Score</td>
            <td> {!QSRMObjList[0].Intent_to_Buy_Score__c} </td>
        </tr>
        <tr>
            <td>Intent to Buy Rating</td>
            <!--<td> <img src="url" /></td>-->
            <td> {!QSRMObjList[0].Intent_to_Buy_Color_Formula__c} </td> 
        </tr>
    </table>
    <br/>
    <table border = "1" width="50%">
        <tr>
            <th width="50%"><b> Potential to Win</b></th>
            <th width="50%" > </th>
        </tr>
        <tr>
            <td> Value proposition and win theme</td>
            <td> {!QSRMObjList[0].How_strong_is_our_value_propostion_and_w__c} </td>
        </tr>
        <tr>
            <td> Do we have right relationship strength</td>
            <td> {!QSRMObjList[0].Do_we_have_the_right_relationship_streng__c} </td>
        </tr>
        <tr>
            <td> Offering fit with clients needs</td>
            <td> {!QSRMObjList[0].How_well_does_our_offering_fit__c} </td>
        </tr>
        <tr>
            <td>Do we know our competition</td>
            <td> {!QSRMObjList[0].Do_we_know_our_competition__c} </td>
        </tr>
        <tr>
            <td>Potential to Win Score</td>
            <td> {!QSRMObjList[0].Potential_to_Win_Score__c} </td>
        </tr>
        <tr>
            <td>Potential to win Rating</td>
            <td> {!QSRMObjList[0].Potential_to_Win_Color_Formula__c} </td>
        </tr>
    </table>
    <br/>
    <table border = "1" width="50%">
        <tr>
            <th width="50%"><b> Deliverability</b></th>
            <th width="50%" > </th>
        </tr>
        <tr>
            <td> Do we have Right skill in right location</td>
            <td> {!QSRMObjList[0].Do_we_have_the_right_level_of_domain__c} </td>
        </tr>
        <tr>
            <td> Commercial Model expected to be</td>
            <td> {!QSRMObjList[0].What_is_the_commercial_model_expected_to__c} </td>
        </tr>
        <tr>
            <td> Deliver within clientpricing expectation</td>
            <td> {!QSRMObjList[0].Can_the_solution_be_delivered_by_each__c} </td>
        </tr>
        <tr>
            <td>Deliverability Score</td>
            <td> {!QSRMObjList[0].Deliverability_Score__c} </td>
        </tr>
        <tr>
            <td>Deliverability Rating</td>
            <td> {!QSRMObjList[0].Deliverability_Color_Formual__c} </td>
        </tr>
    </table>
    <br/>
    <table border = "1" width="50%">
        <tr>
            <th width="50%"><b> OVERALL RATING</b></th>
            <th width="50%" > </th>
        </tr>
        <tr>
            <td> Overall TS QSRM Score</td>
            <td> {!QSRMObjList[0].Overall_Score__c} </td>
        </tr>
        <tr>
            <td> Overall TS QSRM Rating</td>
            <td> {!QSRMObjList[0].Overall_Rating_Color_Formula__c} </td>
        </tr>
    </table>
    
    <br/><b>QSRM approval guidelines:</b><br/>
        Please click on the link to view <a href="https://genpact--c.ap15.content.force.com/servlet/servlet.FileDownload?file=0152v00000G0Evt">guidelines to approve QSRM</a>  
        <br/><br/>
        <b>Recording your Response to TS QSRM:</b><br/>
        <b><u>You can record your response (APPROVE/ REJECT/ REWORK) either by responding to the initial email trigger from SFDC, or by following the below steps:</u></b>
        <br/><br/>
        1. Access the TS QSRM by clicking on the link <a href="{!$Label.TS_QSRM_Link}{!QSRMObjList[0].Id}">{!$Label.TS_QSRM_Link}{!QSRMObjList[0].Id}</a><br/>
        2. Log into the Salesforce mobile app and follow the <a href="https://genpact--c.ap15.content.force.com/servlet/servlet.FileDownload?file=0152v00000G0Evj">steps</a><br/>
        3. Log into Salesforce browser using SSO and follow the <a href="https://genpact--c.ap15.content.force.com/servlet/servlet.FileDownload?file=0152v00000G0Evo">steps </a><br/>
        <br/>
        Regards<br/>
        Genpact SFDC team<br/><br/>
        PS: In case of any query please write to <a href="mailto:sfdc.helpdesk@genpact.com">sfdc.helpdesk@genpact.com</a> <br/>
</apex:component>