trigger updateOpp on Contract (After Insert, After Update, After Delete) {
    
    Integer Count=0;
    String statusAll='';
    Integer Approved_count=0;
    List<opportunity> Opp=new list<opportunity>();
    List <contract> Contract_list_opp=new list<contract>();
    set<id> oppid=new set<id>();
    
    TRY{
        IF(Trigger.isinsert||trigger.isupdate) 
        {   
            For(Contract c:Trigger.New)
            {    
                oppid.add(c.Opportunity_tagged__c);
                
                
                
                
                
            }
            
        }   
        
        
        
        If(trigger.isdelete)
        {       
            For(Contract c:Trigger.old)
            {    
                oppid.add(c.Opportunity_tagged__c);
                
                
                
                
                
            }
            
            
        }
        
        system.debug('hi before query');
        Contract_list_opp=[select id,Status,Survey_Initiated_contract__c from contract where Opportunity_tagged__c IN :oppid ];
        system.debug('hi44'+Contract_list_opp.size());
        For(Opportunity o: [select id,Number_of_Contract__c,StageName,W_L_D__c,Contract_Status__c,survey_initiated_status_from_contract__c from opportunity where id IN :oppid])
        {        
            Count=0;
            Approved_count=0;
                    system.debug('hi in opp static loop');
                    if(trigger.isdelete && Contract_list_opp.size()<1)
                o.Contract_Status__c='';
            
            For(Contract c : Contract_list_opp)
            {
             
                    statusAll=statusAll+','+c.status;
                      //  system.debug('hi in contract loop'+statusAll);
                Count=Count+1;
                
                if(c.Survey_Initiated_contract__c ==true)
                    o.survey_initiated_status_from_contract__c='True';
                
                
                if(c.Status=='Activated')   
                    Approved_count=Approved_count+1;
                
if(statusAll!='')
                    o.Contract_Status__c =statusAll.substring(1,statusAll.length()); 
                                system.debug('hi in contract loop'+o.Contract_Status__c);
                System.debug('Opp_Count'+Count);
                
                
                
            }
            
            o.Number_of_Contract__c = Count;
            
            if(Approved_count==Count && o.W_L_D__c=='Signed' && Count>0)
                o.StageName='6. Signed Deal';
            
            Opp.add(o);
            
            
        }
        
        if(!Opp.isempty())
            Update Opp;
        
    }
    
    catch(Exception e)
    {
        //throw e;
        if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
        {   
            
            Trigger.new[0].Adderror('Please update MSA date on opportunity');
            
        }
        
        
        
        else 
            Trigger.new[0].Adderror( e.getStackTraceString() );
        
        
        
    }
    
}