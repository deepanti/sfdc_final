trigger AccountCreationRequestTrigger on Account_Creation_Request__c (after insert, after update) {
    if(AccountCreationRequestTriggerHandler.FirstRun == TRUE) {
        AccountCreationRequestTriggerHandler.FirstRun = FALSE;
        AccountCreationRequestTriggerHandler.createNewAccount(Trigger.new);
    }
}