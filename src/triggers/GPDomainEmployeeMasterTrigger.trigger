trigger GPDomainEmployeeMasterTrigger on GP_Employee_Master__c (
    after delete, after insert, after update, after undelete, before delete, before insert, before update)
{
    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_employee_master__c')
       && MapofProjectTrigger.get('gp_employee_master__c').GP_Enable_Sobject__c
      && SetToRunEMPMasterORHRMSTrigger.isRunEmpMasterTrigger != 'N')
    {
        fflib_SObjectDomain.triggerHandler(GPDomainEmployeeMaster.class);
    }
}