//AS Sales Leaders as Shared Accounts and Opportunity using Sharing Rules
//We need this trigger to share the corresponding OLIs of the Opportunity visible
//this has a dependency to run the Dashboards.
trigger ShareOLIWithAccountSalesLead on Account (after insert, after update,before update)
{
    list<OpportunityProduct__Share> deleteolisharing=new list<OpportunityProduct__Share>();
    set<OpportunityProduct__Share> setvalues=new set<OpportunityProduct__Share>();
     list<OpportunityProduct__Share> OpportunityProductShareToInsert = new list<OpportunityProduct__Share>();
    map<id,account> oldaccountbulkmap=new map<id,account>();
    if(!singleexecuion.bool_ShareOLIWithAccountSalesLead)
    {
        singleexecuion.bool_ShareOLIWithAccountSalesLead = true;
        set<Id> SalesUnitIdSet = new set<id>();
        Map<Id,Account> AccountsToProcess = new Map<Id,Account>();
        if(trigger.isbefore)
        {
           if(trigger.isupdate) 
           {
            //map<id,OpportunityProduct__Share> mapofOpportunityProductShare=new map<id,OpportunityProduct__Share>();
            /*<<Date Modified: 23 April 2015, Deployment Date: May 1 2015, Bhanu Prasad, Sameer Shah, Jatin>> : On Account Owner Edit the new Account owner is added in the team of all the OLI's tagged to the account, exception for all closed opportunties & no change in owner */
            list<OpportunityProduct__Share> listofOpportunityProductSharedelete=new list<OpportunityProduct__Share>();
            
               for(account accountobj:trigger.new)
            {
                account oldmapofaccount=trigger.oldmap.get(accountobj.id);
                //oldaccountbulkmap.put(oldmapofaccount.ownerid, accountobj);
                if(oldmapofaccount.ownerid!=accountobj.ownerid)
                {
                	oldaccountbulkmap.put(oldmapofaccount.ownerid, accountobj);
                
                }
                system.debug('old map ownerid'+oldmapofaccount.ownerid);
                system.debug('New map ownerid'+accountobj.ownerid);
            }
                
                map<id,opportunity> checkingclosedoppty=new map<id,opportunity>();
               if(!oldaccountbulkmap.isEmpty())
               {
                list<opportunity> Checkingforclosedoppty=[select id,accountid,Stagename,ownerid from opportunity where ownerid=:oldaccountbulkmap.keySet() AND(Stagename='6. Signed Deal' OR Stagename='8. Dropped' OR Stagename='8. Dropped')];
                if(Checkingforclosedoppty.size()>0)
                {
                 for(opportunity opp:Checkingforclosedoppty)
                 {
                     checkingclosedoppty.put(opp.ownerid, opp);
                 }
            }
               
                    
                    /*for(OpportunityProduct__Share OpportunityProductShareobj:[SELECT Id, UserOrGroupId,rowcause FROM OpportunityProduct__Share where Rowcause = 'Manual'] )
                    {
                        mapofOpportunityProductShear.put(OpportunityProductShareobj.UserOrGroupId,OpportunityProductShareobj);
                    }*/
                    list<OpportunityProduct__Share> listofOpportunityProductShareobj=[SELECT Id, UserOrGroupId,rowcause FROM OpportunityProduct__Share where UserOrGroupId =:oldaccountbulkmap.keySet() AND Rowcause = 'Manual'];
                    
                    
                   if(listofOpportunityProductShareobj.size()>0)
                   {
                    for(OpportunityProduct__Share OpportunityProductSharelist:listofOpportunityProductShareobj )
                    {
                    
                        listofOpportunityProductSharedelete.add(OpportunityProductSharelist);  
                        

                   }
                   }
               if(listofOpportunityProductSharedelete.size()>0)
               {
                        for(OpportunityProduct__Share deletesharing:listofOpportunityProductSharedelete)
                        {
                            if(!setvalues.contains(deletesharing))
                            {
                                setvalues.add(deletesharing);
                            }
                        }
           }
               if(setvalues.size()>0)
               {
                        deleteolisharing.addAll(setvalues);
               }
                        
                
                
            
               if(deleteolisharing.size()>0)
               {
                    try
                        {
                            system.debug('delete list'+deleteolisharing);
                            delete deleteolisharing;
                        }
                        catch(exception e)
                        {
                            system.debug('error occured due to'+e);
                        }
               }
               }
        }
    }
        for(Account TempAccountObj : Trigger.new)
        {
            AccountsToProcess.put(TempAccountObj.Id,TempAccountObj);
            if(TempAccountObj.Sales_Unit__c != null)
            {
                SalesUnitIdSet.add(TempAccountObj.Sales_Unit__c);
                
            }
        }
        list<opportunity> listofopptyandproductonbasedaccount=[select id,name,AccountId,ownerId,(select id,name,ownerId from Opportunity_Products__r) from Opportunity where AccountId in : AccountsToProcess.keyset()]; 
        Map<Id,list<OpportunityProduct__c>> accountofolimap=new Map<Id,list<OpportunityProduct__c>> ();
      
        if(listofopptyandproductonbasedaccount.size()>0)
            {
                for(opportunity oppobj:listofopptyandproductonbasedaccount)
                {
                    if(accountofolimap.containsKey(oppobj.accountid))
                    {
                        list<OpportunityProduct__c> TempOpportunityProductLst = new list<OpportunityProduct__c>();
                        TempOpportunityProductLst.addAll(accountofolimap.get(oppobj.accountid));
                        TempOpportunityProductLst.addAll(oppobj.Opportunity_Products__r);
                        accountofolimap.put(oppobj.accountid,TempOpportunityProductLst);
                    }
                    else
                    {
                       accountofolimap.put(oppobj.accountid,oppobj.Opportunity_Products__r); 
                    }
                    
                }
                system.debug('account map based on oli'+accountofolimap);
                if(accountofolimap.size()>0)
                {
                    for(id accountidkey:accountofolimap.keySet())
                    {
                        for(OpportunityProduct__c tempproductobj:accountofolimap.get(accountidkey))
                            {
                              if(AccountsToProcess.containsKey(accountidkey)) 
                              {
                                  if(AccountsToProcess.get(accountidkey).ownerid !=tempproductobj.ownerid)
                                  {
                                      OpportunityProduct__Share AccountownerOLIShr  = new OpportunityProduct__Share();
                                AccountownerOLIShr.ParentId = tempproductobj.id;
                                AccountownerOLIShr.UserOrGroupId = AccountsToProcess.get(accountidkey).ownerid ;
                                AccountownerOLIShr.AccessLevel = 'Read';
                                OpportunityProductShareToInsert.add(AccountownerOLIShr);
                                      
                                  }
                              }
                            }
                        system.debug('opportunity product oli share'+OpportunityProductShareToInsert);
                    }
                }
            }
        if(SalesUnitIdSet.size()>0)
        {
        Map<Id,Sales_Unit__c> SalesUnitMap = new Map<Id,Sales_Unit__c>([select id,name,Sales_Leader__c from Sales_Unit__c where Id in :SalesUnitIdSet]);
        list<Opportunity> LstOfAllOpportunityForAccount = [select id,name,AccountId,ownerId,(select id,name,ownerId from Opportunity_Products__r) from Opportunity where AccountId in : AccountsToProcess.keyset()]; 
        Map<Id,list<OpportunityProduct__c>> LstOfOLIOfAccountMap = new Map<Id,list<OpportunityProduct__c>>();
        if(LstOfAllOpportunityForAccount.size()>0)
        {
            for(Opportunity TempOppObj : LstOfAllOpportunityForAccount)
            {
                   if(LstOfOLIOfAccountMap .containskey(TempOppObj.AccountId))
                   {
                       list<OpportunityProduct__c> TempOpportunityProductLst = new list<OpportunityProduct__c>();
                       TempOpportunityProductLst.addall(LstOfOLIOfAccountMap.get(TempOppObj.AccountId));
                       TempOpportunityProductLst.addall(TempOppObj.Opportunity_Products__r);
                       LstOfOLIOfAccountMap.put(TempOppObj.AccountId,TempOpportunityProductLst);
                   }
                   else
                   {
                       LstOfOLIOfAccountMap.put(TempOppObj.AccountId,TempOppObj.Opportunity_Products__r);
                   }
            }
            if(LstOfOLIOfAccountMap.size()>0)
            {
               
                for(Id AccountIdKey : LstOfOLIOfAccountMap.keyset())
                {
                    for(OpportunityProduct__c TempOppProductObj : LstOfOLIOfAccountMap.get(AccountIdKey))
                    {
                        if(AccountsToProcess.containskey(AccountIdKey))
                        {
                            if(SalesUnitMap.get(AccountsToProcess.get(AccountIdKey).Sales_Unit__c).Sales_Leader__c != TempOppProductObj.ownerId)
                            {
                                OpportunityProduct__Share AccountSalesLeadOLIShr  = new OpportunityProduct__Share();
                                AccountSalesLeadOLIShr.ParentId = TempOppProductObj.id;
                                AccountSalesLeadOLIShr.UserOrGroupId = SalesUnitMap.get(AccountsToProcess.get(AccountIdKey ).Sales_Unit__c).Sales_Leader__c;
                                AccountSalesLeadOLIShr.AccessLevel = 'Read';
                                OpportunityProductShareToInsert.add(AccountSalesLeadOLIShr); 
                            }
                        }
                    }
                }
                
            }
            
            
        }
        }
           // Insert the sharing record and capture the save result. 
          // The false parameter allows for partial processing if multiple records passed 
          // into the operation.  
          if(OpportunityProductShareToInsert.size()>0)
          {
          system.debug('%#%@#$#%#'+OpportunityProductShareToInsert);
          list<Database.SaveResult> srList = Database.insert(OpportunityProductShareToInsert,false);
          for (Database.SaveResult sr : srList) {
    if (sr.isSuccess()) {
        // Operation was successful, so get the ID of the record that was processed
        System.debug('Successfully inserted OLI sharing. OLI sharing: ' + sr.getId());
    }
    else {
        // Operation failed, so get all errors                
        for(Database.Error err : sr.getErrors()) {
            System.debug('The following error has occurred.');                    
            System.debug(err.getStatusCode() + ': ' + err.getMessage());
            System.debug('OLI fields that affected this error: ' + err.getFields());
        }
    }
          }
  } 
       
              }
}