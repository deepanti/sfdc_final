trigger GENtCalculateAndUpdateOpportunity on OpportunityProduct__c (after insert, after update, after delete ,before delete ) {

        Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
        Map<Id, Id> accountOwnerMap = new Map<Id, Id>();
        Map<Id, Id> accountGRMMap = new Map<Id, Id>();
        Map<Id, Id> accountClientPartnerMap = new Map<Id, Id>();
        Map<string, AggregateResult> oppProductAggregateResultMap = new Map<string, AggregateResult>();
        Set<Id> opportunityIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        List<Opportunity> oppList = new List<Opportunity>();
        List<Opportunity> oppWithProducts = new List<Opportunity>();
        Map<String,String> oppstage = new Map<String,String>();
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
String MyProfileName = PROFILE[0].Name;
        Opportunity opp =new Opportunity();
        if(Trigger.isDelete)
        {   
            for(OpportunityProduct__c rec: trigger.old){
                opportunityIds.add(rec.OpportunityId__c);
            }
        }else{
            for(OpportunityProduct__c rec: trigger.new){
                opportunityIds.add(rec.OpportunityId__c);
            }
        }
        
        oppWithProducts = [select id,CMITs_Check__c,Revenue_Product__c,highest_Digital_Offering__c,Nature_of_Work_highest__c,Highest_Revenue_Product__c,CloseDate,stagename,Name,count_no_of_lines__c,Pricing_Inserted__c, ProductionTCV__c,ProductionACV__c,  ProductionCYR__c,  ProductionNYR__c,Product_Family_Savo__c,Service_Line_Savo__c,Product_Information__c,
                          (select Id, name,TCVLocal__c,Product_Group__c,COE__c, TotalContractValue__c,Overwrite_values__c,  ACV__c, CurrentYearRevenue__c, NextYearRevenue__c,Product__c,Product_family_Lookup__c,Service_Line_lookup__c,Product__r.name,Product_family_Lookup__r.name,Service_Line_lookup__r.name, Nature_of_Work_lookup__c, Nature_of_Work_lookup__r.name, Total_TCV__c,Product_Family_OLI__c,Digital_Offerings__c,Nature_of_Work_Sharing__c,Digital_TCV_dollar__c from Opportunity_Products__r) 
                          from Opportunity where id IN:opportunityIds ]; //ACV__c,
        for(Opportunity opp1: oppWithProducts)  
        {   
            for(OpportunityProduct__c oppprod : opp1.Opportunity_Products__r)
            oppstage.put(oppprod.id,opp1.stagename);                  
        }
        
        system.debug('oppstage==========='+oppstage.size()+'  '+oppstage);
        List<AggregateResult> aggResult = [Select  opportunityId__c, Max(EndDate__c) maxenddate,   Min(RevenueStartDate__c) minRevenueStartDate, Max(ContractTermInMonths__c) maxContractTerm FROM OpportunityProduct__c where opportunityId__c IN:opportunityIds group by opportunityId__c];
        for(AggregateResult agg: aggResult ){
            oppProductAggregateResultMap.put(String.valueOf(agg.get('opportunityId__c')), agg);
         }
          
         if((Trigger.isDelete || Trigger.isBefore) && (MyProfileName !='Genpact Super Admin' && MyProfileName !='Genpact Shared Services'))
         {      
            IF((oppstage.get(trigger.old[0].id) == '6. Signed Deal') || (oppstage.get(trigger.old[0].id) == '7. Lost') || (oppstage.get(trigger.old[0].id) == '8. Dropped'))
                trigger.old[0].adderror('Since Deal is closed, you are not authorised to delete this record');
         }
         
        if(Trigger.isAfter){
            
            if(Trigger.isInsert){
            try{
               // GENcGrantSharingOnOLI.grantSharing(trigger.new); 
                GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap);  
             }
             catch(Exception e)
                    {
                     throw e;
                    }
            }
            
            if(Trigger.isUpdate){
            try{
                GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap);  
            }
             catch(Exception e)
                    {
                     throw e;
                    }
            }
            if(Trigger.isDelete)
            {   
            
                /*try{
                    if(oppWithProducts[0].CloseDate < system.today())
                    {
                    system.debug('CheckDate'+oppWithProducts[0].CloseDate);
                
                    trigger.new[0].adderror('MSA /SOW Closure Date cannot be less than today');
                    }
                    else
                    GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap); 
                    }
                    catch(exception e){
                     
                      System.debug('The following exception has occurred: ' + e.getMessage());
                    }
                */
                    //GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap);
                    try{                
      
                        
                        GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap); 
                    }               
                    catch(System.DmlException e)
                    {
                       String ErrorMsg = 'You are not authorised to delete this record';
                       trigger.old[0].adderror(ErrorMsg,false);
                    }

                }
                 
            }
        
}