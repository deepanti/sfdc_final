/*  --------------------------------------------------------------------------------------
      Description: this trigger will Created DSR , Send mails To Tower Lead 
      
      --------------------------------------------------------------------------------------
      events : before insert, after insert, after update
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           05-10-2016                              1.0   
      --------------------------------------------------------------------------------------
      Amit                            28-11-2016                              2.0   
      --------------------------------------------------------------------------------------
 */


trigger GW1_TriggerOnOpportunityCreateDSR on Opportunity(before update,after update,before insert) {

    GW1_triggerSettings__c objSettings = GW1_triggerSettings__c.getValues('GW1_TriggerOnOpportunityCreateDSR');
     for(Opportunity  Opp: trigger.new){
     system.debug('@OLICount'+ Opp.count_no_of_lines__c);
    if (objSettings !=null && objSettings.GW1_Is_Active__c && Opp.count_no_of_lines__c !=1)
    {
        GW1_OpportunityTriggerHandler objHandler = new GW1_OpportunityTriggerHandler();
        objHandler.runTrigger();
    }
    else
    {
        System.debug('=====TRIGGER GW1_TriggerOnOpportunityCreateDSR Incative From Custom Setting=====');
    }
 }
}