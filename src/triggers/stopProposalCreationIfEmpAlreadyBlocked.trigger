trigger stopProposalCreationIfEmpAlreadyBlocked on Proposal__c (before insert) 
{  
   
    List < Id > empmasterIds = new List < Id >();
    List < Id > proposalIds = new List < Id >();

    Profile auth_profile = [select id from profile where name='HS_StandardUser_RMG']; 
    Id v_User = UserInfo.GetProfileId();
    
    for ( Proposal__c  c: Trigger.New ) 
    {
        proposalIds.add( c.Id );
        empmasterIds.add( c.RMG_Employee_Code__c);
    }
    
    List<RMG_Employee_Master__c> opps = [select id, Proposal_Status__c from RMG_Employee_Master__c where id in :empmasterIds];
   
    Map < Id ,RMG_Employee_Master__c > empmap = new Map < Id , RMG_Employee_Master__c >();

    for ( RMG_Employee_Master__c  a : opps   ) 
    {
        empmap.put( a.Id, a);
    }

    List < RMG_Employee_Master__c > EmpToUpdate = new List < RMG_Employee_Master__c >();
    
    
  for(Proposal__c c: Trigger.New) 
    {
         if (c.Status__c != 'Likely Extension'){
            RMG_Employee_Master__c ac = empmap.get( c.RMG_Employee_Code__c );         
            if ( ac == null ) 
            {    
              continue;
            }
        system.debug('line36');
            /* Get all proposals for this employee. */          
            List<Proposal__c> proObj1 = [select Id,Name,Employee_Email_Address__c,Status__c,RMG_Employee_Code__c from Proposal__c where Id !=:c.Id and RMG_Employee_Code__c =:ac.Id  ];
            for(Proposal__c pc1: proObj1 )
            {
                system.debug('----------->'+pc1.Name + ' ' +pc1.Status__c);
                if( pc1.Status__c == 'Selected-New' || pc1.Status__c == 'Blocked' || pc1.Status__c == 'Selected-Extended')
                {
                    c.addError('Employee already selected or blocked. No new Proposal can be created till that block is removed.');
                    //Trigger.new[0].Status__c.addError('If the Proposal with Status as Selected / Blocked exists, no new Proposal can be created');
                }
                        system.debug('line47');
            }
        }
        
               
    } 
}//End of Trigger