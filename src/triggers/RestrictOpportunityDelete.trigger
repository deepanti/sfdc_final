trigger RestrictOpportunityDelete on Opportunity (before delete) 
 { 
     if(label.RestrictOpportunityDeleteFlag == 'false'){
      ID PID= userinfo.getProfileId();

  if(System.Trigger.IsDelete)
   { 
    for (Opportunity opp: trigger.old) 
    {
      if (opp.Opportunity_ID__c != null && PID<>'00e9000000125i2' && PID<>'00e90000001aFVI'  )
 { 
   opp.addError('You cannot delete an Opportunity.Please contact your Salesforce.com Administrator for assistance.');
        }
    } 
   } 
     
 }}