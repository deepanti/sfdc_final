trigger updatecurrentallocationstatus on RMG_Employee_Allocation__c (after insert,after update) 
{
   List<RMG_Employee_Master__c> empMaster = new List<RMG_Employee_Master__c>();
   List<RMG_Employee_Allocation__c> empAllocations = new List<RMG_Employee_Allocation__c>();
   
   List<ID> masterIds = new List<ID>();
   
   Map<ID, String> childDetailMap = new Map<ID, String>();
   
   for(RMG_Employee_Allocation__c c: Trigger.new)
   {
      masterIds.add(c.RMG_Employee_Code__c);
      childDetailMap.put(c.RMG_Employee_Code__c, (c.Client__c));
      
   }
  
   empMaster = [select id from RMG_Employee_Master__c where id in :masterIds];
   
   for(RMG_Employee_Master__c rem: empMaster)
   {
        String currentAllocation=null;
        List<ID> dummyIds = new List<ID>();
        dummyIds.add(rem.id);
        empAllocations = [select id, Client__c, Status__c,is_Zero_PRF__c,Active__c,Project_Category__c from RMG_Employee_Allocation__c where Status__c!='Past' AND Active__c != 'No' AND RMG_Employee_Code__c in :dummyIds];
    
        integer allocationType = -1;
        /*
            -1: Unknown
            0: Only Bench
            1: Only Non-Bench
            2: Both Bench and Non-Bench
            3:Internal Project
        */
    
        for(RMG_Employee_Allocation__c empAllocation: empAllocations)
        {
            //Iterating over all allocations
            if(empAllocation.Project_Category__c=='SGA' && empAllocation.Client__c == 'Bench' )
            {
              allocationType=3;
              break;
              
            }
            else if (empAllocation.Client__c == 'Bench')
            {
                if (allocationType > 0){
                    allocationType = 2;
                } else {
                    allocationType = 0;
                }
            } else {
                if (allocationType == -1){
                    allocationType = 1;
                } if (allocationType == 1){
                    allocationType = 1;
                } else {
                    allocationType = 2;
                }
            }  
        }

        if (allocationType < 0){
            currentAllocation = '';
        } 
        else if(allocationType ==3)
        { 
          currentAllocation='Internal Project';
          }
        else if (allocationType == 0){
            currentAllocation = 'Bench';
        } else {
            currentAllocation = 'Billing-Zero PRF';
            for(RMG_Employee_Allocation__c empAllocation: empAllocations)
            {
                //Iterating over all allocations
                if (empAllocation.Client__c != 'Bench')
                {
                                        
                    if (empAllocation.is_Zero_PRF__c != '1' )
                    {
                       currentAllocation = 'Billing';
                       break;
                    }
                }
                
                 
               
            }
        }      
        rem.Allocation_Status__c =   currentAllocation;
   }
     
   Update empMaster;
                   
}