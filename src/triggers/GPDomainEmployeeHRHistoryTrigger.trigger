trigger GPDomainEmployeeHRHistoryTrigger on GP_Employee_HR_History__c (after delete, after insert, after update, after undelete, before delete, before insert, before update) 
{
     map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
        if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_employee_hr_history__c')
           && MapofProjectTrigger.get('gp_employee_hr_history__c').GP_Enable_Sobject__c && SetToRunEMPMasterORHRMSTrigger.isRunHrmsTrigger !='N')
        {
            fflib_SObjectDomain.triggerHandler(GPDomainEmployeeHRHistory.class);
        }
}