trigger UpdateOppwithBarometerflag on RevenueStormRB__RevenueStorm_Relationship_Barometer__c (After Insert, After delete) 
{

    If(Trigger.isinsert)
    {
        List<Opportunity> OpptoUpdate= new List<Opportunity>();
        List<Opportunity> FinalOpptoUpdate= new List<Opportunity>();
        Set<id> OppID= new Set<id>();
        
        
        For(RevenueStormRB__RevenueStorm_Relationship_Barometer__c RB : Trigger.New)
            {    
                    OppID.add(RB.RevenueStormRB__Opportunity__c);
                                   
            }
        
        OpptoUpdate=[Select id,Assocaie_RS_barometer__c from opportunity where ID in :OppID];
        
        For(Opportunity o: OpptoUpdate)
        {    
            o.Assocaie_RS_barometer__c=True;
            FinalOpptoUpdate.add(o);
        }
        
        Update FinalOpptoUpdate;
        
        
    
    }

    Else
    {    integer Count=0;
        Set<id> OppId = new set<id>();
        List<Opportunity> OpptoUpdate= new List<Opportunity>();
        List<RevenueStormRB__RevenueStorm_Relationship_Barometer__c> RBTochk = New List<RevenueStormRB__RevenueStorm_Relationship_Barometer__c>();
        List<Opportunity> FinalOpptoUpdate= new List<Opportunity>();
        For(RevenueStormRB__RevenueStorm_Relationship_Barometer__c RB : Trigger.old)
            {    
                    OppID.add(RB.RevenueStormRB__Opportunity__c);
                                   
            }
            
        
         Count=[select COUNT() from RevenueStormRB__RevenueStorm_Relationship_Barometer__c where RevenueStormRB__Opportunity__c in :OppID];  
         OpptoUpdate=[Select id,Assocaie_RS_barometer__c from opportunity where ID in :OppID];
            
            If (Count == 0)
            {
                For(Opportunity o: OpptoUpdate)
                    {    
                        o.Assocaie_RS_barometer__c=False;
                        FinalOpptoUpdate.add(o);
                    }
                    
                    Update FinalOpptoUpdate;
            }
    
    
    }



}