trigger UpdateToweremail on GW1_DSR__c (After insert, After update) {
    
    List<Id> DSRID= new list<id>();
    List<GW1_DSR__c> dsrobj= new list<GW1_DSR__c>();
    List<GW1_Tower_Leader_Mapping__c> TLMAP =new List<GW1_Tower_Leader_Mapping__c>();
    String Email='';
    String Email2='';
    String TLName='';
    List<GW1_DSR__c> dsrtoupdate = new List<GW1_DSR__c>();
    
    if(Validator_cls.AllowGenTUpdateAccounArche)
    {
        Validator_cls.AllowGenTUpdateAccounArche = false;
        
        
        if(trigger.isafter && (trigger.isInsert || trigger.isupdate))
        {
            FOR(GW1_DSR__c dsr : trigger.new)
            {
                
                DSRID.add(dsr.id);
                
            }
        }
        
        dsrobj=[select id,GW1_sales_Region__c,product_family__c,GW1_Industry_Vertical__c,GW1_Service_Line_SAVO__c,Type_of_DSR__c,Tower_Lead_Email_DL__c,Tower_Lead_Email_2__c from GW1_DSR__c where id in :DSRID];
        TLMAP=[select id,Name,Tower_Lead_Name__c,GW1_Distribution_List__c,GW2_Nature_of_Work__c,GW2_Sales_Reagion__c,GW1_Industry_Vertical__c,GW1_Product_Family__c,GW3_Distribution_List__c from GW1_Tower_Leader_Mapping__c];
        
        
        
        FOR(GW1_DSR__c d : dsrobj)
        {
            if(d.Type_of_DSR__c=='Consulting'){
                if(d.Type_of_DSR__c!=null){
                    if(d.Type_of_DSR__c!=null){
                        if(d.Type_of_DSR__c!=null){
                            if(d.Type_of_DSR__c!=null){
                                if(d.Type_of_DSR__c!=null){
                                    if(d.Type_of_DSR__c!=null){
                                        if(d.Type_of_DSR__c!=null){
                                            if(d.Type_of_DSR__c!=null){
                                                if(d.Type_of_DSR__c!=null){}
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                FOR(GW1_Tower_Leader_Mapping__c tlmapiing : TLMAP)
                {if(d.GW1_Industry_Vertical__c == tlmapiing.GW1_Industry_Vertical__c &&  d.GW1_sales_Region__c == tlmapiing.GW2_Sales_Reagion__c)
                {
                    Email= tlmapiing.GW1_Distribution_List__c;  
                    TLName= tlmapiing.Tower_Lead_Name__c;
                    Email2= tlmapiing.GW3_Distribution_List__c;
                    
                    
                }
                 
                }
                
                
                d.Tower_Lead_Email_DL__c =Email;
                d.Tower_Lead_Email_2__c = Email2;
                d.Tower_lead_Name__c=TLName;
                
                dsrtoupdate.add(d); 
            }    
            else {
                FOR(GW1_Tower_Leader_Mapping__c tlmapiing : TLMAP)
                { String pf=d.Product_Family__c;
                 if(d.Product_Family__c!=null)
                     pf=d.Product_Family__c.split(',')[0];
                 if(d.GW1_Industry_Vertical__c == tlmapiing.GW1_Industry_Vertical__c &&  pf == tlmapiing.GW1_Product_Family__c)
                 {
                     Email= tlmapiing.GW1_Distribution_List__c;  
                     Email2= tlmapiing.GW3_Distribution_List__c;
                     //TLName= tlmapiing.Tower_Lead_Name__c;
                     
                     
                 }
                 
                }
                
                d.Tower_Lead_Email_DL__c =Email;
                d.Tower_lead_Name__c=TLName;
                d.Tower_Lead_Email_2__c = Email2;
                
                dsrtoupdate.add(d); 
            } 
        }
        
        if(!dsrtoupdate.isempty())
            update dsrtoupdate;
        
        
        
    }
    
}