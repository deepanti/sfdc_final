trigger LinkAccountWithAccountCrationRequest on Account (After insert)
{
    list<Account_Creation_Request__c> ACRToUpdate = new list<Account_Creation_Request__c>();
   
   if(!Test.isRunningTest()){  if(Trigger.isInsert && CheckRecursiveForOLI.runOnceAfter_1()){for(Account TempObj : trigger.new){if(TempObj.ACR_15_Digit_ID__c != null){Account_Creation_Request__c ACRObj = new Account_Creation_Request__c(id=TempObj.ACR_15_Digit_ID__c);ACRObj.Account_Creation_Status__c = 'Account Created'; ACRToUpdate.add(ACRObj); } } if(ACRToUpdate.size()>0) update ACRToUpdate; } }  
}