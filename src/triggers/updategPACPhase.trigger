trigger updategPACPhase on Proposal__c(after delete, after insert,after update) 
{
   
   if (Trigger.isDelete)
   {
   List<RR__c> currentRR = new List<RR__c>();
   List<Proposal__c> currentpropstatus = new List<Proposal__c>();
   
   List<ID> masterIds = new List<ID>();
        
   for(Proposal__c c: Trigger.old)
   {
      masterIds.add(c.Proposal_RR__c);    
   }
  
   currentRR = [select id from RR__c where id in :masterIds];
   system.debug('Current RR'+currentRR);
   
        for(RR__c rem: currentRR)
        {
        List<ID> dummyIds = new List<ID>();
        dummyIds.add(rem.id);
        currentpropstatus = [select Proposal_RR__c,RMG_Employee_Code__c,CAR_Candidate_Code__c from Proposal__c where Status__c!='Closed' AND Proposal_RR__c in:dummyIds];
        system.debug('Current Proposal'+currentpropstatus);
        String currentstatus=null; 
        
        integer allocationType = -1;
        /*
            -1: Unknown
            0: Only Employees
            1: Only CAR
            2: Both Employees and CAR
        */

        for(Proposal__c PropStatus: currentpropstatus)
            {
            //Iterating over all allocations
            if (PropStatus.RMG_Employee_Code__c != null)
            {
                if (allocationType > 0){
                    allocationType = 2;
                } else {
                    allocationType = 0;
                }
            } else {
                if (allocationType == -1){
                    allocationType = 1;
                } if (allocationType == 1){
                    allocationType = 1;
                } else {
                    allocationType = 2;
                }
            }
            }  
            
            if (allocationType < 0){
            currentstatus = '';
        } else if (allocationType == 0){
            currentstatus = 'RMG Employees Proposal';
        } else {
            currentstatus = 'CAR Candidates Proposal';
            for(Proposal__c PropStatus: currentpropstatus)
            {
                //Iterating over all allocations
                if (PropStatus.RMG_Employee_Code__c != null)
                {
                    if ((PropStatus.RMG_Employee_Code__c != null) && (PropStatus.RMG_Employee_Code__c != null))
                    {
                       currentstatus = 'RMG Employees and CAR Candidates Proposal';
                       break;
                    }
                }
            }           
          }
               
        rem.gPAC__c =   currentstatus;
   }
     
   Update currentRR;
   }else 
   {
   for (Proposal__c Prop : Trigger.new)
   {
   if (Prop.Proposal_RR__c <> null)
   {
   List<RR__c> currentRR = new List<RR__c>();
   List<Proposal__c> currentpropstatus = new List<Proposal__c>();
   
   List<ID> masterIds = new List<ID>();
        
   for(Proposal__c c: Trigger.new)
   {
      masterIds.add(c.Proposal_RR__c);    
   }
  
   currentRR = [select id from RR__c where id in :masterIds];
   system.debug('Current RR'+currentRR);
   
        for(RR__c rem: currentRR)
        {
        List<ID> dummyIds = new List<ID>();
        dummyIds.add(rem.id);
        currentpropstatus = [select Proposal_RR__c,RMG_Employee_Code__c,CAR_Candidate_Code__c from Proposal__c where Status__c!='Closed' AND Proposal_RR__c in:dummyIds];
        system.debug('Current Proposal'+currentpropstatus);
        String currentstatus=null; 
        
        integer allocationType = -1;
        /*
            -1: Unknown
            0: Only Employees
            1: Only CAR
            2: Both Employees and CAR
        */

        for(Proposal__c PropStatus: currentpropstatus)
            {
            //Iterating over all allocations
            if (PropStatus.RMG_Employee_Code__c != null)
            {
                if (allocationType > 0){
                    allocationType = 2;
                } else {
                    allocationType = 0;
                }
            } else {
                if (allocationType == -1){
                    allocationType = 1;
                } if (allocationType == 1){
                    allocationType = 1;
                } else {
                    allocationType = 2;
                }
            }
            }  
            
            if (allocationType < 0){
            currentstatus = '';
        } else if (allocationType == 0){
            currentstatus = 'RMG Employees Proposal';
        } else {
            currentstatus = 'CAR Candidates Proposal';
            for(Proposal__c PropStatus: currentpropstatus)
            {
                //Iterating over all allocations
                if (PropStatus.RMG_Employee_Code__c != null)
                {
                    if ((PropStatus.RMG_Employee_Code__c != null) && (PropStatus.RMG_Employee_Code__c != null))
                    {
                       currentstatus = 'RMG Employees and CAR Candidates Proposal';
                       break;
                    }
                }
            }           
          }
               
        rem.gPAC__c =   currentstatus;
   }
     
   Update currentRR;
   }      
   }          
}
}