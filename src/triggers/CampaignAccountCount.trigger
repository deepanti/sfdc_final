/******************************************************************************************************************
* @author   Persistent 
* @description -This trigger will handle all the logic related to getting the number of accounts in a campaign.
*******************************************************************************************************************/
trigger CampaignAccountCount on CampaignMember(after insert, after delete, after update){
    
    
    /***********************************************************************************************************
* Create and instace of handler class and start using the class  
************************************************************************************************************/
    CampaignMemberTriggerHandler handler = new CampaignMemberTriggerHandler();
    
    
    /***********************************************************************************************************
* Handle all business logic for Before Insert
************************************************************************************************************/
    if(Trigger.isInsert && Trigger.isAfter){       
        handler.onAfterInsertforAccountCount();
    }
    if(Trigger.isUpdate && Trigger.isAfter){       
        handler.onAfterInsertforAccountCount();
    }
    
    
}