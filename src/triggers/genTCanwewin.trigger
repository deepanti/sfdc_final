/* This trigger is used to insert the colour name in text field as formula hit the char limit
Can We Win secion is computed in QSRM by using this logic
TS QSRM overall score is also updated in this now.
Version 1.0, Created By: Amit Chaturvedi, Created date: 8th,July,2015, Deployment date: 19th, July,2015*/
trigger genTCanwewin on QSRM__c (before Insert, before update)
{
    for(QSRM__c  qs: trigger.new)
    {   
        Integer excludeTotal=0;
        If (qs.Can_Premise_be_shared_to_other_customers__c =='I don\'t know')
            excludeTotal=excludeTotal+10;
        If (qs.Strong_bus_case_opp__c =='Don\'t know')
            excludeTotal=excludeTotal+5;
        If (qs.What_margin_potential_deal__c =='Don\'t know')
            excludeTotal=excludeTotal+5;
        If (qs.What_degree_risk_associated__c =='Don\'t know')
            excludeTotal=excludeTotal+10;
        If(qs.What_are_facility_Options_post_Rebadge__c=='I don\'t know')
            excludeTotal=excludeTotal+10;
        
        excludeTotal = 100 - excludeTotal;
        qs.Rebadge_Insights_scoreTEXT_c__c=((qs.Score_Client_fully_loaded_costs__c 
                                             + qs.score_Existing_Genpact_site_in_the_count__c 
                                             + qs.score_What_are_facility_post_Rebegged__c 
                                             + qs.Rebadge_risk_associated__c 
                                             + qs.score_Existing_forums_of_employee_repres__c 
                                             + qs.Rebadge_Do_we_have_a_strong_business_cas__c 
                                             + qs.Rebadge_What_is_the_margin_associated__c 
                                             + qs.score_Can_Premise_be_shared_to_other_cus__c)/(excludeTotal))*100;
    
            
            
            
            if(qs.Knowledge_on_decision_making_process__c=='Don\'t know' && qs.Relationship_with_decision_makers__c=='Don\'t know' &&(qs.Our_reputation_with_client__c=='Don\'t know' || qs.Our_previous_track_record_with_prospect__c=='Don\'t know') && qs.Can_we_build_a_differentiated_solution__c=='Don\'t know' && qs.Who_influenced_the_RfP_requirements__c=='Don\'t know' && qs.Incumbent_strength__c=='Don\'t know' && qs.Relationship_with_third_party_advisors__c=='Don\'t know')
        {
            qs.Can_we_win_Trigger__c='RED';
        }
        
        else If(qs.Is_this_a_RFP_or_Sole_sourced_deal__c=='Sole Sourced') 
        {   
            if(qs.Our_reputation_with_client__c=='Promoter'||qs.Our_previous_track_record_with_prospect__c=='Strong history'||qs.Relationship_with_decision_makers__c=='Strong connects with all decision makers and key influencers'|| qs.Can_we_win_score__c > 10)
            {
                qs.Can_we_win_Trigger__c='GREEN';
            }
            else 
            {
                qs.Can_we_win_Trigger__c='YELLOW';
            }
        }
        else 
        {
            if((qs.Our_reputation_with_client__c=='Promoter'|| qs.Our_previous_track_record_with_prospect__c=='Strong history')&&(qs.Can_we_build_a_differentiated_solution__c=='Yes - clearly differentiated' && qs.Relationship_with_decision_makers__c=='Strong connects with all decision makers and key influencers'))
            {   
                qs.Can_we_win_Trigger__c='GREEN';
            }
            else if (qs.Can_we_win_score__c > 10)
            {   
                qs.Can_we_win_Trigger__c='GREEN';
            }
            else if (qs.Can_we_win_score__c > -8 )
            {
                if (qs.Can_we_win_score__c < 11 )
                    if (qs.Can_we_win_score__c < 11 )
                    if (qs.Can_we_win_score__c < 11 )
            {   
                qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
                 qs.Can_we_win_Trigger__c='YELLOW';
            }
            }
            else 
            {
                qs.Can_we_win_Trigger__c='RED';
                
            }
        }
        //code for TS QSRM overall rating color in text field
         if (qs.Overall_Score__c < 10)
        {
            qs.Overall_Rating_Color_Formula__c ='RED';
        }
        else if (qs.Overall_Score__c  >=10 && qs.Overall_Score__c< 18)
        {
            qs.Overall_Rating_Color_Formula__c  ='AMBER';
           
        }
        else if (qs.Overall_Score__c  >=18 )
        {
            qs.Overall_Rating_Color_Formula__c ='GREEN';
        }

    }
}