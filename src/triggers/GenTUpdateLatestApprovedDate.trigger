/* This trigger is updating Opportunity field - Latest_Approvedd_QSRM_Date__c & QSRM_Approved__c*/


trigger GenTUpdateLatestApprovedDate on QSRM__c (after insert,after update,after delete) {

QSRM__c QSRM;

//if (User.Profile == )

 if(trigger.isDelete)
{

QSRM= Trigger.old[0]; 

}         
else 
{QSRM= Trigger.new[0];
}

Opportunity opp = [select id,StageName,TCV1__c,Roll_up_QSRM_App_Rej__c,Target_Source__c,QSRM_Approval_Required__c, Formula_Hunting_Mining__c,Account.Business_Group__c,Latest_Approvedd_QSRM_Date__c,(select id,Approved_Date__c,Status__c from QSRMs__r LIMIT 1) from Opportunity where id=:QSRM.Opportunity__c]; 

if(trigger.isDelete)
{

List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
String MyProfileName = PROFILE[0].Name;
             
    IF((opp.StageName!='1. Discover' ||opp.StageName!='8. Dropped'||opp.StageName!='7. Lost' || opp.StageName!='6. Signed Deal' )&& ((opp.Formula_Hunting_Mining__c=='Hunting')||(opp.Formula_Hunting_Mining__c=='Mining'&& opp.TCV1__c >=250000 && opp.Account.Business_Group__c=='Global Clients'&&((opp.Target_Source__c!='Ramp up')||(opp.Target_Source__c!='Renewal') )) || (opp.TCV1__c >=500000 && opp.Formula_Hunting_Mining__c=='Mining' && opp.Account.Business_Group__c=='GE') || opp.QSRM_Approval_Required__c ) && (opp.Roll_up_QSRM_App_Rej__c<2) && (MyProfileName !='Genpact Super Admin' && MyProfileName !='Genpact Shared Services' && MyProfileName !='Quest Super Admin' && MyProfileName !='Genpact SL / CP'))
        {
        Trigger.old[0].adderror('Atleast one approved QSRM is MANDATORY. Hence you cannot delete the QSRM');
        }
    else if
    ((opp.StageName=='6. Signed Deal' || opp.StageName=='7. Lost' || opp.StageName=='8. Dropped')&& (MyProfileName !='Genpact Super Admin' && MyProfileName !='Genpact Shared Services') )
      {
        Trigger.old[0].adderror('Since Deal is closed, you are not authorized to delete this record');
        }  
}
 
List<QSRM__c> qss= [select id,Approved_Date__c,Status__c from QSRM__c where Opportunity__c=:opp.id];

opp.Latest_Approvedd_QSRM_Date__c = null;
opp.QSRM_Approved__c=False;

for(QSRM__c qs: qss)
    {
    
     If(qs.Status__c=='Approved')
     {opp.Latest_Approvedd_QSRM_Date__c = qs.Approved_Date__c;}
    // else
     //{opp.Latest_Approvedd_QSRM_Date__c = null;}
     
    //opp.QSRM_Approved__c=False;
  
   If(qs.Status__c=='Approved' && opp.QSRM_value_not_approved_rejected__c>0)
   
     {opp.QSRM_Approved__c=True;}
    system.debug('@@@@'+opp.QSRM_value_not_approved_rejected__c); 
   /*If(opp.QSRM_value_not_approved_rejected__c==0)
   {
   opp.QSRM_Approved__c=False;
   }*/
     
    // else
      //{opp.QSRM_Approved__c=False;}
        
     }
     try{
    update opp;
    }
    catch(System.DmlException e)
    {
    /*
        String ErrorMsg = '';
        for (Integer i = 0; i < e.getNumDml(); i++)
        {
          ErrorMsg = ErrorMsg + String.valueof(e.getDmlMessage(0));
        }
       ErrorMsg = ErrorMsg + '</br> Indupal';
       trigger.new[0].adderror(ErrorMsg,false);
       */
       String ErrorMsg = 'Please contact to SFDC helpdesk';
       trigger.new[0].adderror(ErrorMsg,false);
    }
       // }
 



}