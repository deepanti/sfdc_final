/*********************************************

Descripton : Script to Update Annuity_Project__c only for CMIT Data
Date       : 11/06/2014
version    : 1.0
Author     : Pankaj Jangra
********************************************/

trigger Gen_DataloadScriptOpportunity on Opportunity (before Insert, before update) {
    
    for(Opportunity var : Trigger.new){
        if(var.Legacy_CMIT_ID__c != null){
            if(var.Staff_Aug__c){
                var.Annuity_Project__c = 'Staff Aug';
            }else if(var.ProductionTCV__c != null && var.ProductionTCV__c != 0 && var.ProductionACV__c != null && var.ProductionACV__c != 0 && (var.ProductionTCV__c/var.ProductionACV__c >= 2)){
                var.Annuity_Project__c = 'Annuity';
            }else{
                var.Annuity_Project__c = 'Project';
            }

            if(var.Advisor_Firm__c == null || var.Advisor_Firm__c == ''){
                var.Deal_Administrator__c = 'In-house';
            }else{
                var.Deal_Administrator__c = 'Analyst/Advisory';
            }
        }
    }
    
}