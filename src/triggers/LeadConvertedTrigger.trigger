trigger LeadConvertedTrigger on Lead (after update) {
	//List<Contact_Role_Rv__c> ContactRoleList = new List<Contact_Role_Rv__c>();
	List<Opportunity> opportunityList = new List<Opportunity>();
    Set<ID> opportunityIDs = new Set<Id>();
    try{
        for(Lead lead_obj : trigger.new){
            if(lead_obj.IsConverted){
                opportunityIDs.add(lead_obj.ConvertedOpportunityId);
            }
        }   
        List<opportunitycontactRole> contact_role_list = [SELECT id, contactID, isPrimary, role, opportunityId from OpportunityContactRole 
                                                          WHERE opportunityId = :opportunityIDs];
        
        Map<ID, opportunity> opportunity_Map = new Map<ID, Opportunity>([SELECT id, contact1__c, role__c from Opportunity
                                                                         WHERE id = :opportunityIDs]);
        for(OpportunityContactRole oppContactRole : contact_role_list){
            Opportunity opp = opportunity_map.get(oppcontactRole.OpportunityId);
            opp.Contact1__c = oppContactRole.ContactId;
            opp.Role__c = oppContactRole.Role;
            opportunityList.add(opp);        
        }
        update opportunityList;
         OpportunityTriggerHelper.updateContactRole(opportunityList, opportunity_Map);
    }
    catch(Exception e){
        System.debug('ERROR=='+e.getStackTraceString()+' '+e.getMessage());
    }
    
}