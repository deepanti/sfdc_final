trigger updateRollOffDate on RMG_Employee_Allocation__c (after insert,after update) 
{
   List<RMG_Employee_Master__c> empMaster = new List<RMG_Employee_Master__c>();
   //List<RMG_Employee_Allocation__c> empAllocations = new List<RMG_Employee_Allocation__c>();
 
   List<ID> masterIds = new List<ID>();
   Map<ID, Date> childDetailMap = new Map<ID, Date>();
   
   for(RMG_Employee_Allocation__c c: Trigger.new)
   {
      masterIds.add(c.RMG_Employee_Code__c);
      childDetailMap.put(c.RMG_Employee_Code__c,(c.End_Date__c));
   }
  
   empMaster = [select id, Roll_Off_Date__c from RMG_Employee_Master__c where id in :masterIds];
   
   for(RMG_Employee_Master__c empmast: empMaster)
   {
        List<ID> dummyIds = new List<ID>();
        dummyIds.add(empmast.id);
        List<RMG_Employee_Allocation__c> empAllocations = [select id, Start_Date__c, End_Date__c from RMG_Employee_Allocation__c where Status__c!='Past' AND RMG_Employee_Code__c in :dummyIds];
        
        Date rollOffDate = date.today()+1;
        While(true){         
            boolean allocationExists = false;
            for(RMG_Employee_Allocation__c empAllo: empAllocations) 
            {
                date startdate = empAllo.Start_Date__c;
                date enddate = empAllo.End_Date__c;
                if(rollOffDate >= startdate && rollOffDate <= enddate)
                {
                    allocationExists = true;
                    break;
                }
            }//for
            if (allocationExists){
                rollOffDate += 1;
            } else {
                break;
            }
        }//while
        empmast.Roll_Off_Date__c =   rolloffdate;
  }
  Update empMaster;
}