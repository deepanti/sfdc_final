trigger OpportunityTrigger on Opportunity (after insert, after update, after delete) {
 
    if(label.OpportunityTriggerFlag == 'false'){
    
    if(Trigger.isInsert){
        if(CheckRecursive.runOnce()){ 
            OpportunityTriggerHelper.insertContactRole(Trigger.new); 
            
            if(!Test.isRunningTest()){
                
                OpportunityTriggerHelper.saveDealCycleAging(Trigger.newMap.keySet());    
            }
            TaskCreationHandler.createTaskforOpp(Trigger.new);
            DeleteOppSPlitfromOpportunity.CreateOppTeamonOppCreation(Trigger.new);
            System.debug('Limit Query Rows OpportunityTrigger'+System.limits.getQueryRows());
        } 
        // Done by Pallavi
     /*   TaskCreationHandler.createTaskforOpp(Trigger.new);
        DeleteOppSPlitfromOpportunity.CreateOppTeamonOppCreation(Trigger.new);
          System.debug('Limit Query Rows OpportunityTrigger'+System.limits.getQueryRows());                        
   */ }    
   
    if(Trigger.isUpdate){
        if(CheckRecursive.runOnce() || Test.isRunningTest()){
       if(!Test.isRunningTest()){ 
                OpportunityTriggerHelper.saveDealCycleAging(Trigger.newMap.keySet());     
            }
            OpportunityTriggerHelper.updateContactRole(Trigger.new, Trigger.oldMap);
            
            // Done by Pallavi
            //For TASKCREATION TRIGGER USED IN TASKCREATIONHANDLER CLASS
            TaskCreationHandler.createTaskforOpponUpdate(Trigger.new, Trigger.oldMap);
            TaskCreationHandler.removeTaskforOpponUpdate(Trigger.new);
            //for Opportunity split and team insertion on Opp creation and Updates.
         //   DeleteOppSPlitfromOpportunity.deleteOppSplitOwneronOppOwnerchange(Trigger.new, Trigger.oldMap);
         
        } 
    }
   
    if(Trigger.isDelete){
        if(CheckRecursive.runOnce()){
            OpportunityTriggerHelper.deleteContactRole(Trigger.oldMap);
        } 
    }
}}