trigger proposalstatusforRR on Proposal__c (after update, after insert, after delete) 
{  
    if (Trigger.isDelete){
        /* This will always have only one Id */
        List < Id > empmasterIds = new List < Id >();
        List < Id > proposalIds = new List < Id >();
    
        for ( Proposal__c  c: Trigger.old) 
        {
            proposalIds.add( c.Id );
            empmasterIds.add( c.Proposal_RR__c);
        }
        
        /* This will contain the aggregated proposal status of the employee */
        List<RR__c> opps = [select id, Proposal_Status__c, Temp_Proposal_Status__c from RR__c where id in :empmasterIds];
    
        /* Mapping between employeeId and employee*/
        Map < Id ,RR__c > empmap = new Map < Id , RR__c >();
        for ( RR__c  a : opps   ) 
        {
            empmap.put( a.Id, a);
        }

        /* List of employees to be updated. This will have only one employee - the current employee.*/
        List < RR__c > EmpToUpdate = new List < RR__c >();
    
        /* Get the status of the new proposal */
        for(Proposal__c c: Trigger.old) 
        {
           system.debug('Inside Proposal Loop 2');
           /* Fetch the current employee */
           RR__c ac = empmap.get( c.Proposal_RR__c );         
           if (ac == null) 
           {    
               continue;
           }
        
           system.debug('----------->'+c.Status__c);
           /* Get all proposals for this employee and mark them as closed.  */
           String futureProposalStatus = 'Unassigned';
           List<Proposal__c> proObj = [select Id,Status__c,Proposal_RR__c from Proposal__c where Id !=:c.Id and Proposal_RR__c =:ac.Id ];
           for(Proposal__c pc: proObj )
           {
                if ((pc.Status__c == 'Proposed') 
                        || (pc.Status__c == 'Blocked')
                        || (pc.Status__c == 'Selected-New')
                        || (pc.Status__c == 'Likely Extension')
                        || (pc.Status__c == 'Selected-Extended')){
                    futureProposalStatus = pc.Status__c;
                } 
           }//for
           ac.Proposal_Status__c = futureProposalStatus;
           ac.Temp_Proposal_Status__c = futureProposalStatus;
           EmpToUpdate.add(ac);
           system.debug('Inside Proposal Loop 2: '+futureProposalStatus);
         }//For Trigger.old
         upsert EmpToUpdate;       
    } else {
    /* After Insert, After Update */
        /* This will always have only one Id*/
        List < Id > empmasterIds = new List < Id >();
        List < Id > proposalIds = new List < Id >();

        for ( Proposal__c  c: Trigger.New ) 
        {
            proposalIds.add( c.Id );
            empmasterIds.add( c.Proposal_RR__c);
        }
    
        /* This will contain the aggregated proposal status of the employee */
        List<RR__c> opps = [select id, Proposal_Status__c,Temp_Proposal_Status__c from RR__c where id in :empmasterIds];
    
        /* Mapping between employeeId and employee*/
        Map < Id ,RR__c > empmap = new Map < Id , RR__c >();
        for ( RR__c  a : opps   ) 
        {
            empmap.put( a.Id, a);
        }

        /* List of employees to be updated. This will have only one employee - the current employee. */
        List < RR__c > EmpToUpdate = new List < RR__c >();
    
        /* Get the status of the new proposal */
        for(Proposal__c c: Trigger.New) 
        {

        /* Fetch the current employee */
            RR__c ac = empmap.get( c.Proposal_RR__c );        
            if ( ac == null ) 
            {    
                  continue;
            }            

            /* If the new Proposal's status is equal to 'Blocked' or 'Selected' then close rest of the proposals. */
            if((c.Status__c == 'Selected-New') || (c.Status__c == 'Blocked') || (c.Status__c == 'Selected-Extended'))
            {
                system.debug('----------->'+c.Status__c);
                /* Get all proposals for this employee and mark them as closed. */
                List<Proposal__c> proObj = [select Id,Status__c,Proposal_RR__c from Proposal__c where Id !=:c.Id and Proposal_RR__c =:ac.Id ];
                for(Proposal__c pc: proObj )
                {
                   system.debug('BEFORE STATUS--->'+pc.Status__c);
          
                   if ((pc.Status__c != 'Closed')
                       && (pc.Status__c != 'Likely Extension')
                       && (pc.Status__c != 'Blocked')
                       && (pc.Status__c != 'Selected-New')
                       && (pc.Status__c != 'Selected-Extended')){             
                       pc.Status__c = 'Closed';
                       system.debug('AFTER STATUS--->'+pc.Status__c);
                       update pc;
                   }//if
                }//for
            }//if


            List<Proposal__c> currentOpenProposals = [select Id,Status__c,Proposal_RR__c from Proposal__c where Status__c != 'Closed' and Proposal_RR__c =:ac.Id ];
        
        String tempFPS = 'Unassigned';

      for(Proposal__c pc: currentOpenProposals)
      {
        if (tempFPS == 'Unassigned'){
            tempFPS = pc.Status__c;
        } else if ((tempFPS == 'Proposed') && (pc.Status__c == 'Proposed')){
            tempFPS = 'Proposed';
        } else if (  ((tempFPS == 'Proposed') && (pc.Status__c == 'Likely Extension'))
                || ((tempFPS == 'Likely Extension') && (pc.Status__c == 'Proposed')) ){
            tempFPS = 'Likely Extension';
        } else if ((tempFPS =='Selected-New') || (tempFPS =='Selected-Extended') || (tempFPS =='Blocked')){
            // No Action
        } else if (tempFPS =='Likely Extension'){
            tempFPS = pc.Status__c;
        } else {
            tempFPS = 'Indeterminate';
        }
        system.debug('tempFPS--->'+tempFPS);
        system.debug('Proposal Status--->'+pc.Status__c);

    }//for

        if (ac.Proposal_Status__c != tempFPS){
            ac.Proposal_Status__c = tempFPS;
            ac.Temp_Proposal_Status__c = tempFPS;
            EmpToUpdate.add( ac );
        }
          
         }//For Trigger.New
         upsert EmpToUpdate;       
     }//Else
}//End of Trigger