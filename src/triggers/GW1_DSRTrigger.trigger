/*  --------------------------------------------------------------------------------------
	  Description: this trigger will  add Opportunity Owner To DsrTeam,
	  update Owner  Of DSR Gropu When BidManager Changes on DSR,
	  
	  --------------------------------------------------------------------------------------
	  events : aafter insert, Before insert, after update, before update
	  --------------------------------------------------------------------------------------
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          05-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
 */

trigger GW1_DSRTrigger on GW1_DSR__c(after insert, Before insert, after update, before update)
{
	GW1_triggerSettings__c objSettings = GW1_triggerSettings__c.getValues('GW1_DSRTrigger'); // To Active and Inactive the trigger With Custom Setting
	
	
	if (objSettings != null && objSettings.GW1_Is_Active__c)
	{
		GW1_DSRtriggerHandler objHandler = new GW1_DSRtriggerHandler();
		objHandler.runTrigger();
	}
	else
	{
		System.debug('=====TRIGGER GW1_DSRTrigger In Active From Custom Setting=====');
	}
}