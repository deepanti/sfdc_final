//this trigger is used to update the field values on Opportunity Object from Qsrm object 

trigger QSRMToOppFieldUpdate on QSRM__c (after insert,after update,after delete) {
    
   //List of Opportunity to be updated
    List<Opportunity> lstToUpdateOpp=new List<Opportunity>();
    if(trigger.isInsert||trigger.isUpdate)
    {
        //fetching all the Qsrm field which we have to update
        for(Qsrm__c qsm:[select Id,Potential_significant_relationship__c,Name_of_Functional_SPOC_SMEs_from_Horiz__r.name,Opportunity__r.Id,Other_Competitor__c,Named_Competitors__c from Qsrm__c where Id IN:trigger.new])
        {
            Opportunity opp=new Opportunity();
            opp.Id=qsm.Opportunity__r.Id;
            opp.Functional_SPOC_SMEs_QSRM__c = qsm.Name_of_Functional_SPOC_SMEs_from_Horiz__r.name;
            opp.Potential_significant_relationship_QSRM__c=qsm.Potential_significant_relationship__c;
            opp.Other_Comp__c=qsm.Other_Competitor__c;
            opp.Named_Competitor_QSRM__c=qsm.Named_Competitors__c;
            system.debug('value of PotentialSignificatn'+opp.Potential_significant_relationship_QSRM__c);
            lstToUpdateOpp.add(opp);
            
        }
        update lstToUpdateOpp;
        system.debug('---update opp field is working fine---');
    }
     if(trigger.isDelete)
    {
      //this code is used to delete the updated field from Opportunity object when Qsrm is deleted
            for(Qsrm__c qsm:[select Id,Potential_significant_relationship__c,Functional_SPOC_SMEs1__c,Opportunity__r.Id from Qsrm__c where Id IN:trigger.old AND isDeleted=true ALL ROWS ])
            {
                Opportunity opp=new Opportunity();
                opp.Id=qsm.Opportunity__r.Id;
                opp.Functional_SPOC_SMEs_QSRM__c='';
                opp.Potential_significant_relationship_QSRM__c='';
                opp.Other_Comp__c='';
                opp.Named_Competitor_QSRM__c='';
                system.debug('value of PotentialSignificatn'+opp.Potential_significant_relationship_QSRM__c);
                lstToUpdateOpp.add(opp);
                
            }
            update lstToUpdateOpp;
            system.debug('---delete opp field value is working fine---');
        }
        
    
}