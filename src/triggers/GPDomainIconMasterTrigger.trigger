trigger GPDomainIconMasterTrigger on GP_Icon_Master__c (after delete, after insert, after update, after undelete, before delete, before insert, before update) 
  {
      map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
        if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_icon_master__c')
           && MapofProjectTrigger.get('gp_icon_master__c').GP_Enable_Sobject__c)
        {
            fflib_SObjectDomain.triggerHandler(GPDomainIconMaster.class);
        }
}