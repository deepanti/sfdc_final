/*it is used to check account team member's duplicacy and to insert , update and delete account team members into standard account team and standard account share object.
Last modified Date : 24/7/2015 By Jatin Bagri
*/
trigger TriggerToReplicateStandardAccTeam on AccountArchetype__c (before insert,  before update , After insert, After update, before delete) 
{
    system.debug('arrrrrrrrrr=='+'  '+trigger.isAfter+'    '+trigger.isbefore+'   '+trigger.isdelete+'   '+Validator_cls.AllowTriggerToReplicateStandardAccTeam);
    if(Validator_cls.AllowTriggerToReplicateStandardAccTeam)
    {
         //if(trigger.isAfter)
        //{
          //  Validator_cls.AllowTriggerToReplicateStandardAccTeam = false;
        //}
        Set<String> sAcc = new Set<String>();

        List<AccountTeamMember> AccList = new  List<AccountTeamMember>();
        
        List<AccountTeamMember> AccListUpdated = new List<AccountTeamMember>();    
        List<AccountTeamMember> AccListdeleted = new List<AccountTeamMember>();  
             
        List<AccountShare> AccShare= new List<AccountShare>();    
        List<AccountShare> AccSharedeleted = new List<AccountShare>();
        
        // Start Check Duplicate in account Team
      
        Map<String, AccountArchetype__c> archMap = new Map<String,AccountArchetype__c>();
        Map<String, AccountArchetype__c> archMapCode = new Map<String,AccountArchetype__c>();
        set<String> archSet =new set<String>();
     
        if(trigger.isBefore && !trigger.isDelete)
        {
            for (AccountArchetype__c oAcrCheck: System.Trigger.new) 
            {
                if ((oAcrCheck.User__c!= null) && (System.Trigger.isInsert ||(oAcrCheck.User__c!= System.Trigger.oldMap.get(oAcrCheck.Id).User__c))) 
                {
           
                    if (archMapCode.containsKey(oAcrCheck.User__c+'~'+oAcrCheck.Account__c)) 
                    {
                       oAcrCheck.User__c.addError('Account Team member with this name already exists for this Account.');
                    } 
                    else 
                    {
                        archMapCode.put(oAcrCheck.User__c+'~'+oAcrCheck.Account__c, oAcrCheck);
                    }
                }
                archSet.add(oAcrCheck.User__c);
           }
            
            for (AccountArchetype__c Acr : [SELECT User__c,Account__c FROM AccountArchetype__c WHERE User__c IN :archSet]) 
            {
                try
                {
                AccountArchetype__c newAcr = archMapCode.get(Acr.User__c+'~'+Acr.Account__c);
                newAcr.User__c.addError('Account Team member with this name already exists for this Account.');
                }
                catch(exception exp)
                {
                system.debug('Error'+exp);
                }
            }
        }
        
         // End Check Duplicate in account Team
         
 
       if(trigger.isAfter || trigger.isDelete){
         Map<Id,user> ActiveUserMap = new Map<Id,user>([select id,name from user where IsActive = true limit 50000]);
 
        set<String> accid = new Set<String>();
        
        for(AccountArchetype__c aatObj: trigger.isdelete?trigger.old:trigger.new)
        {
              if(aatObj.account__c!=null)
              { 
                 accid.add(aatObj.account__c); 
              }         
        } 
        
         
         
        /*if(trigger.isdelete){
        AccountArchetype__c old_aat=Trigger.old[0];
        AccountTeamMember acctm=[select id,UserId  from AccountTeamMember where UserId =:old_aat.User__c limit 1];
        AccountShare accshares =[Select AccountId,Id from AccountShare where accountid=:acctm.Id limit 1];
        delete accshares;
        delete acctm;
        }*/
           
        AccList = new List<AccountTeamMember>([SELECT AccountAccessLevel,AccountId,Id,TeamMemberRole,UserId FROM AccountTeamMember where AccountId in: accid]);       
        
        AccountShare[] newShare = new AccountShare[]{};  
                   
        newShare = new list<AccountShare>([SELECT AccountAccessLevel,AccountId,CaseAccessLevel,ContactAccessLevel,Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,RowCause,UserOrGroupId FROM AccountShare where AccountId in: accid]);       
        
        Map<string,AccountTeamMember> MapATM = new Map<string,AccountTeamMember>();              
        Map<string,AccountShare> MapAS = new Map<string,AccountShare>(); 
                   
        for(AccountTeamMember at : AccList)         
        {      
            system.debug('aaaaaaaa'+string.valueof(at.AccountId)+string.valueof(at.UserId));                           
            MapATM.put(string.valueof(at.AccountId)+string.valueof(at.UserId),at);  
        } 
         system.debug('Map of Account Team'+MapATM);            
        for(AccountShare ash : newShare)         
        {      
            system.debug('aaaaaaaa'+string.valueof(ash.AccountId)+string.valueof(ash.UserOrGroupId));                           
            MapAS.put(string.valueof(ash.AccountId)+string.valueof(ash.UserOrGroupId),ash);  
        } 
        system.debug('Map of Account Share'+MapAS); 
        Set<id> accArchType = new Set<Id>();
        if(!Trigger.isDelete)
         {
             for(AccountArchetype__c aObj: trigger.new){
                accArchType.add(aObj.id);
             }
         }  
        else{
             for(AccountArchetype__c aObj: trigger.old){
                accArchType.add(aObj.id);
             }
         }
         
        for(AccountArchetype__c a :[Select Account_Access__c,Case_Access__c,Opportunity_Access__c,Contact_Access__c,Archetype__c,Account__c,Id,Team_Member_Role__c,User__c,Account__r.Ownerid From AccountArchetype__c where id in:accArchType])
        {
        
            AccountArchetype__c sAccOld =new AccountArchetype__c();
           
           //if(trigger.isUpdate && a.id <> null && trigger.oldmap.get(a.id)<>null)
                //sAccOld = trigger.oldmap.get(a.id);
           
                system.debug('Acclist------>'+AccList);   
                AccountTeamMember ATM ; 
                AccountTeamMember ATMnew ;  
                AccountShare Acs ;
                AccountShare Asnew ;                  
            
           if(!trigger.isdelete)
           {
               if(ActiveUserMap.containsKey(string.valueof(a.User__c).substring(0,15)))  
               {
                    ATM = new AccountTeamMember();      
                    ATM.AccountId= a.Account__c ;
                    ATM.UserId= a.User__c;
                    ATM.TeamMemberRole = a.Team_Member_Role__c;     
                    AccListUpdated.add(ATM);         
                    system.debug('AccListUpdated'+AccListUpdated);        
                    
                    if(a.User__c!=a.Account__r.ownerid)
                    {  
                        Acs = new AccountShare();      
                        Acs.AccountId=a.Account__c ;
                        Acs.UserOrGroupId=a.User__c;
                       if( a.Account_Access__c=='Read Only'){
                        Acs.AccountAccessLevel='Read';}
                        else if(a.Account_Access__c=='Read/Write'){
                        Acs.AccountAccessLevel='Edit';}
                         Acs.CaseAccessLevel='Edit';
                                 
                        if(a.Opportunity_Access__c=='Private'){  
                        Acs.OpportunityAccessLevel='None';}
                        else if( a.Opportunity_Access__c=='Read Only'){
                        Acs.OpportunityAccessLevel='Read';
                        }else if(a.Opportunity_Access__c=='Read/Write'){
                        Acs.OpportunityAccessLevel='Edit';} 
                        
                        if(a.Contact_Access__c=='Private')
                        {       Acs.ContactAccessLevel='None';
                        } 
                        else if(a.Contact_Access__c=='Read Only')
                        {       Acs.ContactAccessLevel='Read';
                        } 
                        else if(a.Contact_Access__c=='Read/Write')
                        {       Acs.ContactAccessLevel='Edit';
                        } 

                                 
                        AccShare.add(Acs);         
                        system.debug('ttttttt'+AccShare);
                    }
                }
           }                                                             
           If(trigger.isdelete)
           {         
                system.debug('in side delete context'); 
                system.debug('#@!#@!$!@32'+trigger.oldmap.get(a.id));
                sAccOld = trigger.oldmap.get(a.id);
                system.debug('#@!#@!sAccOld'+sAccOld);
                    system.debug('#@!#@!sAccOld'+MapATM);
                
                if(sAccOld.User__c <> null ) 
                {         
                    ATMnew = new AccountTeamMember();   
        
                    if( MapATM.get(string.valueof(a.Account__c)+string.valueof(sAccOld.User__c)) <> null)       
                    {          
                        ATMnew.Id= MapATM.get(string.valueof(a.Account__c)+string.valueof(sAccOld.User__c)).Id;                                     
                        AccListdeleted.add(ATMnew);     
                        system.debug('AccListdeleted isdelet=='+AccListdeleted);      
                    }             
                                                            
                    Asnew = new AccountShare();   
                    if( MapAS.get(string.valueof(a.Account__c)+string.valueof(sAccOld.User__c)) <> null && sAccOld.User__c!=a.Account__r.ownerid)       
                    {          
                    Asnew.Id= MapAS.get(string.valueof(a.Account__c)+string.valueof(sAccOld.User__c)).Id;                                         
                    AccSharedeleted .add(Asnew);     
                    system.debug('sssssss'+AccSharedeleted );      
                    }                       
                }
            }              
         }
         Database.SaveResult[] lsr = Database.insert(AccListUpdated,false);//insert any valid members then add their share entry if they were successfully added
        
        
        system.debug('AccSharedeleted=='+AccSharedeleted+'   '+AccListdeleted);
        
         if(AccListdeleted.size()>0)            
         delete AccListdeleted;
         system.debug('AccShare====183='+AccShare);         
         If(AccShare.size()>0)                            
         insert AccShare;         
         /*if(AccSharedeleted.size()>0)                
         delete AccSharedeleted;*/
                                             
         Integer newcnt=0;
         for(Database.SaveResult sr:lsr)
         {
             if(!sr.isSuccess())
             {
                Database.Error emsg =sr.getErrors()[0];
                system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
             }
             else
             {
                newShare.add(new AccountShare(UserOrGroupId=AccListUpdated[newcnt].UserId, AccountId=AccListUpdated[newcnt].AccountId, AccountAccessLevel='Edit',OpportunityAccessLevel='?Edit'));
             }
             newcnt++;           
         } 
       }
       //UpdateBenchmarkClass.UpdateBenchmarkMethod(trigger.isdelete?trigger.old:trigger.new,trigger.isInsert);

    }  
}