trigger countPromoter on Promoter_info__c (after insert, after delete) {

  List <Promoter_info__C> promoterList = new List <Promoter_info__c>();
  Set<String>SetContactid=new Set<String>();
    
    if(Trigger.isInsert)
    {
        for (Promoter_info__c pro:Trigger.new)
        {
            SetContactid.add(pro.NAME_PROMOTER__C);
        }
        
        promoterList = [Select Id from Promoter_info__c where NAME_PROMOTER__C IN:Setcontactid];
        Integer count=promoterList.size();
        
        for(Promoter_info__C prop:Trigger.new)
        {
            Contact con=[Select Promoter_Count__c from Contact where Id IN:SetContactid];
            con.Promoter_Count__c  = count;
            update con;
        }
        
        
    }
    
    if(Trigger.isDelete)
    {
        for (Promoter_info__c pro:Trigger.old)
        {
            SetContactid.add(pro.NAME_PROMOTER__C);
        }
        
        promoterList = [Select Id from Promoter_info__c where NAME_PROMOTER__C IN:Setcontactid];
        Integer count=promoterList.size();
        
        for(Promoter_info__C prop:Trigger.old)
        {
            Contact con=[Select Promoter_Count__c from Contact where Id IN:SetContactid];
            con.Promoter_Count__c  = count;
            
            if(count ==0)
            {
                con.Promoter_info__c  = false;
            }
            update con;
        }
        
        
    }
 
   
}