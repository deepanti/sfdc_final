trigger UpdateEmployeeinRR on Proposal__c (after insert, after update) 
{
      
      for (Proposal__c Prop : Trigger.new)
      {
      if (Prop.Proposal_RR__c <> null)
      {
      List<id> rrIds = new List<id>();
      
      for (Proposal__c p : Trigger.new)
      {
        rrIds.add(p.Proposal_RR__c);
      }

    List<RR__c> rrs = [Select Id, Selected_Employee__c from RR__c Where Id in :rrIds];
    Map<id, RR__c> rrsMap = new Map<id, RR__c>();
    
    for(RR__c a : rrs)
    {
    rrsMap.put(a.Id,a);
    }  
   
    
    for (Proposal__c p : Trigger.new)
    {
     RR__c thisRr = rrsMap.get(p.Proposal_RR__c);

     If (p.Status__c == 'Selected-New'|| p.Status__c == 'Selected-Extended')
        {
          thisRr.Selected_Employee__c = p.RMG_Employee_Code__c;        
        }
    }
update rrs;
}
}
}