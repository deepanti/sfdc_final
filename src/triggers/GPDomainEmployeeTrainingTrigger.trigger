trigger GPDomainEmployeeTrainingTrigger on GP_Employee_Training__c (after insert, after update, before insert, before update) {
    // Creates Domain class instance and calls appropriate methods
      map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
      if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_employee_training__c') && MapofProjectTrigger.get('gp_employee_training__c').GP_Enable_Sobject__c)
      {
          fflib_SObjectDomain.triggerHandler(GPDomainEmployeeTraining.class);
      }
}