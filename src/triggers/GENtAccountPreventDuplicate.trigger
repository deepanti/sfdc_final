trigger GENtAccountPreventDuplicate on Account (before insert, before update) {

    Set<Id> accId= new Set<Id>();
    Map<String, Account> accMap = new Map<String, Account>();
    Map<String, Account> accMapCode = new Map<String, Account>();
    for (Account Account : System.Trigger.new) 
    {
        if ((Account.AccountNumber != null) && (System.Trigger.isInsert ||(Account.AccountNumber!= System.Trigger.oldMap.get(Account.Id).AccountNumber))) 
        {
   
            if (accMapCode.containsKey(Account.AccountNumber)) 
            {
                Account.AccountNumber.addError('Account with this Code already exists.');
            } 
            else 
            {
                accMapCode.put(Account.AccountNumber, Account);
            }
        }
   }
    
    for (Account Acc : [SELECT AccountNumber FROM Account WHERE AccountNumber IN :accMapCode.KeySet()]) 
    {
        try
        {
        Account newAccount = accMapCode.get(Acc.AccountNumber);
        newAccount.AccountNumber.addError('Account with this Code already exists.');
        }
        catch(exception exp)
        {
        system.debug('Error'+exp);
        }
    }
    
    
}