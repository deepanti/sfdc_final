/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/28/2018
@Description :this trigger is used to update Contact and Lead Fields(vertical__c,archType__c) using Account Object

@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */

trigger MarkFieldUpFromAccount on Account (after update) {
    
    if(trigger.isAfter)
    {
    if(trigger.isUpdate &&CheckRecursiveForOLI.runOnceAfter_1()){
                 Integer batchCalledCount=0;
            //this method executes batch class to update field(Industry vertical and Archtype) from Account object 
            //to contact and Lead object field(Vertical__C and Archtype__C) 
                Database.executeBatch(new LinkAccountWithAccountCrationRequestHl(trigger.new,trigger.oldMap), 5);
                  system.debug('..Count of batch called'+batchCalledCount++);
              }
        else
        {
            system.debug(' After delete trigger and after undelete trigger is not written');
        }
    }
}