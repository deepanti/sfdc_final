trigger GPDomainProjectTrigger on GP_Project__c ( after insert, after update, after undelete,  before insert, before update) 
  {
      // Creates Domain class instance and calls appropriate methods
      map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
      if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_project__c') && MapofProjectTrigger.get('gp_project__c').GP_Enable_Sobject__c)
      {
          fflib_SObjectDomain.triggerHandler(GPDomainProject.class);
      }
  }