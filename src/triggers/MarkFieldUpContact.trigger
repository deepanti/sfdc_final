/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/28/2018
@Description :this trigger is used to update contact field(Vertical__c,ArcheType__c) from Account Object
@Events used  before insert, before update

@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */

trigger MarkFieldUpContact on Contact (before insert,before update) {
    system.debug(':-trigger.isInsert-:'+trigger.isInsert+':--CheckRecursiveForOLI.runOnceAfter_1()--:'+CheckRecursiveForOLI.runOnceAfter_1());
    if(trigger.isBefore)
    {
        
        if(trigger.isInsert && CheckRecursiveForOLI.runOnceBefore())
        {
            //this method is used to update contact field(Archtype__c and Vertical__c) from Account Object
            new ContactTriggerHandler().AccToConFieldUpdate(trigger.new);
        }
        else if(trigger.isUpdate&&CheckRecursiveForOLI.runOnceBefore())
        {
             //this method is used to update contact field(Archtype__c and Vertical__c) from Account Object
            new ContactTriggerHandler().AccToConFieldUpdate(trigger.new,trigger.oldMap);
        }
        else
        {
            system.debug('we do not have before delete trigger on Contact');
        }
    }

}