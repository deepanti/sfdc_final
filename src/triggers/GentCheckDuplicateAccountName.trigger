trigger GentCheckDuplicateAccountName on Account_Creation_Request__c (before insert, before update) {
    Set<String> accName= new Set<String>();
    //Set to hold ids of A/c Crt Req.s completed
    Set<id> accId= new Set<id>();
    Map<String, Account_Creation_Request__c> accMap = new Map<String, Account_Creation_Request__c>();
    Map<String, Account_Creation_Request__c> accMapCode = new Map<String, Account_Creation_Request__c>();
    for (Account_Creation_Request__c oAcrCheck: System.Trigger.new) 
    {
        if ((oAcrCheck.Name != null) && (System.Trigger.isInsert ||(oAcrCheck.Name!= System.Trigger.oldMap.get(oAcrCheck.Id).Name))) 
        {
        

            if (accMapCode.containsKey(oAcrCheck.Name)) 
            {
               oAcrCheck.Name.addError('Account creation request with this name already exists.');
            } 
            else 
            {
                accMapCode.put(oAcrCheck.Name, oAcrCheck);
            }
        }
        
   }
    system.debug('#$!@@$@#!@#!@#'+accMapCode);
    
    for (Account_Creation_Request__c Acr : [SELECT Name FROM Account_Creation_Request__c WHERE Name IN :accMapCode.KeySet()]) 
    {
        try
        {
        Account_Creation_Request__c newAcr = accMapCode.get(Acr.Name);
        newAcr.Name.addError('Account Creation Request with this Name already exists');
        }
        catch(exception exp)
        {
        system.debug('Error'+exp);
        }
    }
    
    // duplicate name in account added by sharif
     for (Account AccObj : [SELECT Name FROM Account WHERE Name IN :accMapCode.KeySet()]) 
    {
        try
        {
        if(accMapCode.containskey(AccObj.Name))
        {
            Account_Creation_Request__c newAcr = accMapCode.get(AccObj.Name);
            newAcr.Name.addError('Account with this Name already exists');
        }
        }
        catch(exception exp)
        {
        system.debug('Error'+exp);
        }
    }
    
//end
    // Map for holding Tasks 
   /* Map<String, Task> tskMap = new Map<String, Task>();
    for(Account_Creation_Request__c oAcr: trigger.new)
    {
        accName.add(oAcr.Name);
    }
    for(Account oAcc: [Select id,Name from account where name in: accName] )
    {
        accMap.put(oAcc.Name,oAcc);
    }
    for(Account_Creation_Request__c oAcrCheck: trigger.new)
    {
        try
        {
            if(accMap.get(oAcrCheck.Name)!=null && (System.Trigger.isInsert ||(oAcrCheck.Name!= System.Trigger.oldMap.get(oAcr.Id).Name))) )
            {
                oAcrCheck.Name.addError('Account creation request with this Name already exists.');
            }
        }*/
       /* try
        {
            if(accMap.get(oAcrCheck.Name)!=null && oAcrCheck.Description__c ==Null && oAcrCheck.Status__c!='Completed')
            {
                oAcrCheck.Name.addError('Account with this Name already exists, Please provide description if you still want to raise a request.');
            }
        }*/
       /* catch(exception exp)
        {
            system.debug('Error'+exp);
        }
    }*/
   // Code to Update related Task on update of A/c Creation Request
 /*  if (Trigger.isUpdate )
   {
   for(Account_Creation_Request__c oAcRTskUpt: trigger.new)
    {
        try
        {
            if(oAcRTskUpt.Status__c=='Completed')
            {
                 accId.add(oAcRTskUpt.id);
            }
        }
        catch(exception exp)
        {
            system.debug('Error'+exp);
        }
    } 
    // Fetch related tasks
    for(Task Tsk: [Select id,status from Task where whatid in: accId] )
    {
        Tsk.status='Completed';
        tskMap.put(Tsk.Id,Tsk);
    }
        
     try
     {
     update tskMap.values();
     }
     catch(exception exp)
        {
            system.debug('Error'+exp);
        }



    }*/


}