trigger GentUpdateQSRMApprover on QSRM__c (after insert,before update) {

    List<OpportunityTeamMember> OppTeamList {get;set;}
    List<OpportunityTeamMember> OppTeamListUpdated = new List<OpportunityTeamMember>();    
    List<OpportunityTeamMember> OppTeamListdeleted = new List<OpportunityTeamMember>();       
    List<OpportunityShare> OppShare= new List<OpportunityShare>();              
    set<Id> opportunityId = new set<id>();              
   // List<OpportunityShare> existingOpptyAccess = new List<OpportunityShare>();
    Map<string,OpportunityShare> shareMap = new Map<string,OpportunityShare>();
   // existingOpptyAccess=[Select id,Opportunityid,UserOrGroupId from OpportunityShare where Opportunityid in:opportunityId];

        
    for(QSRM__c oQSRM :trigger.new)  
    {              
          opportunityId.add(oQSRM.Opportunity__c); 
    }                 
    QSRM__c sQSRMOld;    
   // List<OpportunityShare> OppSharedeleted = new List<OpportunityShare>();     
       
    OppTeamList = new List<OpportunityTeamMember>([SELECT CurrencyIsoCode,Id,IsDeleted,OpportunityAccessLevel,OpportunityId,SystemModstamp,TeamMemberRole,UserId FROM OpportunityTeamMember where OpportunityId IN:opportunityId]);       
    OpportunityShare[] newShare = new OpportunityShare[]{};             
    newShare = new list<OpportunityShare>([SELECT Id,IsDeleted,OpportunityAccessLevel,OpportunityId,RowCause,UserOrGroupId FROM OpportunityShare where OpportunityId IN:opportunityId]);       
    // Map<string,OpportunityShare> MapOS = new Map<string,OpportunityShare>();              
                 
    Map<string,OpportunityTeamMember> MapOTMem = new Map<string,OpportunityTeamMember>();
    for(OpportunityTeamMember OTM : OppTeamList )         
    {      system.debug('aaaaaaaa'+string.valueof(OTM.OpportunityId)+string.valueof(OTM.UserId));                           
        MapOTMem.put(string.valueof(OTM.OpportunityId)+string.valueof(OTM.UserId),OTM );  
    }                    
    for(OpportunityShare oOS: newShare)
    {
    shareMap.put(string.valueof(oOS.OpportunityId) + string.valueof(oOS.UserOrGroupId),oOS);
    }

    for(QSRM__c a :trigger.new)
    {   sQSRMOld = new QSRM__c() ;
               // system.debug('QSRM------>'+sQSRMOld);   
        OpportunityTeamMember OppTeamMem ; 
        OpportunityTeamMember OppTeamMemnew ;  
        OpportunityShare OppSh ; 
        //OpportunityShare OppShnew ;              
        if(a.QSRM_Approver1__c<> null ) 
        {        
            OppTeamMem = new OpportunityTeamMember();      
            OppTeamMem.OpportunityId= a.Opportunity__c;
            OppTeamMem.UserId= a.QSRM_Approver1__c;
            OppTeamMem.TeamMemberRole= 'BD Rep';     
            OppTeamListUpdated.add(OppTeamMem);         
           // system.debug('AccListUpdated'+AccListUpdated);        
              
         OppSh = new OpportunityShare();      
         if(shareMap.get(string.valueof(a.Opportunity__c) + string.valueof(a.QSRM_Approver1__c))==null)
          {             
            OppSh.OpportunityId=a.Opportunity__c;
            //if(OppSh.UserOrGroupId<> null)
            OppSh.UserOrGroupId=a.QSRM_Approver1__c;
            OppSh.OpportunityAccessLevel='Edit';              
            OppShare.add(OppSh);     
            // system.debug('ttttttt'+AccShare);
           }                                          
        }      
         if(Trigger.isUpdate)        {     
         system.debug('zzzzz'+trigger.oldmap.get(a.Id));
         if(a.Id <> null && trigger.oldmap.get(a.Id)<>null)
         sQSRMOld = trigger.oldmap.get(a.Id);                    
         system.debug('ssssss'+sQSRMOld.QSRM_Approver1__c);                      
         system.debug('ppppp'+a.QSRM_Approver1__c);      
         if(sQSRMOld.QSRM_Approver1__c!= a.QSRM_Approver1__c && a.QSRM_Approver1__c <> null ) 
        {         
         
            OppTeamMemnew = new OpportunityTeamMember();   
           system.debug('vvvvvvv'+string.valueof(a.Opportunity__c)+string.valueof(sQSRMOld.QSRM_Approver1__c));                                
            if( MapOTMem.get(string.valueof(a.Opportunity__c)+string.valueof(sQSRMOld.QSRM_Approver1__c))<>null)       
            {          
            OppTeamMemnew.Id= MapOTMem.get(string.valueof(a.Opportunity__c)+string.valueof(sQSRMOld.QSRM_Approver1__c)).Id;                                         
            OppTeamListdeleted.add(OppTeamMemnew);     
           // system.debug('AccListdeleted'+AccListdeleted);      
            }             
                                                    
        }             
        }                     
    }
     Database.SaveResult[] lsr = Database.insert(OppTeamListUpdated,false);//insert any valid members then add their share entry if they were successfully added
    if(OppTeamListdeleted.size()>0)            
     delete OppTeamListdeleted;            
     If(OppShare.size()>0)                            
     insert OppShare;         
                                       
     Integer newcnt=0;
     for(Database.SaveResult sr:lsr)
     {
         if(!sr.isSuccess())
         {
            Database.Error emsg =sr.getErrors()[0];
            system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
         }
         else
         {
            newShare.add(new OpportunityShare(UserOrGroupId=OppTeamListUpdated[newcnt].UserId, OpportunityId=OppTeamListUpdated[newcnt].OpportunityId, OpportunityAccessLevel='​Edit'));
         }
         newcnt++;           
     }
        
     }