/*-------------
        Class Description : Trigger on Quota Setup Detail to prevent to be deleted by Quota Owner.
        Created by   : Arjun Srivastava
        Created Date : 21 May 2014 
        Location     : Genpact        
        Last modified date: 11 August 2014
    ---------------*/
Trigger GENtQuotaSetUpPreventDelete on Set_Quotas__c (Before Insert,Before Update,Before Delete) 
{
  if(!singleexecuion.bool_GENtQuotaSetUpPreventDelete)
  {
    singleexecuion.bool_GENtQuotaSetUpPreventDelete = true;
    Map<String,String> SetquotasMapCode = new Map<String,String>(); 
    Map<String,String> mapprofile=new Map<String,String>();
    
    for(profile profid:[select id from profile where Name='Genpact Shared Services' OR Name='Genpact Super Admin' OR Name='System Administrator' OR Name='Genpact Head of Sales'])
     mapprofile.put(profid.id,profid.id);
         
   if(Trigger.isDelete)    
   {                    
        for(Set_Quotas__c oSQCheck : [select id,Quota_Header__r.Sales_Person__c from Set_Quotas__c where ID IN :Trigger.OLD])
            SetquotasMapCode.Put(oSQCheck.id,oSQCheck.Quota_Header__r.Sales_Person__c);

        for(Set_Quotas__c oSQCheck : Trigger.OLD)   
        {
            system.debug('userinfo.getProfileId==='+userinfo.getProfileId()+'    '+userinfo.getuserid());
            if(SetquotasMapCode.Get(oSQCheck.id) == userinfo.getuserid() && mapprofile.get(userinfo.getProfileId())==null)
                oSQCheck.addError('You CANNOT DELETE YOUR OWN QUOTA');
        }
   }  
   
   if(Trigger.isInsert)    
   {        
        List<string> idslist = new list<string>();
        for(Set_Quotas__c oSQCheck : Trigger.New)
            idslist.add(oSQCheck.Quota_Header__c);  
            
        for(Quota_Header__c oSQCheck : [select id,Sales_Person__c from Quota_Header__c where ID IN : idslist])
            SetquotasMapCode.Put(oSQCheck.Sales_Person__c,oSQCheck.Sales_Person__c);

        for(Set_Quotas__c oSQCheck : Trigger.New)   
        {
            system.debug('userinfo.getProfileId==='+SetquotasMapCode.Get(oSQCheck.id)+'  '+userinfo.getProfileId()+'    '+userinfo.getuserid()+'  '+mapprofile.get(userinfo.getProfileId()));
            if(SetquotasMapCode.Get(userinfo.getuserid()) == userinfo.getuserid() && mapprofile.get(userinfo.getProfileId())==null)
                oSQCheck.addError('You CANNOT SETUP YOUR OWN QUOTA');
        }
   }
   
   if(Trigger.isUpdate)    
   {                    
        for(Set_Quotas__c oSQCheck : [select id,Quota_Header__r.Sales_Person__c from Set_Quotas__c where ID IN :Trigger.New])
            SetquotasMapCode.Put(oSQCheck.id,oSQCheck.Quota_Header__r.Sales_Person__c);

        for(Set_Quotas__c oSQCheck : Trigger.New)   
        {
            system.debug('userinfo.getProfileId==='+userinfo.getProfileId()+'    '+userinfo.getuserid());
            if(SetquotasMapCode.Get(oSQCheck.id) == userinfo.getuserid() && mapprofile.get(userinfo.getProfileId())==null)
                oSQCheck.addError('You CANNOT EDIT YOUR OWN QUOTA');
        }
   }  
  } 
}