trigger TS_NonTS_CycleTime on Opportunity (before update, after update) {
 
    if(label.TS_NonTS_CycleTimeFlag == 'false'){    
    
    if(Trigger.isBefore && Trigger.isUpdate){
        Boolean recursiveFlag = CheckRecursiveForNonTs.runBeforeNonTsHoldTeam();
        system.debug(':----Before update recursiveFlag----:'+recursiveFlag);
        if(recursiveFlag){
            DeleteOppSPlitfromOpportunity.holdOpportunityTeam(Trigger.new, Trigger.oldMap);
        }
    }
    if(Trigger.isAfter && Trigger.isUpdate){
        Boolean recursiveFlag = CheckRecursiveForNonTs.runOnceAfterNonTs();
        system.debug(':----after update recursiveFlag----:'+recursiveFlag);
        if(recursiveFlag)
        {   
            System.debug('Limit Query Rows TS_NonTS_CycleTime'+System.limits.getQueryRows());
            DeleteOppSPlitfromOpportunity.deleteOppSplitOwneronOppOwnerchange(Trigger.new, Trigger.oldMap);
            TsNonTsHelperClass.dealTypeMethod(Trigger.new);
        }
    }   
}}