trigger GPDomainOppProjectLeadershipTrigger on GP_Opp_Project_Leadership__c (after delete, after insert, after update, after undelete, before delete, before insert, before update) {

    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_opp_project_leadership__c') && MapofProjectTrigger.get('gp_opp_project_leadership__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainOppProjectLeadership.class);
    }
}