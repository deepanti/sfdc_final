/*-------------
Trigger Description : This trigger on user will update the attainment records based upon user/manager update and their association with Account GRM,Oppty/OLI owner.
Organisation        : Tech Mahindra NSEZ
Created by          : Arjun Srivastava
Location            : Genpact
Created Date        : 23 June 2014  
Last modified date  : 29 June 2014
---------------*/

Trigger UpdateUserQuotaAttainment on User (After update) 
{   
    if(!singleexecuion.bool_check1)
    {
        singleexecuion.callagain1();        // help prevent trigger to get into recursion.
        system.debug('Trigger.newmap.keyset()=='+Trigger.newmap.keyset());
        Set<ID> userids=new Set<ID>();
        for(User newval: Trigger.new)
        {
            User olduser = Trigger.oldMap.get(newval.Id);
            //System.debug('olduser.Has_Stale_Deals__c:'+olduser.Has_Stale_Deals__c + ' newval.Has_Stale_Deals__c: '+ newval.Has_Stale_Deals__c);
            
            if(((olduser.ManagerId!=newval.ManagerId) ||(olduser.UserRoleId!=newval.UserRoleId)) && (olduser.Has_Stale_Deals__c==newval.Has_Stale_Deals__c) )
                userids.add(newval.id);             // getting ids of users only whose manager has been changed.
                
            System.debug('userids: ' + userids);
        }
        system.debug('userids=='+userids.size());
        
        if(userids.size()>0)
        {
            if(System.isFuture() || System.isBatch())
            {
                UpdateQuotajunccls.UpdateQuotajuncmeth(userids);
            }
            else
            {
                UpdateQuotajunccls.UpdateQuotajuncmeth1(userids);
            }
        }         
    } 
}