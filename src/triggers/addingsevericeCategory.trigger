/*Trigger(for SAVO Application) for updating "prodcut family" and "service line" from OLI to opprtunity*/

trigger addingsevericeCategory on OpportunityProduct__c (after insert,after update,after delete) 
{
    list<id> setofopportunityid=new list<id>();
   map<id,OpportunityProduct__c> mapodbulkfiying=new map<id,OpportunityProduct__c>();
    for(OpportunityProduct__c opptyproductobj:trigger.new)
{
   // setofopportunityid.add(opptyproductobj.Opportunityid__c);
   if(!mapodbulkfiying.containsKey(opptyproductobj.Opportunityid__c))
   {
       mapodbulkfiying.put(opptyproductobj.Opportunityid__c, opptyproductobj);
   }
}          

if(trigger.isafter)
    {
        if(trigger.isinsert || trigger.isupdate )
        {
            
           
    string servicecat='';
    string prodfamily='';
    list<OpportunityProduct__c> opptyproductobjlist=[select id,Service_Line_OLI__c, Product_Family_OLI__c,Opportunityid__c from OpportunityProduct__c where Opportunityid__c IN:mapodbulkfiying.keySet()];
    List<opportunity> opptyupdate=new List<opportunity>();
    list<string> liststring=new list<string>();
    list<string> liststringforproductfamily=new list<string>();
    set<string> noduplicates=new set<string>();
    set<string> noduplicatesforproductfamily=new set<string>();
    if(opptyproductobjlist.size()>0)
    {    
            string tags='';
            string productfamily='';
            for(OpportunityProduct__c Opptyproductobj:opptyproductobjlist)
            {
                //opportunity oppobj=new opportunity(id=Opptyproductobj.Opportunityid__c);
           
                tags =  Opptyproductobj.Service_Line_OLI__c ;
                //oppobj.Service_Line_Cat__c= //tags;
                //tags =  Opptyproductobj.test_Service_Line__c ;
                productfamily=Opptyproductobj.Product_Family_OLI__c;
                liststring.add(tags);
                liststringforproductfamily.add(productfamily);
                
                system.debug('list of service Category'+liststring);
            }
            if(liststring.size()>0)
            {
                for(string foraddinset:liststring)
                {
                    if(!noduplicates.contains(foraddinset))
                    {
                        noduplicates.add(foraddinset);
                    }
                }
            }
            
            if(noduplicates.size()>0)
            {
                for(string s:noduplicates)
                {
                    servicecat+=s+',';
                }
            }
        if(liststringforproductfamily.size()>0)
        {
            for(string forproductfamily:liststringforproductfamily)
            {
                if(!noduplicatesforproductfamily.contains(forproductfamily))
                {
                    noduplicatesforproductfamily.add(forproductfamily);
                }
            }
        }
        if(noduplicatesforproductfamily.size()>0)
        {
            for(string pf:noduplicatesforproductfamily)
            {
                prodfamily+=pf+',';
            }
        }
        
            opportunity oppty=new opportunity(id=opptyproductobjlist[0].Opportunityid__c);
            oppty.Service_Line__c=servicecat;
            oppty.Product_Family__c=prodfamily;
            opptyupdate.add(oppty);
            liststring.clear();
            liststringforproductfamily.clear();
            noduplicates.clear();
            noduplicatesforproductfamily.clear();
            
            
    }
    
    if(opptyupdate.size()>0)
    {
        try
        {
            
            update opptyupdate;
            system.debug('Opportunity Service_Line_Category update'+opptyupdate);
        }
        catch(exception e)
        {
            system.debug('error occured due to'+e);
        }
    }
        }
    
    
        if(trigger.isdelete)
        {
           
            list<id> setofopportunityiddelete= new list<id>();
    string servicecat='';
    string prodfamily='';
for(OpportunityProduct__c opptyproductobj:trigger.old)
{
    setofopportunityiddelete.add(opptyproductobj.Opportunityid__c);
}
    list<OpportunityProduct__c> opptyproductobjlist=[select id,Service_Line_OLI__c, Product_Family_OLI__c,Opportunityid__c from OpportunityProduct__c where Opportunityid__c IN:setofopportunityiddelete];
    List<opportunity> opptyupdate=new List<opportunity>();
    list<string> liststring=new list<string>();
    list<string> liststringforproductfamily=new list<string>();
    set<string> noduplicates=new set<string>();
    set<string> noduplicatesforproductfamily=new set<string>();
            list<opportunity> opptyfornullvaluesupdate=new list<opportunity>();
    if(opptyproductobjlist.size()>0)
    {    
            string tags='';
            string productfamily='';
            for(OpportunityProduct__c Opptyproductobj:opptyproductobjlist)
            {
                //opportunity oppobj=new opportunity(id=Opptyproductobj.Opportunityid__c);
           
                tags =  Opptyproductobj.Service_Line_OLI__c ;
                //oppobj.Service_Line_Cat__c= //tags;
                productfamily=Opptyproductobj.Product_Family_OLI__c;
                liststring.add(tags);
                liststringforproductfamily.add(productfamily);
                
                system.debug('list of service Category'+liststring);
            }
            if(liststring.size()>0)
            {
                for(string foraddinset:liststring)
                {
                    if(!noduplicates.contains(foraddinset))
                    {
                        noduplicates.add(foraddinset);
                    }
                }
            }
            
            if(noduplicates.size()>0)
            {
                for(string s:noduplicates)
                {
                    servicecat+=s+',';
                }
            }
        if(liststringforproductfamily.size()>0)
        {
            for(string forproductfamily:liststringforproductfamily)
            {
                if(!noduplicatesforproductfamily.contains(forproductfamily))
                {
                    noduplicatesforproductfamily.add(forproductfamily);
                }
            }
        }
        if(noduplicatesforproductfamily.size()>0)
        {
            for(string pf:noduplicatesforproductfamily)
            {
                prodfamily+=pf+',';
            }
        }
        
            opportunity oppty=new opportunity(id=opptyproductobjlist[0].Opportunityid__c);
            oppty.Service_Line__c=servicecat;
            oppty.Product_Family__c=prodfamily;
            opptyupdate.add(oppty);
            liststring.clear();
            liststringforproductfamily.clear();
            noduplicates.clear();
            noduplicatesforproductfamily.clear();

    }
     else
     {
         //setofopportunityid
             opportunity oppobject=new opportunity(id=setofopportunityiddelete[0]);
          oppobject.Service_Line__c=servicecat;
            oppobject.Product_Family__c=prodfamily;
         opptyfornullvaluesupdate.add(oppobject);
            liststring.clear();
            liststringforproductfamily.clear();
            noduplicates.clear();
            noduplicatesforproductfamily.clear();

         
     }
                if(opptyfornullvaluesupdate.size()>0)
    {
        try
        {
            
            update opptyfornullvaluesupdate;
            system.debug('Opportunity Service_Line_Category update for nulling values'+opptyfornullvaluesupdate);
        }
        catch(exception e)
        {
            system.debug('error occured due to'+e);
        }
    }  
    
    if(opptyupdate.size()>0)
    {
        try
        {
            
            update opptyupdate;
            system.debug('Opportunity Service_Line_Category update'+opptyupdate);
        }
        catch(exception e)
        {
            system.debug('error occured due to'+e);
        }
    }            
        }
    }


 

}