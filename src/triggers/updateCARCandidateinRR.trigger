trigger updateCARCandidateinRR on Proposal__c (after insert,after update) 
{

        for (Proposal__c Prop: Trigger.New)
        {
        if(Prop.Proposal_RR__c <> null)
        {
        List < Id > rrIds = new List < Id >();
        for ( Proposal__c  c: Trigger.New ) 
        {
           rrIds.add(c.Proposal_RR__c);
        }
        List<RR__c> opps = [select id, Selected_CAR_Candidate__c from RR__c where id in :rrIds];
        Map < Id ,RR__c > empmap = new Map < Id , RR__c >();
        for ( RR__c  a : opps   ) 
        {
            empmap.put( a.Id, a);
        }
        for(Proposal__c c: Trigger.New) 
        {
            RR__c ac = empmap.get( c.Proposal_RR__c);         
            if ( ac == null ) 
            {    
                  continue;
            }
            If (c.Status__c == 'Selected-New' || c.Status__c == 'Selected-Extended')
            {
               ac.Selected_CAR_Candidate__c = c.CAR_Candidate_Code__c;    
            }
         }
        update opps;                                    
        }
        }
}