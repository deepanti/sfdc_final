/*
Trigger Description : 1. This trigger is used to insert/update custom account team(AccountArchetype__c) with GRM/CP/BD Rep w.r.t account record.
                      2. This is also used to update archetype of all custom account members w.r.t Account.
Last modified date : 5 September 2014 3: 29 PM
*/
trigger GenTUpdateAccounArchetypeJunction on Account (after insert, After Update ) 
{
 
    if(Validator_cls.AllowGenTUpdateAccounArche)
    {
    Validator_cls.AllowGenTUpdateAccounArche = false;
    
    Set<String> sAcc = new Set<String>();
    List<AccountArchetype__c> AccList = new List<AccountArchetype__c>();
    List<AccountArchetype__c> AccListforBenchmark = new List<AccountArchetype__c>();
    List<AccountArchetype__c> AccListUpdated = new List<AccountArchetype__c>();    
    List<AccountArchetype__c> AccListdeleted = new List<AccountArchetype__c>();       
    List<AccountArchetype__c> AccArchListUpdated = new List<AccountArchetype__c>();
    List<AccountArchetype__c> AccArcheListUpdated = new List<AccountArchetype__c>();
    AccList = new List<AccountArchetype__c>([SELECT Account_Access__c,Case_Access__c,Opportunity_Access__c,Contact_Access__c,Archetype__c,Account__c,Id,Team_Member_Role__c,User__c FROM AccountArchetype__c where Account__c =: trigger.newmap.keyset()]);       
    Map<string,AccountArchetype__c> MapATM = new Map<string,AccountArchetype__c>();              
   
    for(AccountArchetype__c at : AccList)         
    {   
        system.debug('aaaaaaaa'+string.valueof(at.Account__c)+string.valueof(at.User__c));                           
        MapATM.put(string.valueof(at.Account__c)+string.valueof(at.User__c)+string.valueof(at.Team_Member_Role__c),at);  
        
    }             
    
    set<id> AccountArcheTypeIdSet = new set<id>();
    set<Id> AccountIdForAccoutTeamSet = new set<Id>();
    for(Account a :trigger.new)
    {
        if(a.Archetype__c != null)
            AccountArcheTypeIdSet.add(a.Archetype__c);
        AccountIdForAccoutTeamSet.add(a.id);
    }  
    Map<id,Archetype__c> accArcadeUpdateMap = new Map<id,Archetype__c>();
    Map<id,list<AccountArchetype__c>> AccountAccountTemMemberMap = new Map<id,list<AccountArchetype__c>>();
    if(AccountArcheTypeIdSet.size()>0)
    {
        accArcadeUpdateMap = new Map<id,Archetype__c>([Select id, BD__c,CP__c,GRM__c from Archetype__c where id in : AccountArcheTypeIdSet]);
        list<AccountArchetype__c> AccArcheUpdate= new list<AccountArchetype__c>();
        AccArcheUpdate=[Select id,account__c,Archetype__c,BD__c,CP__c,GRM__c from AccountArchetype__c where account__c in:AccountIdForAccoutTeamSet];
        if(AccArcheUpdate.size()>0)
        {
            for(AccountArchetype__c archID : AccArcheUpdate)
            {
                if(AccountAccountTemMemberMap.containskey(archID.account__c))
                {
                    list<AccountArchetype__c> TempLst = new list<AccountArchetype__c>();
                    TempLst.addall(AccountAccountTemMemberMap.get(archID.account__c));
                    TempLst.add(archID);
                    AccountAccountTemMemberMap.put(archID.account__c,TempLst);
                }
                else
                {
                    list<AccountArchetype__c> TempLst = new list<AccountArchetype__c>();
                    TempLst.add(archID);
                    AccountAccountTemMemberMap.put(archID.account__c,TempLst);
                }
            }
        }
        if(accArcadeUpdateMap.size()>0)
        {
        for(Account a :trigger.new)
        {
            Account sAccOld =new Account();
            if(trigger.isUpdate && a.id <> null && trigger.oldmap.get(a.id)<>null)
                sAccOld = trigger.oldmap.get(a.id);
            
            system.debug('Acclist------>'+AccList); 
                  
       //Start for Account Archetype change  
            
       if(trigger.isUpdate && a.Archetype__c!=sAccOld.Archetype__c )
        {
            system.debug('Tapendra Test 1');
           // Archetype__c accArcadeUpdate = [Select id, BD__c,CP__c,GRM__c from Archetype__c where id=:a.Archetype__c];
           //  list<AccountArchetype__c> AccArcheUpdate= new list<AccountArchetype__c>();
           // AccArcheUpdate=[Select id,Archetype__c,BD__c,CP__c,GRM__c from AccountArchetype__c where account__c=:a.id];
          
             if(AccountAccountTemMemberMap.containskey(a.id))
             {
                for(AccountArchetype__c archID : AccountAccountTemMemberMap.get(a.id))
                {
                    if(accArcadeUpdateMap.containskey(a.Archetype__c))
                    {
                        system.debug('Tapendra Test 2');
                        archID.Archetype__c=a.Archetype__c;
                        archID.BD__c = accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
                        archID.CP__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
                        archID.GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;
                        AccArcheListUpdated.add(archID);
                    }
                }
             }
            /*
                for(AccountArchetype__c archID : AccArcheUpdate)
                {
                    system.debug('Tapendra Test 2');
                    archID.Archetype__c=a.Archetype__c;
                    archID.BD__c = accArcadeUpdate.BD__c; 
                    archID.CP__c = accArcadeUpdate.CP__c; 
                    archID.GRM__c = accArcadeUpdate.GRM__c;
                    AccArcheListUpdated.add(archID);
                } 
             */
         system.debug('Tapendra Test 3'+AccListUpdated);
         update AccArcheListUpdated;
        }
        
      //End for Account Archetype change
          
   
        
     //Start For Client Patner 
       if(a.Client_Partner__c <> null && a.Client_Partner__c <> a.ownerid && a.Client_Partner__c<>sAccOld.Client_Partner__c )
          {   
            AccountArchetype__c AccountTeamJunction ;
            AccountTeamJunction = new AccountArchetype__c();
           // Archetype__c accArcade = [Select id, BD__c,CP__c,GRM__c from Archetype__c where id=:a.Archetype__c]; 
            AccountTeamJunction.Account__c= a.id;
            AccountTeamJunction.Archetype__c=a.Archetype__c;     
            AccountTeamJunction.User__c=a.Client_Partner__c;
            AccountTeamJunction.Team_Member_Role__c='Client Partner';
            AccountTeamJunction.Account_Access__c='Read/Write';   
            AccountTeamJunction.Opportunity_Access__c='Read/Write';
            AccountTeamJunction.Contact_Access__c='Read/Write';
            AccountTeamJunction.Case_Access__c='Read/Write';
            
            AccountTeamJunction.BD__c = accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
            AccountTeamJunction.CP__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
            AccountTeamJunction.GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;  
            AccountTeamJunction.Benchmark_GRM__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
            AccListUpdated.add(AccountTeamJunction);
            system.debug('AccListUpdated1'+AccListUpdated);
                                                       
            }
        else if(a.Client_Partner__c <> null && a.Client_Partner__c == a.ownerid )
        {
            AccountArchetype__c aatype= new AccountArchetype__c();
            if(MapATM.get(string.valueof(a.id)+string.valueof(a.Client_Partner__c)+'BD Rep')<>null)
            {
                aatype=MapATM.get(string.valueof(a.id)+string.valueof(a.Client_Partner__c)+'BD Rep');
                aatype.Team_Member_Role__c='Client Partner';
                AccArchListUpdated.add(aatype);
            }
        }
        if(sAccOld.Client_Partner__c!= a.Client_Partner__c ) 
        {         
            AccountArchetype__c AccountTeamJunctionNew = new AccountArchetype__c();
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c));              
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c)+'Client Partner')<>null)       
            {          
                AccountTeamJunctionNew.Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c)+'Client Partner').Id;                                         
                if(sAccOld.Client_Partner__c !=  a.ownerid)
                        AccListdeleted.add(AccountTeamJunctionNew );     
                else
                {
                        AccountTeamJunctionNew.Team_Member_Role__c='BD Rep';
                        AccArchListUpdated.add(AccountTeamJunctionNew);             
                }               
                system.debug('AccListdeleted cp=='+AccListdeleted);      
            }             
                                                                    
        }    
        //End  For Client Patner 
        
        //Start For GRM Patner 
        
        if(a.Primary_Account_GRM__c <> null && a.Primary_Account_GRM__c<>a.ownerid && a.Primary_Account_GRM__c <>sAccOld.Primary_Account_GRM__c ) 
         {      
            AccountArchetype__c AccountTeamJunction ;
            AccountTeamJunction = new AccountArchetype__c(); 
            // Archetype__c accArcade = [Select id, BD__c,CP__c,GRM__c from Archetype__c where id=:a.Archetype__c];
            AccountTeamJunction.Account__c= a.id;  
            AccountTeamJunction.Archetype__c=a.Archetype__c;   
            AccountTeamJunction.User__c=a.Primary_Account_GRM__c;
            AccountTeamJunction.Team_Member_Role__c='GRM';
            AccountTeamJunction.Account_Access__c='Read/Write';   
            AccountTeamJunction.Opportunity_Access__c='Read/Write';
            AccountTeamJunction.Contact_Access__c='Read/Write';
            AccountTeamJunction.Case_Access__c='Read/Write';
            
            AccountTeamJunction.BD__c = accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
            AccountTeamJunction.CP__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
            AccountTeamJunction.GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;  
            AccountTeamJunction.Benchmark_GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;
            AccListUpdated.add(AccountTeamJunction);
            system.debug('AccListUpdated2'+AccListUpdated);
                                              
         }
         else if(a.Primary_Account_GRM__c <> null && a.Primary_Account_GRM__c == a.ownerid )
         {   
           system.debug('tapendra test 4');
            AccountArchetype__c aatype= new AccountArchetype__c();
              system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(a.Primary_Account_GRM__c)); 
            system.debug('vvvvvvv'+MapATM);
            
            if(MapATM.get(string.valueof(a.id)+string.valueof(a.Primary_Account_GRM__c)+'BD Rep')<>null)
            {
                aatype= MapATM.get(string.valueof(a.id)+string.valueof(a.Primary_Account_GRM__c)+'BD Rep');
                
                aatype.Team_Member_Role__c='GRM';
                AccArchListUpdated.add(aatype);
            }
           /* 
            else
            { 
            aatype.Account__c= a.id;  
            aatype.Archetype__c=a.Archetype__c;   
            aatype.User__c=a.Primary_Account_GRM__c;
            aatype.Team_Member_Role__c='GRM';
            aatype.Account_Access__c='Read/Write';   
            aatype.Opportunity_Access__c='Read/Write';
            aatype.Contact_Access__c='Read/Write';
            aatype.Case_Access__c='Read/Write';
            aatype.BD__c = accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
            aatype.CP__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
            aatype.GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;  
            aatype.Benchmark_GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;
            AccListUpdated.add(aatype);
                
            }  */  
        }
                           
        if(sAccOld.Primary_Account_GRM__c!= a.Primary_Account_GRM__c  )       
        {
            AccountArchetype__c AccountTeamJunctionNew = new AccountArchetype__c();
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c)); 
                                           
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c)+'GRM')<>null)       
            {          
                AccountTeamJunctionNew .Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c)+'GRM').Id;
                
                if(sAccOld.Primary_Account_GRM__c !=  a.ownerid)
                        AccListdeleted.add(AccountTeamJunctionNew );     
                else
                {
                        AccountTeamJunctionNew.Team_Member_Role__c='BD Rep';
                        AccArchListUpdated.add(AccountTeamJunctionNew);             
                }
                system.debug('AccListdeleted grm=='+AccListdeleted);  
            }          
         }      
         
         //End  For GRM Patner 
         
         // Start For Owner 
                       
        if(a.OwnerId !=null && a.OwnerId<>sAccOld.OwnerId) 
         {    
                system.debug('inside the owner');  
                AccountArchetype__c AccountTeamJunction ;
                AccountTeamJunction = new AccountArchetype__c(); 
               // Archetype__c accArcade = [Select id, BD__c,CP__c,GRM__c from Archetype__c where id=:a.Archetype__c];
                AccountTeamJunction.Account__c= a.id; 
                AccountTeamJunction.Archetype__c=a.Archetype__c;    
                AccountTeamJunction.User__c=a.OwnerId;
                AccountTeamJunction.Team_Member_Role__c='BD Rep';    
                AccountTeamJunction.Account_Access__c='Read/Write';   
                AccountTeamJunction.Opportunity_Access__c='Read/Write';
                AccountTeamJunction.Case_Access__c='Read/Write';
                AccountTeamJunction.Contact_Access__c='Read/Write';
                AccountTeamJunction.BD__c = accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
                AccountTeamJunction.CP__c = accArcadeUpdateMap.get(a.Archetype__c).CP__c; 
                AccountTeamJunction.GRM__c = accArcadeUpdateMap.get(a.Archetype__c).GRM__c;  
                AccountTeamJunction.Benchmark_GRM__c =  accArcadeUpdateMap.get(a.Archetype__c).BD__c; 
                AccListUpdated.add(AccountTeamJunction);
                system.debug('AccListUpdated3'+AccListUpdated);
                                                  
         }                      
        if(sAccOld.OwnerId!= a.OwnerId )       
        {
            
            
            AccountArchetype__c AccountTeamJunctionNew = new AccountArchetype__c();
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.OwnerId));                                
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.OwnerId)+'BD Rep')<>null)       
            {          
                AccountTeamJunctionNew.Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.OwnerId)+'BD Rep').Id;                                         
                AccListdeleted.add(AccountTeamJunctionNew );     
                system.debug('AccListdeleted'+AccListdeleted);      
            }         
        } 
        
        //End For Owner                  
     }
     
     
     Database.SaveResult[] lsr = Database.insert(AccListUpdated,false);//insert any valid members then add their share entry if they were successfully added
     if(AccListdeleted.size()>0)            
     delete AccListdeleted;            
        
     if(AccArchListUpdated.size()>0)
     upsert AccArchListUpdated;
                                
     Integer newcnt=0;
     for(Database.SaveResult sr:lsr)
     {
         if(!sr.isSuccess())
         {
            Database.Error emsg =sr.getErrors()[0];
            system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
         }
         /*
         else
         {
            newShare.add(new AccountShare(UserOrGroupId=AccListUpdated[newcnt].UserId, AccountId=AccListUpdated[newcnt].AccountId, AccountAccessLevel='Edit',OpportunityAccessLevel='?Edit'));
         }*/
         newcnt++;           
     }
        }
    } 
    }
}