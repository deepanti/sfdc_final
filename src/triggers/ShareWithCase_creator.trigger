trigger ShareWithCase_creator on Case (before Update,After Update,After Insert) {
    if(singleexecuion.runOncecaseQSRM()){
        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Case' and DeveloperName = 'Support_Case'];
        List<CaseShare> csShareList = new List<CaseShare>();
        
        list<case> caseid=new list<case>();
        id Userid=system.userinfo.getuserid();
        List<case> caseupdate=new List<case>();
        
        
        For(case cs: trigger.new)
        {    
            caseid.add(cs);
        }
        
        List<user> Currentuser=[Select email from User where id=:caseid[0].createdbyid limit 1];
        List<contact> Conemail=[Select id from contact where Email=:Currentuser[0].email];
        // List<contact> Conemail=[Select id from contact where Email IN (Select email from User where id=:caseid[0].createdbyid limit 1)];
        
        if(trigger.isBefore)
        {    
            
            
            For(case c:Trigger.new)
            { system.debug('Contact lookup '+ c.Contact);
             if(!Conemail.isempty() && c.createdbyID != '00590000001HoitAAC' && c.Contactid== null )
             {
                 system.debug(Conemail[0].id);
                 
                 c.Contactid=Conemail[0].id;
             }
             
             if (Userinfo.getUiThemeDisplayed().equals('Theme3')) //user is using system
                 c.Origin='Web';
             else if (Userinfo.getUiThemeDisplayed().equals('Theme4t')) //user is using mobile
                 c.Origin='Mobile';
             
            }
            
        }
        
        if(trigger.isAfter)
        {    
            
            
            For(case c:Trigger.new)
            {    
                
                if(c.Recordtypeid!=RecType.id ){
                    
                    if(c.ownerid!=c.createdbyid)
                    {   CaseShare csShare = new CaseShare(); 
                     csShare.CaseAccessLevel = 'Read';
                     csShare.CaseId = c.id;
                     csShare.UserOrGroupId = caseid[0].createdbyid;
                     csShareList.add( csShare );
                    }
                }   
            }
            
            
            
            if(!csShareList.isempty())
                Insert csShareList;
            
            
        }
        
        if(trigger.isAfter && trigger.isInsert)
        {
            
            List<case> caselist=new List<case>();
            
            //Fetching the assignment rules on case
            AssignmentRule AR = new AssignmentRule();
            AR = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];
            
            //Creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
            
            for (Case caseObj : Trigger.new) 
            {
                
                caseList.add(new Case(id = caseObj.id));
                
                
            }
            Database.update(caseList, dmlOpts);
            
        }
        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
    }
}