/* Author: Ashish Garg
   Created Date: 25-01-2011 Updated Date:11-05-2011
   Description: This Trigger is used to update the Current Manager,Business Group,Project Name,Project Code
   Project Business Group,HR DU Name and Project HSL field from Employee Allocations
*/

trigger upsertmanagername on RMG_Employee_Allocation__c (after insert,after update)
{
   List<RMG_Employee_Master__c> empMasters = new List<RMG_Employee_Master__c>();
   List<RMG_Employee_Allocation__c> empAllocations = new List<RMG_Employee_Allocation__c>();
   List<ID> masterIds = new List<ID>();
      
   for(RMG_Employee_Allocation__c c: Trigger.new)
    {
      masterIds.add(c.RMG_Employee_Code__c);
    }
  
   empMasters = [select id, Current_Manager__c,Business_Group__c,Project__c,Project_Code__c,Project_HSL__c,Project_Business_Group__c,HR_DU_Name__c from RMG_Employee_Master__c where id in :masterIds];
   
   for(RMG_Employee_Master__c employee: empMasters)
    {
        List<ID> empIds = new List<ID>();
        empIds.add(employee.id);
        empAllocations = [select id,Manager_Name__c, Business_Group__c,Project__c,Project_Business_Group__c,Project_Code__c,Project_HSL__c,HR_DU_Name__c from RMG_Employee_Allocation__c where Status__c!='Past' AND Active__c != 'No' AND RMG_Employee_Code__c in :empIds];
    
        String currentmanager=null;
        String businessgroup=null;
        String Projectname=null;
        String Projectcode=null;
        String ProjectHSL=null;
        String ProjectBG=null;
        String HRDUName=null;

        for(RMG_Employee_Allocation__c empAllocation: empAllocations){
            if ((currentmanager == null) && (businessgroup == null)) {
                currentmanager =  empAllocation.Manager_Name__c;
                businessgroup =  empAllocation.Business_Group__c;             
                Projectname =  empAllocation.Project__c;             
                Projectcode =  empAllocation.Project_Code__c;             
                ProjectHSL =  empAllocation.Project_HSL__c;             
                ProjectBG =  empAllocation.Project_Business_Group__c;             
                HRDUName =  empAllocation.HR_DU_Name__c;             

            } 
        }
        employee.Current_Manager__c = currentmanager;
        employee.Business_Group__c =   businessgroup;
        employee.Project__c =   Projectname;
        employee.Project_Code__c =   Projectcode;
        employee.Project_HSL__c =   ProjectHSL;
        employee.Project_Business_Group__c =   ProjectBG;
        employee.HR_DU_Name__c =   HRDUName;


    }
   
      Update empMasters;
}