trigger GPDomainAddressTrigger on GP_Address__c (after delete, after insert, after update, after undelete, before delete, before insert, before update) 
{
    // Creates Domain class instance and calls appropriate methods
    map<string, GP_Sobject_Controller__c>  MapofAddressTrigger = GP_Sobject_Controller__c.getall();
    
    if(MapofAddressTrigger != null && MapofAddressTrigger.containsKey('gp_address__c')
       && MapofAddressTrigger.get('gp_address__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainAddress.class);
    }
    
}