trigger ShareOLIWithAccountGRMAndOppotunityOwner on OpportunityProduct__c (after insert,after update)
{
   // if(System.label.Bypass_ShareOLIWithAccountGRMAndOppotunityOwner_Trigger   =='True')
   // {
        if(!singleexecuion.bool_ShareOLIWithAccountGRMAndOppotunityOwner)
        {
            singleexecuion.bool_ShareOLIWithAccountGRMAndOppotunityOwner=true;
            list<OpportunityProduct__Share> LstToInsert = new List<OpportunityProduct__Share>();
            set<Id> OpportunityId = new set<id>();
            for(OpportunityProduct__c TempObj : trigger.new)
            {
                OpportunityId.add(TempObj.Opportunityid__c);
                system.debug('!@!@@@'+OpportunityId);
            }
            Map<Id,Opportunity> OppMap = new Map<Id,Opportunity>([select id,name,ownerId,accountId,Account.Sales_Unit__c from Opportunity where id in :OpportunityId]);
            set<Id> AccountIdSet = new set<id>();
            
                for(Opportunity TempOppObj : OppMap.values())
            {
                AccountIdSet.add(TempOppObj.accountId);
                
            }
           // map<Id,Account> AllAccount = [select id,name,Sales_Unit__c from Account where id in : AccountIdSet];
            // As discuss with bussiness in Sales_Unit__c there cannot be more the 50 records that why i have write uncodtional query
            Map<Id,Sales_Unit__c> SalesUnitSalesLeadMap = new Map<Id,Sales_Unit__c>([select id,name,Sales_Leader__c from Sales_Unit__c where Sales_Lead_Active__c = true limit 10000]);
            list<AccountTeamMember> AccountTeamMemberLst = [select id,UserId, AccountId,TeamMemberRole  from AccountTeamMember where AccountId in : AccountIdSet];
            map<Id,list<AccountTeamMember>> AccTeamMemberMap = new map<Id,list<AccountTeamMember>>();
            Map<id,account> accountownermap=new map<id,account>();
            for(account accountobject:[select id,ownerid from account where id in:AccountIdSet])
            {
                accountownermap.put(accountobject.id,accountobject);
            }
            for(AccountTeamMember TempAccountMember : AccountTeamMemberLst)
            {
                if(AccTeamMemberMap.containskey(TempAccountMember.AccountId))
                {
                    list<AccountTeamMember> TempLst = AccTeamMemberMap.get(TempAccountMember.AccountId);
                    TempLst.add(TempAccountMember);
                    AccTeamMemberMap.put(TempAccountMember.AccountId,TempLst);
                }
                else
                {
                    list<AccountTeamMember> TempLst = new list<AccountTeamMember>();
                    TempLst.add(TempAccountMember);
                    AccTeamMemberMap.put(TempAccountMember.AccountId,TempLst);
                }
            }
         
            for(OpportunityProduct__c TempObj : trigger.new)
            {
                if(OppMap.containskey(TempObj.Opportunityid__c))
                {
                    /*<<Date Modified: 21 April 2015, Deployment Date: May 1 2015, Bhanu Prasad, Verified:  Sameer Shah, Jatin Bagri>>: Code commneted for stopping addition of account team memeber to the OLI sharing team, The code adds a person added as GRM in account team to OLI sharing team*/
                    /*if(AccTeamMemberMap.containskey(OppMap.get(TempObj.Opportunityid__c).AccountId ))
                    {
                        for(AccountTeamMember TeamMemberObj : AccTeamMemberMap.get(OppMap.get(TempObj.Opportunityid__c).AccountId))
                        {
                     if(TeamMemberObj.TeamMemberRole == 'GRM') // as discuss with DC GRM is the King Of account so all the OLI is get shared with him
                             {
                        OpportunityProduct__Share OLIShr  = new OpportunityProduct__Share();
                                OLIShr.ParentId = TempObj.id;
                                OLIShr.UserOrGroupId = TeamMemberObj.UserId;
                                OLIShr.AccessLevel = 'All';
                                LstToInsert.add(OLIShr);
                             }
                    }
                    }*/
                    /*<<Date Modified: 20 April 2015, Deployment Date: May 1 2015, Bhanu Prasad, Verified: Sameer Shah, Jatin Bagri>>: Code to give access to the account owner and add him up inside the OLI sharing team*/
                    /* For Account owner access*/
                   if(accountownermap.containskey(OppMap.get(TempObj.Opportunityid__c).accountID))
                   { 
                       system.debug('map test'+accountownermap.containskey(OppMap.get(TempObj.Opportunityid__c).accountID));
                        OpportunityProduct__Share ACCOwnerOLIShr  = new OpportunityProduct__Share();
                        ACCOwnerOLIShr.ParentId = TempObj.id;
                        ACCOwnerOLIShr.UserOrGroupId = accountownermap.get(OppMap.get(TempObj.Opportunityid__c).accountID).ownerID;
                       ACCOwnerOLIShr.AccessLevel = 'Read';
                        LstToInsert.add(ACCOwnerOLIShr);
                         system.debug('@@@12345'+ accountownermap.get(OppMap.get(TempObj.Opportunityid__c).accountID).ownerID);
                        system.debug('!@!@@@LstToInser'+LstToInsert);
                    }
                    
                    OpportunityProduct__Share OppOwnerOLIShr  = new OpportunityProduct__Share();
                    OppOwnerOLIShr.ParentId = TempObj.id;
                    OppOwnerOLIShr.UserOrGroupId = OppMap.get(TempObj.Opportunityid__c).ownerId;
                    OppOwnerOLIShr.AccessLevel = 'Read';
                    LstToInsert.add(OppOwnerOLIShr);
                     system.debug('@@@12345'+ OppMap.get(TempObj.Opportunityid__c).ownerId);
                    system.debug('!@!@@@LstToInser'+LstToInsert);
               
                  
                    if(SalesUnitSalesLeadMap.containskey(OppMap.get(TempObj.Opportunityid__c).Account.Sales_Unit__c))
                    {
                        OpportunityProduct__Share OppOwnerOLIShrSLShare  = new OpportunityProduct__Share();
                        OppOwnerOLIShrSLShare.ParentId = TempObj.id;
                        OppOwnerOLIShrSLShare.UserOrGroupId = SalesUnitSalesLeadMap.get(OppMap.get(TempObj.Opportunityid__c).Account.Sales_Unit__c).Sales_Leader__c;
                        OppOwnerOLIShrSLShare.AccessLevel = 'Read';
                        LstToInsert.add(OppOwnerOLIShrSLShare);     
                    }
                    
                }
            }
                //  OpportunityProduct__Share OLIShr  = new OpportunityProduct__Share();
               
            
                // Insert the sharing record and capture the save result. 
                  // The false parameter allows for partial processing if multiple records passed 
                  // into the operation.  
                if(LstToInsert.size()>0)
                {
                    system.debug('%#%@#$#%#'+LstToInsert);
                    list<Database.SaveResult> srList = Database.insert(LstToInsert,false);
                    for (Database.SaveResult sr : srList) 
                    {
                        if (sr.isSuccess()) 
                        {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted OLI sharing. OLI sharing: ' + sr.getId());
                        }
                        else 
                        {
                            // Operation failed, so get all errors                
                            for(Database.Error err : sr.getErrors()) 
                            {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('OLI fields that affected this error: ' + err.getFields());
                            }
                        }
                    }
                }      
        }
   // }
}