trigger OpportunityLineItemTrigger on OpportunityLineItem (after insert, after update, after delete, before insert, before update) {
   if(label.OLI_Trigger== 'false'){
    ID loggeedInUserProfileID = UserInfo.getProfileId();
    String profileName = [Select Name from Profile Where ID =: loggeedInUserProfileID].Name;
    
    if(Trigger.isInsert){
         if(!Test.isRunningTest()){ 
            if(Trigger.isBefore && CheckRecursiveForOLI.runOnceBefore() && Label.Data_Loader_Upload == 'true' && profileName == 'Genpact Super Admin'){    OpportunityLineItemTriggerHelper.InsertAdditionalfieldsInOpportunityLineItems(Trigger.new);
            }    
        }        
        if(Trigger.isAfter ){
            System.debug('Limit Query Rows OpportunityLineItemTriggerHelper'+System.limits.getQueryRows());
            boolean runOnceFlag = CheckRecursiveForOLI.runOnceAfter();
            system.debug(':---runOnceFlag---:'+runOnceFlag);
            if(Label.Data_Loader_Upload == 'true' && runOnceFlag && !Test.isRunningTest() && profileName == 'Genpact Super Admin'){ 
                OpportunityLineItemTriggerHelper.createRevenueSchedule(Trigger.new); 
            }
            if(runOnceFlag){  
                system.debug(':---insertOpportunityTeamMember---:'+runOnceFlag);
                OpportunityLineItemTriggerHelper.insertOpportunityTeamMember(Trigger.new); 
                opportunityLineItemTriggerHelper.updateYearlyRevenue(Trigger.newMap.keySet()); 
            }      
        }
    }
        if(Trigger.isUpdate){ 
            if(Trigger.isAfter){
                boolean runOnceFlag = CheckRecursiveForOLI.runOnceAfter();  
                if(Label.Data_Loader_Upload == 'true' && profileName == 'Genpact Super Admin'){ 
                    if(runOnceFlag){   OpportunityLineItemTriggerHelper.updateAdditionalfieldsInOpportunityLineItems((Trigger.newMap).keySet()); 
                    }
                } 
                Boolean runthriceFlag;// =true;
                // Modified by Ashish Srivastav 
                // 14/12/2018
                
                if(Test.isRunningTest()){
                    runthriceFlag = CheckRecursiveForOLI.runingTestFlag();
                }else{ runthriceFlag = true;//CheckRecursiveForOLI.OpptriggerRecursiveMehtod();
                }
                System.debug('runthriceFlag ==== '+runthriceFlag);
                if(runthriceFlag){ 
                    OpportunityLineItemTriggerHelper.updateOpportunityFields(Trigger.new);
                }
                if(runOnceFlag){ 
                    OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(Trigger.OldMap, Trigger.new); opportunityLineItemTriggerHelper.updateYearlyRevenue(Trigger.newMap.keySet());    
                }
                
            }
        }
        
        if(Trigger.isDelete){ 
            boolean flag = CheckRecursiveForOLI.runOnceAfterDelete();  System.debug('flagflag==='+flag); if(flag){ OpportunityLineItemTriggerHelper.deleteOpportunityTeamMember(Trigger.Old);  OpportunityLineItemTriggerHelper.updateOpportunityFields(Trigger.old);  
            }
        }
        }
    }