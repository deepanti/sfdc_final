trigger TeamRights on Opportunity (Before Insert) 
{
    Set<String>SetAccid=new Set<String>();
    Set<String>SetQSRMapprover=new Set<String>();
    Set<String>SetTeamMember=new Set<String>();
    Set<String>SetSalesUnit =new Set<String>();
    Set<String>SetSalesLeader =new Set<String>();
    Id UID=userinfo.getUserId();
    boolean ByPass_Account_Team = false;
    boolean Legal_person=false;
  
    
    Id PID=userinfo.getProfileId();
    
    if(System.label.Bypass_Team_right_trigger=='True') /*genpact super admin profile bypass Sep2 2016, Sameer Shah*/
    {
    
          list <user> userlist =[select Id,Legal_Person__c, ByPass_Account_Team__c from User where Id=:UID LIMIT 1];
    if (userlist[0].ByPass_Account_Team__c == True)
        {
            
            ByPass_Account_Team = true;
            }
    if (userlist[0].Legal_person__c == True)
        {
            Legal_person = true;
            }   
    
     for (Opportunity opp : Trigger.new) 
     {
            SetAccid.add(opp.AccountId);          
     }  
     
    
     List <AccountArchetype__c> accteammemberlist = [select User__c from AccountArchetype__c where Account__c IN :SetAccid];
     
           for(AccountArchetype__c teamMember:accteammemberlist )
           {
               SetTeamMember.add(teamMember.User__c);                          
           }
           
    List <Account> QSRMapproverlist = [select QSRM_Approver__c from Account where ID IN :SetAccid]; // list of QSRM approver on ACCOUNT
    
    for(Account QSRMapprover:QSRMapproverlist )
           {
               SetQSRMapprover.add(QSRMapprover.QSRM_Approver__c);                          
           }
     
    List <Sales_Unit__c> salesleaderlist = [Select Sales_Leader__c from Sales_Unit__c where ID IN (select Sales_Unit__c from Account where ID IN :SetAccid) AND Sales_Leader__r.IsActive = True LIMIT 1]; //list of sales leader
 
           for(Sales_Unit__c salesleader:salesleaderlist )
           {
               SetSalesLeader.add(salesleader.Sales_Leader__c);                          
           }
           
           
           for(opportunity oppty:trigger.new)
          
           {
              /*
               00e9000000125i2 - System Administrator
                00e90000001aFVI - Genpact Super Admin
                00e90000001aE3j - Genpact SEE Team
                00e90000001aE3k - Genpact Shared Services
                00e90000001aQFi - Quest Super Admin
                00e900000026tvOAAQ - Genpact tower Lead
               */
 
                
               if(label.Profile_Genpact_SOM <> PID && !SetTeamMember.contains(UID) && !SetSalesLeader.contains(UID)  && !SetQSRMapprover.contains(UID)  &&  (PID <> '00e9000000125i2' && PID<> '00e90000001aFVI' && PID <>'00e90000001aE3j'&& PID <>'00e90000001aE3k' && PID <>'00e90000001aQFi' && PID <>'00e900000026tvO') && Legal_person==false && (oppty.stagename!='Prediscover')  && (oppty.stagename!='Pre-discover Dropped') && ByPass_Account_Team==false && oppty.QSRM_Auto_Approved__c==false && oppty.QSRM_Auto_Rejected__c==false )
               {
                  oppty.adderror('User is NOT listed as ACCOUNT TEAM MEMBER. Only user added to the Account team can add or modify opportunity details. Reach out to Account Owner to get yourself added to the account team');
               
               }      
          }

    }
}