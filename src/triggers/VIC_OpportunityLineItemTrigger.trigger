trigger VIC_OpportunityLineItemTrigger on OpportunityLineItem (after insert, after update, after delete, before insert, before update) {
   
    private VIC_OpportunityLineItemTriggerHandler objOLI = new VIC_OpportunityLineItemTriggerHandler();
   if(label.OLI_Trigger== 'false'){
    /*
        @Discription: It will validate if trigger is active or not?
        @Test Class: Not required
        @Author: Vikas Rajput
    */
    if(VIC_TriggerSwitchManager.isTriggerSwitchActive('Opportunity_Line_Item')){ //Checking trigger is active or not
        objOLI.runTriggerAction();
    }
    }
}