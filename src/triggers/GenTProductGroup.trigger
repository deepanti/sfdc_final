trigger GenTProductGroup on Product2 (before Insert, before update) {

 for(Product2  prod: trigger.new) {
 
 IF
 (prod.Product_Family__c=='Core-Ops'){
 
  prod.Formula_Product_Group__c='Vertical Specific BPM';
  
 }
 
  IF((prod.Product_Family__c=='Consulting')||(prod.Product_Family__c=='Risk Management')||(prod.Product_Family__c=='Analytics')||(prod.Product_Family__c=='Re - Engineering')||(prod.Product_Family__c=='SUPPORT'))
  {
   prod.Formula_Product_Group__c='Analytics';
  }
 
  IF(
 (prod.Product_Family__c=='Sourcing & Procurement')){
 
  prod.Formula_Product_Group__c='PSCS';
  
 }
 
  IF
 (prod.Product_Family__c=='IT Services'){
 
  prod.Formula_Product_Group__c='IT Services';
  
 }
 
  IF
 ((prod.Product_Family__c=='Procurement')||(prod.Product_Family__c=='Supply Chain')){
 
  prod.Formula_Product_Group__c='PSCS';
  
 }
 
  IF
 (prod.Product_Family__c=='F&A'){
 
  prod.Formula_Product_Group__c='F&A';
  
 }
 
 IF
 ((prod.Product_Family__c=='Collections')|| (prod.Product_Family__c=='Customer Service')){
 
  prod.Formula_Product_Group__c='Cust. Care*';
  
 }
 
  IF
 (prod.Product_Family__c=='HRO'){
 
  prod.Formula_Product_Group__c='HRO';
  
 }
      
 }
}