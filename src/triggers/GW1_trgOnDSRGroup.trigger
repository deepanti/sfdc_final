/*  --------------------------------------------------------------------------------------
	  Description: this trigger will  create chatter group on after insert of DSR group record,
	  and Change the owner of Chatter group 
	  
	  --------------------------------------------------------------------------------------
	  events : before insert, after insert, after update
	  --------------------------------------------------------------------------------------
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          05-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
 */
trigger GW1_trgOnDSRGroup on GW1_DSR_Group__c(before insert, after insert, after update)
{

	GW1_triggerSettings__c objSettings = GW1_triggerSettings__c.getValues('GW1_trgOnDSRGroup');
	if (objSettings != null && objSettings.GW1_Is_Active__c)
	{
		GW1_DSRGroupTriggerHandler objHandler = new GW1_DSRGroupTriggerHandler();
		objHandler.runTrigger();
	}
	else
	{
		System.debug('=====TRIGGER GW1_trgOnDSRGroup In Active From Custom Setting=====');
	}

}