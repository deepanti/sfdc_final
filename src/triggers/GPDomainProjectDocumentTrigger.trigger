trigger GPDomainProjectDocumentTrigger on GP_Project_Document__c (
  after delete, after insert, after update, after undelete, before delete, before insert, before update)
{
        // Creates Domain class instance and calls appropriate methods
        map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
        if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_project_document__c')
 && MapofProjectTrigger.get('gp_project_document__c').GP_Enable_Sobject__c)
        {
            fflib_SObjectDomain.triggerHandler(GPDomainProjectDocument.class);
        }
}