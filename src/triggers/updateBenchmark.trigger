//it is used for benchmark calculation of all team members based on their role.//

trigger updateBenchmark on AccountArchetype__c (after insert,after update, after delete) 
{
    if(BechmarkClass.firstRound)
    {
        BechmarkClass.firstRound = false;
        //AccountArchetype__c arc_new;
        
        //if(trigger.isDelete){arc_new  = Trigger.old[0];}
        //else{arc_new = Trigger.new[0];}
        Set<Id> AccountIdsInAccountTeam = new set<id>();
        map<string,integer> AccountTeamMemberCountInTeam = new Map<string,integer>();
        for(AccountArchetype__c aatObj: trigger.isdelete?trigger.old:trigger.new)
        {
            if(aatObj.Account__c != null)
                AccountIdsInAccountTeam.add(aatObj.Account__c);
        }
        if(AccountIdsInAccountTeam.size()>0)
        {
            List<AccountArchetype__c> AllArcList = [select id,Account__c,Benchmark_GRM__c,CP__c,BD__c,GRM__c,Team_Member_Role__c from AccountArchetype__c  where Account__c in : AccountIdsInAccountTeam];
            if(AllArcList.size()>0)
            {
                Map<Id,List<AccountArchetype__c>> AccountAndAccountTeamMap = new Map<Id,List<AccountArchetype__c>>();
                for(AccountArchetype__c TempAccountArcheTypeObj : AllArcList)
                {
                    if(AccountAndAccountTeamMap.containskey(TempAccountArcheTypeObj.Account__c))
                    {
                        List<AccountArchetype__c> TempLst = new List<AccountArchetype__c>();
                        TempLst.addall(AccountAndAccountTeamMap.get(TempAccountArcheTypeObj.Account__c));
                        TempLst.add(TempAccountArcheTypeObj);
                        AccountAndAccountTeamMap.put(TempAccountArcheTypeObj.Account__c,TempLst);
                    }
                    else
                    {
                        List<AccountArchetype__c> TempLst = new List<AccountArchetype__c>();
                        TempLst.add(TempAccountArcheTypeObj);
                        AccountAndAccountTeamMap.put(TempAccountArcheTypeObj.Account__c,TempLst);
                    }
                    String FinalKey = '';
                    FinalKey = TempAccountArcheTypeObj.Account__c+'__'+TempAccountArcheTypeObj.Team_Member_Role__c;
                    if(AccountTeamMemberCountInTeam.containskey(FinalKey))
                    {
                        integer TempCount = AccountTeamMemberCountInTeam.get(FinalKey)+1;
                        AccountTeamMemberCountInTeam.put(FinalKey,TempCount);
                    }
                    else
                    {
                        AccountTeamMemberCountInTeam.put(FinalKey,1);
                    }
                }   
                for(Id AccountIds : AccountIdsInAccountTeam)
                {
                    //Account acc = [Select Id from Account where id=:aatObj.Account__c];
                    List<AccountArchetype__c> currentArcList;
                    List<AccountArchetype__c> listToUpdate = new List<AccountArchetype__c>();
                    Integer currentCount = 0;
                    Double val = 0;
                    if(AccountAndAccountTeamMap.containskey(AccountIds))
                    {
                        for(AccountArchetype__c arc: AccountAndAccountTeamMap.get(AccountIds))
                        {
                            if(arc.Team_Member_Role__c!='Non Coverage Based Role')
                            {
                                //currentArcList = [Select id,Account__c,Benchmark_GRM__c,CP__c,BD__c,GRM__c,Team_Member_Role__c from AccountArchetype__c where Account__c=:acc.id AND Team_Member_Role__c =:arc.Team_Member_Role__c];
                                //currentCount = currentArcList.size();
                                String SearchKey = AccountIds+'__'+arc.Team_Member_Role__c;
                                if(AccountTeamMemberCountInTeam.containskey(SearchKey))
                                    currentCount = AccountTeamMemberCountInTeam.get(SearchKey);
                                else
                                    currentCount = 0;
                                if(arc.Team_Member_Role__c=='GRM')
                                {
                                    val=arc.GRM__c;
                                }
                                if(arc.Team_Member_Role__c=='Client Partner')
                                {
                                    val=arc.CP__c;
                                }
                                if(arc.Team_Member_Role__c=='BD Rep')
                                {
                                    val=arc.BD__c;
                                }
                                //if(trigger.isInsert && (arc.Team_Member_Role__c == aatObj.Team_Member_Role__c))
                                if(trigger.isInsert)
                                {   
                                    system.debug('#@!#@!#$@$#'+val);
                                    system.debug('#@!#@!#$@$#'+currentCount);
                                    system.debug('#@!#@!#$@$#'+arc.Benchmark_GRM__c);
                                    if(val != null && currentCount>0)
                                        arc.Benchmark_GRM__c = val/(currentCount);
                                }
                                else 
                                { 
                                    if(val != null && currentCount>0)
                                        arc.Benchmark_GRM__c = val/currentCount;
                                }
                                //listToUpdate.add(arc);
                            }
                            else
                            {
                                arc.Benchmark_GRM__c =null;
                            }
                            listToUpdate.add(arc);
                        }
                    }
                        if(listToUpdate.size()>0)
                            update listToUpdate;
                }
            }
        }
    }
}