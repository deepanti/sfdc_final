trigger Non_TS_Average_Cycle_Time on Opportunity (before update, after insert) 
{
    
   
    
    String NonTSByPassLabel = Label.NonTSTriggerByPass;
    system.debug('=======NonTSByPassLabel======='+NonTSByPassLabel);
    
    
    List <Deal_Cycle__c> DC= new list<Deal_Cycle__c>();
    
    Public Double Discover_age_hun_sole_retail_non_ts{get; set;}
    Public Double Discover_age_hun_sole_medium_non_ts{get; set;}
    Public Double Discover_age_hun_sole_large_non_ts{get; set;}
    Public Double Discover_age_hun_comp_retail_non_ts{get; set;}
    Public Double Discover_age_hun_comp_medium_non_ts{get; set;}
    Public Double Discover_age_hun_comp_large_non_ts{get; set;}
    Public Double Discover_age_min_sole_retail_non_ts{get; set;}
    Public Double Discover_age_min_sole_medium_non_ts{get; set;}
    Public Double Discover_age_min_sole_large_non_ts{get; set;}
    Public Double Discover_age_min_comp_retail_non_ts{get; set;}
    Public Double Discover_age_min_comp_medium_non_ts{get; set;}
    Public Double Discover_age_min_comp_large_non_ts{get; set;}
    Public Double Define_age_hun_sole_retail_non_ts{get; set;}
    Public Double Define_age_hun_sole_medium_non_ts{get; set;}
    Public Double Define_age_hun_sole_large_non_ts{get; set;}
    Public Double Define_age_hun_comp_retail_non_ts{get; set;}
    Public Double Define_age_hun_comp_medium_non_ts{get; set;}
    Public Double Define_age_hun_comp_large_non_ts{get; set;}
    Public Double Define_age_min_sole_retail_non_ts{get; set;}
    Public Double Define_age_min_sole_medium_non_ts{get; set;}
    Public Double Define_age_min_sole_large_non_ts{get; set;}
    Public Double Define_age_min_comp_retail_non_ts{get; set;}
    Public Double Define_age_min_comp_medium_non_ts{get; set;}
    Public Double Define_age_min_comp_large_non_ts{get; set;} 
    
    if(singleexecuion.runOnce()){
        
    if(NonTSByPassLabel != 'True')
    {
        system.debug('NonTSByPassLabel=='+NonTSByPassLabel);
        DC=[Select Stage__c,Ageing__c,Type_of_Deal__c,Type_of_deal_for_non_ts__c,Transformation_Deal__c,Hunting_Mining__c,Nature_Of_Work__c,Deal_Type__c,Average_cycle_time__c,Cycle_Time__c from Deal_Cycle__c  ];
        
        For(Deal_Cycle__c deal_cycle : DC)
        {
            if(deal_cycle.Stage__c=='1. Discover' )
            {
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail' ){
                    Discover_age_hun_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Discover_age_hun_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Discover_age_hun_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Discover_age_hun_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    system.debug('Discover_age_hun_comp_retail_non_ts:'+Discover_age_hun_comp_retail_non_ts);
                    
                }
                
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Discover_age_hun_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Discover_age_hun_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                
                
                
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Discover_age_min_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Discover_age_min_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Discover_age_min_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                    
                }
                
                
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Discover_age_min_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Discover_age_min_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Discover_age_min_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
            }
            
            if(deal_cycle.Stage__c=='2. Define')
            {
                
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Define_age_hun_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Define_age_hun_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Define_age_hun_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                
                
                
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Define_age_hun_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Define_age_hun_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Hunting' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Define_age_hun_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                
                
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Define_age_min_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Define_age_min_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Competitive' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Define_age_min_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                
                
                
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                    Define_age_min_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                    Define_age_min_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
                if(deal_cycle.Hunting_Mining__c=='Mining' && deal_cycle.Deal_Type__c=='Sole Sourced' && deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                    Define_age_min_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2);
                    
                }
            }
            
        }        
        
        if(Trigger.isUpdate && Trigger.isBefore)
        {  
            system.debug('in update');
            For(Opportunity o: trigger.new)
            {
                system.debug('in update loop');       
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ){  o.Insight__c=Discover_age_hun_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c; 
                    system.debug('debug 169'+o.Transformation_deal_ageing_for_non_ts__c+''+Discover_age_hun_sole_retail_non_ts);
                    
                    
                    system.debug('insight value is '+o.Insight__c); 
                    
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ){  o.Insight__c=Discover_age_hun_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ) {  o.Insight__c=Discover_age_hun_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ) {    o.Insight__c=Discover_age_hun_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Discover_age_hun_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ){    o.Insight__c=Discover_age_hun_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Discover_age_min_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) ){ o.Insight__c=Discover_age_min_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Discover_age_min_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) ) {   o.Insight__c=Discover_age_min_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){    o.Insight__c=Discover_age_min_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) ){    o.Insight__c=Discover_age_min_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }           
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Define_age_hun_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ) {   o.Insight__c=Define_age_hun_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                    system.debug('insight is:'+o.Insight__c+ o.id);
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ){    o.Insight__c=Define_age_hun_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ){   o.Insight__c=Define_age_hun_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) ) {   o.Insight__c=Define_age_hun_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) ){   o.Insight__c=Define_age_hun_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Define_age_min_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) ){   o.Insight__c=Define_age_min_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){    o.Insight__c=Define_age_min_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' )) {    o.Insight__c=Define_age_min_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) ){   o.Insight__c=Define_age_min_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) ){   o.Insight__c=Define_age_min_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                 
                /*  if(Trigger.isInsert)
{
Database.Update(o);
}
*/
                system.debug('insight value is '+o.Insight__c+ o.id); 
                system.debug('debug 1'+o);
            }
        }
        
        if(Trigger.isInsert)
        {
            List<Opportunity> op = [SELECT Id, stagename,Insight__c,Type_of_deal_for_non_ts__c,Formula_Hunting_Mining__c,Deal_Type__c,Transformation_deal_ageing_for_non_ts__c FROM Opportunity WHERE Id IN: Trigger.newMap.keySet()];
            For(Opportunity o: op)
            {
                
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    
                    o.Insight__c=Discover_age_hun_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                    
                    system.debug('insight value is '+o.Insight__c); 
                    
                }
                If(( o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    system.debug('abc'+o.Insight__c);
                    o.Insight__c=Discover_age_hun_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Discover_age_hun_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Discover_age_hun_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Discover_age_hun_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Discover_age_hun_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Discover_age_min_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Discover_age_min_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Discover_age_min_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Discover_age_min_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Discover_age_min_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='1. Discover' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Discover_age_min_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }           
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_hun_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_hun_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                    system.debug('insight is:'+o.Insight__c+ o.id);
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_hun_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_hun_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='2. Define' && o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_hun_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Hunting' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_hun_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_min_sole_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Retail' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_min_comp_retail_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_min_sole_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Medium' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_min_comp_medium_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }                                               
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Sole Sourced' ) )
                {   
                    o.Insight__c=Define_age_min_sole_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
                If((o.stagename=='2. Define' &&  o.Type_of_deal_for_non_ts__c=='Large' && o.Formula_Hunting_Mining__c=='Mining' && o.Deal_Type__c=='Competitive' ) )
                {   
                    o.Insight__c=Define_age_min_comp_large_non_ts-o.Transformation_deal_ageing_for_non_ts__c;
                }
            }
            update op;
        }}
    }
}