trigger FileUploadTrigger on ContentDocumentLink (before insert) {
    
    
    for(ContentDocumentLink contentDocLink : Trigger.New ){
            
            String objectType = String.valueOf(contentDocLink.LinkedEntityId.getsobjecttype());
            System.debug('objectType: ' + objectType);
        
        if(objectType == 'Files_and_Notes__c') {
            contentDocLink.ShareType	= 'I';			// Allow for Infered access. This allows non-admins to delete files
            contentDocLink.Visibility 	= 'AllUsers'; 	// Make the file visibile to all users, both internal and external, who already have access to the linked record
        }
    }
}