trigger GPDomainProjectDelete on GP_Project__c (after update) {
    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('GP_Project__Delete__c') && 
       MapofProjectTrigger.get('GP_Project__Delete__c').GP_Enable_Sobject__c) 
   {
	   GPDomainProjectDelete projectDelete = new GPDomainProjectDelete();
	   // BPH Change : have to make the triggered PID as parent of its sibling (Biz-Prod hierarchy update)
       projectDelete.reparentingThePIDs(trigger.new,trigger.OldMap);
	   projectDelete.deleteParentApprovedProjectAndMoveVersionHistory(trigger.new,trigger.OldMap);
	   projectDelete.UpdateOracleSyncStatusVersion(trigger.new,trigger.OldMap);
	   projectDelete.generateComparisonFile(trigger.new,trigger.OldMap);
	   
	   // For sending email notifications on Oracle successfully.
	   if(!GPDomainProject.skipRecursiveCall) {
		   projectDelete.sendEmailNotificationOnOracleSync(trigger.new,trigger.oldmap);
	   }
	   
	   // APC Change
		projectDelete.updateAccountWithPIDNumbers(trigger.new,trigger.OldMap);
   }
}