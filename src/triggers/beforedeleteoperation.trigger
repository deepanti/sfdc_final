/*This trigger is used for checking only account owner can add the account team member in the Account team.And for deleting the account team member check for open opps*/

trigger beforedeleteoperation on AccountArchetype__c (before delete) 
{
    set<id> useridset=new set<id>();
    set<id> accountidset=new set<id>();
    List<Opportunity> Oppfinal= new List<Opportunity>();
    Try{
    for(AccountArchetype__c accountteamobject:trigger.old)
    {
        useridset.add(accountteamobject.User__c);
        accountidset.add(accountteamobject.Account__c);
    }
    map<id,opportunity> opportunitymap=new map<id,opportunity>();
    list<opportunity> opportunityobject=[select id,accountid,stagename,ownerid from opportunity where ownerid=:useridset AND accountid=:accountidset AND 
   (stagename='1. Discover' OR stagename='2. Define' OR stagename='3. On Bid' OR stagename='4. Down Select' OR stagename='5. Confirmed' )];
   
   List<Account> Acc=[select Id,ownerid,Automation__c from Account where id=:accountidset];
   
  
   
    for(opportunity opportunitylist:opportunityobject)
    {
        opportunitymap.put(opportunitylist.accountid, opportunitylist);
    }
    
    
    
    
        for(AccountArchetype__c accountteamobject : trigger.old)
        {       ID UID= userinfo.getUserId();
                ID PID= userinfo.getProfileId();
                //ID Owner= accountteamobject.Account__r.ownerid;
                ID Owner= Acc[0].ownerid; 
                 boolean check = Acc[0].Automation__c; 
                System.debug('Chk owner is same as logged in user  ' + PID + Owner  );
                //Bypassing the profiles : Genpact Super Admin,genpact Shared Services,Quest Super ADmin,System Administrator,genpact See team
            IF( UID<>Owner && (PID <> '00e9000000125i2' && PID<> '00e90000001aFVI' && PID <>'00e90000001aE3j'&& PID <>'00e90000001aE3k'&& PID <>'00e90000001aQFi') &&  check == false)
            {    System.debug('Chk  ' + PID + Owner  );
                accountteamobject.adderror('You are not Authorized');
                break;
            }
            
            
            
            
            else if(opportunitymap.containsKey(accountteamobject.Account__c)&& accountteamobject.Confirm_to_Delete__c==false && (PID <> '00e9000000125i2' && PID<> '00e90000001aFVI' && PID <>'00e90000001aE3j'&& PID <>'00e90000001aE3k'&& PID <>'00e90000001aQFi') && check ==false)
            {
                accountteamobject.Confirm_to_Delete__c.adderror('<Font color="red">'+'<b>'+'ERROR:This team member has a live opportunity therefore cannot be removed from the team. In case you still want to remove team member then reach out to SEE Team'+'</b>'+'</Font>',false);
                //Oppfinal.add((opportunitymap.values()));
            
            }
            Else
            {            try
                            {
                                /*For(opportunity Op : opportunityobject)
                                    {   
                                        System.debug('Opp owner '+op.ownerid);
                                        For(Account Accountfinal : Acc)
                                              {  op.ownerid = Accountfinal.ownerid;
              
                                                     System.debug('Acc owner '+Accountfinal.ownerid); 
                                              }
                                       update op;
                                     }*/
                             }
      
      
                          Catch(Exception e)
                          { 
                                 // trigger.old[0].adderror('Please update opportunities first');
                          }
   
            }
            
        }
    }
    Catch(Exception e){
                
            trigger.old[0].adderror('Please Contact you Administrator');
    }
    
    
    
    
    
}