/* This trigger is to update Result, Condition Applicable, priority
BP result is computed in QSRM by using this logic
Version 2.0, Created By: Amit Chaturvedi, Created date: 8th,July,2015, Deployment date: 19th, July,2015*/


trigger GenTfieldupdate on QSRM__c (before Insert, before update) {
    
    /*
    for(QSRM__c  qs: trigger.new) {
        
        
        IF(qs.Revenue_Product__c=='HRO')
        {
            qs.Result__c='Does not meet blue print criteria';
        }
        
        
        else if ((qs.Revenue_Product__c=='IT Services' || qs.Revenue_Product__c=='Analytics' || qs.Revenue_Product__c=='PSCS' || qs.Revenue_Product__c=='F&A') &&
                 (qs.SubIndustry_Vertical__c=='BFS'
                  ||qs.SubIndustry_Vertical__c=='Insurance - P & C'
                  ||qs.SubIndustry_Vertical__c=='Healthcare – Provider'
                  ||qs.SubIndustry_Vertical__c=='Healthcare - Payor'
                  ||qs.SubIndustry_Vertical__c=='Life Sciences'
                  ||qs.SubIndustry_Vertical__c=='CPG'
                  ||qs.SubIndustry_Vertical__c=='Manufacturing-Automotive'
                  ||qs.SubIndustry_Vertical__c=='Manufacturing-Industrial Machinery'
                  ||qs.SubIndustry_Vertical__c=='TTL & Infra'
                  ||qs.SubIndustry_Vertical__c=='Manufacturing – Aerospace & Defense'
                  ||qs.SubIndustry_Vertical__c=='High Tech (Comm equip, SemiCs, Hardware)'
                  ||qs.SubIndustry_Vertical__c=='High Tech (Consumer Electronics)'
                  ||qs.SubIndustry_Vertical__c=='High Tech (Software and Internet)'
                  ||qs.SubIndustry_Vertical__c=='Capital Markets'
                  ||qs.SubIndustry_Vertical__c=='Hospitality'))
        {
            qs.Result__c='Meets blue print criteria';
        }
        
        
        
        
        else if ((qs.Revenue_Product__c=='IT Services' || qs.Revenue_Product__c=='Cust. Care*') &&
                 (qs.SubIndustry_Vertical__c=='Insurance - Life'
                  ||qs.SubIndustry_Vertical__c=='Retail'
                  ||qs.SubIndustry_Vertical__c=='Manufacturing-Process Industries'
                  ||qs.SubIndustry_Vertical__c=='Natural Resources'
                  ||qs.SubIndustry_Vertical__c=='Utilities'
                  ||qs.SubIndustry_Vertical__c=='Telecom'
                  ||qs.SubIndustry_Vertical__c=='MPE'
                  ||qs.SubIndustry_Vertical__c=='Government'
                  ||qs.SubIndustry_Vertical__c=='Other Services'))
        {
            qs.Result__c='Does not meet blue print criteria';
        }
        
        
        
        else if ((qs.Revenue_Product__c=='Vertical Specific BPM' || qs.Revenue_Product__c=='Cust. Care*') &&
                 (qs.SubIndustry_Vertical__c=='High Tech (Comm equip, SemiCs, Hardware)'
                  ||qs.SubIndustry_Vertical__c=='High Tech (Consumer Electronics)'
                  ||qs.SubIndustry_Vertical__c=='High Tech (Software and Internet)' 
                  || qs.SubIndustry_Vertical__c=='Life Sciences'
                  ||qs.SubIndustry_Vertical__c=='CPG'
                  ||qs.SubIndustry_Vertical__c=='Capital Markets'))
        {
            qs.Result__c='Meets blue print criteria';
        }
        
        else if((qs.SubIndustry_Vertical__c=='Utilities') &&(qs.Revenue_Product__c=='Analytics'))
        {
            qs.Result__c='Meets blue print criteria (only if Power Generation side of business)';
        }   
        
        
        else if((qs.SubIndustry_Vertical__c=='BFS' 
                 || qs.SubIndustry_Vertical__c=='Insurance - P & C' 
                 || qs.SubIndustry_Vertical__c=='Healthcare – Provider'
                 || qs.SubIndustry_Vertical__c=='Healthcare - Payor')&&
                (qs.Revenue_Product__c=='Vertical Specific BPM'))
        {
            qs.Result__c='Meets blue print criteria';
        }
        
        
        
        else if((qs.SubIndustry_Vertical__c=='Manufacturing-Industrial Machinery') &&
                (qs.Revenue_Product__c=='Cust. Care*'))
        {
            qs.Result__c='Meets blue print criteria';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='Manufacturing-Process Industries' 
                 || qs.SubIndustry_Vertical__c=='Natural Resources' 
                 || qs.SubIndustry_Vertical__c=='MPE')&&
                (qs.Revenue_Product__c=='Analytics'))
        {
            qs.Result__c='Meets blue print criteria';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='Manufacturing-Automotive' 
                 || qs.SubIndustry_Vertical__c=='TTL & Infra' 
                 || qs.SubIndustry_Vertical__c=='Manufacturing – Aerospace & Defense'
                 ||qs.SubIndustry_Vertical__c=='Hospitality'
                 ||qs.SubIndustry_Vertical__c=='Manufacturing-Process Industries'
                 ||qs.SubIndustry_Vertical__c=='Natural Resources'
                 ||qs.SubIndustry_Vertical__c=='Utilities'
                 ||qs.SubIndustry_Vertical__c=='Telecom'
                 ||qs.SubIndustry_Vertical__c=='MPE'
                 ||qs.SubIndustry_Vertical__c=='Government'
                 ||qs.SubIndustry_Vertical__c=='Other Services')&&
                (qs.Revenue_Product__c=='Vertical Specific BPM'))
        {
            qs.Result__c='N/A';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='BFS' 
                 || qs.SubIndustry_Vertical__c=='Insurance - P & C' 
                 || qs.SubIndustry_Vertical__c=='Healthcare – Provider'
                 || qs.SubIndustry_Vertical__c=='Healthcare - Payor')&&
                (qs.Revenue_Product__c=='Cust. Care*'))
        {
            qs.Result__c='Meets blue print criteria, if part of Corp ops';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='Insurance - Life' 
                 || qs.SubIndustry_Vertical__c=='Retail' 
                 || qs.SubIndustry_Vertical__c=='Manufacturing-Process Industries'
                 || qs.SubIndustry_Vertical__c=='Natural Resources'
                 || qs.SubIndustry_Vertical__c=='Utilities'
                 || qs.SubIndustry_Vertical__c=='Telecom'
                 || qs.SubIndustry_Vertical__c=='MPE')&&
                (qs.Revenue_Product__c=='F&A'||qs.Revenue_Product__c=='PSCS'))
        {
            qs.Result__c='Meets blue print criteria if TCV>10M, AOI>15%';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='Insurance - Life' || qs.SubIndustry_Vertical__c=='Retail' )&&
                (qs.Revenue_Product__c=='Vertical Specific BPM'||qs.Revenue_Product__c=='Analytics'))
        {
            qs.Result__c='Does not meet blue print criteria';
        }
        
        
        
        else if((qs.SubIndustry_Vertical__c=='Government' || qs.SubIndustry_Vertical__c=='Other Services' )&&
                (qs.Revenue_Product__c=='F&A'||qs.Revenue_Product__c=='PSCS' ||qs.Revenue_Product__c=='Analytics'))
        {
            qs.Result__c='Does not meet blue print criteria';
        }
        
        
        else if((qs.SubIndustry_Vertical__c=='Telecom' )&&
                (qs.Revenue_Product__c=='Analytics'))
        {
            qs.Result__c='Does not meet blue print criteria';
        }
        
        else if (( qs.Revenue_Product__c=='Cust. Care*') && 
                 (qs.SubIndustry_Vertical__c=='Manufacturing-Automotive' ||qs.SubIndustry_Vertical__c=='TTL & Infra' ||qs.SubIndustry_Vertical__c=='Manufacturing-Automotive' || qs.SubIndustry_Vertical__c=='Hospitality' ))
        {
            qs.Result__c='Does not meet blue print criteria';
        }   
        
        else 
        {
            qs.Result__c='N/A';
        }
        
        if
            (qs.SubIndustry_Vertical__c=='BFS'|| qs.SubIndustry_Vertical__c=='CPG'||qs.SubIndustry_Vertical__c=='Insurance - P & C'||qs.SubIndustry_Vertical__c=='Life Sciences'||qs.SubIndustry_Vertical__c=='Manufacturing-Industrial Machinery'){
                qs.Priority__c='Group 1';}
        
        
        
        else if
            (qs.SubIndustry_Vertical__c=='Capital Markets'||qs.SubIndustry_Vertical__c=='Healthcare - Payor'||qs.SubIndustry_Vertical__c=='Healthcare – Provider'){
                qs.Priority__c='Group 2';}
        
        
        else if
            
            (qs.Industry_Vertical__c=='High Tech'||qs.SubIndustry_Vertical__c=='Manufacturing – Aerospace & Defense'||qs.SubIndustry_Vertical__c=='Manufacturing-Automotive'||qs.SubIndustry_Vertical__c=='TTL & Infra'){
                qs.Priority__c='Group 3';}
        
        
        
        else if
            
            (qs.SubIndustry_Vertical__c=='Government'||qs.SubIndustry_Vertical__c=='Insurance - Life'||qs.SubIndustry_Vertical__c=='Manufacturing-Process Industries'||qs.SubIndustry_Vertical__c=='MPE'||qs.SubIndustry_Vertical__c=='Natural Resources'||qs.SubIndustry_Vertical__c=='Other Services'||qs.SubIndustry_Vertical__c=='Retail'||qs.SubIndustry_Vertical__c=='Telecom'||qs.SubIndustry_Vertical__c=='Utilities'){
                qs.Priority__c='Group 4';}
        
        
        else  {qs.Priority__c='N/A';}
        
        
        IF
            (qs.Sales_Genpact_Region__c=='Emerging Markets'&& qs.Sales_country__c=='South Africa'){
                qs.Condition_Applicable__c= '"Rengg with local staff India delivery @15% AOI"';
            }
        
        else if
            
            (qs.Sales_Region__c=='South America' && qs.Sales_Genpact_Region__c=='Emerging Markets')
        {
            qs.Condition_Applicable__c='-Global deals only or if the opportunity provides a view into a global deal  \r\n -Okay if extension to an existing global relationship with 15% AOI threshold \r\n -Re-eng deals with a minimum AOI of 15%';
        }
        
        else if
            (qs.Sales_country__c=='India' && qs.Sales_Genpact_Region__c=='Emerging Markets')
        {
            qs.Condition_Applicable__c='-Global deals only or if the opportunity provides a view into a global deal \r\n -Okay if extension to an existing global relationship with 15% AOI threshold\r\n -Re-eng deals with a minimum AOI of 15%';
        }
        
        else if
            (qs.Sales_Genpact_Region__c=='Emerging Markets'&& qs.Sales_Region__c!='South America' && qs.Sales_country__c!='India' && qs.Sales_country__c!='China')
        {
            qs.Condition_Applicable__c='As part of Global deal (In case of Middle East, India delivery or as part of global deal but with AOI >15% as part of all other Emerging Market)';
        }
        
        else if
            (qs.Sales_Genpact_Region__c=='US'||qs.Sales_Genpact_Region__c=='Europe'||qs.Sales_Genpact_Region__c=='Australia'||qs.Sales_Genpact_Region__c=='Japan')
        {
            qs.Condition_Applicable__c='Not Applicable';
        }
        
        else if
            (qs.Sales_country__c=='China' && qs.Sales_Genpact_Region__c=='Emerging Markets')
        {
            qs.Condition_Applicable__c='-Global deals only or if the opportunity provides a view into a global deal \r\n -Okay if extension to an existing global relationship with 15% AOI threshold\r\n -Re-eng deals with a minimum AOI of 15%';
        }
        else  {qs.Condition_Applicable__c='N/A';}
    } */
}