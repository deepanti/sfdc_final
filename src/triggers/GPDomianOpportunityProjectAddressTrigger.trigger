trigger GPDomianOpportunityProjectAddressTrigger on GP_Opp_Project_Address__c (after insert, after update, before insert, before update) {
	
	map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_opp_project_address__c') && 
    								MapofProjectTrigger.get('gp_opp_project_address__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomianOpportunityProjectAddress.class);
    }
    
}