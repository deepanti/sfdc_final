trigger TaskRelatedToOpp on Task (before insert, before update) 
{   
    /*modified by Naresh to include RingDNA requirement to tag campaign name with task
modified date: 30 NOV 2018  */ 
    try{
        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                set<Id>whoIds=new set<Id>();
                set<Id>whocIds=new set<Id>();
                for(Task t : trigger.new)
                {
                    String wId = t.WhoId; 
                    system.debug('wid==='+wid);
                    if(wId!=null && wId.startsWith('00Q') && t.ringdna__Created_by_RingDNA__c==true)
                    { 
                        whoIds.add(wId);
                    } 
                    if(wId!=null && wId.startsWith('003') && t.ringdna__Created_by_RingDNA__c==true)
                    { 
                        whocIds.add(wId);
                    }
                } 
                map<id,lead> RecentCampaignIds;
                map<id,contact> RecentCampaignConId;
                system.debug(':===whoid==:'+whoIds);
                if(whoIds.size()>0){
                    RecentCampaignIds = new map<id,lead>([select id, Recent_Campaign_ID__c from lead where id IN : whoIds]);
                }
                if(whocIds.size()>0){
                    RecentCampaignConId = new map<id,contact>([select id,Recent_Campaign_ID__c from contact where id IN : whocIds]);
                }
                
                for(Task t : trigger.new)
                { 
                    if(RecentCampaignIds != null){
                        t.Campaign__c = RecentCampaignIds.get(t.WhoId).Recent_Campaign_ID__c;
                    }
                    if(RecentCampaignConId != null){
                        t.Campaign__c = RecentCampaignConId.get(t.WhoId).Recent_Campaign_ID__c;
                    }
                }
                
            } 
        }
        
        Set<Id> opIds = new Set<Id>(); 
        List<OpportunityContactRole > oppConRole = new List<OpportunityContactRole>();
        
        String Conid;
        
        for(Task t : trigger.new)
        {
            String wId = t.WhatId; 
            if(wId!=null && wId.startsWith('006') && !opIds.contains(t.WhatId) && t.AutocreatedTask__c==false)
            { 
                opIds.add(t.WhatId);
            } 
        } 
        system.debug(':===opIds===:'+opIds);
        if(opIds.size()>0)
        {
            // Pull in opp ids and related field to populate task record 
          //  List<Opportunity> taskOps = [Select Id, Nextstep,StageName from Opportunity where Id in :opIds AND (StageName = 'Prediscover' OR StageName = 'Pre-discover Dropped')]; 
            Map<Id, Opportunity> opMap = new Map<Id, Opportunity>([Select Id, Nextstep,StageName from Opportunity where Id in :opIds AND (StageName = 'Prediscover' OR StageName = 'Pre-discover Dropped')]); 
            
            oppConRole=[Select id,Contactid,Opportunityid from OpportunityContactRole where OpportunityId in :opIds AND IsPrimary=true];
            
            system.debug(':===oppConRole==:'+oppConRole);
            FOR(OpportunityContactRole ocr : oppConRole)
            {   
                if(opMap.containsKey(ocr.Opportunityid)) {
                    Conid=ocr.Contactid;
                }
            } 
            
            for(Task t : trigger.new)
            { 
                String wId = t.WhatId;
                if(wId!=null && wId.startswith('006'))
                { 
                    Opportunity thisOp = opMap.get(t.WhatId); 
                    system.debug(':===thisOp==:'+thisOp);
                 if(thisOp!=null && t.Status!='Completed' && t.Automated_Task__c==true)
                 {
                     t.description = thisOp.nextstep;
                     t.whoid=Conid;
                 } 
                 else if(t.Status=='Completed' && t.Subject=='Next Step Note – Dropped' && t.Automated_Task__c==true)
                 {    
                     t.description = thisOp.nextstep;
                     t.whoid=Conid;
                     
                 }
                } 
            } 
        }
    }catch(Exception e){
        system.debug('Error Message'+e.getMessage());
        system.debug('Error Line Number'+e.getLineNumber());
    }
}