trigger UpdateSalesLeaderOnQSRM on QSRM__c (before insert,before update,after insert,After update)  
{
    set<string> qwerId = new set<string>();
    if((trigger.isinsert || trigger.isupdate) && (trigger.isbefore)) 
    {  
       // ApprovalQSRMReportController.approvalMethod(trigger.new);
        system.debug('UpdateSalesLeaderOnQSRM trigger before event');
        List<TS_Mapping__c> TSMapping=[Select id,Approver_Name__c,Approver_Type__c,Industry_Vertical__c,Nature_of_Work__c,Service_Line__c from TS_Mapping__c];
        list <QSRM__c> qsrmlist = new List<QSRM__c>(); 
        for(QSRM__c TempObj : trigger.new)
        {   
           
            qwerId.add(TempObj.CreatedById);
            id loginUserId = userInfo.getUserId();
            if(loginUserId == TempObj.CreatedById){
                if(TempObj.Status__c == 'Rework'){
                    TempObj.Status__c = 'In Approval';
                }
            }
            
            if(TempObj.Opportunity_TCV__c < 100000 && TempObj.QSRM_Type__c == 'TSQSRM'){
                if(TempObj.Overall_Score__c >= 18 && TempObj.Deliverability_Score__c >= 10){
                    TempObj.Auto_Approve_QSRM__c = 'Auto Approve';
                }else if(TempObj.Overall_Score__c < 10 ){
                    TempObj.Auto_Approve_QSRM__c = 'Auto Reject';
                }
            }
            String oppvertical = TempObj.Industry_Vertical__c;
            String vlOnQsrm='';
            String l3OnQsrm='';
            String sllOnQsrm= '';
            String exceptionUser = ''; // added
            String vlOnQsrm1='';
            String sllOnQsrm1= '';
            String exceptionUser1 = '';
            String vlOnQsrm2='';
            String sllOnQsrm2= '';
            String exceptionUser2 = '';
            String vlOnQsrm3='';
            String sllOnQsrm3= '';
            String exceptionUser3 = '';  
            String vlOnQsrm4='';
            String sllOnQsrm4= '';
            String exceptionUser4 = ''; 
            
            boolean isSLL = false;
           
            for (TS_Mapping__c ts : TSMapping)
            {
                 if (oppvertical== ts.Industry_Vertical__c && ts.Approver_Type__c=='Vertical Leader')
                    
                {
                    vlOnQsrm = ts.Approver_Name__c;
                    
                }
                
                if (oppvertical== ts.Industry_Vertical__c && ts.Approver_Type__c=='L3 Escalation Leader')
                {
                    l3OnQsrm = ts.Approver_Name__c;
                }
                system.debug('UpdateSalesLeaderOnQSRM====:'+TempObj.Is_this_is_a_RPA_deal__c);
                system.debug('UpdateSalesLeaderOnQSRM====:'+oppvertical);
                system.debug('UpdateSalesLeaderOnQSRM====:'+ts.Industry_Vertical__c);
                system.debug('UpdateSalesLeaderOnQSRM====:'+ts.Approver_Type__c);
                if (TempObj.Is_this_is_a_RPA_deal__c=='Yes' && oppvertical== ts.Industry_Vertical__c && ts.Approver_Type__c=='RPA' )
                { 
                    sllOnQsrm = ts.Approver_Name__c;
                }
                system.debug('UpdateSalesLeaderOnQSRM====:'+ts.Approver_Type__c);
                system.debug('UpdateSalesLeaderOnQSRM====:'+TempObj.Is_this_is_a_RPA_deal__c);
               if (ts.Approver_Type__c=='Service Line Leader' && TempObj.Is_this_is_a_RPA_deal__c == null )
                {
                    String result=QSRMHandler.businessLogic1(TempObj,ts);
                    system.debug('UpdateSalesLeaderOnQSRM===result=:'+result);
                    if(result!=null){
                        sllOnQsrm=result;
                        isSLL = true;
                        
                    }      
                    else if(!isSLL){
                        system.debug('UpdateSalesLeaderOnQSRM===Label.QSRM_SLL=:'+Label.QSRM_SLL);
                        sllOnQsrm = Label.QSRM_SLL;
                    }
                }
                if(TempObj.Highest_Nature_of_Work__c == ts.Nature_of_Work__c && ts.Approver_type__c=='L2 Escalation Leader')
                {
                    exceptionUser = ts.Approver_Name__c;
                }
            }
            system.debug('UpdateSalesLeaderOnQSRM===Label.QSRM_SLL=:'+Label.QSRM_SLL);
            system.debug('UpdateSalesLeaderOnQSRM===Label.QSRM_SLL=:'+sllOnQsrm);
            TempObj.User_sales_leader__c = TempObj.QSRM_approver_Account__c;
            TempObj.Sales_Leaders__c = TempObj.Sales_Leader__c;
            if(TempObj.User_sales_leader__c !=NULL)
                TempObj.Sales_Leaders__c=TempObj.QSRM_approver_Account__c;
            if (vlOnQsrm <> '' )
                TempObj.Vertical_Leaders__c = vlOnQsrm;
            if (sllOnQsrm <> '' )
                TempObj.Service_Line_Leaders__c = sllOnQsrm;
            if(exceptionUser <> '')
                TempObj.QSRM_Rejection_Mail__c = exceptionUser;
            if(l3OnQsrm <> '')
                TempObj.QSRM_Rejection_Mail_L3__c = l3OnQsrm;
        }     
    }
    if(true){
        if(true){
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
        if(true){
            
        }
    }
    
    if((trigger.isinsert || trigger.isupdate) && (trigger.isAfter)) {
       set<string> qsrmStatus = new set<string>();
        for(QSRM__c TempObj : trigger.new)
        {   
            qwerId.add(TempObj.CreatedById);
            qsrmStatus.add(TempObj.status__c);
        }
      Boolean recursiveFlag = RecursiveTsQsrmContoller.ApprovalMethod();
       if(recursiveFlag){
          id loginUserId = userInfo.getUserId();
          if(qwerId.contains(loginUserId) || qsrmStatus.contains('In Approval')){ 
             
              ApprovalQsrmController.approvalMethod(trigger.new);
          } 
        
        
        }
    }
    
}