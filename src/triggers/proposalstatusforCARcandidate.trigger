trigger proposalstatusforCARcandidate on Proposal__c (after update, after insert, after delete) 
{  

    if (Trigger.isDelete){
        /* This will always have only one Id */
        List < Id > empmasterIds = new List < Id >();
        List < Id > proposalIds = new List < Id >();
    
        for ( Proposal__c  c: Trigger.old) 
        {
            proposalIds.add( c.Id );
            empmasterIds.add( c.CAR_Candidate_Code__c);
        }
        
        /* This will contain the aggregated proposal status of the employee */
        List<CAR_Candidate__c> opps = [select id, Proposal_Status__c, Temp_Proposal_Status__c from CAR_Candidate__c where id in :empmasterIds];
    
        /* Mapping between employeeId and employee */
        Map < Id ,CAR_Candidate__c > empmap = new Map < Id , CAR_Candidate__c >();
        for ( CAR_Candidate__c  a : opps   ) 
        {
            empmap.put( a.Id, a);
        }

        /* List of employees to be updated. This will have only one employee - the current employee. */
        List <CAR_Candidate__c> EmpToUpdate = new List <CAR_Candidate__c>();
    
        /* Get the status of the new proposal */
        for(Proposal__c c: Trigger.old) 
        {
           system.debug('Inside Proposal Loop 2');
           /* Fetch the current employee  */
           CAR_Candidate__c ac = empmap.get( c.CAR_Candidate_Code__c );         
           if (ac == null) 
           {    
               continue;
           }
        
           system.debug('----------->'+c.Status__c);
           /* Get all proposals for this employee and mark them as closed.  */
           String futureProposalStatus = 'Unassigned';
           List<Proposal__c> proObj = [select Id,Status__c,CAR_Candidate_Code__c from Proposal__c where Id !=:c.Id and CAR_Candidate_Code__c=:ac.Id ];
           for(Proposal__c pc: proObj )
           {
                if ((pc.Status__c == 'Proposed') 
                        || (pc.Status__c == 'Blocked')
                        || (pc.Status__c == 'Selected-New')
                        || (pc.Status__c == 'Selected-Extended')){
                    futureProposalStatus = pc.Status__c;
                } 
           }//for
           ac.Proposal_Status__c = futureProposalStatus;
           ac.Temp_Proposal_Status__c = futureProposalStatus;
           EmpToUpdate.add(ac);
           system.debug('Inside Proposal Loop 2: '+futureProposalStatus);
         }//For Trigger.old
         upsert EmpToUpdate;       
    } else {
        /* This will always have only one Id */
        List < Id > empmasterIds = new List < Id >();
        List < Id > proposalIds = new List < Id >();

        for ( Proposal__c  c: Trigger.New ) 
        {
            proposalIds.add( c.Id );
            empmasterIds.add( c.CAR_Candidate_Code__c);
        }
    
        /* This will contain the aggregated proposal status of the employee */
        List<CAR_Candidate__c> opps = [select id, Proposal_Status__c,Temp_Proposal_Status__c from CAR_Candidate__c where id in :empmasterIds];
    
        /* Mapping between employeeId and employee */
        Map < Id ,CAR_Candidate__c > empmap = new Map < Id , CAR_Candidate__c >();
        for ( CAR_Candidate__c  a : opps   ) 
        {
            empmap.put( a.Id, a);
        }

        /* List of employees to be updated. This will have only one employee - the current employee. */
        List < CAR_Candidate__c > EmpToUpdate = new List < CAR_Candidate__c >();
    
        /* Get the status of the new proposal  */
        for(Proposal__c c: Trigger.New) 
        {
            /* Fetch the current employee  */
            CAR_Candidate__c ac = empmap.get( c.CAR_Candidate_Code__c);         
            if ( ac == null ) 
            {    
                  continue;
            }
        
            /* If the new proposal's status is not closed, then set the Aggregated proposal status as the status
             of the new Proposal, else mark it as unassigned.  */
        
                      
           if(c.Status__c == 'Closed')
             {
                if (c.Status__c == 'Proposed')
                     {
                       ac.Proposal_Status__c = 'Proposed';    
                       ac.Temp_Proposal_Status__c = 'Proposed';
                       EmpToUpdate.add( ac );
                     }
             }    
             
           else
               {
               ac.Proposal_Status__c = c.Status__c;
               ac.Temp_Proposal_Status__c = c.Status__c;
               EmpToUpdate.add( ac );
               }        

        
            /* If the new Proposal's status is equal to 'Blocked' or 'Selected' then close rest of the proposals. */
            if((c.Status__c == 'Selected-New') || (c.Status__c == 'Blocked') || (c.Status__c == 'Selected-Extended'))
            {
                system.debug('----------->'+c.Status__c);
                /* Get all proposals for this employee and mark them as closed. */
                List<Proposal__c> proObj = [select Id,Status__c,CAR_Candidate_Code__c from Proposal__c where Id !=:c.Id and CAR_Candidate_Code__c=:ac.Id ];
                for(Proposal__c pc: proObj )
                {
                   system.debug('BEFORE STATUS--->'+pc.Status__c);
          
                   if ((pc.Status__c != 'Closed')
                       && (pc.Status__c != 'Blocked')
                       && (pc.Status__c != 'Selected-New')
                       && (pc.Status__c != 'Selected-Extended')){             
                       pc.Status__c = 'Closed';
                       system.debug('AFTER STATUS--->'+pc.Status__c);
                       update pc;
                   }//if
                }//for
             }//if
         }//For Trigger.New
         upsert EmpToUpdate;       
     }//Else 
}//End of Trigger