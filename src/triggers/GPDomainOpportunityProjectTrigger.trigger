trigger GPDomainOpportunityProjectTrigger on GP_Opportunity_Project__c (after insert,after update, before insert, before update) {
	
	 map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_opportunity_project__c')
       && MapofProjectTrigger.get('gp_opportunity_project__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainOpportunityProject.class);
    }
    
}