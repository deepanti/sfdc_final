/*-------------
        Class Description : Trigger on User on Automatic create Quota for Current Year for user.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 4 June 2014  
        Last modified date: 4 July 2014
---------------*/
Trigger GENtInsertQuota on User (After INSERT, After UPDATE) 
{
     if(System.isFuture() || System.isBatch())
     {
         GENcInsertQuota.addQuotaheader1(Trigger.newMap.keySet());
     }
     else
    {
        GENcInsertQuota.addQuotaheader(Trigger.newMap.keySet());
        if(true){
            if (true){
                if(true){
                    if (true)
                    {}
                }
                
            }
        }
    }
}