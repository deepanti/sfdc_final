trigger GPDomainOppLineItemTrigger on OpportunityLineItem (after insert, after update) {
    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('opportunitylineitem')
       && MapofProjectTrigger.get('opportunitylineitem').GP_Enable_Sobject__c) {
        fflib_SObjectDomain.triggerHandler(GPDomainOpportunityProduct.class);
    }
}