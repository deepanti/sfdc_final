trigger UpdateOppSplitOwner on Account (after Update) {
    
    Boolean runFlag = CheckRecursiveForOLI.onAccountTrigger();
    system.debug(':---runFlag---:'+runFlag);
     if(Trigger.isUpdate  && (runFlag || Test.isRunningTest()))
    {
        try{
            // Added By Madhuri - Date:17/09/2018 VIC - Hunting Mining Issue
            VIC_HuntingCreatedDate.updateHuntingCreatedDate(Trigger.newMap,Trigger.oldMap);
            OnchageSuperSl.UpdateSuperSl(Trigger.NewMap,Trigger.oldMap);
        }catch(exception e){
            system.debug(':--errormsg--: '+e.getMessage()+':---error Line---: '+e.getLineNumber());
        }
        
    }
}