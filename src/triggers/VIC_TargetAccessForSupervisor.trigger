trigger VIC_TargetAccessForSupervisor on User(after Update) {

    
    if(Trigger.IsAfter){
        if(Trigger.IsUpdate){
            
            System.debug('\n\n ------------>>> ENTRY 1');
            
            Map<Id,Id> userSuperVisorMap = new Map<Id,Id>();
            Set<Id> oldSupervisorIds = new Set<Id>();
            Set<Id> userWithNoSupervisorIds = new Set<Id>();
    
            for(User u : Trigger.new){
                System.debug('\n\n ------------>>> ENTRY 2');
                if(u.Supervisor__c != null && u.Supervisor__c != Trigger.oldmap.get(u.Id).Supervisor__c){
                    System.debug('\n\n ------------>>> ENTRY 3');
                    userSuperVisorMap.put(u.Id,u.Supervisor__c);
                    oldSupervisorIds.add(Trigger.oldmap.get(u.Id).Supervisor__c);
                }
                if(u.Supervisor__c == null && Trigger.oldmap.get(u.Id).Supervisor__c != null){
                    System.debug('\n\n ------------>>> ENTRY 4');
                    userWithNoSupervisorIds.add(Trigger.oldmap.get(u.Id).Supervisor__c);
                }
            }
            
            System.debug('\n\n ------------>>> userSuperVisorMap '+userSuperVisorMap);
            System.debug('\n\n ------------>>> oldSupervisorIds '+oldSupervisorIds);
            System.debug('\n\n ------------>>> userWithNoSupervisorIds '+userWithNoSupervisorIds);
             
             
            if(userSuperVisorMap != null && userSuperVisorMap.size() > 0){
                VIC_TargetAccessOfUserSupervisor.updateCurrentUserSupervisiorAccessOnTargets(userSuperVisorMap,oldSupervisorIds);
            }
            //Remove Supervisor access if the current user have no Supervisor tagged now
            if(userWithNoSupervisorIds != null && userWithNoSupervisorIds.size() > 0){
                VIC_TargetAccessOfUserSupervisor.deleteAccessForOldSupervisors(userWithNoSupervisorIds);
            }
        }
    
    }
}