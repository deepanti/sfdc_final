//Trigger to make primary contact mandatory at define stage
trigger PrimaryContactRole on Opportunity (before update) {
   set<Id> oppIdSet = new set<Id>();
   set<Id> OpportunityContactRoleIdSet = new set<Id>();
   Id profileId=userinfo.getProfileId();
   Id UID=userinfo.getUserId();
   String profileName=[Select Id,Name from Profile where Id=:profileId Limit 1].Name;
   Boolean Legal_person=[select Id,Legal_Person__c from User where Id=:UID limit 1].Legal_Person__c;
    
    if(profilename!='Genpact Super Admin'&& Legal_person!=True )
    {
    
                for(Opportunity opp:trigger.new){
                    if(opp.id!=null)
                        oppIdSet.add(opp.Id);
                }
                for(OpportunityContactRole ocr:[select Id,OpportunityId from OpportunityContactRole where OpportunityId in:oppIdSet AND IsPrimary=true])
                {
                    OpportunityContactRoleIdSet.add(ocr.OpportunityId);
                }
                for(Opportunity opp:trigger.new){
                    
                        if(opp.StageName=='2. Define')
                        {
                            if(oppIdSet.contains(opp.Id) && !OpportunityContactRoleIdSet.contains(opp.Id))
                            opp.addError('Before moving to Stage 2.Define, entry of primary contact is MANDATORY under section "Contact Roles" '); 
                        }
              
                    
                }
                
    }           
  }