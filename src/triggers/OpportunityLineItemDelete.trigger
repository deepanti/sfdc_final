trigger OpportunityLineItemDelete  on OpportunityLineItem (After Delete) 
{
if(label.OLI_Trigger== 'false'){
    if(Trigger.isDelete)
    {
        list<OLI_Delete__c> oliLst = new List<OLI_Delete__c>();    
        For(OpportunityLineItem Oli : trigger.old){
            OLI_Delete__c objOli = new OLI_Delete__c();
            objOli.Oli_Id__c = Oli.id;
            objOli.Opportunity_ID__c = Oli.OpportunityID;
            objOli.Deleted_Date__c = system.now();
            oliLst.add(objOli);
        }
       Insert oliLst;
   }
   }
}