trigger GPDomainProjectClassificationTrigger on GP_Project_Classification__c (after insert, after update, before insert, before update) 
{  
 	map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_project_classification__c') && 
    								MapofProjectTrigger.get('gp_project_classification__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainProjectClassification.class);
    }	   
}