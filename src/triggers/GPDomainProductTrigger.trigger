trigger GPDomainProductTrigger on Product2 (
    after delete, after insert, after update, after undelete, before delete, before insert, before update)
{
    // Creates Domain class instance and calls appropriate methods
    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('Product2') 
       && MapofProjectTrigger.get('Product2').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainProduct.class);
    }
}