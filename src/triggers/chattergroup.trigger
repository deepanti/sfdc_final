trigger chattergroup on GW1_DSR_Team__c (after insert,after update)
{
    /* Querying DSR Id*/
    Set<id> DSRID1=new Set<id>();
    for(GW1_DSR_Team__c dsrteam:trigger.new)
    {
       DSRID1.add(dsrteam.GW1_DSR__c);
    }
      /* fetching DSR Details*/
    map<id,GW1_DSR__c> mapofDSRobject=new  map<id,GW1_DSR__c>();
    for(GW1_DSR__c DSRobject:[select id,Pseudo_name__c,Pseudo_name1__c,GW1_Opportunity__r.Opportunity_ID__c from GW1_DSR__c where id=:DSRID1])
    {
      mapofDSRobject.put(DSRobject.id, DSRobject)  ;
    }
 list<GW1_DSR_Group__c> chattergroupinsert=new list<GW1_DSR_Group__c>();
    list<GW1_DSR_Group__c> finalchattergroupinsert=new list<GW1_DSR_Group__c>();
    set<GW1_DSR_Group__c> setchattergroupinsert=new set<GW1_DSR_Group__c>();
 map<string,CollaborationGroup>  chattergroupdetails=new map<string,CollaborationGroup>();
    set<string> groupid=new set<string>();
    /* fetching chatter group object details*/
    for(CollaborationGroup chatterdetails:[select id ,name from CollaborationGroup where name Like '%Think Tank%'])
    {
        chattergroupdetails.put(chatterdetails.name, chatterdetails);
        groupid.add(chatterdetails.id);
    }
    /* fetching DSR group object details*/
    map<id,GW1_DSR_Group__c>  chattergroupdetailss=new map<id,GW1_DSR_Group__c>();
    for(GW1_DSR_Group__c chattergroup:[select id,GW1_Chatter_Group_Id__c from GW1_DSR_Group__c  where GW1_Chatter_Group_Id__c=:groupid])
    {
        chattergroupdetailss.put(chattergroup.GW1_Chatter_Group_Id__c,chattergroup);
    }
    
    for(GW1_DSR_Team__c dsrteamobj:trigger.new)
        
    {
        if(mapofDSRobject.containsKey(dsrteamobj.GW1_DSR__c))
        {
        string s=mapofDSRobject.get(dsrteamobj.GW1_DSR__c).Pseudo_name1__c;
        // +' '+mapofDSRobject.get(dsrteamobj.GW1_DSR__c).GW1_Opportunity__r.Opportunity_ID__c;
        if(chattergroupdetails.containsKey(s+' '+'Think Tank') && !chattergroupdetailss.containskey(chattergroupdetails.get(s+' '+'Think Tank').id) )
        {
            
            /* creating group object */
            GW1_DSR_Group__c chattergroupobjectt=new GW1_DSR_Group__c();
            chattergroupobjectt.GW1_Chatter_Group_Id__c=chattergroupdetails.get(s+' '+'Think Tank').id;
            chattergroupobjectt.name=s+' '+'Think Tank';
            chattergroupobjectt.GW1_DSR__c=dsrteamobj.GW1_DSR__c;
            chattergroupinsert.add(chattergroupobjectt);
            
       }
        
        /*if(chattergroupdetails.containsKey(s.left(15)+' '+'Pricing group') && !chattergroupdetailss.containskey(chattergroupdetails.get(s.left(15)+' '+'Pricing group').id))
        {
            GW1_DSR_Group__c chattergroupobjecttp=new GW1_DSR_Group__c();
            chattergroupobjecttp.GW1_Chatter_Group_Id__c=chattergroupdetails.get(s.left(15)+' '+'Pricing group').id;
            chattergroupobjecttp.name='Pricing group';
            chattergroupobjecttp.GW1_DSR__c=dsrteamobj.GW1_DSR__c;
            chattergroupinsert.add(chattergroupobjecttp);
            
       }*/
    }
}
    
    
        
    
        if(chattergroupinsert!=NULL && chattergroupinsert.size()>0)
    {
       
       // insert chattergroupinsert[0];
       
       setchattergroupinsert.addall(chattergroupinsert);
        finalchattergroupinsert.addAll(setchattergroupinsert);
        If(finalchattergroupinsert.size()>0 && finalchattergroupinsert !=NULL)
        {
            /* inserting DSR group object*/
            insert finalchattergroupinsert;
        }
            
        
       
        
        
   }
    
}