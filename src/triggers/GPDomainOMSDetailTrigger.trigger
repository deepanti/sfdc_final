trigger GPDomainOMSDetailTrigger on GP_OMS_Detail__c (after insert, after update, before insert, before update) {
 
 
 map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('gp_oms_detail__c') && 
    								MapofProjectTrigger.get('gp_oms_detail__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainOMSDetail.class);
    }
       
}