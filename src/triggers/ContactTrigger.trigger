trigger ContactTrigger on Contact (after insert, after update) {
    
    if(CheckRecursiveForContact.runOnce()){
        List<Profile> profileList = [Select Id from Profile where Name=: 'Genpact Partner Community User' limit 1];
        List<User> toBeInsertedUserList = new List<User>();
        List<User> toBeUpdatedUserList = new List<User>();
        User loggedInUser = [SELECT ID, TimeZoneSidKey, LanguageLocaleKey, EmailEncodingKey, LocaleSidKey FROM User 
                             WHERE ID =: UserInfo.getUserId()];
        List<String> emailList = new List<String>();
        List<User> existingUserList;
        
        for(Contact contactObj : Trigger.New){
            emailList.add(contactObj.email);
        }    
        if(emailList.size()>0)
            existingUserList = [SELECT ID, email FROM User WHERE Email =: emailList];
        
        for(Contact contactObj : Trigger.New){
            Boolean isExist = false;
            if(contactObj.Create_Partner_User__c){
                if(existingUserList.size() > 0){
                    for(User u : existingUserList){
                        if(u.email == contactObj.email){
                            u.IsActive = true;
                            toBeUpdatedUserList.add(u);
                            isExist = true;
                        }	
                    }    
                }
                if(!isExist){
                    String firstName;
                    User uObj = new User();  
                    System.debug('contactObj.FirstName=='+contactObj.FirstName);
                    if(contactObj.FirstName == null){
                        firstName= '';
                    }
                    else{
                        firstName = contactObj.FirstName;
                    }
                    String alias = firstName+ContactObj.LastName;
                    if(alias.length()>8){
                        alias = alias.substring(0,6);
                    }                
                    uObj.CommunityNickname = firstName+'.'+contactObj.LastName+'.'+system.now();
                    uObj.Username = contactObj.Email+'.partner';
                    uObj.LastName = contactObj.LastName;
                    uObj.Email = contactObj.Email;
                    uObj.ProfileId = profileList[0].Id;
                    uObj.Alias = alias;
                    uObj.IsActive = true; 
                    uObj.firstName = firstName;
                    uObj.TimeZoneSidKey = loggedInUser.TimeZoneSidKey;
                    uObj.LanguageLocaleKey = loggedInUser.LanguageLocaleKey;
                    uObj.EmailEncodingKey = loggedInUser.EmailEncodingKey;
                    uObj.LocaleSidKey = loggedInUser.LocaleSidKey;
                    uObj.ContactId = contactObj.Id;
                    toBeInsertedUserList.add(uObj);    
                    system.debug('uObj=='+uObj);
                }
            }
        }
        system.debug('toBeInsertedUserList=='+toBeInsertedUserList);
        if(toBeInsertedUserList.size()>0){
            insert toBeInsertedUserList;    
        }
        if(toBeUpdatedUserList.size() > 0){
            update toBeUpdatedUserList;    
        }
        
    }
    
    
    
}