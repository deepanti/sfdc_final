trigger GenTUpdateAccounTeam on Account (after insert,after update) {
/*
    Set<String> sAcc = new Set<String>();
    List<AccountTeamMember> AccList {get;set;}
    List<AccountTeamMember> AccListUpdated = new List<AccountTeamMember>();    
    List<AccountTeamMember> AccListdeleted = new List<AccountTeamMember>();       
    List<AccountShare> AccShare= new List<AccountShare>();    
    List<AccountShare> AccSharedeleted = new List<AccountShare>();     
       
    AccList = new List<AccountTeamMember>([SELECT AccountAccessLevel,AccountId,Id,TeamMemberRole,UserId FROM AccountTeamMember where AccountId =: trigger.newmap.keyset()]);       
    AccountShare[] newShare = new AccountShare[]{};             
    newShare = new list<AccountShare>([SELECT AccountAccessLevel,AccountId,CaseAccessLevel,ContactAccessLevel,Id,IsDeleted,LastModifiedById,LastModifiedDate,OpportunityAccessLevel,RowCause,UserOrGroupId FROM AccountShare where AccountId =: trigger.newmap.keyset()]);       
    Map<string,AccountTeamMember> MapATM = new Map<string,AccountTeamMember>();              
    Map<string,AccountShare> MapAS = new Map<string,AccountShare>();            
    for(AccountTeamMember at : AccList)         
    {      system.debug('aaaaaaaa'+string.valueof(at.AccountId)+string.valueof(at.UserId));                           
        MapATM.put(string.valueof(at.AccountId)+string.valueof(at.UserId),at);  
    }             
    for(AccountShare ash : newShare)         
    {      system.debug('aaaaaaaa'+string.valueof(ash.AccountId)+string.valueof(ash.UserOrGroupId));                           
        MapAS.put(string.valueof(ash.AccountId)+string.valueof(ash.UserOrGroupId),ash);  
    }                
    for(Account a :trigger.new)
    {
        Account sAccOld =new Account();
        if(trigger.isUpdate && a.id <> null && trigger.oldmap.get(a.id)<>null)
        sAccOld = trigger.oldmap.get(a.id);    
        system.debug('Acclist------>'+AccList);   
        AccountTeamMember ATM ; 
        AccountTeamMember ATMnew ;  
        AccountShare Acs ;
        AccountShare Asnew ;              
        if( a.Client_Partner__c <> null && a.Client_Partner__c <> a.ownerid) 
        {        
            ATM = new AccountTeamMember();      
            ATM.AccountId= a.id;
            ATM.UserId= a.Client_Partner__c;
            ATM.TeamMemberRole = 'Client Partner';     
            AccListUpdated.add(ATM);         
            system.debug('AccListUpdated'+AccListUpdated);        
              
            Acs = new AccountShare();      
            Acs.AccountId=a.id;
            Acs.UserOrGroupId=a.Client_Partner__c;
            Acs.AccountAccessLevel='Edit';   
            Acs.OpportunityAccessLevel='Edit';              
            AccShare.add(Acs);         
            system.debug('ttttttt'+AccShare);
                                                    
        }                          
        if(sAccOld.Client_Partner__c!= a.Client_Partner__c && a.Client_Partner__c <> null ) 
        {         
         
            ATMnew = new AccountTeamMember();   
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c));                                
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c))<>null)       
            {          
            ATMnew.Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c)).Id;                                         
            AccListdeleted.add(ATMnew);     
            system.debug('AccListdeleted'+AccListdeleted);      
            }             
                                                    
            Asnew = new AccountShare();   
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c));                                
            //if( MapAS.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c))<>null)       
           // {          
          //  Asnew.Id= MapAS.get(string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c)).Id;                                         
          //  AccSharedeleted .add(Asnew);     
         //   system.debug('sssssss'+AccSharedeleted );      
          //  }                       
        }    
        if(a.Primary_Account_GRM__c <> null && a.Primary_Account_GRM__c<>a.ownerid) 
         {      
            ATM = new AccountTeamMember();  
            ATM.AccountId= a.id;
            ATM.UserId=a.Primary_Account_GRM__c;
            ATM.TeamMemberRole = 'GRM';   
            AccListUpdated.add(ATM);                    
  
  
                 Acs = new AccountShare();      
            Acs.AccountId= a.id;
            Acs.UserOrGroupId= a.Primary_Account_GRM__c;
            Acs.AccountAccessLevel='Edit';   
            Acs.OpportunityAccessLevel='Edit'; 
                      
            AccShare.add(Acs);         
            system.debug('nnnnn'+AccShare);
                                              
         }                  
        if(sAccOld.Primary_Account_GRM__c!= a.Primary_Account_GRM__c && a.Primary_Account_GRM__c <> null )       
        {
            ATMnew = new AccountTeamMember();   
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Client_Partner__c));                                
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c))<>null)       
            {          
            ATMnew.Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c)).Id;                                         
            AccListdeleted.add(ATMnew);     
            system.debug('AccListdeleted'+AccListdeleted);  
            }                   
                                               
            Asnew = new AccountShare();   
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c));                                
           // if( MapAS.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c))<>null)       
           // {          
           // Asnew.Id= MapAS.get(string.valueof(a.id)+string.valueof(sAccOld.Primary_Account_GRM__c)).Id;                                         
          //  AccSharedeleted.add(Asnew);     
           // system.debug('uuuuuuuuuuuu'+AccSharedeleted);  
           // }                     
                       
         }            
        if(a.OwnerId <> null) 
         {      
            ATM = new AccountTeamMember();  
            ATM.AccountId= a.id;
            ATM.UserId=a.OwnerId;
            ATM.TeamMemberRole = 'BD Rep';   
            AccListUpdated.add(ATM);                
  
          //  Acs = new AccountShare();      
           // Acs.AccountId= a.id;
           // Acs.UserOrGroupId=a.OwnerId;
            //Acs.AccountAccessLevel= 'Edit';   
           // Acs.OpportunityAccessLevel= 'Edit'; 
                      
          //  AccShare.add(Acs);         
           // system.debug('AccListUpdated'+AccListUpdated);
      //
                                          
         }                      
        if(sAccOld.OwnerId!= a.OwnerId && a.OwnerId <> null )       
        {
                                                   
            ATMnew = new AccountTeamMember();   
            system.debug('vvvvvvv'+string.valueof(a.id)+string.valueof(sAccOld.OwnerId));                                
            if( MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.OwnerId))<>null)       
            {          
            ATMnew.Id= MapATM.get(string.valueof(a.id)+string.valueof(sAccOld.OwnerId)).Id;                                         
            AccListdeleted.add(ATMnew);     
            system.debug('AccListdeleted'+AccListdeleted);      
            }         
        }                      
     }
     Database.SaveResult[] lsr = Database.insert(AccListUpdated,false);//insert any valid members then add their share entry if they were successfully added
     if(AccListdeleted.size()>0)            
     delete AccListdeleted;            
     If(AccShare.size()>0)                            
     insert AccShare;         
     if(AccSharedeleted.size()>0)                
     delete AccSharedeleted;                                     
     Integer newcnt=0;
     for(Database.SaveResult sr:lsr)
     {
         if(!sr.isSuccess())
         {
            Database.Error emsg =sr.getErrors()[0];
            system.debug('\n\nERROR ADDING TEAM MEMBER:'+emsg);
         }
         else
         {
            newShare.add(new AccountShare(UserOrGroupId=AccListUpdated[newcnt].UserId, AccountId=AccListUpdated[newcnt].AccountId, AccountAccessLevel='Edit',OpportunityAccessLevel='​Edit'));
         }
         newcnt++;           
     }
        */
     }