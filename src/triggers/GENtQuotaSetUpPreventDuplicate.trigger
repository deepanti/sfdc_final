/*-------------
        Class Description : Trigger on Quota to check combination of user and account name to be unique.
        Created by   : Arjun Srivastava
        Created Date : 14 May 2014 
        Location     : Genpact        
        Last modified date: 15 August 2014
    ---------------*/
Trigger GENtQuotaSetUpPreventDuplicate on Set_Quotas__c (before insert,before update) 
{
  if(!singleexecuion.bool_GENtQuotaSetUpPreventDuplicate)
  {  
    singleexecuion.bool_GENtQuotaSetUpPreventDuplicate=true;
      
    //code to check no 2 records are same in Quota line for BD rep\GRM
    Map<String, Set_Quotas__c> SetquotasMapCode = new Map<String, Set_Quotas__c>();
    
    Map<String, Set_Quotas__c> SetquotasMapCode_UI = new Map<String, Set_Quotas__c>();  //used for user+unidentified checkbox
    
    Map<String, Set_Quotas__c> SetquotasMapCode_SL = new Map<String, Set_Quotas__c>();  //used for user+SL recordtype+year
    
    Map<String,String> quota_yearmap=new Map<String,String>();
    
    
    String Quota_year=string.valueof(System.today().year());
    
    String recordtypeid=[select id from recordtype where developername='Account_GRM' AND SobjectType='Set_Quotas__c'].id ;  
    for (Set_Quotas__c SQ : [SELECT Id,Quota_header__c,Account_Name__c FROM Set_Quotas__c where recordtype.developername='Account_GRM'  AND Quota_header__r.Year__c=:Quota_year]) 
    { 
        SetquotasMapCode.put(SQ.Quota_Header__c+''+SQ.Account_Name__c, SQ);  
    }
    
    for (Set_Quotas__c SQ_UI : [SELECT Id,Quota_header__c,Identified_UnIdentified__c FROM Set_Quotas__c where recordtype.developername='Account_GRM'  AND Quota_header__r.Year__c=:Quota_year AND Identified_UnIdentified__c=true AND Account_Name__c=null]) 
    { 
        SetquotasMapCode_UI.put(SQ_UI.Quota_Header__c+''+String.valueof(SQ_UI.Identified_UnIdentified__c), SQ_UI);  
    }
    
    for (Set_Quotas__c SQ_SL : [SELECT Id,Quota_header__c,Quota_header__r.Year__c,recordtypeid FROM Set_Quotas__c where recordtype.developername='Opportunity_Line_Owner'  AND Quota_header__r.Year__c=:Quota_year]) 
    { 
        SetquotasMapCode_SL.put(SQ_SL.Quota_Header__c+''+SQ_SL.recordtypeid+''+SQ_SL.Quota_header__r.Year__c,SQ_SL);  
    }
    
    for(Quota_header__c QH_yr:[select id,year__c from Quota_header__c where year__c=:Quota_year])
    {
        quota_yearmap.put(QH_yr.id,QH_yr.year__c);
    }
    
    if(Trigger.isInsert)
    for (Set_Quotas__c oSQCheck : Trigger.new) 
    {       
        if(recordtypeid == oSQCheck.recordtypeid)       //validation only for Account_GRM recordtype
        {
            if(oSQCheck.Account_Name__c!=null)
            {
                system.debug('recordtype=isInsert====='+oSQCheck.recordtypeid+'   '+oSQCheck.recordtype.name);
                if(SetquotasMapCode.containsKey(oSQCheck.Quota_header__c+''+oSQCheck.Account_Name__c))
                    oSQCheck.addError('Same account “Quota CANNOT be set for the same "ACCOUNT" twice');
                else 
                    SetquotasMapCode.put(oSQCheck.Quota_Header__c+''+oSQCheck.Account_Name__c, oSQCheck);
            }
            else
            {
                if(SetquotasMapCode_UI.containsKey(oSQCheck.Quota_header__c+''+String.valueof(oSQCheck.Identified_UnIdentified__c)))
                    oSQCheck.addError('Multiple Unidentified Quota CANNOT be set for the "UNIDENTIFIED" category twice');
                else 
                    SetquotasMapCode_UI.put(oSQCheck.Quota_Header__c+''+String.valueof(oSQCheck.Identified_UnIdentified__c), oSQCheck);
            }   
        }
        else                //validation only for Opportunity_Line_Owner recordtype
        {
                if(SetquotasMapCode_SL.containsKey(oSQCheck.Quota_header__c+''+oSQCheck.recordtypeid+''+quota_yearmap.get(oSQCheck.Quota_header__c)))
                oSQCheck.addError('Multiple Sales Leader Quota\'s "Multiple Quota ITEMS for any Sales Leader / Client Partner CANNOT be set"');
            else 
                SetquotasMapCode_SL.put(oSQCheck.Quota_header__c+''+oSQCheck.recordtypeid+''+quota_yearmap.get(oSQCheck.Quota_header__c), oSQCheck);
        }
    }
    if(Trigger.isUpdate)
    for (Set_Quotas__c oSQCheck : Trigger.new) 
    {   
        Set_Quotas__c oSQCheckold=Trigger.oldMap.get(oSQCheck.Id);
        if(recordtypeid == oSQCheck.recordtypeid)       //validation only for this recordtype
        {            
            if(oSQCheckold.Account_Name__c!=null && oSQCheck.Account_Name__c!=null)
            {
                if(oSQCheck.Account_Name__c==oSQCheckold.Account_Name__c)
                {} 
                else
                {
                    system.debug('recordtype==isUpdate===='+oSQCheck.Account_Name__c+'   '+oSQCheckold.Account_Name__c);
                    if(SetquotasMapCode.containsKey(oSQCheck.Quota_header__c+''+oSQCheck.Account_Name__c))
                        oSQCheck.addError('Same account “Quota CANNOT be set for the same "ACCOUNT" twice');
                    else 
                        SetquotasMapCode.put(oSQCheck.Quota_Header__c+''+oSQCheck.Account_Name__c, oSQCheck); 
                }
            }      
            if(oSQCheck.Account_Name__c == null)
            {   
                if(oSQCheckold.Identified_UnIdentified__c == oSQCheck.Identified_UnIdentified__c)
                {}
                else
                {
                    if(SetquotasMapCode_UI.containsKey(oSQCheck.Quota_header__c+''+String.valueof(oSQCheck.Identified_UnIdentified__c)))
                        oSQCheck.addError('Multiple Unidentified Quota CANNOT be set for the "UNIDENTIFIED" category twice');
                    else 
                        SetquotasMapCode_UI.put(oSQCheck.Quota_Header__c+''+String.valueof(oSQCheck.Identified_UnIdentified__c), oSQCheck);
                }
            }
        }
        else            //validation only for Opportunity_Line_Owner recordtype
        {
            if(oSQCheckold.recordtypeid == oSQCheck.recordtypeid)  {}
            else
            {
                if(SetquotasMapCode_SL.containsKey(oSQCheck.Quota_header__c+''+oSQCheck.recordtypeid+''+quota_yearmap.get(oSQCheck.Quota_header__c)))
                    oSQCheck.addError('Multiple Sales Leader Quota\'s "Multiple Quota ITEMS for any Sales Leader / Client Partner CANNOT be set"');
                else 
                    SetquotasMapCode_SL.put(oSQCheck.Quota_header__c+''+oSQCheck.recordtypeid+''+quota_yearmap.get(oSQCheck.Quota_header__c), oSQCheck);
            }
        }
    }
   } 
}