/* Trigger updating the Revenue start date on the OLIs when the Revenue start date on the Opportunity is updated*/
/*Created Date: Deployment Date: July 17 2015, Created By : Chiranjeevi Kunamalla*/

trigger dateupdate on Opportunity (after update) {
    
    if(system.Label.DisableDateupdate =='true'){  // to deactivate the trigger
        if(CheckRecursiveForOLI.dateUpdateFlag()){
            System.debug(':---Custome Label Test--:');
            
            boolean runME = true;
            HelpClass.Rec=false;
            System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
            list<OpportunityLineItem> OpportunityProductupdate=new list<OpportunityLineItem>();
            list<OpportunityLineItem> OpportunityProducts=new list<OpportunityLineItem>();
            
            Set<Id> idset= Trigger.newMap.keySet();
            map<id,opportunity> oppMap = new map<id,opportunity>();
            Integer dueDate;
            if(trigger.isupdate && trigger.isafter ){//&& CheckRecursiveForOLI.runOnceAfter_1()) {   
                try{
                    for(opportunity opportunityobj:trigger.new) {
                        oppMap.put(opportunityobj.id,opportunityobj); 
                        /*   opportunity opptyofoldmap=trigger.oldmap.get(opportunityobj.id);
if(opptyofoldmap.closeDate!=null&opptyofoldmap.closeDate!=opportunityobj.closeDate)  {      
dueDate=opptyofoldmap.closeDate.daysBetween(opportunityobj.closeDate); 
if(dueDate!=0 && dueDate > 0 ) {
oppMap.put(opportunityobj.id,opportunityobj); 
}
}  */
                    }
                    if(oppMap.size() > 0){
                        list<OpportunityLineItem> newopp=[select id,Revenue_Start_Date__c,Opportunityid, UnitPrice, Revenue_Exchange_Rate__c, 
                                                          TCV__c, contract_term__c, CYR__c, NYR__c from OpportunityLineItem where Opportunityid=:oppMap.keySet()];// AND Revenue_Start_Date__c < :opportunityobj.closeDate];
                        
                        for(OpportunityLineItem OpportunityProductobject : newopp) {
                            if(OpportunityProductobject.Revenue_Start_Date__c < oppMap.get(OpportunityProductobject.OpportunityId).closeDate)
                                
                                OpportunityProductobject.Revenue_Start_Date__c=oppMap.get(OpportunityProductobject.OpportunityId).closeDate +1;
                            
                            //OpportunityProductobject.Revenue_Start_Date__c=OpportunityProductobject.Revenue_Start_Date__c+dueDate;
                            OpportunityProductupdate.add(OpportunityProductobject);
                        } 
                    }
                }
                catch(exception e)
                {
                    system.debug('ERROR=='+e.getLineNumber()+' == '+e.getMessage()+'=='+e.getStackTraceString());
                }
                
                
                if(OpportunityProductupdate.size()>0)
                {
                    try
                    {
                        //   Delete [Select ID FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId =:OpportunityProductupdate];
                        for(OpportunityLineItem oppLineItem : OpportunityProductupdate){
                            if(oppLineItem.Revenue_Exchange_Rate__c != null){
                                oppLineItem.UnitPrice = (oppLineItem.TCV__c / oppLineItem.Revenue_Exchange_Rate__c).setScale(2, RoundingMode.HALF_UP);          
                            }
                            else{
                                oppLineItem.UnitPrice = (oppLineItem.TCV__c).setScale(2, RoundingMode.HALF_UP);            
                            }
                            OpportunityProducts.add(oppLineItem);
                            
                        }  
                        System.debug('OpportunityProducts=='+OpportunityProducts);
                        update OpportunityProducts;
                        //   AddproductController.createRevenueSchedule(OpportunityProducts);
                    }
                    catch(exception e)
                    {
                        system.debug('error has occured due to=='+e.getLineNumber()+' == '+e.getMessage()+'=='+e.getStackTraceString());
                    }
                }       
            }
        }    
    }
}