trigger GenTPopulateCurrencyfields on OpportunityProduct__c (before insert, before Update) {


/*
    Bussiness want to see my opportnity and my team Opportunity saparatly from the standard report drop down
    As we cannot make changes in standrad filter provided by salesforce 
    Now as we add OLI BD Rep in Opportunity Team Member repoet will show the that BD Rep as Team will it not do when we are using opportunity share
    and bussiness also want ot use Opportunity team members.
*/
List<OpportunityShare> existingOpptyAccess = new List<OpportunityShare>();

List<OpportunityShare> lineOwnerOpptyAccess = new List<OpportunityShare>();

Map<string,OpportunityShare> shareMap = new Map<string,OpportunityShare>();

/*
List<OpportunityTeamMember> OpportunityTeamMemberLstToInsert = new List<OpportunityTeamMember>();
List<OpportunityTeamMember> existingOpportunityTeamMember = new List<OpportunityTeamMember>();
Set<id> AlradyATeamMember = new Set<id>();
*/
Set<Id> OpportunityIds =new Set<id>();
set<id> ActiveUserLst = new set<Id>();
Map<String, DatedConversionRate> exchangeRateMap = new Map<String, DatedConversionRate>();

for(DatedConversionRate exRate: [SELECT ConversionRate,Id,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE NextStartDate >= today])
{    
        if(exRate.IsoCode <> null)
         {      
          exchangeRateMap.put(exRate.IsoCode, exRate);        
         }
}

for(OpportunityProduct__c oOppProd: trigger.new)
{
    OpportunityIds.add(oOppProd.Opportunityid__c);
    ActiveUserLst.add(oOppProd.SalesExecutive__c);
    
}
map<id,user> ActiveUserMap = new Map<Id,user>([select id,name from user where id in :ActiveUserLst and IsActive = true]);

// as disccussed with amitabh i place of adding BD Rep in Opportunity share now we need to add the BD Rep in Opportunity team member

existingOpptyAccess=[Select id,Opportunityid,UserOrGroupId from OpportunityShare where Opportunityid in:OpportunityIds];

//existingOpportunityTeamMember=[Select id,Opportunityid,UserId from OpportunityTeamMember where Opportunityid in:OpportunityIds];


for(OpportunityShare oOS: existingOpptyAccess)
{
    shareMap.put(string.valueof(oOS.Opportunityid) + string.valueof(oOS.UserOrGroupId),oOS);
}
/*
for(OpportunityTeamMember TempOppTeamMemberObj : existingOpportunityTeamMember)
{
    AlradyATeamMember.add(TempOppTeamMemberObj.UserId); 
}
*/

List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
String MyProflieName = PROFILE[0].Name;

for(OpportunityProduct__c oOppProd: trigger.new)
{
    oOppProd.Ownerid=oOppProd.SalesExecutive__c;
    oOppProd.TCV_to_Roll_Up__c=oOppProd.Total_TCV__c;
    oOppProd.ACV_to_Rollup__c=oOppProd.Total_ACV__c;
    oOppProd.CYR_to_Rollup__c=oOppProd.Total_CYR__c;
    oOppProd.NYR_to_Rollup__c=oOppProd.Total_NYR__c;

if((MyProflieName !='Custom System Administrator')&&(MyProflieName !='Genpact Shared Services')){
// this code is added to fill the TotalContractValue__c when data in insert from dataloader or any other mean other wise it was handeled in nw and edit class alrady
//Start

    if(oOppProd.LocalCurrency__c <> null && oOppProd.ExchangeRate__c == null)
    {
        if(exchangeRateMap.containskey(oOppProd.LocalCurrency__c))
            oOppProd.ExchangeRate__c=exchangeRateMap.get(oOppProd.LocalCurrency__c).ConversionRate;
        else
            oOppProd.adderror('This Currency Is not enable in salesforce');
    } 
    else if(oOppProd.LocalCurrency__c == null && oOppProd.ExchangeRate__c == null)
    {
         oOppProd.adderror('Please enter Local Currency Or Exchange Rate, Local Currency and Exchange Rate both cannot be null');
    }
if(oOppProd.ExchangeRate__c <> null && oOppProd.TCVLocal__c <> null)
         {
    oOppProd.TotalContractValue__c = oOppProd.TCVLocal__c / oOppProd.ExchangeRate__c;
}}
//End
        
    
        OpportunityShare olineOwnerOpptyAccess = new OpportunityShare();
        if(shareMap.get(string.valueof(oOppProd.OpportunityId__c) + string.valueof(oOppProd.SalesExecutive__c))==null)
        {
            if(oOppProd.SalesExecutive__c != null && ActiveUserMap.containskey(oOppProd.SalesExecutive__c))
            {
                olineOwnerOpptyAccess.OpportunityId=oOppProd.OpportunityId__c;
                olineOwnerOpptyAccess.OpportunityAccessLevel='Read';
                olineOwnerOpptyAccess.UserOrGroupId=oOppProd.SalesExecutive__c;
                lineOwnerOpptyAccess.add(olineOwnerOpptyAccess);
            }
        }
   /*
    if(!(AlradyATeamMember.contains(oOppProd.SalesExecutive__c)))
    {
        OpportunityTeamMember OppTeamMemberObj = new OpportunityTeamMember();
        if(oOppProd.SalesExecutive__c != null && ActiveUserMap.containskey(oOppProd.SalesExecutive__c))
        {
            OppTeamMemberObj.UserId = oOppProd.SalesExecutive__c;
            OppTeamMemberObj.TeamMemberRole = 'BD Rep';
            OppTeamMemberObj.OpportunityId =  oOppProd.OpportunityId__c;
          //  OppTeamMemberObj.OpportunityAccessLevel = 'Read Only';
            OpportunityTeamMemberLstToInsert.add(OppTeamMemberObj);
        }  
    } */

}

    if(lineOwnerOpptyAccess <> null)
    {
        system.debug('lineOwnerOpptyAccess'+ lineOwnerOpptyAccess);   
        insert lineOwnerOpptyAccess;
    }
   /*
    if(OpportunityTeamMemberLstToInsert.size()>0)
    {
        system.debug('OpportunityTeamMemberLstToInsert=='+ OpportunityTeamMemberLstToInsert);   
        insert OpportunityTeamMemberLstToInsert;
    } */
}