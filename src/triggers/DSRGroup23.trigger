trigger DSRGroup23 on GW1_DSR_Team__c (After insert,After update) 
{
    /* Querying DSR Id*/
    Set<id> DSRID1=new Set<id>();
    for(GW1_DSR_Team__c dsrteam:trigger.new)
    {
       DSRID1.add(dsrteam.GW1_DSR__c) ;
    }
     /* fetching DSR Details*/
    map<id,GW1_DSR__c> mapofDSRobject=new  map<id,GW1_DSR__c>();
    
    for(GW1_DSR__c DSRobject:[select id,Pseudo_name__c,Pseudo_name1__c,GW1_Opportunity__r.Opportunity_ID__c,Create_chatter_Group__c,GW1_Bid_Manager__r.GW1_Bid_Manager__r.id  from GW1_DSR__c where id=:DSRID1])
    {
      mapofDSRobject.put(DSRobject.id, DSRobject)  ;
    }
     /* Querying  for DSR user*/
     list<GW1_DSR_Group__c> chattergroupinsert=new list<GW1_DSR_Group__c>();
    map<id,user> listofDSRUSER=new map<id,user>();
    
    for(user userobject : [select id ,name,GW1_Role__c from user where GW1_Role__c <> ''])
    {
        listofDSRUSER.put(userobject.id, userobject);
    }
  map<string,CollaborationGroup> mapchattergroup=new map<string,CollaborationGroup>();
  
  map<string,CollaborationGroup> mapchattergroup2=new map<string,CollaborationGroup>();
  set<id> DSRid=new set<id>();
  list<CollaborationGroup>lstChatterGroupToInsert=new list<CollaborationGroup>();
  list<CollaborationGroup>lstChatterGroup2ToInsert=new list<CollaborationGroup>();
  /* Fetching chatter group details*/

for(CollaborationGroup chattergroupobject:[select id,name from CollaborationGroup where name Like '%Think Tank%'])
{
    mapchattergroup.put(chattergroupobject.name,chattergroupobject);
}
/*for(CollaborationGroup chattergroupobject:[select id,name from CollaborationGroup ])
{
    mapchattergroup2.put(chattergroupobject.name,chattergroupobject);
}*/

for(GW1_DSR_Team__c DSRteamobject:trigger.new)
{
    /* checking whether DSR Exsist*/
    if(mapofDSRobject.containsKey(DSRteamobject.GW1_DSR__c))
    {
        
    string s=mapofDSRobject.get(DSRteamobject.GW1_DSR__c).Pseudo_name1__c;
    //+' '+mapofDSRobject.get(DSRteamobject.GW1_DSR__c).GW1_Opportunity__r.Opportunity_ID__c;
    string s1=mapofDSRobject.get(DSRteamobject.GW1_DSR__c).GW1_Bid_Manager__c;
        boolean dsrcreatedcheckbox=mapofDSRobject.get(DSRteamobject.GW1_DSR__c).Create_chatter_Group__c;
   /* checking for chatter group exist and bid manager is not empty*/ 
//if(!(mapchattergroup.containskey(s+' '+'Think Tank')) &&  (mapofDSRobject.get(DSRteamobject.GW1_DSR__c).Create_chatter_Group__c)==True)
if(!(mapchattergroup.containskey(s+' '+'Think Tank')))
    
    {
        system.debug('dsr check box after if condition'+dsrcreatedcheckbox);
        /*  chacking DSR Team Role is BD,Lead Solutions Architect and Deal Manager*/
         if((DSRteamobject.GW1_Role__c=='BD'|| DSRteamobject.GW1_Role__c=='Lead Solutions Architect' || DSRteamobject.GW1_Role__c=='Deal Manager'|| DSRteamobject.GW1_Role__c=='Sales Leader' || DSRteamobject.GW1_Role__c=='Tower Lead' || DSRteamobject.GW1_Role__c=='Pricing' || DSRteamobject.GW1_Role__c=='Commercial Proposal Leader'|| DSRteamobject.GW1_Role__c=='Commercial Proposal Governance Leader'|| DSRteamobject.GW1_Role__c=='Commercial Proposal Governance team member')  )
      {
          /* checking DSR chatter group is checked*/
          system.debug('dsr check box'+dsrcreatedcheckbox);
          //if(mapofDSRobject.get(DSRteamobject.GW1_DSR__c).Create_chatter_Group__c==true)
          //if((DSRteamobject.Create_Group_check_box__c) )
          if(dsrcreatedcheckbox==true)
          {
        /* creating chatter group*/  
        
         CollaborationGroup objGrp = new CollaborationGroup();
            objGrp.Name = s+' '+'Think Tank';
            objGrp.CollaborationType = 'Private';
                //  objGrp.OwnerId = DSRteamobject.
              system.debug('@objGrp'+objGrp);
         //    objGrp.OwnerId= DSRteamobject.GW1_DSR__r.Deal_Manager_ID__c;
                system.debug('@objGrpDSRteamobject'+DSRteamobject.GW1_DSR__r.Deal_Manager_ID__c);
            system.debug('think tank owner if end'+mapofDSRobject.get(DSRteamobject.GW1_DSR__c).GW1_Bid_Manager__r.GW1_Bid_Manager__r.id);
            objGrp.OwnerId = mapofDSRobject.get(DSRteamobject.GW1_DSR__c).GW1_Bid_Manager__r.GW1_Bid_Manager__r.id;
             lstChatterGroupToInsert.add(objGrp);
            }
      
           
    }
}
  /*system.debug('think tank owner if started');
if((mapchattergroup.containskey(s+' '+'Think Tank')))
{

DSRteamobject.DSR_Group__r.ownerid=mapofDSRobject.get(DSRteamobject.GW1_DSR__c).GW1_Bid_Manager__r.GW1_Bid_Manager__r.id;

}
  system.debug('think tank owner if ended');*/
/* if(!(mapchattergroup2.containskey(s.left(15)+' '+'Pricing group')) && ((DSRteamobject.GW1_Role__c=='Pricing'|| DSRteamobject.GW1_Role__c=='Lead Solutions Architect'|| DSRteamobject.GW1_Role__c=='Digital')) && (DSRteamobject.GW1_DSR__r.GW1_Bid_Manager__c!=NULL || string.isBlank(s1)))
    {
         CollaborationGroup objGrp = new CollaborationGroup();
            objGrp.Name = s.left(15)+' '+'Pricing group';
            objGrp.CollaborationType = 'Private';
            objGrp.OwnerId = DSRteamobject.ownerid;
            
            lstChatterGroupToInsert.add(objGrp);
           
    }*/

}
}

if(lstChatterGroupToInsert!= NULL || lstChatterGroupToInsert.size()>0)
{/* inserting chatter group*/
    Insert lstChatterGroupToInsert;
    
    
}
list<CollaborationGroupMember> lstCollaborationGroupMember=new list<CollaborationGroupMember>();
set<id> GroupID =new set<id>();
map<string,CollaborationGroup> mapchattergroupforteam=new map<string,CollaborationGroup>();
/* quering insert added chatter group to  add members */
    for(CollaborationGroup chattergroupobject:[select id,name from CollaborationGroup where name Like '%Think Tank%' ])
{
    mapchattergroupforteam.put(chattergroupobject.name,chattergroupobject);
    GroupID.add(chattergroupobject.id);
}/* querying member in chatter group*/
 map<string,CollaborationGroupMember> mapchattermembercheck=new map<string,CollaborationGroupMember>();
    for(CollaborationGroupMember member:[select id,memberid,CollaborationGroupId from CollaborationGroupMember where CollaborationGroupId=:GroupID])
    {
        String s2=member.CollaborationGroupId;
        string s3=member.memberid;
        mapchattermembercheck.put(s2+s3,member);
    }

for(GW1_DSR_Team__c DSRTEAMobject:trigger.new)

  {
       if(mapofDSRobject.containsKey(DSRTEAMobject.GW1_DSR__c))
    {
        
    string s=mapofDSRobject.get(DSRTEAMobject.GW1_DSR__c).Pseudo_name1__c;
    //+' '+mapofDSRobject.get(DSRTEAMobject.GW1_DSR__c).GW1_Opportunity__r.Opportunity_ID__c;
      
     
      
      
      string s1=mapofDSRobject.get(DSRTEAMobject.GW1_DSR__c).GW1_Bid_Manager__c;
      String s2;
        boolean dsrcreatedcheckbox=mapofDSRobject.get(DSRteamobject.GW1_DSR__c).Create_chatter_Group__c;
      system.debug('mapchattergroupforteam: '+ mapchattergroupforteam);
      if(mapchattergroupforteam.containskey(s+' '+'Think Tank'))
      {
       s2=mapchattergroupforteam.get(s+' '+'Think Tank').id;
      }
      ID id1=DSRTEAMobject.GW1_User__c;
      string s3=id1;
     
      
      if(!mapchattermembercheck.containsKey(s2+s3) && mapchattergroupforteam.containskey(s+' '+'Think Tank')  )
     {
         /*  chacking DSR Team Role is BD,Lead Solutions Architect and Deal Manager*/
      if((DSRteamobject.GW1_Role__c=='BD'|| DSRteamobject.GW1_Role__c=='Lead Solutions Architect' || DSRteamobject.GW1_Role__c=='Deal Manager'|| DSRteamobject.GW1_Role__c=='Sales Leader' || DSRteamobject.GW1_Role__c=='Tower Lead' || DSRteamobject.GW1_Role__c=='Pricing' || DSRteamobject.GW1_Role__c=='Commercial Proposal Leader'|| DSRteamobject.GW1_Role__c=='Commercial Proposal Governance Leader'|| DSRteamobject.GW1_Role__c=='Commercial Proposal Governance team member') )
      {
          
          //if( mapofDSRobject.get(DSRTEAMobject.GW1_DSR__c).Create_chatter_Group__c==true)
              //if((DSRTEAMobject.Create_Group_check_box__c) )
              system.debug('dsr check box'+dsrcreatedcheckbox);
              
              if(dsrcreatedcheckbox==true)
          {
          /* Adding members to chatter group */
              system.debug('dsr check box after if condition'+dsrcreatedcheckbox);
          
                            CollaborationGroupMember grpMr = new CollaborationGroupMember();
                            grpMr.memberid = id1;
                            grpMr.CollaborationGroupId = mapchattergroupforteam.get(s+' '+'Think Tank').id;
                            system.debug('@grpMr'+grpMr);
                            lstCollaborationGroupMember.add(grpMr);
          }
                            
                            
     }
     }
                           
/* if(mapchattergroupforteam.containskey(s.left(10)+' '+'Pricing group') && listofDSRUSER.containsKey(DSRTEAMobject.GW1_User__c) && (DSRTEAMobject.GW1_DSR__r.GW1_Bid_Manager__c!=NULL || string.isBlank(s1)))                      
     
    {
         if(DSRTEAMobject.GW1_Role__c=='Pricing'|| DSRTEAMobject.GW1_Role__c=='Lead Solutions Architect'|| DSRTEAMobject.GW1_Role__c=='Digital')
      {
          CollaborationGroupMember grpMr = new CollaborationGroupMember();
                            grpMr.memberid = listofDSRUSER.get(DSRTEAMobject.GW1_User__c).id;
                            grpMr.CollaborationGroupId = mapchattergroupforteam.get(s.left(10)+' '+'Pricing group').id;
                            lstCollaborationGroupMember.add(grpMr);
                            }
                            
      }*/
  }
}
  if(lstCollaborationGroupMember.size()>0)
{
    /* inserting chatter group members*/
    Insert lstCollaborationGroupMember;
}


    
  
  
  
  
  
  
  }