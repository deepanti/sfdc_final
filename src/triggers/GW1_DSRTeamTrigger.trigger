/*  --------------------------------------------------------------------------------------
	  Description: this trigger will  Create Chatter Member, add Sharing Setting and removeSharing
	  Setting if User not active
	  --------------------------------------------------------------------------------------
	  events : after insert, after delete, before update
	  --------------------------------------------------------------------------------------
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          03-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
 */

trigger GW1_DSRTeamTrigger on GW1_DSR_Team__c(after insert, after delete, before update)
{

	GW1_triggerSettings__c objSettings = GW1_triggerSettings__c.getValues('GW1_DSRTeamTrigger');
	if (objSettings != null && objSettings.GW1_Is_Active__c)
	{
		GW1_DSRTeamTriggerHandler objHandler = new GW1_DSRTeamTriggerHandler();
		objHandler.runTrigger();
	}
	else
	{
		System.debug('=====TRIGGER GW1_DSRTeamTrigger In Active From Custom Setting=====');
	}
}