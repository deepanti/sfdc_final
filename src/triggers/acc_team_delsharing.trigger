/**
@ Developed by   : Arjun Srivastava
@ date            23 April, 2011
@ description     This trigger will help in removal of a oli sharing when account team member is deleted.
*/
Trigger acc_team_delsharing on AccountArchetype__c (Before Delete) 
{/*
    List<ID> userids=new List<ID>();    //get all the userids
    List<ID> accids=new List<ID>();     //get all the accountids
    List<OpportunityProduct__c> oliList=new List<OpportunityProduct__c>();
    For(AccountArchetype__c acc:Trigger.old)
    {
        userids.add(acc.User__c);
        accids.add(acc.Account__c);
    }
    
    //fetching all oli's associated with the given accounts
    oliList=[select id from OpportunityProduct__c where OpportunityId__r.Accountid IN:accids];
    
    //Removing the OLI sharing for account team members
    Delete [Select id from OpportunityProduct__Share where ParentId IN:oliList AND UserOrGroupId IN:userids];
*/
}