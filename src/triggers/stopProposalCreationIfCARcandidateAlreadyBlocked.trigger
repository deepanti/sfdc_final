trigger stopProposalCreationIfCARcandidateAlreadyBlocked on Proposal__c (before insert) 
{  
    List < Id > empmasterIds = new List < Id >();
    List < Id > proposalIds = new List < Id >();

    Profile auth_profile = [select id from profile where name='HS_StandardUser_RMG']; 
    Id v_User = UserInfo.GetProfileId();
    
    for ( Proposal__c  c: Trigger.New ) 
    {
        proposalIds.add( c.Id );
        empmasterIds.add( c.CAR_Candidate_Code__c);
    }
    
    List<CAR_Candidate__c> opps = [select id, Proposal_Status__c from CAR_Candidate__c where id in :empmasterIds];
   
    Map < Id ,CAR_Candidate__c > empmap = new Map < Id , CAR_Candidate__c >();

    for ( CAR_Candidate__c  a : opps   ) 
    {
        empmap.put( a.Id, a);
    }

    List < CAR_Candidate__c > EmpToUpdate = new List < CAR_Candidate__c >();
    
    
  for(Proposal__c c: Trigger.New) 
    {
            CAR_Candidate__c ac = empmap.get( c.CAR_Candidate_Code__c );         
            if ( ac == null ) 
            {    
              continue;
            }
        
            /* Get all proposals for this employee.           */
            List<Proposal__c> proObj1 = [select Id,Name,Employee_Email_Address__c,Status__c,CAR_Candidate_Code__c from Proposal__c where Id !=:c.Id and CAR_Candidate_Code__c =:ac.Id  ];
            for(Proposal__c pc1: proObj1 )
            {
                system.debug('----------->'+pc1.Name + ' ' +pc1.Status__c);
                if( pc1.Status__c == 'Selected-New' || pc1.Status__c == 'Blocked' || pc1.Status__c == 'Selected-Extended')
                {
                    c.addError('Employee already selected or blocked. No new Proposal can be created till that block is removed.');
                    //Trigger.new[0].Status__c.addError('If the Proposal with Status as Selected / Blocked exists, no new Proposal can be created');
                }
            }
        }

        
   
}//End of Trigger