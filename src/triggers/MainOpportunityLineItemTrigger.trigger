trigger MainOpportunityLineItemTrigger on OpportunityLineItem (before insert,before update,before delete,After insert,After update,After Delete) {
    map<string, MainOpportunityHelperFlag__c>  MapofProjectTrigger = MainOpportunityHelperFlag__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('MainOpportunityLineItemTrigger') && MapofProjectTrigger.get('MainOpportunityLineItemTrigger').HelperName__c){
        ID loggeedInUserProfileID = UserInfo.getProfileId();
        String profileName = [Select Name from Profile Where ID =: loggeedInUserProfileID].Name;
        if(trigger.isbefore){
            if(trigger.isInsert){
                if(!Test.isRunningTest()){  if(Trigger.isBefore && CheckRecursiveForOLI.runOnceBefore() && Label.Data_Loader_Upload == 'true' && profileName == 'Genpact Super Admin'){  
                        if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('beforeInsertMethodOli') && MapofProjectTrigger.get('beforeInsertMethodOli').HelperName__c){  MainOpportunityLineItemTriggerHelper.beforeInsertMethod(Trigger.new);
                        }
                    }    
                }    
            }
            if(trigger.isUpdate){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('beforeUpdateMethodOli') && MapofProjectTrigger.get('beforeUpdateMethodOli').HelperName__c){
                    MainOpportunityLineItemTriggerHelper.beforeUpdateMethod(Trigger.new,Trigger.oldMap);
                }
            }
            if(trigger.isDelete){
                
            }
        }
        if(trigger.isAfter){
            if(trigger.isInsert){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterInsertMethodOli') && MapofProjectTrigger.get('afterInsertMethodOli').HelperName__c){ MainOpportunityLineItemTriggerHelper.afterInsertMethod(Trigger.new,Trigger.newMap,profileName);
                }
            }
            if(trigger.isUpdate){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterUpdateMethodOli') && MapofProjectTrigger.get('afterUpdateMethodOli').HelperName__c){
                    MainOpportunityLineItemTriggerHelper.afterUpdateMethod(Trigger.new,Trigger.oldMap,profileName);
                }
            }
            if(trigger.isDelete){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterDeleteMethodOli') && MapofProjectTrigger.get('afterDeleteMethodOli').HelperName__c){    MainOpportunityLineItemTriggerHelper.afterDeleteMethod(Trigger.Old,Trigger.oldMap);
                }
            }
        }
    }
    /*Added by vikas on oli insert 27/02/2019*/
    map<string, GP_Sobject_Controller__c>  MapofProjectTriggerpinnacle = GP_Sobject_Controller__c.getall();
    if(MapofProjectTriggerpinnacle  != null && MapofProjectTriggerpinnacle.containsKey('opportunitylineitem') && MapofProjectTriggerpinnacle.get('opportunitylineitem').GP_Enable_Sobject__c) {
        fflib_SObjectDomain.triggerHandler(GPDomainOpportunityProduct.class);
    }
}