trigger ContactRoleRVTrigger on Contact_Role_Rv__c (After insert, After update, After delete, Before insert, Before Update, Before delete) 
{    
    if(Trigger.isInsert){ 
        if(Trigger.isBefore){
            if(CheckRecursiveForContactRole.runOnceBefore())
                ContactRoleRVTriggerHelper.checkDuplicateContact(Trigger.new);  
        }
        if(Trigger.isAfter){
             if(CheckRecursiveForContactRole.runOnceAfter())
                ContactRoleRVTriggerHelper.insertContactRoleAndRevenueStormBarometer(Trigger.new);     
        }
             
    }
    
    if(Trigger.isUpdate){
        if(Trigger.isBefore){
              if(CheckRecursiveForContactRole.runOnceBefore())
                ContactRoleRVTriggerHelper.checkDuplicateContact(Trigger.new);
        }
        if(Trigger.isAfter){
              if(CheckRecursiveForContactRole.runOnceAfter())
                ContactRoleRVTriggerHelper.updateContactRoleAndRevenueStormBarometer(Trigger.newMap, Trigger.old);    
        }
        
    }
    
    if(Trigger.isDelete){
        if(Trigger.isBefore){
          ContactRoleRVTriggerHelper.restrictPrimaryContact(Trigger.old);
        }
        if(Trigger.isAfter){
            ContactRoleRVTriggerHelper.deleteContactRoleAndRevenueStormBarometer(Trigger.old);  
        }   
    }
}