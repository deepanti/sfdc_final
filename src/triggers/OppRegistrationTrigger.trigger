trigger OppRegistrationTrigger on Opportunity_Registration__c (before insert) {
	OppRegistrationHandler.populateGAContact(trigger.new);
}