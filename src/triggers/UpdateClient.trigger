trigger UpdateClient on RMG_Employee_Allocation__c (after insert,after update) 
{
   List<RMG_Employee_Master__c> employeeList = new List<RMG_Employee_Master__c>();
   List<RMG_Employee_Allocation__c> empAllocations = new List<RMG_Employee_Allocation__c>();
   
   List<ID> masterIds = new List<ID>();
   Map<ID, String> childDetailMap = new Map<ID, String>();
   
   for(RMG_Employee_Allocation__c c: Trigger.new)
   {
      masterIds.add(c.RMG_Employee_Code__c);
      childDetailMap.put(c.RMG_Employee_Code__c, (c.Client__c));
   }
  
   employeeList= [select id, Current_Allocation1__c from RMG_Employee_Master__c where id in :masterIds];
   
   for(RMG_Employee_Master__c employee: employeeList)
   {
        List<ID> empIds = new List<ID>();
        empIds.add(employee.id);
        empAllocations = [select id, Client__c from RMG_Employee_Allocation__c where Status__c!='Past' AND Active__c != 'No' AND RMG_Employee_Code__c in :empIds];
    
        String currentAllocation=null;
        for(RMG_Employee_Allocation__c empAllocation: empAllocations){
            if (currentAllocation == null){
                currentAllocation =  empAllocation.Client__c;
            } else if(!currentAllocation.contains(empAllocation.Client__c)){
                currentAllocation += ';'+empAllocation.Client__c;
            }
        }
        employee.Current_Allocation1__c =   currentAllocation;
   }
     
   Update employeeList;
}