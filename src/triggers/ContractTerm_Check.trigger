trigger ContractTerm_Check on Opportunity (before update) {


    Set<Id> idset= Trigger.newMap.keySet();
    list<OpportunityProduct__c> newopp=[Select id, name,EndDate__c, Opportunityid__c from OpportunityProduct__c where Opportunityid__c in :idset];
       
    for(Opportunity opp:trigger.new){
        Integer i=0;
       
        String[] s= new list<String>();
        // Oprod= [Select id, name,EndDate__c, Opportunityid__c from OpportunityProduct__c  where Opportunityid__c=:opp.id ];
        for(OpportunityProduct__c newOprod : newopp ){
             
            // System.debug('OLI date '+newOprod.EndDate__c);
        //if(newOprod.Opportunityid__c == opp.id ){
                if(opp.End_Date__c<newOprod.EndDate__c) {
                    S.add(newOprod.Name);
                   i++;
                }
            }
        
        /*if(i>0){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust contract term as'+S[1]+'end date is greater then Opportunity End Date');
            }*/
        if(i>5){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term as '+S[0]+','+S[1]+','+S[2]+','+S[3]+','+S[4]+' ,....... End Dates are greater then Opportunity End Date');
            }
  
          else if(i>4){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term, as '+S[0]+','+S[1]+','+S[2]+','+S[3]+','+S[4]+' End Dates are greater then Opportunity End Date');
            }
          else if(i>3){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term, as '+S[0]+','+S[1]+','+S[2]+','+S[3]+' End Dates are greater then Opportunity End Date');
            }
            else if(i>2){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term, as '+S[0]+','+S[1]+','+S[2]+' End Dates are greater then Opportunity End Date');
            }
               else if(i>1){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term, as '+S[0]+','+S[1]+' End Dates are greater then Opportunity End Date');
            } 
            else if(i>0){
                             
                opp.Contract_Term_in_months__c.addError('Re-adjust Contract Term, as '+S[0]+' End Date is greater then Opportunity End Date');
            }   
            
    }
}