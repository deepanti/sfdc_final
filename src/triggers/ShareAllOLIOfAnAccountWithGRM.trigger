trigger ShareAllOLIOfAnAccountWithGRM on AccountArchetype__c (after insert , after update)
{
    if(Validator_cls.AllowShareAllOLIOfAnAccountWithGRM)
    {
    Validator_cls.AllowShareAllOLIOfAnAccountWithGRM = false;
    map<Id,AccountArchetype__c> RecordsToProcess = new Map<Id,AccountArchetype__c>();
    Map<id,list<AccountArchetype__c>> AccountToPrecess = new  Map<id,list<AccountArchetype__c>>();
    Set<id> OldOwner = new set<id>();
    for(integer i =0;i<trigger.new.size();i++)
    {
        if(trigger.new[i].Team_Member_Role__c == 'GRM')
        {
            if(trigger.IsInsert)
            {
                RecordsToProcess.put(trigger.new[i].id,trigger.new[i]);
                OldOwner.add(trigger.New[i].User__c);
            }
            if(trigger.IsUpdate)
            {
                if(trigger.new[i].User__c != trigger.old[i].User__c)
                {
                     RecordsToProcess.put(trigger.new[i].id,trigger.new[i]);
                     OldOwner.add(trigger.Old[i].User__c);
                }
            }
            
            if(AccountToPrecess.containsKey(trigger.new[i].Account__c))
            {
                list<AccountArchetype__c> TempLst = new list<AccountArchetype__c>();
                TempLst.addall(AccountToPrecess.get(trigger.new[i].Account__c));
                TempLst.add(trigger.new[i]);
                AccountToPrecess.put(trigger.new[i].Account__c,TempLst);
            }
            else
            {
                list<AccountArchetype__c> TempLst = new list<AccountArchetype__c>();
                TempLst.add(trigger.new[i]);
                AccountToPrecess.put(trigger.new[i].Account__c,TempLst);
            }
        }
    }
    system.debug('#@!#@!#@!#@!RecordsToProcess.size='+RecordsToProcess.size());
    if(RecordsToProcess.size()>0)
    {
        Map<Id,OpportunityProduct__c> AllOppOLIMap = new Map<Id,OpportunityProduct__c>();
        AllOppOLIMap = new Map<Id,OpportunityProduct__c>([select id,name,Opportunityid__c,Opportunityid__r.AccountId, ownerId  from OpportunityProduct__c where Opportunityid__r.AccountId in : AccountToPrecess.keyset()]);
        list<OpportunityProduct__Share> LstOLIShare = new List<OpportunityProduct__Share>();
        LstOLIShare = [select id,UserOrGroupId,ParentId,AccessLevel,RowCause from OpportunityProduct__Share where ParentId in : AllOppOLIMap.keyset() and UserOrGroupId in : OldOwner and RowCause ='Manual'];
        // as of now Sharing table updata in not allowed So i am Deleteing the shraing record and then crate the new onc
        if(!(LstOLIShare.IsEmpty()))
        {
            delete LstOLIShare;
        }
        list<OpportunityProduct__Share> LstOLIShareRecordToInsert = new List<OpportunityProduct__Share>();
        system.debug('#@^$%$%%#@$#!AccountToPrecess='+AccountToPrecess);
        for(OpportunityProduct__c TempOLIObj : AllOppOLIMap.values())
        { system.debug('#@^$%$%%#@$#!TempOLIObj.Opportunityid__r.AccountId='+TempOLIObj.Opportunityid__r.AccountId);
            if(AccountToPrecess.containskey(TempOLIObj.Opportunityid__r.AccountId))
            {
                for(AccountArchetype__c TempAccountMemberObj : AccountToPrecess.get(TempOLIObj.Opportunityid__r.AccountId))     
                {
                    system.debug('#@^$%$%%#@$#!AccountToPrecess='+TempAccountMemberObj.User__c);
                    system.debug('#@^$%$%%#@$#!TempOLIObj.ownerId='+TempOLIObj.ownerId);
                    if(TempAccountMemberObj.User__c != TempOLIObj.ownerId )
                    {
                        OpportunityProduct__Share OppOwnerOLIShr  = new OpportunityProduct__Share(); 
                        OppOwnerOLIShr.ParentId = TempOLIObj.id;
                        OppOwnerOLIShr.UserOrGroupId = TempAccountMemberObj.User__c;
                        OppOwnerOLIShr.AccessLevel = 'Read';
                        LstOLIShareRecordToInsert.add(OppOwnerOLIShr);
                    }
                }
            }
        }
        if(LstOLIShareRecordToInsert.size()>0)
        insert LstOLIShareRecordToInsert;
    }
  }
}