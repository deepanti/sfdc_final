trigger GPDomainOpportunityTrigger on Opportunity  (after delete, after insert, after update, after undelete, before delete, before insert, before update) 
{
    map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('opportunity')
       && MapofProjectTrigger.get('opportunity').GP_Enable_Sobject__c) {
        fflib_SObjectDomain.triggerHandler(GPDomainOpportunity.class);
    }
}