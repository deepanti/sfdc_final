trigger GenTHighestRevenueProduct on OpportunityProduct__c (after insert, after update, after delete) 
{
/*
    if(!singleexecuion.bool_check_hrevenue)
    {
        
        singleexecuion.bool_check_hrevenue=true;
        set<Id> OpportunityIdSet = new set<id>();
        for(OpportunityProduct__c OLITempObj: trigger.isdelete?trigger.old:trigger.new)
        {   
            if(OLITempObj.Opportunityid__c!=null)
                OpportunityIdSet.add(OLITempObj.Opportunityid__c);
        }
        if(OpportunityIdSet.size()>0)
        {
            Map<Id,Opportunity> OpportunityMap = new Map<Id,Opportunity>();
            OpportunityMap = new Map<Id,Opportunity>([select id,CloseDate,name,Revenue_Product__c,CMITs_Check__c,(select id,name,TCVLocal__c,Product_Group__c,COE__c from Opportunity_Products__r) from Opportunity where id in: OpportunityIdSet]);
            if(OpportunityMap.size()>0)
            {
                for(Opportunity OpportunityObj : OpportunityMap.values())
                {
                    Map<String,Double> Opp_ProductGroupTCVSum = new Map<String,Double>();
                    OpportunityObj.CMITs_Check__c=False;
                    System.debug('@!#@!$@#$@#$@#$!'+OpportunityObj.Opportunity_Products__r);
                    for(OpportunityProduct__c opty: OpportunityObj.Opportunity_Products__r)
                    {
                        if(opty.COE__c=='CMITS')
                        {
                            OpportunityObj.CMITs_Check__c=True;
                        }
                        if(opty.Product_Group__c != null)
                        {
                            if(Opp_ProductGroupTCVSum.containskey(opty.Product_Group__c))
                            {
                                Double Temp = Opp_ProductGroupTCVSum.get(opty.Product_Group__c);
                                if(opty.TCVLocal__c != null)
                                    Temp = Temp + opty.TCVLocal__c;
                                Opp_ProductGroupTCVSum.put(opty.Product_Group__c,Temp);
                                
                            }
                            else
                            {
                                Opp_ProductGroupTCVSum.put(opty.Product_Group__c,opty.TCVLocal__c);
                            }
                        }
                    }
                    Double highest_tcv = 0;
                    OpportunityObj.Revenue_Product__c = null;
                    for(String ProductGroupKey : Opp_ProductGroupTCVSum.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_ProductGroupTCVSum.get(ProductGroupKey);
                        if(TempHighestTCV > highest_tcv)
                        {
                            OpportunityObj.Revenue_Product__c = ProductGroupKey;
                            highest_tcv = TempHighestTCV;
                        }
                    
                }
               try{
                update OpportunityMap.values();
                }
                
                catch(System.DmlException e){
                
               String ErrorMsg = 'Entry of CMITS Project Type and CMITS Project Margin is MANDATORY since at least one of the Products have Delivering Organisation as CMITS';
              trigger.old[0].adderror(ErrorMsg,false);
              }
                
                
               
    
    
   //     String ErrorMsg = '';
     //   for (Integer i = 0; i < e.getNumDml(); i++)
     //   {
     //     ErrorMsg = ErrorMsg + String.valueof(e.getDmlMessage(0));
     //   }
   //    ErrorMsg = ErrorMsg + '</br> @@@';
     //  trigger.new[0].adderror(ErrorMsg,false);
       
       
    
            }           
        }
        }
    }
}
*/

/*
Old Trigger commted as not bulkify above code is bulkify 
*/
    //if(!singleexecuion.bool_check_hrevenue)
    {
        singleexecuion.bool_check_hrevenue=true;
        OpportunityProduct__c OLI;
        Double highest_tcv;
        Double temp;
        boolean AllowRun = false;
        list<Opportunity> opp = new list<Opportunity>();
        
       
        
        if(trigger.isDelete)
        {
        if(Trigger.old.size() == 1){
            OLI = Trigger.old[0];
            AllowRun = true;
        }
        }
        else
        {
            OLI = Trigger.new[0];
            if(Trigger.new.size() == 1)
            AllowRun = true;
        }
      
       if(AllowRun)
       {
               opp = [select id,CloseDate,name,Revenue_Product__c,(select id,name,TCVLocal__c,Product_Group__c,Product_Family_OLI__c,COE__c from Opportunity_Products__r) from Opportunity where id=:oli.Opportunityid__c];
            if(opp.size()>0)
           {
                system.debug('#!@#@!#@!#'+oli.Opportunityid__c);
               // Opportunity Opp1=new Opportunity();
              //  system.debug('#!@#@!#@!#'+opp[0].CloseDate);
              //  if(opp[0].CloseDate< System.today())
              //  {
             //   system.debug('#!@#@!#@!#inner'+opp[0].CloseDate);
             //   trigger.new[0].adderror('MSA /SOW Closure Date cannot be less than today');
             //   }
              //  else
             //   {
                system.debug('#!@#@!#@!#'+opp[0].Opportunity_Products__r.size());
                //opp.Revenue_Product__c = opp.Opportunity_Products__r[0].Product_Group__c;
                highest_tcv = 0;
                //Opp[0].IT_Service_Product__c=False;
                Opp[0].CMITs_Check__c=False;
             //   Opp[0].BFS_Product__c=False;
               // Opp[0].Analytics_Product__c=False;
              //  Opp[0].Akritiv__c=False;
               // Opp[0].Healthcare_Product__c=False;
               // Opp[0].Risk_Product__c=False;
              //  Opp[0].Insurance_Product__c=False;
             //   Opp[0].CMIT_Product__c=False;
                for(OpportunityProduct__c opty: opp[0].Opportunity_Products__r)
                {
                
                     if(opty.COE__c=='CMITS')
                     {
                        Opp[0].CMITs_Check__c=True;
                     }
                      
                    // if(opty.Product_family__c=='IT Managed Services'||opty.Product_family__c=='Enterprise Application Services and Adm')
                    // {
                  //      Opp[0].IT_Service_Product__c=True;
                  //   }
                  //   if(opty.Product_family__c=='BANKING & FINANCIAL SERVICES OPERATIONS')
             //        {
                   //     Opp[0].BFS_Product__c=True;
                 //    }
                  //   if(opty.Product_family__c=='Analytics')
                //     {
                  //      Opp[0].Analytics_Product__c=True;
                 //    }
                  //   if(opty.Product_family__c=='AKRITIV')
                  //   {
                  //      Opp[0].Akritiv__c=True;
                 //    }
                 //    if(opty.Product_family__c=='Healthcare Operations')
                //     {
                 //       Opp[0].Healthcare_Product__c=True;
                 //    }
                 //    if(opty.Product_family__c=='Insurance Operations')
                  //   {
                 //       Opp[0].Insurance_Product__c=True;
                 //    }
                     
                   //    if(opty.Product_family__c=='RiskManagementServices')
                    // {
                 //       Opp[0].Risk_Product__c=True;
                   //  }
                     
                  //     if(opty.Product_family__c=='CMIT')
                   //  {
                    //    Opp[0].CMIT_Product__c=True;
                  //   }
                }
                opp[0].Revenue_Product__c = null;
                for(OpportunityProduct__c op: opp[0].Opportunity_Products__r)
                {
                    List<AggregateResult> o= [Select SUM(TCVLocal__c) from OpportunityProduct__c where Product_Group__c=:op.Product_Group__c and Opportunityid__c=:opp[0].id];
                    system.debug('#!@#!@$!@#@'+op.id);
                    system.debug('#!@#!@$!@#@'+(double) o[0].get('expr0'));
                   
                    temp = (double) o[0].get('expr0');
                    if(temp>highest_tcv)
                    {
                        opp[0].Revenue_Product__c = op.Product_Group__c;
                        highest_tcv = temp;
                        temp=0;
                    }
                }
                update Opp[0];
            }
            }
      } 
      
      
 }