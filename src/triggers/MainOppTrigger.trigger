trigger MainOppTrigger on Opportunity (before insert, after insert, before update, after update, before delete, after delete) {
    system.debug(':---maintrigger----:');
    map<string, MainOpportunityHelperFlag__c>  MapofProjectTrigger = MainOpportunityHelperFlag__c.getall();
   
    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('opportunity') && MapofProjectTrigger.get('opportunity').HelperName__c){
        system.debug(':===testCustomSetting');
        if(trigger.isbefore){
            if(trigger.isInsert){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('beforeInsertMethod') && MapofProjectTrigger.get('beforeInsertMethod').HelperName__c){
                    MainOppTriggerHelper.beforeInsertMethod(Trigger.new);
                }
            }
            if(trigger.isUpdate){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('beforeUpdateMethod') && MapofProjectTrigger.get('beforeUpdateMethod').HelperName__c){
                    MainOppTriggerHelper.beforeUpdateMethod(Trigger.new, Trigger.oldMap);
                }
            }
            if(trigger.isDelete){ 
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('beforeDeleteMethod') && MapofProjectTrigger.get('beforeDeleteMethod').HelperName__c){
                    MainOppTriggerHelper.beforeDeleteMethod(trigger.old,trigger.oldMap);
                }
            }
        }
        if(trigger.isAfter){
            if(trigger.isInsert){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterInsertMethod') && MapofProjectTrigger.get('afterInsertMethod').HelperName__c){
                    MainOppTriggerHelper.afterInsertMethod(trigger.new,trigger.newMap);
                }
            }
            if(trigger.isUpdate){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterUpdateMethod') && MapofProjectTrigger.get('afterUpdateMethod').HelperName__c){
                    MainOppTriggerHelper.afterUpdateMethod(trigger.new,trigger.oldMap);
                    system.debug('after trigger');
                }
            }
            if(trigger.isDelete){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('afterDeleteMethod') && MapofProjectTrigger.get('afterDeleteMethod').HelperName__c){
                    MainOppTriggerHelper.afterDeleteMethod(trigger.old,trigger.oldMap);
                }
            }
        }
       
            if(CheckRecursive.runOncePinnacle()){
                map<string, GP_Sobject_Controller__c>  MapofProjectTrigger = GP_Sobject_Controller__c.getall();
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('opportunity')
                   && MapofProjectTrigger.get('opportunity').GP_Enable_Sobject__c) {
                       fflib_SObjectDomain.triggerHandler(GPDomainOpportunity.class);
                   }
            }  
        
    }
}