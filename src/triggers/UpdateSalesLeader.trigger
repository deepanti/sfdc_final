trigger UpdateSalesLeader on Account_Creation_Request__c (before insert,before Update) 
{
    set<id> SalesUnitId = new set<id>();
    map<id,list<Account_Creation_Request__c>> SalesUnitBasedAccountCreation = new map<id,list<Account_Creation_Request__c>>();
    for(Account_Creation_Request__c TempObj : trigger.new)
    {
        if(TempObj.Sales_Unit__c != null)
        {
            SalesUnitId.add(TempObj.Sales_Unit__c);
            if(SalesUnitBasedAccountCreation.containskey(TempObj.Sales_Unit__c))
            {
                list<Account_Creation_Request__c> TempLst = new list<Account_Creation_Request__c>();
                TempLst.addall(SalesUnitBasedAccountCreation.get(TempObj.Sales_Unit__c));
                TempLst.add(TempObj);
                SalesUnitBasedAccountCreation.put(TempObj.Sales_Unit__c,TempLst);
            }
            else
            {
                list<Account_Creation_Request__c> TempLst = new list<Account_Creation_Request__c>();
                TempLst.add(TempObj);
                SalesUnitBasedAccountCreation.put(TempObj.Sales_Unit__c,TempLst);
            }
        }
    }
    list<Sales_Unit__c> SalesUnitLst = [select id,name,Sales_Leader__c,Sales_Lead_Active__c from Sales_Unit__c where Id in : SalesUnitId];
    for(Sales_Unit__c TempSalesUnitObj : SalesUnitLst)
    {
        if(SalesUnitBasedAccountCreation.containskey(TempSalesUnitObj.id))
        {
            for(Account_Creation_Request__c TempObj : SalesUnitBasedAccountCreation.get(TempSalesUnitObj.id))
            {
                TempObj.User_sales_leader__c = TempSalesUnitObj.Sales_Leader__c;
            }
        }
    }
}