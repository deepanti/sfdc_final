trigger stopProposalCreationIfRRAlreadyBlockedandselectednew on Proposal__c (before insert) 
{  
   
    List < Id > empmasterIds = new List < Id >();
    List < Id > proposalIds = new List < Id >();

    //Profile auth_profile = [select id from profile where name='HS_StandardUser_RMG']; 
    //Id v_User = UserInfo.GetProfileId();
    
    for ( Proposal__c  c: Trigger.New ) 
    {
         proposalIds.add( c.Id );
        empmasterIds.add( c.Proposal_RR__c);
    }
    
    List<RR__c> opps = [select id, Proposal_Status__c from RR__c where id in :empmasterIds];
   
    Map < Id,RR__c > RRmap = new Map < Id,RR__c >();

    for ( RR__c a : opps   ) 
    {
        RRmap.put( a.Id, a);
    }

    List < RR__c > RRToUpdate = new List < RR__c >();
    
    
  for(Proposal__c c: Trigger.New) 
    {
            RR__c ac = RRmap.get( c.Proposal_RR__c );         
            if ( ac == null ) 
            {    
              continue;
            }
        
            /* Get all proposals for this employee.           */
            List<Proposal__c> proObj1 = [select Id,Name,Employee_Email_Address__c,Status__c,Proposal_RR__c from Proposal__c where Id !=:c.Id and Proposal_RR__c=:ac.Id  ];
            for(Proposal__c pc1: proObj1 )
            {
                system.debug('----------->'+pc1.Name + ' ' +pc1.Status__c);
                if( pc1.Status__c == 'Selected-New' || pc1.Status__c == 'Blocked' || pc1.Status__c == 'Selected-Extended')
                {
                    c.addError('There can be only one Selected / Blocked, proposal on the same RR');
                    //Trigger.new[0].Status__c.addError('If the Proposal with Status as Selected / Blocked exists, no new Proposal can be created');
                }
            }
        }

}//End of Trigger