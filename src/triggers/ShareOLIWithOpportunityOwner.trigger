trigger ShareOLIWithOpportunityOwner on Opportunity (after update)
{
    /* When user update the Opportunity Owner same user will be added in OLI sharing
        and old owner will get deleted from OLI sharing
    */
    set<Id> OpportunitiesToPrecess = new set<Id>();
    Set<id> OldOwner = new set<id>();
        
    for(integer i=0; i<Trigger.new.size(); i++)
    {
       if(trigger.new[i].ownerId != trigger.old[i].ownerId)
       {
           OpportunitiesToPrecess.add(trigger.new[i].id);
           OldOwner.add(trigger.old[i].ownerId);
       }        
    } 
    if(!(OpportunitiesToPrecess.IsEmpty()))
    {
        Map<Id,OpportunityProduct__c> AllOppOLIMap = new Map<Id,OpportunityProduct__c>();
        AllOppOLIMap = new Map<Id,OpportunityProduct__c>([select id,name,Opportunityid__c,ownerId  from OpportunityProduct__c where Opportunityid__c in : OpportunitiesToPrecess]);
        if(AllOppOLIMap.size()>0)
        {
            list<OpportunityProduct__Share> LstOLIShare = new List<OpportunityProduct__Share>();
            LstOLIShare = [select id,UserOrGroupId,ParentId,AccessLevel,RowCause from OpportunityProduct__Share where ParentId in : AllOppOLIMap.keyset() and UserOrGroupId in : OldOwner and RowCause ='Manual'];
            // as sharing table updata in not allowed i need to delete the shraing record and then crate the new onc
            if(!(LstOLIShare.IsEmpty()))
            {
                delete LstOLIShare;
            }
            list<OpportunityProduct__Share> LstOLIShareRecordToInsert = new List<OpportunityProduct__Share>();
            for(OpportunityProduct__c TempObj : AllOppOLIMap.values())
            {
                if(TempObj.ownerId != Trigger.newMap.get(TempObj.Opportunityid__c).ownerId)
                {
                    OpportunityProduct__Share OppOwnerOLIShr  = new OpportunityProduct__Share(); 
                    OppOwnerOLIShr.ParentId = TempObj.id;
                    OppOwnerOLIShr.UserOrGroupId = Trigger.newMap.get(TempObj.Opportunityid__c).ownerId;
                    OppOwnerOLIShr.AccessLevel = 'Read';
                    LstOLIShareRecordToInsert.add(OppOwnerOLIShr);
                }
             }
             if(LstOLIShareRecordToInsert.size()>0)
             insert LstOLIShareRecordToInsert;
        }
    }
}