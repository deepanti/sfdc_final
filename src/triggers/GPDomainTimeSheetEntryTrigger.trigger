trigger GPDomainTimeSheetEntryTrigger on GP_Timesheet_Entry__c (after delete, after insert, after update, after undelete, before delete, before insert, before update) {
    map<string, GP_Sobject_Controller__c>  MapOfTimeSheetEntryTrigger = GP_Sobject_Controller__c.getall();
    if(MapOfTimeSheetEntryTrigger != null && MapOfTimeSheetEntryTrigger.containsKey('gp_timesheet_entry__c') 
       && MapOfTimeSheetEntryTrigger.get('gp_timesheet_entry__c').GP_Enable_Sobject__c)
    {
        fflib_SObjectDomain.triggerHandler(GPDomainTimesheetEntry.class);
    }
}