trigger ProductRevenueScheduleTrigger on Product_Revenue_Schedule__c (after insert, after update) {
	ProductRevenueScheduleTriggerHelper.insertRevenueScheduleForOLI(Trigger.new);
}