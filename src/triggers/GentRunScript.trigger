trigger GentRunScript on Product_Schedule_Dummy__c (before insert) 
{
    List<Product_Schedule_Dummy__c> LstOfRecordPassTheValidation = new  List<Product_Schedule_Dummy__c>();
    Map<Id,List<Product_Schedule_Dummy__c>> ProductAndRevinewKeyMap = new Map<Id,List<Product_Schedule_Dummy__c>>();
    integer i = 0;
    for(Product_Schedule_Dummy__c TempObj : trigger.new)
    {
        if(TempObj.Product_Internal_ID__c != null)
        {
            if(ProductAndRevinewKeyMap.containskey(TempObj.Product_Internal_ID__c))
            {
                List<Product_Schedule_Dummy__c> TempLst = ProductAndRevinewKeyMap.get(TempObj.Product_Internal_ID__c);
                TempLst.add(TempObj);
                ProductAndRevinewKeyMap.put(TempObj.Product_Internal_ID__c,TempLst);
            }
            else
            {
                List<Product_Schedule_Dummy__c> TempLst = new  List<Product_Schedule_Dummy__c>();
                TempLst.add(TempObj);
                ProductAndRevinewKeyMap.put(TempObj.Product_Internal_ID__c,TempLst);
            }    
           LstOfRecordPassTheValidation.add(TempObj);
        } 
        else
        {
            trigger.new[i].adderror('Please Enter Product Internal ID');
        }  
        i++;
     }
     Map<Id,OpportunityProduct__c> OpportunityProductMap = new Map<Id,OpportunityProduct__c>([select id,name,Opportunityid__c,Opportunityid__r.ownerId from OpportunityProduct__c where id in : ProductAndRevinewKeyMap.keyset()]);
     for(integer j=0;j<trigger.new.size();j++)
     {
         if(trigger.new[j].Product_Internal_ID__c != null)
         {
             if(OpportunityProductMap.containskey(trigger.new[j].Product_Internal_ID__c))
             {
                 trigger.new[j].ownerId = OpportunityProductMap.get(trigger.new[j].Product_Internal_ID__c).Opportunityid__r.ownerId; 
                 trigger.new[j].OpportunityId__c = OpportunityProductMap.get(trigger.new[j].Product_Internal_ID__c).Opportunityid__c;
             }
         }
     }
     list<ProductSchedule__c> ExistingProductScheduleLst = [select id,name from ProductSchedule__c where OpportunityProductId__c in : ProductAndRevinewKeyMap.keyset()];
     list<RevenueSchedule__c> ExistingRevenueScheduleLst = [select id,name from RevenueSchedule__c where OpportunityProduct__c in : ProductAndRevinewKeyMap.keyset()];
     if(ExistingRevenueScheduleLst.size()>0)
        delete ExistingRevenueScheduleLst;
    if(ExistingProductScheduleLst.size()>0)
        delete ExistingProductScheduleLst;
     system.debug('$#@#$#$@#$@#'+LstOfRecordPassTheValidation);
     if (LstOfRecordPassTheValidation.size()>0)
        system.debug('$#@#$#$@#$@#'+LstOfRecordPassTheValidation[0].OpportunityId__c);
      GENcApexScriptForProduct_Schedule.RunScript(LstOfRecordPassTheValidation);

 
   

   }