trigger QSRMTrigger on QSRM__c (before insert, before update) {
    
    for(QSRM__c QSRM: Trigger.new){
        if((QSRM.KP_Responce__c == 'Approve') &&((QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected') &&(QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') &&(QSRM.Vertical_Leader_approval_Status__c != 'Approved' && QSRM.Vertical_Leader_approval_Status__c != 'Rejected'))) { QSRM.TS_Sales_Leader_Approval_Status__c='Approved'; QSRM.Service_Line_leader_SL_Approval__c='Approved';  QSRM.Vertical_Leader_approval_Status__c='Approved';}
        
        else if( (QSRM.KP_Responce__c == 'Rejected') &&((QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected') &&(QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') &&(QSRM.Vertical_Leader_approval_Status__c != 'Approved' && QSRM.Vertical_Leader_approval_Status__c != 'Rejected'))) { QSRM.TS_Sales_Leader_Approval_Status__c='Rejected'; QSRM.Service_Line_leader_SL_Approval__c='Rejected'; QSRM.Vertical_Leader_approval_Status__c='Rejected';
        }
        
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.Vertical_Leader_approval_Status__c != 'Approved'  && QSRM.Vertical_Leader_approval_Status__c != 'Rejected' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected') { QSRM.Vertical_Leader_approval_Status__c = 'Rejected'; QSRM.TS_Sales_Leader_Approval_Status__c='Rejected'; QSRM.Status__c='Rejected';
        }
        
        
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.Vertical_Leader_approval_Status__c != 'Approved'  && QSRM.Vertical_Leader_approval_Status__c != 'Rejected' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') { QSRM.Vertical_Leader_approval_Status__c = 'Rejected'; QSRM.Service_Line_leader_SL_Approval__c='Rejected';QSRM.Status__c='Rejected';
        }    
        
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') { QSRM.TS_Sales_Leader_Approval_Status__c = 'Rejected'; QSRM.Service_Line_leader_SL_Approval__c='Rejected';QSRM.Status__c='Rejected';
        }  
        
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.Vertical_Leader_approval_Status__c != 'Approved' && QSRM.Vertical_Leader_approval_Status__c != 'Rejected'){ QSRM.Vertical_Leader_approval_Status__c = 'Rejected';QSRM.Status__c='Rejected';
        }
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected'){ QSRM.TS_Sales_Leader_Approval_Status__c = 'Rejected';QSRM.Status__c='Rejected';
                }
        else if(QSRM.KP_Responce__c == 'Rejected' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected'){ QSRM.Service_Line_leader_SL_Approval__c = 'Rejected';QSRM.Status__c='Rejected';
                }
        
        
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.Vertical_Leader_approval_Status__c != 'Approved'  && QSRM.Vertical_Leader_approval_Status__c != 'Rejected' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected') { QSRM.Vertical_Leader_approval_Status__c = 'Approved'; QSRM.TS_Sales_Leader_Approval_Status__c='Approved';QSRM.Status__c='Approved';
        }
        
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.Vertical_Leader_approval_Status__c != 'Approved' && QSRM.Vertical_Leader_approval_Status__c != 'Rejected' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') { QSRM.Vertical_Leader_approval_Status__c = 'Approved'; QSRM.Service_Line_leader_SL_Approval__c='Approved';QSRM.Status__c='Approved';
        }
        
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected') { QSRM.TS_Sales_Leader_Approval_Status__c = 'Approved'; QSRM.Service_Line_leader_SL_Approval__c='Approved';QSRM.Status__c='Approved';
        }
        
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.Vertical_Leader_approval_Status__c != 'Approved' && QSRM.Vertical_Leader_approval_Status__c != 'Rejected'){ QSRM.Vertical_Leader_approval_Status__c = 'Approved';QSRM.Status__c='Approved';
                }
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Approved' && QSRM.TS_Sales_Leader_Approval_Status__c != 'Rejected'){ QSRM.TS_Sales_Leader_Approval_Status__c = 'Approved';QSRM.Status__c='Approved';
                }
        else if(QSRM.KP_Responce__c == 'Approve' && QSRM.Service_Line_leader_SL_Approval__c != 'Approved' && QSRM.Service_Line_leader_SL_Approval__c != 'Rejected'){ QSRM.Service_Line_leader_SL_Approval__c = 'Approved';QSRM.Status__c='Approved';
                }
        
    }
}