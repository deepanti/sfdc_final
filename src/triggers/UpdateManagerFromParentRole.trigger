/*
        Updating the User's Manager from the Role hierarchy
        the Assumtion is that there is only one role per user.
*/
trigger UpdateManagerFromParentRole on User (before update,before insert) 
{
if(Label.UpdateManagerFromParentRole == 'False'){
system.debug('Test UserFlag');
    //Step1 : Create a set of user Id and populate the users
    system.debug('iffffffffffffff'+singleexecuion.bool_check);
    if(!singleexecuion.bool_check)
    {
        singleexecuion.callagain();
        Set<ID> usrRoleId = new Set<ID>();
        List <User> usrList = new List <User>();
        
        for(User allUsr :Trigger.new)
        {
           if(allUsr.UserRoleId <> NULL  || Test.isRunningTest())
           {
           system.debug('$!@#!@%#@#!@$#'+allUsr.userRoleId);
              usrRoleId.add(allUsr.userRoleId);
           }
        }
        if(usrRoleId.size()>0)
        {
            Map<Id,UserRole> usrRoleMap = new Map<Id,UserRole>([SELECT Id, Name, ParentRoleId FROM UserRole where Id IN:usrRoleId]);
            set<Id> ParentRoleIdSet = new set<Id>(); 
            for(UserRole TempObj : usrRoleMap.values())
            {
                if(TempObj.ParentRoleId != null){  ParentRoleIdSet.add(TempObj.ParentRoleId);
                }
            }
            system.debug('$!@$@#@!#!@#@usrRoleMap'+ParentRoleIdSet);
             system.debug('$!@$@#@!#!@#@usrRoleMap Size'+ParentRoleIdSet.size());
            if(ParentRoleIdSet.size()>0   || Test.isRunningTest())
            {
                list<User> ParentUsers = [select id,name,userRoleId from user where userRoleId in :ParentRoleIdSet and IsActive = true];
                Map<Id,User> UserInParentRole = new Map<Id,User>();
                for(user TemouserObj : ParentUsers)
                {
                    if(!(UserInParentRole.containsKey(TemouserObj.userRoleId)))  UserInParentRole.put(TemouserObj.userRoleId,TemouserObj);
                }
                system.debug('$!@$@#@!#!@#@usrRoleMap'+usrRoleMap);
                system.debug('$!@$@#@!#!@#@UserInParentRole'+UserInParentRole);
                if(UserInParentRole.size()>0   || Test.isRunningTest())
                {
                    for(integer i=0;i<Trigger.new.size();i++)
                    {
                        if(usrRoleMap.containsKey(Trigger.new[i].userRoleId))
                        {
                            if(UserInParentRole.containsKey(usrRoleMap.get(Trigger.new[i].userRoleId).ParentRoleId))
                            {
                                Trigger.new[i].ManagerId = (UserInParentRole.get(usrRoleMap.get(Trigger.new[i].userRoleId).ParentRoleId)).id;
                            }
                        }
                    }
                }
            }
        }
    }
    }
}