@isTest
public class HomePageOpportunityInPipelineTest {
    @TestSetup
    //creating data
    public static void init()
    {
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'xyz@gmail.com', '12345678');
        insert con;
        List<Opportunity>oppDiscoverList=TestDataFactoryUtility.createTestOpportunitiesRecordsDiscover(acc.Id, 1,system.today(),con);
        insert oppDiscoverList;
        
    }
    @isTest
    //cheking Seller pipeline records 
    public static void getOpportunityPipelineTestSeller()
    {
        test.startTest();
        system.assertEquals(1, HomePageOpportunityInPipelineController.getOpportunityPipeline(true).size());
        test.stopTest();
    }
    @isTest
    //cheking Sales Leader pipeline records 
    public static void getOpportunityPipelineTestSalesLeader()
    {
        test.startTest();
        system.assertEquals(1, HomePageOpportunityInPipelineController.getOpportunityPipeline(false).size());
        test.stopTest();
    }
   
}