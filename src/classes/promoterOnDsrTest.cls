@istest
public class promoterOnDsrTest {
    public static testMethod void testController()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
		oAccount.Sales_Unit__c = salesunit.id;
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        oContact.title='Mr.';
        oContact.Promoter_info__c = TRUE;
        update oContact;
        Promoter_info__c promoterobject=new Promoter_info__c(Ability_to_Influence__c='yes',Can_Make_a_Reference__c='yes',Can_make_Public_Appearances__c='yes',Can_Make_External_Introduction__c='yes',Suitable_for_Analyst_Reference__c='yes',Origination__c='Sole source',
                                                             Commercial_Structure__c='Acquisition',Engagement_Model__c='Other',Offerings__c='Other',Transition_Model__c='Complex',Unique_Client_Geos_Services__c='Other',
                                                             Business_Drivers__c='Other',G_Service_Delivery_Locations__c='India',Lean_Digital__c='Systems of Engagement',Transformation__c='Other',
                                                             Personal_Agenda_Career_Aspirations__c='test',Professional_Agenda_Areas_of_Interest__c='test',Current_Disposition__c='NA',Topics_To_Avoid__c='test',
                                                             Last_updated_disposition_date__c=system.today(),Social_Style__c='Driving',Nurturer__c='test',Name_Promoter__c=oContact.id,Service_Line__c='HRO',Sub_Service_Line_short__c='test',
                                                             Can_intro_to_New_internal_buying_center__c='Yes',Unique_about_this_engagement__c='test',X2015_NPS_Disposition__c='Promoter');
        insert promoterobject;

        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Competitor__c='Accenture',Deal_Type__c='Competitive');
        insert opp;
      //  Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
      //  insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Collections',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;

        
        GW1_DSR__c dsr = new GW1_DSR__c(GW1_Account__c=oAccount.id,GW1_Opportunity__c=opp.id,GW1_Service_Line_SAVO__c = 'Core F&A',Product_Family__c = 'F&A',GW1_Named_Competitors__c= 'Accenture' );
        insert dsr;

        
        // start the test execution context
        Test.startTest();
        
        // set the test's page to your VF page (or pass in a PageReference)
        PageReference tpageRef = Page.promoter_on_dsr;
        
        // call the constructor
        ApexPages.StandardController std = new ApexPages.StandardController(dsr);
        PromoterOnDsr controller = new PromoterOnDsr(std);
        Test.setCurrentPage(tpageRef);
        controller.runQuery();
        controller.toggleSort();
        String soql=controller.debugSoql;
                
        // stop the test
        Test.stopTest();
    }
}