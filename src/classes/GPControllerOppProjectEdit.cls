public class GPControllerOppProjectEdit {
    private static final String SUCCESS_LABEL = 'SUCCESS';
    
    private Id oppProjectId;
    private GP_Opportunity_Project__c oppProject;
    
    private Set<String> setOfServiceLine = new Set<String>();
    private Map<String, Set<String>> mapOfServiceLineToNatureOfWork = new Map<String, Set<String>>();
     private Map<String, list<GPOptions>> mapOfNatureOfWorkToProduct     = new Map<String, list<GPOptions>>();
    private JSONGenerator gen = JSON.createGenerator(true);
    
    public GpControllerOppProjectEdit(Id oppProjectId) {
        this.oppProjectId = oppProjectId;
    }
    
     public GpControllerOppProjectEdit(GP_Opportunity_Project__c oppProject) {
        this.oppProject = oppProject;
    }
    
    public GPAuraResponse getOppProjectData() {
        try {
            setOppProjectRecord();
            setMapOfOptions();
            setJSON();
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }
    
    private void setOppProjectRecord() {
        oppProject = [SELECT Id, GP_Vertical__c,
                      GP_Service_Line__c, 
                      GP_Nature_of_Work__c, 
                      GP_Product_Id__c 
                      FROM GP_Opportunity_Project__c
                      WHERE Id = :oppProjectId];        
    }
    
    private void setMapOfOptions() {
        
        setOfServiceLine = new Set<String>();
        
        mapOfServiceLineToNatureOfWork = new Map<String, Set<String>>();
        mapOfNatureOfWorkToProduct     = new Map<String, list<GPOptions>>();
        
        for (GP_Product_Master__c productMaster: GPSelectorProductMaster.getAllProductMaster(oppProject.GP_Vertical__c)) {
            if(productMaster.GP_Service_Line__c == null) {
                continue;
            }
            
            setOfServiceLine.add(productMaster.GP_Service_Line__c);
            
            if(productMaster.GP_Service_Line__c != null) {
                //add map of service line to nature of work.
                if (!mapOfServiceLineToNatureOfWork.containsKey(productMaster.GP_Service_Line__c)) {
                    mapOfServiceLineToNatureOfWork.put(productMaster.GP_Service_Line__c, new Set<String>());
                }
                
                mapOfServiceLineToNatureOfWork.get(productMaster.GP_Service_Line__c)
                    .add(productMaster.GP_Nature_of_Work__c);
                
            }
            
            if(productMaster.GP_Nature_of_Work__c != null) {
                //add map of nature of work to product family
                if (!mapOfNatureOfWorkToProduct.containsKey(productMaster.GP_Nature_of_Work__c)) {
                    mapOfNatureOfWorkToProduct.put(productMaster.GP_Nature_of_Work__c, new list<GPOptions>());
                }
                
                mapOfNatureOfWorkToProduct.get(productMaster.GP_Nature_of_Work__c)
                    .add( new GPOptions(productMaster.GP_Product_ID__c,productMaster.Name ));
            }
        }
    }
    
    private void setJSON() {
        
        gen.writeStartObject();
        if (setOfServiceLine != null) {
            gen.writeObjectField('setOfServiceLine', setOfServiceLine);
        }
        
        if (mapOfServiceLineToNatureOfWork != null) {
            gen.writeObjectField('mapOfServiceLineToNatureOfWork', mapOfServiceLineToNatureOfWork);
        }
        
        if (mapOfNatureOfWorkToProduct != null) {
            gen.writeObjectField('mapOfNatureOfWorkToProduct', mapOfNatureOfWorkToProduct);
        }
        
        if(oppProject != null) {
            gen.writeObjectField('oppProject', oppProject);
        }
        gen.writeEndObject();
    }
    
    
    public GPAuraResponse saveOppProjectData() {
        try {
            update oppProject;
            
            system.debug('oppProjectId::'+oppProjectId);
            list<GP_Project_Classification__c> lstProjClassification = [select GP_ATTRIBUTE1__c,id,GP_Opp_Project__c ,GP_EP_Project_Number__c,GP_PINNACLE_PUSH_DATE__c from 	
        																GP_Project_Classification__c where 
        																GP_Opp_Project__c = : oppProject.id and GP_CLASS_CODE__c ='Product Family' limit 1];
        	if(lstProjClassification != null && lstProjClassification.size()>0)
        	{
        		system.debug('lstProjClassification::'+lstProjClassification);
        		lstProjClassification[0].GP_ATTRIBUTE1__c = oppProject.GP_Product_Id__c;
        		GPCommon.isOpportunityProjectInsertionEvent = false;
        		update lstProjClassification; 
        	}
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);            
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }
}