@isTest
public class GPServiceProjectWorkLocationSDOTracker {
    
    static List<GP_Project_Work_Location_SDO__c> listOfProjectWorkLocation;
    static GP_Work_Location__c workLocation;
    static GP_Project__c project;
    
    @testSetup
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();        
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        GP_Address__c billToAddress =  new GP_Address__c(GP_Country__c = 'India');
        insert billToAddress;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Bill_to_Address__c = billToAddress.Id;
        insert parentProject;
        
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Bill_to_Address__c = billToAddress.Id;
        project.GP_Project_Currency__c = 'INR';
        project.GP_Oracle_PID__c = 'NA';
        
        insert project ;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        insert objPrjBillingMilestone;
        
    }
    
    @isTest 
    public static void testGPServiceProjectWorkLocation() {
        fetchData();
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = true;
        
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = true;
        
        
        GP_Project_Work_Location_SDO__c projectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(project.Id, workLocation.Id);
        projectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        insert projectWorkLocation;
        
        
        GP_Project_Work_Location_SDO__c projectSDO = GPCommonTracker.getProjectWorkLocationSDO(project.Id, workLocation.Id);
        projectSDO.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO');
        insert projectSDO;
        
        listOfProjectWorkLocation = new List<GP_Project_Work_Location_SDO__c>();
        projectWorkLocation.GP_Work_Location__r = new GP_Work_Location__c();
        projectWorkLocation.GP_Work_Location__c = null;
        projectWorkLocation.RecordType = new RecordType(); 
        projectWorkLocation.RecordType.Name = 'Work Location';
        
        
        listOfProjectWorkLocation.add(projectWorkLocation);
        listOfProjectWorkLocation.add(projectSDO);
        
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        
        project.RecordType.Name = 'BPM';
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = true;
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = true;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        
        
        projectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(project.Id, null);    
        projectWorkLocation.Id = null;
        projectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        projectWorkLocation.GP_Work_Location__r = new GP_Work_Location__c();
        projectWorkLocation.RecordType = new RecordType(); 
        projectWorkLocation.RecordType.Name = 'Work Location';
        projectWorkLocation.GP_Primary__c = true;
        projectWorkLocation.GP_Work_Location__r.GP_Start_Date_Active__c = System.today().addDays(-111);
        projectWorkLocation.GP_Work_Location__c = null;
        listOfProjectWorkLocation.add(projectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        
        
        GP_Project_Work_Location_SDO__c invalidEndDateProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(project.Id, null);    
        invalidEndDateProjectWorkLocation.Id = null;
        invalidEndDateProjectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        invalidEndDateProjectWorkLocation.GP_Work_Location__r = new GP_Work_Location__c();
        invalidEndDateProjectWorkLocation.RecordType = new RecordType(); 
        invalidEndDateProjectWorkLocation.RecordType.Name = 'Work Location';
        invalidEndDateProjectWorkLocation.GP_Primary__c = true;
        invalidEndDateProjectWorkLocation.GP_Work_Location__r.GP_End_Date_Active__c = System.today().addDays(-10);
        invalidEndDateProjectWorkLocation.GP_Work_Location__r.GP_Country__c = 'Pakistan';
        listOfProjectWorkLocation.add(projectWorkLocation);
        
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
    }
    @isTest 
    public static void testGPServiceProjectWorkLocatione() {
        
        //New Method        
        fetchData();
        
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = true;        
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = true;        
        
        GP_Project_Work_Location_SDO__c projectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(project.Id, workLocation.Id);
        projectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        insert projectWorkLocation;        
        
        GP_Project_Work_Location_SDO__c projectSDO = GPCommonTracker.getProjectWorkLocationSDO(project.Id, workLocation.Id);
        projectSDO.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO');
        insert projectSDO;
        
        listOfProjectWorkLocation = new List<GP_Project_Work_Location_SDO__c>();
        projectWorkLocation.GP_Work_Location__r = new GP_Work_Location__c();
        projectWorkLocation.GP_Work_Location__c = null;
        projectWorkLocation.RecordType = new RecordType(); 
        projectWorkLocation.RecordType.Name = 'Work Location';        
        
        listOfProjectWorkLocation.add(projectWorkLocation);
        listOfProjectWorkLocation.add(projectSDO);
        
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        
        project.RecordType.Name = 'BPM';
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = true;
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = true;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        
        GPServiceProjectWorkLocationSDO.isFormattedLogRequired = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        GPServiceProjectWorkLocationSDO.isLogForTemporaryId = false;
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, null);
        //-------------
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        //-------------------------------
        projectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(project.Id, null);    
        projectWorkLocation.Id = null;
        projectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        projectWorkLocation.GP_Work_Location__r = new GP_Work_Location__c();
        projectWorkLocation.RecordType = new RecordType(); 
        projectWorkLocation.RecordType.Name = 'Work Location';
        projectWorkLocation.GP_Primary__c = true;
        projectWorkLocation.GP_Work_Location__r.GP_Start_Date_Active__c = System.today().addDays(-111);
        projectWorkLocation.GP_Work_Location__r.GP_Status__c = 'Inactive';
        projectWorkLocation.GP_Work_Location__c = sdo.id;
        projectWorkLocation.GP_is_Active__c=true;
        projectWorkLocation.GP_Work_Location__r.GP_Hide_For_BPM__c=true;
        projectWorkLocation.GP_Work_Location__r.GP_IsSEZ__c='true';
        listOfProjectWorkLocation.add(projectWorkLocation);
        GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(project, listOfProjectWorkLocation);
    }
    
    static void fetchData() {
    	// ECR-HSN Changes
    	// Added GP_Oracle_PID__c and GP_Parent_Project__c
        project = [select Id, Name, 
                   GP_Deal_Category__c,
                   RecordType.Name,
                   GP_Oracle_PID__c,
                   GP_Parent_Project__c,
                   GP_Start_Date__c, 
                   GP_End_Date__c,
                   GP_Bill_To_Country__c,
                   GP_Project_Template__c, 
                   GP_Primary_SDO__c, 
                   GP_Project_Stages_for_Approver__c, 
                   GP_Approval_Status__c, 
                   GP_GPM_Start_Date__c,
                   GP_Last_Temporary_Record_Id__c,GP_Project_Currency__c
                   from GP_Project__c 
                   limit 1]; 
        
        workLocation = [Select Id from GP_Work_Location__c LIMIT 1];
    }
}