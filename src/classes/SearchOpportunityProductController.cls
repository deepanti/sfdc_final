public class SearchOpportunityProductController 
{   
    @auraEnabled
    public static ProductListWrapper getProducts(ID opportunityID)
    {
        try
        {
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            if(opportunity_list.size() > 0)
            {  
                 Map<ID, Product2> product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical AND Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);              
              
                List<Product2> product_list = getProductsHavingPriceBookEntry(product_map);
                product_list.sort();
                ProductListWrapper wrapper = new ProductListWrapper();                
                
                wrapper.total = product_list.size();
                system.debug('wrapper.total=='+wrapper.total);
                wrapper.productList = product_list; 
                return wrapper;
            }
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
        }
        return NULL;
    }  
        
    @auraEnabled
    public static ProductListWrapper getProductsOnChange(ID opportunityID, String SelectedServiceLine, String SelectedNatureOfWork, String SelectedProductName)
    {
        try
        {
          List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            
           Map<ID, Product2> product_map;
           if(opportunity_list.size() > 0)
            {  
                
                if(SelectedServiceLine != '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName == '--None--')
                {
                    System.debug('only service line having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName == '--None--')
                {
                    System.debug('only Nature of Work having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName != '--None--')
                {
                    System.debug('only product name having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Name=:SelectedProductName  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName == '--None--')
                {
                    System.debug('when  service line , nature of work having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine and Nature_of_Work__c=:SelectedNatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName != '--None--')
                {
                    System.debug('when  service line , product name having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine and name=:SelectedProductName and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName != '--None--')
                {
                    System.debug('when  Nature of work , product name having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork and name=:SelectedProductName and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName != '--None--'){
                    System.debug('when  service Line, Nature of work , product name having====');
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork and name=:SelectedProductName and Service_Line__c=:SelectedServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                
                List<Product2> product_list = getProductsHavingPriceBookEntry(product_map);
                
                ProductListWrapper wrapper = new ProductListWrapper();                
                
                wrapper.total = product_list.size();
                wrapper.productList = product_list; 
                return wrapper;
            }
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
        }
        return NULL;
    } 
       
    private static List<Product2> getProductsHavingPriceBookEntry(Map<ID, Product2>product_map)
    {
        try
        {
            List<Product2> product_list = new List<Product2>();
            List<PricebookEntry> priceBookEntryList = [SELECT Id, Product2Id FROM PricebookEntry 
                                                       Where Product2ID =:product_map.keySet()];
            for(PricebookEntry priceEntry : priceBookEntryList)
            {
              if(product_map.containsKey(priceEntry.Product2ID))
              {
                product_list.add(product_map.get(priceEntry.Product2ID));    
              }      
            }
            system.debug('product_list=='+product_list);
            return product_list;       
        }
        catch(Exception e)
        {
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }

    @auraEnabled 
    public static ProductListWrapper getSearchedProduct(ID opportunityID, String ServiceLine, String NatureOfWork, String ProductName)
    {        
        try
        {
            
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            List<Product2> product_list = new List<Product2>();
            List<Product2> proList;            
          
            if(opportunity_list.size() > 0)
            {
                if(ServiceLine != '--None--' && NatureOfWork != '--None--' && ProductName != '--None--')
                {
                    system.debug('=======when ServiceLine, NatureOfWork, ProductName having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                }
                else if(ServiceLine != '--None--' && NatureOfWork == '--None--' && ProductName == '--None--')
                {
                    system.debug('=======when only ServiceLine having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                }
                else if(ServiceLine == '--None--' && NatureOfWork != '--None--' && ProductName == '--None--')
                {
                    system.debug('=======when only NatureOfWork having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                }
                else if(ServiceLine == '--None--' && NatureOfWork == '--None--' && ProductName != '--None--')
                {
                    system.debug('=======when only ProductName having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                }
                else if(ServiceLine != '--None--' && NatureOfWork != '--None--' && ProductName == '--None--')
                {
                    system.debug('=======when ServiceLine, NatureOfWork having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                }
                else if(ServiceLine != '--None--' && NatureOfWork == '--None--' && ProductName != '--None--')
                {
                    system.debug('=======when ServiceLine, ProductName having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                }
                if(ServiceLine == '--None--' && NatureOfWork != '--None--' && ProductName != '--None--')
                {
                    system.debug('=======when  NatureOfWork, ProductName having values========');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                }
                      
               	product_list = getProductsHavingPriceBookEntry(new Map<ID, Product2>(proList)); 
                ProductListWrapper wrapper = new ProductListWrapper();
                wrapper.total = product_list.size();
                wrapper.productList = product_list;
                return wrapper;
            }    
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
        
    }
    
  @auraEnabled
    public static DateWrapper getOpportunityClosedDate (String opportunityID)
    {
        try
        {
            DateWrapper wrapper = new DateWrapper();
            system.debug('recordId=='+opportunityID);
            List<Opportunity> opportunity_list = [SELECT CloseDate FROM Opportunity WHERE ID =:opportunityID];
            if(opportunity_list.size() > 0){
                wrapper.closedDate = opportunity_list[0].CloseDate;
            }
            User loggedInUser = [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            System.debug('loggedInUser=='+loggedInUser);
            wrapper.loggedInUser = loggedInUser;
                return wrapper;   
        } 
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }   
 
    public class DateWrapper
    {
        @auraenabled
        public Date closedDate;
        @auraEnabled
        public User loggedInUser;
     }
}