public class InsideSalesAssignCampaign {
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for fetching campaign
* @param   campaignId - String storing campaignId
* @return  campaign data
**************************************************************************************************************************/
    @AuraEnabled    
    public static Campaign fetchCampaign(String campaignId){        
        List<Campaign> campaigns= [SELECT id,Name,Type,NumberOfLeads,Status,Channel__c,NumberOfContacts,AccountCount__c
                                   FROM campaign 
                                   WHERE id=: campaignId LIMIT 1];         
        if(campaigns.size() > 0) {
            return campaigns[0];           
        }
        else 
            return null;
    }
    
    
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Wrapper based on Account of Campaign Member 
**************************************************************************************************************************/
    public class AccountWrapper{        
        @AuraEnabled  public Boolean isSelected;
        @AuraEnabled  public String wrapperId;
        @AuraEnabled  public List<CampaignMember> campMembersIds;
        @AuraEnabled  public String assignedTo;
        @AuraEnabled  public Id assignedToId;
        @AuraEnabled  public String companyName;
        @AuraEnabled  public Id accountId;
        @AuraEnabled  public Integer memberCount;
        @AuraEnabled  public String memberType;        
    }
    
    
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for selecting and assigning campaign members to caller
* @param   CampaignId - Campaign Id
* @return  List<AccountWrapper>
**************************************************************************************************************************/  
    @AuraEnabled
    
    public static List<AccountWrapper> getCampaignMembers(String campaignId) {
        try{
            List<AccountWrapper> campMemberWrapList = new List<AccountWrapper>();
            AccountWrapper wrapper;
            List<CampaignMember> campaignMembersList = new List<CampaignMember>();
            
            campaignMembersList = [SELECT Id,CampaignId,LeadId,Lead.Account__c,Lead.Account__r.Name,Lead.Company,
                                   ContactId,Contact.AccountId,Contact.Account.Name,Campaign.Name, Type,Assign_Name__r.Name                         
                                   FROM CampaignMember
                                   WHERE  CampaignId=: campaignId LIMIT:limits.getLimitQueryRows()];
            
            //map of comapnyName, Type(Contact/Lead) and count of members
            Map<String,Map<String,List<CampaignMember>>> companyCountMapDetail = new Map<String,Map<String,List<CampaignMember>>>();
            
            
            Map<String,List<CampaignMember>> companyCountMapRecord;
            List<CampaignMember> campMembers;
            Integer count;
            String companyName;
            if(!campaignMembersList.isEmpty()){
                for(CampaignMember campMem: campaignMembersList){ 
                    companyCountMapRecord = new Map<String,List<CampaignMember>>();
                    campMembers = new List<CampaignMember>();
                    if(campMem.Type == 'Contact')
                    {
                        if(companyCountMapDetail.containsKey(campMem.Contact.Account.Name)){
                            companyCountMapRecord = companyCountMapDetail.get(campMem.Contact.Account.Name);
                            if(companyCountMapRecord != null && companyCountMapRecord.containsKey(campMem.Type))
                            {
                                campMembers = companyCountMapRecord.get(campMem.Type);
                                campMembers.add(campMem);
                                companyCountMapRecord.put(campMem.Type,campMembers);
                                companyCountMapDetail.put(campMem.Contact.Account.Name,companyCountMapRecord);
                            }
                            else{
                                companyCountMapRecord.put('Contact',new List<CampaignMember>{campMem});
                                companyCountMapDetail.put(campMem.Contact.Account.Name,companyCountMapRecord);
                            }
                            
                        }
                        else{
                            companyCountMapRecord.put('Contact',new List<CampaignMember>{campMem});
                            companyCountMapDetail.put(campMem.Contact.Account.Name,companyCountMapRecord);
                        }
                    }
                    else if(campMem.Type == 'Lead')
                    {
                        if(campMem.Lead.Account__c!=null)
                            companyName = campMem.Lead.Account__r.Name;
                        else
                            companyName = campMem.Lead.Company;  
                        if(companyCountMapDetail.containsKey(companyName)){
                            companyCountMapRecord = companyCountMapDetail.get(companyName);
                            if(companyCountMapRecord != null && companyCountMapRecord.containsKey(campMem.Type))
                            {
                                campMembers = companyCountMapRecord.get(campMem.Type);
                                campMembers.add(campMem);
                                companyCountMapRecord.put(campMem.Type,campMembers);
                                companyCountMapDetail.put(companyName,companyCountMapRecord);
                            }
                            else{
                                companyCountMapRecord.put('Lead',new List<CampaignMember>{campMem});
                                companyCountMapDetail.put(companyName,companyCountMapRecord);
                            }
                        }
                        else{
                            companyCountMapRecord.put('Lead',new List<CampaignMember>{campMem});
                            companyCountMapDetail.put(companyName,companyCountMapRecord);
                        }
                    }
                }
            } 
            
            //to display wrapper
            if(companyCountMapDetail.keySet().size() > 0)
            {
                for(String companysName: companyCountMapDetail.keySet())
                {
                    if(companyCountMapDetail.get(companysName).containsKey('Contact'))
                    { 
                        wrapper =  new AccountWrapper();
                        wrapper.wrapperId =companyCountMapDetail.get(companysName)+'Contact';
                        wrapper.companyName  = companyCountMapDetail.get(companysName).get('Contact')[0].Contact.Account.Name;
                        wrapper.accountId = companyCountMapDetail.get(companysName).get('Contact')[0].Contact.AccountId;
                        wrapper.memberCount =  companyCountMapDetail.get(companysName).get('Contact').size();
                        wrapper.memberType = 'Contact';
                        wrapper.isSelected = false;
                        wrapper.campMembersIds = companyCountMapDetail.get(companysName).get('Contact');
                        wrapper.assignedTo = companyCountMapDetail.get(companysName).get('Contact')[0].Assign_Name__r.Name;
                        campMemberWrapList.add(wrapper);
                        
                    }
                    if(companyCountMapDetail.get(companysName).containsKey('Lead'))
                    { 
                        wrapper =  new AccountWrapper();
                        wrapper.wrapperId =companyCountMapDetail.get(companysName)+'Lead';
                        if(companyCountMapDetail.get(companysName).get('Lead')[0].Lead.Account__c == null){
                            wrapper.companyName  = companyCountMapDetail.get(companysName).get('Lead')[0].Lead.Company;
                            wrapper.accountId = null;
                        }
                        else{
                            wrapper.companyName  = companyCountMapDetail.get(companysName).get('Lead')[0].Lead.Account__r.Name;
                            wrapper.accountId = companyCountMapDetail.get(companysName).get('Lead')[0].Lead.Account__c;
                        }                        
                        wrapper.memberCount =  companyCountMapDetail.get(companysName).get('Lead').size();
                        wrapper.memberType = 'Lead';
                        wrapper.isSelected = false;
                        wrapper.campMembersIds = companyCountMapDetail.get(companysName).get('Lead');
                        wrapper.assignedTo = companyCountMapDetail.get(companysName).get('Lead')[0].Assign_Name__r.Name;
                        campMemberWrapList.add(wrapper);
                        
                    }
                    
                }
            }
            return campMemberWrapList;
            
        }
        catch(Exception ex){
            system.debug(ex.getMessage()+ ' Trace===>' + ex.getCause()+ ' LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());   
        }
    }
    
    
    
    
    
/************************************************************************************************************************
* @author  Persistent
* @description  This method will save caller to list of campaign members
* @Param   List<AccountWrapper> AccountWrapper - wrapperList
* @return  void
*************************************************************************************************************************/  
    @AuraEnabled
    public static void saveCallersToCampaignMembers(String wrapperListStr){
        List<AccountWrapper> wrapperList = (List<AccountWrapper>)JSON.deserialize(wrapperListStr,List<AccountWrapper>.class);
        Campaign camp = [select id,is_Being_Processed__c from campaign where id=:wrapperList[0].campMembersIds[0].CampaignId];
        camp.is_Being_Processed__c= true;
        update camp;
        UpdateCampaignMembersAsyn(wrapperListStr);
    }
    
    /************************************************************************************************************************
* @author  Persistent
* @description  This method will save caller to list of campaign members async
* @Param   List<AccountWrapper> AccountWrapper - wrapperList
* @return  void
*************************************************************************************************************************/  
    @future
    private static void UpdateCampaignMembersAsyn(String wrapperListStr)
    {
        try{
            List<SObject> recordsToUpdate = new List<Sobject>();
        	List<AccountWrapper> wrapperList = (List<AccountWrapper>)JSON.deserialize(wrapperListStr,List<AccountWrapper>.class);
            List<CampaignMember> campaignMembers = new List<CampaignMember>();
            Set<String> AssignedToSet = new Set<String>();
            Integer countLeadInPlay = 0;
            Integer countConInPlay = 0;
            List<Contact> contactsToUpdate = new List<Contact>();
            List<Lead> leadsToUpdate = new List<Lead>();
            
            String campID;
            for(AccountWrapper wrapperInst : wrapperList){
                if(wrapperInst.assignedToId!=null){
                    for(CampaignMember campaignMem : wrapperInst.campMembersIds)
                    {
                        campaignMem.Assign_Name__c = wrapperInst.assignedToId;
                        AssignedToSet.add(wrapperInst.assignedTo);
                    }
                    campaignMembers.addAll(wrapperInst.campMembersIds);
                }
                for(CampaignMember campaignMem :wrapperInst.campMembersIds){
                    if(campaignMem.Type =='Lead' && campaignMem.Assign_Name__c!=null){
                        countLeadInPlay++;
                        leadsToUpdate.add(new Lead(Id=campaignMem.leadId,Recent_Campaign_ID__c=campaignMem.Campaign.id,Recent_Channel_Detail__c=campaignMem.Campaign.Name,Tele_Caller__c=campaignMem.Assign_Name__c));
                    }
                    if(campaignMem.Type =='Contact' && campaignMem.Assign_Name__c!=null){
                        countConInPlay++;
                        contactsToUpdate.add(new Contact(Id=campaignMem.ContactId,Recent_Campaign_ID__c=campaignMem.Campaign.id,Recent_Channel_Detail__c=campaignMem.Campaign.Name,Tele_Caller__c=campaignMem.Assign_Name__c));
                    }
                    campID = campaignMem.CampaignId;
                }
                
            }
            Campaign campaign = [Select Id,LeadContactNumberInPlay__c,ContactsCountInPlay__c from Campaign where Id=: campID LIMIT 1]; 
            campaign.LeadContactNumberInPlay__c = countLeadInPlay;
            campaign.ContactsCountInPlay__c = countConInPlay;
            campaign.AssignedTo__c ='';
        	campaign.is_Being_Processed__c= false;
            campaign.Assigned_date__c =  system.now();
            for(String callerName : AssignedToSet)
                campaign.AssignedTo__c = campaign.AssignedTo__c + ', ' + callerName;
            campaign.AssignedTo__c = campaign.AssignedTo__c.replaceFirst(',','');
            campaign.AssignedTo__c = campaign.AssignedTo__c.trim();
            recordsToUpdate.addAll(campaignMembers);
            recordsToUpdate.add(campaign);
            update recordsToUpdate;
            recordsToUpdate = new List<Sobject>();
            recordsToUpdate.addAll(leadsToUpdate);
            recordsToUpdate.addAll(contactsToUpdate);
            update recordsToUpdate;
    }catch(Exception ex){
            system.debug(ex.getMessage()+ ' Trace===>' + ex.getCause()+ ' LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());   
        }
}
        
        
    /************************************************************************************************************************
* @author  Persistent
* @description  This method will get Callers to Skip In Play list view
* @return  List of User IDs
*************************************************************************************************************************/  
    @AuraEnabled
    public static List<Id> getCallersToSkipInPlay()
    {
        List<Id> UserIdLIst = new List<Id>();
        for(Campaign camp : [select id,(select Assign_Name__c from campaignMembers) from Campaign where is_in_play__c =True and Status='Launched'])
        {
            for(CampaignMember cm : camp.CampaignMembers)
            {
                UserIdList.add(cm.Assign_Name__c);
            }
        }
        return userIdList;
    }
}