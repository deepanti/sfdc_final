/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/28/2018
@Description :this test class is used to cover all code for MarkFieldUpContact trigger

@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */
@isTest
public class MarkFieldUpContactTest {
    
      
    public static TestMethod void contactBeforeInsert()
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount;
        Contact oldContact;
        System.runAs(u){
            oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                        oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            oldContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                          'test121@xyz.com','99999999999');
            
        }
        List<Contact> con=new List<contact>();
        con.add(oldContact);
        new ContactTriggerHandler().AccToConFieldUpdate(con);
    }
    
    public static testMethod void beforeUpdateContact()
        
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount;
        Account upAcc;
        Contact oContact;
        final Contact oldCon=new Contact();
        System.runAs(u){
            oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                        oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                        'test121@xyz.com','99999999999');
            upAcc = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                     oBS.Id,oSB.Id,'BFS','Aerospace & Defense','Mining Priority','12','12345676'); 
        }
        oldcon.Id=oContact.Id;
         Map<Id,Contact> mpCon=new Map<Id,Contact>();
        mpCon.put(oldCon.Id,oldCon);
        oContact.AccountId=upAcc.Id;
        update oContact;
        List<Contact> con=new List<contact>();
        con.add(oContact);
        new ContactTriggerHandler().AccToConFieldUpdate(con,mpCon);
   
    }
    
    public static testmethod void testException()
    {
        List<Contact> con;
        Map<Id,Contact> mp;
        try{
        new ContactTriggerHandler().AccToConFieldUpdate(con);
        new ContactTriggerHandler().AccToConFieldUpdate(con,mp);
        }
        catch(Exception e)
        {
            system.debug(e);
        }
    }
}