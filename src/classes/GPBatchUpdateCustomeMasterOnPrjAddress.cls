global class GPBatchUpdateCustomeMasterOnPrjAddress implements Database.Batchable<sObject> {
    //Query all address record where customer is null
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, GP_Customer_Account_ID__c, GP_Customer__c 
                                         FROM GP_Address__c
                                         WHERE GP_Customer__c = null]);
    }
    //fetch the customer master record with oracle id equal to address custome oracle id 
    //and update the customer sfdc id on address.
    global void execute(Database.BatchableContext bc, List<GP_Address__c> listOfAddress) {
        savepoint sp = database.setSavepoint();
        try {
            set<string> setOfCustomerOracleIdId = new set<string> ();
            map<string, Id> mapOfCustomerOracleIdToCustomerId = new map<string, Id>();
            
            for (GP_Address__c address: listOfAddress) {
                setOfCustomerOracleIdId.add(address.GP_Customer_Account_ID__c);
            }
            
            for(GP_Customer_Master__c customerMaster : [Select Id, GP_Oracle_Customer_Id__c 
                                                        from GP_Customer_Master__c 
                                                        where GP_Oracle_Customer_Id__c in :setOfCustomerOracleIdId]) {
                mapOfCustomerOracleIdToCustomerId.put(customerMaster.GP_Oracle_Customer_Id__c, customerMaster.Id);
            }
            
            for (GP_Address__c address: listOfAddress) {
                address.GP_Customer__c = mapOfCustomerOracleIdToCustomerId.get(address.GP_Customer_Account_ID__c);
            }
            
            update listOfAddress;
        } catch (exception ex) {
            database.rollback(sp);
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
}