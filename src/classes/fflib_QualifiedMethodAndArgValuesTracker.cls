@isTest
public class fflib_QualifiedMethodAndArgValuesTracker{
    @isTest static void testMethodAndArgValues(){
        fflib_QualifiedMethod qm1 = new fflib_QualifiedMethod('Type1', 'Method1', new List<Type>{ Integer.class } );
        fflib_MethodArgValues argValues = new fflib_MethodArgValues(new List<Object>{ null });
        fflib_ArgumentCaptor argument = fflib_ArgumentCaptor.forClass(fflib_MethodArgValues.class);
        Object capturedArg = argument.getValue();
        fflib_QualifiedMethodAndArgValues objQFMAV = new fflib_QualifiedMethodAndArgValues(qm1, argValues, capturedArg );
        objQFMAV.getQualifiedMethod();
        objQFMAV.getMethodArgValues();
        objQFMAV.getMockInstance();
        objQFMAV.toString();
    }
}