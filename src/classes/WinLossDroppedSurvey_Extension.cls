public class WinLossDroppedSurvey_Extension
{
    
    public ID opportunity_ID{get;set;}
    public string contract_ID{get;set;} 
    public string oppStageName {get;set;}
    public WinLossDroppedSurvey_Extension(ApexPages.StandardController controller)
    {
                
        If (ApexPages.currentPage().getParameters().get('oppid') != NULL) 
        {
            opportunity_ID = ApexPages.currentPage().getParameters().get('oppid');
        }
         Else
        {
            opportunity_ID  = controller.getRecord().Id;
        }
        list<opportunity> opp = [select stageName from opportunity where id =: opportunity_ID limit 1];
        
          oppStageName = opp[0].StageName;
               
        If (ApexPages.currentPage().getParameters().get('conid') != NULL) 
        {
            contract_ID = ApexPages.currentPage().getParameters().get('conid');
        }
        else
        {
            contract_ID='dummy123';
        }
    }
}