@isTest(seeAllData=false)
private class GPScheduleSendNotificationOnPIDTracker
{
    @testSetup
    static void testData() {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'GP_Project__Delete__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c prjObj1 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj1.OwnerId = objuser.Id;
        insert prjObj1;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Parent_Project__c = prjObj1.id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Oracle_Status__c = 'NA';
        prjObj.GP_is_Send_Notification__c=true;
        insert prjObj;
        
        GP_Resource_Allocation__c prjResource=GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert prjResource;
    }
    
    @isTest
    static void schedulerTest() {
        String cronExp = '0 0 0 15 3 ? *';        
        
        Test.startTest();
        
        String jobId = System.schedule('GPScheduleSendNotificationOnPIDTracker', cronExp, new GPScheduleSendNotificationOnPIDSync());
        
        System.assert(String.isNotBlank(jobId), 'Schedule job is not executed.');
        
        Test.stopTest();
    }
}