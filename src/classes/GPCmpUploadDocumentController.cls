/**
* @group Upload Document(Base)
* @group-content ../../ApexDocContent/UplaodDocument.htm
*
* @description Apex service class having Aura enabled methods
*              to Upload Dcument in Salesforce.
*/
public with sharing class GPCmpUploadDocumentController {
    
    /**
    * @description Save the file Uploaded.
    * @param parentId Related record Id.
    * @param fileName File Name.
    * @param base64Data Content of the document.
    * @param contentType Content type of the file uploaded.
    * @param fileId used to append data in case of chunk save.
    * 
    * @return Id SFDC Id of the file uploaded.
    * 
    * @example
    * GPCmpUploadDocumentController.saveChunk(<parentId>, <fileName>, <base64Data>, <contentType>, <fileId>);
    */
    @AuraEnabled
    public static Id saveChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
        // check if fileId id ''(Always blank in first chunk), then call the saveTheFile method,
        // which is save the check data and return the attachemnt Id after insert, 
        // next time (in else) we are call the appentTOFile() method
        // for update the attachment with reamins chunks  
        if (fileId == '') {
            fileId = saveTheFile(parentId, fileName, base64Data, contentType);
        } else {
            appendToFile(fileId, base64Data);
        }
        
        if(fileId != null)
        	return Id.valueOf(fileId);
        
        return null;
    }
    
    /**
    * @description Save the file Uploaded.
    * @param projectId Related Project record Id.
    * @param fileName File Name.
    * @param base64Data Content of the document.
    * @param contentType Content type of the file uploaded.
    * 
    * @return Id SFDC Id of the file uploaded.
    *
    * @example
    * GPCmpUploadDocumentController.saveTheFile(<parentId>, <fileName>, <base64Data>, <contentType>); 
    */
    public static Id saveTheFile(Id projectId, String fileName, String base64Data, String contentType) {
        if(base64Data != null && base64Data != ''){
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
            
            ContentVersion objContentVersion = new ContentVersion();
            
            objContentVersion.versionData = EncodingUtil.base64Decode(base64Data);
            objContentVersion.title = fileName;
            objContentVersion.PathOnClient = '/' + fileName;  
            objContentVersion.origin = 'H';
            
            insert objContentVersion;
            
            Id projectDocumentID = insertProjectDocumentRecord (projectId,fileName,contentType,objContentVersion.id);
            
            ContentDocumentLink cdl = new ContentDocumentLink();
            
            cdl.ContentDocumentId = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id =: objContentVersion.Id].ContentDocumentId;
            cdl.LinkedEntityId = projectDocumentID;
            cdl.Visibility = 'AllUsers';
            cdl.ShareType = 'V';
            
            insert cdl;
            
            return objContentVersion.id;
        }
        return null;
    }
    
    /**
    * @description Append to the file Uploaded.
    * @param fileId Related File record Id.
    * @param base64Data Content of the document.
    *
    * @example
    * GPCmpUploadDocumentController.appendToFile(<fileId>, <base64Data>); 
    */
    public static void appendToFile(Id fileId, String base64Data) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        
        ContentVersion objcontentVersion = [
            SELECT Id, versionData
            FROM ContentVersion
            WHERE Id =: fileId
        ];
        
        String existingBody = EncodingUtil.base64Encode(objcontentVersion.versionData);
        
        objcontentVersion.versionData = EncodingUtil.base64Decode(existingBody + base64Data);
        
        update objcontentVersion;
    }

    /**
    * @description Insert Project Document Record.
    * @param projectId Related Project record Id.
    * @param fileName Name of the file uploaded.
    * @param contentType Content of the document.
    * @param contentVersionId Content Version reocrd Id of the document.
    *
    * @return Id SFDC Id of the file uploaded.
    *
    * @example
    * GPCmpUploadDocumentController.insertProjectDocumentRecord(<projectId>, <fileName>, <contentType>, <contentVersionId>); 
    */
    public static Id insertProjectDocumentRecord (Id projectId, String fileName, String contentType, Id contentVersionId) {

       	String baseURL = System.Label.GP_File_Download_Base_URL; 

        GP_Project_Document__c objProjectDocument = new GP_Project_Document__c();
        objProjectDocument.name = fileName;
        objProjectDocument.GP_Project__c = projectId;
        objProjectDocument.GP_Type__c = contentType;
        objProjectDocument.GP_Source__c='Salesforce';
        objProjectDocument.GP_Document_URL__c = baseURL +contentVersionId + +'?asPdf=false';
        objProjectDocument.GP_Content_Version_Id__c = contentVersionId;

        insert objProjectDocument;
        return objProjectDocument.id;
    }
}