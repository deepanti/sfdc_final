@isTest
public class GPServiceUserTracker {
	@isTest
    public static void testGPServiceUser() {
        GP_Role__c r = new GP_Role__c();
        r.GP_Active__c = true;
        insert r;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@genpact.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u;
        
        
        GP_User_Role__c userRole = new GP_User_Role__c();
        userRole.GP_User__c = u.Id;
        userRole.GP_Role__c = r.Id;
        userRole.GP_Active__c = true;
        insert userRole;
        
        GPServiceUser userService = new GPServiceUser();
        GPServiceUser.DeactivateUserRole(new Set<Id> {u.Id});
        GPServiceUser.GPServiceUserException userServiceException = new GPServiceUser.GPServiceUserException();
    }
}