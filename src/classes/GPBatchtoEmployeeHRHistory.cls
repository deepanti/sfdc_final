global class GPBatchtoEmployeeHRHistory implements Database.Batchable<sObject>, Database.Stateful
    {
        map<ID, GP_Employee_Master__c> MapofIDTOEM = new map<id,GP_Employee_Master__c>();
        list<GP_Employee_Master__c> updatelstofEM = new list<GP_Employee_Master__c>();
        set<id> setofID = new set<id>();
        
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return Database.getQueryLocator([Select Id,GP_Employee_HR_History__c from GP_Employee_Master__c]);      
    }

    global void execute(Database.BatchableContext bc, List<GP_Employee_Master__c> lstofEmpMaster)
    {
        system.debug('lstofEmpMaster@@@'+lstofEmpMaster);
        if(lstofEmpMaster != null && lstofEmpMaster.size()>0)
        {
            for(GP_Employee_Master__c  objEM: lstofEmpMaster)
            {
                MapofIDTOEM.put(objEM.ID,objEM);
            }
        }
        system.debug('MapofIDTOEM@@@'+MapofIDTOEM);
        if(MapofIDTOEM != null && MapofIDTOEM.size()>0)
        {
            for(GP_Employee_HR_History__c ehr :[Select ID,GP_Employees__c,GP_ASGN_EFFECTIVE_START__c,GP_ASGN_EFFECTIVE_END__c 
            from GP_Employee_HR_History__c where GP_Employees__c IN:MapofIDTOEM.KeySet()])
            {
                if(ehr.GP_ASGN_EFFECTIVE_START__c <= system.today() && ehr.GP_ASGN_EFFECTIVE_END__c >= system.today())
                {   
                    system.debug('ehr@@@'+ehr);
                    if(!setofID.contains(ehr.ID))
                    {
                        setofID.add(ehr.ID);
                        MapofIDTOEM.get(ehr.GP_Employees__c).GP_Employee_HR_History__c = ehr.ID;
                    }   
                }
            }
        }
        system.debug('MapofIDTOEM.values()@@@'+MapofIDTOEM.values());
        update MapofIDTOEM.values();
    }

    global void finish(Database.BatchableContext bc)
    {

    }
}