/*
 * @author: Prashant Kumar1
 * @since: 11/06/2018
 * @description: 
 * 
*/
public class VIC_CalcAnnualIncentivesBatch implements Database.Batchable<sObject> {
    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([
            SELECT Id, Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,
            Target_Component__r.Target__r.User__r.Id
            From Target_Achievement__c
            WHERE Target_Component__r.Master_Plan_Component__r.vic_Component_Category__c = 'Annual'
        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<Target_Achievement__c> incentives){
        Set<Id> userIds = new Set<Id>();
        for(Target_Achievement__c incentive: incentives){
            userIds.add(incentive.Target_Component__r.Target__r.User__r.Id);
        }
        
        // now getting plan codes of users
        
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}