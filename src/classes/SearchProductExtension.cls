public class SearchProductExtension {
	private final Opportunity opp;
    public ID opportunity_ID{get;set;}
    
    public SearchProductExtension(ApexPages.StandardController controller){
         this.opp = (Opportunity)controller.getRecord();
        opportunity_ID = opp.Id;
    }
}