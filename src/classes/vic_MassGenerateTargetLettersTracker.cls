@istest
private class vic_MassGenerateTargetLettersTracker {
    
    @istest
    static void getFinancialYearandVicRole_Test(){
        
        vic_MassGenerateTargetLetters.getFinancialYear();
    }
    
    @istest
    static void getTargetRecords_Test(){
        Integer curr = 19;
        Profile pf= [Select Id from profile where Name='System Administrator']; 

        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = 'uniqueName' + '@test' + 'org' + '.org', 
                         Username = 'uniqueName' + '@test' + 'orgId' + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = '123test', 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', ProfileId=pf.Id,
                         LanguageLocaleKey = 'en_US'); 
        insert uu;
        
        Master_VIC_Role__c role=new Master_VIC_Role__c();
        role.Horizontal__c='';
        role.Is_Active__c=true;
        role.Name='SL';
        role.OwnerId=uu.Id;
        role.Role__c='BD';
        role.Year__c = '2019';
        insert role;
        
        vic_MassGenerateTargetLetters.getVicRole(curr);
        
        Target__c tar=new Target__c();
        tar.user__c=uu.Id;
        tar.Is_Active__c=true;
        tar.Start_date__c=System.today();
        insert tar;
        
        Target__c tar1=new Target__c();
        tar1.user__c=uu.Id;
        tar1.Is_Active__c=true;
        tar1.Start_date__c=System.today();
        tar1.Is_Revised__c=true;
        tar1.Target_Letter_shared__c=true;
        tar1.Letter_Send_Status__c=true;
        insert tar1;
        
        User_VIC_Role__c vcRole=new User_VIC_Role__c();
        vcRole.User__c=uu.Id;
        vcRole.Master_VIC_Role__c=role.Id;
        insert vcRole;
        
        string Id=role.Id;
        
        List<Id> lstTrgtIds=new List<Id>();
        lstTrgtIds.add(tar.Id);
        lstTrgtIds.add(tar1.Id);
        
        vic_MassGenerateTargetLetters.getTargetRecords(Id,'Draft','2018');
        vic_MassGenerateTargetLetters.getTargetRecords(Id,'Revised','2018');
        //vic_MassGenerateTargetLetters.getBonusRecords(Id,2018);
        vic_MassGenerateTargetLetters.setUpdateTargetRecords(lstTrgtIds, 'Draft');
        vic_MassGenerateTargetLetters.setUpdateTargetRecords(lstTrgtIds, 'Revised');
		//vic_MassGenerateTargetLetters.getHorizontal();
    } 


}