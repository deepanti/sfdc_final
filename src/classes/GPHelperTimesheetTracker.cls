@isTest public class GPHelperTimesheetTracker {
    public static DateTime entryDate = system.today();
    public static String monthYear = 'May-18';//entryDate.format('MMMMM') + '-' + entryDate.year();	
    @testSetup static void setupCommonData() 
    {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Person_ID__c  = 'EMP-001';
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO'; 
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        timesheettrnsctnObj.GP_Month_Year__c = monthYear;
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        timeshtentryObj.GP_Project_Oracle_PID__c = 'ORACLE-001';
        timeshtentryObj.GP_Project_Task_Oracle_Id__c = 'ORACLE-001';
        insert timeshtentryObj ;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        insert address ;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert resourceAllocationObj; 
        
        GP_Job__c objJob = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJob;
        
        prjObj.GP_Oracle_PID__c='ORACLE-001';
        prjObj.GP_Is_Closed__c = false;
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Oracle_Status__c = 'S';
        update prjObj;
        
        GP_Temporary_Data__c objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJob.Id,
                                                                                                 'EMP-001',
                                                                                                 'ORACLE-001',
                                                                                                 '');
        insert objTemporaryTimeSheet;
        
        GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        projectTaskObj1.GP_Active__c = true;
        projectTaskObj1.GP_Task_Number__c = 'ORACLE-001';
        projectTaskObj1.GP_Project_Number__c  = 'ORACLE-001';
        projectTaskObj1.GP_Start_Date__c = system.today();
        projectTaskObj1.GP_End_Date__c = system.today().addMonths(3);
        insert projectTaskObj1;
        
        GP_Timesheet__c  timesheetObj = new GP_Timesheet__c();
        timesheetObj.GP_Month_Year__c = monthYear;
        timesheetObj.GP_Employee_Name__c = empObj.Name;
        timesheetObj.GP_PersonId_MonthYear__c = 'EMP-001'+monthYear;
        timesheetObj.GP_Expenditure_Item_Date__c = system.today();
        timesheetObj.GP_Person_Id__c = empObj.id;
        timesheetObj.GP_Project_Number__c = 'ORACLE-001';
        timesheetObj.GP_Task_Number__c = 'ORACLE-001';
        insert timesheetObj;
        
        //objTemporaryTimeSheet.GP_TSC_Project_Task__c = projectTaskObj1.id;//'ORACLE-001';
        //objTemporaryTimeSheet.GP_Project__c = 'ORACLE-001';
        //objTemporaryTimeSheet.GP_Employee__c = 'EMP-001';
        //objTemporaryTimeSheet.GP_TSC_Month_Year__c = monthYear;
        
    }     
    
    @isTest static void testGPCmpServiceTimesheetCorrection()
    {
        GPHelperTimesheet obj = new GPHelperTimesheet();
     	testgetTimeSheetData();
        testsetProjectTasks();
    }
    
    private static void testgetTimeSheetData(){
        set<string> settime = new Set<string>();
        settime.add(monthYear);
        
        List < GP_Timesheet__c > lstTimesheet = GPHelperTimesheet.getTimeSheetData(settime);
        List < GP_Timesheet_Entry__c > lstTimesheetEntry = GPHelperTimesheet.getTimeSheetEntryData(settime);
        List <GP_Timesheet_Transaction__c > lstTimesheetTrn = GPHelperTimesheet.getTimeSheetTransactionData(settime);
        
        Map < String, GP_Timesheet__c > mapTimeSheet = GPHelperTimesheet.getUniqueTimeSheetData(lstTimesheet);
    }
    
    private static void testsetProjectTasks(){
        Map<string,GP_Project_Task__c> mapProjectTask =  new Map<string,GP_Project_Task__c>([Select Id,GP_Project_Number__c,GP_Task_Number__c,
                                                                                             		GP_Start_Date__c,GP_End_Date__c 
                                                                                             From GP_Project_Task__c]);
        Map<string,GP_Project__c> mapProject =  new Map<string,GP_Project__c>([Select Id,GP_Oracle_PID__c From GP_Project__c]);
        set<string> setProject = new Set<string>();
        setProject.add('ORACLE-001');
        GPHelperTimesheet.setMapOfProjectTasks(setProject,mapProjectTask);
        GPHelperTimesheet.getMapOfProject(setProject,mapProject);
        
        Map<string,GP_Employee_Master__c> mapEmployee =  new Map<string,GP_Employee_Master__c>([Select Id,Name,GP_Person_ID__c From GP_Employee_Master__c]);
        set<string> setEMP = new Set<string>();
        setEMP.add('EMP-001');
        GPHelperTimesheet.getMapOfEmployee(setEMP,mapEmployee);
        
        List<GP_Temporary_Data__c> lstTemporaryData = [Select Id,GP_TSC_Project_Task__c,GP_Project__c,GP_Employee__c,GP_TSC_Ex_Type__c,
                                                       		  GP_TSC_Date__c,GP_TSC_Modified_Hours__c,GP_TSC_Month_Year__c 
                                                       from GP_Temporary_Data__c limit 1];
        
         List<GP_Timesheet_Entry__c> lstTimesheetEntryForBatch = GPHelperTimesheet.getAllTimeSheetEntriesForBatch(lstTemporaryData,mapEmployee,mapProject,mapProjectTask,new Map<String,GP_Temporary_Data__c>());
        
        set<string> settime = new Set<string>();
        settime.add('EMP-001'+monthYear);
        List<GP_Timesheet_Entry__c> lstTimesheetEntry = GPHelperTimesheet.getAllTimeSheetEntries(settime,mapProject,mapProjectTask);
    }
    
    
}