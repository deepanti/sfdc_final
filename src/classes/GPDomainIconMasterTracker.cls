@isTest
public class GPDomainIconMasterTracker 
{
    public static GP_Project__c prjObj,objChildProj;
    @isTest
    public static void testProject() 
    {
        GPDomainIconMaster obj = new GPDomainIconMaster(new List<GP_Icon_Master__c>{new GP_Icon_Master__c()});
        createData();
        
    }
    
    Public static void createData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_icon_master__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMasterForIcon = new GP_Icon_Master__c();
        objIconMasterForIcon.GP_CONTRACT__c = 'Test Contract';
        objIconMasterForIcon.GP_Status__C = 'Expired';
        objIconMasterForIcon.GP_END_DATE__c = date.newInstance(2017, 12, 25);
        objIconMasterForIcon.GP_START_DATE__c = system.today();
        insert objIconMasterForIcon;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_Role__c objroleforShare = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objroleforShare.GP_Type_of_Role__c = 'Delivery Org';
        objroleforShare.GP_HSL_Master__c = null;
        objroleforShare.GP_Work_Location_SDO_Master__c = null;
        insert objroleforShare;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        User objuserForProjet = GPCommonTracker.getUser();
        objuserForProjet.LastName = 'Test User For Project';
        objuserForProjet.Email = 'xyz@gmail.com';
        objuserForProjet.Username = 'sal@sales.com';
        insert objuserForProjet; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        objChildProj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        objChildProj.OwnerId=objuser.Id;
        objChildProj.GP_Parent_Project__c = prjObj.Id;
        objChildProj.GP_Customer_Hierarchy_L4__c = accountObj.id;
        Insert objChildProj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        insert projectLeadership;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.Id, prjObj.Id);
        insert resourceAllocationObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        GP_Project__c projectObj = new GP_Project__c();
        projectObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObj.GP_Primary_SDO__c = objSDO.id;
        projectObj.OwnerId = objuser.Id;
        projectObj.GP_HSL__c = objpinnacleMaster.id;
        projectObj.GP_PID_Approver_User__c = objuserForProjet.Id;
        projectObj.GP_Parent_Project__c = null;
        projectObj.GP_OMS_Deal_ID__c = dealObj.id;
        insert projectObj;
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.id = projectObj.id;
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_CRN_Number__c = objIconMasterForIcon.id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_Approval_Status__c = 'Draft';
        projectObjForIcon.GP_Is_Closed__c = False;
        //projectObjForIcon.GP_Clone_Source__c = 'system';
        update projectObjForIcon;
        
        objIconMasterForIcon.GP_END_DATE__c = system.today();
        update objIconMasterForIcon;
    }
}