public class GPSelectorTimesheetTransaction extends fflib_SObjectSelector {
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Timesheet_Transaction__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Timesheet_Transaction__c.sObjectType;
    }
    
    public List<GP_Timesheet_Transaction__c> selectById(Set<ID> idSet) {
        return (List<GP_Timesheet_Transaction__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Timesheet_Transaction__c> selectTimeSheetTransactionAndEntriesRecord(Set<Id> setOfEmployeeId,Set<String> setOfMonthYear){
        return [SELECT id,GP_Month_Year__c ,GP_Approval_Status__c,
                (select id,GP_Project__c,GP_Date__c 
                 from Timesheet_Entries__r where 
                 GP_Employee__c in :setOfEmployeeId
                 ORDER BY GP_Date__c DESC NULLS LAST LIMIT 1) 
                from GP_Timesheet_Transaction__c where GP_Employee__c in :setOfEmployeeId
                AND GP_Month_Year__c in :setOfMonthYear ];
    }
    
    public List<GP_Timesheet_Transaction__c> selectTimeSheetTransactionRecords(Id employeeId , String monthYearLower){
        return [select id,GP_Approval_Status__c from GP_Timesheet_Transaction__c where GP_Month_Year__c LIKE :monthYearLower
                and GP_Employee__c =:employeeId];
    }
    
    public List<GP_Timesheet_Transaction__c> selectTimeSheetTransactionRecordsForAMonthYear(Set<String> setOfMonthYearLower){
        return [select id,GP_Employee__c,GP_Month_Year__c,Name,GP_Approver__r.Name,GP_Oracle_Status__c,GP_Approval_Status__c,Owner.Name,GP_Approver__r.OHR_ID__c
         from GP_Timesheet_Transaction__c 
                where GP_Month_Year__c in :setOfMonthYearLower];
    }
    
    public List<GP_Timesheet_Transaction__c> getTimeSheetTransactionEmployeeMonthYear(Set<String> setOfEmployeeMonthYear){
        return [select Id,GP_Employee_Month_Year_Unique__c,CreatedDate,LastModifiedDate,GP_Oracle_Process_Log__c,
        GP_Approval_Status__c,Name,GP_Approver__r.Name,GP_Employee__c,
        GP_Oracle_Status__c,Owner.Name,GP_Approver__r.OHR_ID__c,GP_Month_Year__c
                from GP_Timesheet_Transaction__c 
                where GP_Employee_Month_Year_Unique__c in :setOfEmployeeMonthYear
               ORDER BY LastModifiedDate];//and GP_Approval_Status__c ='Draft'
    }

    public List < GP_Timesheet_Transaction__Share > selectProjectShareById(Set < ID > setOfProjectId, Set < ID > setUserID) {
        return (List < GP_Timesheet_Transaction__Share > )[SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId FROM GP_Timesheet_Transaction__Share where ParentId in: setOfProjectId AND UserOrGroupId in: setUserID];
    }
    }