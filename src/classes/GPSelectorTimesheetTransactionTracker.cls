@isTest
public class GPSelectorTimesheetTransactionTracker {
  public Static GP_Timesheet_Transaction__c timesheettransaction;


    @testSetup
    public static void buildDependencyData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        timesheettransaction = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettransaction;
        
    }
    
    @isTest
    public static void testgetSObjectType() {
        GPSelectorTimesheetTransaction timesheettransactionSelector = new GPSelectorTimesheetTransaction();
        Schema.SObjectType returnedType = timesheettransactionSelector.getSObjectType();
        Schema.SObjectType expectedType = GP_Timesheet_Transaction__c.sObjectType;
        
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectById() {
        GPSelectorTimesheetTransaction timesheetTransactionSelector = new GPSelectorTimesheetTransaction(); 
        
        GP_Timesheet_Transaction__c objTS = new GP_Timesheet_Transaction__c();
        objTS = [Select Id,Name from GP_Timesheet_Transaction__c Limit 1];
        List<GP_Timesheet_Transaction__c> listOfReturnedTimesheetTransaction = timesheetTransactionSelector.selectById(new Set<id>());
        timesheetTransactionSelector.selectTimeSheetTransactionAndEntriesRecord(null, null);
        timesheetTransactionSelector.selectTimeSheetTransactionRecords(null, null);
        timesheetTransactionSelector.selectTimeSheetTransactionRecordsForAMonthYear(null);
        timesheetTransactionSelector.getTimeSheetTransactionEmployeeMonthYear(null);
    }
}