public class vic_MassAssignTargetsController {
    
    @AuraEnabled
    public static List<String> getFinancialYear(){
        
        List<String> addFinancialYear=new List<String>();
        
        Integer currentYear = System.Today().year();
        Integer previousYear=currentYear - 1;
        Integer nextYear=currentYear + 1;
        addFinancialYear.add(String.valueof(currentYear));
        addFinancialYear.add(String.valueof(previousYear));
        addFinancialYear.add(String.valueof(nextYear));
        addFinancialYear.sort();
        return addFinancialYear;
    }
    
    @AuraEnabled
    public static List<wrapperVicRole> getVicRole(Integer currentFinancialYear){
        System.debug('currentFinancialYear == '+currentFinancialYear);
        List<wrapperVicRole> objWrprRolelst=new List<wrapperVicRole>();
        List<Master_VIC_Role__c> objRoleLst= [select id,name from Master_VIC_Role__c Where Year__c =:String.valueOf(currentFinancialYear) ORDER BY Name];
        
        for(Master_VIC_Role__c role:objRoleLst){
            wrapperVicRole obj=new wrapperVicRole();
            obj.strId=role.Id;
            obj.name=role.name;
            objWrprRolelst.add(obj);
        }
        
        return objWrprRolelst;
    }
    
    @AuraEnabled
    public static List<User> getAllUsers(string getVicRole, string financialYear){
        System.debug('financialYear == '+financialYear);
        //Date obCurrentDate = Date.today();
        Integer previousYear = (Date.today().year())-1;
        List<User_VIC_Role__c> usersObj=new List<User_VIC_Role__c>();
        Set<Id> setUserId = new Set<Id>();
        List<Target__c> lstSlctdUsr=new List<Target__c>();
        List<User> objUser=new List<User>();
        Map<Id,Id> mapUserandTarget =new Map<Id,Id>();
        Set<Id> objUserId = new Set<Id>();
        Set<Id> setOldYearUserId = new Set<Id>();
        usersObj = [Select User__c,vic_For_Previous_Year__c from User_VIC_Role__c where Master_VIC_Role__c= :getVicRole and Not_Applicable_for_VIC__c=false and User__r.IsActive=true];
        System.debug('usersObj === >>'+usersObj);
        //Logic to check previous year mapping for selecting master VIC role
        for(User_VIC_Role__c vicRole :usersObj){ 
            if(previousYear == Integer.valueOf(financialYear) && vicRole.vic_For_Previous_Year__c ){
                setOldYearUserId.add(vicRole.User__c);
            }           
            setUserId.add(vicRole.User__c);
        }
        if(!setOldYearUserId.isEmpty()){
            setUserId = setOldYearUserId;
        }
        System.debug('setOldYearUserId === >> '+setOldYearUserId);
        System.debug('setUserId === >> '+setUserId);
        // objUser=[select Id,Name from User where id in :objUserId];
        Date startDate = Date.newInstance(Integer.valueOf(financialYear.trim()), 1, 1);
        lstSlctdUsr = [select Id,User__c,Start_date__c from Target__c where User__c In :setUserId];
        System.debug('lstSlctdUsr === >> '+lstSlctdUsr);
        for(Target__c eachTar :lstSlctdUsr){
            if(eachTar.Start_date__c!=null){
                Integer year=eachTar.Start_date__c.Year();
                Integer passedYear=Integer.valueOf(financialYear);
                if(year==passedYear)
                    mapUserandTarget.put(eachTar.User__c, eachTar.Id);
            }
        }
        
        for(User_VIC_Role__c roleVic : usersObj){    
            if(mapUserandTarget.get(roleVic.User__c)==null){     
                objUserId.add(roleVic.User__c);
            }
        }
        
        objUser=[select Id,Name from User where id in :objUserId];
        System.debug('Selected Users === '+objUser);
        return objUser;
    }
    
    @auraEnabled
    public static List<User> getAllPlans(List<Id> userId){
        
        List<User> objUser=new List<User>();
        objUser=[select Id,Name from user where id in :userId];
        
        return objUser;
        
    }
    
    
    @auraEnabled
    public static DynamicWrapper getPlanName(String vicRole,List<String> lstUserDetails,string financialYear){
        
        List<Plan_Component__c> objPlanCom=new List<Plan_Component__c>();
        List<VIC_Role__c> lstVcRole=new List<VIC_Role__c>();
        List<Plan__c> plan=new List<Plan__c>();
        Set<Id> setPlanId=new Set<Id>();
        DynamicWrapper objWrapper = new DynamicWrapper();
        
        try{            
            lstVcRole = [select Plan__c from VIC_Role__c where Master_VIC_Role__c = :vicRole AND Plan__r.Is_Active__c = TRUE AND Plan__r.Year__c = :financialYear.trim() LIMIT 1];
            system.debug(lstVcRole + 'lstVcRole');
            if(lstVcRole == null || lstVcRole.size()<=0){
                //throw new AuraHandledException('No active plan found for selected financial year.');
                throw new vic_Exception('No active plan found for selected financial year.');
            }
            
            for(VIC_Role__c role : lstVcRole){
                setPlanId.add(role.Plan__c);
            }
            
            plan = [select Name,Id,(select Id,Master_Plan_Component__c,Master_Plan_Component__r.Component_Usage__c,Master_Plan_Component__r.Bonus_Type__c,Weightage_Applicable__c,Master_Plan_Component__r.Name,Master_Plan_Component__r.Point_type__c from Plan_Components__r) from Plan__c where id in :setPlanId];
            
            
            if(plan != null && plan.size() > 0){
                List<MasterWrapper> lstMasterCmpName = new List<MasterWrapper>();
                for(Plan_Component__c objPlancmp : plan[0].Plan_Components__r){
                    MasterWrapper objMaster = new MasterWrapper();
                    if(objPlancmp.Master_Plan_Component__r.Point_type__c != null && objPlancmp.Master_Plan_Component__r.Point_type__c == 'Currency' && objPlancmp.Master_Plan_Component__r.Name !='Bonus (Currency)'){
                        objMaster.strMasterName = objPlancmp.Master_Plan_Component__r.Name + ' (In Million)';
                    }else{
                        objMaster.strMasterName = objPlancmp.Master_Plan_Component__r.Name;
                    }
                    
                    objMaster.strMasterType = objPlancmp.Master_Plan_Component__r.Point_type__c;
                    objMaster.strMasterId = objPlancmp.Master_Plan_Component__c;
                    objMaster.weightage=objPlancmp.Weightage_Applicable__c;
                    objMaster.BonusType = objPlancmp.Master_Plan_Component__r.Bonus_Type__c;
                    objMaster.componentUsage=objPlancmp.Master_Plan_Component__r.Component_Usage__c;
                    if(objPlancmp.Master_Plan_Component__r.Component_Usage__c != null && objPlancmp.Master_Plan_Component__r.Component_Usage__c != '' && objPlancmp.Master_Plan_Component__r.Component_Usage__c.contains('Target')){
                        objMaster.componentUsageType = true;
                    }else{
                        objMaster.componentUsageType = false;
                    }
                    lstMasterCmpName.add(objMaster);
                }
                objWrapper.lstMasterCmpName = lstMasterCmpName;
                objWrapper.lstPlan = plan;
                
                List<WrapperRows> lstWrapperRows = new List<WrapperRows>();
                for(String str : lstUserDetails){
                    system.debug(str +'str');
                    WrapperRows objWrapperRows = new WrapperRows();
                    objWrapperRows.strUserId = str.split('-')[0];
                    objWrapperRows.strUserName = str.split('-')[1];
                    objWrapperRows.lstMasterCmpName = lstMasterCmpName;
                    objWrapperRows.objPicklistWrapper = getEmptyTargetObject(); 
                    objWrapperRows.strSelectedCurrency = 'USD';
                    lstWrapperRows.add(objWrapperRows);
                }
                objWrapper.lstWrapperRows = lstWrapperRows;
            }
            return objWrapper;
            
        }
        catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    
    
    @auraEnabled
    public static String insertTarget(String planId,String strWrapperRows, String financialYear, String selectCurrency){
        
        try{
            
            Date startDate=null;
            Date endDate=null;
            
            Integer financialYearInteger;
            if(financialYear != null){
                financialYearInteger = integer.valueof(financialYear); 
            }   
            if(financialYear != null){
                startDate = Date.newInstance(financialYearInteger,1,1);
                endDate = Date.newInstance(financialYearInteger,12,31); 
            }
            
            List<WrapperRows> lstRows = (List<WrapperRows>)json.deserialize(strWrapperRows, List<WrapperRows>.class);
            
            List<Target__c> lstTarget = new List<Target__c>();
            List<WrapperTargetAndComponentList> lstWrapps = new List<WrapperTargetAndComponentList>();
            for(WrapperRows objwr : lstRows){
                
                Target__c objTar=new Target__c();
                objTar.User__c=objwr.strUserId;
                objTar.Plan__c=planId;
                objTar.start_Date__c=startDate;
                objTar.End_Date__c=endDate;
                objTar.Is_Active__c=true;
                objTar.CurrencyIsoCode=objwr.strSelectedCurrency;
                objTar.vic_Guaranteed_Payout__c = objwr.guaranteedPayout;
                objTar.Target_Bonus__c = objwr.targetBonus;
                lstTarget.add(objTar);
                
                WrapperTargetAndComponentList objWrap = new WrapperTargetAndComponentList();
                objWrap.objTarget = objTar;
                objWrap.lstComponents = new List<Target_Component__c>();
                for(MasterWrapper objMw : objwr.lstMasterCmpName){
                    Target_Component__c eachCmp=new Target_Component__c();
                    eachCmp.Master_Plan_Component__c=objMw.strMasterId;
                    if(objMw.targetCurrency!=null && objMw.targetCurrency>0){
                        if(objMw.strMasterName != 'Bonus (Currency)'){
                           eachCmp.Target_In_Currency__c=objMw.targetCurrency * 1000000; 
                        }
                        else{
                            eachCmp.Target_In_Currency__c=objMw.targetCurrency;
                        }
                        //eachCmp.Target_In_Currency__c=objMw.targetCurrency;
                        //eachCmp.Target_In_Currency__c=eachCmp.Target_In_Currency__c * 1000000;
                    }
                    eachCmp.Target_Percent__c=objMw.targetPercent;
                    eachCmp.Target_Status__c='Active';
                    eachCmp.Weightage__c=objMw.weightageVal;
                    eachCmp.Component_Usage__c=objMw.componentUsage;  //[Commented by Shivam]
                    eachCmp.Is_Bonus_Component__c = (objMw.componentUsage != null && objMw.componentUsage != '') ? (objMw.componentUsage.contains('Bonus')? TRUE: FALSE) : FALSE;
                    eachCmp.Bonus_Type__c=objMw.BonusType;
                    eachCmp.Is_Weightage_Applicable__c=objMw.weightage;
                    objWrap.lstComponents.add(eachCmp);
                }
                lstWrapps.add(objWrap);
            }
            
            
            if(lstTarget != null && lstTarget.size() > 0){
                insert lstTarget;
                List<Target_Component__c> lstTargetCompInsert = new List<Target_Component__c>();
                for(WrapperTargetAndComponentList objWrapp : lstWrapps){
                    for(Target_Component__c objComp : objWrapp.lstComponents){
                        objComp.Target__c=objWrapp.objTarget.Id;
                        lstTargetCompInsert.add(objComp);
                    }
                }
                if(lstTargetCompInsert != null && lstTargetCompInsert.size() >0){
                    insert lstTargetCompInsert;
                }
            }
            return 'SUCCESS';
        }
        catch(Exception ex){
            system.debug('Error is :::' + ex.getMessage());
            return ex.getMessage();
        }
        
    }
    
    @auraEnabled
    public static List<wrprCrncy> getEmptyTargetObject(){
        List<currencytype> lstCrncyIso=[select Id,IsoCode from currencytype order by IsoCode];
        List<wrprCrncy> lstwrprCrncy=new List<wrprCrncy>();
        for(currencytype eachCrncy : lstCrncyIso){
            wrprCrncy obj=new wrprCrncy();
            obj.IsoCode=eachCrncy.IsoCode;
            obj.Id=eachCrncy.Id;
            lstwrprCrncy.add(obj);
        }
        return lstwrprCrncy;
        
    }
    
    @auraEnabled
    public static Boolean getLocalCurrency(String vcRoleId){
        Boolean check=false;
        List<VIC_Role__c> lstChkBoln=[select Master_VIC_Role__r.Capture_User_Local_Currency__c from VIC_Role__c where Master_VIC_Role__c= :vcRoleId]; 
        for(VIC_Role__c eachVcRole : lstChkBoln){
            check=eachVcRole.Master_VIC_Role__r.Capture_User_Local_Currency__c;
            
        }
        
        return check;
        
    }
    
    public class WrapperTargetAndComponentList{
        public Target__c objTarget;
        public list<Target_Component__c> lstComponents;
    }
    public class MasterWrapper{
        @AuraEnabled
        public String strMasterName;
        @AuraEnabled
        public String strMasterId;
        @AuraEnabled
        public String strMasterType;
        @AuraEnabled
        public Decimal targetCurrency;
        @AuraEnabled
        public decimal targetPercent;
        @AuraEnabled
        public Boolean weightage;
        @AuraEnabled
        public Decimal weightageVal;
        @auraEnabled
        public String componentUsage;
        @auraEnabled
        public Boolean componentUsageType;
        @auraEnabled
        public String BonusType;
        
    }
    
    public class WrapperRows{
        @AuraEnabled
        public String strUserName;
        @AuraEnabled
        public String strUserId;
        @AuraEnabled
        public List<MasterWrapper> lstMasterCmpName;
        @AuraEnabled
        public List<wrprCrncy> objPicklistWrapper;
        @AuraEnabled
        public String strSelectedCurrency;
        @AuraEnabled
        public Decimal guaranteedPayout;
        @AuraEnabled
        public Decimal targetBonus;
        
    }
    
    public class DynamicWrapper{
        @AuraEnabled
        public List<MasterWrapper> lstMasterCmpName;
        @AuraEnabled
        public List<Plan__c> lstPlan;
        @AuraEnabled
        public List<WrapperRows> lstWrapperRows;
    }
    public class wrapperVicRole{
        
        @auraenabled
        public string strId;
        @auraenabled
        public string name;
        
    }
    
    public class wrapperInsertTargetandComponents{
        
        @auraenabled
        public string planId;
        @auraenabled
        public string userId;
        @auraenabled
        public Date startDate;
        @auraenabled
        public Date endDate;
        @auraenabled
        public string userName;
        @auraenabled
        public string targetPercent;
        @auraenabled
        public string targetCurrency;
    }
    
    public class wrprCrncy{
        
        @auraenabled
        public string Id;
        @auraenabled
        public string IsoCode;
    }
    
}