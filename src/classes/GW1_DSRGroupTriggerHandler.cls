// Class is handler class of DSRGroupTrigger ,Class is used to create chatter group on after insert of 
// DSR group record .
// On After update Event if owner is changed the owner of chatter group is also changed
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1.1        03-10-2016       Rishi Kumar              Created
// ---------------------------------------------------------------------------------------------
// v1.1        10-10-2016       Pankaj               Change Owner functionality.
// ---------------------------------------------------------------------------------------------
// v1.2       4-8-2016          Rishi              on after insert of group we will any existing team members
public class GW1_DSRGroupTriggerHandler
{

	public void runTrigger()
	{
		if (trigger.isAfter && trigger.isInsert)
		{
				// v1.2       4-8-2016          Rishi              on after insert of group we will any existing team members
				onAfterInsert((list<GW1_DSR_Group__c>) trigger.new);
		}
		if (trigger.IsBefore && trigger.isInsert)
		{
			onBeforeInsert((list<GW1_DSR_Group__c>) trigger.new);
		}

		if (trigger.isAfter && trigger.IsUpdate)
		{
			onAfterUpdate((list<GW1_DSR_Group__c>) trigger.new, (map<id, GW1_DSR_Group__c>) trigger.oldmap, (map<id, GW1_DSR_Group__c>) trigger.newMap);
		}
	}

	// v1.2       4-8-2016          Rishi     
	// Any existing Team like opportunity owner ,tower lead , Bid manager, will be added into chatter group
	private void onAfterInsert(list<GW1_DSR_Group__c> lstTriggerNew)
	{
		Set<ID> setParentDSRID = new Set<ID> ();
		list<GW1_DSR_Team__c> existingDSRTeam = new list<GW1_DSR_Team__c>();
		for (GW1_DSR_Group__c objDSRGroup : lstTriggerNew)
		{			
				// putting Chatter Group ID to Respective DSR Group 			
				setParentDSRID.add(objDSRGroup.GW1_DSR__c);			
		}
		list<GW1_DSR__c> lstDSR = new list<GW1_DSR__c>( [Select GW1_Opportunity__c,GW1_Opportunity__r.Accountid, GW1_Primary_Chatter_ID__c ,(select Id, name,GW1_DSR__c,GW1_Is_active__c  ,GW1_Role__c , GW1_User__c, GW1_DSR_Name__c from DSR_Team__r ) from GW1_DSR__c where id in:setParentDSRID]);
		
		if (lstDSR!=null && lstDSR.size() > 0)
		{

			
			for(GW1_DSR__c objDSR:lstDSR)
			{
				for(GW1_DSR_Team__c objDSRTeam:objDSR.DSR_Team__r)
				{
					existingDSRTeam.add(objDSRTeam);
				}
			}
			// Adding ExistingTeam Memmber In group
			if (existingDSRTeam != null && existingDSRTeam.size() > 0)
			{
				GW1_DSRTeamTriggerHandler objDsrTeamHandler = new GW1_DSRTeamTriggerHandler();
				objDsrTeamHandler.addChatterMembers(existingDSRTeam);
			}
		}

	}
	
	private void onBeforeInsert(list<GW1_DSR_Group__c> lstTriggerNew)
	{
		createChatterGroup(lstTriggerNew); // this method will create chatter group 
	}

	// this method will execute  on  After Update  
	private void onAfterUpdate(list<GW1_DSR_Group__c> lstTriggerNew, map<id, GW1_DSR_Group__c> oldmap, map<id, GW1_DSR_Group__c> newMap)
	{
		changeChatterOwner(lstTriggerNew, oldmap, newMap);
	}


	/*
	  This method will create chatter group 
	  --------------------------------------------------------------------------------------
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          03-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
	 */
	private void createChatterGroup(list<GW1_DSR_Group__c> lstTriggerNew)
	{
		list<CollaborationGroup> lstChatterGroupToInsert = new list<CollaborationGroup> ();
		map<string, CollaborationGroup> mapDSRIDOChatterGroup = new map<string, CollaborationGroup> ();
		


		for (GW1_DSR_Group__c objDSRGroup : lstTriggerNew)
		{
			// Creating Chatter gruop against each DSR group
			CollaborationGroup objGrp = new CollaborationGroup();
			objGrp.Name = objDSRGroup.name;
			objGrp.CollaborationType = 'Private';
			objGrp.OwnerId = objDSRGroup.OwnerId;
			String fullFileURL = 'Open DSR : ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + objDSRGroup.GW1_DSR_Link__c + ' \n\n  ';
			fullFileURL += 'Account : ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + objDSRGroup.GW1_Account_ID__c + ' \n \n ';
			fullFileURL += 'Opportunity : ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + objDSRGroup.GW1_Opportunity_ID__c + ' \n \n ';
			objGrp.Description = fullFileURL;
			lstChatterGroupToInsert.add(objGrp);
			mapDSRIDOChatterGroup.put(objDSRGroup.GW1_DSR__c, objGrp); // creating map DSR GROUP ID TO CHATTER GROUP 

		}

		if (lstChatterGroupToInsert != null && lstChatterGroupToInsert.size() > 0)
		{
			try
			{
				insert lstChatterGroupToInsert; // inserting new list of chatter group 
			}
			catch(exception e)
			{
					System.debug('');	
                	return ;
			}
		}

		system.debug('mapDSRIDOChatterGroup::' + mapDSRIDOChatterGroup);


		Set<ID> setParentDSRID = new Set<ID> ();
		list<CollaborationGroupRecord > lstchatterGroupRecords= new list<CollaborationGroupRecord >();
		for (GW1_DSR_Group__c objDSRGroup : lstTriggerNew)
		{
			if (mapDSRIDOChatterGroup != null && mapDSRIDOChatterGroup.get(objDSRGroup.GW1_DSR__c) != null)
			{
				// putting Chatter Group ID to Respective DSR Group 
				objDSRGroup.GW1_Chatter_Group_Id__c = mapDSRIDOChatterGroup.get(objDSRGroup.GW1_DSR__c).id;
				setParentDSRID.add(objDSRGroup.GW1_DSR__c);
			}
		}

		//now we will add any existing members to chatter group
	
		if(setParentDSRID!=null && setParentDSRID.size()>0)
		{
			list<GW1_DSR_Team__c> existingDSRTeam = new list<GW1_DSR_Team__c>();
		    list<GW1_DSR__c> lstDSRtoUpdate = new list<GW1_DSR__c>();
			list<GW1_DSR__c> lstDSR = new list<GW1_DSR__c>( [Select GW1_Opportunity__c,GW1_Opportunity__r.Accountid, GW1_Primary_Chatter_ID__c ,(select Id, name,GW1_DSR__c ,GW1_Role__c , GW1_User__c, GW1_DSR_Name__c from DSR_Team__r ) from GW1_DSR__c where id in:setParentDSRID]);
			
			for(GW1_DSR__c objDSR:lstDSR)
			{
			
				// Adding Chatter group Id in DSR
				if (mapDSRIDOChatterGroup != null && mapDSRIDOChatterGroup.get(objDSR.ID) != null)
				    {
				    	  objDSR.GW1_Primary_Chatter_ID__c= mapDSRIDOChatterGroup.get(objDSR.ID).id;
				    	  lstDSRtoUpdate.add(objDSR);
				    	  
				    	  // Adding DSR Record 
				    	 if(objDSR.GW1_Opportunity__c!=null) 
				    	 {
				    	  CollaborationGroupRecord  objgroupDsr = new CollaborationGroupRecord ();
						  objgroupDsr.recordid=objDSR.id;
						  objgroupDsr.CollaborationGroupId=mapDSRIDOChatterGroup.get(objDSR.ID).id;
						  lstchatterGroupRecords.add(objgroupDsr);
						  
						  // Adding Opportunity 
						   CollaborationGroupRecord  objgroupOpp = new CollaborationGroupRecord ();
						   objgroupOpp.recordid=objDSR.GW1_Opportunity__c;
						   objgroupOpp.CollaborationGroupId=mapDSRIDOChatterGroup.get(objDSR.ID).id;
						   lstchatterGroupRecords.add(objgroupOpp);
						  
						  // Adding Account 
						   CollaborationGroupRecord  objgroupAccount = new CollaborationGroupRecord ();
						   objgroupAccount.recordid=objDSR.GW1_Opportunity__r.Accountid;
						   objgroupAccount.CollaborationGroupId=mapDSRIDOChatterGroup.get(objDSR.ID).id;
						   lstchatterGroupRecords.add(objgroupAccount);
				    	 }  
				    }
				
			}
			
			if(lstDSRtoUpdate!=null && lstDSRtoUpdate.size()>0)
			{
				 update lstDSRtoUpdate;
			}
			
			if(lstchatterGroupRecords!=null && lstchatterGroupRecords.size()>0)
			{
   					try{
   						  insert lstchatterGroupRecords;
   					  }catch(exception e)
   					  {
   					  	System.debug(e);
   					  }
			}
		}
	}


	/*
	  This method will Change the chatter group owner 
	  --------------------------------------------------------------------------------------
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          03-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
	 */

	public void changeChatterOwner(list<GW1_DSR_Group__c> lstTriggerNew, map<id, GW1_DSR_Group__c> triggerOldMap, map<id, GW1_DSR_Group__c> triggernewMap)
	{

		set<id> setchatterGroupId = new set<id> ();
		system.debug('-------' + lstTriggerNew);

		for (GW1_DSR_Group__c objDSRgroup : lstTriggerNew)
		{
			// If Prevoius Owner and new owner is not same and DSR Group has A chatter group Associated with it 
			if (triggerOldMap.get(objDSRgroup.id).ownerid != objDSRgroup.ownerid && objDSRgroup.GW1_Chatter_Group_Id__c != '')
			setchatterGroupId.add(objDSRgroup.GW1_Chatter_Group_Id__c);
		}

		if (setchatterGroupId != NULL && setchatterGroupId.size() > 0)
		{
			list<CollaborationGroup> lstChattergrouptoupdate = new list<CollaborationGroup> ();
			list<CollaborationGroupMember> lstCollaborationGroupMember = new list<CollaborationGroupMember> ();
			map<id, Set<id>> mapGoupidToMeneberID = new map<id, Set<id>> ();
			// Fecthing All Chatter group , whose Owner going to be chnaged 
			map<id, CollaborationGroup> mapChattergroup = new map<id, CollaborationGroup> ([select id, ownerid, (Select Id, MemberId From GroupMembers) from CollaborationGroup where id in :setchatterGroupId]);

			if (mapChattergroup != null && mapChattergroup.size() > 0)
			{
				// creating map Chatter group Id To Set Of memember Ids
				for (ID objChatterGroupID : mapChattergroup.keyset())
				{
					if (mapGoupidToMeneberID.get(objChatterGroupID) == null)
					mapGoupidToMeneberID.put(objChatterGroupID, new set<id> ());
					for (CollaborationGroupMember objGroupMember : mapChattergroup.get(objChatterGroupID).GroupMembers)
					{
						mapGoupidToMeneberID.get(objChatterGroupID).add(objGroupMember.MemberId);
					}

				}

				system.debug('mapChattergroup::' + mapChattergroup);
			}
			set<id> SetDsrid = new Set<id> (); //  to Generate  unique Feeds  Dsr
			
			if (mapChattergroup != null && mapChattergroup.size() > 0)
			{
				for (GW1_DSR_Group__c objDSRgroup : lstTriggerNew)
				{
					//If Prevoius Owner and new owner is not same and DSR Group has A chatter group Associated with it 
					if (mapChattergroup.get(objDSRgroup.GW1_Chatter_Group_Id__c) != null && triggerOldMap.get(objDSRgroup.id).ownerid != objDSRgroup.ownerid && objDSRgroup.GW1_Chatter_Group_Id__c != '')
					{
						// If new Owner is not a Group Memeber 
						if (mapGoupidToMeneberID.get(objDSRgroup.GW1_Chatter_Group_Id__c) != null && !mapGoupidToMeneberID.get(objDSRgroup.GW1_Chatter_Group_Id__c).contains(objDSRgroup.ownerid))
						{
							CollaborationGroupMember grpMr = new CollaborationGroupMember();
							grpMr.memberid = objDSRgroup.ownerid;
							grpMr.CollaborationGroupId = mapChattergroup.get(objDSRgroup.GW1_Chatter_Group_Id__c).id;
							lstCollaborationGroupMember.add(grpMr);
						}
						
						mapChattergroup.get(objDSRgroup.GW1_Chatter_Group_Id__c).ownerid = objDSRgroup.ownerid; // changing  Chatter group owner with  new
						lstChattergrouptoupdate.add(mapChattergroup.get(objDSRgroup.GW1_Chatter_Group_Id__c));
						
						
						
						
					}
				}

				// if list has some Chatter group to update
				if (lstChattergrouptoupdate != null && lstChattergrouptoupdate.size() > 0)
				{
					try
					{
						if (lstCollaborationGroupMember != null && lstCollaborationGroupMember.size() > 0)
							 insert lstCollaborationGroupMember;
					   

						update lstChattergrouptoupdate; // updating chantter group
						
						
					}

					catch(exception e)
					{
						if (e.getMessage().contains('The owner of a group must be a group member.'))
						{
							lstTriggerNew[0].adderror('Please add this user to the team member.');
						}
					}
				}
			}
		}
	}
}