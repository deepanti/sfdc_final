public class CheckRecursiveForNonTs {
    
    public static boolean runAfterNonTs = true;
    public static boolean runBeforeNonTsHoldTeam = true;
    
    public static Boolean runBeforeNonTsHoldTeam(){
        if(runBeforeNonTsHoldTeam){
           runBeforeNonTsHoldTeam = false; 
            return true;
        }else{
            return runBeforeNonTsHoldTeam;
        }
    }
    
    public static boolean runOnceAfterNonTs() { 
        if(runAfterNonTs){
            runAfterNonTs=false;
            return true;
        } else {
            return runAfterNonTs;
        }
    }
}