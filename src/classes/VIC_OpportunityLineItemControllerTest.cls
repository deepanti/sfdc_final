@isTest
public class VIC_OpportunityLineItemControllerTest {
    static testMethod void testOpportunityLineItem(){
        Account acc=vic_commonTest.createAccount('AI');
        insert acc;
        user objuser = VIC_CommonTest.createUser('Test','Test Name2','jdnclljn@jkdj.com','Genpact Sales Rep','China');
        insert objuser;
         user objuser1 = VIC_CommonTest.createUser('Test1','Test Name1','jdnclljn@jkdjk.com','Genpact Sales Rep','China');
        insert objuser1;
        user objuser2 = VIC_CommonTest.createUser('Test2','Test Name2','jdnclljn@ikdjk.com','Genpact Sales Rep','China');
        insert objuser2;
         
       opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',acc.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;
       
    OpportunityLineItem   objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.Product_BD_Rep__c=objuser.id;
objOLI.vic_VIC_User_3__c=objuser1.id;
objOLI.vic_VIC_User_4__c =objuser2.id;
objOLI.vic_Is_Split_Calculated__c=false;
objOLI.vic_CPQ_Deal_Status__c= 'Active';  
objOLI.vic_TCV_EBIT_Consideration_ForNPV__c='5';
insert objOLI;
        test.startTest();
        VIC_OpportunityLineItemController.getOLI(objOLI.id);
        VIC_OpportunityLineItemController.fetchOpportunities('AI', 'UI');
        system.assertequals(200,200);
        test.stopTest();
    }
}