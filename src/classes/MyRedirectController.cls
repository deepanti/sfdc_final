/*This class is used to redirect to getfeeback survey page
Version 1.0, Created By: Ankit Nagpal, Created date: 27th,July,2015, Deployment date: 31th, July,2015*/

public with sharing class MyRedirectController {
    public opportunity opportunityobject ;
    public string opportunityobjowner;
    public string opportunityobjID;
    public string OppobjID;
    
    public MyRedirectController(ApexPages.StandardController controller) 
    {
        
    }
    //Method to redirect to getfeedback survey page
    public PageReference doRedirect() 
    {
        opportunityobject=[select id,owner.name,Survey_Filled__c,Opportunity_ID__c from opportunity where id=:apexpages.currentpage().getparameters().get('id')];
        opportunityobjowner=opportunityobject.owner.name;
        opportunityobjID=opportunityobject.id;
        OppobjID=opportunityobject.Opportunity_ID__c;
        if(opportunityobject.Survey_Filled__c==false)//redirect to survey page only if survey is not already taken
        {
            //PageReference pageRef = new PageReference('https://www.getfeedback.com/r/kGKWyQ4f?OpportunityOwner='+opportunityobjowner+'&OpportunityID='+opportunityobjID+'&UserDevice=Mobile');
            PageReference pageRef = new PageReference('https://www.getfeedback.com/r/nrZyUxCX?OpportunityOwner='+opportunityobjowner+'&OpportunityID='+opportunityobjID+'&OppID='+OppobjID+'&UserDevice=Mobile');
            pageRef.setredirect(true);
            return pageRef;
        }
        else // show error in case survey is already taken for the opportunity
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You have already taken this survey!'));
            return null;
        }
    }
    
}