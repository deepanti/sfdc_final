public class MainOpportunityLineItemTriggerHelper {
    public static map<string, MainOpportunityHelperFlag__c>  MapofProjectTrigger = MainOpportunityHelperFlag__c.getall();
    
    public static void beforeInsertMethod(list<opportunityLineItem> newOliList){
        try{
            if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('InsertAdditionalfieldsInOli') && MapofProjectTrigger.get('InsertAdditionalfieldsInOli').HelperName__c){
                OpportunityLineItemTriggerHelper.InsertAdditionalfieldsInOpportunityLineItems(newOliList);
            }
        }catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber()); CreateErrorLog.createErrorRecord('MainOpportunityLineItemTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOpportunityLineItemTriggerHelper', 'beforeInsertMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }
    }
    
    public static void beforeUpdateMethod(list<opportunityLineItem> newOliList,Map<id,opportunityLineItem> oldOliMap){
        try{
            VIC_OpportunityLineItemTriggerHandler objOLI = new VIC_OpportunityLineItemTriggerHandler();
            if(VIC_TriggerSwitchManager.isTriggerSwitchActive('Opportunity_Line_Item')){ //Checking trigger is active or not
                objOLI.runTriggerAction();
            }
        }catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());  CreateErrorLog.createErrorRecord('MainOpportunityLineItemTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOpportunityLineItemTriggerHelper', 'beforeUpdateMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }
    }
    
    public static void beforeDeleteMethod(){
        
    }
    
    public static void afterInsertMethod(list<opportunityLineITem> newOliList,Map<id,opportunityLineITem> newOliMap,String profileName){
        try{
            boolean runOnceFlag = CheckRecursiveForOLI.runOnceAfter();
            if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('createRevenueSchedule') && MapofProjectTrigger.get('createRevenueSchedule').HelperName__c){
                if(Label.Data_Loader_Upload == 'true' && runOnceFlag && !Test.isRunningTest() && profileName == 'Genpact Super Admin'){  OpportunityLineItemTriggerHelper.createRevenueSchedule(newOliList);
                }   
            }
            system.debug(':--runOnceFlag--'+runOnceFlag);
            if(runOnceFlag){ 
                system.debug(':--insertOpportunityTeamMember-');
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('insertOpportunityTeamMember') && MapofProjectTrigger.get('insertOpportunityTeamMember').HelperName__c){
                    OpportunityLineItemTriggerHelper.insertOpportunityTeamMember(newOliList);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateOpportunityFields') && MapofProjectTrigger.get('updateOpportunityFields').HelperName__c){ OpportunityLineItemTriggerHelper.updateOpportunityFields(newOliList); 
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateYearlyRevenue') && MapofProjectTrigger.get('updateYearlyRevenue').HelperName__c){
                    OpportunityLineItemTriggerHelper.updateYearlyRevenue(newoliMap.keySet());    
                }
            }
        }catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());  CreateErrorLog.createErrorRecord('MainOpportunityLineItemTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOpportunityLineItemTriggerHelper', 'afterInsertMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }
    }
    
    public static void afterUpdateMethod(list<opportunityLineITem> newOliList,Map<id,opportunityLineITem> oldOliMap,String profileName){
        try{
            boolean runOnceFlag = CheckRecursiveForOLI.runOnceAfter();  
            if(Label.Data_Loader_Upload == 'true' && profileName == 'Genpact Super Admin'){ 
                if(runOnceFlag){
                    if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updatelfieldsInOLineItems') && MapofProjectTrigger.get('updatelfieldsInOLineItems').HelperName__c){
                        OpportunityLineItemTriggerHelper.updateAdditionalfieldsInOpportunityLineItems(oldOliMap.keySet()); 
                    }
                }
            } 
            if(runOnceFlag){ 
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateYearlyRevenue') && MapofProjectTrigger.get('updateYearlyRevenue').HelperName__c){
                    OpportunityLineItemTriggerHelper.updateYearlyRevenue(oldOliMap.keySet()); 
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateOpportunityTeamMember') && MapofProjectTrigger.get('updateOpportunityTeamMember').HelperName__c){   OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(oldOliMap, newOliList);
                }
            }
            Boolean runthriceFlag;
            if(Test.isRunningTest()){
                runthriceFlag = CheckRecursiveForOLI.runingTestFlag();
            }else{
                runthriceFlag = true;
            }
            if(runthriceFlag){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateOpportunityFields') && MapofProjectTrigger.get('updateOpportunityFields').HelperName__c){
                    OpportunityLineItemTriggerHelper.updateOpportunityFields(newOliList);
                }
            }
        }catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());  CreateErrorLog.createErrorRecord('MainOpportunityLineItemTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOpportunityLineItemTriggerHelper', 'afterUpdateMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }
    }
    
    public static void afterDeleteMethod(list<opportunityLineITem> oldOliList,Map<id,opportunityLineITem> oldOliMap){
        try{
            boolean deleteflag = CheckRecursiveForOLI.runOnceAfterDelete();
            System.debug('flagflag==='+deleteflag);
            if(deleteflag){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('delteHelperMethod') && MapofProjectTrigger.get('delteHelperMethod').HelperName__c){
                    OpportunityLineItemTriggerHelper.deleteHelperMethod(oldOliList);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('deleteOpportunityTeamMember') && MapofProjectTrigger.get('deleteOpportunityTeamMember').HelperName__c){
                    OpportunityLineItemTriggerHelper.deleteOpportunityTeamMember(oldOliList); 
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateOpportunityFields') && MapofProjectTrigger.get('updateOpportunityFields').HelperName__c){
                    OpportunityLineItemTriggerHelper.updateOpportunityFields(oldOliList);  
                }
            }
        }catch(Exception e){  system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber()); CreateErrorLog.createErrorRecord('MainOpportunityLineItemTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOpportunityLineItemTriggerHelper', 'afterDeleteMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }
    }
    
    public static void afterUndeleteMethod(){
        integer i =0;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
         i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
    }
    
}