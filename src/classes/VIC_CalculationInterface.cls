/*
 * @author: Prashant Kumar1
 * @since: 22/05/2018
 * @description: This interface is used in classes related to calculation
 * based on plans (plan code)
 * 
*/
public interface VIC_CalculationInterface {
    /*
     * @params: OpportunityProduct(OpportunityLineItem),
     * 			Plan Code(String)
     * @return: Incentive amount(Decimal)
     * @description: calculate 100% of incentive for a plan component of specific plan.
     * 	User wise split is not calculated by this method.
    */
    Decimal calculate(OpportunityLineItem oppProduct, String planCode);
}