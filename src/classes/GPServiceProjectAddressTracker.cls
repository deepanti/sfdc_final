//================================================================================================================
//  Description: Test Class for GPServiceProjectAddress
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPServiceProjectAddressTracker {
    Public Static List<GP_Project_Address__c> listOfAddress = new List<GP_Project_Address__c>();
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    Public Static GP_Address__c objaddress;
    Public Static GP_Address__c objAdressQuery;
    Public Static GP_Pinnacle_Master__c objpinnacleMaster;
    
    public static void LoadData()
    {
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;
        
        //GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        //insert exceptionemp;
        
        //GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        //empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        //insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        
        
        customerObj = new GP_Customer_Master__c();
        customerObj.Name = 'New Customer';
        customerObj.GP_Oracle_Customer_Id__c = '1234';
        customerObj.GP_Is_Dummy__c = true;
        
        GP_Pinnacle_Master__c pinnacleObj = GPCommonTracker.GetpinnacleMaster();
        pinnacleObj.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        pinnacleObj.GP_Oracle_Id__c = '1234';
        insert pinnacleObj;
        
        objaddress = new GP_Address__c();
        objaddress.GP_Address_Line_1__c = 'New Delhi';
        objaddress.GP_Billing_Entity_Id__c ='1234';
        objaddress.GP_Customer_Account_ID__c = '1234';
        objaddress.GP_Customer__c = customerObj.id;
        objaddress.GP_Billing_Entity__c = objpinnacleMaster.id;
        objaddress.GP_Site_Use_Code__c = 'SHIP_TO';
        objaddress.GP_Status__c = 'Active';
        insert objaddress;
        
        objAdressQuery = [Select id, name, GP_Address_Line_1__c, 
                          GP_Billing_Entity_Id__c, GP_Customer_Account_ID__c from GP_Address__c limit 1];
    }
    
    @isTest
    public static void testGPServiceProjectAddress() {
        test.startTest();
        LoadData();
        insert prjobj;
        insert customerObj;
        GP_Project_Address__c objPrjAddress = new GP_Project_Address__c();
        objPrjAddress.GP_Project__c = prjobj.id;
        objPrjAddress.GP_Customer_Name__c = customerObj.id;
        insert objPrjAddress;
        
        GP_Project_Address__c objPrjAddress2 = new GP_Project_Address__c();
        objPrjAddress2.GP_Project__c = prjobj.id;
        objPrjAddress2.GP_Bill_To_Address__c = objAdressQuery.id;
        objPrjAddress2.GP_Ship_To_Address__c = objAdressQuery.id;
        objPrjAddress2.GP_Customer_Name__c = customerObj.id;
        insert objPrjAddress2;
        
        objPrjAddress.GP_Customer_Name__r = new GP_Customer_Master__c(GP_Is_Dummy__c = true);

        listOfAddress = [Select id, GP_Project__c, GP_Bill_To_Address__c,GP_Relationship__c,GP_Customer_Name__c,
                         GP_Bill_To_Address__r.Name, GP_Customer_Name__r.GP_Is_Dummy__c,GP_Ship_To_Address__c ,
                         GP_Ship_To_Address__r.Name, GP_Last_Temporary_Record_Id__c
                         from GP_Project_Address__c where id=:objPrjAddress.id or id=:objPrjAddress2.id];
        //listOfAddress.add(objPrjAddress);
        //listOfAddress.add(objPrjAddress2);
        GPWrapFieldValue objWrapFieldValue = new GPWrapFieldValue();
        objWrapFieldValue.strStageName = 'test Stage';
        String userDefinedStg = JSON.serialize(objWrapFieldValue);
        GPServiceProjectAddress.userDefinedStage = userDefinedStg;
        GPServiceProjectAddress.validateProjctAddress(prjobj,listOfAddress);
        
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        prjobj.RecordType = new RecordType();
        prjobj.RecordType.Name = 'Indirect PID';
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        test.stopTest();
    }
    
     @isTest
    public static void testGPServiceProjectAddress2() {
        LoadData();
        insert prjobj;
        insert customerObj;
        GP_Project_Address__c objPrjAddress = new GP_Project_Address__c();
        objPrjAddress.GP_Project__c = prjobj.id;
        objPrjAddress.GP_Relationship__c = 'Direct';
        objPrjAddress.GP_Bill_To_Address__c = objaddress.id;
        insert objPrjAddress;
        objPrjAddress.GP_Customer_Name__r = new GP_Customer_Master__c();
        objPrjAddress.GP_Customer_Name__r.GP_Is_Dummy__c = true;
        listOfAddress.add(objPrjAddress);
        GPWrapFieldValue objWrapFieldValue = new GPWrapFieldValue();
        objWrapFieldValue.strStageName = 'test Stage';
        String userDefinedStg = JSON.serialize(objWrapFieldValue);
        GPServiceProjectAddress.userDefinedStage = userDefinedStg;
        GPServiceProjectAddress.validateProjctAddress(prjobj,listOfAddress);
        
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
    }
    
     @isTest
    public static void testGPServiceProjectAddress3() {
        LoadData();
        insert prjobj;
        insert customerObj;
        GP_Project_Address__c objPrjAddress = new GP_Project_Address__c();
        objPrjAddress.GP_Project__c = prjobj.id;
        objPrjAddress.GP_Bill_To_Address__c = objaddress.id;
        insert objPrjAddress;
        listOfAddress.add(objPrjAddress);
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPWrapFieldValue objWrapFieldValue = new GPWrapFieldValue();
        objWrapFieldValue.strStageName = 'test Stage';
        String userDefinedStg = JSON.serialize(objWrapFieldValue);
        GPServiceProjectAddress.userDefinedStage = userDefinedStg;
        GPServiceProjectAddress.validateProjctAddress(prjobj,listOfAddress);
        
        
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
    }
    
     @isTest
    public static void testGPServiceProjectAddress4() {
        LoadData();
        insert prjobj;
        insert customerObj;
        GP_Project_Address__c objPrjAddress = new GP_Project_Address__c();
        objPrjAddress.GP_Customer_Name__c = customerObj.id;
        objPrjAddress.GP_Project__c = prjobj.id;
        objPrjAddress.GP_Bill_To_Address__c = objaddress.id;
        insert objPrjAddress;
        listOfAddress.add(objPrjAddress);
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPWrapFieldValue objWrapFieldValue = new GPWrapFieldValue();
        objWrapFieldValue.strStageName = 'test Stage';
        String userDefinedStg = JSON.serialize(objWrapFieldValue);
        GPServiceProjectAddress.userDefinedStage = userDefinedStg;
        GPServiceProjectAddress.validateProjctAddress(prjobj,listOfAddress);
        
        
        GPServiceProjectAddress.isFormattedLogRequired = false;
        GPServiceProjectAddress.isLogForTemporaryId = true;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
        
        GPServiceProjectAddress.isLogForTemporaryId = false;
        GPServiceProjectAddress.validateProjctAddress(prjobj, listOfAddress);
    }
}