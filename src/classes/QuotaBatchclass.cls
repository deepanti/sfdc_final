/*-------------
        Class Description : This Class is a batch class for quota attainment purposes.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 11 June 2014  
        Last modified date: 2 September 2014
---------------*/
global class QuotaBatchclass implements Database.Batchable<sObject>
{
    global final String Query;
    
    Set<ID> quotajuncids=new Set<ID>();
    
    //To maintain Quota Year
    String Quota_Year = String.valueof(System.today().year());
   
    // To maintain all failed records with error message
    global List<String> ListOfErrorMessages;
  
    // counter to count records getting failed
    global integer counter = 0 ;
      
    // Constructor    
   global QuotaBatchclass (String q)
   {
   
        ListOfErrorMessages = new List<String>();
        //Query=q;
        String StrQuery ='';
        Map<String, Opportunity_timestamp__c> opptytimestamp = Opportunity_timestamp__c.getAll();
        List<QuotaLineOpportunityJunction__c> quotajunc=[SELECT id,Products_Revenue__c FROM QuotaLineOpportunityJunction__c WHERE Revaluate__c=true AND Quota_header__r.year__c=:Quota_Year];
        
        
        StrQuery='SELECT Id,SalesExecutive__c,Total_ACV__c,Total_TCV__c,Total_CYR__c,Opportunityid__r.ownerid,Opportunityid__r.account.Primary_Account_GRM__c,Opportunityid__r.account.Client_Partner__c FROM OpportunityProduct__c  WHERE Opportunityid__r.StageName=\'6. Signed Deal\'  AND  Opportunityid__r.account.Business_Group__c != \'Genpact\' AND Opportunityid__r.Target_Source__c != \'Renewal\' AND CALENDAR_YEAR(Opportunityid__r.Actual_Close_Date__c)= '+Quota_Year;
        for(QuotaLineOpportunityJunction__c qtajunc : quotajunc)
        {   
            quotajuncids.add(qtajunc.Products_Revenue__c);          
        }
        system.debug('quotajuncids==='+quotajuncids.size());
        
        //if(quotajuncids.size()>0)
            //StrQuery=StrQuery+' AND (Id IN :quotajuncids';
            
        if(opptytimestamp.keyset().size()>0)
        {
            Datetime tstamp = opptytimestamp.get('Tstamp').timestamp__c;
            
            Boolean FullQuota_Bool = opptytimestamp.get('Tstamp').Full_Quota_Run__c;
            
            system.debug('tstamp=='+tstamp+'   '+FullQuota_Bool);
            
            String formatedDt = tstamp.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            
            Datetime Fullquotadate =  datetime.newInstance(System.today().year(), 1, 1, 12, 01, 00);

            String formatedDt2 = Fullquotadate.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');            
        
            system.debug('formatedDt==formatedDt2=='+formatedDt+'       '+formatedDt2);        
        
            if(FullQuota_Bool)                      // Check if Full Quota Run setting has been enabled.
                formatedDt = formatedDt2;
        
            //need to fetch timestamp from custom setting and add filter under below query  
            if(quotajuncids.size()>0)
                StrQuery=StrQuery+' AND (Id IN :quotajuncids'+' OR (Opportunityid__r.LastModifieddate>'+formatedDt+' OR Opportunityid__r.Account.LastModifieddate>'+formatedDt+' OR LastModifieddate>'+formatedDt+'))';  
            else
                StrQuery=StrQuery+' AND (Opportunityid__r.LastModifieddate>'+formatedDt+' OR Opportunityid__r.Account.LastModifieddate>'+formatedDt+' OR LastModifieddate>'+formatedDt+')';
        }
        
        StrQuery=StrQuery+' order by Opportunityid__r.LastModifieddate';     
        system.debug('strquery=='+StrQuery);
      
      
      Query=StrQuery;
      
      system.debug('Query=='+Query);
      
      //Flushing out all the attainments before revalutation 
      //Delete [select id from QuotaLineOpportunityJunction__c where Quota_header__r.year__c=:Quota_Year];
        Integer i=0;
        list<QuotaLineOpportunityJunction__c> attainmentdeletelist = [select id from QuotaLineOpportunityJunction__c where Quota_header__r.year__c=:Quota_Year];
        list<QuotaLineOpportunityJunction__c> RecToDel = new list<QuotaLineOpportunityJunction__c>();
        for(QuotaLineOpportunityJunction__c thing : attainmentdeletelist) 
        {
            i++;
            if(i==9999)        // checking record count is equal to 10k
            {   
                System.debug('errrrrrr111=='+i+'   '+RecToDel.size());
                Delete RecToDel;
                i=0;
                RecToDel = new list<QuotaLineOpportunityJunction__c>();
            }
            else
                RecToDel.add(thing);
        }
        if(RecToDel.size()>0)
        {   
            System.debug('errrrrrr22=='+i+ '  '+RecToDel.size());
            Delete RecToDel;    
        }
   }
    
   // Process Launching method.This method gets executed only once at beginning of Batch Process    
   global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
   {    
      return Database.getQueryLocator(query);
   }

   // method to execute logic for each Batch 
   global void execute(Database.BatchableContext BC, List<sObject> scope)           // Execute Logic    
   {    
      try
      {
        system.debug('inside execute method=='+scope.size());
        if(scope.size()>0)
        {
            String acc_grm,acc_cp,oliowner,opptyowner='';       // strings to hold 4 owners(GRM,CP,Oppty,OLi)           
            Set<String> userlist=new Set<String>();
            Set<String> finaluserlist=new Set<String>();
            Set<String> nonparticipentlistids=new Set<String>();
            system.debug('scope=='+scope);
            OpportunityProduct__c oliobj=(OpportunityProduct__c)scope[0];
            system.debug('oppobject.Opportunity_Products__r=='+oliobj);
            
            //Delete [select id from QuotaLineOpportunityJunction__c where Products_Revenue__c=:oliobj.id];
            
            /*
            //adding account CP owner
            if(oliobj.Opportunityid__r.account.Client_Partner__c!=null)
            {
                acc_cp=String.valueof(oliobj.Opportunityid__r.account.Client_Partner__c);
                userlist.add(acc_cp);
            }*/
            
            //adding account GRM owner
            if(oliobj.Opportunityid__r.account.Primary_Account_GRM__c!=null)
            {
                acc_grm=String.valueof(oliobj.Opportunityid__r.account.Primary_Account_GRM__c);
                userlist.add(acc_grm);
            }
            
            //adding oppty owner
            opptyowner=String.valueof(oliobj.Opportunityid__r.ownerid);
            userlist.add(opptyowner);
            
            //adding OLI sales executive
            if(oliobj.SalesExecutive__c!=null)
            {
                oliowner=String.valueof(oliobj.SalesExecutive__c);
                userlist.add(oliowner); 
            }
            System.debug('userlist=='+userlist);
            
            
            for(String uid:userlist)            //fetching all managers upto 7 level for above 4 owners
            finaluserlist.addALL(new CalculateAttainment().getusermanagerlist(uid));
            
            System.debug('finaluserlist=='+finaluserlist.size()+'  '+finaluserlist);
            
            for(String fuid:finaluserlist)      //providing attainment to the owners
            {
                String rolename='';
                if(fuid==acc_grm)             rolename='Enterprise GRM';
                //else  if(fuid==acc_cp)        rolename='Account CP';
                else  if(fuid==opptyowner)    rolename='Opportunity Owner';
                else  if(fuid==oliowner)      rolename='Product BD Rep';
                else                          rolename='Manager';
                system.debug('CalculateAttainment====='+fuid+'     '+rolename);
                new CalculateAttainment().AddQuotaAttainment(fuid,oliobj.id,rolename,Quota_Year,'Account_GRM');
            }
            
            
            //After providing attainment,provide attainment to non participiant salesleader who have not got attainment above.
            
            list<Set_Quotas__c> nonparticipantsqt=new List<Set_Quotas__c>([select Quota_Header__r.Sales_Person__c from Set_Quotas__c where Quota_Header__r.Sales_Person__c NOT IN :finaluserlist AND recordtype.developername!='Account_GRM' AND Quota_header__r.year__c=:Quota_Year]);
            
            //get all the nonparticipant user ids
            for(Set_Quotas__c sqt:nonparticipantsqt)
            nonparticipentlistids.add(sqt.Quota_Header__r.Sales_Person__c);
            
            System.debug('nonparticipentlistids=='+nonparticipentlistids.size()+'  '+nonparticipentlistids);
            
            // providing attainment to non participiant owners.
            for(String ids:nonparticipentlistids)
            new CalculateAttainment().AddQuotaAttainment(ids,oliobj.id,'Sales Leader',Quota_Year,'Opportunity_Line_Owner');
        }    
      }
      catch(Exception e)      
      {
        system.debug('eeeeee==='+e);
        counter++;
        ListOfErrorMessages.add(e.getmessage());
      }
   }
   
    // Logic to be Executed at finish
   global void finish(Database.BatchableContext BC)
   {
        
        //Sending an email to Admin if any error occurs
        if(ListOfErrorMessages.size()>0)
        {
            String body = 'Hi ,Quota Batch Process has been completed.\n';
            body += 'TOtal Batch failed ==>'+counter+' Following Error were addressed:';
       
            for(String msg : ListOfErrorMessages)
            {
                body += 'Error message ==> '+msg+'';
            }           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(userInfo.getuserId());
            mail.setSubject('Batch Compeletion Report');      
            mail.setSaveAsActivity(false);
            mail.setHtmlBody(body);
            Messaging.SendEmail(new Messaging.SingleEmailMessage[] { mail });           
        }
        
        //updating recent opportunity timestamp in the custom setting        
        List<Opportunity> opp=new list<Opportunity>();
        
        opp=[SELECT LastModifiedDate FROM Opportunity where StageName='6. Signed Deal' order by LastModifiedDate  DESC limit 1];
        
        if(opp.size()>0)
        {
            Datetime tstamp= opp[0].LastModifiedDate;
    
            system.debug('tstamp=='+tstamp);
        
            String formatedDt = tstamp.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        
            system.debug('formatedDt=='+formatedDt);
            
            List<Opportunity_timestamp__c> configEntry = [Select Id, timestamp__c From Opportunity_timestamp__c Where Name = 'Tstamp'];
            if(configEntry.size()>0)
            {   
                configEntry[0].timestamp__c = tstamp;  
                update configEntry[0]; 
            }
        }    
   }
}