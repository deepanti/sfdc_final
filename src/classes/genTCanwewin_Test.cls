@istest
public class genTCanwewin_Test {
 public static Opportunity opp;
    public static User u;
    public static QSRM__c qsrm,qsrm1,qsrm2,qsrm3,qsrm4,qsrm5,qsrm6,qsrm7,qsrm8,qsrm9;
    public static Map<String, String> pickListMap= new Map<String, String>();
    public static Account oAccount;
    public static Contact oContact;
    public static Id QSRMrecordTypeId;
    
    public static testmethod void KpResponseApproveTest(){
        Test.startTest();
        setupTestData();
      opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp1;
     qsrm1 = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp1.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id);
     insert qsrm1; 
        Test.stopTest();
    }
    public static testmethod void validateTestM1(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecordTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
       
      Test.stopTest();
    }
 
    private static void setupTestData(){
        list<qsrm__c> qsrmLst = new list<qsrm__c>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );           
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        List<Schema.PicklistEntry> ple = QSRM__c.Service_Line_leader_SL_Approval__c.getDescribe().getPicklistValues();
        System.debug('ple=='+ple);
        for( Schema.PicklistEntry f : ple){
       
          pickListMap.put(f.getLabel(), f.getValue());
        }
         System.debug('pickListMap=='+pickListMap);
        
        System.runAs(u){
            // test.startTest();
             oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
             oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp =new opportunity(name='1234',StageName='1. Discover',QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,Amount=1000000,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            
            QSRMrecordTypeId = Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('TS QSRM').getRecordTypeId();
            qsrm = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id);
           
            
            qsrmLst.add(qsrm);
            insert qsrmLst;
        }
    } 
}