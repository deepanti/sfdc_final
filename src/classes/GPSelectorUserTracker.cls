@isTest
public class GPSelectorUserTracker {
 
    @isTest
    public static void testgetSObjectType() {
        GPSelectorUser userSelector = new GPSelectorUser();
        Schema.SObjectType returnedType = userSelector.getSObjectType();
        Schema.SObjectType expectedType = User.sObjectType;
        
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectById() {
        GPSelectorUser userSelector = new GPSelectorUser();
        List<User> listOfReturnedUser = userSelector.selectById(new Set<Id> {
            UserInfo.getUserId()
                });
        
        System.assertEquals(listOfReturnedUser.size(), 1);
    }
}