@isTest
public class OpportunityPrediscoverHelperTest {
     static testMethod void method1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        User u = new User(Alias = 'standt1', Email='standarduser12016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1gg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
       
        opportunity opp=new opportunity(name='1234',StageName=' Prediscover',CloseDate=system.today(),accountid=accountobject.id, NextStep='test');
        insert opp;
        
        
        opportunity opp1=new opportunity(name='12345',StageName='Prediscover',CloseDate=system.today(),accountid=accountobject.id);
        insert opp1;
        
        
        opp1.NextStep = 'test2';
        update opp1;
         map<id,opportunity> oliMap = new map<id, opportunity>();
        oliMap.put(opp1.id,opp1);
        Task tk= new Task(Subject='Next Step Note', WhatId=opp1.Id, Status__c='Not Started',AutocreatedTask__c=false);
        insert tk;
        
        List<task> task_lst = new  List<task>();
        task_lst = [SELECT id,Subject, Status,AutocreatedTask__c  FROM task WHERE whatid=:opp1.id];                     
         
        system.debug(' checking a task 1 '+task_lst );
        
        List<task> task_lst2 = new  List<task>();
        task_lst2 = [SELECT id,Subject, Status,AutocreatedTask__c  FROM task WHERE whatid=:opp1.id];      
        
        opp1.NextStep = 'test4';
        update opp1;
        system.debug(' checking task 2 '+task_lst2 ); 
       list<opportunity> oliList = new list<opportunity>();
        oliList.add(opp1);
       
        OpportunityPrediscover_RemoveTasksHelper.Prediscover_RemoveTasksMethod(oliList,oliMap);
        
        
        test.stopTest();      
    }

}