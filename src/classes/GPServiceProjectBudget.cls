/**
 * @group ServiceLayer. 
 *
 * @description Service layer for Project Budget.
 */
public with sharing class GPServiceProjectBudget {

    private static final String REQUIRED_FIELDS_ERROR_LABEL = 'Band/Country/Product/Effort can not be empty';
    private static final String UNIQUE_ENTRY_ERROR_LABEL = 'Band/Country/Product/Effort Should be unique';

    public static Boolean isFormattedLogRequired = true;
    public static Boolean isLogForTemporaryId = true;
    public static GP_Project__c project;

    public class ProjectBudgetException extends Exception {}

    public static string UpdateProjectBudget(string projectID, list < GP_Project_Budget__c > lstofOmsProjectBudget) {
        map < string, GP_Project_Budget__c > mapofOmsIDToPB = new map < string, GP_Project_Budget__c > ();
        map < string, GP_Project_Budget__c > mapofOmsIDofSychToPB = new map < string, GP_Project_Budget__c > ();

        list < GP_Project_Budget__c > lstofProjectBudget = new list < GP_Project_Budget__c > ();
        list < GP_Project_Budget__c > lstofProjBudforSoftDel = new list < GP_Project_Budget__c > ();

        if (lstofOmsProjectBudget != null && lstofOmsProjectBudget.size() > 0) {
            for (GP_Project_Budget__c objPB: lstofOmsProjectBudget) {
                mapofOmsIDofSychToPB.put(objPB.GP_OMS_Id__c, objPB);
            }
        }


        for (GP_Project_Budget__c objPB: new GPSelectorProjectBudget().selectProjectBudgetRecords(projectID)) {
            mapofOmsIDToPB.put(objPB.GP_OMS_Id__c, objPB);
            if (!mapofOmsIDofSychToPB.containsKey(objPB.GP_OMS_Id__c)) {
                objPB.GP_IsSoftDeleted__c = true;
                lstofProjBudforSoftDel.add(objPB);
            }
        }

        if (lstofOmsProjectBudget != null && lstofOmsProjectBudget.size() > 0) {
            for (GP_Project_Budget__c objPB: lstofOmsProjectBudget) {
                GP_Project_Budget__c obj;
                if (mapofOmsIDToPB != null && mapofOmsIDToPB.size() > 0 && mapofOmsIDToPB.containsKey(objPB.GP_OMS_Id__c)) {
                    obj = new GP_Project_Budget__c(Id = mapofOmsIDToPB.get(objPB.GP_OMS_Id__c).Id);
                    obj.GP_Band__c = objPB.GP_Band__c;
                    obj.GP_Cost_Rate__c = objPB.GP_Cost_Rate__c;
                    obj.GP_Country__c = objPB.GP_Country__c;
                    obj.GP_TCV__c = objPB.GP_TCV__c;
                    obj.GP_OMS_Id__c = objPB.GP_OMS_Id__c;
                    obj.GP_Total_Cost__c = objPB.GP_Total_Cost__c;
                    lstofProjectBudget.add(obj);
                } else {
                    obj = new GP_Project_Budget__c();
                    obj.GP_Band__c = objPB.GP_Band__c;
                    obj.GP_Cost_Rate__c = objPB.GP_Cost_Rate__c;
                    obj.GP_Country__c = objPB.GP_Country__c;
                    obj.GP_TCV__c = objPB.GP_TCV__c;
                    obj.GP_Total_Cost__c = objPB.GP_Total_Cost__c;
                    obj.GP_OMS_Id__c = objPB.GP_OMS_Id__c;
                    obj.GP_Project__c = projectID;
                    lstofProjectBudget.add(obj);
                }
            }
        }

        if (lstofProjectBudget != null && lstofProjectBudget.size() > 0) {
            upsert lstofProjectBudget;
        }

        if (lstofProjBudforSoftDel != null && lstofProjBudforSoftDel.size() > 0) {
            update lstofProjBudforSoftDel;
        }
        return 'Suceess';
    }

    public class GPServiceProjectBudgetException extends Exception {}

    /**
     * @description Validate Project Budget.
     * 
     * @param listOfProjectBudget : List of Project Budget that needs to be validated.
     *
     * @return Map < String, List < String >> : Return Map of Sub Section with List of errors.
     */
    public static Map < String, List < String >> validateProjectBudget(List < GP_Project_Budget__c > listOfProjectBudget) {
        Map < String, List < String >> mapOfSubSectionToListOfErrors = new Map < String, List < String >> ();
        Map < String, Boolean > mapOfUniqueCombination = new Map < String, Boolean > ();

        String validationMessage = '', key;

        if (listOfProjectBudget != null && listOfProjectBudget.size() > 0) {
            for (GP_Project_Budget__c projectBudget: listOfProjectBudget) {

                if (isFormattedLogRequired) {
                    key = 'Project Budget';
                } else if (isLogForTemporaryId) {
                    key = projectBudget.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = projectBudget.Id;
                }
                if(projectBudget.GP_Data_Source__c != 'OMS'){
                String uniqueCombination = projectBudget.GP_Band__c + '-';
                uniqueCombination += projectBudget.GP_Country__c + '-';
                uniqueCombination += projectBudget.GP_Effort_Hours__c + '-';
                uniqueCombination += projectBudget.GP_Product__c + '-';

                //validate for duplicacy
                if (!mapOfUniqueCombination.containsKey(uniqueCombination)) {
                    mapOfUniqueCombination.put(uniqueCombination, true);
                } else {
                    validationMessage = projectBudget.Name + ': ' + UNIQUE_ENTRY_ERROR_LABEL;

                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }

                //validate for mandatory fields
                if (!isValidProjectBudget(projectBudget)) {
                    validationMessage = projectBudget.Name + ': ' + REQUIRED_FIELDS_ERROR_LABEL;

                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }
            }
            }
        }

        return mapOfSubSectionToListOfErrors;
    }

    /**
     * @description Validate Each Project Budget.
     * 
     * @param projectBudgetObj : Project Budget Record that needs to be validated.
     *
     * @return Boolean : Return Required fields Budget record.
     */
    private static Boolean isValidProjectBudget(GP_Project_Budget__c projectBudgetObj) {
        return projectBudgetObj != null &&
            projectBudgetObj.GP_Band__c != null &&
            projectBudgetObj.GP_Country__c != null &&
            projectBudgetObj.GP_Effort_Hours__c != null &&
            projectBudgetObj.GP_Product__c != null &&
            projectBudgetObj.GP_Project__c != null;
    }
}