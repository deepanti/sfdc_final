@isTest
public class GPLWCTrackerCustomComponent {
    @testSetup
    static void createTestData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_SOA_External_Id__c = '15022';
        objpinnacleMaster.Name = 'NET 33 Days';
        objpinnacleMaster.GP_Description__c = 'Amount due in 33 days';
        objpinnacleMaster.GP_Value_In_Use__c = 'Y';
        insert objpinnacleMaster ;
    }
    
    @isTest
    static void testFetchLookupRecords() {
        GPLWCServiceCustomComponent.fetchLookupRecords('GP_Pinnacle_Master__c', '15022', 'GP_SOA_External_Id__c', 'GP_Status__c = \'Active\'', '--', 'Name', 'lookup');
        GPLWCServiceCustomComponent.fetchLookupRecords('GP_Pinnacle_Master__c', '15022', 'GP_SOA_External_Id__c', 'GP_Status__c = \'Active\'', '--', 'Name', 'other');
    }
    
    @isTest
    static void testFetchPicklistValues() {
        GPLWCServiceCustomComponent.fetchPicklistValues('GP_Project__c', 'GP_Clone_Source__c', 'Manual');
    }
    
    @isTest
    static void testFetchFieldSetPerObject() {
        GPLWCServiceCustomComponent.fetchFieldSetPerObject('GP_All_Fields', 'GP_Project__c');
    }
}