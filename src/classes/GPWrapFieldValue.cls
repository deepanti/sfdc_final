/**
 * @group Project Approval. 
 *
 * @author Adarsh
 *
 * @description This class is used in GPStageWiseFieldProject for getting the Stage value with respect to the field entered.
 */
public with sharing class GPWrapFieldValue {
    public Map < String, Map < String, List < String >>> mapOfSectionToMapOfSubSectionToListOfError;
    public list < string > lstFields;
    public string strStageName;
    public boolean isSuccess;
    public string strMessage;
    public Boolean isRevenueStageValid = true;
}