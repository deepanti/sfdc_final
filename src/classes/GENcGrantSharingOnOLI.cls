/*
    used in GENtCalculateAndUpdateOpportunity  trigger in now no more required please delete the same
*/

global class GENcGrantSharingOnOLI 
{
    
    public static void grantSharing(List<OpportunityProduct__c> oOP)
    {
    Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>();
    List<User> ActiveUsers = new list<User>([Select id from user where isactive=true]);
    Map<id,User> sActiveUser= new Map<id,User>();
    for(User oUser: ActiveUsers )
    sActiveUser.put(oUser.id, oUser);
    Map<Id, Id> accountOwnerMap = new Map<Id, Id>();
    Map<Id, Id> accountGRMMap = new Map<Id, Id>();
    Map<Id, Id> accountClientPartnerMap = new Map<Id, Id>();
    Map<string, AggregateResult> oppProductAggregateResultMap = new Map<string, AggregateResult>();
    Set<Id> opportunityIds = new Set<Id>();
    Set<Id> accountIds = new Set<Id>();
    List<Opportunity> oppList = new List<Opportunity>();
    List<Opportunity> oppWithProducts = new List<Opportunity>();

    for(OpportunityProduct__c rec: oOP){
            opportunityIds.add(rec.OpportunityId__c);
        }

    oppList = [Select id, name, OwnerId, Account.Id, Account.OwnerId, Account.Client_Partner__c, Account.Primary_Account_GRM__c from Opportunity where id IN:opportunityIds];
    for(Opportunity o: oppList) {
        accountIds.add(o.Account.Id);
        opportunityMap.put(o.Id, o);
        accountGRMMap.put(o.Id, o.Account.Primary_Account_GRM__c);
        accountOwnerMap.put(o.Id, o.Account.OwnerId);
        accountClientPartnerMap.put(o.Id, o.Account.Client_Partner__c);
    }
    List<OpportunityShare> oppMembers = [SELECT Id,OpportunityId,OpportunityAccessLevel,UserOrGroupId FROM OpportunityShare WHERE OpportunityId in: opportunityIds AND UserOrGroupId in:sActiveUser.keyset()];
    List<AccountShare> accMembers = [SELECT Id,AccountAccessLevel,Accountid,OpportunityAccessLevel,UserOrGroupId FROM AccountShare WHERE AccountId in: accountIds AND UserOrGroupId in:sActiveUser.keyset()];
    Map<id,Set<id>> accessOwnerMap= new Map<id,Set<id>>();
    
    for(Opportunity opp: oppList) {
    Set<id> oppMemList = new Set<id>();
        for(OpportunityShare oShare: oppMembers)
        {
            if(opp.id== oShare.OpportunityId)
                oppMemList.add(oShare.UserOrGroupId);
        }
        for(AccountShare accShare: accMembers)
        {
            if(opp.Account.id== accShare.Accountid)
                oppMemList.add(accShare.UserOrGroupId);
        }
        accessOwnerMap.put(opp.id,oppMemList);
    }


            List<OpportunityProduct__Share> opptyShares  = new List<OpportunityProduct__Share>();
            List<OpportunityProduct__Share> opptySharesToBeInserted  = new List<OpportunityProduct__Share>();
            Set<OpportunityProduct__Share> opptySharesSet = new Set<OpportunityProduct__Share>();
            OpportunityProduct__Share opptyOwnerShare;
            OpportunityProduct__Share accountOwnerShare;
            OpportunityProduct__Share accountGRMShare;
            OpportunityProduct__Share accountClientPartnerShare;
            
            for(OpportunityProduct__c op: oOP){
                
                Opportunity oppInfo = opportunityMap.get(op.OpportunityId__c);
                
                if(op.OwnerId <> oppInfo.OwnerId && oppInfo.OwnerId!=null && sActiveUser.get(oppInfo.OwnerId)!=null){
                    opptyOwnerShare = new OpportunityProduct__Share();
                    opptyOwnerShare.ParentId = op.Id;
                    opptyOwnerShare.UserOrGroupId = oppInfo.OwnerId;
                    opptyOwnerShare.AccessLevel = 'edit';
                    opptyShares.add(opptyOwnerShare);
                }
                
                if(accountOwnerMap.get(op.OpportunityId__c)!=null && sActiveUser.get(accountOwnerMap.get(op.OpportunityId__c))!=null && oppInfo.OwnerId <> accountOwnerMap.get(op.OpportunityId__c) && op.OwnerId <> accountOwnerMap.get(op.OpportunityId__c)){
                    accountOwnerShare = new OpportunityProduct__Share();
                    accountOwnerShare.ParentId = op.Id;
                    accountOwnerShare.UserOrGroupId = accountOwnerMap.get(op.OpportunityId__c);
                    accountOwnerShare.AccessLevel = 'edit';
                    opptyShares.add(accountOwnerShare);
                }
                if(accountGRMMap.get(op.OpportunityId__c)!=null && sActiveUser.get(accountGRMMap.get(op.OpportunityId__c))!=null && oppInfo.OwnerId <> accountGRMMap.get(op.OpportunityId__c) && op.OwnerId <> accountGRMMap.get(op.OpportunityId__c) && accountOwnerMap.get(op.OpportunityId__c) <> accountGRMMap.get(op.OpportunityId__c)){
                    accountGRMShare = new OpportunityProduct__Share();
                    accountGRMShare.ParentId = op.Id;
                    accountGRMShare.UserOrGroupId = accountGRMMap.get(op.OpportunityId__c);
                    accountGRMShare.AccessLevel = 'edit';
                    opptyShares.add(accountGRMShare);
                }
                if(accountClientPartnerMap.get(op.OpportunityId__c)!=null && sActiveUser.get(accountClientPartnerMap.get(op.OpportunityId__c))!=null && oppInfo.OwnerId <> accountClientPartnerMap.get(op.OpportunityId__c) && op.OwnerId <> accountClientPartnerMap.get(op.OpportunityId__c) && accountOwnerMap.get(op.OpportunityId__c) <> accountClientPartnerMap.get(op.OpportunityId__c) && accountGRMMap.get(op.OpportunityId__c) <> accountClientPartnerMap.get(op.OpportunityId__c)){
                    accountClientPartnerShare = new OpportunityProduct__Share();
                    accountClientPartnerShare.ParentId = op.Id;
                    accountClientPartnerShare.UserOrGroupId = accountClientPartnerMap.get(op.OpportunityId__c);
                    accountClientPartnerShare.AccessLevel = 'edit';
                    opptyShares.add(accountClientPartnerShare);
                }
                if(accessOwnerMap.get(op.OpportunityId__c) != null)
                for(id oUserid: accessOwnerMap.get(op.OpportunityId__c))
                {    
                    if(op.SalesExecutive__c != oUserid)
                    {
                        OpportunityProduct__Share oliShare= new OpportunityProduct__Share();
                        oliShare.ParentId = op.Id;
                        oliShare.UserOrGroupId = oUserid;
                        oliShare.AccessLevel = 'edit';
                        opptyShares.add(oliShare);
                    }
                }

            }
            
            if(opptyShares.size() > 0 ) {
                opptySharesSet.addAll(opptyShares);
                opptySharesToBeInserted.addAll(opptySharesSet);
                system.debug('opptySharesToBeInserted'+opptySharesToBeInserted);
                Database.SaveResult[] lsr = Database.insert(opptySharesToBeInserted,false);
                
                Integer i=0;
                for(Database.SaveResult sr : lsr){
                    if(!sr.isSuccess()){  
                        Database.Error err = sr.getErrors()[0]; 
                        if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  &&  err.getMessage().contains('AccessLevel'))){
                            trigger.newMap.get(opptyShares[i].ParentId).addError('Unable to grant sharing access due to following exception: '+ err.getMessage());
                        }
                    }
                    i++;
                }
            }
    }
}