/**
 * @author Pankaj.Adhikari 
 * @date 03/02/2018
 *
 * @group OpportunityProject
 * @group-content ../../ApexDocContent/OpportunityProject.html
 *
 * @description Domain class for Opportunit Project,
 * 
 */
public without Sharing class GPDomainOpportunityProject extends fflib_SObjectDomain {
    private fflib_SObjectUnitOfWork uow;
    public Static boolean isOpportunityProjectUpdated = false;

    public GPDomainOpportunityProject(List < GP_Opportunity_Project__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainOpportunityProject(sObjectList);
        }
    }
    
    boolean checkOppProjectProb;

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Opportunity_Project__c.SObjectType,
            GP_Project_Classification__c.SobjectType
    };

    public override void onBeforeInsert() {}

    public override void onAfterInsert() {
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //shareOpportunityRecordToSDO(uow, null);
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateHighestTCV(oldSObjectMap);
        updateAccountDetails(oldSObjectMap);
        accountdetailonopportunityproject(oldSObjectMap);
        updatePushDate(oldSObjectMap);
        updateEPProjectNumber(oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        createRelatedRecods(oldSObjectMap);
        shareOpportunityRecordToSDO(uow, oldSObjectMap);
        updateEPProjectNumberClassification(uow, oldSObjectMap);
        uow.commitWork();
    }
    /**
     * @description Method to update Pushdate
     * @param trigger.oldmap
     * 
     * @example
     * new updatePushDate(trigger.oldmap);
     */
    private void updatePushDate(Map < Id, SObject > oldSObjectMap) {
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            GP_Opportunity_Project__c objoldOppProject = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.id) : null;

            if (checkOppProjectProb(objOppProject, objoldOppProject)) {
                objOppProject.GP_Pinnacle_Push_Date__c = system.today();
                // to stop recursion : While updating isUpdated false from workFlow 
                if (!GPCommon.isStopRecursion) {
                    objOppProject.GP_IsUpdated__c = true;
                }
            }
        }
    }

    private void updateAccountDetails(Map < Id, SObject > oldSObjectMap) {
        map < id, Account > mapofaccount = new map < id, Account > ();
        set < id > setofAccountId = new set < id > ();
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            GP_Opportunity_Project__c objoldOppProject = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.id) : null;

            // Update account details on isUpdated field too.
            // Avalara Change
            if (objOppProject.GP_IsUpdated__c && !isOpportunityProjectUpdated || 
                (objOppProject.GP_Oracle_PID__c != 'NA' 
                && objOppProject.GP_Customer_L4_Name__c != null 
                && objoldOppProject.GP_Customer_L4_Name__c != objOppProject.GP_Customer_L4_Name__c)) {
                setofAccountId.Add(objOppProject.GP_Customer_L4_Name__c);
            }
        }

        if (setofAccountId.size() > 0) {
            mapofaccount = new GPSelectorAccount().selectaccount(setofAccountId);

            for (GP_Opportunity_Project__c oppproj: (List < GP_Opportunity_Project__c > ) Records) {
                if (oppproj.GP_Customer_L4_Name__c != null && mapofaccount != Null && mapofaccount.get(oppproj.GP_Customer_L4_Name__c) != null) {
                    oppproj.GP_Business_Group_L1__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Group__c;
                    oppproj.GP_Business_Segment_L2__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Segment__r.Name;
                    oppproj.GP_Business_Segment_L2_Id__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Segment__r.Id;
                    oppproj.GP_Sub_Business_L3__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Sub_Business__r.Name;
                    oppproj.GP_Sub_Business_L3_Id__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Sub_Business__r.Id;

                    oppproj.GP_Pinnacle_Push_Date__c = system.today();
                    // to stop recursion : While updating isUpdated false from workFlow

                    if (!GPCommon.isStopRecursion) {
                        oppproj.GP_IsUpdated__c = true;
                    }
                }
            }
        }
    }
    /**
     * @description Method to check probability greater than 33 %
     * @param <new opportunity Project record>,<old opportunity Project record>
     * 
     * @example
     * new checkOppProjectProb(<new opportunity Project record>,<old opportunity Project record>);
     */
    private boolean checkOppProjectProb(GP_Opportunity_Project__c objOppProject, GP_Opportunity_Project__c objoldOppProject) {
        if (objOppProject.GP_Probability__c != null && objOppProject.GP_Probability__c >= Decimal.valueof(System.Label.GP_Opportunity_Probability) &&
            (objoldOppProject == null || (objOppProject.GP_Probability__c != objoldOppProject.GP_Probability__c &&
                objoldOppProject.GP_Probability__c < Decimal.valueof(System.Label.GP_Opportunity_Probability)))) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * @description: Method update Highest TCV values on Opportunity Project from OLIs.
     * @modifieddate: 04-04-2019.
     * @param <trigger old Map>
     * 
     * @example
     * new checkOppProjectProb(<trigger old Map>);
     */
	 // Avalara Change: DS:2nd Aug 19:
	 // Only consider OLI directly to get max TCV details.
	private void updateHighestTCV(Map < Id, SObject > oldSObjectMap) {
		Set <String> setOpportunityIDs = new Set <String> ();
        
        GP_Opportunity_Project__c objoldOppProject;
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            objoldOppProject = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.id) : null;
            
            // Update product details on isUpdated field too.
            // Avalara Change
            if (objOppProject.GP_IsUpdated__c && !isOpportunityProjectUpdated || 
                checkOppProjectProb(objOppProject, objoldOppProject)) {
                setOpportunityIDs.add(objOppProject.GP_Opportunity_Id__c);
            }
        }
		
		if(setOpportunityIDs != null && setOpportunityIDs.size() > 0) {
			Map<String, Opportunity> mapOfOpportunitiesWithMaxOLIs = new Map<String, Opportunity>();
			
            // Opportunity_Id__c is unique per Opportunity.
			for(Opportunity opp : [Select id, Opportunity_Id__c, 
										(Select id, Product2Id, TCV__c, Custom_Id__c,
										   Product2.Nature_of_Work__c, Product2.Service_Line__c, 
										   Opportunity.Account.Sub_Industry_Vertical__c, 
										   Opportunity.Account.Industry_Vertical__c 
										   from OpportunityLineItems Order by TCV__c DESC NULLS LAST limit 1) 
								from Opportunity where Opportunity_Id__c =: setOpportunityIDs]) 
			{
				mapOfOpportunitiesWithMaxOLIs.put(opp.Opportunity_Id__c, opp);
			}
			System.debug('==Map=='+mapOfOpportunitiesWithMaxOLIs);
            if(mapOfOpportunitiesWithMaxOLIs != null && mapOfOpportunitiesWithMaxOLIs.size() > 0) {
				for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
                    objoldOppProject = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.id) : null;
                    
                    // Check if probability is changed to more than 33%.
                    // Avalara Change
                    if (objOppProject.GP_IsUpdated__c && !isOpportunityProjectUpdated || checkOppProjectProb(objOppProject, objoldOppProject)) {
                        if (mapOfOpportunitiesWithMaxOLIs.containskey(objOppProject.GP_Opportunity_Id__c)
                            && mapOfOpportunitiesWithMaxOLIs.get(objOppProject.GP_Opportunity_Id__c) != null 
                            && mapOfOpportunitiesWithMaxOLIs.get(objOppProject.GP_Opportunity_Id__c).OpportunityLineItems.size() > 0) 
                        {
                            OpportunityLineItem maxOLI = mapOfOpportunitiesWithMaxOLIs.get(objOppProject.GP_Opportunity_Id__c).OpportunityLineItems[0];
                            System.debug('==oli='+maxOLI);
                            if(maxOLI != null) {
                                //GP_Industry_Vertical__c, GP_Product_Family__c, GP_Product_Name__c

                                objOppProject.GP_Vertical__c = maxOLI.Opportunity.Account.Industry_Vertical__c;
                                objOppProject.GP_Sub_Vertical__c = maxOLI.Opportunity.Account.Sub_Industry_Vertical__c;
                                objOppProject.GP_Service_Line__c = maxOLI.Product2.Service_Line__c;
                                objOppProject.GP_Nature_of_Work__c = maxOLI.Product2.Nature_of_Work__c;
                                objOppProject.GP_Product_Id__c = maxOLI.Product2Id;
                                objOppProject.GP_TCV__c = maxOLI.TCV__c;
                            }
                        }
                    }
                }
			}
		}
	}    
    /**
     * @description Method to create Opp project leaderShip , opp project Address and opportunity Classification.
     * @param trigger.oldmap
     * 
     * @example
     * new createRelatedRecods(trigger.oldmap);
     */
    private void createRelatedRecods(Map < Id, SObject > oldSObjectMap) {
        if (!GPCommon.isOpportunityProjectInsertionEvent) {
            GPCommon.isOpportunityProjectInsertionEvent = true;
        }
        
        // to stop recursion : While updating isUpdated false from workFlow 
        if (GPCommon.isStopRecursion) {
        	return;
        }
        //return;
        
        list < GP_Opportunity_Project__c > lstOpportunityProjectToUpdate = new list < GP_Opportunity_Project__c > ();
        GPResponseWrapper objGPResponseWrapper = new GPResponseWrapper();
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            GP_Opportunity_Project__c objoldOppProject = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.id) : null;

            if (checkOppProjectProb(objOppProject, objoldOppProject)) {
                lstOpportunityProjectToUpdate.add(objOppProject);
            }
        }

        if (lstOpportunityProjectToUpdate.size() > 0) {
            GPServiceOppProjectLeadership objServiceProjectLeaderShip = new GPServiceOppProjectLeadership(lstOpportunityProjectToUpdate);
            objGPResponseWrapper = objServiceProjectLeaderShip.createOppProjectLeadership();

            if (objGPResponseWrapper.code == -1) {
                Records[0].adderror(objGPResponseWrapper.message);
                return;
            }
            GPServiceOppProjectAddress objServiceProjectAddress = new GPServiceOppProjectAddress(lstOpportunityProjectToUpdate);
            objGPResponseWrapper = objServiceProjectAddress.createOppProjectAddress();

            if (objGPResponseWrapper.code == -1) {
                Records[0].adderror(objGPResponseWrapper.message);
                return;
            }

            GPServiceOppProjectClassification objServiceProjectClassification = new GPServiceOppProjectClassification(lstOpportunityProjectToUpdate);
            objGPResponseWrapper = objServiceProjectClassification.createProjectClassification();

            if (objGPResponseWrapper.code == -1) {
                Records[0].adderror(objGPResponseWrapper.message);
                return;
            }
            
            GPCommon.isStopRecursion = true;
        }
        /*if(!lstOpportunityProjectToUpdate.isEmpty()) {
            //update lstOpportunityProjectToUpdate;
        }*/
    }

    /**
     * @description method used generate new EP project number whenever isupdate checkbox is checked.
     * @param trigger.oldmap
     * 
     * @example
     * new updateEPProjectNumber(trigger.oldmap);
     */
    private void updateEPProjectNumber(map < id, Sobject > oldSObjectMap) {
        set < string > setOpportunityNumber = new set < string > ();
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            GP_Opportunity_Project__c objOldProj = (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.Id);
			// Avalara Change
            if (objOppProject.GP_IsUpdated__c && objOldProj.GP_IsUpdated__c == false && !isOpportunityProjectUpdated) {
                if (!String.isBlank(objOppProject.GP_EP_Project_Number__c)) {
                    setOpportunityNumber.add(objOppProject.GP_EP_Project_Number__c);
                }
            }
        }

        if (setOpportunityNumber != null) {
            set < String > setAutonumber = generateOpportunnityProjectNumber(setOpportunityNumber);
            System.debug('==setAutonumber@@=='+setAutonumber);
            list < String > lstAutoNumber = new list < String > (setAutonumber);
            for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
                GP_Opportunity_Project__c objOldProj = (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.Id);
				System.debug('==objOldProj@@=='+objOldProj.GP_EP_Project_Number__c);
                if (objOppProject.GP_IsUpdated__c && !objOldProj.GP_IsUpdated__c) {
                    if (!String.isBlank(objOppProject.GP_EP_Project_Number__c)) {
                        if (lstAutoNumber.size() > 0) {
                            objOppProject.GP_EP_Project_Number__c = lstAutoNumber[0];
                            lstAutoNumber.remove(0);
                            // Blank Oracle Status to blank wherever EP Project Number Changes changes
                            objOppProject.GP_Oracle_Status__c = '';
                            objOppProject.GP_Oracle_Process_Log__c = '';

                        }
                    } else {
                        objOppProject.GP_EP_Project_Number__c = objOppProject.GP_Unique_EP_Number__c;
                        // Blank Oracle Status to blank wherever EP Project Number Changes changes
                        objOppProject.GP_Oracle_Status__c = '';
                        objOppProject.GP_Oracle_Process_Log__c = '';
                    }
                }
                System.debug('==objOppProject@@=='+objOppProject);
            }
        }
    }

    /**
     * @description method used generate new EP project number whenever isupdate checkbox is checked.
     * @param trigger.oldmap
     * 
     * @example
     * new generateOpportunnityProjectNumber(fflib_SObjectUnitOfWork ,<set Opportunity Number>);
     */
    private set < String > generateOpportunnityProjectNumber(set < string > setOpportunityNumber) {
        savePoint s = database.setSavepoint();
        list < GP_Opportunity_Project__c > lstOpportunityProject = new list < GP_Opportunity_Project__c > ();
        set < id > setOpportunityProjectId = new set < id > ();
        set < String > setAutonumber = new set < String > ();
        for (string strAutonumber: setOpportunityNumber) {
            GP_Opportunity_Project__c objOppProject = new GP_Opportunity_Project__c(Name = strAutonumber);
            lstOpportunityProject.add(objOppProject);

        }
        if (lstOpportunityProject != null && !lstOpportunityProject.isEmpty()) {
            insert(lstOpportunityProject);

            for (GP_Opportunity_Project__c objProject: lstOpportunityProject) {
                setOpportunityProjectId.add(objProject.id);
            }
            if (setOpportunityProjectId.Size() > 0) {
                list < GP_Opportunity_Project__c > lstInsertOpportunityProject = new GPSelectorOpportunityProject().selectById(setOpportunityProjectId);
                for (GP_Opportunity_Project__c objProject: lstInsertOpportunityProject) {
                    setAutonumber.add(objProject.GP_Unique_EP_Number__c);
                }
            }
        }
        database.rollback(s);
        return setAutonumber;
    }
    /**
     * @description method used to update default information on Opportunity Project for meta data. Owner, SDO 
     * @param trigger.oldmap
     * 
     * @example
     * new accountDetailonOpportunityProject(trigger.oldmap);
     */
    private void accountDetailonOpportunityProject(Map < Id, SObject > oldSObjectMap) {
        Set < ID > setofAccountId = new Set < Id > ();
        Map < Id, Account > mapofaccount = new Map < Id, Account > ();
        String strOracleID = '';
        String strCreatorRecordID = '';
        for (GP_Opportunity_Project__c oppproj: (List < GP_Opportunity_Project__c > ) Records) {
            GP_Opportunity_Project__c objOldProj = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(oppproj.id) : null;
            
            if (checkOppProjectProb(oppproj, objOldProj)) {
                if (oppproj.GP_Customer_L4_Name__c != null)
                    setofAccountId.Add(oppproj.GP_Customer_L4_Name__c);
            }

        }

        if (setofAccountId.size() > 0) {
            mapofaccount = new GPSelectorAccount().selectaccount(setofAccountId);

            List < GP_Opp_Project_Default__mdt > lstmetadata = [select Id, GP_CLASS_CATEGORY__c, GP_Attribute1__c, GP_Attribute2__c, GP_Attribute3__c, GP_Attribute4__c, GP_Attribute5__c,
                GP_Attribute6__c, GP_Attribute7__c, GP_Attribute8__c, GP_Attribute9__c, GP_Attribute10__c, GP_Attribute11__c
                from GP_Opp_Project_Default__mdt
                l where GP_Record_Type__c = 'Opp Project'
            ];

            for (GP_Opportunity_Project__c oppproj: (List < GP_Opportunity_Project__c > ) Records) {
                if (oppproj.GP_Customer_L4_Name__c != null && mapofaccount != Null && mapofaccount.get(oppproj.GP_Customer_L4_Name__c) != null) {
                    oppproj.GP_Business_Group_L1__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Group__c;
                    oppproj.GP_Business_Segment_L2__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Segment__r.Name;
                    oppproj.GP_Business_Segment_L2_Id__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Business_Segment__r.Id;
                    oppproj.GP_Sub_Business_L3__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Sub_Business__r.Name;
                    oppproj.GP_Sub_Business_L3_Id__c = mapofaccount.get(oppproj.GP_Customer_L4_Name__c).Sub_Business__r.Id;

                    // Following values will be fetched from Custom Metadata "Opp Project Default"
                    if (!lstmetadata.isEmpty()) {
                        oppproj.GP_SDO_Oracle_Id__c = lstmetadata[0].GP_Attribute1__c; //Attribute 1
                        oppproj.GP_Customer_Account_Id__c = lstmetadata[0].GP_Attribute2__c; // Attribute 2
                        oppproj.GP_Oracle_Template_Id__c = lstmetadata[0].GP_Attribute3__c; // attribute 3
                        oppproj.GP_PINNACLE_CREATOR_PERSON_ID__c = lstmetadata[0].GP_Attribute4__c; //attribute 4
                        oppproj.GP_Project_Currency_Code__c = lstmetadata[0].GP_Attribute5__c; // Attribute 5
                        oppproj.GP_Project_Owning_Org_Id__c = lstmetadata[0].GP_Attribute6__c; //Attribute 6
                        oppproj.GP_Project_Status_Code__c = lstmetadata[0].GP_Attribute7__c; //Attribute 7
                        oppproj.GP_Attribute_Category__c = lstmetadata[0].GP_Attribute8__c; //Attribute 8
                        list<String> lstDate =  lstmetadata[0].GP_Attribute9__c.split('/');
                        oppproj.GP_Project_End_Date__c =  Date.newInstance(Integer.valueof(lstDate[2]),Integer.valueof(lstDate[0]),Integer.valueof(lstDate[1])); //attribute 9
                        oppproj.GP_Project_Start_Date__c = System.today(); //attribute 9
                        oppproj.GP_Allow_Cross_Charge_Flag__c = lstmetadata[0].GP_Attribute10__c; // Attribute 10
                        oppproj.GP_Shift_Allowed__c = lstmetadata[0].GP_Attribute11__c; //Attribute 11
                        oppproj.GP_Project_Category__c = lstmetadata[0].GP_CLASS_CATEGORY__c;
                    }
                    strOracleID = oppproj.GP_SDO_Oracle_Id__c;
                    strCreatorRecordID = oppproj.GP_PINNACLE_CREATOR_PERSON_ID__c;
                    //oppproj.GP_Project_Type__c = 'Opportunity'; // not required in Oracle mapping
                    //oppproj.GP_Project_Category__c = 'Opportunity'; // not required in Oracle mapping

                    string strOpportunityProjectName = oppproj.GP_Opportunity_Id__c + '_' + oppproj.GP_Opportunity_Name__c;
                    if (!string.isBlank(strOpportunityProjectName)) {
                        oppproj.GP_Project_Abbr_Name__c = strOpportunityProjectName.length() >= 30 ? strOpportunityProjectName.Left(30) : strOpportunityProjectName;
                        oppproj.GP_Project_Long_Name__c = strOpportunityProjectName.length() >= 30 ? strOpportunityProjectName.Left(30) : strOpportunityProjectName;
						// APC Change
						oppproj.GP_Project_Description__c = strOpportunityProjectName;
                        
                        oppproj.GP_Project_Name__c = strOpportunityProjectName.length() >= 30 ? strOpportunityProjectName.Left(30) : strOpportunityProjectName;
                    }
                }
            }
        }

        map < String, id > mpOracleIdtoSDO = new map < String, id > ();
        map < String, id > mpCreatorIDtoUser = new map < String, id > ();
        if (!String.IsBlank(strOracleID)) {

            list < GP_Work_Location__c > lstWorkLocation = new GPSelectorWorkLocationSDOMaster().selectByOracleIdofWOrkLocationAndSDO(new set < String > { strOracleID });
            for (GP_Work_Location__c objWL: lstWorkLocation) {
                mpOracleIdtoSDO.put(objWL.GP_Oracle_Id__c, objWL.id);
            }
        }

        if (!String.IsBlank(strCreatorRecordID)) {
            list < GP_Employee_Master__c > lstEmployeeMaster = new GPSelectorEmployeeMaster().getEmployeeListForPersonId(new set < String > { strCreatorRecordID });
            for (GP_Employee_Master__c objWL: lstEmployeeMaster) {
                mpCreatorIDtoUser.put(objWL.GP_Person_ID__c, objWL.GP_SFDC_User__c);
            }
        }

        for (GP_Opportunity_Project__c oppproj: (List < GP_Opportunity_Project__c > ) Records) {
            if (oppproj.GP_SDO_Code__c == null && mpOracleIdtoSDO.get(strOracleID) != null) {
                oppproj.GP_SDO_Code__c = mpOracleIdtoSDO.get(strOracleID);
            }
            if (!String.IsBlank(strCreatorRecordID) && mpCreatorIDtoUser != null && mpCreatorIDtoUser.get(strCreatorRecordID) != null) {
                oppproj.ownerid = mpCreatorIDtoUser.get(strCreatorRecordID);
            }
        }

    }
    /**
     * @description method used to update EP Project Number on Opp Project Classification 
     * @param trigger.oldmap
     * 
     * @example
     * new updateEPProjectNumberClassification(trigger.oldmap);
     */
    // 24-07-19.
    // Avalara Change : Update the classification records on change of Product and Business hierarchy also.
    private void updateEPProjectNumberClassification(fflib_SObjectUnitOfWork uow, map < id, Sobject > oldSObjectMap) {
        
        if (!System.Test.isRunningTest() && GPCommon.isStopRecursion) {
        	return;
        }
        
        set < id > setOppProjID = new set < id > ();
        GP_Opportunity_Project__c objOldProj;
        map < id, Sobject > newSObjectMap = new map < id, Sobject > (Records);
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
            objOldProj = (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.Id);
            /*if (!String.isBlank(objOppProject.GP_EP_Project_Number__c) && !String.isBlank(objOldProj.GP_EP_Project_Number__c) &&
                objOppProject.GP_EP_Project_Number__c != objOldProj.GP_EP_Project_Number__c) {
                setOppProjID.add(objOppProject.id);
            }*/
            // Avalara Change : If ep project number, product or business hierarchy is changed.
            System.debug('==objOppProject.EPNo=='+objOppProject.GP_EP_Project_Number__c);
            System.debug('==objOldProj.EPNo=='+objOldProj.GP_EP_Project_Number__c);
			
			// BGC Change
            if((objOppProject.GP_IsUpdated__c && !isOpportunityProjectUpdated) || isProductOrBusinessHierarchyChange(objOppProject, objOldProj, 'all')) {
                setOppProjID.add(objOppProject.id);
            }
        }
		//System.debug('==setOppProjID@@==' + setOppProjID);
        if (setOppProjID != null && setOppProjID.size() > 0) {
            list < GP_Project_Classification__c > lstProjClassificationUpdate = new list < GP_Project_Classification__c > ();
            GP_Opportunity_Project__c objNewProj;
            for (GP_Project_Classification__c objProjClassification: [SELECT id, GP_CLASS_CODE__c, GP_Opp_Project__c, GP_EP_Project_Number__c,
                                                                      GP_PINNACLE_PUSH_DATE__c
                                                                      FROM GP_Project_Classification__c
                                                                      WHERE GP_Opp_Project__c in: setOppProjID
        	]) {
                objNewProj = (GP_Opportunity_Project__c) newSObjectMap.get(objProjClassification.GP_Opp_Project__c);
                objOldProj = (GP_Opportunity_Project__c) oldSObjectMap.get(objProjClassification.GP_Opp_Project__c);
                Boolean isUpdated = false;
				
                // Avalara Change : if ep project no changed.
                if(isProductOrBusinessHierarchyChange(objNewProj, objOldProj, 'ep_number')) {
                   objProjClassification.GP_EP_Project_Number__c = objNewProj.GP_EP_Project_Number__c;
                    
                   if (objProjClassification.GP_CLASS_CODE__c == 'PBB Attributes') {
                        objProjClassification.GP_ATTRIBUTE1__c = objNewProj.GP_EP_Project_Number__c;
                   }
                   objProjClassification.GP_IsUpdated__c = true;
                   isUpdated = true;
                }
                
                // Avalara Change : if product change.
                if(objProjClassification.GP_CLASS_CODE__c == 'Product Family' && 
                	isProductOrBusinessHierarchyChange(objNewProj, objOldProj, 'product') ) {
                		objProjClassification.GP_ATTRIBUTE1__c = objNewProj.GP_Product_Id__c;
                		objProjClassification.GP_PINNACLE_PUSH_DATE__c = System.today();
                        objProjClassification.GP_IsUpdated__c = true;
                        isUpdated = true;
                }
                // Avalara Change : if business change.
                if(objProjClassification.GP_CLASS_CODE__c == 'Business Hierarchy' && 
                	isProductOrBusinessHierarchyChange(objNewProj, objOldProj, 'business')) {
                       //objProjClassification.GP_CLASS_CODE__c = 'Business Hierarchy For PBB';
                       //objProjClassification.GP_CLASS_CATEGORY__c = 'Business Hierarchy For PBB';
                       //objProjClassification.GP_ATTRIBUTE_CATEGORY__c = 'Business Hierarchy For PBB';
                       objProjClassification.GP_ATTRIBUTE1__c = objNewProj.GP_Business_Group_L1__c;
                       objProjClassification.GP_ATTRIBUTE2__c = objNewProj.GP_Business_Segment_L2_Id__c;
                       objProjClassification.GP_ATTRIBUTE3__c = objNewProj.GP_Sub_Business_L3_Id__c;
                       objProjClassification.GP_ATTRIBUTE4__c = objNewProj.GP_Customer_L4_Name__c;
                       objProjClassification.GP_PINNACLE_PUSH_DATE__c = System.today();
                       //objProjClassification.GP_PINNACLE_PUSH_DATE__c = objNewProj.GP_Pinnacle_Push_Date__c;
                       //objProjClassification.GP_EP_Project_Number__c = objNewProj.GP_EP_Project_Number__c;
                       objProjClassification.GP_IsUpdated__c = true;
                       isUpdated = true;
                }
                // Avalara Change
                if(isUpdated) {
                    System.debug('==list@@=='+ lstProjClassificationUpdate);
                	lstProjClassificationUpdate.add(objProjClassification);                    
                }
            }
			// Avalara Change
            if (lstProjClassificationUpdate.size() > 0) {
                isOpportunityProjectUpdated = true;
                GPCommon.isStopRecursion = true;
                uow.registerdirty(lstProjClassificationUpdate);
            }
        }
    }

    /**
     * @description method used to Share Opportunity Project record with SDO group users
     * @param trigger.oldmap
     * 
     * @example
     * new shareOpportunityRecordToSDO(trigger.oldmap);
     */
    private void shareOpportunityRecordToSDO(fflib_SObjectUnitOfWork uow, map < id, Sobject > oldSObjectMap) {
        set < id > setOpportunityProjectId = new set < id > ();
        set < string > setOfGrpName = new set < String > ();
        for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {

            GP_Opportunity_Project__c objOldProj = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.Id) : null;

            if (objOppProject.GP_SDO_Code__c != null && (objOldProj == null || objOldProj.GP_SDO_Code__c != objOppProject.GP_SDO_Code__c)) {
                setOfGrpName.add('GP_SDO_' + objOppProject.GP_SDO_Code__c + Label.GP_Group_Read_Label);
                setOpportunityProjectId.add(objOppProject.Id);
            }
        }

        if (setOpportunityProjectId.size() > 0) {
            map < string, Group > mapOfGrp;
            if (setOfGrpName != null && setOfGrpName.size() > 0) {
                mapOfGrp = GPCommon.getGroupByName(setOfGrpName);
            }

            list < GP_Opportunity_Project__share > lstShare = new list < GP_Opportunity_Project__share >

                (new GPSelectorOpportunityProject().selectOpportunityProjectShareById(setOpportunityProjectId));

            map < string, GP_Opportunity_Project__share > mapOfAccess = new map < string, GP_Opportunity_Project__share > ();

            if (lstShare != null && lstShare.size() > 0) {
                for (GP_Opportunity_Project__share EachShare: lstShare) {
                    mapOfAccess.put(EachShare.UserOrGroupId, EachShare);
                }
            }

            list < GP_Opportunity_Project__share > lstOppProjShare2Insert = new list < GP_Opportunity_Project__share > ();
            string key;
            set < String > setGrpstoBeCreated = new set < String > ();
            for (GP_Opportunity_Project__c objOppProject: (List < GP_Opportunity_Project__c > ) Records) {
                GP_Opportunity_Project__c objOldProj = oldSObjectMap != null ? (GP_Opportunity_Project__c) oldSObjectMap.get(objOppProject.Id) : null;

                if (objOppProject.GP_SDO_Code__c != null && objOldProj.GP_SDO_Code__c != objOppProject.GP_SDO_Code__c) {
                    key = 'GP_SDO_' + objOppProject.GP_SDO_Code__c + Label.GP_Group_Read_Label;

                    if (mapOfGrp != null && mapOfGrp.get(key) != null && !mapOfAccess.containsKey(mapOfGrp.get(key).id)) {
                        GP_Opportunity_Project__share objShare = new GP_Opportunity_Project__share();
                        objShare.ParentId = objOppProject.Id;
                        objShare.UserOrGroupId = mapOfGrp.get(key).id;
                        objShare.AccessLevel = 'Read';
                        lstOppProjShare2Insert.add(objShare);
                    }
                }
            }

            if (lstOppProjShare2Insert != null && lstOppProjShare2Insert.size() > 0) {
                insert lstOppProjShare2Insert;
            }
        }
    }    
    // Avalara Change.
    public static Boolean isProductOrBusinessHierarchyChange(GP_Opportunity_Project__c newRecord, GP_Opportunity_Project__c oldRecord, String checkUpdated) {
        
        Boolean isChange = false;
        Boolean isProductIdChange = (String.isNotBlank(newRecord.GP_Product_Id__c) && newRecord.GP_Product_Id__c != oldRecord.GP_Product_Id__c);
        Boolean isBusinessChange = ((newRecord.GP_Customer_L4_Name__c != null && newRecord.GP_Customer_L4_Name__c != oldRecord.GP_Customer_L4_Name__c) ||
            (String.isNotBlank(newRecord.GP_Sub_Business_L3_Id__c) && newRecord.GP_Sub_Business_L3_Id__c != oldRecord.GP_Sub_Business_L3_Id__c) ||
            (String.isNotBlank(newRecord.GP_Business_Segment_L2_Id__c) && newRecord.GP_Business_Segment_L2_Id__c != oldRecord.GP_Business_Segment_L2_Id__c) ||
            (String.isNotBlank(newRecord.GP_Business_Group_L1__c) && newRecord.GP_Business_Group_L1__c != oldRecord.GP_Business_Group_L1__c));
        Boolean isEPProjectNoChange = (String.isNotBlank(newRecord.GP_EP_Project_Number__c) && String.isNotBlank(oldRecord.GP_EP_Project_Number__c) &&
                newRecord.GP_EP_Project_Number__c != oldRecord.GP_EP_Project_Number__c);
            
        if(checkUpdated.equalsIgnoreCase('all') && (isProductIdChange || isBusinessChange || isEPProjectNoChange)) {
            isChange = true;
        } else if(checkUpdated.equalsIgnoreCase('product') && isProductIdChange) {
            isChange = true;
        } else if(checkUpdated.equalsIgnoreCase('business') && isBusinessChange) {
            isChange = true;
        } else if(checkUpdated.equalsIgnoreCase('ep_number') && isEPProjectNoChange) {
            isChange = true;
        }
        
        return isChange;
    }
}