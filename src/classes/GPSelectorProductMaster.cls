public class GPSelectorProductMaster extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Product_Master__c.Name,
                GP_Product_Master__c.Id, 
                GP_Product_Master__c.GP_Industry_Vertical__c,
                GP_Product_Master__c.GP_Is_Active__c,
                GP_Product_Master__c.GP_Nature_of_Work__c,
                GP_Product_Master__c.GP_Nature_of_Work_ID__c,
                GP_Product_Master__c.GP_Product_Name__c,
                GP_Product_Master__c.GP_Product_Catalogue1__c,
                GP_Product_Master__c.GP_Product_Catalogue1_ID__c,
                GP_Product_Master__c.GP_Product_Family__c,
                GP_Product_Master__c.GP_Product_Family_ID__c,
                GP_Product_Master__c.GP_Product_ID__c,
                GP_Product_Master__c.GP_Service_Line__c,
                GP_Product_Master__c.GP_Service_Line_ID__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Product_Master__c.sObjectType;
    }
    
    public List<GP_Product_Master__c> selectById(Set<ID> idSet) {
        return (List<GP_Product_Master__c>) selectSObjectsById(idSet);
    }
    
    public GP_Product_Master__c selectById(ID productMasterId) {
        Set<Id> idSet = new Set<Id> {
            productMasterId
                };
                    return (GP_Product_Master__c) selectSObjectsById(idSet).get(0);
    }
    
    public static List<GP_Product_Master__c> getProductMaster(set<String> setofString){
        return [SELECT Id, Name,GP_Product_Catalogue1_ID__c, GP_Concatinate_All_IDs__c
                FROM GP_Product_Master__c
                WHERE GP_Concatinate_All_IDs__c IN :setofString AND GP_Is_Active__c = true ];
    }

    public static List<GP_Product_Master__c> getAllProductMaster(String industryVertical){
        return [SELECT Id, Name,GP_Product_ID__c,GP_Product_Name__c,
                        GP_Service_Line__c,
                        GP_Nature_of_Work__c,
                        GP_Product_Family__c
                    FROM GP_Product_Master__c
                    WHERE GP_Industry_Vertical__c = :industryVertical AND GP_Is_Active__c = true];
    }

    public static List<GP_Product_Master__c> getAllProductMaster(){
        return [SELECT Id, Name,GP_Product_ID__c,GP_Product_Name__c,
                        GP_Service_Line__c,
                        GP_Nature_of_Work__c,
                        GP_Product_Family__c
                    FROM GP_Product_Master__c Where GP_Is_Active__c = true];
    }
	// Mass Update Change
    public List<GP_Product_Master__c> getProductMasterDataForMassUpdate(Set<String> setOfProducts, Set<String> setOfNatureOfWorks, Set<String> setOfServiceLines) {
        String queryStr = 'SELECT ID, GP_Product_Name__c, GP_Industry_Vertical__c, GP_Product_ID__c, GP_Service_Line__c, GP_Nature_of_Work__c FROM GP_Product_Master__c WHERE GP_Is_Active__c = true AND';        
        if(setOfProducts.size() > 0) {
            queryStr += ' GP_Product_Name__c =: setOfProducts AND';
        }
        if(setOfNatureOfWorks.size() > 0) {
            queryStr += ' GP_Nature_of_Work__c =: setOfNatureOfWorks AND';
        }
        if(setOfServiceLines.size() > 0) {
            queryStr += ' GP_Service_Line__c =: setOfServiceLines AND';
        }
        queryStr = queryStr.removeEnd('AND');        
        return Database.query(queryStr);
    }
}