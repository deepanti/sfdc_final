/*
    No use class and its code from anywhere
*/
public class VIC_SplitIncentive {   
    public static Decimal getSplitedIncentiveAmounts(Decimal percantage, Decimal amount){
        return percantage*amount;
    }
    /*
    public static VICIncentiveInfo getIncentive(OpportunityLineItem oli, String planCode, Decimal fixedIncentiveAmount){
        VICIncentiveInfo vicIninfo = new VICIncentiveInfo();        
        if(vic_CommonUtil.getPlanCodeOfUser(oli.vic_Owner__c) == planCode)
              vicIninfo.primarySaleRepAmount = getSplitedIncentiveAmounts(oli.vic_Primary_Sales_Rep_Split__c,fixedIncentiveAmount);
        if(vic_CommonUtil.getPlanCodeOfUser(oli.Product_BD_Rep__c) == planCode)
              vicIninfo.productBDRepAmount = getSplitedIncentiveAmounts(oli.vic_Product_BD_Rep_Split__c,fixedIncentiveAmount);
        if(vic_CommonUtil.getPlanCodeOfUser(oli.vic_VIC_User_3__c) == planCode)
              vicIninfo.user3Amount = getSplitedIncentiveAmounts(oli.vic_VIC_User_3_Split__c,fixedIncentiveAmount);
         if(vic_CommonUtil.getPlanCodeOfUser(oli.vic_VIC_User_4__c) == planCode)
              vicIninfo.user4Amount = getSplitedIncentiveAmounts(oli.vic_VIC_User_4_Split__c,fixedIncentiveAmount);
  
        return vicIninfo;
    }
    */
    
}