public class MainOppTriggerHelper {
    
     public static map<string, MainOpportunityHelperFlag__c>  MapofProjectTrigger = MainOpportunityHelperFlag__c.getall();
    
    public static void beforeInsertMethod(list<opportunity> newOppList){
        
    }
    public static void beforeUpdateMethod(list<opportunity> newOppList,Map<id,Opportunity> OldOppMap){
        try{          
            
            
            Boolean recursiveFlag = CheckRecursiveForNonTs.runBeforeNonTsHoldTeam();
            if(recursiveFlag){
                
                
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('holdOpportunityTeam') && MapofProjectTrigger.get('holdOpportunityTeam').HelperName__c){
                    DeleteOppSPlitfromOpportunity.holdOpportunityTeam(newOppList, OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('UpdatePrediscoverMethod') && MapofProjectTrigger.get('UpdatePrediscoverMethod').HelperName__c){
                    OpportunityTriggerHelper.UpdatePrediscoverMethod(newOppList, OldOppMap);
                }
            }
        }
        catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber()); CreateErrorLog.createErrorRecord('MainOppTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOppTriggerHelper', 'beforeUpdateMethod','Fail','',String.valueOf(e.getLineNumber())); 
                           }
        
    }
    public static void beforeDeleteMethod(list<opportunity> OldOppList,Map<id,Opportunity> OldOppMap){
        try{
            Boolean beforeRecursiveFlag = CheckRecursive.runBeforeDelete();
            if(beforeRecursiveFlag){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('opportunityRestrictMethod') && MapofProjectTrigger.get('opportunityRestrictMethod').HelperName__c){
                    OpportunityTriggerHelper.opportunityRestrictMethod(OldOppList);
                }
            }
        }catch(Exception e){  system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber()); CreateErrorLog.createErrorRecord('MainOppTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOppTriggerHelper', 'beforeDeleteMethod','Fail','',String.valueOf(e.getLineNumber())); 
                           }
        
    }
    public static void afterInsertMethod(list<opportunity> newOppList,Map<id,Opportunity> OldOppMap){
        try{
            Boolean afterInsertRecursiveFlag = CheckRecursive.afterInsertMethod();
            if(afterInsertRecursiveFlag){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('insertContactRole') && MapofProjectTrigger.get('insertContactRole').HelperName__c){
                    OpportunityTriggerHelper.insertContactRole(newOppList); 
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('saveDealCycleAging') && MapofProjectTrigger.get('saveDealCycleAging').HelperName__c){
                    OpportunityTriggerHelper.saveDealCycleAging(OldOppMap.keySet()); 
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('createTaskforOpp') && MapofProjectTrigger.get('createTaskforOpp').HelperName__c){
                    //TaskCreationHandler.createTaskforOpp(newOppList);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('createTaskforOpponUpdate') && MapofProjectTrigger.get('createTaskforOpponUpdate').HelperName__c){  DeleteOppSPlitfromOpportunity.CreateOppTeamonOppCreation(newOppList);
                                                                                                                                                                                   }
            }
        }catch(Exception e){
            system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('MainOppTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOppTriggerHelper', 'afterInsertMethod','Fail','',String.valueOf(e.getLineNumber())); 
        }  
        
    }
    public static void afterUpdateMethod(list<opportunity> newOppList,Map<id,Opportunity> OldOppMap){
        try{
            Boolean afterUpdateRecursiveFlag = CheckRecursive.afterUpdateMethod();
         //   if(afterUpdateRecursiveFlag){
                
                
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('contactLifeCycleMethod') && MapofProjectTrigger.get('contactLifeCycleMethod').HelperName__c)
                {
                   
                   
                    for(opportunity o: newOppList)
                    { 
                        system.debug('o.stagename=='+o.stagename);
                        system.debug('OldOppMap.get(o.id).stagename=='+OldOppMap.get(o.id).stagename);
                        if(o.stagename != OldOppMap.get(o.id).stagename) {  
                            
                            ContactLifeCycleHelper.contactLifeCycleMethod(newOppList, OldOppMap);
                        }
                    }
                   
                }
              if(afterUpdateRecursiveFlag){  
                
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('dateUpdateMethod') && MapofProjectTrigger.get('dateUpdateMethod').HelperName__c){
                    OpportunityTriggerHelper.dateUpdateMethod(newOppList,OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('staleDealMethod') && MapofProjectTrigger.get('staleDealMethod').HelperName__c ){
                    Set<Id> uid=new Set<id>(); //Modified By Neha P on 7-Mar-2019
                    //Id uid; 
                    String sessionId = userinfo.getSessionId();
                    if(singleexecuion.runItOnce()){
                        for(Opportunity opp : newOppList){
                            uid.add(opp.OwnerId); //Modified By Neha P on 7-Mar-2019
                            //uid = opp.OwnerId;
                            
                        }
                        
                            staleDealOwners.updateStaleUser(uid,sessionId);
                    }
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('dealTypeMethod') && MapofProjectTrigger.get('dealTypeMethod').HelperName__c){
                    TsNonTsHelperClass.dealTypeMethod(newOppList);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('saveDealCycleAging') && MapofProjectTrigger.get('saveDealCycleAging').HelperName__c){
                    OpportunityTriggerHelper.saveDealCycleAging(OldOppMap.keySet());
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('updateContactRole') && MapofProjectTrigger.get('updateContactRole').HelperName__c){
                    OpportunityTriggerHelper.updateContactRole(newOppList, OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('createTaskforOpponUpdate') && MapofProjectTrigger.get('createTaskforOpponUpdate').HelperName__c){
                    // TaskCreationHandler.createTaskforOpponUpdate(newOppList, OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('removeTaskforOpponUpdate') && MapofProjectTrigger.get('removeTaskforOpponUpdate').HelperName__c){
                    //  TaskCreationHandler.removeTaskforOpponUpdate(newOppList);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('Prediscover_RemoveTasksMethod') && MapofProjectTrigger.get('Prediscover_RemoveTasksMethod').HelperName__c){
                    OpportunityPrediscover_RemoveTasksHelper.Prediscover_RemoveTasksMethod(newOppList,OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('deleteOppSplitOwneronOppOwnerchange') && MapofProjectTrigger.get('deleteOppSplitOwneronOppOwnerchange').HelperName__c){
                    DeleteOppSPlitfromOpportunity.deleteOppSplitOwneronOppOwnerchange(newOppList, OldOppMap);
                }
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('PricerDynamicLinkMethod') && MapofProjectTrigger.get('deleteOppSplitOwneronOppOwnerchange').HelperName__c){
                  //  OptyPricerDynamicLinkHelper.PricerDynamicLinkMethod(newOppList,OldOppMap);
                } 
                
        }
        }catch(Exception e){ system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber()); CreateErrorLog.createErrorRecord('MainOppTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOppTriggerHelper', 'afterUpdateMethod','Fail','',String.valueOf(e.getLineNumber())); 
                           } 
        
    }
    public static void afterDeleteMethod(list<opportunity> newOppList,Map<id,Opportunity> OldOppMap){
        try{
            Boolean afterDeleteRecursiveFlag = CheckRecursive.afterDeleteMethod();
            if(afterDeleteRecursiveFlag){
                if(MapofProjectTrigger != null && MapofProjectTrigger.containsKey('deleteContactRole') && MapofProjectTrigger.get('deleteContactRole').HelperName__c){
                    OpportunityTriggerHelper.deleteContactRole(OldOppMap);
                }
            }
        }catch(Exception e){  system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());  CreateErrorLog.createErrorRecord('MainOppTriggerHelper',e.getMessage(), '', e.getStackTraceString(),'MainOppTriggerHelper', 'afterDeleteMethod','Fail','',String.valueOf(e.getLineNumber())); 
                           }  
        
        
    }
    public static void afterUndeleteMethod(list<opportunity> newOppList){
        integer i =0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
    }
    
}