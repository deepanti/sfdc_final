/**
 *
 * @group Mass Upload/Update.
 * @group-content ../../ApexDocContent/MassUploadAndUpdate.html.
 *
 * @description Apex Controller for Mass Upload and Update.
 */
public class GPControllerMassUploadAndUpdate {

    private List < GP_Job_Bulk_Upload__mdt > lstOfJobBulkUploadMetaData;
    private map < String, String > mapOfColumnNameToFieldAPIName;
    private String parseToObjectLabel;
    private List < String > headerRow;
    private String exportfileName;
    private String instructions;
    private GP_Job__c jobRecord;
    private JSONGenerator gen;
    private String jobType;
    private String jobId;

    private final String REQUIRED_FIELD_ERROR_LABEL = 'Please select Project for Job type Project,Project and Project Worklocation for job type Project Worklocation and Project and Leadership Role for job type Project Leadership.';
    private final String MAP_OF_COLUMN_TO_API_NAME_LABEL = 'mapOfColumnNameToFieldAPIName';
    private final String DUPLICATE_DATA_ERROR_LABEL = 'Your CSV Contains Duplicate Data!';
    private final String PARSE_TO_OBJECT_LABEL = 'parseToObjectLabel';
    private final String JOB_TYPE_INSTRUCTIONS_LABEL = 'instructions';
    private final String EXPORT_FILE_NAME_LABEL = 'exportfileName';
    private final String HEADER_ROW_LABEL = 'headerRow';
    private final String JOB_TYPE_LABEL = 'jobType';
    private final String SUCCESS_LABEL = 'SUCCESS';

    /**
     * @description Parameterized constructor to accept Job Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerMassUploadAndUpdate(<SFDC 18/15 digit Job Id>);
     */
    public GPControllerMassUploadAndUpdate(String jobId) {
        this.jobId = jobId;
    }

    public GPControllerMassUploadAndUpdate() {}

    /**
     * @description method to return job Configurations.
     * 
     * @example
     * new GPControllerMassUploadAndUpdate(<SFDC 18/15 digit JOb Id>).getJobConfigurationData();
     */
    public GPAuraResponse getJobConfigurationData() {
        try {
            setJobRecord();
            setlstOfJobBulkUploadMetaData();
            setJobConfiguration();
            setJson();

            return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    /**
     * @description method to insert Temporary Records Uploaded.
     * 
     * @param listOfTemporaryRecords : list of temporary records.
     * @example
     * new GPControllerMassUploadAndUpdate(<SFDC 18/15 digit JOb Id>).insertTemporaryRecords(List<GP_Temporary_Data__c>);
     */
    public GPAuraResponse insertTemporaryRecords(List < GP_Temporary_Data__c > listOfTemporaryRecords, string JobType, String fileName) {
        try {
         
            if (jobType == 'Upload Time Sheet Entries') {
                setEmployeeOHR(listOfTemporaryRecords);
            }
            
            insert listOfTemporaryRecords;
            setJobFileName(fileName);
            return new GPAuraResponse(true, SUCCESS_LABEL, null);
        } catch (Exception ex) {
            system.debug('ex'+ex);
            if (ex.getMessage().contains('duplicate value found'))
                return new GPAuraResponse(false, DUPLICATE_DATA_ERROR_LABEL, null);
            else if (ex.getMessage().contains(REQUIRED_FIELD_ERROR_LABEL))
                return new GPAuraResponse(false, REQUIRED_FIELD_ERROR_LABEL, null);
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    private void setEmployeeOHR(List < GP_Temporary_Data__c > listOfTemporaryRecords) {
        set < String > setOHRid = new set < String > ();
        for (GP_Temporary_Data__c objTempData: listOfTemporaryRecords) {
            if (!String.IsBlank(objTempData.GP_PL_Employee_OHR__c)) {
                setOHRid.add(objTempData.GP_PL_Employee_OHR__c);
            }
        }
        if (setOHRid.size() > 0) {
            map < String, String > mapOHRtoPersonId = GPServiceEmployeeMaster.getMapOHRtoPersonId(setOHRid);
            for (GP_Temporary_Data__c objTempData: listOfTemporaryRecords) {
                if (!String.IsBlank(objTempData.GP_PL_Employee_OHR__c) && mapOHRtoPersonId.get(objTempData.GP_PL_Employee_OHR__c) != null) {
                    objTempData.GP_Employee__c = mapOHRtoPersonId.get(objTempData.GP_PL_Employee_OHR__c);
                }
            }
        }
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (mapOfColumnNameToFieldAPIName != null)
            gen.writeObjectField(MAP_OF_COLUMN_TO_API_NAME_LABEL, mapOfColumnNameToFieldAPIName);
        if (parseToObjectLabel != null)
            gen.writeObjectField(PARSE_TO_OBJECT_LABEL, parseToObjectLabel);
        if (instructions != null)
            gen.writeObjectField(JOB_TYPE_INSTRUCTIONS_LABEL, instructions);
        if (exportfileName != null)
            gen.writeObjectField(EXPORT_FILE_NAME_LABEL, exportfileName);
        if (headerRow != null)
            gen.writeObjectField(HEADER_ROW_LABEL, headerRow);
        if (jobType != null)
            gen.writeObjectField(JOB_TYPE_LABEL, jobType);
            
        gen.writeEndObject();
    }

    /**
     * @description Query Job Record.
     * 
     */
    private void setJobRecord() {
        jobRecord = new GPSelectorJob().SelectJobRecord(jobId);
        jobType = jobRecord.GP_Job_Type__c;
        parseToObjectLabel = jobRecord.GP_Job_Type__c;
        exportfileName = jobRecord.GP_Job_Type__c + '.csv';
        instructions = 'Intro';
    }

    /**
     * @description Query Job Bulk Upload Metadata.
     * 
     */
    private void setlstOfJobBulkUploadMetaData() {
        lstOfJobBulkUploadMetaData = [select GP_Type__c, GP_Column_Name__c, GP_Sequence_Number__c, GP_Field_API_Name__c
            from GP_Job_Bulk_Upload__mdt where GP_Type__c =: jobType ORDER BY GP_Sequence_Number__c
        ];
    }

    /**
     * @description Set Header Row Using Bulk Upload Metadata.
     * 
     */
    private void setJobConfiguration() {
        mapOfColumnNameToFieldAPIName = new map < String, String > ();
        headerRow = new List < String > ();

        for (GP_Job_Bulk_Upload__mdt objBulkUploadMetdata: lstOfJobBulkUploadMetaData) {
            mapOfColumnNameToFieldAPIName.put(objBulkUploadMetdata.GP_Column_Name__c.toLowerCase(), objBulkUploadMetdata.GP_Field_API_Name__c);
            if (jobRecord.GP_Column_s_To_Update_Upload__c != null && jobRecord.GP_Column_s_To_Update_Upload__c.contains(objBulkUploadMetdata.GP_Field_API_Name__c))
                headerRow.add(objBulkUploadMetdata.GP_Column_Name__c.toUpperCase());
        }
    }
    /**
     * @description Query Job Record to update fileName.
     *
     * @param fileName : name of the file uploaded.
     */
    private void setJobFileName(String fileName) {
        jobRecord = new GPSelectorJob().SelectJobRecord(jobId);
        jobRecord.GP_Uploaded_File_Name__c = fileName;
        update jobRecord;
    }

}