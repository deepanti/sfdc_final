public without sharing class UpdateForecastDealController {
    
    public static boolean updateOpportunitySaveMethod(list<opportunity> oppList){
        try{
           // update oppList;
           Database.SaveResult [] updateResult = Database.update(oppList, false);
            for (Database.SaveResult r : updateResult) {
                if (!r.isSuccess())  {    for (Database.Error e : r.getErrors()) {    CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), 'e.getFields()', 'oppListempty','UpdateForecastDealController', 'updateOpportunitySaveMethod','Fail','',String.valueOf(e.getStatusCode()));
                    }
               }
            }
           
            return true;
        }
            catch(Exception e){  System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());   CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'UpdateForecastDealController', 'updateOpportunitySaveMethod','Fail','',String.valueOf(e.getLineNumber()));       }    return false;
        
    }

}