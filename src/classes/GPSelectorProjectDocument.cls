public class GPSelectorProjectDocument extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project_Document__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Document__c.sObjectType;
    }

    public List < GP_Project_Document__c > selectById(Set < ID > idSet) {
        return (List < GP_Project_Document__c > ) selectSObjectsById(idSet);
    }

    public List < GP_Project_Document__c > selectOtherProjectDocumentRecords(Id projectId) {
        Id otherDocumentRecordTypeId = Schema.SObjectType.GP_Project_Document__c.getRecordTypeInfosByName().get('Other Document').getRecordTypeId();
        return [SELECT Id, Name, GP_Project__c, GP_Source__c, GP_Project__r.Name, GP_Project__r.GP_Project_Status__c,
              	GP_File_Name__c,GP_Attachment_Description__c,
            GP_Document_URL__c, GP_Type__c, GP_Content_Version_Id__c, GP_Parent_Project_Document__c,GP_Document_Creation_Date__c
            FROM GP_Project_Document__c
            WHERE(GP_Project__c =: projectId and RecordTypeID =: otherDocumentRecordTypeId)
        ];

    }

    public List < GP_Project_Document__c > selectPOProjectDocumentRecords(Id projectId) {
        Id poDocumentRecordTypeId = Schema.SObjectType.GP_Project_Document__c.getRecordTypeInfosByName().get('PO Document').getRecordTypeId();
        return [SELECT Id, Name, GP_Project__c, GP_Source__c, GP_Project__r.Name, GP_Project__r.GP_Project_Status__c,
            GP_Document_URL__c, GP_Type__c, GP_Content_Version_Id__c, GP_Payment_Terms__c, GP_PO_Amount__c,
            GP_PO_End_Date__c, GP_PO_Necessary_On_Invoice__c, GP_PO_Number__c, GP_PO_Remarks__c,GP_Document_Creation_Date__c,
            GP_PO_Start_Date__c, GP_Parent_Project_Document__c
            FROM GP_Project_Document__c
            WHERE(GP_Project__c =: projectId and RecordTypeID =: poDocumentRecordTypeId)
        ];
    }
}