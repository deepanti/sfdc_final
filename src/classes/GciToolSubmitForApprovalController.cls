public class GciToolSubmitForApprovalController {
    
    @AuraEnabled
    public static void  submitForApprovalMethod(String comments,String oppID)
    {
        try{
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments(comments);
            req1.setObjectId(oppID);			
        	req1.setSubmitterId(UserInfo.getUserId()); 
        	req1.setProcessDefinitionNameOrId('RS_PP_Tool');
       		
            // Submit the approval request for the account
        	Approval.ProcessResult result = Approval.process(req1);
        	}
        catch(Exception ex)
        {
            CreateErrorLog.createErrorRecord(oppID,ex.getMessage(), '', ex.getStackTraceString(),'GciToolSubmitForApprovalController', 'submitForApprovalMethod','Fail','',String.valueOf(ex.getLineNumber()));
            if(ex.getMessage().contains('No applicable approval process was found'))
            	throw new AuraHandledException('Please fill in GCI tools before you submit them for approval to salesleader');
            else if(ex.getMessage().contains('Sales Leader field'))
                throw new AuraHandledException('Update the sales leader field on the opportunity');
            else if(ex.getMessage().contains('This record is currently in an approval process'))
                 throw new AuraHandledException('This record is currently in an approval process');
            else
                throw new AuraHandledException(ex.getMessage());
        }
        
    }
}