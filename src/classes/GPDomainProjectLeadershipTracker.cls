@isTest
public class GPDomainProjectLeadershipTracker {
    public static boolean isRun = true;
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Project_Leadership__c projectLeadership1;
    public static GP_Project_Leadership__c projectLeadership;
    public static String jsonresp;
    public static recordType objRec = new recordType();
   
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_leadership__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-employee';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = false;
        update empObj;
        
        GP_Employee_Master__c empObj2 = GPCommonTracker.getEmployee();
        empObj2.GP_Employee_Type__c = 'Ex-employee';
        empObj2.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empObj2.GP_SDO_OracleId__c = '1234';
        //empObj2.GP_Batch_Number__c = '12345';
        empObj2.Name = 'Testemployee2';
        empObj2.GP_EMPLOYEE_TYPE__c = 'Full Time';
        empObj2.GP_Person_ID__c = 'EMP-002';
        empObj2.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test2@test.com';
        insert empObj2;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        projectLeadership1 = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership1.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership1.GP_Pinnacle_End_Date__c = system.today();
        projectLeadership1.GP_Project__c = projectObjForIcon.id;
        projectLeadership1.GP_Employee_ID__c = empObj.id;
        projectLeadership1.GP_Leadership_Role_Name__c = 'PID Approver';
        
        
        projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership.GP_Pinnacle_End_Date__c = system.today();
        projectLeadership.GP_Project__c = projectObjForIcon.id;
        projectLeadership.GP_Leadership_Role_Name__c = 'PID Approver Finance';
        projectLeadership.GP_Employee_ID__c = empObj.id;
        
        
    }
    
    @isTest
    public static void testData() {
        buildDependencyData();
        insert projectLeadership1;
        projectLeadership.GP_Parent_Project_Leadership__c = projectLeadership1.id;
        insert projectLeadership;
        Update projectLeadership;
        delete projectLeadership;
    }
    
     @isTest
    public static void testData2() {
        buildDependencyData();
        projectLeadership1.GP_Leadership_Role_Name__c = 'FP&A';
        insert projectLeadership1;
        projectLeadership.GP_Parent_Project_Leadership__c = projectLeadership1.id;
        insert projectLeadership;
        Update projectLeadership;
        delete projectLeadership;
    }
}