/*
 * Author: Enrico Murru (http://enree.co, @enreeco)
 */
@isTest
private class InputLookupAuraControllerTest {
    
    @testSetup
    private static void setup(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
       GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
       GEN_Util_Test_Data.CreateUser('standarduser2016@testorg.com',p.Id,'standardusertestgen2016@testorg.com' );
       GEN_Util_Test_Data.CreateUser('standarduser2017@testorg.com',p.Id,'standardusertestgen2017@testorg.com' );
    }
    
    private static testmethod void test_get_name(){
        List<User> contacts = [Select Id, LastName, FirstName, Name From User];
        
        Test.startTest();
        
        String ret = InputLookupAuraController.getCurrentValue(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', 'INVALID_ID');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('INVALID_OBJECT', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('User', '000000000000000');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.getCurrentValue('User', contacts[0].Id);
        System.assert(ret == contacts[0].Name, 'Should return '+contacts[0].Name+ ' ['+ret+']');
            
        Test.stopTest();
    }
    
    private static testmethod void test_search(){
        List<User> contacts = [Select Id, LastName, FirstName, Name From User];
        Test.startTest();
        
        String ret = InputLookupAuraController.searchSObject(null, null);
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.searchSObject('INVALID_OBJECT', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isBlank(ret), 'Should return null string ['+ret+']');
        
        ret = InputLookupAuraController.searchSObject('User', 'NO_RESULT_SEARCH_STRING');
        System.assert(String.isNotBlank(ret), 'Should return non null string ['+ret+']');
        List<InputLookupAuraController.SearchResult> sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
            List<InputLookupAuraController.SearchResult>.class);
        System.assert(sResList.isEmpty(), 'Why not empty list? ['+sResList.size()+' instead]');
        
        Test.setFixedSearchResults(new List<String>{contacts[0].Id,contacts[1].Id,contacts[2].Id});
        ret = InputLookupAuraController.searchSObject('User', 'standarduser');
        System.assert(String.isNotBlank(ret), 'Should return a serialized list string ['+ret+']');
        //sResList = (List<InputLookupAuraController.SearchResult>)JSON.deserialize(ret, 
           // List<InputLookupAuraController.SearchResult>.class);
       // System.assert(sResList.size() == 3, 'Why not 3 items found? ['+sResList.size()+' instead]');
        Test.stopTest();
    }
}