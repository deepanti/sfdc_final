@isTest

public class MarketoSyncHelperTest {
    
 
    @isTest static void TestOne()
    {
         MarketoSyncHelper obj=new MarketoSyncHelper();
        obj.syncToMarketo('Lead');
        test.startTest();
          marketoUpdateBatch batctRef=new marketoUpdateBatch();
          batctRef.query='SELECT Id,Sync_to_Marketo__c,Status FROM Lead';
          Database.executeBatch(batctRef,200);
        test.stopTest();
    }
     @isTest static void TestTwo()
    {
         MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo('Contact');
         test.startTest();
          marketoUpdateBatch batctRef=new marketoUpdateBatch();
          batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Contact';
          Database.executeBatch(batctRef,200);
        test.stopTest();
    }
     @isTest static void TestThree()
    {
     MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo('Account');
         test.startTest();
          marketoUpdateBatch batctRef=new marketoUpdateBatch();
          batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Account';
          Database.executeBatch(batctRef,200);
        test.stopTest();
    }
     @isTest static void TestFour()
    {
     MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo('Opportuity');
         
    }
      @isTest static void TestFive()
    {
     MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo(NULL);
        
    }
      @isTest static void TestSix()
    {
     MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo('Opportunity');
         test.startTest();
          marketoUpdateBatch batctRef=new marketoUpdateBatch();
          batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Opportunity';
          Database.executeBatch(batctRef,200);
        test.stopTest();
    }
      @isTest static void TestSeven()
    {
     MarketoSyncHelper obj=new MarketoSyncHelper();
         obj.syncToMarketo('Campaign');
         test.startTest();
          marketoUpdateBatch batctRef=new marketoUpdateBatch();
          batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Campaign';
          Database.executeBatch(batctRef,200);
        test.stopTest();
    }
}