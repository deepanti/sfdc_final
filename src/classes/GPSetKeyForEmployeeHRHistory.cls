global class GPSetKeyForEmployeeHRHistory implements Database.Batchable<sObject>,Database.stateful,schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {
        return database.getQueryLocator(System.Label.GP_Set_Key_HRMS);
    }
    global void execute(database.BatchableContext bc, List<GP_Employee_HR_History__c> lstHRMS)
    {
        SetToRunEMPMasterORHRMSTrigger.isRunEmpMasterTrigger='N';
        SetToRunEMPMasterORHRMSTrigger.isRunHrmsTrigger ='N';
        
        List<GP_Employee_HR_History__c> newLstHrms=new List<GP_Employee_HR_History__c>();
        
        for(GP_Employee_HR_History__c obj :lstHRMS )
        {           
            obj.GP_Attribute2__c = obj.GP_ASSIGNMENT_ID__c +'|'+ formatDate(obj.GP_ASGN_EFFECTIVE_START__c);    
            obj.Is_Search__c = true;
            newLstHrms.add(obj);
        }
            
        if(newLstHrms.size()>0)
        {
         //Schema.SObjectField f = GP_Employee_HR_History__c.Fields.GP_Attribute2__c;
         Database.SaveResult [] srList = Database.update(newLstHrms, false); 
            list<GP_Employee_HR_History__c> lstobjUpIsSearch = new List<GP_Employee_HR_History__c>();
            for(Integer index = 0, size = srList.size(); index < size; index++) {
                    
                   
                  if(!srList.get(index).isSuccess()){
                        // DML operation failed
                        GP_Employee_HR_History__c objUpIsSearch = new GP_Employee_HR_History__c();                           
                        objUpIsSearch.Id=newLstHrms[index].Id;
                        objUpIsSearch.Is_Search__c=true;                     
                        lstobjUpIsSearch.add(objUpIsSearch);
                    }
            
        }
            if(lstobjUpIsSearch.size()>0)
            {
                update lstobjUpIsSearch;
            }
        
        
    }
    }
    global void finish(Database.BatchableContext bc)
    {  
    }
    global void Execute(SchedulableContext sc)
    {        
        GPSetKeyForEmployeeHRHistory objBatch=new GPSetKeyForEmployeeHRHistory();
        Database.executeBatch(objBatch);
    }
    global static String formatDate(Date d) {
        
        Map<Integer,String> monthMap = new Map<Integer,String>();
        monthMap.put(1,'JAN');
        monthMap.put(2,'FEB');
        monthMap.put(3,'MAR');
        monthMap.put(4,'APR');
        monthMap.put(5,'MAY');
        monthMap.put(6,'JUN');
        monthMap.put(7,'JUL');
        monthMap.put(8,'AUG');
        monthMap.put(9,'SEP');
        monthMap.put(10,'OCT');
        monthMap.put(11,'NOV');
        monthMap.put(12,'DEC');
        
        Map<Integer,String> dayMap = new Map<Integer,String>();
        dayMap.put(1,'01');
        dayMap.put(2,'02');
        dayMap.put(3,'03');
        dayMap.put(4,'04');
        dayMap.put(5,'05');
        dayMap.put(6,'06');
        dayMap.put(7,'07');
        dayMap.put(8,'08');
        dayMap.put(9,'09');       
        system.debug('==='+d.day());
        String day= d.day()<=9 ? String.valueOf(dayMap.get(d.day())):String.ValueOf(d.day());
        return day + '-' + monthMap.get(d.month()) + '-' + String.valueOf(d.year()).right(2);
    }
}