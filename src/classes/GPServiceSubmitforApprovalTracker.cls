@isTest
public class GPServiceSubmitforApprovalTracker {
    private static GP_Project__c project;
    public static GP_Project__c prjObj;  
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
   
    public static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = objSdo.Id + '-' + 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
       	objrole.GP_Active__c = true;
        insert objrole;

        objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; 
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        prjObj.GP_PID_Approver_User__c = loggedInUser; 
        prjObj.GP_FP_A_Approver__c = loggedInUser; 
        prjObj.GP_New_SDO_s_MF_User__c = loggedInUser; 
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        
        insert prjObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        String strFieldSetName = '';
        for(Schema.FieldSet f : SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }
    }
	
    @isTest
    public static void testGPServiceSubmitforApproval() {
        setupCommonData();
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        
        project.GP_PID_Approver_User__c = UserInfo.getUserId();
        update project;
        
        GPServiceSubmitforApproval approvalService;
        
        approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        
        approvalService.submitforApproval();
		
        list<GP_Project__c> lstProjects = new list<GP_Project__c>();
        lstProjects.add(project);
        approvalService = new GPServiceSubmitforApproval(lstProjects);
    }
    
    @isTest
    public static void testGPServiceSubmitforApproval1() {
        setupCommonData();
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        project.GP_BPR_Approval_Required__c = false;
        project.GP_Controller_Approver_Required__c = true;
        update project;
        
        GPServiceSubmitforApproval approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        approvalService.submitforApproval();
    }
    
    @isTest
    public static void testGPServiceSubmitforApproval2() {
        setupCommonData();
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        project.GP_Controller_Approver_Required__c = false;
        project.GP_PID_Approver_Finance__c = true;
        update project;
        
        GPServiceSubmitforApproval approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        approvalService.submitforApproval();
    }
    
    @isTest
    public static void testGPServiceSubmitforApproval3() {
        setupCommonData();
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        project.GP_FP_And_A_Approval_Required__c = true;
        project.GP_PID_Approver_Finance__c = false;
        update project;
        
        GPServiceSubmitforApproval approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        approvalService.submitforApproval();
    }
    
    @isTest
    public static void testGPServiceSubmitforApproval4() {
        setupCommonData();
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        project.GP_FP_And_A_Approval_Required__c = false;
        project.GP_MF_Approver_Required__c = true;
        update project;
        
        GPServiceSubmitforApproval approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        approvalService.submitforApproval();
    }
    
     @isTest
    public static void testGPServiceSubmitforApproval5() {
        setupCommonData();
        
        GP_Role__c objrole1 = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole1.GP_Type_of_Role__c = 'Global';
        objrole1.GP_Role_Category__c = 'Product Approver';
        objrole1.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole1.GP_HSL_Master__c = null;
        insert objrole1;
        
        GP_User_Role__c objuserrole1 = GPCommonTracker.getUserRole(objrole1, objuser);
        insert objuserrole1;
        
        project = [Select Id, GP_Auto_Approval__c from GP_Project__c LIMIT 1];
        project.GP_Product_Approval_Required__c = true;
        project.GP_MF_Approver_Required__c = false;
        project.GP_PID_Approver_User__c = objuser.Id;
        update project;
        
        GPServiceSubmitforApproval approvalService = new GPServiceSubmitforApproval(new Set<Id> {project.Id});
        approvalService.submitforApproval();
    }
}