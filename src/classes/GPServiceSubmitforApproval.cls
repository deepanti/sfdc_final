public class GPServiceSubmitforApproval 
{
    private list<GP_Project__c> lstProjects;
    private set<id> setProjectIds;
    Map < String, List < Id >> mapOfRoleToListOfUserId;
    
    public  GPServiceSubmitforApproval(set<id> setProjectIds) {
        this.setProjectIds = setProjectIds;
    }
    public  GPServiceSubmitforApproval(list<GP_Project__c> lstProjects) {
        this.lstProjects = lstProjects;
    }
  
    private void fetchUserApproval(){
        Set < String > setOfRoles = new Set < String > ();
        Set < id > setSDO = new Set < id > ();
        Approval.ProcessSubmitRequest approvalProcess;
        for( GP_Project__c objProject: lstProjects)
        {
            if (objProject.GP_BPR_Approval_Required__c) {
                setOfRoles.add('BPR Approver');
            }
            if (objProject.GP_Product_Approval_Required__c) {
                setOfRoles.add('Product Approver');
            }
            setSDO.add(objProject.GP_Primary_SDO__c);
        }
        
        if(setOfRoles.size()>0){
        	mapOfRoleToListOfUserId = getMapOfRoleToApproverId(setOfRoles,setSDO);
        }
       
    }
    
    public void submitforApproval() {	
     	List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
     	set<id> setofUserIdsForApproval ;
     	string strKey;
     	setProjects();
     	fetchUserApproval();
     	Approval.ProcessSubmitRequest approvalProcess ;
     	map<id,List < Approval.ProcessSubmitRequest >> mapProjectIDtolstApproval = new map<id,List < Approval.ProcessSubmitRequest >>();
        
     	for(GP_Project__c objProject: lstProjects) {
     		//conditionally add BPR approval to list of approvals to be fired.
     		 	
            strKey =  objProject.GP_Primary_SDO__c+'-'+'BPR Approver';
            setofUserIdsForApproval = new set<id>();
            if (objProject.GP_BPR_Approval_Required__c &&
                mapOfRoleToListOfUserId.containsKey(strKey) && mapOfRoleToListOfUserId.get(strKey) != null) {
                for (Id userId: mapOfRoleToListOfUserId.get('BPR Approver')) {
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    
                    approvalProcess.setComments('Submitting request for BPR approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { userId });
                    approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                    setofUserIdsForApproval.add(userId);
                    requests.add(approvalProcess);
                }
            }
            //conditionally add Controller approval to list of approvals to be fired.
            if (objProject.GP_Controller_Approver_Required__c &&
                objProject.GP_Controller_User__c != null) {
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    approvalProcess.setComments('Submitting request for Controller approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { objProject.GP_Controller_User__c });
                    approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                    setofUserIdsForApproval.add(objProject.GP_Controller_User__c);
                    requests.add(approvalProcess);
                }
				
            //conditionally add PID approval to list of approvals to be fired.
            if (objProject.GP_PID_Approver_Finance__c &&
                objProject.GP_PID_Approver_User__c != null) {
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    
                    approvalProcess.setComments('Submitting request for PID approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { objProject.GP_PID_Approver_User__c });
                    setofUserIdsForApproval.add(objProject.GP_PID_Approver_User__c);
                    approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                    requests.add(approvalProcess);
                }
				
            if (objProject.GP_FP_And_A_Approval_Required__c &&
                objProject.GP_FP_A_Approver__c != null) {
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    
                    approvalProcess.setComments('Submitting request for FP&A approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { objProject.GP_FP_A_Approver__c });
                    setofUserIdsForApproval.add(objProject.GP_FP_A_Approver__c);
                    approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                    requests.add(approvalProcess);
                }
            if (objProject.GP_MF_Approver_Required__c &&
                objProject.GP_New_SDO_s_MF_User__c != null) {
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    
                    approvalProcess.setComments('Submitting request for MF approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { objProject.GP_New_SDO_s_MF_User__c });
                    setofUserIdsForApproval.add(objProject.GP_New_SDO_s_MF_User__c);
                    approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                    requests.add(approvalProcess);
                }
				strKey =  'Product Approver';
            if (objProject.GP_Product_Approval_Required__c &&
                mapOfRoleToListOfUserId.containsKey('Product Approver') && mapOfRoleToListOfUserId.get('Product Approver') != null) {
                    for (Id userId: mapOfRoleToListOfUserId.get('Product Approver')) {
                        approvalProcess = new Approval.ProcessSubmitRequest();
                        
                        approvalProcess.setComments('Submitting request for Product approval');
                        approvalProcess.setObjectId(objProject.id);
                        approvalProcess.setNextApproverIds(new Id[] { userId });
                        approvalProcess.setSubmitterId(objProject.GP_Current_Working_User__c);
                        setofUserIdsForApproval.add(userId);
                        requests.add(approvalProcess);
                    }
                }

            if (requests != null && requests.size() > 0) {
                mapProjectIDtolstApproval.put(objProject.id,requests);
            } else {
                System.debug('GP_Auto_Approval__c: ' + objProject.GP_Auto_Approval__c);
                
                if(objProject.GP_Auto_Approval__c)
                {	
                    approvalProcess = new Approval.ProcessSubmitRequest();
                    approvalProcess.setComments('Submitting request for Product Auto Approval');
                    approvalProcess.setObjectId(objProject.id);
                    approvalProcess.setNextApproverIds(new Id[] { objProject.GP_PID_Approver_User__c });
                    requests.add(approvalProcess);
                    mapProjectIDtolstApproval.put(objProject.id,requests);
                    //return new GPAuraResponse(true, 'Project has been auto approved as no specific Approval required.', null);
                }
            }
        }
        System.debug('requests: ' + requests);
        System.debug('requests.size(): ' + requests.size());
        if(requests.size()>0){
            List < Approval.ProcessResult > results = Approval.process(requests);
        }
    }
    
    private void setProjects() {
        if(lstProjects == null) {
            lstProjects = getProjectData();
        }
    }
    
    private list<GP_Project__c> getProjectData(){
        return  [Select Id, GP_Approval_Status__c, GP_Primary_SDO__c, GP_Current_Working_User__c,
                 GP_BPR_Approval_Required__c,GP_Controller_Approver_Required__c,
                 GP_Controller_User__c,GP_PID_Approver_Finance__c, GP_PID_Approver_User__c,
                 GP_Additional_Approval_Required__c,GP_FP_And_A_Approval_Required__c,
                 GP_FP_A_Approver__c,GP_MF_Approver_Required__c,GP_New_SDO_s_MF_User__c,
                 GP_Old_MF_Approver_Required__c, GP_Old_SDO_s_MF_User__c,
                 GP_Product_Approval_Required__c,GP_Band_3_Approver_Status__c,
                 GP_Auto_Approval__c
                 from GP_Project__c
                 where Id in: setProjectIds];
    }
    
    private Map < String, List < Id >> getMapOfRoleToApproverId(Set < String > setOfRoles, set<id> setSDO) {
        Map < String, List < Id >> mapOfRoleToListOfUserId = new Map < String, List < Id >> ();
		string strkey ;
        for (GP_Role__c objRole: [SELECT id, GP_Role_Category__c,GP_Type_of_Role__c,(SELECT Id, GP_User__c,GP_User__r.isActive, GP_Active__c 
                                            FROM User_Roles__r WHERE GP_User__r.isActive = true and GP_Active__c = true)
                                            FROM GP_Role__c
                                            WHERE GP_Role_Category__c in: setOfRoles
                                            and GP_Active__c = true
                							and((GP_Work_Location_SDO_Master__c in : setSDO and GP_Type_of_Role__c = 'SDO') 
                                                OR(GP_Type_of_Role__c = 'Global'))]) {
            if (objRole.User_Roles__r.size() > 0) {
                for (GP_User_Role__c objUserRole: objRole.User_Roles__r) {
                    if(objRole.GP_Type_of_Role__c=='SDO') {
                    	strkey =objRole.GP_Work_Location_SDO_Master__c+'-'+objRole.GP_Role_Category__c;
                    } else {
                    	strkey = objRole.GP_Role_Category__c;
                    }
                    
                    if(!mapOfRoleToListOfUserId.containsKey(strkey)) {
                        mapOfRoleToListOfUserId.put(strkey, new List < Id > ());
                    }
					mapOfRoleToListOfUserId.get(strkey).add(objUserRole.GP_User__c);
                }
            } else {
               mapOfRoleToListOfUserId.put(strkey, null);
            }
        }
        return mapOfRoleToListOfUserId;
    }
}