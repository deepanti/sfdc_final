@isTest
public class VIC_AdminApprovalDataManagerCtlrTest {

    public static OpportunityLineItem  objOLI;     
    public static user objuser;
     public static Master_Plan_Component__c masterPlanComponentObj;
    public static list<Target_Achievement__c> lstInc = new list<Target_Achievement__c>();
     public static list<Target_Achievement__c> lstInc1 = new list<Target_Achievement__c>();
      public static list<Target_Achievement__c> lstInc2 = new list<Target_Achievement__c>();
       public static list<Target_Achievement__c> lstInc3 = new list<Target_Achievement__c>();
    public static Target_Achievement__c ObjInc;
    public static Target_Achievement__c ObjInc1;
    public static Target_Achievement__c ObjInc2;
    public static Target_Achievement__c ObjInc3;
    public static case objcase;
    public static Map<Id,VIC_OpportunityLineItemDataWrapCtlr> mapCaseIdToOLIWrap= new Map<Id,VIC_OpportunityLineItemDataWrapCtlr>();
    public static list<VIC_OpportunityLineItemDataWrapCtlr> lstWrap= new list<VIC_OpportunityLineItemDataWrapCtlr>();
    static  testmethod void VIC_AdminApprovalDataManagerCtlr(){
        LoadData();
        test.startTest();
        VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
        VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('true','HR - OnHold');
        VIC_AdminApprovalDataManagerCtlr.approvSubmitIncentive('Test');
        VIC_AdminApprovalDataManagerCtlr.updatingIncentive(lstInc);
        Map<String, String> mapUserIdToVICRole = vic_CommonUtil.getUserVICRoleNameinMap();
        VIC_AdminApprovalDataManagerCtlr.fetchUserByVICRoleUser(mapUserIdToVICRole);
        VIC_AdminApprovalDataManagerCtlr.fetchUserBySupervisor(objuser.id,mapUserIdToVICRole);
        objcase = new case();
        insert objcase;
       Map<Target_Achievement__c,Case > mapIncentiveToObjCase = new Map<Target_Achievement__c,Case >();
       mapIncentiveToObjCase.put(ObjInc,objcase);
       
       VIC_OpportunityLineItemDataWrapCtlr OpportunityLineItemDataWrapCtlr=new VIC_OpportunityLineItemDataWrapCtlr(true,null,'','',
                                                         '','','','','','','','',true,'','',null,null,null,null,null,mapCaseIdToOLIWrap);
        OpportunityLineItemDataWrapCtlr.strCaseId= objcase.id;
        OpportunityLineItemDataWrapCtlr.strTotalVICPayable='20000';
        OpportunityLineItemDataWrapCtlr.lstUpfrontIncentive=lstInc ;
        OpportunityLineItemDataWrapCtlr.lstITKicker=lstInc1;
        OpportunityLineItemDataWrapCtlr.lstNewLogoBonus=lstInc2;
        OpportunityLineItemDataWrapCtlr.lstRenewal=lstInc3;
        OpportunityLineItemDataWrapCtlr.objAchievement=ObjInc;
        OpportunityLineItemDataWrapCtlr.objOLI=objOLI;
        lstWrap.add(OpportunityLineItemDataWrapCtlr);    
                                                     
        mapCaseIdToOLIWrap.put(OpportunityLineItemDataWrapCtlr.strCaseId,OpportunityLineItemDataWrapCtlr);                                               
        VIC_AdminApprovalDataManagerCtlr.addingIncentiveDataToOLIWraper(OpportunityLineItemDataWrapCtlr,ObjInc);
        VIC_AdminApprovalDataManagerCtlr.addONHoldIncentiveDataToOLIWraper(OpportunityLineItemDataWrapCtlr,ObjInc,objOLI);
        VIC_OpportunityDataWrapCtlr wrap1=new VIC_OpportunityDataWrapCtlr(false,null,'', '', '', '', '', '','','','','', '',null);
        wrap1.lstOLIDataWrap=lstWrap;
        wrap1.strTotalVICPayable='20000';
        VIC_AdminApprovalDataManagerCtlr.removeEmptyOLIDataWrap(wrap1);
        VIC_AdminApprovalDataManagerCtlr.updatingIncentiveStatus(mapIncentiveToObjCase,OpportunityLineItemDataWrapCtlr,'Test',objcase);
        System.assertEquals(200,200);
        test.stopTest();

    }
     static  testmethod void VIC_AdminApprovalDataManagerNCtlr(){
        LoadData();
         masterPlanComponentObj.vic_Component_Code__c='TCV_Accelerators'; 
         update masterPlanComponentObj;
         ObjInc.vic_Deal_Type__c='IO';
         update ObjInc;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
          test.starttest();
          VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
       System.assertEquals(200,200);

    }static  testmethod void VIC_AdminApprovalNDataManagerNCtlr(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='TCV_Accelerators';
         update masterPlanComponentObj;
         ObjInc.vic_Deal_Type__c='TS';
         update ObjInc;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
         test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
         System.assertEquals(200,200);
    }
    static  testmethod void VIC_NAdminApprovalNDataManagerNCtlr(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='OP';
         update masterPlanComponentObj;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       
        test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
       
          System.assertEquals(200,200);
    }
    static  testmethod void VIC_AdminApprovalNDataNManagerNCtlr(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='PM';
         update masterPlanComponentObj;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       
         test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
         System.assertEquals(200,200);

    }
    static  testmethod void VIC_AdminNApprovalNDataNManagerNCtlr(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='MBO';
         update masterPlanComponentObj;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
         System.assertEquals(200,200);

    }
    static  testmethod void VIC_NAdminNApprovalNDataNManagerNCtlr(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='TCV';
         update masterPlanComponentObj;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       
         test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
         System.assertEquals(200,200);

    }
    static  testmethod void VIC_AdminNApprovalNDataNManagerNCtlrN(){
        LoadData();
         
         masterPlanComponentObj.vic_Component_Code__c='IO_TCV';
         update masterPlanComponentObj;
         VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       
         test.starttest();
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
         test.stoptest();
       System.assertEquals(200,200);

    }
     static void LoadData()
    {
         VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
         vicInfo.VIC_Process_Year__c=2018;
        vicInfo.VIC_Annual_Process_Year__c=2018;
        vicInfo.VIC_Calculate_HR_Day__c=2018;
         insert vicInfo;
        
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn11@jdjhdg.com','Genpact Super Admin','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
            
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
            
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
            
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
            
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
            
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';  
        planObj1.Year__c ='2018';
        insert planobj1;
                
            
        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser1.id;
        objopp.Sales_country__c='Canada';
        
        insert objOpp;
        
        
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_Contract_Term__c=24;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;   
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.vic_IO_TS__c ='TS';    
        insert objOLI;
         
        objopp.Number_of_Contract__c=2;    
        objOpp.stagename='6. Signed Deal';
       system.runas(objuser){
        update objOpp;
        }
        
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        targetObj.Plan__c=planobj1.id;
        insert targetObj;
        
         masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Discretionary_Payment'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
         Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='kickers'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
        Master_Plan_Component__c masterPlanComponentObj2=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj2.vic_Component_Code__c='New_logo_Bonus'; 
        masterPlanComponentObj2.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj2;
         Master_Plan_Component__c masterPlanComponentObj3=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj3.vic_Component_Code__c='Renewal'; 
        masterPlanComponentObj3.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj3;
            
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
            
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.target__r=targetObj;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        Target_Component__c targetComponentObj1=VIC_CommonTest.getTargetComponent();
        targetComponentObj1.Target__C=targetObj.id;
        targetComponentObj1.target__r=targetObj;
        targetComponentObj1.Master_Plan_Component__c=masterPlanComponentObj1.id;
        insert targetComponentObj1;
        Target_Component__c targetComponentObj2=VIC_CommonTest.getTargetComponent();
        targetComponentObj2.Target__C=targetObj.id;
        targetComponentObj2.target__r=targetObj;
        targetComponentObj2.Master_Plan_Component__c=masterPlanComponentObj2.id;
        insert targetComponentObj2;
        Target_Component__c targetComponentObj3=VIC_CommonTest.getTargetComponent();
        targetComponentObj3.Target__C=targetObj.id;
        targetComponentObj3.target__r=targetObj;
        targetComponentObj3.Master_Plan_Component__c=masterPlanComponentObj3.id;
        insert targetComponentObj3;
        
        
        ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.Target_Component__r=targetComponentObj;
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc.vic_Status__c ='HR - OnHold';
        ObjInc.VIC_Description__c='Tests';
        insert objInc;
       
        ObjInc1= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc1.Target_Component__r=targetComponentObj1;
        ObjInc1.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc1.vic_Status__c ='HR - OnHold';
        insert objInc1;
       
        ObjInc2= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc2.Target_Component__r=targetComponentObj2;
        ObjInc2.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc2.vic_Status__c ='HR - OnHold';
        insert objInc2;
        ObjInc3= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc3.Target_Component__r=targetComponentObj3;
        ObjInc3.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc3.vic_Status__c ='HR - OnHold';
        insert objInc3;
        
        lstInc.add(objInc);
        lstInc1.add(objInc1);
        lstInc2.add(objInc2);
        lstInc3.add(objInc3);
        
    }
}