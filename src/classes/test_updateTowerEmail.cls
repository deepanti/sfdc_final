@istest
public class test_updateTowerEmail {
    private static Account objaccount;
    private static Opportunity objOpportunity;
    private static QSRM__c objQsrm;
    private static OpportunityProduct__c objproduct;
    private static user objuser;
    
    private static void Loaddata(boolean isDSRCreated, boolean dontCreateDSR, boolean dsrCreatedManually)
    {       
        GW1_CommonTracker.createTriggerCustomSetting();
        objaccount = GW1_CommonTracker.createAccount('abcd');
        objaccount.Industry_Vertical__c = 'BFS';
        update objaccount;
        objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', isDSRCreated, dontCreateDSR,dsrCreatedManually, objAccount.Id);
        //objproduct = GW1_CommonTracker.createOppProduct(objOpportunity,'Consulting'); // 
        //objproduct.TCVLocal__c = 5000000;
        //update objproduct;
       // objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
    } 
    
    static testMethod void testScenario1()
    {        Test.startTest();
        Loaddata(false,false,false);
     Nature_Of_Work__c objNature = new Nature_Of_Work__c();
        objNature.Name = 'Consulting';
        insert objNature;
        
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Pricebook2 oPriceBook = GEN_Util_Test_Data.CreatePricebook2();
        ID objPbID = Test.getStandardPricebookId();


        PricebookEntry objPriceBookEntry = new PricebookEntry(); // Line Commented by Shubhi      
        //List<Pricebook2> lstPriceBook = [select Id , name from Pricebook2 where isStandard=true];
        //objPriceBookEntry =new PriceBookEntry();
        objPriceBookEntry.isActive = true;
        objPriceBookEntry.Product2id = oProduct.id;
        objPriceBookEntry.UnitPrice = 122.00;

        //objPriceBookEntry.PriceBook2id=lstPriceBook!=null && lstPriceBook.size()>0 ?lstPriceBook[0].id : objpricebook2.Id;
        objPriceBookEntry.PriceBook2id = objPbID;
        objPriceBookEntry.UseStandardPrice = false;
        insert objPriceBookEntry;

        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.Product_Autonumber__c = 'OLI';
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today();
        oOpportunityProduct.ContractTerminmonths__c = 26;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId();
        oOpportunityProduct.Nature_Of_Work_Lookup__c = objNature.Id;
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c = 2;
        oOpportunityProduct.FTE_7th_month__c = 2;
        oOpportunityProduct.FTE_10th_month__c = 2;
        oOpportunityProduct.OpportunityId__c = objOpportunity.Id;
        oOpportunityProduct.TCVLocal__c = 6000000;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c = true;
        oOpportunityProduct.HasSchedule__c = true;
        oOpportunityProduct.HasQuantitySchedule__c = false;
        insert oOpportunityProduct;

     
     
        objOpportunity.StageName = '2. Define';
     objOpportunity.GW1_DSR_Created_Manually__c = true;
     objOpportunity.GW1_DSR_Created__c = false;
     objOpportunity.Nature_Of_Work_OLI__c = 'Consulting';
     objOpportunity.DSR_bypass__c = true;
     
     GW1_Tower_Leader_Mapping__c tlmap  = new GW1_Tower_Leader_Mapping__c();
     tlmap.GW1_Industry_Vertical__c = 'BFS';
     tlmap.GW2_Sales_Reagion__c = 'Asia';
     insert tlmap;
     Validator_cls.AllowGenTUpdateAccounArche=true;
     
     
     	update objOpportunity;
        
     //GW1_DSR__c dsr = new GW1_DSR__c();

		
        Test.stoptest();
    }
    
}