global class ExcelExportLead implements Database.Batchable <sobject>,Database.stateful
{
    Static string header = 'Name' + ',' + 'Email' + ',' + 'Phone' + ',' + 'COUNTRY OF RESIDENCE' + ',' + 'COMPANY' + ',' + 'INDUSTRY VERTICAL' + ',' + 'TITLE' + ',' + 'BUYING CENTER' + ',' + 'LEVEL OF CONTACT' + ',' + 'RECORD TYPE' + ',' + 'ACCOUNT ARCHETYPE' + ',' + 'LEAD SCORE' + ',' + 'LIFECYCLE STATUS' + ',' + 'RECENT CAMPAIGN NAME';
    
    Public List<string> Lines;
    private String strParameterQ2;  
    
    public ExcelExportLead(String strParamq2)
    {
        Lines =new List<string>(); 
        strParameterQ2 = strParamq2;    
        system.debug('=====strParameterQ1===in Constructor=='+strParameterQ2);        
    }
    Public String generatedCSVFile ='';
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    {    lines.add(header);
     system.debug('======strParameterQ2====start=='+strParameterQ2);
     string q = strParameterQ2;       
     system.debug('=====q======='+q);       
     return database.getQueryLocator(q);
    }
    
    global void execute(Database.BatchableContext bc, List<Lead> scope)
    {    //Boolean HeaderFlag = True;
        // system.debug('=====scope===='+scope);   
        string fileRow;
        // if(HeaderFlag){
        // lines.add(header); 
        //  HeaderFlag = False;
        //        }
        for(Lead  a: scope)
        {     
            //SELECT name,Email,Phone,Country_Of_Residence__c,Company,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto71_Lead_Score__c,Lifecycle_Status__c,Recent_Channel_Detail__c,Vertical__c FROM Lead  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false 
            
            if(a.name != null && a.name != ''){
                if(a.name.contains(',')){
                    fileRow = '"'+a.name+'"';
                }else{
                    fileRow =  a.name; 
                }
            }
            else{fileRow =''; }
            
            
            if(a.Email != null && a.Email != ''){
                if(a.Email.contains(',')){
                    fileRow = fileRow +','+'"'+a.Email+'"';
                }else{
                    fileRow = fileRow +','+ a.Email;
                }
            }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Phone != null && a.Phone != ''){
                if(a.Phone.contains(',')){
                    fileRow = fileRow +','+'"'+a.Phone+'"';
                }else{
                    fileRow = fileRow +','+ a.Phone;
                }       
            }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Country_Of_Residence__c != null && a.Country_Of_Residence__c != ''){
                if(a.Country_Of_Residence__c.contains(',')){
                    fileRow = fileRow +','+'"'+a.Country_Of_Residence__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Country_Of_Residence__c;
                }
            }else{fileRow =fileRow +','+'';}  
            
            
            
            if(a.Company != null && a.Company != ''){
                if(a.Company.contains(',')){
                    fileRow = fileRow +','+'"'+a.Company+'"';
                }else{
                    fileRow = fileRow +','+ a.Company;
                }
            }else{fileRow =fileRow +','+'';}
            
            
            if(a.Vertical__c != null && a.Vertical__c != ''){
                if(a.Vertical__c.contains(',')){
                    fileRow = fileRow +','+'"'+a.Vertical__c+'"'; 
                }else{
                    fileRow = fileRow +','+ a.Vertical__c; 
                }
            }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Title != null && a.Title != ''){
                if(a.Title.contains('"') ){
                    String Str = a.Title.replace('"','');
                    fileRow = fileRow +','+'"'+Str+'"';
                }else if(a.Title.contains(',')){
                    fileRow = fileRow +','+'"'+a.Title+'"';
                }else{
                    fileRow = fileRow +','+ a.Title;
                }
            }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Buying_Center__c != null && a.Buying_Center__c != ''){
                if(a.Buying_Center__c.contains(',')){ fileRow = fileRow +','+'"'+a.Buying_Center__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Buying_Center__c;
                }
            }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Level_of_Contact__c != null && a.Level_of_Contact__c != ''){
                if(a.Level_of_Contact__c.contains(',')){  fileRow = fileRow +','+'"'+a.Level_of_Contact__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Level_of_Contact__c;
                }
            } else{fileRow =fileRow +','+'';}
            
            
            
            fileRow = fileRow +','+'Lead';
            
            
            
            if(a.Archetype__c != null && a.Archetype__c != ''){
                if(a.Archetype__c.contains(',')){  fileRow = fileRow +','+'"'+a.Archetype__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Archetype__c; }}else{fileRow =fileRow +','+'';}  
            
            
            
            if(a.mkto71_Lead_Score__c != null){
                fileRow = fileRow +','+ a.mkto71_Lead_Score__c; }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Lifecycle_Status__c != null && a.Lifecycle_Status__c != ''){
                if(a.Lifecycle_Status__c.contains(',')){ fileRow = fileRow +','+'"'+a.Lifecycle_Status__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Lifecycle_Status__c; } }else{fileRow =fileRow +','+'';}
            
            
            
            if(a.Recent_Channel_Detail__c != null && a.Recent_Channel_Detail__c != ''){
                if(a.Recent_Channel_Detail__c.contains(',')){ fileRow = fileRow +','+'"'+a.Recent_Channel_Detail__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Recent_Channel_Detail__c; } }else{fileRow =fileRow +','+'';}
            
            
            lines.add(fileRow); 
        }
        system.debug('lines-=====execute========='+lines);     
    }
    
    global void finish(Database.BatchableContext bc)
    {  if(lines.size() > 2){
        system.debug('lines-=====finish='+lines);
        generatedCSVFile = generatedCSVFile + string.join(lines, '\n');
        lines=null;
        String today = Datetime.now().format('dd-MM-yyyy');
        Blob csvBlob = blob.valueOf(generatedCSVFile);
        String csvName = 'Lead Data as on '+today+'.csv';   
       
            Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
            Blob csvBlob1 = blob.valueOf(generatedCSVFile);
            String csvName1 = 'MDM Lead Report.csv';
            csvAttachment.setFileName(csvName1);
            csvAttachment.setBody(csvBlob1);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{UserInfo.getUserEmail()};
                String subject = 'MDM Lead Report Export';
            email.setSubject(subject);
            email.setToAddresses(toAddresses);
            email.setPlainTextBody('Lead details  CSV');
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
            system.debug('======email=Lead==='+email);
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});         
        }
    }
    
}