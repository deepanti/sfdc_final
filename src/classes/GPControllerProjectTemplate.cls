/**
 * @author Anmol.kumar
 * @date 22/10/2017
 *
 * @group ProjectTemplate
 * @group-content ../../ApexDocContent/ProjectTemplate.html
 *
 * @description Apex Service for project Template module,
 * has aura enabled method to CRUD project Template data.
 */
public without sharing class GPControllerProjectTemplate {

    private static final String SUCCESS_LABEL = 'SUCCESS';
    private static final String ERROR_LABEL = 'ERROR';

    private static final String MAP_OF_SERVICE_LINE_TO_SUBCATEGORY_OPTIONS = 'mapOfServiceLineToListOfSubCategory';
    private static final String PARENT_PROJECT_FOR_GROUP_INVOICE_LABEL = 'listOfParentProjectForGroupInvoiceOptions';
    private static final String UNAUTHORIZED_LABEL = 'You are not authorized to edit this project';
    private static final String MAP_OF_FIELD_NAME_TO_OPTIONS = 'mapOfFieldNameToListOfOptions';
    private static final String ERROR_ACTIVE_TEMPLATE_LABEL = 'Project template is Active';
    private static final String PROJECT_TEMPLATE_LABEL = 'ProjectTemplate';
    private static final String INVALID_CONTEXT_LABEL = 'Invalid Context';
    private static final String PROJECT_FIELDSET_NAME = 'GP_All_Fields';
    private static final String PROJECT_API_NAME = 'GP_Project__c';
    private static final String PROJECT_LABEL = 'Project';

    private static GP_Employee_Master__c golEmployee, grmEmployee;
    private static GP_Project__c projectData;
    private static GP_Deal__c dealData;
    private final static List < String > lstOfFinalProjectTemplateFields = new List < String > {
        'Name',
        'GP_Final_JSON_1__c',
        'GP_Final_JSON_2__c',
        'GP_Final_JSON_3__c',
        'GP_Without_SFDC__c'
    };

    private final static List < String > lstOfDeltaProjectTemplateFields = new List < String > {
        'Name',
        'GP_Active__c',
        'GP_Without_SFDC__c',
        'GP_Delta_JSON_1__c',
        'GP_Delta_JSON_2__c',
        'GP_Delta_JSON_3__c',
        'GP_Business_Name__c',
        'GP_Business_Type__c',
        'GP_Business_Group_L1__c',
        'GP_Business_Segment_L2__c'
    };

    private static Map < String, Object > mapOfMappingForDealData = new Map < String, Object > ();
    private static Map < String, List < GPOptions >> mapOfServiceLineToListOfSubCategory;
    private static Map < String, List < GPOptions >> mapOfFieldNameToListOfOptions;

    public static GPAuraResponse getProjectWithTemplate(Id projectId) {
        //validate project Id
        if (!isProject(projectId))
            return new GPAuraResponse(false, INVALID_CONTEXT_LABEL, null);

        //validate user access1
        string strAccess = GPCommon.getScreenAccess('projectcreation', userInfo.getUserId());

        if (strAccess != null &&
            !strAccess.equals('read') &&
            !strAccess.equals('edit') &&
            !strAccess.equals('modify')) {
            return new GPAuraResponse(false, UNAUTHORIZED_LABEL, null);
        }

        List < String > listOfFields = getFieldSet(PROJECT_FIELDSET_NAME, PROJECT_API_NAME);

        String projectQueryString = 'Select ' + String.join(listOfFields, ', ');

        projectQueryString += ', GP_Deal__r.GP_Deal_Category__c, GP_Operating_Unit__r.GP_Oracle_Id__c,GP_Operating_Unit__r.GP_LE_Code__c,GP_GOL__r.GP_Employee_Unique_Name__c,GP_Collector__r.GP_Employee_Unique_Name__c,GP_GRM__r.GP_Employee_Unique_Name__c, RecordType.Name';
        projectQueryString += ' from GP_Project__c where Id = \'' + projectId + '\'';

        JSONGenerator gen = JSON.createGenerator(true);

        try {
            projectData = Database.query(projectQueryString);
            dealData = [Select Id, GP_Sales_Opportunity_Id__c, GP_Account_Name_L4__c, GP_Deal_Category__c From GP_Deal__c where Id =: projectData.GP_Deal__c];
            GP_Project_Template__c projectTemplate = getProjectTemplate(projectData.GP_Project_Template__c, lstOfFinalProjectTemplateFields);

            setMapOfFieldNameToOptions();
            setMapOflookupPicklist();
            gen.writeStartObject();
            gen.writeObjectField(PROJECT_LABEL, projectData);

            if (projectTemplate != null) {
                gen.writeObjectField(PROJECT_TEMPLATE_LABEL, projectTemplate);
            }

            if (mapOfFieldNameToListOfOptions != null) {
                gen.writeObjectField(MAP_OF_FIELD_NAME_TO_OPTIONS, mapOfFieldNameToListOfOptions);
            }

            if (mapOfServiceLineToListOfSubCategory != null) {
                gen.writeObjectField(MAP_OF_SERVICE_LINE_TO_SUBCATEGORY_OPTIONS, mapOfServiceLineToListOfSubCategory);
            }

            gen.writeEndObject();
        } catch (Exception ex) {
            System.debug('ex.getMessage()' + ex.getMessage());
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }



    @TestVisible
    private static void setMapOfFieldNameToOptions() {
        mapOfFieldNameToListOfOptions = new Map < String, List < GPOptions >> ();

        List < GPOptions > listOfOperatingUnitOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Operating_Unit__c');
        List < GPOptions > listOfProjectTypeOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Project_type__c');
        List < GPOptions > listOfBillingRateTypeOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Bill_Rate_Type__c');
        List < GPOptions > listOfProjectCurrencyOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Project_Currency__c');
        List < GPOptions > listOfRemittanceModeOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Remittance_Mode__c');
        List < GPOptions > listOfCrossSellOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_CrossSellCharge__c');
        List < GPOptions > listOfMODOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_MOD__c');
        List < GPOptions > listOfBillingCurrencyOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Billing_Currency__c');
        List < GPOptions > listOfTimeSheetRequirementOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Timesheet_Requirement__c');
        List < GPOptions > listOfOMSStatusOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_OMS_Status__c');
        List < GPOptions > listOfInvoiceFormatOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Invoice_Format__c');
        //List<GPOptions> listOfDailyNormalHoursOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Timesheet_Requirement__c');
        //List<GPOptions> listOfMarketServicedOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Market_Serviced__c');
        List < GPOptions > listOfInvoiceFrequencyOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Invoice_Frequency__c');
        List < GPOptions > listOfProjectProfileOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Project_Profile__c');
        List < GPOptions > listOfProjectStatusOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Project_Status__c');
        List < GPOptions > listOfUserRequestedStatusOptions = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_User_Requested_Status__c');

        List < GPOptions > listOfUsCity = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_City__c');
        List < GPOptions > listOfUsState = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_State__c');
		//Avinash : Added to fetch country on Other classification form.
        List < GPOptions > listOfUsCountry = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Country__c');
		 //<!--Avinash: GE Dvine code changes -->
        List < GPOptions > listOfGPDivine = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Divine__c');
       
        List < GPOptions > listOfServicesToBeDeliveredFrom = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Services_To_Be_Delivered_From__c');
        List < GPOptions > listOfProjectCategory = GPInputPicklistService.getPicklistOptions('GP_Project__c', 'GP_Project_Category__c');

        mapOfFieldNameToListOfOptions.put('operatingUnitOptions', listOfOperatingUnitOptions);
        mapOfFieldNameToListOfOptions.put('ProjectTypeOptions', listOfProjectTypeOptions);
        mapOfFieldNameToListOfOptions.put('BillingRateTypeOptions', listOfBillingRateTypeOptions);
        mapOfFieldNameToListOfOptions.put('listOfProjectCurrencyOptions', listOfProjectCurrencyOptions);
        mapOfFieldNameToListOfOptions.put('RemittanceModeOptions', listOfRemittanceModeOptions);
        mapOfFieldNameToListOfOptions.put('CrossSellOptions', listOfCrossSellOptions);
        mapOfFieldNameToListOfOptions.put('listOfMODOptions', listOfMODOptions);
        mapOfFieldNameToListOfOptions.put('listOfOMSStatusOptions', listOfOMSStatusOptions);
        mapOfFieldNameToListOfOptions.put('listOfTimeSheetRequirementOptions', listOfTimeSheetRequirementOptions);
        mapOfFieldNameToListOfOptions.put('listOfInvoiceFormatOptions', listOfInvoiceFormatOptions);
        //mapOfFieldNameToListOfOptions.put('listOfDailyNormalHoursOptions', listOfDailyNormalHoursOptions);
        //mapOfFieldNameToListOfOptions.put('listOfMarketServicedOptions', listOfMarketServicedOptions);
        mapOfFieldNameToListOfOptions.put('listOfInvoiceFrequencyOptions', listOfInvoiceFrequencyOptions);
        mapOfFieldNameToListOfOptions.put('listOfProjectProfileOptions', listOfProjectProfileOptions);
        mapOfFieldNameToListOfOptions.put('listOfProjectStatusOptions', listOfProjectStatusOptions);
        mapOfFieldNameToListOfOptions.put('listOfUserRequestedStatusOptions', listOfUserRequestedStatusOptions);
        mapOfFieldNameToListOfOptions.put('listOfBillingCurrencyOptions', listOfBillingCurrencyOptions);

        mapOfFieldNameToListOfOptions.put('listOfUsCity', listOfUsCity);
        mapOfFieldNameToListOfOptions.put('listOfUsState', listOfUsState);
		//Avinash :
        mapOfFieldNameToListOfOptions.put('listOfUsCountry', listOfUsCountry);
		//<!--Avinash: GE Divine code changes -->
        mapOfFieldNameToListOfOptions.put('listOfGPDivine', listOfGPDivine);
        
        mapOfFieldNameToListOfOptions.put('listOfServicesToBeDeliveredFrom', listOfServicesToBeDeliveredFrom);
        mapOfFieldNameToListOfOptions.put('listOfProjectCategory', listOfProjectCategory);
    }

    public static List < String > getFieldSet(String fieldSetName, String objectName) {

        Map < String, Schema.SObjectType > globalDescribeMap = Schema.getGlobalDescribe();

        Schema.SObjectType sObjectTypeObj = globalDescribeMap.get(objectName);
        Schema.DescribeSObjectResult describeSObjectResultObj = sObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = describeSObjectResultObj
            .FieldSets
            .getMap()
            .get(fieldSetName);

        if (fieldSetObj != null) {
            return getListOfFields(fieldSetObj.getFields());
        } else {
            return null;
        }
    }

    private static List < String > getListOfFields(List < Schema.FieldSetMember > listOfFieldSetMembers) {

        Set < String > setOfFields = new Set < String > ();

        for (Schema.FieldSetMember member: listOfFieldSetMembers) {
            String fieldName = member.getFieldPath();
            setOfFields.add(fieldName);
            String fieldType;

            if (!fieldName.contains('__r')) {
                fieldType = getFieldType('GP_Project__c', fieldName);
            }

            if (fieldType == 'REFERENCE') {
                setOfFields.add(fieldName.replace('__c', '__r.Name'));
            } else if (fieldType == 'ID') {
                setOfFields.add(fieldName.replace('ID', '.Name'));
            }
        }
        List < String > lstOfFields = new List < String > ();
        lstOfFields.addAll(setOfFields);
        return lstOfFields;
    }

    public static String getFieldType(String SObjectName, String fieldName) {
        Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType objectSchema = schemaMap.get(SObjectName);
        Map < String, Schema.SObjectField > fieldMap = objectSchema.getDescribe().fields.getMap();
        String fieldType = String.valueOf(fieldMap.get(fieldName).getDescribe().getType());

        return fieldType;
    }

    public static GPAuraResponse getFinalProjectTemplate(Id projectTemplateId, String serializedDealData) {
        GP_Project_Template__c projectTemplate;
        JSONGenerator gen;
        try {
            dealData = (GP_Deal__c) JSON.deserialize(serializedDealData, GP_Deal__c.class);

            if (projectTemplateId != null) {
                projectTemplate = getProjectTemplate(projectTemplateId, lstOfFinalProjectTemplateFields);
            }

            setMapOfFieldNameToOptions();
            setMapOflookupPicklist();

            gen = JSON.createGenerator(true);
            gen.writeStartObject();

            if (projectTemplate != null) {
                gen.writeObjectField(PROJECT_TEMPLATE_LABEL, projectTemplate);
            }

            if (mapOfFieldNameToListOfOptions != null) {
                gen.writeObjectField(MAP_OF_FIELD_NAME_TO_OPTIONS, mapOfFieldNameToListOfOptions);
            }

            if (mapOfServiceLineToListOfSubCategory != null) {
                gen.writeObjectField(MAP_OF_SERVICE_LINE_TO_SUBCATEGORY_OPTIONS, mapOfServiceLineToListOfSubCategory);
            }

            gen.writeEndObject();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    public static GPAuraResponse getDeltaProjectTemplate(Id projectTemplateId) {

        GP_Project_Template__c assesmentTemplate;
        Integer numberOfProjectsUnderTemplate;
        // Payment Term Changes
        Boolean isSystemAdmin = false;

        try {
            if (projectTemplateId != null) {
                assesmentTemplate = getProjectTemplate(projectTemplateId, lstOfDeltaProjectTemplateFields);
                numberOfProjectsUnderTemplate = [SELECT count() FROM GP_Project__c where GP_Project_Template__c =: projectTemplateId];
                // Payment Term Changes
                isSystemAdmin = UserInfo.getProfileId() == Label.GP_System_Admin_Profile_Id ? true:false;
            }
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField('assesmentTemplate', assesmentTemplate);
        gen.writeObjectField('numberOfProjectsUnderTemplate', numberOfProjectsUnderTemplate);
        // Payment Term Changes
        gen.writeObjectField('isSystemAdmin', isSystemAdmin);
        gen.writeEndObject();

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    private static GP_Project_Template__c getProjectTemplate(Id projectTemplateId, List < String > listOfFields) {
        String ProjectTemplateQueryString = 'Select ';
        ProjectTemplateQueryString += String.join(listOfFields, ', ');
        ProjectTemplateQueryString += ' from GP_Project_Template__c where Id = \'' + projectTemplateId + '\'';

        GP_Project_Template__c projectTemplate = Database.query(ProjectTemplateQueryString);

        return projectTemplate;
    }

    public static GPAuraResponse createProjectTemplate(String serializedTemplateRecord) {

        GPProjectTemplateWrapper templateWrapper = (GPProjectTemplateWrapper) JSON.deserialize(serializedTemplateRecord, GPProjectTemplateWrapper.class);
        GP_Project_Template__c projectTemplate;

        if (templateWrapper.assesmentTemplateId == null) {
            projectTemplate = new GP_Project_Template__c();
        } else {
            projectTemplate = getProjectTemplate(templateWrapper.assesmentTemplateId, lstOfDeltaProjectTemplateFields);
        }

        projectTemplate.GP_Default_JSON_1__c = templateWrapper.defaultJSON1;
        projectTemplate.GP_Default_JSON_2__c = templateWrapper.defaultJSON2;
        projectTemplate.GP_Default_JSON_3__c = templateWrapper.defaultJSON3;

        projectTemplate.GP_Delta_JSON_1__c = templateWrapper.deltaJSON1;
        projectTemplate.GP_Delta_JSON_2__c = templateWrapper.deltaJSON2;
        projectTemplate.GP_Delta_JSON_3__c = templateWrapper.deltaJSON3;

        projectTemplate.GP_Final_JSON_1__c = templateWrapper.finalJSON1;
        projectTemplate.GP_Final_JSON_2__c = templateWrapper.finalJSON2;
        projectTemplate.GP_Final_JSON_3__c = templateWrapper.finalJSON3;

        projectTemplate.GP_Stage_Wise_Field_Project__c = templateWrapper.stageWiseFieldJson;

        projectTemplate.Name = templateWrapper.templateName;

        projectTemplate.GP_Business_Name__c = templateWrapper.businessName;
        projectTemplate.GP_Business_Type__c = templateWrapper.businessType;

        projectTemplate.GP_Active__c = templateWrapper.isActive;
        projectTemplate.GP_Without_SFDC__c = templateWrapper.isWithoutSFDC;

        projectTemplate.GP_Business_Group_L1__c = templateWrapper.businessGroupL1;
        projectTemplate.GP_Business_Segment_L2__c = templateWrapper.businessSegmentL2;

        try {
            upsert projectTemplate;
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, projectTemplate.Id);
    }

    private static Boolean isProject(Id sObjectId) {
        return sObjectId != null &&
            sObjectId
            .getSObjectType()
            .getDescribe()
            .getName() == PROJECT_API_NAME;
    }

    private static void setMapOflookupPicklist() {
        //set list of bill to name
        setMapOfParentProfile();
        setMapOfPinnacleMaster();
        setMapOfCRNNumber();
    }

    private static void setMapOfCRNNumber() {
        if (projectData == null && dealData == null) {
            return;
        }

        List < GPOptions > listOfCRNNumber = getListOfCRNNUmber();

        mapOfFieldNameToListOfOptions.put('CRNNumber', listOfCRNNumber);
    }

    public static List < GPOptions > getListOfCRNNUmber() {

        List < GPOptions > listOfCRNNumber = new List < GPOptions > ();
        string opportunityId = dealData != null ? dealData.GP_Sales_Opportunity_Id__c : null; //projectData != null ? projectData.GP_Opportunity__c : dealData.GP_Opportunity_ID__c;
        Id accountId = projectData != null ? projectData.GP_Customer_Hierarchy_L4__c : (dealData != null ? dealData.GP_Account_Name_L4__c : null);

        String activeSignedContractStatus = System.Label.GP_Active_CRN_Status_On_Project;
        List < String > listOfActiveStatus = activeSignedContractStatus.split(',');

      
        //Pankaj 
        if(projectData != null &&  projectData.GP_CRN_Number__c != null && !activeSignedContractStatus.ContainsIgnorecase(projectData.GP_CRN_Status__c) ){
            listOfActiveStatus.add(projectData.GP_CRN_Status__c);
        }

        String query = 'SELECT ID, Name,GP_Type__c FROM GP_Icon_Master__c WHERE GP_Status__c  in :listOfActiveStatus ';

        if (opportunityId != null) {
            query += 'AND  GP_Opportunity_Code__c = :opportunityId ';
        } else if (accountId != null) {
            query += 'AND  GP_ACCOUNT_CODE__c =: accountId ';
        }

        if (opportunityId == null && accountId == null && (dealData == null || dealData.GP_Deal_Category__c == null || dealData.GP_Deal_Category__c != 'Sales SFDC')) {
            query += ' AND GP_Type__c = \'Dummy\' LIMIT 1';
        } else {
            query += ' AND GP_Type__c != \'Dummy\'';
        }

        List < GP_Icon_Master__c > listOfIconMaster = Database.query(query);

        if (listOfIconMaster == null || listOfIconMaster.isEmpty()) {
            listOfIconMaster = [SELECT ID, Name, GP_Type__c FROM GP_Icon_Master__c WHERE GP_Status__c in: listOfActiveStatus And GP_Type__c = 'Dummy'
                LIMIT 1
            ];
        }

        for (GP_Icon_Master__c iconMaster: listOfIconMaster) {
            listOfCRNNumber.add(new GPOptions(iconMaster.Id, iconMaster.Name, iconMaster.GP_Type__c));
        }

        return listOfCRNNumber;
    }

    private static void setMapOfPinnacleMaster() {
        mapOfServiceLineToListOfSubCategory = new Map < String, List < GPOptions >> ();

        List < GPOptions > listOfHSL = new List < GPOptions > ();
        List < GPOptions > listOfVSL = new List < GPOptions > ();
        List < GPOptions > listOfSubDivision = new List < GPOptions > ();
        List < GPOptions > listOfSILO = new List < GPOptions > ();
        List < GPOptions > listOfPortalMaster = new List < GPOptions > ();
        List < GPOptions > listOfProjectOrganization = new List < GPOptions > ();
        List < GPOptions > listOfSubCategoryOfServices = new List < GPOptions > ();
        List < GPOptions > listOfOperatingUnit = new List < GPOptions > ();
        List < GPOptions > listOfHSN = new List < GPOptions > ();

        List < GP_Pinnacle_Master__c > listOfPinnacleMaster;

        if (projectData != null && projectData.Id != null) {
            listOfPinnacleMaster = GPSelectorPinnacleMasters.getAllPinnacleMasterData(projectData);
        } else {
            listOfPinnacleMaster = GPSelectorPinnacleMasters.getAllPinnacleMasterData();
        }

        GP_Pinnacle_Master__c globalSubDivisionRecord, globalSiloRecord;

        for (GP_Pinnacle_Master__c pinnacleMaster: listOfPinnacleMaster) {
            String label = pinnacleMaster.GP_Description__c + ' -- ';
            label += pinnacleMaster.GP_Oracle_Id__c != null ? pinnacleMaster.GP_Oracle_Id__c : 'Oracle Id Not Available';

            if (pinnacleMaster.RecordType.Name == 'HSL Master') {
                listOfHSL.add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
            } else if (pinnacleMaster.RecordType.Name == 'VSL Master') {
                listOfVSL.add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
            } else if (pinnacleMaster.RecordType.Name == 'Sub Division') {
                if (pinnacleMaster.GP_Parent_Customer_L2__c == null || pinnacleMaster.GP_Parent_Customer_L2__c == '')
                    globalSubDivisionRecord = pinnacleMaster;
                else if (projectData != null && projectData.GP_Business_Segment_L2__c == pinnacleMaster.GP_Parent_Customer_L2__c)
                    listOfSubDivision.add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
            } else if (pinnacleMaster.RecordType.Name == 'Silo') {
                if (pinnacleMaster.GP_Parent_Customer_L2__c == null || pinnacleMaster.GP_Parent_Customer_L2__c == '')
                    globalSiloRecord = pinnacleMaster;
                else if (projectData != null && projectData.GP_Business_Segment_L2__c == pinnacleMaster.GP_Parent_Customer_L2__c)
                    listOfSILO.add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
            } else if (pinnacleMaster.RecordType.Name == 'Portal Master') {
                listOfPortalMaster.add(new GPOptions(pinnacleMaster.Id, label, pinnacleMaster.GP_Oracle_Id__c));
            } else if (pinnacleMaster.RecordType.Name == 'Project Organization') {
                listOfProjectOrganization.add(new GPOptions(pinnacleMaster.Id, label));
            } else if (pinnacleMaster.RecordType.Name == 'Service Sub Category') {
                String serviceLine = pinnacleMaster.GP_Service_Line__c;

                if (!mapOfServiceLineToListOfSubCategory.containsKey(serviceLine)) {
                    mapOfServiceLineToListOfSubCategory.put(serviceLine, new List < GPOptions > ());
                }

                mapOfServiceLineToListOfSubCategory.get(serviceLine)
                    .add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
                //listOfSubCategoryOfServices.add(new GPOptions(pinnacleMaster.Id, pinnacleMaster.GP_Description__c));
            } else if (pinnacleMaster.RecordType.Name == 'Billing Entity') {
                // added by amitppt//23022018 pinnacleMaster.GP_Oracle_Id__c.
                // added by DS-02-03-19 pinnacleMaster.GP_LE_Code__c.
                listOfOperatingUnit.add(new GPOptions(pinnacleMaster.Id, label, pinnacleMaster.GP_Oracle_Id__c, String.valueOf(pinnacleMaster.GP_Is_GST_Enabled__c), pinnacleMaster.GP_LE_Code__c));
            } else if (pinnacleMaster.RecordType.Name == 'HSN Category') {
                String hsnLabel = pinnacleMaster.GP_Oracle_Id__c + ' -- ' + pinnacleMaster.GP_Long_Description__c;
                // added by amitppt//23022018 pinnacleMaster.GP_Oracle_Id__c
                // added by DS-02-03-19 pinnacleMaster.GP_Oracle_Id__c.
                listOfHSN.add(new GPOptions(pinnacleMaster.GP_Oracle_Id__c + '-' + pinnacleMaster.GP_Long_Description__c, hsnLabel, pinnacleMaster.GP_Long_Description__c, pinnacleMaster.GP_Oracle_Id__c));
            }
        }

        mapOfFieldNameToListOfOptions.put('HSL', listOfHSL);
        mapOfFieldNameToListOfOptions.put('VSL', listOfVSL);

        if (listOfSubDivision.size() > 0) {
            mapOfFieldNameToListOfOptions.put('SubDivision', listOfSubDivision);
        } else if (globalSubDivisionRecord != null) {
            mapOfFieldNameToListOfOptions.put('SubDivision', new List < GPOptions > { new GPOptions(globalSubDivisionRecord.Id, globalSubDivisionRecord.GP_Description__c) });
        }

        if (listOfSILO.size() > 0) {
            mapOfFieldNameToListOfOptions.put('Silo', listOfSILO);
        } else if (globalSiloRecord != null) {
            mapOfFieldNameToListOfOptions.put('Silo', new List < GPOptions > { new GPOptions(globalSiloRecord.Id, globalSiloRecord.GP_Description__c) });
        }

        mapOfFieldNameToListOfOptions.put('PortalMaster', listOfPortalMaster);
        mapOfFieldNameToListOfOptions.put('ProjectOrganization', listOfProjectOrganization);
        mapOfFieldNameToListOfOptions.put('listOfSubCategoryOfServices', listOfSubCategoryOfServices);
        mapOfFieldNameToListOfOptions.put('listOfOperatingUnit', listOfOperatingUnit);
        mapOfFieldNameToListOfOptions.put('listOfHSN', listOfHSN);
    }

    private static void setMapOfParentProfile() {
        if (projectData == null) {
            return;
        }

        List < GPOptions > listOfParentProjectForGroupInvoiceOptions = new List < GPOptions > ();
        List < GP_Project_Address__c > listOfProjectAddress = [SELECT Id,
            GP_Customer_Name__c,
            GP_Bill_To_Address__c,
            GP_Ship_To_Address__c
            FROM GP_Project_Address__c
            WHERE GP_Project__c =: projectData.Id
            LIMIT 1
        ];

        if (listOfProjectAddress != null && listOfProjectAddress.size() > 0) {
            for (GP_Project_Address__c parentProjectAddress: [SELECT Id,
                    GP_Customer_Name__c,
                    GP_Bill_To_Address__c,
                    GP_Ship_To_Address__c,
                    GP_Project__r.Id,
                    GP_Project__r.Name
                    FROM GP_Project_Address__c
                    WHERE GP_Customer_Name__c =: listOfProjectAddress[0].GP_Customer_Name__c
                    And GP_Bill_To_Address__c =: listOfProjectAddress[0].GP_Bill_To_Address__c
                    And GP_Ship_To_Address__c =: listOfProjectAddress[0].GP_Ship_To_Address__c
                    And GP_Project__r.GP_Operating_Unit__c =: projectData.GP_Operating_Unit__c
                    And GP_Project__r.Id !=: projectData.Id
                    order by GP_Project__r.Name desc
                ]) {
                listOfParentProjectForGroupInvoiceOptions.add(new GPOptions(parentProjectAddress.GP_Project__r.Id, parentProjectAddress.GP_Project__r.Name));
            }
        }

        mapOfFieldNameToListOfOptions.put(PARENT_PROJECT_FOR_GROUP_INVOICE_LABEL, listOfParentProjectForGroupInvoiceOptions);

    }

    public static GPAuraResponse getParentProjectInvoice(String serializedProject) {
        try {
            projectData = (GP_Project__c) JSON.deserialize(serializedProject, GP_Project__c.class);
            mapOfFieldNameToListOfOptions = new Map < String, List < GPOptions >> ();
            setMapOfParentProfile();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(mapOfFieldNameToListOfOptions.get(PARENT_PROJECT_FOR_GROUP_INVOICE_LABEL)));
    }


    public static GPAuraResponse getStageWiseFieldForProject(Id projectId) {
        List < GP_Leadership_Master__c > leadershipMaster;

        GPSelectorLeadershipMaster leadershipMasterSelector = new GPSelectorLeadershipMaster();
        Map < String, String > mapOfFieldNameToLabel = new Map < String, String > ();
        GP_Project__c project;

        try {
            project = [Select RecordType.Name, GP_Deal_Category__c,
                GP_Project_Template__r.GP_Stage_Wise_Field_Project__c
                from GP_Project__c
                where Id =: projectId
            ];
            mapOfFieldNameToLabel = GPCommon.getMapOfFieldNameToLabel('GP_Project__c');

            if (project.GP_Deal_Category__c == 'Sales SFDC' || project.GP_Deal_Category__c == 'OMS') {
                leadershipMaster = leadershipMasterSelector.getBillableRoleMasterLeadership();
            } else {
                leadershipMaster = leadershipMasterSelector.getNonBillableRoleMasterLeadership();
            }
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if (project != null &&
            project.GP_Project_Template__r != null &&
            project.GP_Project_Template__r.GP_Stage_Wise_Field_Project__c != null) {
            gen.writeObjectField('stageWiseFields', project.GP_Project_Template__r.GP_Stage_Wise_Field_Project__c);
        }

        if (project != null &&
            project.RecordType != null &&
            project.RecordType.Name != null) {
            gen.writeObjectField('RecordTypeName', project.RecordType.Name);
        }

        if (mapOfFieldNameToLabel != null) {
            gen.writeObjectField('mapOfFieldNameToLabel', mapOfFieldNameToLabel);
        }

        if (leadershipMaster != null) {
            gen.writeObjectField('leadershipMaster', leadershipMaster);
        }

        gen.writeEndObject();

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }


    public static GPAuraResponse getPickList(String serializedProjectData, String serializedDealData) {

        if (serializedDealData != null) {
            dealData = (GP_Deal__c) JSON.deserialize(serializedDealData, GP_Deal__c.class);
        }

        if (serializedProjectData != null) {
            projectData = (GP_Project__c) JSON.deserialize(serializedProjectData, GP_Project__c.class);
        }

        mapOfFieldNameToListOfOptions = new Map < String, List < GPOptions >> ();
        try {
            setMapOfFieldNameToOptions();
            setMapOflookupPicklist();
            setDealMastersToProjectRecord();
        } catch (Exception ex) {
            System.System.debug('ex.getMe >>> ' + ex.getMessage());
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (mapOfFieldNameToListOfOptions != null) {
            gen.writeObjectField('mapOfFieldNameToListOfOptions', mapOfFieldNameToListOfOptions);
        }

        if (mapOfServiceLineToListOfSubCategory != null) {
            gen.writeObjectField('mapOfServiceLineToListOfSubCategory', mapOfServiceLineToListOfSubCategory);
        }

        if (mapOfMappingForDealData != null) {
            gen.writeObjectField('mapOfMappingForDealData', mapOfMappingForDealData);
        }

        gen.writeEndObject();

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    private static void setDealMastersToProjectRecord() {
        if (dealData != null && (dealData.GP_GOL__c != '' || dealData.GP_GRM__c != '')) {
            for (GP_Employee_Master__c employee: [select id, GP_Final_OHR__c, GP_Employee_Unique_Name__c, Name from GP_Employee_Master__c
                    where(GP_Final_OHR__c =: dealData.GP_GOL__c OR GP_Final_OHR__c =: dealData.GP_GRM__c)
                    AND GP_isActive__c = true
                ]) {
                if (employee.GP_Final_OHR__c == dealData.GP_GOL__c) {
                    mapOfMappingForDealData.put('golEmployee', employee);
                }
                if (employee.GP_Final_OHR__c == dealData.GP_GRM__c) {
                    mapOfMappingForDealData.put('grmEmployee', employee);
                }
            }
        }

        if (dealData != null && dealData.GP_Mode_of_dispatch_Id__c != '') {
            for (GP_Pinnacle_Master__c pinnacleMaster: [select GP_Description__c, Name, GP_Oracle_Id__c
                    from GP_Pinnacle_Master__c where
                    GP_Oracle_Id__c =: dealData.GP_Mode_of_dispatch_Id__c and RecordType.Name = 'Mode of Dispatch'
                ]) {
                if (pinnacleMaster.GP_Oracle_Id__c == dealData.GP_Mode_of_dispatch_Id__c) {
                    mapOfMappingForDealData.put('pinnacleMaster', pinnacleMaster);
                }
            }
        }
    }

}