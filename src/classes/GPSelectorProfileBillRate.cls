public class GPSelectorProfileBillRate extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Profile_Bill_Rate__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Profile_Bill_Rate__c.sObjectType;
    }

    public List < GP_Profile_Bill_Rate__c > selectById(Set < ID > idSet) {
        return (List < GP_Profile_Bill_Rate__c > ) selectSObjectsById(idSet);
    }

    public Map < Id, GP_Profile_Bill_Rate__c > getMapOfProjectRelatedProfileBillRate(Id projectId) {
        return new Map < Id, GP_Profile_Bill_Rate__c > ([SELECT Id, GP_Bill_Rate__c, GP_Project__c, GP_Profile__c FROM GP_Profile_Bill_Rate__c
            where GP_Project__c =: projectId
        ]);
    }

    public List < GP_Profile_Bill_Rate__c > getMapOfProjectRelatedProfileBillRates(Id projectId) {
        return [SELECT Id, Name, GP_Bill_Rate__c, GP_Project__c, GP_Profile__c, GP_Project__r.GP_Bill_Rate_Type__c FROM GP_Profile_Bill_Rate__c
            where GP_Project__c =: projectId and GP_Profile__c != null
            and GP_Bill_Rate__c != null
        ];
    }

}