public class VIC_ComponentDetailsCtrl {
    /*@AuraEnabled
    public static List <Target_Component__c> getComponentDetails(String userId) {
        List<Target__c> targetList=[Select id,User__c,(Select id,vic_Achievement__c,vic_Achievement_Percent__c,vic_Already_Paid_Incentive__c,Bonus_Display__c,Bonus_Type__c from Target_Components__r) from Target__c where User__c=:userId Limit 1];
        if(!targetList.isEmpty())
            return targetList[0].Target_Components__r;
        else
            return null;
    }*/
    
    @AuraEnabled
    public static List<Target_Achievement__c> getComponentDetails(String userId){
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        Integer processingYear = Integer.ValueOf(vicInfo.VIC_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        List<Target_Achievement__c> targetAchievementList = new List<Target_Achievement__c>();
        List<Target_Component__c> targetComponentList = [select id,vic_Achievement__c,vic_Achievement_Percent__c,vic_Already_Paid_Incentive__c,Bonus_Display__c,Bonus_Type__c,(select Id, Bonus_Amount__c, VIC_Description__c from Target_Achievements__r where vic_Status__c =: 'HR - Pending')
                                                         from Target_Component__c 
                                                         where (Target__r.Is_Active__c =: True) AND (Target__r.Start_date__c =: targetStartDate) AND (Target__r.User__c =: userId) AND (Master_Plan_Component__r.vic_Component_Code__c =: 'Discretionary_Payment') Limit 1];
        for(Target_Component__c targetComponentObject : targetComponentList){
            targetAchievementList.add(targetComponentObject.Target_Achievements__r);
        }
        return targetAchievementList;
    }
}