/**
 * @author Salil.Sharma
 * @date 19/12/2017
 *
 * @group ProjectProfileBillRate
 * @group-content ../../ApexDocContent/ProjectResourceAllocation.htm
 *
 * @description Apex service class having Aura enabled methods
 * to fetch, insert, update and delete resource to be allocated under a project.
 */
public class GPCmpServiceProjectProfileBillRate {

    /**
     * @description Returns Project profile bill Rate data for a given project Id
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse json of Project Resource Allocation data
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.getProjectResourceAllocationData(<18 or 15 digit project Id>);
     */

    @AuraEnabled
    public static GPAuraResponse getProjectProfileBillRateData(Id projectId) {
        GPControllerProfileBillRate profileBillRateController = new GPControllerProfileBillRate();
        return profileBillRateController.getProfileBillRateData(projectId);

    }

    /**
     * @description Saves Profile bill rate data for the specific project
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse json of Projectprofile bill rate Data
     * 
     * @example
     * GPCmpServiceProfileBillRate.saveProjectProfileBillRate(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse saveProfileBillRate(String serializedListOfProfileBillRate) {
        List < GP_Profile_Bill_Rate__c > listOfProjectProfileBillRate = (List < GP_Profile_Bill_Rate__c > ) JSON.deserialize(serializedListOfProfileBillRate, List < GP_Profile_Bill_Rate__c > .class);
        GPControllerProfileBillRate ProfileBillRateController = new GPControllerProfileBillRate(listOfProjectProfileBillRate);
        return ProfileBillRateController.saveProjectProfileBillRate();
    }

    /**
     * @description deletes Project Profile Bill Rate data for a given project Profile Bill Rate Id
     * @param projectExpenseId Salesforce 18/15 digit Id of project Profile Bill Rate
     * 
     * @return GPAuraResponse json of Project Profile Bill rate data has been deleted
     * 
     * @example
     * GPCmpServiceProjectProfileBillRate.deleteProfileBillRate(<18 or 15 digit project expense Id>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteProfileBillRate(Id profileBillRate) {
        GPControllerProfileBillRate profileBillRateController = new GPControllerProfileBillRate();
        return profileBillRateController.deleteProfileBillRate(profileBillRate);
    }

}