public class GPSelectorPinnacleMasters extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Pinnacle_Master__c.Name,
            GP_Pinnacle_Master__c.Id,
            GP_Pinnacle_Master__c.GP_Unique_Number__c
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Pinnacle_Master__c.sObjectType;
    }

    public List<GP_Pinnacle_Master__c> selectById(Set<ID> idSet) {
        return (List<GP_Pinnacle_Master__c>) selectSObjectsById(idSet);
    }

    public GP_Pinnacle_Master__c selectById(ID masterId) {
        Set<Id> idSet = new Set<Id> {
            masterId
        };
        return (GP_Pinnacle_Master__c) selectSObjectsById(idSet).get(0);
    }

    public static List < GP_Pinnacle_Master__c > selectGlobalSettingRecord() {
        /*Id globalSettingRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        return [Select Id, CurrencyIsoCode, GP_TimeSheet_Financial_Month__c, GP_Contractor_Emp_Min_Hrs__c, GP_Contractor_Emp_Max_Hrs__c,
            GP_Full_Time_Emp_Min_Hrs__c, GP_Full_Time_Emp_Max_Hrs__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :globalSettingRecordTypeId
        ];*/
        Id globalSettingRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Global Settings');
        return [Select Id, CurrencyIsoCode, GP_TimeSheet_Financial_Month__c, GP_Contractor_Emp_Min_Hrs__c, GP_Contractor_Emp_Max_Hrs__c,
            GP_Full_Time_Emp_Min_Hrs__c, GP_Full_Time_Emp_Max_Hrs__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :globalSettingRecordTypeId];
    }
    
    public static GP_Pinnacle_Master__c selectByOperatingUnitId(Id operatingUnitId) {
        return [Select Id, CurrencyIsoCode, GP_Currency__c
            FROM GP_Pinnacle_Master__c
            WHERE Id =: operatingUnitID];
    }

    public static List < GP_Pinnacle_Master__c > selectBillingEntityRecords(set < String > setofBillingEntityID) {
        Id billingEntityRecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        return [Select Id, GP_Oracle_Id__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeID =: billingEntityRecordTypeId and
            GP_Oracle_Id__c IN: setofBillingEntityID];
    }


    public static List<GP_Pinnacle_Master__c> selectOracleTemplateId(Set<String> setOfProjectType, Set<Id> setOfOperatingUnit) {
        Id oracleTemplateTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Oracle Template').getRecordTypeId();
        
        return [SELECT Id, GP_Oracle_Id__c, GP_Entity_Id__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :oracleTemplateTypeId
            And GP_Project_Type__c in : setOfProjectType
            AND GP_Entity_Id__c in : setOfOperatingUnit];
    }
    public static List<GP_Pinnacle_Master__c> selectOracleTemplateId(Set<String> setOfProjectType, Set<String> setOfOperatingUnit) {
        Id oracleTemplateTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Oracle Template').getRecordTypeId();
        
        return [SELECT Id, GP_Oracle_Id__c, GP_Entity_Id__c, GP_Project_Type__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :oracleTemplateTypeId
            And GP_Project_Type__c in : setOfProjectType
            AND GP_Entity_Id__c in : setOfOperatingUnit];
    }
    public static List<GP_Pinnacle_Master__c> selectOracleTemplateIdAndMODOracleID(Set<String> setOfProjectType, Set<String> setOfOperatingUnit, Set<String> setOfMODTypes , 
    					Set<String> setOfProjectRecordType ) {
        Id oracleTemplateTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Oracle Template').getRecordTypeId();
        Id MODTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Mode of Dispatch').getRecordTypeId();

        return [SELECT Id, GP_Oracle_Id__c, GP_Project_Record_Type__c, GP_Entity_Id__c,GP_Project_Type__c, RecordType.Name, GP_Description__c,GP_Business_Type__c ,GP_Project_Category_Customer_L2__c 
            FROM GP_Pinnacle_Master__c
            WHERE (RecordTypeId = :oracleTemplateTypeId 
            And GP_Project_Type__c in : setOfProjectType
            AND GP_Entity_Id__c in : setOfOperatingUnit
            AND GP_Status__c = 'Active'
            AND GP_Project_Record_Type__c in: setOfProjectRecordType )
            OR (RecordType.Id = :MODTypeId
            And GP_Description__c in : setOfMODTypes
                And GP_Status__c = 'Active')];
    }
    public static Map<Id,GP_Pinnacle_Master__c> selectTrainingMasters() {
        Id trainingMasterRecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        
        return new Map<Id,GP_Pinnacle_Master__c>([SELECT Id, GP_Training_Validity__c,Name,GP_Description__c,GP_Oracle_Id__c,GP_Training_Category__c,GP_Emp_SDO_to_Validate__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :trainingMasterRecordTypeId
           // And GP_Validate_Resource_Allocation__c = true//to be deleted
            AND GP_Status__c = 'Active'
        ]);
    }
    
    public static list<GP_Pinnacle_Master__c> selectTrainingMastersforCourseId(set<String> setCourseId) {
        Id trainingMasterRecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        
        return ([SELECT Id, GP_Training_Validity__c,Name,GP_Description__c,GP_Oracle_Id__c,GP_Training_Category__c,GP_Emp_SDO_to_Validate__c
            FROM GP_Pinnacle_Master__c
            WHERE RecordTypeId = :trainingMasterRecordTypeId
           // And GP_Validate_Resource_Allocation__c = true//to be deleted
            AND GP_Status__c = 'Active' AND GP_Oracle_Id__c in : setCourseId
        ]);
    }
    
    public static List<GP_Pinnacle_Master__c> selectMODOracleId(Set<String> setOfMODTypes) {
        Id MODTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Mode of Dispatch').getRecordTypeId();
        
        return [SELECT Id, 
                       Name, 
                       GP_Status__c, 
                       GP_Oracle_Id__c,
                       GP_Project_Type__c
                FROM GP_Pinnacle_Master__c
                WHERE RecordTypeId = :MODTypeId
                And GP_Project_Type__c in : setOfMODTypes
                And GP_Status__c = 'Active'];
    }
    // Mass Update Change : 05-12-19.
    public static List<GP_Pinnacle_Master__c> selectRecordsFromOracleIdSet(Set<String> setOfOracleIds) {
        
        Id hslMasterRecordTypeId            = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        Id siloRecordTypeId                 = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        Id portalMasterRecordTypeId         = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Portal Master');
        
        return [SELECT Id, GP_Oracle_Id__c
                FROM GP_Pinnacle_Master__c
                WHERE GP_Oracle_Id__c in : setOfOracleIds 
                AND (RecordTypeId = : hslMasterRecordTypeId
                     OR RecordTypeId = : siloRecordTypeId
                     OR RecordTypeId = : portalMasterRecordTypeId)
               ];
    }
    
    public Map<Id,GP_Pinnacle_Master__c> selectByRecordId(Set<Id> setOfIds){
        return new Map<Id,GP_Pinnacle_Master__c>([Select Id, CurrencyIsoCode,GP_Controller_User__c
                                                  FROM GP_Pinnacle_Master__c
                                                  WHERE Id in :setOfIds]);
    }
	// HSN Changes: Added GP_LE_Code__c in SOQL.
    public static List<GP_Pinnacle_Master__c> getAllPinnacleMasterData() {

        Id hsnRecordTypeId                  = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSN Category');
        Id siloRecordTypeId                 = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        Id taxCodeRecordTypeId              = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');
        Id hslMasterRecordTypeId            = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        Id vslMasterRecordTypeId            = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'VSL Master');
        Id subDivisionRecordTypeId          = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        Id portalMasterRecordTypeId         = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Portal Master');
        Id billingEntityRecordTypeId        = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        Id serviceSubCategoryRecordTypeId   = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Service Sub Category');
        Id projectOrganizationRecordTypeId  = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Project Organization');

        return [SELECT ID, Name, 
                RecordType.Name, 
                GP_Entity_Id__c, 
                GP_Oracle_Id__c,
                GP_Description__c,
                GP_Service_Line__c,
                GP_Is_GST_Enabled__c,
                GP_Long_Description__c,
                GP_Parent_Customer_L2__c,
                GP_LE_Code__c
                FROM GP_Pinnacle_Master__c
                WHERE(RecordTypeId = :hslMasterRecordTypeId
                      OR RecordTypeId = :vslMasterRecordTypeId
                      OR RecordTypeId = :subDivisionRecordTypeId
                      OR RecordTypeId = :siloRecordTypeId
                      OR RecordTypeId = :portalMasterRecordTypeId
                      OR RecordTypeId = :projectOrganizationRecordTypeId
                      OR RecordTypeId = :serviceSubCategoryRecordTypeId   
                      OR RecordTypeId = :billingEntityRecordTypeId
                      OR RecordTypeId = :hsnRecordTypeId)
                and GP_Status__c = 'Active'
                Order By Name desc];
    }
	// HSN Changes: Added GP_LE_Code__c on SOQL.
    public static List<GP_Pinnacle_Master__c> getAllPinnacleMasterData(GP_Project__c project) {

        Id hsnRecordTypeId                  = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSN Category');
        Id siloRecordTypeId                 = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        Id taxCodeRecordTypeId              = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');
        Id hslMasterRecordTypeId            = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        Id vslMasterRecordTypeId            = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'VSL Master');
        Id subDivisionRecordTypeId          = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        Id portalMasterRecordTypeId         = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Portal Master');
        Id billingEntityRecordTypeId        = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        Id serviceSubCategoryRecordTypeId   = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Service Sub Category');
        Id projectOrganizationRecordTypeId  = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Project Organization');

        return [SELECT ID, Name, 
                RecordType.Name, 
                GP_Entity_Id__c, 
                GP_Oracle_Id__c,
                GP_Description__c, 
                GP_Service_Line__c,
                GP_Is_GST_Enabled__c,
                GP_Long_Description__c,
                GP_Parent_Customer_L2__c,
                GP_LE_Code__c
                FROM GP_Pinnacle_Master__c
                WHERE(RecordTypeId = :hslMasterRecordTypeId
                    OR RecordTypeId = :vslMasterRecordTypeId
                    OR RecordTypeId = :subDivisionRecordTypeId
                    OR RecordTypeId = :siloRecordTypeId
                    OR RecordTypeId = :portalMasterRecordTypeId
                    OR RecordTypeId = :projectOrganizationRecordTypeId
                    OR RecordTypeId = :serviceSubCategoryRecordTypeId
                    OR RecordTypeId = :billingEntityRecordTypeId
                    OR RecordTypeId = :hsnRecordTypeId)
                and GP_Status__c = 'Active'
                Order By Name desc];
    }

    public static List<GP_Pinnacle_Master__c> getTaxCode(GP_Project__c project) {
        Id taxCodeRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');

        return [SELECT ID, Name, RecordType.Name, GP_Entity_Id__c, GP_Oracle_Id__c
                FROM GP_Pinnacle_Master__c
                WHERE RecordTypeId = :taxCodeRecordTypeId
                and GP_Status__c = 'Active'
                and GP_Entity_Id__c = :project.GP_Operating_Unit__r.GP_Oracle_Id__c
                Order By Name desc];
    }

    public static GP_Pinnacle_Master__c selectByRecordTypeName(String recordTypeName) {
        Id recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', recordTypeName);
        return [SELECT Id, GP_Delivery_Sub_Delivery_Mapping__c
                 FROM GP_Pinnacle_Master__c
                 where RecordTypeId = :recordTypeId
                 LIMIT 1];
    }

    public static List<GP_Pinnacle_Master__c> getTaxCodeRecord(String operatingUnitOracleID) {
        Id taxCodeRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');
        return [Select Name 
                from GP_Pinnacle_Master__c 
                where GP_Entity_Id__c = :operatingUnitOracleID
                And RecordTypeId = :taxCodeRecordTypeId];
    }

    public static GP_Pinnacle_Master__c getGenpactInternationalIncOperatingUnit() {
        Id billingEntityRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');

        return  [SELECT ID, Name, 
                RecordType.Name, 
                GP_Entity_Id__c, 
                GP_Oracle_Id__c,
                GP_Description__c, 
                GP_Is_GST_Enabled__c,
                GP_Long_Description__c,
                GP_Parent_Customer_L2__c 
                FROM GP_Pinnacle_Master__c
                WHERE RecordTypeId = :billingEntityRecordTypeId
                and GP_Status__c = 'Active'
                AND Name = 'Genpact International Inc.'
                LIMIT 1];
    }

    public static List<GP_Pinnacle_Master__c> getTaxMasterCodeRecord(String taxCode) {
        Id taxCodeRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');
        return [Select Name ,Id
                from GP_Pinnacle_Master__c 
                where GP_Tax_Code__c = :taxCode
                And RecordTypeId = :taxCodeRecordTypeId];
    }
    // HSN Changes
    public static List<GP_Pinnacle_Master__c> getGSTEnabledOUs() {
        Id billingEntityRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        
        return [SELECT Id, GP_LE_Code__c FROM GP_Pinnacle_Master__c 
                WHERE GP_Is_GST_Enabled__c = true AND GP_Status__c = 'Active' 
                AND RecordTypeId =: billingEntityRecordTypeId];
    }
}