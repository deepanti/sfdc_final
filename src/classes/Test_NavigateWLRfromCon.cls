@istest//(SeeAllData=true)
Public class Test_NavigateWLRfromCon
{   
    static testMethod void method1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        //RedirectToWLRFromCon.isInSalesforce1 = true;
        
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        
		Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
		
		
		
		
		
		
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='',Contact1__c= oContact.Id);
        insert opp;
        
        
        
        
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');  //, 	Number_of_Attachments__c=0
        insert oContract;
        
        Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
		system.debug('=====attach====test===='+attach);
        insert attach;
        
		test.startTest();
        PageReference tpageRef = Page.NavigateToWLR_FromCon;
        
        ApexPages.StandardController std = new ApexPages.StandardController(oContract);
        RedirectToWLRFromCon controller = new RedirectToWLRFromCon(std);
        Test.setCurrentPage(tpageRef);
    ApexPages.currentPage().getParameters().put('id', oContract.Id);
        controller.Redirect();
        
        opportunity o =[select id,W_L_D__c from opportunity where id= :opp.id];
        o.W_L_D__c='Signed';
        
        update o;
        
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :oContract.id];
        //System.assertEquals(processInstances.size(),1);
        
        test.stopTest();
        
        
        
    }
    
	static testMethod void method2()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        //RedirectToWLRFromCon.isInSalesforce1 = true;
        
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
                
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='', Contact1__c= oContact.Id);
        insert opp;
        
   
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');//, 	Number_of_Attachments__c=1
        insert oContract;
        
        
        //Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
        //insert attach;
         test.startTest();
        PageReference tpageRef = Page.NavigateToWLR_FromCon;
        
        ApexPages.StandardController std = new ApexPages.StandardController(oContract);
        RedirectToWLRFromCon controller = new RedirectToWLRFromCon(std);
        Test.setCurrentPage(tpageRef);
    ApexPages.currentPage().getParameters().put('id', oContract.Id);
        controller.Redirect();
        
        opportunity o =[select id,W_L_D__c from opportunity where id= :opp.id];
        o.W_L_D__c='Signed';
        
        update o;
        
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :oContract.id];
        //System.assertEquals(processInstances.size(),1);
        
        test.stopTest();
        
        
        
    }
    
           static testMethod void method3()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        //RedirectToWLRFromCon.isInSalesforce1 = true;
    
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        	
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,survey_initiated__c=true, W_L_D__c='Signed', Contact1__c = oContact.Id, Survey_Name__c='WLR');
        insert opp;
                
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');
        insert oContract;
        
        Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
        insert attach;
         test.startTest();
        PageReference tpageRef = Page.NavigateToWLR_FromCon;
        
        ApexPages.StandardController std = new ApexPages.StandardController(oContract);
        RedirectToWLRFromCon controller = new RedirectToWLRFromCon(std);
        Test.setCurrentPage(tpageRef);
    ApexPages.currentPage().getParameters().put('id', oContract.Id);
        controller.Redirect();
        
        opportunity o =[select id,W_L_D__c from opportunity where id= :opp.id];
        o.W_L_D__c='Signed';
        
        update o;
        
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :oContract.id];
        //System.assertEquals(processInstances.size(),1);
        
        test.stopTest();
        
        
        
    } 
    
           static testMethod void method4()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        //RedirectToWLRFromCon.isInSalesforce1 = true;
       
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        
		
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,survey_initiated__c=true,Survey_Name__c='WLR',W_L_D__c='Signed',Contact1__c = oContact.Id);
        insert opp;
        
        
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');
        insert oContract;
        
        Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
        insert attach;
         test.startTest();
        PageReference tpageRef = Page.NavigateToWLR_FromCon;
        
        ApexPages.StandardController std = new ApexPages.StandardController(oContract);
        RedirectToWLRFromCon controller = new RedirectToWLRFromCon(std);
        Test.setCurrentPage(tpageRef);
    ApexPages.currentPage().getParameters().put('id', oContract.Id);
        
        
        opportunity o =[select id,W_L_D__c from opportunity where id= :opp.id];
        o.W_L_D__c='Signed';
        
        update o;
        controller.Redirect();
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :oContract.id];
        //System.assertEquals(processInstances.size(),1);
        
        test.stopTest();
        
        
        
    } 
               static testMethod void method5()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        //RedirectToWLRFromCon.isInSalesforce1 = true;
     
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        		
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,survey_initiated__c=true,Survey_Name__c='WLR',Contact1__c = oContact.Id );
        insert opp;
        
        
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');
        insert oContract;
        
        Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
        insert attach;
         test.startTest();
        PageReference tpageRef = Page.NavigateToWLR_FromCon;
        
        ApexPages.StandardController std = new ApexPages.StandardController(oContract);
        RedirectToWLRFromCon controller = new RedirectToWLRFromCon(std);
        Test.setCurrentPage(tpageRef);
    ApexPages.currentPage().getParameters().put('id', oContract.Id);
        
        
        opportunity o =[select id,W_L_D__c from opportunity where id= :opp.id];
        o.W_L_D__c='Lost';
        
        update o;
        controller.Redirect();
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        List<ProcessInstance> processInstances = [select Id, Status from ProcessInstance where TargetObjectId = :oContract.id];
        //System.assertEquals(processInstances.size(),1);
        
        test.stopTest();
        
    } 
 

 
 }