@isTest
public class GENcUpdateExchRateFieldOnOppProductTest 
{
    static testMethod void test() 
    {
       Database.QueryLocator QL;
        Database.BatchableContext BC;
        List<OpportunityProduct__c> AcctList = new List<OpportunityProduct__c>();

        GENcUpdateExchangeRateFieldOnOppProduct AR = new GENcUpdateExchangeRateFieldOnOppProduct('2014-07-01T08:49:47.000Z','2017-07-01T08:49:47.000Z',100);


          QL = AR.start(bc);
          Database.QueryLocatorIterator QIT =  QL.iterator();
        while (QIT.hasNext())
        {
            OpportunityProduct__c Acc = (OpportunityProduct__c)QIT.next();            
            System.debug(Acc);
         }        

         AR.execute(BC, AcctList);
         AR.finish(BC);        

    
        }
        }