/**
 * @author Pankaj.Adhikari
 * @date 20/03/2018
 *
 * @group OpportunityProject
 * @group-content ../../ApexDocContent/EmployeeTraining.html
 *
 * @description Domian class for Employee Training,
 * 
 */
public with sharing class GPDomainEmployeeTraining extends fflib_SObjectDomain {
    private fflib_SObjectUnitOfWork uow;

    public GPDomainEmployeeTraining(List < GP_Employee_Training__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainEmployeeTraining(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Employee_Training__c.SObjectType
    };

    public override void onBeforeInsert() {
        setExternalValitTo();
    }

    public override void onAfterInsert() {
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        deleteOldTrainings();
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }

    /**
     * @description Method to update valid to , Valid from and external valid to dates on records
     * @param 
     * 
     * @example
     * new setExternalValitTo();
     */
    private void setExternalValitTo() {
        set < String > setCourseId = new set < String > ();
        for (GP_Employee_Training__c objEmpTraining: (list < GP_Employee_Training__c > ) records) {

            if (!String.isBlank(objEmpTraining.GP_Course_Id__c)) {
                setCourseId.add(objEmpTraining.GP_Course_Id__c);
            }
        }
        if (setCourseId.size() > 0) {
            list < GP_Pinnacle_Master__c > lstPinnacleMaster = GPSelectorPinnacleMasters.selectTrainingMastersforCourseId(setCourseId);
            map < String, GP_Pinnacle_Master__c > mpCourseidToCourse = new map < String, GP_Pinnacle_Master__c > ();
            for (GP_Pinnacle_Master__c objCourse: lstPinnacleMaster) {
                mpCourseidToCourse.put(objCourse.GP_Oracle_Id__c, objCourse);
            }

            for (GP_Employee_Training__c objEmpTraining: (list < GP_Employee_Training__c > ) records) {
                if (!String.isBlank(objEmpTraining.GP_Course_Id__c) && mpCourseidToCourse.get(objEmpTraining.GP_Course_Id__c) != null) {

                    if (!String.isblank(mpCourseidToCourse.get(objEmpTraining.GP_Course_Id__c).GP_Training_Validity__c)) {
                        integer intTraningvalidity = Integer.Valueof(mpCourseidToCourse.get(objEmpTraining.GP_Course_Id__c).GP_Training_Validity__c);
                        objEmpTraining.GP_Valid_from__c = objEmpTraining.GP_Status_Change_Date__c.addMonths(-intTraningvalidity);
                        objEmpTraining.GP_Valid_to__c = objEmpTraining.GP_Status_Change_Date__c.addMonths(intTraningvalidity);
                        objEmpTraining.GP_External_Valid_to__c = objEmpTraining.GP_Valid_to__c.addDays(
                            Integer.valueof(System.label.GP_Emp_Training_External_Valid_to));

                        objEmpTraining.GP_Unique_Training_Id__c =
                            objEmpTraining.GP_Course_Id__c + '-' + objEmpTraining.GP_Employee_Number__c;
                    }
                }
            }
        }
    }

    private void deleteOldTrainings() {
        set < String > setCourseId = new set < String > ();
        set < String > setNewTrainingID = new set < String > ();
        set < String > setEmployeeOHR = new set < String > ();
        Set < String > setUniqueTraining = new set < String > ();
        for (GP_Employee_Training__c objEmpTraining: (list < GP_Employee_Training__c > ) records) {

            if (!String.isBlank(objEmpTraining.GP_Unique_Training_Id__c)) {
                setNewTrainingID.add(objEmpTraining.id);
                setUniqueTraining.add(objEmpTraining.GP_Unique_Training_Id__c);
            }

        }
        if (setUniqueTraining.size() > 0) {
            list < GP_Employee_Training__c > lstEmpOldTraining = GPSelectorEmployeeTraining.selectOldEmpTrainingRecords(setUniqueTraining, setNewTrainingID);
            delete lstEmpOldTraining;
        }

    }
}