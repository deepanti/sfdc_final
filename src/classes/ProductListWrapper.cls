public class ProductListWrapper {
	  @AuraEnabled
    public Integer page { get;set; }

    @AuraEnabled
    public Integer total { get;set; }

    @AuraEnabled
    public List<Product2> productList { get;set; }
    
    @AuraEnabled
    public List<opportunityLineItem> oliList { get;set; }
    
    //anjali   
    @AuraEnabled
    public List<OLIWrapper> oliWrapperList { get;set; }
    
    public class OLIWrapper {
        @AuraEnabled
        public OpportunityLineItem oli {get;set;}
        
        @AuraEnabled
        public Boolean isEdited {get; set;}
    }
}