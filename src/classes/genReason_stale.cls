Public class genReason_stale
{     

    public String L1obj{get;set;}
    public List<selectOption> l1_lst{get;set;}
    public Opportunity oppty {get; set;}
    public String conid {get; set;}
    Public String WLDvar{get; set;}
    
            Public boolean Checkboxes_flag=false;
            Public boolean Competitor_flag=false;
            Public boolean Other_Comp_flag=false;
            Public boolean Other_Winner_flag=false;
            Public boolean Winner_flag=false;
            Public boolean survey_flag=false;
            Public boolean survey_recipient_flag=false;
            
    public string Error_message='';
    
    public List<selectOption> Named_winner_list{get;set;}
    
    
    
    public genReason_stale(ApexPages.StandardController controller) {
        
        String Checkbox_query = 'Partner_ally_a_guide_or_an_influence__c, Senior_leadership_connect__c, Engagement_with_Advisor__c, Detractor_managed_well__c, Understanding_of_the_network_of_influenc__c, Alignment_to_business_priorities__c, Alignment_to_personal_agendas__c, Political_coaching_influence_understand__c, Client_involved_in_value_discovery_with__c, Quality_of_pursuit_team__c, Responsiveness_in_client_interactions__c, Quality_of_customer_meetings_site_visit__c, Understanding_of_the_client_buying_roles__c, Understanding_of_the_client_buying_crite__c, Understanding_of_the_client_decision_fac__c, Lean_digital__c, Operational_excellence_Lean_Six_Sigma__c, Quality_of_resources_aligned__c, Availability_of_the_resources_aligned__c, Subject_matter_expertise_for_required_sc__c, Availability_and_quality_of_relevant_cli__c, NPS_on_existing_services_scope__c, ROI_realization_in_previous_engagements__c, On_time_On_budget_delivery__c, Clearly_defined_Win_Theme_aligned_with_b__c, Understanding_of_the_key_competitors_inc__c, Counter_tactics_to_leverage_our_strength__c, Competitive_intel_leveraged_for_pricing__c, Value_proposition_co_developed_with_the__c, Key_differentiators_identified_and_valid__c, Value_proposition_aligned_with_business__c, Projected_financial_business_impact_ROI__c, Overall_price__c, Financial_innovation_rebadging_inves__c, Total_cost_of_ownership__c, Terms_and_Conditions__c, Location_staffing_approach__c, Partnerships_if_any__c, Scale_or_complexity_of_work__c, Capability_of_delivery_team__c, Bundled_offering_with_Consulting_Analyt__c, Bundled_offering_with_only_Analytics_and__c, Digital_Offering__c, Lean_Six_Sigma_methodology_with_SEP__c, Compatibility_of_the_solution_offering_t__c, Proposed_delivery_location_model__c, Platform_availability_scalability__c, Transition_approach__c, Transformation_roadmap__c, Not_in_our_focus_geo__c, Not_our_focus_service_line__c, Not_our_focus_vertical__c, One_or_more_towers_not_in_our_focus_area__c, No_potential_for_downstream_revenue__c, Not_possible_to_meet_desired_price_point__c, No_unique_value_proposition_that_G_can_o__c, Risk_in_engaging_financially_with_the_cl__c, Low_probability_of_winning_due_to_incumb__c, Low_probability_of_winning_due_to_strong__c, No_weak_executive_relationships_conn__c, No_weak_advisor_relationship__c, Not_a_strategic_priority_for_the_client__c, Client_did_not_have_the_resources_money__c, No_sponsorship_from_client_side__c, Client_not_serious_about_moving_forward__c, Duplicate_Deal_ID__c, Price_was_more_than_the_allocated_budget__c, Budget_cut_during_the_evaluation_process__c, No_clear_benefit_in_making_the_change__c, Change_in_company_strategy__c, Change_in_management__c, No_vendor_met_the_qualifying_criteria__c, Needed_to_reassess_the_scope_of_work__c ';
        
        String Closure = 'Deal_Type__c,Competitor__c,Winner__c,Other_Competitor__c,Other_Winner__c,Send_Client_Survey__c,Contact__c,Please_Select_Client_Survey_recipient_2__c,Please_Select_Client_Survey_recipient_3__c ';
        
        String ID;
         
        If (ApexPages.currentPage().getParameters().get('oppid') != NULL) 
        {
            ID = ApexPages.currentPage().getParameters().get('oppid');
        }
        Else
        {
            ID = controller.getRecord().Id;
        }
        
        If (ApexPages.currentPage().getParameters().get('conid') != NULL) 
        {
            conid = ApexPages.currentPage().getParameters().get('conid');
        }
        
        String query_final = 'select id,name,ownerid,owner.name,Opportunity_ID__c,TCV1__c,Annuity_Project__c,Survey_Initiated__c,Survey_Name__c,W_L_D__c,Bypass_Validations__c,'+ Checkbox_query +','+ Closure +  'from opportunity where id = \'' +ID+ '\'';
    
        //oppty = [select id,name,ownerid,Pursuit_Execution_Client_involved_in_v__c,Relationship_Alignment_Partner_Ally__c,Relationship_Alignment_Engagement_with__c,TCV1__c,Annuity_Project__c,Survey_Initiated__c,Survey_Name__c,W_L_D__c,Dropped_By__c,Budget_Cut__c   from opportunity where id =:controller.getRecord().Id];
    oppty = database.query(query_final );
    
    //Reset();
    
    dynamic_Winner();
    
    }


    
    
     public pageReference saveitem()
     {
        try{ 
                if(Validate() && Validate_Closure())     
                { 
                         decimal tcv=1000000; 
                         If(((oppty.Annuity_Project__c=='Annuity' && oppty.TCV1__c>=tcv) || (oppty.Annuity_Project__c=='Project' && oppty.TCV1__c>=500000)) && oppty.Survey_Initiated__c==false)
                         {    oppty.Survey_Initiated__c=true;
                              oppty.Bypass_Validations__c=true;
                              oppty.Survey_Name__c='wlr';
                              oppty.W_L_D__c=WLDvar;
                              Update oppty;
                              
                              /*IF(conid!=Null)
                            {   
                                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                                req.setComments('Submitted for approval. Please approve.');
                                req.setObjectId(conid);
                                // submit the approval request for processing
                                Approval.ProcessResult result = Approval.process(req);
                            }*/
                              
                              if (Userinfo.getUiThemeDisplayed().equals('Theme3'))
                              {
                                      PageReference pageRef = new PageReference(System.label.Getfeedback_link+'?OpportunityOwner='+oppty.owner.name+'&OpportunityID='+oppty.id+'&UserDevice=Desktop'+'&OppId='+oppty.Opportunity_ID__c);
                                      pageRef.setRedirect(true);
                                      return pageRef;
                              }
                              
                              else if (Userinfo.getUiThemeDisplayed().equals('Theme4t'))
                              {
                                      PageReference pageRef = new PageReference(System.label.Getfeedback_link+'?OpportunityOwner='+oppty.owner.name+'&OpportunityID='+oppty.id+'&UserDevice=Mobile'+'&OppId='+oppty.Opportunity_ID__c);
                                      pageRef.setRedirect(true);
                                      return pageRef;
                              
                              }
                              else if (Userinfo.getUiThemeDisplayed().equals('Theme4d'))
                              {
                                      PageReference pageRef = new PageReference(System.label.Getfeedback_link+'?OpportunityOwner='+oppty.owner.name+'&OpportunityID='+oppty.id+'&UserDevice=Desktop_Lightning'+'&OppId='+oppty.Opportunity_ID__c);
                                      pageRef.setRedirect(true);
                                      return pageRef;
                              
                              }
                              
                              else
                                      Return null;
                              
                         }
                         
                         Else
                         {    
                                            
                                            oppty.Survey_Name__c='wlr';
                                            oppty.Bypass_Validations__c=true;
                                            oppty.W_L_D__c=WLDvar;
                                            Update oppty;
                                            
                                            IF(conid!=Null && WLDvar=='Signed')
                                            {   
                                                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                                                //req.setComments('Submitted for approval. Please approve.');
                                                req.setObjectId(conid);
                                                // submit the approval request for processing
                                                Approval.ProcessResult result = Approval.process(req);
                                            }
                                            
                                             if (Userinfo.getUiThemeDisplayed().equals('Theme3'))
                                                {
                                                    PageReference pageRef = new PageReference('/' + oppty.id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                }
                                             else if (Userinfo.getUiThemeDisplayed().equals('Theme4t'))
                                                {
                                                    PageReference pageRef = new PageReference('/apex/NavigateToDetail_mobile?oppid=' + oppty.id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                
                                                }
                                             else if(Userinfo.getUiThemeDisplayed().equals('Theme4d'))
                                                {
                                                    PageReference pageRef = new PageReference('/apex/NavigateToDetail_mobile?oppid=' + oppty.id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef;
                                                } 
                                              else
                                                    Return null;
                                              
                                                
                         
                         }
                 }
                 
                 
                 else
                 {       
                         Error_message=errorMessage();  
                         ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, Error_message));
                         return null;
                 
                 }
                 
                 
         }
     
     Catch(Exception e)
     {    
         if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please fill all the required information in closure section.'));
         else 
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
             
             return null;
     
     }
     
     
     
     }

     public void Reset()
     {
                oppty.Alignment_to_personal_agendas__c=False;
                oppty.Alignment_to_business_priorities__c=False;
                oppty.Availability_and_quality_of_relevant_cli__c=False;
                oppty.Availability_of_the_resources_aligned__c=False;
                oppty.Budget_cut_during_the_evaluation_process__c=False;
                oppty.Bundled_offering_with_Consulting_Analyt__c=False;
                oppty.Bundled_offering_with_only_Analytics_and__c=False;
                oppty.Capability_of_delivery_team__c=False;
                oppty.Change_in_company_strategy__c=False;
                oppty.Change_in_management__c=False;
                oppty.Clearly_defined_Win_Theme_aligned_with_b__c=False;
                oppty.Client_did_not_have_the_resources_money__c=False;
                oppty.Client_involved_in_value_discovery_with__c=False;
                oppty.Client_not_serious_about_moving_forward__c=False;
                oppty.Financial_innovation_rebadging_inves__c=False;
                oppty.Compatibility_of_the_solution_offering_t__c=False;
                oppty.Competitive_intel_leveraged_for_pricing__c=False;
                oppty.Counter_tactics_to_leverage_our_strength__c=False;
                oppty.Detractor_managed_well__c=False;
                oppty.Digital_Offering__c=False;
                oppty.Duplicate_Deal_ID__c=False;
                oppty.Engagement_with_Advisor__c=False;
                oppty.Key_differentiators_identified_and_valid__c=False;
                oppty.Lean_digital__c=False;
                oppty.Lean_Six_Sigma_methodology_with_SEP__c=False;
                oppty.Location_staffing_approach__c=False;
                oppty.Low_probability_of_winning_due_to_incumb__c=False;
                oppty.Low_probability_of_winning_due_to_strong__c=False;
                oppty.Needed_to_reassess_the_scope_of_work__c=False;
                oppty.No_weak_advisor_relationship__c=False;
                oppty.No_weak_executive_relationships_conn__c=False;
                oppty.No_clear_benefit_in_making_the_change__c=False;
                oppty.No_potential_for_downstream_revenue__c=False;
                oppty.No_sponsorship_from_client_side__c=False;
                oppty.Not_a_strategic_priority_for_the_client__c=False;
                oppty.Not_in_our_focus_geo__c=False;
                oppty.Not_our_focus_service_line__c=False;
                oppty.Not_our_focus_vertical__c=False;
                oppty.Not_possible_to_meet_desired_price_point__c=False;
                oppty.No_unique_value_proposition_that_G_can_o__c=False;
                oppty.No_vendor_met_the_qualifying_criteria__c=False;
                oppty.NPS_on_existing_services_scope__c=False;
                oppty.One_or_more_towers_not_in_our_focus_area__c=False;
                oppty.On_time_On_budget_delivery__c=False;
                oppty.Operational_excellence_Lean_Six_Sigma__c=False;
                oppty.Partner_ally_a_guide_or_an_influence__c=False;
                oppty.Partnerships_if_any__c=False;
                oppty.Platform_availability_scalability__c=False;
                oppty.Political_coaching_influence_understand__c=False;
                oppty.Price_was_more_than_the_allocated_budget__c=False;
                oppty.Overall_price__c=False;
                oppty.Projected_financial_business_impact_ROI__c=False;
                oppty.Proposed_delivery_location_model__c=False;
                oppty.Quality_of_customer_meetings_site_visit__c=False;
                oppty.Quality_of_pursuit_team__c=False;
                oppty.Quality_of_resources_aligned__c=False;
                oppty.Terms_and_Conditions__c=False;
                oppty.Responsiveness_in_client_interactions__c=False;
                oppty.Risk_in_engaging_financially_with_the_cl__c=False;
                oppty.ROI_realization_in_previous_engagements__c=False;
                oppty.Scale_or_complexity_of_work__c=False;
                oppty.Senior_leadership_connect__c=False;
                oppty.Subject_matter_expertise_for_required_sc__c=False;
                oppty.Total_cost_of_ownership__c=False;
                oppty.Transformation_roadmap__c=False;
                oppty.Transition_approach__c=False;
                oppty.Understanding_of_the_client_buying_crite__c=False;
                oppty.Understanding_of_the_client_buying_roles__c=False;
                oppty.Understanding_of_the_client_decision_fac__c=False;
                oppty.Understanding_of_the_key_competitors_inc__c=False;
                oppty.Understanding_of_the_network_of_influenc__c=False;
                oppty.Value_proposition_aligned_with_business__c=False;
                oppty.Value_proposition_co_developed_with_the__c=False;
                //WLDvar='';
                
                oppty.Competitor__c='';
                oppty.Other_Competitor__c='';
                oppty.Winner__c='';
                oppty.Send_Client_Survey__c='';
                oppty.Other_Winner__c='';
                oppty.Contact__c=Null;
                oppty.Please_Select_Client_Survey_recipient_2__c=Null;
                oppty.Please_Select_Client_Survey_recipient_3__c=Null;
            
     
     }
     
     
     
Public Boolean Validate()
{
                    if(
            (oppty.Partner_ally_a_guide_or_an_influence__c== True || 
            oppty.Senior_leadership_connect__c== True || 
            oppty.Engagement_with_Advisor__c== True || 
            oppty.Detractor_managed_well__c== True || 
            oppty.Understanding_of_the_network_of_influenc__c== True || 
            oppty.Alignment_to_business_priorities__c== True || 
            oppty.Alignment_to_personal_agendas__c== True || 
            oppty.Political_coaching_influence_understand__c== True || 
            oppty.Client_involved_in_value_discovery_with__c== True || 
            oppty.Quality_of_pursuit_team__c== True || 
            oppty.Responsiveness_in_client_interactions__c== True || 
            oppty.Quality_of_customer_meetings_site_visit__c== True || 
            oppty.Understanding_of_the_client_buying_crite__c== True || 
            oppty.Understanding_of_the_client_buying_roles__c== True || 
            oppty.Understanding_of_the_client_decision_fac__c== True || 
            oppty.Lean_digital__c== True || 
            oppty.Operational_excellence_Lean_Six_Sigma__c== True || 
            oppty.Quality_of_resources_aligned__c== True || 
            oppty.Availability_of_the_resources_aligned__c== True || 
            oppty.Subject_matter_expertise_for_required_sc__c== True || 
            oppty.Availability_and_quality_of_relevant_cli__c== True || 
            oppty.NPS_on_existing_services_scope__c== True || 
            oppty.ROI_realization_in_previous_engagements__c== True || 
            oppty.On_time_On_budget_delivery__c== True || 
            oppty.Clearly_defined_Win_Theme_aligned_with_b__c== True || 
            oppty.Understanding_of_the_key_competitors_inc__c== True || 
            oppty.Counter_tactics_to_leverage_our_strength__c== True || 
            oppty.Competitive_intel_leveraged_for_pricing__c== True || 
            oppty.Value_proposition_co_developed_with_the__c== True || 
            oppty.Key_differentiators_identified_and_valid__c== True || 
            oppty.Value_proposition_aligned_with_business__c== True || 
            oppty.Projected_financial_business_impact_ROI__c== True || 
            oppty.Financial_innovation_rebadging_inves__c== True || 
            oppty.Overall_price__c== True || 
            oppty.Total_cost_of_ownership__c== True || 
            oppty.Terms_and_Conditions__c== True || 
            oppty.Location_staffing_approach__c== True || 
            oppty.Partnerships_if_any__c== True || 
            oppty.Scale_or_complexity_of_work__c== True || 
            oppty.Capability_of_delivery_team__c== True || 
            oppty.Bundled_offering_with_Consulting_Analyt__c== True || 
            oppty.Bundled_offering_with_only_Analytics_and__c== True || 
            oppty.Digital_Offering__c== True || 
            oppty.Lean_Six_Sigma_methodology_with_SEP__c== True || 
            oppty.Compatibility_of_the_solution_offering_t__c== True || 
            oppty.Proposed_delivery_location_model__c== True || 
            oppty.Platform_availability_scalability__c== True || 
            oppty.Transition_approach__c== True || 
            oppty.Transformation_roadmap__c== True 
            
            ) && (WLDvar=='Signed' || WLDvar=='Lost') )
            
            {    Checkboxes_flag=true; 
                 return True;
            }
            
            Else if(
            (oppty.Not_in_our_focus_geo__c== True || 
            oppty.Not_our_focus_service_line__c== True || 
            oppty.Not_our_focus_vertical__c== True || 
            oppty.One_or_more_towers_not_in_our_focus_area__c== True || 
            oppty.No_potential_for_downstream_revenue__c== True || 
            oppty.Not_possible_to_meet_desired_price_point__c== True || 
            oppty.No_unique_value_proposition_that_G_can_o__c== True || 
            oppty.Risk_in_engaging_financially_with_the_cl__c== True || 
            oppty.Low_probability_of_winning_due_to_incumb__c== True || 
            oppty.Low_probability_of_winning_due_to_strong__c== True || 
            oppty.No_weak_executive_relationships_conn__c== True || 
            oppty.No_weak_advisor_relationship__c== True || 
            oppty.Not_a_strategic_priority_for_the_client__c== True || 
            oppty.Client_did_not_have_the_resources_money__c== True || 
            oppty.No_sponsorship_from_client_side__c== True || 
            oppty.Client_not_serious_about_moving_forward__c== True || 
            oppty.Duplicate_Deal_ID__c== True) &&  WLDvar== 'Dropped by Genpact' )
            
            {    Checkboxes_flag=true;
                 return True;
            }
            
            Else if(
            (oppty.Price_was_more_than_the_allocated_budget__c== True || 
            oppty.Budget_cut_during_the_evaluation_process__c== True || 
            oppty.No_clear_benefit_in_making_the_change__c== True || 
            oppty.Change_in_company_strategy__c== True || 
            oppty.Change_in_management__c== True || 
            oppty.No_vendor_met_the_qualifying_criteria__c== True || 
            oppty.Needed_to_reassess_the_scope_of_work__c== True) && WLDvar=='Dropped by Client')
            
            {    Checkboxes_flag=true;
                 Return True;
            }
            
            Else
            
            {    Checkboxes_flag=false;
                 return false;
            }
            
        
        
}

 public pageReference Custom_cancel()
     {
                                            if (Userinfo.getUiThemeDisplayed().equals('Theme3'))
                                                {
                                                    PageReference pageRef = new PageReference('/' + oppty.id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                }
                                             else if (Userinfo.getUiThemeDisplayed().equals('Theme4t'))
                                                {
                                                    PageReference pageRef = new PageReference('/apex/Stale_deal_v2');
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                
                                                }
                                             else if(Userinfo.getUiThemeDisplayed().equals('Theme4d'))
                                                {    
                                                    PageReference pageRef = new PageReference('/apex/NavigateToDetail_mobile?oppid=' + oppty.id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef;
     
                                                }       
                                             else
                                                return Null;
     
         }
         
     
     Public boolean Validate_Closure()
     {      
            
            
        if(oppty.Deal_Type__c != 'Sole Sourced')
        {
            If (oppty.Deal_Type__c == 'Competitive' && !(string.isblank(oppty.Competitor__c)))
            {
                Competitor_flag=true;
                
                if(oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c!='' )
                    Other_Comp_flag=true;
                else
                    Other_Comp_flag=false;  
                
            }
            Else
                Competitor_flag=false;
            
            If(oppty.Deal_Type__c== 'Competitive' && WLDvar =='Lost' && !(string.isblank(oppty.Winner__c)))
            {   
                Winner_flag=true;
                
                if(oppty.Winner__c== 'Others' && oppty.Other_Winner__c!='' )
                    Other_Winner_flag=true;
                else
                    Other_Winner_flag=false;
                
            }
            Else 
                Winner_flag=false;
            
            If (oppty.Deal_Type__c == 'Competitive' && !(string.isblank(oppty.Send_Client_Survey__c)))
            {
                survey_flag=true;
            }
            Else
                survey_flag=false;
                
            If (oppty.Deal_Type__c == 'Competitive' && oppty.Send_Client_Survey__c == 'Yes' && oppty.Contact__c==Null)
            {
                survey_recipient_flag=true;
            }
            Else
                survey_recipient_flag=false;    
                
            
            if(Competitor_flag && survey_flag && WLDvar =='Signed' )
                {
                    if((oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c=='') || survey_recipient_flag)
                            return false;
                    else
                        return true;        
                
                }
                
            else if(Competitor_flag && Winner_flag && survey_flag && WLDvar =='Lost' )
                {
                    if((oppty.Winner__c== 'Others' && oppty.Other_Winner__c=='') || (oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c=='') || survey_recipient_flag)
                            return false;
                    else
                        return true;        
                
                }
                
            else if(Competitor_flag &&  WLDvar =='Dropped by Genpact' )
                {
                    if(oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c=='')
                            return false;
                    else
                        return true;        
                
                }
                
            else if(Competitor_flag && survey_flag && WLDvar =='Dropped by Client' )
                {
                    if((oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c=='') || survey_recipient_flag)
                            return false;
                    else
                        return true;        
                
                }
            Else
                return false;
            
        }
        
        Else
            return true;        
     
     
    }
    
    Public void reset_other_textbox()
    {
                IF(string.isblank(oppty.Competitor__c) )        
                {
                    oppty.Other_Competitor__c='';
                    oppty.Other_Winner__c='';
                }
                
                                
                
                if((!string.isblank(oppty.Competitor__c)) && (!oppty.Competitor__c.contains('Others')) )
                {
                    oppty.Other_Competitor__c='';
                    oppty.Other_Winner__c='';
                }
                
                
                //if(string.isblank(oppty.Winner__c))
                //{
                //    oppty.Other_Winner__c='';
                //}
                
                Else if(oppty.Winner__c!='Others')
                {
                    oppty.Other_Winner__c='';
                }
                    
                
              
            
            dynamic_Winner();  
    }
    
    
    Public Void dynamic_Winner()
    {
        Named_winner_list = new list<selectOption>();
        Named_winner_list.add(new selectOption('', '- None -'));
        
            if(!String.isBlank(oppty.Competitor__c))
            {

                List<String> pickValues = oppty.Competitor__c.split(';');
                For(String s : pickValues)
                {
                    Named_winner_list.add(new selectOption(s, s));
                
                }
                
            }
        
    
    }
    
    
    Public string errorMessage()
    {   string message='';
        
        if(Checkboxes_flag==false)
        {
            message='Atleast one input(checkbox) is required.';
        }
        
        if( string.isblank(oppty.Competitor__c))
        {
            message=message + '<br> Since "Competition Presence" is Competitive, entry of at least one Competitor is MANDATORY.';
        }
        
        if(!string.isblank(oppty.Competitor__c) && oppty.Competitor__c.contains('Others') && oppty.Other_Competitor__c=='')
        {
            message= message + '<br> Entry of data in other field is MANDATORY since "Named Competitor(s)" is "Other".';
        }
        
        if(WLDvar == 'Lost' && string.isblank(oppty.Winner__c))
        {
            message= message + '<br> Since the deal is a Loss in a Competitive scenario, entry of the eventual "Winner" is MANDATORY.';
        }
        
        if(oppty.Winner__c == 'Others' && oppty.Other_Winner__c == '')
        {
            message= message + '<br> Entry of data in Other field is MANDATORY since "Named Winner" is "Other."';
        }
        
        if(WLDvar != 'Dropped by Genpact' && string.isblank(oppty.Send_Client_Survey__c))
        {
            message= message + '<br> Do you wish to send Client Survey.';
        }
        
        if(oppty.Send_Client_Survey__c=='Yes' && oppty.Contact__c == null)
        {
            message= message + '<br> Please Select person from client side who will receive the survey.';
        }
        
        return message;    
    
    }
    
    public List<selectOption> WLD_value{
    get {
      if (WLD_value == null) {
         WLD_value = new List<selectOption>();
                 
        Schema.DescribeFieldResult field = Opportunity.W_L_D__c.getDescribe();
        WLD_value.add(new selectOption('None', '--None--'));
         for (Schema.PicklistEntry f : field.getPicklistValues())
          {
            IF(f.getvalue().equals('Dropped by Genpact') || f.getvalue().equals('Dropped by Client'))
                WLD_value.add(new selectOption(f.getvalue(),f.getLabel()));
            }
      }
      return WLD_value;          
    }
    set;
    }
     
}