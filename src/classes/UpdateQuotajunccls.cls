/*-------------
        Trigger Description : This class will be used in trigger UpdateUserQuotaAttainment on user for quota attainment recalulation.
        Organisation        : Tech Mahindra NSEZ
        Created by          : Arjun Srivastava
        Location            : Genpact
        Created Date        : 29 June 2014  
        Last modified date  : 29 June 2014
---------------*/
global class UpdateQuotajunccls {
    @future
    public static void UpdateQuotajuncmeth1(SET<ID> userids) 
    {
          UpdateQuotajuncmeth(userids); 
    }
    
    public static void UpdateQuotajuncmeth(SET<ID> userids) 
    {
        //checking if the quota has been setup for this user?
        if([select id from Set_Quotas__c  where Quota_Header__r.Sales_Person__c IN:userids AND Quota_Header__r.Year__c=:String.valueof(system.today().year())].size()>0)
        {
            //updating Quotajunc record for revaluation in batch
            List<QuotaLineOpportunityJunction__c> quotajunc=[select id,Revaluate__c,Products_Revenue__c from QuotaLineOpportunityJunction__c where Quota_header__r.Sales_Person__c IN:userids];
            for(QuotaLineOpportunityJunction__c qjunc: quotajunc)
            {
                qjunc.Revaluate__c=true;
            }
            update quotajunc;   //updating quotajunc records.
        }  
    }
}