global class OpportunityProductsController {
    public List<OLIWrapper> oppLineItemWrapperList{get;set;}
    public ID selectedId{get;set;}
    public OpportunityLineItem selectedOLI{get;set;}
    public Integer productCount{get;set;}
    public ID oppID{get;set;}
    public String closedate {get;set;}
    public String jsonString{get;set;}
    public Boolean isQsrmInApproval{get;set;}
    
    public OpportunityProductsController(){
        oppID = ApexPages.currentPage().getParameters().get('recordID');
        System.debug('oppID =='+oppID);
        
        List<OpportunityLineItem> oppProduct_list = [SELECT ID, Name, Product2ID, Product2.Name,Product2.Service_line__c,Product2.Nature_of_work__c, OpportunityId, Opportunity.Name, Delivering_Organisation__c,opportunity.stageName, 
                                                     Delivery_Location__c, Ebit__c, FTE__c, LastModifiedBy.name, Local_Currency__c, Opportunity_Id__c,lastModifiedDate,
                                                     Pricer_name__c, Product_BD_Rep__r.Name, Revenue_Start_Date__c, Sub_Delivering_Organisation__c,createdDate,
                                                     UnitPrice, createdby.name, Contract_Term__c, CYR__c, NYR__c, Revenue_Exchange_Rate__c, Y1_TCV_USD_OLI_CPQ__c,
                                                     Y10_TCV_USD_OLI_CPQ__c, Y2_TCV_USD_OLI_CPQ__c, Y3_TCV_USD_OLI_CPQ__c, Y4_TCV_USD_OLI_CPQ__c, 
                                                     Y5_TCV_USD_OLI_CPQ__c, Y6_TCV_USD_OLI_CPQ__c, Y7_TCV_USD_OLI_CPQ__c, Y8_TCV_USD_OLI_CPQ__c, Y9_TCV_USD_OLI_CPQ__c,
                                                     Y1_AOI_USD_OLI_CPQ__c, Y10_AOI_USD_OLI_CPQ__c, Y2_AOI_USD_OLI_CPQ__c, Y3_AOI_USD_OLI_CPQ__c, Y4_AOI_USD_OLI_CPQ__c, 
                                                     Y5_AOI_USD_OLI_CPQ__c, Y6_AOI_USD_OLI_CPQ__c, Y7_AOI_USD_OLI_CPQ__c, Y8_AOI_USD_OLI_CPQ__c, Y9_AOI_USD_OLI_CPQ__c, 
                                                     Total_TCV_Product_CPQ__c, Overall_AOI_CPQ__c, Pricing_Pricer_Name_OLI_CPQ__c, Pricing_Deal_Type_OLI_CPQ__c,
                                                     EBIT_OLI_CPQ__c, Price_line_Exchg_Rate__c, QSRM_Status__c, opportunity.Deal_Status_In_CPQ__c, 
                                                     Opportunity.OwnerId,opportunity.CloseDate, opportunity.product_Count__c, (SELECT ID, Revenue, Type, Quantity, Description, ScheduleDate, opportunityLineItemID
                                                                                                                               FROM OpportunityLineItemSchedules ORDER BY ScheduleDate ASC LIMIT 24) FROM OpportunityLineItem WHERE OpportunityId =: oppID ORDER BY Product2.Name];
        
        System.debug('oli in cunstructor=='+oppProduct_list[0].OpportunityLineItemSchedules);  
        productCount = 0;
        oppLineItemWrapperList = new List<OLIWrapper>();
        
        String qsrmStatus = oppProduct_list[0].QSRM_Status__c;
        if(qsrmStatus != 'Your QSRM is submitted for Approval'){
            isQsrmInApproval = true;            
        }
        else{
            isQsrmInApproval = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'While QSRM is in approval status, the deals cannot be edited.'));
        }
        
        Date oppCloseDate = oppProduct_list[0].opportunity.CloseDate;
        closedate = DateTime.newInstance(oppCloseDate.year(),oppCloseDate.month(),oppCloseDate.day()).format('yyyy-MM-dd');
        
        for(OpportunityLineItem oli : oppProduct_list){
            List<OppScheduleWrapper> ScheduleList = new List<OppScheduleWrapper>();
            for(OpportunityLineItemSchedule oppSchedule :oli.OpportunityLineItemSchedules ){
                ScheduleList.add(new OppScheduleWrapper(oppSchedule));  
            }
            oppLineItemWrapperList.add(new OLIWrapper(oli, ScheduleList));
            productCount++;
        }
        jsonString = JSON.serialize(oppLineItemWrapperList);
        system.debug('jsonString=='+jsonString);
        selectedId = oppLineItemWrapperList[0].oppProduct.ID;
        selectedOLI = oppLineItemWrapperList[0].oppProduct;
        
    }
    
    @RemoteAction
    global static PageReference deleteOppProduct(ID oppProductID, ID opportunityID){
        try{
            OpportunityLineItem oli = [SELECT ID FROM OpportunityLineItem WHERE ID = :oppProductID];
            system.debug('oli=='+oli);
            DELETE oli;
            PageReference pf = new PageReference('/'+opportunityID);
            pf.setRedirect(true);
            return pf;   
        }
        catch(Exception e){
            System.debug('ERROR==='+e.getMessage()+ '' +e.getLineNumber());
        }
        return null;
    }
    
    public PageReference save(){
        Set<ID> oppIds = new Set<ID>();
        List<OpportunityLineItemSchedule> oppScheduleList = new List<OpportunityLineItemSchedule>();
        
        for(OLIWrapper wrapper : oppLineItemWrapperList){
            for(OppScheduleWrapper oppScheduleWrapper : wrapper.revenueScheduleWrapperList){
                System.debug('oppLineItemScheduleList=='+wrapper.revenueScheduleWrapperList);
                OpportunityLineItemSchedule oppSchedule = new OpportunityLineItemSchedule();
                oppSchedule.Id = oppScheduleWrapper.scheduleID;
                oppSchedule.Revenue = oppScheduleWrapper.revenue;
                oppSchedule.ScheduleDate = Date.valueOf(oppScheduleWrapper.scheduleDate);
                oppScheduleList.add(oppSchedule);
            } 
            oppIDs.add(wrapper.oppProduct.ID);
        } 
        
        update oppScheduleList;
        
        OpportunityLineItemTriggerHelper.updateYearlyRevenue(oppIds);
        PageReference prf = new PageReference('/apex/OpportunityProductDetailPage?recordID='+oppID);
        prf.setRedirect(true);
        return prf;
    }
    
    public class OppScheduleWrapper{
        public Decimal revenue{get;set;}
        public String type{get;set;}
        public String scheduleDate{get;set;}
        public ID scheduleID{get;set;}
        public OppScheduleWrapper(OpportunityLineItemSchedule oppSchedule){
            revenue = oppSchedule.Revenue;
            scheduleDate = (String.valueOf(oppSchedule.ScheduleDate)).substring(0,10);
            scheduleID = oppSchedule.Id;
        }
    }
    
    
    public class OLIWrapper{
        public OpportunityLineItem oppProduct{get;set;}
        public String revenueStartDate{get;set;}
        public List<OppScheduleWrapper> revenueScheduleWrapperList{get;set;}
        public OLIWrapper(OpportunityLineItem oli, List<OppScheduleWrapper> ScheduleWrapperList){
            
            oppProduct = oli;
            revenueStartDate = String.valueOf(oli.Revenue_Start_Date__c);
            revenueScheduleWrapperList = ScheduleWrapperList;
            System.debug('oppProduct in wrapper =='+oppProduct);
        }
    }
}