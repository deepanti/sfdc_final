//Description : Tracker Class for GPBatchAutoDeleteRejectPrjctSchedular
@isTest
public class GPBatchAutoDltRejctPrjctSchedularTracker {

    public static testmethod void testschedule() {
        Test.StartTest();
        GPBatchAutoDeleteRejectPrjctSchedular obj = new GPBatchAutoDeleteRejectPrjctSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, obj);
        Test.stopTest();
    }
}