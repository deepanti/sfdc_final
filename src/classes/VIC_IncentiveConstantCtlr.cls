/*
 * @author: Prahant Kumar1
 * @since: 23/05/2018
 * @description: this class contains method to get data 
 * related to vic_Incentive_Constant__mdt
 * 
*/
public class VIC_IncentiveConstantCtlr {
    @TestVisible
    private static Map<String, vic_Incentive_Constant__mdt>lableToMetadataMap = new Map<String, vic_Incentive_Constant__mdt>();
    @TestVisible
    private static Map<String, vic_Incentive_Constant__mdt> mapStrKeyToIncentiveCons = new Map<String, vic_Incentive_Constant__mdt>();
    static{
        for(vic_Incentive_Constant__mdt objMetaData: [SELECT Label, DeveloperName, vic_Value__c FROM vic_Incentive_Constant__mdt]){
            lableToMetadataMap.put(objMetaData.Label, objMetaData);
            mapStrKeyToIncentiveCons.put(objMetaData.DeveloperName , objMetaData);
        }
    }
    
    /*
     * @params: label (String)
     * @return: vic_Value__c (String)
     * @description: This method returns value of incentive constant identefied by label.
     * label is case sensitive
     * 
    */
    public static String getIncentiveConstantByLabel(String label){
        return lableToMetadataMap.containsKey(label)? lableToMetadataMap.get(label).vic_Value__c : '';
    }
    
    /*
     * @params: DeveloperName (String)
     * @return: vic_Value__c (String)
     * @description: This method returns value of incentive constant identefied by DeveloperName.
     * DeveloperName is case sensitive
    */
    public static String getIncentiveConstantByDeveloperName(String DeveloperName){
        String value = '';
        for(String label: lableToMetadataMap.keySet()){
            if(lableToMetadataMap.get(label).DeveloperName == DeveloperName){
                value = lableToMetadataMap.get(label).vic_Value__c;
            }
        }
        return value;
    }
    
    /*
        @Description: function will return map of all metadata with key of nature of work(Label)
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Map<String, vic_Incentive_Constant__mdt> getAllMetaData(){
        return mapStrKeyToIncentiveCons;
    }
}