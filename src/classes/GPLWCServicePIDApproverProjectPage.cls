public with sharing class GPLWCServicePIDApproverProjectPage {
    @AuraEnabled
    public static GPAuraResponse getProjectHeaderDetails(String recordId) {
        //recordId = [SELECT Id FROM GP_Project__c where GP_EP_Project_Number__c = '300347649' limit 1][0].Id;
        GPControllerProjectHeaderInfo gcphi = new GPControllerProjectHeaderInfo(recordId);
        return gcphi.getProjectDetails();
    }
    /*
     * Fetch Project Details data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse getProjectDetail(String recordId) {
        try{
            List<GP_Project__c> listofPIDs = [SELECT Id, GP_Approval_Status__c, GP_Temp_Approval_Status__c 
            FROM GP_Project__c WHERE id =: recordId];

            if(listofPIDs != null && listofPIDs.size() > 0) {
                if(listofPIDs[0].GP_Approval_Status__c != 'Pending For Approval') {
                    return new GPAuraResponse(true, 'PID is in ' + listofPIDs[0].GP_Approval_Status__c + ' status.', null);
                } else {
                    if(String.isNotBlank(listofPIDs[0].GP_Temp_Approval_Status__c)) {
                        Map<String,GPWrapperUserApprovalStatus> mapOftempApprovalStatus = 
                        (Map<String,GPWrapperUserApprovalStatus>) JSON.deserialize(listofPIDs[0].GP_Temp_Approval_Status__c, 
                        Map<String,GPWrapperUserApprovalStatus>.class);

                        if(mapOftempApprovalStatus != null && mapOftempApprovalStatus.containsKey(UserInfo.getUserId())
                        && mapOftempApprovalStatus.get(UserInfo.getUserId()).status == 'Pending For Approval') {
                            GPControllerProjectApproval objController = new GPControllerProjectApproval(recordId);
                            return objController.getProjectDetail();
                        } else {
                            return new GPAuraResponse(true, 'PID not in your bucket', null);
                        }
                    } else {
                        return new GPAuraResponse(true, 'No Approval required for now.', null);
                    }
                }
            } else {
                return new GPAuraResponse(true, 'PID not found', null);
            }
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }    
    /*
     * Fetch Approval history data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchApprovalHistoryData(String recordId) {
        //recordId = [SELECT Id FROM GP_Project__c where GP_EP_Project_Number__c = '300348102' limit 1][0].Id;
        GPControllerProjectApprovalHistory objProjectApprovalHistory = new GPControllerProjectApprovalHistory();
        return objProjectApprovalHistory.getApprovalsHistory(recordId);
    }
    /*
     * Submit the PID for Approve or reject
	* */
    @AuraEnabled
    public static GPAuraResponse submissionByPIDApprover(String recordId, String requestType, String submissionType, string strSelectedStage, 
    String strComment, String taxCode, String reports, String productCode) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(recordId);
        GPAuraResponse response;
        if(requestType == 'close') {
            if(submissionType == 'approve') {
                response = objController.ProjectStatusChange('', strComment, 'Approve', null, null, productCode);
            } else {
                response = objController.ProjectStatusChange('', strComment, 'Reject', null, null, productCode);
            }
        } else {
            if(submissionType == 'approve') {
                response = objController.ProjectStatusChange(strSelectedStage, strComment, 'Approve', taxCode, reports, productCode);
            } else {
                response = objController.ProjectStatusChange(strSelectedStage, strComment, 'Reject', null, null, productCode);
            }
        }
        return response;
    }
    /*
     * Fetch Object data on page load.
	* */
    /*@AuraEnabled
    public static GPAuraResponse fetchObjectData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        if(objectName == 'GP_Project__c') {
            return glcpapp.fetchPIDData();
        } else if(objectName == 'GP_Project_Work_Location_SDO__c') {
            return glcpapp.fetchWorkLocationSDOData();
        } else if(objectName == 'GP_Project_Leadership__c') {
            return glcpapp.fetchProjectLeadershipSDOData();
        } else if(objectName == 'GP_Billing_Milestone__c') {
            return glcpapp.fetchBillingMilestoneData();
        } else if(objectName == 'GP_Profile_Bill_Rate__c') {
            return glcpapp.fetchProfileBillRateData();
        } else if(objectName == 'GP_Resource_Allocation__c') {
            return glcpapp.fetchResourceAllocationData();
        } else if(objectName == 'GP_Project_Document__c') {
            return glcpapp.fetchDocumentData();
        } else if(objectName == 'GP_Project_Budget__c') {
            return glcpapp.fetchBudgetAndExpensesData();
        } else {
            return new GPAuraResponse(false, 'SObject is valid.', null);
        }
    }*/

    /*
     * Fetch PID data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchPIDData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchPIDData();
    }

    /*
     * Fetch work location data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchWorkLocationSDOData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchWorkLocationSDOData();
    }

    /*
     * Fetch project leadership data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchProjectLeadershipSDOData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchProjectLeadershipSDOData();
    }

    /*
     * Fetch project resource allocation data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchResourceAllocationData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchResourceAllocationData();
    }
    /*
     * Fetch project Billing Milestone data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchBillingMilestoneData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchBillingMilestoneData();
    }
    /*
     * Fetch project document data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchDocumentData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchDocumentData();
    }
    /*
     * Fetch project document data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchProfileBillRateData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchProfileBillRateData();
    }
    /*
     * Fetch project Budget and expenses data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchBudgetAndExpensesData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchBudgetAndExpensesData();
    }
    /*
     * Fetch change summary data on page load.
	* */
    @AuraEnabled
    public static GPAuraResponse fetchChangeSummaryData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchChangeSummaryDetails(true);
    }
    /*
     * Fetch PID summary data on page load (some required fields on PID creation).
	* */
    @AuraEnabled
    public static GPAuraResponse fetchPIDSummaryData(String recordId, String objectName) {
        GPLWCControllerPIDApproverProjectPage glcpapp = new GPLWCControllerPIDApproverProjectPage(recordId, objectName);
        return glcpapp.fetchPIDSummaryDetails();
    }    
}