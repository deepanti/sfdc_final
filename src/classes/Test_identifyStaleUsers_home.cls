@isTest
public class Test_identifyStaleUsers_home 
{
    /* static TestMethod void Test1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);   
        oOpportunity.OwnerId = u.id;
        oOpportunity.CloseDate = System.today().addDays(2);
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(2);
        update oOpportunity;
        Test.startTest();
        identifyStaleUsers.chkSF1();
        identifyStaleUsers.getStaleCount();
        identifyStaleUsers.getNonTsStaleCount();
        identifyStaleUsers.getStalledCount();
        identifyStaleUsers.getNonTsStalledCount();
        identifyStaleUsers.chkProfile();
        Test.stopTest();
    }
 
   Static testmethod void testmethod3()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        //Profile p1 = [SELECT Id FROM Profile WHERE Name='Stale Deals Owner']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        //     User u2 =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p1.Id,'standardusertestgen2018@testorg.com' );
        User u1 =GEN_Util_Test_Data.CreateUser('standarduser2018@tesgygb.com',p.Id,'standardusertestgen20198@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
    Sales_Unit__c salesunitobject1=new Sales_Unit__c(name='test2',Sales_Leader__c=u.id);
        insert salesunitobject1;
        account accountobject=new account(name='test1',Hunting_Mining__c='Hunting',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
     account accountobject1=new account(name='test2',Hunting_Mining__c='Mining',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject1.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject1;
       
        List<opportunity> opplist= new list<opportunity> ();
        //     opportunity opp=new opportunity(select name,StageName,CloseDate,Insight__c,Revenue_Start_Date__c,Previous_Stage__c,Highest_Revenue_Product__c,Nature_of_Work_highest__c,CloseDate,CreatedDate,Last_stage_change_date__c where name='1234876876',StageName='1. Discover',CloseDate=system.today()-10,Previous_Stage__c='',CreatedDate=system.today()-40,Last_stage_change_date__c='',Insight__c=system.today()+30,Revenue_Start_Date__c=system.today(),accountid=accountobject.id and W_L_D__c='',Highest_Revenue_Product__c='Consulting',Nature_of_Work_highest__c='Analytics');
        
        
        opportunity opp1=new opportunity(name='opp1234876876',StageName='1. Discover',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today()+20,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp2=new opportunity(name='opp123487687',StageName='1. Discover',CloseDate=system.today()+120,Revenue_Start_Date__c=system.today()+120,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
            opportunity opp3=new opportunity(name='opp124876876',StageName='1. Discover',CloseDate=system.today()+120,Revenue_Start_Date__c=system.today()+120,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
                opportunity opp4=new opportunity(name='opp123476876',StageName='1. Discover',CloseDate=system.today()+120,Revenue_Start_Date__c=system.today()+120,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp5=new opportunity(name='opp1276876',StageName='1. Discover',CloseDate=system.today()+120,Revenue_Start_Date__c=system.today()+120,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp6=new opportunity(name='opp127876',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp7=new opportunity(name='opp12876876',StageName='1. Discover',CloseDate=system.today()+120,Revenue_Start_Date__c=system.today()+120,accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp8=new opportunity(name='opp127687',StageName='1. Discover',CloseDate=system.today()+12,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
            opportunity opp9=new opportunity(name='opp124876',StageName='1. Discover',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
                opportunity opp10=new opportunity(name='opp1476876',StageName='1. Discover',CloseDate=system.today()+15,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp11=new opportunity(name='opp12776',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp12=new opportunity(name='opp12787',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp13=new opportunity(name='opp1234876876',StageName='2. Define',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp14=new opportunity(name='opp123487687',StageName='2. Define',CloseDate=system.today()+12,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
            opportunity opp15=new opportunity(name='opp124876876',StageName='2. Define',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
                opportunity opp16=new opportunity(name='opp123476876',StageName='2. Define',CloseDate=system.today()+15,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp17=new opportunity(name='opp1276876',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp18=new opportunity(name='opp127876',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp19=new opportunity(name='opp12876876',StageName='2. Define',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
        opportunity opp20=new opportunity(name='opp127687',StageName='2. Define',CloseDate=system.today()+12,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
            opportunity opp21=new opportunity(name='opp124876',StageName='2. Define',CloseDate=system.today()+20,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
                opportunity opp22=new opportunity(name='opp1476876',StageName='2. Define',CloseDate=system.today()+15,Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp23=new opportunity(name='opp12776',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');
opportunity opp24=new opportunity(name='opp12787',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject1.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics');


         opplist.add(opp1);
        opplist.add(opp2);
       opplist.add(opp3);
        opplist.add(opp4);
    opplist.add(opp5);  
        opplist.add(opp6);  
      opplist.add(opp7);
        opplist.add(opp8);
       opplist.add(opp9);
        opplist.add(opp10);
    opplist.add(opp11);  
        opplist.add(opp12);  
            opplist.add(opp13);
        opplist.add(opp14);
       opplist.add(opp15);
        opplist.add(opp16);
    opplist.add(opp17);  
        opplist.add(opp18);  
      opplist.add(opp19);
        opplist.add(opp20);
       opplist.add(opp21);
        opplist.add(opp22);
    opplist.add(opp23);  
        opplist.add(opp24);  
        
        insert opplist;
        
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp1.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp1.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Analytics',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
 
        List<opportunity> updateopplist=new List<opportunity>();    
        
Deal_Cycle__c   DC= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC3= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC4= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC5= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC6= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC7= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC8= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC9= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC10= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC11= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC12= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC13= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC14= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC15= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC16= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC17= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC18= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC19= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC20= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC21= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC22= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC23= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);

        Deal_Cycle__c   DC24= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Retail',Ageing__c=20); 
        Deal_Cycle__c   DC25= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Large',Ageing__c=30);                       
        
        Insert DC;
         Insert DC1;
        Insert DC2;
        Insert DC3;
       Insert DC4;
       Insert DC5;
        Insert DC6;
        Insert DC7;
     Insert DC8;
        Insert DC9;
       Insert DC10;
       Insert DC11;
        Insert DC12;
        Insert DC13;
          Insert DC14;
        Insert DC15;
        Insert DC16;
       Insert DC17;
       Insert DC18;
        Insert DC19;
        Insert DC20;
     Insert DC21;
        Insert DC22;
       Insert DC23;
      Insert DC24;
       Insert DC25;
        
        Double Discover_age_hun_sole_retail_non_ts=DC.Cycle_Time__c/2;
        Double Discover_age_hun_sole_medium_non_ts=DC1.Cycle_Time__c/2;
Double Discover_age_hun_sole_large_non_ts=DC2.Cycle_Time__c/2;
Double Discover_age_hun_comp_retail_non_ts=DC3.Cycle_Time__c/2;
Double Discover_age_hun_comp_medium_non_ts=DC4.Cycle_Time__c/2;
Double Discover_age_hun_comp_large_non_ts=DC5.Cycle_Time__c/2;
        Double Discover_age_min_sole_retail_non_ts=DC6.Cycle_Time__c/2;
        Double Discover_age_min_sole_medium_non_ts=DC7.Cycle_Time__c/2;
Double Discover_age_min_sole_large_non_ts=DC8.Cycle_Time__c/2;
Double Discover_age_min_comp_retail_non_ts=DC9.Cycle_Time__c/2;
Double Discover_age_min_comp_medium_non_ts=DC10.Cycle_Time__c/2;
Double Discover_age_min_comp_large_non_ts=DC11.Cycle_Time__c/2;
        Double Define_age_hun_sole_retail_non_ts=DC.Cycle_Time__c/2;
        Double Define_age_hun_sole_medium_non_ts=DC1.Cycle_Time__c/2;
Double Define_age_hun_sole_large_non_ts=DC2.Cycle_Time__c/2;
Double Define_age_hun_comp_retail_non_ts=DC3.Cycle_Time__c/2;
Double Define_age_hun_comp_medium_non_ts=DC4.Cycle_Time__c/2;
Double Define_age_hun_comp_large_non_ts=DC5.Cycle_Time__c/2;
        Double Define_age_min_sole_retail_non_ts=DC6.Cycle_Time__c/2;
        Double Define_age_min_sole_medium_non_ts=DC7.Cycle_Time__c/2;
Double Define_age_min_sole_large_non_ts=DC8.Cycle_Time__c/2;
Double Define_age_min_comp_retail_non_ts=DC9.Cycle_Time__c/2;
Double Define_age_min_comp_medium_non_ts=DC10.Cycle_Time__c/2;
Double Define_age_min_comp_large_non_ts=DC11.Cycle_Time__c/2;
        
    
        updateopplist= new list<opportunity>();
//double huntmin=accountobject.Hunting_Mining__c;

        
        opp1.Nature_of_Work_highest__c='Managed';
       // opp1.Highest_Revenue_Product__c='Consulting';
        opp1.Amount=10000;
    //opp1.QSRM_value_not_approved_rejected__c=0;
        opp1.stageName='1. Discover';
        opp1.closedate=System.today()+120;
        opp1.ProductionTCV__c = 100000;
        opp1.Transformation_stale_deal_timestamp__c=null;
        opp1.ownerid=u.id;
        opp1.Previous_Stage__c='';
    
        opp1.Deal_Type__c='Competitive';
        opp1.Last_stage_change_date__c=NULL;
        opp1.Insight__c=30;
        opp1.Revenue_Start_Date__c=system.today()+120;
      
     Date op1=System.today()+40;
                     updateopplist.add(opp1);
    
          
    opp2.Nature_of_Work_highest__c='Managed';
      //  opp2.Highest_Revenue_Product__c='Consulting';
        opp2.Amount=10000;
        opp2.stageName='1. Discover';
        opp2.closedate=System.today()+120;
        opp2.ProductionTCV__c = 6000000;
    opp2.Deal_Type__c='Competitive';
        opp2.Transformation_stale_deal_timestamp__c=null;
        opp2.ownerid=u.id;
        opp2.Previous_Stage__c='';
    
      //  opp2.QSRM_value_not_approved_rejected__c=0;
        opp2.Last_stage_change_date__c=NULL;
       opp2.Insight__c=40;
        opp2.Revenue_Start_Date__c=system.today()+120;
 updateopplist.add(opp2);
  

opp3.Nature_of_Work_highest__c='Managed';
    //    opp3.Highest_Revenue_Product__c='Consulting';
        opp3.Amount=10000;
        opp3.stageName='1. Discover';
  //  opp3.QSRM_value_not_approved_rejected__c!=0;
        opp3.closedate=System.today()+120;
        opp3.ProductionTCV__c = 50000000;
        opp3.Transformation_stale_deal_timestamp__c=null;
        opp3.ownerid=u.id;
  
        opp3.Previous_Stage__c='';
        opp3.Deal_Type__c='Sole Sourced';
        opp3.Last_stage_change_date__c=NULL;
      //  opp3.Insight__c=30;
        opp3.Revenue_Start_Date__c=system.today()+120;

                    updateopplist.add(opp3);

    
opp4.Nature_of_Work_highest__c='Managed';
    //    opp4.Highest_Revenue_Product__c='Consulting';
        opp4.Amount=10000;
        opp4.stageName='1. Discover';
        opp4.closedate=System.today()+120;
        opp4.ProductionTCV__c = 4000000;
        opp4.Transformation_stale_deal_timestamp__c=null;
        opp4.ownerid=u.id;
        opp4.Previous_Stage__c='';
        opp4.Deal_Type__c='Sole Sourced';
        opp4.Last_stage_change_date__c=NULL;
      //  opp4.Insight__c=20;
  //  opp4.QSRM_value_not_approved_rejected__c=0;
        opp4.Revenue_Start_Date__c=system.today()+120;

                    updateopplist.add(opp4);

    opp5.Nature_of_Work_highest__c='Managed';
   //     opp5.Highest_Revenue_Product__c='Consulting';
        opp5.Amount=10000;
        opp5.stageName='1. Discover';
        opp5.closedate=System.today()+120;
        opp5.ProductionTCV__c = 40000;
        opp5.Transformation_stale_deal_timestamp__c=null;
        opp5.ownerid=u.id;
        opp5.Previous_Stage__c='';
        opp5.Deal_Type__c='Sole Sourced';
        opp5.Last_stage_change_date__c=NULL;
      //  opp5.Insight__c=20;
  //  opp5.QSRM_value_not_approved_rejected__c=0;
        opp5.Revenue_Start_Date__c=system.today()+120;

                    updateopplist.add(opp5);

          
          opp6.Nature_of_Work_highest__c='Managed';
   //     opp6.Highest_Revenue_Product__c='Consulting';
        opp6.Amount=10000;
        opp6.stageName='1. Discover';
        opp6.closedate=System.today()+20;
        opp6.ProductionTCV__c = 20000000;
        opp6.Transformation_stale_deal_timestamp__c=null;
        opp6.ownerid=u.id;
        opp6.Previous_Stage__c='';
        opp6.Deal_Type__c='Competitive';
        opp6.Last_stage_change_date__c=NULL;
      //  opp6.Insight__c=20;
  //  opp6.QSRM_value_not_approved_rejected__c=0;
        opp6.Revenue_Start_Date__c=system.today()+20;

                    updateopplist.add(opp6);
          
          
        opp7.Nature_of_Work_highest__c='Managed';
   //     opp7.Highest_Revenue_Product__c='Consulting';
        opp7.Amount=10000;
    //opp7.QSRM_value_not_approved_rejected__c=0;
        opp7.stageName='1. Discover';
        opp7.closedate=System.today()+20;
        opp7.ProductionTCV__c = 100000;
        opp7.Transformation_stale_deal_timestamp__c=null;
        opp7.ownerid=u.id;
        opp7.Previous_Stage__c='';
        opp7.Deal_Type__c='Competitive';
        opp7.Last_stage_change_date__c=NULL;
        //opp7.Insight__c=30;
        opp7.Revenue_Start_Date__c=system.today()+20;
      
     
                     updateopplist.add(opp7);
    
          
    opp8.Nature_of_Work_highest__c='Managed';
  //      opp8.Highest_Revenue_Product__c='Consulting';
        opp8.Amount=10000;
        opp8.stageName='1. Discover';
        opp8.closedate=System.today()+10;
        opp8.ProductionTCV__c = 6000000;
    opp8.Deal_Type__c='Competitive';
        opp8.Transformation_stale_deal_timestamp__c=null;
        opp8.ownerid=u.id;
        opp8.Previous_Stage__c='';
      //  opp8.QSRM_value_not_approved_rejected__c=0;
        opp8.Last_stage_change_date__c=NULL;
       // opp8.Insight__c=40;
        opp8.Revenue_Start_Date__c=system.today();
 updateopplist.add(opp8);
  

opp9.Nature_of_Work_highest__c='Managed';
     //   opp9.Highest_Revenue_Product__c='Consulting';
        opp9.Amount=10000;
        opp9.stageName='1. Discover';
  //  opp9.QSRM_value_not_approved_rejected__c!=0;
        opp9.closedate=System.today()+10;
        opp9.ProductionTCV__c = 50000000;
        opp9.Transformation_stale_deal_timestamp__c=null;
        opp9.ownerid=u.id;
        opp9.Previous_Stage__c='';
        opp9.Deal_Type__c='Sole Sourced';
        opp9.Last_stage_change_date__c=NULL;
      //  opp9.Insight__c=30;
        opp9.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp9);

              
opp10.Nature_of_Work_highest__c='Managed';
   //     opp10.Highest_Revenue_Product__c='Consulting';
        opp10.Amount=10000;
        opp10.stageName='1. Discover';
        opp10.closedate=System.today()+30;
        opp10.ProductionTCV__c = 4000000;
        opp10.Transformation_stale_deal_timestamp__c=null;
        opp10.ownerid=u.id;
        opp10.Previous_Stage__c='';
        opp10.Deal_Type__c='Sole Sourced';
        opp10.Last_stage_change_date__c=NULL;
      //  opp10.Insight__c=20;
  //  opp10.QSRM_value_not_approved_rejected__c=0;
        opp10.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp10);


    opp11.Nature_of_Work_highest__c='Managed';
   //     opp11.Highest_Revenue_Product__c='Consulting';
        opp11.Amount=10000;
        opp11.stageName='1. Discover';
        opp11.closedate=System.today()+30;
        opp11.ProductionTCV__c = 40000;
        opp11.Transformation_stale_deal_timestamp__c=null;
        opp11.ownerid=u.id;
        opp11.Previous_Stage__c='';
        opp11.Deal_Type__c='Sole Sourced';
        opp11.Last_stage_change_date__c=NULL;
      //  opp11.Insight__c=20;
  //  opp11.QSRM_value_not_approved_rejected__c=0;
        opp11.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp11);

          

          opp12.Nature_of_Work_highest__c='Managed Services';
   //     opp12.Highest_Revenue_Product__c='Consulting';
        opp12.Amount=10000;
        opp12.stageName='1. Discover';
        opp12.closedate=System.today()+30;
        opp12.ProductionTCV__c = 20000000;
        opp12.Transformation_stale_deal_timestamp__c=null;
        opp12.ownerid=u.id;
        opp12.Previous_Stage__c='';
        opp12.Deal_Type__c='Competitive';
        opp12.Last_stage_change_date__c=NULL;
      //  opp12.Insight__c=20;
  //  opp12.QSRM_value_not_approved_rejected__c=0;
        opp12.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp12);
          
        
        opp13.Nature_of_Work_highest__c='Managed';
    //    opp13.Highest_Revenue_Product__c='Consulting';
        opp13.Amount=10000;
    //opp13.QSRM_value_not_approved_rejected__c=0;
        opp13.stageName='2. Define';
        opp13.closedate=System.today()+10;
        opp13.ProductionTCV__c = 100000;
        opp13.Transformation_stale_deal_timestamp__c=null;
        opp13.ownerid=u.id;
        opp13.Previous_Stage__c='';
        opp13.Deal_Type__c='Competitive';
        opp13.Last_stage_change_date__c=NULL;
        //opp13.Insight__c=30;
        opp13.Revenue_Start_Date__c=system.today();
      
     
                     updateopplist.add(opp13);
    
          
    opp14.Nature_of_Work_highest__c='Managed';
   //     opp14.Highest_Revenue_Product__c='Consulting';
        opp14.Amount=10000;
        opp14.stageName='2. Define';
        opp14.closedate=System.today()+10;
        opp14.ProductionTCV__c = 6000000;
    opp14.Deal_Type__c='Competitive';
        opp14.Transformation_stale_deal_timestamp__c=null;
        opp14.ownerid=u.id;
        opp14.Previous_Stage__c='';
      //  opp14.QSRM_value_not_approved_rejected__c=0;
        opp14.Last_stage_change_date__c=NULL;
       // opp14.Insight__c=40;
        opp14.Revenue_Start_Date__c=system.today();
 updateopplist.add(opp14);
  

opp15.Nature_of_Work_highest__c='Managed';
  //      opp15.Highest_Revenue_Product__c='Consulting';
        opp15.Amount=10000;
        opp15.stageName='2. Define';
  //  opp15.QSRM_value_not_approved_rejected__c!=0;
        opp15.closedate=System.today()+10;
        opp15.ProductionTCV__c = 50000000;
        opp15.Transformation_stale_deal_timestamp__c=null;
        opp15.ownerid=u.id;
        opp15.Previous_Stage__c='';
        opp15.Deal_Type__c='Sole Sourced';
        opp15.Last_stage_change_date__c=NULL;
      //  opp15.Insight__c=30;
        opp15.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp15);

    
opp16.Nature_of_Work_highest__c='Managed';
   //     opp16.Highest_Revenue_Product__c='Consulting';
        opp16.Amount=10000;
        opp16.stageName='2. Define';
        opp16.closedate=System.today()+30;
        opp16.ProductionTCV__c = 4000000;
        opp16.Transformation_stale_deal_timestamp__c=null;
        opp16.ownerid=u.id;
        opp16.Previous_Stage__c='';
        opp16.Deal_Type__c='Sole Sourced';
        opp16.Last_stage_change_date__c=NULL;
      //  opp16.Insight__c=20;
  //  opp16.QSRM_value_not_approved_rejected__c=0;
        opp16.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp16);

    opp17.Nature_of_Work_highest__c='Managed';
  //      opp17.Highest_Revenue_Product__c='Consulting';
        opp17.Amount=10000;
        opp17.stageName='2. Define';
        opp17.closedate=System.today()+30;
        opp17.ProductionTCV__c = 40000;
        opp17.Transformation_stale_deal_timestamp__c=null;
        opp17.ownerid=u.id;
        opp17.Previous_Stage__c='';
        opp17.Deal_Type__c='Sole Sourced';
        opp17.Last_stage_change_date__c=NULL;
      //  opp17.Insight__c=20;
  //  opp17.QSRM_value_not_approved_rejected__c=0;
        opp17.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp17);

          
          opp18.Nature_of_Work_highest__c='Managed';
  //      opp18.Highest_Revenue_Product__c='Consulting';
        opp18.Amount=10000;
        opp18.stageName='2. Define';
        opp18.closedate=System.today()+30;
        opp18.ProductionTCV__c = 4000000;
        opp18.Transformation_stale_deal_timestamp__c=null;
        opp18.ownerid=u.id;
        opp18.Previous_Stage__c='';
        opp18.Deal_Type__c='Competitive';
        opp18.Last_stage_change_date__c=NULL;
      //  opp18.Insight__c=20;
  //  opp18.QSRM_value_not_approved_rejected__c=0;
        opp18.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp18);
          
          
        opp19.Nature_of_Work_highest__c='Managed';
  //      opp19.Highest_Revenue_Product__c='Consulting';
        opp19.Amount=10000;
    //opp19.QSRM_value_not_approved_rejected__c=0;
        opp19.stageName='2. Define';
        opp19.closedate=System.today()+30;
        opp19.ProductionTCV__c = 100000;
        opp19.Transformation_stale_deal_timestamp__c=null;
        opp19.ownerid=u.id;
        opp19.Previous_Stage__c='';
        opp19.Deal_Type__c='Competitive';
        opp19.Last_stage_change_date__c=NULL;
        //opp19.Insight__c=30;
        opp19.Revenue_Start_Date__c=system.today();
      
     
                     updateopplist.add(opp19);
    
          
    opp20.Nature_of_Work_highest__c='Managed';
  //      opp20.Highest_Revenue_Product__c='Consulting';
        opp20.Amount=10000;
        opp20.stageName='2. Define';
        opp20.closedate=System.today()+10;
        opp20.ProductionTCV__c = 6000000;
    opp20.Deal_Type__c='Competitive';
        opp20.Transformation_stale_deal_timestamp__c=null;
        opp20.ownerid=u.id;
        opp20.Previous_Stage__c='';
      //  opp20.QSRM_value_not_approved_rejected__c=0;
        opp20.Last_stage_change_date__c=NULL;
       // opp20.Insight__c=40;
        opp20.Revenue_Start_Date__c=system.today();
 updateopplist.add(opp20);
  

opp21.Nature_of_Work_highest__c='Managed Services';
   //     opp21.Highest_Revenue_Product__c='Consulting';
        opp21.Amount=10000;
        opp21.stageName='2. Define';
  //  opp21.QSRM_value_not_approved_rejected__c!=0;
        opp21.closedate=System.today()+10;
        opp21.ProductionTCV__c = 50000000;
        opp21.Transformation_stale_deal_timestamp__c=null;
        opp21.ownerid=u.id;
        opp21.Previous_Stage__c='';
        opp21.Deal_Type__c='Sole Sourced';
        opp21.Last_stage_change_date__c=NULL;
      //  opp21.Insight__c=30;
        opp21.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp21);

              
opp22.Nature_of_Work_highest__c='Managed Services';
   //     opp22.Highest_Revenue_Product__c='Consulting';
        opp22.Amount=10000;
        opp22.stageName='2. Define';
        opp22.closedate=System.today()+30;
        opp22.ProductionTCV__c = 4000000;
        opp22.Transformation_stale_deal_timestamp__c=null;
        opp22.ownerid=u.id;
        opp22.Previous_Stage__c='';
        opp22.Deal_Type__c='Sole Sourced';
        opp22.Last_stage_change_date__c=NULL;
      //  opp22.Insight__c=20;
  //  opp22.QSRM_value_not_approved_rejected__c=0;
        opp22.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp22);


    opp23.Nature_of_Work_highest__c='Managed Services';
   //     opp23.Highest_Revenue_Product__c='Consulting';
        opp23.Amount=10000;
        opp23.stageName='2. Define';
        opp23.closedate=System.today()+30;
        opp23.ProductionTCV__c = 40000;
        opp23.Transformation_stale_deal_timestamp__c=null;
        opp23.ownerid=u.id;
        opp23.Previous_Stage__c='';
        opp23.Deal_Type__c='Sole Sourced';
        opp23.Last_stage_change_date__c=NULL;
      //  opp23.Insight__c=20;
  //  opp23.QSRM_value_not_approved_rejected__c=0;
        opp23.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp23);

          

          opp24.Nature_of_Work_highest__c='Managed Services';
   //     opp24.Highest_Revenue_Product__c='Consulting';
        opp24.Amount=10000;
        opp24.stageName='2. Define';
        opp24.closedate=System.today()+30;
        opp24.ProductionTCV__c = 4000000;
        opp24.Transformation_stale_deal_timestamp__c=null;
        opp24.ownerid=u.id;
        opp24.Previous_Stage__c='';
        opp24.Deal_Type__c='Competitive';
        opp24.Last_stage_change_date__c=NULL;
      //  opp24.Insight__c=20;
  //  opp24.QSRM_value_not_approved_rejected__c=0;
        opp24.Revenue_Start_Date__c=system.today();

                    updateopplist.add(opp24);
  


        update updateopplist;
         Set<id> Oppidset = new Set<id>();
        for(opportunity o : updateopplist)
        {
            Oppidset.add(o.id);
        }
        
 
        system.runas(u)
        {
            //Restricted_Access_stale_deal  controller = new Restricted_Access_stale_deal ();
            test.startTest();
            
            identifyStaleUsers.chkSF1();
    //identifyStaleUsers.getStaleCount();
        identifyStaleUsers.getNonTsStaleCount();
        //identifyStaleUsers.getStalledCount();
        identifyStaleUsers.getNonTsStalledCount();
    identifyStaleUsers.chkProfile();
            test.stopTest();
        }
    
    

}

    static TestMethod void Test4()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );

        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);   
        oOpportunity.OwnerId = u.id;
        oOpportunity.CloseDate = System.today().addDays(2);
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(2);
        update oOpportunity;
        Test.startTest();
    
      identifyStaleUsers.chkSF1();
    identifyStaleUsers.getStaleCount();
        identifyStaleUsers.getNonTsStaleCount();
        identifyStaleUsers.getStalledCount();
        identifyStaleUsers.getNonTsStalledCount();
    identifyStaleUsers.chkProfile();
        Test.stopTest();
        
       
    }

    
    */
    }