/*
    @Discription: Class will be used for handling Master VIC Role agianst User
    @Test Class: UserPlanComponentCtlrTest, Possibly will capture by "VICOpportunityLineItemTriggerHandler" class
    @Author: Vikas Rajput
*/
public with sharing class VIC_UserPlanComponentCtlr{
    /*
        @Discription: function will be used to fetch Master VIC Role based on user id(Owner, Primary Bd Rep).
        @Param: List of user Id
        @Return: Map<Id,User_VIC_Role__c>
        @Author: Vikas Rajput
    */
    public Map<Id,User_VIC_Role__c> fetchMasterVicRoleBySetUserId(Set<Id> setUserIDARG){
        Map<Id,User_VIC_Role__c> mapUserIdTOUserVICRoleLCL = new Map<Id,User_VIC_Role__c>();
        for(User_VIC_Role__c objUserVICITR :[select id,vic_For_Previous_Year__c,Master_VIC_Role__c,User__c,Unique_User__c,
                                             Not_Applicable_for_VIC__c,Master_VIC_Role__r.Is_Active__c,Master_VIC_Role__r.Role__c,
                                             Master_VIC_Role__r.Capture_User_Local_Currency__c,Master_VIC_Role__r.Horizontal__c 
                                             from User_VIC_Role__c where User__c IN :setUserIDARG AND User__r.IsActive =:true AND 
                                             Not_Applicable_for_VIC__c =:false AND vic_For_Previous_Year__c =:false]){
            mapUserIdTOUserVICRoleLCL.put(objUserVICITR.User__c, objUserVICITR);
        }
        return mapUserIdTOUserVICRoleLCL;
    }

}