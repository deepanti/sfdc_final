/**
 * @group ServiceLayer. 
 *
 * @description Service layer for Project Expense.
 */
public with sharing class GPServiceProjectExpense {

    private static final String EXPENSE_MANDATORY_ERROR_LABEL = 'Expense Category/Expense Type/Location can not be empty';
    private static final String EXPENSE_UNIQUE_ERROR_LABEL = 'Expense Category/Expense Type/Location can not be duplicate';
    private static final String AMOUNT_ERROR_LABEL = 'Expense amount can not be less than/equal to 0';

    public static Boolean isFormattedLogRequired = true;
    public static Boolean isLogForTemporaryId = true;
    public static GP_Project__c project;


    public static string UpdateProjectExpense(string projectID, list < GP_Project_Expense__c > lstofOmsProjectExpense) {
        map < string, GP_Project_Expense__c > mapofOmsIDToPB = new map < string, GP_Project_Expense__c > ();
        map < string, GP_Project_Expense__c > mapofOmsIDofSychToPE = new map < string, GP_Project_Expense__c > ();
        list < GP_Project_Expense__c > lstofProjectExpense = new list < GP_Project_Expense__c > ();
        list < GP_Project_Expense__c > lstofProjExpforSoftDel = new list < GP_Project_Expense__c > ();

        if (lstofOmsProjectExpense != null && lstofOmsProjectExpense.size() > 0) {
            for (GP_Project_Expense__c objEX: lstofOmsProjectExpense) {
                mapofOmsIDofSychToPE.put(objEX.GP_OMS_Id__c, objEX);
            }
        }


        for (GP_Project_Expense__c objEX: new GPSelectorProjectExpense().selectProjectExpenseRecords(projectID))

        {
            mapofOmsIDToPB.put(objEX.GP_OMS_Id__c, objEX);
            if (!mapofOmsIDofSychToPE.containsKey(objEX.GP_OMS_Id__c)) {
                objEX.GP_IsSoftDeleted__c = true;
                lstofProjExpforSoftDel.add(objEX);
            }
        }


        if (lstofOmsProjectExpense != null && lstofOmsProjectExpense.size() > 0) {
            for (GP_Project_Expense__c objEX: lstofOmsProjectExpense) {
                GP_Project_Expense__c obj;
                if (mapofOmsIDToPB != null && mapofOmsIDToPB.size() > 0 && mapofOmsIDToPB.containsKey(objEX.GP_OMS_Id__c)) {
                    obj = new GP_Project_Expense__c(Id = mapofOmsIDToPB.get(objEX.GP_OMS_Id__c).Id);
                    obj.GP_Amount__c = objEX.GP_Amount__c;
                    obj.GP_Data_Source__c = objEX.GP_Data_Source__c;
                    obj.GP_Expenditure_Type__c = objEX.GP_Expenditure_Type__c;
                    obj.GP_Expense_Category__c = objEX.GP_Expense_Category__c;
                    obj.GP_OMS_ID__c = objEX.GP_OMS_ID__c;
                    obj.GP_Remark__c = objEX.GP_Remark__c;
                    lstofProjectExpense.add(obj);
                } else {
                    obj = new GP_Project_Expense__c();
                    obj.GP_Amount__c = objEX.GP_Amount__c;
                    obj.GP_Data_Source__c = objEX.GP_Data_Source__c;
                    obj.GP_Expenditure_Type__c = objEX.GP_Expenditure_Type__c;
                    obj.GP_Expense_Category__c = objEX.GP_Expense_Category__c;
                    obj.GP_OMS_ID__c = objEX.GP_OMS_ID__c;
                    obj.GP_Remark__c = objEX.GP_Remark__c;
                    obj.GP_Project__c = projectID;
                    lstofProjectExpense.add(obj);
                }
            }
        }
        if (lstofProjectExpense != null && lstofProjectExpense.size() > 0) {
            upsert lstofProjectExpense;
        }

        if (lstofProjExpforSoftDel != null && lstofProjExpforSoftDel.size() > 0) {
            update lstofProjExpforSoftDel;
        }

        return 'Suceess';
    }

    /**
     * @description Validate Project Expense.
     * 
     * @param listOfProjectExpense : List of Project Expense that needs to be validated.
     *
     * @return Map < String, List < String >> : Return Map of Sub Section with List of errors.
     */
    public static Map < String, List < String >> validateProjectExpense(List < GP_Project_Expense__c > listOfProjectExpense) {
        Map < String, Boolean > mapOfUniqueCombination = new Map < String, Boolean > ();
        Map < String, List < String >> mapOfSubSectionToListOfErrors = new Map < String, List < String >> ();

        String validationMessage = '';
        String key;

        if (listOfProjectExpense != null && listOfProjectExpense.size() > 0) {
            for (GP_Project_Expense__c projectExpense: listOfProjectExpense) {
                if (isFormattedLogRequired) {
                    key = 'Project Expense';
                } else if (isLogForTemporaryId) {
                    key = projectExpense.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = projectExpense.Id;
                }

                String uniqueCombination = projectExpense.GP_Expense_Category__c + '-';
                uniqueCombination += projectExpense.GP_Expenditure_Type__c + '-';
                uniqueCombination += projectExpense.GP_Location__c;

                // check for uniqueness
         /*       if (!mapOfUniqueCombination.containsKey(uniqueCombination)) {
                    mapOfUniqueCombination.put(uniqueCombination, true);
                } else {
                    validationMessage = projectExpense.Name + ': ' + EXPENSE_UNIQUE_ERROR_LABEL;

                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }
			*/
                //validate for required fields
                if (!validateEachExpense(projectExpense)) {
                    validationMessage = projectExpense.Name + ': ' + EXPENSE_MANDATORY_ERROR_LABEL;
                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }

                //validate if amount is less than equal to 0
                if (projectExpense.GP_Data_Source__c == 'Pinnacle' && projectExpense.GP_Amount__c <= 0) {
                    validationMessage = projectExpense.Name + ': ' + AMOUNT_ERROR_LABEL;

                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }
            }
        }

        return mapOfSubSectionToListOfErrors;
    }

    /**
     * @description Validate Each Project Expense.
     * 
     * @param projectExpense : Project Expense Record that needs to be validated.
     *
     * @return Boolean : Return Required fields expense record.
     */
    private static Boolean validateEachExpense(GP_Project_Expense__c projectExpense) {
        return (projectExpense != null &&
            projectExpense.GP_Expense_Category__c != null &&
            projectExpense.GP_Expenditure_Type__c != null &&
            projectExpense.GP_Location__c != null &&
            projectExpense.GP_Amount__c != null);
    }

    public class GPServiceProjectExpenseException extends Exception {}

    public static List < GP_Project_Expense__c > expenseAmountConversion(List < GP_Project_Expense__c > lstofProjectExpense,Map < Id, GP_Project__c > mapOfProject,
                        Map < Id, GP_Project__c > mapOfOldProject ) {
        Set < String > currencyISOCode = new Set < String > ();
        Set < Id > projectIds = new Set < Id > ();
        for (GP_Project_Expense__c projectExpense: lstofProjectExpense) {
            if (projectExpense.CurrencyIsoCode != null) {
                projectIds.add( projectExpense.GP_Project__c);
       		 }
        }

        if (projectIds.size() > 0) {
            if(mapOfProject == null)
                mapOfProject = new Map < Id, GP_Project__c > ([select Id, CurrencyIsoCode from GP_Project__c where Id in: projectIds]);
			
            for(GP_Project__c objProject : mapOfProject.values()){
                 currencyISOCode.add(objProject.CurrencyIsoCode);
            }
            if(mapOfOldProject != null){
                for(GP_Project__c objProject : mapOfOldProject.values()){ 
                     currencyISOCode.add(objProject.CurrencyIsoCode);
                }
            }
            Map < String, Decimal > isoWithRateMap = new Map < String, Decimal > ();

            List < CurrencyType > currencyTypeList = [select id, IsoCode, ConversionRate from CurrencyType where
                isActive = true and IsoCode in: currencyISOCode
            ];

            for (CurrencyType objcurrency: currencyTypeList) {
                isoWithRateMap.put(objcurrency.IsoCode, objcurrency.ConversionRate);
            }

            for (GP_Project_Expense__c projectExpense: lstofProjectExpense) {
                if (mapOfProject.containsKey(projectExpense.GP_Project__c) && isoWithRateMap.containsKey(mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode) && projectExpense.GP_Amount__c != null) {
                    //projectExpense.GP_Amount__c = projectExpense.GP_Amount__c * isoWithRateMap.get(mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode);
                    if(mapOfOldProject == null){
                        if(mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode != projectExpense.CurrencyIsoCode ){
                        	projectExpense.GP_Amount__c = convertCurrency('USD',mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode,isoWithRateMap, projectExpense.GP_Amount__c);
                            projectExpense.currencyIsoCOde=  mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode;
                        }
                    }
                    else{
                        projectExpense.GP_Amount__c = convertCurrency(mapOfOldProject.get(projectExpense.GP_Project__c).CurrencyIsoCode,mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode,isoWithRateMap, projectExpense.GP_Amount__c);
                        projectExpense.currencyIsoCOde=  mapOfProject.get(projectExpense.GP_Project__c).CurrencyIsoCode;
                    }
                }
            }
        }
        return lstofProjectExpense;
    }

    public static  Decimal convertCurrency(string currentCurrency, string convertedCurrency,  Map < String, Decimal > isoWithRateMap , Decimal Value){

       if(Value > 0){
        if( currentCurrency =='USD' ){
            if(isoWithRateMap.containsKey(convertedCurrency) != null ){
                return (Value * isoWithRateMap.get(convertedCurrency)).setscale(2);
            }
            else{
                return Value;
            }
        }
        else
        {
            Decimal USDValue = 0; 
            if(isoWithRateMap.containsKey(currentCurrency) != null){
                USDValue = Value / isoWithRateMap.get(currentCurrency); 

                return  isoWithRateMap.get(convertedCurrency) != null? ( USDValue * isoWithRateMap.get(convertedCurrency)).setscale(2) : Value ;
            }
            return Value;
        }
       }
       return 0;
    }
}