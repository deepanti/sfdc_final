public with sharing class GPServiceEmployeeMaster {
    @future
    public static void inActivateUser(set<Id> setOfId)
    {
        //system.debug('@@@Methodcalll');
        GPSelectorEmployeeMaster selectorEmpMaster = new GPSelectorEmployeeMaster();        
        if(Setofid != null && Setofid.size() >0)
        {
            //list<id> listofUserId = new list<id>();
            list<user> listofUserId = new list<user>( selectorEmpMaster.getUserByEmployeeId(Setofid));
            if(listofUserId != null && listofUserId.size() > 0)
            {
                for(user objUser : listofUserId)
                {
                    objUser.isActive = False;
                    //listofUserId.add(objemployee.GP_SFDC_User__c);
                }
                system.debug('listofUserId______'+listofUserId);
                update listofUserId;
                // uow.registerDirty(lstOfemployeeWithActiveUser);
            }
        }
    }  
    
    
    public static map<string,string> getMapOHRtoPersonId(set<string> setOHRID)
    {
        //system.debug('@@@Methodcalll');
        map<String,String> mapOHRtoPersonId = new map<String,String>();       
        if(setOHRID != null && setOHRID.size() >0)
        {
        	for( GP_Employee_Master__c objEMPMaster : new GPSelectorEmployeeMaster().getEmployeeListForOHRId(setOHRID)){
        		mapOHRtoPersonId.put(objEMPMaster.GP_Final_OHR__c, objEMPMaster.GP_Person_id__c );
        	}
        	
        }
        return mapOHRtoPersonId;
    }
    
      @future
    public static void ActivateUser(set<Id> setOfId)
    {
       // system.debug('@@@Methodcalll');
        GPSelectorEmployeeMaster selectorEmpMaster = new GPSelectorEmployeeMaster();        
        if(Setofid != null && Setofid.size() >0)
        {
            //list<id> listofUserId = new list<id>();
            list<user> listofUserId = new list<user>( selectorEmpMaster.getUserByEmployeeId(Setofid));
            map<Id,String> mapOfEmployeeIdVsPersonId = new Map<Id,String>();
            for(GP_Employee_Master__c objEmployeeMaster : selectorEmpMaster.selectActiveEmployeeWithSFDCUserIds(Setofid))
            {
                mapOfEmployeeIdVsPersonId.put(objEmployeeMaster.GP_SFDC_User__c,objEmployeeMaster.GP_Person_ID__c);
            }
            if(listofUserId != null && listofUserId.size() > 0)
            {
                for(user objUser : listofUserId)
                {
                    objUser.isActive = true;
                    objUser.GP_Person_Id__c = mapOfEmployeeIdVsPersonId.containsKey(objUser.Id) ? mapOfEmployeeIdVsPersonId.get(objUser.Id) : '';
                    //listofUserId.add(objemployee.GP_SFDC_User__c);
                }
                system.debug('listofUserId______'+listofUserId);
                update listofUserId;
                // uow.registerDirty(lstOfemployeeWithActiveUser);
            }
        }
    }  
    public class GPServiceEmployeeMasterException extends Exception {} 
}