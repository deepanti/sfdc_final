public class ContractMessage {
    
    @AuraEnabled
    public static List <Opportunity> chkContract(String parentId) {
        
       return [select id,StageName,Contract_Status__c,QSRM_Status__c from Opportunity where id =: parentId LIMIT 1 ];
       
    }
    
    @AuraEnabled
    public static List <Contract> chkAttach(String conparentId) {
        
       return [select id,Number_of_Attachments__c from Contract where id =: conparentId LIMIT 1 ];
       
    }
}