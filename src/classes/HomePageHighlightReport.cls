/*************************************************************************************************************************
* @name : HomePageHighlightReport
* @author : Persistent
* @description  : Used by home page component, highlight panel metric (HomePageHighlightPanelLineItem)
**************************************************************************************************************************/
public class HomePageHighlightReport {
    /*************************************************************************************************************************
* @name : getReportData
* @author : Persistent
* @description  : To get the report data for highlight panel metrics on home page
* @param  : reportId - the report to run
* @return : The grand summary value from the report results in decimal
**************************************************************************************************************************/
    @AuraEnabled
    public static List<ReportFormattedData> getReportData(Id reportId, Boolean showGrandSummaryOnly){
        try{
            //Get the report
            Report reportRec = [SELECT Id FROM Report WHERE Id = :reportId limit 1];
            //Run reprot synchrnously
            Reports.ReportResults reportResult = Reports.ReportManager.runReport(reportRec.Id, false);
            Map<String,Reports.ReportFact> factMap = reportResult.getFactMap();
            ReportFormattedData rfdInstance;
            List<ReportFormattedData> dataToReturn = new List<ReportFormattedData>();
            if(!showGrandSummaryOnly)
            {
                //Process results for non grand summary
                List<Reports.GroupingValue> groupings =reportResult.getGroupingsDown().getGroupings();
                for(Reports.GroupingValue grouping : groupings)
                {
                    //Add instance of wrapper
                    // 'K' Logic;
                    //Integer kValue = Math.round((Decimal)factMap.get(grouping.getKey()+'!T').getAggregates()[0].getValue())/1000;
                    // 'M' Logic;
                    //Integer kValue = Math.round((Decimal)factMap.get(grouping.getKey()+'!T').getAggregates()[0].getValue());
                    Long kValue = Math.roundToLong((Decimal)factMap.get(grouping.getKey()+'!T').getAggregates()[0].getValue());
                    rfdInstance = new ReportFormattedData(grouping.getLabel(),kValue,false);
                    dataToReturn.add(rfdInstance);
                }
            }
            
            //Process grand summary
            if((Decimal)factMap.get('T!T').getAggregates()[0].getValue()==null)
                rfdInstance = new ReportFormattedData('T!T',0,true);
            else
            	rfdInstance = new ReportFormattedData('T!T',Math.roundToLong((Double)factMap.get('T!T').getAggregates()[0].getValue()),true);
            dataToReturn.add(rfdInstance);
            //Return wrapper list
            return dataToReturn;
        }
        catch(Exception e)
        {throw new AuraHandledException('Something is wrong with the report contact system admin');
        }
    }
    
    public class ReportFormattedData
    {
        @AuraEnabled public string Label;
        @AuraEnabled public Long Value;
        @AuraEnabled public Boolean isGrandSummary;
        
        public ReportFormattedData(String label, Long value, Boolean isGrandSummary)
        {
            this.Label = label;
            this.Value = value;
            this.isGrandSummary = isGrandSummary;
        }
    }
}