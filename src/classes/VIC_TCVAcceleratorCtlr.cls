/*
    @Description: Class will be used for handling all Opportunity Line Item which are TCV calculation
    @Test Class: TCVAcceleratorCtlrTest
    @Author: Vikas Rajput
    @Required Input: totalTCVAmount, percentOP, percentPM, isEnableIO = true for IO, isEnableIO = true for TS, mapValueToObjIncentiveConst value for VIC_CommonUtil
*/
public class VIC_TCVAcceleratorCtlr implements VIC_ScorecardCalculationInterface{
    public Boolean isEnableIO{get;set;} //If it is false then it will be considered for TS calculation     
    public Decimal percentOP{get;set;}
    public Decimal percentPM{get;set;}
    public Decimal vicGrid{get;set;}
    public Decimal totalTCVAmount{get;set;} //Its value depend on type of TCV such as IO or TS
    //public Target_Achievement__c objTargetIncentive{get;set;}
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    String strCumulativeIOTCVForBDE = 'Cumulative_IO_TCV_For_BDE_Plan';
    String strCumulativeIOTCVForEnterpriseGRM = 'Cumulative_IO_TCV_For_EnterpriseGRM';
    String strCumulativeIOTCVForITGRM = 'Cumulative_IO_TCV_For_IT_GRM';
    String strOPTCVPercent = 'OP_TCV_Accelerator';
    String strPMTCVPercent = 'PM_TCV_Accelerator';
    String strBDETCV = 'BDE_TCV_Accelerator_Label';
    String strCapitalMarketGRMTCV = 'Capital_Market_GRM_TCV_Accelerator';
    String strEnterpriseGRMTCV = 'EnterpriseGRM_TCV_Accelerator';
    String strITGRMTCV = 'IT_GRM_TCV_Accelerator';
    String strNewIncentiveType = 'New';
    String strAdjuctIncentiveType = 'Adjustment';
    String strDisbursed = 'Disbursed';
    
    public VIC_TCVAcceleratorCtlr(){
        isEnableIO = true;  // if true then logic will be executed for TCV IO by default
        totalTCVAmount = 0.00; // Initiating tcv amount value default to 0.00, 
    }
    
    /*  
        @Description: It will calculate (TCV IO)/(TCV TS) after applying matrix based on local field isEnableIO
        @Param: Target_Component__c objComponentARG, String objCalcMatrixItemIdARG()
        @Author: Vikas Rajput
        @Note: We can use configuration setting here for strong TCV IO/TCV TS check value and label, Need to discuss with @Rishabh
    */
    public Decimal calculate(Target_Component__c objComponentARG, List<VIC_Calculation_Matrix_Item__c> lstMatrixItem){
         Decimal dcmValue = totalTCVAmount; //isEnableIO = true then totalTCVAmount will be sum of tcv io type
         if(dcmValue != null && dcmValue != 0){
             if(isEnableIO){
                 System.debug('===================TCV_ACC_IO====================='+objComponentARG);
                 System.debug('===================TCV_ACC_IO=====================lstMatrixItem'+lstMatrixItem);
                 System.debug('===================TCV_ACC_IO=====================lstMatrixItem_SIZE'+lstMatrixItem.Size());
                 return calculateTCVIO(objComponentARG, lstMatrixItem, dcmValue);
             }else{
                 System.debug('===================TCV_ACC_TS====================='+objComponentARG);
                 System.debug('===================TCV_ACC_TS=====================lstMatrixItem'+lstMatrixItem);
                 System.debug('===================TCV_ACC_TS=====================lstMatrixItem_SIZE'+lstMatrixItem.Size());
                 return calculateTCVTS(objComponentARG, lstMatrixItem, dcmValue);
             }
         }
         return null;
    }
    /*   
         @CalculationType: TCV IO
         @PrimaryCheck: Target Component should not be null
                         Target of Target Component should be mapped
                          Plan of Target should be mapped
         @Description: After verifying primary check(above) then it will check for plan type
         @Author: Vikas Rajput
    */
    public Decimal calculateTCVIO(Target_Component__c objComponentARG, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem, Decimal dcmValue){
        if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strBDETCV).vic_Value__c && 
                 mapValueToObjIncentiveConst.containsKey(strCumulativeIOTCVForBDE) &&
                     dcmValue > Integer.valueOf(mapValueToObjIncentiveConst.get(strCumulativeIOTCVForBDE).vic_Value__c)){ //1000000 now picking from METADATA
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "BDE" plan 
         
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && percentPM > Integer.valueOf(mapValueToObjIncentiveConst.get(strPMTCVPercent).vic_Value__c) && 
                 objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strCapitalMarketGRMTCV).vic_Value__c){
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "Capital_Market_GRM" plan 
         
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && mapValueToObjIncentiveConst.containsKey(strCumulativeIOTCVForEnterpriseGRM) && 
                 dcmValue > Integer.valueOf(mapValueToObjIncentiveConst.get(strCumulativeIOTCVForEnterpriseGRM).vic_Value__c) &&
                     objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strEnterpriseGRMTCV).vic_Value__c){ //1000000 now picking from METADATA
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "EnterpriseGRM" plan 
         
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && 
                 percentPM > Integer.valueOf(mapValueToObjIncentiveConst.get(strPMTCVPercent).vic_Value__c) && 
                     mapValueToObjIncentiveConst.containsKey(strCumulativeIOTCVForITGRM) && 
                         dcmValue > Integer.valueOf(mapValueToObjIncentiveConst.get(strCumulativeIOTCVForITGRM).vic_Value__c) &&
                             objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strITGRMTCV).vic_Value__c){ //1000000 now picking from METADATA            
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "IT_GRM" plan
         }
         return 0.00;
    }                                          
    /*                                         
         @CalculationType: TCV TS
         @PrimaryCheck: Target Component should not be null
                         Target of Target Component should be mapped
                          Plan of Target should be mapped
         @Description: After verifying primary check(above) then it will check for plan type
         @Author: Vikas Rajput
    */
    public Decimal calculateTCVTS(Target_Component__c objComponentARG, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem, Decimal dcmValue){
        if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strBDETCV).vic_Value__c ){ //Getting plan from metadata
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "BDE" plan 
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && 
                 percentPM > Integer.valueOf(mapValueToObjIncentiveConst.get(strPMTCVPercent).vic_Value__c) && 
                     objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strCapitalMarketGRMTCV).vic_Value__c){
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "Capital_Market_GRM" plan 
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && 
                 objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strEnterpriseGRMTCV).vic_Value__c){ 
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "EnterpriseGRM" plan 
         }else if(objComponentARG != null && objComponentARG.Target__r.Plan__c != null &&  
             percentOP > Integer.valueOf(mapValueToObjIncentiveConst.get(strOPTCVPercent).vic_Value__c) && 
                 percentPM > Integer.valueOf(mapValueToObjIncentiveConst.get(strPMTCVPercent).vic_Value__c) && 
                     objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == mapValueToObjIncentiveConst.get(strITGRMTCV).vic_Value__c){
             //Decimal itGRMTCV = objComponentARG.Target__r.vic_Total_TCV__c;
             return calcualteTCVIOTS(objComponentARG, lstCalcMatrixItem, dcmValue); //@Returning calcuated TCV IO/TS value for "IT_GRM" plan
         }
         return 0.00;
    }
    
    /*
        @Description: Calculate TCV IO and TS based on received payout and calculation matrix.
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal calcualteTCVIOTS(Target_Component__c objComponentARG, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem, Decimal totalAchievment){
         VIC_Calculation_Matrix_Item__c oldMatrixItem; 
         Decimal calcAmount = 0.00;
         for(VIC_Calculation_Matrix_Item__c item : lstCalcMatrixItem){
            if((totalAchievment > item.vic_Start_Value__c && totalAchievment <= item.vic_End_Value__c) 
                || (totalAchievment > item.vic_Start_Value__c && item.vic_End_Value__c == null)
                    || (totalAchievment == item.vic_Start_Value__c && totalAchievment == item.vic_End_Value__c)){   
               if(item.vic_End_Value__c == null && item.vic_Start_Value__c != null && oldMatrixItem.vic_Payout__c != null && item.vic_Payout__c != null){
                   vicGrid = item.vic_Payout__c;
                   calcAmount = (item.vic_Start_Value__c*oldMatrixItem.vic_Payout__c) + (totalAchievment - item.vic_Start_Value__c)*item.vic_Payout__c;
               }else if(item.vic_End_Value__c != null && item.vic_Start_Value__c != null && item.vic_Payout__c != null){
                   vicGrid = item.vic_Payout__c;
                   calcAmount = totalAchievment*item.vic_Payout__c;
               }
               break;
            }
            oldMatrixItem = item;
         }
         return calcAmount/100;
         
    }
}