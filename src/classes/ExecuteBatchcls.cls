/*   ==============================================================
    Developer       :   Arjun Srivastava
    Client          :   Genpact
    Date            :   24/June/2012
    Description     :   This Controller is used in the scheduler for the quota attainment.
====================================================================== */
public class ExecuteBatchcls
{
    public static void ExecuteBatchmeth()
    {    
        /*      
        QuotaBatchclass reassign = new QuotaBatchclass('');        
        
        ID batchprocessid = Database.executeBatch(reassign,1);    // running a batch with batch size 1
        */        
        QuotaDeleteBatchclass reassign = new QuotaDeleteBatchclass('');        
       
        ID batchprocessid = Database.executeBatch(reassign,200);   // running a batch with batch size 200
    }
}