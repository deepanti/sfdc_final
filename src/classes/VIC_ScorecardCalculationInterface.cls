/*
 * @author: Rishabh Pilani
 * @since: 22/05/2018
 * @description: This interface is used in classes related to scorecard calculation
 * 
*/
public interface VIC_ScorecardCalculationInterface {
    Decimal calculate(Target_Component__c objComponent, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem);
}