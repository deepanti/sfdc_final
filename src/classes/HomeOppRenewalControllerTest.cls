@isTest 
public class HomeOppRenewalControllerTest {
    @isTest static void  testfunctions()
    {
        
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        List<Opportunity>oppList=TestDataFactoryUtility.createTestOpportunitiesRecords(acc.Id, 2, 'Prediscover', system.today().addMonths(12));
        insert oppList;
        Contact_Role_Rv__c conRoleCreate=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        conRoleCreate.IsPrimary__c	= True;
        conRoleCreate.Role__c='Decision Maker';
        insert conRoleCreate;
        Contact_Role_Rv__c conRoleCreate2=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        conRoleCreate2.Role__c='Decision Maker';
        insert conRoleCreate2;
        Contact_Role_Rv__c conRoleDelete=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        conRoleDelete.Role__c='Decision Maker';
        insert conRoleDelete;
        List<Product2> pList = TestDataFactoryUtility.createTestProducts2(1);
        insert pList;
        system.debug('pList'+ pList);
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000,  
            IsActive = true);
        insert standardPrice;
        List<OpportunityLineItem> oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList[0].Id,standardPrice.id);
        insert oppLineItem;
        Test.startTest();
        //check for fetchListOpportunity ,whether is empty or not 
        system.assertNotEquals(NULL, HomeOppRenewalController.fetchListOpportunity(5));
        String oppId0=oppList[0].id;
        HomeOppRenewalController.updateHideForRenewal(oppId0);
        //Opportunity oppRecord0=[select Hide_For_Renewal__c from Opportunity where id=:oppId0];
        //system.assertEquals(true, oppRecord0.Hide_For_Renewal__c);
        
        //check for updateOpportunityStatusToDiscover ,whether Discover field is properly set or not 
        String oppId1=oppList[1].id;
        /*HomeOppRenewalController.updateOpportunityStatusToDiscover(oppId1);
        Opportunity oppRecord1=[select StageName from Opportunity where id=:oppId1];
        system.assertEquals('1. Discover', oppRecord1.StageName);*/
        HomeOppRenewalController.createDeepCloneOpportunity(oppId0,false,'1. Discover');
        //HomeOppRenewalController.createDeepCloneOpportunity(oppId0,True,'1. Discover');
        Test.stopTest(); 
    }
   
}