/**
 * @author Anmol.kumar
 * @date 13/09/2017
 *
 * @group ProjectClone
 * @group-content ../../ApexDocContent/ProjectClone.htm
 *
 * @description Apex Service for ProjectClone module,
 * has aura enabled method to clone project.
 */
public class GPCmpServiceProjectClone {
    /**
     * @description Returns cloned project Id
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse cloned project Id
     * 
     * @example
     * GPCmpServiceProjectClone.cloneProject(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static string cloneProject(string strProjectId) {
        GPCheckRecursive.mapProjectSkipBudgetCheck.put(strProjectId, true);
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new Set < Id > { strProjectId }, 'Manual', false, false);
        string strResponse = projectCloneController.cloneProject();
        GPCheckRecursive.mapProjectSkipBudgetCheck.put(strProjectId, false);

        return strResponse;
    }

    @AuraEnabled
    public static GPAuraResponse getChildProjectRecord(string strProjectId) {
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone();
        return projectCloneController.getChildProjectRecord(strProjectId);
    }
}