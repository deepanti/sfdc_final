public class GPSelectorProjectAddress extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Project_Address__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Project_Address__c.sObjectType;
    }
    
    public List<GP_Project_Address__c> selectById(Set<ID> idSet) {
        return (List<GP_Project_Address__c>) selectSObjectsById(idSet); 
    }
    
    public List<GP_Project_Address__c> selectProjectAddressRecords(Set<Id> lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Project_Address__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery,MapOfSchema);
        strQuery += ' from GP_Project_Address__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }   

    public List<GP_Project_Address__c> selectProjectAddressUnderProject(Id projectId) {
        return [SELECT Id, GP_Relationship__c,
                        GP_Project__c,
                        GP_Customer_Name__c, 
                        GP_Bill_To_Address__c, 
                        GP_Ship_To_Address__c, 
                        GP_Customer_Name__r.Name,
                		GP_Parent_Project_Address__c,
                        GP_Bill_To_Address__r.GP_Country__c,
						GP_Bill_To_Address__r.GP_State__c,//Avinash - Avalara
                		GP_Bill_To_Address__r.GP_City__c,//Avinash - Avalara
                		GP_Bill_To_Address__r.GP_Postal_Code__c,//Avinash - Avalara
                        GP_Bill_To_Address__r.GP_Address_Line_1__c,//Avinash - Avalara
                        GP_Bill_To_Address__r.GP_Address_Line_2__c,//Avinash - Avalara
                        GP_Bill_To_Address__r.GP_Address_Line_3__c,//Avinash - Avalara
                		GP_Ship_To_Address__r.GP_Country__c,
                		GP_Ship_To_Address__r.GP_State__c,//Avinash - Avalara
                		GP_Ship_To_Address__r.GP_City__c,//Avinash - Avalara
                		GP_Ship_To_Address__r.GP_Postal_Code__c,//Avinash - Avalara
                        GP_Ship_To_Address__r.GP_Address_Line_1__c,//Avinash - Avalara
                        GP_Ship_To_Address__r.GP_Address_Line_2__c,//Avinash - Avalara
                        GP_Ship_To_Address__r.GP_Address_Line_3__c,
                        GP_Ship_To_Address__r.GP_Address__c,
                        GP_Bill_To_Address__r.GP_Address__c,
                        GP_Ship_To_Address__r.GP_SITE_NUMBER__c,
                        GP_Bill_To_Address__r.GP_SITE_NUMBER__c,
                        GP_Customer_Name__r.GP_CUSTOMER_NUMBER__c
                    FROM GP_Project_Address__c 
                    WHERE GP_Project__c = :projectId
                    order by GP_Sequence_Number__c]; 
    }


    public static GP_Project_Address__c selectDirectProjectAddressUnderProject(Id projectId) {
        return [SELECT Id, GP_Relationship__c,
                        GP_Project__c,
                        GP_Customer_Name__c, 
                        GP_Customer_Name__r.Name,
                        GP_Bill_To_Address__c, 
                        GP_Ship_To_Address__c,
                		GP_Parent_Project_Address__c,
                        GP_Bill_To_Address__r.GP_Address__c,
                        GP_Bill_To_Address__r.GP_Country__c,
                        GP_Ship_To_Address__r.GP_Address__c,  
                        GP_Customer_Name__r.GP_Oracle_Customer_Id__c
                    FROM GP_Project_Address__c 
                    WHERE GP_Project__c = :projectId
                    AND GP_Relationship__c = 'Direct'
                    order by GP_Sequence_Number__c]; 
    }
}