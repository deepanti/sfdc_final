@isTest
public class GPCmpServiceProjectDocumentTracker {

    public static String jsonrespPODocumentWithId, jsonrespOtherDocument, jsonrespPODocument;
    public static GP_Project_Document__c objProjectDocument, objProjectPODocument;
    public static GP_Project__c prjObj;

    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        insert prjObj;
        
        ContentVersion objContentVersion = new ContentVersion();
        String encodedString = 'abcdefghijklmnopqrstuvwxyz';
        objContentVersion.versionData = EncodingUtil.base64Decode(encodedString);
        objContentVersion.PathOnClient = 'File Name';
        insert objContentVersion;
        
        ContentVersion objContentVersion2 = new ContentVersion();
        String encodedString2 = 'abcdefghijklmnopqrstuvwxyz';
        objContentVersion2.versionData = EncodingUtil.base64Decode(encodedString2);
        objContentVersion2.PathOnClient = 'File Name 2';
        insert objContentVersion2;

        GP_Project_Document__c objPODocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'PO Document');
        objPODocument.GP_File_Name__c = 'File Name';
        objPODocument.GP_Content_Version_Id__c = objContentVersion.Id;
        insert objPODocument;

        GP_Project_Document__c objOtherDocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'Other Document');
        objOtherDocument.GP_File_Name__c = 'File Name 2';
        objOtherDocument.GP_Content_Version_Id__c = objContentVersion2.Id;
        insert objOtherDocument;

    }
    @isTest static void testGPCmpServiceProjectDocument() {
        fetchData();
        testgetProjectDocuments();
        testFailureNoContentToSave();
        testsaveProjectDocument();
        testsavePOProjectDocument();
        testsavePOProjectDocumentWithId();
        testdeleteProjectDocument();
    }
    public static void fetchData() {
        prjObj = [select id, Name from GP_Project__c limit 1];
        objProjectDocument = [select id, GP_File_Name__c, GP_Content_Version_Id__c from GP_Project_Document__c where RecordType.Name = 'Other Document'];
        objProjectPODocument = [select id, GP_File_Name__c, GP_Content_Version_Id__c from GP_Project_Document__c where RecordType.Name = 'PO Document'];
        jsonrespOtherDocument = '[{"objProjectDocument":{"GP_Project__c":"' + prjObj.Id + '","Name":"TEST","GP_File_Name__c":"' + objProjectDocument.GP_File_Name__c +'","GP_Content_Version_Id__c":"' + objProjectDocument.GP_Content_Version_Id__c +'"},"filedata":"qwertyuio","filename":"Test.csv"}]';
        jsonrespPODocument = '[{"objProjectDocument":{"GP_Project__c":"' + prjObj.Id + '","Name":"TEST","GP_PO_Number__c":"PO_001","GP_File_Name__c":"' + objProjectPODocument.GP_File_Name__c +'","GP_Content_Version_Id__c":"' + objProjectPODocument.GP_Content_Version_Id__c +'"},"filedata":"qwertyuio","filename":"Test.csv"}]';
        jsonrespPODocumentWithId = '[{"objProjectDocument":{"GP_Project__c":"' + prjObj.Id + '","Name":"TEST","GP_PO_Number__c":"PO_001","Id":"' + objProjectPODocument.Id + '"},"filedata":"qwertyuio","filename":"Test.csv"}]';

    }
    public static void testgetProjectDocuments() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.getProjectDocuments(prjObj.id);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testFailureNoContentToSave() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.saveProjectDocument(null, prjObj.id, 'OtherDocument');
        //System.assertEquals(false, returnedResponse.isSuccess);
    }
    public static void testsaveProjectDocument() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.saveProjectDocument(jsonrespOtherDocument, prjObj.id, 'OtherDocument');
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testsavePOProjectDocument() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.saveProjectDocument(jsonrespPODocument, prjObj.id, 'PODocument');
		//System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testsavePOProjectDocumentWithId() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.saveProjectDocument(jsonrespPODocumentWithId, prjObj.id, 'PODocument');
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testdeleteProjectDocument() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDocument.deleteProjectDocument(objProjectDocument.id, null);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
}