/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing CampaignMemberTriggerHandler class
**************************************************************************************************************************/
@isTest (SeeAllData = false)
public class CampaignMemberTriggerHandlerTest {
    public static User u;
    public static Campaign insertedCampaign;
    public static Campaign insertedCampaign2;
    public static Account acc;
    public static Lead lead1;
    public static Lead lead2;
    public static Contact con1;
    public static Contact con2;
    public static List<Campaign> campList;
    public static CampaignMember campLead1Mem;
    public static CampaignMember campLead2Mem;
    public static CampaignMember campLead3Mem;
    public static CampaignMember campCon1Mem;
    public static CampaignMember campCon2Mem;
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    public static void setUpData()
    {   
        RecordType contactCreation = [select id from recordType where sObjectType = 'Contact' And DeveloperName = 'Default_Record_Type' limit 1];
        
        u = TestDataFactoryUtility.createTestUser('System Administrator','kaushikee_kumar@persistent.com','kaushikee_kumar@persistent.com');
        
        acc = TestDataFactoryUtility.createTestAccountRecord();
        
        con1 = new Contact(lastname='abc',firstname='pqr',email='lmntest@gmail.com',AccountId=acc.Id,Title='Manager',Country_of_Residence__c='India',
                           RecordTypeId = contactCreation.Id);
        
        con2 = new Contact(lastname='hehe',firstname='pqr',email='hehetest@gmail.com',AccountId=acc.Id,Title='Manager',Country_of_Residence__c='India',
                           RecordTypeId = contactCreation.Id);
        
        lead1 = new Lead(lastname='abc',company='abc',Account__c=acc.Id,firstname='uuu',Title='Manager',email='xfytest@gmail.com',Status='Raw',Country_Of_Residence__c='India');
        
        lead2 = new Lead(lastname='abcl',company='abcg',firstname='uuul',Title='Managerl',email='xfytlest@gmail.com',Status='Raw',Country_Of_Residence__c='India');
        
        
    }
    
    public static testMethod void onAfterInsertforAccountCount()
    {
        setUpData();
        System.runAs(u){
            Test.startTest();
            insert acc;
            insert con1;
            insert con2;
            insert lead1;
            insert lead2;
            insertedCampaign = new Campaign(Name='CampaignTest',Status='Launched',Channel__c='Telenurturing',AssignedTo__c='',AccountCount__c=0);
            insert insertedCampaign;
            insertedCampaign2 = new Campaign(Name='CampaignTest2',Status='Launched',Channel__c='Telenurturing',AssignedTo__c='',AccountCount__c=0);
            insert insertedCampaign2;
            campList = new List<Campaign>();
            campList.add(insertedCampaign);
            campList.add(insertedCampaign2);
            lead1.Account__c = acc.Id;
            update lead1;
            campLead1Mem = new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=lead1.Id);
            insert campLead1Mem;
            campLead2Mem =new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=lead2.Id);
            insert campLead2Mem;
            campLead3Mem =new CampaignMember(CampaignId=insertedCampaign2.Id,LeadId=lead2.Id);
            insert campLead3Mem;
            
            Test.stopTest();
        }
    }
}