@isTest
public class ApprovalQsrmController_Test {
    public static Opportunity opp;
    public static User u,u1;
    public static QSRM__c qsrm,qsrm1,qsrm2,qsrm3,qsrm4,qsrm5,qsrm6,qsrm7,qsrm8,qsrm9;
    public static Map<String, String> pickListMap= new Map<String, String>();
    public static Account oAccount;
    public static Contact oContact;
    public static id QSRMrecTypeId;
    
    public static testmethod void validateTest(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
         
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
        ApprovalQsrmController.sendReworkQSRMEmailTemplate(u.id,u1.id,'test',testQSRM.id);
      Test.stopTest();
    }
    public static testmethod void validateTestM1(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
       
      Test.stopTest();
    }
    public static testmethod void validateTestA1(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
         
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
      Test.stopTest();
    }
    public static testmethod void validateTestA2(){
        Test.startTest();
        setupTestData();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Product_Service_Line__c ='',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTestMore5(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
             'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTestMore6(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u1,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTest1(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTestMore4(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u1,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTest2(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            'Exec Connection With Buyer',
            'Budget Approved',
            '',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '',
            '',
            'We know one or more buyers and also one or more influencers',
            '',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
public static testmethod void validateTest3(){
        Test.startTest();
        setupTestData();
    opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            'Exec Connection With Buyer',
            'Budget Approved',
            '',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '',
            '',
            'We know one or more buyers and also one or more influencers',
            '',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
public static testmethod void validateTestA3(){
        Test.startTest();
        setupTestData();
    opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
      Test.stopTest();
    }


    public static testmethod void validateTest4(){
        Test.startTest();
        setupTestData();
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '',
            '',
            '',
            '',
            '',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    public static testmethod void validateTest5(){
        Test.startTest();
        setupTestData();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            'Time & Materials',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    
    public static testmethod void validateTestLess2(){
        Test.startTest();
        setupTestData();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        
      Test.stopTest();
    }
    
    public static testmethod void validateTest6(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
         
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }
    
    public static testmethod void validateTest7(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
         
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }

    public static testmethod void validateTestless1(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        opportunity  opp1 =new opportunity(name='1234',StageName='1. Discover',Amount=2000,QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1,
                                           Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                           Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp1;
         
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u1,opp1.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }
    public static testmethod void validateTestmore1(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();

        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u1,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }
    
public static testmethod void validateTest8(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }
    public static testmethod void validateTestMore8(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u1,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            '',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
       
      Test.stopTest();
    }
    
    public static testmethod void validateTest9(){
        Test.startTest();
        setupTestData();
        list<qsrm__c> qsrmlst = new list<qsrm__c>();
        
        QSRM__c testQSRM = TESTQsrmDataUtility.createTestQSRMRecord(u,u,opp.Id,QSRMrecTypeId,'Client views us as trusted advisor and/or we have a historically strong delivery track record.',
            'Yes to a large extent',
            'High probability of achieving target margin',
            'Exec Connection With Buyer',
            'Budget Approved',
            'Decision maker controls the funding for this opportunity',
            'Client sponsor/decision maker is known and has had a neutral or positive experience with our services',
            '1-3 months',
            'Client thinks our win themes and value proposition are compelling and differentiated.',
            'We know one or more buyers and also one or more influencers',
            'Client believes our solution could be an excellent fit for their needs',
            'We are well positioned compared to our competitors or this is a sole sourced opportunity',
            'We have the skills available to start the project and no risk of jeapordizing the project start date',
            'We have the skills available in the right locations and there is no risk to project delivery',
            '',
            'Solution can be delivered at target margin and aligns with client pricing expectations');
        insert testQSRM;
        qsrmlst.add(testQSRM);
       ApprovalQsrmController cont = new ApprovalQsrmController(testQSRM.id);
        ApprovalQsrmController.autoMailappreject(qsrmlst);
      Test.stopTest();
    }

    
    private static void setupTestData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
		u1 = GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
        
        opp =new opportunity(name='1234',StageName='1. Discover',QSRM_Nature_Type__c ='TS',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                             Revenue_Start_Date__c=system.today()+1,Product_Service_Line__c ='',Amount=5000000,accountid=oAccount.id,W_L_D__c='',
                             Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp;
        
        QSRMrecTypeId = Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('TS QSRM').getRecordTypeId();
        
        
    }  
    
}