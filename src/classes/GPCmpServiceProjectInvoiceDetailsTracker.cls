/*=======================================================================================
Description: Test Data Class for GPCmpServiceProjectInvoiceDetails

=======================================================================================
Version#     Date                           Author                    Description
=======================================================================================
1.0          10-may-2017                   Ved prakash            Initial Version 
=======================================================================================  
*/
@isTest public class GPCmpServiceProjectInvoiceDetailsTracker {
    public static GP_Project__c prjObj; 
    public static GP_Invoice_Email_Notification__c invoiceEmail;    
    
    static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ; 
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        GP_Project_Template__c indirectTemp = GPCommonTracker.getProjectTemplate();
        indirectTemp.GP_Active__c = true;
        indirectTemp.Name = 'INDIRECT_INDIRECT';
        indirectTemp.GP_Business_Type__c = 'Indirect';
        indirectTemp.GP_Business_Name__c = 'Indirect';
        indirectTemp.GP_Business_Group_L1__c = 'All';
        indirectTemp.GP_Business_Segment_L2__c = 'All';
        insert indirectTemp;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',indirectTemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        insert prjObj ;
        
        invoiceEmail = new GP_Invoice_Email_Notification__c();
        invoiceEmail.GP_Project__c = prjObj.Id;
        invoiceEmail.GP_User__c = UserInfo.getUserId();
        invoiceEmail.GP_Status__c = 'Not Initiated';
        insert invoiceEmail;
    }
    
    @isTest public static void testgetOracleProjectNumber()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectInvoiceDetails.getOracleProjectNumber(prjObj.id);
    }
    
    @isTest public static void testgetProjectInvoiceDetails()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectInvoiceDetails.getProjectInvoiceDetails(prjObj.id);
    }
    
    @isTest public static void testSentProjectInvoiceDetails()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectInvoiceDetails.getProjectInvoiceDetails(prjObj.id);
    }
    
    @isTest public static void testsaveEmailInvoiceRequest()
    {        
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HttpStaticfakeResp');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        setupCommonData();
        
      
        string tempJSON =' { "serviceDeliveryStartDate" : "23-Jun-2017", '
            +' "serviceDeliveryEndDate" :"23-Jun-2017",'
            +'	"dueDate" :"27-Jun-2017",'
            +'	"TRXID" :"1234",'
            +' "invoiceDate":"21-Jun-2017 ",'
            +' "invoiceNumber" :"12346",'
            +' 	"PO" :"PO12345",'
            +'  "oracleInvoiceNumber" :"INV234518",'
            +' "amount":"10000"'
            
            +' }';
        
        list<GP_Invoice_Email_Notification__c> obj = new list<GP_Invoice_Email_Notification__c>{new GP_Invoice_Email_Notification__c(GP_Oracle_Invoice_ID__c='q')};
        string strObj = JSON.serialize(obj); 
        GPControllerProjectInvoices objGPControllerProjectInvoices = GPControllerProjectInvoices.parse(tempJSON);
        objGPControllerProjectInvoices.sendInvoiceNotification(strObj);
        
    }
    
    @isTest public static void testsaveEmailInvoiceRequestWithList()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('HttpStaticfakeResp');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/xml');
        Test.setMock(HttpCalloutMock.class, mock);
        setupCommonData();
        
        GPAuraResponse obj = GPCmpServiceProjectInvoiceDetails.saveEmailInvoiceRequest(JSON.serialize(new List<GP_Invoice_Email_Notification__c>{new GP_Invoice_Email_Notification__c(GP_Oracle_Invoice_ID__c='q')}));
        
    }
}