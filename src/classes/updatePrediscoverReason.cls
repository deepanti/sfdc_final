public without sharing class updatePrediscoverReason {
    
    @AuraEnabled 
    Public static Wrapper getDropReasonsList(){
        try{
            Wrapper obj = new Wrapper();
            list<string> mLst = new list<string>(); //getPicklist.GetDependentOptions
            map<string,list<string>> depentendPicklist = MyPickListInfo.getFieldDependencies('opportunity','Drop_Category__c','Drop_Reasons__c');
           // map<string,list<string>> depentendPicklist = getPicklist.GetDependentOptions('opportunity','Drop_Category__c','Drop_Reasons__c');
            system.debug(':==depentendPicklist==:'+depentendPicklist);
            for(string s :  depentendPicklist.keyset()){
                mLst.add(s);
            }//:====obj.MastValue====(Duplicity, Qualification, Seller Assessment, Process Related)
            system.debug(':====obj.MastValue===='+mLst);
            obj.MastValue = mLst;
            obj.Duplicity = depentendPicklist.get('Duplicity');
            obj.Qualification = depentendPicklist.get('Qualification');
            obj.SellerAssessment = depentendPicklist.get('Seller Assessment');
            obj.ProcessRelated = depentendPicklist.get('Process Related');
            
            system.debug(':====obj.Duplicity===='+obj.Duplicity);
            system.debug(':====obj.Qualification===='+obj.Qualification);
            system.debug(':====obj.SellerAssessment===='+obj.SellerAssessment);
            system.debug(':====obj.ProcessRelated===='+obj.ProcessRelated);
            
            
            return obj; 
        }catch(Exception e){
            system.debug(':===erro r lmst  = '+e.getMessage()+':===error line===:'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('updatePrediscoverReason',e.getMessage(), '', e.getStackTraceString(),'updatePrediscoverReason', 'getDropReasonsList','Fail','',String.valueOf(e.getLineNumber())); 
       
            return null;
        }
    }
    
    @AuraEnabled 
    public static void saveDropReasons(Map<String, Object> resultData,String oppId) {
        try{
        system.debug(':==oppId=='+oppId);
        List<Opportunity> updateOppList=new List<Opportunity>();
        List<Opportunity> opplist=[Select id,StageName,Drop_Category__c,Drop_Reasons__c from Opportunity where Id =: oppId limit 1];
         system.debug(':==opplt== :' +opplist);
            /* Map<String,Object> resultMap=new Map<String,Object>();
        For(String key: resultData.keySet()){
            object value=resultData.get(key);
             String finalValue=String.ValueOf(value);
             System.debug('values=========++'+finalValue);
              System.debug(' Key======='+Key);  
        } */
            string mt = string.valueof(resultData.keyset());
            string val = string.valueof(resultData.values());
            string val1 = val.removeEnd('))');
            string valueset = val1.removeStart('((');
            
            string master = mt.removeEnd('}');
            string keyset = master.removeStart('{');
            
            system.debug(':==keyset==:'+keyset);
            system.debug(':==valueset==:'+valueset);
            for(Opportunity op : opplist){
                opportunity opp = new opportunity();
                opp.Drop_Reasons__c=valueset;
                opp.Drop_Category__c=keyset;
                opp.W_L_D__c = 'Dropped';
                opp.StageName = '8. Dropped';
                opp.id = op.Id;
                    updateOppList.add(opp);
                
            }
        If(updateOppList.size()>0){
       		update  updateOppList; 
            
        }
        }catch(exception e){
            system.debug(':==error---:'+e.getMessage()+':===error line===:'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('updatePrediscoverReason',e.getMessage(), '', e.getStackTraceString(),'updatePrediscoverReason', 'saveDropReasons','Fail','',String.valueOf(e.getLineNumber())); 
       
        }
    }
    
    public class Wrapper{
        @auraenabled
        public list<string> MastValue;
        @auraenabled
        public list<string> Duplicity;
        @auraenabled
        public list<string> Qualification;
        @auraenabled
        public list<string> SellerAssessment;
        @auraenabled
        public list<string> ProcessRelated;
    }
    
    
}