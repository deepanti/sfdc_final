public class VIC_PayoutApprovalCtrl {
@AuraEnabled
    Public static boolean RenderPayoutRoll(){
         boolean incavailable; 
       list<Target_Achievement__c> lstInc=[Select id,vic_Status__c from Target_Achievement__c where vic_Status__c=:'HR - Pending'];
        if(lstInc!=null && lstInc.size()>0){
           incavailable=true; 
        }
        else{
          incavailable=false;  
        }
     return incavailable;
    }
}