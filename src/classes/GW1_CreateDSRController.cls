// Class is Controller class of vfpage GW1_CreateDSR
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1        03-10-2016       Rishi Kumar              
// ---------------------------------------------------------------------------------------------


public with sharing class GW1_CreateDSRController {
    private ApexPages.StandardController oppcontroller;
    private Opportunity parentOpportunity = new Opportunity();


    /*
      constructor of GW1_CreateDSRController 
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */ 
     
    public GW1_CreateDSRController(ApexPages.StandardController controller) {

        this.oppController = controller;
        System.debug('@@in Constructor controller::' + controller);
        
    }

    /*
      called on page  action="{!createDSR}"
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */
    
    public pageReference createDSR()
    {

        string parentOppId = ApexPages.currentPage().getParameters().get('id');

        system.debug('parentOppId ' + parentOppId);
        parentOpportunity = [SELECT Id, AccountId, GW1_Include_in_pilot__c,Approved__c, GW1_DSR_Created__c, Sub_Industry_Vertical__c, Industry_Vertical__c, Service_Line__c, Competitor__c, TCV1__c,
                             GW1_DSR_Created_Manually__c,DSR_bypass__c, GW1_Dont_Create_DSR__c,Nature_Of_Work_OLI__c,StageName,Why_Manual_DSR_created__c, name from Opportunity where Id = :parentOppId];

        
        
        System.debug('@@in method parentOpportunity::' + oppcontroller);
        List<GW1_DSR__c> newLstDsr = new List<GW1_DSR__c> ();       
        GW1_OpportunityTriggerHandler objTriggerHandler = new GW1_OpportunityTriggerHandler();
        
        system.debug('parentOpportunity.Why_Manual_DSR_created__c: '+ parentOpportunity.Why_Manual_DSR_created__c);
        
        if(parentOpportunity.Why_Manual_DSR_created__c == null)
        {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Enter the Reason for Manual DSR'));
                    return null;
        }
        
        if (parentOpportunity.Nature_Of_Work_OLI__c== null)
        {
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Atleast one OLI required'));
                    return null;
        }
        
        else if(parentOpportunity.Why_Manual_DSR_created__c != null)
        {
        parentOpportunity.DSR_bypass__c= True;
       // parentOpportunity.GW1_DSR_Created_Manually__c= True;
        update parentOpportunity;
        }
/*    
        if(parentOpportunity.GW1_Include_in_pilot__c==false)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please clicks Include Pilot'));
                    return null;
        }
        
        /*Delivery Deal
        if (parentOpportunity.GW1_DSR_Created__c == false && parentOpportunity.Nature_Of_Work_OLI__c!='Consulting') //create new dsr if not already created
        {

            /*
              SCENARIO 1
              Creation of DSR when QSRM is approved
              QSRM Approved  = TRUE
              Don't create DSR = FALSE
              DSR created manually = FALSE

              CREATE DSR

             
            if (parentOpportunity.Approved__c == TRUE && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                list<Opportunity> listParentOpp = new list<Opportunity> ();
                listParentOpp.add(parentOpportunity);
                try
                {
                    newLstDsr = objTriggerHandler.CreateDSRHandler(listParentOpp);
                }
                catch(exception e)
                {
                    if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                    }
                    else
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        }
                    return null;
                }
                
                if (newLstDsr != null && newLstDsr.size() > 0)
                {
                    parentOpportunity.GW1_DSR_Created_Manually__c = true;
                    parentOpportunity.GW1_DSR_Created__c = true;
                    try
                    {
                        update parentOpportunity;
                        PageReference dsrpg = new PageReference('/' + newLstDsr[0].id);
                        dsrpg.setRedirect(true);
                        return dsrpg;
                    }
                    catch(exception e)
                    {
                        if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                        }
                        else
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        }
                        return null;
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No Product Found'));
                    return null;
                }
            }
            /*
              SCENARIO 2
              QSRM approved and BD user does not want DSR to be created
              QSRM Approved  = TRUE
              Don't create DSR = TRUE 
              DSR created manually = FALSE 

              DON'T CREATE DSR
             
            if (parentOpportunity.Approved__c == TRUE && parentOpportunity.GW1_Dont_Create_DSR__c == TRUE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Colaboration is not required for this opportunity'));
                return null;
            }
            /*
              SCENARIO 3
              QSRM Approved, but DSR crearted manually beforehand
              QSRM Approved  = TRUE
              Don't create DSR = FALSE 
              DSR created manually = TRUE
              DON'T CREATE DSR
             

            if (parentOpportunity.Approved__c == TRUE && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == TRUE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'A Deal Service Request already exists for this opportunity'));
                return null;
            }
            /*
              SCENARIO 4
              BD user has indicated he does not want DSR to be created, so when he clicks on Create DSR button no DSR shouldbe created
              QSRM Approved  = FALSE
              Don't create DSR = TRUE 
              DSR created manually = FALSE

              DON'T CREATE DSR
             
            if (parentOpportunity.Approved__c == FALSE && parentOpportunity.GW1_Dont_Create_DSR__c == TRUE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'DSR Creation Not Allowed'));
                return null;
            }

            /*
              SCENARIO 5
              When user clicks on DSR button and all three checkboxes are false
              QSRM Approved  = FALSE
              Don't create DSR = FALSE
              DSR created manually = FALSE

              CREATE DSR
             
            system.debug('parentOpportunity.Approved__c ' + parentOpportunity.Approved__c);
            if (parentOpportunity.Approved__c == FALSE && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                list<Opportunity> listParentOpp = new list<Opportunity> ();
                listParentOpp.add(parentOpportunity);
                try
                {
                    newLstDsr = objTriggerHandler.CreateDSRHandler(listParentOpp);                  
                    PageReference dsrpg = new PageReference('/' + newLstDsr[0].id);
                    parentOpportunity.GW1_DSR_Created_Manually__c = true;
                    parentOpportunity.GW1_DSR_Created__c = true;
                    update parentOpportunity;
                    dsrpg.setRedirect(true);
                    return dsrpg;
                }
                catch(exception e)
                {
                    if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                    }
                    else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    }
                    return null;
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No product found in opportunity'));
                return null;
            }           
        }
        
        /*Consulting Deal
       else if (parentOpportunity.GW1_DSR_Created__c == false && parentOpportunity.Nature_Of_Work_OLI__c=='Consulting') //create new dsr if not already created
        {

            /*
              SCENARIO 1
              Creation of DSR when QSRM is approved
              QSRM Approved  = TRUE
              Don't create DSR = FALSE
              DSR created manually = FALSE

              CREATE DSR

             
            if (parentOpportunity.StageName == '2. Define' && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                list<Opportunity> listParentOpp = new list<Opportunity> ();
                listParentOpp.add(parentOpportunity);
                try
                {
                    newLstDsr = objTriggerHandler.CreateDSRHandler(listParentOpp);
                }
                catch(exception e)
                {
                    if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                    }
                    else
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        }
                    return null;
                }
                
                if (newLstDsr != null && newLstDsr.size() > 0)
                {
                    parentOpportunity.GW1_DSR_Created_Manually__c = true;
                    parentOpportunity.GW1_DSR_Created__c = true;
                    try
                    {
                        update parentOpportunity;
                        PageReference dsrpg = new PageReference('/' + newLstDsr[0].id);
                        dsrpg.setRedirect(true);
                        return dsrpg;
                    }
                    catch(exception e)
                    {
                        if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                        }
                        else
                        {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                        }
                        return null;
                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No Product Found'));
                    return null;
                }
            } 
            /*
              SCENARIO 2
              QSRM approved and BD user does not want DSR to be created
              QSRM Approved  = TRUE
              Don't create DSR = TRUE 
              DSR created manually = FALSE 

              DON'T CREATE DSR
             
            if (parentOpportunity.StageName == '2. Define' && parentOpportunity.GW1_Dont_Create_DSR__c == TRUE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Colaboration is not required for this opportunity'));
                return null;
            }
            /*
              SCENARIO 3
              QSRM Approved, but DSR crearted manually beforehand
              QSRM Approved  = TRUE
              Don't create DSR = FALSE 
              DSR created manually = TRUE
              DON'T CREATE DSR
             
            if (parentOpportunity.StageName == '2. Define' && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == TRUE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'A Deal Service Request already exists for this opportunity'));
                return null;
            }
            /*
              SCENARIO 4
              BD user has indicated he does not want DSR to be created, so when he clicks on Create DSR button no DSR shouldbe created
              QSRM Approved  = FALSE
              Don't create DSR = TRUE 
              DSR created manually = FALSE

              DON'T CREATE DSR
             
            if (parentOpportunity.StageName == '2. Define' && parentOpportunity.GW1_Dont_Create_DSR__c == TRUE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'DSR Creation Not Allowed'));
                return null;
            }

            /*
              SCENARIO 5
              When user clicks on DSR button and all three checkboxes are false
              QSRM Approved  = FALSE
              Don't create DSR = FALSE
              DSR created manually = FALSE

              CREATE DSR
            
           system.debug('parentOpportunity.Approved__c ' + parentOpportunity.Approved__c);
           if (parentOpportunity.StageName == '2. Define' && parentOpportunity.GW1_Dont_Create_DSR__c == FALSE && parentOpportunity.GW1_DSR_Created_Manually__c == FALSE)
            {
                list<Opportunity> listParentOpp = new list<Opportunity> ();
                listParentOpp.add(parentOpportunity);
                try
                {
                    newLstDsr = objTriggerHandler.CreateDSRHandler(listParentOpp);                  
                    PageReference dsrpg = new PageReference('/' + newLstDsr[0].id);
                    parentOpportunity.GW1_DSR_Created_Manually__c = true;
                    parentOpportunity.GW1_DSR_Created__c = true;
                    update parentOpportunity;
                    dsrpg.setRedirect(true);
                    return dsrpg;
                }
                catch(exception e)
                {
                    if (e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage().substring(e.getMessage().indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION') + 34)));
                    }
                    else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
                    }
                    return null;
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No product found in opportunity'));
                return null;
            }   
        }
        */
        else
        {
            
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'A Deal Service Request already exists for this opportunity'));
        }

        return null;
    }
}