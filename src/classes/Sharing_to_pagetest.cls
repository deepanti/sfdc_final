@istest
public class Sharing_to_pagetest
{
   
    public static testMethod void testController()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
             
     }
   static testMethod void testController1()
            {
             Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;   
        
          
        account accobj=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accobj;
       
         Sharing_SP__c sharingobject=new Sharing_SP__c (Account__c=accobj.id,Comments__c='testaccount');
         insert sharingobject;    
         
       FeedItem feedAccount = new FeedItem (ParentId = sharingobject.id,Body = 'Hello Account');
       insert feedAccount;  
             
         PageReference pageRef = Page.Sharetopage; // Add your VF page Name here
             Test.setCurrentPage(pageRef);
             Sharing_to_page testshare = new Sharing_to_page();
             testshare.selectedOption='Account';  
             testshare.save(); 
                
             testshare.reset();
             testshare.Done();
           
        Test.stoptest();
        
            }
            
     static testMethod void testController2()
            {
              Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;   
        
          
        account accobj=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accobj;
          
        Contact contobj=new Contact(firstname='Manoj',lastname='Pandey',accountid=accobj.Id,Email='test1@gmail.com');
        insert contobj;
        
         Sharing_SP__c sharingobject=new Sharing_SP__c (Contact__c=contobj.id,Comments__c='testcontact');
         insert sharingobject;
        
     FeedItem feedContact = new FeedItem (
     ParentId = sharingobject.id,Type = 'LinkPost',
      Body = 'Hello Contact'
     );
        insert feedContact;
             
         
             PageReference pageRef = Page.Sharetopage; // Add your VF page Name here
             Test.setCurrentPage(pageRef);
             Sharing_to_page testshare = new Sharing_to_page();
             testshare.selectedOption='Contact';     
             testshare.save(); 
                
             testshare.reset();
             testshare.Done();
            
        Test.stoptest();
            }   
    
    
 static testMethod void testController3()
            {
              Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
        
        test.startTest();
        Lead leadobj=new Lead(lastname='Pandey', Status='Raw', Company='testcompany', Email='test1@gmail.com',LeadSource='Digital');
        insert leadobj;
        
         Sharing_SP__c sharingobject=new Sharing_SP__c (Lead__c=leadobj.id,Comments__c='testLead');
         insert sharingobject;    
             
             FeedItem feedLead = new FeedItem (ParentId = sharingobject.id, Body = 'Hello Lead');
         insert feedLead;
             
             PageReference pageRef = Page.Sharetopage; // Add your VF page Name here
             Test.setCurrentPage(pageRef);
             Sharing_to_page testshare = new Sharing_to_page();
             testshare.selectedOption='Lead';    
             testshare.save(); 
             testshare.reset();
             testshare.Done();
            
        Test.stoptest();
            }      
    
 static testMethod void testController4()
            {
              Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;   
        
          
        account accobj=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accobj;
        
       opportunity oppobj=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accobj.id);
        insert oppobj;
        GW1_DSR__c dsrobj = new GW1_DSR__c(GW1_Account__c=accobj.id,GW1_Opportunity__c=oppobj.id,GW1_Service_Line_SAVO__c = 'Core F&A',Product_Family__c = 'F&A',GW1_Named_Competitors__c= 'Accenture' );
        insert dsrobj;
        
         Sharing_SP__c sharingobject=new Sharing_SP__c (DSR__c=dsrobj.id,Comments__c='testLead');
         insert sharingobject;    
         
         FeedItem feedDSR = new FeedItem (ParentId = sharingobject.id,Body = 'Hello DSR');
         insert feedDSR;
                     
             PageReference pageRef = Page.Sharetopage; // Add your VF page Name here
             Test.setCurrentPage(pageRef);
             Sharing_to_page testshare = new Sharing_to_page();
             testshare.selectedOption='DSR';    
             testshare.save(); 
             testshare.reset();
             testshare.Done();
            
        Test.stoptest();
                
                System.assertEquals ('Hello DSR', feedDSR.body);
            }      
       
      static testMethod void testController5()
            {
             Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u = new User(Alias = 'stduser', Email='stduser2017@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='stdtestuser', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='stduser2017@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;   
        
          
        account accobj=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accobj;
        
       opportunity oppobj=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accobj.id);
        insert oppobj;
        
        Contact contobj=new Contact(firstname='Manoj',lastname='Pandey',accountid=accobj.Id,Email='test1@gmail.com');
        insert contobj;
        
        Lead leadobj=new Lead(lastname='Pandey', Status='Raw', Company='testcompany', Email='test12@gmail.com',LeadSource='Digital');
        insert leadobj;
        
        GW1_DSR__c dsrobj = new GW1_DSR__c(GW1_Account__c=accobj.id,GW1_Opportunity__c=oppobj.id,GW1_Service_Line_SAVO__c = 'Core F&A',Product_Family__c = 'F&A',GW1_Named_Competitors__c= 'Accenture' );
        insert dsrobj;
        Sharing_SP__c sharingobject=new Sharing_SP__c (Account__c=accobj.id,Contact__c=contobj.id, Lead__c=leadobj.id,DSR__c=dsrobj.id);
         insert sharingobject;   
         
       FeedItem feedAll = new FeedItem (
           ParentId = sharingobject.id,
            Body = 'Hello All'
            );
         insert feedAll;
             
             PageReference pageRef = Page.Sharetopage; // Add your VF page Name here
             Test.setCurrentPage(pageRef);
             Sharing_to_page testshare = new Sharing_to_page();
             testshare.selectedOption='All'; 
             testshare.taggedSP.Account__c=accobj.id;
             //ids.add(accobj.id);
             //ids.add(leadobj.id);
             
             testshare.save(); 
             testshare.reset();
             testshare.Done();
            
        Test.stoptest();
                
                
         }    
    
 }