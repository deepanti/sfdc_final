/*
    @Description: Class will be used for handling all business logic of Opportunity Line Item
    @Test Class: VIC_OpportunityLineItemTriggerHandlerTest
    @Note: VIC_ prefix is still not recomended from dev end
    @Author: Vikas Rajput
*/
public class VIC_OpportunityLineItemTriggerHandler{
    VIC_IncentiveConstantCtlr objCommUtil = new VIC_IncentiveConstantCtlr ();
    String strITService = objCommUtil.getAllMetaData().get('IT_Services').vic_Value__c;//vic_CommonUtil.getITServiceLabel();
    String strNONITService = objCommUtil.getAllMetaData().get('Non_IT_Services').vic_Value__c;//vic_CommonUtil.getNONITServiceLabel();
    String strOppStage = objCommUtil.getAllMetaData().get('Opportunity_Eligible_Stage_For_VIC').vic_Value__c;
    List<OpportunityLineItem> lstServiceOpportunityLineItem = new List<OpportunityLineItem>();
    List<OpportunityLineItem> lstNONServiceOpportunityLineItem = new List<OpportunityLineItem>();
    Map<Id,User_VIC_Role__c> mapUserIdTOUserVICRole = new  Map<Id,User_VIC_Role__c>();
    Map<Id,Opportunity> mapIdToObjOpp = new Map<Id,Opportunity>();
    Map<String, VIC_Split_Percentage__mdt> mapKeyToSplitPercent = new vic_CommonUtil().initSplitPercentage();//It will return Split Key Split Percent
    List<String> natureOfWorkListIO = new List<String>();
    /*
        @Description: function will handle all trigger action
        @Author: Vikas Rajput
    */
    public void runTriggerAction(){
        natureOfWorkListIO = VIC_NatureOfWorkToDealTypeCtlr.getNatureOfWorkList('IO');
        if(Trigger.isBefore && Trigger.isInsert){
            //No insert process is needed for VIC(Genpact) implementation
        }
        if(Trigger.isBefore && Trigger.isUpdate){
            initLstOfOLIServiceOrNonService(Trigger.new, new VIC_NatureOfWorkToDealTypeCtlr().fetchAllDealType());//This is primary call for further process
            oliDataManager();
            assignPostCondtionData(Trigger.new);
        }
    }
    /*
        @Description: Checking that every oli has same bd rep and primary rep user
        @Called After:  function will be called after oliDataManager() function
        @Param: NAN
        @Return: List<OpportunityLineItem>
        @Author: Vikas Rajput
    */
    public void assignPostCondtionData(List<OpportunityLineItem> lstOLIARG){
        for(OpportunityLineItem eachOLI : lstOLIARG){
            if(!String.isBlank(eachOLI.Product_BD_Rep__c) && mapIdToObjOpp.containsKey(eachOLI.OpportunityID) && 
                Id.valueOf(mapIdToObjOpp.get(eachOLI.OpportunityID).OwnerId) == Id.valueOf(eachOLI.Product_BD_Rep__c) &&
                    eachOLI.vic_Final_Data_Received_From_CPQ__c){
                eachOLI.vic_Product_BD_Rep_Split__c = 0.00;
                eachOLI.vic_Product_BD_Rep_Approval_Status__c = 'Not Required';
            }
        }
    }
    
    /*
        @Description: function will be used to process service and non service opportunity line item, 
        @Called After:  function will be called after initLstOfOLIServiceOrNonService(param1, param2) function
        @Param: NAN
        @Return: void
        @Author: Vikas Rajput
    */
    public void oliDataManager(){        
        List<String> natureOfWorkListIO = VIC_NatureOfWorkToDealTypeCtlr.getNatureOfWorkList('IO');
        if(!lstNONServiceOpportunityLineItem.isEmpty()){
            new VIC_OLINonITServiceCtlr().serviceManagerForNonIT(lstNONServiceOpportunityLineItem, mapUserIdTOUserVICRole, mapKeyToSplitPercent);//It will NON IT Service OLI
            for(OpportunityLineItem o:lstNONServiceOpportunityLineItem){
                if(natureOfWorkListIO.contains(o.Nature_of_Work__c)){
                    o.vic_IO_TS__c = 'IO';
                }else{
                    o.vic_IO_TS__c = 'TS';
                }
            }
        }
        if(!lstServiceOpportunityLineItem.isEmpty()){
            new VIC_OLIITServiceCtlr().serviceManagerForIT(lstServiceOpportunityLineItem, mapUserIdTOUserVICRole, mapIdToObjOpp, mapKeyToSplitPercent);//It will IT Service OLI
            for(OpportunityLineItem o:lstServiceOpportunityLineItem){
                if(natureOfWorkListIO.contains(o.Nature_of_Work__c)){
                    o.vic_IO_TS__c = 'IO';
                }else{
                    o.vic_IO_TS__c = 'TS';
                }
            }
        }        
        
    }
    /*
        @Description: function to identify that trigger is active or not ?
        @Param: lstOLIARG(list of opportunitylineitem)
        @Return: List of opportunitylineitem which has stage as "Signed Deal" && VIC Final Data Received From CPQ as "true"
        @Author: Vikas Rajput
    */
    public void initLstOfOLIServiceOrNonService(List<OpportunityLineItem> lstOLIARG, Map<String, String> mapStrDealTypeToServiceARG){
        Set<Id> setUserIDLCL = new Set<Id>();
        Set<Id> setOppId = new Set<Id>();
        List<OpportunityLineItem> lstSeriveOppLineItemLCL = new List<OpportunityLineItem>();
        List<OpportunityLineItem> lstNONSeriveOppLineItemLCL = new List<OpportunityLineItem>();
        for(OpportunityLineItem objOLIITR : lstOLIARG){
            setOppId.add(objOLIITR.OpportunityId);
        }
        mapIdToObjOpp = fetchOpportunity(setOppId);
        for(OpportunityLineItem objOLIITR : lstOLIARG){
            if(mapIdToObjOpp.containsKey(objOLIITR.OpportunityId) &&
                    vic_CommonUtil.getPreCondtionForOLI(objOLIITR, mapIdToObjOpp.get(objOLIITR.OpportunityId),strOppStage) && 
                        mapStrDealTypeToServiceARG.containsKey(objOLIITR.Nature_of_Work__c) && 
                            String.isNotEmpty(strITService) &&
                                mapStrDealTypeToServiceARG.get(objOLIITR.Nature_of_Work__c) == strITService &&
                                    !objOLIITR.vic_Is_Split_Calculated__c){
                objOLIITR.vic_Is_Split_Calculated__c = true; //Line is need to add per new discussion
                lstSeriveOppLineItemLCL.add(objOLIITR);
            }else if(mapIdToObjOpp.containsKey(objOLIITR.OpportunityId) &&
                    vic_CommonUtil.getPreCondtionForOLI(objOLIITR, mapIdToObjOpp.get(objOLIITR.OpportunityId), strOppStage) && 
                        mapStrDealTypeToServiceARG.containsKey(objOLIITR.Nature_of_Work__c) && 
                            String.isNotEmpty(strNONITService) &&
                                mapStrDealTypeToServiceARG.get(objOLIITR.Nature_of_Work__c) == strNONITService &&
                                    !objOLIITR.vic_Is_Split_Calculated__c){
                objOLIITR.vic_Is_Split_Calculated__c = true; //Line is need to add per new discussion
                lstNONSeriveOppLineItemLCL.add(objOLIITR);
            }
            setUserIDLCL.add(objOLIITR.vic_Owner__c);
            setUserIDLCL.add(objOLIITR.Product_BD_Rep__c);
        }
        lstServiceOpportunityLineItem = lstSeriveOppLineItemLCL;
        lstNONServiceOpportunityLineItem = lstNONSeriveOppLineItemLCL;
        mapUserIdTOUserVICRole = new VIC_UserPlanComponentCtlr().fetchMasterVicRoleBySetUserId(setUserIDLCL);
    }
    /*
        @Description: function will return map of opportunity against opportunity id
        @Param: Set<Id>(set of opportunity id)
        @Return: return map of opportunity against opportunity id
        @Author: Vikas Rajput
    */
    public Map<Id,Opportunity> fetchOpportunity(Set<Id> setOppIdARG){
        return new Map<Id,Opportunity>([select id,OwnerId,StageName,Account.Hunting_Mining__c from Opportunity where Id IN: setOppIdARG]);
    }
    
}