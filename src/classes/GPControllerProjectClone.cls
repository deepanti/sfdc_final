/**
 *
 * @group Project Clone.
 * @group-content ../../ApexDocContent/ProjectClone.html.
 *
 * @description Apex Controller for Project Clone.
 *              Inherits Base Project Util Class. 
 */
public without Sharing class GPControllerProjectClone extends GPBaseProjectUtil {

    private final String ANOTHER_DRAFT_PROJECT_ERROR_LABEL = 'Error : Another draft version of this project is already created!';
	
    Boolean skipParentTagging;
    Boolean validateProject;
    Set < Id > setOfProjectId;
    String cloneSource;
    // OMS Changes.
    public String dealId = '';

    GPWrapperApprovalValidatorConfig projectApprovalValidatorConfig = new GPWrapperApprovalValidatorConfig(false);
    GPProjectValidatorConfig validationConfig = new GPProjectValidatorConfig(false);
    GPServiceProjectClone cloneProjectService = new GPServiceProjectClone();

    /**
     * @description Parameterized constructor to accept set of project Id, Clone Source, Skip Parent Tagging and validate Project Records.
     * @param setOfProjectId set of project Ids.
     * @param cloneSource Clone Source.
     * @param skipParentTagging Skip Parent Tagging.
     * @param validateProject Validate Project Records.
     * 
     * @example
     * new GPControllerProjectClone(<setOfProjectId>, <cloneSource>, <skipParentTagging>, <validateProject>);
     */
    public GPControllerProjectClone(Set < Id > setOfProjectId, String cloneSource, Boolean skipParentTagging, Boolean validateProject) {
        this.skipParentTagging = skipParentTagging;
        this.validateProject = validateProject;
        this.setOfProjectId = setOfProjectId;
        this.cloneSource = cloneSource;
    }

    public GPControllerProjectClone() {}

    public GPAuraResponse getChildProjectRecord(String strProjectId) {
        List < GP_Project__c > listOfChilProject = [select id, OwnerId, Name, GP_Current_Working_User__c, GP_Current_Working_User__r.Name from GP_Project__c where GP_Parent_Project__c =: strProjectId];
        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(listOfChilProject));
    }
    /**
     * @description Clone the Project and associated child Records.
     *
     */
    public string cloneProject() {
        Savepoint sp = Database.setSavepoint();
        try {
            // Query all project and associated child records using Project Id.
            queryProjectAndChilRecords(setofprojectId); // Class member of GPBaseProjectUtil.
            // Set Map Of project and its Associated child Records.
            setMapOfProjectChildRecords(); // Class member of GPBaseProjectUtil.
            // Skip Version History Creation from domain project.
            GPDomainProject.skipVersionHistoryCreation = true;
            // Set Approval Configuration instance for validating records.
            cloneProjectService = cloneProjectService.setProjectApprovalValidatorConfig(projectApprovalValidatorConfig); // Class member of GPServiceProjectClone.

            if (lstProject.size() > 0) {
                Map < String, List < String >> mapOfProjectValidation = cloneProjectService.cloneProject(lstProject, cloneSource, validateProject, skipParentTagging);
  
                if (mapOfProjectValidation.size() > 0) {
                    if (mapOfProjectValidation.values()[0][0].contains('duplicate value found'))
                        return ANOTHER_DRAFT_PROJECT_ERROR_LABEL;
                    return JSON.serialize('Error' + mapOfProjectValidation.values());
                }
                GP_Project__c objInsertedProject = lstProject[0];
                // OMS Changes.
                if(objInsertedProject.RecordTypeId == cloneProjectService.strCMITSRecordType 
                   && objInsertedProject.GP_Deal_Category__c == 'Sales SFDC') 
                {
                    this.dealId = objInsertedProject.GP_Deal__c;
                }
               
                //cloneProjectService = cloneProjectService.setRelatedProjectRecord(lstProject[0].GP_Parent_Project__c);
                if (skipParentTagging) {
                    cloneChildRecords(new list < Id > (setofprojectId)[0], objInsertedProject.Id);
                } else {
                    cloneChildRecords(objInsertedProject.GP_Parent_Project__c, objInsertedProject.Id);
                }
                return objInsertedProject.Id;
            }
            return 'Error : Project Not Found';
        } catch (GPServiceProjectClone.GPServiceProjectCloneException serviceCloneException) {
            Database.rollback(sp);
            if (serviceCloneException.exceptionString.contains('duplicates value'))
                return ANOTHER_DRAFT_PROJECT_ERROR_LABEL;
            else if (serviceCloneException.exceptionString.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                return 'Error:' + GPCommon.parseErrorMessage(serviceCloneException.exceptionString);

            return 'Error:' + serviceCloneException.exceptionString;
        } catch (Exception E) {
            Database.rollback(sp);
            if (E.getMessage().contains('duplicates value'))
                return ANOTHER_DRAFT_PROJECT_ERROR_LABEL;

            return 'Error' + E.getMessage();
        }
    }
    private void cloneChildRecords(Id cloneSourceProjectId, Id objInsertedProjectId) {
		system.debug('==in cloneChildRecords==');
        cloneProjectService.setMapOfOldProjectIdVsNewProjectId(new Map < Id, Id > { cloneSourceProjectId => objInsertedProjectId });
        // OMS Changes.
        if(String.isNotBlank(this.dealId)) {
            compareBudgetAndExpensesFromOMSDetails(mapOfProjectExpense, mapOfProjectBudget, cloneSourceProjectId);
        }
        
        cloneProjectService.cloneProjectExpense(mapOfProjectExpense.get(cloneSourceProjectId), validationConfig.validateProjectExpense, skipParentTagging);
        cloneProjectService.cloneProjectDocument(mapOfProjectDocument.get(cloneSourceProjectId), validationConfig.validateDocumentUpload, skipParentTagging);
        cloneProjectService.cloneProjectBillRate(mapOfProjectProfileBillRate.get(cloneSourceProjectId), validationConfig.validateProfileBillRate, skipParentTagging);
        cloneProjectService.cloneProjectBudget(mapOfProjectBudget.get(cloneSourceProjectId), validationConfig.validateProjectBudget, skipParentTagging);
        cloneProjectService.cloneProjectBillingMileStone(mapOfBillingMilestone.get(cloneSourceProjectId), validationConfig.validateBillingMilestone, skipParentTagging);
        cloneProjectService.cloneProjectWorkLocationAndSDO(mapOfProjectWorkLocation.get(cloneSourceProjectId), validationConfig.validateWorkLocationAndSDO, skipParentTagging);
        cloneProjectService.cloneProjectResourceAllocations(mapOfResourceAllocation.get(cloneSourceProjectId), validationConfig.validateResourceAllocation, skipParentTagging);
        cloneProjectService.cloneProjectAddress(mapOfProjectAdress.get(cloneSourceProjectId), validationConfig.validateProjectAddress, skipParentTagging);

        // Create Version History from domain project.
        GPDomainProject.skipVersionHistoryCreation = false;

        cloneProjectService.cloneProjectLeadership(mapOfProjectLeadership.get(cloneSourceProjectId), validationConfig.validateProjectLeadership, skipParentTagging);
    }
    // OMS Changes.
    private void compareBudgetAndExpensesFromOMSDetails(Map<Id, List<GP_Project_Expense__c>> mapOfProjectExpense, 
                                                        Map<Id, List<GP_Project_Budget__c>> mapOfProjectBudget, 
                                                        Id cloneSourceProjectId) 
    {        
        List<GP_Project_Budget__c> listOfProjectBudgets = new List<GP_Project_Budget__c>();
        List<GP_Project_Expense__c> listOfProjectExpenses = new List<GP_Project_Expense__c>();
        List<GP_Project_Budget__c> listOfTempProjectBudgets = new List<GP_Project_Budget__c>();
        List<GP_Project_Expense__c> listOfTempProjectExpenses = new List<GP_Project_Expense__c>();

        if(mapOfProjectBudget != null && mapOfProjectBudget.containskey(cloneSourceProjectId)) {
            listOfProjectBudgets = mapOfProjectBudget.get(cloneSourceProjectId);
        }

        if(mapOfProjectExpense != null && mapOfProjectExpense.containskey(cloneSourceProjectId)) {
            listOfProjectExpenses = mapOfProjectExpense.get(cloneSourceProjectId);
        }
		
        if(listOfProjectExpenses.size() > 0 || listOfProjectBudgets.size() > 0) {

            for(GP_OMS_Detail__c omsd : GPSelectorOMSDetail.getOMSDetailsPerDealIds(new Set<Id>{this.dealId})) {
                if(omsd.RecordType.Name == 'Pricing View') {
                    listOfTempProjectBudgets.add(createProjectBudget(omsd, cloneSourceProjectId));
                }
                if(omsd.RecordType.Name == 'Expense') {
                    listOfTempProjectExpenses.add(createProjectExpenses(omsd, cloneSourceProjectId));
                }
            }
            if(listOfTempProjectBudgets.size() > 0) {
                mapOfProjectBudget.put(cloneSourceProjectId, listOfTempProjectBudgets);
            }
            
            if(listOfProjectExpenses.size() > 0) {
                for(GP_Project_Expense__c px : listOfProjectExpenses) {
                    if(px.GP_Data_Source__c != 'OMS') {
                        listOfTempProjectExpenses.add(px);
                    }
                }
            }

            if(listOfTempProjectExpenses.size() > 0) {
                mapOfProjectExpense.put(cloneSourceProjectId, listOfTempProjectExpenses);
            }
        }
    }

    private GP_Project_Budget__c createProjectBudget(GP_OMS_Detail__c omsDetail, String cloneSourceProjectId) {
        GP_Project_Budget__c objPrjBudget = new GP_Project_Budget__c();

        objPrjBudget.GP_Project__c = cloneSourceProjectId;
        //objPrjBudget.GP_Budget_Master__c = mapOfBudgetCodeToWorkLocation.get(omsDetail.GP_Work_Location_Code__c).id;
        objPrjBudget.GP_Track__c = omsDetail.GP_Track__c;
        objPrjBudget.GP_Band__c = omsDetail.GP_Band__c;
        objPrjBudget.GP_Country__c = omsDetail.GP_Country__c;
        objPrjBudget.GP_Effort_Hours__c = omsDetail.GP_Effort_in_Hours__c;
        objPrjBudget.GP_Total_Cost__c = omsDetail.GP_TotalCost__c;

        objPrjBudget.GP_Data_Source__c = 'OMS';
        objPrjBudget.GP_TCV__c = omsDetail.GP_TCVInUSD__c;
        objPrjBudget.GP_Cost_Rate__c = omsDetail.GP_CostRateUSDPerHour__c;

        return objPrjBudget;
    }

    private GP_Project_Expense__c createProjectExpenses(GP_OMS_Detail__c omsDetail, String cloneSourceProjectId) {
        GP_Project_Expense__c objPrjExpense = new GP_Project_Expense__c();

        objPrjExpense.GP_Project__c = cloneSourceProjectId;
        objPrjExpense.GP_Expenditure_Type__c = omsDetail.GP_Expense_Type__c;
        objPrjExpense.GP_Amount__c = omsDetail.GP_Expense_Amount__c;
        objPrjExpense.GP_Location__c = omsDetail.GP_Location__c;
        objPrjExpense.GP_Expense_Category__c = omsDetail.GP_Category__c;
        objPrjExpense.GP_Data_Source__c = 'OMS';
        
        return objPrjExpense;
    }
}