/**
* @Description: Class used to calculate OP Component incentive 
* @Author: Vikas Rajput (previous written code by @Bashim Khan) Last Modified by @Rajan
* @date: May 2018
  @ModifiedBy: Vikas Rajput
*/

public class VIC_CalculateOPIncentivectrl implements VIC_ScorecardCalculationInterface {
    
    public Decimal pmAcheivmentPercent{get;set;}
    public Map <Id, User_VIC_Role__c> mapUserIdToUserVicRoles{get;set;}
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    //This method should not be as static 
    public Decimal calculate(Target_Component__c objTargetCmp,  List<VIC_Calculation_Matrix_Item__c> lstMatrixItem){
        Decimal opAttainmentPercent  = objTargetCmp.vic_Achievement_Percent__c; //This is attainment% of Operating Plan component
        //Role = "SL" & Subrole = "CM/ITO" Exist
        
        if(mapUserIdToUserVicRoles.containsKey(objTargetCmp.Target__r.User__c) && 
            (mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Role__c == 'SL' && 
              mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Horizontal__c == 'CM/ITO')){
            if(pmAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('OP_PM_Percent').vic_Value__c)){    
                Decimal payoutPercent = calculateAdditionalPayout(opAttainmentPercent,lstMatrixItem);
                Decimal calTotalIncentive = calculateOP(objTargetCmp,null,opAttainmentPercent,payoutPercent);
                return calTotalIncentive; 
            }else{
                Decimal acheivmentPercent = opAttainmentPercent > 100?100:objTargetCmp.vic_Achievement_Percent__c;
                Decimal payoutPercent = calculateAdditionalPayout(opAttainmentPercent,lstMatrixItem);
                Decimal calTotalIncentive = calculateOP(objTargetCmp,null,opAttainmentPercent,payoutPercent);
                return calTotalIncentive;
            }
        }
        Decimal payoutPercent = calculateAdditionalPayout(opAttainmentPercent,lstMatrixItem);
        Decimal calTotalIncentive = calculateOP(objTargetCmp,null,opAttainmentPercent,payoutPercent); //@Vikas: null becuase new logic change it can be removed after testing is done
        return calTotalIncentive;
    }
    
    public Decimal calculateOP(Target_Component__c objComponentARG, VIC_Calculation_Matrix_Item__c objCalcMatrixItem, Decimal opAttainmentPercent, Decimal payoutPercent){
        if(objComponentARG != null && opAttainmentPercent != null && payoutPercent != null && 
             objComponentARG.Target__r.Target_Bonus__c != null && objComponentARG.Weightage__c != null && 
                 opAttainmentPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_AttainmentPercent_For_SEM_SLCP_CMGRM').vic_Value__c) && 
                     !String.isBlank(objComponentARG.Target__r.Plan__r.vic_Plan_Code__c) &&
                         (objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'Capital_Market_GRM' ||
                          objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'SEM' || 
                          objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'SL_CP_Enterprise' || 
                          objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'SL_CP_CMIT')){
                          
            payoutPercent = payoutPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_SEM_SLCP_CMGRM').vic_Value__c)?Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_SEM_SLCP_CMGRM').vic_Value__c):payoutPercent;
            Decimal opTargetedIncentive = objComponentARG.Target__r.Target_Bonus__c*objComponentARG.Weightage__c;
            return (opTargetedIncentive*payoutPercent)/10000; //10000 is used for balancing % value 
        }else if(objComponentARG != null && opAttainmentPercent != null && payoutPercent != null && 
             objComponentARG.Target__r.Target_Bonus__c != null && objComponentARG.Weightage__c != null && 
                 opAttainmentPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_AttainmentPercent_For_EnterpriseGRM').vic_Value__c) && 
                     !String.isBlank(objComponentARG.Target__r.Plan__r.vic_Plan_Code__c) &&
                         objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'EnterpriseGRM'){
                         
            payoutPercent = payoutPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_EnterpriseGRM').vic_Value__c)?Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_EnterpriseGRM').vic_Value__c):payoutPercent;
            Decimal opTargetedIncentive = objComponentARG.Target__r.Target_Bonus__c;
            return (opTargetedIncentive*payoutPercent)/100; //10000 is used for balancing % value 
        }else if(objComponentARG != null && opAttainmentPercent != null && payoutPercent != null && 
             objComponentARG.Target__r.Target_Bonus__c != null && objComponentARG.Weightage__c != null && 
                 opAttainmentPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_AttainmentPercent_For_IT_GRM').vic_Value__c) && 
                     !String.isBlank(objComponentARG.Target__r.Plan__r.vic_Plan_Code__c) &&
                         objComponentARG.Target__r.Plan__r.vic_Plan_Code__c == 'IT_GRM' && 
                             pmAcheivmentPercent > 90){
            payoutPercent = payoutPercent > Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_IT_GRM').vic_Value__c)?Decimal.valueOf( mapValueToObjIncentiveConst.get('OP_PayoutPercent_For_IT_GRM').vic_Value__c):payoutPercent;
            Decimal opTargetedIncentive = objComponentARG.Target__r.Target_Bonus__c*objComponentARG.Weightage__c;
          
            return (opTargetedIncentive*payoutPercent)/10000; //10000 is used for balancing % value 
        }
        return 0.00;
    }
    
     /*
        @Description: It is helper function for fetching Payout as addtional OP Component.
        @Param: Decimal tempValue, VIC_Calculation_Matrix_Item__c objCalMatrixItem
        @Author: Vikas Rajput Last Modified By @Rajan Last Modified Again @Vikas
    */
    public static Decimal calculateAdditionalPayout(Decimal achievementPer, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        Decimal calcuAddtionalPayout = 0.00;
        for(VIC_Calculation_Matrix_Item__c eachMatrixItem : lstCalcMatrixItem){
            if(eachMatrixItem.Is_Additional_Payout_Applicable__c){
                if(achievementPer > eachMatrixItem.vic_Start_Value__c && eachMatrixItem.vic_End_Value__c != null && 
                    achievementPer <= eachMatrixItem.vic_End_Value__c && eachMatrixItem.For_Each__c != null && 
                    eachMatrixItem.For_Each__c != 0 && eachMatrixItem.Additional_Payout__c != null && 
                        eachMatrixItem.vic_Payout__c !=null){
                    Decimal diffPayoutVal = (achievementPer - eachMatrixItem.vic_Start_Value__c);
                    Decimal additionalPayoutValue = diffPayoutVal.round(System.RoundingMode.DOWN)/eachMatrixItem.For_Each__c;
                    calcuAddtionalPayout = (eachMatrixItem.vic_Payout__c + (additionalPayoutValue*eachMatrixItem.Additional_Payout__c));
                    return calcuAddtionalPayout;
                }
            }else if(!eachMatrixItem.Is_Additional_Payout_Applicable__c){
                if(achievementPer == eachMatrixItem.vic_Start_Value__c && eachMatrixItem.vic_End_Value__c != null && achievementPer == eachMatrixItem.vic_End_Value__c){
                    return eachMatrixItem.vic_Payout__c;
                }else if(achievementPer > eachMatrixItem.vic_Start_Value__c && eachMatrixItem.vic_End_Value__c != null && achievementPer <= eachMatrixItem.vic_End_Value__c){
                    return eachMatrixItem.vic_Payout__c;
                }else if(achievementPer > eachMatrixItem.vic_Start_Value__c && eachMatrixItem.vic_End_Value__c == null){
                    return eachMatrixItem.vic_Payout__c;
                }
            }
        }
        return calcuAddtionalPayout;
    }
}