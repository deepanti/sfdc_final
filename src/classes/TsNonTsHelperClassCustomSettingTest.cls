@istest(seeAllData = true)
public class TsNonTsHelperClassCustomSettingTest {
    
    static opportunity opp;
        static User u1;
    
    Static testmethod void testHelperMethod(){
        setUpTestMethod();
         List<opportunity> new_OLI_list = new List<opportunity>();
             new_OLI_list.add(opp);
          TsNonTsHelperClass.dealTypeMethod(new_OLI_list);
    }
    
    private static void setUpTestMethod(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        list<opportunity> oliList= new List<opportunity>();
        opp=new opportunity(name='1234',StageName='1. Discover',Nature_Of_Work_OLI__c = 'Analytics,Consulting,Managed Services',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                            amount = 2000000);
      
        oliList.add(opp);
       
        
        insert oliList;
       
        
    }
}