public without sharing class vic_AssignTargetController {
    
    @AuraEnabled
    public Static userTargetAndComponentWrapper fetchTargetUserDetails(String recordId){
        List<Target__c> usertargetObj = [SELECT Id, User__r.Name, Start_Date__c, Is_Revised__c, Status__c, Is_Active__c, Comments__c From Target__c WHERE Id=: recordId];
        List<Target_Component__c> lstPlanComponent = new List<Target_Component__c>();
        userTargetAndComponentWrapper userTargetAndComponentWrapperObj = null;
        integer financeialYear; 
        if(usertargetObj.size()>0 && usertargetObj[0].Start_Date__c != null){
            financeialYear = (usertargetObj[0].Start_Date__c).year();
        }
        if(usertargetObj.size()>0){
            lstPlanComponent =[SELECT Id, Weightage__c,Master_Plan_Component__r.Name, Name, Target__r.Name, Target_In_Currency__c,Master_Plan_Component__r.Point_type__c,Target_Percent__c, Target__c,Is_Weightage_Applicable__c from Target_Component__c WHERE Target__c=:usertargetObj[0].Id AND Master_Plan_Component__r.Component_Usage__c INCLUDES('Target')];
            if(usertargetObj[0].Status__c == 'Released'){ //ADDED BY RISHABH PILANI
                usertargetObj[0].Is_Revised__c = true;
            }
            userTargetAndComponentWrapperObj = new userTargetAndComponentWrapper(usertargetObj[0], lstPlanComponent, financeialYear);
        }
        return userTargetAndComponentWrapperObj;
    }
    
    Public class userTargetAndComponentWrapper{
        @AuraEnabled
        public Target__c usertargetObj;
        @AuraEnabled
        public List<Target_Component__c> lstPlanComponent;  
        @AuraEnabled
        public integer financeialYear;
        Public userTargetAndComponentWrapper(Target__c usertargetObj, List<Target_Component__c> lstPlanComponent, integer financeialYear){
            this.usertargetObj = usertargetObj;
            this.lstPlanComponent = lstPlanComponent;
            this.financeialYear=financeialYear;
        }
    }
    
    @AuraEnabled
    public static string updatingTargetComponent(Target__c userTargetObj, list<Target_Component__c> targetComponent){
        if(userTargetObj != null)
            update userTargetObj;
        if(targetComponent.size() >0)
            update targetComponent;
        if(userTargetObj != null)
            return userTargetObj.Id;
        
        return null;
    }
    
    @AuraEnabled
    public static list<String> getFinancialYear(){
        return vic_CommonUtil.getYearForDisplay();   
    }
    
    @AuraEnabled
    public static String getCurrentYear(){
        return String.valueof(system.today().year());
    }
    
    
    @AuraEnabled
    public static saveTargetwrapper saveTargetUser(Target__c userTarget, Integer financialYear){
        String strFinancialYear = null;
        userTarget.User__r = null;
        if(financialYear != null){
            strFinancialYear= string.valueOf(financialYear);
            userTarget.Start_Date__c = Date.newInstance(financialYear,1,1);
            userTarget.End_Date__c = Date.newInstance(financialYear,12,31); 
        }
        List<User_VIC_Role__c> userlstVICRole = [SELECT Id, User__c, Master_VIC_Role__c,vic_For_Previous_Year__c from User_VIC_Role__c WHERE User__c =:userTarget.User__c and Not_Applicable_for_VIC__c=false and User__r.IsActive=true];
        if(userlstVICRole.isEmpty()){
            saveTargetwrapper saveTargetwrapperObj = new saveTargetwrapper(null, 'No VIC role associated to selected user or User not applicable for VIC.', true);
            return saveTargetwrapperObj;
        } 
        Integer previousYear = (Date.today().year())-1;
        if(userlstVICRole.size() > 0){
            User_VIC_Role__c objUserVICRole = null;
            for(User_VIC_Role__c itrUserVICRole : userlstVICRole){
                if(previousYear == Integer.valueOf(financialYear) && itrUserVICRole.vic_For_Previous_Year__c){
                    objUserVICRole = itrUserVICRole;
                }
            }
            if(!VIC_GeneralChecks.IsNotNull(objUserVICRole) ){
                objUserVICRole = userlstVICRole[0];
            }
            List<VIC_Role__c> lstVICRole = [SELECT Id, Plan__c 
                                            from VIC_Role__c 
                                            WHERE Master_VIC_Role__c =:objUserVICRole.Master_VIC_Role__c 
                                            AND Plan__r.Year__c=:strFinancialYear 
                                            AND Plan__r.Is_Active__c = true];
            if(lstVICRole.isEmpty()){
                saveTargetwrapper saveTargetwrapperObj = new saveTargetwrapper(null, 'No active plan found for selected financial year.', true);
                return saveTargetwrapperObj;
            } 
            if(lstVICRole.size() > 0){
                if(lstVICRole[0].Plan__c == null){
                    saveTargetwrapper saveTargetwrapperObj = new saveTargetwrapper(null, 'Selected VIC role is not associated with any active plan.', true);
                    return saveTargetwrapperObj;
                }else{
                    userTarget.Plan__c = lstVICRole[0].Plan__c;
                }
            }
        }
        if(userTarget != null){
            system.debug(userTarget+'userTarget');            
            insert userTarget; 
            /*try{
                User u = [Select ID, Domicile__c from user where id=:userTarget.User__c];
                if(u.Domicile__c != null){
                    Map <String, String> mapDomicileToISO = vic_CommonUtil.getDomicileToISOCodeMap();
                    userTarget.CurrencyISOCode = mapDomicileToISO.containsKey(u.Domicile__c) ? mapDomicileToISO.get(u.Domicile__c) : 'USD';
                    update userTarget;
                }
            }catch(Exception ex){}*/
        }
        list<Plan_Component__c> planComponent = [SELECT Id,Master_Plan_Component__c,Weightage_Applicable__c, Plan__c, 
                                                 Master_Plan_Component__r.Bonus_Type__c,Master_Plan_Component__r.Component_Usage__c 
                                                 from Plan_Component__c 
                                                 WHERE Plan__c=:userTarget.Plan__c];
        list<Target_Component__c> targetCompoObjList = new list<Target_Component__c>();
        if(planComponent.size()>0){
            for(Plan_Component__c planCompObj : planComponent){
                Target_Component__c targetCompoObj = new Target_Component__c();
                targetCompoObj.Master_Plan_Component__c= planCompObj.Master_Plan_Component__c;
                targetCompoObj.Target__c= userTarget.Id;
                targetCompoObj.Target_Status__c='Active';
                targetCompoObj.Is_Weightage_Applicable__c=planCompObj.Weightage_Applicable__c;
                targetCompoObj.Component_Usage__c = planCompObj.Master_Plan_Component__r.Component_Usage__c;
                targetCompoObj.Bonus_Type__c = planCompObj.Master_Plan_Component__r.Bonus_Type__c;
                targetCompoObj.Is_Bonus_Component__c = (planCompObj.Master_Plan_Component__r.Component_Usage__c != null && planCompObj.Master_Plan_Component__r.Component_Usage__c != '') ? (planCompObj.Master_Plan_Component__r.Component_Usage__c.contains('Bonus')? TRUE: FALSE) : FALSE;
                targetCompoObjList.add(targetCompoObj);
            } 
        }
        if(!targetCompoObjList.isEmpty())
            insert targetCompoObjList;
        
        List<Target_Component__c> lstPlanComponent =[SELECT Id,Weightage__c,Master_Plan_Component__r.Component_Usage__c,Is_Weightage_Applicable__c,
                                                        Master_Plan_Component__r.Name, Master_Plan_Component__r.Point_type__c, Name, Target__r.Start_date__c, 
                                                        Target__r.Name, Target__r.Plan__r.Name,Target__r.User__r.Name, Target__c, Target_Percent__c from Target_Component__c 
                                                        WHERE Target__c=:userTarget.Id 
                                                        AND Master_Plan_Component__r.Component_Usage__c INCLUDES('Target')];
       
        saveTargetwrapper saveTargetwrapperObj = new saveTargetwrapper(lstPlanComponent, null, false);
        return saveTargetwrapperObj;           
    }
    
    Public class saveTargetwrapper{
        @AuraEnabled
        Public List<Target_Component__c> lstPlanComponent;
        @AuraEnabled
        Public String errorMessage;
        @AuraEnabled
        Public Boolean isErrorMessage;
        
        Public saveTargetwrapper(List<Target_Component__c> lstPlanComponent, String errorMessage, Boolean isErrorMessage){
            this.lstPlanComponent = lstPlanComponent;
            this.errorMessage = errorMessage;
            this.isErrorMessage = isErrorMessage;
        }
    }
    
    @AuraEnabled
    public static string saveingTargetComponent(List<Target_Component__c> targetComponent){
        if(!targetComponent.isEmpty()){
            for(Target_Component__c allTrgtCmpts : targetComponent){
                if(allTrgtCmpts.Is_Weightage_Applicable__c==true){
                    allTrgtCmpts.Is_Weightage_Applicable__c=true;
                }
                system.debug('hello' + allTrgtCmpts.Target_Percent__c );
                if(allTrgtCmpts.Target_In_Currency__c != null && allTrgtCmpts.Target_In_Currency__c > 0 && allTrgtCmpts.Master_Plan_Component__r.Name !='Bonus (Currency)'){
                    double  currencyinMillion = 1000000;
                    allTrgtCmpts.Target_In_Currency__c = allTrgtCmpts.Target_In_Currency__c * currencyinMillion;
                    
                }
            }
            update targetComponent;
            return targetComponent[0].Target__c;
        } 
        return null;
    }
    
    @auraEnabled
    public static wrprChkUsrTrgt checkUserStatus(String usrId, String financialYear){
        try{
            List<User_VIC_Role__c> lstChkAplblforVC=[Select User__c from User_VIC_Role__c where User__c = :usrId and Not_Applicable_for_VIC__c=false and User_VIC_Role__c.User__r.IsActive=true];
            Integer Year=Integer.valueof(financialYear);
            List<Target__c> lstCrtdTrgt=[Select id,Start_date__c from Target__c where user__c = :usrId];
            List<Target__c> lstCrtdTrgtChkYr=new List<Target__c>();
            
            for(Target__c alltar : lstCrtdTrgt){
                
                integer startYear=alltar.Start_date__c.Year();
                if(Year==startYear)
                lstCrtdTrgtChkYr.add(alltar);
            }
            
            //if(lstChkAplblforVC.isEmpty() && lstCrtdTrgt.isEmpty()){
            wrprChkUsrTrgt objWrpr=new wrprChkUsrTrgt();
            if(!lstChkAplblforVC.isEmpty())    
                objWrpr.checkUserStatus='SUCCESS';
            else
                objWrpr.checkUserStatus='FAIL';
            if(lstCrtdTrgtChkYr.isEmpty())  
            objWrpr.checkTrgtStatus='SUCCESS';
            else
                objWrpr.checkTrgtStatus='FAIL';
            return objWrpr;
            
        }
        catch(Exception ex){
            wrprChkUsrTrgt objWrpr=new wrprChkUsrTrgt();
            objWrpr.checkUserStatus='FAIL';
            objWrpr.checkTrgtStatus='SUCCESS';
            return objWrpr;
        }
        
    }
    
    
    
    
    public class wrprChkUsrTrgt{
        
        @auraEnabled
        public string checkUserStatus;
        @auraEnabled
        public string checkTrgtStatus;
        
    }
}