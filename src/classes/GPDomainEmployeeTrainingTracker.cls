//================================================================================================================
//  Description: Test Class for GPDomainEmployeeTraining
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          9-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPDomainEmployeeTrainingTracker {
    
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    public static GP_Project_Leadership__c projectLeadership;
    
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_employee_training__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        empObjwithsfdcuser.GP_Final_OHR__c = '123456';
        insert empObjwithsfdcuser;
        
        //GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        //insert exceptionemp;
        
        //GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        //empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        //insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Oracle_Id__c = 'test';
        objpinnacleMaster.GP_Status__c = 'Active';
        objpinnacleMaster.GP_Training_Validity__c = '10';
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        insert prjobj;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Type_of_Leadership__c = 'Billing Approver'; 
        insert leadershipMaster;
        
        projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.recordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        projectLeadership.GP_Project__c = prjobj.id;
        insert projectLeadership;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 10;
        oppproobj.GP_Oracle_PID__c = 'test';
        oppproobj.GP_Customer_L4_Name__c = accobj.id;
        oppproobj.GP_EP_Project_Number__c = '1234';
        insert oppproobj;
        
        GP_Opp_Project_Leadership__c oppProjectLeadership2 = GPCommonTracker.getOppProjectLeadership();
        oppProjectLeadership2.GP_Emp_Person_Id__c = '123456';
        oppProjectLeadership2.GP_Leadership_Role__c = '1234';
        oppProjectLeadership2.GP_Opportunity_Project__c = oppproobj.id;
        oppProjectLeadership2.GP_Active__c = true;
        oppProjectLeadership2.GP_Start_Date__c = system.today().adddays(10);
        //oppProjectLeadership.GP_Opportunity_Project__c = '1234';
        oppProjectLeadership2.GP_IsUpdated__c = false;
        insert oppProjectLeadership2;
        
        
        GP_Opp_Project_Leadership__c oppProjectLeadership = GPCommonTracker.getOppProjectLeadership();
        oppProjectLeadership.GP_Emp_Person_Id__c = '123456';
        oppProjectLeadership.GP_Leadership_Role__c = '1234';
        oppProjectLeadership.GP_Opportunity_Project__c = oppproobj.id;
        oppProjectLeadership.GP_Active__c = true;
        oppProjectLeadership.GP_Start_Date__c = system.today().adddays(11);
        //oppProjectLeadership.GP_Opportunity_Project__c = '1234';
        oppProjectLeadership.GP_IsUpdated__c = false;
        insert oppProjectLeadership;
        oppProjectLeadership.GP_IsUpdated__c = true;
        update oppProjectLeadership;
        
        GP_Employee_Training__c objEmployeeTraining = new GP_Employee_Training__c();
        objEmployeeTraining.GP_Course_Id__c = 'test'; 
        objEmployeeTraining.GP_Status_Change_Date__c = system.today().adddays(10);
        insert objEmployeeTraining;
        Update objEmployeeTraining;
    }
    
    public static testmethod void testGPDomainEmployeeTraining() 
    {
        buildDependencyData();
    }
}