public class GPSelectorOpportunityProduct extends fflib_SObjectSelector {
  public List<Schema.SObjectField> getSObjectFieldList() {
       return new List<Schema.SObjectField> {
            OpportunityLineItem.Name
               
        };
    }

    public Schema.SObjectType getSObjectType() {
        return OpportunityLineItem.sObjectType;
    }

    public List<OpportunityLineItem> selectById(Set<ID> idSet) {
        return (List<OpportunityLineItem>) selectSObjectsById(idSet);
    }
    
       
    public List<OpportunityLineItem> selectOppproduct(Set<Id> setOfOppId) {
        return (List<OpportunityLineItem>) [Select ID,
                                                   Name,
                                                   Product2Id,
                                                   End_Date__c,
                                                   OpportunityId,
                                                   TCV__c,
                                                   Custom_Id__c,
                                                   SFDC_18_digit_ID__c,
                                                   Product2.Name,
                                                   Product2.Family,
                                                   Opportunity.Name, 
                                                   Delivering_Organisation__c,
                                                   Sub_Delivering_Organisation__c,
                                                   Opportunity.AccountID, 
                                                   Product2.Service_Line__c,
                                                   Opportunity.Account.Name,
                                                   Product2.Nature_of_Work__c, 
                                                   Opportunity.Probability,
                                                   Opportunity.Opportunity_ID__c,
                                                   Revenue_Start_Date__c,
                                                   Opportunity.Account.Sub_Business__c,
                                                   Opportunity.Account.Business_Group__c,
                                                   Opportunity.Account.Business_Segment__c,
                                                   Opportunity.Account.Industry_Vertical__c,
                                                   Opportunity.Account.Sub_Industry_Vertical__c,
                                                   Opportunity.Account.Business_Segment__r.Name,
                                                   Opportunity.Account.Sub_Business__r.Name 
                                                   from OpportunityLineItem 
                                                   where OpportunityId IN: setOfOppId];
    }
    
    public List<OpportunityLineItem> selectupdateOppproduct(Set<Id> setOfOppId, Set<Id> setofOLIID) {
        return (List<OpportunityLineItem>) [Select ID,
                                                   Name,
                                                   Product2Id,
                                                   End_Date__c,
                                                   OpportunityId,
                                                   SFDC_18_digit_ID__c,
                                                   Custom_Id__c,
                                                    TCV__c,
                                                   Product2.Name,
                                                   Product2.Family,
                                                   Opportunity.Name,                                                   
                                                   Opportunity.AccountID, 
                                                   Product2.Service_Line__c,
                                                   Opportunity.Account.Name,
                                                   Product2.Nature_of_Work__c,
                                                   Delivering_Organisation__c,
                                                   Sub_Delivering_Organisation__c,
                                                   Opportunity.Opportunity_ID__c,
                                                   Revenue_Start_Date__c,
                                                   Opportunity.Account.Sub_Business__c,
                                                   Opportunity.Account.Business_Group__c,
                                                   Opportunity.Account.Business_Segment__c,
                                                   Opportunity.Account.Sub_Business__r.Name, 
                                                   Opportunity.Account.Industry_Vertical__c,
                                                   Opportunity.Account.Sub_Industry_Vertical__c,
                                                   Opportunity.Account.Business_Segment__r.Name,Opportunity.Probability
                                                   from OpportunityLineItem 
                                                   where id IN : setofOLIID  
                                                   and OpportunityId IN: setOfOppId 
                                                   and Opportunity.Probability >=33];
    }
    
    public List<OpportunityLineItem> fetchOLIsByOppIds(Set<String> setOfOppId) {
        return (List<OpportunityLineItem>) [Select ID,
                                                   Name,
                                                   Product2Id,
                                                   End_Date__c,
                                                   OpportunityId,
                                                   TCV__c,
                                                   Custom_Id__c,
                                                   SFDC_18_digit_ID__c,
                                                   Product2.Name,
                                                   //Product2.Family,
                                            	   Product2.Product_Family__c,
                                                   Opportunity.Name, 
                                                   Delivering_Organisation__c,
                                                   Sub_Delivering_Organisation__c,
                                                   Opportunity.AccountID, 
                                                   Product2.Service_Line__c,
                                                   Opportunity.Account.Name,
                                                   Product2.Nature_of_Work__c, 
                                                   Opportunity.Probability,
                                                   Opportunity.Opportunity_ID__c,
                                                   Revenue_Start_Date__c,
                                                   Opportunity.Account.Sub_Business__c,
                                                   Opportunity.Account.Business_Group__c,
                                                   Opportunity.Account.Business_Segment__c,
                                                   Opportunity.Account.Industry_Vertical__c,
                                                   Opportunity.Account.Sub_Industry_Vertical__c,
                                                   Opportunity.Account.Business_Segment__r.Name,
                                                   Opportunity.Account.Sub_Business__r.Name 
                                                   from OpportunityLineItem 
                                                   where Opportunity_Id__c IN: setOfOppId];
    }
}