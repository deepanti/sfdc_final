public with sharing class MarketoSyncHelper {
    
    public String syncToMarketo(String ObjectName)
    {
        
        String status;
        
        if(ObjectName!=NULL)
        {
            if(ObjectName.equalsIgnoreCase('Account'))
            { 
                marketoUpdateBatch batctRef=new marketoUpdateBatch();
                batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Account';
                Database.executeBatch(batctRef,200);
                
                
                return status='Batch class is invoked';
            }
            else if(ObjectName.equalsIgnoreCase('Lead'))
            {
                marketoUpdateBatch batctRef=new marketoUpdateBatch();
                batctRef.query='SELECT Id,Sync_to_Marketo__c,Status FROM Lead ';
                Database.executeBatch(batctRef,200);
                return status='Batch class is invoked';
            }
            else if( ObjectName.equalsIgnoreCase('Contact'))
            {
                marketoUpdateBatch batctRef=new marketoUpdateBatch();
                batctRef.query='SELECT Id,Sync_to_Marketo__c FROM contact';
                Database.executeBatch(batctRef,200);
                return status='Batch class is invoked';
            }
             else if( ObjectName.equalsIgnoreCase('Opportunity'))
            {
                marketoUpdateBatch batctRef=new marketoUpdateBatch();
                batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Opportunity';
                Database.executeBatch(batctRef,200);
                return status='Batch class is invoked';
            }
             else if( ObjectName.equalsIgnoreCase('Campaign'))
            {
                marketoUpdateBatch batctRef=new marketoUpdateBatch();
                batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Campaign';
                Database.executeBatch(batctRef,200);
                return status='Batch class is invoked';
            }
            else
            { 
                system.debug('Invalid Object type');
                return status='Invalid Object';
            }
        }
        else
        {
            return status='Null value provide instead of Object name';
        }
        
    }
    
}