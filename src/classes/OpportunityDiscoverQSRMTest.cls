/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing OpportunityDiscoverQSRM class
**************************************************************************************************************************/
@isTest (SeeAllData = false)
public class OpportunityDiscoverQSRMTest{
    public static User u;
    public static User testUser;
    public static User  salesLeader;
    public static Account acc;
    public static Contact con1;
    public static Opportunity oppList;    
    public static List<Product2> pList;
    public static PricebookEntry standardPrice; 
    public static  Id pricebookId;
    public static QSRM__c testQSRM;
    public static QSRM_Leadership_Approvals__c qsrmApproval; 
    public static  Id qsrmId;
    public static List<OpportunityLineItem> oppLineItem; 
    public static List<OpportunityLineItemSchedule> oppLineItemSchedules;
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    public static void setUpData()
    {	
        
        acc = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
        insert acc;
        con1 = TestDataFactoryUtility.CreateContact('First','Last',acc.id,'Test','Test','fed@gtr.com','78342342343');
        insert con1;
        oppList = TestDataFactoryUtility.CreateOpportunity('Test',acc.id,con1.id);
        insert oppList;
        pList = TestDataFactoryUtility.createTestProducts2(1);
        pList[0].Industry_Vertical__c='BFS';
        pList[0].isActive=true;
        insert pList;
        system.debug('pList'+ pList);
        pricebookId = Test.getStandardPricebookId();
        standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000, 
            IsActive = true);
        insert standardPrice;
        oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);
        
        insert oppLineItem;
        testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','vikasrathiac@bc.com','vikasrathiac@bc.com');
        insert testUser;
        Test.startTest();
        qsrmId = [Select Id,Name from RecordType where name = 'TS QSRM'].id;
        salesLeader = [Select id,Name from User where email = 'vikasrathiac@bc.com'];
        testQSRM = TestDataFactoryUtility.createTestQSRMRecord(oppList.Id,qsrmId);
        testQSRM.Bid_pro_support_needed__c = 'Yes';
        insert testQSRM;
        Test.stopTest();
        qsrmApproval = new QSRM_Leadership_Approvals__c();
        qsrmApproval.QSRM__c = testQSRM.Id;
        qsrmApproval.QSRM_Owner__c = salesLeader.Id;
        qsrmApproval.Status__c = 'In Approval';
        insert qsrmApproval;
        
        
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverQSRM.fetchApprovedOrInApprovalQSRM mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void fetchApprovedOrInApprovalQSRMTest()
    {
        setUpData();
        System.runAs(salesLeader){
            
            qsrmId = OpportunityDiscoverQSRM.fetchApprovedOrInApprovalQSRM(oppList.Id);
            system.assertEquals('Annuity',testQSRM.Deal_Type__c); 
            
        }
    }
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverQSRM.fetchApprovedOrInApprovalQSRM mtd with null parameter
* @return - void
**************************************************************************************************************************/
    public static testMethod void fetchApprovedOrInApprovalQSRMTest2()
    {
        setUpData();
        System.runAs(salesLeader){
            
            qsrmId = OpportunityDiscoverQSRM.fetchApprovedOrInApprovalQSRM(null);
            
        }
    }
    
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverQSRM.fetchQSRMOwnerName mtd
* @return - void
**************************************************************************************************************************/
    public static testMethod void fetchQSRMOwnerNameTest()
    {
        setUpData();
        System.runAs(salesLeader){
            List<String> OwnerLIst = new List<String>();
            OwnerLIst =  OpportunityDiscoverQSRM.fetchQSRMOwnerName(testQSRM.Id);
            system.assertEquals('Annuity',testQSRM.Deal_Type__c); 
            
        }
    }
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverQSRM.fetchQSRMOwnerName mtd with null parameter
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void fetchQSRMOwnerNameTest2()
    {
        setUpData();
        System.runAs(salesLeader){
            List<String> OwnerLIst = new List<String>();
            OwnerLIst =  OpportunityDiscoverQSRM.fetchQSRMOwnerName(null);
        }
    }
    
    Public static testMethod void fetchOppDetailsMethod(){
         setUpData();
        OpportunityDiscoverQSRM.fetchOppDetail(oppList.id);
    }
}