public class ApprovalQsrmController {
    //ApprovalQsrmController.sendReworkQSRMEmailTemplate();
    public static String qId{get;set;}
    public static List<OpportunityLineItem> oliList{get;set;}
    public static List<QSRM__c>  QSRMObjList   {set;get;}
    public static List<Opportunity> OpptyList	{set;get;}
    public static String firstlevelComments {set;get;}
    public static List<ProcessInstance> ProcessInstanceList{set;get;}
    public static list<String> statusList=new list<String> {'Approved' , 'Rejected','Removed','Pending'};
        public static string action = '';
    
   public ApprovalQsrmController(String recordId) {
        System.debug('Record Id == '+recordId);
        loadDataInTemplate(recordId);
        OpptyList = new List<Opportunity>();
        oliList = new List<OpportunityLineItem>();
        QSRMObjList = new List<QSRM__c>();
       firstlevelComments = '';
        
    }   
    
    public ApprovalQsrmController() {
    }   
    
    public static void approvalMethod(list<QSRM__c> newQsrmlst){
        String QsrmId = '';
        String QsrmIdAutonumber = '';
        String SalesLineLeader = '';
        String VerticalLeader = '';
        String EmailTemplateName = '';
        set<string> approverSet = new set<string>();
        try{
            system.debug('ApprovalQsrmController ====:action '+action);
            list<string> approverStatus = new list<string>();
            list<string> rejectStatus = new list<string>();
            list<string> pendingStatus = new list<string>();
            list<ProcessInstance> processHistoryList = [SELECT Id,Status  FROM ProcessInstance WHERE TargetObjectId =: newQsrmlst[0].id];
            system.debug('ApprovalQsrmController=='+processHistoryList.size());
            for(ProcessInstance pi : processHistoryList){
                if(pi.Status =='Approved'){
                    approverStatus.add(pi.Status);
                }else if(pi.Status =='Rejected'){
                    rejectStatus.add(pi.Status);
                }else if(pi.Status =='pending'){
                    pendingStatus.add(pi.Status);
                }  
            }
            system.debug('ApprovalQsrmController===approverStatus:'+approverStatus+':==rejectStatus==:'+rejectStatus+'======pendingStatus====='+pendingStatus);
            system.debug('ApprovalQsrmController==processHistoryList    '+processHistoryList.size());
            system.debug('ApprovalQsrmController==approverStatus    '+approverStatus.size());
            system.debug('ApprovalQsrmController==pendingStatus    '+pendingStatus.size());
            //  if((processHistoryList.size() < 1 || processHistoryList[0].Status =='Approved' || processHistoryList[0].Status =='Removed')){
            if((processHistoryList.size() < 1 || processHistoryList[0].Status =='Approved' || processHistoryList[0].Status =='Removed') &&  approverStatus.size() < 2 && pendingStatus.size() < 1){
                Boolean AutoApprove = false;
                system.debug('ApprovalQsrmController===:'+newQsrmlst.size()+':approval==:'+newQsrmlst);
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                id approvarIdVertical;
                id approvarIdSales;
                for(QSRM__c qsrm : newQsrmlst){
                    QsrmId += qsrm.Id;
                    QsrmIdAutonumber +=qsrm.name;// Service_Line_Leaders__c,Vertical_Leaders__c
                    system.debug('ApprovalQsrmController====qsrm.QSRM_Type__c=:'+qsrm.QSRM_Type__c);
                    if(qsrm.QSRM_Type__c == 'TSQSRM'){
                        approverSet.add(qsrm.Vertical_Leaders__c);
                        approverSet.add(qsrm.Service_Line_Leaders__c);
                        system.debug('ApprovalQsrmController=====approverSet:'+approverSet.size() +':===approverSet=: '+approverSet);
                        system.debug('ApprovalQsrmController=====qsrm.Status__c:'+qsrm.Status__c);
                       system.debug('ApprovalQsrmController=====:'+datetime.now());
                        if((qsrm.Status__c =='In Approval'  ||  qsrm.Service_Line_leader_SL_Approval__c == 'Approved')){
                            // if(){}
                            system.debug('ApprovalQsrmController=====:');
                            system.debug(':Approval===:'+QsrmId);
                            approvarIdVertical = qsrm.Vertical_Leaders__c;//'00590000001ICb5';//qsrm.Vertical_Leaders__c;
                            system.debug(':Approval===:'+approvarIdVertical);
                            approvarIdSales = qsrm.Service_Line_Leaders__c;//'0059000000UIOdT';//qsrm.Service_Line_Leaders__c;
                            system.debug(':Approval===:'+approvarIdSales);
                            req1.setComments('Submitting request for approval.');
                            req1.setObjectId(qsrm.id);
                            req1.setSubmitterId(qsrm.CreatedById );
                            req1.setSkipEntryCriteria(true);
                            if(qsrm.Opportunity_TCV__c < 100000 ){
                                system.debug(':Approval-----:'+qsrm.Opportunity_TCV__c);
                                if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c >= 16){
                                    system.debug('ApprovalQsrmController=====1:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'Auto Approval Email To VTL';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                } else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c > 10 && qsrm.Deliverability_Score__c >= 16){
                                    system.debug('ApprovalQsrmController=====2:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'QSRM Email to VTL (TCV >= 100K)';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                }else if(qsrm.Overall_Score__c <10 && qsrm.Deliverability_Score__c >= 16){
                                    system.debug('ApprovalQsrmController=====3:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'Auto Reject Email To VTL';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                }else if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c <= 8){
                                    system.debug('ApprovalQsrmController=====4:');
                                    VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template For SL';  req1.setProcessDefinitionNameOrId('TS_QSRM_ApprovalToTS'); req1.setNextApproverIds(new Id[] {approvarIdSales});
                                }else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c >= 10 && qsrm.Deliverability_Score__c <= 8){
                                    system.debug('ApprovalQsrmController=====5:');
                                    SalesLineLeader = approvarIdSales;
                                    EmailTemplateName = 'QSRM Email Template For SL';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_ApprovalToTS');
                                    req1.setNextApproverIds(new Id[] {approvarIdSales});
                                }else if(qsrm.Overall_Score__c <10 && qsrm.Deliverability_Score__c <= 8){
                                    system.debug('ApprovalQsrmController=====6:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'Auto Reject Email To VTL';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                }else if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c <= 14 && qsrm.Deliverability_Score__c >= 10){
                                    system.debug('ApprovalQsrmController=====7:');
                                    VerticalLeader = approvarIdVertical; SalesLineLeader = approvarIdSales; EmailTemplateName = 'Auto Approval Email To VTL'; req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1'); req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                } else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c >= 10 && qsrm.Deliverability_Score__c <= 14 && qsrm.Deliverability_Score__c >= 10){
                                    system.debug('ApprovalQsrmController=====9:');
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;
                                        EmailTemplateName = 'QSRM Email Template To VTL Post SLL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template To VTL Post SLL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        SalesLineLeader = approvarIdSales;
                                        EmailTemplateName = 'QSRM Email Template For SL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                }else if(qsrm.Overall_Score__c <10 && qsrm.Deliverability_Score__c <= 14 && qsrm.Deliverability_Score__c >= 10){
                                    system.debug('ApprovalQsrmController=====8:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'Auto Reject Email To VTL';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    
                                } 	
                                
                            }else if(qsrm.Opportunity_TCV__c >= 100000){
                                
                                system.debug(':Approval--else---:'+qsrm.Opportunity_TCV__c);
                                system.debug(':Approval-----:'+qsrm.Opportunity_TCV__c);
                                if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c >= 16){
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'QSRM Email to VTL (TCV >= 100K)';
                                    system.debug('ApprovalQsrmController=====1:');
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                } else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c >= 10 && qsrm.Deliverability_Score__c >= 16){
                                    system.debug('ApprovalQsrmController=====2:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'QSRM Email to VTL (TCV >= 100K)';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    
                                }else if(qsrm.Overall_Score__c <10 && qsrm.Deliverability_Score__c >= 16 ){
                                    system.debug('ApprovalQsrmController=====3:');
                                    VerticalLeader = approvarIdVertical;
                                    EmailTemplateName = 'QSRM Email to VTL (TCV >= 100K)';
                                    req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                    req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                }else if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c  <=8 ){
                                    system.debug('ApprovalQsrmController=====4:');
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;  EmailTemplateName = 'QSRM Email Template To VTL Post SLL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template To VTL Post SLL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales; EmailTemplateName = 'QSRM Email Template For SL'; req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                }else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c >= 10 && qsrm.Deliverability_Score__c <= 8){
                                    system.debug('ApprovalQsrmController=====5:');
                                    
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;
                                        EmailTemplateName = 'QSRM Email Template To VTL Post SLL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;  EmailTemplateName = 'QSRM Email Template To VTL Post SLL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales;
                                        EmailTemplateName = 'QSRM Email Template For SL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                }else if(qsrm.Overall_Score__c < 10 && qsrm.Deliverability_Score__c <= 8){
                                    system.debug('ApprovalQsrmController=====6:'); 
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;
                                        EmailTemplateName = 'QSRM Email Template To VTL Post SLL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====6:');
                                        VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template To VTL Post SLL'; req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales;
                                        EmailTemplateName = 'QSRM Email Template For SL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                }else if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c >= 10 && qsrm.Deliverability_Score__c <= 14){
                                    system.debug('ApprovalQsrmController=====7:');
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template To VTL Post SLL'; req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;  EmailTemplateName = 'QSRM Email Template To VTL Post SLL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1'); req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales;  EmailTemplateName = 'QSRM Email Template For SL';  req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');  req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                } else if(qsrm.Overall_Score__c < 18 && qsrm.Overall_Score__c >= 10 && qsrm.Deliverability_Score__c >= 10 && qsrm.Deliverability_Score__c <= 14){
                                    system.debug('ApprovalQsrmController=====9:');
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;
                                        EmailTemplateName = 'QSRM Email Template To VTL Post SLL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else  if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;  EmailTemplateName = 'QSRM Email Template To VTL Post SLL';   req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1'); req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales;
                                        EmailTemplateName = 'QSRM Email Template For SL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                }else if(qsrm.Overall_Score__c <=10 && qsrm.Deliverability_Score__c >= 10 && qsrm.Deliverability_Score__c <= 14){
                                    system.debug('ApprovalQsrmController=====8:');
                                    if(approverSet.size() < 2){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical;
                                        EmailTemplateName = 'QSRM Email Template To VTL Post SLL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else  if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                                        system.debug('ApprovalQsrmController=====4:');
                                        VerticalLeader = approvarIdVertical; EmailTemplateName = 'QSRM Email Template To VTL Post SLL'; req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_Process1'); req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }else{
                                        system.debug('ApprovalQsrmController=====:');
                                        SalesLineLeader = approvarIdSales;
                                        EmailTemplateName = 'QSRM Email Template For SL';
                                        req1.setProcessDefinitionNameOrId('TS_QSRM_Approval_ProcessSlToVertical');
                                        req1.setNextApproverIds(new Id[] {approvarIdVertical});
                                    }
                                } 
                            }
                        }
                    }
                    system.debug('ApprovalQsrmController=====:'+req1);
                    //if(req1 != null)
                    if(req1.getSkipEntryCriteria() != null){
                        Approval.ProcessResult result = Approval.process(req1);
                        sendReworkQSRMEmailTemplate(SalesLineLeader,VerticalLeader,EmailTemplateName,QsrmId);
                        system.debug('ApprovalQsrmController====result=:'+result);
                    }
                }
            } 
        }catch (exception e){
            system.debug('ApprovalQsrmController=:error msg '+e.getMessage()+':====error line== :'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('Dynamic Approval Process QSRM',e.getMessage(), QsrmId, e.getStackTraceString(),'ApprovalQsrmController', 'approvalMethod','Fail','',String.valueOf(e.getLineNumber()));  
        }
        
    }    
    
    //ApprovalQsrmController.sendReworkQSRMEmailTemplate('0059000000UIOdT','00590000009RwkwAAC','Auto Reject Email To VTL','a075D000002dA1pQAE');
    @InvocableMethod
    public static void autoMailappreject(list<qsrm__c> qsrmlst){
       system.debug('ApprovalQsrmController ==== '+qsrmlst);
        String qsrmId ='';
        try{
            String sll ='';
            String ownerid ='';
            String Etemp ='';
            if(qsrmlst.size() > 0){
            List<ProcessInstanceWorkitem> pw=[SELECT id,actorId FROM ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:qsrmlst[0].id AND ProcessInstance.status ='Pending' limit 1];
          
            for(qsrm__c qsrm : qsrmlst){
                if(qsrm.Opportunity_TCV__c < 100000){
                    if(qsrm.Overall_Score__c >= 18 && qsrm.Deliverability_Score__c >= 8){
                        if(qsrm.Status__c == 'approved'){
                            ownerid = qsrm.CreatedById;  Etemp ='Email To Seller when QSRM is approved';   qsrmId = qsrm.Id;
                        }
                    }else if(qsrm.Overall_Score__c <= 10){
                        if(qsrm.Status__c == 'Rejected'){
                            ownerid = qsrm.CreatedById;  Etemp ='Email Alert to Seller when QSRM is rejected';   qsrmId = qsrm.Id;
                        }
                    }
                }
                if(qsrm.QSRM_Type__c == 'TSQSRM' && qsrm.Status__c == 'In Approval' && qsrm.ReminderMailFlag__c =='SendReminder'){
                    system.debug('ApprovalQSRmController send reminder Mail');
                    system.debug('ApprovalQSRmController send reminder Mail pending size'+pw.size());
                    system.debug('ApprovalQSRmController send reminder Mail approve id'+pw);
                    if(pw.size() > 0){
                        ownerid = pw[0].actorId;
                        Etemp ='Reminder Email To VTL and SLL'; 
                        qsrmId = qsrm.Id;
                    }
                }
            }
            if(Etemp != '' && qsrmId != '' && ownerid != ''){
                system.debug('ApprovalQSRmController===call send email method=: ');
                sendReworkQSRMEmailTemplate(sll,ownerid,Etemp,qsrmId);
                system.debug('ApprovalQSRmController===call send email method=: ');
            }
            system.debug('ApprovalQsrmController == sll '+sll+':==ownerid==: '+ownerid+':===Etemp== '+Etemp+':===qsrmId== '+qsrmId);
            } 
        }catch (exception e){
            system.debug('ApprovalQsrmController=:error msg '+e.getMessage()+':====error line== :'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('Dynamic Approval Process QSRM',e.getMessage(), qsrmId, e.getStackTraceString(),'ApprovalQsrmController', 'autoMailappreject','Fail','',String.valueOf(e.getLineNumber()));  
        }
    }
    
    public static void sendReworkQSRMEmailTemplate(string sll,string vtl,string etem,string QsrmId){
        try{
            system.debug('ApprovalQsrmController==sll=:'+sll+':--vtl==:'+vtl+':---etem===:'+etem+':---qsrmId===:'+QsrmId);
            qId = QsrmId;
            loadDataInTemplate(qId);
            List<String> recipientAddress = new List<String>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            
            if(sll != null && sll != '' && vtl!= null &&  vtl != ''){
                mail.setTargetObjectId(vtl);
                User usr = [Select id, Email from User Where id =: sll ];
                recipientAddress.add(usr.Email);
                mail.setCcAddresses(recipientAddress);
            }else{
                if(sll != null && sll != ''){
                    mail.setTargetObjectId(sll);
                } else if(vtl!= null &&  vtl != ''){
                    mail.setTargetObjectId(vtl);
                }
            }
            
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'ts.qsrm@genpact.com'];
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
            mail.setReplyTo('qsrmrework@5-33ox65eaysjndtecadvnoo6k1qfx4sb8xs7y9yj6a4645h9o6x.5d-16jnuay.cs72.apex.sandbox.salesforce.com');
            mail.setUseSignature(false); 
            mail.setBccSender(false); 
            mail.setSaveAsActivity(false); 
            EmailTemplate et=[Select id from EmailTemplate where Name =: etem]; 
            mail.setTemplateId(et.id); 
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            system.debug('ApprovalQsrmController==== SendEmailResult ==='+r);
        }catch(Exception ex){
            system.debug('ApprovalQsrmController  error msg'+ex.getMessage()+ 'ApprovalQsrmController  Trace===>' + ex.getCause()+ 'ApprovalQsrmController error LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
        }
    }
    //ApprovalQsrmController.loadDataInTemplate('a075D000002eIbV');
    public static void loadDataInTemplate(string qId){
        
        system.debug('qId====>'+qId);
        QSRMObjList = [select Id,Name,Deal_Type__c,Service_Line_leader_SL_Approval__c,Account_Name__c,Service_Line_Leaders__r.name,Vertical_Leaders__r.name,Opportunity__c,CreatedDate,Account_Priority_from_Account__c, 
                       What_is_our_track_record_at_this_client__c,Does_this_project_align_with_the_client__c,Does_the_client_rate_card_or_proposed_c__c,
                       In_case_of_non_named_account__c,Alignment_to_Priorities_Score__c,Alignment_to_Priorities_Color__c,Does_the_client_have_an_approved_budget__c,
                       Is_there_a_clear_client_sponsor_decision__c,Has_the_client_sponsor_decision_maker__c,Do_we_understand_the_client__c,Intent_to_Buy_Score__c,
                       Intent_to_Buy_Color__c,How_strong_is_our_value_propostion_and_w__c,Do_we_have_the_right_relationship_streng__c,How_well_does_our_offering_fit__c,
                       Do_we_know_our_competition__c,Potential_to_Win_Score__c,Potential_to_win_Color__c,Do_we_have_the_right_level_of_domain__c,What_is_the_commercial_model_expected_to__c,
                       Can_the_solution_be_delivered_by_each__c,Deliverability_Score__c,Deliverability_Color__c,Overall_Score__c,Overall_Rating_TS_QSRM__c,Alignment_to_Priorities_Color_Formula__c,
                       Potential_to_Win_Color_Formula__c,Intent_to_Buy_Color_Formula__c,Deliverability_Color_Formual__c,Overall_Rating_Color_Formula__c
                       From QSRM__c where id =: qId limit 1];
        if(QSRMObjList.size() > 0){
            System.debug('ApprovalQsrmController\n\n == QSRMObjList '+QSRMObjList);
            
            System.debug('ApprovalQsrmController\n\n == QSRMObjList Opportunity ID '+QSRMObjList[0].Opportunity__c);
            
            OpptyList = [select Id,Name,Description,Summary_of_opportunity__c,Current_challenges_client_facing__c,End_objective_the_client_trying_to_meet__c,Win_Theme__c From Opportunity where Id=: QSRMObjList[0].Opportunity__c];  
            
            System.debug('ApprovalQsrmController\n\n == OpptyList '+OpptyList);
            
            oliList = [SELECT Id, TCV__c,Opportunity.Pricer_Name__r.Name,Nature_of_Work__c,Product_Name__c,Service_Line__c,Type_of_Deal__c,vic_Contract_Term__c FROM OpportunityLineItem Where opportunityId =: QSRMObjList[0].Opportunity__c   limit 10];
            System.debug('ApprovalQsrmController\n\n == oliList '+oliList);
            
            ProcessInstanceList = [SELECT Id, Status, SubmittedBy.Name,CompletedDate,CreatedDate,CreatedBy.Name,TargetObjectId,TargetObject.Name, 
                                   (SELECT Comments  FROM StepsAndWorkitems  ORDER BY CreatedDate DESC LIMIT 9),(SELECT Id, StepStatus, Comments  FROM Steps  ORDER BY CreatedDate DESC )FROM ProcessInstance WHERE TargetObjectId =:QSRMObjList[0].id And Status IN : statusList ORDER BY CreatedDate DESC limit 1];
            
            if(QSRMObjList[0].Service_Line_leader_SL_Approval__c == 'Approved'){
               list<ProcessInstance> PList = [select id,  (SELECT Comments  FROM StepsAndWorkitems  ORDER BY CreatedDate DESC LIMIT 9) FROM ProcessInstance WHERE TargetObjectId =:QSRMObjList[0].id And Status IN : statusList ORDER BY CreatedDate DESC limit 2];
                for(ProcessInstance pi : PList){
                    for(ProcessInstanceHistory pih :pi.StepsAndWorkitems){
                        if(pih.Comments != 'Submitting request for approval.' && pih.Comments != null){
                            firstlevelComments = pih.Comments;
                            system.debug(':=======history====:'+pih.Comments);
                            break;

                         }
                    }
                }
                
            }
            System.debug('ApprovalQsrmController\n\n -----------ProcessInstanceList '+ProcessInstanceList);
        }else{
            system.debug('ApprovalQsrmController === No QSRM in list ');
        }
    }
}