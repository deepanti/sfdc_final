@IsTest
public class OnchageSuperSl_Test {
    public static Opportunity opp;
    public static Account oAccount;
    public static OpportunitySplit opp_split;
    public static User utest,utest4, utest5, utest6, utest7,utest8,utest9,utest10,utest11;
    public static OpportunitySplitType oppSplitType;
    public static OpportunityTeamMember opp_team_member;
    public static OpportunityTeamMember opp_team_member1;
    public static OpportunityLineItem opp_item;
    public static List<AggregateResult> oliList;
    public static  DECIMAL total_used;
    public static  DECIMAL totalTCV;
    
    
    
    public static testMethod void insertTestMethod1(){  
        test.startTest();
        setupTestData();
        Map<ID, Account> old_acc_map = new Map<ID, Account>();
        old_acc_map.put(oAccount.ID, oAccount);
        
        oAccount.Client_Partner__c=utest8.id;
        update oAccount;
        
        Map<ID, Account> new_acc_map = new Map<ID, Account>();
        new_acc_map.put(oAccount.ID, oAccount);
        
        OnchageSuperSl.UpdateSuperSl(new_acc_map,old_acc_map); 
        test.stopTest();
    }

    
    public static void setupTestData(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        utest =GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
        utest4 =GEN_Util_Test_Data.CreateUser('standarduser2020@testorg.com',p.Id,'standardusertestgen2020@testorg.com' );
        utest5 =GEN_Util_Test_Data.CreateUser('standarduser2021@testorg.com',p.Id,'standardusertestgen2021@testorg.com' );
        utest6 =GEN_Util_Test_Data.CreateUser('standarduser2022@testorg.com',p.Id,'standardusertestgen2022@testorg.com' );
        utest7 =GEN_Util_Test_Data.CreateUser('standarduser2023@testorg.com',p.Id,'standardusertestgen2023@testorg.com' );
        utest8 =GEN_Util_Test_Data.CreateUser('standarduser2024@testorg.com',p.Id,'standardusertestgen2024@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Testing Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Testing Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('Testing Test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 4',Sales_Leader__c = utest4.id);
        insert salesunit;
        Sales_Unit__c salesunit1 = new Sales_Unit__c(Name='Sales Unit 5',Sales_Leader__c = utest8.id);
        insert salesunit1;
        //salesunit.Sales_Leader__c
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),utest4.id,oAT.Id,'Testing Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
       
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'Testing Test','Cross-Sell',
                                                            'test1212@xyz.com','99999999999');
        
        System.runAs(utest4){
            
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1,OwnerID=utest8.id,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive',Super_SL_overlay_created__c=False,SL_overlay_created__c=false,Target_Source__c='Ramp up');
            insert opp;
            
            Product2 product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = utest4.ID,
                                                                   unitPrice = 100.00, TCV__c = 100); 
            insert opp_item;
            
            oppSplitType = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
            opp_split = new OpportunitySplit(SplitPercentage =100,SplitTypeId =oppSplitType.id, SplitOwnerId =utest6.id,OpportunityId =opp.id);
            insert opp_split;
            opp_team_member = new OpportunityTeamMember(OpportunityAccessLevel ='Edit',UserId =utest5.id, TeamMemberRole ='Super SL',OpportunityId=opp.id);
            insert opp_team_member;
            
            
        }
    }
    
}