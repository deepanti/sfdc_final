/**
* @author: Shivam Singh
* @date: March 2018
*/

@isTest
public class vic_GenetareBonusLetterTracker {
    static testMethod void validatevic_GenetareBonusLetterTracker() {
        vic_GenetareBonusLetter.getFinancialYear();
        vic_GenetareBonusLetter.getYearForDisplay();
        vic_GenetareBonusLetter.getCurrentYear();
        
        Master_VIC_Role__c masterVICRoleObj = vic_UtilTracker.getMasterVICRole();
        insert masterVICRoleObj;
        
        vic_GenetareBonusLetter.getVicRole();
        
        User_VIC_Role__c userVICRole = vic_UtilTracker.getUserVICRole();
        userVICRole.Master_VIC_Role__c=masterVICRoleObj.Id;
        insert userVICRole;
        
        Target__c targetObj = vic_UtilTracker.getTarget();
        insert targetObj;
        list<Id> listTargetObj = new list<Id>();
        listTargetObj.add(targetObj.Id);
        
        Target_Component__c targetComponentObj = vic_UtilTracker.getTargetComponent();
        targetComponentObj.Target__c = targetObj.Id;
        insert targetComponentObj;
        
        Target_payout__c targetPayout = vic_UtilTracker.getTargetPayout();
        targetPayout.Component__c = targetComponentObj.Id;
        insert targetPayout;
        
        vic_GenetareBonusLetter.getSearchItems(masterVICRoleObj.Id, 2017, '', 'Yearly', null, null, 2017);
        vic_GenetareBonusLetter.getSearchItems('All VIC Role', 2017, '', 'Yearly', null, null, 2017);
        vic_GenetareBonusLetter.getSearchItems(masterVICRoleObj.Id, 2017, 'Canada', 'Yearly', null, null, 2017);
        vic_GenetareBonusLetter.getSearchItems('All VIC Role', 2017, 'Canada', 'Yearly', null, null, 2017);
        vic_GenetareBonusLetter.getCongaUrlForBonusLetter(listTargetObj, String.valueOf(system.today()), String.valueOf(system.today()));
    }

}