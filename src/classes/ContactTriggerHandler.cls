/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/26/2018
@Description :this class is used to update fields(Vertical__c and Archtype__c) value from Account object 


@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */

public with sharing class ContactTriggerHandler{
    
     //this method is to update field(Industry vertical and Archtype) from Account object 
     //to contact field(Vertical__C and Archtype__C) using before Insert event
    public void AccToConFieldUpdate(Contact[] newContact)
    {
        try{
            system.debug('..Inside contactTriggerHandler before Insert..');
            for(Contact con:newContact)
            {
                if(con.AccountId!=NULL)
                {
                    //Account_Archetype__c and Industry_Vertical__c are formulla fields
                    con.Archetype__c=con.Account_Archetype__c;
                    con.Vertical__c=con.Industry_Vertical__c;
                }
            }
        }
        catch(Exception e)
        {system.debug('..We have got an Error at'+String.valueOf(e.getLineNumber())+' '+e.getMessage());
        }
        
    }
    //this method is to update field(Industry vertical and Archtype) from Account object 
     //to contact field(Vertical__C and Archtype__C) using before update event
    public void AccToConFieldUpdate(Contact[] newContact,Map<Id,Contact> oldContact)
    {
        try{
            system.debug('..Inside contactTriggerHandler before Update..');
            for(Contact con:newContact)
            {
                Contact oldCon=oldContact.get(con.Id);
                if(con.AccountId!=NULL&&oldCon.AccountId!=con.AccountId)
                {
                    //Account_Archetype__c and Industry_Vertical__c are formulla fields
                    con.Archetype__c=con.Account_Archetype__c;
                    con.Vertical__c=con.Industry_Vertical__c;
                    
                }
      
            }
        }
        catch(Exception e)
        { system.debug('..We have got an Error at'+String.valueOf(e.getLineNumber())+' '+e.getMessage());
        }
    }
}