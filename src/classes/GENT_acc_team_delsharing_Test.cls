@isTest
private class GENT_acc_team_delsharing_Test
{
    public static testMethod void RunTest()
    {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];  
            User u =GEN_Util_Test_Data.CreateUser('standarduser201566@testorg.com',p.Id,'standardusertestgen201566@testorg.com' );
            User u1 =GEN_Util_Test_Data.CreateUser('standarduser2015166@testorg.com',p.Id,'standardusertestgen2015166@testorg.com' );
            Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
            Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
            Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test123');
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
            //AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
            Contact oContact = GEN_Util_Test_Data.CreateContact('Fistname','lastname',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
            Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
            Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Yahoo');
            OpportunityProduct__c oppp=GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,5,1000);
            Archetype__c arrchetype = GEN_Util_Test_Data.CreateArchetype('Archname');    
            AccountArchetype__c accountarch= GEN_Util_Test_Data.createNewAccountArchetype(u1.id,oAccount.Id,arrchetype.Id); 
            OpportunityProduct__Share oshare=GEN_Util_Test_Data.createShareOpportunityProduct(oppp.Id,u.Id);
            delete accountarch;
}
}