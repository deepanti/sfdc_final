@isTest
public class GPSelectorProductMasterTracker {
    static GP_Product_Master__c productMaster;
    
	@testSetup
    static void buildDependencyData() {
        productMaster = new GP_Product_Master__c();
        insert productMaster;
    }
    
    @isTest 
    static void testProductMasterSelector() {
        fetchData();
        GPSelectorProductMaster productMasterSelector = new GPSelectorProductMaster();
        productMasterSelector.selectById(productMaster.Id);
        productMasterSelector.selectById(new Set<Id> {productMaster.Id});
        GPSelectorProductMaster.getProductMaster(null);
        GPSelectorProductMaster.getAllProductMaster(null);
        GPSelectorProductMaster.getAllProductMaster();
		// Mass Update Change
        new GPSelectorProductMaster().getProductMasterDataForMassUpdate(new Set<String>{'Product Name'},
                                                                  new Set<String>{'Nature of Work'}, 
                                                                  new Set<String>{'Service Line'});
    }
    
    static void fetchData() {
        productMaster = [Select Id from GP_Product_Master__c LIMIT 1];
    }
}