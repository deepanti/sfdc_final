@isTest
public class GPSelectorOpportunityProductTracker {
    @isTest
    public static void testgetSObjectType() {
        GPSelectorOpportunityProduct opportunityProductSelector = new GPSelectorOpportunityProduct();
        Schema.SObjectType returnedType = opportunityProductSelector.getSObjectType();
        Schema.SObjectType expectedType = OpportunityProduct__c.getSobjectType();
        
        opportunityProductSelector.selectById(new Set<Id>());
        opportunityProductSelector.selectOppproduct(null);
    	opportunityProductSelector.selectupdateOppproduct(null, null);
    	// 04-04-19:DS Modified GPBatchCreateDeals.
        opportunityProductSelector.fetchOLIsByOppIds(null);
    }
}