public class GPDomianOpportunityProjectAddress extends fflib_SObjectDomain {

    public GPDomianOpportunityProjectAddress(List < GP_Opp_Project_Address__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomianOpportunityProjectAddress(sObjectList);
        }
    }

    //SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] { GP_Opp_Project_Address__c.SobjectType };

    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateBilltoShiptAddress(null);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateIsUpdatedOnOpportunityProject(oldSObjectMap);
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateBilltoShiptAddress(oldSObjectMap);
        updateIsUpdatedOnChange(oldSObjectMap);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the isUpdated Checkbox on GP_Opp_Project_Leadership__c          
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------    
    private void updateIsUpdatedOnChange(Map < Id, SObject > oldSObjectMap) {
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Opp_Project_Address__c objOppProjectAddress: (List < GP_Opp_Project_Address__c > ) Records) {
            GP_Opp_Project_Address__c objOldOppProjectAddress = (GP_Opp_Project_Address__c) oldSObjectMap.get(objOppProjectAddress.id);
            // change in LeaderShipRole
            if (!String.isBlank(objOppProjectAddress.GP_Account_Id__c) && oldSObjectMap != null &&
                objOldOppProjectAddress.GP_Account_Id__c != objOppProjectAddress.GP_Account_Id__c) {
                objOppProjectAddress.GP_IsUpdated__c = true;
            }
            // change in Employee
            if (!String.isBlank(objOppProjectAddress.GP_Bill_To_Address_Id__c) && oldSObjectMap != null &&
                objOldOppProjectAddress.GP_Bill_To_Address_Id__c != objOppProjectAddress.GP_Bill_To_Address_Id__c) {
                objOppProjectAddress.GP_IsUpdated__c = true;
            }
            // change in StartDate
            if (!String.isBlank(objOppProjectAddress.GP_Relationship__c) && oldSObjectMap != null &&
                objOldOppProjectAddress.GP_Relationship__c != objOppProjectAddress.GP_Relationship__c) {
                objOppProjectAddress.GP_IsUpdated__c = true;
            }
            // change in EndDate
            if (!String.isBlank(objOppProjectAddress.GP_Ship_To_Address_Id__c) && oldSObjectMap != null &&
                objOldOppProjectAddress.GP_Ship_To_Address_Id__c != objOppProjectAddress.GP_Ship_To_Address_Id__c) {
                objOppProjectAddress.GP_IsUpdated__c = true;
            }
        }
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the isUpdated Checkbox on Opportunity Project          
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------    
    private void updateIsUpdatedOnOpportunityProject(Map < Id, SObject > oldSObjectMap) {
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Opp_Project_Address__c objOppProjectAddress: (List < GP_Opp_Project_Address__c > ) Records) {
            GP_Opp_Project_Address__c objOldOppProjectAddress = (GP_Opp_Project_Address__c) oldSObjectMap.get(objOppProjectAddress.id);
            // change in IsUpdated CheckBox
            if (objOppProjectAddress.GP_IsUpdated__c && oldSObjectMap != null && !objOldOppProjectAddress.GP_IsUpdated__c && objOppProjectAddress.GP_Opp_Project__c != null) {
                setOppProjectId.add(objOppProjectAddress.GP_Opp_Project__c);
            }
        }
        if (setOppProjectId.size() > 0) {
            list < GP_Opportunity_Project__c > lstOpportunityProject = new list < GP_Opportunity_Project__c > ();
            for (id OpportunityProjectID: setOppProjectId) {
                lstOpportunityProject.add(new GP_Opportunity_Project__c(id = OpportunityProjectID, GP_IsUpdated__c = true));
            }
            update lstOpportunityProject;
        }
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the bill to Address and Ship to Address        
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------    

    private void updateBilltoShiptAddress(Map < Id, SObject > oldSObjectMap) {
        GP_Opp_Project_Address__c objOldOppProjectAddress;
        set < string > setAddressID = new set < string > ();
        for (GP_Opp_Project_Address__c objOppProjectAddress: (List < GP_Opp_Project_Address__c > ) Records) {
            objOldOppProjectAddress = oldSObjectMap != null ? (GP_Opp_Project_Address__c) oldSObjectMap.get(objOppProjectAddress.id) : null;

            if (!String.IsBlank(objOppProjectAddress.GP_Bill_To_Address_Id__c) && (oldSObjectMap == null ||
                    objOppProjectAddress.GP_Bill_To_Address_Id__c != objOldOppProjectAddress.GP_Bill_To_Address_Id__c)) {
                setAddressID.add(objOppProjectAddress.GP_Bill_To_Address_Id__c);
            }

            if (!String.IsBlank(objOppProjectAddress.GP_Bill_To_Address_Id__c) && (oldSObjectMap == null ||
                    objOppProjectAddress.GP_Bill_To_Address_Id__c != objOldOppProjectAddress.GP_Bill_To_Address_Id__c)) {
                setAddressID.add(objOppProjectAddress.GP_Bill_To_Address_Id__c);
            }
        }

        if (setAddressID != null && setAddressID.size() > 0) {
            list < GP_Address__c > lstAddress = GPSelectorAddress.getAddressBasedOnOracleId(setAddressID);
            map < string, id > mapOracleIDToSFDCId = new map < string, id > ();
            for (GP_Address__c objAddress: lstAddress) {
                if (!String.isBlank(objAddress.GP_ADDRESS_ID__c)) {
                    mapOracleIDToSFDCId.put(objAddress.GP_ADDRESS_ID__c, objAddress.id);
                }
            }

            for (GP_Opp_Project_Address__c objOppProjectAddress: (List < GP_Opp_Project_Address__c > ) Records) {
                objOldOppProjectAddress = oldSObjectMap != null ? (GP_Opp_Project_Address__c) oldSObjectMap.get(objOppProjectAddress.id) : null;

                if (!String.IsBlank(objOppProjectAddress.GP_Bill_To_Address_Id__c) && (oldSObjectMap == null ||
                        objOppProjectAddress.GP_Bill_To_Address_Id__c != objOldOppProjectAddress.GP_Bill_To_Address_Id__c)) {
                    if (mapOracleIDToSFDCId.get(objOppProjectAddress.GP_Bill_To_Address_Id__c) != null) {
                        objOppProjectAddress.GP_Bill_To_Address__c = mapOracleIDToSFDCId.get(objOppProjectAddress.GP_Bill_To_Address_Id__c);
                    }
                }

                if (!String.IsBlank(objOppProjectAddress.GP_Bill_To_Address_Id__c) && (oldSObjectMap == null ||
                        objOppProjectAddress.GP_Bill_To_Address_Id__c != objOldOppProjectAddress.GP_Bill_To_Address_Id__c)) {

                    if (mapOracleIDToSFDCId.get(objOppProjectAddress.GP_Bill_To_Address_Id__c) != null) {
                        objOppProjectAddress.GP_Ship_To_Address__c = mapOracleIDToSFDCId.get(objOppProjectAddress.GP_Bill_To_Address_Id__c);
                    }
                }
            }
        }
    }
}