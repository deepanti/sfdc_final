/**
* @author: Prashant Kumar1
* @since: 28/05/2018
* @description: This batch class calculates incentive for each user on Opportunity product.
* These incentives are based on plan and plan component and inserted as a child of target component.
* This class also updates opportunity product marking that icentive is calculated. It also update Target Object
* of user on opprtunity product IO/TS values for profitable IO/TS components.
* 
* isAnniversaryCal is true for Anniversary caluclation of BDE plan for new profitable booking IO component
* and this value is true only while calling from screen
*/
public class VIC_CalculateUpfrontIncentivesBatch implements Database.Batchable<sObject>, Database.stateful {
    
    @TestVisible
    private Boolean isAnniversaryCal;
    public String strOLIId {get; set;}
    public VIC_CalculateUpfrontIncentivesBatch(Boolean isAnniversaryCal){
        this.isAnniversaryCal = isAnniversaryCal;
        strOLIId = null;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        //String dealType = VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('Deal Type');
        String strCPQDealStatus = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('CPQ_Deal_Status');
        String opportunityStagename = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Eligible_Stage_For_VIC');
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        Date processingYearEnd =  processingYearStart.addYears(1).addDays(-1);       
        
        if(this.isAnniversaryCal){
            if(strOLIId != null && strOLIId != ''){
                return Database.getQueryLocator([
                    SELECT Id, Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, Opportunity.Actual_Close_Date__c, Opportunity.CloseDate,Opportunity.VIC_Total_CYR_IO__c,
                    VIC_NPV__c, vic_Contract_Term__c, Contract_Term__c,  Opportunity.Id,
                    CYR__c, TCV__c, Opportunity.OwnerId, Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c, Opportunity.StageName,
                    vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c, vic_VIC_User_3_Split__c, vic_is_CPQ_Value_Changed__c,
                    vic_VIC_User_4_Split__c, VIC_IsIncentiveCalculatedForPrimaryRep__c, VIC_IsIncentiveCalculatedForBDRep__c,
                    vic_Sales_Rep_Approval_Status__c, vic_Product_BD_Rep_Approval_Status__c,
                    Opportunity.AccountId,
                    Opportunity.Sales_country__c,
                    Opportunity.vic_Total_Renewal_TCV__c,
                    Opportunity.Account.Business_Group__c, Opportunity.Account.Business_Segment__r.Name ,
                    vic_Old_TCV_Spilt_For_Sales_Rep__c,vic_Old_TCV_Spilt_For_Product_BD_Rep__c,
                    vic_Old_TCV_Spilt_For_User_3__c,vic_Old_TCV_Spilt_For_User_4__c,vic_TCV_Accelerator_Variable_Credit__c,
                    Opportunity.VIC_TCV_USD_Sum__c, Opportunity.Opportunity_Source__c, Opportunity.Account.Hunting_Mining__c
                    FROM OpportunityLineItem 
                    WHERE Opportunity.StageName =: opportunityStagename
                    AND Opportunity.Actual_Close_Date__c >= :processingYearStart
                    AND Opportunity.Actual_Close_Date__c <= :processingYearEnd
                    AND vic_CPQ_Deal_Status__c =: strCPQDealStatus AND vic_Final_Data_Received_From_CPQ__c = true 
                    AND (
                        vic_Sales_Rep_Approval_Status__c = 'Approved' OR vic_Product_BD_Rep_Approval_Status__c = 'Approved'
                    )
                    AND Id = :strOLIId
                ]);
            }else{
                            return Database.getQueryLocator([
                    SELECT Id, Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, Opportunity.Actual_Close_Date__c, Opportunity.CloseDate,Opportunity.VIC_Total_CYR_IO__c,
                    VIC_NPV__c, vic_Contract_Term__c, Contract_Term__c,  Opportunity.Id,
                    CYR__c, TCV__c, Opportunity.OwnerId, Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c, Opportunity.StageName,
                    vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c, vic_VIC_User_3_Split__c, vic_is_CPQ_Value_Changed__c,
                    vic_VIC_User_4_Split__c, VIC_IsIncentiveCalculatedForPrimaryRep__c, VIC_IsIncentiveCalculatedForBDRep__c,
                    vic_Sales_Rep_Approval_Status__c, vic_Product_BD_Rep_Approval_Status__c,
                    Opportunity.AccountId,
                    Opportunity.Sales_country__c,
                    Opportunity.vic_Total_Renewal_TCV__c,
                    Opportunity.Account.Business_Group__c, Opportunity.Account.Business_Segment__r.Name ,
                    vic_Old_TCV_Spilt_For_Sales_Rep__c,vic_Old_TCV_Spilt_For_Product_BD_Rep__c,
                    vic_Old_TCV_Spilt_For_User_3__c,vic_Old_TCV_Spilt_For_User_4__c,vic_TCV_Accelerator_Variable_Credit__c,
                    Opportunity.VIC_TCV_USD_Sum__c, Opportunity.Opportunity_Source__c, Opportunity.Account.Hunting_Mining__c
                    FROM OpportunityLineItem 
                    WHERE Opportunity.StageName =: opportunityStagename
                    AND Opportunity.Actual_Close_Date__c >= :processingYearStart
                    AND Opportunity.Actual_Close_Date__c <= :processingYearEnd
                    AND vic_CPQ_Deal_Status__c =: strCPQDealStatus AND vic_Final_Data_Received_From_CPQ__c = true 
                    AND (
                        vic_Sales_Rep_Approval_Status__c = 'Approved' OR vic_Product_BD_Rep_Approval_Status__c = 'Approved'
                    )
                ]);
            }
        }
        else{
            return Database.getQueryLocator([
                SELECT Id, Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, Opportunity.Actual_Close_Date__c, Opportunity.CloseDate,Opportunity.VIC_Total_CYR_IO__c,
                VIC_NPV__c, vic_Contract_Term__c, Contract_Term__c, Opportunity.Id,
                CYR__c, TCV__c, Opportunity.OwnerId, Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c, Opportunity.StageName,
                vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c, vic_VIC_User_3_Split__c, vic_is_CPQ_Value_Changed__c,
                vic_VIC_User_4_Split__c, VIC_IsIncentiveCalculatedForPrimaryRep__c, VIC_IsIncentiveCalculatedForBDRep__c,
                vic_Sales_Rep_Approval_Status__c, vic_Product_BD_Rep_Approval_Status__c,
                Opportunity.AccountId,
                Opportunity.Sales_country__c,
                Opportunity.vic_Total_Renewal_TCV__c,
                Opportunity.Account.Business_Group__c, Opportunity.Account.Business_Segment__r.Name ,
                vic_Old_TCV_Spilt_For_Sales_Rep__c,vic_Old_TCV_Spilt_For_Product_BD_Rep__c,
                vic_Old_TCV_Spilt_For_User_3__c,vic_Old_TCV_Spilt_For_User_4__c,vic_TCV_Accelerator_Variable_Credit__c,
                Opportunity.VIC_TCV_USD_Sum__c, Opportunity.Opportunity_Source__c, Opportunity.Account.Hunting_Mining__c
                FROM OpportunityLineItem 
                WHERE Opportunity.StageName =: opportunityStagename
                AND Opportunity.Actual_Close_Date__c >= :processingYearStart
                AND Opportunity.Actual_Close_Date__c <= :processingYearEnd
                AND vic_CPQ_Deal_Status__c =: strCPQDealStatus AND vic_Final_Data_Received_From_CPQ__c = true 
                AND (
                    (vic_Sales_Rep_Approval_Status__c = 'Approved' OR vic_Product_BD_Rep_Approval_Status__c = 'Approved')
                )
                AND (vic_is_CPQ_Value_Changed__c = true OR 
                     (vic_is_CPQ_Value_Changed__c = false AND 
                      (VIC_IsIncentiveCalculatedForPrimaryRep__c = false OR VIC_IsIncentiveCalculatedForBDRep__c = false)
                     )
                    )
              
            ]);
        }
        
    }
    
    public void execute(Database.BatchableContext bc, List<OpportunityLineItem> oppProds){
        try{
            Set<Id> userIds = new Set<Id>();
            Set<String> oppProdIds = new Set<String>();
            
            VIC_UpfrontIncentivesBatchHelper batchHelper = new VIC_UpfrontIncentivesBatchHelper();
            batchHelper.isAnniversaryCal = isAnniversaryCal;
            
            List<Target_Achievement__c> incentives = new List<Target_Achievement__c>();
            Map<Id, List<Target_Component__c>> userIdVsTargetComponentsMap;
            Map<Id, User> userIdVsUserMap;
            
            //preparing set of userIds and set of OpprtunityProduct Ids
            for(OpportunityLineItem obj: oppProds){
                userIds.addAll(batchHelper.getOpportunityProductUsers(obj));
                oppProdIds.add('' + obj.Id);
            }
            
            //fetching target components List for users
            userIdVsTargetComponentsMap = batchHelper.getTargetComponents(userIds, oppProdIds);
            if(!userIds.isEmpty()){
                //getUsersVsPlanMap: getting plan code for users
                Map<Id, String> usersVsPlanCodes = batchHelper.getUsersVsPlanMap(userIds);
                //fetching user details
                userIds = usersVsPlanCodes.keySet();
                
                userIdVsUserMap = new Map<Id, User>([SELECT Id, Domicile__c 
                                                     FROM User 
                                                     WHERE Id =: userIds
                                                     AND isActive = true
                                                    ]);
                Set<String> planCodesSet = new Set<String>();
                planCodesSet.addAll(usersVsPlanCodes.values());
                
                
                //getting plan components for corresponding plan codes
                Map<String, List<Plan_Component__c>> planCodeVsPlanComponentsMap 
                    = batchHelper.getPlanCodeVsPlanComponentsMap(planCodesSet);
                Map<Id, Target__c> targetsNeedUpdate = new Map<Id, Target__c>();
                
                List<String> natureOfWorkListIO = VIC_NatureOfWorkToDealTypeCtlr.getNatureOfWorkList('IO');
                //Now get plan components, perform calculation and insert incentive records
                
                for(OpportunityLineItem oppProd: oppProds){
                    if(oppProd.vic_is_CPQ_Value_Changed__c){
                        oppProd.VIC_IsIncentiveCalculatedForPrimaryRep__c = false;
                        oppProd.VIC_IsIncentiveCalculatedForBDRep__c = false;
                    }
                    if(natureOfWorkListIO.contains(oppProd.Nature_of_Work__c)){
                        oppProd.vic_IO_TS__c = 'IO';
                    }
                    else{
                        oppProd.vic_IO_TS__c = 'TS';
                    }
                    
                    List<Target_Achievement__c> oppProdIncentives = 
                        batchHelper.getOpprtunityProductIncentivesList(oppProd, 
                                                                       userIdVsUserMap, 
                                                                       usersVsPlanCodes, 
                                                                       planCodeVsPlanComponentsMap, 
                                                                       userIdVsTargetComponentsMap,
                                                                       targetsNeedUpdate
                                                                      );
                    incentives.addAll(oppProdIncentives);

                    if(oppProd.VIC_IsIncentiveCalculatedForPrimaryRep__c || oppProd.VIC_IsIncentiveCalculatedForBDRep__c){
                        oppProd.vic_is_CPQ_Value_Changed__c = false;
                    }
                }
                List<Target__c> targetList = targetsNeedUpdate.values();
                System.debug('targetList===>>'+targetList);
                INSERT incentives;
                UPDATE targetList;
                UPDATE oppProds;
            }
        }
        catch(Exception ex){
            LoggerUtility.logException('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH, ex);
            LoggerUtility.commitLog();
        }
    }
    public void finish(Database.BatchableContext bc){
        System.debug('Upfront job finished!');
        //calling other batches after it
        if(!Test.isRunningTest() && !this.isAnniversaryCal){
           VIC_CalculateKickersIncentiveBatch kickersBatch = new VIC_CalculateKickersIncentiveBatch();
           Database.executeBatch(kickersBatch, 2);
        }else if(!Test.isRunningTest() && this.isAnniversaryCal){
            VIC_CurrencyConversionBatch  objBatch = new VIC_CurrencyConversionBatch(TRUE);
            Database.executeBatch(objBatch, 2);
        }
    }
}