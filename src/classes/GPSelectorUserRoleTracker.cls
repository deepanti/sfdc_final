@isTest
public class GPSelectorUserRoleTracker {
    
    public Static GP_User_Role__c objUserRole;
    public Static User objuser;
    static GP_Project__c project;
    @testSetup
    public static void buildDependencyData() {
         GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        insert project ;
    }
    @isTest
    public static void testgetSObjectType() {
        GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();
        Schema.SObjectType returnedType = userRoleSelector.getSObjectType();
        Schema.SObjectType expectedType = GP_User_Role__c.sObjectType;
        
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectById() {
        GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();
        // objUserRole = [Select Id,GP_Role__c from GP_User_Role__c Limit 1];
        List<GP_User_Role__c> listOfReturnedUserRole = userRoleSelector.selectById(new Set<Id> ());
    }
    
    @isTest
    public static void testselectByRoleAndUser() {
        GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();
        //GP_Role__c objrole = [select id from GP_Role__c limit 1];
        //objuser = [select id from User limit 1];
        //objUserRole = [Select Id,GP_Role__c from GP_User_Role__c Limit 1];
        List<GP_User_Role__c> listOfReturnedUserRole = userRoleSelector.selectByRoleAndUser(new Set<String>(),
                                                                                            new Set<String> ());
    }
    
    @isTest
    public static void testSelectEmployeeMasterRecord() {
        project = [Select Id, GP_Primary_SDO__c from GP_Project__c LIMIT 1];
        GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();
        try {
            userRoleSelector.selectById(null);
        } catch(Exception ex) {}
        try {
            userRoleSelector.selectByRoleAndUser(null, null);
        } catch(Exception ex) {}
        try {
            //userRoleSelector.selectEmployeeMasterRecord(null, null);
        } catch(Exception ex) {}
        userRoleSelector.selectByActiveStatus(null);
        userRoleSelector.selectByRoleId(null);
        userRoleSelector.selectByUserId(null);
        userRoleSelector.selectUserfromAdminUserRole();
        userRoleSelector.selectUserofResourceAllocationCategory(project);
        userRoleSelector.selectUserofPIDCreationCategory(project);
        userRoleSelector.selectUserofAdminCategory();
        userRoleSelector.selectUserRolesForAddUserRoles(null);
        userRoleSelector.selectUserRolesForRemoveUserRoles(null);
        userRoleSelector.selectRolesforUser(null, null);
        GPSelectorUserRole.selectPIDCreatorRoles();
        GPSelectorUserRole.selectLegacyPIDCreatorRoles();
        GPSelectorUserRole.selectPIDCreatorRolesForLegacyProjects();
    }
}