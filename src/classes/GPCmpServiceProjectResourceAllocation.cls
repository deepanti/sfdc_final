/**
 * @group ProjectResourceAllocation
 * @group-content ../../ApexDocContent/ProjectResourceAllocation.htm
 *
 * @description Apex service class having Aura enabled methods
 *              to fetch, insert, update and delete resource to be allocated under a project.
 */
public class GPCmpServiceProjectResourceAllocation {

	//Commented 14052018@AuraEnabled
   // public static GPAuraResponse getProjectResourceAllocationData(Id resourceAllocationId) {
        //GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation(resourceAllocationId,null);
        //return resourceAllocationController.getResourceAllocationRecord();
       // return null;
   // }

    /**
     * @description Returns Project Resource Allocation data for a given project Id
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse json of Project Resource Allocation data
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.getProjectResourceAllocationData(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getResourceAllocationData(Id projectId) {
        GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation(null, projectId);
        return resourceAllocationController.getResourceAllocationData(projectId);
    }

    /**
     * @description Saves Resource data
     * @param strListOfProjectResourceAllocation serialized list of project Resource Allocation records to be inserted/updated
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully inserted
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.saveResourceAllocationData(<Serialized list of project Resource Allocation record>);
     */
    @AuraEnabled
    public static GPAuraResponse saveResourceAllocationData(String strResourceAllocation) {
        GP_Resource_Allocation__c resourceAllocation = (GP_Resource_Allocation__c) JSON.deserialize(strResourceAllocation, GP_Resource_Allocation__c.class);
        GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation(resourceAllocation);
        return resourceAllocationController.upsertResourceAllocationRecord();
    }

    /**
     * @description Deletes resource allocation data for a given resourceAllocationId Id
     * @param resourceAllocationId Salesforce 18/15 digit Id of project ResourceAllocation
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully deleted
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.deleteResourceAllocationData(<18 or 15 digit resource allocation Id>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteResourceAllocationData(String projectResourceAllocationId) {

        GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation();
        return resourceAllocationController.deleteResourceAllocationRecord(projectResourceAllocationId);
    }


    //Commented 14052018@AuraEnabled
    //public static GPAuraResponse getResourceAllocationServiceRelatedDetails(Id projectId) {
        //GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation(null,projectId);
        //return resourceAllocationController.getDummyResourceAllocationRecord();
      //  return null;
    //}

    /* @AuraEnabled
public static GPAuraResponse getEmployeeResourceAllocationServiceDetails(Id employeeId) {
GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation();
return resourceAllocationController.getEmployeeResourceAllocationDetails(employeeId);
}*/
    /**
     * @description get Employee Lending SDO.
     * @param employeeId Salesforce 18/15 digit Id of employee.
     * @param projectId Salesforce 18/15 digit Id of Project.
     * 
     * @return GPAuraResponse json employee lending SDO.
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.getLendingSDO(<18 or 15 digit employee Id>,<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getLendingSDO(String employeeId, String projectId) {
        GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation();
        return resourceAllocationController.getLendingSDOToSelect(employeeId, projectId);
    }

    //Commented 14052018@AuraEnabled
    //public static GPAuraResponse getEmployeeHRDataService(Id employeeId) {
        //GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation(null,null);
        //return resourceAllocationController.getEmployeeHRData(employeeId);
      //  return null;
    //}

    /**
     * @description get processed Mass upload Resource Allocation Records.
     * @param listOfCSVRecordsUploaded : serialized mass upload resource allocation records.
     * @param projectId Salesforce 18/15 digit Id of Project.
     * 
     * @return GPAuraResponse json process Mass Upload Resource Allocation Records.
     * 
     * @example
     * GPCmpServiceProjectResourceAllocation.getLendingSDO(<18 or 15 digit employee Id>,<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getProcessedCSVRecordsService(String listOfCSVRecordsUploaded, String projectId) {
        try {
            List < GP_Resource_Allocation__c > listOfResourceAllocation = (List < GP_Resource_Allocation__c > ) JSON.deserialize(listOfCSVRecordsUploaded, List < GP_Resource_Allocation__c > .class);
            GPControllerProjectResourceAllocation resourceAllocationController = new GPControllerProjectResourceAllocation();
            return resourceAllocationController.getProcessedCSVRecords(listOfResourceAllocation, projectId);
        } catch (Exception E) {
            return new GPAuraResponse(false, 'Please enter the data correctly as per the given instruction', null);
        }
    }
}