// DS: Updated the batch to handle upsert dml instead of insert.
global class GPBatchCreateDeals implements Database.Batchable < sObject > , Database.Stateful , schedulable {   
     // Avinash : Valueshare Changes.
    global Map<String,String> mapOfValueshareProjectTypes = new Map<String,String>();
    global GPBatchCreateDeals() {
        this.mapOfValueshareProjectTypes = (Map<String,String>) JSON.deserialize(System.Label.GP_Valueshare_Map, Map<String,String>.class);
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // Get ndays from custom label.
        Integer ndays = Integer.valueOf(Label.GP_Last_N_Days_for_Create_Deal_batch);
        
        Date lastNDays = Date.today();
        
        if(ndays > 0) {
            lastNDays = lastNDays.addDays(-ndays);
        }
        
        System.debug('==GPBatchCreateDeals:=lastNDays=='+lastNDays);
		
		// Get the OLIs which are either created/Modified in lastNDays.
		//return Database.getQueryLocator([Select id, Opportunity_Id__c from OpportunityLineItem 
                                         //where Createddate >=: lastNDays]);
                                         
        return Database.getQueryLocator([Select ID,
                                         Name,
                                         Product2Id,
                                         End_Date__c,
                                         OpportunityId,
                                         Opportunity_Id__c,
										 Is_Value_Share_Involved__c,
                                         TCV__c,
                                         Custom_Id__c,
                                         SFDC_18_digit_ID__c,
                                         Product2.Name,
                                         Product2.Product_Family__c,
                                         Opportunity.Name, 
                                         Delivering_Organisation__c,
                                         Sub_Delivering_Organisation__c,
                                         Opportunity.AccountID, 
                                         Product2.Service_Line__c,
                                         Opportunity.Account.Name,
                                         Product2.Nature_of_Work__c, 
                                         Opportunity.Probability,
                                         Opportunity.Opportunity_ID__c,
                                         Revenue_Start_Date__c,
                                         Opportunity.Account.Sub_Business__c,
                                         Opportunity.Account.Business_Group__c,
                                         Opportunity.Account.Business_Segment__c,
                                         Opportunity.Account.Industry_Vertical__c,
                                         Opportunity.Account.Sub_Industry_Vertical__c,
                                         Opportunity.Account.Business_Segment__r.Name,
                                         Opportunity.Account.Sub_Business__r.Name 
                                         from OpportunityLineItem 
                                         where (LastModifiedDate >=: lastNDays
                                               OR Opportunity.LastModifiedDate >=: lastNDays
                                               OR Opportunity.Account.LastModifiedDate >=: lastNDays
                                               OR Opportunity.Account.Business_Segment__r.LastModifiedDate >=: lastNDays
                                               OR Opportunity.Account.Sub_Business__r.LastModifiedDate >=: lastNDays)
                                         ]);
    }
    
    global void execute(SchedulableContext sc) {
        GPBatchCreateDeals objAutoReject = new GPBatchCreateDeals();
        Database.executebatch(objAutoReject); 
    }
    
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> lstOfOpportunityLineItems) {
        
        try {
            Set<String> setOpportunityID = new Set<String>();
            Set<String> setOfOppIdsOnOppPIDs = new Set<String>();
            Map<String,Id> mapOfOppRefenenceIdToOpp = new Map<String,Id> ();
			Map<String,GP_Deal__c> mapOLIWithDealRecords = new Map<String,GP_Deal__c>();
			
			for(OpportunityLineItem oliObj : lstOfOpportunityLineItems) {
				setOpportunityID.add(oliObj.Opportunity_Id__c);
			}
            
            system.debug('==setOpportunityID=='+JSON.serialize(setOpportunityID));
            
            if(setOpportunityID.size() > 0) {
				// Get the opportunity project.
                for(GP_Opportunity_Project__c objOppProject : new GPSelectorOpportunityProject().selectOppProject(setOpportunityID)) {
                    //setOfOppIdsOnOppPIDs.add(objOppProject.GP_Opportunity_Id__c);
                    mapOfOppRefenenceIdToOpp.put(objOppProject.GP_Opportunity_Id__c, objOppProject.Id);
                }

                // Get deals of Opp Ids.
                for(GP_Deal__c objDeal : [Select id, GP_Probability__c, GP_Opportunity_Project__c, 
                                          GP_OLI_SFDC_Id__c, GP_Business_Segment_L2__c, 
                                          GP_Business_Segment_L2_Id__c, GP_Sub_Vertical__c, 
                                          GP_Sub_Business_L3__c, GP_Business_Group_L1__c, 
                                          GP_Sub_Business_L3_Id__c, GP_Sales_Opportunity_Id__c, GP_Vertical__c, 
                                          GP_Service_Line_Description__c, GP_Sub_Delivery_Org__c, 
                                          GP_Start_Date__c, GP_Nature_of_Work__c, GP_Delivery_Org__c, 
                                          GP_Account_Name_L4__c, GP_Internal_Opportunity_ID__c, 
                                          GP_Deal_Category__c, GP_Service_Line__c, GP_Opportunity_Name__c, 
                                          GP_Product_Family__c, GP_Opportunity_ID__c, GP_Product__c, GP_Project_Type__c,
                                          GP_Product_Id__c, GP_TCV__c, GP_End_Date__c, Name 
                                          from GP_Deal__c 
                                          where GP_Sales_Opportunity_Id__c IN: setOpportunityID]) {
                    mapOLIWithDealRecords.put(objDeal.GP_OLI_SFDC_Id__c, objDeal);
                }

				system.debug('==mapOfOppRefenenceIdToOpp=='+mapOfOppRefenenceIdToOpp);
                system.debug('==mapOLIWithDealRecords=='+mapOLIWithDealRecords);
                
                list<GP_Deal__c> lstOfDeal = new list<GP_Deal__c>();
                
                for(OpportunityLineItem objOLI : lstOfOpportunityLineItems) {
                    
                    GP_Deal__c objDeal;
                    Boolean isChangesFound = false;
                    // Get the Opp PID Id.
                    Id oppPIDId = mapOfOppRefenenceIdToOpp.containsKey(objOLI.Opportunity.Opportunity_ID__c) 
                        ? mapOfOppRefenenceIdToOpp.get(objOLI.Opportunity.Opportunity_ID__c) : null;
                    
                    // Get the oli Id.
                    Id oliId = objOLI.Custom_Id__c != null ? objOLI.Custom_Id__c : objOLI.Id;
                    
                    // Determine whether to insert or update deal record.
                    if(mapOLIWithDealRecords != null && mapOLIWithDealRecords.containsKey(oliId)) {
                        
						objDeal = new GP_Deal__c(Id = mapOLIWithDealRecords.get(oliId).Id);                        
						objDeal = compareDealWithOLI(objDeal, objOLI, mapOLIWithDealRecords.get(oliId), oppPIDId);
						
						if(objDeal != null) {
							lstOfDeal.add(objDeal);
						}
                        
                        if(!isChangesFound) {						
                            continue;
                        }
                        
                    } else {
                        objDeal = new GP_Deal__c();
                        isChangesFound = true;
                    }
                    
                    
                    if(isChangesFound) {
                        
                        objDeal.GP_Probability__c = objOLI.Opportunity.Probability;
                        objDeal.GP_Opportunity_Project__c = oppPIDId;
                        objDeal.GP_Business_Segment_L2__c = objOLI.Opportunity.Account.Business_Segment__r.Name;
                        objDeal.GP_Business_Segment_L2_Id__c = objOLI.Opportunity.Account.Business_Segment__c;
                        objDeal.GP_Sub_Vertical__c = objOLI.Opportunity.Account.Sub_Industry_Vertical__c;
                        objDeal.GP_Sub_Business_L3__c = objOLI.Opportunity.Account.Sub_Business__r.Name;
                        objDeal.GP_Business_Group_L1__c = objOLI.Opportunity.Account.Business_Group__c;
                        objDeal.GP_Sub_Business_L3_Id__c = objOLI.Opportunity.Account.Sub_Business__c;
                        objDeal.GP_Sales_Opportunity_Id__c = objOLI.Opportunity.Opportunity_ID__c;
                        objDeal.GP_Vertical__c = objOLI.Opportunity.Account.Industry_Vertical__c;
                        objDeal.GP_Service_Line_Description__c = objOLI.Product2.Service_Line__c;
                        objDeal.GP_Sub_Delivery_Org__c = objOLI.Sub_Delivering_Organisation__c;
                        objDeal.GP_Start_Date__c = objOLI.Revenue_Start_Date__c;
                        objDeal.GP_Nature_of_Work__c = objOLI.Product2.Nature_of_Work__c;
                        objDeal.GP_Delivery_Org__c = objOLI.Delivering_Organisation__c;
                        objDeal.GP_Account_Name_L4__c = objOLI.Opportunity.AccountID;
                        objDeal.GP_Internal_Opportunity_ID__c = objOLI.OpportunityId;
                        objDeal.GP_Deal_Category__c = 'Sales SFDC';
                        objDeal.GP_Service_Line__c = objOLI.Product2.Service_Line__c;
                        objDeal.GP_Opportunity_Name__c = objOLI.Opportunity.Name;
                        objDeal.GP_OLI_SFDC_Id__c = objOLI.Custom_Id__c != null ? objOLI.Custom_Id__c : objOLI.id;
                        objDeal.GP_Product_Family__c = objOLI.Product2.Product_Family__c;
                        objDeal.GP_Opportunity_ID__c = objOLI.OpportunityId;
                        objDeal.GP_Product__c = objOLI.Product2.Name;
                        objDeal.GP_Product_Id__c = objOLI.Product2Id;
						 // Avinash : Valueshare Changes
                        objDeal.GP_Project_Type__c = this.mapOfValueshareProjectTypes.containsKey(objOLI.Is_Value_Share_Involved__c) 
                        ? this.mapOfValueshareProjectTypes.get(objOLI.Is_Value_Share_Involved__c) : '';                        
                        
                        objDeal.GP_TCV__c = objOLI.TCV__c;
                        objDeal.GP_End_Date__c = objOLI.End_Date__c;
                        objDeal.Name = objOLI.Name.length() > 80 ?objOLI.Name.left(80):objOLI.Name;
                        lstOfDeal.add(objDeal);
                    }
                }
                
                System.debug('==GPBatchCreateDeals:=lstOfDeal=='+lstOfDeal);
                
                if(lstOfDeal != null && lstOfDeal.size() > 0) {
                    Database.UpsertResult [] upsertResult = Database.Upsert(lstOfDeal, false);
                    
                    for (Database.UpsertResult sr : upsertResult) {
                        if (!sr.isSuccess() || !test.isRunningTest()) {
                            for(Database.Error err : sr.getErrors()) {
                                GPErrorLogUtility.logError('GPBatchCreateDeals', 'execute',null, err.getMessage(), null, null, null, null, err.getMessage());
                            }
                        }
                    }
                }
            }
        } catch(Exception ex) {
            GPErrorLogUtility.logError('GPBatchCreateDeals', 'execute',ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());   
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
    
    private GP_Deal__c compareDealWithOLI(GP_Deal__c newDeal, OpportunityLineItem objOLI, GP_Deal__c deal, Id oppPIDId) {
        Boolean isChange = false;

        if(deal.GP_Opportunity_Project__c == null && oppPIDId != null) {
            newDeal.GP_Opportunity_Project__c = oppPIDId;
            isChange = true;
        }
        
        if(objOLI.Opportunity.Probability != null && deal.GP_Probability__c != objOLI.Opportunity.Probability) {
            newDeal.GP_Probability__c = objOLI.Opportunity.Probability;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Business_Segment__r.Name != null && deal.GP_Business_Segment_L2__c != objOLI.Opportunity.Account.Business_Segment__r.Name) {
            newDeal.GP_Business_Segment_L2__c = objOLI.Opportunity.Account.Business_Segment__r.Name;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Business_Segment__c != null && deal.GP_Business_Segment_L2_Id__c != objOLI.Opportunity.Account.Business_Segment__c) {
            newDeal.GP_Business_Segment_L2_Id__c = objOLI.Opportunity.Account.Business_Segment__c;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Sub_Industry_Vertical__c != null && deal.GP_Sub_Vertical__c != objOLI.Opportunity.Account.Sub_Industry_Vertical__c) {
            newDeal.GP_Sub_Vertical__c = objOLI.Opportunity.Account.Sub_Industry_Vertical__c;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Sub_Business__r.Name != null && deal.GP_Sub_Business_L3__c != objOLI.Opportunity.Account.Sub_Business__r.Name) {
            newDeal.GP_Sub_Business_L3__c = objOLI.Opportunity.Account.Sub_Business__r.Name;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Business_Group__c != null && deal.GP_Business_Group_L1__c != objOLI.Opportunity.Account.Business_Group__c) {
            newDeal.GP_Business_Group_L1__c = objOLI.Opportunity.Account.Business_Group__c;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Sub_Business__c != null && deal.GP_Sub_Business_L3_Id__c != objOLI.Opportunity.Account.Sub_Business__c) {
            newDeal.GP_Sub_Business_L3_Id__c = objOLI.Opportunity.Account.Sub_Business__c;
            isChange = true;
        }
        if(objOLI.Opportunity.Opportunity_ID__c != null && deal.GP_Sales_Opportunity_Id__c != objOLI.Opportunity.Opportunity_ID__c) {
            newDeal.GP_Sales_Opportunity_Id__c = objOLI.Opportunity.Opportunity_ID__c;
            isChange = true;
        }
        if(objOLI.Opportunity.Account.Industry_Vertical__c != null && deal.GP_Vertical__c != objOLI.Opportunity.Account.Industry_Vertical__c) {
            newDeal.GP_Vertical__c = objOLI.Opportunity.Account.Industry_Vertical__c;
            isChange = true;
        }
        if(objOLI.Product2.Service_Line__c != null && deal.GP_Service_Line_Description__c != objOLI.Product2.Service_Line__c) {
            newDeal.GP_Service_Line_Description__c = objOLI.Product2.Service_Line__c;
            newDeal.GP_Service_Line__c = objOLI.Product2.Service_Line__c;
            isChange = true;
        }
        if(objOLI.Sub_Delivering_Organisation__c != null && deal.GP_Sub_Delivery_Org__c != objOLI.Sub_Delivering_Organisation__c) {
            newDeal.GP_Sub_Delivery_Org__c = objOLI.Sub_Delivering_Organisation__c;
            isChange = true;
        }
        if(objOLI.Revenue_Start_Date__c != null && deal.GP_Start_Date__c != objOLI.Revenue_Start_Date__c) {
            newDeal.GP_Start_Date__c = objOLI.Revenue_Start_Date__c;
            isChange = true;
        }
        if(objOLI.Product2.Nature_of_Work__c != null && deal.GP_Nature_of_Work__c != objOLI.Product2.Nature_of_Work__c) {
            newDeal.GP_Nature_of_Work__c = objOLI.Product2.Nature_of_Work__c;
            isChange = true;
        }
        if(objOLI.Delivering_Organisation__c != null && deal.GP_Delivery_Org__c != objOLI.Delivering_Organisation__c) {
            newDeal.GP_Delivery_Org__c = objOLI.Delivering_Organisation__c;
            isChange = true;
        }
        if(objOLI.Opportunity.AccountID != null && deal.GP_Account_Name_L4__c != objOLI.Opportunity.AccountID) {
            newDeal.GP_Account_Name_L4__c = objOLI.Opportunity.AccountID;
            isChange = true;
        }
        if(objOLI.OpportunityId != null && deal.GP_Internal_Opportunity_ID__c != objOLI.OpportunityId) {
            newDeal.GP_Internal_Opportunity_ID__c = objOLI.OpportunityId;
            newDeal.GP_Opportunity_ID__c = objOLI.OpportunityId;
            isChange = true;
        }
        if(objOLI.Opportunity.Name != null && deal.GP_Opportunity_Name__c != objOLI.Opportunity.Name) {
            newDeal.GP_Opportunity_Name__c = objOLI.Opportunity.Name;
            isChange = true;
        }
        
        // Use OLI's Product_Family__c instead of Product2.Family.
        if(objOLI.Product2.Product_Family__c != null && deal.GP_Product_Family__c != objOLI.Product2.Product_Family__c) {
            newDeal.GP_Product_Family__c = objOLI.Product2.Product_Family__c;
            isChange = true;
        }
        
        if(objOLI.Product2.Name != null && deal.GP_Product__c != objOLI.Product2.Name) {
            newDeal.GP_Product__c = objOLI.Product2.Name;
            isChange = true;
        }
        if(objOLI.Product2Id != null && deal.GP_Product_Id__c != objOLI.Product2Id) {
            newDeal.GP_Product_Id__c = objOLI.Product2Id;
            isChange = true;
        }
        if(objOLI.TCV__c != null && deal.GP_TCV__c != objOLI.TCV__c) {
            newDeal.GP_TCV__c = objOLI.TCV__c;
            isChange = true;
        }
        if(objOLI.End_Date__c != null && deal.GP_End_Date__c != objOLI.End_Date__c) {
            newDeal.GP_End_Date__c = objOLI.End_Date__c;
            isChange = true;
        }
         // Avinash : Valueshare Changes
        if(objOLI.Is_Value_Share_Involved__c != null && this.mapOfValueshareProjectTypes.containsKey(objOLI.Is_Value_Share_Involved__c) 
        && deal.GP_Project_Type__c != this.mapOfValueshareProjectTypes.get(objOLI.Is_Value_Share_Involved__c)) {
            newDeal.GP_Project_Type__c = this.mapOfValueshareProjectTypes.get(objOLI.Is_Value_Share_Involved__c);
            isChange = true;
        }
        if(!isChange) {
            newDeal = null;
        }
            
        return newDeal;
    }
}