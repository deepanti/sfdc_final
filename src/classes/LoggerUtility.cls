/** 
    * @Author:      Alok Verma
    * @Company:     SaasFocus
    * @Created:     12-June-20118
    * @Description: Utility class to help logged log messages into the Log__c object
    * @Key Details: source          - Originating Process
                    sourceFunction  - Originating function (method of the above class that initiates the logging)
                    logLevel        - INFO, DEBUG, WARNING, ERROR,FATAL, EXCEPTION
                    message         - The user friendly message associated with the log record       
                    refIds          - The salesforce record or job ID related to the log
                    refInfos        - Additional context about the reference ID (e.g. Apex Batch, Web Callout, Contact etc,,,)
                    refCat          - Optional reference category, used for reporting purposes
                    reqMsg          - Optional request payload, used to capture request message for integrations
                    resMsg          - Optional response payload, used to capture response message for integrations 
                    ex              - The standard exception object for errors
            
*/
public class LoggerUtility {

    // Declare Enums to define the type of logs
    public Enum LogLevel {INFO, DEBUG, WARNING, ERROR, FATAL, EXC}
    public Enum RefCategory {INBOUNDCALL,OUTBOUNDCALL,SCHEDULE,BATCH,VISUALFORCE,LIGHTNINGCOMPONENT,APEXTRIGGER,FLOW,OTHER}

    //private static Boolean isEnabled {get;set;}
    private static List<vic_Log__c> logMessages = new list<vic_Log__c>();
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Method to add a single record to the logMessages. 
                            Must call commitLog() to commit all loggs into the system. 
        * @Param:           String source, 
                            String sourceFunction, 
                            LoggerUtility.LogLevel logLevel,
                            String message, 
                            String refIds,
                            String refInfos,
                            String refCat,
                            String reqMsg,
                            String resMsg,
                            Exception ex
        * @Return:          None 
        * @Revision History: 
            <Date>              <Authors Name>          <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    private static Void addLog(String source, 
                              String sourceFunction, 
                              LoggerUtility.LogLevel logLevel,
                              String message, 
                              String refIds,
                              String refInfos,
                              LoggerUtility.RefCategory refCat,
                              String reqMsg,
                              String resMsg,
                              Exception ex) {
        try {
            vic_Log__c newLogMessage = new vic_Log__c(
                                    Source__c = source,
                                    Source_Function__c = sourceFunction,        
                                    Log_Level__c = logLevel.name() == 'EXC' ? 'EXCEPTION':logLevel.name(),
                                    Message__c = message,
                                    Reference_Id_s__c = refIds,
                                    Reference_Info__c = refInfos,
                                    Reference_Category__c = refCat.name(),
                                    Request_Message__c = reqMsg,
                                    Response_Message__c = resMsg,
                                    Stack_Trace__c = ex != null ? 'Message: '+ ex.getMessage() + ' | Stack Trace: ' + ex.getStackTraceString() : null
                                    );
            logMessages.add(newLogMessage);
        }
        catch(Exception e) {
            System.debug('Failed to add Log '
                    + ' Error = ' + ex != null ? ex.getMessage() : null
                    + ' logLevel='+logLevel
                    + ' sourceClass='+source
                    + ' sourceFunction='+sourceFunction
                    + ' ex='+ex
                    + ' message='+message
                    + ' referenceID='+refIds
                    + ' referenceInfo='+refInfos
                    + ' Request Payload='+reqMsg
                    + ' Response Payload='+resMsg);
        }
    }
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to create the log message for manual errors. 
        * @Param:           String source, 
                            String sourceFunction, 
                            LoggerUtility.LogLevel logLevel,
                            String message, 
                            String refIds,
                            String refInfos,
                            String refCat
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void logError(String source, 
                                  String sourceFunction, 
                                  LoggerUtility.LogLevel logLevel,
                                  String message, 
                                  String refIds,
                                  String refInfos,
                                  LoggerUtility.RefCategory refCat) {
        addLog(source, 
                  sourceFunction, 
                  logLevel,
                  message, 
                  refIds,
                  refInfos,
                  refCat,
                  null,
                  null,
                  null);
    }
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to create the log message for exception. 
        * @Param:           String className, 
                            String methodName, 
                            Exception e,
                            LoggerUtility.LogLevel LogLevel
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void logException(String source, 
                                  String sourceFunction, 
                                  LoggerUtility.LogLevel logLevel,
                                  String message, 
                                  String refIds,
                                  String refInfos,
                                  LoggerUtility.RefCategory refCat,
                                  Exception ex) {
        addLog(source, 
                  sourceFunction, 
                  logLevel,
                  message, 
                  refIds,
                  refInfos,
                  refCat,
                  null,
                  null,
                  ex);
    }
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to create the log message for APIs. 
        * @Param:           String source, 
                            String sourceFunction, 
                            LoggerUtility.LogLevel logLevel,
                            String message, 
                            String refIds,
                            String refInfos,
                            String refCat,
                            String reqMsg,
                            String resMsg
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void logAPIError(String source, 
                                  String sourceFunction, 
                                  LoggerUtility.LogLevel logLevel,
                                  String message, 
                                  String refIds,
                                  String refInfos,
                                  LoggerUtility.RefCategory refCat,
                                  String reqMsg,
                                  String resMsg) {
        addLog(source, 
                  sourceFunction, 
                  logLevel,
                  message, 
                  refIds,
                  refInfos,
                  refCat,
                  reqMsg,
                  resMsg,
                  null);
    }
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to create the log message for APIs. 
        * @Param:           String source, 
                            String sourceFunction, 
                            LoggerUtility.LogLevel logLevel,
                            String message, 
                            String refIds,
                            String refInfos,
                            String refCat,
                            String reqMsg,
                            String resMsg,
                            Exception ex
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void logAPIException(String source, 
                                  String sourceFunction, 
                                  LoggerUtility.LogLevel logLevel,
                                  String message, 
                                  String refIds,
                                  String refInfos,
                                  LoggerUtility.RefCategory refCat,
                                  String reqMsg,
                                  String resMsg,
                                  Exception ex) {
        addLog(source, 
              sourceFunction, 
              logLevel,
              message, 
              refIds,
              refInfos,
              refCat,
              reqMsg,
              resMsg,
              ex);
    }
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to create the log message for generic errors/exception. 
        * @Param:           String source, 
                            String sourceFunction, 
                            LoggerUtility.LogLevel logLevel,
                            String message, 
                            String refIds,
                            String refInfos,
                            String refCat,
                            String reqMsg,
                            String resMsg,
                            Exception ex
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void logMessage(String source, 
                                  String sourceFunction, 
                                  LoggerUtility.LogLevel logLevel,
                                  String message, 
                                  String refIds,
                                  String refInfos,
                                  LoggerUtility.RefCategory refCat,
                                  String reqMsg,
                                  String resMsg,
                                  Exception ex) {
        addLog(source, 
              sourceFunction, 
              logLevel,
              message, 
              refIds,
              refInfos,
              refCat,
              reqMsg,
              resMsg,
              ex);
    }
    
    
    /**
        * @Author:          Alok Verma
        * @Company:         SaasFocus
        * @Description:     Call this method to commit the log messages into system. 
        * @Param:           None
        * @Return:          None 
        * @Revision History: 
            <Date>              Authors Name>           <Brief Description of Change>
            06-Apr-2018         Alok Verma              Created
    */
    public static Void commitLog() {
        try{
            if(logMessages.isEmpty()) return;
            System.Debug(logMessages);
            insert logMessages;
            //Database.insert(logMessages, false);
            logMessages= new list<vic_Log__c>();
        }
        catch(Exception ex) {
            System.debug(
                'Failed to INSERT the [Apex Debug Log] ADL record. ' +
                'Error: ' + ex.getMessage()
            );
        }
           
    }
}