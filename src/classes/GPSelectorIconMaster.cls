public class GPSelectorIconMaster extends fflib_SObjectSelector {
  public List<Schema.SObjectField> getSObjectFieldList() {
       return new List<Schema.SObjectField> {
            GP_Icon_Master__c.Name
               
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Icon_Master__c.sObjectType;
    }

    public List<GP_Icon_Master__c> selectById(Set<ID> idSet) {
        return (List<GP_Icon_Master__c>) selectSObjectsById(idSet);
    }
    
    public GP_Icon_Master__c selectICONRecord(Id crnId) {
        return [Select g.GP_Status__c, g.GP_START_DATE__c, g.GP_OPPORTUNITY__c, g.GP_END_DATE__c, g.GP_CONTRACT__c From GP_Icon_Master__c g
                where Id =:crnId];
    }
    
    public list<GP_Icon_Master__c> getIconMasterDetail(set<Id>setOfIconMasterID) {
        return [Select ID,GP_CONTRACT__c,GP_START_DATE__c,GP_END_DATE__c,GP_Status__c from GP_Icon_Master__c where ID IN:setOfIconMasterID];
    }
    
    
    
}