public class GPDomainProjectBudget extends fflib_SObjectDomain {

    public GPDomainProjectBudget(List < GP_Project_Budget__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainProjectBudget(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Project_Budget__c.SObjectType
    };


    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //validateProjectBudgetRecordAccess(uow, null);
    }

    public override void onAfterInsert() {

        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, null);
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        setIsUpdateCheckBox();
        //validateProjectBudgetRecordAccess(uow, oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        // Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, oldSObjectMap);
        //uow.commitWork();

    }
    /*public override void onValidate() {
// Validate GP_Project_Budget__c 
//for(GP_Project_Budget__c  ObjProject : (List<GP_Project_Budget__c >) Records) {
//  }
}*/

    /*public override void onApplyDefaults() {

// Apply defaults to GP_Project_Budget__c 
//for(GP_Project_Budget__c  ObjProject : (List<GP_Project_Budget__c >) Records) { 
//}               
}*/

    /*public void updatePIDApproverFinance(fflib_SObjectUnitOfWork uow, Map<Id, SOBject> oldSObjectMap) {
        Set<String> fieldSet = new Set<String>();
        GP_Project_Budget__c oldProjBudget;
        Map<ID,GP_Project__c > mapofIdToobjProject = new Map<Id,GP_Project__c>();
        
        //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
        for(Schema.FieldSetMember fields :Schema.SObjectType.GP_Project_Budget__c.fieldSets.getMap().get('GP_PID_Approver_Finance').getFields()){
            fieldSet.add(fields.getFieldPath());
        }
        if(fieldSet !=null && fieldSet.size()>0)
        {
            for(GP_Project_Budget__c  objPB : (List<GP_Project_Budget__c >)records){ 
                
                mapofIdToobjProject.put(objPB.GP_Project__c, new GP_Project__c(id=objPB.GP_Project__c, GP_PID_Approver_Finance__c = false));
                
                if(mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c != true){
                    for(string eachField: fieldSet){
                        if(oldSObjectMap != null)
                        {
                            oldProjBudget = (GP_Project_Budget__c)oldSObjectMap.get(objPB.Id);
                            if(objPB.get(eachField) != oldProjBudget.get(eachField)){
                                mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true;
                            }   
                        }
                        else
                        {
                            mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true; 
                        }
                    }  
                }
            }
        }
        
        if(mapofIdToobjProject !=null && mapofIdToobjProject.size()>0)
        {
            list<GP_Project__c> lstProjecttoUpdate = new list<GP_Project__c>();
            for(GP_Project__c objPrj : mapofIdToobjProject.values())
            {
                if(objPrj.GP_PID_Approver_Finance__c == true){
                    lstProjecttoUpdate.add(objPrj);
                }
            }
            if(lstProjecttoUpdate !=null && lstProjecttoUpdate.size()>0)
            {
                uow.registerDirty(lstProjecttoUpdate);
            }
        }   
    } */


    //Method not called as not called 08052018
    //public void validateProjectBudgetRecordAccess(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
    //    Set < ID > setOfParentProjectID = new Set < ID > ();
    //    Map < ID, boolean > mapofIDAndBoolean = new Map < ID, boolean > ();
    //    for (GP_Project_Budget__c projectBudget: (List < GP_Project_Budget__c > ) records) {
    //        setOfParentProjectID.add(projectBudget.GP_Project__c);
    //    }
    //    if (setOfParentProjectID != null && setOfParentProjectID.size() > 0) {
    //        mapofIDAndBoolean = GPServiceProject.validateProjectandChildRecordAccess(setOfParentProjectID);
    //    }

    //    for (GP_Project_Budget__c projectBudget: (List < GP_Project_Budget__c > ) records) {
    //        if (mapofIDAndBoolean.get(projectBudget.GP_Project__c) == false) {
    //            projectBudget.addError('You are not a current working users so you are not authorized to work on Project Budget.');
    //        }
    //    }
    //}

    public void setIsUpdateCheckBox() {
        List < String > lstOfFieldAPINames = new List < String > ();
        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Project_Budget__c.fieldSets.getMap().get('GP_PID_Update_Fields').getFields()) {
            lstOfFieldAPINames.add(fields.getFieldPath());
        }
        Set < Id > setOfParentRecords = new Set < Id > ();
        for (GP_Project_Budget__c projectBudget: (List < GP_Project_Budget__c > ) records) {
            if (projectBudget.GP_Parent_Project_Budget__c != null)
                setOfParentRecords.add(projectBudget.GP_Parent_Project_Budget__c);
        }
        if (setOfParentRecords.size() > 0) {
            map < Id, GP_Project_Budget__c > mapOfParentRecords = new map < Id, GP_Project_Budget__c > ();
            for (GP_Project_Budget__c projectBudget: new GPSelectorProjectBudget().selectProjectBudgetRecords(setOfParentRecords)) {
                mapOfParentRecords.put(projectBudget.Id, projectBudget);
            }
            GPCommon.setIsUpdatedField(records, mapOfParentRecords, lstOfFieldAPINames, 'GP_Parent_Project_Budget__c');
        }
    }
}