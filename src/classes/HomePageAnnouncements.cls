/*************************************************************************************************************************
        * @name : HomePageAnnouncements
        * @author : Persistent
        * @description  : Used by home page component, Announcement (HomePageAnnouncements)
**************************************************************************************************************************/
public class HomePageAnnouncements {
/*************************************************************************************************************************
        * @name : getActiveAnnouncements
        * @author : Persistent
        * @description  : To get all the active annoucements by removing the one's in user preferences
        * @param  : na
        * @return : List of AnnouncementWrapper (Announcement + ContentDocument)
**************************************************************************************************************************/
    @AuraEnabled
	public static List<AnnouncementWrapper> getActiveAnnouncements()
    {
        List<AnnouncementWrapper> wrapperList = new List<AnnouncementWrapper>();
        try
        {
            //Get User preferences
            List<Id> announcementIds = new List<Id>();
            for(User_Announcements_Preference__c userPref : [select Announcement__c from User_Announcements_Preference__c where User__c=:UserInfo.getUserId() and Announcement__r.isActive__c=True limit :Limits.getLimitQueryRows()])
            {
                announcementIds.add(userPref.Announcement__c);
            }
            
            //Fetch all active announcement which are not closed by user
            Map<Id,Announcement__c> availableAnnoucements = new map<id,Announcement__c>([select id,title__c from Announcement__c where isActive__c=True and id not in :announcementIds order by valid_from__c desc limit :Limits.getLimitQueryRows()]);
            set<id> announcementIdSet = availableAnnoucements.keySet();
            Map<Id,Id> AnnToCDLIdMap = new Map<Id,Id>();
            //Special treatment to object ContentDocumentLink
            for( ContentDocumentLink cdl : [select id,contentDocumentId,LinkedEntityId from ContentDocumentLink where LinkedEntityId in :announcementIdSet limit :Limits.getLimitQueryRows()])
                AnnToCDLIdMap.put(cdl.LinkedEntityId,cdl.contentDocumentId);
            //Create and return wrapper
            for(Announcement__c announcement : availableAnnoucements.values())
            {
                wrapperList.add(new AnnouncementWrapper(announcement,AnnToCDLIdMap.get(announcement.id)));
            }
            return wrapperList;
        }
        catch(exception e)
        {
            return null;
        }
    }
    
    /*************************************************************************************************************************
        * @name : updateUserPreference
        * @author : Persistent
        * @description  : To add a record of User Announcement Preference when user clicks on close
        * @param  : Announcement Id - the one closed by the user
        * @return : na
    **************************************************************************************************************************/
    @AuraEnabled
    public static void updateUserPreference(Id AnnouncementId)
    {
        try
        {
            User_Announcements_Preference__c preference = new User_Announcements_Preference__c(User__c=UserInfo.getUserId(),Announcement__c=AnnouncementId);
            insert preference;
        }
        catch(exception e)
        {}
    }
    
    public class AnnouncementWrapper
    {
        //The announcement record
        @AuraEnabled
        public Announcement__c Announcement {get;set;}
        //The content document if any
        @AuraEnabled
        public id ContentDocumentId {get;set;}
        
        public AnnouncementWrapper(Announcement__c announcement, Id ContentDocumentId)
        {
            this.announcement = announcement;
            this.ContentDocumentId = ContentDocumentId;
        }
    }
}