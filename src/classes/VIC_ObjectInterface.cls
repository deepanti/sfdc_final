/**
    @author:        Dheeraj Chawla
    @date:          09/01/2018
    @description:   This interface is implemented by GenerateBonusLetter classes
                    (To solve the problem of Class.InnerClass and Object in
                    lightning components)
**/

public interface VIC_ObjectInterface {
    //Body
}