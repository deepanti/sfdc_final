@isTest
private class GPCmpServiceProjectDetailTracker {

    public static GP_Employee_Master__c empObj;
    public static GP_Project__c prjlst;
    public static GP_Deal__c  dealobj; 
    public static GP_Address__c address;
    public static Account accobj;
    public static GP_Project_Template__c objprjtemp;
    public static String jsonresp;
    public static GP_Icon_Master__c iconMaster;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    public static GP_Customer_Master__c customerMaster;
    public static GP_Work_Location__c objSdo;
     
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
 
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c bpmTemplate = GPCommonTracker.getProjectTemplate();
        bpmTemplate.Name = 'BPM-BPM-ALL';
        bpmTemplate.GP_Active__c = true;
        bpmTemplate.GP_Business_Type__c = 'BPM';
        bpmTemplate.GP_Business_Name__c = 'BPM';
        bpmTemplate.GP_Business_Group_L1__c = 'All';
        bpmTemplate.GP_Business_Segment_L2__c = 'All';
        insert bpmTemplate ;
        
        GP_Project_Template__c bpmWithoutSFDCTemplate = GPCommonTracker.getProjectTemplate();
        
        bpmWithoutSFDCTemplate.GP_Active__c = true;
        bpmWithoutSFDCTemplate.Name = 'BPM-BPM-ALL-Without-SFDC';
        bpmWithoutSFDCTemplate.GP_Business_Type__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Name__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Group_L1__c = 'All';
        bpmWithoutSFDCTemplate.GP_Business_Segment_L2__c = 'All';
        bpmWithoutSFDCTemplate.GP_Without_SFDC__c = true;
        
        insert bpmWithoutSFDCTemplate;
        
        GP_Project_Template__c indirectTemp = GPCommonTracker.getProjectTemplate();
        indirectTemp.GP_Active__c = true;
        indirectTemp.Name = 'INDIRECT_INDIRECT';
        indirectTemp.GP_Business_Type__c = 'Indirect';
        indirectTemp.GP_Business_Name__c = 'Indirect';
        indirectTemp.GP_Business_Group_L1__c = 'All';
        indirectTemp.GP_Business_Segment_L2__c = 'All';
        insert indirectTemp;
        
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
		prjObj.GP_Country__c='US';
        prjObj.GP_State__c='TX';
		// Gainshare Change
        prjObj.GP_Customer_Hierarchy_L4__c = accobj.Id;
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        insert timeshtentryObj ;
        customerMaster = new GP_Customer_Master__c();
        insert customerMaster;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        address.GP_Customer__c = customerMaster.Id;
        address.GP_Billing_Entity__c = objpinnacleMaster.Id;
        address.GP_Status__c = 'Active';
        address.GP_BILL_TO_FLAG__c = true;
        address.GP_SHIP_TO_FLAG__c = true;
        
        insert address ;
        
        GP_Address__c  dummyAddress = GPCommonTracker.getAddress();
        dummyAddress.GP_Customer__c = customerMaster.Id;
        dummyAddress.GP_Billing_Entity__c = objpinnacleMaster.Id;
        dummyAddress.GP_Status__c = 'Active';
        dummyAddress.GP_BILL_TO_FLAG__c = true;
        dummyAddress.GP_SHIP_TO_FLAG__c = true;
        dummyAddress.GP_City__c = 'GENPACT Dummy';
        
        insert dummyAddress;
        
        GP_Product_Master__c productMaster = new GP_Product_Master__c();
        productMaster.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster.GP_Product_Family__c = 'Sample Family';
        productMaster.GP_Service_Line__c = 'Sample Service';
        
        insert productMaster;
        
        GP_Product_Master__c productMaster2 = new GP_Product_Master__c();
        productMaster2.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster2.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster2.GP_Product_Family__c = 'Sample Family';
        
        insert productMaster2;
		
		// Gainshare change
        GP_Icon_Master__c objIconMasterForIcon = new GP_Icon_Master__c();
        objIconMasterForIcon.GP_CONTRACT__c = 'Test Contract';
        objIconMasterForIcon.GP_Status__C = 'Expired';
        objIconMasterForIcon.GP_END_DATE__c = date.newInstance(2017, 12, 25);
        objIconMasterForIcon.GP_START_DATE__c = system.today();
        objIconMasterForIcon.GP_ACCOUNT_CODE__c = accobj.Id;
        insert objIconMasterForIcon;
    }    
    
    @isTest
    public static void testGPCmpServiceProjectDetail()
    {
        fetchData();
        testgetRecordType();
        testgetRecordTypeInvalid();
        testsaveProject();
        testsaveProjectInvalid();
        testgetProjectTemplate();
        testgetProjectData();
        testgetDefaultProjectData();
        testgetDefaultProjectDataInvalid();
        testgetAccountRecord();
        testgetAccountRecordInvalid();
        //testgetProjectContainerStatus();   
        //testgetProjectContainerStatusInvalid();
        testgetOUCurrency();
        testgetOUCurrencywithoutID();
        testgetProjectsForDeal();
        testgetDealPicklist();
        testgetDealData();
        testgetVerticalSubVerticalForAccount();
        testcloneBPMProject();
        testsaveLegacyProject();
        testgetFilteredListOfTaxCode();
        
        testgetDefaultOUForIndirectProject();
        testgetSDOData();
        testgetPickList();
        testgetCRNData();
        testgetParentProjectForGroupInvoice();
        testgetParentProjectInvoice();
        testgetIsAvailableForLegacyProjectCreation();
        testgetFilteredSOD();
		
		testgetBillToAddressForProject();
        testgetDependentMap();
        testCheckAvalaraLECodeExists();
		testSetPOFlag();
		testgetOperatingUnitLECode();
        testSetServiceDeliveryInCanadaFlag();
		
		// Gainshare Change
        testgetCRNDataPerCustomerHierarachy();
    }
    
    public static void fetchData() {
        empObj = [select id from GP_Employee_Master__c limit 1];
		// Gainshare Change
        prjlst = [select id,Name,GP_Customer_Hierarchy_L4__c from GP_Project__c limit 1];        
        dealobj = [select id,Name, GP_Business_Group_L1__c, GP_Business_Segment_L2__c, GP_Deal_Type__c from GP_Deal__c  limit 1];        
        address = [select id,Name from GP_Address__c  limit 1];        
        accobj = [select id,Name from Account limit 1];
        objprjtemp = [select id from GP_Project_Template__c limit 1];
        jsonresp= (String)JSON.serialize(prjlst); 
        iconMaster = [Select Id from GP_Icon_Master__c LIMIT 1];
        objpinnacleMaster = [Select Id from GP_Pinnacle_Master__c LIMIT 1];
        customerMaster = [Select Id from GP_Customer_Master__c LIMIT 1];
        
        objSdo = [Select Id from GP_Work_Location__c LIMIT 1];
    }
    
    public static void testgetRecordType(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getRecordType('GP_Project__c');
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetRecordTypeInvalid(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getRecordType('GP_Project_c');
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    public static void testsaveProject(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.saveProject(jsonresp);
        System.assertEquals(true, returnedResponse.isSuccess);
        
        Id bpmRecordTypeId = GPCommon.getRecordTypeId('GP_Project__c', 'BPM');
		GP_Project__c project = (GP_Project__c) JSON.deserialize(jsonresp, GP_Project__c.class);
        project.RecordTypeId = bpmRecordTypeId;
        project.Id = null;
        jsonresp = JSON.serialize(project);
        returnedResponse = GPCmpServiceProjectDetail.saveProject(jsonresp);
        
        //System.assertEquals(true, returnedResponse.isSuccess);        
    }
    
    public static void testsaveProjectInvalid(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.saveProject(null);
        System.assertEquals(false, returnedResponse.isSuccess);        
    }
    
    public static void testgetProjectTemplate(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getProjectTemplate(objprjtemp.Id, 'group1', 'segment2', false, false, false);
        returnedResponse = GPCmpServiceProjectDetail.getProjectTemplate(objSdo.Id, 'group1', 'segment2', false, false,false);
        returnedResponse = GPCmpServiceProjectDetail.getProjectTemplate(objSdo.Id, 'All', 'All', true, false,false);
        returnedResponse = GPCmpServiceProjectDetail.getProjectTemplate(objSdo.Id, 'All', 'All', false, true,false);
    }
    
    public static void testgetProjectData(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getProjectData(prjlst.id);
        //commented because logic at Role is failing.
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetDefaultProjectData(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getDefaultProjectData(dealobj.Id );
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetDefaultProjectDataInvalid(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getDefaultProjectData(null);
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    public static void testgetAccountRecord(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getAccountRecord(accobj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetAccountRecordInvalid(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getAccountRecord(null);
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
     public static void testgetOUCurrency() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getOUCurrency(objpinnacleMaster.Id); 
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetOUCurrencywithoutID() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getOUCurrency(null); 
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    public static void testgetDealPicklist() {
        User u = [Select Id from User Limit 1];
        System.runAs(u) {
            GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getDealPicklist(); 
            System.assertEquals(true, returnedResponse.isSuccess);     
        }
    }
    public static void testgetProjectsForDeal() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getProjectsForDeal(dealobj.Id); 
        System.assertEquals(true, returnedResponse.isSuccess);        
        returnedResponse = GPCmpServiceProjectDetail.getProjectsForDeal(null); 
    }
    public static void testgetDealData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getDealData(dealobj.Id); 
        System.assertEquals(true, returnedResponse.isSuccess);

        returnedResponse = GPCmpServiceProjectDetail.getDealData(null);         
    }
     
    public static void testgetVerticalSubVerticalForAccount() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getVerticalSubVerticalForAccount(accobj.Id); 
        System.assertEquals(true, returnedResponse.isSuccess);        
        GPAuraResponse returnedInvalidResponse = GPCmpServiceProjectDetail.getVerticalSubVerticalForAccount(null); 
        System.assertEquals(false, returnedInvalidResponse.isSuccess);        
    }
    public static void testcloneBPMProject() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.cloneBPMProject(prjlst.Id,null); 
    }
    public static void testsaveLegacyProject() {
        GPAuraResponse returnedInvalidResponse = GPCmpServiceProjectDetail.saveLegacyProject(JSON.serialize(prjlst), JSON.serialize(dealobj)); 
        System.assertEquals(false, returnedInvalidResponse.isSuccess); 
        
        GP_Deal__c newDeal = new GP_Deal__c(); 
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.saveLegacyProject(JSON.serialize(prjlst), JSON.serialize(newDeal)); 
        System.assertEquals(true, returnedResponse.isSuccess); 
    } 
    
    
    public static void testgetFilteredListOfTaxCode() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getFilteredListOfTaxCode(null); 
        
        System.assertEquals(false, returnedResponse.isSuccess); 
        
        Id taxCodeRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Tax Code Master');
        GP_Pinnacle_Master__c taxCode = GPCommonTracker.GetpinnacleMaster();
 		taxCode.GP_Entity_Id__c = '123';
        taxCode.RecordTypeId = taxCodeRecordTypeId;
        insert taxCode;
        returnedResponse = GPCmpServiceProjectDetail.getFilteredListOfTaxCode('123'); 
    } 
    
    public static void testgetDefaultOUForIndirectProject() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getDefaultOUForIndirectProject();

        Id billingEntityRecordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        
        GP_Pinnacle_Master__c defaultOperatingUnit = GPCommonTracker.GetpinnacleMaster();
        defaultOperatingUnit.Name = 'Genpact International Inc.';
        defaultOperatingUnit.RecordTypeId = billingEntityRecordTypeId;
        insert defaultOperatingUnit;
     
        returnedResponse = GPCmpServiceProjectDetail.getDefaultOUForIndirectProject();
    }
    
    
    public static void testgetSDOData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getSDOData(objSdo.Id);
        returnedResponse = GPCmpServiceProjectDetail.getSDOData(null);
    }
    
    public static void testgetPickList() {
        String serializedProjectData = jsonresp;
        String serializedDealData = JSON.serialize(dealobj);
        
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getPickList(serializedProjectData, serializedDealData);
    }
    
    public static void testgetCRNData() {        
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getCRNData(iconMaster.Id);
        returnedResponse = GPCmpServiceProjectDetail.getCRNData(null);
    }
	
    public static void testgetParentProjectForGroupInvoice() {        
        String serializedProjectData = jsonresp;
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getParentProjectForGroupInvoice(serializedProjectData);
    }
    
	public static void testgetParentProjectInvoice() {        
        String serializedProjectData = jsonresp;
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getParentProjectInvoice(serializedProjectData);
    }

    public static void testgetIsAvailableForLegacyProjectCreation() {        
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getIsAvailableForLegacyProjectCreation();
    }
    
	public static void testgetFilteredSOD() {        
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getFilteredSOD(false,false);
        returnedResponse = GPCmpServiceProjectDetail.getFilteredSOD(true,false);
    }	
	public static void testgetBillToAddressForProject() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectDetail.getBillToAddressForProject(prjlst.Id); 
    }
	public static void testgetDependentMap() {
        //sObject objDetail=new sObject();
        //objDetail.add('Contact');
        GP_Project__c objPrj=new GP_Project__c();       
        GPCmpServiceProjectDetail.getDependentMap(objPrj,'GP_Country__c','GP_State__c'); 
    }
    public static void testCheckAvalaraLECodeExists() {
        
        List<GP_Avalara_LE_Code__c> lstCustSet=new List<GP_Avalara_LE_Code__c>();
        GP_Avalara_LE_Code__c objLeCode = new GP_Avalara_LE_Code__c();
        objLeCode.Name ='LE Code List';
        objLeCode.LE_Code__c='3448,3446,3447,9711,3401,3416,3424,3426,3427,3430,3432,3434,3201,3435,3436,3438,3439,3441,3442,3412';
        lstCustSet.add(objLeCode);
        insert lstCustSet;
        
        GPCmpServiceProjectDetail.CheckAvalaraLECodeExists('9711');   
        GPCmpServiceProjectDetail.CheckAvalaraLECodeExists('9711123'); 
        
        GPCmpServiceProjectDetail.PicklistEntryWrapper objWrapper=new GPCmpServiceProjectDetail.PicklistEntryWrapper();    
		objWrapper.active='True';
		objWrapper.defaultValue='test';
		objWrapper.label='test';
		objWrapper.validFor='test';
		objWrapper.value='test';
		
        GPCmpServiceProjectDetail.base64ToBits('test123');
        GPCmpServiceProjectDetail.base64ToBits('');
        GPCmpServiceProjectDetail.decimalToBinary(7);
    }
	 public static void testSetPOFlag() {
             
        GPCmpServiceProjectDetail.SetPOFlag(prjlst.Id,true); 
    }
    public static void testgetOperatingUnitLECode() {
             
        GPCmpServiceProjectDetail.getOperatingUnitLECode(prjlst.Id); 
    }
    public static void testSetServiceDeliveryInCanadaFlag() {
        GPCmpServiceProjectDetail.SetServiceDeliveryInCanadaFlag(prjlst.Id,false); 
    }	
	// Gainshare Change
    public static void testgetCRNDataPerCustomerHierarachy() {
        GPCmpServiceProjectDetail.getCRNDataPerCustomerHierarachy(prjlst.GP_Customer_Hierarchy_L4__c);
    }
}