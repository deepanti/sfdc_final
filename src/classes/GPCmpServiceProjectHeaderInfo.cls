public with sharing class GPCmpServiceProjectHeaderInfo {
    @AuraEnabled
    public static GPAuraResponse getProjectDetails(Id projectId) {
        GPControllerProjectHeaderInfo headerInfoController = new GPControllerProjectHeaderInfo(projectId);
        return headerInfoController.getProjectDetails();
    }
}