@isTest
private class GPDomainAddressTracker {
    
    @testSetup
    private static void setupCommonData(){
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_address__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Customer_Master__c customerObj = new GP_Customer_Master__c();
        customerObj.Name = 'New Customer';
        customerObj.GP_Oracle_Customer_Id__c = '1234';
        insert customerObj;
        
        GP_Pinnacle_Master__c pinnacleObj = GPCommonTracker.GetpinnacleMaster();
        pinnacleObj.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        pinnacleObj.GP_Oracle_Id__c = '1234';
        insert pinnacleObj;
        
        GP_Address__c objaddress = new GP_Address__c();
        objaddress.GP_Address_Line_1__c = 'New Delhi';
        objaddress.GP_Billing_Entity_Id__c ='1234';
        objaddress.GP_Customer_Account_ID__c = '1234';
        insert objaddress;
        objaddress.GP_Billing_Entity_Id__c = null;
        objaddress.GP_Customer_Account_ID__c = null;
        update objaddress;
        objaddress.GP_Billing_Entity_Id__c = '000';
        objaddress.GP_Customer_Account_ID__c = '000';
        update objaddress;
    }
     @isTest
    public static void testGPDomainAddress()
    {
    }
}