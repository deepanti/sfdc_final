@isTest
public class VIC_HuntingCreatedDateTest {
     
     public static Account objAccount;
     public static Opportunity objOpp;
     
     static testmethod void VIC_HuntingCreatedDateTest(){
         loadData();
         objAccount.Hunting_Mining__c = 'Hunting';
         test.starttest(); 
         update objAccount;
          test.stoptest(); 
        
         Account acc = [Select Id,name,VIC_Hunting_Start_Date__c from Account where Id=:objAccount.Id];
         System.assertEquals(acc.VIC_Hunting_Start_Date__c, objopp.CloseDate);
         
     }
     static void loadData(){
       
       User objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
       insert objuser;
       User objuser1= VIC_CommonTest.createUser('Test1','Test1 Name','jdncjn@jdjhdg1.com','Genpact Sales Rep','China');
       insert objuser1;
       User objuser2= VIC_CommonTest.createUser('Test2','Test2 Name','jdncjn@jdjhdg2.com','System Administrator','China');
       insert objuser2;
         
       System.runAs(objuser){ 
           VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
           vicInfo.VIC_Process_Year__c=2018;
           insert vicInfo;
             
           objAccount=VIC_CommonTest.createAccount('Test Account');
           insert objAccount;
           
           objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
           objOpp.Actual_Close_Date__c=system.today();
           objOpp.CloseDate = system.today();
           objOpp.vic_Is_Kickers_Calculated__c=false;            
           objOpp.Number_of_Contract__c = 12;
           insert objOpp;
           
           objOpp.StageName='6. Signed Deal';
           update  objOpp;
           
           OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
           objOLI.vic_Final_Data_Received_From_CPQ__c=true;
           objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
           objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
           objOLI.Product_BD_Rep__c=UserInfo.getUserID();
           objOLI.vic_VIC_User_3__c=objuser1.id;
           objOLI.vic_VIC_User_3_Split__c = 17;
           objOLI.vic_VIC_User_4__c=objuser2.id;
           objOLI.vic_VIC_User_4_Split__c = 10;
           objOLI.vic_is_CPQ_Value_Changed__c = true;
           objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
           objOLI.Contract_Term__c=36;
           objOLI.TCV__c=3500000;
           insert objOLI;
           
           Opportunity objOpp1=VIC_CommonTest.createOpportunity('Test Opp1','Prediscover','Ramp Up',objAccount.id);
           objOpp1.Actual_Close_Date__c=system.today().addDays(60);
           objOpp1.CloseDate = system.today().addDays(60);
           objOpp1.vic_Is_Kickers_Calculated__c=false;
           objOpp1.Number_of_Contract__c = 12;
           insert objOpp1;    
           
           objOpp1.StageName='6. Signed Deal';
           update  objOpp1;
           OpportunityLineItem  objOLI1= VIC_CommonTest.createOpportunityLineItem('Active',objOpp1.id);
           objOLI1.vic_Final_Data_Received_From_CPQ__c=true;
           objOLI1.vic_Sales_Rep_Approval_Status__c = 'Approved';
           objOLI1.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
           objOLI1.Product_BD_Rep__c=UserInfo.getUserID();       
           objOLI1.vic_is_CPQ_Value_Changed__c = true;
           objOLI1.vic_VIC_User_3__c=objuser1.id;
           objOLI1.vic_VIC_User_3_Split__c = 19;
           objOLI1.vic_VIC_User_4__c=objuser2.id;
           objOLI1.vic_VIC_User_4_Split__c = 20;
           objOLI1.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
           objOLI1.Contract_Term__c=36;
           objOLI1.TCV__c=3500000;
           insert objOLI1;
       }
       
     }
}