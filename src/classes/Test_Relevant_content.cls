@isTest
public class Test_Relevant_content
{ 


static testMethod void method()
    {    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
            insert u;
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
            insert salesunitobject;    
        
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
            insert accountobject;
      
       
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
        insert opp;
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
         insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        test.stopTest();
       // system.assertEquals( opp.Service_Line__c,'Multi Channel Customer Service,');
        //system.debug('value check '+opp.Service_Line__c);
       
        
    }
    
    
    
    static testMethod void method2()
    {   
        test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
            insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
            insert accountobject;
        
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='',Competitor__c='Accenture',Deal_Type__c='Competitive');
            insert opp;
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
            insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
            insert oppcontactrole;

        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
            insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Collections',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
            insert OLi;
        
        ApexPages.StandardController std = new ApexPages.StandardController(opp);
        ReleVant_Content_class controller = new ReleVant_Content_class(std);
            controller.display='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/_layouts/15/WopiFrame.aspx?sourcedoc={005ac493-3333-4bda-934f-016719a71b73}&action=view';
            controller.retrieve_items();
            //controller.W_L_D__c='Won Reason';
            
            controller.retrieve_items_product_family();
            controller.retrieve_items_service_line();
            controller.retrieve_Internal_Experts();
            controller.retrieve_External_Experts();
            controller.retrieve_items_competitors();
        
            controller.retrieve_All();
            controller.Commentbody ='testing';
            controller.postFileToFeed();
            controller.showdoc();
            controller.Shareonpage();
            controller.Reset();
            controller.Requetsanexpert();
       
            //controller.Custom_cancel();
             Test.stoptest();
     
        }
    
     
    
    
}