@isTest
public class OpportunityTriggerHelperTest {
    
    public static Opportunity opp, opp1, opp2, opp3, opp4, opp5, opp6, opp7, opp8, opp9, opp10,
        opp11, opp12, opp13, opp14, opp15, opp16;
    public static Contact oContact;
    public static User u;
    
    public static testMethod void insertOpportunityTest(){  
        setupTestData();
        List<Opportunity>opportunity_list = new List<Opportunity>();
        opportunity_list.add(opp);
        OpportunityTriggerHelper.insertContactRole(opportunity_list);
        OpportunityTriggerHelper.opportunityRestrictMethod(opportunity_list);
        MainOppTriggerHelper.beforeInsertMethod(opportunity_list);
        MainOppTriggerHelper.afterUndeleteMethod(opportunity_list);
        
    }
    
    public static testMethod void updateOpportunityTest(){   
        setupTestData();
        Test.startTest();
        Map<ID, Opportunity> old_opp = new Map<Id,Opportunity>();
        old_opp.put(opp.id,opp);
        System.runAs(u){
            opp.Role__c = 'Influencer';
            update opp;
            List<Opportunity> new_opp = new List<Opportunity>();
            new_opp.add(opp);
            OpportunityTriggerHelper.updateContactRole(new_opp, old_opp);
            OpportunityTriggerHelper.dateUpdateMethod(new_opp, old_opp);//beforeDeleteMethod
            MainOppTriggerHelper.beforeUpdateMethod(new_opp, old_opp);
            MainOppTriggerHelper.beforeDeleteMethod(new_opp, old_opp);
            MainOppTriggerHelper.afterInsertMethod(new_opp, old_opp);
            MainOppTriggerHelper.afterUpdateMethod(new_opp, old_opp);
            MainOppTriggerHelper.afterDeleteMethod(new_opp, old_opp);
            
            Test.stopTest();
        }       
    }
    
     public static testMethod void updateOpportunityTestwithExistingContactRole(){   
        setupTestData();
        Test.startTest();
         Map<ID, Opportunity> old_opp = new Map<Id,Opportunity>();
        old_opp.put(opp2.id,opp2);
        System.runAs(u){
           // Contact_Role_RV__c contact_role2 = new Contact_Role_RV__c(contact__c = oContact.id, isPrimary__c = true, opportunity__c = opp2.ID);
           // insert contact_role2;
            opp2.Contact1__c = oContact.id;
            opp2.Role__c = 'Influencer';
            //CheckRecursive.run = true;
            update opp2;
             List<Opportunity> new_opp = new List<Opportunity>();
            new_opp.add(opp2);
            OpportunityTriggerHelper.updateContactRole(new_opp, old_opp);
            Test.stopTest();
        }       
    }
    
   public static testMethod void deleteOpportunityTest(){   
        setupTestData();
        System.runas(u){
            Test.startTest();
            CheckRecursive.run = true;
            delete opp;  
        }       
    }
    
    public static testMethod void OpportunityExceptionTest(){
        Test.startTest();
        OpportunityTriggerHelper.deleteContactRole(null);
         OpportunityTriggerHelper.insertContactRole(null);
        Test.stopTest();
    }
    
    public static testMethod void getContactRoleTest(){
        setupTestData();
        Test.startTest();
        OpportunityTriggerHelper.getContactRole(opp);
        Test.stopTest();
    }
    
  public static testMethod void testDealCycleTest(){
        setupTestDataForDealCycle();
        Set<ID> oppIDs = new Set<ID>();
        oppIDs.add(opp1.ID);
        oppIDs.add(opp2.ID);
        oppIDs.add(opp3.ID);
        oppIDs.add(opp4.ID);
        oppIDs.add(opp5.ID);
        oppIDs.add(opp6.ID);
        oppIDs.add(opp7.ID);
        oppIDs.add(opp8.ID);
        oppIDs.add(opp9.ID);
        oppIDs.add(opp10.ID);
        oppIDs.add(opp11.ID);
        oppIDs.add(opp12.ID);
        oppIDs.add(opp13.ID);
        oppIDs.add(opp14.ID);
        oppIDs.add(opp15.ID);
        oppIDs.add(opp16.ID);
        
      OpportunityTriggerHelper.saveDealCycleAging(oppIDs);
    } 
    
    private static void setupTestData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        //oAccount.Sales_Unit__c = salesunit.id;
        
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        System.runAs(u){
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp=new opportunity(name='1234',StageName='1. Discover',Nature_of_Work_highest__c = 'Consulting', CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                Competitor__c='Accenture',contact1__c = oContact.ID, role__c = 'Other');
            
           opp2=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(opp);
            oppList.add(opp2);
          CheckRecursive.run = true;
            insert oppList;
            list<MainOpportunityHelperFlag__c> custmSettingList = new list<MainOpportunityHelperFlag__c>();
            MainOpportunityHelperFlag__c setting = new MainOpportunityHelperFlag__c(Name = 'opportunity',HelperName__c = true);
            MainOpportunityHelperFlag__c setting1 = new MainOpportunityHelperFlag__c(Name = 'beforeInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting2 = new MainOpportunityHelperFlag__c(Name = 'beforeUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting3 = new MainOpportunityHelperFlag__c(Name = 'beforeDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting4 = new MainOpportunityHelperFlag__c(Name = 'afterInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting5 = new MainOpportunityHelperFlag__c(Name = 'afterUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting6 = new MainOpportunityHelperFlag__c(Name = 'afterDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting7 = new MainOpportunityHelperFlag__c(Name = 'holdOpportunityTeam',HelperName__c = true);
            MainOpportunityHelperFlag__c setting8 = new MainOpportunityHelperFlag__c(Name = 'UpdatePrediscoverMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting9 = new MainOpportunityHelperFlag__c(Name = 'opportunityRestrictMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingq = new MainOpportunityHelperFlag__c(Name = 'insertContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingw = new MainOpportunityHelperFlag__c(Name = 'saveDealCycleAging',HelperName__c = true);
            MainOpportunityHelperFlag__c settinge = new MainOpportunityHelperFlag__c(Name = 'createTaskforOpp',HelperName__c = true);
            MainOpportunityHelperFlag__c settingr = new MainOpportunityHelperFlag__c(Name = 'createTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingt = new MainOpportunityHelperFlag__c(Name = 'dateUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingy = new MainOpportunityHelperFlag__c(Name = 'staleDealMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settinga = new MainOpportunityHelperFlag__c(Name = 'dealTypeMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settings = new MainOpportunityHelperFlag__c(Name = 'updateContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingd = new MainOpportunityHelperFlag__c(Name = 'removeTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingf = new MainOpportunityHelperFlag__c(Name = 'Prediscover_RemoveTasksMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingg = new MainOpportunityHelperFlag__c(Name = 'deleteOppSplitOwneronOppOwnerchange',HelperName__c = true);
            MainOpportunityHelperFlag__c settingh = new MainOpportunityHelperFlag__c(Name = 'deleteContactRole',HelperName__c = true);
       		custmSettingList.add(setting); custmSettingList.add(setting1); custmSettingList.add(setting2);custmSettingList.add(setting3);
            custmSettingList.add(setting4); custmSettingList.add(setting5); custmSettingList.add(setting6);custmSettingList.add(setting7);
            custmSettingList.add(setting8); custmSettingList.add(setting9); custmSettingList.add(settingq);custmSettingList.add(settingw);
            custmSettingList.add(settinge); custmSettingList.add(settingr); custmSettingList.add(settingt);custmSettingList.add(settingy);//setting2,setting3,setting4,setting5,setting6,setting7,setting8,setting9,settingq,settingw,settinge,settingr,settingt,settingy);
    		custmSettingList.add(settinga); custmSettingList.add(settings); custmSettingList.add(settingd);custmSettingList.add(settingf);
            custmSettingList.add(settingg); custmSettingList.add(settingh); 
            Insert custmSettingList;
            
            
            
        }
    }
    
    private static void setupTestDataForDealCycle(){
        
         Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes', Stage__c='2. Define', Cycle_Time__c=400, Type_of_Deal__c='Retail', Ageing__c=30);
            Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes', Stage__c='2. Define', Cycle_Time__c=400, Type_of_Deal__c='Large', Ageing__c=30);
           
            Deal_Cycle__c   DC3= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC4= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC5= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC6= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC7= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC8= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC9= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC10= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC11= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC12= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC13= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC14= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC15= new Deal_Cycle__c(Transformation_Deal__c='Yes', Stage__c='1. Discover', Cycle_Time__c=400, Type_of_Deal__c='Retail', Ageing__c=30);
            Deal_Cycle__c   DC16= new Deal_Cycle__c(Transformation_Deal__c='Yes', Stage__c='1. Discover', Cycle_Time__c=400, Type_of_Deal__c='Large', Ageing__c=30);
           
            Deal_Cycle__c   DC17= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC18= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC19= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC20= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC21= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC22= new Deal_Cycle__c(Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC23= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC24= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            Deal_Cycle__c   DC25= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced', Cycle_Time__c=118);
            
            Deal_Cycle__c   DC26= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC27= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive', Cycle_Time__c=118);
            Deal_Cycle__c   DC28= new Deal_Cycle__c(Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive', Cycle_Time__c=118);
            
             
            List<Deal_Cycle__c> dc_list = new List<Deal_Cycle__C>();
            dc_list.add(DC1);   dc_list.add(DC15);
            dc_list.add(DC2);   dc_list.add(DC16);
            dc_list.add(DC3);   dc_list.add(DC17);
            dc_list.add(DC4);   dc_list.add(DC18);
            dc_list.add(DC5);   dc_list.add(DC19);
            dc_list.add(DC6);   dc_list.add(DC20);
            dc_list.add(DC7);   dc_list.add(DC21);
            dc_list.add(DC8);   dc_list.add(DC22);
            dc_list.add(DC9);   dc_list.add(DC23);
            dc_list.add(DC10);  dc_list.add(DC24);
            dc_list.add(DC11);  dc_list.add(DC25);
            dc_list.add(DC12);  dc_list.add(DC26);
            dc_list.add(DC13);  dc_list.add(DC27);
            dc_list.add(DC14);  dc_list.add(DC28);
            
            insert dc_list;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        User u1 = GEN_Util_Test_Data.CreateUser('standarduser2016@testorg.com',p.Id,'standardusertestgen2016@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Hunting','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        Account oAccount1 = CreateAccount(Userinfo.getUserId(),u1.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining','12','12345676'); 
        oAccount1.Sales_Unit__c = salesunit.id;
        
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        System.runAs(u){
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            //For TS opp Testing
            opp1=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 10000, Insight__c = 30,
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other',  Nature_of_Work_highest__c = 'consulting');
            
            opp2=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 2000000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Sole Sourced', Nature_of_Work_highest__c = 'consulting');
            
            opp3=new opportunity(name='1234',StageName='2. Define',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 2000000, Insight__c = 30,
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other',  Nature_of_Work_highest__c = 'consulting');
            
            opp4=new opportunity(name='1234',StageName='2. Define',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 20000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Sole Sourced', Nature_of_Work_highest__c = 'consulting');
     
            //For Non-TS opp Testing
            opp5=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Amount= 10000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Sole Sourced', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp6=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Amount= 1100000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Sole Sourced', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp7=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Amount= 40000000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Sole Sourced',  Nature_of_Work_highest__c = 'IT services');
            
            opp8=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Amount= 10000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp9=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Amount= 1100000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp10=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Amount= 40000000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Competitive',  Nature_of_Work_highest__c = 'IT services');
            
            opp11=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='', Amount= 10000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Sole Sourced', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp12=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='', Amount= 1100000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Sole Sourced', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp13=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 40000000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Sole Sourced',  Nature_of_Work_highest__c = 'IT services');
            
            opp14=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='', Amount= 10000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp15=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='', Amount= 1100000, Insight__c = 30, 
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other', Nature_of_Work_highest__c = 'IT services');
            
            opp16=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount1.id,W_L_D__c='',Amount= 40000000, Insight__c = 30,
                                 Competitor__c='Accenture',Deal_Type__c='Competitive',  Nature_of_Work_highest__c = 'IT services');
            
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(opp1);
            oppList.add(opp2);
            oppList.add(opp3);
            oppList.add(opp4);
            oppList.add(opp5);
            oppList.add(opp6);
            oppList.add(opp7);
            oppList.add(opp8);
            oppList.add(opp9);
            oppList.add(opp10);
            oppList.add(opp11);
            oppList.add(opp12);
            oppList.add(opp13);
            oppList.add(opp14);
            oppList.add(opp15);
            oppList.add(opp16);
            test.startTest();
            insert oppList;
            test.stopTest();
            
        }
    }
    
     private static Account CreateAccount(id userid ,id uId, id AId, String strName,String strBusiness_Group,Id oBS,Id oSB,String strIndustry_Vertical,String strSub_industry_vertical,string strAccount_classification,string strForbes_Company_Name,String strAccountNum)
    {
         Account oAccount = new Account();
         oAccount.Name = strName;
         oAccount.AccountNumber = strAccountNum;
         oAccount.Business_group__c= strBusiness_Group;
         oAccount.business_segment__c = oBS;
         oAccount.sub_business__c= oSB;
         oAccount.industry_vertical__c= strIndustry_Vertical;
         oAccount.sub_industry_vertical__c= strSub_industry_vertical;
         //oAccount.Account_classification__c= strAccount_classification;
         //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
         oAccount.website = 'www.genpact.com';
         oAccount.sic= 'test code';
         oAccount.hunting_mining__c= strAccount_classification;
         oAccount.TAM__c=5000;
         oAccount.PTAM__c=6000;
         oAccount.Archetype__c=AId;
         //oAccount.DUNS_Number__c=454554;    
         oAccount.Client_Partner__c=userid;
         oAccount.Primary_Account_GRM__c=uId;
         oAccount.TickerSymbol= 'test ticker';
         oAccount.AnnualRevenue= 50000;
         oAccount.ownerid=uId;
         oAccount.Ownership= 'test Owner';
         oAccount.Account_headquarters_country__c= 'test Account headquarters';
         oAccount.Account_Owner_from_ACR__c=uId;
         //oAccount.base_country__c= 'test base country';
         insert oAccount;
         return oAccount;
    }
}