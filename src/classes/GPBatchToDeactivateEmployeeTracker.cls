@isTest
public class GPBatchToDeactivateEmployeeTracker {
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c employee = GPCommonTracker.getEmployee();
        employee.GP_ACTUAL_TERMINATION_Date__c = System.Today();
        employee.GP_EMPLOYEE_TYPE__c = 'Contractor';
        employee.GP_isActive__c = true;
        insert employee;       
        
    }
    @isTest
    public static void testGPBatchToDeactivateEmployee() {
      GPBatchToDeactivateEmployee batcher = new GPBatchToDeactivateEmployee();
        Id batchprocessid = Database.executeBatch(batcher, 10);
    }    
   
}