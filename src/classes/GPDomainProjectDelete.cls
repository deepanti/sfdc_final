public without sharing class GPDomainProjectDelete {

	// BPH Change : tag the triggerd PID to parent PID's pending for Approval PID. (Make sibling as parent)
    public void reparentingThePIDs(List < GP_Project__c > projects, Map < Id, GP_Project__c > triggerOldMap) {
        Map < Id,Id > mapOfParentWithChildPIDs = new Map < Id,Id > ();
        List<GP_Project__c> listOfSiblingPIds = new List<GP_Project__c>();
        
        for (GP_Project__c project: projects) {
            if (triggerOldMap.get(project.Id).GP_Oracle_Status__c != project.GP_Oracle_Status__c
                && project.GP_Approval_Status__c == 'Approved'
                && project.GP_Oracle_Status__c == 'S' 
                && project.GP_Parent_Project__c != null
                && project.GP_Clone_Source__c == 'Batch - Hierarchy Update')
            {
				mapOfParentWithChildPIDs.put(project.GP_Parent_Project__c, project.Id);
            }
        }
        
        if(mapOfParentWithChildPIDs.keySet().size() > 0) {
            for(GP_Project__c pid : [SELECT id, GP_Parent_Project__c FROM GP_Project__c 
                                     WHERE GP_Parent_Project__c in: mapOfParentWithChildPIDs.keySet() 
                                     AND Id not in: mapOfParentWithChildPIDs.values()]) {
                if(mapOfParentWithChildPIDs.containsKey(pid.GP_Parent_Project__c)) {
                    pid.GP_Parent_Project__c = mapOfParentWithChildPIDs.get(pid.GP_Parent_Project__c);
                    listOfSiblingPIds.add(pid);
                }
            }
        }
        
        if(listOfSiblingPIds.size() > 0) {
            update listOfSiblingPIds;
        }
    }
    
    public void deleteParentApprovedProjectAndMoveVersionHistory(List < GP_Project__c > projects, Map < Id, GP_Project__c > triggerOldMap) {
        Set < Id > setOfProjectIdsToBeDeleted = new Set < Id > ();
        Map < Id, Id > mapOfParentProjectIdVsProjectId = new Map < Id, Id > ();
        List < GP_project__c > listOfProjectsToBeDeleted = new List < GP_Project__c > ();
        
        for (GP_Project__c project: projects) {
            if ((triggerOldMap.get(project.Id).GP_Approval_Status__c != project.GP_Approval_Status__c || triggerOldMap.get(project.Id).GP_Oracle_Status__c != project.GP_Oracle_Status__c) &&
                (project.GP_Approval_Status__c == 'Approved' || project.GP_Approval_Status__c == 'Closed') && project.GP_Oracle_Status__c == 'S' && project.GP_Parent_Project__c != null) {
                    setOfProjectIdsToBeDeleted.add(project.GP_Parent_Project__c);
                    mapOfParentProjectIdVsProjectId.put(project.GP_Parent_Project__c, project.Id);
                    GP_Project__c parentProject = new GP_Project__c(id = project.GP_Parent_Project__c);
                    listOfProjectsToBeDeleted.add(parentProject);
                }
        }
        System.debug('setOfProjectIdsToBeDeleted::'+setOfProjectIdsToBeDeleted);
        if (setOfProjectIdsToBeDeleted.size() > 0) {
            
            List < GP_Project_Version_History__c > listOfProjectVersionsToBeUpdated = new List < GP_Project_Version_History__c > ();
            for (GP_Project_Version_History__c projectVesrion: [Select Id, GP_project__c from GP_Project_Version_History__c
                                                                where GP_project__c in: setOfProjectIdsToBeDeleted
                                                               ]) 
            {
                
                projectVesrion.GP_Project__c = mapOfParentProjectIdVsProjectId.get(projectVesrion.GP_Project__c);
                listOfProjectVersionsToBeUpdated.add(projectVesrion);
                //GP_Project__c project = new GP_Project__c(id = projectVesrion.GP_Project__c);
                //listOfProjectsToBeDeleted.add(project);
            }
            
            list < GP_Project_Classification__c > lstProjectClassification = [SELECT Id
                                                                              FROM GP_Project_Classification__c
                                                                              WHERE GP_Project__c in: setOfProjectIdsToBeDeleted
                                                                             ];
            
            
            system.debug('listOfProjectVersionsToBeUpdated::'+listOfProjectVersionsToBeUpdated);
            if (!listOfProjectVersionsToBeUpdated.isEmpty()) {
                //uow.registerDirty(listOfProjectVersionsToBeUpdated);
                update listOfProjectVersionsToBeUpdated;
            }
            
            if (!listOfProjectsToBeDeleted.isEmpty()) {
                //uow.registerDeleted(listOfProjectsToBeDeleted);
                delete listOfProjectsToBeDeleted;
            }
            
            if (lstProjectClassification != null && lstProjectClassification.size() > 0) {
                delete(lstProjectClassification);
            }
        }
    }
    
    // DS050919-Instead of quering all versions pick only the one with External EP no = PID's External EP no.
    // APC Change
    public void UpdateOracleSyncStatusVersion(List < GP_Project__c > projects, Map < Id, GP_Project__c > triggerOldMap) {
        
        set<String> setOfExternalEPNumbers = new set<String>();
        map<String, GP_Project__c> mapEPProjectNumberToProject = new map<string,GP_Project__c>();
        for (GP_Project__c project: projects) {
            
            if (triggerOldMap.get(project.Id).GP_Oracle_Status__c != project.GP_Oracle_Status__c &&
                (project.GP_Approval_Status__c == 'Approved' || project.GP_Approval_Status__c == 'Closed') && project.GP_Oracle_Status__c == 'S') {
                    setOfExternalEPNumbers.add(project.GP_External_EP_Project_Number__c);
                    mapEPProjectNumberToProject.put(project.GP_External_EP_Project_Number__c, project);
                }
        }
        System.debug('==setOfExternalEPNumbers=='+setOfExternalEPNumbers);
        if(setOfExternalEPNumbers != null && setOfExternalEPNumbers.size() > 0) {
            
            List < GP_Project_Version_History__c > listOfProjectVersionsToBeUpdated = new List < GP_Project_Version_History__c > ();
           
            for (GP_Project_Version_History__c projectVersion: [Select Id, GP_Oracle_Status__c, GP_External_EP_Number__c from GP_Project_Version_History__c
                                                                where GP_External_EP_Number__c in: setOfExternalEPNumbers limit 1])             
            {
                //system.debug('---------------------'+projectVesrion.GP_Project_JSON__c);
                projectVersion.GP_Oracle_Status__c = mapEPProjectNumberToProject.get(projectVersion.GP_External_EP_Number__c).GP_Oracle_Status__c;
                listOfProjectVersionsToBeUpdated.add(projectVersion);
            }
            System.debug('==listOfProjectVersionsToBeUpdated=='+listOfProjectVersionsToBeUpdated);
            if(listOfProjectVersionsToBeUpdated != null && listOfProjectVersionsToBeUpdated.size()>0){
                update listOfProjectVersionsToBeUpdated;
            }
        }
    }
    
    public void generateComparisonFile(List < GP_Project__c > projects, Map < Id, GP_Project__c > triggerOldMap){
        
        Set < Id > setOfProjectIdToCreateFiles = new Set < Id > ();
        for(GP_Project__c project : projects) {
            if (triggerOldMap.get(project.Id).GP_Oracle_Status__c != project.GP_Oracle_Status__c &&
                project.GP_Approval_Status__c == 'Approved' && project.GP_Oracle_Status__c == 'S') {
                    setOfProjectIdToCreateFiles.add(project.Id);
                } 
        }
        
        if(setOfProjectIdToCreateFiles.size() > 0) {
            GPServiceProjectVersionHistory.attachComparisonProjectFiles(setOfProjectIdToCreateFiles);
        }            
    }
    // APC Change
    public void updateAccountWithPIDNumbers(List < GP_Project__c > projects, Map < Id, GP_Project__c > triggerOldMap) {
        
        Map<Id, List<GP_Project__c>> mapOfAccIdWithListOfPIds = new Map<Id, List<GP_Project__c>>();
        List<Account> listOfAccountsToUpdate = new List<Account>();
        
        for(GP_Project__c project : projects) {
            if (triggerOldMap.get(project.Id).GP_Oracle_Status__c != project.GP_Oracle_Status__c &&
                project.GP_Approval_Status__c == 'Approved' && project.GP_Oracle_Status__c == 'S' 
                && String.isNotBlank(project.GP_Clone_Source__c) 
                && (project.GP_Clone_Source__c.equalsIgnoreCase('First Account PID')
                    || project.GP_Clone_Source__c.equalsIgnoreCase('Second Account PID')))
            {
                if(!mapOfAccIdWithListOfPIds.containsKey(project.GP_Customer_Hierarchy_L4__c)) {
                    mapOfAccIdWithListOfPIds.put(project.GP_Customer_Hierarchy_L4__c, new List<GP_Project__c>());
                }
                mapOfAccIdWithListOfPIds.get(project.GP_Customer_Hierarchy_L4__c).add(project);
            }
        }
        
        if(mapOfAccIdWithListOfPIds.keySet().size() > 0) {
            for(Id accId : mapOfAccIdWithListOfPIds.keySet()) {
                Account accObj = new Account();
                for(GP_Project__c pid : mapOfAccIdWithListOfPIds.get(accId)) {
                    if(pid.GP_Clone_Source__c.equalsIgnoreCase('First Account PID')) {                        
                        accObj.GP_Account_BD_Spend__c = pid.GP_Oracle_PID__c;
                    } else {
                        accObj.GP_Account_PID__c = pid.GP_Oracle_PID__c;
                    }
                }
                accObj.id = accId;
                accObj.GP_Pinnacle_Process_Log__c = 'PID Created Successfully';
                listOfAccountsToUpdate.add(accObj);
            }
        }
        
        if(listOfAccountsToUpdate.size() > 0) {
            update listOfAccountsToUpdate;
        }
    }
    
    public void sendEmailNotificationOnOracleSync(List < GP_Project__c > records, Map<Id,SObject> oldSObjectMap) {
        
        List<GP_Project__c> listOfApprovedPIDs = new List<GP_Project__c>();
        
        for (GP_project__c EachProj: records) {
            GP_Project__c objOldProj = oldSObjectMap != null ? (GP_Project__c) oldSObjectMap.get(EachProj.id) : null;
            
            // Execute notification when PID is approved and synced with Oracle.
            // Not sending notification for BPM PIDs as they don't have resource allocations.
            if(EachProj.GP_Oracle_PID__c != 'NA'
               && EachProj.RecordTypeId != System.label.GP_BPM_Recordtype_Id
               && EachProj.LastModifiedById == System.label.GP_Pinnacle_Integration_Use
               && EachProj.GP_Approval_Status__c == 'Approved'
			   && EachProj.GP_Oracle_Status__c == 'S' 
               && EachProj.GP_Oracle_Status__c != objOldProj.GP_Oracle_Status__c) {
                   listOfApprovedPIDs.add(new GP_Project__c(id=EachProj.Id,GP_is_Send_Notification__c=true));
               }
        }
        
        if(!listOfApprovedPIDs.isEmpty() || test.isRunningTest()) {
            
            try{
                GPDomainProject.skipRecursiveCall = true;
                update listOfApprovedPIDs;
               /* 
                 //commented by: Anoop Verma
                // Date:27-Jun-2020
                // CR:CRQ000000076988 
                Integer cronObjectCount = 0;                   
                List<CronTrigger> listOfCronTriggers = [Select id, CronJobDetail.Name from CronTrigger 
                                                        where CronJobDetail.Name Like 'Pinnacle Oracle Sync-%' Order by CreatedDate DESC limit 1];
                
                if(!listOfCronTriggers.isEmpty() ) {
                    String cronName = listOfCronTriggers[0].CronJobDetail.Name;
                    cronObjectCount = Integer.valueOf(cronName.split('-')[1]);
                    cronObjectCount++;
                }
                
                String cronName = 'Pinnacle Oracle Sync-' + cronObjectCount;
                
                System.schedule(cronName, getCronExpression(), new GPScheduleSendNotificationOnPIDSync());
              */
            } catch(Exception ex) {
            	// Error log.
            }
        }
    }
    
    public String getCronExpression() {
        String cronExpression = '';
        //String sch = '0 1 * * * ?';
        
        DateTime now = DateTime.now();
        Integer hours = 0;
        Integer minutes = 0;
        
        if(now.minute() + Integer.valueOf(System.label.GP_Oracle_Sync_Time) > 59) {
            minutes = (now.minute() + Integer.valueOf(System.label.GP_Oracle_Sync_Time)) - 60;
            hours = now.hour() + 1;
        } else {
            minutes = (now.minute() + Integer.valueOf(System.label.GP_Oracle_Sync_Time));
            hours = now.hour();
        }
        
        if(hours > 23) {
            hours = hours - 24;
        }
        
        cronExpression = String.valueOf(now.second()) + ' ' + String.valueOf(minutes) + ' ' + String.valueOf(hours) + ' * * ?';
        
        return cronExpression;        
    }
}