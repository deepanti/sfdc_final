@isTest
public class ContactLifeCycleHelper_Test {
    
     public static Opportunity opp, opp1, opp2, opp3, opp4, opp5, opp6, opp7, opp8, opp9, opp10,
        opp11, opp12, opp13, opp14, opp15, opp16;
    public static Contact oContact;
    public static User u;

    public static testMethod void MyPickListTest(){  
        setupTestData();
        Map<ID, Opportunity> old_opp = new Map<Id,Opportunity>();
        old_opp.put(opp2.id,opp2);
        System.debug('value of old_opp'+old_opp);
        List<Opportunity> oppList = new List<Opportunity>();
        opp2.StageName = '6. Signed Deal';
       
        update opp2;
        
       // List<Opportunity> new_opp = new List<Opportunity>();
        oppList.add(opp2);
        System.debug('value of old_opp'+old_opp);
        
        ContactLifeCycleHelper.contactLifeCycleMethod(oppList,old_opp);
    }
    private static void setupTestData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        
		oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       // System.runAs(u){
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp=new opportunity(name='1234',StageName='1. Discover',Nature_of_Work_highest__c = 'Consulting', CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                Competitor__c='Accenture',contact1__c = oContact.ID, role__c = 'Other');
            
           opp2=new opportunity(name='1234',StageName='5. Confirmed',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(opp);
            oppList.add(opp2);
          insert oppList;
         //       }
    }
    
}