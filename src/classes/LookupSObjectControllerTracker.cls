/**
 * @group TestClass. 
 *
 * @description Test Class for LookupSObjectController.
 */
@isTest
public class LookupSObjectControllerTracker {
    static GP_Employee_Master__c employeeMaster;

    @testSetup
    public static void buildDependencyData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
    }

    @isTest
    public static void testLookupSObjectController() {
        fetchData();
        testLookup();
        testlookupByFields();
        testLookupOnClickSearch();
        testLookupWithoutFilter();
        testLookupByFieldsOnClickSearch();
        testlookupByFieldsWithoutFilter();
        testLookupOnClickSearchWithoutFilter();
        testLookupByFieldsOnClickSearchWithoutFilter();
        testLookupByFieldsOnClickSearchWithoutFieldList();
    }

    public static void fetchData() {
        employeeMaster = [Select Id from GP_Employee_Master__c limit 1];
    }

    public static void testLookup() {
        string filterCondition = 'GP_EMPLOYEE_TYPE__c=\'Employee\'';
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookup('Testemployee', 'GP_Employee_Master__c', filterCondition, true);
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupWithoutFilter() {
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookup('Testemployee', 'GP_Employee_Master__c', null, true);
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupOnClickSearch() {
        string filterCondition = 'GP_EMPLOYEE_TYPE__c=\'Employee\'';
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookup('Testemployee', 'GP_Employee_Master__c', filterCondition, false);
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupOnClickSearchWithoutFilter() {
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookup('Testemployee', 'GP_Employee_Master__c', null, false);
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupByFieldsOnClickSearch() {
        string filterCondition = 'GP_EMPLOYEE_TYPE__c=\'Employee\'';
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookupByFields('Testemployee', 'GP_Employee_Master__c', filterCondition, false, new List < String > { 'GP_EMPLOYEE_TYPE__c' });
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupByFieldsOnClickSearchWithoutFieldList() {
        string filterCondition = 'GP_EMPLOYEE_TYPE__c=\'Employee\'';
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookupByFields('Testemployee', 'GP_Employee_Master__c', filterCondition, false, null);
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testLookupByFieldsOnClickSearchWithoutFilter() {
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookupByFields('Testemployee', 'GP_Employee_Master__c', null, false, new List < String > { 'GP_EMPLOYEE_TYPE__c' });
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testlookupByFields() {
        string filterCondition = 'GP_EMPLOYEE_TYPE__c=\'Employee\'';
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookupByFields('Testemployee', 'GP_Employee_Master__c', filterCondition, true, new List < String > { 'GP_EMPLOYEE_TYPE__c' });
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }

    public static void testlookupByFieldsWithoutFilter() {
        LookupSObjectController.Result[] returnedResult = LookupSObjectController.lookupByFields('Testemployee', 'GP_Employee_Master__c', null, true, new List < String > { 'GP_EMPLOYEE_TYPE__c' });
        if (returnedResult.size() > 0)
            System.assertEquals(employeeMaster.id, returnedResult[0].SObjectId);
    }
}