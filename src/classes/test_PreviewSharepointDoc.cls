@istest (SeeAllData=true)
Public class test_PreviewSharepointDoc{

  static testMethod void method1()
    {  
    
     List <items_Sharepoint_Files__x > mockedRequests = new List <items_Sharepoint_Files__x >();
     
     items_Sharepoint_Files__x request  =new items_Sharepoint_Files__x (
     DisplayUrl='test'
     
     );
     
        mockedRequests.add(request ); 
    //public static items_Sharepoint_Files__x  findById(Id requestId) 
     items_Sharepoint_Files__x  req=new  items_Sharepoint_Files__x ();
        if(Test.isRunningTest()) {
           req=( (mockedRequests.size() > 0) ? mockedRequests[0] : null);
        }
        else
        {

        req = [
            SELECT  Id, DisplayUrl, Name__c
            FROM items_Sharepoint_Files__x 
            WHERE Id =: request.Id
        ];
}
       
    
     ID hardid='x00N0000000CderIAC';
     
    ApexPages.StandardController std = new ApexPages.StandardController(req );
        PreviewSharepointDoc controller = new PreviewSharepointDoc(std);
        controller.Name='Google.doc';
        controller.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/_layouts/15/WopiFrame.aspx?sourcedoc={e0c3db0d-95f9-4ef1-9769-424e97d853bb}&action=view';
        controller.s1='google.doc';
        controller.Previewdoc();
        //controller.showdoc();
        
        
        
    }
    
    static testMethod void method2()
    {  
    
     List <items_Sharepoint_Files__x > mockedRequests = new List <items_Sharepoint_Files__x >();
     
     items_Sharepoint_Files__x request  =new items_Sharepoint_Files__x (
     DisplayUrl='test'
     
     );
     
        mockedRequests.add(request ); 
    //public static items_Sharepoint_Files__x  findById(Id requestId) 
     items_Sharepoint_Files__x  req=new  items_Sharepoint_Files__x ();
        if(Test.isRunningTest()) {
           req=( (mockedRequests.size() > 0) ? mockedRequests[0] : null);
        }
        else
        {

        req = [
            SELECT  Id, DisplayUrl, Name__c
            FROM items_Sharepoint_Files__x 
            WHERE Id =: request.Id
        ];
}
       
    
     ID hardid='x00N0000000CderIAC';
     
    ApexPages.StandardController std = new ApexPages.StandardController(req );
        PreviewSharepointDoc controller = new PreviewSharepointDoc(std);
        controller.Name='Google.mp4';
        controller.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/_layouts/15/WopiFrame.aspx?sourcedoc={e0c3db0d-95f9-4ef1-9769-424e97d853bb}&action=view';
        controller.s1='google.mp4';
        controller.Previewdoc();
        //controller.showdoc();
        
        
        
    }
    
    
    static testMethod void method3()
    {  
    
     List <items_Sharepoint_Files__x > mockedRequests = new List <items_Sharepoint_Files__x >();
     
     items_Sharepoint_Files__x request  =new items_Sharepoint_Files__x (
     DisplayUrl='test'
     
     );
     
        mockedRequests.add(request ); 
    //public static items_Sharepoint_Files__x  findById(Id requestId) 
     items_Sharepoint_Files__x  req=new  items_Sharepoint_Files__x ();
        if(Test.isRunningTest()) {
           req=( (mockedRequests.size() > 0) ? mockedRequests[0] : null);
        }
        else
        {

        req = [
            SELECT  Id, DisplayUrl, Name__c
            FROM items_Sharepoint_Files__x 
            WHERE Id =: request.Id
        ];
}
       
    
     ID hardid='x00N0000000CderIAC';
     
    ApexPages.StandardController std = new ApexPages.StandardController(req );
        PreviewSharepointDoc controller = new PreviewSharepointDoc(std);
        controller.Name='Google.pdf';
        controller.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/_layouts/15/WopiFrame.aspx?sourcedoc={e0c3db0d-95f9-4ef1-9769-424e97d853bb}&action=view';
        controller.s1='google.pdf';
        controller.Previewdoc();
        //controller.showdoc();
        
        
        ApexPages.StandardController std1 = new ApexPages.StandardController(req );
        PreviewSharepointDoc controller1 = new PreviewSharepointDoc(std1);
          controller1.s1='mp4';
        controller1.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/_layouts/15/WopiFrame.aspx?sourcedoc={e0c3db0d-95f9-4ef1-9769-424e97d853bb}&action=view';
      
        controller1.Previewdoc();
        
    }

}