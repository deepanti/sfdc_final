//================================================================================================================
//  Description: Test Class for GPCmpServiceDeleteAndCloneProject
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          10-May-2018                 Ved Prakash              Initial Version
//================================================================================================================
@isTest public class GPCmpServiceOppProjectEditTracker {
	public static GP_Project__c prjObj;
    public static GP_Opportunity_Project__c oppproobj;
    
     static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Business_Type__c = 'BPM';
        dealObj.GP_Business_Name__c = 'BPM';
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        insert prjObj;
         
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
         
        GP_Timesheet_Entry__c timeshtentryObj1 = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        insert timeshtentryObj1;
         
        GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        insert projectTaskObj1;
        
        GP_Project_Version_History__c projectVersionHistory1 = GPCommonTracker.getProjectVersionHistory(prjObj.Id);
        insert projectVersionHistory1;
        
        GP_Project_Classification__c projectClassification1 = GPCommonTracker.getProjectClassification(prjObj.Id);
        projectClassification1.GP_CLASS_CODE__c ='Product Family';
        insert projectClassification1;
        
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        oppproobj.GP_Vertical__c = 'Sample Vertical';
        insert oppproobj;
         
        prjObj.GP_Opportunity_Project__c = oppproobj.Id;
        prjObj.GP_Oracle_Status__c = 'S';
        UPDATE prjObj;
         
        projectClassification1.GP_Opp_Project__c = oppproobj.Id;
        update projectClassification1;
         
        GP_Product_Master__c productMaster = new GP_Product_Master__c();
        productMaster.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster.GP_Product_Family__c = 'Sample Family';
        productMaster.GP_Service_Line__c = 'Sample Service';
        
        insert productMaster;
        
        GP_Product_Master__c productMaster2 = new GP_Product_Master__c();
        productMaster2.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster2.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster2.GP_Product_Family__c = 'Sample Family';
        
        insert productMaster2;
    }
    
    @isTest public static void testgetOppProjectData() {
        setupCommonData();
        GPAuraResponse response = GPCmpServiceOppProjectEdit.getOppProjectData(oppproobj.Id);
        //System.assertEquals(true, response.isSuccess);
    }
    
    @isTest public static void testsaveOppProject() {
        setupCommonData();
        string strJSONResult =  JSON.serialize(oppproobj);
        GPAuraResponse response = GPCmpServiceOppProjectEdit.saveOppProject(strJSONResult);
        //System.assertEquals(true, response.isSuccess);
    }
}