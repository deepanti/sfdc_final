public with sharing class GPControllerProjectHeaderInfo {
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String ERROR_LABEL = 'Something is wrong! Please contact with your system admin';
    private final String APPROVAL_STATUS = 'strApprovalStatus';
    private final String FIELD_LIST = 'strFieldList';
    private Id projectId;
    
    
    public GPControllerProjectHeaderInfo(Id projectId) {
        this.projectId = projectId;
    }
    
    public GPAuraResponse getProjectDetails() {
        
        GP_Project__c project;
        JSONGenerator gen = JSON.createGenerator(true);
        GPApprovalLogger logger;

        try {
            project = new GPSelectorProject().getProjectFieldDetail(projectId);
        } catch(Exception ex) {
            return new GPAuraResponse(false, ERROR_LABEL, null); 
        } 
            
        gen.writeStartObject();
        if(project != null)
            gen.writeObjectField('project', project);
        if(project != null && project.GP_Approval_Status__c != null)
            gen.writeStringField(APPROVAL_STATUS, project.GP_Approval_Status__c);
        if(project != null && project.GP_Approval_Error__c != null)
            gen.writeStringField(FIELD_LIST, project.GP_Approval_Error__c);
        gen.writeEndObject();
                    
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

}