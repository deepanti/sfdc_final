/*
    @Author: Vikas Rajput
    @Description: It will bind user and opportunity related data  and Target related data
*/
public class VIC_UserOpportunityDataWrapCtlr{
    @AuraEnabled public Boolean isUserChecked{get;set;}
    @AuraEnabled public Boolean isUserSelected = false;
    @AuraEnabled public User objUser;
    @AuraEnabled public String strVICRole;
    @AuraEnabled public String strTCVBooked;
    @AuraEnabled public String strTCVValidated;
    @AuraEnabled public String strTotalVICComputed;
    @AuraEnabled public String strVICAlreadyPaid;
    @AuraEnabled public String strTotalVICPayable;
    @AuraEnabled public String strApprovedIncentive;
    @AuraEnabled public String strRenewal;
    @AuraEnabled public String strTargetLetterStatus;
    @AuraEnabled public String strComment;
    @AuraEnabled public List<VIC_OpportunityDataWrapCtlr> lstOppDataWrap;
    @AuraEnabled public String strOppCount = '0';
    @AuraEnabled public String strDiscretionaryPayment = '0';
    //Scorecard Variable
    @AuraEnabled public String strTCVAcceleratorIO = '0';
    @AuraEnabled public String strTCVAcceleratorTS = '0';
    @AuraEnabled public String strTotalScorecard = '0';
    @AuraEnabled public String strTotalAmtScorecardInLocal = '0';
    @AuraEnabled public String adjustmentDiscretionary = '0';
    @AuraEnabled public String strTotalVIC = '0';
    @AuraEnabled public String strTotalDiscrAndVIC = '0'; //Sum of Total Vic amount and Total Discreationary
    @AuraEnabled public String strTotalVICInLocal = '0';
    @AuraEnabled public String strTotalUpfrontInLocal = '0';
    @AuraEnabled public String strISOCode = '';
    //Set of all Incentive Id for all scorecard
    @AuraEnabled public List<Id> lstIncentiveId = new List<Id>();
    @AuraEnabled public String adjustmentTCVAcceleratorIO = '';
    @AuraEnabled public String adjustmentTCVAcceleratorTS = '';
    
    //@Constructor
    public VIC_UserOpportunityDataWrapCtlr(Boolean isUserChecked, User objUser, String strVICRole, String strTCVBooked, 
                String strTCVValidated, String strTotalVICComputed, String strVICAlreadyPaid, String strTotalVICPayable,
                    String strApprovedIncentive,String strRenewal,String strTargetLetterStatus,
                    String strComment, List<VIC_OpportunityDataWrapCtlr> lstOppDataWrap){
        this.isUserChecked = isUserChecked;
        this.objUser = objUser;
        this.strVICRole = strVICRole;
        this.strTCVBooked = strTCVBooked;
        this.strTCVValidated = strTCVValidated;
        this.strTotalVICComputed = strTotalVICComputed;
        this.strVICAlreadyPaid = strVICAlreadyPaid;
        this.strTotalVICPayable = strTotalVICPayable;
        this.strApprovedIncentive = strApprovedIncentive;
        this.strComment = strComment;
        this.lstOppDataWrap = lstOppDataWrap;
        this.strRenewal = strRenewal;
        this.strTargetLetterStatus = strTargetLetterStatus;
    }
}