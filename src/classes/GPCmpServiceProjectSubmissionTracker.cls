/*=======================================================================================
Description: Test Data Class containing all test data for every sObject

=======================================================================================
Version#     Date                           Author                    Description
=======================================================================================
1.0          11-May-2018                   Ved Prakash            Initial Version 
=======================================================================================  
*/
@isTest public class GPCmpServiceProjectSubmissionTracker {
    public static GP_Project__c prjObj;  
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    
    public static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; 
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        
        insert prjObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        String strFieldSetName = '';
        for(Schema.FieldSet f : SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }
    }
	
    @isTest public static void testgetProjectStatus()
    {
       	setupCommonData();
        
        GP_Role__c objrole1 = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole1.GP_Type_of_Role__c = 'SDO';
        objrole1.GP_Role_Category__c = 'PID Creation';
        objrole1.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole1.GP_HSL_Master__c = null;
        objrole1.GP_Active__c = true;
        insert objrole1;
        
        GP_User_Role__c objuserrole1 = GPCommonTracker.getUserRole(objrole1, objuser);
        objuserrole1.GP_Active__c = true;
        objuserrole1.GP_User__c= userInfo.getUserId();
        insert objuserrole1;
        
        prjObj.GP_Pricing_Status__c = 'Approved';
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Oracle_Template_Id__c = '12345';
        update prjObj;
        
        GPAuraResponse obj = GPCmpServiceProjectSubmission.getProjectStatus(prjObj.id);   
        //System.assertEquals(true, obj.isSuccess);
        
        obj = GPCmpServiceProjectSubmission.getProjectStatus(null);   
		System.assertEquals(false, obj.isSuccess);
        
        GPControllerProjectSubmission objProjectSubmission = new GPControllerProjectSubmission(prjObj.id);
		objProjectSubmission.skipApprovalValidation = true;
        obj = objProjectSubmission.getProjectStatus();
        
    }
    
    @isTest public static void testgetProjectStatusForClosure()
    {
        setupCommonData();
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        objDoc.GP_Type__c = 'Project Closure Signoff';
        insert objDoc;
        GPAuraResponse obj = GPCmpServiceProjectSubmission.getProjectStatusForClosure(prjObj.id);   
        System.assertEquals(true, obj.isSuccess);
        obj = GPCmpServiceProjectSubmission.getProjectStatusForClosure(null);  
        System.assertEquals(false, obj.isSuccess);
    }
    
    @isTest public static void testsubmitforApproval()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        GP_Role__c objrole1 = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole1.GP_Type_of_Role__c = 'Global';
        objrole1.GP_Role_Category__c = 'Product Approver';
        objrole1.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole1.GP_HSL_Master__c = null;
        insert objrole1;
        
        GP_User_Role__c objuserrole1 = GPCommonTracker.getUserRole(objrole1, objuser);
        insert objuserrole1;
        
        prjObj.GP_Product_Approval_Required__c = true;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_PID_Approver_User__c = objuser.Id;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        prjObj.GP_Additional_Approval_Required__c = true;
        prjObj.GP_BPR_Approval_Required__c = false;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');   
        
        
        prjObj.GP_Auto_Approval__c = true;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');  
        
        objSdo.GP_IsChina__c = true;
        objSdo.GP_Band_3_Approver__c = userInfo.getUserId();
        update objSdo;
        prjObj.GP_Band_3_Approver_Status__c = 'Draft';
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');  
        //System.assertEquals(true, obj.isSuccess);
        
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = true;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
       
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_PID_Approver_Finance__c = true;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = true;
        prjObj.GP_FP_A_Approver__c = objuser.Id;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = true;
        prjObj.GP_New_SDO_s_MF_User__c = objuser.Id;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        prjObj.GP_Old_MF_Approver_Required__c = true;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_SDO_s_MF_User__c = objuser.Id;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        prjObj.GP_Auto_Approval__c = true;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        update prjObj;
        obj = GPCmpServiceProjectSubmission.submitforApproval(prjObj.id,'Cost Charging','Test Comment', 'Approved');
        
        GPControllerProjectSubmission objProjectSubmission =new GPControllerProjectSubmission(prjObj.id);
        GPAuraResponse obj1 = objProjectSubmission.recallApprovalProcess(prjObj.id);
    }
    
    @isTest public static void testsubmitforClosure()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectSubmission.submitforClosure(prjObj.id,'Cost Charging','Test Comment', 'test');   
        
    }
    
    @isTest public static void testrecallRequest()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpServiceProjectSubmission.recallRequest(prjObj.id);   
    }
    
}