/**
* @author Anmol.kumar
* @date 22/10/2017
*
* @group ChangeSummary
* @group-content ../../ApexDocContent/ChangeSummary.html
*
* @description Apex Service for Change Summary module,
* has aura enabled method to fetch project Version data.
*/
public class GPCmpServiceChangeSummary {
	/**
    * @description Aura enabled method to return project versions.
    * @param projectId SFDC 18/15 digit Project Id
    * 
    * @return GPAuraResponse json of Project version data
    * 
    * @example
    * GPCmpServiceChangeSummary.getProjectVersions(<SFDC 18/15 digit Project Id>);
    */
	@AuraEnabled
    public static GPAuraResponse getProjectVersions(Id projectId) {
        GPControllerChangeSummary changeSummaryController = new GPControllerChangeSummary(projectId);
        return changeSummaryController.getProjectVersions();
    }
}