public class VIC_ScorecardBatch implements Database.Batchable<sObject>,Database.Stateful{
    
    VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    Map <Id, User_VIC_Role__c> mapUserIdToUserVicRoles = vic_CommonUtil.getUserVICRoleinMap();
    Integer currentYear = integer.valueof(processInfo.VIC_Annual_Process_Year__c);
    Date startFYDate = Date.newInstance(currentYear, 1, 1);
    Date endFYDate = Date.newInstance(currentYear, 12, 31);
    String strPlanYear = String.valueOf(currentYear);
    List<Target_Achievement__c> lstTargetIncentive = new List<Target_Achievement__c >();
    String strOP = 'OP';
    String strPM = 'PM';
    String strMBO = 'MBO';
    //String strTCVAccelerator = 'TCV Accelerator';
    String strIOTCV = 'IO_TCV';
    String strTSTCV = 'TS_TCV';
    String strNewIncentiveType = 'New';
    String strAdjuctIncentiveType = 'Adjustment';
    String strDisbursed = 'VIC Team - Pending';
    Map<String, vic_Nature_Of_Work_To_Deal_Type__mdt> mapPMVNatureOfWorkToProfitalbeType = new VIC_NatureOfWorkToDealTypeCtlr().getAllMetaData();
    
    Map<Id,Decimal> mapPMVUserIdToIOTCVAmount = new Map<Id,Decimal>();
    Map<Id,Decimal> mapPMVUserIdToTSTCVAmount = new Map<Id,Decimal>();
    Map<Id,Decimal> mapPMVUserIdToTotalTCVAmount = new Map<Id,Decimal>();
    
    Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapPlanIdToLstIOTypeVICMatrixItem = new Map<Id,List<VIC_Calculation_Matrix_Item__c>>(); //Mapping IO Type VIC Matrix by VIC Matrix Item List
    Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapPlanIdToLstTSTypeVICMatrixItem = new Map<Id,List<VIC_Calculation_Matrix_Item__c>>(); //Mapping TX Type VIC Matrix by VIC Matrix Item List
    
    Map<String,List<VIC_Calculation_Matrix_Item__c>> mapPlanMPCStrToLstIOTypeVICMatrixItem = new Map<String,List<VIC_Calculation_Matrix_Item__c>>(); //(Plan+MasterPlanCMP) => (List of VIC Matrix Item) for IO type
    Map<String,List<VIC_Calculation_Matrix_Item__c>> mapPlanMPCStrToLstTSTypeVICMatrixItem = new Map<String,List<VIC_Calculation_Matrix_Item__c>>(); //(Plan+MasterPlanCMP) => (List of VIC Matrix Item) for TS type
    
    Map<Id,Target_Component__c> mapTargetIdToObjOPTypeTargetCMP = new Map<Id,Target_Component__c>(); // Mapping OP Type Target Component
    Map<Id,Target_Component__c> mapTargetIdToObjPMTypeTargetCMP = new Map<Id,Target_Component__c>(); // Mapping PM Type Target Component
    
    Map<String, vic_Incentive_Constant__mdt> mapPMVStrLabelToIncentiveConst = new VIC_IncentiveConstantCtlr().getAllMetaData();
    
    Map <String, String> mapDomicileTiISOCode = vic_CommonUtil.getDomicileToISOCodeMap();
    Map<String,vic_Currency_Exchange_Rate__c> mapMonthYearISOToCER = vic_CommonUtil.fetchCurrencyExchangeRates();
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        initUserToTCVAmountByOLI(mapPMVUserIdToTotalTCVAmount, mapPMVUserIdToIOTCVAmount, mapPMVUserIdToTSTCVAmount, mapPMVNatureOfWorkToProfitalbeType, mapPMVStrLabelToIncentiveConst);
        Set<String> setTCVPlans = new Set<String>{'EnterpriseGRM','SL_CP_Enterprise','SEM','Capital_Market_GRM','IT_GRM','SL_CP_CMIT'};
        return Database.getQueryLocator([
            SELECT Id,Target__r.Plan__r.vic_Plan_Code__c,vic_Achievement_Percent__c,Target__r.Target_Bonus__c,Weightage__c,Target__r.VIC_Joining_Date__c,
            Target_In_Currency__c,Target__r.vic_Total_TCV__c,Overall_Achievement__c,Master_Plan_Component__c,
            Master_Plan_Component__r.vic_Component_Code__c,Target__r.Domicile__c,Target__r.User__r.Domicile__c,
            Master_Plan_Component__r.Name,Target__r.Plan__c,Target__r.vic_TCV_IO__c,Target__r.User__c,
            VIC_Total_Achievement_Scoreboard__c,VIC_Total_Achievement_Count__c,Target__r.Name,Target__r.Id,
            (SELECT Id,Bonus_Amount__c,vic_Status__c,vic_Incentive_Type__c,vic_Deal_Type__c,vic_Incentive_Details__c,
            vic_PPP__c,Achievement_Date__c,Target_Component__c from Target_Achievements__r order by CreatedDate ASC) 
            From Target_Component__c where Target__r.Plan__r.vic_Plan_Code__c IN :setTCVPlans AND 
            Target__r.Is_Active__c =:true AND Target_Status__c =:'Active' AND 
            Target__r.User__c != null AND Target__r.User__r.Domicile__c != null AND 
            Master_Plan_Component__c != null AND Master_Plan_Component__r.vic_Component_Code__c != null AND 
            Target__r.Plan__r.Year__c =:strPlanYear AND   
            Target__r.Start_date__c >= :startFYDate AND 
            Target__r.Start_date__c <= :endFYDate
        ]);
    }
    
    public void execute(Database.BatchableContext bc, List<Target_Component__c> lstTargetCMP){
        Integer intMonth = System.now().month();
        Integer intYear = endFYDate.year();
        
        Set<Id> setPlanId = new Set<Id>();
        Set<Id> setTargetId = new Set<Id>();
        for(Target_Component__c objTC : lstTargetCMP){
            setPlanId.add(objTC.Target__r.Plan__c);
            setTargetId.add(objTC.Target__c);
        }
        //Map<ID,Target_Achievement__c> mapIDTCtoObjIncentive = fetchLastIncentive(lstTargetCMP);//No use of this line now
        System.debug('************1************lstTargetCMP'+lstTargetCMP);
        System.debug('************1************lstTargetCMP__SIZE'+lstTargetCMP.size());
        System.debug('************1************setPlanId'+setPlanId);
        System.debug('************1************setTargetId'+setTargetId);
        
        initAllPlanTargetDataToCalculation(setPlanId,setTargetId); //Initializing all Plan and Target data map here by calling this function
        
        //Calculating TCV Accelerator
        for(Target_Component__c objTargetCMPITR : lstTargetCMP){
                
            String strKeyPlanMPC = ID.valueOf(objTargetCMPITR.Target__r.Plan__c)+''+ID.valueOf(objTargetCMPITR.Master_Plan_Component__c);
            String strIncentiveType = (objTargetCMPITR.VIC_Total_Achievement_Count__c == 0)?strNewIncentiveType:strAdjuctIncentiveType;
            Decimal dcmOPPercent = 0.00;
            Decimal dcmPMPercent = 0.00;
            if(mapTargetIdToObjOPTypeTargetCMP.containsKey(objTargetCMPITR.Target__c) ){
                dcmOPPercent = mapTargetIdToObjOPTypeTargetCMP.get(objTargetCMPITR.Target__c).vic_Achievement_Percent__c;
            }
            if(mapTargetIdToObjPMTypeTargetCMP.containsKey(objTargetCMPITR.Target__c)){
                dcmPMPercent = mapTargetIdToObjPMTypeTargetCMP.get(objTargetCMPITR.Target__c).vic_Achievement_Percent__c;
            }
            
            Decimal dcmTargetBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c;
            if(objTargetCMPITR.Target__r.VIC_Joining_Date__c != null){
                    objTargetCMPITR.Target__r.Target_Bonus__c= vic_CommonUtil.fetchProRatedAmount(objTargetCMPITR.Target__r.Target_Bonus__c,objTargetCMPITR.Target__r.VIC_Joining_Date__c);
            }
            Decimal dcmCalBonousAmount = null;
            if(mapDomicileTiISOCode.containsKey(objTargetCMPITR.Target__r.User__r.Domicile__c) && mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c) != Label.VIC_User_Currency){
                String keyOfMothYearISO = ''+ intMonth + intYear + mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c);
                if(mapMonthYearISOToCER.containsKey(keyOfMothYearISO) && mapMonthYearISOToCER.get(keyOfMothYearISO).vic_Exchange_Rate__c != null && objTargetCMPITR.Target__r.Target_Bonus__c != null){
                    dcmCalBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c/mapMonthYearISOToCER.get(keyOfMothYearISO).vic_Exchange_Rate__c;
                    System.debug('============Block_A_CalculatedBonousAmount==========='+dcmCalBonousAmount);
                }
            }else if(mapDomicileTiISOCode.containsKey(objTargetCMPITR.Target__r.User__r.Domicile__c) && mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c) == Label.VIC_User_Currency){
                dcmCalBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c;
                System.debug('============Block_B_Calculated==========='+dcmCalBonousAmount);
            }
            objTargetCMPITR.Target__r.Target_Bonus__c = dcmCalBonousAmount;
            //Calculating OP 
            if(mapPlanMPCStrToLstIOTypeVICMatrixItem.containsKey(strKeyPlanMPC) && objTargetCMPITR.Master_Plan_Component__r.vic_Component_Code__c == strOP ){
                VIC_CalculateOPIncentivectrl objOPCalc = new VIC_CalculateOPIncentivectrl();
                objOPCalc.pmAcheivmentPercent = dcmPMPercent;
                objOPCalc.mapValueToObjIncentiveConst = mapPMVStrLabelToIncentiveConst;
                objOPCalc.mapUserIdToUserVicRoles = mapUserIdToUserVicRoles;
                Decimal totalIncentiveAMT = objOPCalc.calculate(objTargetCMPITR,mapPlanMPCStrToLstIOTypeVICMatrixItem.get(strKeyPlanMPC));
                initIncentiveLst(objTargetCMPITR,totalIncentiveAMT,strIncentiveType,strDisbursed,lstTargetIncentive);
            }
            // Calculating IO_TCV
            if(mapPlanMPCStrToLstIOTypeVICMatrixItem.containsKey(strKeyPlanMPC) && objTargetCMPITR.Master_Plan_Component__r.vic_Component_Code__c == strIOTCV && mapPlanMPCStrToLstIOTypeVICMatrixItem.get(strKeyPlanMPC).size()!=0){
                VIC_IOTCVCalculationCtlr objIOTCV = new VIC_IOTCVCalculationCtlr(); 
                objIOTCV.opAcheivmentPercent = dcmOPPercent;
                objIOTCV.pmAcheivmentPercent = dcmPMPercent;
                objIOTCV.mapValueToObjIncentiveConst = mapPMVStrLabelToIncentiveConst;
                objIOTCV.mapUserIdToUserVicRoles = mapUserIdToUserVicRoles;
                Decimal totalIncentiveAMT = objIOTCV.calculate(objTargetCMPITR,mapPlanMPCStrToLstIOTypeVICMatrixItem.get(strKeyPlanMPC));
                initIncentiveLst(objTargetCMPITR,totalIncentiveAMT,strIncentiveType,strDisbursed,lstTargetIncentive);
            }
            // Calculating TS_TCV
            if(mapPlanMPCStrToLstIOTypeVICMatrixItem.containsKey(strKeyPlanMPC) && objTargetCMPITR.Master_Plan_Component__r.vic_Component_Code__c == strTSTCV ){
                VIC_TSTCVCalculationCtlr objTSTCV = new VIC_TSTCVCalculationCtlr(); 
                objTSTCV.opAcheivmentPercent = dcmOPPercent;
                objTSTCV.pmAcheivmentPercent = dcmPMPercent;
                objTSTCV.mapValueToObjIncentiveConst = mapPMVStrLabelToIncentiveConst;
                objTSTCV.mapUserIdToUserVicRoles = mapUserIdToUserVicRoles;
                Decimal totalIncentiveAMT = objTSTCV.calculate(objTargetCMPITR,mapPlanMPCStrToLstIOTypeVICMatrixItem.get(strKeyPlanMPC));
                initIncentiveLst(objTargetCMPITR,totalIncentiveAMT,strIncentiveType,strDisbursed,lstTargetIncentive);
            }
            // Calculating MBO
            if(objTargetCMPITR.Master_Plan_Component__r.vic_Component_Code__c == strMBO){
                VIC_CalculateMBOIncentivectrl objMBO = new VIC_CalculateMBOIncentivectrl();
                objMBO.opAcheivmentPercent = dcmOPPercent;
                Decimal totalIncentiveAMT = objMBO.calculate(objTargetCMPITR,null);
                initIncentiveLst(objTargetCMPITR,totalIncentiveAMT,strIncentiveType,strDisbursed,lstTargetIncentive);
            }
            
            //calculate PM%
            if(mapPlanMPCStrToLstIOTypeVICMatrixItem.containsKey(strKeyPlanMPC) && objTargetCMPITR.Master_Plan_Component__r.vic_Component_Code__c == strPM ){
                VIC_CalculatePMIncentivectrl objPM = new VIC_CalculatePMIncentivectrl();
                objPM.opAcheivmentPercent = dcmOPPercent;
                objPM.mapValueToObjIncentiveConst = mapPMVStrLabelToIncentiveConst;
                Decimal totalIncentiveAMT = objPM.calculate(objTargetCMPITR,mapPlanMPCStrToLstIOTypeVICMatrixItem.get(strKeyPlanMPC));   
                initIncentiveLst(objTargetCMPITR,totalIncentiveAMT,strIncentiveType,strDisbursed,lstTargetIncentive);
            }
            objTargetCMPITR.Target__r.Target_Bonus__c = dcmTargetBonousAmount;
        }
    }
    
    public void finish(Database.BatchableContext bc){
        if(!lstTargetIncentive.isEmpty()){
            insert lstTargetIncentive;
        }
        if(!Test.isRunningTest()){
              VIC_CurrencyConversionBatch  CuCvBatch = new VIC_CurrencyConversionBatch(false);
              Database.executeBatch(CuCvBatch, 2); 
        }
        
    }
    /*
        @Description: Function will be used for optimizing the logic, It will initiate the List of Incentive record
        @Author: Vikas Rajput
    */
    public void initIncentiveLst(Target_Component__c objTargetCMPARG,Decimal totalIncentiveAMT,String strIncentiveType,String strDisbursed,List<Target_Achievement__c> lstTargetIncentive){
        if(totalIncentiveAMT != null && objTargetCMPARG.VIC_Total_Achievement_Scoreboard__c != null){
            Decimal calcPayoutAmount = (totalIncentiveAMT - objTargetCMPARG.VIC_Total_Achievement_Scoreboard__c);
            if(calcPayoutAmount != 0){
                Target_Achievement__c objTargetIncentive = vic_CommonUtil.initTargetIncentivRecOBJ(objTargetCMPARG, calcPayoutAmount, strIncentiveType, strDisbursed);
                lstTargetIncentive.add(objTargetIncentive);
            }
        }
    }
    
    public void initAllPlanTargetDataToCalculation(Set<Id> setPlanId,Set<Id> setTargetId){
        initPlanCmpData(setPlanId);
        initTargetCmpData(setTargetId);
    }
    
    //Possible no user function in future
    public Map<ID,Target_Achievement__c> fetchLastIncentive(List<Target_Component__c> lstTargetCMP){
        Map<ID,Target_Achievement__c> mapIDTCtoObjIncentive = new Map<ID,Target_Achievement__c>();
        for(Target_Component__c everyTC : lstTargetCMP){
            for(Target_Achievement__c everyTA : everyTC.Target_Achievements__r){
                mapIDTCtoObjIncentive.put(everyTC.ID,everyTA);
            }
        }
        return mapIDTCtoObjIncentive;
    }
    /*
        @Description: It is initiating map data all plan data such as 
                        (Plan+MasterPlanCMP) => (IO VIC Matrix Item List), 
                        (Plan+MasterPlanCMP) => (TS VIC Matrix Item List)
                        
        @Author: Vikas Rajput
    */
    public void initPlanCmpData(Set<Id> setPlanId){
        Map<String,Id> mapPlanMPCStrToIOVICMatrixId = new Map<String,Id>();  //Mapping IO Type VIC Matrix with key = objPlan.Id + objPlanCmp.Master_Plan_Component__c;
        Map<String,Id> mapPlanMPCStrToTSTVICMatrixId = new Map<String,Id>(); //Mapping TS Type VIC Matrix with key = objPlan.Id + objPlanCmp.Master_Plan_Component__c;
        Set<Id> setMatrixId = new Set<Id>();
        for(Plan__c objPlan : [SELECT id,Is_Active__c,vic_Plan_Code__c,Year__c,Conga_Email_Template__c,Conga_Template__c,Conga_Template_For_Bonus__c,
                     (SELECT Id,Master_Plan_Component__c,Master_Plan_Component__r.Name,Plan__c,VIC_IO_Calculation_Matrix_1__c,VIC_TS_Calculation_Matrix_2__c,
                      Master_Plan_Component__r.vic_Component_Code__c,
                      Weightage_Applicable__c from Plan_Components__r) from Plan__c where Id IN:setPlanId AND 
                      Year__c =:strPlanYear]){
            //Here filter data for IO and TS type VIC Matrix
            for(Plan_Component__c objPlanCmp : objPlan.Plan_Components__r){
                    String strKeyPlanMPC = ID.valueOf(objPlan.Id)+''+ID.valueOf(objPlanCmp.Master_Plan_Component__c);
                    mapPlanMPCStrToIOVICMatrixId.put(strKeyPlanMPC,objPlanCmp.VIC_IO_Calculation_Matrix_1__c);
                    mapPlanMPCStrToTSTVICMatrixId.put(strKeyPlanMPC,objPlanCmp.VIC_TS_Calculation_Matrix_2__c);
            }
        }
        setMatrixId.addAll(mapPlanMPCStrToIOVICMatrixId.values()); //Adding all VIC Matrix Id To Set
        setMatrixId.addAll(mapPlanMPCStrToTSTVICMatrixId.values()); //Adding all VIC Matrix Id To Set
        //Mapping all VIC Matrix by List VIC Matrix Item
        Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapVICMatrixIDToLstMatrixItem = fetchVICMatrixItemByLstMatrixId(setMatrixId);
        //Setting map of (Plan+MasterPlanCMP) => (List of VIC Matrix Item) for IO type
        for(String everyKey : mapPlanMPCStrToIOVICMatrixId.keySet()){
            ID idVICMatrix = mapPlanMPCStrToIOVICMatrixId.get(everyKey);
            if(mapVICMatrixIDToLstMatrixItem.containsKey(idVICMatrix)){
                mapPlanMPCStrToLstIOTypeVICMatrixItem.put(everyKey,mapVICMatrixIDToLstMatrixItem.get(idVICMatrix));
            }
        }
        //Setting map of (Plan+MasterPlanCMP) => (List of VIC Matrix Item) for TS type
        for(String everyKey : mapPlanMPCStrToTSTVICMatrixId.keySet()){
            ID idVICMatrix = mapPlanMPCStrToTSTVICMatrixId.get(everyKey);
            if(mapVICMatrixIDToLstMatrixItem.containsKey(idVICMatrix)){
                mapPlanMPCStrToLstTSTypeVICMatrixItem.put(everyKey,mapVICMatrixIDToLstMatrixItem.get(idVICMatrix));
            }
        }
    }
    /*
        @Description: It is initiating map data all target data such as 
                        (Target => OP Target Component), 
                        (Target => PM Target Component)         
        @Author: Vikas Rajput
    */
    public void initTargetCmpData(Set<Id> setTargetId){
        for(Target__c objTrg : [SELECT Id,Amount_Paid__c,Domicile__c,Is_Active__c,Master_VIC_Role__c,Plan__c,vic_TCV_IO__c,
            vic_Total_TCV__c,vic_TCV_TS__c,VIC_Achievement__c,
                (SELECT Id,vic_Achievement__c,vic_Achievement_Percent__c,Overall_Achievement__c,Overall_Achievement_Pr__c,
                    Target_In_Currency__c,Target__c,Target_Percent__c,Total_Payouts_Per__c,Weightage__c,Total_Payouts_Amount__c,
                        Master_Plan_Component__c,Master_Plan_Component__r.vic_Component_Code__c,Target__r.Name,Target__r.Id  
                        FROM Target_Components__r) from Target__c where ID IN :setTargetId AND Is_Active__c =:true]){
            for(Target_Component__c itrTrgtCMP : objTrg.Target_Components__r){
                if(!mapTargetIdToObjOPTypeTargetCMP.containsKey(itrTrgtCMP.Target__c) && itrTrgtCMP.Master_Plan_Component__c != null &&
                    itrTrgtCMP.Master_Plan_Component__r.vic_Component_Code__c == strOP ){
                    mapTargetIdToObjOPTypeTargetCMP.put(itrTrgtCMP.Target__c,itrTrgtCMP);
                }
                if(!mapTargetIdToObjPMTypeTargetCMP.containsKey(itrTrgtCMP.Target__c) && itrTrgtCMP.Master_Plan_Component__c != null &&
                    itrTrgtCMP.Master_Plan_Component__r.vic_Component_Code__c == strPM){
                    mapTargetIdToObjPMTypeTargetCMP.put(itrTrgtCMP.Target__c,itrTrgtCMP);
                }
            }
        }
    }
    /*
        @Description: It is initiating map data all target data such as (VIC Matrix Id => List Of VIC Matrix Id Item).      
        @Author: Vikas Rajput
    */
    public Map<Id,List<VIC_Calculation_Matrix_Item__c>> fetchVICMatrixItemByLstMatrixId(Set<Id> setMatrixId){
        Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapVICMatrixIdToLstOfVICMatrixItem = new Map<Id,List<VIC_Calculation_Matrix_Item__c>>();
        for(VIC_Calculation_Matrix__c objCalcMatrix : [Select Id,Name,(Select id,Additional_Payout__c,For_Each__c,
                    vic_Component_Type__c,vic_Start_Value__c,vic_End_Value__c,vic_Payout__c,Is_Additional_Payout_Applicable__c 
                    from VIC_Calculation_Matrix_Items__r order by vic_Start_Value__c ASC) from VIC_Calculation_Matrix__c where Id IN:setMatrixId]){
            mapVICMatrixIdToLstOfVICMatrixItem.put(objCalcMatrix.Id,objCalcMatrix.VIC_Calculation_Matrix_Items__r);
        }
        return mapVICMatrixIdToLstOfVICMatrixItem;
    }
    /*
        @Description: It is initiating map data all target data such as (User Id => Total TCV Amount).      
        @Author: Vikas Rajput
    */
    public void initUserToTCVAmountByOLI(Map<Id,Decimal> mapUserIdToTotalTCVAmt, Map<Id,Decimal> mapUserIdToIOTCVAmount, Map<Id,Decimal> mapUserIdToTSTCVAmount, Map<String,vic_Nature_Of_Work_To_Deal_Type__mdt> mapNatureOfWorkToProfitalbeType, Map<String, vic_Incentive_Constant__mdt> mapStrLabelToIncentiveConst){ 
        //In future use objIncentiveConstant instead of VIC_IncentiveConstantCtlr
        String cpqDealStatus = mapStrLabelToIncentiveConst.get('CPQ_Deal_Status').vic_Value__c ;
        String opportunityStagename = mapStrLabelToIncentiveConst.get('Opportunity_Eligible_Stage_For_VIC').vic_Value__c ;
        for(OpportunityLineItem objEachOLI : [SELECT Id, Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, Opportunity.Actual_Close_Date__c, Opportunity.CloseDate,
                VIC_NPV__c, vic_Contract_Term__c, Contract_Term__c,  Opportunity.Id,
                TCV__c, Opportunity.OwnerId, Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c, Opportunity.StageName,
                vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c, vic_VIC_User_3_Split__c, 
                vic_VIC_User_4_Split__c,Opportunity.AccountId,Opportunity.Sales_country__c,Opportunity.vic_Total_Renewal_TCV__c 
                FROM OpportunityLineItem WHERE Opportunity.StageName =: opportunityStagename 
                AND Opportunity.Actual_Close_Date__c >= :startFYDate AND Opportunity.Actual_Close_Date__c <= :endFYDate   
                AND vic_CPQ_Deal_Status__c =: CPQDealStatus AND vic_Final_Data_Received_From_CPQ__c = true 
                AND(
                    (vic_Sales_Rep_Approval_Status__c = 'Approved' AND vic_Product_BD_Rep_Approval_Status__c = 'Approved')
                    OR (vic_Sales_Rep_Approval_Status__c = 'Approved' AND vic_Product_BD_Rep_Approval_Status__c = 'Not Required')
                )]){
            String strProfitableType = mapNatureOfWorkToProfitalbeType.get(objEachOLI.Nature_of_Work__c).vic_Profitable_Booking_Type__c;
            fetchUserIdToTCVAmtMap(objEachOLI.Opportunity.OwnerId, objEachOLI.TCV__c, objEachOLI.vic_Primary_Sales_Rep_Split__c, strProfitableType, mapUserIdToTotalTCVAmt, mapUserIdToIOTCVAmount, mapUserIdToTSTCVAmount);
            fetchUserIdToTCVAmtMap(objEachOLI.Product_BD_Rep__c, objEachOLI.TCV__c, objEachOLI.vic_Product_BD_Rep_Split__c, strProfitableType, mapUserIdToTotalTCVAmt, mapUserIdToIOTCVAmount, mapUserIdToTSTCVAmount);
            fetchUserIdToTCVAmtMap(objEachOLI.vic_VIC_User_3__c, objEachOLI.TCV__c, objEachOLI.vic_VIC_User_3_Split__c, strProfitableType, mapUserIdToTotalTCVAmt, mapUserIdToIOTCVAmount, mapUserIdToTSTCVAmount);
            fetchUserIdToTCVAmtMap(objEachOLI.vic_VIC_User_4__c, objEachOLI.TCV__c, objEachOLI.vic_VIC_User_4_Split__c, strProfitableType, mapUserIdToTotalTCVAmt, mapUserIdToIOTCVAmount, mapUserIdToTSTCVAmount);
        }
    }
    /*
        @Description: Function will callcultate total tcv amount against user.     
        @Param: userID, mapUserIdToTotalTCVAmt, totalTCV, splitPercent
        @Author: Vikas Rajput
    */
    public void fetchUserIdToTCVAmtMap(Id userID, Decimal totalTCV, Decimal splitPercent,String strOLIType, 
                        Map<Id,Decimal> mapUserIdToTotalTCVAmtARG, Map<Id,Decimal> mapUserIdToIOTCVAmountARG, 
                                        Map<Id,Decimal> mapUserIdToTSTCVAmountARG){
                                        
        if(!String.isEmpty(userID) && !mapUserIdToTotalTCVAmtARG.containsKey(userID)){
            mapUserIdToTotalTCVAmtARG.put(userID,0);
            mapUserIdToIOTCVAmountARG.put(userID,0);
            mapUserIdToTSTCVAmountARG.put(userID,0);
        }
        if(totalTCV != null && totalTCV != 0 && splitPercent != null && splitPercent != 0){
            Decimal totalTCVAmount = (mapUserIdToTotalTCVAmtARG.get(userID) + ((totalTCV*splitPercent)/100));
            mapUserIdToTotalTCVAmtARG.put(userID, totalTCVAmount);
            if(strOLIType.equalsIgnoreCase('IO')){
                Decimal totalIOTCVAmount = (mapUserIdToIOTCVAmountARG.get(userID)+ ((totalTCV*splitPercent)/100));
                mapUserIdToIOTCVAmountARG.put(userID, totalIOTCVAmount);
            }else if(strOLIType.equalsIgnoreCase('TS')){
                Decimal totalTSTCVAmount = (mapUserIdToTSTCVAmountARG.get(userID)+ ((totalTCV*splitPercent)/100));
                mapUserIdToTSTCVAmountARG.put(userID, totalTSTCVAmount);
            }
        }
    }   
}