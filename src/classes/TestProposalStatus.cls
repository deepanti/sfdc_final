@isTest
private class TestProposalStatus 
{
  static testMethod void testTrigger()
   {
      
      RMG_Employee_Master__c Empmaster=new RMG_Employee_Master__c(); 
      Insert Empmaster;
      RMG_Employee_Master__c Empmaster_mngr= new RMG_Employee_Master__c();
      insert Empmaster_mngr;
      Proposal__c Proposal=new Proposal__c(Status__c='Proposed',RMG_Employee_Code__c=Empmaster.id,Manager__c=Empmaster_mngr.id,Client_Interview_Y_N__c='N');
      Proposal__c Proposal_1=new Proposal__c(Status__c='Blocked',RMG_Employee_Code__c=Empmaster.id,Manager__c=Empmaster_mngr.id,Client_Interview_Y_N__c='N');
      //Proposal__c Proposal_2=new Proposal__c(Status__c='Selected-New',RMG_Employee_Code__c=Empmaster.id,Manager__c=Empmaster_mngr.id,Client_Interview_Y_N__c='N');
     
     
      Insert Proposal;
      Insert Proposal_1;
     // Insert Proposal_2;
      update Proposal;
    //  update Proposal_2;
      
     
      
      Proposal__c newproposal=[select Name, Status__c from Proposal__c where id = :Proposal.id];
      Proposal__c newproposal_1=[select Name, Status__c from Proposal__c where id = :Proposal_1.id];
      // Proposal__c newproposal_2=[select Name, Status__c from Proposal__c where id = :Proposal_2.id];
     
      try 
      { 
      delete Proposal; 
      delete Proposal_1;
       delete Proposal_1;
      }        
      catch (Exception e) 
      {
      System.debug('Proposal has been deleted');
      }

      if(newproposal.Status__c != Null)
      {
            if(newproposal.Status__c != 'Closed')
              {
                      String futureProposalStatus = newproposal.Status__c;      
                      System.assertEquals(futureProposalStatus,newproposal.Status__c);
              }
      else if( newproposal.Status__c == 'Selected-New' || newproposal.Status__c == 'Blocked' || newproposal.Status__c == 'Selected-Extended') 
              { 
                      Proposal.addError('Employee already selected or blocked. No new Proposal can be created till that block is removed.'); 
              }
      else if(newproposal.Status__c == 'Proposed')
              {
                      System.assertEquals('AUP',newproposal.Status__c);
              }
       else if(newproposal.Status__c == 'Closed')
           {
               
                       String futureProposalStatus = 'Unassigned';      
                      System.assertEquals(futureProposalStatus,'Unassigned');
           
           }
     
     
     
      }
      
      
      
      
   }

}