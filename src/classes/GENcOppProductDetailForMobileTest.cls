@isTest
private class GENcOppProductDetailForMobileTest
{
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,0,12000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunityProduct);
        GENcOpportunityProductDetailForMobile  oGENcOpportunityProductDetailForMobile  = new GENcOpportunityProductDetailForMobile(std);
        oGENcOpportunityProductDetailForMobile.revenueRollingYearIndex = 1;
        GENcOpportunityProductDetailForMobile.ProductScheduleWrapper oProductScheduleWrapper = new GENcOpportunityProductDetailForMobile.ProductScheduleWrapper(oProductSchedule,1);
        GENcOpportunityProductDetailForMobile.RevenueRollingYearWrapper oRevenueRollingYearWrapper = new GENcOpportunityProductDetailForMobile.RevenueRollingYearWrapper(100);
        oGENcOpportunityProductDetailForMobile.doCancel();
        oGENcOpportunityProductDetailForMobile.getRevenueRollingYear();
        oGENcOpportunityProductDetailForMobile.getProductShedule();
        oGENcOpportunityProductDetailForMobile.Edit_Mobile();
        //oGENcOpportunityProductDetailForMobile.createSchedule();
        //oGENcOpportunityProductDetailForMobile.getScheduleTypes();
       // oGENcOpportunityProductDetailForMobile.getInstallmentPeriods();
       // oGENcOpportunityProductDetailForMobile.getHasRevenueRollingYearTwoRendered();
       // oGENcOpportunityProductDetailForMobile.gethasScheculeListRollingYearTwoRendered();
       // oGENcOpportunityProductDetailForMobile.sethasScheculeListRollingYearTwoRendered(true);
       // oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
        oGENcOpportunityProductDetailForMobile.gethasScheculeListRollingYearOneRendered();
        oGENcOpportunityProductDetailForMobile.sethasScheculeListRollingYearOneRendered(true);
        oGENcOpportunityProductDetailForMobile.sethasScheculeListRollingYearOneRendered(true);
        oGENcOpportunityProductDetailForMobile.getHasScheculeListRendered();
        oGENcOpportunityProductDetailForMobile.setHasRevenueRollingYearTwoRendered(true);
        oGENcOpportunityProductDetailForMobile.setHasScheculeListRendered(true);
        oGENcOpportunityProductDetailForMobile.gethasScheculeListRollingYearTwoRendered();
        oGENcOpportunityProductDetailForMobile.sethasScheculeListRollingYearTwoRendered(true);
        oGENcOpportunityProductDetailForMobile.getHasRevenueRollingYearTwoRendered();
        oGENcOpportunityProductDetailForMobile.getHasScheculeSectionRendered();
        oGENcOpportunityProductDetailForMobile.setHasScheculeSectionRendered(true);
        oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper.setHasAutoPopulateDisabled(true); 
        
    }
    
}