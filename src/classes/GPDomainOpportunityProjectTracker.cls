//================================================================================================================  
//  Description: Test Class for GPDomainOpportunityProject
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPDomainOpportunityProjectTracker {
    
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Opportunity_Project__c oppproobj;
    public static Account accobj2;
    public static GP_Work_Location__c objSdo2;
    public static String jsonresp;
    
    
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_opportunity_project__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        objSdo2 = GPCommonTracker.getWorkLocation();
        objSdo2.GP_Status__c = 'Active and Visible';
        objSdo2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo2;
        
        accobj2 = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj2;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 10;
        oppproobj.GP_Oracle_PID__c = 'test';
        oppproobj.GP_Customer_L4_Name__c = accobj.id;
        oppproobj.GP_SDO_Code__c = objSdo.id;
    }
    @isTest
    public static void testGPDomainOpportunityProject() 
    {
        buildDependencyData();  
        oppproobj.GP_EP_Project_Number__c = '1234';
        insert oppproobj;
        oppproobj.GP_Probability__c = 90;
        oppproobj.GP_Customer_L4_Name__c = accobj2.id;
        oppproobj.GP_SDO_Code__c = objSdo2.id;
        update oppproobj;
        
        GP_Deal__c ObjDeal  = new GP_Deal__c();
        ObjDeal.GP_Opportunity_Project__c = oppproobj.id;
        ObjDeal.GP_TCV__c = 1234;
        insert ObjDeal;  
    }
    
     @isTest
    public static void testGPDomainOpportunityProject2() 
    {
        buildDependencyData();  
        //oppproobj.GP_EP_Project_Number__c = '1234';
        insert oppproobj;
        oppproobj.GP_Probability__c = 90;
        oppproobj.GP_Customer_L4_Name__c = accobj2.id;
        update oppproobj;
        
        GP_Deal__c ObjDeal  = new GP_Deal__c();
        ObjDeal.GP_Opportunity_Project__c = oppproobj.id;
        ObjDeal.GP_TCV__c = 1234;
        insert ObjDeal;
        
        // Avalara Change
        oppproobj.GP_IsUpdated__c = true;
        update oppproobj;
		
        
        GPServiceOppProjectClassification oppProjectClassificationService = new GPServiceOppProjectClassification(new List<Id> {oppproobj.Id});
        oppProjectClassificationService.createProjectClassification();
        
        GPServiceOppProjectAddress oppProjectAddressService = new GPServiceOppProjectAddress(new List<Id> {oppproobj.Id});
        oppProjectAddressService.createOppProjectAddress();
    
        GPServiceOppProjectLeadership oppProjectLeadership = new GPServiceOppProjectLeadership(new List<Id> {oppproobj.Id});
        oppProjectLeadership.createOppProjectLeadership();
        
        oppProjectLeadership = new GPServiceOppProjectLeadership();
    }
}