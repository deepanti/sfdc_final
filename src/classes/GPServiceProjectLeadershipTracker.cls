@isTest
public class GPServiceProjectLeadershipTracker {
    
     
    private static GP_Employee_Master__c employeeMaster;    
    private static List<GP_Project_Leadership__c> listOfProjectLeadership;
    private static GP_Leadership_Master__c leadershipMaster;
    public Static GP_Project__c project;
  public static GP_Project_Template__c projectTemplate;
    public static GP_Employee_Master__c supervisorempObj;
    public static User objuser;
    
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        project.GP_End_Date__c = System.today().addDays(10);
        insert project;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        insert objPrjBillingMilestone;
        
    }
    
    public static void buildDependencyData2() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today();
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-30);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        project.GP_End_Date__c = System.today().addDays(10);
        insert project;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        insert objPrjBillingMilestone;
        
    }
    
    @isTest
    public static void testupdateEmployeeEndDateInOldVersion() {
        buildDependencyData();
        fetchData();
        createProjectLeadership();
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        
        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    } 
    
     @isTest
    public static void testupdateEmployeeEndDateInOldVersion3() {
        buildDependencyData2();
        fetchData();
        createProjectLeadership();
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        listOfProjectLeadership[1].GP_Start_Date__c = system.today() +1;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                                new Map<String, String>());

        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                                new List<GP_Project_Leadership__c>(),
                                                                new Map<String, String>());

        


        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                                new Map<String, String>());
       listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
       listOfProjectLeadership[0].GP_Leadership_Role__c = 'PROJECT MANAGER';
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    } 
    
    static void fetchData() {
        project = [select id, 
                   Name, RecordType.Name,
                   GP_HSL__r.Parent_HSL__c,
                   GP_Start_Date__c,GP_End_Date__c,
                   GP_Last_Temporary_Record_Id__c,
                   GP_Deal__r.GP_Deal_Category__c, 
                   GP_Project_Template__c, 
                   GP_Primary_SDO__c, GP_Project_Stages_for_Approver__c, GP_Approval_Status__c, GP_GPM_Start_Date__c 
                  from GP_Project__c limit 1];
        
        projectTemplate = [Select Id, GP_Final_JSON_1__c, GP_Final_JSON_2__c, GP_Final_JSON_3__c FROM GP_Project_Template__c LIMIT 1];
        // 07-05-19: Added Name in query.
        employeeMaster = [SELECT ID, Name, GP_HIRE_Date__c, GP_ACTUAL_TERMINATION_Date__c From GP_Employee_Master__c LIMIT 1];
    }
    
    private static void createProjectLeadership() {
        Date todayDate = System.today();
        listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        List<GP_Leadership_Master__c> listOfLeadershipMaster = new List<GP_Leadership_Master__c>();
        
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        listOfLeadershipMaster.add(leadershipMaster);
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Account', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Account';
        
        listOfLeadershipMaster.add(leadershipMaster);

        insert listOfLeadershipMaster;
        
        for(GP_Leadership_Master__c leadershipMaster: listOfLeadershipMaster) {
           
            GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            
            projectClonedLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            projectLeadership.Is_Cloned_from_Parent__c = false;
            projectLeadership.GP_Employee_ID__r = employeeMaster;
            projectLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectClonedAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectClonedAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectClonedAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectClonedAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectClonedAccountLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedAccountLeadership.GP_Active__c = false;
            
            
            GP_Project_Leadership__c projectAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectAccountLeadership.Is_Cloned_from_Parent__c = false;
            projectAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectAccountLeadership.GP_Active__c = false;
            
            listOfProjectLeadership.add(projectLeadership);
            listOfProjectLeadership.add(projectClonedLeadership);
            
            listOfProjectLeadership.add(projectAccountLeadership);
            listOfProjectLeadership.add(projectClonedAccountLeadership);
        }
        
        insert listOfProjectLeadership;
        /*
        List<GP_Project_Leadership__c> listOfClonedProjectLeadership = new List<GP_Project_Leadership__c>();
        
        for(GP_Project_Leadership__c projectLeadership : listOfProjectLeadership) {
            
            projectLeadership.GP_Parent_Project_Leadership__c = projectLeadership.Id;
            projectLeadership.Id = null;
            projectLeadership.Is_Cloned_from_Parent__c = false;
            
            listOfClonedProjectLeadership.add(projectLeadership);
        }
        
        insert listOfClonedProjectLeadership;*/
    }
    
    @isTest
    public static void testupdateEmployeeEndDateInOldVersion2() {
        buildDependencyData();
        fetchData(); 
        
        Date todayDate = System.today();
        listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        List<GP_Leadership_Master__c> listOfLeadershipMaster = new List<GP_Leadership_Master__c>();
        
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        listOfLeadershipMaster.add(leadershipMaster);
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Account', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Account';
        
        listOfLeadershipMaster.add(leadershipMaster);

        insert listOfLeadershipMaster;
        
        GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
        projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
        projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
        projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
        projectClonedLeadership.Is_Cloned_from_Parent__c = true;
        projectClonedLeadership.GP_Employee_ID__r = employeeMaster;
        projectClonedLeadership.GP_Active__c = false;
        listOfProjectLeadership.add(projectClonedLeadership);
        insert projectClonedLeadership;
        
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        
        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
         
         
         listOfProjectLeadership[0].GP_Start_Date__c = system.today();
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        listOfProjectLeadership[0].GP_Start_Date__c = System.today();
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        

        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
       // listOfProjectLeadership[0].GP_Leadership_Role__c = 'PROJECT MANAGER';
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.validateEmployeeOnLeadership(listOfProjectLeadership,listOfLeadershipMaster);
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    }
}