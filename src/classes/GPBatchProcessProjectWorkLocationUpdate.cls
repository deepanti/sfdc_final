/**
 * @group Mass Update/Upload. 
 *
 * @description Batch class for Mass Update Project WorkLocation.
 *              Inherits GPBatchMassUpdateAndUploadHelper Class.
 */
global class GPBatchProcessProjectWorkLocationUpdate extends GPBatchMassUpdateAndUploadHelper implements Database.Batchable < sObject > {

    private final String PROJECT_WORKLOCATION_NOT_FOUND_ERROR_LABEL = 'Project WorkLocation Not Found In Database which you are trying to update !';
    private final String INAVLID_WORKLOCATION_ID_ERROR_LABEL = 'Invalid Id Of Work Location & SDO !';

    private List < GP_Project_Work_Location_SDO__c > lstOfProjectWorkLocation = new List < GP_Project_Work_Location_SDO__c > ();

    /**
     * @description Constructor with jobId whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * 
     */
    global GPBatchProcessProjectWorkLocationUpdate(String jobRecordId) {
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
        jobType = 'Update Project WorkLocation';
    }

    /**
     * @description Constructor with jobId and setOf Project Oracle PID's whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * @param setOfProjectOracleIds : Set of Project Oracle PID's.
     * 
     */
    global GPBatchProcessProjectWorkLocationUpdate(Set < String > setOfProjectOracleIds, String jobRecordId) {
        setOfProjectOracleIdsForBatch = setOfProjectOracleIds;
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
        jobType = 'Update Project WorkLocation';
    }

    /**
     * @description Batch start method.
     * @param BC : BatchableContext.
     * 
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {

        // Update Job Record with 'Pending' Status.
        updateJobRecord('In Progress', 0, ''); // Class member of GPBatchMassUpdateAndUploadHelper.
        //Query Project Records to manage one by one project processing of temporary Data.
        String query = 'SELECT id, GP_Current_Working_User__c, GP_Oracle_PID__c from GP_Project__c where GP_Oracle_PID__c in :setOfProjectOracleIdsForBatch ORDER BY LastModifiedDate Desc';

        return Database.getQueryLocator(Query);
    }

    /**
     * @description Batch execute method.
     * @param BC : BatchableContext.
     * @param lstProjectsfromBatch : list of project records that will be processed.
     * 
     */
    global void execute(Database.BatchableContext BC, List < Sobject > listOfProjectRecords) {

        getJobRecord(jobId); // Class member of GPBatchMassUpdateAndUploadHelper.

        Savepoint sp = Database.setSavepoint();

        String oraclePID = String.ValueOf(listOfProjectRecords[0].get('GP_Oracle_PID__c'));
        String query = 'SELECT id, GP_Is_Failed__c,GP_PWL_BCP_Flag__c, GP_PWL_BCP_Requirement__c, GP_PWL_Primary__c, GP_PWL_Work_Location__c,';
        query += 'GP_Project__c from GP_Temporary_Data__c where GP_Job_Id__c = \'' + jobId + '\' and GP_Project__c = :oraclePID ';

        listOfTemporaryRecords = Database.query(query);

        try {
            // Set Validation configuration for project WL to validate updated project WL records.
            // projectApprovalValidatorConfig.validateCustomFieldsOnWorkLocation = true; // Class member of GPBatchMassUpdateAndUploadHelper.
            // projectApprovalValidatorConfig.validateWorkLocation = true; // Class member of GPBatchMassUpdateAndUploadHelper.
            
            // Set Validation configuration for project Worklocation to validate updated project Worklocation records.
            validationConfig.validateWorkLocationAndSDO = true; // Class member of GPBatchMassUpdateAndUploadHelper.

            // Create Project Oracle PID set.
            setOracleIdSet(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
            // Query Project Records using project Oracle PID set.
            setListOfProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper. 

            if (listOfProjectRecords.size() > 0) {
                // Filter the PID's which can't be processed by Mass Update/Upload depening upon their Appproval Status and OMS status.
                setStatusWiseProjectSet(); // Class member of GPBatchMassUpdateAndUploadHelper. 
                // Create map of Project Oracle Ids Vs its status of processing for mass upload and update.
                setMapToCheckValidRecordsForProjectStatus(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Filter the list of temporary data depending upon the project's approval status.
                setLstOfValidatedTemporaryData(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Create map of all masters using their oracle PID's.
                setMasterDataMap(); // Class member of GPBatchMassUpdateAndUploadHelper.

                if (lstOfValidatedTemporaryData.size() > 0) {
                    // Query all project and associated child records.
                    queryProjectAndChilRecordsUsingOraclePID(setOfOracleIds); // Class member of GPBaseProjectUtil.
                    // Set Map Of project and its Associated child Records.
                    setMapOfProjectChildRecords(); // Class member of GPBaseProjectUtil.

                    //setProjectQuery();//get project records.//Not to be called
                    //setlistOfProjectRecordsWithChildQueried();//Not to be called

                    // Append temporary RecordId to project Record.
                    putTemporaryIdToProjectRecord(); // Class member of GPBatchMassUpdateAndUploadHelper.

                    //setCommitedProjectAndChildRecords();//Not to be called

                    // Clone Project and Validate depending upon the Validation Configuration.
                    cloneAndValidateProject(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    // Filter the Project Records which have passed the validations.
                    setListOfValidatedProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.

                    if (listOfValidatedProjectRecords.size() > 0) {
                        //insertListOfValidatedProjectRecords();
                        // Create map Of Parent Project Id Vs Child Project Id.
                        setMapOfOldProjectIdVsNewProjectId(); // Class member of GPBatchMassUpdateAndUploadHelper.
                        // Set Project Worklocation Records from Map Of Project worklocation(member of parent class).
                        setLstOfProjectWorkLocation(); // Class method.
                        // Merge Temporary record content with queried project worklocation records.
                        mergeTemporaryFieldValuesWithProjectWorkLocationRecord(); // Class method.
                        // Check for worklocation records for the associated project in pinnacle system that needs to be updated from Mass Update.
                        checkForNonExistantRecordsToBeUpated(); // Class method.
                        // Clone Project Child Records and Validate depending upon the Validation Configuration.
                        cloneAndValidateProjectChildRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    }
                }
                // Update Job Record and the temporary records with validation result.
                String status = mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0 ? 'Failed' : 'Completed';
                updateJobRecord(status, mapOfInvalidEntries.values().size(), ''); // Class member of GPBatchMassUpdateAndUploadHelper.
                Database.update(lstOfNewProjectCreated, false); // Class member of GPBatchMassUpdateAndUploadHelper.
                updateJobLineItemRecord(); // Class member of GPBatchMassUpdateAndUploadHelper.
            } else {
                Database.rollback(sp);
                // Update Job Record with Exception Message.
                updateJobRecord('Failed', listOfTemporaryRecords.size(), PROJECT_NOT_FOUND_FOR_ORACLE_PID_ERROR_LABEL); // Class member of GPBatchMassUpdateAndUploadHelper.
            }
        } catch (GPServiceProjectClone.GPServiceProjectCloneException cloneException) {
            Database.rollback(sp);

            if (cloneException.mapOfValidations == null && cloneException.relatedOrcaleId != null) // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.relatedOrcaleId, cloneException.exceptionString); // Class member of GPBatchMassUpdateAndUploadHelper.
            else if(cloneException.relatedOrcaleId == null) // Update Validation Result of record in Invalid Entries map.
				systemThrownExceptionValue = cloneException.exceptionString;
            else // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.mapOfValidations); // Class member of GPBatchMassUpdateAndUploadHelper.

            // Update Job Record and the temporary records with validation result.
            String status = ((mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0) || systemThrownExceptionValue != null || systemThrownExceptionValue != '' ) ? 'Failed' : 'Completed';
            updateJobRecord(status, mapOfInvalidEntries.values().size(), systemThrownExceptionValue); // Class member of GPBatchMassUpdateAndUploadHelper.
        } catch (Exception E) {
            Database.rollback(sp);
            // Update Job Record with Exception Message.
            updateJobRecord('Failed', listOfTemporaryRecords.size(), E.getMessage()); // Class member of GPBatchMassUpdateAndUploadHelper.
        }
    }

    /**
     * @description Batch finish method.
     * @param BC : BatchableContext.
     *  
     */
    global void finish(Database.BatchableContext BC) {
        //updateJobRecord('Completed', 0, '');
        System.debug('Success');
    }

    /**
     * @description Merge Temporary record content with queried project worklocation records.
     * 
     */
    private void mergeTemporaryFieldValuesWithProjectWorkLocationRecord() {
        system.debug('mapOfTemporaryDataWithUniqueKey'+mapOfTemporaryDataWithUniqueKey);
        //map clear leadership and then reassign with updated content.
        mapOfProjectWorkLocation = new Map < Id, List < GP_Project_Work_Location_SDO__c >> ();
        GP_Temporary_Data__c objUpdatedProjectWorkLocationValues;
        String uniqueKey;

        for (GP_Project_Work_Location_SDO__c objProjectWorkLocation: lstOfProjectWorkLocation) {
            uniqueKey = objProjectWorkLocation.GP_Project__r.GP_Oracle_PID__c + objProjectWorkLocation.GP_Work_Location__r.GP_Oracle_Id__c;

            if (mapOfTemporaryDataWithUniqueKey.containsKey(uniqueKey)) {
                objUpdatedProjectWorkLocationValues = mapOfTemporaryDataWithUniqueKey.get(uniqueKey);
                updateValueWithTemporaryRecord(objProjectWorkLocation, objUpdatedProjectWorkLocationValues);
            }

            if (mapOfProjectWorkLocation.containsKey(objProjectWorkLocation.GP_Project__c))
                mapOfProjectWorkLocation.get(objProjectWorkLocation.GP_Project__c).add(objProjectWorkLocation);
            else
                mapOfProjectWorkLocation.put(objProjectWorkLocation.GP_Project__c, new List < GP_Project_Work_Location_SDO__c > { objProjectWorkLocation });

            mapOfProjectOracleIdVsUniqueKeyOfChild.get(objProjectWorkLocation.GP_Project__r.GP_Oracle_PID__c).remove(uniqueKey);
        }
    }

    /**
     * @description Update field values with temporary record and Master data Map.
     * @param objProjectWorkLocation : project worklocation Record.
     * @param objUpdatedProjectWorkLocationValues : temporary data record with new values of project worklocation record.
     * 
     */
    private void updateValueWithTemporaryRecord(GP_Project_Work_Location_SDO__c objProjectWorkLocation, GP_Temporary_Data__c objUpdatedProjectWorkLocationValues) {

        objProjectWorkLocation.GP_Last_Temporary_Record_Id__c = objUpdatedProjectWorkLocationValues.Id;

        if (!mapOfWorkLocationMasterOracleIdVsSFDCId.containskey(objUpdatedProjectWorkLocationValues.GP_PWL_Work_Location__c)) {
            throw new GPServiceProjectClone.GPServiceProjectCloneException(INAVLID_WORKLOCATION_ID_ERROR_LABEL, objUpdatedProjectWorkLocationValues.GP_PWL_Work_Location__c);
        }
        //objProjectWorkLocation.GP_Project__c = mapOfWorkLocationMasterOracleIdVsSFDCId.get(objUpdatedProjectWorkLocationValues.GP_Project__c);
        objProjectWorkLocation.GP_Work_Location__c = mapOfWorkLocationMasterOracleIdVsSFDCId.get(objUpdatedProjectWorkLocationValues.GP_PWL_Work_Location__c);

        //objProjectWorkLocation.GP_BCP__c = objUpdatedProjectWorkLocationValues.GP_PWL_BCP_Requirement__c;
        setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectWorkLocationValues, 'GP_PWL_BCP_Requirement__c', objProjectWorkLocation, 'GP_BCP__c');

        //objProjectWorkLocation.GP_BCP_FLAG__c = objUpdatedProjectWorkLocationValues.GP_PWL_BCP_Flag__c;
        setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectWorkLocationValues, 'GP_PWL_BCP_Flag__c', objProjectWorkLocation, 'GP_BCP_FLAG__c');

        //objProjectWorkLocation.GP_Primary__c = objUpdatedProjectWorkLocationValues.GP_PWL_Primary__c;
        setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectWorkLocationValues, 'GP_PWL_Primary__c', objProjectWorkLocation, 'GP_Primary__c');

    }

    /**
     * @description Set Project worklocation Records from Map Of Project Worklocation(member of parent class).
     * 
     */
    private void setLstOfProjectWorkLocation() {
        lstOfProjectWorkLocation = (List < GP_Project_Work_Location_SDO__c > ) convertlistOfListToAList(mapOfProjectWorkLocation.Values());
    }

    /**
     * @description Check for worklocation records for the associated project in pinnacle system that needs to be updated from Mass Update.
     * 
     */
    private void checkForNonExistantRecordsToBeUpated() {
        Boolean isError = false;
        Id tempId;

        for (String projectOracleId: mapOfProjectOracleIdVsUniqueKeyOfChild.KeySet()) {
            if (mapOfProjectOracleIdVsUniqueKeyOfChild.get(projectOracleId).size() > 0) {
                for (String uniqueKey: mapOfProjectOracleIdVsUniqueKeyOfChild.get(projectOracleId)) {
                    if (mapOfTemporaryDataWithUniqueKey.containsKey(uniqueKey))
                        break;

                    tempId = mapOfTemporaryDataWithUniqueKey.get(uniqueKey).Id;
                    isError = true;

                    if (mapOfInvalidEntries.containsKey(tempId)) {
                        mapOfInvalidEntries.get(tempId).GP_Validation_Result__c = PROJECT_WORKLOCATION_NOT_FOUND_ERROR_LABEL;
                        mapOfInvalidEntries.get(tempId).GP_Is_Failed__c = true;
                    } else {
                        GP_Temporary_Data__c objTemporaryData = mapOfTemporaryData.get(tempId);
                        objTemporaryData.GP_Validation_Result__c = PROJECT_WORKLOCATION_NOT_FOUND_ERROR_LABEL;
                        objTemporaryData.GP_Is_Failed__c = true;
                        mapOfInvalidEntries.put(tempId, objTemporaryData);
                    }
                }
            }
        }

        if (isError)
            throw new GPServiceProjectClone.GPServiceProjectCloneException(PROJECT_WORKLOCATION_NOT_FOUND_ERROR_LABEL, tempId);
    }
}