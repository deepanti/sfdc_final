public with sharing class GPLWCServiceCustomComponent {
    @AuraEnabled
    public static GPAuraResponse fetchLookupRecords(String objectName, String searchKey, String searchfield, String filterString, 
    String displayTextSeparator, String displayTextFields, String type) {
        GPLWCControllerCustomComponent glccc = new GPLWCControllerCustomComponent(objectName, searchKey, searchfield, 
        filterString, displayTextSeparator, displayTextFields, type);
        return glccc.fetchLookupRecords();
    }

    @AuraEnabled
    public static GPAuraResponse fetchPicklistValues(String sObjectName, String fieldapi, String selectedValue) {
        GPLWCControllerCustomComponent glccc = new GPLWCControllerCustomComponent(sObjectName, fieldapi, selectedValue);
        return glccc.fetchPicklistValues();
    }
    
    @AuraEnabled
    public static GPAuraResponse fetchFieldSetPerObject(String fieldSetName, String objectName) {
        GPLWCControllerCustomComponent glccc = new GPLWCControllerCustomComponent();
        return glccc.fetchFieldSetPerObject(fieldSetName, objectName);
    }
}