@isTest(SeeAllData = false)
/************************************************************************************************************
* @author   Persistent 
* @description  - This class will be the utility class for test data creation
*************************************************************************************************************/
public class TestDataFactoryUtility{
    
/************************************************************************************************************
@description  - Creation of Test Data for Accounts
************************************************************************************************************/       
    public static List<Account> createTestAccountRecords(Integer numAccts) {
        List<Account> accts = new List<Account>();
        for(Integer i=0;i<numAccts;i++) {
            Account acc = new Account(Name='TestAccount' + i);
            accts.add(acc);
        }
        return accts;
    }  
    
    /************************************************************************************************************
@description  - Creation of Test Data for Opportunities
************************************************************************************************************/       
    public static Opportunity CreateOpportunity(String strName,Id AccountId,Id ContactId)
    {
        Deal_Cycle__c   DC= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC3= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC4= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC5= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC6= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC7= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC8= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC9= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC10= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC11= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC12= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC13= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC14= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC15= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC16= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC17= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC18= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC19= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC20= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
        Deal_Cycle__c   DC21= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC22= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
        Deal_Cycle__c   DC23= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
        
        Deal_Cycle__c   DC24= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Retail',Ageing__c=20); 
        Deal_Cycle__c   DC25= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Large',Ageing__c=30);                       
        
        List<Sobject> objectsToInsert = new List<Sobject>();
        objectsToInsert.add(DC);
        objectsToInsert.add(DC1);
        objectsToInsert.add(DC2);
        objectsToInsert.add(DC3);
        objectsToInsert.add(DC4);
        objectsToInsert.add(DC5);
        objectsToInsert.add(DC6);
        objectsToInsert.add(DC7);
        objectsToInsert.add(DC8);
        objectsToInsert.add(DC9);
        objectsToInsert.add(DC10);
        objectsToInsert.add(DC11);
        objectsToInsert.add(DC12);
        objectsToInsert.add(DC13);
        objectsToInsert.add(DC14);
        objectsToInsert.add(DC15);
        objectsToInsert.add(DC16);
        objectsToInsert.add(DC17);
        objectsToInsert.add(DC18);
        objectsToInsert.add(DC19);
        objectsToInsert.add(DC20);
        objectsToInsert.add(DC21);
        objectsToInsert.add(DC22);
        objectsToInsert.add(DC23);
        objectsToInsert.add(DC24);
        objectsToInsert.add(DC25);
        
        insert objectsToInsert;
        
        Opportunity oOpportunity = new Opportunity();
        oOpportunity.Name = strName;
        oOpportunity.AccountId = AccountId;
        oOpportunity.StageName = '1. Discover';        
        oOpportunity.Target_Source__c = 'Consulting Pull Through';
        oOpportunity.Opportunity_Origination__c='Proactive';
        oOpportunity.Is_RS_parameter_approve__c	= True;
        oOpportunity.Sales_Region__c='India';
        oOpportunity.Sales_country__c = 'India';
        oOpportunity.Deal_Type__c = 'Sole Sourced';        
        oOpportunity.CloseDate = System.today();
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(2);
        oOpportunity.Contract_Term_in_months__c=36;        
        oOpportunity.Revenue_Product__c = 'Test';
        oOpportunity.CMITs_Check__c = False;       
        oOpportunity.Advisor_Firm__c = 'Trestle';
        oOpportunity.Contact__c = ContactId;
        oOpportunity.Advisor_Name__c = 'Bernhard Janischowsky';        
        oOpportunity.Annuity_Project__c = 'Project';
        oOpportunity.Deal_Type__c = 'Sole Sourced';       
        oOpportunity.Win_Loss_Dropped_reason1__c='test';
        oOpportunity.Win_Loss_Dropped_reason2__c='test';
        oOpportunity.Win_Loss_Dropped_reason3__c='test';
        oOpportunity.SPOC_s__c='Nitesh Aggarwal';        
        
        return oOpportunity;
    } 
    
    /************************************************************************************************************
@description  - Creation of Test Data for COntacts 
************************************************************************************************************/       
    public static List<Contact> createTestContactsRecordsWOAccount(Integer numContacts) {  
        List<Contact> cons = new List<Contact>();                
        for (Integer k=0;k<numContacts;k++) {
            Contact contaactRecord = new Contact(firstname='Test'+k,
                                                 lastname='Test'+k,
                                                 Birthdate = Date.newInstance(1950, 5, 16),                              
                                                 Email = 'Test'+k+'@Test.com');            
            cons.add(contaactRecord );        
        }
        insert cons;
        return cons;
    }
    
    /************************************************************************************************************
@description  - Creation of Test Data for Leads
@author - Persistent
************************************************************************************************************/   
    public static List<Lead> createTestLeadRecords(Integer numLeads) {    
        List<Lead> testLeadList = new List<Lead>();
        for(Integer i=0;i<numLeads;i++) {
            Lead testLead = new Lead(firstname='test'+i,
                                     lastname='Test'+i,
                                     company='testCompany'+i,
                                     email='test'+i+'@test.com',
                                     phone='1111111111'
                                    );
            testLeadList.add(testLead);
        }
        insert testLeadList;
        return testLeadList;
    }  
    /************************************************************************************************************
@description  - Creation of Test Data for Product
************************************************************************************************************/   
    public static List<Product2> createTestProducts(Integer count)
    {
        List<Product2> prodList = new List<Product2>();
        for(Integer i=0;i<count;i++)
        {
            Product2 prod = new Product2(
                Name = 'Genpact Cora SeQuence-Professional Services',
                Nature_of_Work__c = 'Digital',
                Service_Line__c = 'F&A Multi-tower Consulting'
            );
            prodList.add(prod);
        }
        return prodList;
    }
    
    /************************************************************************************************************
@description  - Creation of Test Data for Product for Revenue Schedule
************************************************************************************************************/   
    public static List<Product2> createTestProducts2(Integer count)
    {
        List<Product2> prodList = new List<Product2>();
        for(Integer i=0;i<count;i++)
        {
            Product2 prod = new Product2(
                Name = 'Genpact Cora SeQuence-Professional Services',
                Nature_of_Work__c = 'Digital',
                Service_Line__c = 'F&A Multi-tower Consulting',
                CanUseRevenueSchedule=true
            );
            prodList.add(prod);
        }
        return prodList;
    }
    
    /************************************************************************************************************
@description  - Creation of Test Data for OLI
************************************************************************************************************/      
    public static List<OpportunityLineItem> createOpportunityLineItems(List<Product2> prodList,Id oppId,Id pricebookentryid)
    {
        List<OpportunityLineItem> oppLineItem = new List<OpportunityLineItem>();
        for(Product2 prod : prodList)
        {
            oppLineItem.add(new OpportunityLineItem(
                OpportunityId = oppId,
                Product2Id = prod.Id,
                Contract_Term__c = 2,
                Local_Currency__c = 'USD',
                TCV__c = 100000000.00,
                Revenue_Start_Date__c = System.today(),
                Delivering_Organisation__c = 'AMERICAS',
                Sub_Delivering_Organisation__c = 'Brazil',
                Product_BD_Rep__c = UserInfo.getUserId(),
                Delivery_Location__c = 'Americas',
                PricebookEntryId = pricebookentryid
            ));
        }
        return oppLineItem;       
    }
    /************************************************************************************************************
@description  - Creation of Test Data for User
@author - Persistent
************************************************************************************************************/
    
    /************************************************************************************************************
@description  - Creation of Test Data for OLI VH
************************************************************************************************************/      
    public static OpportunityLineItem createOpportunityLineItem(Product2 prod,Id oppId,Id pricebookentryid)
    {
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
            new OpportunityLineItem(
                OpportunityId = oppId,
                Product2Id = prod.Id,
                Contract_Term__c = 2,
                Local_Currency__c = 'USD',
                TCV__c = 100000000.00,
                Revenue_Start_Date__c = System.today(),
                Delivering_Organisation__c = 'AMERICAS',
                Sub_Delivering_Organisation__c = 'Brazil',
                Product_BD_Rep__c = UserInfo.getUserId(),
                Delivery_Location__c = 'Americas',
                Type_of_Deal__c = 'Renewal',
                PricebookEntryId = pricebookentryid
            );
        return oppLineItem;       
    }
    /************************************************************************************************************
@description  - Creation of Test Data for User
@author - Persistent
************************************************************************************************************/
    
    public static User createTestUser(String profileName,String username,String email)
    {
        Id profileId = [Select id,Name from Profile where name =: profileName Limit 1].Id;
        User u = new User();        
        u.ProfileId = profileId;
        u.Username = username;
        u.Email = email;
        u.FirstName = 'Test';
        u.LastName = 'Test';
        u.Alias = 'Test.Te';
        u.TimeZoneSidKey = 'America/Los_Angeles';
        u.LocaleSidKey = 'en_US';
        u.LanguageLocaleKey = 'en_US';
        u.EmailEncodingKey = 'ISO-8859-1';
        return u;
    }
    
    
    /************************************************************************************************************
@description  - Creation of Test Data for User
@author - Persistent
************************************************************************************************************/      
    public static Account createTestAccountRecord() {
        Business_Segments__c businessSegment;
        Sub_Business__c subBusiness;
        Archetype__c archeType;
        Sales_Unit__c salesUnit;
        Account acc;
        RecordType customerAccount = [select id from recordType where sObjectType = 'Account' And DeveloperName = 'Accounts' limit 1];
        
        businessSegment = new Business_Segments__c(Name='Genpact',For__c ='Genpact');
        insert businessSegment;
        subBusiness = new Sub_Business__c(Business_Segment__c=businessSegment.Id,Name='Genpact');
        insert subBusiness;
        archeType = new Archetype__c(Name='ok');
        insert archeType;
        salesUnit = new Sales_Unit__c(Name='SU');
        insert salesUnit;
        acc = new Account(Name='abc',
                          Business_Group__c ='Genpact',
                          Industry_Vertical__c ='BFS',
                          Business_Segment__c =businessSegment.Id,
                          Sub_Industry_Vertical__c ='BFS',
                          Hunting_Mining__c ='Hunting/Mining',
                          Sub_Business__c=subBusiness.Id,
                          Website='https://www.alfasystems.com',
                          Data_visibility__c='CMIT',
                          Account_Headquarter_Genpact_Regions__c='Africa',
                          Archetype__c=archeType.Id,
                          Account_Headquarters_Country__c='Algeria',
                          Named_Account__c='Yes',
                          Account_Trigger__c='Yes',
                          Sales_Unit__c=salesUnit.Id,
                          AnnualRevenue= 125,
                          TAM__c = 55,
                          PTAM__c = 66,
                          CMIT_BU__c='BFSI & HT',
                          CMIT_Sub_BU__c='BFS',
                          RecordTypeId = customerAccount.Id);
        return acc;
    }
    public static QSRM__c createTestQSRMRecord(Id opportunityId,Id recType)
    {
        
        QSRM__c qsrmRecord = new QSRM__c(Opportunity__c=opportunityId,
                                         Deal_Administrator__c='Analyst/Advisory Firm',
                                         Deal_Type__c='Annuity',
                                         Named_Advisor__c='5 Star Consultants,LLC',
                                         Named_Analyst__c='Linda Hoge',
                                         Is_this_is_a_RPA_deal__c='Yes',
                                         Does_the_client_have_a_budget_for_Opp__c='Don\'t Know',
                                         What_is_likely_decision_date__c='Don\'t Know',
                                         Does_the_client_have_compelling_need__c='Don\'t Know',
                                         Do_we_have_the_right_domain_knowledge__c='Don\'t Know',
                                         Do_we_have_client_references__c='Don\'t Know',
                                         Do_we_have_connect_with_decision_maker__c='Don\'t Know',
                                         Who_is_the_competition_on_this_deal__c='Others',
                                         What_is_the_type_of_Project__c='Gainshare/Outcome',
                                         What_is_the_nature_of_work__c='Don\'t Know',
                                         Is_this_a_RFP_or_Sole_sourced_deal__c='Sole Sourced',
                                         Status__c  = 'In Approval',
                                         RecordTypeId = recType,
                                         Bid_pro_support_needed__c='No'
                                        ); 
        return qsrmRecord;
    }
    public static List<Opportunity> createTestOpportunitiesRecordsDiscover(Id accId,Integer oppSize,Date closureDate,Contact con) {
        List<Opportunity> accts = new List<Opportunity>();
        for(Integer i=0;i<oppSize;i++) {
            Opportunity acc = new Opportunity(
                Name='Discover Opportunity' + i,
                AccountId = accId,
                StageName = '1. Discover',
                RFXStatus__c='No',
                Target_Source__c = 'Connects',
                Opportunity_Source__c = 'Market Maker',
                Opportunity_Origination__c = 'Proactive',
                Deal_Type__c = 'Sole Sourced',
                CloseDate = closureDate,
                Sales_Region__c = 'Europe',
                Sales_country__c = 'Armenia',
                NextStep='next',
                Contact1__c=con.id,
                Role__c='Decision Maker',
                Annuity_Project__c='Annuity',
                Advisor_Firm__c='5 Star Consultants,LLC',
                Deal_Administrator__c='Analyst/Advisory Firm',
                Advisor_Name__c='Linda Hoge'
                //Competitor__c='HCL'
            );
            accts.add(acc);
        }
        system.debug('acc****************************************'+accts);
        return accts;
    } 
    public static Account createTestAccountRecordforDiscoverOpportunity() {
        Business_Segments__c businessSegment;
        Sub_Business__c subBusiness;
        Archetype__c archeType;
        Sales_Unit__c salesUnit;
        Account acc;
        RecordType customerAccount = [select id from recordType where sObjectType = 'Account' And DeveloperName = 'Accounts' limit 1];
        
        businessSegment = new Business_Segments__c(Name='Global Clients',For__c ='Global Clients');
        insert businessSegment;
        subBusiness = new Sub_Business__c(Business_Segment__c=businessSegment.Id,Name='Global Clients');
        insert subBusiness;
        archeType = new Archetype__c(Name='ok');
        insert archeType;
        salesUnit = new Sales_Unit__c(Name='SU');
        insert salesUnit;
        
        acc = new Account(Name='abc',
                          Business_Group__c ='Global Clients',
                          Industry_Vertical__c ='BFS',
                          Business_Segment__c =businessSegment.Id,
                          Sub_Industry_Vertical__c ='BFS',
                          Hunting_Mining__c ='Mining',
                          Sub_Business__c=subBusiness.Id,
                          Website='https://www.alfasystems.com',
                          Data_visibility__c='CMIT',
                          Account_Headquarter_Genpact_Regions__c='Africa',
                          Archetype__c=archeType.Id,
                          Account_Headquarters_Country__c='Algeria',
                          Named_Account__c='Yes',
                          Account_Trigger__c='Yes',
                          Sales_Unit__c=salesUnit.Id,
                          AnnualRevenue= 125,
                          TAM__c = 55,
                          PTAM__c = 66,
                          CMIT_BU__c='BFSI & HT',
                          CMIT_Sub_BU__c='BFS',
                          BL__c = UserInfo.getUserId(),
                          Vintage_In_Years__c = 'Test',
                          RecordTypeId = customerAccount.Id);
        return acc;
    }
    public static Contact_Role_Rv__c createTestContactRole(Id oppId) {
        
        Contact_Role_Rv__c conRole=new Contact_Role_Rv__c
            (
                IsPrimary__c=false,
                Opportunity__c=oppId,
                Role__c='Other');
        return conRole;
    }
    
    public static List<Product2> testProducts(Integer count,String natureOfWork)
    {
        List<Product2> prodList = new List<Product2>();
        for(Integer i=0;i<count;i++)
        {
            Product2 prod = new Product2(
                Name = 'Test'+i,
                Nature_of_Work__c = natureOfWork,
                Service_Line__c = 'Accounts Payable'
            );
            prodList.add(prod);
        }
        return prodList;
    }
    public static List<Opportunity> createTestOpportunitiesRecordsConfirmed(Id accId,Integer oppSize,Date closureDate,Contact con,Id pricerId) {
        List<Opportunity> oppList = new List<Opportunity>();
        for(Integer i=0;i<oppSize;i++) {
            Opportunity acc = new Opportunity(
                Name='Confirmed Opportunity' + i,
                AccountId = accId,
                StageName = '5. Confirmed',
                Target_Source__c = 'Connects',
                Opportunity_Source__c = 'Market Maker',
                Opportunity_Origination__c = 'Proactive',
                Deal_Type__c = 'Sole Sourced',
                CloseDate = closureDate,
                Sales_Region__c = 'Europe',
                Sales_country__c = 'Armenia',
                NextStep='next',
                Contact1__c=con.id,
                Role__c='Decision Maker',
                Annuity_Project__c='Annuity',
                Advisor_Firm__c='5 Star Consultants,LLC',
                Deal_Administrator__c='Analyst/Advisory Firm',
                Advisor_Name__c='Linda Hoge',
                Contract_type__c='MSA or over-arching agreement',
                Pricer_Name__c=pricerId
            );
            oppList.add(acc);
        }
        return oppList;
    } 
    public static Pricer__c createTestPricer()
    {
        Pricer__c pricerRecord=new Pricer__c(Name='PricerName',OHR_ID__c='OhrId',Email__c='abc@gmail.com');
        return pricerRecord;
    }
    public static ForecastingQuota createTestQuota(Id salesPersonId )
    {
        ForecastingType ft = [select id from ForecastingType limit 1];
        
        ForecastingQuota quotaRecord=new ForecastingQuota(QuotaOwnerId=salesPersonId,QuotaAmount=1000,ForecastingTypeId=ft.Id,startDate=system.today());
        return quotaRecord;
    }
    public static Contact CreateContact(String strFirstName,String strLastName,Id AccountId,String strTitle,string strLeadSource,String strEmail,string strPhone)
    {
        Contact oContact = new Contact();
        oContact.FirstName = strFirstName;
        oContact.LastName = strLastName;
        oContact.AccountId = AccountId;
        oContact.Title = strTitle;
        oContact.LeadSource = strLeadSource;
        oContact.Email = strEmail;  
        oContact.Phone = strPhone;
        return oContact;
    }
    
    
    public static List<Opportunity> createTestOpportunitiesRecords(Id accId,Integer oppSize,String stage,Date closureDate) 
    {
        List<Opportunity> accts = new List<Opportunity>();
        for(Integer i=0;i<oppSize;i++) {
            Opportunity acc = new Opportunity(
                Name='TestOpportunity' + i,
                AccountId = accId,
                StageName = stage,
                Target_Source__c = 'Connects',
                Opportunity_Source__c = 'Market Maker',
                Opportunity_Origination__c = 'Reactive',
                Deal_Type__c = 'Competitive',
                CloseDate = closureDate,
                Sales_Region__c = 'Asia',
                Sales_country__c = 'India'                
            );
            accts.add(acc);
        }
        
        return accts;
    }
    public static List<Product2> createTestProducts3(Integer count)
    {
        List<Product2> prodList = new List<Product2>();
        for(Integer i=0;i<count;i++)
        {
            Product2 prod = new Product2(
                Name = 'Invoice To Cash',
                Nature_of_Work__c = 'Managed Services',
                Service_Line__c = 'I2C-Contract Admin'
            );
            prodList.add(prod);
        }
        return prodList;
    }
}