/*************************************************************************************************************************
* @name : OpportunityConfirmedAddContract
* @author : Persistent
* @description  : Used by OpportunityConfirmedAddContract component 
**************************************************************************************************************************/

public class OpportunityConfirmedAddContract {
    
    /*************************************************************************************************************************
* @name : getContractId
* @author : Persistent
* @description  : returns count of contracts in draft state
* @param  : opportunityId 
* @return : contract id
**************************************************************************************************************************/
    
    @AuraEnabled 
    public static Integer getContractInDraft(String opportunityId)
    {
        try
        {
            Integer count=0;//count of contracts in draft state
            count=[select count() from Contract where Opportunity_tagged__c=:opportunityId and status='Draft'];
            
            return count;//returns count of contracts in draft state
        }
        catch(Exception ex)
        {
            //creating error log
            CreateErrorLog.createErrorRecord(opportunityId,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityConfirmedAddContract', 'getContractInDraft','Fail','',String.valueOf(ex.getLineNumber())); 
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    /*************************************************************************************************************************
* @name : submitForApproval
* @author : Persistent
* @description  : function for submiting the approval process
* @param  : contractId
* @return : isSucess
**************************************************************************************************************************/
    
    @AuraEnabled
    public static void submitForApproval(String opportunityId)
    {
        try{
            
            List<Contract> ContractObjectList;
            ContractObjectList=[select id,name,Opportunity_tagged__c,Number_of_Attachments__c ,Survey_Filled__c,Survey_Initiated_contract__c,Deal_outcome__c from Contract where Opportunity_tagged__c = :opportunityId and status='Draft'];
            if(ContractObjectList.size()>0)
            {
                for(Contract ContractObject:ContractObjectList)
                {
                    //cheking if winloss survey is filled or not and deal is signed or not 
                   
                    if(ContractObject.Survey_Filled__c==true && ContractObject.Deal_outcome__c=='Signed')
                    {
                        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                        req.setObjectId(ContractObject.id);
                        // submit the approval request for processing
                        Approval.ProcessResult result = Approval.process(req);
                             
                    }
                    
                }
                Opportunity oppRecord=[select LatestTabId__c from opportunity where id=:opportunityId];
                oppRecord.LatestTabId__c='20';
                update oppRecord;
            }
            else
            {
                throw new AuraHandledException('No contracts in Draft state');
            }
        }
        catch(Exception ex)
        {
            //creating error log
            CreateErrorLog.createErrorRecord(opportunityId,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityConfirmedAddContract', 'submitForApproval','Fail','',String.valueOf(ex.getLineNumber())); 
        }
    } 
    @AuraEnabled 
    public static void setContractFile(String opportunityId,List<String> fileId)
    {
        try
        {
            List<ContentVersion>conVerList=new List<ContentVersion>();
            for(integer i=0;i<fileid.size();i++)
            {
                ContentVersion conVer=[select Id,ContractFile__c from  ContentVersion where ContentDocumentId=:fileId[i]];
            	conVer.ContractFile__c=true;
                conVerList.add(conVer);
            }
            
            update conVerList;
        }
        catch(Exception ex)
        {
            //creating error log
            CreateErrorLog.createErrorRecord(opportunityId,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityConfirmedAddContract', 'setContractFile','Fail','',String.valueOf(ex.getLineNumber())); 
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    /*************************************************************************************************************************
* @name : convertToAttachment
* @author : Persistent
* @description  :converting file to attachment 
* @param  : contractId,fileId
* @return : na
**************************************************************************************************************************/
    
    @AuraEnabled 
    public static void convertToAttachment(String opportunityId,String contractId,List<String> fileId)
    {
        try
        {
         	system.debug('fileId'+fileId);
            List<ContentVersion> conVersion;
            conVersion=[select Id,VersionData,ContentDocument.title,ContentDocument.FileExtension from ContentVersion where ContentDocumentId IN: fileId limit: Limits.getLimitQueryRows()];
            //inserting file data in attachment object
            system.debug('conVersion'+conVersion);
            List<Attachment_Contract__c> attachContractList=new List<Attachment_Contract__c>();
            for(Integer i=0;i<conVersion.size();i++)
            {
                Attachment_Contract__c attachContract=new Attachment_Contract__c(File_Name__c=conVersion[i].ContentDocument.title ,Contract__c=contractId);    
       			attachContractList.add(attachContract);
            }
            insert attachContractList; 
            system.debug('attachContractList'+attachContractList);
            List<Attachment> attachList=new List<Attachment>();
            for(Integer i=0;i<conVersion.size();i++)
            {
            String attachmentName=conVersion[i].ContentDocument.title+'.'+conVersion[i].ContentDocument.FileExtension;
            Attachment attach=new Attachment(Name=attachmentName,ParentId=attachContractList[i].id);
            attach.Body=conVersion[i].VersionData;
            attachList.add(attach);
            }
            insert attachList;
            system.debug('attachList'+attachList);
            for(Integer i=0;i<attachContractList.size();i++)
            {
                attachContractList[i].Attachment_ID__c=attachList[i].id;
            
            }
            update attachContractList;
            system.debug('attachContractList'+attachContractList);
            List<ContentDocument> cdList=new List<ContentDocument>();
            for(ContentDocumentLink cdl : [Select id,ContentDocument.Id from ContentDocumentLink where LinkedEntityId=:opportunityId and ContentDocument.LatestPublishedVersion.ContractFile__c=true])
            {
                cdList.add(new ContentDocument(id=cdl.ContentDocument.Id));
            }
            
            delete cdList;
        }
        catch(Exception ex)
        {
            //creating error log
            CreateErrorLog.createErrorRecord(opportunityId,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityConfirmedAddContract', 'convertToAttachment','Fail','',String.valueOf(ex.getLineNumber())); 
            throw new AuraHandledException(ex.getMessage());
        }
    }
}