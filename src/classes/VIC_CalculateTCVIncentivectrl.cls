/*
  @Description: Class used to calculate TCV Component incentive
  @Author: Vikas Rajput (previous written code by @Bashim Khan)
  @Date: May 2018
  @ModifiedBy: Vikas Rajput
  @RequiredParam: Target_Component__c , List<VIC_Calculation_Matrix_Item__c>, List<Target_Achievement__c> 
           of the same component and Target_Achievement__c's status should not include "Declined",
*/
public class VIC_CalculateTCVIncentivectrl implements VIC_ScorecardCalculationInterface{
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    /*    
        @Author: Vikas Rajput
        @Description: //TCV% Targeted Incentive = (Target Bonous * weightage)*Payout
    */
    public Decimal calculate(Target_Component__c objTargetComponent, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        Set<String> setPlans = new Set<String>{'IT_GRM'};
        
      Decimal TotalTCV =0;
      if(objTargetComponent.Target__r.vic_TCV_IO__c !=null && objTargetComponent.Target__r.vic_TCV_TS__c !=null){
        TotalTCV = objTargetComponent.Target__r.vic_TCV_IO__c + objTargetComponent.Target__r.vic_TCV_TS__c;
      }else if(objTargetComponent.Target__r.vic_TCV_IO__c ==null && objTargetComponent.Target__r.vic_TCV_TS__c !=null){
           TotalTCV =  objTargetComponent.Target__r.vic_TCV_TS__c;
      }else if(objTargetComponent.Target__r.vic_TCV_IO__c !=null && objTargetComponent.Target__r.vic_TCV_TS__c ==null){
           TotalTCV = objTargetComponent.Target__r.vic_TCV_IO__c;
      }
      Decimal ytdAttainmentPercent = 0;
      if(TotalTCV != 0 && objTargetComponent.Target_In_Currency__c != null && objTargetComponent.Target_In_Currency__c != 0){
          ytdAttainmentPercent = (TotalTCV /objTargetComponent.Target_In_Currency__c)*100;
      }
      if(setPlans.contains(objTargetComponent.Target__r.Plan__r.vic_Plan_Code__c) && 
                 Decimal.valueof(mapValueToObjIncentiveConst.get('Minimum_Attainment_TCV').vic_Value__c) 
                 <= ytdAttainmentPercent && 
                 objTargetComponent.Target__r.Target_Bonus__c != null && objTargetComponent.Weightage__c != null && 
                 objTargetComponent.Target_In_Currency__c != null && TotalTCV  != null){
               
            Decimal tcvPercentTargetedIncentive = (objTargetComponent.Target__r.Target_Bonus__c*objTargetComponent.Weightage__c)/100;
            //Decimal ytdAttainmentPercent = (TotalTCV /objTargetComponent.Target_In_Currency__c)*100;
            Decimal payoutPercent = 0.00; //payoutPercent will give total calculated payout percent
            
            for(VIC_Calculation_Matrix_Item__c objCalMatrixItem : lstCalcMatrixItem){
                if(ytdAttainmentPercent > objCalMatrixItem.vic_Start_Value__c && objCalMatrixItem.vic_End_Value__c != null && ytdAttainmentPercent <= objCalMatrixItem.vic_End_Value__c && objCalMatrixItem.vic_Payout__c != null){
                    payoutPercent = ytdAttainmentPercent*objCalMatrixItem.vic_Payout__c;
                }else if(ytdAttainmentPercent > objCalMatrixItem.vic_Start_Value__c && objCalMatrixItem.vic_End_Value__c != null && ytdAttainmentPercent > objCalMatrixItem.vic_End_Value__c && objCalMatrixItem.vic_Payout__c != null){
                    payoutPercent = objCalMatrixItem.vic_End_Value__c*objCalMatrixItem.vic_Payout__c;
                }else if(ytdAttainmentPercent > objCalMatrixItem.vic_Start_Value__c && objCalMatrixItem.vic_End_Value__c == null && objCalMatrixItem.vic_Payout__c != null){
                    payoutPercent = payoutPercent + (ytdAttainmentPercent - objCalMatrixItem.vic_Start_Value__c )*objCalMatrixItem.vic_Payout__c;
                }
            }
            return (tcvPercentTargetedIncentive * payoutPercent)/100;
        }
        return 0.00;
    }
}