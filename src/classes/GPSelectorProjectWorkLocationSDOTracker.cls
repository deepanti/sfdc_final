@isTest
public class GPSelectorProjectWorkLocationSDOTracker {
    public Static GP_Project_Work_Location_SDO__c projectWorkLocation;
    public Static GP_Project__c project;
    
    @testSetup
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        insert project;
        
        GP_Project_Work_Location_SDO__c projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Project__c = project.Id;
        projectWorkLocation.GP_Work_Location__c = sdo.Id;
        
        insert projectWorkLocation;
    }
     
     @isTest
    public static void testgetSObjectType() {
        projectWorkLocation = [Select Id from GP_Project_Work_Location_SDO__c LIMIT 1];
        project = [Select Id from GP_Project__c LIMIT 1];
        
        GPSelectorProjectWorkLocationSDO objselectorprojectworklocation = new GPSelectorProjectWorkLocationSDO();       
        objselectorprojectworklocation.selectById(new Set<Id>());
        GPSelectorProjectWorkLocationSDO.selectProjectSDOforProjectIds(new Set<Id>());
        objselectorprojectworklocation.selectProjectWorkLocationAndSDORecords(project.Id);
        objselectorprojectworklocation.selectProjectWorkLocationAndSDORecords(new Set<Id>());
        GPSelectorProjectWorkLocationSDO.selectProjectWorkLocationAndSDORecordsUnderProject(project.Id);
        GPSelectorProjectWorkLocationSDO.selectProjectWorkLocationforProjectIds(null);
    }
}