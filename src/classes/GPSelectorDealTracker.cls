@isTest
public class GPSelectorDealTracker {
	@testSetup
    public static void buildDependencyData()
    {
        GP_Deal__c deal = GPCommonTracker.getDeal();
        insert deal;
    }
    @isTest 
    public static void testSelectBySetOfId()
    {
        GPSelectorDeal dealSelector = new GPSelectorDeal();
        GP_Deal__c expectedDeal = [SELECT Id FROM GP_Deal__c LIMIT 1];
        List<GP_Deal__c> returnedDeals = dealSelector.selectById(new Set<Id> {expectedDeal.Id});
        System.assertEquals(expectedDeal.Id, returnedDeals[0].Id);
    }
    @isTest
    public static void testSelectById() 
    {
        GPSelectorDeal dealSelector = new GPSelectorDeal();
        GP_Deal__c expectedDeal = [SELECT Id FROM GP_Deal__c LIMIT 1];
        GP_Deal__c returnedDeal = dealSelector.selectById(expectedDeal.Id);
        System.assertEquals(expectedDeal.Id, returnedDeal.Id);
    }
    @isTest
    public static void testSelectDealWithOpportunity()
    {
        GPSelectorDeal dealSelector = new GPSelectorDeal();
        GP_Deal__c expectedDeal = [SELECT Id FROM GP_Deal__c LIMIT 1];
        GP_Deal__c returnedDeal = dealSelector.selectDealWithOpportunity(expectedDeal.Id);
        System.assertEquals(expectedDeal.Id, returnedDeal.Id);
    }
    
    @isTest
    public static void testgetDealDataForTemplateSelection()
    {
        //GPSelectorDeal dealSelector = new GPSelectorDeal();
        GP_Deal__c expectedDeal = [SELECT Id FROM GP_Deal__c LIMIT 1];
        GP_Deal__c returnedDeal = GPSelectorDeal.getDealDataForTemplateSelection(expectedDeal.Id);
        System.assertEquals(expectedDeal.Id, returnedDeal.Id);
    }
    
    @isTest
    public static void testselectupdateOppDeal()
    {
        set<String> setOfId  = new set<String>();
        GPSelectorDeal dealSelector = new GPSelectorDeal();
        GP_Deal__c expectedDeal = [SELECT Id FROM GP_Deal__c LIMIT 1];
        setOfId.add(expectedDeal.Id);
        list<GP_Deal__c> returnedDeal = dealSelector.selectupdateOppDeal(setOfId);
        System.assertEquals(returnedDeal.size(), 0);
        dealSelector.selectOppDeal(null);
        try {
            GPSelectorDeal.getDealWithMaxTCVUnderOppProject(null);            
        } catch(Exception ex) {}
    }
    
    @isTest
    static void testDealDetailsForCreateDealBatch() {
        
        GPSelectorDeal dealSelector = new GPSelectorDeal();
        dealSelector.selectOppDealOnOppId(new Set<String>());
        
        GPSelectorDeal.getDealDetailsForCreateDealBatch(new List<Id>());
    }
	// OMS Change 17-9-19.
	@isTest
    static void testDealDataToClonePID() {
        GPSelectorDeal.getDealDataToClonePID(new Set<Id>());
    }
}