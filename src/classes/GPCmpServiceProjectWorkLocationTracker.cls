@isTest
public class GPCmpServiceProjectWorkLocationTracker {

    public static GP_Employee_Master__c empObj;
    public static GP_Project__c prjlst;
    public static GP_Deal__c  dealobj;
    public static GP_Address__c address;
    public static Account accobj;
    public static GP_Project_Template__c objprjtemp;
    public static String jsonresp;
    public static GP_Icon_Master__c iconMaster;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    public static GP_Customer_Master__c customerMaster;
    public static GP_Work_Location__c objSdo;
    public static List<GP_Project_Work_Location_SDO__c> listOfProjectWorkLocation;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
         Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
 
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c bpmTemplate = GPCommonTracker.getProjectTemplate();
        bpmTemplate.Name = 'BPM-BPM-ALL';
        bpmTemplate.GP_Active__c = true;
        bpmTemplate.GP_Business_Type__c = 'BPM';
        bpmTemplate.GP_Business_Name__c = 'BPM';
        bpmTemplate.GP_Business_Group_L1__c = 'All';
        bpmTemplate.GP_Business_Segment_L2__c = 'All';
        insert bpmTemplate ;
        
        GP_Project_Template__c bpmWithoutSFDCTemplate = GPCommonTracker.getProjectTemplate();
        
        bpmWithoutSFDCTemplate.GP_Active__c = true;
        bpmWithoutSFDCTemplate.Name = 'BPM-BPM-ALL-Without-SFDC';
        bpmWithoutSFDCTemplate.GP_Business_Type__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Name__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Group_L1__c = 'All';
        bpmWithoutSFDCTemplate.GP_Business_Segment_L2__c = 'All';
        bpmWithoutSFDCTemplate.GP_Without_SFDC__c = true;
        
        insert bpmWithoutSFDCTemplate;
        
        GP_Project_Template__c indirectTemp = GPCommonTracker.getProjectTemplate();
        indirectTemp.GP_Active__c = true;
        indirectTemp.Name = 'INDIRECT_INDIRECT';
        indirectTemp.GP_Business_Type__c = 'Indirect';
        indirectTemp.GP_Business_Name__c = 'Indirect';
        indirectTemp.GP_Business_Group_L1__c = 'All';
        indirectTemp.GP_Business_Segment_L2__c = 'All';
        insert indirectTemp;
        
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        insert timeshtentryObj ;
        customerMaster = new GP_Customer_Master__c();
        insert customerMaster;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        address.GP_Customer__c = customerMaster.Id;
        address.GP_Billing_Entity__c = objpinnacleMaster.Id;
        address.GP_Status__c = 'Active';
        address.GP_BILL_TO_FLAG__c = true;
        address.GP_SHIP_TO_FLAG__c = true;
        
        insert address ;
        
        GP_Address__c  dummyAddress = GPCommonTracker.getAddress();
        dummyAddress.GP_Customer__c = customerMaster.Id;
        dummyAddress.GP_Billing_Entity__c = objpinnacleMaster.Id;
        dummyAddress.GP_Status__c = 'Active';
        dummyAddress.GP_BILL_TO_FLAG__c = true;
        dummyAddress.GP_SHIP_TO_FLAG__c = true;
        dummyAddress.GP_City__c = 'GENPACT Dummy';
        
        insert dummyAddress;
        GP_Project_Work_Location_SDO__c projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Project__c = prjObj.Id;
		projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Primary__c = true;
        projectWorkLocation.GP_Project__c = prjObj.Id;
		projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        insert projectWorkLocation;
    }    
    
    @isTest
    public static void testGPCmpServiceProjectDetail()
    {
        fetchData();
        testGetWorklocationData();
        testSaveWorklocationData();
        testUpdateWorklocationData();
        testDeleteProjectWorkLocation();
        testGetRelatedDataForWorkLocation();
    }
    
    public static void fetchData() {
        empObj = [select id from GP_Employee_Master__c limit 1];        
        prjlst = [select id,Name from GP_Project__c limit 1];        
        dealobj = [select id,Name, GP_Business_Group_L1__c, GP_Business_Segment_L2__c, GP_Deal_Type__c from GP_Deal__c  limit 1];        
        address = [select id,Name from GP_Address__c  limit 1];        
        accobj = [select id,Name from Account limit 1];
        objprjtemp = [select id from GP_Project_Template__c limit 1];
        jsonresp= (String)JSON.serialize(prjlst); 
        iconMaster = [Select Id from GP_Icon_Master__c LIMIT 1];
        objpinnacleMaster = [Select Id from GP_Pinnacle_Master__c LIMIT 1];
        customerMaster = [Select Id from GP_Customer_Master__c LIMIT 1];
        
        objSdo = [Select Id from GP_Work_Location__c LIMIT 1];
        listOfProjectWorkLocation = [Select Id from GP_Project_Work_Location_SDO__c];
    }
    public static void testGetWorklocationData(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectWorkLocation.getWorklocationData(prjlst.Id);
        System.assertEquals(true, returnedResponse.isSuccess);
        
        dealobj.GP_Deal_Category__c = 'Sales SFDC';
        update dealobj; 
        returnedResponse = GPCmpServiceProjectWorkLocation.getWorklocationData(prjlst.Id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testSaveWorklocationData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectWorkLocation.saveWorklocationData(JSON.serialize(listOfProjectWorkLocation), '9000', 'Test HSN Category', false);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testUpdateWorklocationData() {
        List<GP_Project_Work_Location_SDO__c> listOfWorklocationToBeDeleted = new List<GP_Project_Work_Location_SDO__c>();
        listOfWorklocationToBeDeleted.add(listOfProjectWorkLocation[0]);
        listOfWorklocationToBeDeleted.remove(0);
        
        GPAuraResponse returnedResponse = GPCmpServiceProjectWorkLocation.updateWorklocationData(JSON.serialize(listOfProjectWorkLocation), JSON.serialize(listOfWorklocationToBeDeleted));
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testDeleteProjectWorkLocation() {        
        GPAuraResponse returnedResponse = GPCmpServiceProjectWorkLocation.deleteProjectWorkLocation(listOfProjectWorkLocation[0].Id, true);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    
    public static void testGetRelatedDataForWorkLocation() {        
        GPAuraResponse returnedResponse = GPCmpServiceProjectWorkLocation.getRelatedDataForWorkLocation(objSdo.Id);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
}