@istest

Public class RemovePackagelicense_Test{

    public static testMethod void RunTest()
    {    Test.starttest();
        UserRole Obj = new UserRole();
        Obj.Name='TestRole';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standartestgen2015@testorg.com' );
        string pckgLicenseList  = [SELECT Id, NamespacePrefix FROM PackageLicense WHERE NamespacePrefix = 'RevenueStormRB' Limit 1].id;
   
                        
        UserPackageLicense UPL = new  UserPackageLicense ();
        UPL.Userid = u.id;
        UPL.PackageLicenseid = pckgLicenseList;
        insert UPL;
    
        u.isactive=false;
        update u;
        Test.stoptest();
    }




}