public class GPProjectValidatorConfig {
    public Boolean validateProject;
    public Boolean validateProjectLeadership;
    public Boolean validateResourceAllocation;
    public Boolean validateBillingMilestone;
    public Boolean validateProjectBudget;
    public Boolean validateProjectExpense;
    public Boolean validateDocumentUpload;
    public Boolean validateProfileBillRate;
    public Boolean validateWorkLocationAndSDO;
    public Boolean validateProjectAddress;
    public Boolean validateUserStage;
    
    public GPProjectValidatorConfig(Boolean validate) {
        validateProject = validate;
        validateProjectLeadership = validate;
        validateResourceAllocation = validate;
        validateBillingMilestone = validate;
        validateProjectBudget = validate;
        validateDocumentUpload = validate;
        validateProjectExpense = validate;
        validateProfileBillRate = validate;
        validateWorkLocationAndSDO = validate;
        validateProjectAddress = validate;
        validateUserStage = validate;
    }
}