// ------------------------------------------------------------------------------------------------ 
// Description: When the CRN Stage is Approved And the next CRN Stage date is updated for the next defined
// hours then the email notification is sent to the Defined leadership roles
// ------------------------------------------------------------------------------------------------
// Modification Date::05-DEC -2017  Created By : Mandeep Singh Chauhan
// ------------------------------------------------------------------------------------------------ 
global class GPBatchNotifyProjectUsersCRNStgeUpdte implements
Database.Batchable < sObject > , Database.Stateful {

    global Messaging.SingleEmailMessage message;
    global list < Messaging.SingleEmailMessage > EmailTolist;
    //Map<GP_Project__c,Set<GP_Project_Leadership__c>> mapOfProjectAndLeadrship = new map<GP_Project__c,set<GP_Project_Leadership__c>>();
    global list < String > listofemails;
    //changed to day format...
    global datetime today = system.now();
    global datetime initialdate = system.now().addDays(-2);
    global datetime secondarydate = system.now().addDays(-3);
    Map < Id, Map < String, String >> mapOfProjectIDVsRoleNameVsEmailIds = new Map < Id, Map < String, String >> ();
    List < GP_Project__c > listOfProjectRecords = new List < GP_Project__c > ();
    //global datetime initialdate = system.now();
    //global datetime secondarydate = system.now().addHours(-(integer.valueOf(system.label.GP_CRN_Stage_Date_Second_level)));
    Id MandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('initialdate'+initialdate);
        return Database.getQueryLocator([Select Id, Name, GP_Next_CRN_Stage_Date__c, GP_GPM_Employee_Email__c,
            GP_GPM_Employee__r.GP_SFDC_User__c, GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
            GP_Oracle_PID__c, GP_GRM__c, GP_GRM__r.GP_OFFICIAL_EMAIL_ADDRESS__c, GP_Approval_Status__c,GP_End_Date__c,
            (select id, GP_Employee_ID__r.GP_SFDC_User__c,GP_Leadership_Role__c,
                GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c from Project_Leaderships__r where RecordTypeID =: MandatoryKeyMembersRecordTypeId and(GP_Leadership_Role__c =: System.Label.GP_Billing_Approver_Role_Code or GP_Leadership_Role__c =: System.Label.GP_Billing_SPOC_Leadership_Role_Code or GP_Leadership_Role__c =: System.Label.GP_FP_A_Approver))
            From GP_Project__c where
            GP_CRN_Status__c = 'Signed Contract Received'
            and(GP_Next_CRN_Stage_Date__c =: initialdate or GP_Next_CRN_Stage_Date__c =: secondarydate)
        ]);

    }

    global void execute(Database.BatchableContext bc, List < GP_Project__c > listOfProjectRecords) {
        if (listOfProjectRecords != null && listOfProjectRecords.size() > 0) {
            this.listOfProjectRecords = listOfProjectRecords;
            for (GP_Project__c project: listOfProjectRecords) {
                if (project.Project_Leaderships__r != null) {
                    for (GP_Project_Leadership__c projectLeadership: project.Project_Leaderships__r) {
                        if (!(projectLeadership.GP_Employee_ID__c != null && projectLeadership.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null))
                            break;

                        if (!mapOfProjectIDVsRoleNameVsEmailIds.containsKey(project.Id)) {
                            mapOfProjectIDVsRoleNameVsEmailIds.put(project.Id, new Map < String, String > ());
                        }
                        if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_Billing_Approver_Role_Code)
                            mapOfProjectIDVsRoleNameVsEmailIds.get(project.Id).put(projectLeadership.GP_Leadership_Role__c, projectLeadership.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                        else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_Billing_SPOC_Leadership_Role_Code)
                            mapOfProjectIDVsRoleNameVsEmailIds.get(project.Id).put(projectLeadership.GP_Leadership_Role__c, projectLeadership.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                        else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_FP_A_Approver)
                            mapOfProjectIDVsRoleNameVsEmailIds.get(project.Id).put(projectLeadership.GP_Leadership_Role__c, projectLeadership.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                    }
                }
            }
        }
    }

    global void finish(Database.BatchableContext bc) {
        try {
            EmailTolist = new list < Messaging.SingleEmailMessage > ();

            EmailTemplate EmpTemplate = [Select id, DeveloperName, body, subject, htmlvalue
                from EmailTemplate where
                developername = 'GP_Email_Template_for_Batch_Notifying_CRN_Stage_Update'
                limit 1
            ];
			Id orgWideEmailId = GPCommon.getOrgWideEmailId();
            for (GP_Project__c project: listOfProjectRecords) {
                listofemails = new list < string > ();
                message = new GPCommon().notificationThroughEmailForBatch(EmpTemplate, project, 'GP_Field_Set_For_Batch_For_Email_Notific', 'GP_Project__c');
                //message.settargetobjectid(project);
                Map < String, String > mapOfRoleCodeVsEmailId = mapOfProjectIDVsRoleNameVsEmailIds.get(project.Id);
                EmailTolist.add(message);
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_Billing_Approver_Role_Code))
                    listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_Billing_Approver_Role_Code));
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_Billing_SPOC_Leadership_Role_Code))
                    listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_Billing_SPOC_Leadership_Role_Code));
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_FP_A_Approver))
                    listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_FP_A_Approver));
                //In case of third or final notification mail has to be sent to the Leadership roles including
                // GPM Employee and in case of secondary and initial notification no mail to be sent to the GPM Employee

                if (project.GP_Next_CRN_Stage_Date__c != null && project.GP_Next_CRN_Stage_Date__c == secondarydate &&
                    project.GP_GPM_Employee__c != null && project.GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                    listofemails.add(project.GP_GRM__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                }


                if (listofemails != null && listofemails.size() > 0) {
                    message.setToAddresses(listofemails);
                }
                if (message != null) {
                    // since a single settargetobjectid can only be added in a message so 
                    // they are added individualy in the individual message above
                    message.settargetobjectid(null);
                    if(orgWideEmailId != null)
                    	message.setOrgWideEmailAddressId(orgWideEmailId);
                    EmailTolist.add(message);
                }
            }

            if (EmailTolist != null && EmailTolist.size() > 0 && GPCommon.allowSendingMail()) {
                Messaging.SendEmailResult[] EHRBP = Messaging.sendEmail(EmailTolist);
            }
        } catch (Exception ex) {
            System.debug('Exc' + ex.getMessage());
        }
    }
}