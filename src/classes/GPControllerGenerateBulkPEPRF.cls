public without sharing class GPControllerGenerateBulkPEPRF {
    
    private final String LIST_OF_APPROVED_PROJECT_LABEL = 'listOfApprovedProjects';
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String WARNING_LABEL = 'WARNING';
    
    private Map < Id, GP_Project__c > mapOfApprovedProjects = new Map < Id, GP_Project__c > ();
    private List < GP_Project__c > listOfApprovedProjects = new List < GP_Project__c > ();
    private List<String> listOfBusinessNames = new List<String>();
    private Date startDate, endDate;
    private String selectedStatus, businessName;
    private Boolean isHasRecords;
    private JSONGenerator gen;
    
    public GPControllerGenerateBulkPEPRF() {}
    
    public GPControllerGenerateBulkPEPRF(Date startDate, Date endDate, String selectedStatus, String businessName) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.selectedStatus = selectedStatus;
        this.businessName = businessName;
        this.isHasRecords = true;
    }
    
    public GPAuraResponse getApprovedPinnaclePIDForDuration() {
        try {
            setMapOfApprovedProjects();
            if(isHasRecords) {
                setContentVersionHistoryList();
            }            
            setJSON();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }
    
    private void setMapOfApprovedProjects() {
        String query = 'Select id, Name, GP_Content_Version_Document_Id__c, GP_Oracle_PID__c, GP_Business_Name__c from GP_Project__c where GP_Oracle_Status__c = \'S\' AND GP_Oracle_PID__c != \'NA\' AND GP_Business_Type__c = \'PBB\'';
        
        if(selectedStatus == 'Create') {
            query += ' AND (GP_PID_Creation_Date__c <=: endDate AND GP_PID_Creation_Date__c >=: startDate) AND RecordType.Name != \'Indirect PID\'';
        } else {
            query += ' AND (DAY_ONLY(GP_Approval_DateTime__c) <= :endDate AND DAY_ONLY(GP_Approval_DateTime__c) >= :startDate) AND RecordType.Name !=\'Indirect PID\'';
        }
        
        if(String.isNotBlank(businessName) && !businessName.equalsIgnoreCase('All')) {
            query +=' AND GP_Business_Name__c = \'' + businessName + '\'';
        }
        
        query += ' Order By GP_Oracle_PID__c';
        
        for(GP_Project__c project : Database.query(query)) {
            mapOfApprovedProjects.put(project.id, project);
        }
        
        if(selectedStatus == 'Create' && mapOfApprovedProjects.keySet().size() == 0) {
            isHasRecords = false;
        }
    }
    
    private void setContentVersionHistoryList() {
        GP_Project__c project;
        
        String query = 'Select id, Name, GP_Project__r.Name, GP_Content_Version_Document_Id__c, GP_Project__r.GP_Business_Name__c,'
            + ' GP_Project__r.GP_Oracle_PID__c, GP_Oracle_Status__c, GP_Status__c, GP_Version_No__c, GP_Project__c from GP_Project_Version_History__c'
            + ' where GP_Oracle_Status__c = \'S\' AND GP_Content_Version_Document_Id__c != null AND GP_Project__c != null'
            + ' AND GP_Project__r.GP_Oracle_PID__c != \'NA\' AND GP_Project__r.GP_Business_Type__c = \'PBB\' AND GP_Project__r.RecordType.Name != \'Indirect PID\''
            + ' AND (GP_Document_Creation_Date__c <=: endDate AND GP_Document_Creation_Date__c >=: startDate)';
                
        if(String.isNotBlank(businessName) && !businessName.equalsIgnoreCase('All')) {
            query +=' AND GP_Project__r.GP_Business_Name__c = \'' + businessName + '\'';
        }
        
        if(selectedStatus == 'Create') {
            String pids = '';
            
            for(String pid : mapOfApprovedProjects.Keyset()) {
                pids += '\'' + pid + '\',';
            }
            
            if(String.isNotBlank(pids)) {
                query += ' AND GP_Project__c IN  (' + pids.removeEnd(',') + ')';
            }
        }
        
        System.debug('==query=='+query);
        
        query += ' Order By GP_Project__r.GP_Oracle_PID__c';
        
        /*for(GP_Project_Version_History__c projectVersionHistory : [Select id, Name, GP_Project__r.Name, GP_Content_Version_Document_Id__c, GP_Project__r.GP_Business_Name__c,
                                                                   GP_Project__r.GP_Oracle_PID__c, GP_Oracle_Status__c, GP_Status__c, GP_Version_No__c, GP_Project__c 
                                                                   from GP_Project_Version_History__c where GP_Oracle_Status__c = 'S' and
                                                                   GP_Project__c in : mapOfApprovedProjects.Keyset() and GP_Content_Version_Document_Id__c != null
                                                                  AND GP_Document_Creation_Date__c <=:endDate AND GP_Document_Creation_Date__c >= :startDate ])*/
        
        for(GP_Project_Version_History__c projectVersionHistory : Database.query(query)) {
            project = new GP_Project__c();
            project.Id = projectVersionHistory.GP_Project__c;
            project.GP_Oracle_PID__c = projectVersionHistory.GP_Project__r.GP_Oracle_PID__c;
            project.Name = projectVersionHistory.GP_Project__r.Name;
            project.GP_Content_Version_Document_Id__c = projectVersionHistory.GP_Content_Version_Document_Id__c;
            project.GP_Project_Version_Name__c = projectVersionHistory.Name;
            project.GP_Business_Name__c = projectVersionHistory.GP_Project__r.GP_Business_Name__c;
 			listOfApprovedProjects.add(project);
            
            if(mapOfApprovedProjects.containsKey(project.Id)) {
                mapOfApprovedProjects.remove(project.Id);
            }
        }
        
        if(mapOfApprovedProjects.size() > 0) {
            listOfApprovedProjects.addAll(mapOfApprovedProjects.values());
        }
    }
    
    public GPAuraResponse getBusinessNames() {
        Schema.DescribeFieldResult businessNameField = GP_Deal__c.GP_Business_Name__c.getDescribe();
        List<Schema.PicklistEntry> businessNameValues = businessNameField.getPicklistValues();
        
        for(Schema.PicklistEntry bv : businessNameValues) {
            listOfBusinessNames.add(bv.getValue());
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfBusinessNames));
    }
    
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        System.debug('==listOfApprovedProjects for JSON=='+listOfApprovedProjects.size());
        if (listOfApprovedProjects != null) {
            gen.writeObjectField(LIST_OF_APPROVED_PROJECT_LABEL, listOfApprovedProjects);
        }
        
        gen.writeEndObject();        
    }    
}