@isTest
private class ShareWithCase_creatorTest
{
    public static testMethod void RunTest()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        
        Pre_discover__c oPre_Discover = GEN_Util_Test_Data.CreatePre_discover('Test');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        User u2 =GEN_Util_Test_Data.CreateUser('stfsdfsdfsd5@testorg.com',p.Id,'stfsdfsdfsd5@testorg.com' );
       List<CaseShare> csShareList = new List<CaseShare>();
        AccountArchetype__c TemoObj = GEN_Util_Test_Data.createNewAccountArchetype(u2.id,oAccount.id,oAT.id);
        
        Test.startTest();
        
        Case cs = new Case(Contactid=oContact.id,Accountid=oAccount.id,description='Describe case',iPitch_Normal_48_hours__c=True);
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Case' and DeveloperName = 'iPitch'];
        cs.RecordTypeId = RecType.Id;
        cs.OwnerId = u.id;
       // cs.CreatedById = u.id;
        system.runAs(u2){
        insert cs;
            cs.description = 'u.id';
        update cs;    
        }
        
   
        
        CaseShare csShare = new CaseShare(); 
        
                            csShare.CaseId = cs.id;
                            csShare.UserOrGroupId = u2.id ;
                            csShare.CaseAccessLevel = 'Read';
                            csShareList.add( csShare );
        
            //if(!csShareList.isempty())
            Insert csShareList;
        Test.stopTest();
    }
}