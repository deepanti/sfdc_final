public class teammembercomp {
    
    @AuraEnabled
    public static List<String> getpickval() {
        List<String> options = new List<String>();

        Schema.DescribeFieldResult fieldResult = AccountArchetype__c.Team_Member_Role__c.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        }       
        return options;
    }
    
    
    @AuraEnabled
    public static Account getAccount(Id accountId) {
        // Perform isAccessible() checks here
        return [SELECT Name, Archetype__c,BillingCity, BillingState,id FROM Account WHERE Id = :accountId];
    }
    
    @AuraEnabled
    public static AccountArchetype__c saveAccountTeam(AccountArchetype__c AccTeam, Id accountId) {
        // Perform isAccessible() and isUpdateable() checks here
        AccTeam.Account__c = accountId;
        upsert AccTeam;
        return AccTeam;
    }
    
    
    @AuraEnabled
    public static boolean get_profile(Id accountId) {
        // Perform isAccessible() checks here
        Account Acc = [SELECT ownerid,id FROM Account WHERE Id = :accountId limit 1];
        Id OID= Acc.ownerid;
        Id profileId=userinfo.getProfileId();
        Id UID=userinfo.getUserId();
        Boolean SL= [Select id,Sales_Leader__c from user where id = :UID limit 1].Sales_Leader__c;
         
        
        
        if(OID == UID || SL== true || profileId=='00e90000001aE3j' || profileId =='00e90000001aE3k' || profileId =='00e9000000125i2' || profileId == '00e90000001aFVI')
        {
            Return false;
        }
        else
            return true;
        
        
        
    }

}