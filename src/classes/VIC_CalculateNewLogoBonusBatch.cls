/**
* @Description: This batch class used to calculate New Logo Bonus
* @author: Bashim Khan
* @date: Jun 2018
*/

global class VIC_CalculateNewLogoBonusBatch implements Database.Batchable<sObject>, Database.stateful{  
    
    public Date firstDayOfThisYear, lastDayOfThisYear; 
    @TestVisible
    private Boolean isAfterYearCalculation = false;
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        String opportunityStagename 
            = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Eligible_Stage_For_VIC');
        String huntingAccount 
            = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Hunting_Account_Identifier');
        
        if(firstDayOfThisYear == null || lastDayOfThisYear == null){
            isAfterYearCalculation = false;
            firstDayOfThisYear = Date.newInstance(System.today().year(), 1, 1);
            lastDayOfThisYear = Date.newInstance(System.today().year(), 12, 31);
        }
        else{
            isAfterYearCalculation = true;
        }
        
        return Database.getQueryLocator([
            SELECT Id, VIC_Hunting_Start_Date__c, VIC_Hunting_End_Date__c,
            (
                SELECT Id, Actual_Close_Date__c 
                FROM Opportunities
                WHERE StageName = :opportunityStagename
                AND Actual_Close_Date__c != null
            )
            FROM Account
            WHERE Hunting_Mining__c = :huntingAccount
            AND vic_Hunting_On__c <> NULL
            AND VIC_Hunting_Start_Date__c <> NULL
            AND (VIC_Hunting_Start_Date__c >= :firstDayOfThisYear OR VIC_Hunting_End_Date__c <=:lastDayOfThisYear)
        ]);
        
        /*
        AND vic_Hunting_On__c >= :firstDayOfThisYear
            AND vic_Hunting_On__c <= :lastDayOfThisYear
        */
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accounts){
        try{
            System.debug('accounts : ' + accounts);
            VIC_CalculateNewLogoBonusBatchHelper batchHelper = new VIC_CalculateNewLogoBonusBatchHelper();
            Map<Id, Account> mapOfAccount = new Map<Id, Account>(accounts);
            Set<Id> oppIds = new Set<Id>();
            
            for(Account acc: accounts){
                for(Opportunity opp: acc.Opportunities){
                    if(opp.Actual_Close_Date__c >= acc.VIC_Hunting_Start_Date__c 
                       && opp.Actual_Close_Date__c <= acc.VIC_Hunting_End_Date__c)
                    {
                        oppIds.add(opp.Id);
                    }
                }
            }
            System.debug('=====>>oppIds11: '+ oppIds);
            //getting Map of account id and Ordered Opportunities and Map of Users in wrapper
            VIC_CalculateNewLogoBonusBatchHelper.AccountsAndUsersDetailWrapper accountsWithOrderedOpportunitiesAndUsersMap
                = batchHelper.getAccountsWithOrderedOpportunitiesAndUsersMap(oppIds);
            
            Map<Id, List<Opportunity>> accountsWithOrderedOpportunities 
                = accountsWithOrderedOpportunitiesAndUsersMap.accountsWithOrderedOpportunities;
            
            Map<Id, User> mapOfUsers = accountsWithOrderedOpportunitiesAndUsersMap.mapOfUsers;
            
            Map<Id, String> userIdVsPlanCode = batchHelper.getUsersVsPlanMap(mapOfUsers.keySet());
            Map<Id, Target_Component__c> userVsTargetComponent 
                = batchHelper.getTargetComponents(mapOfUsers.keySet(), mapOfAccount.keySet());
            
            String maxIncentiveCapStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_12_Month_50_M_Incentive_Amt');
            String halfYearIncentiveStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_6_Month_500_K_Incentive_Amt');
            String afterYearIncentiveStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_After_Year_1M_Incentive_Amt');
            String tcvHalfYearLimitStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_Six_Months_TCV_Limit');
            String tcvFullYearLimitStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_Twelve_Months_TCV_Limit');
            String tcvAfterYearLimitStr 
                = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('BDE_New_Logo_After_12_Month_TCV_Limit');
            Decimal maxIncentiveCap = VIC_GeneralChecks.isDecimal(maxIncentiveCapStr) ? Decimal.valueOf(maxIncentiveCapStr) : 0;
            Decimal halfYearIncentive = VIC_GeneralChecks.isDecimal(halfYearIncentiveStr) ? Decimal.valueOf(halfYearIncentiveStr) : 0;
            Decimal afterYearIncentive = VIC_GeneralChecks.isDecimal(afterYearIncentiveStr) ? Decimal.valueOf(afterYearIncentiveStr) : 0;
            Decimal tcvHalfYearLimit = VIC_GeneralChecks.isDecimal(tcvHalfYearLimitStr) ? Decimal.valueOf(tcvHalfYearLimitStr) : 0;
            Decimal tcvFullYearLimit = VIC_GeneralChecks.isDecimal(tcvFullYearLimitStr) ? Decimal.valueOf(tcvFullYearLimitStr) : 0;
            Decimal tcvAfterYearLimit = VIC_GeneralChecks.isDecimal(tcvAfterYearLimitStr) ? Decimal.valueOf(tcvAfterYearLimitStr) : 0;
            
            VIC_CalculateNewLogoBonusBatchHelper.IncentiveConstantWrapper incentiveConstantsWrap = 
                new VIC_CalculateNewLogoBonusBatchHelper.IncentiveConstantWrapper(maxIncentiveCap,
                                                                                  halfYearIncentive,
                                                                                  afterYearIncentive,
                                                                                  tcvHalfYearLimit,
                                                                                  tcvFullYearLimit,
                                                                                  tcvAfterYearLimit
                                                                                 );
            
            List<Target_Achievement__c> incentiveRecords = new List<Target_Achievement__c>();
            
            System.debug('RP-> accountsWithOrderedOpportunities -> ' + accountsWithOrderedOpportunities);
            System.debug('RP-> userVsTargetComponent -> ' + userVsTargetComponent);
            System.debug('RP-> userIdVsPlanCode -> ' + userIdVsPlanCode);
            System.debug('RP-> mapOfUsers -> ' + mapOfUsers);
            System.debug('RP-> isAfterYearCalculation -> ' + isAfterYearCalculation);
            System.debug('RP-> incentiveConstantsWrap -> ' + incentiveConstantsWrap);
            System.debug('RP-> accounts -> ' + accounts);
            
            //getting incentive records for each account
            for(Account acc: accounts){
                System.debug('RP-> Account -> ' + acc);
                if(accountsWithOrderedOpportunities.containsKey(acc.Id)){
                    System.debug('eligible Opprtuinity Found for account: '+ acc);
                    List<Target_Achievement__c> partOfIncentiveRecords = batchHelper.getIncentiveRecordsForAccount(acc, 
                                                                                                                   accountsWithOrderedOpportunities, 
                                                                                                                   userVsTargetComponent, 
                                                                                                                   userIdVsPlanCode, 
                                                                                                                   mapOfUsers, 
                                                                                                                   isAfterYearCalculation, 
                                                                                                                  incentiveConstantsWrap);
                    System.debug('=====>>partOfIncentiveRecords: '+ partOfIncentiveRecords);
                    if(partOfIncentiveRecords != null){
                        incentiveRecords.addAll(partOfIncentiveRecords);
                    }
                    
                }
                else{
                    System.debug('No eligible Opprtuinity Found for account: '+ acc);
                }
            }
            if(incentiveRecords != null && incentiveRecords.size() > 0){
                INSERT incentiveRecords;
            }
        }
        catch(Exception ex){
            LoggerUtility.logException('VIC_CalculateNewLogoBonusBatch', 'execute', LoggerUtility.LogLevel.EXC, 'message', 'refIds', 'refInfos', LoggerUtility.RefCategory.Batch, ex);
            LoggerUtility.commitLog();
        }
    }
    
    global void finish(Database.BatchableContext bc){
        System.debug('New Logo bonous job finished!');
         if(!Test.isRunningTest()){
              VIC_CurrencyConversionBatch  CuCvBatch = new VIC_CurrencyConversionBatch(true);
              Database.executeBatch(CuCvBatch, 2);
        }
    }
    
}