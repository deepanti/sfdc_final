global class GPBatchPidApprovalReminder implements  Database.Batchable<sObject>, Database.Stateful {
    
      date dtReminderDate ; 
	  string strLevel ; 
	  
	  global GPBatchPidApprovalReminder( integer intDays , string strLevel){
		this.dtReminderDate = system.today().addDays(-intDays);
		this.strLevel = strLevel;
	}


    global Database.QueryLocator start(Database.BatchableContext bc) {
	
        return Database.getQueryLocator( [select Name, id,GP_PID_Approver_User__c,GP_EP_Project_Number__c,GP_Oracle_PID__c,GP_Band_3_Approver_Status__c,
																GP_Temp_Approval_Status__c,GP_Project_Submission_Date__c from GP_Project__c 
																where GP_Project_Submission_Date__c =: dtReminderDate 
																AND (GP_Approval_Status__c ='Pending for Approval' 
																	OR GP_Approval_Status__c ='Pending for closure')  ]);      
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> lstProjects){
	
		map<id,List<GP_Project__c>> mapPidUsertoLstProjects = new map<id,List<GP_Project__c>>();
        Map < String, GPWrapperUserApprovalStatus > mapOfUserWiseApprovalStatus;
		set<id> setPidUsers = new set<id>(); 
        for( GP_Project__c  objProject: lstProjects){
            
            if(objProject.GP_PID_Approver_User__c != null && objProject.GP_Temp_Approval_Status__c != null && objProject.GP_Band_3_Approver_Status__c !='Pending') 
            {
                mapOfUserWiseApprovalStatus = (Map < String, GPWrapperUserApprovalStatus > ) JSON.deserialize(objProject.GP_Temp_Approval_Status__c, Map < String, GPWrapperUserApprovalStatus > .Class);
                if(mapOfUserWiseApprovalStatus.get(objProject.GP_PID_Approver_User__c) != null && mapOfUserWiseApprovalStatus.get(objProject.GP_PID_Approver_User__c).status == 'Pending For Approval'){
                    setPidUsers.add(objProject.GP_PID_Approver_User__c );
                    if( !mapPidUsertoLstProjects.containsKey(objProject.GP_PID_Approver_User__c)){
                        mapPidUsertoLstProjects.put( objProject.GP_PID_Approver_User__c,new list<GP_Project__c>());
                    }
                    mapPidUsertoLstProjects.get(objProject.GP_PID_Approver_User__c).add(objProject );
                }
            }
        }
       
		List<Messaging.SingleEmailMessage> lstEmailMails = new List<Messaging.SingleEmailMessage>();
        if(setPidUsers.size()>0){
			Id orgWideEmailId = GPCommon.getOrgWideEmailId();
			map<id,User> mapIdUser = new map<Id,User>([Select Name, Managerid ,id, email, manager.Email,Manager.Manager.email from User where id in: setPidUsers]);
			
			map<id,GP_Employee_Master__c>  mpPidusertoEmpMaster = getPidEmployee(setPidUsers);

			for(GP_Employee_Master__c objUser:  mpPidusertoEmpMaster.values()){
					
				String html = '';
				html += ' <body>';
				html += '  <table width="100%" style="font-family:Arial;font-size:13px">'; 
				html += '   <tr>'; 
				html += '    <td bgcolor="#005595" width="100%" height="30px" color="white" align="left">'; 
				html += '     <b style="color:white;margin-left:10px">SFDC-Pinnacle Notification!</b>'; 
				html += '    </td>'; 
				html += '   </tr>'; 
				html += '   <tr>'; 
				html += '    <td>'; 
				html += '     <div style="color:red;margin-left:10px;margin-top:10px">Note : Any request pending for more than '+ System.Label.GP_PID_AutoRejected_Days +' days will be auto rejected by SFDC-Pinnacle</div>'; 
				html += '  </td></tr>';
				html += '    <td>'; 
				html += '     <div style="color:#333399;margin-left:10px;margin-top:10px">Dear '+ objUser.Name+',</div>'; 
				html += '     <div style="color:#333399;margin-left:10px;margin-top:20px">The following request(s) are pending for your approval from more than';
				if(strLevel == '1'){	
					html += '		24 hours.</div>'; 
				} else if( strLevel == '2'){
					html += '		48 hours.</div>'; 
				}else if( strLevel == '3'){
					html += '		72 hours.</div>';
				}
				html += '  </td></tr><tr><td> <div style="color:#333399;margin-left:10px;margin-top:10px"> ';
				html += ' <table  width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">'; 
				html += ' 	<thead>'; 
				html += ' 		<tr>'; 
				html += ' 			<th bgcolor="#005595" style="color:white">Pinnacle Project No</th>'; 
				html += ' 			<th bgcolor="#005595" style="color:white">Oracle PID</th>'; 
				html += ' 			<th bgcolor="#005595" style="color:white">Process Name</th>'; 
				html += '			<th bgcolor="#005595" style="color:white" >Pending From</th>'; 
				html += ' 		</tr>'; 
				html += ' 	</thead>'; 
				html += ' 	<tbody>'; 
				
				for(GP_Project__c objProjects: mapPidUsertoLstProjects.get(objUser.GP_SFDC_User__c) ){
					html += ' 		<tr>'; 
					html += ' 			<td>'+objProjects.GP_EP_Project_Number__c +'</td>'; 
					html += ' 			<td>'+objProjects.GP_Oracle_PID__c+'</td>'; 
					html += ' 			<td>'+objProjects.Name+'</td>'; 
					html += ' 			<td>'+objProjects.GP_Project_Submission_Date__c+'</td>'; 
					html += ' 		</tr>'; 
				}
				html += ' 	</tbody>'; 
				html += ' </table>';
				html += '</div></td></tr><tr><td> ';
				html += '     <div style="color:#333399;margin-left:10px;">Please log on to Pinnacle-SFDC application to approve/reject the same.</div>'; 
				
				html += '    </td>'; 
				html += '   </tr>'; 
				html += '   <tr>'; 
				html += '    <td>'; 
				html += '     <div style="color:#333399;margin-left:10px;margin-top:20px">This is an Auto Generated Mail ... Please do not reply</div>'; 
				html += '    </td>'; 
				html += '   </tr>'; 
				html += '  </table>'; 
				html += ' </body>';	
			
			
				Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
				string [] lstEmailMails1 = new List<String>();
                List<String> lstCCEmailAddress = new List<String>();
				if(strLevel == '1'){
					lstEmailMails1.add(objUser.GP_OFFICIAL_EMAIL_ADDRESS__c);
				}
				else if( strLevel == '2')
				{
					if(objUser.GP_Supervisor_Employee__c != null){
						lstEmailMails1.add(objUser.GP_OFFICIAL_EMAIL_ADDRESS__c);
						lstCCEmailAddress.add(objUser.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
					 }
						//message.toAddresses= new String[]{objUser.Email ,objUser.Manager.Email};
					else
					 lstEmailMails1.add(objUser.GP_OFFICIAL_EMAIL_ADDRESS__c);
				 } 
				 else if( strLevel == '3')
				 {
					 if(objUser.GP_Supervisor_Employee__c  != null)
					 {
						  lstEmailMails1.add(objUser.GP_OFFICIAL_EMAIL_ADDRESS__c);
						  lstCCEmailAddress.add(objUser.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
						  if(objUser.GP_Supervisor_Employee__r.GP_Supervisor_Employee__c != null)
							{
								lstCCEmailAddress.add(objUser.GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
							}
					 }
					 else{
						  lstEmailMails1.add(objUser.GP_OFFICIAL_EMAIL_ADDRESS__c);
					 }
				 }
                system.debug('lstEmailMails1'+lstEmailMails1);
				message.toAddresses= lstEmailMails1;
                message.ccaddresses = lstCCEmailAddress;
				message.setUseSignature(false);
				message.setSubject('PID/Project Request Pending for Approval of '+ objUser.Name+ '- Reminder'+ strLevel);            
				message.setSaveAsActivity(false);
				message.setHtmlBody(html);
                if(orgWideEmailId != null)
                    	message.setOrgWideEmailAddressId(orgWideEmailId);
				lstEmailMails.add(message); 
		  }
		}
		
		if(lstEmailMails != null && lstEmailMails.size() > 0 && GPCommon.allowSendingMail())
        {
           Messaging.sendEmail(lstEmailMails);
        }
    }        
    
	private map<id,GP_Employee_Master__c> getPidEmployee(set<id> setPidUsers)
	{
			map<id,GP_Employee_Master__c>  mpPidusertoEmpMaster = new map<id,GP_Employee_Master__c>();
			
			for( GP_Employee_Master__c objEM :  [Select Id, Name, GP_SFDC_User__c,
												GP_OFFICIAL_EMAIL_ADDRESS__c, 
												GP_ACTUAL_TERMINATION_Date__c, GP_Supervisor_Employee__c,
												GP_Supervisor_Employee__r.GP_Supervisor_Employee__c,
												GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
												GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c
												
												From GP_Employee_Master__c 
												where GP_SFDC_User__c in : setPidUsers]){
			
					mpPidusertoEmpMaster.put(objEM.GP_SFDC_User__c,objEM);
		}
		return mpPidusertoEmpMaster;
	}

    global void finish(Database.BatchableContext bc){
      
    }    

}