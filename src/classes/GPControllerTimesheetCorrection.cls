/**
 * @group TimesheetCorrection
 * @group-content ../../ApexDocContent/TimeSheetCorrection.htm
 *
 * @description Apex controller class having business logics 
 * to fetch, insert, update and delete timesheetentries for an employee.
 */
public without sharing class GPControllerTimesheetCorrection {

    private static final String NO_TIMESHEET_FOUND_ERROR_LABEL = 'No Time Sheet Entries found For Combination entered!';
    private static final String NO_GLOBAL_SETTINGS_CONFIGURED_ERROR_LABEL = 'No Global setting For TimePeriod Found!';
    private static final String TIMESHEET_LOCKED_ERROR_LABEL = 'This TimeSheet is locked due to Approval!';
    private static final String SUCCESS_LABEL = 'SUCCESS';

    private static Map < String, Map < Id, GP_Project_Task__c >> mapOfProjectOraclePIDAndAssociatedTasks = new Map < String, Map < Id, GP_Project_Task__c >> ();
    private static Map < String, wrapperTimeSheetEntry > mapOfEmployeeTimeSheetEntryProjectWise = new Map < String, wrapperTimeSheetEntry > ();
    private static employeeProjectAllocationData objemployeeProjectAllocationData = new employeeProjectAllocationData();
    private static Map < String, GP_Project_Task__c > mapOfProjectTasks = new Map < String, GP_Project_Task__c > ();
    private static List < GP_Project__c > listOfEmployeeAssociatedProjects = new List < GP_Project__c > ();
    private static Map < String, GP_Project__c > mapOfProject = new Map < String, GP_Project__c > ();
    private static List < GP_Pinnacle_Master__c > lstOfPinnacleMasterRecord;
    private static List < wrapperTimeSheetEntry > lstWrapTimeSheetEntries;
    private static List < GP_Timesheet_Entry__c > lstOfTimeSheetEntries;
    private static GP_Timesheet_Transaction__c objTimeSheetTransaction;
    private static List < GP_Employee_Master__c > objEmployeeMaster;
    private static GP_Employee_Master__c objEmployee;
    private static String timeSheetTransactionId;
    private static Boolean isCompeletlyDisabled;
    private static List < String > monthYear;
    private static String employeeMonthYear;
    private static String monthYearLower;
    private static Integer maxDateIndex;
    private static Integer monthNumber;
    private static Integer numberDays;
    private static Integer year;

    /**
     * @description Returns Timesheet data for a given employee-month-year.
     * @param objJSONTimeSheetTransaction serialized timesheet transaction object.
     * 
     * @return GPAuraResponse json of TimeSheet Entries.
     * 
     * @example
     * GPControllerTimesheetCorrection.getTimesheetTransactions(<Serialized Timesheet Transaction Record>);
     */
    public static GPAuraResponse getTimesheetTransactions(String objJSONTimeSheetTransaction) {
        objTimeSheetTransaction = (GP_Timesheet_Transaction__c) JSON.deserialize(objJSONTimeSheetTransaction,
            GP_Timesheet_Transaction__c.class);

        objEmployeeMaster = getEmployeeMasterRecords(new Set < Id > { objTimeSheetTransaction.GP_Employee__c });
		Boolean validateHoursCheck = objEmployeeMaster[0].GP_EMPOYMENT_CATEGORY__c != null && objEmployeeMaster[0].GP_EMPOYMENT_CATEGORY__c.containsIgnoreCase('Full Time') && objEmployeeMaster[0].GP_Employee_Category__c == 'CMITS' ? true:false;
        List < GP_Timesheet_Transaction__c > lstOfTimesheetTransactions = new GPSelectorTimesheetTransaction().getTimeSheetTransactionEmployeeMonthYear(new Set < String > { objEmployeeMaster[0].GP_Person_ID__c + objTimeSheetTransaction.GP_Month_Year__c });
        //return  GPValidatorTimesheetCorrection.validateLoggedinUser(loggedInUserId);
        Boolean pinnoraSyncPending = getPinnoraSyncResultForNewCreation (lstOfTimesheetTransactions);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (lstOfTimesheetTransactions != null) {
            gen.writeObjectField('listOfTimesheetTransactionInSystem', lstOfTimesheetTransactions);
        }
        
        if(validateHoursCheck != null){
            gen.writeObjectField('validateHoursCheck',validateHoursCheck);
        }
        
        if (pinnoraSyncPending != null) {
            gen.writeObjectField('pinnoraSyncPending', pinnoraSyncPending);
        }

        gen.writeEndObject();
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }
	
    private static Boolean getPinnoraSyncResultForNewCreation(List < GP_Timesheet_Transaction__c > lstOfTimesheetTransactions){
        Boolean isPinnoraSyncPending = false;
        String empPersonIdVsMonthYearPending;
        String transactionNumber;
        for(GP_Timesheet_Transaction__c timesheetTransactionRecord : lstOfTimesheetTransactions){
            if(timesheetTransactionRecord.GP_Oracle_Status__c == 'S'){
                empPersonIdVsMonthYearPending = timesheetTransactionRecord.GP_Employee_Month_Year_Unique__c;
                transactionNumber = timesheetTransactionRecord.Name;
                break;
            }
        }
        if(empPersonIdVsMonthYearPending != null && empPersonIdVsMonthYearPending != ''){
            for(GP_Timesheet__c timesheet : new GPSelectorTimeSheet().selectByEmployeeMonthYearUniqueField(new Set<String>{empPersonIdVsMonthYearPending})){
                if(timesheet.GP_Orig_Transaction_Reference__c != null && timesheet.GP_Orig_Transaction_Reference__c.contains(transactionNumber)){
                    isPinnoraSyncPending = false;
                    break;
                }
            }
        }
        return isPinnoraSyncPending;
    }

    /**
     * @description Returns Timesheet data for a given employee-month-year.
     * @param objJSONTimeSheetTransaction serialized timesheet transaction object.
     * 
     * @return GPAuraResponse json of TimeSheet Entries.
     * 
     * @example
     * GPControllerTimesheetCorrection.getTimeSheetEntries(<Serialized Timesheet Transaction Record>);
     */
    public static GPAuraResponse getTimeSheetEntries(String objJSONTimeSheetTransaction) {
        objTimeSheetTransaction = (GP_Timesheet_Transaction__c) JSON.deserialize(objJSONTimeSheetTransaction,
            GP_Timesheet_Transaction__c.class);
        objEmployeeMaster = getEmployeeMasterRecords(new Set < Id > { objTimeSheetTransaction.GP_Employee__c });

        setMonthYearRelatedValues();

        String returnVal = setIsCompeletlyDisabledOrMaxDateIndex();

        if (validateWithGlobalSettingRecord(returnVal))
            return new GPAuraResponse(false, NO_GLOBAL_SETTINGS_CONFIGURED_ERROR_LABEL, null);

        year = Integer.ValueOf(String.ValueOf(system.today().Year()).substring(0, 2) + monthYear[1].substring(monthYear[1].length() - 2));

        if (objTimeSheetTransaction.Id == null || (objTimeSheetTransaction.Id != null && objTimeSheetTransaction.GP_Approval_Status__c == 'Draft'))
            lstOfTimeSheetEntries = GPHelperTimesheet.getAllTimeSheetEntries(new Set < String > { employeeMonthYear }, mapOfProject,
                mapOfProjectTasks); // GPCalloutHandlerTimesheetCorrection.getTimeSheetEntries(lstOfTimeSheetTransactions[0].id,GPCommon.mapOfMonthNameToNumber.get(monthYear[0]));
        else {
            lstOfTimeSheetEntries = GPHelperTimesheet.getAllTimeSheetEntriesInSFDC(mapOfProject,
                mapOfProjectTasks, objTimeSheetTransaction);
        }
        //if (!(lstOfTimeSheetEntries.size() > 0))
        //    return new GPAuraResponse(false, NO_TIMESHEET_FOUND_ERROR_LABEL, null);

        if (lstOfTimeSheetEntries.size() > 0) {
            timeSheetTransactionId = lstOfTimeSheetEntries[0].GP_Timesheet_Transaction__c;

            /*if (timeSheetTransactionId != null && Approval.isLocked(timeSheetTransactionId))
                return new GPAuraResponse(false, TIMESHEET_LOCKED_ERROR_LABEL, null);*/
            setmapOfEmployeeTimeSheetEntryPojectWise();

            if (mapOfEmployeeTimeSheetEntryProjectWise.size() > 0) {
                setemployeeProjectAllocationData();
                return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(objemployeeProjectAllocationData, true));
            }
        } else {
            setemployeeProjectAllocationDataDefaultRow();
            return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(objemployeeProjectAllocationData, true));
        }

        return new GPAuraResponse(false, NO_TIMESHEET_FOUND_ERROR_LABEL, null);
    }

    /**
     * @description set month year related values of class members.
     * 
     */
    private static void setMonthYearRelatedValues() {
        monthYearLower = (objTimeSheetTransaction.GP_Month_Year__c);
        monthYear = ((monthYearLower).trim()).split('-');
        monthNumber = GPCommon.mapOfMonthNameToNumber.get(monthYear[0]);
        year = Integer.ValueOf(String.ValueOf(system.today().Year()).substring(0, 2) + monthYear[1].substring(monthYear[1].length() - 2));
        numberDays = Date.daysInMonth(year, monthNumber);
        employeeMonthYear = objEmployeeMaster[0].GP_Person_ID__c + (objTimeSheetTransaction.GP_Month_Year__c);
    }

    /**
     * @description set wrapper instance of employeeProjectAllocationData that will be returned.
     * 
     */
    private static void setemployeeProjectAllocationData() {

        objemployeeProjectAllocationData.wrapperTimeSheetEntries = mapOfEmployeeTimeSheetEntryProjectWise.values();
        objemployeeProjectAllocationData.isCompeletlyDisabled = isCompeletlyDisabled;
        objemployeeProjectAllocationData.maxDateIndex = maxDateIndex;
        objemployeeProjectAllocationData.employeeId = objTimeSheetTransaction.GP_Employee__c;
        objemployeeProjectAllocationData.timeSheetTransactionId = timeSheetTransactionId;

        setMinAndMaxHours(objTimeSheetTransaction.GP_Employee__c);

        setResourceAllocationEntriesToObject(objTimeSheetTransaction.GP_Employee__c);

        setlistOfEmployeeHRInactiveRecords();
    }
    /**
     * @description add default row.
     * 
     */
    private static void setemployeeProjectAllocationDataDefaultRow() {

        objemployeeProjectAllocationData.wrapperTimeSheetEntries = getTimesheetEntryDefaultRow(); //mapOfEmployeeTimeSheetEntryPojectWise.values();
        objemployeeProjectAllocationData.isCompeletlyDisabled = isCompeletlyDisabled;
        objemployeeProjectAllocationData.maxDateIndex = maxDateIndex;
        objemployeeProjectAllocationData.employeeId = objTimeSheetTransaction.GP_Employee__c;
        objemployeeProjectAllocationData.timeSheetTransactionId = timeSheetTransactionId;

        setMinAndMaxHours(objTimeSheetTransaction.GP_Employee__c);

        setResourceAllocationEntriesToObject(objTimeSheetTransaction.GP_Employee__c);

        setlistOfEmployeeHRInactiveRecords();
    }
    /**
     * @description add default row of timesheet Entry.
     * 
     */
    private static List < wrapperTimeSheetEntry > getTimesheetEntryDefaultRow() {
        //GP_Timesheet_Entry__c objTimeSheetEntryRecord = new GP_Timesheet_Entry__c(GP_Project__c = null, GP_Project_Task__c = null, GP_Ex_Type__c = null);
        GP_Timesheet_Entry__c objTimeSheetEntryRecord = new GP_Timesheet_Entry__c(GP_Project_Oracle_PID__c = null, GP_Project_Task_Oracle_Id__c = null, GP_Ex_Type__c = null);
        wrapperTimeSheetEntry defaultRowTimeSheetEntry = new wrapperTimeSheetEntry(numberDays, objTimeSheetEntryRecord, null, objTimeSheetTransaction.GP_Employee__c, year, monthNumber);
        defaultRowTimeSheetEntry.newRecord = false;
        defaultRowTimeSheetEntry.isDefault = true;
        return new List < wrapperTimeSheetEntry > { defaultRowTimeSheetEntry };
    }

    /**
     * @description query resource allocation entries for the employee for the month entered.
     * 
     */
    public static List < GP_Resource_Allocation__c > getResourceAllocationEntries(String employeeId) {

        Date monthStartDate = date.newinstance(year, monthNumber, 1);
        Date monthEndDate = monthStartDate.addMonths(1).addDays(-1);
        return new GPSelectorResourceAllocation().selectResourceAllocationRecordsForAMonth(employeeId, monthStartDate, monthEndDate);
    }

    /**
     * @description set map of resource allocation entries for the employee to validate it on the screen.
     * 
     */
    private static void setResourceAllocationEntriesToObject(String empId) {
        List < GP_Resource_Allocation__c > lstOfResourceAllocation = getResourceAllocationEntries(empId);
        system.debug('lstOfResourceAllocation' + lstOfResourceAllocation);
        if (lstOfResourceAllocation.size() > 0) {
            Set < String > setOfEmployeeAssociatedProjectOrclePIds = new Set < String > ();
            Map < String, List < GP_Resource_Allocation__c >> mapOfProjectWiseResourceAllocations = new Map < String, List < GP_Resource_Allocation__c >> ();

            for (GP_Resource_Allocation__c objResourceAllocation: lstOfResourceAllocation) {
                setOfEmployeeAssociatedProjectOrclePIds.add(objResourceAllocation.GP_Project__r.GP_Oracle_PID__c);

                if (mapOfProjectWiseResourceAllocations.containsKey(objResourceAllocation.GP_Project__r.GP_Oracle_PID__c)) {
                    (mapOfProjectWiseResourceAllocations.get(objResourceAllocation.GP_Project__r.GP_Oracle_PID__c)).add(objResourceAllocation);
                } else {
                    listOfEmployeeAssociatedProjects.add(objResourceAllocation.GP_Project__r);
                    mapOfProjectWiseResourceAllocations.put(objResourceAllocation.GP_Project__r.GP_Oracle_PID__c, new List < GP_Resource_Allocation__c > { objResourceAllocation });
                }
            }

            if (setOfEmployeeAssociatedProjectOrclePIds.size() > 0) {
                objEmployeeProjectAllocationData.setOfEmployeeAssociatedProjectId = setOfEmployeeAssociatedProjectOrclePIds;
                objEmployeeProjectAllocationData.mapOfwrapperResourceAllocation = mapOfProjectWiseResourceAllocations;
                objEmployeeProjectAllocationData.listOfEmployeeAssociatedProjects = listOfEmployeeAssociatedProjects;
                setmapOfProjectAndAssociatedTasks(setOfEmployeeAssociatedProjectOrclePIds);
                objEmployeeProjectAllocationData.mapOfProjectAndAssociatedTasks = mapOfProjectOraclePIDAndAssociatedTasks;
            }
        }
    }

    /**
     * @description set map of Project and Associated Tasks.
     * 
     */
    private static void setmapOfProjectAndAssociatedTasks(Set < String > setOfEmployeeAssociatedProjectOrclePIds) {
        Date monthStartDate = date.newinstance(year, monthNumber, 1);
        for (GP_Project_Task__c projectTask: new GPSelectorProjectTask().getListOfProjectTaskForSetOfProjectIds(setOfEmployeeAssociatedProjectOrclePIds, monthStartDate)) {
            if (!mapOfProjectOraclePIDAndAssociatedTasks.containsKey(projectTask.GP_Project_Number__c)) {
                mapOfProjectOraclePIDAndAssociatedTasks.put(projectTask.GP_Project_Number__c, new Map < Id, GP_Project_Task__c > { projectTask.Id => projectTask });
            }
            mapOfProjectOraclePIDAndAssociatedTasks.get(projectTask.GP_Project_Number__c).put(projectTask.Id, projectTask);
        }
    }

    /**
     * @description save timesheet entries.
     * 
     */
    public static GPAuraResponse saveTimeSheetEntries(String lstJSONTimeSheetEntries, Boolean isForApproval, String monthYearValue, String timeSheetTransactionId, String employeeId) {
        Savepoint sp = Database.setSavepoint();
        GP_Timesheet_Transaction__c timeSheetTransaction = new GP_Timesheet_Transaction__c();
        lstWrapTimeSheetEntries = (List < wrapperTimeSheetEntry > ) JSON.deserialize(lstJSONTimeSheetEntries, List < wrapperTimeSheetEntry > .class);

        monthYear = monthYearValue.split('-');
        List < GP_Timesheet_Entry__c > timeSheetEntriesToBeUpserted = new List < GP_Timesheet_Entry__c > ();
        Set < String > setOfProjectOraclePIds = new Set < String > ();

        try {
            for (wrapperTimeSheetEntry objWrapTimeSheetEntries: lstWrapTimeSheetEntries) {
                integer countDay = 1;
                for (GP_Timesheet_Entry__c objTimeSheetEntry: objWrapTimeSheetEntries.lstOfTimeSheetEntries) {
                    if (objTimeSheetEntry.Id != null || (objTimeSheetEntry.GP_Modified_Hours__c != null && objTimeSheetEntry.GP_Modified_Hours__c > 0) || (objTimeSheetEntry.GP_Actual_Hours__c > 0 && objTimeSheetEntry.GP_Modified_Hours__c >= 0)) {
                        setTimeSheetEntry(objTimeSheetEntry, objWrapTimeSheetEntries, countDay);
                        objTimeSheetEntry.GP_Employee__c = employeeId;
                        if (objTimeSheetEntry.GP_Timesheet_Transaction__c == null)
                            objTimeSheetEntry.GP_Timesheet_Transaction__c = objWrapTimeSheetEntries.timeSheetTransactionId != null ?
                            objWrapTimeSheetEntries.timeSheetTransactionId : timeSheetTransactionId;
                        setOfProjectOraclePIds.add(objWrapTimeSheetEntries.projectId);
                        timeSheetEntriesToBeUpserted.add(objTimeSheetEntry);
                    }
                    countDay++;
                }
            }
            if (timeSheetEntriesToBeUpserted.size() > 0) {
                try {
                    if (timeSheetTransactionId == null) {
                        objTimeSheetTransaction = new GP_Timesheet_Transaction__c();
                        objTimeSheetTransaction.GP_Month_Year__c = monthYearValue;
                        objTimeSheetTransaction.GP_Employee__c = employeeId;
                        insert objTimeSheetTransaction;
                        timeSheetTransactionId = objTimeSheetTransaction.id;
                        for (GP_Timesheet_Entry__c timesheetEntry: timeSheetEntriesToBeUpserted) {
                            timesheetEntry.GP_Timesheet_Transaction__c = timeSheetTransactionId;
                        }
                    } else {
                        objTimeSheetTransaction = [select id, GP_Month_Year__c, GP_Employee__c from GP_Timesheet_Transaction__c where id =: timeSheetTransactionId];
                    }
                } catch (Exception Ex) {
                    return new GPAuraResponse(false, Ex.getMessage(), null);
                }
                upsert timeSheetEntriesToBeUpserted;
                if (isForApproval) {
                    Boolean isApprovalRequired = GPCommon.getisApprovalRequired(setOfProjectOraclePIds,
                        GPCommon.mapOfMonthNameToNumber.get(monthYear[0]),
                        Integer.ValueOf(monthYear[1]));
                    if (isApprovalRequired) {
                        objTimeSheetTransaction = GPCommon.processApproval(timeSheetEntriesToBeUpserted, timeSheetTransactionId);
                        upsert objTimeSheetTransaction;
                    } else {
                        objTimeSheetTransaction.Id = timeSheetTransactionId;
                        objTimeSheetTransaction.GP_Approval_Status__c = 'Approved';
                        upsert objTimeSheetTransaction;
                    }

                }
            } else {
                return new GPAuraResponse(false, 'No Record was created as all modified hours were set to zero.', null);
            }
            return getTimeSheetEntries(JSON.serialize(objTimeSheetTransaction));
        } catch (Exception Ex) {
            system.debug('Ex' + Ex);
            Database.rollback(sp);
            return new GPAuraResponse(false, Ex.getMessage(), null);
        }
    }

    /**
     * @description set timesheet entry object with values.
     * 
     */
    private static void setTimeSheetEntry(GP_Timesheet_Entry__c objTimeSheetEntry, wrapperTimeSheetEntry objWrapTimeSheetEntries, Integer countDay) {
        objTimeSheetEntry.GP_Date__c = Date.newInstance(objTimeSheetEntry.GP_Date__c.Year(), objTimeSheetEntry.GP_Date__c.Month(), countDay);
        //objTimeSheetEntry.GP_Project_Task__c = objWrapTimeSheetEntries.taskId;
        objTimeSheetEntry.GP_Project_Task_Oracle_Id__c = objWrapTimeSheetEntries.taskId;
        //objTimeSheetEntry.GP_Project__c = objWrapTimeSheetEntries.projectId;
        objTimeSheetEntry.GP_Project_Oracle_PID__c = objWrapTimeSheetEntries.projectId;
        objTimeSheetEntry.GP_Ex_Type__c = objWrapTimeSheetEntries.exType;
        //objTimeSheetEntry.GP_Project_Task__r = null;
        objTimeSheetEntry.GP_Employee__r = null;
        //objTimeSheetEntry.GP_Project__r = null;
    }

    /**
     * @description set max and min hour range to validate on screen for particular type of an employee.
     * 
     */
    private static void setMinAndMaxHours(Id employeeId) {

        objEmployee = new GPSelectorEmployeeMaster().selectEmployeeMasterRecord(employeeId);
        if (lstOfPinnacleMasterRecord.size() > 0) {
            if (objEmployee.GP_EMPLOYEE_TYPE__c != null && objEmployee.GP_EMPLOYEE_TYPE__c.EqualsIgnoreCase('Contractor')) {
                objemployeeProjectAllocationData.minHours = lstOfPinnacleMasterRecord[0].GP_Contractor_Emp_Min_Hrs__c;
                objemployeeProjectAllocationData.maxHours = lstOfPinnacleMasterRecord[0].GP_Contractor_Emp_Max_Hrs__c;
            } else {
                objemployeeProjectAllocationData.minHours = lstOfPinnacleMasterRecord[0].GP_Full_Time_Emp_Min_Hrs__c;
                objemployeeProjectAllocationData.maxHours = lstOfPinnacleMasterRecord[0].GP_Full_Time_Emp_Max_Hrs__c;
            }
        } else {
            objemployeeProjectAllocationData.minHours = 8;
            objemployeeProjectAllocationData.maxHours = 24;
        }
    }

    /**
     * @description set date range for which timesheet have been synced to oracle and cannot be edited in pinnacle.
     * 
     */
    private static String setIsCompeletlyDisabledOrMaxDateIndex() {
        String returnString = 'false@@-1';
        lstOfPinnacleMasterRecord = GPSelectorPinnacleMasters.selectGlobalSettingRecord();

        Date monthStartDate = Date.newInstance(year, monthNumber, 1);
        Date monthEndDate = Date.newInstance(year, monthNumber + 1, 1).addDays(-1);

        if (lstOfPinnacleMasterRecord.size() > 0) {
            if (lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c <= monthStartDate &&
                lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c < monthEndDate)
                returnString = 'true@@1'; //compeletly disabled 
            else if (lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c > monthStartDate &&
                lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c >= monthEndDate)
                returnString = 'false@@-2'; //compeletly enabled
            else if (lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c >= monthStartDate &&
                lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c < monthEndDate)
                returnString = 'false@@' + lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c.Day(); //partially enabled
        }

        return returnString;
    }

    /**
     * @description set employee inactive HR records for the month.
     * 
     */
    private static void setlistOfEmployeeHRInactiveRecords() {
        Date startDate = Date.newInstance(year, monthNumber, 1);
        Date endDate = startDate.addMonths(1).addDays(-1);
        List < GP_Employee_HR_History__c > lstOfInactiveEmployeeHRRecords = GPSelectorEmployeeHR.selectInactiveRecordsForAMonthYear(objEmployee.id,
            startDate, endDate);
        objemployeeProjectAllocationData.listOfEmployeeHRInactiveRecords = lstOfInactiveEmployeeHRRecords;
    }

    /**
     * @description query employee master records.
     * 
     */
    private static List < GP_Employee_Master__c > getEmployeeMasterRecords(Set < Id > setOfEmployeeIds) {
        return new GPSelectorEmployeeMaster().selectEmployeeMasterRecords(setOfEmployeeIds);
    }

    /**
     * @description validate global setting record for timesheet lock period.
     * 
     */
    private static Boolean validateWithGlobalSettingRecord(String returnVal) {
        List < String > returnValueSplit = returnVal.split('@@');
        isCompeletlyDisabled = Boolean.valueOf(returnValueSplit[0]);
        maxDateIndex = Integer.valueOf(returnValueSplit[1]);
        return (!isCompeletlyDisabled && maxDateIndex == -1);
    }

    /**
     * @description set map of employee to its project with timesheet entries.
     * 
     */
    private static void setmapOfEmployeeTimeSheetEntryPojectWise() {
        for (GP_Timesheet_Entry__c objTimeSheetEntry: lstOfTimeSheetEntries) {
            if (timeSheetTransactionId == null && objTimeSheetEntry.GP_Timesheet_Transaction__c != null)
                timeSheetTransactionId = objTimeSheetEntry.GP_Timesheet_Transaction__c;

            wrapperTimeSheetEntry objWrapperTimeSheetEntry;
            String key = objTimeSheetEntry.GP_Project_Oracle_PID__c + '@@';
            key += objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + '@@';
            key += objTimeSheetEntry.GP_Ex_Type__c;

            if (mapOfEmployeeTimeSheetEntryProjectWise.containsKey(key))
                objWrapperTimeSheetEntry = mapOfEmployeeTimeSheetEntryProjectWise.get(key);
            else {
                objWrapperTimeSheetEntry = new wrapperTimeSheetEntry(numberDays, objTimeSheetEntry,
                    timeSheetTransactionId,
                    objTimeSheetTransaction.GP_Employee__c,
                    year,
                    monthNumber);
                objWrapperTimeSheetEntry.projectName = objTimeSheetEntry.GP_Project_Oracle_PID__c + '--' + mapOfProject.get(objTimeSheetEntry.GP_Project_Oracle_PID__c).Name;
                objWrapperTimeSheetEntry.projectRecordTypeName = mapOfProject.get(objTimeSheetEntry.GP_Project_Oracle_PID__c).RecordType.Name; //objTimeSheetEntry.GP_Project__r.RecordType.Name;
                objWrapperTimeSheetEntry.projectId = objTimeSheetEntry.GP_Project_Oracle_PID__c; //objTimeSheetEntry.GP_Project__c != null ?
                //objTimeSheetEntry.GP_Project__c : objTimeSheetEntry.GP_Project__r.Id;

                objWrapperTimeSheetEntry.taskName = mapOfProjectTasks.get(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c).Name; //objTimeSheetEntry.GP_Project_Task__r.Name;

                objWrapperTimeSheetEntry.taskId = objTimeSheetEntry.GP_Project_Task_Oracle_Id__c; //objTimeSheetEntry.GP_Project_Task__c != null ?
                //objTimeSheetEntry.GP_Project_Task__c : objTimeSheetEntry.GP_Project_Task__r.Id;

                //to do....
                objWrapperTimeSheetEntry.projectTask = mapOfProjectTasks.get(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c); //objTimeSheetEntry.GP_Project_Task__r;

                objWrapperTimeSheetEntry.exType = objTimeSheetEntry.GP_Ex_Type__c;
                objWrapperTimeSheetEntry.newRecord = false;
                objWrapperTimeSheetEntry.isDefault = false;
                objWrapperTimeSheetEntry.timeSheetTransactionId = timeSheetTransactionId;

            }

            Integer index = (objTimeSheetEntry.GP_Date__c).day();
            objWrapperTimeSheetEntry.lstOfTimeSheetEntries[index - 1] = objTimeSheetEntry;

            objWrapperTimeSheetEntry.lstOfTimeSheetEntries[index - 1].GP_Modified_Hours__c = objTimeSheetEntry.GP_Modified_Hours__c != null ?
                objTimeSheetEntry.GP_Modified_Hours__c : objTimeSheetEntry.GP_Actual_Hours__c;

            mapOfEmployeeTimeSheetEntryProjectWise.put(key, objWrapperTimeSheetEntry);

        }
    }

    /**
     * @description get job recordId for Mass upload timesheet.
     * 
     */
    public static GPAuraResponse getJobId() {
        GPSelectorJob selector = new GPSelectorJob();
        list < GP_job__C > lstOfJobIfExist = new GPSelectorJob().selectJobRecordsWithoutChildRecords('Upload Time Sheet Entries');
        GP_job__C objJob;

        if (lstOfJobIfExist != null && lstOfJobIfExist.size() > 0) {
            objJob = lstOfJobIfExist[0];
        } else {
            objJob = new GP_job__C();
            objJob.GP_Job_Type__c = 'Upload Time Sheet Entries';
            objJob.GP_Column_s_To_Update_Upload__c = 'GP_Project__c;GP_TSC_Ex_Type__c;GP_PL_Employee_OHR__c;GP_TSC_Modified_Hours__c;GP_TSC_Project_Task__c;GP_TSC_Date__c';
            objJob.OwnerId = userInfo.getUserId();
            insert objJob;
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(objJob, true));
    }

    public GPAuraResponse timesheetStatusChange(Id timesheetTransactionId, String strComment, String strStatus) {

        SavePoint s = database.setSavepoint();
        try {
            List < Approval.ProcessWorkitemRequest > lstRequests = GPCommon.getRequestList(timesheetTransactionId, strStatus, strComment);
            if (lstRequests != null && lstRequests.size() > 0) {
                GP_Timesheet_Transaction__c timeSheetRecord = [select Id, GP_Approval_Status__c from GP_Timesheet_Transaction__c where Id =: timesheetTransactionId];
                Approval.ProcessResult[] processResults = Approval.process(lstRequests);
                return new GPAuraResponse(true, JSON.serialize(timeSheetRecord), null);
            }
            return new GPAuraResponse(false, 'No Approvals Found.', null);
        } catch (exception ex) {
            database.rollback(s);
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

    }
    
    public GPAuraResponse recallApprovalProcess(Id timesheetTransactionId) {

        try {

            list < ProcessInstanceWorkitem > lstPendingWorkItems = getPendingWorkItems(timesheetTransactionId);
            list < Approval.ProcessWorkItemRequest > lstWorkItems = new list < Approval.ProcessWorkItemRequest > ();
            for (ProcessInstanceWorkitem objIntaceWorkitems: lstPendingWorkItems) {
                Approval.ProcessWorkItemRequest req = new Approval.ProcessWorkItemRequest();
                req.setWorkItemId(objIntaceWorkitems.Id);
                req.setAction('Removed'); // This means to remove/recall Approval Request
                req.setComments('withdraw by submitter.');
                lstWorkItems.add(req);

            }
            if (lstWorkItems.size() > 0) {

                list < Approval.ProcessResult > results = Approval.process(lstWorkItems);
                GP_Timesheet_Transaction__c objTimeSheetTransaction = new GP_Timesheet_Transaction__c();
				 objTimeSheetTransaction.Id = timeSheetTransactionId;
                        objTimeSheetTransaction.GP_Approval_Status__c = 'Draft';
                        upsert objTimeSheetTransaction;
               return new GPAuraResponse(true, JSON.serialize(objTimeSheetTransaction), null);
            }
        } catch (exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, 'Success', null);
    }
	private list < ProcessInstanceWorkitem > getPendingWorkItems(Id timesheetTransactionId) {
        return [Select p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId, p.ProcessInstanceId, p.OriginalActorId, p.Id, p.ActorId, p.CreatedDate
            From ProcessInstanceWorkitem p where ProcessInstance.Status = 'Pending'
            AND ProcessInstance.TargetObjectId =: timesheetTransactionId
        ];


    }
}