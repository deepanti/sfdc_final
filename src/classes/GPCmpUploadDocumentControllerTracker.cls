//================================================================================================================
//  Description: Test Class for GPCmpUploadDocumentController
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPCmpUploadDocumentControllerTracker {
    
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_address__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;
        
        //GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        //insert exceptionemp;
        
        //GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        //empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        //insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        insert prjobj;
        
        customerObj = new GP_Customer_Master__c();
        customerObj.Name = 'New Customer';
        customerObj.GP_Oracle_Customer_Id__c = '1234';
        customerObj.GP_Is_Dummy__c = true;
        insert customerObj;
        
        GP_Pinnacle_Master__c pinnacleObj = GPCommonTracker.GetpinnacleMaster();
        pinnacleObj.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        pinnacleObj.GP_Oracle_Id__c = '1234';
        insert pinnacleObj;
        
        GP_Address__c objaddress = new GP_Address__c();
        objaddress.GP_Address_Line_1__c = 'New Delhi';
        objaddress.GP_Billing_Entity_Id__c ='1234';
        objaddress.GP_Customer_Account_ID__c = '1234';
        insert objaddress;
        
        GP_Project_Address__c objPrjAddress2 = new GP_Project_Address__c();
        objPrjAddress2.GP_Project__c = prjobj.id;
        objPrjAddress2.GP_Relationship__c = 'Direct';
        objPrjAddress2.GP_Customer_Name__c = customerObj.id;
        insert objPrjAddress2;
        
        GP_Project_Address__c objPrjAddress = new GP_Project_Address__c();
        objPrjAddress.GP_Project__c = prjobj.id;
        objPrjAddress.GP_Parent_Project_Address__c = objPrjAddress2.id;
        insert objPrjAddress;
        Update objPrjAddress;
    }
    
    public static testmethod void testGPCmpUploadDocumentController() 
    {
        buildDependencyData();
        List < staticResource > ListOfTemplate = [SELECT id, Body, Name
                                                  FROM staticResource
                                                  WHERE Name = 'baseProjectTemplate'
                                                  OR Name = 'FinalProjectTemplate'
                                                  OR Name = 'StageWiseField'
                                                 ];
        staticResource baseTemplateData = GPCommonTracker.getStaticResourceByName('baseProjectTemplate', ListOfTemplate);
        String base64ConvertedRecord = EncodingUtil.base64Encode(baseTemplateData.Body);
        try{
        GPCmpUploadDocumentController.saveChunk(prjobj.id,'test',base64ConvertedRecord,'Text','');
        GPCmpUploadDocumentController.appendToFile(prjobj.id, base64ConvertedRecord);
        }
        catch(Exception e){}
    }
}