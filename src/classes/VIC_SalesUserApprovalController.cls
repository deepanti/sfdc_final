public class VIC_SalesUserApprovalController {
    
    public static List<ID> lstUserID = new List<ID>();
   
    
    @AuraEnabled 
    public static list<OpportunityWrapper> getOpportunityPending(){
        
        /*List<User> lstUser = [SELECT Phone, Id FROM User WHERE Id = : UserInfo.getUserId()];               
        for(User eachUser : lstUser){
            lstUserID.add(eachUser.ID);                                             
        } */   
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        Date processingYearEnd =  processingYearStart.addYears(1).addDays(-1); 
        
        Map<Id,Integer> mapOpp = new map<Id,Integer>();
        Map<Id,List<OpportunityLIWrapper>> oppAndLIMap = new Map<Id,List<OpportunityLIWrapper>>();
        List<OpportunityLIWrapper> opportunityLIWrapperObj = new List<OpportunityLIWrapper>();
        list<opportunity> oppListPending = new list<opportunity>();
        for(opportunity opplst :[SELECT Id,CurrencyIsoCode,VIC_Cumulative_TCV_CPQ__c, Account.Name,Annuity_Project__c,Name,Description,Type,Total_TCV_of_All_OLI2__c,Opportunity_ID__c,TCV1__c,Margin__c,VIC_Total_Ebit_Of_Each_OLI__c,
                                (SELECT OpportunityId, vic_Primary_User__c, Product_BD_Rep__c, vic_VIC_User_3__c,
                                vic_VIC_User_4__c,Product2Id, Name,TCV__c,Y1_TCV_USD_OLI_CPQ__c,EBIT_OLI_CPQ__c,Product2.Name,
                                Y2_TCV_USD_OLI_CPQ__c,Y3_TCV_USD_OLI_CPQ__c, Y4_TCV_USD_OLI_CPQ__c,
                                Y5_TCV_USD_OLI_CPQ__c, Y6_TCV_USD_OLI_CPQ__c, Y7_TCV_USD_OLI_CPQ__c,
                                Y8_TCV_USD_OLI_CPQ__c, Y9_TCV_USD_OLI_CPQ__c, Y10_TCV_USD_OLI_CPQ__c,Opportunity.ownerID,
                                Y1_AOI_USD_OLI_CPQ__c, Y2_AOI_USD_OLI_CPQ__c,Y3_AOI_USD_OLI_CPQ__c,
                                Y4_AOI_USD_OLI_CPQ__c, Y5_AOI_USD_OLI_CPQ__c,Y6_AOI_USD_OLI_CPQ__c,vic_Primary_Sales_Rep_Split__c,
                                Y7_AOI_USD_OLI_CPQ__c,Y8_AOI_USD_OLI_CPQ__c, Y9_AOI_USD_OLI_CPQ__c,vic_Product_BD_Rep_Split__c,
                                vic_Primary_Sales_Rep_Case__c,vic_Primary_Sales_Rep_Case__r.Description,
                                vic_Product_BD_Rep_Case__c,vic_Product_BD_Rep_Case__r.Description,
                                Nature_of_Work__c,vic_IO_TS__c,vic_Exchange_Rate__c,VIC_TOTAL_EBIT__c,
                                vic_Old_TCV_Spilt_For_Product_BD_Rep__c,vic_Old_TCV_Spilt_For_Sales_Rep__c,
                                vic_Old_TCV_Spilt_For_User_3__c,vic_Old_TCV_Spilt_For_User_4__c,Pricing_Deal_Type_OLI_CPQ__c,
                                vic_Primary_Sales_Rep_Split_TCV__c,vic_Product_BD_Rep_Split_TCV__c,
                                Y10_AOI_USD_OLI_CPQ__c,vic_Sales_Rep_Approval_Status__c ,vic_Product_BD_Rep_Approval_Status__c,CurrencyIsoCode,VIC_ProductShortName__c  
                                FROM OpportunityLineItems
                                where (((vic_Sales_Rep_Approval_Status__c ='Pending' OR vic_Sales_Rep_Approval_Status__c ='On-Hold') 
                                          AND (Opportunity.ownerID =:UserInfo.getUserId())) OR ((vic_Product_BD_Rep_Approval_Status__c ='Pending' 
                                          OR vic_Product_BD_Rep_Approval_Status__c ='On-Hold') AND (Product_BD_Rep__c =:UserInfo.getUserId())))
                                /*where (Opportunity.ownerID =:UserInfo.getUserId() OR Product_BD_Rep__c =:UserInfo.getUserId())
                                AND (vic_Sales_Rep_Approval_Status__c ='Pending' OR vic_Sales_Rep_Approval_Status__c ='On-Hold' OR vic_Product_BD_Rep_Approval_Status__c ='Pending' 
                                      OR vic_Product_BD_Rep_Approval_Status__c ='On-Hold') */
                                AND vic_Final_Data_Received_From_CPQ__c=True)  
                                FROM Opportunity 
                                where StageName ='6. Signed Deal'
                                AND Actual_Close_Date__c >=:processingYearStart
                                AND Actual_Close_Date__c <=:processingYearEnd]){
                                                                                                                                                          
                                    
                                    // system.debug('opplst==========='+opplst);                          
                                      if(opplst.OpportunityLineItems !=null && opplst.OpportunityLineItems.size() >0){
                                          
                                          for(OpportunityLineItem oppItem :opplst.OpportunityLineItems){
                                              //system.debug('oppItem======-'+oppItem);
                                              if(oppAndLIMap.containskey(opplst.Id)){
                                                  List<OpportunityLIWrapper> oppLIObj1 = oppAndLIMap.get(opplst.Id);
                                                  //changes by Dheeraj Chawla on 05-09-2018 for GV-307
                                                  //Total TCV sum of pending OLI.
                                                  //oppLIObj1.total_TCV += oppItem.TCV__c;
                                                  oppLIObj1.add(new OpportunityLIWrapper(oppItem));
                                                  oppAndLIMap.put(opplst.Id,oppLIObj1);
                                                  
                                              }else{
                                                  List<OpportunityLIWrapper> oppLIObj2 = new List<OpportunityLIWrapper>();
                                                  oppLIObj2.add(new OpportunityLIWrapper(oppItem));
                                                  oppAndLIMap.put(opplst.Id,oppLIObj2);
                                              }
                                         
                                          } 
                                      }     
                                      
                                      //for(opportunity opps :oppListPending ){
                                         // opps.Total_TCV_of_All_OLI2__c = mapOpp.get(opps.Id);
                                    //  
                                    //  }                                                                                                                                 
                                      
                                      if(opplst.OpportunityLineItems.size() >0){        
                                          oppListPending.add(opplst);
                                      }                                                
                                  }    
        
       // system.debug('oppListPending============'+oppListPending);
        list<OpportunityWrapper> pportunityWrapperObj = new list<OpportunityWrapper>();       
        for(Opportunity oppobj : oppListPending){
            pportunityWrapperObj.add(new OpportunityWrapper(oppobj, oppAndLIMap.get(oppobj.Id)));
        }
        
       // system.debug('pportunityWrapperObj ==========='+pportunityWrapperObj);
        return pportunityWrapperObj;
        
        
    }    
    
    Public class OpportunityWrapper{
        @AuraEnabled
        Public Opportunity oppObj;
        @AuraEnabled
        Public Boolean oppSelected;
        @AuraEnabled
        Public List<OpportunityLIWrapper> opportunityLIWrapperObj;
        @AuraEnabled
        Public decimal tcv=0;
        @AuraEnabled
        Public Boolean DefaultColor;
        
        
        Public OpportunityWrapper(Opportunity oppObj, List<OpportunityLIWrapper> opportunityLIWrapperObj){
            this.oppObj = oppObj;
            this.opportunityLIWrapperObj = opportunityLIWrapperObj;
            this.oppSelected = oppSelected;
            
            if(opportunityLIWrapperObj!=null)
            {
               for(OpportunityLIWrapper objOLIWrap:opportunityLIWrapperObj)
               {
                    tcv=tcv+(objOLIWrap.tcv!=null?objOLIWrap.tcv:0);
               
               }
               
            }
            
        }
    }
    
    Public class OpportunityLIWrapper{
        @AuraEnabled
        Public OpportunityLineItem oppLIObj;
        @AuraEnabled
        Public Boolean oppLISelected;
        @AuraEnabled
        Public string cuId;
        @AuraEnabled
        Public string psrID;
        @AuraEnabled
        public decimal tcv;
        @AuraEnabled
        Public string strComment;
        @Auraenabled
        Public string strOliId;
        @Auraenabled
        Public string strStatus;
        
        
        Public OpportunityLIWrapper(OpportunityLineItem oppLIObj){
            this.oppLIObj = oppLIObj;
            oppLISelected = false;
            cuId = UserInfo.getUserId();
            psrID = oppLIObj.Opportunity.ownerID;
            tcv=oppLIObj.TCV__c;
            
            if(oppLIObj.vic_Sales_Rep_Approval_Status__c=='On-Hold' && oppLIObj.vic_Primary_Sales_Rep_Case__r.Description!=null  && cuId==psrID){
             strComment = oppLIObj.vic_Primary_Sales_Rep_Case__r.Description;
             }
             else 
             {
               if(oppLIObj.vic_Product_BD_Rep_Approval_Status__c=='On-Hold' &&  oppLIObj.vic_Product_BD_Rep_Case__r.Description!=null){
                strComment =oppLIObj.vic_Product_BD_Rep_Case__r.Description;
                 }
             else{
              strComment='';
             
              }
            }
            strOliId=oppLIObj.id;
            if(oppLIObj.vic_Sales_Rep_Approval_Status__c=='On-Hold' && cuId==psrID){
                
                strStatus='On-Hold';
            }
            else{
                if(oppLIObj.vic_Product_BD_Rep_Approval_Status__c=='On-Hold'){
                  strStatus='On-Hold';
                }
                
            }
            
        }
        
    }
    
    
    @AuraEnabled
    Public Static void getupdateStatus(List<OpportunityLineItem> selectedOpportunityLI){
        system.debug('selectedOpportunityLI' + selectedOpportunityLI);
        try{
         Id objUserId = UserInfo.getUserId();
        for(OpportunityLineItem oppLI : selectedOpportunityLI){          
            if(objUserId == oppLI.opportunity.ownerId){
                oppLI.vic_Sales_Rep_Approval_Status__c = 'Approved';                
            }        
           if(objUserId == oppLI.Product_BD_Rep__c){
                 oppLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';            
            }
        }
        update selectedOpportunityLI;
         } 
     catch(Exception e){
        
        }
            
    } 
    
   // public static set<id> setId = new set<id>();
    @AuraEnabled
    Public Static void getupdateStatusQuery(List<OpportunityLineItem> selectedOpportunityLIQuery){
        
        Id objUserId = UserInfo.getUserId();
        for(OpportunityLineItem oppLItem : selectedOpportunityLIQuery){          
            if(objUserId == oppLItem.opportunity.ownerId){
                oppLItem.vic_Sales_Rep_Approval_Status__c = 'On-Hold';                
            }        
           if(objUserId == oppLItem.Product_BD_Rep__c){
                 oppLItem.vic_Product_BD_Rep_Approval_Status__c = 'On-Hold';            
            }
        }
        update selectedOpportunityLIQuery;
    }
        
    @AuraEnabled 
    public static  list<OpportunityWrapper1>  getOpportunityApproved(){
  
        Map<Id,List<OpportunityLIWrapper1>> oppAndLIMap = new Map<Id,List<OpportunityLIWrapper1>>();
        List<OpportunityLIWrapper1> opportunityLIWrapperObj1 = new List<OpportunityLIWrapper1>();
        
        List<OpportunityWrapper1> opportunityWrapperObj1 = new List<OpportunityWrapper1>();    
        
       list<opportunity> oppListPending = new list<opportunity>();
       list<opportunity> oppListApproved = new list<opportunity>(); 
       Map<Id,String> mapIncentiveStatusAdjAmt = new Map<Id, String>();
       Map<String,Decimal> mapOfIncentive = new Map<String,Decimal>(); 
       Map<String,String> mapOfISO = new Map<String,String>(); 
       map<string,string> mapOPPLIToStatus = new map<string,string>();
     map<string,string> mapDomtoISO =vic_CommonUtil.getDomicileToISOCodeMap();
       map<string,decimal> mapOPPLIToExchangeRate = new map<string,decimal>();
       String OppoliID;
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        Date processingYearEnd =  processingYearStart.addYears(1).addDays(-1); 
        AggregateResult[] groupedResults = [select vic_Opportunity_Product_Id__c oliId,sum(vic_Incentive_In_Local_Currency__c)incen from  Target_Achievement__c where (vic_Status__c != 'HR - Rejected' and vic_Status__c != 'VIC Team - Rejected' and vic_Status__c != 'Supervisor - Rejected') AND Target_Component__r.Target__r.Start_Date__c=:processingYearStart Group By vic_Opportunity_Product_Id__c];      
             for(AggregateResult reslt : groupedResults) {
                  OppoliID = (String)reslt.get('oliId');
                 if(OppoliID !=null ){
                    mapOfIncentive.put(OppoliID, (Decimal)reslt.get('incen'));
                    system.debug('mapOfIncentive===*********'+mapOfIncentive);
                }
         }
        list<Target_Achievement__c> lstAchievement = [select id,vic_Opportunity_Product_Id__c,vic_Exchange_Rate__c,CurrencyIsoCode,vic_Status__c,Target_Component__r.Target__r.Domicile__c,
                                                                  vic_Incentive_In_Local_Currency__c,vic_Incentive_Type__c 
                                                                  from  Target_Achievement__c order by CreatedDate DESC];   
                                                                    
        
                //system.debug('lstAchievement==========='+lstAchievement);
                        for(Target_Achievement__c target :lstAchievement ){
                            if(mapOPPLIToStatus.containsKey(target.vic_Opportunity_Product_Id__c) == false) {
                                mapOPPLIToStatus.put(target.vic_Opportunity_Product_Id__c,target.vic_Status__c);
                            }
                            if(mapOPPLIToExchangeRate.containskey(target.vic_Opportunity_Product_Id__c)==false){
                                mapOPPLIToExchangeRate.put(target.vic_Opportunity_Product_Id__c,target.vic_Exchange_Rate__c);
                            }
                            
                           if(mapDomtoISO!=null && mapDomtoISO.size()>0 &&  mapDomtoISO.containskey(target.Target_Component__r.Target__r.Domicile__c))
                            mapOfISO.put(target.vic_Opportunity_Product_Id__c,mapDomtoISO.get(target.Target_Component__r.Target__r.Domicile__c));
                            
                            if(target.vic_Incentive_Type__c =='Adjustment'){
                                
                                mapIncentiveStatusAdjAmt.put(target.vic_Opportunity_Product_Id__c , target.vic_Incentive_Type__c);
                                
                                
                               system.debug('mapIncentiveStatusAdjAmt--=======*********'+mapIncentiveStatusAdjAmt.get(target.vic_Opportunity_Product_Id__c));
                            }
                        }
        
       for(opportunity opplst :[SELECT Id,CurrencyISOCode,Opportunity_ID__c, VIC_Cumulative_TCV_CPQ__c ,Account.Name,Annuity_Project__c,Name,Description,Type,Total_TCV_of_All_OLI2__c,TCV1__c,Margin__c,VIC_Total_Ebit_Of_Each_OLI__c,
                              (SELECT Name,vic_Primary_User__c, Product_BD_Rep__c, vic_VIC_User_3__c,Product2.Name,
                              vic_Primary_Sales_Rep_Case__c,vic_Primary_Sales_Rep_Case__r.Description,vic_Primary_Sales_Rep_Split__c,
                                  vic_Product_BD_Rep_Case__c,vic_Product_BD_Rep_Case__r.Description,vic_Product_BD_Rep_Split__c,
                               vic_VIC_User_4__c,TCV__c,Y1_AOI_USD_OLI_CPQ__c,vic_Sales_Rep_Approval_Status__c,EBIT_OLI_CPQ__c,VIC_TOTAL_EBIT__c,
                               Nature_of_Work__c,vic_IO_TS__c,vic_Exchange_Rate__c,Pricing_Deal_Type_OLI_CPQ__c,vic_Product_BD_Rep_Approval_Status__c,VIC_ProductShortName__c 
                               FROM OpportunityLineItems where ((vic_Sales_Rep_Approval_Status__c ='Approved' AND Opportunity.ownerID =:UserInfo.getUserId() ) 
                                OR (vic_Product_BD_Rep_Approval_Status__c ='Approved' AND Product_BD_Rep__c =:UserInfo.getUserId())
                                OR (vic_VIC_User_3__c=:UserInfo.getUserId() OR vic_VIC_User_4__c=:UserInfo.getUserId()))
                               AND vic_Final_Data_Received_From_CPQ__c=True) 
                                FROM Opportunity where StageName ='6. Signed Deal'
                                AND Actual_Close_Date__c >=:processingYearStart
                                AND Actual_Close_Date__c <=:processingYearEnd]){                                                             
                                
                               if(opplst.OpportunityLineItems !=null && opplst.OpportunityLineItems.size() >0){
                                          
                                          OpportunityWrapper1 oppWrap = new OpportunityWrapper1();
                                          oppWrap.oppObj = opplst;
                                          oppWrap.opportunityLIWrapperObj1 = new List<OpportunityLIWrapper1>();
                                          for(OpportunityLineItem oppItem :opplst.OpportunityLineItems){
                                                OpportunityLIWrapper1 oli = new OpportunityLIWrapper1();
                                                oli.oppLIObj = oppItem;
                                                oli.tcv=oppItem.TCV__c;
                                              system.debug('mapOPPLIToExchangeRate'+mapOPPLIToExchangeRate);  
                                              if(mapOPPLIToExchangeRate!=null && mapOPPLIToExchangeRate.size()>0 && mapOPPLIToExchangeRate.get(oppItem.id)!=null )
                                               {
                                                 oli.excrate=mapOPPLIToExchangeRate.get(oppItem.id);
                                                  
                                               }
                                               
                                               oppWrap.opportunityLIWrapperObj1.add(oli);
                                               
                                               
                                                
                                          }
                                       
                                         for(OpportunityLIWrapper1 objoli:oppWrap.opportunityLIWrapperObj1){
                                                  oppWrap.tcv=oppWrap.tcv+objoli.tcv;
                                                  
                                          }
                                          
                                          opportunityWrapperObj1.add(oppWrap);
                                      }     
                                   
                                    for(OpportunityWrapper1 oppWrapData : opportunityWrapperObj1){
                                        
                                        Decimal TotalIncetiveEachOLI = 0;
                                       for(OpportunityLIWrapper1 oliDataWrap : oppWrapData.opportunityLIWrapperObj1){
                                           oliDataWrap.OliStatus = 'Approved';
                                           if(mapOfIncentive.containsKey(oliDataWrap.oppLIObj.ID) && mapOfIncentive !=null && mapOfIncentive.size() >0 && mapOfIncentive.get(oliDataWrap.oppLIObj.ID)!=null ){
                                               oliDataWrap.totalIncentive = mapOfIncentive.get(oliDataWrap.oppLIObj.ID);
                                               
                                              TotalIncetiveEachOLI = TotalIncetiveEachOLI+mapOfIncentive.get(oliDataWrap.oppLIObj.ID);
                                              system.debug('TotalIncetiveEachOLI===='+TotalIncetiveEachOLI);
                                           }
                                           if(mapIncentiveStatusAdjAmt.containsKey(oliDataWrap.oppLIObj.ID) && mapIncentiveStatusAdjAmt !=null && mapIncentiveStatusAdjAmt.size() >0 && mapIncentiveStatusAdjAmt.get(oliDataWrap.oppLIObj.ID)!=null){
                                               oliDataWrap.adjustmentAmount = mapIncentiveStatusAdjAmt.get(oliDataWrap.oppLIObj.ID);
                                             // system.debug(' adjst incentive map==='+mapIncentiveStatusAdjAmt.get(oliDataWrap.oppLIObj.ID)); 
                                           }
                                           
                                           if(mapOfISO.containsKey(oliDataWrap.oppLIObj.ID) && mapOfISO !=null && mapOfISO.size() >0 && mapOfISO.get(oliDataWrap.oppLIObj.ID)!=null){
                                               oliDataWrap.ISOCode = mapOfISO.get(oliDataWrap.oppLIObj.ID);
                                             // system.debug(' adjst incentive map==='+mapIncentiveStatusAdjAmt.get(oliDataWrap.oppLIObj.ID)); 
                                           }
                                           if(mapOPPLIToStatus.size()>0 && mapOPPLIToStatus.get(oliDataWrap.oppLIObj.ID) != null) {
                                               oliDataWrap.OliStatus = mapOPPLIToStatus.get(oliDataWrap.oppLIObj.ID);
                                           }
                                           if(oliDataWrap.OliStatus == 'Supervisor Pending' || oliDataWrap.OliStatus == 'VIC Team - Pending'){
                                               TotalIncetiveEachOLI = 0;
                                           }
                                       }
                                      oppWrapData.totalIncentiveEachOLI =TotalIncetiveEachOLI; 
                                      
                                   }
                                    
                                      if(opplst.OpportunityLineItems.size() >0){        
                                          oppListPending.add(opplst);
                                      }  
                                  }    
        
                            list<OpportunityWrapper1> pportunityWrapperObj = new list<OpportunityWrapper1>();       
                            for(Opportunity oppobj : oppListPending){
                                pportunityWrapperObj.add(new OpportunityWrapper1(oppobj, oppAndLIMap.get(oppobj.Id)));
                                
                                }      
                       system.debug('mapOPPLIToExchangeRate'+mapOPPLIToExchangeRate); 
                       system.debug('pportunityWrapperObj= finall=='+opportunityWrapperObj1);
                        return opportunityWrapperObj1;
                
        
    }    
         
      Public class OpportunityWrapper1{
        @AuraEnabled
        Public Opportunity oppObj;
        @AuraEnabled
        Public Boolean oppSelected;
        @AuraEnabled
        Public Decimal totalIncentiveEachOLI;  
        @AuraEnabled
        Public List<OpportunityLIWrapper1> opportunityLIWrapperObj1;
        @AuraEnabled
        Public decimal tcv=0;  
        
        
        Public OpportunityWrapper1(Opportunity oppObj, List<OpportunityLIWrapper1> opportunityLIWrapperObj1){
            this.oppObj = oppObj;
            this.opportunityLIWrapperObj1 = opportunityLIWrapperObj1;
            this.oppSelected = oppSelected;
            
            
            
        }
          Public OpportunityWrapper1(){}
    }
    
    Public class OpportunityLIWrapper1{
        @AuraEnabled
        Public OpportunityLineItem oppLIObj;
        @AuraEnabled
        Public String ISOCode;
        @AuraEnabled
        Public String OliStatus;
        
        @AuraEnabled
        Public string adjustmentAmount;
        @AuraEnabled
        Public Decimal totalIncentive;
        @AuraEnabled
        Public Decimal tcv;
        @AuraEnabled
        Public decimal excrate;
        
        public OpportunityLIWrapper1(OpportunityLineItem oppLIObj){
            this.oppLIObj = oppLIObj;
            
           
        }
        public OpportunityLIWrapper1(){}
    }    
   
    public static string createCase(String strCaseRaisedBy, String strDescription, OpportunityLineItem objOLI){
         Id objUserId = UserInfo.getUserId();
        Id caseRecordTypeId = Schema.SObjectType.case.getRecordTypeInfosByName().get('VIC Cases').getRecordTypeId();
        List<Group> lstGroup = [select Id,Name 
                                from Group 
                                where Name = 'VIC Team' 
                                and Type = 'Queue'];                                
    
        Case objCase = new Case();
        objCase.OwnerId = lstGroup != null && lstGroup.size() > 0 ? lstGroup[0].id : strCaseRaisedBy; 
        objCase.Opportunity__c = objOLI.OpportunityId;
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Type = 'Question';
        objCase.Subject = 'Sales Rep Query';
        objCase.Opportunity_Product_ID__c = objOLI.id;
        /*if(objUserId ==objOLI.opportunity.ownerId ){
            objCase.VIC_Case_Created_For__c= objOLI.opportunity.ownerId;
         }else{
            objCase.VIC_Case_Created_For__c=objUserId;            
        }*/
        objCase.VIC_Case_Created_For__c=objUserId;
        objCase.recordtypeid = caseRecordTypeId;
        objCase.Description= strDescription;
        
        insert objCase;
        return objCase.id;        
    } 
    
    public static void reopenCase(String strCaseId){
        update new case(id = strCaseId, Status = 'Re-Open');
    }
    
    public static void addCaseComments(String strCaseId, String strComments){
        insert new CaseComment(ParentId=strCaseId, CommentBody = strComments);
    }
    
    @AuraEnabled 
    public static void updateOLIStatus(string strRecordId , string strDescription){
      system.debug('OLI ID : ' + strRecordId );
      OpportunityLineItem objOLI =[select id,opportunity.ownerId,vic_Sales_Rep_Approval_Status__c,vic_Primary_Sales_Rep_Case__r.Status,vic_Product_BD_Rep_Case__r.Status,vic_Product_BD_Rep_Case__r.Description,vic_Primary_Sales_Rep_Case__r.Description,Product_BD_Rep__c,vic_Primary_User__c,vic_Product_BD_Rep_Approval_Status__c 
                                          from OpportunityLineItem 
                                          where Id =:strRecordId
                                          LIMIT 1];
      Id objUserId = UserInfo.getUserId();
      Id RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('VIC Cases').getRecordTypeId();
      
      OpportunityLineItem objUpdateOLI = null;
       
      if(objUserId == objOLI.opportunity.ownerId){
          if(objOLI.vic_Primary_Sales_Rep_Case__c != null){
              if(objOLI.vic_Primary_Sales_Rep_Case__r.Status =='Closed'){
                  reopenCase(objOLI.vic_Primary_Sales_Rep_Case__c);
              }
              addCaseComments(objOLI.vic_Primary_Sales_Rep_Case__c, strDescription);
              objUpdateOLI = new OpportunityLineItem(id=objOLI.id, vic_Sales_Rep_Approval_Status__c = 'On-Hold');
          }else{              
              objUpdateOLI = new OpportunityLineItem(id=objOLI.id, vic_Sales_Rep_Approval_Status__c = 'On-Hold',vic_Primary_Sales_Rep_Case__c = createCase(objOLI.opportunity.ownerId, strDescription, objOLI));
          }
      }else if(objOLI.Product_BD_Rep__c != null && objOLI.opportunity.ownerId != objOLI.Product_BD_Rep__c && objUserId == objOLI.Product_BD_Rep__c){
          if(objOLI.vic_Product_BD_Rep_Case__c != null){
              if(objOLI.vic_Product_BD_Rep_Case__r.Status =='Closed'){
                  reopenCase(objOLI.vic_Product_BD_Rep_Case__c);
              }
              addCaseComments(objOLI.vic_Product_BD_Rep_Case__c, strDescription);
              objUpdateOLI = new OpportunityLineItem(id=objOLI.id, vic_Product_BD_Rep_Approval_Status__c = 'On-Hold');
          }else{
              objUpdateOLI = new OpportunityLineItem(id=objOLI.id, vic_Product_BD_Rep_Approval_Status__c = 'On-Hold', vic_Product_BD_Rep_Case__c = createCase(objOLI.Product_BD_Rep__c, strDescription, objOLI));
          }
      }
      
      if(objUpdateOLI != null){
          update objUpdateOLI;
      }
      
      
        /*Case objCase = new Case(); 
        objCase.Opportunity__c = OliList[0].OpportunityId;
        objCase.Status = 'New';
        objCase.Origin = 'Web';
        objCase.Type = 'Question';                   
        objCase.recordtypeid = RecordTypeIdCase ;
        objCase.Description= description;
        
      
       
      if(objUserId == OliList[0].opportunity.ownerId){
         if(OliList[0].vic_Primary_Sales_Rep_Case__c!=null){
             
             if(OliList[0].vic_Primary_Sales_Rep_Case__r.Status =='Closed'){
                    Case css = new case();
                    css.Id = OliList[0].vic_Primary_Sales_Rep_Case__c;
                    css.Status = 'Re-Open';
                    update css;
                     
                    CaseComment casecomm = new CaseComment();
                    casecomm.ParentID  = OliList[0].vic_Primary_Sales_Rep_Case__c;
                    casecomm.commentbody = description;
                    insert casecomm;                                                                    
                 
             }
                    CaseComment casecomm = new CaseComment();
                    casecomm.ParentID  = OliList[0].vic_Primary_Sales_Rep_Case__c;
                    casecomm.commentbody = description;
                    insert casecomm;
             }
              
         }
        else{
                Case caseObj = new Case(); 
                caseObj.Opportunity__c = OliList[0].OpportunityId;
                caseObj.Status = 'New';
                caseObj.Origin = 'Web';
                caseObj.Type = 'Question';                   
                caseObj.recordtypeid =RecordTypeIdCase;
                caseObj.Description= description;
              
                insert caseObj;
                OliList[0].vic_Primary_Sales_Rep_Case__c=  caseObj.id;
          }
          
         OliList[0].vic_Sales_Rep_Approval_Status__c = 'On-Hold';
     
        
        
     if(objUserId == OliList[0].vic_Product_BD_Rep_Case__c){
         if(OliList[0].vic_Product_BD_Rep_Case__c!=null){
             
             if(OliList[0].vic_Product_BD_Rep_Case__r.Status =='Closed'){
                 //OliList[0].vic_Product_BD_Rep_Case__r.Status ='Open';
                   Case css = new case();
                    css.Id = OliList[0].vic_Primary_Sales_Rep_Case__c;
                    css.Status = 'Re-Open';
                    update css;
             
                 CaseComment casecomm = new CaseComment();
                    casecomm.ParentID  = OliList[0].vic_Product_BD_Rep_Case__c;
                    casecomm.commentbody = description;
                    casecomm.ispublished = true;
                    insert casecomm;
             
             }
           }  
        
        else{
                Case caseObj = new Case(); 
                caseObj.Opportunity__c = OliList[0].OpportunityId;
                caseObj.Status = 'New';
                caseObj.Origin = 'Web';
                caseObj.Type = 'Question';                   
                caseObj.recordtypeid =RecordTypeIdCase;
                caseObj.Description= description;
              
                insert caseObj;
                OliList[0].vic_Product_BD_Rep_Case__c=  caseObj.id;
             }
          
           OliList[0].vic_Product_BD_Rep_Approval_Status__c = 'On-Hold';
       
     
     }   
         update OliList;*/
        
    }
    
    
    @AuraEnabled 
    public static void updatePreOLIStatus(string lstRecordIdpen ){
        
        //OpportunityLineItem  oppPreItem= new OpportunityLineItem();
        list<OpportunityLineItem> OliList =[select id,opportunity.ownerId,vic_Sales_Rep_Approval_Status__c,Product_BD_Rep__c,vic_Primary_User__c,vic_Product_BD_Rep_Approval_Status__c from OpportunityLineItem where Id =:lstRecordIdpen];
        Id objUserId = UserInfo.getUserId();
       
        if(objUserId == OliList[0].opportunity.ownerId){
             OliList[0].vic_Sales_Rep_Approval_Status__c = 'Pending';
            system.debug('=if');
          }
        
         if(objUserId == OliList[0].Product_BD_Rep__c){
             OliList[0].vic_Product_BD_Rep_Approval_Status__c = 'Pending';
            system.debug('=if else');
        }
     
        //OliList[0].Id = lstRecordId;
        system.debug('==updateOLIStatus preoli====='+lstRecordIdpen);
        system.debug('==updateOLIStatus pre oli reccc====='+OliList);
        update OliList;
        system.debug('==updateOLIStatus pre oli reccc=updated===='+OliList);
    }
    
    
    @AuraEnabled
    public static List<String> getFinancialYear(){
        return vic_CommonUtil.getFinancialYearForDisplay();
    }
     
    @AuraEnabled
    public static String submitBonusLetter(String strYear){
        //Considering strYear value will never be future year value.
        if(String.isNotBlank(strYear) && Integer.valueOf(strYear) < = Date.today().year()){
            Integer selectedYear = Integer.valueOf(strYear);
            Date startDate = Date.newInstance(selectedYear, 1, 1);
            Date endDate = Date.today();
            if(selectedYear != Date.today().year()){
                endDate = Date.newInstance((selectedYear+1), 12, 31);
            }
            return '/apex/VIC_GenerateBonusMatrix?userId=' + UserInfo.getUserId() + '&startdate=' + startDate + '&endDate=' + endDate;
        }
        return null;
    }
    
 
 /*   @AuraEnabled 
    public static void updateApprovedOLIStatus(string lstRecordId ){
        
        //OpportunityLineItem  oppAppItem= new OpportunityLineItem();
        list<OpportunityLineItem> OliList =[select id,opportunity.ownerId,vic_Sales_Rep_Approval_Status__c,Product_BD_Rep__c,vic_Primary_User__c,vic_Product_BD_Rep_Approval_Status__c from OpportunityLineItem where Id =:lstRecordId];
        Id objUserId = UserInfo.getUserId();
        
        if(objUserId == OliList[0].opportunity.ownerId){
             OliList[0].vic_Sales_Rep_Approval_Status__c = 'Approved';
            
          }
        
        if(objUserId == OliList[0].Product_BD_Rep__c){
             OliList[0].vic_Product_BD_Rep_Approval_Status__c = 'Approved';
            
        }
     
        //OliList[0].Id = lstRecordId;
        
        update OliList;
        
    }
    
    
    //public static List<Id> Ids = new List<Id>();
        @AuraEnabled 
        public static void calAllIncentive(){ 
            
          
        
        }
    */
}