@isTest
public class GPSelectorAddressTracker {
	public Static GP_Address__c address;
    
    @testSetup
    public static void buildDependencyData() {
        address = GPCommonTracker.getAddress();
        insert address;
    }
    
    @isTest
    public static void testgetSObjectType() {
        GPSelectorAddress addressSelector = new GPSelectorAddress();
        Schema.SObjectType returnedType = addressSelector.getSObjectType();
        Schema.SObjectType expectedType = GP_Address__c.sObjectType;
        
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectById() {
        GPSelectorAddress addressSelector = new GPSelectorAddress();
        address = [Select Id, GP_City__c from GP_Address__c Limit 1];
        GP_Address__c returnedAddress = addressSelector.selectById(address.Id);
        
        System.assertEquals(returnedAddress.GP_City__c, address.GP_City__c);
        List<GP_Address__c> listOfReturnedAddress = addressSelector.selectById(new Set<Id> {
            address.Id
        });
        
        System.assertEquals(listOfReturnedAddress.size(), 1);
    }
}