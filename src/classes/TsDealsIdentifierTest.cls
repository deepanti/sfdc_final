@isTest
public class TsDealsIdentifierTest {
	
    static opportunity opp, opp1, opp2, opp3;
    static User u1;
    
     Static testmethod void testTsDealsBatch(){
        setUpTestMethod();
       	Database.executeBatch(new TsDealsIdentifierBatch(), 100);
        
     }
     
    private static void setUpTestMethod(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        
        list<opportunity> oliList= new List<opportunity>();
        opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS');
        opp1=new opportunity(name='1235',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS');
        opp2=new opportunity(name='1236',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts');
        opp3=new opportunity(name='1237',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts');
        oliList.add(opp);
        oliList.add(opp1);
        oliList.add(opp2);
        oliList.add(opp3);
        insert oliList;
        
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Analytics',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
     }
}