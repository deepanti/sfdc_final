global class ExcelExport implements Database.Batchable <sobject>,Database.stateful
{
    Static string header = 'Name' + ',' + 'Email' + ',' + 'Phone' + ',' + 'COUNTRY OF RESIDENCE' + ',' + 'COMPANY' + ',' + 'INDUSTRY VERTICAL' + ',' + 'TITLE' + ',' + 'BUYING CENTER' + ',' + 'LEVEL OF CONTACT' + ',' + 'RECORD TYPE' + ',' + 'ACCOUNT ARCHETYPE' + ',' + 'LEAD SCORE' + ',' + 'LIFECYCLE STATUS' + ',' + 'RECENT CAMPAIGN NAME';
          
    Public List<string> Lines;
    private String strParameterQ1;
   
    public ExcelExport(String strParamq1)
    {
        Lines =new List<string>(); 
        strParameterQ1 = strParamq1;
        system.debug('=====strParameterQ1===in Constructor=='+strParameterQ1);
        
    }
    Public String generatedCSVFile ='';
    
    global Database.QueryLocator start(Database.BatchableContext bc)
    { lines.add(header);
        system.debug('======strParameterQ1====start=='+strParameterQ1);
       string q = strParameterQ1; // 'select Id,name from Lead';    --'select Id,Name from account';Lead
       
        system.debug('=====q======='+q);       
        return database.getQueryLocator(q);
    }
    
    global void execute(Database.BatchableContext bc, List<Contact> scope)
    {
        system.debug('=====scope===='+scope);       
      // Boolean HeaderFlag = True;
          string fileRow;
       // if(HeaderFlag){
      //  lines.add(header); 
       //     HeaderFlag = False;
       // }
        for(Contact  a: scope)
        {     
            //SELECT name,Email,Phone,Country_of_Residence__c,DSE__DS_Company__c,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto_si__Mkto_Lead_Score__c,Status__c,Recent_Channel_Detail__c,Industry_Vertical__c FROM Contact  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false
            
             
         if(a.name != null && a.name != '')
            {
                if(a.name.contains(',')){
                fileRow = '"'+a.name+'"';
                }else{
                   fileRow =  a.name; 
                }
            }
            else
            {
                fileRow =''; 
            }
            
            if(a.Email != null && a.Email != '')
            {
                if(a.Email.contains(',')){
                fileRow = fileRow +','+'"'+a.Email+'"';
                }else{
                    fileRow = fileRow +','+ a.Email;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.Phone != null && a.Phone != '')
            {
                if(a.Phone.contains(',')){
                fileRow = fileRow +','+'"'+a.Phone+'"';
                }else{
                    fileRow = fileRow +','+ a.Phone;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.Country_of_Residence__c != null && a.Country_of_Residence__c != '')
            {
                if(a.Country_of_Residence__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Country_of_Residence__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Country_of_Residence__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.DSE__DS_Company__c != null && a.DSE__DS_Company__c != '')
            {
                if(a.DSE__DS_Company__c.contains(',')){
                fileRow = fileRow +','+'"'+a.DSE__DS_Company__c+'"';
                }else{
                    fileRow = fileRow +','+ a.DSE__DS_Company__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            if(a.Industry_Vertical__c != null && a.Industry_Vertical__c != '')
            {
                if(a.Industry_Vertical__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Industry_Vertical__c+'"';  
                }else{
                    fileRow = fileRow +','+ a.Industry_Vertical__c;  
                }
            }
            
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.Title != null && a.Title != ''){
                if(a.Title.contains('"') ){
                    String Str = a.Title.replace('"','');
                    fileRow = fileRow +','+'"'+Str+'"';
                }else if(a.Title.contains(',')){
                    fileRow = fileRow +','+'"'+a.Title+'"';
                }else{
                    fileRow = fileRow +','+ a.Title;
                }
            }
            else{fileRow =fileRow +','+'';}  
            
            
            if(a.Buying_Center__c != null && a.Buying_Center__c != '')
            {
                if(a.Buying_Center__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Buying_Center__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Buying_Center__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.Level_of_Contact__c != null && a.Level_of_Contact__c != '')
            {
                if(a.Level_of_Contact__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Level_of_Contact__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Level_of_Contact__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            fileRow = fileRow +','+ 'Contact'; 
            
            if(a.Archetype__c != null && a.Archetype__c != '')
            {
                if(a.Archetype__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Archetype__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Archetype__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.mkto71_Lead_Score__c != null)
            {
               // if(a.mkto_si__Mkto_Lead_Score__c.contains(',')){
               // fileRow = fileRow +','+'"'+a.mkto_si__Mkto_Lead_Score__c+'"';
              //  }else{
                    fileRow = fileRow +','+ a.mkto71_Lead_Score__c;
              //  }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
            if(a.Status__c != null && a.Status__c != '')
            {
                if(a.Status__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Status__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Status__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            
           if(a.Recent_Channel_Detail__c != null && a.Recent_Channel_Detail__c != '')
            {
                if(a.Recent_Channel_Detail__c.contains(',')){
                fileRow = fileRow +','+'"'+a.Recent_Channel_Detail__c+'"';
                }else{
                    fileRow = fileRow +','+ a.Recent_Channel_Detail__c;
                }
            }
            else
            {
                fileRow =fileRow +','+'';
            }
            lines.add(fileRow); //fileRow
        }
        system.debug('lines length'+lines.Size());
        system.debug('lines-=====execute========='+lines);     
    }
    
    global void finish(Database.BatchableContext bc)
    {if(lines.size() > 2){
       // String CurrentUserEmail = UserInfo.getUserEmail();
        system.debug('lines-=====finish='+lines);
        system.debug('lines-=====finish='+lines.size());
        generatedCSVFile = generatedCSVFile + string.join(lines, '\n');
        lines=null;
        String today = Datetime.now().format('dd-MM-yyyy');
        Blob csvBlob = blob.valueOf(generatedCSVFile);
        String csvName = 'Contact Data as on '+today+'.csv';   
        
            Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
            Blob csvBlob1 = blob.valueOf(generatedCSVFile);
            String csvName1 = 'MDM Contact Report.csv';
            csvAttachment.setFileName(csvName1);
            csvAttachment.setBody(csvBlob1);
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{UserInfo.getUserEmail()};
                String subject = 'MDM Contact Report Export';
            email.setSubject(subject);
            email.setToAddresses(toAddresses);
            email.setPlainTextBody('Contact Details CSV');
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
            system.debug('======email===Contact='+email);
            Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});  
        }
    }
 
}