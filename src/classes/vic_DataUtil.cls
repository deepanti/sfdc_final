/**
* @author: Shivam Singh
* @date: March 2018
*/
public class vic_DataUtil {
    /**
* @author: Shivam Singh
* @date: March 2018
*/
    public static List<Master_VIC_Role__c> getActiveMasterVICRole(){
        return [select id,name from Master_VIC_Role__c where is_Active__c=true order by name];
    }
    
    public static List<User_VIC_Role__c> getActiveUserVICRoleByVicRole(String vicRole){
        return [Select User__c from User_VIC_Role__c where Master_VIC_Role__c= :vicRole AND Not_Applicable_for_VIC__c = false AND User__r.isActive = true];
    }
    
    public static List<User_VIC_Role__c> getActiveUserVICRoleByVicRoleAndDomicile(String vicRole, String strDomicile){
        return [Select User__c 
                from User_VIC_Role__c 
                where Master_VIC_Role__c= :vicRole 
                AND Not_Applicable_for_VIC__c = false 
                AND User__r.isActive = true
                AND User__r.Domicile__c=:strDomicile];
    }
    
    public static List<User_VIC_Role__c> getAllActiveUserVICRole(){
        return [Select User__c from User_VIC_Role__c where Not_Applicable_for_VIC__c = false AND User__r.isActive = true];
    }
    
    public static List<User_VIC_Role__c> getAllActiveUserVICRoleAndDomicile(String strDomicile){
        return [Select User__c from User_VIC_Role__c where Not_Applicable_for_VIC__c = false AND User__r.isActive = true AND User__r.Domicile__c=:strDomicile];
    }
    
}