//================================================================================================================
//  Description: Test Class for GPDomianOpportunityProjectAddress
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          10-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPDomianOpportunityProjectAddressTracker {
    
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Opportunity_Project__c oppproobj;
    public static Account accobj2;
    public static GP_Work_Location__c objSdo2;
    public static String jsonresp;
    
    
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_opp_project_address__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
        
        Account accobj2 = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj2 ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        objSdo2 = GPCommonTracker.getWorkLocation();
        objSdo2.GP_Status__c = 'Active and Visible';
        objSdo2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo2;
        
        accobj2 = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj2;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 10;
        oppproobj.GP_Oracle_PID__c = 'test';
        oppproobj.GP_Customer_L4_Name__c = accobj.id;
        oppproobj.GP_SDO_Code__c = objSdo.id;
        insert oppproobj;
        
        GP_Deal__c ObjDeal  = new GP_Deal__c();
        ObjDeal.GP_Opportunity_Project__c = oppproobj.id;
        ObjDeal.GP_TCV__c = 1234;
        insert ObjDeal;  
        
        GP_Opp_Project_Address__c objPrjAddress = new GP_Opp_Project_Address__c();
        objPrjAddress.GP_Opp_Project__c = oppproobj.id;
        objPrjAddress.GP_Bill_To_Address_Id__c = 'test';
        objPrjAddress.GP_Relationship__c = 'testrel';
        objPrjAddress.GP_Ship_To_Address_Id__c = 'testShip';
        objPrjAddress.GP_Account_Id__c = accobj.id;
        insert objPrjAddress;
        objPrjAddress.GP_Account_Id__c = accobj2.id;
        objPrjAddress.GP_IsUpdated__c = true;
        objPrjAddress.GP_Bill_To_Address_Id__c = 'test2';
        objPrjAddress.GP_Relationship__c = 'testrel2';
        objPrjAddress.GP_Ship_To_Address_Id__c = 'testShip2';
        Update objPrjAddress;
    }
    @isTest
    public static void testGPDomianOpportunityProjectAddress() 
    {
        buildDependencyData();  
    }
}