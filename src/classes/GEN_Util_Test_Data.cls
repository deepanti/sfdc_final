/*
 * This is a helper class for test classes
 * where the test data has been created
*/

public class GEN_Util_Test_Data
{  
    // Sales Unit
    public static Sales_Unit__c CreateSalesUnit(String SalesUnitName, id SalesLeader, string SalesUnitGroup)
    {
       Sales_Unit__c SalesUnitObj = new Sales_Unit__c();
       SalesUnitObj.Name = SalesUnitName;
       SalesUnitObj.Sales_Leader__c = SalesLeader;
       SalesUnitObj.Sales_Unit_Group__c = SalesUnitGroup;     
       insert SalesUnitObj;
       return SalesUnitObj;
    }
    
    //Create User 
    public static User CreateUser(String Email, id ProfileId, string UserName )
    {
        User oUser = new User();
        oUser.Alias = 'Test';
        //oUser.Legal_Person__c = True;
        oUser.Email = Email;
        oUser.EmailEncodingKey='UTF-8';
        oUser.LastName='Test';
        oUser.LanguageLocaleKey='en_US';
        oUser.LocaleSidKey='en_US';
        Ouser.ProfileId=ProfileId;
        oUser.OHR_ID__c='700060235';
        oUser.TimeZoneSidKey='America/Los_Angeles';
        oUser.UserName=UserName;
        insert oUser ;
        return oUser ;    
    }
    
    //Create Business Segment
    public static Business_Segments__c CreateBusiness_Segments(String strName, String strFor)
    {
        Business_Segments__c oBS = new Business_Segments__c();
        oBS.Name = strName;
        oBS.For__c = strFor;
        insert oBS;
        return oBS;    
    }
    
    //Create Pricebook
    public static Pricebook2 CreatePricebook2()
    {
        pricebook2 pb = new pricebook2(name='test');
        insert pb;
        return pb;
    }   
     
    //Create Quota headers     
    public static Quota_header__c CreateQuotaheader(string uid)
    {        
        Quota_header__c oQuota = new Quota_header__c();        
        //oQuota.Name='test';
        oQuota.Sales_Person__c=uid;
        oQuota.Year__c = String.valueof(system.today().year());        
        insert oQuota;        
        return oQuota;            
     }    
        
    //Create Set Quotas
    public static Set_Quotas__c CreateQuotaGRM(id AccountId,Id QuotaHeaderid)
    {
        Set_Quotas__c oQuota = new Set_Quotas__c();
        oQuota.Account_Name__c =AccountId;
        oQuota.recordtypeid=[select id from recordtype where developername='Account_GRM' limit 1].id;
        oQuota.ACV__c =100000;
        oQuota.CYR__c =100000;
        oQuota.Quota_header__c = QuotaHeaderid;
        oQuota.TCV__c =200000;
        insert oQuota;
        return oQuota;
    }     
  
    //Create PriceBookEntry
    public static PriceBookEntry CreatePriceBookEntry(Id  ProductId, Id PriceBookId)
    {
        Pricebook2 stdPb = [select Id from Pricebook2 where isStandard=true limit 1];
        PriceBookEntry oPB = new PriceBookEntry();
        oPB.Product2Id = ProductId;
        oPB.Pricebook2Id = stdPb.Id;
        oPB.UnitPrice = 0;
        oPB.isActive = true;
        insert oPB;
        return oPB;        
    }
    
    //Create Product
    public static Product2 CreateProduct(String strName)
    {
        Product2 oProduct = new Product2();
        //oProduct.Name='Additional Client Licenses';
        oProduct.Name=strName;
        oProduct.IsActive=true;
        insert oProduct;
        return oProduct;
    }
    
    // Create Sub_Business__c
    public static  Sub_Business__c CreateSub_Business(String strName,String strFor,Id oBS)
    {
        Sub_Business__c oSB = new Sub_Business__c();
        oSB.name = strName;
        oSB.for__c = strFor;
        oSB.Business_Segment__c = oBS;
        insert oSB;
        return oSB;
    }
    
    //Create Archetype
    public static Archetype__c CreateArchetype(string Name)
    {
        Archetype__c oArchtype = new Archetype__c();
        oArchtype.Name=Name;
        insert oArchtype;
        return oArchtype;
    }
    
    //Create test active Account
    public static Account CreateAccount(id userid ,id uId, id AId, String strName,String strBusiness_Group,Id oBS,Id oSB,String strIndustry_Vertical,String strSub_industry_vertical,string strAccount_classification,string strForbes_Company_Name,String strAccountNum)
    {
         Account oAccount = new Account();
         oAccount.Name = strName;
         oAccount.AccountNumber = strAccountNum;
         oAccount.Business_group__c= strBusiness_Group;
         oAccount.business_segment__c = oBS;
         oAccount.sub_business__c= oSB;
         oAccount.industry_vertical__c= strIndustry_Vertical;
         oAccount.sub_industry_vertical__c= strSub_industry_vertical;
         //oAccount.Account_classification__c= strAccount_classification;
         //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
         oAccount.website = 'www.genpact.com';
         oAccount.sic= 'test code';
         oAccount.hunting_mining__c= 'test mining';
         oAccount.TAM__c=5000;
         oAccount.PTAM__c=6000;
         oAccount.Archetype__c=AId;
         //oAccount.DUNS_Number__c=454554;    
         oAccount.Client_Partner__c=userid;
         oAccount.Primary_Account_GRM__c=uId;
         oAccount.TickerSymbol= 'test ticker';
         oAccount.AnnualRevenue= 50000;
         oAccount.ownerid=uId;
         oAccount.Ownership= 'test Owner';
         oAccount.Account_headquarters_country__c= 'test Account headquarters';
         oAccount.Account_Owner_from_ACR__c=uId;
         //oAccount.base_country__c= 'test base country';
         insert oAccount;
         return oAccount;
    }
    
    //Create test Inactive Account
    public static Account CreateInactiveAccount(String strName,String strBusiness_Group,Id oBS,Id oSB,String strIndustry_Vertical,String strSub_industry_vertical,string strAccount_classification,string strForbes_Company_Name,String strAccountNum)
    {
        Account oAccount = new Account();
        oAccount.Name = strName;
        oAccount.AccountNumber = strAccountNum;
        oAccount.Business_group__c= strBusiness_Group;
        oAccount.business_segment__c = oBS;
        oAccount.sub_business__c= oSB;
        oAccount.industry_vertical__c= strIndustry_Vertical;
        oAccount.sub_industry_vertical__c= strSub_industry_vertical;
        //oAccount.Account_classification__c= strAccount_classification;
        //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
        oAccount.website = 'www.genpact.com';
        oAccount.sic= 'test code';
        oAccount.hunting_mining__c= 'test mining';
        oAccount.TickerSymbol= 'test ticker';
        //oAccount.Public_Non_Public__c='Public';
        oAccount.AnnualRevenue= 50000;
        oAccount.Ownership= 'test Owner';
        oAccount.Account_headquarters_country__c= 'test Account headquarters';
        //oAccount.base_country__c= 'test base country';
        oAccount.Inactive__c = true;
        insert oAccount;
        return oAccount;
    }
    
    //Create Account Team Member
    public static AccountTeamMember CreateAccountTeamMember(id aId,id uId) 
    {
        AccountTeamMember oAccountTeamMember= new AccountTeamMember();
        oAccountTeamMember.AccountId=aId;
        oAccountTeamMember.userid=uId;        
        oAccountTeamMember.teammemberrole='BD Rep';
        insert oAccountTeamMember;
        return oAccountTeamMember;        
    }
    
    //Create Contact 
    public static Contact CreateContact(String strFirstName,String strLastName,Id AccountId,String strTitle,string strLeadSource,String strEmail,string strPhone)
    {
        Contact oContact = new Contact();
        oContact.FirstName = strFirstName;
        oContact.LastName = strLastName;
        oContact.AccountId = AccountId;
        oContact.Title = strTitle;
        oContact.LeadSource = strLeadSource;
        oContact.Email = strEmail;  
        oContact.Phone = strPhone;
        insert oContact;
        return oContact;
    }
    
    //Create the Opportunity
    public static Opportunity CreateOpportunity(String strName,Id AccountId,Id ContactId)
    {
        Opportunity oOpportunity = new Opportunity();
        oOpportunity.Name = strName;
        oOpportunity.AccountId = AccountId;
        oOpportunity.StageName = '1. Discover';        
        oOpportunity.Target_Source__c = 'Consulting Pull Through';
        oOpportunity.Opportunity_Origination__c='Proactive';
        oOpportunity.Is_RS_parameter_approve__c	= True;
        oOpportunity.Sales_Region__c='India';
        oOpportunity.Sales_country__c = 'India';
        oOpportunity.Deal_Type__c = 'Sole Sourced';        
        oOpportunity.CloseDate = System.today();
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(2);
        oOpportunity.Contract_Term_in_months__c=36;        
        oOpportunity.Revenue_Product__c = 'Test';
        oOpportunity.CMITs_Check__c = False;       
        oOpportunity.Advisor_Firm__c = 'Trestle';
        oOpportunity.Contact__c = ContactId;
        oOpportunity.Advisor_Name__c = 'Bernhard Janischowsky';        
        oOpportunity.Annuity_Project__c = 'Project';
        oOpportunity.Deal_Type__c = 'Sole Sourced';       
        oOpportunity.Win_Loss_Dropped_reason1__c='test';
        oOpportunity.Win_Loss_Dropped_reason2__c='test';
        oOpportunity.Win_Loss_Dropped_reason3__c='test';
        oOpportunity.SPOC_s__c='Nitesh Aggarwal';        
        insert oOpportunity;
        return oOpportunity;
    }
    
    //Create the Opportunity with Errors
    public static Opportunity CreateErrorOpportunity(String strName,Id AccountId,Id ContactId)
    {
        Opportunity oOpportunity = new Opportunity();
        oOpportunity.Name = strName;
        oOpportunity.AccountId = AccountId;
        oOpportunity.Target_Source__c = 'Consulting Pull Through';
        oOpportunity.Opportunity_Origination__c='Proactive';
        oOpportunity.Is_RS_parameter_approve__c	= True;
        oOpportunity.Sales_Region__c='India';
        oOpportunity.Sales_country__c = 'India';
        oOpportunity.Deal_Type__c = 'Sole Sourced';        
        oOpportunity.CloseDate = System.today();
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(2);
        oOpportunity.Contract_Term_in_months__c=12;        
        oOpportunity.Revenue_Product__c=null;
        oOpportunity.CMITs_Check__c=False;       
        oOpportunity.Advisor_Firm__c = 'Trestle';
        oOpportunity.Contact__c = ContactId;
        oOpportunity.Advisor_Name__c = 'Bernhard Janischowsky';        
        oOpportunity.Annuity_Project__c = 'Project';
        oOpportunity.Deal_Type__c = 'Sole Sourced';       
        oOpportunity.Win_Loss_Dropped_reason1__c='test';
        oOpportunity.Win_Loss_Dropped_reason2__c='test';
        oOpportunity.Win_Loss_Dropped_reason3__c='test';
        oOpportunity.SPOC_s__c='Nitesh Aggarwal';        
        return oOpportunity;
    }
    
    //Create Opportunity Team Member
    public static OpportunityTeamMember CreateOpportunityTeamMember(id oppId,id uId) 
    {
        OpportunityTeamMember oOpportunityTeamMember= new OpportunityTeamMember();
        oOpportunityTeamMember.OpportunityId=oppId;
        oOpportunityTeamMember.userid=uId;
        insert oOpportunityTeamMember;
        return oOpportunityTeamMember;
    }
    
    //Create the PreDiscover
    public static Pre_discover__c CreatePre_discover(String strName)
    {
        Pre_discover__c oPre_discover = new Pre_discover__c();
        oPre_discover.Name = strName;
        oPre_discover.Stage__c = 'Dropped';
        insert oPre_discover;
        return oPre_discover;
    }
    
    //Create the NatureOfWork
    public static Id NatureOfWork(){
       Nature_of_Work__c nOWork = new Nature_of_Work__c();
       nOWork.Name = 'Test Data=';
       insert nOWork;
       return nOWork.Id;
    }
    
    //Create Service Line Lookup
    public static Id ServiceLineLookup(){
	   Service_Line__c sLine = new Service_Line__c();
	   sLine.Name = 'Test Lookup';
	   sLine.NS_Id__c = 'Test';
	   insert sLine;
	   return sLine.Id;     
    }
    
    //Create Product Family Lookup
    public static Id ProductFamilyLookup(){
	   Product_family__c pFamily = new Product_family__c();
	   pFamily.Name = 'Test Family';
	   pFamily.IsActive__c = True;
       pFamily.Product_Group__c = 'Group';
	   insert pFamily;
	   return pFamily.Id;     
    }    
    
    //Create the OpportunityProduct__c
    public static OpportunityProduct__c CreateOpportunityProduct(Id OpportunityId,Id ProductId,integer intContractTerminmonths,integer DecTCV_Local)
    {
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c='1';
        oOpportunityProduct.Service_line_OLI__c='COBAM';
        oOpportunityProduct.Digital_Assets__c = 'Accounts Receivable Management Suite';
        oOpportunityProduct.Product_family_Lookup__c = ProductFamilyLookup();
        oOpportunityProduct.Service_Line_lookup__c = ServiceLineLookup();
        oOpportunityProduct.Digital_Offerings__c = 'Yes';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = 'Core-Ops';
        oOpportunityProduct.Product__c = ProductId;
        oOpportunityProduct.COE__c = 'IMS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Auto';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().addDays(2);
        oOpportunityProduct.ContractTerminmonths__c = intContractTerminmonths;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        oOpportunityProduct.Revenue_for_Rolling_Year_1__c = 10000;
        oOpportunityProduct.TotalContractValue__c = 10000;
        oOpportunityProduct.Nature_of_Work_lookup__c = GEN_Util_Test_Data.NatureOfWork();
        oOpportunityProduct.Analytics__c = 'No';        
        oOpportunityProduct.LocalCurrency__c = 'USD';
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c = 2;
        oOpportunityProduct.FTE_7th_month__c = 2;
        oOpportunityProduct.FTE_10th_month__c = 2;
        oOpportunityProduct.Digital_Offerings__c = 'No';
        oOpportunityProduct.OpportunityId__c = OpportunityId;
        oOpportunityProduct.TCVLocal__c =DecTCV_Local   ;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
        return oOpportunityProduct;
    }
    
    //Create the OpportunityProduct__c with Errors
    public static OpportunityProduct__c CreateErrorOpportunityProduct(Id OpportunityId,Id ProductId,integer intContractTerminmonths,integer DecTCV_Local)
    {
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c='1';
        oOpportunityProduct.Service_line_OLI__c='COBAM';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = 'Core-Ops';
        oOpportunityProduct.Product__c = ProductId;
        oOpportunityProduct.COE__c = 'CMITS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Auto';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.ExchangeRate__c = 4.90;
        oOpportunityProduct.RevenueStartDate__c = System.today().addDays(-5);
        oOpportunityProduct.ContractTerminmonths__c = intContractTerminmonths;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        oOpportunityProduct.Revenue_for_Rolling_Year_1__c = 10000;
        oOpportunityProduct.TotalContractValue__c = 10000;
        oOpportunityProduct.Nature_of_Work_lookup__c = GEN_Util_Test_Data.NatureOfWork();
        oOpportunityProduct.Analytics__c = 'No';        
        oOpportunityProduct.LocalCurrency__c = Null;
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c = Null;
        oOpportunityProduct.FTE_7th_month__c = Null;
        oOpportunityProduct.FTE_10th_month__c = Null;
        oOpportunityProduct.DeliveryLocation__c = Null;
        oOpportunityProduct.Digital_Offerings__c = 'No';
        oOpportunityProduct.OpportunityId__c = OpportunityId;
        oOpportunityProduct.TCVLocal__c =DecTCV_Local   ;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
        return oOpportunityProduct;
    }
    
    //Create the Task
    public static Task CreateTask(string strAccountName,string strIndustry_Vertical, string strURL,string Sub_Industry_Vertifacal,String strStatus)
    {
        Task oTask = new Task();
        oTask.Account_Name__c = strAccountName;
        oTask.Ownerid = UserInfo.getUserId ();
        oTask.Industry_Vertical__c = strIndustry_Vertical;
        oTask.URL__c = strURL;
        oTask.Sub_Industry_Vertical__c = Sub_Industry_Vertifacal;
        oTask.Status = strStatus;
        oTask.Priority = 'Normal';
        insert oTask;
        return oTask;
    }
    
    //Create the Event
    public static Event CreateEvent(Id AccountId,Id RecordTypeId)
    {
        Event oEvent = new Event();
        oEvent.Visit_Type__c = 'Follow up visit';
        oEvent.StartDateTime = System.today();
        oEvent.EndDateTime = System.today();
        oEvent.WhatId = AccountId;
        oEvent.RecordTypeId = RecordTypeId;
        oEvent.Subject ='Email';
        insert oEvent;
        return oEvent;
    }
    
    //Create the RevenueSchedule__c
    public static RevenueSchedule__c CreateRevenueSchedule(Id OpprtunityProductId,integer intRollingYear,integer intNumberofInstallment,integer intReminderInstallment,decimal decRevenueLocal)
    {
        RevenueSchedule__c oRevenueSchedule  = new RevenueSchedule__c();
        // oRevenueSchedule.InstallmentPeriod__c = OpprtunityProductId;
        oRevenueSchedule.NumberOfInstallments__c = intNumberofInstallment;
        oRevenueSchedule.OpportunityProduct__c = OpprtunityProductId;
        oRevenueSchedule.RemainderInstallment__c  = intReminderInstallment;
        //oRevenueSchedule.Revenue__c = 2000;
        oRevenueSchedule.RollingYear__c = intRollingYear;
        //oRevenueSchedule.RollingYearRevenue__c  =  2013;
        oRevenueSchedule.RollingYearRevenueLocal__c = decRevenueLocal;
        //oRevenueSchedule.ScheduleType__c = 'Divide amount into multiple installment';
        oRevenueSchedule.StartDate__c = system.today().adddays(2);
        insert oRevenueSchedule;
        return oRevenueSchedule;
    }
    
    //Create the ProductSchedule__c
    public static ProductSchedule__c CreateProductSchedule(Id OpprtunityProductId,Id RevenueScheduleId)
    {
        ProductSchedule__c oProductSchedule  = new ProductSchedule__c();
        oProductSchedule.OpportunityProductId__c = OpprtunityProductId;
        oProductSchedule.Description__c = 'This is dummy data for test class';
        oProductSchedule.Quantity__c = 1;
        oProductSchedule.Revenue__c = 1000;
        oProductSchedule.RevenueLocal__c = 2000;
        oProductSchedule.RevenueScheduleId__c = RevenueScheduleId;
        oProductSchedule.ScheduleDate__c =  System.today().adddays(1);
        oProductSchedule.Type__c = 'Quantity';
        insert oProductSchedule;
        return oProductSchedule;
    }
    
    //Create the ProductSchedule__c
    public static Product_Schedule_Dummy__c CreateProductScheduleDummy(Id OpprtunityProductId,integer intMonthlyBreakup, integer intContractTerm)
    {
        
        Product_Schedule_Dummy__c oProductScheduleDummy=  new Product_Schedule_Dummy__c();  
        oProductScheduleDummy.Month_1_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_1_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_2_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_2_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_3_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_3_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_4_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_4_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_5_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_5_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_6_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_6_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_7_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_7_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_8_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_8_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_9_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_9_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_10_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_10_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_11_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_11_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_12_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_12_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_13_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_13_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_14_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_14_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_15_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_15_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_16_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_16_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_17_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_17_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_18_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_18_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_19_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_19_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_20_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_20_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_21_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_21_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_22_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_22_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_23_local__c = intMonthlyBreakup;  
        oProductScheduleDummy.Month_23_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_241_local__c = intMonthlyBreakup; 
        oProductScheduleDummy.Month_24_USD__c = intMonthlyBreakup; 
        oProductScheduleDummy.Name = 'test'; 
        oProductScheduleDummy.Product_Internal_ID__c = OpprtunityProductId; 
        oProductScheduleDummy.Revenue_start_Date__c= system.today().adddays(2);
        oProductScheduleDummy.Contract_Term__c= intContractTerm;
        return oProductScheduleDummy;
    }
    
    // Revenue Pricing Header
    public static RevenuePricingHeader__c CreateRevenuePricingHeader (Id AccountId,Id Userid,Id ContactId,Id OpportunityId)
    {
        RevenuePricingHeader__c oRevenuePricingHeader  = new RevenuePricingHeader__c();
        oRevenuePricingHeader.Name='test';
        oRevenuePricingHeader.Account_Deal_Name__c =AccountId;
        oRevenuePricingHeader.Pricing_FP_A_Contact__c=ContactId;   
        if(OpportunityId!=null)    
        oRevenuePricingHeader.CRM_Deal_Id__c=OpportunityId;
        oRevenuePricingHeader.Fx_Rate_for_currency_other_than_USD__c=0.0166;
        oRevenuePricingHeader.Type_of_Deal__c='None';
        oRevenuePricingHeader.Currency__c='CNY';
        oRevenuePricingHeader.Sales_Person__c= Userid;
        oRevenuePricingHeader.SOW_signature_date__c=system.today();
        oRevenuePricingHeader.contract_Term_in_months__c =4;
        insert oRevenuePricingHeader  ;
        return oRevenuePricingHeader ;
    }

    // Revenue Pricing
    public static Revenue_Pricing__c CreateRevenuePricing (Id RevenuePricingHeader)
    {
        Revenue_Pricing__c oRevenuePricing = new Revenue_Pricing__c();
        oRevenuePricing.Name='test1';
        oRevenuePricing.RevenuePricingHeader__c =RevenuePricingHeader;
        oRevenuePricing.CurrencyIsoCode='CNY';
        insert oRevenuePricing ;
        return oRevenuePricing ;
    }

    // Revenue Pricing Year	
    public static Revenue_Pricing_Year__c CreateRevenuePricingYear (Id RevenuePricing)
    {
        Revenue_Pricing_Year__c oRevenuePricingYear = new Revenue_Pricing_Year__c();
        oRevenuePricingYear.Name='test11';
        oRevenuePricingYear.Revenue_Pricing__c =RevenuePricing;
        oRevenuePricingYear.Revenue__c=4000;   
        oRevenuePricingYear.CurrencyIsoCode='CNY';     
        oRevenuePricingYear.Year__c=2013;           
        insert oRevenuePricingYear ;
        return oRevenuePricingYear ;
    }
    
    //Create QSRM
    public static QSRM__c CreateQSRM(Id OpportunityId,Id UserId)
    {
        QSRM__c oQSRM=new QSRM__c();
        
        oQSRM.Opportunity__c=OpportunityId;
        oQSRM.User_sales_leader__c=UserId;
        oQSRM.Corporate_Business_Obj_Goals_strategie__c='Test';
        oQSRM.Major_Business_Challenges__c='Test';
        oQSRM.Industry_Climate_Highlight__c='Stable';
        oQSRM.Preliminary_Value_Statement__c='Test';
        oQSRM.Formal_Decision_Making_Process__c='Test';
        oQSRM.Dept_Business_Obj_Goals_Strategies__c='Test';
        oQSRM.Client_s_Financial_Success_Measures__c='Test';
        oQSRM.Buying_Expectations_and_Criteria__c='Test';
        //oQSRM.Potential_Revenue__c=50000;
        oQSRM.Priority__c='NULL';
        //oQSRM.Condition_Applicable__c='Rengg with local staff<br>India delivery @15% AOI';
        oQSRM.Result__c='Does not meet blue print criteria';
        oQSRM.Incumbent_Reference_if_any__c='Test';
        insert oQSRM;
        return oQSRM;               
    }
    
    //New Account Creation Request
    public static Account_Creation_Request__c CreateNewAccountCreationRequest (Id UserId,string AccountName, string description  )
    {
        Account_Creation_Request__c oNewAccountCreationRequest = new Account_Creation_Request__c();
        oNewAccountCreationRequest.Name=AccountName;
        oNewAccountCreationRequest.Account_Owner__c=UserId;
        oNewAccountCreationRequest.Description__c=description;
        oNewAccountCreationRequest.Indutsry_Vertical__c='BFS';
        oNewAccountCreationRequest.Website__c='www.google.com';
        oNewAccountCreationRequest.Sub_Industry_Vertical__c='Commercial Banks';
        //oNewAccountCreationRequest.Status__c='completed';
        //oNewAccountCreationRequest.Priority__c='High';
        insert oNewAccountCreationRequest;
        return oNewAccountCreationRequest;
    }
    
    // Test Account Arche type
    public static AccountArchetype__c createNewAccountArchetype(Id UserId,Id AccountId,Id ArchetypeID)
    {
        AccountArchetype__c arrch= new  AccountArchetype__c();
        arrch.Account__c = AccountId;
        arrch.User__c = UserId;
        arrch.Archetype__c=ArchetypeId;
        insert arrch;
        return arrch;
    }
    
    // Opportunity Product Share
    public static OpportunityProduct__Share createShareOpportunityProduct(Id OpportunityId,Id UserId)
    {
       OpportunityProduct__Share os = new OpportunityProduct__Share();
       os.ParentId  = OpportunityId; 
       os.AccessLevel = 'Edit';
       os.UserOrGroupId = UserId;
       //os.RowCause = 'Owner';
       insert os;
       return os;
    }
}