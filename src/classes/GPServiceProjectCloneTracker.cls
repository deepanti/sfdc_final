@isTest
private class GPServiceProjectCloneTracker { 
    
    private static GP_Employee_Master__c employeeMaster;    
    private static List<GP_Project_Leadership__c> listOfProjectLeadership;
    private static GP_Leadership_Master__c leadershipMaster;
    public Static GP_Project__c project;
    public static GP_Project_Template__c projectTemplate;
    public static GP_Employee_Master__c supervisorempObj;
    public static GP_Project_Expense__c projectExpense;
    public static GP_Project_Document__c objPODocument;
    public static GP_Project_Document__c objOtherDocument;
    public static GP_Profile_Bill_Rate__c objProfileBillRate; 
    public static GP_Project_Budget__c objPrjBudjet1; 
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO1; 
    public static GP_Resource_Allocation__c objResourceAllocation1;
    public static GP_Project_Leadership__c projectLeadership1;
    public static GP_Project_Address__c objPrjAddress2;
    public static GP_Project_Version_History__c objPrjVHistory;
    public static User objuser;
    
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        /*GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        insert empHours ;*/  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Approved';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        project.GP_End_Date__c = System.today().addDays(10);
        project.GP_TCV__c = 10;
        project.GP_Oracle_PID__c = '4532';
        project.GP_OMS_Status__c = 'Approved';
        project.GP_Oracle_Status__c = 'S';
        
        insert project;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        objPrjBillingMilestone.GP_Project__c = project.id;
        insert objPrjBillingMilestone;
        
        projectExpense = GPCommonTracker.getProjectExpense(project.Id);
        insert projectExpense;
        
        objPODocument = GPCommonTracker.getProjectDocumentWithRecordType(project.Id, 'PO Document');
        objPODocument.GP_Project__c = project.Id;
        insert objPODocument;
	
        objOtherDocument = GPCommonTracker.getProjectDocumentWithRecordType(project.Id, 'Other Document');
        objOtherDocument.GP_Project__c = project.Id;
        insert objOtherDocument;
        
        objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Project__c = project.id;
        insert objProfileBillRate;
        
        objPrjBudjet1 = GPCommonTracker.getProjectBudget(project.id,objPrjBdgtMaster.id);
        insert objPrjBudjet1;
        
        objProjectWorkLocationSDO1 = GPCommonTracker.getProjectWorkLocationSDO(project.Id, sdo.Id);
        //objProjectWorkLocationSDO1.recordtypeId = gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        insert objProjectWorkLocationSDO1;
        
        objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,project.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
         
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        projectLeadership1 = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id);
        projectLeadership1.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership1.GP_Pinnacle_End_Date__c = system.today();
        projectLeadership1.GP_Project__c = projectObjForIcon.id;
        projectLeadership1.GP_Employee_ID__c = empObj.id;
        projectLeadership1.GP_Leadership_Role_Name__c = 'PID Approver';
        insert projectLeadership1;
        
        GP_Customer_Master__c customerObj = new GP_Customer_Master__c();
        customerObj.Name = 'New Customer';
        customerObj.GP_Oracle_Customer_Id__c = '1234';
        customerObj.GP_Is_Dummy__c = true;
        insert customerObj;
        
        objPrjAddress2 = new GP_Project_Address__c();
        objPrjAddress2.GP_Project__c = project.id;
        objPrjAddress2.GP_Relationship__c = 'Direct';
        objPrjAddress2.GP_Customer_Name__c = customerObj.id;
        insert objPrjAddress2;
        
        objPrjVHistory = new GP_Project_Version_History__c();
        insert objPrjVHistory;
    }
    
    public static void buildDependencyData2() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today();
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-30);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        /*GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        insert empHours ;*/  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Approved';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        project.GP_End_Date__c = System.today().addDays(10);
        project.GP_TCV__c = 10;
        project.GP_Oracle_PID__c = '4532';
        project.GP_OMS_Status__c = 'Approved';
        project.GP_Oracle_Status__c = 'S';
        project.GP_Last_Temporary_Record_Id__c = '12345';
        insert project;
        
        GP_Project__c  OldprjObj = project.clone(false, true);
        OldprjObj.name = 'test 2';
        insert OldprjObj;
        
        List<GP_Project__c> lstProj = new List<GP_Project__c>();
        lstProj.add(project);
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        insert objPrjBillingMilestone;
        
        Map < Id, Id > mapOfOldProjectIdVsNewProjectId = new Map < Id, Id >();
        mapOfOldProjectIdVsNewProjectId.put(project.Id, OldprjObj.Id);
        
        GPServiceProjectClone objClass = new GPServiceProjectClone();
        GPServiceProjectClone objRes1 = objClass.setMapOfOldProjectIdVsNewProjectId(mapOfOldProjectIdVsNewProjectId);
        
        GPWrapperApprovalValidatorConfig objConfig1 = new GPWrapperApprovalValidatorConfig();
        GPServiceProjectClone objRes2 = objClass.setProjectApprovalValidatorConfig(objConfig1);
        
        GPServiceProjectClone objRes3 = objClass.setRelatedProjectRecord(project);
        
        Map < String, List < String >> mapOfResponse = objClass.cloneProject(lstProj, 'Test', false, false);
        //Map < String, List < String >> mapOfResponse2 = objClass.cloneProject(lstProj, 'Test', true, true);
        
        
    }
    
    @isTest
    public static void testupdateEmployeeEndDateInOldVersion() {
        buildDependencyData();
        fetchData();
        createProjectLeadership();
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        
        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    } 
    
     @isTest
    public static void testupdateEmployeeEndDateInOldVersion3() {
        buildDependencyData2();
        fetchData();
        createProjectLeadership();
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        
        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    } 
    
    static void fetchData() { 
        project = [select id, 
                   Name, RecordType.Name,
                   GP_HSL__r.Parent_HSL__c,
                   GP_Start_Date__c,GP_End_Date__c,
                   GP_Last_Temporary_Record_Id__c,
                   GP_Deal__r.GP_Deal_Category__c, 
                   GP_Project_Template__c,                   
                   GP_Primary_SDO__c, GP_Project_Stages_for_Approver__c, GP_Approval_Status__c, GP_GPM_Start_Date__c 
                  from GP_Project__c limit 1]; 
        
        projectTemplate = [Select Id, GP_Final_JSON_1__c, GP_Final_JSON_2__c, GP_Final_JSON_3__c FROM GP_Project_Template__c LIMIT 1];
        
        employeeMaster = [SELECT ID, Name, GP_HIRE_Date__c, GP_ACTUAL_TERMINATION_Date__c From GP_Employee_Master__c LIMIT 1];
    }
    
    private static void createProjectLeadership() {
        Date todayDate = System.today();
        listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        List<GP_Leadership_Master__c> listOfLeadershipMaster = new List<GP_Leadership_Master__c>();
        
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        listOfLeadershipMaster.add(leadershipMaster);
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Account', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Account';
        
        listOfLeadershipMaster.add(leadershipMaster);

        insert listOfLeadershipMaster;
        
        for(GP_Leadership_Master__c leadershipMaster: listOfLeadershipMaster) {
           
            GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            projectClonedLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            projectLeadership.Is_Cloned_from_Parent__c = false;
            projectLeadership.GP_Employee_ID__r = employeeMaster;
            projectLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectClonedAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectClonedAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectClonedAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectClonedAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectClonedAccountLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedAccountLeadership.GP_Active__c = false;
            
            
            GP_Project_Leadership__c projectAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectAccountLeadership.Is_Cloned_from_Parent__c = false;
            projectAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectAccountLeadership.GP_Active__c = false;
            
            listOfProjectLeadership.add(projectLeadership);
            listOfProjectLeadership.add(projectClonedLeadership);
            
            listOfProjectLeadership.add(projectAccountLeadership);
            listOfProjectLeadership.add(projectClonedAccountLeadership);
        }
        
        insert listOfProjectLeadership;
        /*
        List<GP_Project_Leadership__c> listOfClonedProjectLeadership = new List<GP_Project_Leadership__c>();
        
        for(GP_Project_Leadership__c projectLeadership : listOfProjectLeadership) {
            
            projectLeadership.GP_Parent_Project_Leadership__c = projectLeadership.Id;
            projectLeadership.Id = null;
            projectLeadership.Is_Cloned_from_Parent__c = false;
            
            listOfClonedProjectLeadership.add(projectLeadership);
        }
        
        insert listOfClonedProjectLeadership;*/
    }
    
    @isTest
    public static void testupdateEmployeeEndDateInOldVersion2() {
        buildDependencyData();
        fetchData(); 
        
        Date todayDate = System.today();
        listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        List<GP_Leadership_Master__c> listOfLeadershipMaster = new List<GP_Leadership_Master__c>();
        
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        listOfLeadershipMaster.add(leadershipMaster);
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Account', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Account';
        
        listOfLeadershipMaster.add(leadershipMaster);

        insert listOfLeadershipMaster;
        
        GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
        projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
        projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
        projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
        projectClonedLeadership.Is_Cloned_from_Parent__c = true;
        projectClonedLeadership.GP_Employee_ID__r = employeeMaster;
        projectClonedLeadership.GP_Active__c = false;
        listOfProjectLeadership.add(projectClonedLeadership);
        insert projectClonedLeadership;
        
        GPServiceProjectLeadership.project = project;
        GPServiceProjectLeadership.projectTemplate = projectTemplate;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateEmployeeOnLeadership(new List<Id> {project.Id});
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.validateActiveEmployeeOnLeadership(listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isFormattedLogRequired = false;
        GPServiceProjectLeadership.isLogForTemporaryId = true;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.isLogForTemporaryId = false;
        GPServiceProjectLeadership.customValidateProjectLeadershipLeadership(project, listOfProjectLeadership);
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-Employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        //supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        
        listOfProjectLeadership[0].GP_Employee_ID__c = supervisorempObj.id;
        GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        listOfProjectLeadership[0].Is_Cloned_from_Parent__c = true;
        listOfProjectLeadership[0].GP_Leadership_Role__c = 'PROJECT MANAGER';
         GPServiceProjectLeadership.getUpdatedProjectLeadership(project.Id, 
                                                               listOfProjectLeadership, 
                                                               new List<GP_Project_Leadership__c>(),
                                                              new Map<String, String>());
        
        GPServiceProjectLeadership.validateEmployeeOnLeadership(listOfProjectLeadership,listOfLeadershipMaster);
        try {
            GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(null);
        } catch(Exception ex) {}
    }
    
     @isTest
    public static void testGPServiceProjectClone() {
        buildDependencyData();
        GPServiceProjectClone objPrjClone = new GPServiceProjectClone();
        List<GP_Project__c> listOfPrj = new List<GP_Project__c>();
        listOfPrj.add(project);
        System.debug('anmol: project'+ project);
        map<id,id> mapOfPrjID = new map<id,id>();
        System.debug('anmol 2 '+ project);
        
        mapOfPrjID.put(project.id, project.id);
        objPrjClone.setMapOfOldProjectIdVsNewProjectId(mapOfPrjID);
        
        objPrjClone.setProjectApprovalValidatorConfig(new GPWrapperApprovalValidatorConfig());
        
        objPrjClone.cloneProject(listOfPrj,'Manual',false,false);
        objPrjClone.cloneProject(listOfPrj,'Manual',true,true);
        List<GP_Project_Expense__c> listOfPrjExp = new List<GP_Project_Expense__c>();
        listOfPrjExp.add(projectExpense);
        
        GPControllerProjectApprovalValidator objApVal = new GPControllerProjectApprovalValidator(project.id);
        GPControllerProjectApprovalValidator objApVal2 = new GPControllerProjectApprovalValidator(project);
        projectLeadership1.GP_Active__c = true;
        GPControllerProjectApprovalValidator objApVal3 = new 
            GPControllerProjectApprovalValidator(new List<GP_Project_Leadership__c>{projectLeadership1});
        GPControllerProjectApprovalValidator objApVal4 = new 
            GPControllerProjectApprovalValidator(new List<GP_Project_Work_Location_SDO__c>{objProjectWorkLocationSDO1});
        GPControllerProjectApprovalValidator objApVal5 = new 
            GPControllerProjectApprovalValidator(new List<GP_Project_Document__c>{objOtherDocument});
        GPControllerProjectApprovalValidator objApVal6 = new 
            GPControllerProjectApprovalValidator(new List<GP_Resource_Allocation__c>{objResourceAllocation1});
        GPControllerProjectApprovalValidator objApVal7 = new 
            GPControllerProjectApprovalValidator(new List<GP_Billing_Milestone__c>{objPrjBillingMilestone});
        GPControllerProjectApprovalValidator objApVal8 = new 
            GPControllerProjectApprovalValidator(new List<GP_Project_Budget__c>{objPrjBudjet1});
        GPControllerProjectApprovalValidator objApVal9 = new GPControllerProjectApprovalValidator(listOfPrjExp);
        GPControllerProjectApprovalValidator objApVal10 = new 
            GPControllerProjectApprovalValidator(new List<GP_Project_Address__c>{objPrjAddress2});
        
        
        
        objPrjClone.setRelatedProjectRecord(project);
        System.debug('listOfPrjExp: ' + listOfPrjExp);
        try{objPrjClone.cloneProjectExpense(listOfPrjExp,true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectExpense(listOfPrjExp,false,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectDocument(new List<GP_Project_Document__c>{objOtherDocument},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectBillRate(new List<GP_Profile_Bill_Rate__c>{objProfileBillRate},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectBudget(new List<GP_Project_Budget__c>{objPrjBudjet1},true,true);} catch(Exception e){}
        
        try{objPrjClone.cloneProjectBillingMileStone(new List<GP_Billing_Milestone__c>{objPrjBillingMilestone},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectWorkLocationAndSDO(new List<GP_Project_Work_Location_SDO__c>{objProjectWorkLocationSDO1},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectResourceAllocations(new List<GP_Resource_Allocation__c>{objResourceAllocation1},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectLeadership(new List<GP_Project_Leadership__c>{projectLeadership1},true,true);} catch(Exception e){}
        try{objPrjClone.cloneProjectAddress(new List<GP_Project_Address__c>{objPrjAddress2},true,true);} catch(Exception e){}
        
        try{objPrjClone.cloneProjectVersionHistory(objPrjVHistory,1);} catch(Exception e){}
        
        try{objPrjClone.cloneProjectDocument(new List<GP_Project_Document__c>{objOtherDocument},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectBillRate(new List<GP_Profile_Bill_Rate__c>{objProfileBillRate},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectBudget(new List<GP_Project_Budget__c>{objPrjBudjet1},false,false);} catch(Exception e){}
        
        try{objPrjClone.cloneProjectBillingMileStone(new List<GP_Billing_Milestone__c>{objPrjBillingMilestone},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectWorkLocationAndSDO(new List<GP_Project_Work_Location_SDO__c>{objProjectWorkLocationSDO1},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectResourceAllocations(new List<GP_Resource_Allocation__c>{objResourceAllocation1},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectLeadership(new List<GP_Project_Leadership__c>{projectLeadership1},false,false);} catch(Exception e){}
        try{objPrjClone.cloneProjectAddress(new List<GP_Project_Address__c>{objPrjAddress2},false,false);} catch(Exception e){}
    }
}