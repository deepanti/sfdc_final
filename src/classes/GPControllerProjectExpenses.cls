/**
 * @group Project Expense
 * @group-content ../../ApexDocContent/ProjectExpense.htm
 *
 * @description Apex controller class having business logics
 *              to fetch, insert, update and delete expenses under a project.
 */
public class GPControllerProjectExpenses {

    private final String IS_EXPENSE_NATIVELY_AVAILABLE_LABEL = 'isExpenseNativelyAvailable';
    private final String LIST_OF_EXPENDITURE_TYPE_LABEL = 'listOfExpenditureType';
    private final String LIST_OF_EXPENSE_CATEGORY_LABEL = 'listOfExpenseCategory';
    private final String LIST_OF_PROJECT_EXPENSE_LABEL = 'listOfProjectExpense';
    private final String LIST_OF_LOCATION_LABEL = 'listOfLocation';
    private final String BILLING_CURR_LABEL = 'billingCurrency';
    private final String ISUPDATEABLE_LABEL = 'isUpdatable';
    private final String IS_OMS_TYPE_LABEL = 'isOMSType';
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String PROJECT_LABEL = 'project';

    private List < GPOptions > listOfExpenseCategory, listOfLocation, listOfExpenditureType;
    private List < GP_Project_Expense__c > listOfProjectExpense;
    private Boolean isExpenseNativelyAvailable;

    private Decimal nextExpenseCounter;
    private String strbillingCurrency;
    private GP_Project__c projectObj;
    private Boolean isUpdatable;
    private JSONGenerator gen;
    private Id projectId;

    public GPControllerProjectExpenses() {}

    /**
     * @description Parameterized constructor to accept Project Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerProjectExpenses(<SFDC 18/15 digit project Id>);
     */
    public GPControllerProjectExpenses(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description Parameterized constructor to accept Project Expense List.
     * @param listOfProjectBudget List Of Project Expense.
     * 
     * @example
     * new GPControllerProjectExpenses(<List of Project Expense>);
     */
    public GPControllerProjectExpenses(List < GP_Project_Expense__c > listOfProjectExpense, Decimal nextExpenseCounter, Id projectId) {
        this.listOfProjectExpense = listOfProjectExpense;
        this.nextExpenseCounter = nextExpenseCounter;
        this.projectId = projectId;
    }

    /**
     * @description method to upsert Project Expense Records.
     * 
     * @example
     * new GPControllerProjectExpenses(<List Of Project Expense>).saveProjectExpense();
     */
    public GPAuraResponse saveProjectExpense() {
        Savepoint sp;

        try {
            sp = Database.setSavePoint();
            upsert listOfProjectExpense;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJECT EXPENSE');
            project.GP_Next_Expense_Counter__c = nextExpenseCounter;
            project.Id = projectId;
			
            update project;
        } catch (Exception ex) {
            Database.rollback(sp);
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(listOfProjectExpense, true));
    }

    /**
     * @description method to delete Project Expense Records.
     * 
     * @example
     * new GPControllerProjectExpenses(<SFDC 18/15 digit project Id>).deleteProjectExpense(<SFDC 18/15 digit project expense Id>);
     */
    public GPAuraResponse deleteProjectExpense(Id projectExpenseId) {
        try {
              GP_Project_Expense__c expense = new GPSelectorProjectExpense().selectProjectExpenseRecord(projectExpenseId);
            projectId = expense.GP_Project__c;
           delete expense;
            
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJECT EXPENSE');
            update project; 
        } catch (Exception ex) {
            return new GPAuraResponse(false, 'ERROR', null);
        }
        return new GPAuraResponse(true, 'SUCCESS', null);
    }

    /**
     * @description method to return Project Expense.
     *
     * @example
     * new GPControllerProjectExpenses(<SFDC 18/15 digit project Id>).getProjectExpenseRecord();
     */
    @testVisible
    public GPAuraResponse getProjectExpenseRecord() {
        try {
            setProject();
            setProjectExpense();
            setProjectExpenseCategory();
            setProjectExpenditureType();
            setProjectLocation();
            getBillingCurrency();

            if (isProjectExpenseNativelyAvailable()) {
                setJson();
            } else {
                getOMSProjectExpenseRecord();
            }

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description method to return OMS Project Expense.
     *
     * @example
     * new GPControllerProjectExpenses(<SFDC 18/15 digit project Id>).getOMSProjectExpenseRecord();
     */
    @testVisible
    public GPAuraResponse getOMSProjectExpenseRecord() {
        try {
            isExpenseNativelyAvailable = true;
            setProject();
            setOMSJson();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description method to return Billing Currency of Project.
     *
     */
    private void getBillingCurrency() {
        list < GP_Project__c > lstProjects = new GPSelectorProject().getBillingCurrencyFromProject(projectId);
        if (!lstProjects.isEmpty()) {
            strbillingCurrency = lstProjects[0].GP_Billing_Currency__c;
        }
    }

    /**
     * @description Set Project Expense Category picklist.
     * 
     */
    private void setProjectExpenseCategory() {
        listOfExpenseCategory = GPInputPicklistService.getPicklistOptions('GP_Project_Expense__c', 'GP_Expense_Category__c');
    }

    /**
     * @description Set Project Expenditure Type picklist.
     * 
     */
    private void setProjectExpenditureType() {
        listOfExpenditureType = GPInputPicklistService.getPicklistOptions('GP_Project_Expense__c', 'GP_Expenditure_Type__c');
    }

    /**
     * @description Set Project Worklocation picklist.
     * 
     */
    private void setProjectLocation() {
        listOfLocation = GPInputPicklistService.getPicklistOptions('GP_Project_Expense__c', 'GP_Location__c');
    }

    /**
     * @description Query Project Expense Records.
     *
     */
    private void setProjectExpense() {
        listOfProjectExpense = new GPSelectorProjectExpense().selectProjectExpenseRecords(projectId);
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        setCommonFields();
        gen.writeBooleanField(IS_OMS_TYPE_LABEL, false);
        gen.writeEndObject();
    }


    /**
     * @description Set GPAuraResponse object for OMS type.
     * 
     */
    private void setOMSJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        setCommonFields();
        gen.writeBooleanField(IS_OMS_TYPE_LABEL, true);
        gen.writeEndObject();
    }

    /**
     * @description Set common fields for GPAuraResponse.
     * 
     */
    private void setCommonFields() {
        if (isExpenseNativelyAvailable != null)
            gen.writeBooleanField(IS_EXPENSE_NATIVELY_AVAILABLE_LABEL, isExpenseNativelyAvailable);

        if (listOfExpenseCategory != null && listOfExpenseCategory.size() > 0)
            gen.writeObjectField(LIST_OF_EXPENSE_CATEGORY_LABEL, listOfExpenseCategory);

        if (listOfExpenditureType != null && listOfExpenditureType.size() > 0)
            gen.writeObjectField(LIST_OF_EXPENDITURE_TYPE_LABEL, listOfExpenditureType);

        if (listOfProjectExpense != null && listOfProjectExpense.size() > 0)
            gen.writeObjectField(LIST_OF_PROJECT_EXPENSE_LABEL, listOfProjectExpense);

        if (listOfLocation != null && listOfLocation.size() > 0)
            gen.writeObjectField(LIST_OF_LOCATION_LABEL, listOfLocation);

        if (isUpdatable != null)
            gen.writeObjectField(ISUPDATEABLE_LABEL, isUpdatable);

        if (strbillingCurrency != null)
            gen.writeObjectField(BILLING_CURR_LABEL, strbillingCurrency);

        if (projectObj != null)
            gen.writeObjectField(PROJECT_LABEL, projectObj);

    }

    /**
     * @description Check for OMS type.
     *
     */
    private Boolean isProjectExpenseNativelyAvailable() {

        setProject();

        isExpenseNativelyAvailable = !(projectObj.RecordType.Name == 'CMITS' &&
            (projectObj.GP_Project_type__c == 'Fixed Price' ||
                projectObj.GP_Project_type__c == 'Fixed monthly'));

        return isExpenseNativelyAvailable;
    }

    /**
     * @description Query Project Record and associated class members.
     *
     */
    private void setProject() {
        projectObj = new GPSelectorProject().selectProjectRecord(projectId);
        isUpdatable = projectObj.GP_Approval_Status__c == 'Approved' ? false : true;
    }
}