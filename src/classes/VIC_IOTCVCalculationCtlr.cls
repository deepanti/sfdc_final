/*
    @Description: Class will be used for handling all IO Type TCV
    @Test Class: TCVAcceleratorCtlrTest
    @Author: Vikas Rajput
*/
public class VIC_IOTCVCalculationCtlr implements VIC_ScorecardCalculationInterface{
    public Decimal pmAcheivmentPercent{get;set;}
    public Decimal opAcheivmentPercent{get;set;}
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    public Map <Id, User_VIC_Role__c> mapUserIdToUserVicRoles{get;set;}
    
    /*
        @Description: Overriding calculate function for getting target payout
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal calculate(Target_Component__c objTargetCmp,  List<VIC_Calculation_Matrix_Item__c> lstMatrixItem){
        Set<String> plansRenewalIncentive = new Set<String>{'SL_CP_Enterprise','SL_CP_CMIT'}; //Adding plan for which need a target calculation
        if(plansRenewalIncentive.contains(objTargetCmp.Target__r.Plan__r.vic_Plan_Code__c)
            && objTargetCmp.Target__r.Target_Bonus__c != null && objTargetCmp.Weightage__c != null){
            
            Decimal iOTCVTargetedIncentive  = (objTargetCmp.Target__r.Target_Bonus__c*objTargetCmp.Weightage__c)/100;
            //NOT FOR "SL" && "CM/ITO"
            if(mapUserIdToUserVicRoles.containsKey(objTargetCmp.Target__r.User__c) && 
                !(mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Role__c == 'SL' && 
                  mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Horizontal__c == 'CM/ITO')){
    
                if(opAcheivmentPercent <= Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_OP_Percent').vic_Value__c)){
                    Decimal acheivmentPercent = objTargetCmp.vic_Achievement_Percent__c > 100?100:objTargetCmp.vic_Achievement_Percent__c;
                    return (iOTCVTargetedIncentive*fetchPayout(acheivmentPercent, lstMatrixItem))/100;
                }else{
                    return (iOTCVTargetedIncentive*fetchPayout(objTargetCmp.vic_Achievement_Percent__c, lstMatrixItem))/100;
                }
            }else{    //EXIST FOR "SL" && "CM/ITO"            IO_TCV_PM_Percent = 95     AND   IO_TCV_OP_Percent = 90
                if((pmAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_PM_Percent').vic_Value__c) && 
                        opAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_OP_Percent').vic_Value__c))){
                    return (iOTCVTargetedIncentive*fetchPayout(objTargetCmp.vic_Achievement_Percent__c, lstMatrixItem))/100;
                }else{
                    Decimal acheivmentPercent = objTargetCmp.vic_Achievement_Percent__c > 100?100:objTargetCmp.vic_Achievement_Percent__c;
                    return (iOTCVTargetedIncentive*fetchPayout(acheivmentPercent, lstMatrixItem))/100;
                }
            } 
        }
        return 0.00;
    }
    /*
        @Description: It will return additional payout from Calculation Matrix Items
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal fetchPayout(Decimal achievementParcent, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        Decimal dcmValue = 0.00;
        for(VIC_Calculation_Matrix_Item__c objCalMatrixItem : lstCalcMatrixItem){
            if((achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && objCalMatrixItem.vic_End_Value__c != null)&& achievementParcent >= objCalMatrixItem.vic_Start_Value__c && achievementParcent >= objCalMatrixItem.vic_End_Value__c){
                Decimal tempValue = ((objCalMatrixItem.vic_End_Value__c - objCalMatrixItem.vic_Start_Value__c)+1);
                dcmValue = calcAdditionalPayout(tempValue, objCalMatrixItem, dcmValue);
             //   System.debug('----------------------IOTCV-----------------------1dcmValue=='+dcmValue);
            }else if(achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && objCalMatrixItem.vic_End_Value__c != null && achievementParcent >= objCalMatrixItem.vic_Start_Value__c && achievementParcent<= objCalMatrixItem.vic_End_Value__c){
                Decimal tempValue = ((achievementParcent - objCalMatrixItem.vic_Start_Value__c)+1);
                //Decimal tempValue = ((objCalMatrixItem.vic_End_Value__c - objCalMatrixItem.vic_Start_Value__c)+1);
                dcmValue = calcAdditionalPayout(tempValue, objCalMatrixItem, dcmValue);
             //   System.debug('----------------------IOTCV-----------------------2dcmValue=='+dcmValue);
            }else if((achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && achievementParcent >= objCalMatrixItem.vic_Start_Value__c ) && (objCalMatrixItem.vic_End_Value__c == null )){
                Decimal tempValue = ((achievementParcent - objCalMatrixItem.vic_Start_Value__c)+1); //Comment this line if additional payout is not check in VIC Matrix Item
                dcmValue = calcAdditionalPayout(tempValue, objCalMatrixItem,dcmValue);
             //   System.debug('----------------------IOTCV-----------------------3dcmValue=='+dcmValue);
            }
             //   System.debug('----------------------IOTCV-----------------------TOTAL_Payout=='+dcmValue);
        }
        return dcmValue;
    } 
    /*
        @Description: It is helper function for fetchPayout(function of current class)
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal calcAdditionalPayout(Decimal tempValue, VIC_Calculation_Matrix_Item__c objCalMatrixItem, Decimal totalPayout){
        if(objCalMatrixItem.Is_Additional_Payout_Applicable__c && objCalMatrixItem.For_Each__c != null && 
                objCalMatrixItem.Additional_Payout__c != null && objCalMatrixItem.For_Each__c != 0){
            Decimal calcValue = (tempValue.round(System.RoundingMode.DOWN))/objCalMatrixItem.For_Each__c;
            return (totalPayout + (calcValue*objCalMatrixItem.Additional_Payout__c));
        }else{
            return objCalMatrixItem.vic_Payout__c;
        }
    }
    
}