public with sharing class OppRegistrationHandler {
    //public static Boolean firstRun 			= TRUE;
    
    public static void populateGAContact(List<Opportunity_Registration__c> newOppReg) {
        
        List<Opportunity_Registration__c> oppRegList	= new List<Opportunity_Registration__c>();
        
        for(Opportunity_Registration__c opReg : newOppReg) {
            opReg.GA_Contact__c			=	[SELECT contact.account.GA_Contact__c FROM User WHERE Id = :userinfo.getuserid() LIMIT 1].contact.account.GA_Contact__c;
        }
    }
}