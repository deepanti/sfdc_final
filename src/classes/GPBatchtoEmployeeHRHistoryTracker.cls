@isTest public class GPBatchtoEmployeeHRHistoryTracker {
	@testSetup static void setupCommonData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Employee_Master__c empObjforsupervisor3 = GPCommonTracker.getEmployee();
        empObjforsupervisor3.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Prior_Days_of_exiting_date));
        empObjforsupervisor3.GP_SFDC_User__c = objuser.id;
        empObjforsupervisor3.GP_Person_ID__c = '1233';
        insert empObjforsupervisor3;

        GP_Employee_HR_History__c emphistoryobj3 = new GP_Employee_HR_History__c();
        emphistoryobj3.GP_Employees__c = empObjforsupervisor3.id;
        emphistoryobj3.GP_Supervisor_Employee__c = empObjforsupervisor3.id;
        emphistoryobj3.GP_ASGN_EFFECTIVE_START__c = System.Today().adddays(-2);
        emphistoryobj3.GP_ASGN_EFFECTIVE_END__c = System.Today().adddays(2);
        insert emphistoryobj3;

        GP_Employee_Master__c empObjforsupervisor = GPCommonTracker.getEmployee();
        empObjforsupervisor.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Prior_Days_of_exiting_date));
        empObjforsupervisor.GP_OFFICIAL_EMAIL_ADDRESS__c = '123@abc.com';
        empObjforsupervisor.GP_Employee_HR_History__c = emphistoryobj3.ID;
        empObjforsupervisor.GP_Person_ID__c = '123';
        insert empObjforsupervisor;

        GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        exceptionemp.GP_Person_ID__c = '1230';
        exceptionemp.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Prior_Days_of_exiting_date));
        insert exceptionemp;

        GP_Employee_HR_History__c emphistoryobj = new GP_Employee_HR_History__c();
        emphistoryobj.GP_Employees__c = empObjforsupervisor.id;
        emphistoryobj.GP_Supervisor_Employee__c = empObjforsupervisor.id;
        insert emphistoryobj;

        GP_Employee_Master__c empObjforsupervisor2 = GPCommonTracker.getEmployee();
        empObjforsupervisor2.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforsupervisor2.GP_SFDC_User__c = objuser.id;
        empObjforsupervisor2.GP_Person_ID__c = '1232';
        insert empObjforsupervisor2;

        GP_Employee_HR_History__c emphistoryobj2 = new GP_Employee_HR_History__c();
        emphistoryobj2.GP_Employees__c = empObjforsupervisor2.id;
        emphistoryobj2.GP_Supervisor_Employee__c = empObjforsupervisor2.id;
        insert emphistoryobj2;

        GP_Employee_Master__c empObjforsecondary = GPCommonTracker.getEmployee();
        empObjforsecondary.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Second_Prior_Days_of_exiting_date));
        empObjforsecondary.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforsecondary.GP_Person_ID__c = '1235';
        insert empObjforsecondary;

        GP_Employee_Master__c empObjforsecondary2 = GPCommonTracker.getEmployee();
        empObjforsecondary2.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Second_Prior_Days_of_exiting_date));
        empObjforsecondary2.GP_Employee_HR_History__c = emphistoryobj2.ID;
        empObjforsecondary2.GP_Person_ID__c = '12329';
        insert empObjforsecondary2;

        GP_Employee_Master__c empObjforthird = GPCommonTracker.getEmployee();
        empObjforthird.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Third_Prior_Days_of_exiting_date));
        empObjforthird.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforthird.GP_Person_ID__c = '12356';
        insert empObjforthird;

        GP_Employee_Master__c empObjforthird2 = GPCommonTracker.getEmployee();
        empObjforthird2.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Third_Prior_Days_of_exiting_date));
        empObjforthird2.GP_Employee_HR_History__c = emphistoryobj2.ID;
        empObjforthird2.GP_Person_ID__c = '12357';
        insert empObjforthird2;

    }

    @isTest public static void testMethod1() {
        GPBatchtoEmployeeHRHistory batcher = new GPBatchtoEmployeeHRHistory();
        Id batchprocessid = Database.executeBatch(batcher, 10);
    }
}