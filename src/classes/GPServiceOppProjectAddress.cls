/**
* @author Salil.Sharma
* @date 11/01/2018
*
* @group OpportunityProjectLeadership
* @group-content ../../ApexDocContent/OpportunityProjectLeadership.html
*
* @description Apex Service for Opportunity project Leadership module,
* has methods to create Opportunity project Leadership data on project approval.
*/
public without sharing class GPServiceOppProjectAddress {
      private static final String SUCCESS_LABEL = 'SUCCESS';

    private List<Id> listOfOppProjectId;
    private List<GP_Opportunity_Project__c> listOfOppProject;
    private List<GP_Opp_Project_Address__c> listOfOppProjectAddress;
    private List<GP_Opp_Project_Default__mdt> listOfOppProjectLeadershipDefaults;
    
    /**
    * @description Creates record of opp project leadership.
    *  
    * @param Id Opportunity Project Id
    * @example
    * new GPServiceOppProjectAddress({oppProjectId});
    */
    public GPServiceOppProjectAddress(List<Id> listOfOppProjectId) {
        this.listOfOppProjectId = listOfOppProjectId;
    }

    public GPServiceOppProjectAddress(List<GP_Opportunity_Project__c> listOfOppProject) {
        this.listOfOppProject = listOfOppProject;
    }
    
    /**
    * @description Creates record of opp project address.
    * Records are created based on records in
    * GP_Opp_Project_Default__mdt - 'Opp Proj Address' record type
    *  
    * @example
    * new GPServiceOppProjectAddress({oppProjectId}).createOppProjectAddress();
    */
    public GPResponseWrapper createOppProjectAddress(){
        listOfOppProjectAddress = new List<GP_Opp_Project_Address__c>();
        setDefaultOppProjectLeadership();

        if(listOfOppProject == null) {
            setOpportunityProject();
        }

        for(GP_Opportunity_Project__c oppProject: listOfOppProject) {
            setOppProjectAddress(oppProject);
        }

        if(listOfOppProjectAddress == null || 
            listOfOppProjectAddress.isEmpty()) {
            return new GPResponseWrapper(0, SUCCESS_LABEL);
        }

        try {
            insert listOfOppProjectAddress;
        } catch(Exception ex) {
            return new GPResponseWrapper(-1, ex.getMessage());
        }
        
        return new GPResponseWrapper(0, SUCCESS_LABEL);
    }
    
    private void setOpportunityProject() {
        listOfOppProject = GPSelectorOppProjectLeadership.getOppProjectLeadershipUnderOppProject(listOfOppProjectId);
    }
    
    private void setOppProjectAddress(GP_Opportunity_Project__c oppProject) {

        for(GP_Opp_Project_Default__mdt defaultOppProjectLeadership :listOfOppProjectLeadershipDefaults) {

            GP_Opp_Project_Address__c oppProjectAddress = new GP_Opp_Project_Address__c();

            oppProjectAddress.GP_Relationship__c    = defaultOppProjectLeadership.GP_Address_Relationship__c;
            oppProjectAddress.GP_Bill_To_Address_Id__c = defaultOppProjectLeadership.GP_Bill_To_Address_Id__c;
            oppProjectAddress.GP_Ship_To_Address_Id__c = defaultOppProjectLeadership.GP_Ship_To_Address_Id__c;
            oppProjectAddress.GP_Customer_Name__c   = defaultOppProjectLeadership.GP_Customer_Id__c;
            oppProjectAddress.GP_Account_Id__c = defaultOppProjectLeadership.GP_Customer_Id__c;
            oppProjectAddress.GP_Opp_Project__c     = oppProject.Id;

            listOfOppProjectAddress.add(oppProjectAddress);
        }
    }

    private void setDefaultOppProjectLeadership() {
        listOfOppProjectLeadershipDefaults = [Select GP_Customer_Id__c,
                                                     GP_Bill_To_Address_Id__c,
                                                     GP_Ship_To_Address_Id__c,
                                                     GP_Address_Relationship__c
                                                FROM GP_Opp_Project_Default__mdt
                                                Where GP_Record_Type__c = 'Opp Project Address'];
    }
}