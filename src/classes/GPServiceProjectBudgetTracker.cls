@isTest
public class GPServiceProjectBudgetTracker {
   public static GP_Project__c prjObj,objChildProj;
  public  static list<GP_Project_Budget__c  > lstofProjBudget;
    
    @isTest
    public static void testProject() 
    {
        createData();
        lstofProjBudget = new list<GP_Project_Budget__c  >();
        GP_Project_Budget__c objPrjBdgt =  new GP_Project_Budget__c(); 
        objPrjBdgt.GP_Cost_Rate__c = 11;
        objPrjBdgt.GP_Band__c = 'GP_Band__c';
        objPrjBdgt.GP_Product__c = 'GP_Product__c';
        objPrjBdgt.GP_Country__c =  'India';
        objPrjBdgt.GP_Effort_Hours__c = 10;
        objPrjBdgt.GP_TCV__c =  111;
        objPrjBdgt.GP_Total_Cost__c = 11;
        objPrjBdgt.GP_OMS_Id__c = '1';      
        objPrjBdgt.GP_Project__c = prjObj.ID;
        lstofProjBudget.add(objPrjBdgt);
        GP_Project_Budget__c objPrjBdgt1 =  new GP_Project_Budget__c(); 
        objPrjBdgt1.GP_Cost_Rate__c = 11;
        objPrjBdgt1.GP_Country__c =  'India';
        objPrjBdgt.GP_TCV__c =  111;
        objPrjBdgt1.GP_Total_Cost__c = 11;
        objPrjBdgt1.GP_OMS_Id__c = '2';     
        objPrjBdgt1.GP_Project__c = prjObj.ID;
        lstofProjBudget.add(objPrjBdgt1);
        
        
        string str = GPServiceProjectBudget.UpdateProjectBudget(prjObj.ID,lstofProjBudget);
        GPServiceProjectBudget.validateProjectBudget(lstofProjBudget);
        
    }
    
    private static void createData()
    {
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        insert prjObj ;
        
        objChildProj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        objChildProj.OwnerId=objuser.Id;
        objChildProj.GP_Parent_Project__c = prjObj.Id;
        insert objChildProj ;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        insert projectLeadership;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.Id, prjObj.Id);
        insert resourceAllocationObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);      
        objPrjBdgt.GP_Cost_Rate__c = 11;
        objPrjBdgt.GP_Country__c =  'India';
        objPrjBdgt.GP_TCV__c =  111;
        objPrjBdgt.GP_Total_Cost__c = 11;
        objPrjBdgt.GP_OMS_Id__c = '1';      
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);     
        insert projectExpense;
        
      
    }
}