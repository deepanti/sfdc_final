/*
    @Author: Vikas Rajput
    @Description: It will bind opportunity related data and Target related data
*/
public class VIC_OpportunityDataWrapCtlr{
    @AuraEnabled public Boolean isOppChecked{get;set;}
    @AuraEnabled public Boolean isOppSelect = false;
    @AuraEnabled public Opportunity objOpp;
    @AuraEnabled public String strTotalTCV;
    @AuraEnabled public String strVICCreditPercent;
    @AuraEnabled public String strTotalVICPayable;
    @AuraEnabled public String strVICPaid;
    @AuraEnabled public String strUpfrontVIC;
    @AuraEnabled public String strITKicker;
    @AuraEnabled public String strNewLogoBonus;
    @AuraEnabled public String strApprovedIncentive;
    @AuraEnabled public String strTCVBooked;
    @AuraEnabled public String strRenewal;
    @AuraEnabled public String strComment;
    @AuraEnabled public List<VIC_OpportunityLineItemDataWrapCtlr> lstOLIDataWrap;
    @AuraEnabled public String strTotalUpfrontInLocal = '0';
    //@Constructor
    public VIC_OpportunityDataWrapCtlr(Boolean isOppChecked,Opportunity objOpp,String strTotalTCV,String strVICCreditPercent,
               String strTotalVICPayable,String strVICPaid,String strUpfrontVIC,String strITKicker,String strNewLogoBonus,
               String strApprovedIncentive,String strTCVBooked,String strRenewal,String strComment,List<VIC_OpportunityLineItemDataWrapCtlr> lstOLIDataWrap){
        this.isOppChecked = isOppChecked;
        this.objOpp = objOpp;
        this.strTotalTCV = strTotalTCV;
        this.strVICCreditPercent = strVICCreditPercent;
        this.strTotalVICPayable =strTotalVICPayable;
        this.strVICPaid = strVICPaid;
        this.strUpfrontVIC = strUpfrontVIC;
        this.strITKicker = strITKicker;
        this.strNewLogoBonus = strNewLogoBonus;
        this.strApprovedIncentive = strApprovedIncentive;
        this.strComment = strComment;
        this.lstOLIDataWrap = lstOLIDataWrap;
        this.strTCVBooked = strTCVBooked;
        this.strRenewal = strRenewal;
    }
}