/*
    @Author: Rishabh Pilani
    @Description: This class is used for approvals of incentive records by HR team.
    @Created on: 31/07/2018
    @Version : 1.0
    @Pending Logic: BDE Anniversary Amount Calculation is pending.
*/
public class VIC_HRApprovalCtrl {
    static Map < String, String > mapUserVicRoles = vic_CommonUtil.getUserVICRoleNameinMap();
    static VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    static Map < String, String > mapDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap();
    static Map < String, List < HRDataWrapper >> mapOfDomicileVsIncentiveData = new Map < String, List < HRDataWrapper >> ();
    /*
        Method Name : validateLoggedinUser
        Purpose : This method is used to validate the current loggedin user.
    */
    @AuraEnabled
    public static Boolean validLoggedinUser() {
        return true;
    }

    /*
        Method Name: getScreenData
        Purpose: This method is used to load the default data of HR screen.
        Return: List<HRViewWraper>
    */
    public Map < String, List < HRDataWrapper >> getScreenData() {
        List < HRViewWraper > returnList = new List < HRViewWraper > ();
        if (processInfo != null && processInfo.VIC_Process_Year__c != null) {
            Map < String, List < Target__c >> mapTargets = getTargetsMapDomicileWise();
            for (String s: mapTargets.KeySet()) {
                HRViewWraper objHRViewWrap = new HRViewWraper();

                List < HRDataWrapper > lstDataWrap = new List < HRDataWrapper > ();
                List < Target__c > lstTargets = mapTargets.get(s);

                if (lstTargets.size() > 0) {
                    for (Target__c t: lstTargets) {
                        HRDataWrapper objData = new HRDataWrapper();
                        objData.IsOnHold = t.vic_Is_Payment_Hold_By_HR__c;
                        objData.UserId = t.User__r.Id;
                        objData.UserName = t.User__r.Name;
                        objData.VICRole = mapUserVicRoles.containsKey(t.User__r.Id) ? mapUserVicRoles.get(t.User__r.Id) : '-';
                        objData.CurrencyISOCode = mapDomicileToISOCode.containsKey(t.User__r.Id) ? mapDomicileToISOCode.get(t.User__r.Id) : '';
                        objData.Comments = t.vic_HR_Comments__c;
                        Decimal NewBookingAmount = 0;
                        Decimal RenewalAmount = 0;
                        Decimal NewLogoBonusAmount = 0;
                        Decimal ITKickersAmount = 0;
                        Decimal BDEAnniversaryAmount = 0;
                        Decimal DiscretionaryAmount = 0;
                        for (Target_Component__c c: t.Target_Components__r) {
                            if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_IO') || c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_TS')) {
                                NewBookingAmount += c.vic_HR_Pending_Amount__c;
                            } else if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Renewal')) {
                                RenewalAmount += c.vic_HR_Pending_Amount__c;
                            } else if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('New_Logo_Bonus')) {
                                NewLogoBonusAmount += c.vic_HR_Pending_Amount__c;
                            } else if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Kickers')) {
                                ITKickersAmount += c.vic_HR_Pending_Amount__c;
                            } else if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('--------')) {
                                BDEAnniversaryAmount += c.vic_HR_Pending_Amount__c;
                            } else if (c.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Discretionary_Payment')) {
                                DiscretionaryAmount += c.vic_HR_Pending_Amount__c;
                            }
                        }
                        objData.TotalPayoutAmount = NewBookingAmount + RenewalAmount + NewLogoBonusAmount + ITKickersAmount + BDEAnniversaryAmount + DiscretionaryAmount;
                    	lstDataWrap.add(objData);
                    }
                }

                objHRViewWrap.Domicile = s;
                objHRViewWrap.Data = lstDataWrap;
                returnList.add(objHRViewWrap);
                if (!mapOfDomicileVsIncentiveData.containsKey(s)) {
                    mapOfDomicileVsIncentiveData.put(s, new List < HRDataWrapper > ());
                }
                mapOfDomicileVsIncentiveData.get(s).addAll(lstDataWrap);
            }
        } else {
            throw new AuraHandledException('VIC process default year is missing. Please contact your VIC support team.');
        }
        system.debug('returnList'+returnList);
        return mapOfDomicileVsIncentiveData;
    }

    /*
        Author: Rishabh Pilani
        Description: This method is used to fetch targets records and group in domicile wise.
        Return : Map<String, List<Target__c>>
    */
    public Static Map < String, List < Target__c >> getTargetsMapDomicileWise() {
        Date targetDate = date.newinstance(Integer.valueOf(processInfo.VIC_Process_Year__c), 1, 1);
        Map < String, List < Target__c >> mapDomicileWiseTargets = new Map < String, List < Target__c >> ();
        mapDomicileWiseTargets.put('US', new List < Target__c > ());
        mapDomicileWiseTargets.put('UK', new List < Target__c > ());
        mapDomicileWiseTargets.put('OTHERS', new List < Target__c > ());

        String strUSRegionDomiciles = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('US_Region_Domiciles');
        String strUKRegionDomiciles = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('UK_Region_Domiciles');

        for (Target__c t: [SELECT Id, User__r.Id, User__r.Name, Domicile__c, vic_HR_Comments__c, vic_Is_Payment_Hold_By_HR__c,
                (Select Id, Master_Plan_Component__r.vic_Component_Code__c, vic_HR_Pending_Amount__c FROM Target_Components__r)
                FROM Target__c
                WHERE Is_Active__c = TRUE
                AND User__r.IsActive = true
                AND Start_date__c =: targetDate
                AND Domicile__c != null
                Order BY Domicile__c
            ]) {
                system.debug('targetsss' + t);
            if (strUSRegionDomiciles.ContainsIgnoreCase(t.Domicile__c)) {
                List < Target__c > lstTargets = mapDomicileWiseTargets.get('US');
                lstTargets.add(t);
                mapDomicileWiseTargets.put('US', lstTargets);
            } else if (strUKRegionDomiciles.ContainsIgnoreCase(t.Domicile__c)) {
                List < Target__c > lstTargets = mapDomicileWiseTargets.get('UK');
                lstTargets.add(t);
                mapDomicileWiseTargets.put('UK', lstTargets);
            } else {
                List < Target__c > lstTargets = mapDomicileWiseTargets.get('OTHERS');
                lstTargets.add(t);
                mapDomicileWiseTargets.put('OTHERS', lstTargets);
            }
        }
        system.debug('mapDomicileWiseTargets'+mapDomicileWiseTargets);
        return mapDomicileWiseTargets;
    }

    /*
        Method Name : getSearchResult
        Parameters : strDomicile, strSearchQuery
        Purpose : This method is used to provide the search result.
    */
    @AuraEnabled
    public static HRViewWraper getSearchResult(String strDomicile, String strSearchQuery) {
        return null;
    }

    @AuraEnabled
    public static Boolean UpdateRecords(List < HRDataWrapper > lstData) {
        return null;
    }

    public class HRViewWraper {
        public String Domicile { get;
            set; }
        public List < HRDataWrapper > Data { get;
            set; }
    }


    public class HRDataWrapper {
        public boolean IsSelected { get;
            set; }
        public String UserId { get;
            set; }
        public String UserName { get;
            set; }
        public String VICRole { get;
            set; }
        public String CurrencyISOCode { get;
            set; }
        public Decimal NewBookingAmount { get;
            set; }
        public Decimal RenewalAmount { get;
            set; }
        public Decimal NewLogoBonusAmount { get;
            set; }
        public Decimal ITKickersAmount { get;
            set; }
        public Decimal BDEAnniversaryAmount { get;
            set; }
        public Decimal DiscretionaryAmount { get;
            set; }
        public Decimal TotalPayoutAmount { get;
            set; }
        public String Comments { get;
            set; }
        public Boolean IsOnHold { get;
            set; }
    }
}