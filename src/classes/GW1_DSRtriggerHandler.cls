// Class is handler class of DSR trigger 
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1        03-10-2016       Rishi Kumar               Created 
// ---------------------------------------------------------------------------------------------
//           05-10-2016       Pankaj Adhikari           Added Change bid Manager functionality.  
// ---------------------------------------------------------------------------------------------

public class GW1_DSRtriggerHandler
{
    public void runTrigger()
    {
        if (trigger.isAfter && trigger.isInsert)
        {
            onAfterInsert((list<GW1_DSR__c>) trigger.new);
        }
        if (trigger.isAfter && trigger.IsUPdate) 
        {
            onAfterUpdate((list<GW1_DSR__c>) trigger.new, (map<id, GW1_DSR__c>) trigger.oldmap, (map<id, GW1_DSR__c>) trigger.newmap);
        }
    }


    private void onAfterInsert(list<GW1_DSR__c> lstTriggerNew)
    {
        //createDSRGroup(lstTriggerNew); now groups are created after assigning bid manager
        addOpportunityOwnerToDsrTeam(lstTriggerNew);
        //renameDSR(lstTriggerNew);
    }
    private void onAfterUpdate(list<GW1_DSR__c> lstTriggerNew, map<id, GW1_DSR__c> oldmap, map<id, GW1_DSR__c> Newmap)
    {
        createDSRGroupOnBidManagerAssign(lstTriggerNew, oldmap, Newmap);
        addBidManagerToDSRteam(lstTriggerNew, oldmap);
        addTowerLeadTOTeam(lstTriggerNew,oldmap,Newmap);
        //changeGroupOwner(lstTriggerNew, oldmap, Newmap);

    }

    /*
      This method will add Opportunity owner to DSR Team .
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */
    public void addOpportunityOwnerToDsrTeam(list<GW1_DSR__c> lstTriggerNew)
    {

        set<id> setOppIDS = new set<id> ();
        list<GW1_DSR_Team__c> lstDSRTeam = new list<GW1_DSR_Team__c> ();

        for (GW1_DSR__c objDSR : lstTriggerNew)
        {
            setOppIDS.add(objDSR.GW1_Opportunity__c); // ids Of All Opportunity 
        }

        if (lstTriggerNew != null && lstTriggerNew.size() > 0)
        {
            //Fecthing related opportunity 
            map<id, Opportunity> mapIdOpp = new map<id, Opportunity> ([select id, name, ownerID from opportunity where Id in :setOppIDS]);

            for (GW1_DSR__c objDSR : lstTriggerNew)
            {
                // Adding Opporunity Owner To Team member
                if (mapIdOpp != null && mapIdOpp.get(objDSR.GW1_Opportunity__c) != null)
                {
                    GW1_DSR_Team__c objDSRTeam = new GW1_DSR_Team__c();
                    objDSRTeam.GW1_User__c = mapIdOpp.get(objDSR.GW1_Opportunity__c).ownerid;
                    objDSRTeam.GW1_Role__c = 'BD';
                    objDSRTeam.GW1_Is_active__c = true;
                    objDSRTeam.GW1_DSR__c = objDSR.id;
                    lstDSRTeam.add(objDSRTeam);
                    
                     GW1_DSR_Team__c objDSRTeam1 = new GW1_DSR_Team__c();
                    objDSRTeam1.GW1_User__c = objDSR.Sales_Leader__c;
                    objDSRTeam1.GW1_Role__c = 'Sales Leader';
                    objDSRTeam1.GW1_Is_active__c = true;
                    objDSRTeam1.GW1_DSR__c = objDSR.id;
                    lstDSRTeam.add(objDSRTeam1);
                    
                    for(DSR_Permanent_Member__c members : DSR_Permanent_Member__c.getAll().values()){
                            GW1_DSR_Team__c objDSRTeam2 = new GW1_DSR_Team__c();
                            objDSRTeam2.GW1_User__c = members.User_ID__c;
                            objDSRTeam2.GW1_Role__c = members.Role__c ;
                            objDSRTeam2.GW1_Is_active__c = true;
                            objDSRTeam2.GW1_DSR__c = objDSR.id;
                            lstDSRTeam.add(objDSRTeam2);
                        
                        } 
               
                }

            }
            if (lstDSRTeam != null && lstDSRTeam.size() > 0)
            {
                insert lstDSRTeam;
            }
        }
    }

    /*
      This method is use to change owner of dsr when Bid manager is assigned
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */
    private void createDSRGroupOnBidManagerAssign(list<GW1_DSR__c> lstTriggerNew, map<id, GW1_DSR__c> triggerOldMap, map<id, GW1_DSR__c> triggerNewMap)
    {
        list<GW1_DSR__c> listDsrWithAssignedBidManager = new list<GW1_DSR__c> ();
        set<id> bidManagerId = new set<Id> ();
        set<id> setDsrId = new set<Id> ();
        for (GW1_DSR__c objDSR : lstTriggerNew)
           
        {
            //if bid manager is assigned for first time create a DSR GROUP
            system.debug('objDSR.GW1_Bid_Manager__c:'+objDSR.GW1_Bid_Manager__c);
            system.debug('objDSR.GW1_Primary_Chatter_ID__c:'+objDSR.GW1_Primary_Chatter_ID__c);
            system.debug('triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c'+triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c);
             system.debug('Create_chatter_Group__c.'+objDSR.Create_chatter_Group__c);
            
            if (objDSR.GW1_Bid_Manager__c != null)
            {
                bidManagerId.add(objDSR.GW1_Bid_Manager__c);
                setDsrId.add(objDSR.id);
                system.debug('insideCreate_chatter_Group__c.'+objDSR.Create_chatter_Group__c);
                // If chatter is not created
                if (String.isBlank(objDSR.GW1_Primary_Chatter_ID__c) && objDSR.Create_chatter_Group__c)
                {
                    listDsrWithAssignedBidManager.add(objDSR);
                    
                }
            }

        }
        if (bidManagerId != null && bidManagerId.size() > 0)
        {
            Map<id, GW1_Bid_Manager__c> MapIDtoBidManager = new Map<id, GW1_Bid_Manager__c> ([select id, GW1_Bid_Manager__c
                                                                                             from GW1_Bid_Manager__c
                                                                                             where id in :bidManagerId]);


            //DsrGroup will be created after assigning Bid Manager
            if (listDsrWithAssignedBidManager != null && listDsrWithAssignedBidManager.size() > 0)
            {
                
                createDSRGroup(listDsrWithAssignedBidManager);
            }
            if (setDsrId != null && setDsrId.size() > 0 && MapIDtoBidManager != null && MapIDtoBidManager.size() > 0)
            {
                //if DSr group already exist change group owner 
                changeGroupOwner(lstTriggerNew, triggerOldMap, triggerNewMap, setDsrId, MapIDtoBidManager); // change owner of the Related Dsr Group
            }

        }
    }


    /*
      This method is use to create DSR Group when bid manager is assigned
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */
   private void createDSRGroup(list<GW1_DSR__c> lstTriggerNew)
    {
        if (lstTriggerNew != null && lstTriggerNew.size() > 0)
        {
            list<GW1_DSR_Group__c> listOfDSRGroupInsert = new list<GW1_DSR_Group__c> ();

            for (GW1_DSR__c objDSR : lstTriggerNew)
            {
                GW1_DSR_Group__c objDSRGroup = new GW1_DSR_Group__c();
                 string s=objDSR.Pseudo_name1__c ;
                 //+'-'+objDSR.GW1_Opportunity_Id__c;
              //   SUBSTITUTE(Pseudo_name__c ," ","")
                  system.debug('DSRGroupName:'+s);
                 
                
             /*   if( objDSR.Pseudo_name__c!=null && objDSR.Pseudo_name__c!='')
                
              && objDSR.Pseudo_name__c.length()>40)
                {
                    objDSRGroup.Name=objDSR.Pseudo_name__c.substring(0,29)+'-'+objDSR.GW1_Opportunity_Id__c;
                }   
                else 
                { */
                    
                objDSRGroup.Name=s;
              //  objDSRGroup.Name=objDSR.Pseudo_name__c+'-'+objDSR.GW1_Opportunity_Id__c;
                //+'-'+objDSR.GW1_Opportunity_Id__c;
                objDSRGroup.GW1_DSR__c = objDSR.id;
                objDSRGroup.ownerid = objDSR.ownerid;
                objDSRGroup.GW1_Is_Primary_Group__c = true;     
                objDSRGroup.GW1_DSR_Name__c =s;
                listOfDSRGroupInsert.add(objDSRGroup);
            }

            if (listOfDSRGroupInsert != null && listOfDSRGroupInsert.size() > 0)
            insert listOfDSRGroupInsert;
        }
    }

    /*
      This function is called on Update event to change owner of DSR Group if  
      DSR Owner chnaged
      -------------------------------------------------------------------------------------- -----------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------------------
      Pankaj Adhikari             05-10-2016                Added Change bid Manager functionality.      
      --------------------------------------------------------------------------------------------------
     
     */
    // update required -> we donot need to change owner of those dsr which is created in this transiction
    private void changeGroupOwner(list<GW1_DSR__c> lstTriggerNew, map<id, GW1_DSR__c> triggerOldMap, map<id, GW1_DSR__c> triggerNewMap, set<id> setDSRIds, Map<id, GW1_Bid_Manager__c> MapIDtoBidManager)
    {

        if (setDSRIds != null && setDSRIds.size() > 0)
        {
            list<GW1_DSR_Group__c> lstDsrGroup = new list<GW1_DSR_Group__c> ([select GW1_DSR__r.name, GW1_DSR__c, ownerid, GW1_Chatter_Group_Id__c from GW1_DSR_Group__c where GW1_DSR__c in :setDSRIds]);

            if (lstDsrGroup != null && lstDsrGroup.size() > 0)
            {
                for (GW1_DSR_Group__c objDSRGroup : lstDsrGroup)
                {
                    if (triggerNewMap.get(objDSRGroup.GW1_DSR__c).GW1_Bid_Manager__c != null && MapIDtoBidManager != null && MapIDtoBidManager.get(triggerNewMap.get(objDSRGroup.GW1_DSR__c).GW1_Bid_Manager__c) != null)
                    {
                        if (MapIDtoBidManager.get(triggerNewMap.get(objDSRGroup.GW1_DSR__c).GW1_Bid_Manager__c).GW1_Bid_Manager__c != null)
                        {
                            // Assigning  new ownner to DSR Group i.e Bid Manager
                            objDSRGroup.ownerid = MapIDtoBidManager.get(triggerNewMap.get(objDSRGroup.GW1_DSR__c).GW1_Bid_Manager__c).GW1_Bid_Manager__c;
                        }
                    }
                }
                system.debug('lstDsrGroup::' + lstDsrGroup);
                try
                {
                    update lstDsrGroup;
                }

                catch(exception e)
                {
                    if (e.getMessage().contains('The owner of a group must be a group member.'))
                    {
                        lstTriggerNew[0].adderror('Please add this user to the team member.');
                    }
                }
            }
        }
    }

    /*
      This method is use to add bid manager in DSR TEam if changed and also inactive existing bid manager 
      --------------------------------------------------------------------------------------
      Name                              Date                                Version                     
      --------------------------------------------------------------------------------------
      Rishi                           03-10-2016                              1.0   
      --------------------------------------------------------------------------------------
     */

    private void addBidManagerToDSRteam(List<GW1_DSR__c> triggerNew, Map<Id, GW1_DSR__c> triggerOldMap)
    {
        List<GW1_DSR_Team__c> lstDSRTeam = new List <GW1_DSR_Team__c>();
        List<ConnectApi.BatchInput> batchInputsFeeds = new List<ConnectApi.BatchInput>();  // USed For Feeds
        Set<ID> setofNewBidManagers = new Set<ID> ();
        Set<ID> setofOldBidManagers = new Set<ID> ();
        set<id> setDSRid = new set<id>();
        for (GW1_DSR__c objDSR :triggerNew)
        {
            //if dsr bid manager is not null and is changed add him in new dsr team and add old dsr b
            if (objDSR.GW1_Bid_Manager__c != null && triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c != objDSR.GW1_Bid_Manager__c )
            {
                setDSRid.add(objDSR.id);
                setofNewBidManagers.add(objDSR.GW1_Bid_Manager__c); 
                setofNewBidManagers.add(triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c);   //this managers will be added into dsr team
                //setofOldBidManagers.add(triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c);//these bid managers might be added on a team so , will disable them
            }
        }
        Map<Id, GW1_Bid_Manager__c> mapIDToBidManager = new Map<Id, GW1_Bid_Manager__c> ([select Id, GW1_Bid_Manager__c from GW1_Bid_Manager__c where Id = :setofNewBidManagers]);
        // Set OF bid manager 
        // added by pankaj
        for(GW1_Bid_Manager__c objBidManager:mapIDToBidManager.values())
        {
            if(objBidManager.GW1_Bid_Manager__c!=null)
                setofOldBidManagers.add(objBidManager.GW1_Bid_Manager__c );
        
        }
        
        Map<Id, GW1_DSR_Team__c>  mapUserIDToDSRTeam  = new Map<Id, GW1_DSR_Team__c>([select id ,GW1_User__c,GW1_DSR__c ,GW1_Role__c from GW1_DSR_Team__c where GW1_User__c = :setofOldBidManagers and GW1_DSR__c in:setDSRid]);
        List <GW1_DSR_Team__c> listDSRTeamtoInActive = new list<GW1_DSR_Team__c>();
        Map<string, GW1_DSR_Team__c>  mapDSRUserIDToDSRTeam = new map<string,GW1_DSR_Team__c>();
        
        for (GW1_DSR_Team__c objDSRTeam :mapUserIDToDSRTeam.values())
        {
            string key = objDSRTeam.GW1_DSR__c+'-'+ objDSRTeam.GW1_User__c;
            mapDSRUserIDToDSRTeam.put(key,objDSRTeam);
        }
        
        for (GW1_DSR__c objDSR :triggerNew)
        {
            //if dsr bid manager is not null and is changed add him in new dsr team and add old dsr b
            
            if (objDSR.GW1_Bid_Manager__c != null && triggerOldMap.get(objDSR.id).GW1_Bid_Manager__c != objDSR.GW1_Bid_Manager__c && mapIDToBidManager.get(objDSR.GW1_Bid_Manager__c)!=null && mapIDToBidManager.get(objDSR.GW1_Bid_Manager__c).GW1_Bid_Manager__c!=null)
            { 
                
                string key =objDSR.id+'-'+mapIDToBidManager.get(objDSR.GW1_Bid_Manager__c).GW1_Bid_Manager__c;
                // if bid manager is already a team member
                if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.get(key)!=null)
                 {
                    mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c = true;
                    lstDSRTeam.add(mapDSRUserIDToDSRTeam.get(key));
                    mapDSRUserIDToDSRTeam.remove(key);
                    system.debug('key'+mapDSRUserIDToDSRTeam); 
                    system.debug('mapDSRUserIDToDSRTeam'+mapDSRUserIDToDSRTeam);
                    //system.debug('mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c::'+mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c);
                 } 
                 else
                 {
                    GW1_DSR_Team__c objDSRTeam = new GW1_DSR_Team__c();
                    objDSRTeam.GW1_User__c =mapIDToBidManager.get(objDSR.GW1_Bid_Manager__c).GW1_Bid_Manager__c;
                    objDSRTeam.GW1_Is_active__c = true;
                    objDSRTeam.GW1_DSR__c = objDSR.id;
                    objDSRTeam.GW1_Role__c = 'Deal Manager';
                    lstDSRTeam.add(objDSRTeam);
                 }
                 // Posting Feed Where ever Bid Manger changes After group Manger Changes
                        
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput> ();

                mentionSegmentInput.id = mapIDToBidManager.get(objDSR.GW1_Bid_Manager__c).GW1_Bid_Manager__c;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = ' You have been Assigned Deal Manager for ' + objDSR.Name;
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = objDSR.id;

                ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
                batchInputsFeeds.add(batchInput);
                                
                            
                 
           }
           
        }
        // Inactive Prevoius bid manager 
        system.debug('mapDSRUserIDToDSRTeam::'+mapDSRUserIDToDSRTeam);
        if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.size()>0)
        {
            for(GW1_DSR_Team__c objDSRTeam: mapDSRUserIDToDSRTeam.values() )
            {
                 objDSRTeam.GW1_Is_active__c = false ;
                 listDSRTeamtoInActive.add(objDSRTeam);
                 system.debug('objDSRTeam::'+objDSRTeam);
            }
        }
        

        if (lstDSRTeam != null && lstDSRTeam.size() > 0)
        {
            upsert lstDSRTeam;
        }
        
        if(batchInputsFeeds!= null && batchInputsFeeds.size()>0) // Inserting Feeds 
        {
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputsFeeds);
        }
        if (listDSRTeamtoInActive != null && listDSRTeamtoInActive.size() > 0)
        {
            update listDSRTeamtoInActive;
        }
        
    }
    
 //---------------------------------------------------------------------------------------------------------------
 // Description ::  This method will add DSR Owner to the team and create Sharing Rule for extisting members.
 //---------------------------------------------------------------------------------------------------------------  
 // Created By: Pankaj Adhikari         Date:09-MAY-2016        Email:Pankaj.adhikari@saasfocus.com
 //---------------------------------------------------------------------------------------------------------------
    private void addTowerLeadTOTeam(List<GW1_DSR__c> triggerNew, Map<Id, GW1_DSR__c> triggerOldMap,  Map<Id, GW1_DSR__c> triggerNewMap)
    {
        
        set<id> setDsrID= new Set<id>();
        set<id> setUserId = new set<id>();
        for(GW1_DSR__c objDsr:triggerNew )
        {
            if(   objDsr.ownerid!=null && string.valueOf(objDSR.ownerID).startsWith('005') && objDsr.ownerid!=triggerOldMap.get(objDsr.id).ownerid )
            {
                setDsrID.add(objDsr.id);
                setUserId.add(objDsr.ownerid);
                if( string.valueOf(triggerOldMap.get(objDsr.id).ownerid).startsWith('005'))
                       setUserId.add(triggerOldMap.get(objDsr.id).ownerid);
            }
        }
        
        if( setDsrID!=null && setDsrID.size()>0)
        {
            list<GW1_DSR_Team__c>  lstDSRTeamPresent  = new list<GW1_DSR_Team__c >([select id ,GW1_User__c,GW1_Is_active__c,GW1_DSR__c ,GW1_Role__c from GW1_DSR_Team__c where  GW1_DSR__c in:setDsrID ]);
            List <GW1_DSR_Team__c> listDSRTeamtoInActive = new list<GW1_DSR_Team__c>();
            List<ConnectApi.BatchInput> batchInputsFeeds = new List<ConnectApi.BatchInput>();  // USed For Feeds
            list <GW1_DSR_Team__c> lstDSRTeam = new list<GW1_DSR_Team__c>();
            Map<string, GW1_DSR_Team__c>  mapDSRUserIDToDSRTeam = new map<string,GW1_DSR_Team__c>();
            
            for (GW1_DSR_Team__c objDSRTeam :lstDSRTeamPresent)
            {
                 string key = objDSRTeam.GW1_DSR__c+'-'+ objDSRTeam.GW1_User__c;
                 mapDSRUserIDToDSRTeam.put(key,objDSRTeam);
            }
            
           for(id DSRid:setDsrID )
           {
             if(triggerNewMap.get(DSRid)!=null && triggerNewMap.get(DSRid).ownerid!=null)
             {
                 string key= DSRid+'-'+triggerNewMap.get(DSRid).ownerid;
                 
                 if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.get(key)!=null)
                 {
                    mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c = true;
                    lstDSRTeam.add(mapDSRUserIDToDSRTeam.get(key));
                    mapDSRUserIDToDSRTeam.remove(key);
                    system.debug('key'+mapDSRUserIDToDSRTeam);
                    system.debug('mapDSRUserIDToDSRTeam'+mapDSRUserIDToDSRTeam);
                    //system.debug('mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c::'+mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c);
                 } 
                 else
                 {
                    GW1_DSR_Team__c objDSRTeam = new GW1_DSR_Team__c();
                    objDSRTeam.GW1_User__c =triggerNewMap.get(DSRid).ownerid;
                    objDSRTeam.GW1_Is_active__c = true;
                    objDSRTeam.GW1_DSR__c = DSRid;
                    objDSRTeam.GW1_Role__c = 'RFx Tower Lead';
                    lstDSRTeam.add(objDSRTeam);
                 }
                 
             }
                // Feeds
                ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput> ();

                mentionSegmentInput.id = triggerNewMap.get(DSRid).ownerid;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = ' You have been Assigned RFx Tower Lead for ' + triggerNewMap.get(DSRid).name +' .Kindly assign Deal Manager.';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = DSRid;

                ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
                batchInputsFeeds.add(batchInput);
          }
            
            if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.size()>0)
                {
                    for(GW1_DSR_Team__c objDSRTeam: mapDSRUserIDToDSRTeam.values() )
                    {
                        
                        
                       if(setUserId.contains(objDSRTeam.GW1_User__c))
                       {  objDSRTeam.GW1_Is_active__c = false ;
                            listDSRTeamtoInActive.add(objDSRTeam);
                       }
                         system.debug('objDSRTeam::'+objDSRTeam);
                    }
                }
          
          if (lstDSRTeam != null && lstDSRTeam.size() > 0)
            {
                upsert lstDSRTeam;
            }
            try  {
            if(batchInputsFeeds!= null && batchInputsFeeds.size()>0) // Inserting Feeds 
            {
                ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputsFeeds);
            }
            
            if (listDSRTeamtoInActive != null && listDSRTeamtoInActive.size() > 0)
                    {
                        update listDSRTeamtoInActive;
                    }
            } catch(exception e)
                {
                    if (e.getMessage().contains('Before deactivating this user please transfer chatter group owner to other member.'))
                    {
                        triggerNew[0].adderror('Before deactivating this user please transfer chatter group owner to other member.');
                    }
                   
                }
            
            // Adding Sharing rule for 
            system.debug('lstDSRTeamPresent::'+lstDSRTeamPresent);
            if(lstDSRTeamPresent!=null && lstDSRTeamPresent.size()>0)
            {   
                GW1_DSRTeamTriggerHandler objHandler = new GW1_DSRTeamTriggerHandler();
                objHandler.addSharingSetting(lstDSRTeamPresent);
            }
                
        }
    }
    
    
    
}