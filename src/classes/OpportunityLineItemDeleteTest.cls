@IsTest
public class OpportunityLineItemDeleteTest{
    public static testMethod void insertTestMethod(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        User utest = GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
         Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Testing Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Testing Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('Testing Test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 4',Sales_Leader__c = utest.id);
        insert salesunit;
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),utest.id,oAT.Id,'Testing Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        Opportunity opp = new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1,OwnerID=utest.id,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive',Super_SL_overlay_created__c=False,SL_overlay_created__c=false,Target_Source__c='Ramp up');
         insert opp;
            
            Product2 product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            OpportunityLineItem opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = utest.ID,
                                                                   unitPrice = 100.00, TCV__c = 100); 
            insert opp_item;
            Delete opp_item;
        
        OLI_Delete__c oli = new OLI_Delete__c();
        oli.Oli_Id__c = '0069000000v4kOMAAY';
        insert oli;
    }
}