@isTest(SeeAllData = false)
/************************************************************************************************************
* @author   Vikas Rathi 
* @description  - This class will be the utility class for test data creation
*************************************************************************************************************/
public class TESTQsrmDataUtility{
 public static QSRM__c createTestQSRMRecord(User u,user u1,Id oppId,Id recType,String s,String s1,String s2,String s3,String s4,String s5,String s6,String s7,String s8,String s9,String s10,String s11,String s12,String s13,String s14,String s15){
     QSRM__c qsrmRecord = new QSRM__c(Deal_Administrator__c = 'In-house',
                                      recordTypeId = recType,
                                      Vertical_Leaders__c = u.id,
                                      Service_Line_Leaders__c = u1.id,
                                      Deal_Type__c = 'Project',
                                      Bid_pro_support_needed__c = 'No',
                                      Opportunity__c = oppId,
                                      What_is_our_track_record_at_this_client__c = s,
                                      Does_this_project_align_with_the_client__c = s1,
                                      Does_the_client_rate_card_or_proposed_c__c = s2,
                                      In_case_of_non_named_account__c = s3,
                                      Does_the_client_have_an_approved_budget__c = s4,
                                      Is_there_a_clear_client_sponsor_decision__c = s5,
                                      Has_the_client_sponsor_decision_maker__c = s6,
                                      Do_we_understand_the_client__c = s7,
                                      How_strong_is_our_value_propostion_and_w__c = s8,
                                      Do_we_have_the_right_relationship_streng__c = s9,
                                      How_well_does_our_offering_fit__c = s10,
                                      Do_we_know_our_competition__c = s11,
                                      Do_we_have_the_right_level_of_domain__c = s12,
                                      Do_we_have_skills_at_right_locations__c = s13,
                                      What_is_the_commercial_model_expected_to__c = s14,
                                      Can_the_solution_be_delivered_by_each__c = s15
                                      );
     return qsrmRecord;
 }
}