/**
	Description  : This class will create verson line items history records for PID when resource allocations in PID are more than 20.
				   The queueable is called from GPDomainProject class.
	Created by   : Dhruv Singh.
	OHR 	     : 703248825.
	Created On   : 16-09-19. 
*/
public class GPQueueableCreateVersionLineItems implements Queueable {
    
    GPBaseProjectUtil baseProjectUtil;
    Map<Id,Id> mapOfProjectIdVsVersionHistoryParentId;
    
    public GPQueueableCreateVersionLineItems(GPBaseProjectUtil baseProjectUtil, Map<Id,Id> mapOfProjectIdVsVersionHistoryParentId) {
        this.baseProjectUtil = baseProjectUtil;
        this.mapOfProjectIdVsVersionHistoryParentId = mapOfProjectIdVsVersionHistoryParentId;
    }
	
    public void execute(QueueableContext context) {
        
        try{
            Map<Id, String> mapOfParentIdWithUniqueId = new Map<Id, String>();
            List < GP_Project_Version_Line_Item_History__c > listOfVersionLineItemToInsert = new List < GP_Project_Version_Line_Item_History__c > ();
            
            if(mapOfProjectIdVsVersionHistoryParentId != null && mapOfProjectIdVsVersionHistoryParentId.keySet().size() > 0 && baseProjectUtil != null) {
                
                for(GP_Project_Version_History__c pvh : [Select id, GP_Project__c, GP_Unique_Version_History__c 
                                                         from GP_Project_Version_History__c 
                                                         where GP_Project__c =: mapOfProjectIdVsVersionHistoryParentId.values()
                                                         Order by createdDate DESC limit 1]) 
                {
                    mapOfParentIdWithUniqueId.put(pvh.GP_Project__c, pvh.GP_Unique_Version_History__c);
                }
                
                System.debug('==mapOfParentIdWithUniqueId=='+mapOfParentIdWithUniqueId);
                
                if(mapOfParentIdWithUniqueId.keySet().size() > 0) {
                    for(GP_Project__c eachProject : baseProjectUtil.lstProject) {
                        
                        JSONGenerator gen = JSON.createGenerator(true);
                        
                        if(mapOfProjectIdVsVersionHistoryParentId.containsKey(eachProject.Id) 
                           && mapOfParentIdWithUniqueId.containsKey(mapOfProjectIdVsVersionHistoryParentId.get(eachProject.Id))) 
                        {
                            String uniqueKey = mapOfParentIdWithUniqueId.get(mapOfProjectIdVsVersionHistoryParentId.get(eachProject.Id));
                            System.debug('==uniqueKey=='+uniqueKey);
                            if (baseProjectUtil.mapOfProjectLeadership.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectLeadership.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectLeadership.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String leadershipJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Leadership__c.fields.getMap(), gen, 'Leadership', baseProjectUtil.mapOfProjectLeadership.get(eachProject.Id));
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfProjectLeadershipVersion = baseProjectUtil.getListOfVersionLineItemForJSON(leadershipJSON, 'Project Leadership', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfProjectLeadershipVersion);
                            }
                            
                            if (baseProjectUtil.mapOfResourceAllocation.containsKey(eachProject.Id) && baseProjectUtil.mapOfResourceAllocation.get(eachProject.Id) != null && baseProjectUtil.mapOfResourceAllocation.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                //objVersion.GP_Resource_Allocation_JSON__c = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap(), gen, 'Resource Allocation', baseProjectUtil.mapOfResourceAllocation.get(eachProject.Id));
                                String resourceAllocationJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap(), gen, 'Resource Allocation', baseProjectUtil.mapOfResourceAllocation.get(eachProject.Id));
                                List < GP_Project_Version_Line_Item_History__c > listOfResourceAllocationVersion = baseProjectUtil.getListOfVersionLineItemForJSON(resourceAllocationJSON, 'Resource Allocation', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfResourceAllocationVersion);
                            }
                            
                            if (baseProjectUtil.mapOfProjectBudget.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectBudget.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectBudget.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String budgetJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Budget__c.fields.getMap(), gen, 'Project Budget', baseProjectUtil.mapOfProjectBudget.get(eachProject.Id));
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfBudgetVersion = baseProjectUtil.getListOfVersionLineItemForJSON(budgetJSON, 'Budget', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfBudgetVersion);
                            }
                            
                            if (baseProjectUtil.mapOfProjectExpense.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectExpense.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectExpense.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String expenseJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Expense__c.fields.getMap(), gen, 'Project Expense', baseProjectUtil.mapOfProjectExpense.get(eachProject.Id));
                                
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfExpenseVersion = baseProjectUtil.getListOfVersionLineItemForJSON(expenseJSON, 'Expense', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfExpenseVersion);
                            }
                            
                            if (baseProjectUtil.mapOfProjectWorkLocation.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectWorkLocation.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectWorkLocation.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String workLocationJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Work_Location_SDO__c.fields.getMap(), gen, 'Work Location', baseProjectUtil.mapOfProjectWorkLocation.get(eachProject.Id));
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfWorkLocationVersion = baseProjectUtil.getListOfVersionLineItemForJSON(workLocationJSON, 'Work Location', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfWorkLocationVersion);
                            }
                            
                            if (baseProjectUtil.mapOfBillingMilestone.containsKey(eachProject.Id) && baseProjectUtil.mapOfBillingMilestone.get(eachProject.Id) != null && baseProjectUtil.mapOfBillingMilestone.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String billingMilestone = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Billing_Milestone__c.fields.getMap(), gen, 'Billing Milestone', baseProjectUtil.mapOfBillingMilestone.get(eachProject.Id));                            
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfBillingMilestoneVersion = baseProjectUtil.getListOfVersionLineItemForJSON(billingMilestone, 'Billing Milestone', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfBillingMilestoneVersion);
                            }
                            
                            if (baseProjectUtil.mapOfProjectDocument.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectDocument.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectDocument.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                
                                String documentJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Document__c.fields.getMap(), gen, 'Project Document', baseProjectUtil.mapOfProjectDocument.get(eachProject.Id));                            
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfDocumentVersion = baseProjectUtil.getListOfVersionLineItemForJSON(documentJSON, 'Document', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfDocumentVersion);
                            }
                            
                            if (baseProjectUtil.mapOfProjectProfileBillRate.containsKey(eachProject.Id) && baseProjectUtil.mapOfProjectProfileBillRate.get(eachProject.Id) != null && baseProjectUtil.mapOfProjectProfileBillRate.get(eachProject.Id).size() > 0) {
                                gen = JSON.createGenerator(true);
                                String billRateJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Profile_Bill_Rate__c.fields.getMap(), gen, 'Project Bill Rate', baseProjectUtil.mapOfProjectProfileBillRate.get(eachProject.Id));                            
                                
                                List < GP_Project_Version_Line_Item_History__c > listOfBillRateVersion = baseProjectUtil.getListOfVersionLineItemForJSON(billRateJSON, 'Profile Bill Rate', uniqueKey);
                                
                                listOfVersionLineItemToInsert.addAll(listOfBillRateVersion);
                            }
                        }
                    }
                }
            }
            
            if (listOfVersionLineItemToInsert != null && !listOfVersionLineItemToInsert.isEmpty()) {
                insert listOfVersionLineItemToInsert;
            }
        } catch(Exception ex) {
            GPErrorLogUtility.logError('GPQueueableCreateVersionLineItems', 'execute', ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());
        }
    }
    
    private void generateJSONOfPreviousVersion(SObject objectToGenerateJSON, Map < String, Schema.SObjectField > MapOfSchema, JSONGenerator gen) {
        gen.writeStartObject();
        for (String EachField: MapOfSchema.keySet()) {
            if (objectToGenerateJSON.get(EachField) != null) {
                createSobjectJSON(MapOfSchema, EachField, gen, objectToGenerateJSON);
            } else {
                gen.writeNullField(EachField);
            }
        }
        gen.writeEndObject();
    }
    
    private String generateJSONOfChildRecordsOfPreviousVersion(Map < String, Schema.SObjectField > MapOfSchema, JSONGenerator gen, String objectName, List < Sobject > lstOfChildRecord) {
        gen.writeStartObject();
        gen.writeFieldName(objectName);
        gen.writeStartArray();
        for (Sobject sobj: lstOfChildRecord) {
            generateJSONOfPreviousVersion(sobj, MapOfSchema, gen);
        }
        gen.writeEndArray();
        gen.writeEndObject();
        return gen.getAsString();
    }
    
    private void createSobjectJSON(Map < String, Schema.SObjectField > MapOfSchema, string field, JSONGenerator genJson, Sobject obj) {
        Set < String > setOfStandardFields = new Set < String > { 'connectionreceivedid', 'connectionsentid' };

        if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.BOOLEAN)
            genJson.writeBooleanField(field, (BOOLEAN) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DATETIME)
            genJson.writeDateTimeField(field, (DATETIME) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DATE)
            genJson.writeDateField(field, (DATE) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.PERCENT)
            genJson.writeNumberField(field, (decimal) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DOUBLE ||
            MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.CURRENCY ||
            MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.Integer)
            genJson.writeNumberField(field, (DOUBLE) obj.get(field));
        else
            genJson.writeStringField(field, (string) obj.get(field));

        if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.REFERENCE) {
            if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                if (field.contains('__c')) {
                    field = field.replace('__c', '__r');
                } else if (field.contains('id') && !setOfStandardFields.contains(field)) {
                    field = field.replace('id', '');
                }
                genJson.writeStringField(field + '.name', (string) obj.getSobject(field).get('name'));
            }
        }
    }    
}