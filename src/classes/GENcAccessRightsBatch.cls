global class GENcAccessRightsBatch implements Database.batchable<sObject>, Database.Stateful
{
    List<User> userList = new List<User>(); 
    List<Account>accList=new List<Account>();
    List<Opportunity> oppList = new List<Opportunity>(); 
    List<OpportunityProduct__c> oOppProdList = new List<OpportunityProduct__c>();
    List<OpportunityProduct__Share> oOppProdShare = new List<OpportunityProduct__Share>();
    Map<Id,AccountShare> oAccountIdMap = new Map<Id,AccountShare>();
    Map<Id,Opportunity> oOpportunityIdMap = new Map<Id,Opportunity>();
    Map<Id,OpportunityProduct__c> oOpptyProdIdMap = new Map<Id,OpportunityProduct__c>();
    string Value ='None';
    String Query ='SELECT Id,AccountAccessLevel,Accountid,OpportunityAccessLevel,UserOrGroupId FROM AccountShare WHERE OpportunityAccessLevel !='+ '\'Edit\''+' and LastModifiedDate< LAST_N_DAYS:1';
    global Database.QueryLocator start(Database.BatchableContext bc)
     {    
       system.debug('Query'+Query);
       return database.getQuerylocator(Query);
     }
    global void execute(Database.batchableContext info, List<AccountShare> AccShare)
    {
        for(AccountShare oAS: AccShare) 
        {
            oAccountIdMap.put(oAS.Accountid,oAS);
        }
        oppList =[Select id from Opportunity where Accountid in : oAccountIdMap.Keyset()];
        for(Opportunity oOpp :oppList)
        {
            oOpportunityIdMap.put(oOpp.id,oOpp);
        }
        oOppProdList =[Select id,OpportunityId__c,ownerID,SalesExecutive__c from OpportunityProduct__c where OpportunityId__c in : oOpportunityIdMap.Keyset()];
        for(OpportunityProduct__c oOppProd :oOppProdList)
        {
            oOpptyProdIdMap.put(oOppProd.id,oOppProd);
        }
        oOppProdShare =[Select id,ParentId from OpportunityProduct__Share where ParentId In: oOpptyProdIdMap.KeySet() And RowCause!='Owner'];
        delete oOppProdShare;
        GENcGrantSharingOnOLI.grantSharing(oOppProdList);            
    }
global void finish(Database.batchableContext info)
{        

} 
    
}