@isTest
public class test_iConnectOnOpp {

    static testMethod void method()
    { 
                Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
            insert u;
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        
            insert salesunitobject;    
        
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
            insert accountobject;
        
        Contact oContact =new Contact(firstname='',lastname='test',accountid=accountobject.Id,Email='test1@gmail.com',LeadSource='SEE Team Connect');
        	insert oContact;
        Opportunity oppObject = new Opportunity(name='test', accountid=accountobject.id,stageName = '1. Discover', CloseDate = System.Today()+1);
        insert oppObject;
        Lead oLead =new Lead(firstname='',lastname='test',Account__c=accountobject.Id,Company = 'test',Email='test2@gmail.com',LeadSource='SEE Team Connect');
        	insert oLead;
        
        G_Connector__c gc = new G_Connector__c(contact__c = oContact.id);
        insert gc;
        
        G_Connector__c gc2 = new G_Connector__c(Lead__c = oLead.id);
        insert gc2;
      
        test.startTest();
		iConnectOnOpp.fetchAccount(oppObject.id);
        iConnectOnOpp.fetchAccountLead(oppObject.id);
        iConnectOnOpp.mailToString(oppObject.id);
        test.stopTest();

    }
}