@isTest
public class GPBatchPidApprvlRemndrSchedularTracker {
    
    Public Static List<GP_Project_Address__c> listOfAddress = new List<GP_Project_Address__c>();
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    
    static void setupCommonData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;
        
        //GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        //insert exceptionemp;
        
        //GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        //empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        //insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        prjobj.GP_Project_Submission_Date__c = system.today().adddays(-5);
        prjobj.GP_Approval_Status__c = 'Pending for Approval';
        insert prjobj;
        prjobj.GP_Project_Submission_Date__c = system.today().adddays(-5);
        update prjobj;
        GP_Project__c prjObj = [select id, GP_Project_Submission_Date__c from GP_Project__c where id=: prjObj.id];
        system.assertEquals(system.today().adddays(-5), prjObj.GP_Project_Submission_Date__c);
    }
    
    public static testmethod  void testschedule() {
        Test.StartTest();
        GPBatchPidApprovalReminderSchedular obj = new GPBatchPidApprovalReminderSchedular();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, obj);
        Test.stopTest();
    }
}