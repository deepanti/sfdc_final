public class GPProjectTemplateWrapper {
    public String defaultJSON1{get; set;}
    public String defaultJSON2{get; set;}
    public String defaultJSON3{get; set;}
    
    public String deltaJSON1{get; set;}
    public String deltaJSON2{get; set;}
    public String deltaJSON3{get; set;}
    
    public String finalJSON1{get; set;}
    public String finalJSON2{get; set;}
    public String finalJSON3{get; set;}
    
    public String stageWiseFieldJson{get; set;}
    
    public String templateName{get; set;}

    public String businessName{get; set;}
    public String businessType{get; set;}
    
    public Id assesmentTemplateId{get; set;}
    
    public Boolean isActive{get; set;}
    public Boolean isVisible{get; set;}
    public Boolean isWithoutSFDC{get; set;}
    
    public String businessGroupL1{get; set;}
    public String businessSegmentL2{get; set;}
}