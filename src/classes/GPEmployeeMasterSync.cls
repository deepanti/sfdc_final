@RestResource(urlMapping='/EmployeeMaster/*')
global with sharing class GPEmployeeMasterSync {
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;        
        EmployeeMasterHRMSAPI__c objReqRes=new 	EmployeeMasterHRMSAPI__c();       
        objReqRes.RequestType__c='EmployeeMaster';
        String BatchNumber='';
        List<GPEmployeeICSResponse> responseList=new List<GPEmployeeICSResponse>(); 
        try{
            if(!String.isBlank(req.requestBody.toString()) && req.requestBody.toString() !='""'){
                
                Map<String,Object> employeeMasterDataMap=(Map<String,Object>)JSON.deserializeUntyped(req.requestBody.toString()); 
                
                List<object> empMasterLst =(List<object>) employeeMasterDataMap.get('GP_Employee_Master__c');       
                system.debug('==size==='+empMasterLst.size());
                List<GP_Employee_Master__c> lstEmpMaster=new List<GP_Employee_Master__c>();
                //List<GPEmployeeICSResponse> responseList=new List<GPEmployeeICSResponse>();       
                for(integer i=0; i< empMasterLst.size();i++)
                {
                    map<String,Object> m = (map<String,Object>) empMasterLst[i];
                    try{
                        GP_Employee_Master__c  gpEmpMasterObj=new 	GP_Employee_Master__c();           
                        
                        gpEmpMasterObj.Name=(String)m.get('Name');
                        //gpEmpMasterObj.GP_Band__c=(String)m.get('GP_Band__c');
                        gpEmpMasterObj.GP_Person_ID__c=(String)m.get('GP_Person_ID__c'); 
                        gpEmpMasterObj.GP_BUSINESS_GROUP_ID__c=(String)m.get('GP_BUSINESS_GROUP_ID__c');
                        gpEmpMasterObj.GP_Business_Group_NAME__c=(String)m.get('GP_Business_Group_NAME__c');
                        gpEmpMasterObj.GP_Employee_Number__c=(String)m.get('GP_Employee_Number__c');
                        gpEmpMasterObj.GP_EMP_EFF_START_DATE__c=ConvertStringToDate((String)m.get('GP_EMP_EFF_START_DATE__c'));
                        gpEmpMasterObj.GP_EMP_EFF_END_DATE__c=ConvertStringToDate((String)m.get('GP_EMP_EFF_END_DATE__c'));
                        gpEmpMasterObj.GP_NPW__c=(String)m.get('GP_NPW__c');
                        gpEmpMasterObj.GP_FIRST_NAME__c=(String)m.get('GP_FIRST_NAME__c');
                        gpEmpMasterObj.GP_MIDDLE_NAMES__c=(String)m.get('GP_MIDDLE_NAMES__c');
                        gpEmpMasterObj.GP_LAST_NAME__c=(String)m.get('GP_LAST_NAME__c');
                        gpEmpMasterObj.GP_FULL_NAME__c=(String)m.get('GP_FULL_NAME__c');            
                        gpEmpMasterObj.GP_NATIONAL_IDENTIFIER__c=(String)m.get('GP_NATIONAL_IDENTIFIER__c');
                        gpEmpMasterObj.GP_OFFICIAL_EMAIL_ADDRESS__c=((String)m.get('GP_OFFICIAL_EMAIL_ADDRESS__c')).replace(';','');
                        gpEmpMasterObj.GP_EMPLOYEE_TYPE__c=(String)m.get('GP_EMPLOYEE_TYPE__c');
                        
                        gpEmpMasterObj.GP_CURRENT_EMPLOYEE_FLAG__c=((String)m.get('GP_CURRENT_EMPLOYEE_FLAG__c')=='Y' && (String)m.get('GP_CURRENT_EMPLOYEE_FLAG__c') != '') ? 'Yes':'No'; 
                        gpEmpMasterObj.GP_CURRENT_NPW_FLAG__c=((String)m.get('GP_CURRENT_NPW_FLAG__c')=='Y' && (String)m.get('GP_CURRENT_NPW_FLAG__c') != '') ? 'Yes':'No';
                        
                        gpEmpMasterObj.GP_GENDER__c=(String)m.get('GP_GENDER__c');
                        gpEmpMasterObj.GP_Date_OF_BIRTH__c=ConvertStringToDate((String)m.get('GP_Date_OF_BIRTH__c'));
                        gpEmpMasterObj.GP_HIRE_Date__c=ConvertStringToDate((String)m.get('GP_HIRE_Date__c'));
                        gpEmpMasterObj.GP_DFF_DATE_JOINED_BUSINESS__c=ConvertStringToDate((String)m.get('GP_DFF_DATE_JOINED_BUSINESS__c'));
                        gpEmpMasterObj.GP_PAN_Number__c=(String)m.get('GP_PAN_Number__c');
                        //gpEmpMasterObj.GP_PERSONAL_EMAIL_ADDRESS__c=((String)m.get('GP_PERSONAL_EMAIL_ADDRESS__c')).replace(';','');
                        gpEmpMasterObj.GP_PPOS_START_date__c=ConvertStringToDate((String)m.get('GP_PPOS_START_date__c'));
                        gpEmpMasterObj.GP_PROJECTED_TERMINATION_Date__c=ConvertStringToDate((String)m.get('GP_PROJECTED_TERMINATION_Date__c'));
                        gpEmpMasterObj.GP_NOTIFIED_TERMINATION_Date__c=ConvertStringToDate((String)m.get('GP_NOTIFIED_TERMINATION_Date__c'));
                        gpEmpMasterObj.GP_ACTUAL_TERMINATION_Date__c=ConvertStringToDate((String)m.get('GP_ACTUAL_TERMINATION_Date__c'));
                        gpEmpMasterObj.GP_LEAVING_REASON__c=(String)m.get('GP_LEAVING_REASON__c');
                        gpEmpMasterObj.GP_TRANSFER_Business_Group__c=(String)m.get('GP_TRANSFER_Business_Group__c');                 
                        gpEmpMasterObj.GP_PREVIOUS_EXP__c=decimal.valueOf((String)m.get('GP_PREVIOUS_EXP__c') == '' ? '0' : (String)m.get('GP_PREVIOUS_EXP__c'));
                        gpEmpMasterObj.GP_TOTAL_EXP__c=decimal.valueOf((String)m.get('GP_TOTAL_EXP__c') == '' ? '0' : (String)m.get('GP_TOTAL_EXP__c'));
                        gpEmpMasterObj.GP_CURRENT_EXP__c=decimal.valueOf((String)m.get('GP_CURRENT_EXP__c') == '' ? '0' : (String)m.get('GP_CURRENT_EXP__c'));                
                        gpEmpMasterObj.GP_EMP_CREATION_DATE__c=ConvertStringToDate((String)m.get('GP_EMP_CREATION_DATE__c'));
                        gpEmpMasterObj.GP_EMP_LAST_UPDATE_DATE__c=ConvertStringToDate((String)m.get('GP_EMP_LAST_UPDATE_DATE__c'));
                        gpEmpMasterObj.GP_ORIGINAL_DATE_OF_HIRE__c=ConvertStringToDate((String)m.get('GP_ORIGINAL_DATE_OF_HIRE__c'));
                        gpEmpMasterObj.GP_KNOWN_AS__c=(String)m.get('GP_KNOWN_AS__c');
                        gpEmpMasterObj.GP_NATIONALITY__c=(String)m.get('GP_NATIONALITY__c');
                        gpEmpMasterObj.GP_MARRIAGE_ANNIVERSARY_Date__c=ConvertStringToDate((String)m.get('GP_MARRIAGE_ANNIVERSARY_Date__c'));
                        gpEmpMasterObj.GP_Batch_Number__c=(String)m.get('GP_Batch_Number__c');               
                        gpEmpMasterObj.GP_Final_OHR__c = (String)m.get('GP_Final_OHR__c');
                        BatchNumber=gpEmpMasterObj.GP_Batch_Number__c;
                        //gpEmpMasterObj.GP_Employee_Unique_Name__c = (String)m.get('GP_Final_OHR__c');
                        gpEmpMasterObj.GP_Attribute1__c=(String)m.get('GP_Attribute1__c');//unique field
                        gpEmpMasterObj.GP_Seq_Number__c=(String)m.get('GP_Seq_Number__c');//Seq Number
                        gpEmpMasterObj.GP_Oracle_Creation_Date__c=ConvertStringToDate((String)m.get('GP_Oracle_Creation_Date__c'));//Seq Number
						
						 if (gpEmpMasterObj.GP_EMPLOYEE_TYPE__c != null && 
                            (gpEmpMasterObj.GP_EMPLOYEE_TYPE__c.toLowerCase().startsWith('ex') 
                             || gpEmpMasterObj.GP_ACTUAL_TERMINATION_Date__c < System.today()))
                        {
                            gpEmpMasterObj.GP_isActive__c = false;                 
                            //employee.GP_EMPLOYEE_TYPE__c = employee.GP_CURRENT_NPW_FLAG__c=='Yes' ? 'Ex-contingent Worker' : 'Ex-Employee';
                                                      
                        }
                        else{
                            gpEmpMasterObj.GP_isActive__c = true;
                        }
                                                           
                        
                        if( String.isBlank(gpEmpMasterObj.Name))
                        {
                            GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
                            objRes.Id=(String)m.get('GP_Person_ID__c');
                            objRes.Status='Fail';
                            objRes.Message= 'Employee Master Name Required';
                            objRes.SeqNum=(String)m.get('GP_Seq_Number__c');
                            responseList.add(objRes);
                        }
                        else{                                       
                            lstEmpMaster.add(gpEmpMasterObj);
                        }
                    }
                    catch(exception ex){               
                        GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
                        objRes.Id=(String)m.get('GP_Person_ID__c');
                        objRes.Status='Fail';
                        objRes.Message=ex.getMessage();
                        objRes.SeqNum=(String)m.get('GP_Seq_Number__c');
                        responseList.add(objRes);
                    }
                }  
                system.debug('===lstEmpMaster==='+lstEmpMaster.size());
                system.debug('===responseList==='+responseList.size());
                
                //Start : check to handle employee on the basis of Eff Start date
                
                Set<String> personIdS=new Set<String>();
                for(GP_Employee_Master__c p : lstEmpMaster)
                {
                    personIdS.add(p.GP_Person_ID__c);
                }
                system.debug('===personIdS==='+personIdS);
                Map<String,GP_Employee_Master__c> empMasterBeforeUpsertMAP=new Map<String,GP_Employee_Master__c>();
                for(GP_Employee_Master__c empMstBeforeUpsert : [Select Id,GP_Person_ID__c, GP_EMP_EFF_START_DATE__c from GP_Employee_Master__c where
                                                                GP_Person_ID__c in: personIdS])
                {
                    
                    empMasterBeforeUpsertMAP.put(empMstBeforeUpsert.GP_Person_ID__c, empMstBeforeUpsert);
                }
                
                List<GP_Employee_Master__c> empMasterforUpsert=new List<GP_Employee_Master__c>();
                List<GP_Employee_Master__c> empMasterNoNeedToUpsert=new List<GP_Employee_Master__c>();
                //Map<String,GP_Employee_Master__c> mapNoNeedToInsertEmp=new Map<String,GP_Employee_Master__c>();
                system.debug('===empMasterBeforeUpsertMAP.keySet().size()==='+empMasterBeforeUpsertMAP.keySet().size());
                //if(empMasterBeforeUpsertMAP.keySet().size()>0){
                    for(GP_Employee_Master__c lem: lstEmpMaster)
                    {
                        
                        GP_Employee_Master__c gpempmst=empMasterBeforeUpsertMAP.get(lem.GP_Person_ID__c);
                        if(gpempmst != null)
                        {   
                            if(lem.GP_EMP_EFF_START_DATE__c >= gpempmst.GP_EMP_EFF_START_DATE__c || gpempmst.GP_EMP_EFF_START_DATE__c == null )
                            {
                                
                                empMasterforUpsert.add(lem);
                            }
                            else
                            {
                                
                                empMasterNoNeedToUpsert.add(lem);
                                //mapNoNeedToInsertEmp.put(lem.GP_Person_ID__c,lem);
                            }
                        }
                        else
                        {
                            system.debug('===lem==='+lem.GP_Person_ID__c);
                            empMasterforUpsert.add(lem); 
                        }
                    }     
                    system.debug('===empMasterforUpsert==='+empMasterforUpsert.size());
                    system.debug('===empMasterNoNeedToUpsert==='+empMasterNoNeedToUpsert.size());
                //}
                /*  for(GP_Employee_Master__c emplstMaster : lstEmpMaster)
{
if(mapNoNeedToInsertEmp.keySet().size()>0){
if(!mapNoNeedToInsertEmp.containsKey(emplstMaster.GP_Person_ID__c))
{                           
empMasterforUpsert.add(emplstMaster);
}
}
} */
                
                system.debug('===lstEmpMaster==='+lstEmpMaster.size());
                system.debug('===empMasterforUpsert==='+empMasterforUpsert.size());
                lstEmpMaster=empMasterforUpsert;
                
                if(empMasterNoNeedToUpsert.size()>0)
                {
                    
                    for(Integer index = 0, size = empMasterNoNeedToUpsert.size(); index < size; index++) {
                        GPEmployeeICSResponse resp=new GPEmployeeICSResponse();
                        resp.Id=empMasterNoNeedToUpsert[index].GP_Person_ID__c;
                        resp.Status='Fail';
                        resp.SeqNum=empMasterNoNeedToUpsert[index].GP_Seq_Number__c;
                        resp.Message='Latest employee record already exists.';
                        if(resp.Status!=null)
                            responseList.add(resp);
                    }
                    
                }
                system.debug('===responseList 1==='+responseList.size());
                //End : check to handle employee on the basis of Eff Start date
                
                if(lstEmpMaster.size()>0){
                    system.debug('===lstEmpMaster==='+lstEmpMaster.size());
                    Schema.SObjectField f = GP_Employee_Master__c.Fields.GP_Person_ID__c;
                    Database.UpsertResult [] srList = Database.upsert(lstEmpMaster, f, false);
                    List<GP_Employee_Master__c> duplicateMasterList= new List<GP_Employee_Master__c>();
                    system.debug('===srList.size()==='+srList.size());
                    for(Integer index = 0, size = srList.size(); index < size; index++) {
                        GPEmployeeICSResponse resp=new GPEmployeeICSResponse();
                        if(srList[index].isSuccess()) {  
                            resp.Id=lstEmpMaster[index].GP_Person_ID__c;
                            resp.SeqNum=lstEmpMaster[index].GP_Seq_Number__c;
                            resp.Status='Success';
                        }
                        else if (!srList.get(index).isSuccess()){ 
                            Database.Error error = srList.get(index).getErrors().get(0);
                            String failedDML = error.getMessage();  
                            system.debug('===failedDML==='+failedDML);
                            if(failedDML.contains('Duplicate external id specified'))
                            {
                                duplicateMasterList.add(lstEmpMaster[index]);              
                            }
                            else{
                                system.debug('===lstEmpMaster[index].GP_Person_ID__c==='+lstEmpMaster[index].GP_Person_ID__c);
                                resp.Id=lstEmpMaster[index].GP_Person_ID__c;
                                resp.Status='Fail';
                                resp.SeqNum=lstEmpMaster[index].GP_Seq_Number__c;
                                resp.Message=failedDML;      
                            }
                        }
                        if(resp.Status!=null)
                            responseList.add(resp);
                    }
                    system.debug('===responseList 2==='+responseList.size());
                    if(duplicateMasterList.size()>0){
                        SetToRunEMPMasterORHRMSTrigger.isRunWorklocationCodeOnce=null;
                        Set<String> personId=new Set<String>();
                        for(GP_Employee_Master__c emp : duplicateMasterList)
                        {
                            personId.add(emp.GP_Person_ID__c);
                        }  
                        
                        Map<String,List<GP_Employee_Master__c>> mapUniquePersonId=new Map<String,List<GP_Employee_Master__c>>();
                        for(String st: personId)
                        {
                            List<GP_Employee_Master__c> segregateEmployeeDupMaster=new List<GP_Employee_Master__c>();
                            for(GP_Employee_Master__c empMst:duplicateMasterList)
                            {                 
                                if(st==empMst.GP_Person_ID__c){                    
                                    segregateEmployeeDupMaster.add(empMst);}
                            }
                            mapUniquePersonId.put(st,segregateEmployeeDupMaster);
                        }
                        //system.debug('===mapUniquePersonId==='+mapUniquePersonId.keySet());
                        
                        List<GP_Employee_Master__c> ActualEmployeeFrmDup=new List<GP_Employee_Master__c>();
                        for(String key:mapUniquePersonId.KeySet())
                        {
                            List<GP_Employee_Master__c> lstEmpDataByKey=new  List<GP_Employee_Master__c>();
                            lstEmpDataByKey=mapUniquePersonId.get(key);
                            GP_Employee_Master__c actEmp=new GP_Employee_Master__c();
                            //system.debug('===lstEmpDataByKey==='+lstEmpDataByKey.size());
                            actEmp=lstEmpDataByKey[0];
                            for(Integer i=0; i < lstEmpDataByKey.size()-1;i++)
                            {        
                                
                                if(actEmp.GP_Person_ID__c == lstEmpDataByKey[i+1].GP_Person_ID__c )  
                                {
                                    if(actEmp.GP_EMP_EFF_START_DATE__c > lstEmpDataByKey[i+1].GP_EMP_EFF_START_DATE__c)
                                    {                  
                                        actEmp = actEmp;                 
                                    }
                                    else{
                                        actEmp = lstEmpDataByKey[i+1];
                                    }
                                }               
                            }
                            ActualEmployeeFrmDup.add(actEmp);
                        }
                        if(ActualEmployeeFrmDup.size()>0)
                        {
                            Database.UpsertResult [] srActList = Database.upsert(ActualEmployeeFrmDup, f, false);
                            
                            for(Integer index = 0, size = srActList.size(); index < size; index++) {
                                GPEmployeeICSResponse respN=new GPEmployeeICSResponse();
                                if(srActList[index].isSuccess()) {  
                                    respN.Id=ActualEmployeeFrmDup[index].GP_Person_ID__c;
                                    respN.SeqNum=ActualEmployeeFrmDup[index].GP_Seq_Number__c;
                                    respN.Status='Success';
                                }
                                else if (!srActList.get(index).isSuccess()){
                                    // DML operation failed
                                    
                                    Database.Error errorN = srActList.get(index).getErrors().get(0);
                                    String failedDML = errorN.getMessage(); 
                                    respN.Id=ActualEmployeeFrmDup[index].GP_Person_ID__c;
                                    respN.Status='Fail';
                                    respN.SeqNum=ActualEmployeeFrmDup[index].GP_Seq_Number__c;
                                    respN.Message=errorN.getMessage();      
                                }
                                responseList.add(respN);
                            }
                            
                        }
                        system.debug('===responseList 3==='+responseList.size());
                        
                        //for duplicate external id sending error response
                        List<GP_Employee_Master__c> FakeEmployeeFrmDup=new List<GP_Employee_Master__c>();
                        for(GP_Employee_Master__c empMas: ActualEmployeeFrmDup)
                        {
                            for(GP_Employee_Master__c dpEmp: duplicateMasterList)
                            {
                                if(empMas.GP_Person_ID__c == dpEmp.GP_Person_ID__c )  
                                {
                                    if((empMas.GP_EMP_EFF_START_DATE__c != dpEmp.GP_EMP_EFF_START_DATE__c) || 
                                       (empMas.GP_Seq_Number__c != dpEmp.GP_Seq_Number__c))
                                    {
                                        FakeEmployeeFrmDup.add(dpEmp);
                                    }
                                }
                            }
                        }
                        for(Integer index = 0, size = FakeEmployeeFrmDup.size(); index < size; index++) {
                            GPEmployeeICSResponse respNM=new GPEmployeeICSResponse();
                            respNM.Id=FakeEmployeeFrmDup[index].GP_Person_ID__c;
                            respNM.Status='Fail';
                            respNM.SeqNum=FakeEmployeeFrmDup[index].GP_Seq_Number__c;
                            respNM.Message='Latest employee record already exists.';   
                            responseList.add(respNM);
                        }  
                        system.debug('===responseList 4==='+responseList.size());
                        //End here - for duplicate external id sending error response 
                    }
                }
                String strJSON = '{ "Employee_Master_Response":';
                strJSON +=  JSON.serialize(responseList);   
                strJSON +='}';  
                objReqRes.Request_Data__c=String.valueOf(empMasterLst.size());
                upsert objReqRes; 
                AddAttachment('Employee_Master_Request',objReqRes.Id,req.requestBody.toString(),BatchNumber);
                AddAttachment('Employee_Master_Response',objReqRes.Id,strJSON,BatchNumber);
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(strJSON);
                
            }
            else
            {
                GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
                objRes.Id='';
                objRes.Status='Fail';
                objRes.Message='Blank Request';
                objRes.SeqNum='';
                responseList.add(objRes);
                String strJSON = '{ "Employee_Master_Response":';
                strJSON +=  JSON.serialize(responseList);   
                strJSON +='}';
                objReqRes.Request_Data__c='Blank Request';
                //objReqRes.response__c=  strJSON;
                upsert objReqRes; 
                AddAttachment('Employee_Master_Request',objReqRes.Id,req.requestBody.toString(),BatchNumber);
                AddAttachment('Employee_Master_Response',objReqRes.Id,strJSON,BatchNumber);
                system.debug('===responseList 5==='+responseList.size());
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(strJSON);
            }
        }
        catch(exception ex)
        {           
            GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
            objRes.Id='';
            objRes.Status='Fail';
            objRes.Message=ex.getMessage();
            objRes.SeqNum='';
            responseList.add(objRes);
            String strJSON = '{ "Employee_Master_Response":';
            strJSON +=  JSON.serialize(responseList);
            strJSON +='}';
            
            //objReqRes.response__c=  strJSON;
            upsert objReqRes; 
            AddAttachment('Employee_Master_Request',objReqRes.Id,req.requestBody.toString(),BatchNumber);
            AddAttachment('Employee_Master_Response',objReqRes.Id,strJSON,BatchNumber);
            system.debug('===responseList 6==='+responseList.size());
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(strJSON);
        }
    }     
    global static void AddAttachment(String Name, String ParentId, String body,String BatchNumber)
    {
        Attachment attach = new Attachment();
        attach.contentType = 'text/plain';
        attach.name = Name+'_'+system.now()+'_'+BatchNumber; 
        attach.parentId = ParentId;                
        attach.body = Blob.valueOf(body);
        insert attach;
    }    
    global static Date ConvertStringToDate(String MapDate)
    {        
        if(!String.isBlank(MapDate))
        {
            MapDate = MapDate.replace('4712','4000');//check for infinity value
        }
        
        return String.isBlank(MapDate) ? null :Date.valueOf(MapDate);        
    }
}