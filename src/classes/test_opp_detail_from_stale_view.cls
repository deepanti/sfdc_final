@istest
Public class test_opp_detail_from_stale_view
{

    Static testmethod void testmethod4()
    {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
            
            
            Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
                insert salesunitobject;
            account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
                insert accountobject;
            
            
            opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+60,Revenue_Start_Date__c=system.today()+61,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting',Last_stage_change_date__c=system.today()-25);
                insert opp;
                      
                
            Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
                insert oContact;
            OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
                insert oppcontactrole;

            Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
                insert oProduct;
            OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Analytics',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
                insert OLi;
            
        //ApexPages.StandardController std = new ApexPages.StandardController(opp);
        PageReference tpageRef = Page.Redirect_to_opp_detail_from_stale_view;
        
        ApexPages.StandardController std = new ApexPages.StandardController(opp);
        
        Test.setCurrentPage(tpageRef);
        ApexPages.currentPage().getParameters().put('oppid', opp.id);
        Redirect_opp_from_stale controller = new Redirect_opp_from_stale();
        controller.Opp =opp;
        controller.opportunityid = opp.id;
        controller.update_TS_flag();
        /*
        Redirect_opp_from_stale controller = new Redirect_opp_from_stale(opp);
        system.debug('oppid' +opp.id);
        controller.opportunityid = opp.id;
        controller.update_TS_flag();*/
        
        
    }


}