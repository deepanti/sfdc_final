@isTest
public class GPSelectorBillingMilestoneTracker {
    @testSetup
    static void buildDependencyData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        GP_Project__c prjobj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjobj.OwnerId = objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c = 'Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c = system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        insert prjobj;

        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'INR';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;
    }
  
    @isTest
    public static void testSelectProjectBillingMilestoneRecords() {
        GP_Billing_Milestone__c billingMileStone = [Select Id from GP_Billing_Milestone__c LIMIT 1];
        GP_Project__c project = [Select Id from GP_Project__c LIMIT 1];
        
        GPSelectorBillingMilestone billingMilestoneSelector = new GPSelectorBillingMilestone();    
        billingMilestoneSelector.selectCurrencyFromProject(new Set<Id>());
        billingMilestoneSelector.queryOnBillingMilestone(null);
        billingMilestoneSelector.selectById(new Set<Id>());
        billingMilestoneSelector.selectProjectBillingMilestoneRecord(billingMilestone.Id);
        billingMilestoneSelector.selectProjectBillingMilestoneRecords(project.Id);
        billingMilestoneSelector.selectProjectBillingMilestoneRecords(new Set<Id> ());
        billingMilestoneSelector.selectProjectBillingMilestones(new List<Id>());
        
    }
}