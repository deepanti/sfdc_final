public class VIC_ComponentIncentiveSumWrapper{
    
    @AuraEnabled public String strTCVAcceleratorIO = '0';
    @AuraEnabled public String strTCVAcceleratorTS = '0';
    @AuraEnabled public String strOP = '0';
    @AuraEnabled public String strPM = '0';
    @AuraEnabled public String strMBO = '0';
    @AuraEnabled public String strTCV = '0';
    @AuraEnabled public String strIOTCV = '0';
    @AuraEnabled public String strTSTCV = '0';
    @AuraEnabled public String strDiscretionaryPayment = '0';
    @AuraEnabled public String strTotalScorecard = '0';
    @AuraEnabled public String strTotalAmtScorecardInLocal = '0';
    //Incentive Record against component type
    @AuraEnabled public List<Target_Achievement__c> lstTCVAcceleratorIO = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstTCVAcceleratorTS = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstOP = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstPM = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstMBO = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstTCV = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstIOTCV = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstTSTCV = new List<Target_Achievement__c>();
    @AuraEnabled public List<Target_Achievement__c> lstDiscretionaryPayment = new List<Target_Achievement__c>();
    //Set of all Incentive Id for all scorecard
    @AuraEnabled public List<Id> lstIncentiveId = new List<Id>();
    //Adjustment Variable
    @AuraEnabled public String adjustmentTCVAcceleratorIO = '';
    @AuraEnabled public String adjustmentTCVAcceleratorTS = '';
    @AuraEnabled public String adjustmentAdjustmentOP = '';
    @AuraEnabled public String adjustmentPM = '';
    @AuraEnabled public String adjustmentMBO = '';
    @AuraEnabled public String adjustmentTCV = '';
    @AuraEnabled public String adjustmentIOTCV = '';
    @AuraEnabled public String adjustmentTSTCV = '';
    @AuraEnabled public String adjustmentDiscretionary = '';
}