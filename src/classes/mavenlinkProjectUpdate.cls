global class mavenlinkProjectUpdate implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'Select Id, mavenlink__Automatically_Created__c, mavenlink__Opportunity__c, mavenlink__Account__c, mavenlink__Related_OLI__c From mavenlink__Mavenlink_Project__c where mavenlink__Opportunity__c=null';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<mavenlink__Mavenlink_Project__c> Projects) {
        StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = 'ProjectUpload' LIMIT 1];
        String body = sr.Body.toString();
        list<string> lines= body.split('[\n\r]');
        
        for(integer i=0; i<Projects.size();i++){
            try{
                for(string l:lines){
                    list<string> parts=l.split('\\|');   
                    string projId=parts[0];
                    if(Projects[i].id==projId){
                        list<string> targets=parts[1].split(',');
                        string oppId = targets[0];
                        string acctId=targets[1];
                        string oliId=targets[2];
                        Projects[i].mavenlink__Opportunity__c=oppId;
                        Projects[i].mavenlink__Account__c=acctId;
                        Projects[i].mavenlink__Related_OLI__c=oliId;
                        Projects[i].mavenlink__Automatically_Created__c=true;
                        Update Projects[i];
                        Projects.remove(i);
                    }   
                }
            }catch(Exception e){} 
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}