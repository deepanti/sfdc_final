@isTest
private class GENcOpportunityProductDetailTest
{
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,0,12000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunityProduct);
        GENcOpportunityProductDetail  oGENcOpportunityProductDetail  = new GENcOpportunityProductDetail(std);
        oGENcOpportunityProductDetail.revenueRollingYearIndex = 1;
        GENcOpportunityProductDetail.ProductScheduleWrapper oProductScheduleWrapper = new GENcOpportunityProductDetail.ProductScheduleWrapper(oProductSchedule,1);
        GENcOpportunityProductDetail.RevenueRollingYearWrapper oRevenueRollingYearWrapper = new GENcOpportunityProductDetail.RevenueRollingYearWrapper(100);
        oGENcOpportunityProductDetail.doCancel();
        oGENcOpportunityProductDetail.getRevenueRollingYear();
        oGENcOpportunityProductDetail.getProductShedule();
        //oGENcOpportunityProductDetail.createSchedule();
        //oGENcOpportunityProductDetail.getScheduleTypes();
       // oGENcOpportunityProductDetail.getInstallmentPeriods();
       // oGENcOpportunityProductDetail.getHasRevenueRollingYearTwoRendered();
       // oGENcOpportunityProductDetail.gethasScheculeListRollingYearTwoRendered();
       // oGENcOpportunityProductDetail.sethasScheculeListRollingYearTwoRendered(true);
       // oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
        oGENcOpportunityProductDetail.gethasScheculeListRollingYearOneRendered();
        oGENcOpportunityProductDetail.sethasScheculeListRollingYearOneRendered(true);
        oGENcOpportunityProductDetail.sethasScheculeListRollingYearOneRendered(true);
        oGENcOpportunityProductDetail.getHasScheculeListRendered();
        oGENcOpportunityProductDetail.setHasRevenueRollingYearTwoRendered(true);
        oGENcOpportunityProductDetail.setHasScheculeListRendered(true);
        oGENcOpportunityProductDetail.gethasScheculeListRollingYearTwoRendered();
        oGENcOpportunityProductDetail.sethasScheculeListRollingYearTwoRendered(true);
        oGENcOpportunityProductDetail.getHasRevenueRollingYearTwoRendered();
        oGENcOpportunityProductDetail.getHasScheculeSectionRendered();
        oGENcOpportunityProductDetail.setHasScheculeSectionRendered(true);
        oGENcOpportunityProductDetail.revenueScheduleList = new List<RevenueSchedule__c>();
        oGENcOpportunityProductDetail.isEditScheduleClicked = 'Y';
        oGENcOpportunityProductDetail.revenueSplitRollingYear = '2';
        oGENcOpportunityProductDetail.productScheduleWrapperList = new List<GENcOpportunityProductDetail.ProductScheduleWrapper>();
        oGENcOpportunityProductDetail.revenueForRollingYearOne = 4.0;
        oGENcOpportunityProductDetail.revenueForRollingYearTwo = 5.0;
        oGENcOpportunityProductDetail.revenueToSplitUp = 3.0;
        oGENcOpportunityProductDetail.revenueRollingYearToDelete = 1;
        oGENcOpportunityProductDetail.revenueRollingYearToEdit=2;
        oGENcOpportunityProductDetail.doClone();
        oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper.setHasAutoPopulateDisabled(true); 
        
    }
    
}