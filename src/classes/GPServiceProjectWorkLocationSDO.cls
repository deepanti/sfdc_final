public with sharing class GPServiceProjectWorkLocationSDO {
    
    public static Boolean isFormattedLogRequired;
    public static Boolean isLogForTemporaryId;

    public static Map<String, List<String>> validateWorkLocationAndSDO(GP_Project__c project, List<GP_Project_Work_Location_SDO__c> listOfProjectWorkLocation) {
        Map<String, List<String>> mapOfSubSectionToListOfErrors = new Map<String, List<String>>();
        // HSN Changes
        Integer primaryCount = 0, projectWorkLocationCount = 0, sezWorkLocationCount = 0, gstWorkLocationCount = 0;
        String key;
        
        // ECR-HSN Changes
        String billable = System.Label.GP_Billable_Project_Type_Values;
        // HSN Changes
        Map<String, GP_Pinnacle_Master__c> mapOfGSTEnabledOUs = getGSTEnabledOUs();
        
        if(listOfProjectWorkLocation != null && listOfProjectWorkLocation.size() > 0) {
            for(GP_Project_Work_Location_SDO__c projectWorkLocation : listOfProjectWorkLocation) {
                
                if(projectWorkLocation.RecordType.Name == 'Work Location') {
                    projectWorkLocationCount++;
                }

                if(isFormattedLogRequired) {
                    key = projectWorkLocation.RecordType.Name;
                } else if(isLogForTemporaryId) {
                    key = projectWorkLocation.GP_Last_Temporary_Record_Id__c;
                } else if(!isLogForTemporaryId) {
                    key = projectWorkLocation.Id;
                }

                //update primary count.
                if(projectWorkLocation.GP_Primary__c) {
                    primaryCount++;
                }
                
                //SEZ work location count.
                /*if(String.isNotBlank(projectWorkLocation.GP_Work_Location__r.GP_IsSEZ__c)) {
                    sezWorkLocationCount++;
                }*/
                
                //work location field can not be empty
                if(projectWorkLocation.GP_Work_Location__c == null) {
                    String validationMessage = 'Work Location can not be empty.';                    
                    
                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                    continue;
                }
                
                if(isFormattedLogRequired) {
                    key = projectWorkLocation.RecordType.Name;
                } else if(isLogForTemporaryId) {
                    key = projectWorkLocation.GP_Last_Temporary_Record_Id__c;
                } else if(!isLogForTemporaryId) {
                    key = projectWorkLocation.Id;
                }

                //validate active and visible status check for work location
                if(projectWorkLocation.GP_Work_Location__r.GP_Status__c == 'Inactive' ) {
                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' is not Active';                    
                    
                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }

                //validate Start date and end date
                // DS-22-11-18:: Have commented below validation to avoid getting error 
                // for previously selected closed Work Locations.                
                /*if(//projectWorkLocation.GP_Work_Location__r.GP_Start_Date_Active__c > project.GP_Start_Date__c ||
                    (projectWorkLocation.GP_Work_Location__r.GP_End_Date_Active__c != null && 
                    projectWorkLocation.GP_Work_Location__r.GP_End_Date_Active__c  < project.GP_End_Date__c)) {

                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' is not Active for project start and end date range';                    
                    
                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }*/

                //validate billable check
                if(project.GP_Deal_Category__c != 'Support' && 
                    projectWorkLocation.RecordType.Name == 'Work Location' && 
                   projectWorkLocation.GP_Primary__c && 
                   projectWorkLocation.GP_Work_Location__r.GP_Billable__c != 'Yes') {
                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' is not billable';                    

                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }   

                //validate hide for BPM check in case of BPM Projects.
                if(projectWorkLocation.GP_Project_Record_Type__c == 'BPM' && projectWorkLocation.GP_Work_Location__r.GP_Hide_For_BPM__c) {
                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' is not available for a BPM Project.';                    

                    if(!mapOfSubSectionToListOfErrors.containsKey(projectWorkLocation.GP_Work_Location__r.RecordType.Name)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                } 

                //if GP_Bill_To_Country__c of project is India then GP_Country__c of work location can only be india
                if(project.GP_Bill_To_Country__c == 'India' && projectWorkLocation.GP_Work_Location__r.GP_Country__c != 'India') {
                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' is not applicable for Indian Projects.';                    

                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                } 

                if(projectWorkLocation.GP_Primary__c &&  projectWorkLocation.GP_is_Active__c) {
                    String validationMessage = projectWorkLocation.GP_Work_Location__r.Name + ' cannot be deactiavted as it is the primary work location.';                    

                    if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List<String>());
                    }

                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                }
                // HSN Changes
                // Count of WLs which have GST enabled locations tagged.
                if(mapOfGSTEnabledOUs.containsKey(projectWorkLocation.GP_Work_Location_LE_Code__c)) {
                    gstWorkLocationCount++;
                }
            }
        }       
        
        if(project.RecordType.Name != 'BPM' && project.GP_Deal_Category__c != 'Support' && primaryCount == 0) {
            String validationMessage = 'Primary Work Location not defined.';

            if(isFormattedLogRequired) {
                key = 'Work Location';
            } else if(isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if(!isLogForTemporaryId) {
                key = project.Id;
            }

            if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                mapOfSubSectionToListOfErrors.put(key, new List<String>());
            }

            mapOfSubSectionToListOfErrors.get(key).add(validationMessage);  
        } else if((project.RecordType.Name == 'BPM' || project.GP_Deal_Category__c == 'Support') && 
                    projectWorkLocationCount == 0) {
            String validationMessage = 'Atleast one Work Location is mandatory.';                    

            if(isFormattedLogRequired) {
                key = 'Work Location';
            } else if(isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if(!isLogForTemporaryId) {
                key = project.Id;
            }

            if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                mapOfSubSectionToListOfErrors.put(key, new List<String>());
            }

            mapOfSubSectionToListOfErrors.get(key).add(validationMessage);  
        } /*else if(sezWorkLocationCount > 0 && project.GP_Project_Currency__c == 'INR') {
            String validationMessage = 'SEZ Work Location can\'t be assigned to project with \'INR\' project currency.';
            
            if(isFormattedLogRequired) {
                key = 'Work Location';
            } else if(isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if(!isLogForTemporaryId) {
                key = project.Id;
            }

            if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                mapOfSubSectionToListOfErrors.put(key, new List<String>());
            }

            mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
        }*/
       	                   
        // ECR-HSN Changes
        // HSN Changes
        // Tagged Work location is GST enabled and OU is non GST and HSN details are not captured.        
        else if(project.GP_Oracle_PID__c == 'NA' && 
        	String.isBlank(project.GP_Parent_Project__c) && 
        	(project.RecordType.Name != 'Indirect PID' ||
            (String.isNotBlank(project.GP_Project_type__c) && 
             billable.containsIgnoreCase(project.GP_Project_type__c))) && 
             gstWorkLocationCount > 0 && String.isBlank(project.GP_HSN_Category_ID__c) && 
             !project.GP_Operating_Unit__r.GP_Is_GST_Enabled__c) {
            String validationMessage = 'HSN Category is mandatory, atleast one GST work location is tagged.';
            
            if(isFormattedLogRequired) {
                key = 'Work Location';
            } else if(isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if(!isLogForTemporaryId) {
                key = project.Id;
            }

            if(!mapOfSubSectionToListOfErrors.containsKey(key)) {
                mapOfSubSectionToListOfErrors.put(key, new List<String>());
            }

            mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
        }

        return mapOfSubSectionToListOfErrors; 
    }
    // HSN Changes
    private static Map<String, GP_Pinnacle_Master__c> getGSTEnabledOUs() {
        Map<String, GP_Pinnacle_Master__c> mapOfLECodeWithOUs = new Map<String, GP_Pinnacle_Master__c>();
        
        for(GP_Pinnacle_Master__c pm : GPSelectorPinnacleMasters.getGSTEnabledOUs()) {
            mapOfLECodeWithOUs.put(pm.GP_LE_Code__c, pm);
        }
        
        return mapOfLECodeWithOUs;
    }

    public class GPServiceProjectWorkLocationSDOException extends Exception {} 
}