@isTest
public class GPDomainTimesheetEntryTracker {
    public static GP_Timesheet_Transaction__c objTimeSheetTransaction;
    public static GP_Employee_Master__c empObj;
    public static GP_Project__c prjObj;
    @testSetup static void setupCommonData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_timesheet_entry__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id); 
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        insert prjObj ;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        insert address ;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert resourceAllocationObj;
        
        GP_Employee_HR_History__c objEmployeeHR = GPCommonTracker.getEmployeeHR(empObj.id);
        insert objEmployeeHR;
        
        /*GPTriggerSwitch__c triggerSwitch = GPCommonTracker.getTriggerSwitch('GPDomainTimeSheetEntryTrigger');
        insert triggerSwitch;*/
    }     
    @isTest
    public static void testGPDomainTimesheetEntryTracker()
    {
        fetchData();
        testCreateTimeSheetEntry();
    }
    public static void fetchData() { 
        
        objTimeSheetTransaction = [select id from GP_Timesheet_Transaction__c limit 1];
        empObj =[select id from GP_Employee_Master__c limit 1];
        prjObj = [select id from GP_Project__c limit 1];
    }
    public static void testCreateTimeSheetEntry() { 
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,objTimeSheetTransaction);
        timeshtentryObj.GP_Date__c = system.today();
        insert timeshtentryObj ;
        update timeshtentryObj;
    }
}