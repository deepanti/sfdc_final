global class HelperForSharingTrigger
{
@future public static void insertsharingrec(Set<String> olids)
{

    //oli1 id === userid1, oli1 id === userid2, oli2 id === userid2

    Map<String,List<String>> mapoliuserid = new Map<String,List<String>>();
    List<String> Templist = new List<String>();
    for(String temp : olids)
    {
        Templist = new List<String>();
        Templist = temp.split('=&&=');
        System.debug('$$$$$$$$$=='+temp+'   '+Templist);
        
        if(mapoliuserid.containskey(Templist[0]))
        {
            list<String> TempLst = mapoliuserid.get(Templist[0]);
            TempLst.add(Templist[1]);
            mapoliuserid.put(Templist[0],TempLst);
        }
        else
        {
            list<String> TempLst = new list<String>();
            TempLst.add(Templist[1]);
            mapoliuserid.put(Templist[0],TempLst);
        }
    
    }
        System.debug('mapoliuserid $$$$$$$$$=='+mapoliuserid);
    
    list<OpportunityProduct__Share> LstToInsert = new List<OpportunityProduct__Share>();

    for(String keyval : mapoliuserid.keyset())
    {
        for(String userid : mapoliuserid.get(keyval))
        {
            OpportunityProduct__Share OLIShr  = new OpportunityProduct__Share();
            OLIShr.ParentId = keyval;
            OLIShr.UserOrGroupId = userid;
            OLIShr.AccessLevel = 'Edit';
            LstToInsert.add(OLIShr);
        }
    }
    system.debug('future LstToInsert==='+LstToInsert);
    insert LstToInsert;
    
}
}