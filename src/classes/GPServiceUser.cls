public with sharing class GPServiceUser {
    @future
    public Static Void DeactivateUserRole(Set<Id> setOfUserId)
    {
        list<GP_User_Role__c> lstUserRole = [select Id,Name,GP_Active__c from GP_User_Role__c 
                                             where GP_User__c in : setOfUserId AND GP_Active__c = true];
        if(lstUserRole != null && lstUserRole.size() > 0) {
            for(GP_User_Role__c objUserRole : lstUserRole){
                objUserRole.GP_Active__c = False;
            }
        }
        update lstUserRole;
    } 
    public class GPServiceUserException extends Exception {} 
}