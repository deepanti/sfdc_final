/**
* @author Anmol.kumar
* @date 22/12/2017
*
* @group GPApprovalLogger
* @group-content ../../ApexDocContent/GPApprovalLogger.html
*
* @description Apex Utility class to keep log of project approval.
*/
public class GPApprovalLogger {    
    private static final String PROJECT_LABEL = 'Project';

    
    public Boolean isFormattedLogRequired = true,
                    isValid = true;
    
    private Map<String, Map<String, List<String>>> mapOfSectionToSubSectionNameToListOfErrors;
    private map<String, List<String>> mapOfSobjectIdToListOfErrors;

    /**
    * @description Default Constructor, Instantiates mapOfSectionToSubSectionNameToListOfErrors
    * 
    * @example
    * new GPApprovalLogger();
    */
    public GPApprovalLogger() {
        mapOfSectionToSubSectionNameToListOfErrors = new Map<String, Map<String, List<String>>>();
    }
    
    /**
    * @description Parameterized Constructor.
    * @param isFormattedLogRequired: Boolean
    * 
    * @example
    * new GPApprovalLogger(<true/false>);
    */
    public GPApprovalLogger(Boolean isFormattedLogRequired) {
		this.isFormattedLogRequired = isFormattedLogRequired;
        mapOfSobjectIdToListOfErrors = new map<String, List<String>>();
        mapOfSectionToSubSectionNameToListOfErrors = new Map<String, Map<String, List<String>>>();
    }
    
    /**
    * @description Returns isValid Attribute.
    * 
    * @return returns isValid
    * 
    * @example
    * new GPApprovalLogger().isValid();
    */
    public Boolean isValid() {
        return isValid;
    }
    
    /**
    * @description adds error message for project section.
    * @param String error
    * 
    * @return GPAuraResponse json of Project leadership data
    * 
    * @example
    * new GPApprovalLogger().getRecordType(<error>);
    */
    public void addError(String error) {
        String sectionName = PROJECT_LABEL;
        String subSectionName = PROJECT_LABEL;
        addError(sectionName, subSectionName, error);
    }
    
    /**
    * @description adds list of error message for project section.
    * @param List<String> errors
    * 
    * @return GPAuraResponse json of Project leadership data
    * 
    * @example
    * new GPApprovalLogger().addError(<errors>);
    */
    public void addError(List<String> errors) {
        String sectionName = PROJECT_LABEL;
        String subSectionName = '';
        addError(sectionName, subSectionName, errors);
    }
    
    /**
    * @description add error for a section.
    * @param String sectionName,String subSectionName, String error
    * 
    * @example
    * new GPApprovalLogger().addError(sectionName, subSectionName, error);
    */
    public void addError(String sectionName,String subSectionName, String error) {
        if(!mapOfSectionToSubSectionNameToListOfErrors.containsKey(sectionName))
            mapOfSectionToSubSectionNameToListOfErrors.put(sectionName, new Map<String, List<String>>());
        
        Map<String, List<String>> mapOfSubSectionNameToListOfError = mapOfSectionToSubSectionNameToListOfErrors.get(sectionName);
        if(!mapOfSubSectionNameToListOfError.containsKey(subSectionName)) {
            mapOfSubSectionNameToListOfError.put(subSectionName, new List<String>());
        }
        
        mapOfSubSectionNameToListOfError.get(subSectionName).add(error);
        
        mapOfSectionToSubSectionNameToListOfErrors.put(sectionName, mapOfSubSectionNameToListOfError);
        isValid = false;
    }
    
    /**
    * @description add list of error for a section.
    * @param String sectionName,String subSectionName, List<String> errors
    * 
    * @example
    * new GPApprovalLogger().addError(sectionName, subSectionName, errors);
    */
    public void addError(String sectionName, String subSectionName, List<String> errors) {
        if(!mapOfSectionToSubSectionNameToListOfErrors.containsKey(sectionName))
            mapOfSectionToSubSectionNameToListOfErrors.put(sectionName, new Map<String, List<String>>());
        
        Map<String, List<String>> mapOfSubSectionNameToListOfError = mapOfSectionToSubSectionNameToListOfErrors.get(sectionName);
        if(!mapOfSubSectionNameToListOfError.containsKey(subSectionName)) {
            mapOfSubSectionNameToListOfError.put(subSectionName, new List<String>());
        }
        
        mapOfSubSectionNameToListOfError.get(subSectionName).addAll(errors);
        
        mapOfSectionToSubSectionNameToListOfErrors.put(sectionName, mapOfSubSectionNameToListOfError);
        isValid = false;
    }
    
    /**
    * @description add error for a record.
    * @param Id sObjectId, String errorMessage
    * 
    * @example
    * new GPApprovalLogger().addError(sObjectId, errorMessage);
    */
    public void addError(String sObjectId, String errorMessage) {
        if(!mapOfSobjectIdToListOfErrors.containsKey(sObjectId)) {
            mapOfSobjectIdToListOfErrors.put(sObjectId, new List<String>());
        }
        mapOfSobjectIdToListOfErrors.get(sObjectId).add(errorMessage); 
        isValid = false;       
    }
    
    /**
    * @description add list of error for a record.
    * @param Id sObjectId, List<String> listOfErrorMessages
    * 
    * @example
    * new GPApprovalLogger().addError(sObjectId, listOfErrorMessages);
    */
    public void addError(String sObjectId, List<String> listOfErrorMessages) {
        if(!mapOfSobjectIdToListOfErrors.containsKey(sObjectId)) {
            mapOfSobjectIdToListOfErrors.put(sObjectId, new List<String>());
        }
        mapOfSobjectIdToListOfErrors.get(sObjectId).addAll(listOfErrorMessages);   
        isValid = false;     
    }
    
    /**
    * @description add list of error for a record.
    * 
    * @return Serialized approval log
    *
    * @example
    * new GPApprovalLogger().toString();
    */
    public override String toString() {
        if(isFormattedLogRequired) {
            return getFormattedErrorMessage();
        } else {
            return getMapOfErrors();
        }
    }
    
    private String getMapOfErrors() {
        try {
            return JSON.serialize(mapOfSobjectIdToListOfErrors);   
        } catch(Exception ex) {
            System.debug('mapOfSectionToSubSectionNameToListOfErrors: ' + mapOfSobjectIdToListOfErrors);
            return '';
        }
    }

    private String getFormattedErrorMessage() {
        try {
            return JSON.serialize(mapOfSectionToSubSectionNameToListOfErrors);   
        } catch(Exception ex) {
            System.debug('mapOfSectionToSubSectionNameToListOfErrors: ' + mapOfSectionToSubSectionNameToListOfErrors);
            return '';
        }
    }
}