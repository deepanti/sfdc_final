global class staleDealsIdentifier implements Database.Batchable<sObject>, Database.Stateful
{
    global String Query;
    global Set<User> user_list = new Set<User>();
    global staleDealsIdentifier (String q)
    {
        //Query=q;
        String today_date;
        today_date = System.now().format('yyyy-MM-dd');
        system.debug('today_date=='+today_date);
        String pipelinestages= '\'1. Discover\',\'2. Define\',\'3. On bid\',\'4. Down Select\',\'5. Confirmed\'';
        Query='select id,ownerid,stagename from Opportunity where stagename IN ('+pipelinestages+') AND closeDate <  LAST_N_DAYS:7   and Contract_Status__c = \'\' ';
        
        if(Test.isRunningTest())
        {
            Query +=' Limit 1';
        }
        system.debug('Query=='+Query);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
    {    
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> oppIdList)           // Execute Logic    
    {    
        system.debug('inside execute method=='+oppIdList.size()+'  '+oppIdList);
        set<Id> opplist=new Set<ID>();
        for(Opportunity oid: (List<Opportunity>)oppIdList)
            opplist.add(oid.id);
        system.debug('inside execute methoduserlist=='+opplist.size()+'  '+opplist);
        user_list.addAll(staleDealOwners.addStaleDealsUser(opplist));    
		System.debug('user_list in execute=='+user_list.size());        
    }
    
    // Logic to be Executed at finish
    global void finish(Database.BatchableContext BC)
    {
        System.debug('user_list=='+user_list.size());
        update new List<User>(user_list);
    }
}