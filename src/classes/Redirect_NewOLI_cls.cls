public class Redirect_NewOLI_cls
{
  @testVisible    
  PageReference ReturnPage {get;set;}
  
   public Opportunity theOpportunity {get; set;}   
   public String oppid {get; set;}
   public String showError {get; set;}

    public Redirect_NewOLI_cls(ApexPages.StandardController controller)
    { 
        oppid = ApexPages.currentPage().getParameters().get('opid');
                
        theOpportunity = [select Id, Name, Roll_up_QSRM_App_Rej__c, Approved__c, QSRM_Status__c, account.Industry_Vertical__c, Revenue_Start_Date__c,End_Date__c, StageName,Pricebook2Id,Contract_Term_in_months__c,Margin__c,Industry_Vertical__c,CloseDate,Project_Margin__c,Priority_Types__c, Pricebook2.Name  from Opportunity where Id =:oppid];
        
        if( theOpportunity.QSRM_Status__c=='Your QSRM is submitted for Approval')
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'Since QSRM is in Approval status,you cannot edit the deal'));
            showError = 'True';
        }
    }
}