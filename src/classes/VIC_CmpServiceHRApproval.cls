public class VIC_CmpServiceHRApproval {

    @AuraEnabled
    public static VICAuraResponse getPendingForApprovalIncentives(String incentiveStatus, String leaderName,String screenName) {
        VIC_ControllerHRApproval pendingForApprovalIncentives = new VIC_ControllerHRApproval(incentiveStatus, leaderName,screenName);
        return pendingForApprovalIncentives.getPendingForApprovalRecords();
    }

    @AuraEnabled
    public static VICAuraResponse updateDataOfIncentiveRecords(String incentiveJson,String screenName, String discretionaryRecordJson) {
        VIC_ControllerHRApproval HRApprovalController = new VIC_ControllerHRApproval();
        return HRApprovalController.updateStatusOfIncentiveRecords(incentiveJson,screenName, discretionaryRecordJson);
    }

    @AuraEnabled
    public static VICAuraResponse updateToPendingDataOfIncentiveRecord(String wrapperRecord,String screenName,String discretionaryRecordJson) {
        VIC_ControllerHRApproval HRApprovalController = new VIC_ControllerHRApproval();
        return HRApprovalController.updateIncentives(wrapperRecord,screenName,discretionaryRecordJson);
    }

    @AuraEnabled
    public static List<Target_Achievement__c> getComponentDetails(String userId){
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        Integer processingYear = Integer.ValueOf(vicInfo.VIC_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        List<Target_Achievement__c> targetAchievementList = new List<Target_Achievement__c>();
        List<Target_Component__c> targetComponentList = [select id,vic_Achievement__c,vic_Achievement_Percent__c,vic_Already_Paid_Incentive__c,Bonus_Display__c,Bonus_Type__c,(select Id, Bonus_Amount__c, VIC_Description__c from Target_Achievements__r where vic_Status__c =: 'HR - Pending')
                                                         from Target_Component__c 
                                                         where (Target__r.Is_Active__c =: True) AND (Target__r.Start_date__c =: targetStartDate) AND (Target__r.User__c =: userId) AND (Master_Plan_Component__r.vic_Component_Code__c =: 'Discretionary_Payment') Limit 1];
        for(Target_Component__c targetComponentObject : targetComponentList){
            targetAchievementList.add(targetComponentObject.Target_Achievements__r);
        }
        return targetAchievementList;
    }
    
    @AuraEnabled
    public static List<Target_Achievement__c> getIncentiveDetails(String strUserId, String strIncentiveStatus){
        List<Target_Achievement__c> returnList = new List<Target_Achievement__c>();
        
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        Integer processingYear = Integer.ValueOf(vicInfo.VIC_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        Date targetEndDate = DATE.newInstance(processingYear, 12, 31);
        
        /*String strIncentiveStatus;
        If(strScreenName.containsIgnoreCase('HRApproval')){
            strIncentiveStatus = 'HR - Pending';
        }  */      
        List<Target_Component__c> targetComponentList = [Select id, vic_Achievement__c,vic_Achievement_Percent__c,vic_Already_Paid_Incentive__c,Bonus_Display__c,Bonus_Type__c,
                                                         (Select Id, Bonus_Amount__c, VIC_Description__c,Achievement_Date__c,vic_Incentive_In_Local_Currency__c,
                                                          Opportunity__c,Opportunity__r.Name,vic_Opportunity_Product_Id__c
                                                          From Target_Achievements__r 
                                                          Where vic_Status__c =:strIncentiveStatus)
                                                         From Target_Component__c 
                                                         Where Target__r.User__c =: strUserId
                                                         AND Target__r.Is_Active__c = True 
                                                         AND Target__r.Start_date__c =:targetStartDate                                                    
                                                         AND Master_Plan_Component__r.vic_Component_Code__c ='Discretionary_Payment'
                                                         Limit 1];
        if(targetComponentList != null && targetComponentList.size() > 0){
            for(Target_Component__c t : targetComponentList){
                if(t.Target_Achievements__r != null){
                    returnList.addAll(t.Target_Achievements__r);
                }
               
            }
        }
        return returnList;
    }   
    
}