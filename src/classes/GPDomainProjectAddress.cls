public class GPDomainProjectAddress extends fflib_SObjectDomain {
    public GPDomainProjectAddress(List < GP_Project_Address__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainProjectAddress(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Project_Address__c.SObjectType
    };



    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        applyDefaultsOnInsertUpdate(null);
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        setIsUpdateCheckBox();
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updateProjectCustomerName(uow, oldSObjectMap);
		updateHighestRelationProjectCustomerName(uow, oldSObjectMap);
        uow.commitWork();
    }

    public override void onAfterInsert() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updateProjectCustomerName(uow, null);
		updateHighestRelationProjectCustomerName(uow, null);
        uow.commitWork();
    }
    
    public override void onAfterDelete() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updateProjectCustomerName(uow, null);
		updateHighestRelationProjectCustomerName(uow, null);
        uow.commitWork();
    }

    public void applyDefaultsOnInsertUpdate(Map < Id, SOBject > oldSObjectMap) {
        for (GP_Project_Address__c objPWL: (List < GP_Project_Address__c > ) records) {

            if (oldSObjectMap == null && objPWL.GP_Project_Oracle_PID__c != 'NA' && objPWL.GP_Parent_Project_Address__c == null) {
                objPWL.GP_isUpdated__c = true;
            }
        }
    }

    public void setIsUpdateCheckBox() {
        List < String > lstOfFieldAPINames = new List < String > ();
        Set < Id > setOfParentRecords = new Set < Id > ();

        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Project_Address__c.fieldSets.getMap().get('GP_PID_Update_Fields').getFields()) {
            lstOfFieldAPINames.add(fields.getFieldPath());
        }

        for (GP_Project_Address__c projectAdress: (List < GP_Project_Address__c > ) records) {
            if (projectAdress.GP_Parent_Project_Address__c != null) {
                setOfParentRecords.add(projectAdress.GP_Parent_Project_Address__c);
            }
        }

        if (setOfParentRecords.size() > 0) {
            map < Id, GP_Project_Address__c > mapOfParentRecords = new map < Id, GP_Project_Address__c > ();
            for (GP_Project_Address__c projectAdress: new GPSelectorProjectAddress().selectProjectAddressRecords(setOfParentRecords)) {
                mapOfParentRecords.put(projectAdress.Id, projectAdress);
            }
            GPCommon.setIsUpdatedField(records, mapOfParentRecords, lstOfFieldAPINames, 'GP_Parent_Project_Address__c');
        }
    }

   /* public void updateProjectCustomerName(fflib_SObjectUnitOfWork uow, map < id, Sobject > oldSObjectMap) {
        map < Id, GP_Project__c > mapOfProject = new map < Id, GP_Project__c > ();
        for (GP_Project_Address__c eachProjectAddress: (list < GP_Project_Address__c > ) records) {
            if (eachProjectAddress.GP_Project__c != null &&
                eachProjectAddress.GP_Relationship__c == 'Direct' &&
                checkForCustomerNameChange(eachProjectAddress, oldSObjectMap)) {
                GP_Project__c objProject = new GP_Project__c(id = eachProjectAddress.GP_Project__c);
                objProject.GP_Customer_Master__c = eachProjectAddress.GP_Customer_Name__c;
                mapOfProject.put(objProject.Id, objProject);
            }

        }

        // try {
        if (mapOfProject.values().size() > 0) {
            update mapOfProject.values();
        }
        //} catch (Exception e) {
        //system.debug('Exception' + E);
        //}
    }*/

    public void updateHighestRelationProjectCustomerName(fflib_SObjectUnitOfWork uow, map < id, Sobject > oldSObjectMap) {
        List < GP_Project__c > lstOfProject = new List< GP_Project__c > ();
        Set < Id > setOfProjectId = new Set < Id >();
        for (GP_Project_Address__c eachProjectAddress: (list < GP_Project_Address__c > ) records) {
            if (eachProjectAddress.GP_Project__c != null &&
                checkForCustomerNameChange(eachProjectAddress, oldSObjectMap)) {
                    setOfProjectId.add(eachProjectAddress.GP_Project__c);
            }
        }
        if(setOfProjectId.size() > 0) {
            for(GP_Project__c project : [SELECT Id,
                                         (SELECT Id ,GP_Customer_Name__c,
                                          GP_Sequence_Number__c FROM Project_Addresses__r 
                                          ORDER BY GP_Sequence_Number__c DESC LIMIT 1)
                                         FROM GP_Project__c WHERE Id in :setOfProjectId ])
            {
                if(project.Project_Addresses__r.size() > 0)
                	project.GP_Customer_Master__c = project.Project_Addresses__r[0].GP_Customer_Name__c;
                else
                    project.GP_Customer_Master__c = null;
                
                lstOfProject.add(project);
            }
            
            if (lstOfProject.size() > 0) {
                update lstOfProject;
            }
        }
    }

    private static Boolean checkForCustomerNameChange(GP_Project_Address__c eachProjectAddress, Map < Id, SOBject > oldSObjectMap) {
        return eachProjectAddress.GP_Customer_Name__c != null &&
            (oldSObjectMap == null ||
                (oldSObjectMap != null &&
                    oldSObjectMap.get(eachProjectAddress.Id).get('GP_Customer_Name__c') != eachProjectAddress.GP_Customer_Name__c));
    }
}