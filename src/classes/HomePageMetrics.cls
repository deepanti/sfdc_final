/*************************************************************************************************************************
* @name : HomePageHighlightReport
* @author : Persistent
* @description  : Used by home page component, highlight panel metric (HomePageHighlightPanelLineItem)
**************************************************************************************************************************/
public class HomePageMetrics {
/*************************************************************************************************************************
* @name : getMetricValues
* @author : Persistent
* @description  : To get the Metric data from the apex class for SalesLeader, to be uncommented in the last sprint
* @param  : isSalesLeader - if the logged in user is salesleader or not
* @return : The list of MetricData
**************************************************************************************************************************/
    @AuraEnabled(cacheable=true)
    public static List<MetricData> getMetricValues()
    {
        try
        { 
            List<MetricData> metrics;
            metrics = new List<MetricData>();
            AggregateResult ar;
            Decimal total;
            Decimal sumTCV;
            Decimal totalTCV;
            Decimal IOSum;
            Decimal TSSum;
            MetricData metricDataInstance;
            MetricData subTotal;
            string userId =  userInfo.getUserId();
            userId = userId.substring(0, 15);
            
            //Create a map of split for logged in user
            Map<Id,Decimal> splitMap = new Map<Id,Decimal>();
            for(OpportunitySplit op: [select SplitPercentage,OpportunityId from opportunitySplit where SplitOwnerid=:userId and SplitType.DeveloperName='Overlay' limit :limits.getLimitQueryRows()])
            {
                splitMap.put(op.OpportunityId,op.SplitPercentage);
            }
            
            //Fetch total TCV Booked
            totalTCV=  0;
            IOSum = 0;
            TSSum =0;
            total=0;
            for(OpportunityLineItem OLI : [select TCV__c,PricebookEntry.Product2.Nature_of_Work__c,opportunityId from opportunityLineItem where Opportunity.GE_GC_Formula__c in ('GE','Global Clients') and Opportunity.Opportunity_Source__c!='Renewal' and Opportunity.CloseDate=THIS_YEAR and Opportunity.StageName='6. Signed Deal' and OpportunityId in :splitMap.keyset() and (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId ) limit :limits.getLimitQueryRows()])
            {
                sumTCV = OLI.tcv__c==null?0:OLI.tcv__c;
                total += sumTCV*splitMap.get(OLI.opportunityId)/100;
                String Nature_of_work = OLI.PricebookEntry.Product2.Nature_of_Work__c;
                if(Nature_of_work=='Managed Services' || Nature_of_work=='IT Services')
                    IOSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
                else
                    TSSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
            }
            totalTCV= IOSum + TSSum;
           
            metricDataInstance = new MetricData('Total TCV Booked',totalTCV ,true,'USD','currency');
            //Handle subtotals
            subTotal = new MetricData('TS',(TSSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            subTotal = new MetricData('IO',(IOSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            metrics.add(metricDataInstance);
            
            //Fetch TCV in pipeline
            total = 0;
            IOSum = 0;
            TSSum=0;
            for(OpportunityLineItem OLI : [select TCV__c,PricebookEntry.Product2.Nature_of_Work__c,opportunityId from opportunityLineItem where Opportunity.GE_GC_Formula__c in ('GE','Global Clients') and Opportunity.Opportunity_Source__c!='Renewal' and Opportunity.StageName not in('Prediscover','6. Signed Deal','7. Lost','8. Dropped') and Opportunity.CloseDate=THIS_YEAR and OpportunityId in :splitMap.keyset() and (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId) limit :limits.getLimitQueryRows()])
            {
                sumTCV = OLI.tcv__c==null?0:OLI.tcv__c;
                total += sumTCV*splitMap.get(OLI.opportunityId)/100;
                String Nature_of_work = OLI.PricebookEntry.Product2.Nature_of_Work__c;
                if(Nature_of_work=='Managed Services' || Nature_of_work=='IT Services')
                    IOSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
                else
                    TSSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
            }
            
            metricDataInstance = new MetricData('TCV in pipeline',total ,true,'USD','currency');
            //Handle subtotals
            subTotal = new MetricData('TS',(TSSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            subTotal = new MetricData('IO',(IOSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            metrics.add(metricDataInstance);
            
            
            //TCV Closing this quarter
            total = 0;
            IOSum = 0;
            TSSum=0;
            for(OpportunityLineItem OLI : [select TCV__c,PricebookEntry.Product2.Nature_of_Work__c,opportunityId from opportunityLineItem where Opportunity.GE_GC_Formula__c in ('GE','Global Clients') and Opportunity.Opportunity_Source__c!='Renewal' and Opportunity.StageName not in('Prediscover','6. Signed Deal','7. Lost','8. Dropped') and Opportunity.CloseDate = THIS_QUARTER and OpportunityId in :splitMap.keyset() and (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId) limit :limits.getLimitQueryRows() ])
            {
                sumTCV = OLI.tcv__c==null?0:OLI.tcv__c;
                total += sumTCV*splitMap.get(OLI.opportunityId)/100;
                String Nature_of_work = OLI.PricebookEntry.Product2.Nature_of_Work__c;
                if(Nature_of_work=='Managed Services' || Nature_of_work=='IT Services')
                    IOSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
                else
                    TSSum += sumTCV*splitMap.get(OLI.opportunityId)/100;
            }
             
            metricDataInstance = new MetricData('TCV closing this Quarter',total ,true,'USD','currency');
            //Handle subtotals
            subTotal = new MetricData('TS',(TSSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            subTotal = new MetricData('IO',(IOSum).intValue(),false,'USD','currency');
            metricDataInstance.Subtotals.add(subTotal);
            metrics.add(metricDataInstance);
            
            //TCV pending to meet quota
            //metrics.add(getQuotaInformation(totalTCV));
            
            //Inflows (option for commissions paid)
            total = 0;
            IOSum = 0;
            TSSum=0;
            for(OpportunityLineItem OLI : [select TCV__c,PricebookEntry.Product2.Nature_of_Work__c,opportunityId from opportunityLineItem where Opportunity.GE_GC_Formula__c in ('GE','Global Clients') and Opportunity.Opportunity_Source__c!='Renewal' and Opportunity.StageName!='Prediscover' and Opportunity.CloseDate = THIS_QUARTER and OpportunityId in :splitMap.keyset() and (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId) limit :limits.getLimitQueryRows() ])
            {
                sumTCV = OLI.tcv__c==null?0:OLI.tcv__c;
                total += sumTCV*splitMap.get(OLI.opportunityId)/100;
            }
          
            metrics.add(new MetricData('CY Inflows',total,false,'USD','currency'));
            
            
            //PDO's assigned
            ar = [select count(Id) cnt from opportunity where GE_GC_Formula__c in ('GE','Global Clients') and Opportunity_Source__c!='Renewal' and StageName='Prediscover' and (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId) ];
            total = ar.get('cnt')!=null?(Decimal)ar.get('cnt'):0;
           
            metrics.add(new MetricData('PDO\'s assigned',total,false,'','number'));
            
            return metrics;
        }
        catch(Exception e)
        {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
/*************************************************************************************************************************
* @name : getQuotaInformation
* @author : Persistent
* @description  : To get the quota for the logged in user and substract TCV from it
* @param  : n/A
* @return : The instance of Metric Data
**************************************************************************************************************************/
    @AuraEnabled
    public static MetricData getQuotaInformation()
    {
        //Create map of split for the logged in user
        Map<Id,Decimal> splitMap = new Map<Id,Decimal>();
        string userId =  userInfo.getUserId().substring(0, 15);
        for(OpportunitySplit op: [select SplitPercentage,OpportunityId from opportunitySplit where SplitOwnerid=:userId])
        {
            splitMap.put(op.OpportunityId,op.SplitPercentage);
        }
        //Fetch the attained TCV
        AggregateResult ar = [select sum(TCV__c) sumTCV from opportunityLineItem where Opportunity.GE_GC_Formula__c in ('GE','Global Clients') and Opportunity.Opportunity_Source__c!='Renewal' and Opportunity.StageName='6. Signed Deal' and Id in :splitMap.keyset() and Opportunity.CloseDate=THIS_YEAR];
        Decimal totalTCV = (Decimal) ar.get('sumTCV')==null?0:(Decimal) ar.get('sumTCV');
        //Call Overloaded Method
        return getQuotaInformation(totalTCV);
    }
    
/*************************************************************************************************************************
* @name : getQuotaInformation (Overloaded)
* @author : Persistent
* @description  : To get the quota for the logged in user and substract TCV from it
* @param  : total TCV attained by the user
* @return : The instance of Metric Data
**************************************************************************************************************************/
    private static MetricData getQuotaInformation(Decimal TotalTCV)
    {
        Decimal quota;
        try{
            //TODO: QuotaInfo to get from forecasting quota object
			string userId =  userInfo.getUserId().substring(0, 15);
            //Fetch the quota of the logged in user for current year           
            
            AggregateResult ar = [SELECT SUM(QuotaAmount) sumQuotaAmount FROM ForecastingQuota where QuotaOwnerId =:userId and startDate=THIS_YEAR];
            Decimal TotalQuotaAmount = (Decimal) ar.get('sumQuotaAmount')==null?0:(Decimal) ar.get('sumQuotaAmount');
            if(TotalQuotaAmount==0)
                throw new AuraHandledException('No quota has been set.');
            Decimal total = totalTCV-TotalQuotaAmount;
            
           // Decimal total = totalTCV-quota;
            String title = total>0?'TCV exceeding Quota':'TCV Pending to meet Quota';
            MetricData quotaInfo = new MetricData(title,total ,true, 'USD','currency');
            quotaInfo.Subtotals.add(new MetricData('Quota Set' , TotalQuotaAmount ,false, 'USD','currency'));
            return quotaInfo;
        }
        catch(Exception e){
            //Throw exception to be handled in lightning
            throw new AuraHandledException('No quota has been set.');
        }
    }
    
    public class MetricData
    {
        @AuraEnabled public String Title;	//Title for the metric
        @AuraEnabled public Decimal Total;	//Value
        @AuraEnabled public String Unit;	//Unit of the value
        @AuraEnabled public String Type;	//Type as currency/number
        @AuraEnabled public List<MetricData> Subtotals;	//Subtotals to be displayed at the footer level
        
        public MetricData(String title, Decimal total, Boolean needSubtotals, String unit, String type)
        {
            this.Title = title;
            this.Total = total;
            this.Unit = Unit;
            this.type = type;
            if(needSubtotals)
            {
                Subtotals = new List<MetricData>();
            }
        }
    }
}