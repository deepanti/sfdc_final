public class accWithContController {
 @AuraEnabled
 public static list < Contact > fetchAccount(ID AId) {
  // query 10 records from account with their relevant contacts and return query.
  List <Contact> lstOfAcc = [select id, account.name ,firstName,LastName, title, (Select id, Tooltip_Prompt__c,G_Designation__c,G_Connection_Owner__r.Name,Connection_Path__c, Connection_Path_Details__c,Connection_Type__c from G_Connectors__r) from contact where Id IN (SELECT contact__c from G_Connector__c WHERE contact__c != '') and AccountId=: AId];
  system.debug('lstOfAcc: '+ lstOfAcc);
     return lstOfAcc;
 }
    
    @AuraEnabled
 public static list < Lead > fetchAccountLead(ID AccId) {
  // query 10 records from account with their relevant contacts and return query.
  List <Lead> lstOfAccLead = [select id,firstName,LastName, title, (Select id, Tooltip_Prompt__c,G_Designation__c,G_Connection_Owner__r.Name,Connection_Path__c, Connection_Path_Details__c,Connection_Type__c from G_Connectors__r) from Lead where Id IN (SELECT lead__c from G_Connector__c WHERE lead__c != '') and Account__c=: AccId];
  system.debug('lstOfAccLead: '+ lstOfAccLead);
     return lstOfAccLead;   
 }   
    
    @AuraEnabled
 public static String mailToString(ID AccId) {
  // query 10 records from account with their relevant contacts and return query.
  List<Account> accList = [select name,Archetype__r.name from Account where id =: AccId];
  String accName = accList[0].name;
     String archetype = accList[0].Archetype__r.name;
  String uname = Userinfo.getFirstName() + ' ' + Userinfo.getLastName();
     String uemail = Userinfo.getUserEmail();
  //system.debug('accName: '+ accName);
    // return accName;
    String rString =  'mailto:Shivangi.kochar@genpact.com;Sanjay.singh9@genpact.com?subject=Request%20for%20Connection%20Support%20for%20'+ accName +'&body=Requester%20name%3A%20'+ uname +'%0ARequester%20email%20ID%3A%20'+ uemail +'%0AAccount%20name%3A%20'+ accName +'%0AAccount%20Archetype%3A%20'+ archetype +'%0AWhen%20do%20you%20need%20this%20by%3F%3A%0ADescribe%20your%20Connection%20Support%20request%3A%20';
 return rString;
 }
    
}