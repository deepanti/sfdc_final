/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* This Class Use for ForecastCatagory Lightning Component Which show the loging User 
* Opportunities (As He have the access of opportunities)
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vikas Chand   <vikas.chand@genpact.com>
* Version 1
* Created Date : 9/5/2018
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/

public with sharing class ForeCastingDealController {
    /* This Method Run on onload page*/
    @AuraEnabled
    public static List <Opportunity> getOpportunities() {
        List <Opportunity> oppList = new list<opportunity>();
        try{
            Integer queryLimit = System.limits.getLimitQueryRows();
            oppList = [SELECT Id, name, Opportunity_ID__c, Owner.Name, CloseDate, StageName, Forecats_Stage__c, TCV1__c, ForecastCategoryName, Forecasted_TCV__c,
                       Ground_Level_Forecast__c, Ground_Level_Probabilty_forcast_page__c,Opportunity_Close_Date_Month__c FROM Opportunity where isclosed = false  AND Opportunity_Source__c !='Renewal' AND StageName != 'Prediscover' ORDER BY TCV1__c desc Limit :queryLimit];//Limit 20];
            system.debug(':---oppList---: '+oppList);
           if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return oppList;
        }catch(Exception e){
            system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'getOpportunities','Fail','',String.valueOf(e.getLineNumber())); }  return oppList;
    }
    @AuraEnabled
    public static  List<ForecastCategoryValue__c> getForecastCategoryCustomSetting(){
        try{
            List<ForecastCategoryValue__c> forecastCategoryList = new List<ForecastCategoryValue__c>();
            forecastCategoryList = ForecastCategoryValue__c.getall().values();
             if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return forecastCategoryList;
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage()); 
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'getForecastCategorycustomsetting','Fail','',String.valueOf(e.getLineNumber()));  } return NULL;
    }
    
    @auraEnabled /* This Method use for get the picklist value of Forcast_Industry_Vertical__c*/
    public static List<String> getVerticalPicklist()
    {
        try{
            List<String> verticalPicklist = Utility.getPickListValue('Opportunity','Forcast_Industry_Vertical__c');
            if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return verticalPicklist; 
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage()); 
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController',  'getVerticalPicklist','Fail','',String.valueOf(e.getLineNumber()));  } return NULL;
    }
    @auraEnabled /* This method use for get the picklist value of Opportunity_Close_Date_Month__c*/
    public static List<String> getOpportunityCloseMonth()
    {
        try{
            List<String> OpportunityCloseMonth = Utility.getPickListValue('Opportunity','Opportunity_Close_Date_Month__c'); 
            system.debug(':--test close month--:'+OpportunitycloseMonth);
             if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return OpportunityCloseMonth; 
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage());
             CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'getOpportunitycloseMonth','Fail','',String.valueOf(e.getLineNumber()));   } return NULL;
    }
    
    @auraEnabled /* This method use for get the picklist value of ForecastCategoryName */
    public static List<String> getForcastPicklist()
    {
        try{
            List<String> forecastPicklist = Utility.getPickListValue('Opportunity','ForecastCategoryName');
              if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return forecastPicklist; 
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage()); 
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'getforcastpicklist','Fail','',String.valueOf(e.getLineNumber()));     } return NULL;
    }
    
    @auraEnabled /* This method use for get the picklist value of ForecastCategoryName */
    public static List<String> getForcastStage()
    {
        try{
            List<String> forecastStage = Utility.getPickListValue('Opportunity','Forecats_Stage__c');
             if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return forecastStage; 
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage()); 
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'getforcastStage','Fail','',String.valueOf(e.getLineNumber()));     } return NULL;
    }
    
    @AuraEnabled/*This method use for get the filter opportunity base on the filter  by user on multiple field base */
    public static List <Opportunity> getFilterOpportunity(string dealValue,string vertical,string quater,string closeDateYear,string supersl,string sl) {
        system.debug(':--dealValue---'+dealValue+':-vertical-:'+vertical+':-quater-:'+quater+':-supersl-:'+supersl+':-sl-:'+sl);
        List <Opportunity> oppList = new list<opportunity>();
        try{
            List<String> vetical = new list<string>();
            if(!String.isBlank(vertical)){
                String removevertical = vertical.removeStart('×');
                vetical = removevertical.split('×');
            }
            Integer fiscalyear;
            if(closeDateYear != Null && closeDateYear != ''){
                if(closeDateYear == 'CurrentYear'){
                    fiscalyear = system.today().year();
                }else if(closeDateYear == 'NextYear'){
                    fiscalyear = system.today().year() + 1;
                }
            }
            String query = 'SELECT Id, name, Opportunity_ID__c, Owner.Name, CloseDate, StageName, Forecats_Stage__c, TCV1__c, ForecastCategoryName, Forecasted_TCV__c, Ground_Level_Forecast__c, Ground_Level_Probabilty_forcast_page__c,Opportunity_Close_Date_Month__c FROM Opportunity where isclosed = false AND Opportunity_Source__c !=\'Renewal\' AND StageName != \'Prediscover\' AND';
            //Opportunity_Source__c !='Renewal' AND StageName != 'Prediscover'
          // query += 'Opportunity_Source__c !=\'Renewal\' AND ';
            system.debug(':--ver--:'+vetical.size());
            system.debug(':--ver--:'+vetical);
            if(vetical.size() > 0){ 
                query +=' Industry_Vertical__c IN : vetical' ;
            }  
          
            if(vetical.size() > 0 && quater !='None'){
                query = query +' AND CALENDAR_QUARTER(CloseDate) = '+ integer.valueof(quater); 
            } else if(quater !='None'){
                query = query +' CALENDAR_QUARTER(CloseDate) =' + integer.valueof(quater); 
            }
            
            if(supersl != null && !string.isBlank(supersl) && (vetical.size() > 0 || quater !='None')){ // ' WHERE Year__c =:\'' + fpyear + '\' +
                query = query +' AND Account.Client_Partner__c = '+'\''+supersl+'\'';
            } else if(supersl != null && !string.isBlank(supersl) ){    
                query = query +' Account.Client_Partner__c = '+'\''+supersl+'\'';
            }
            system.debug('--sl--'+!string.isBlank(sl));
            if(sl != null && !string.isBlank(sl) && (supersl != null || vetical.size() > 0 || quater !='None')){
                query = query +' AND Account.Sales_Leader_User_Id__c Like'+'\''+'%'+sl+'%'+'\'';
            } else if(sl != null  && !string.isBlank(sl)){   query = query +' Account.Sales_Leader_User_Id__c Like'+'\''+'%'+sl+'%'+'\'';
            }
            system.debug(':--dealValue---'+dealValue);
            if(dealValue != Null && !string.isBlank(dealValue) && ((sl != null && !string.isBlank(sl)) || supersl != null || vetical.size() > 0 || quater !='None')){
                if(dealValue == '1'){
                    query = query +' AND  TCV1__c < 1000000'; 
                } else if(dealValue == '2'){
                    query = query +' AND  TCV1__c >= 1000000';
                }
            }else if(dealValue != Null && !string.isBlank(dealValue)){
                if(dealValue == '1'){    query = query +' TCV1__c < 1000000';   } else if(dealValue == '2'){    query = query +' TCV1__c >= 1000000';
                }
            }
        
            if(closeDateYear != 'None' &&  (dealValue != 'None'  || ((sl != null && !string.isBlank(sl)) || supersl != null || vetical.size() > 0 || quater !='None'))){
                
                query = query +' AND CALENDAR_YEAR(CloseDate) = '+ fiscalyear;
            } else if(closeDateYear != 'None'){
                query = query +' CALENDAR_YEAR(CloseDate) =' + fiscalyear;  
            }
            
            query = query + ' ORDER BY TCV1__c desc Limit 50000';
            system.debug(':---oppList---: '+query);
            oppList = Database.query(query);
            system.debug(':---oppList---: '+oppList);
             
            return oppList;
        }catch(Exception e){
            system.debug(':---error Msg----:'+e.getMessage()+':----error line----:'+e.getLineNumber());
             CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController','getfilterOpportunity','Fail','',String.valueOf(e.getLineNumber()));   }   return oppList;
    }
    @AuraEnabled/*This method use for update the opporunity*/
    public static boolean updateOpportunity(String updateOppList){
        try{
            system.debug(':---update---:'+updateOppList);
            list<opportunity> oppList = new list<opportunity>();
            List<OpportunityWrapper> updateList = (List<OpportunityWrapper>)JSON.deserialize(updateOppList, List<OpportunityWrapper>.Class);
            for(OpportunityWrapper oppwrap : updateList){
                opportunity opp = new opportunity();
                opp.id = oppwrap.Id;
                opp.ForecastCategoryName = oppwrap.ForeCastingCategoryName;
                opp.Bypass_Validations__c = true;
                opp.Opportunity_Close_Date_Month__c = oppwrap.closeDateMonth;
                opp.Ground_Level_Probabilty_forcast_page__c = oppwrap.groundLevelProb; 
                opp.Forecats_Stage__c = oppwrap.forecastStage;
                oppList.add(opp);
            }
            if(oppList.size() > 0){
                UpdateForecastDealController.updateOpportunitySaveMethod(oppList);
            }
           // update opplist;  
            system.debug(':---opplist---:'+opplist);
             if(Test.isRunningTest())
			{
			//	integer vakue = 2/0;
			}
            return true;
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
            CreateErrorLog.createErrorRecord('forecastDealLightningComponent',e.getMessage(), '', e.getStackTraceString(),'ForeCastingDealController', 'updateopportunity','Fail','',String.valueOf(e.getLineNumber()));  }    return false;
    }
    /*Wrapper class use to save the updated value*/
    public class OpportunityWrapper{
        @auraEnabled
        public ID Id;
        
        @auraEnabled
        public String ForeCastingCategoryName;
        
        @auraEnabled
        public String closeDateMonth; 
        
        @auraEnabled
        public Integer groundLevelProb;
        
        @auraEnabled
        public String forecastStage;
        
    }
    
}