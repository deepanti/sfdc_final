@isTest
public class GPDomainProjectDocumentTracker {
    
     @testSetup
    public static Void buildDependencyData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_document__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        insert prjObj;

        GP_Project_Document__c objPODocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'PO Document');
        insert objPODocument;

        GP_Project_Document__c objOtherDocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'Other Document');
        insert objOtherDocument;
        
    }
    
    @isTest
    public static void testGPDomainProjectDocument() {
    }
    
    
    
}