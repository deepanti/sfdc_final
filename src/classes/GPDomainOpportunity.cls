public without sharing class GPDomainOpportunity extends fflib_SObjectDomain {
    
    private Map<String, GP_Opportunity_Project__c> mapOfOppRefenenceIdToOpp = new Map<string, GP_Opportunity_Project__c>();
    private List<GP_Deal__c> updateandInsertlstOfDeal = new List<GP_Deal__c>();
    private Set<string> setofOppAutoNumberId = new Set<string>();
    private Map<ID,ID> mapOfOLIIdToDealID = new Map<ID,ID>();
    
    private Set<ID> setofOLIID = new Set<Id>();
    private Set<String> setofSFDCOLIID = new Set<String>();
    private Set<ID> setofOppID = new Set<Id>();
    private fflib_SObjectUnitOfWork uow;
    
    public GPDomainOpportunity(List<Opportunity > sObjectList) {
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new GPDomainOpportunity(sObjectList);
        }
    }
    
    // SObject's used by the logic in this service, listed in dependency order
    private static List<Schema.SObjectType> DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        Opportunity.SObjectType,
            GP_Opportunity_Project__c.SObjectType,
            GP_Opportunity_Project__c.SObjectType,
            GP_Deal__c.SObjectType
            
            };
                
    public override void onAfterUpdate(Map<Id, SObject> oldSObjectMap) {
		
        try {
            fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
            createDealUnderOpportunityProject(uow, oldSObjectMap);
            uow.commitWork();
            
        } catch(Exception ex) {
            GPErrorLogUtility.logError('GPDomainOpportunity', 'Opportunity Update', ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());
        }
    }
    
    private void createDealUnderOpportunityProject(fflib_SObjectUnitOfWork uow, map<id, Sobject> oldSObjectMap)
    {
        if(GPcheckRecursive.isFirstTime){
            
            if(records != null && records.size() > 0)
            {
                set<id> setOfOppId = new set<Id>();
                set<string> setofOppAutoNumberId = new  set<string>();
                map<string,GP_Opportunity_Project__c> mapOfOppRefenenceIdToOpp = new map<string,GP_Opportunity_Project__c>();
                list<GP_Deal__c> lstOfDeal = new list<GP_Deal__c>();
                list<GP_Deal__c> DelExistinglstOfDeal = new list<GP_Deal__c>();
                
                set<string> setofOppProjectID = new set<string>();
                
                Opportunity objOldProj;
                for(Opportunity EachOpportunity : (List<Opportunity >)records)
                {    
                    objOldProj = (Opportunity)oldSObjectMap.get(EachOpportunity.Id);
                    
                    // When probability reaches 33 %
                    if(EachOpportunity.Probability >= integer.valueof(system.label.GP_Opportunity_Probability) && 
                       // 28-12-18: Changed to allow deal update on 85%.
                       objOldProj.Probability <= integer.valueof(system.label.GP_Opportunity_Probability)	&& 	
                                              
                       EachOpportunity.Probability != objOldProj.Probability)
                    {
                        setOfOppId.add(EachOpportunity.ID);
                        if(EachOpportunity.Opportunity_ID__c != null){
                            setofOppAutoNumberId.add(EachOpportunity.Opportunity_ID__c);
                        }
                        system.debug(setofOppAutoNumberId);
                    }
                    // IF account changes
                    if(EachOpportunity.Probability >= integer.valueof(system.label.GP_Opportunity_Probability )  && EachOpportunity.AccountID != objOldProj.AccountID ){
                        setOfOppId.add(EachOpportunity.ID);
                        setofOppAutoNumberId.add(EachOpportunity.Opportunity_ID__c);
                    }
                }
                
                if(setofOppAutoNumberId != null && setofOppAutoNumberId.size()> 0)
                {
                    for(GP_Opportunity_Project__c objOppProject: new GPSelectorOpportunityProject().selectOppProject(setofOppAutoNumberId))
                    {
                        mapOfOppRefenenceIdToOpp.put(objOppProject.GP_Opportunity_Id__c,objOppProject);
                        setofOppProjectID.add(objOppProject.GP_Opportunity_Id__c);
                    }
                }
                
                if(setofOppProjectID !=null && setofOppProjectID.size()>0)
                {
                    for(GP_Deal__c objDeal:new GPSelectorDeal().selectOppDealOnOppId(setofOppProjectID))
                    {
                        mapOfOLIIdToDealID.put(objDeal.GP_OLI_SFDC_Id__c,objDeal.id);
                    }
                }
                
                if(setOfOppId != null && setOfOppId.size()>0 && mapOfOppRefenenceIdToOpp != null && mapOfOppRefenenceIdToOpp.size()>0)
                {
                    for(OpportunityLineItem objOLI :new GPSelectorOpportunityProduct().selectOppproduct(setOfOppId))
                    {
                        GP_Deal__c objDeal;
                        id sfdcOliCId = objOLI.Custom_Id__c !=null?objOLI.Custom_Id__c : objOLI.id; 
                        system.debug('mapOfOLIIdToDealID::'+mapOfOLIIdToDealID);
                        if(mapOfOLIIdToDealID.get(sfdcOliCId) != null) {
                            objDeal = new GP_Deal__c(ID = mapOfOLIIdToDealID.get(sfdcOliCId));
                        } else {
                            objDeal = new GP_Deal__c(); 
                        }
                        /*objDeal.GP_Probability__c = mapOfOppRefenenceIdToOpp.containsKey(objOLI.OpportunityId) ? 
mapOfOppRefenenceIdToOpp.get(objOLI.OpportunityId).GP_Probability__c : objOLI.Opportunity.Probability;*/
                        // DS-22-11-18:: Updated code to fix the probability issue on deals.
                        objDeal.GP_Probability__c = mapOfOppRefenenceIdToOpp.containsKey(objOLI.Opportunity.Opportunity_ID__c) 
                            && mapOfOppRefenenceIdToOpp.get(objOLI.Opportunity.Opportunity_ID__c).GP_Probability__c != null ? 
                            mapOfOppRefenenceIdToOpp.get(objOLI.Opportunity.Opportunity_ID__c).GP_Probability__c : objOLI.Opportunity.Probability;
                        
                        objDeal.GP_Opportunity_Project__c = mapOfOppRefenenceIdToOpp.containsKey(objOLI.Opportunity.Opportunity_ID__c) ? 
                            mapOfOppRefenenceIdToOpp.get(objOLI.Opportunity.Opportunity_ID__c).id : null;
                        
                        objDeal.GP_Business_Segment_L2__c = objOLI.Opportunity.Account.Business_Segment__r.Name;
                        objDeal.GP_Business_Segment_L2_Id__c = objOLI.Opportunity.Account.Business_Segment__c;
                        objDeal.GP_Sub_Vertical__c = objOLI.Opportunity.Account.Sub_Industry_Vertical__c;
                        objDeal.GP_Sub_Business_L3__c = objOLI.Opportunity.Account.Sub_Business__r.Name;
                        objDeal.GP_Business_Group_L1__c = objOLI.Opportunity.Account.Business_Group__c;
                        objDeal.GP_Sub_Business_L3_Id__c = objOLI.Opportunity.Account.Sub_Business__c;
                        objDeal.GP_Sales_Opportunity_Id__c = objOLI.Opportunity.Opportunity_ID__c;
                        objDeal.GP_Vertical__c = objOLI.Opportunity.Account.Industry_Vertical__c;
                        objDeal.GP_Service_Line_Description__c = objOLI.Product2.Service_Line__c;
                        objDeal.GP_Sub_Delivery_Org__c = objOLI.Sub_Delivering_Organisation__c;
                        objDeal.GP_Start_Date__c = objOLI.Revenue_Start_Date__c;
                        objDeal.GP_Nature_of_Work__c = objOLI.Product2.Nature_of_Work__c;
                        objDeal.GP_Delivery_Org__c = objOLI.Delivering_Organisation__c;
                        objDeal.GP_Account_Name_L4__c = objOLI.Opportunity.AccountID;
                        objDeal.GP_Internal_Opportunity_ID__c = objOLI.OpportunityId;
                        objDeal.GP_Deal_Category__c = 'Sales SFDC';
                        objDeal.GP_Service_Line__c = objOLI.Product2.Service_Line__c;
                        objDeal.GP_Opportunity_Name__c = objOLI.Opportunity.Name;
                        objDeal.GP_OLI_SFDC_Id__c = objOLI.Custom_Id__c !=null?objOLI.Custom_Id__c : objOLI.id;
                        objDeal.GP_Product_Family__c = objOLI.Product2.Family;
                        objDeal.GP_Opportunity_ID__c = objOLI.OpportunityId;
                        objDeal.GP_Product__c = objOLI.Product2.Name;
                        objDeal.GP_Product_Id__c = objOLI.Product2Id;
                        objDeal.GP_TCV__c = objOLI.TCV__c;
                        objDeal.GP_End_Date__c = objOLI.End_Date__c;
                        objDeal.Name = objOLI.Name.length() > 80 ?objOLI.Name.left(80):objOLI.Name;
                        lstOfDeal.add(objDeal);
                    }
                }
                
                if(lstOfDeal != null && lstOfDeal.size()>0)
                {
                    if(!System.Test.isRunningTest()){
                        upsert lstOfDeal;
                        GPcheckRecursive.isFirstTime = false;
                    }                    
                }
            }
        }
    }
}