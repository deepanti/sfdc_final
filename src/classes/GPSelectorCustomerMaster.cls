public class GPSelectorCustomerMaster extends fflib_SObjectSelector {
    public GPSelectorCustomerMaster(){}
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Customer_Master__c.Name
                };
                    }
    public Schema.SObjectType getSObjectType() {
        return GP_Customer_Master__c.sObjectType;
    }
    
    public Static List<GP_Customer_Master__c> selectCustomerRecords(Set<String> setofCustomerAccountID) {
        return [select id,Name,GP_Oracle_Customer_Id__c from GP_Customer_Master__c 
                where GP_Oracle_Customer_Id__c in :setofCustomerAccountID];
    }

    public Static List<GP_Customer_Master__c> selectActiveCustomerRecords() {
        return [SELECT ID, Name
                FROM GP_Customer_Master__c
                WHERE GP_Status__c = 'Active'
                Order By Name desc
            ];
    }
}