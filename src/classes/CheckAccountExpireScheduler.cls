global class CheckAccountExpireScheduler implements Schedulable
{
   global void execute(SchedulableContext sc)
   {
       String StrQuery = 'SELECT Id, Name,Type , (select id,name,CloseDate,StageName,TCV1__c from opportunities) FROM Account';
       CheckAccountExpire reassign = new CheckAccountExpire(StrQuery);
       ID batchprocessid = Database.executeBatch(reassign);
   }
}