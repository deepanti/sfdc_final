Public class PreviewSharepointDoc{

    


    Public items_Sharepoint_Files__x item{get; set;}
    Public String Final_URL {get; set;}
    Public Boolean pdf_file {get; set;}
    Public Boolean video_file {get; set;}
    Public Boolean other_file {get; set;}
    public string Name{get;set;}
    public string URL{get;set;}
    Public String path_URL {get; set;}
    Public String s1 {get; set;}
    Public List<items_Sharepoint_Files__x> Docs{get; set;}  

     Public PreviewSharepointDoc(ApexPages.StandardController controller)
        {  
        
            if(!Test.isRunningTest()) {
        
                item =[select id,DisplayUrl,Name__c,Path__c from items_Sharepoint_Files__x where id= :Controller.getrecord().id];
            
            }
            //retrieve_items();
            pdf_file =false;
            video_file = false;
            other_file =false;
            
        }

public void Previewdoc()
{
            try{

             if(!Test.isRunningTest()) { 
             
                                Name = item.Name__c;
            }
            
              s1 = Name.substringAfterLast('.'); 
              
              if(!Test.isRunningTest()) { 
               
                                URL=item.DisplayUrl;
             }
             
             if(s1.contains('pdf') || s1.contains('PDF') || s1.contains('mp4') || s1.contains('MP4'))
             { 
                     String s3 = Name.substringAfterLast('.'); 
                     string s4= Name.substring(0,Name.length()-s3.length()-1);
                     
                     system.debug('Name string'+ s4);
                     Docs=new list<items_Sharepoint_Files__x>();
                     string query='select Path__c from items_Sharepoint_Files__x where Name__C like \'' +'%' + s4 +'%\'' + ' Limit 1';
                     if(!Test.isRunningTest())  
                     {
                                 Docs=database.query(query);
                                 For(items_Sharepoint_Files__x s:Docs)
                                 {
                                     path_URL=s.Path__c ;
                                 }
                                 
                     }
                     path_URL=path_URL.replace(' ','%20');
                     System.debug('Path'+ path_URL);
             
             }
             
             String s2 = URL.substring(0,URL.length()-4);
             s2= s2 +'embedview';
             
             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
             {  if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))  
                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))  
                                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                                             if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                                                     if(s1.contains('doc') || s1.contains('ppt') || s1.contains('xls'))
                                                                                                                                                                                                                         
                                                                                                                             Final_URL= s2 ;
             }
             else if(s1.contains('pdf') || s1.contains('PDF') )
             {
                     Final_URL=  path_URL;
                     pdf_file =true;
             }
             
             else if(s1.contains('mp4') || s1.contains('MP4'))
             {
                     Final_URL=path_URL;
                     video_file=true;
             
             }
             else
             {
                     other_file =true;
             
             }
             
             System.debug('URL FInal' + Final_URL);


}

Catch(Exception e)
    {
    
        
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Preview is not available for this file. You can download it by clicking the “Download URL” or can view it by clicking the “Display URL" given below.'));
         
    }    
}

 

        
}