public class OpportunityDiscoverAddProductController 
{   
    @auraEnabled
    public static ProductListWrapper getProducts(ID opportunityID)
    {
        try
        {
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            if(opportunity_list.size() > 0)
            {  
                Map<ID, Product2> product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical AND Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);              
                
                List<Product2> product_list = getProductsHavingPriceBookEntry(product_map);
                product_list.sort();
                ProductListWrapper wrapper = new ProductListWrapper();                
                
                wrapper.total = product_list.size();
               
                wrapper.productList = product_list; 
                return wrapper;
            }
        }
        catch(Exception e){
            //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(opportunityID),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddProductController', 'getProducts','Fail','',String.valueOf(e.getLineNumber())); 
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
        }
        return NULL;
    }  
    
    @auraEnabled
    public static ProductListWrapper getProductsOnChange(ID opportunityID, String SelectedServiceLine, String SelectedNatureOfWork, String SelectedProductName )
    {
        
        ProductListWrapper wrapper = new ProductListWrapper();                
        try
        {
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            
            
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            
            Map<ID, Product2> product_map = new Map<ID, Product2>();
            if(opportunity_list.size() > 0)
            {  
                
                if(SelectedServiceLine != '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName == '--None--')
                {
                   
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName == '--None--')
                {
                  
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName != '--None--')
                {
                   
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Name=:SelectedProductName  and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName == '--None--')
                {
           
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine and Nature_of_Work__c=:SelectedNatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork == '--None--' && SelectedProductName != '--None--')
                {
             
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Service_Line__c=:SelectedServiceLine and name=:SelectedProductName and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine == '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName != '--None--')
                {
                    
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork and name=:SelectedProductName and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                else if(SelectedServiceLine != '--None--' && SelectedNatureOfWork != '--None--' && SelectedProductName != '--None--'){
                   
                    product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c = :industryVertical and Nature_of_Work__c=:SelectedNatureOfWork and name=:SelectedProductName and Service_Line__c=:SelectedServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true ORDER BY Name ASC]);
                }
                
           
                
                List<Product2> products_With_PBEntry = getProductsHavingPriceBookEntry(product_map);                                
               
                
                wrapper.total = products_With_PBEntry.size();
                wrapper.productList = products_With_PBEntry; 
                return wrapper;
            }
        }
        catch(Exception e){
            //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(opportunityID),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddProductController', 'getProductsOnChange','Fail','',String.valueOf(e.getLineNumber())); 
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage()+e.getlinenumber());
        }
        return NULL;
    } 
    
    private static List<Product2> getProductsHavingPriceBookEntry(Map<ID, Product2>product_map)
    {
        try
        {
            List<Product2> product_list = new List<Product2>();
            List<PricebookEntry> priceBookEntryList = [SELECT Id, Product2Id FROM PricebookEntry 
                                                       Where Product2ID =:product_map.keySet()];
            for(PricebookEntry priceEntry : priceBookEntryList)
            {
                if(product_map.containsKey(priceEntry.Product2ID))
                {
                    product_list.add(product_map.get(priceEntry.Product2ID));    
                }      
            }
           
            return product_list;       
        }
        catch(Exception e)
        {
              //creating error log
            CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddProductController', 'getProductsHavingPriceBookEntry','Fail','',String.valueOf(e.getLineNumber())); 
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
    @auraEnabled 
    public static ProductListWrapper getSearchedProduct(ID opportunityID, String ServiceLine, String NatureOfWork, String ProductName,boolean isSearching)
    {     
        String message='';
        ProductListWrapper wrapper = new ProductListWrapper();
        try
        {
            
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c,QSRM_Status__c FROM Opportunity WHERE ID =:opportunityID];
            
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            List<Product2> products_With_PBEntry = new List<Product2>();
            List<Product2> proList;
            
          
            
            
            if(opportunity_list.size() > 0)
            {
                if(ServiceLine != '--None--' && NatureOfWork != '--None--' && ProductName != '--None--')
                {
                    
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                }
                if(isSearching == true){
                    
                    if(ServiceLine != '--None--' && NatureOfWork == '--None--' && ProductName == '--None--')
                    {
                       
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                    }
                    else if(ServiceLine == '--None--' && NatureOfWork != '--None--' && ProductName == '--None--')
                    {
                       
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                    }
                    else if(ServiceLine == '--None--' && NatureOfWork == '--None--' && ProductName != '--None--')
                    {
                        
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                    }
                    else if(ServiceLine != '--None--' && NatureOfWork != '--None--' && ProductName == '--None--')
                    {
                       
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and isActive = true ORDER BY Name ASC];
                    }
                    else if(ServiceLine != '--None--' && NatureOfWork == '--None--' && ProductName != '--None--')
                    {
                        
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Service_Line__c =:ServiceLine and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                    }
                    if(ServiceLine == '--None--' && NatureOfWork != '--None--' && ProductName != '--None--')
                    {
                        
                        proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 WHERE Industry_Vertical__c =:industryVertical and Nature_of_Work__c =:NatureOfWork and Nature_of_Work__c != :label.Product_Nature_of_Work and Name =:ProductName and isActive = true ORDER BY Name ASC];
                    }
                } 
                
                list<opportunitylineitem>    insertOlis = new   list<opportunitylineitem>(); 
                products_With_PBEntry = getProductsHavingPriceBookEntry(new Map<ID, Product2>(proList)); 
                
                products_With_PBEntry.sort();
               
                
                wrapper.total = products_With_PBEntry.size();
                wrapper.productList = products_With_PBEntry;
                
                List<ProductListWrapper.OLIWrapper> oliWrpList = new List<ProductListWrapper.OLIWrapper>();
                
               
                for(product2 prod:products_With_PBEntry){
                    
                    opportunitylineitem oli = new opportunitylineitem();
                    oli.product2Id= prod.id;
                    oli.product2=prod;
                    oli.product2.Nature_of_Work__c=prod.Nature_of_Work__c;
                    oli.product2.Service_Line__c=prod.Service_Line__c;
                    oli.product2.Name=prod.Name;
                   
                    insertOlis.add(oli);
                    
                    ProductListWrapper.OLIWrapper oliwrp = new ProductListWrapper.OLIWrapper();
                    oliwrp.oli = oli;
                    oliwrp.isEdited = false;
                    
                    oliWrpList.add(oliwrp);
                }
                
                
                wrapper.oliWrapperList = oliWrpList;
                wrapper.oliList = insertOlis;
                return wrapper;
            }    
        }
        catch(Exception e){
              //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(opportunityID),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddProductController', 'getSearchedProduct','Fail','',String.valueOf(e.getLineNumber())); 
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
            throw new AuraHandledException(e.getMessage());			
        }
        return wrapper;
        
    }
    
    @auraEnabled
    public static DateWrapper getOpportunityClosedDate (String opportunityID)
    {
        try
        {
            DateWrapper wrapper = new DateWrapper();
          
            List<Opportunity> opportunity_list = [SELECT CloseDate ,ownerId,QSRM_Status__c FROM Opportunity WHERE ID =:opportunityID];
            if(opportunity_list.size() > 0){
                wrapper.closedDate = opportunity_list[0].CloseDate;
                wrapper.ownerId = opportunity_list[0].ownerId;
                wrapper.QSRMStatus=opportunity_list[0].QSRM_Status__c;
            }
            User loggedInUser = [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            
            wrapper.loggedInUser = loggedInUser;
            return wrapper;   
        } 
        catch(Exception e){
                //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(opportunityID),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddProductController', 'getOpportunityClosedDate','Fail','',String.valueOf(e.getLineNumber())); 
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }   
    

    @AuraEnabled
    public static List<OpportunityLineItem> getAddedProducts_LineItems(String oppId){
        List<OpportunityLineItem> opportunity_list = [SELECT ID,Product2Id, Product2.Name, Product2.Nature_of_Work__c, Product2.Service_Line__c, Industry_Vertical__c From 	OpportunityLineItem  WHERE OpportunityId =:oppId];
        
        return opportunity_list;
    }
    
    
    @AuraEnabled
  public static void saveOppLineItems(String final_OLI_List,String oppId){
         system.debug('>>>>>In saveOppLineItems '+final_OLI_List);
        String message='';
        list<OpportunityLineItem> oliList= new list<OpportunityLineItem>();
        list<OpportunityLineItem> deloliList= new list<OpportunityLineItem>();
        list<OpportunityLineItem> editDeloliList= new list<OpportunityLineItem>();
        list<string> prodIds= new list<string>();
        list<string> editDeloliListids= new list<string>();
        map<string,string> oliIds= new map<string,string>();
         map<id,OpportunityLineItem> cloneOliList= new map<id,OpportunityLineItem>();
        List<OpportunityLineItem> newProductList = (List<OpportunityLineItem>)System.JSON.deserialize(final_OLI_List, List<OpportunityLineItem>.class);
       
        Savepoint sp = Database.setSavepoint();
        try
        {
            //looping through new prodlist  
            for(OpportunityLineItem ol:newProductList){
                prodIds.add(ol.product2Id);
                oliIds.put(ol.id,ol.product2Id);
            }
             system.debug('prodIds***'+prodIds);
            system.debug('oliIds***'+oliIds);
            //getting oli from db
            map<id,OpportunityLineItem> oldProductList =new map<id,OpportunityLineItem>([select id,product2Id, opportunityId,Product_BD_Rep__c,QSRM_Status__c,
                                                                                         opportunity.StageName,Contract_Term__c,Opportunity.CloseDate,Revenue_Start_Date__c,
                                                                                         Local_Currency__c,TCV__c,Delivering_Organisation__c,Sub_Delivering_Organisation__c,
                                                                                         Delivery_Location__c,FTE__c,UnitPrice,Opportunity.ownerId 
                                                                                         from OpportunityLineItem where opportunityId = :oppId ]);
            system.debug('>>>>> getting oli from db : '+oldProductList);
           
            //if oli not contains oliId adding on delete list and if new product2Id is diffrent from new product2Id adding in editlist and cloning the Olis
            for(OpportunityLineItem oli:oldProductList.values()){
                if(!oliIds.containsKey(oli.id) ){
                    deloliList.add(oli);
                }else if(oliIds.get(oli.id)!=oli.product2Id){
                    OpportunityLineItem cloneOli = oli.clone();
                    cloneOliList.put(oli.id,cloneOli);
                    editDeloliList.add(oli);
                    editDeloliListids.add(oli.id);
                }
            }
            
            system.debug('editDeloliListids***'+editDeloliListids);
            system.debug('Id of OLI which needs to be deleted : deloliList: '+deloliList);
            system.debug('Id of OLI which needs to be deleted : editDeloliList: '+editDeloliList);
            
          //looping through newProductList and preparing olilist to upsert 
            for(OpportunityLineItem oli:newProductList){
                OpportunityLineItem ol= new OpportunityLineItem();
              
                if((oli.Id==null) || editDeloliListids.contains(oli.id)){
                    if(cloneOliList.containskey(oli.id)){
                     system.debug('clone*** : '+cloneOliList.get(oli.id));
                    }
                    ol=cloneOliList.containskey(oli.id)?cloneOliList.get(oli.id):new OpportunityLineItem();
                    ol.id=null;
                    ol.product2Id=oli.product2Id;
                    ol.opportunityID=oppId;
                    if(ol.Type_of_Deal__c != 'Renewal'){
                    ol.Type_of_Deal__c = 'Booking';
                    }
                     oliList.add(ol);
                }
               
            }
              system.debug('OLI for delete : '+deloliList);
            system.debug('OLI for creation : '+oliList);
            system.debug('newProductList2'+oliList.size());
            
          
            
            ID loggedInUserID = UserInfo.getUserId();        
            
            for(OpportunityLineItem del:deloliList){
                if(loggedInUserID != del.Opportunity.ownerId && loggedInUserID != del.Product_BD_Rep__c && !deloliList.isEmpty()){              
                    message='You are not authorized to delete the product.';
                     throw new AuraHandledException(message);				
                }
            }
            
            
            delete deloliList;
            
            upsert oliList;
            delete editDeloliList;
            
            system.debug('cloneOliList::::'+cloneOliList);
         
            
            message='Data Saved Successfully';
           
        }
        catch(Exception ex) 
        {
             System.debug('Error=='+ex.getStackTraceString()+'::'+ex.getMessage());
             //creating error log
            //CreateErrorLog.createErrorRecord(oppId,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityDiscoverAddProductController', 'saveOppLineItems','Fail','',String.valueOf(ex.getLineNumber())); 
            throw new AuraHandledException(message);
            Database.rollback(sp);
        }
        
    }
    
    @AuraEnabled
    public static string saveChanges(Id opportunityID,String SelectedServiceLine, String SelectedNatureOfWork, String SelectedProductName){
       
        List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
        String industryVertical = opportunity_list[0].Industry_Vertical__c;
        Map<ID, Product2> product_map = new Map<ID, Product2>();
        product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c,Industry_Vertical__c FROM Product2 WHERE  Service_Line__c =:SelectedServiceLine and Nature_of_Work__c =:SelectedNatureOfWork and Name =:SelectedProductName and Industry_Vertical__c=:industryVertical]);
        List<Product2> product_list = getProductsHavingPriceBookEntry(product_map);
        if(!product_list.isEmpty()){
            return product_list[0].id;
        }
       
        return null;
    }
    
    
    public class DateWrapper
    {
        @auraenabled
        public Id ownerId;
        @auraenabled
        public Date closedDate;
        @auraEnabled
        public User loggedInUser;
        @auraEnabled
        public string QSRMStatus;
    }
}