@isTest
public class GPBatchCreateDealsTracker {
    Public Static List<GP_Project_Address__c> listOfAddress = new List<GP_Project_Address__c>();
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    private Static User objuser;
    
    @testsetup
    static void setupCommonData() {
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        accobj.Sub_Industry_Vertical__c = 'Services';
        //accobj.Business_Group__c = 'GE';
        accobj.Industry_Vertical__c	= 'Services';
        insert accobj;
        
        objuser = GPCommonTracker.getUser();
        
        Profile p = [select id, name from profile where name = 'Genpact Super Admin'
                     limit 1
                    ];
        objuser.ProfileID = p.id;
        insert objuser;
        
        system.runAS(objuser) {
            
            List<Product2> listOfProds = new List<Product2>();
            List<PricebookEntry> listOfProdEntrys = new List<PricebookEntry>();
            List<OpportunityLineItem> listOfOpps = new List<OpportunityLineItem>();
            
            Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
            oppobj.Name = 'test opportunity';
            oppobj.StageName = 'Prediscover';
            oppobj.AccountId = accobj.id;
            oppobj.Probability = 1;
            //oppobj.Opportunity_ID__c = '12345';
            oppobj.Actual_Close_Date__c = System.Today().adddays(-10);
            System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
            oppobj.Sales_Leader__c=objuser.id;
            insert oppobj;
            oppobj.Bypass_Validations__c = true;
            
            //oppobj.StageName = '2. Define';
            //oppobj.W_L_D__c='Lost';
            //oppobj.Actual_Close_Date__c = System.Today().adddays(-10);
            update oppobj;
            
            Id pricebookId = Test.getStandardPricebookId();
            
            Product2 prod = new Product2(
                Name = 'Product X',
                ProductCode = 'Pro-X',
                isActive = true,
                Product_Family__c = 'None',
                Service_Line__c = 'Claims',
                Nature_of_Work__c = 'Product & Services - AI, Automation & Experience'
            );
            //insert prod;
            listOfProds.add(prod);
            
            Product2 prod1 = new Product2(
                Name = 'Product X',
                ProductCode = 'Pro-X',
                isActive = true,
                Product_Family__c = 'None'
            );
            listOfProds.add(prod1);
            
            insert listOfProds;
            
            PricebookEntry pbEntry = new PricebookEntry(
                Pricebook2Id = pricebookId,
                Product2Id = listOfProds[0].Id,
                UnitPrice = 100.00,
                IsActive = true
            );
            listOfProdEntrys.add(pbEntry);
            //insert pbEntry;
            
            PricebookEntry pbEntry1 = new PricebookEntry(
                Pricebook2Id = pricebookId,
                Product2Id = listOfProds[1].Id,
                UnitPrice = 100.00,
                IsActive = true
            );
            listOfProdEntrys.add(pbEntry1);
            
            insert listOfProdEntrys;
            
            OpportunityLineItem oli = new OpportunityLineItem(
                OpportunityId = oppobj.Id,
                Quantity = 5,
				Type_of_Deal__c = 'Renewal',
                PricebookEntryId = listOfProdEntrys[0].Id,
                TotalPrice = 8 * listOfProdEntrys[0].UnitPrice,
                Sub_Delivering_Organisation__c = 'Content',
                Revenue_Start_Date__c = Date.Today(),
                Delivering_Organisation__c = 'Rage',
                TCV__c = 100
            );
            listOfOpps.add(oli);
            //insert oli;
            
            OpportunityLineItem oli1 = new OpportunityLineItem(
                OpportunityId = oppobj.Id,
				Type_of_Deal__c = 'Renewal',
                Quantity = 5,
                PricebookEntryId = listOfProdEntrys[1].Id,
                TotalPrice = 8 * listOfProdEntrys[1].UnitPrice
            );
            listOfOpps.add(oli1);
            //insert oli1;
            
            insert listOfOpps;
            
            Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
            //OpportunityLineItem oliDetail = [Select id from OpportunityLineItem where id =: listOfOpps[0].id];
            
            GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
            oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
            oppproobj.GP_Probability__c = 70;
            oppproobj.GP_Project_Start_Date__c = system.today();
            insert oppproobj;
            system.debug('oppproobj::'+oppproobj);
            
            Product2 prodobj = GPCommonTracker.getProduct();
            insert prodobj;
            
            OpportunityProduct__c oppprodobj = GPCommonTracker.getOpportunityProduct(prodobj.id,oppobj.id);
            insert oppprodobj;            
            
            GP_Deal__c dealobj1 = GPCommonTracker.getDeal();
            //dealobj1.GP_Opportunity_Project__c = oppproobj.id;
            dealobj1.GP_OLI__c = oppprodobj.ID;
            dealobj1.GP_OLI_SFDC_Id__c = listOfOpps[0].id;
            dealobj1.GP_Sales_Opportunity_Id__c = objOpp.Opportunity_ID__c;
            
            insert Dealobj1;
        }
    }
    
    @isTest
    public static void testCreateDeal() {
        //setupCommonData();
        date dateofOppClosure = System.Today().adddays(-10);
        system.debug('QQQ'+ [Select Id, Name, stagename, Probability, Opportunity_ID__c from Opportunity where(stagename = '7. Lost'
                                                                                                               or stagename = '8. Dropped') and Actual_Close_Date__c =: dateofOppClosure]);
        system.debug('OPporunity Project::'+[Select id ,GP_Opportunity_Id__c, GP_Project_Start_Date__c,(Select id from Deals__r) from GP_Opportunity_Project__c]
                    );
        Test.startTest();
        GPBatchCreateDeals batcher = new GPBatchCreateDeals();
        Id batchprocessid = Database.executeBatch(batcher);
        Test.stopTest();
    }
    @isTest
    public static void testCreateDealforfailure() {
        
        Test.startTest();
        GPBatchCreateDeals sh1 = new GPBatchCreateDeals();      
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        Test.stopTest();
    }
    
}