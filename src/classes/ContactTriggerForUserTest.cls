@isTest(seeAllData=true)
public class ContactTriggerForUserTest {
  
    public static testmethod void userInsertionTest(){
     Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        User u = [Select ID, UserroleID from User where ProfileId =:p.id AND IsActive=true AND UserRoleId != null Limit 1];
       // User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
       // u.UserRoleId = portalRole.Id;
      //  update u;
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        System.runAs(u){
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
          
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'data';
        con.Email = 'test@data.com';
        con.AccountID = oAccount.ID;
        con.Create_Partner_User__c = true;
        insert con;
        }
    }
}