global class GPBatchCloneSubmitProject implements Database.Batchable<sObject>, Database.Stateful {
    private static final String CLASS_NAME_LABEL = 'GPBatchCloneSubmitProject';
    private static final String OBJECT_NAME_LABEL = 'GP_Project__c';
    private static final String FUNCTION_NAME_LABEL = 'execute';
	// BPH Change
    private String CLONE_SOURCE_LABEL = 'System';
     
    Map<Id, GP_Deal__c> mapOfProjectIdToDeal;
    private Boolean submitForApproval;
	
    @TestVisible
    private Boolean cloneProject = true;
    
    set<Id> setProjectId;
    
    public GPBatchCloneSubmitProject(set<id> setProjectId, Map<Id, GP_Deal__c> mapOfProjectIdToDeal){
        this.mapOfProjectIdToDeal = mapOfProjectIdToDeal;
        this.setProjectId = setProjectId;
        this.submitForApproval = false;
		// BPH Change : keep clone source as 'Batch - Hierarchy Update'.
        this.CLONE_SOURCE_LABEL = 'Batch - Hierarchy Update';
    }
    
    public GPBatchCloneSubmitProject(set<id> setProjectId, Boolean submitForApproval){
        
        this.setProjectId = setProjectId;
        this.submitForApproval = submitForApproval;
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select id from GP_Project__c where id in: setProjectId] );
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> listProjectToClone) {
        
        Id projectId;
        savepoint sp = database.setSavepoint();
        try {
            if(listProjectToClone.size() > 0) {
                projectId = listProjectToClone[0].id;
                GPCheckRecursive.mapProjectSkipBudgetCheck.put(projectId, true);
                GPCheckRecursive.OmsBatch = true;
                GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new set<Id>{projectId}, CLONE_SOURCE_LABEL, false,false);
                //GPControllerProjectClone projectCloneController = new GPControllerProjectClone(setProjectId, 'System', false,false);
                
                String strCloneProjectId = null;
                if(cloneProject)
                    strCloneProjectId = projectCloneController.cloneProject();
                
                if(strCloneProjectId != null && strCloneProjectId.containsIgnoreCase('ERROR')) {
                    GP_Error_log__c objError = createErrorLog(FUNCTION_NAME_LABEL,strCloneProjectId, OBJECT_NAME_LABEL, CLASS_NAME_LABEL);
                    insert objError;
                    return;
                }
                
                GP_Project__c clonedProject = new GP_Project__c();
                // For Business and Product hierachy 
                if(!submitForApproval) {
                    
                    GP_Deal__c deal = mapOfProjectIdToDeal.get(projectId);
                    
                    // APC Change
                    if(deal.GP_Is_Created_per_Account__c && String.isNotEmpty(deal.GP_Sales_Leader_OHR__c)) {
                        alterGPMLeadershipForAccountPIDs(strCloneProjectId, deal);
                    }
                    
                    clonedProject.Id = strCloneProjectId;
                    
                    clonedProject.GP_Business_Group_L1__c = deal.GP_Business_Group_L1__c;
                    clonedProject.GP_Business_Segment_L2__c = deal.GP_Business_Segment_L2__c;
                    clonedProject.GP_Sub_Business_L3__c = deal.GP_Sub_Business_L3__c;
                    clonedProject.GP_Customer_Hierarchy_L4__c = deal.GP_Account_Name_L4__c;
                    clonedProject.GP_Service_Line__c = deal.GP_Service_Line__c;
                    clonedProject.GP_Nature_of_Work__c = deal.GP_Nature_of_Work__c;
                    clonedProject.GP_Product_Id__c = deal.GP_Product_Id__c;
                    clonedProject.GP_Product__c = deal.GP_Product__c;                    
                    clonedProject.GP_Business_Segment_L2_Id__c = deal.GP_Business_Segment_L2_Id__c;
                    clonedProject.GP_Sub_Business_L3_Id__c = deal.GP_Sub_Business_L3_Id__c;
                    clonedProject.GP_Vertical__c = deal.GP_Vertical__c;
                    clonedProject.GP_Sub_Vertical__c = deal.GP_Sub_Vertical__c;
                    clonedProject.GP_Approval_status__c = 'Approved';
                    
                    update clonedProject;
                    
                    return;
                }
                
                list<GP_Project__c> objProject = [SELECT id,
                                                  GP_Parent_Project__r.GP_Project_Stages_for_Approver__c,
                                                  GP_Deal__r.GP_OMS_TCV__c
                                                  FROM GP_Project__c 
                                                  WHERE id =: strCloneProjectId];           
                //System.debug('==objProject update 1=='+ objProject[0].GP_TCV__c);
                if(objProject.size() > 0) {
                    objProject[0].GP_TCV__c = objProject[0].GP_Deal__r.GP_OMS_TCV__c;
                    //objProject[0].GP_Project_Stages_for_Approver__c = objProject[0].GP_Parent_Project__r.GP_Project_Stages_for_Approver__c;
                    update objProject[0]; 
                }
                
                GPServiceOMSPricing objServiceOMS = new GPServiceOMSPricing( new set<id>{strCloneProjectId});
                objServiceOMS.createProjectRelatedDataForOMSDeal();
                GPAuraResponse objResponse;
                GPCheckRecursive.mapProjectSkipBudgetCheck.put(projectId, false);
                GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(strCloneProjectId);
                
                submissionController.skipApprovalValidation = true;
                
                objResponse = submissionController.getProjectStatus();
                
                System.debug('objResponse >>> ' + objResponse);
                
                if(objResponse.getisSuccess) {
                    objResponse = submissionController.submitforApproval('','','');
                    
                    if(!objResponse.getisSuccess) {
                        database.rollback(sp);
                        GP_Error_log__c objError = createErrorLog(FUNCTION_NAME_LABEL, objResponse.getMessage, OBJECT_NAME_LABEL, CLASS_NAME_LABEL);
                        insert objError;
                    }
                } else {
                    database.rollback(sp);
                    GP_Error_log__c objError = createErrorLog(FUNCTION_NAME_LABEL, objResponse.getMessage, OBJECT_NAME_LABEL, CLASS_NAME_LABEL);
                    insert objError;
                }
            }           
        } catch(exception ex) {
            database.rollback(sp);
            GP_Error_log__c objError = createErrorLog(FUNCTION_NAME_LABEL, ex.getMessage(), OBJECT_NAME_LABEL, CLASS_NAME_LABEL);
            insert objError;
        }       
    }
    
    global void finish(Database.BatchableContext bc) {}
    
    private GP_Error_log__c createErrorLog(string strFuntionName, string strMsg , string objectname , string strSource){
        return  new GP_Error_log__c(GP_Function_Name__c = strFuntionName, 
                                    GP_Error_Description__c = strMsg,
                                    GP_Object_Name__c = objectname,
                                    GP_Source__c = strSource);
    }
    
    private void alterGPMLeadershipForAccountPIDs(String strCloneProjectId, GP_Deal__c deal) {
        // Check for change in Sales Leader OHR                        
        // query GPM Leader via strCloneProjectId
        List<GP_Project_Leadership__c> listOfProjectLeaderships = new List<GP_Project_Leadership__c>();
        
        List<GP_Project_Leadership__c> plList = [SELECT id, GP_Employee_ID__r.GP_Final_OHR__c, GP_Active__c, 
                                                 GP_Employee_Field_Type__c, GP_Employee_ID__c, GP_End_Date__c, 
                                                 GP_IsUpdated__c, GP_Leadership_Master__c,
                                                 GP_Leadership_Role__c, GP_Leadership_Role_Name__c, GP_Mandatory_for_stage__c,
                                                 GP_Pinnacle_End_Date__c, GP_Project__c, GP_Start_Date__c
                                                 FROM GP_Project_Leadership__c 
                                                 WHERE GP_Project__c =: strCloneProjectId 
                                                 AND GP_Leadership_Role__c = 'PROJECT MANAGER'];
        // if same then ok else close the previous one and create the new one.
        if(plList.size() > 0 && plList[0].GP_Employee_ID__r.GP_Final_OHR__c != deal.GP_Sales_Leader_OHR__c) {
            List<GP_Employee_Master__c> empList = [SELECT id FROM GP_Employee_Master__c 
                                                   WHERE GP_Final_OHR__c =: deal.GP_Sales_Leader_OHR__c 
                                                   AND GP_isActive__c = true];
            if(empList.size() > 0) {
                // Create new leadership record
                GP_Project_Leadership__c plNew = plList[0].clone(false, true, false, false);
                plNew.GP_Employee_ID__c = empList[0].Id;
                plNew.GP_Start_Date__c = Date.Today().addDays(1);
                listOfProjectLeaderships.add(plNew);
                // Update previuos one
                plList[0].GP_End_Date__c = Date.Today();
                plList[0].GP_Pinnacle_End_Date__c = Date.Today();
                plList[0].GP_Active__c = false;
                listOfProjectLeaderships.add(plList[0]);
            }
        }
        
        if(listOfProjectLeaderships.size() > 0) {
            upsert listOfProjectLeaderships;
        }
    }
}