public class GPSelectorUser extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            User.Name
           
            };
    }

    public Schema.SObjectType getSObjectType() {
        return User.sObjectType;
    }

    public List<User> selectById(Set<ID> idSet) {
        return (List<User>) selectSObjectsById(idSet);
    }
    
    
    public list<User> getUserDetail(){
        list<User> lstOfUser = [Select Id,Profile.Name
                                            from User
                                            where Profile.Name ='System Administrator'];
        
        return lstOfUser;
    }
}