public class OpportunityRecordTypeController {
    //public static String theme{get;set;}    
   	
    public OpportunityRecordTypeController(ApexPages.StandardController controller){    	
    }
    
     @auraEnabled
    public static ID getRecordTypeID(String recordType){
        try{
            
            system.debug('recordType=='+recordType);
            ID recordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordType).getRecordTypeId();
        	System.debug('recordTypeID=='+recordTypeID);
            return recordTypeID;
        }
        catch(Exception e){
            System.debug('ERROR=='+e.getStackTraceString()+' '+e.getMessage());
        }
        return null;
    }
}