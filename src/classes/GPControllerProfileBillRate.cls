/**
 * @group Project Profile Bill Rate
 * @group-content ../../ApexDocContent/ProjectProfileBillRate.htm
 *
 * @description Apex controller class having business logics
 *              to fetch, insert, update and delete profile bill rate under a project.
 */
public class GPControllerProfileBillRate {

    public List < GP_Profile_Bill_rate__c > listOfProfileBillRateobj, listOfProjectProfileBillRate;
    //public list<GP_Project__c> projectBillingCurrency;
    private String projectBillingCurrency;
    private JSONGenerator gen;
    private Id projectId;

    private final String PROJECT_BILLING_CURRENCY_LABEL = 'projectBillingCurrency';
    private final String PROFIL_BILL_RATES_LABEL = 'listOfProfileBillRates';

    public GPControllerProfileBillRate() {}

    /**
     * @description Parameterized constructor to accept Project Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerProfileBillRate(<SFDC 18/15 digit project Id>);
     */
    public GPControllerProfileBillRate(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description Parameterized constructor to accept Project Profile BillRate List.
     * @param listOfProjectProfileBillRate List Of Project Profile Bill Rate.
     * 
     * @example
     * new GPControllerProfileBillRate(<List Of Project Profile Bill Rate>);
     */
    public GPControllerProfileBillRate(List < GP_Profile_Bill_Rate__c > listOfProjectProfileBillRate) {
        this.listOfProjectProfileBillRate = listOfProjectProfileBillRate;
    }

    /**
     * @description method to return Project Profile Bill Rates.
     *
     * @example
     * new GPControllerProfileBillRate().getProfileBillRateData(<SFDC 18/15 digit project Id>);
     */
    public GPAuraResponse getProfileBillRateData(Id projectId) {
        try {

            listOfProfileBillRateobj = new GPSelectorProfileBillRate().getMapOfProjectRelatedProfileBillRates(projectId);
            //projectBillingCurrency = selectorProfile.getBillingCurrencyFromProject(projectId);
            List < GP_Project__c > lstOfProject = new GPSelectorProject().getBillingCurrencyFromProject(projectId);

            if (lstOfProject.size() > 0)
                projectBillingCurrency = lstOfProject[0].GP_Billing_Currency__c;

            setJson();
            if (listOfProfileBillRateobj != null)
                return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());

            return new GPAuraResponse(true, 'ERROR', null);

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (projectBillingCurrency != null)
            gen.writeObjectField(PROJECT_BILLING_CURRENCY_LABEL, projectBillingCurrency);
        if (listOfProfileBillRateobj != null)
            gen.writeObjectField(PROFIL_BILL_RATES_LABEL, listOfProfileBillRateobj);

        gen.writeEndObject();
    }

    /**
     * @description method to upsert Project Profile Bill Rate Records.
     * 
     * @example
     * new GPControllerProfileBillRate(<List Of Project Profile Bill Rate>).saveProjectProfileBillRate();
     */
    public GPAuraResponse saveProjectProfileBillRate() {
        try {
            projectId = listOfProjectProfileBillRate[0].GP_Project__c;
            upsert listOfProjectProfileBillRate;
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROFILE BILL RATE');
            update project; 
            
            Set < Id > setOfProfileBillRateId = getProfileBillRateId(listOfProjectProfileBillRate);
            listOfProjectProfileBillRate = [SELECT Id, GP_Profile__c,
                GP_Bill_Rate__c, GP_Bill_Rate_Type__c,GP_Project__c,
                GP_Project__r.GP_Bill_Rate_Type__c
                from GP_Profile_Bill_Rate__c
                WHERE Id in: setOfProfileBillRateId
            ];

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(listOfProjectProfileBillRate, true));
    }

    /**
     * @description method to get Set<Id> of ProfileBillRates.
     * 
     * @example
     * new GPControllerProfileBillRate().getProfileBillRateId(<List Of Project Profile Bill Rate>);
     */
    public Set < Id > getProfileBillRateId(List < GP_Profile_Bill_Rate__c > listOfProjectProfileBillRate) {
        Set < Id > setOfProfileBillRateId = new Set < Id > ();

        for (GP_Profile_Bill_Rate__c profileBillRate: listOfProjectProfileBillRate) {
            setOfProfileBillRateId.add(profileBillRate.Id);
        }

        return setOfProfileBillRateId;
    }

    /**
     * @description method to delete Project Profile Bill Rate Records.
     * 
     * @example
     * new GPControllerProfileBillRate(<SFDC 18/15 digit project Id>);.deleteProfileBillRate(<SFDC 18/15 digit project Profile Bill Rate Id>);
     */
    public GPAuraResponse deleteProfileBillRate(Id ProfileBillRate) {
        try {
            GP_Profile_Bill_Rate__c billRate = [select id,GP_Project__c from GP_Profile_Bill_Rate__c where Id =: ProfileBillRate];
            Id projectId = billRate.GP_Project__c;
            Delete billRate;
            
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJECT BUDGET');
            update project; 
            
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, 'SUCCESS', null);
    }
}