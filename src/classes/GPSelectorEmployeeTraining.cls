public class GPSelectorEmployeeTraining extends fflib_SObjectSelector {
 public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Employee_Training__c.Name,
            GP_Employee_Training__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Employee_Training__c.sObjectType;
    }

    public List < GP_Employee_Training__c > selectById(Set < ID > idSet) {
        return (List < GP_Employee_Training__c > ) selectSObjectsById(idSet);
    }

    public GP_Employee_Training__c selectById(ID masterId) {
        Set < Id > idSet = new Set < Id > {
            masterId
        };
        return (GP_Employee_Training__c) selectSObjectsById(idSet).get(0);
    }
    public static List<GP_Employee_Training__c> selectEmployeeTrainingRecords(Set<Id> setOfEmployeeIds) {
        return [Select Id, GP_Course__c,GP_Enrollment_Status__c,GP_Learner__c,GP_Status_Change_Date__c
            FROM GP_Employee_Training__c
            WHERE GP_Learner__c in :setOfEmployeeIds
        ];
    }

    public static List<GP_Employee_Training__c> selectEmployeeTrainingRecordsWithOHRId(Set<String> setOfEmployeeOHRs) {
        return [Select Id, GP_Course__c,GP_Enrollment_Status__c,GP_Learner__c,GP_Status_Change_Date__c,GP_Course_Id__c ,GP_Employee_Number__c
             ,GP_Valid_From__c,GP_Valid_To__c FROM GP_Employee_Training__c
            WHERE GP_Employee_Number__c in :setOfEmployeeOHRs
        ];
    }
    
    
    public static List<GP_Employee_Training__c> selectOldEmpTrainingRecords(set<String> setUniqueTraining, set<String>  setNewTrainingID) {
        return [    Select Id,GP_Course_Id__c,GP_Employee_Number__c, GP_Course__c,GP_Enrollment_Status__c,
                                GP_Learner__c,GP_Status_Change_Date__c
                                FROM GP_Employee_Training__c
                                WHERE GP_Unique_Training_Id__c in : setUniqueTraining 
                                AND id Not In: setNewTrainingID
                ];
    }
}