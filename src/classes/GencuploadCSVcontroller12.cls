public class GencuploadCSVcontroller12 
{
    Public Opportunity opport {get;set;}    
    public String Opid {get;set;}
    public boolean isClicked {get;set;}
    public Blob contentFile  {get; set;}
    public String nameFile   {get; set;}     
    public String oppId {get;set;}    
    public Integer rowCount  {get; set;}
    public Integer colCount  {get; set;}              
    public Integer decterm1 {get; set;}      
    public boolean hasError {get; set;}              
    public decimal intFinalResult {get; set;}    
    public boolean returnFinalList {get; set;}
    public boolean returnRenderedList {get; set;}
    public List<RevenuePricingHeader__c> revenueheaderlst = new List<RevenuePricingHeader__c>();
    public List<List<String>> parsedCSV = new List<List<String>>();
    public Map<String,List<String>> RevenueList=new Map<String,List<String>>();   
    public boolean isValid {get; set;}
    public GencuploadCSVcontroller12(ApexPages.StandardController controller) {
         Opid =ApexPages.currentPage().getParameters().get('id');              
       //  Opid =controller.getId();    
         try {
         opport = new Opportunity();
         opport =[select Id, Name, Contract_Term_in_months__c,TCV1__c,StageName  from Opportunity where Id =: Opid ];        
                  }
                  catch (System.QueryException e) {
                                     System.debug('caught ya!');
                                     }
    system.debug('fetch oppId'+ Opid);   
    isValid = true;       
         
    }

    public List<List<String>> getResults() 
    {    
        rowCount = 0;
        colCount = 0;
        try            
            {                 
                if (contentFile != null)
        { 
            String fileString = contentFile.toString();
            parsedCSV = parseCSV(fileString, False); // Calling method to separate data
            system.debug('Retrun all fields:'+parsedCSV);            
            rowCount = parsedCSV.size();
            for (List<String> row : parsedCSV)
            {
                if (row.size() > colCount)
                {
                    colCount = row.size();
                }
            }
        }               
          }   catch(Exception e){
            if(e.getMessage().contains('BLOB is not a valid UTF-8 string'))
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Pricing Template can be uploaded using csv file only'));
             } 
   
           
        return parsedCSV;                          
    }
    
    public static List<List<String>> parseCSV(String contents,Boolean skipHeaders) 
    {
        
        List<List<String>> allFields = new List<List<String>>();
        System.debug('**** Contents before blanks'+contents);
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');// Removing blank columns
        System.debug('**** Contents after blanks'+contents);
        contents = contents.replaceAll('""','DBLQT');// Removing blank columns
        System.debug('**** Contents after blanks'+contents);
        List<String> lines = new List<String>();
        try 
        {
            lines = contents.split('\r'); 
            System.debug('**** lines*******'+lines);
            
        }
        catch (System.ListException e) 
        {
            System.debug('Limits exceeded?' + e.getMessage());
        }
       
        for(String line: lines) 
        {
            if (line.replaceAll(',','').trim().length() == 0) break;
            List<String> fields = line.split(',');
            if(fields.size()==1)
            {
                fields.add('');
            }       
           /* for(String temp :fields)
            {
                Integer j =0;
                if(temp.contains('Amount in base currency'))
                {
                    fields.remove(j);
                    break;
                    
                }
                j++;
                
            } */
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            System.debug('***** field*******'+ fields);
            for(String field: fields) 
            {
                if (field.startsWith('"') && field.endsWith('"')) 
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
                else if (field.startsWith('"')) 
                {
                    makeCompositeField = true;
                    system.debug('Important Data field:'+ field);
                    compositeField = field;
                }
                else if (field.endsWith('"')) 
                {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                }
                else if (makeCompositeField) 
                {
                    compositeField +=  ',' + field;
                }
                else
                {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
                System.debug('***** cleanFields'+ cleanFields);
            }        
            allFields.add(cleanFields);
            system.debug('TEST VALUES'+allFields);   
        }  
        if (skipHeaders) allFields.remove(0);
        return allFields;    
    }
   //public String getNameFile() {        return null;    } public String getContentFile() {        return null;    }
   //public String getColCount() {        return null;    } public String getRowCount() {        return null;    }  
    
    public pageReference loadCSVdata()    
    {    system.debug('TIMES CALLED:'+parsedCSV);
       
    if(parsedCSV!=null && parsedCSV.size()>0)                      
    {                
        system.debug('TIMES CALLED:');
        RevenuePricingHeader__c rec = new RevenuePricingHeader__c(); 
        system.debug('size of parse parsedCSV:'+parsedCSV);
        isClicked = true;
        integer intYear1=0;
        integer intYear2=0;
        integer intYear3=0;
        integer intYear4=0;
        integer intYear5=0;
        integer intYear6=0;
        integer intYear7=0;
        integer intYear8=0;
        integer intYear9=0;
        integer intYear10=0;
        integer intterm1=0;   
        integer decterm =0;    
        decimal intTotalPricingTCV  =0;                 
        decimal intTotalSystemTCV  =0; 
        intFinalResult =0;       
        decimal intFinalResult1 =0;                                    
        for(List<String> parsedlist :parsedCSV)
        {
            system.debug('#!@#!@$@#@!#@!#!@#'+parsedlist);
            list<String> revenue1=new List<string>();
          
                    
             if(parsedlist[1]!=null && parsedlist[1]!='0'&& parsedlist[1]!='')
              {     
                    if(parsedlist[0] != null) 
                {
                    parsedlist[0] = parsedlist[0].trim(); 
               
                    }                  
                       
              /*  if(parsedlist[0] == 'Sales Person')
                { 
                    rec.Sales_Person__c =parsedlist[1];
                    
                }
                if(parsedlist[0] == 'Account / Deal Name')
                { 
                    rec.Account_Deal_Name__c =parsedlist[1]; 
                }*/
               if(parsedlist[0] == 'Contract Term(in months)' )
                {        
                    system.debug('#!@#$@!#!@#@!3parsedlist[1]'+integer.valueof(parsedlist[1]));
                        system.debug('#!@#$@!#!@#@!3Contract_Term_in_months__c'+opport.Contract_Term_in_months__c);
                    if(opport.Contract_Term_in_months__c!=null && opport.Contract_Term_in_months__c == integer.valueof(parsedlist[1]) )                  
                     {        
                    rec.contract_Term_in_months__c = integer.valueof(parsedlist[1]); 
                    system.debug('intvalue'+integer.valueof(parsedlist[1]));
                    intterm1 += integer.valueof(parsedlist[1]) ; 
                    system.debug('decterm--->'+decterm1);         
                    system.debug('intterm'+intterm1); 
                    if (intterm1<=12) 
                       { 
                         decterm1 = 1;              
                       }
                       else
                     {//if(decterm!=0)
                     decterm=  integer.valueof(intterm1/12);
                     decterm1 =math.abs(decterm);      
                     } 
                     }       
                     else                      
                     {    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Value of Contract Term(in months) in csv does not match the opportunity contract Term'));           
                          isValid=false;
                      }          
                             
                   }  system.debug('intterm121'+decterm1); 
                      system.debug(' '+decterm); 

                /*   if(decterm1!=0 && decterm1!=null )  
                    for(integer i=decterm1+1;i<=10;i++)          
                {                      
                        if(parsedlist[0]== 'Production Billing' || parsedlist[0]== 'One time Billing (Transition IT  etc) -Transition billing' ||parsedlist[0]== 'Total EBIT $')        
                        if(parsedlist[i]!=null && integer.valueof(parsedlist[i])!=0)        
                         {          
                            hasError=true;   
                                 
                            system.debug('haseeeeeeeeeee'+hasError);                    
                            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Value in year field should be equal to no of contract years'));           
                                   isValid=false;
                              break; }                                  
                         else       
                         {     
                            hasError=false; 
                                                            
                         }            
                }    */                        
               /* if(parsedlist[0] == 'SOW signature date')
                {
                    date mydate = date.parse(parsedlist[1]);
                    rec.SOW_signature_date__c=mydate;  
                }*/
               if(parsedlist[0] == 'SFDC Opportunity ID')
                {     
                    if(Opid!=null && Opid==parsedlist[1])  
                          rec.CRM_Deal_Id__c=parsedlist[1];   
                    else                      
                     {       ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Value of SFDC Opportunity ID in csv does not match the opportunity ID'));           
                                   isValid=false;
                               }                    
                }
              /*  if(parsedlist[0] == 'Pricing & FP&A Contact')
                {
                    rec.Pricing_FP_A_Contact__c=parsedlist[1];  
                }
                if(parsedlist[0] == 'Type of Deal')
                {
                    rec.Type_of_Deal__c=parsedlist[1];   
                }
                if(parsedlist[0] == 'Currency')
                {
                    rec.Currency__c=parsedlist[1];        
                }*/
                if(parsedlist[0] == 'Fx Rate (for currency other than USD)')
                {  
                         
                    Decimal myDecimal = decimal.valueOf(parsedlist[1]);
                    rec.Fx_Rate_for_currency_other_than_USD__c=myDecimal;
                }
                if(parsedlist[0] == 'Total Contract Value (TCV) $')
                {
                 rec.TCV__c=decimal.valueof(parsedlist[1]);                       
                 intTotalSystemTCV +=decimal.valueof(parsedlist[1]);       
                 system.debug('System TCV'+intTotalSystemTCV );                                      
                }
                System.debug('This is rec  : '+rec);
                if(parsedlist[0]== 'Production Billing')
                {
                    revenue1=parsedlist.clone();
                    system.debug('@!#!@#!@!#!@!@!#revenue1 ='+revenue1);
                    revenue1.remove(0);
                   // revenue1.remove(revenue1.size()-1);
                    system.debug('@!#!@#!@!#!@!@!#revenue1 ='+revenue1);
                    RevenueList.put(parsedlist[0],revenue1);
                     if(intYear1!=null && intYear1!=0 )
                    intYear1 += integer.valueof(parsedlist[1]) ;
                     if(intYear2!=null && intYear2!=0 )
                    intYear2 += integer.valueof(parsedlist[2]);
                     if(intYear3!=null && intYear3!=0 )
                    intYear3 += integer.valueof(parsedlist[3]);
                     if(intYear4!=null && intYear4!=0 )
                    intYear4 += integer.valueof(parsedlist[4]);
                     if(intYear5!=null && intYear5!=0 )
                    intYear5 += integer.valueof(parsedlist[5]);
                     if(intYear6!=null && intYear6!=0 )
                    intYear6 += integer.valueof(parsedlist[6]);
                    if(intYear7!=null && intYear7!=0 )
                    intYear7 += integer.valueof(parsedlist[7]);
                     if(intYear8!=null && intYear8!=0 )
                    intYear8 += integer.valueof(parsedlist[8]);
                     if(intYear9!=null && intYear9!=0 )
                    intYear9 += integer.valueof(parsedlist[9]);
                     if(intYear10!=null && intYear10!=0 )
                    intYear10 += integer.valueof(parsedlist[10]);
                }
              if(parsedlist[0]== 'One time Billing (Transition IT  etc) -Transition billing')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    //revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                    if(intYear1!=null && intYear1!=0 )
                    intYear1 += integer.valueof(parsedlist[1]) ;
                     if(intYear2!=null && intYear2!=0 )
                    intYear2 += integer.valueof(parsedlist[2]);
                     if(intYear3!=null && intYear3!=0 )
                    intYear3 += integer.valueof(parsedlist[3]);
                     if(intYear4!=null && intYear4!=0 )
                    intYear4 += integer.valueof(parsedlist[4]);
                     if(intYear5!=null && intYear5!=0 )
                    intYear5 += integer.valueof(parsedlist[5]);
                     if(intYear6!=null && intYear6!=0 )
                    intYear6 += integer.valueof(parsedlist[6]);
                    if(intYear7!=null && intYear7!=0 )
                    intYear7 += integer.valueof(parsedlist[7]);
                     if(intYear8!=null && intYear8!=0 )
                    intYear8 += integer.valueof(parsedlist[8]);
                     if(intYear9!=null && intYear9!=0 )
                    intYear9 += integer.valueof(parsedlist[9]);
                     if(intYear10!=null && intYear10!=0 )
                    intYear10 += integer.valueof(parsedlist[10]);
                }
             /*   if(parsedlist[0]== 'Other recurring Billing 1 (Scanning  Tools BCP etc)')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                    intYear1 += integer.valueof(parsedlist[1]) ;
                    intYear2 += integer.valueof(parsedlist[2]);
                    intYear3 += integer.valueof(parsedlist[3]);
                    intYear4 += integer.valueof(parsedlist[4]);
                    intYear5 += integer.valueof(parsedlist[5]);
                    intYear6 += integer.valueof(parsedlist[6]);
                    intYear7 += integer.valueof(parsedlist[7]);
                    intYear8 += integer.valueof(parsedlist[8]);
                    intYear9 += integer.valueof(parsedlist[9]);
                    intYear10 += integer.valueof(parsedlist[10]);
                 }
                if(parsedlist[0]== 'Other recurring Billing 2 (Scanning  Tools BCP etc)')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                    intYear1 += integer.valueof(parsedlist[1]) ;
                    intYear2 += integer.valueof(parsedlist[2]);
                    intYear3 += integer.valueof(parsedlist[3]);
                    intYear4 += integer.valueof(parsedlist[4]);
                    intYear5 += integer.valueof(parsedlist[5]);
                    intYear6 += integer.valueof(parsedlist[6]);
                    intYear7 += integer.valueof(parsedlist[7]);
                    intYear8 += integer.valueof(parsedlist[8]);
                    intYear9 += integer.valueof(parsedlist[9]);
                    intYear10 += integer.valueof(parsedlist[10]);
                }
                if(parsedlist[0]== 'Other recurring Billing 3 (Scanning  Tools BCP etc)')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);

                    intYear1 += integer.valueof(parsedlist[1]) ;
                    intYear2 += integer.valueof(parsedlist[2]);
                    intYear3 += integer.valueof(parsedlist[3]);
                    intYear4 += integer.valueof(parsedlist[4]);
                    intYear5 += integer.valueof(parsedlist[5]);
                    intYear6 += integer.valueof(parsedlist[6]);
                    intYear7 += integer.valueof(parsedlist[7]);
                    intYear8 += integer.valueof(parsedlist[8]);
                    intYear9 += integer.valueof(parsedlist[9]);
                    intYear10 += integer.valueof(parsedlist[10]);
                }
                if(parsedlist[0]== 'Other recurring Billing 4 (Scanning  Tools BCP etc)')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);

                    intYear1 += integer.valueof(parsedlist[1]) ;
                    intYear2 += integer.valueof(parsedlist[2]);
                    intYear3 += integer.valueof(parsedlist[3]);
                    intYear4 += integer.valueof(parsedlist[4]);
                    intYear5 += integer.valueof(parsedlist[5]);
                    intYear6 += integer.valueof(parsedlist[6]);
                    intYear7 += integer.valueof(parsedlist[7]);
                    intYear8 += integer.valueof(parsedlist[8]);
                    intYear9 += integer.valueof(parsedlist[9]);
                    intYear10 += integer.valueof(parsedlist[10]);
                }*/
                /*if(parsedlist[0]== 'Total Billing')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                }
                if(parsedlist[0]== 'EBIT %')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                    revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                }*/
                if(parsedlist[0]== 'Total EBIT $')
                {
                    revenue1=parsedlist.clone();
                    revenue1.remove(0);
                   // revenue1.remove(revenue1.size()-1);
                    RevenueList.put(parsedlist[0],revenue1);
                }
          system.debug('yeaaar'+intYear1);
          
}
          else
          { 
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please enter the appropriate value in all the rows of your csv'));           
            isValid=false;
          }      
                   
       }
      //double int1=0;
     // Integer int2=0;
   
        Double decYear1=0.00;
        //Decimal div1=0.0;
        Decimal decYear2=0.00;
        Decimal decYear3=0.00;
        Decimal decYear4=0.00;
        Decimal decYear5=0.00;
        Decimal decYear6=0.00;
        Decimal decYear7=0.00;
        Decimal decYear8=0.00;
        Decimal decYear9=0.00;
        Decimal decYear10=0.00;
       /* Integer decYearr2=0;
        Decimal decYearr2=0.00;
        Decimal decYear3=0.00;
        Decimal decYear4=0.00;
        Decimal decYear5=0.00;
        Decimal decYear6=0.00;
        Decimal decYear7=0.00;
        Decimal decYear8=0.00;
        Decimal decYear9=0.00;
        Decimal decYear10=0.00 */     
        system.debug('11size of parse parsedCSV:'+parsedCSV);
        system.debug('22size of parse parsedCSV:'+parsedCSV.size());

     /*   for(List<String> parsedlist :parsedCSV)
        {
          system.debug('11size of parse parsedCSV:'+parsedlist[1]);
        
          if(parsedlist[1] !=null && parsedlist[1] !='0' && parsedlist[1] !='') 
             s{ 
                if(parsedlist[0] != null) 
                {
                    parsedlist[0] = parsedlist[0].trim(); 
                }
                if(parsedlist[0]== 'Total EBIT $')
                {   system.debug('parsed list'+parsedlist[1]);
                    system.debug('intyear'+intYear1);

                 if(intYear1!=null && parsedlist[1]!=null && intYear1!=0 )
                    {
                    system.debug('parsed list'+parsedlist[1]);

                     //int1=double.valueof(parsedlist[1]);
                    // system.debug('int1'+int1);
                     //div1=; 
                     //system.debug('div1'+div1);
                     decYear1=((double.valueof(parsedlist[1])/intYear1)*100);
                     system.debug('decYear1'+decYear1);
  
                    }
                    if(intYear2!=null && parsedlist[2]!=null && intYear2!=0 )
                    {
                    decYear2=((double.valueof(parsedlist[2])/intYear2)*100);
                    }
                    if(intYear3!=null && parsedlist[3]!=null && intYear3!= 0)
                    {
                         decYear3=((double.valueof(parsedlist[3])/intYear3)*100);   
                    }
                    if(intYear4!=null && parsedlist[4]!=null && intYear4!=0)
                    {
                         decYear4=((integer.valueof(parsedlist[4])/intYear4)*100);   
                    }
                    if(intYear5!=null && parsedlist[5]!=null && intYear5!=0)
                    {
                         decYear5=((double.valueof(parsedlist[5])/intYear5)*100);   
                    }
                    if(intYear6!=null && parsedlist[6]!=null && intYear6!=0)
                    {
                         decYear6=((double.valueof(parsedlist[6])/intYear6)*100);   
                    }
                    if(intYear7!=null && parsedlist[7]!=null && intYear7!=0)
                    {
                         decYear7=((double.valueof(parsedlist[7])/intYear7)*100);   
                    }
                    if(intYear8!=null && parsedlist[8]!=null && intYear8!=0)
                    {
                         decYear8=((double.valueof(parsedlist[8])/intYear8)*100);   
                    }
                    if(intYear9!=null && parsedlist[9]!=null && intYear9!=0)
                    {
                         decYear9=((double.valueof(parsedlist[9])/intYear9)*100);   
                    }
                    if(intYear10!=null && parsedlist[10]!=null && intYear10!=0)
                    {
                         decYear10=((double.valueof(parsedlist[10])/intYear10)*100);   
                    }
                }
                /*   if(parsedlist[0] == 'Contract Term')
                { 
                   if(intterm1!= null && parsedlist[1]!=null  &&integer.valueof(parsedlist[1]!= 0)       
                     {
                        decterm  = intterm1/12;
                     }      
                  //   if(parsedlist[0]== 'Production Billing' && 0<= decterm <=1  &&)
        
                       //  if(0<= decterm <=1                  
                }*/

            
         /*   }      
            else
            { 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please enter the appropriate value in all the rows of your csv'));         
                isValid=false;  
            }    
        } */
      //  if(isValid==true && hasError==false)
     //comment by sharif Start 
    if(isValid==true )
        {                                                
        intTotalPricingTCV  +=  intYear1+intYear2+intYear3+intYear4+intYear5+intYear6+intYear7+intYear8+intYear9+intYear10;             
        system.debug('Pricing TCV'+intTotalPricingTCV);                 
        List<String> totalBillingRevenue =new List<String>();
        List<String> totalEBIT =new List<String>();
        if(intYear1 != 0)
        totalBillingRevenue.add(string.valueof(intYear1));
         if(intYear2 != 0)
        totalBillingRevenue.add(string.valueof(intYear2));
         if(intYear3 != 0)
        totalBillingRevenue.add(string.valueof(intYear3));
         if(intYear4 != 0)
        totalBillingRevenue.add(string.valueof(intYear4));
         if(intYear5 != 0)
        totalBillingRevenue.add(string.valueof(intYear5));
         if(intYear6!= 0)
        totalBillingRevenue.add(string.valueof(intYear6));
         if(intYear7!= 0)
        totalBillingRevenue.add(string.valueof(intYear7));
         if(intYear8!= 0)
        totalBillingRevenue.add(string.valueof(intYear8));
         if(intYear9!= 0)
        totalBillingRevenue.add(string.valueof(intYear9));
         if(intYear10!= 0)
        totalBillingRevenue.add(string.valueof(intYear10)); 
        
    //  comment by sharif End 
        
        
        
       /*  if(decYear1!= 0)
         system.debug('yeaaarnnn'+decYear1);
        totalEBIT.add(string.valueof(decYear1));
         if(decYear2!= 0)
        totalEBIT.add(string.valueof(decYear2));
          if(decYear3!= 0)
        totalEBIT.add(string.valueof(decYear3));
          if(decYear4!= 0)
        totalEBIT.add(string.valueof(decYear4));
          if(decYear5!= 0)
        totalEBIT.add(string.valueof(decYear5));
          if(decYear6!= 0)
        totalEBIT.add(string.valueof(decYear6));
          if(decYear7!= 0)
        totalEBIT.add(string.valueof(decYear7));
          if(decYear8!= 0)
        totalEBIT.add(string.valueof(decYear8));
          if(decYear9!= 0)
        totalEBIT.add(string.valueof(decYear9));
          if(decYear10!= 0)
        totalEBIT.add(string.valueof(decYear10));*/
    
 /* comment by sharif Start 
       RevenueList.put('Total Billing',totalBillingRevenue);
        system.debug('revenue list'+totalEBIT);
comment by sharif End 
        */
        
        
      //  RevenueList.put('EBIT%',totalEBIT);  system.debug('revenue list'+RevenueList);
        system.debug('this is rec:--->'+rec);      
        revenueheaderlst.add(rec);     
        system.debug('display revenueheaderlst'+revenueheaderlst);         
        system.debug('Final result'+intFinalResult );             
                  
     /*   if(intTotalSystemTCV!=0 &&intTotalSystemTCV!=null && totalBillingRevenue.size() >0 )
        { //if(intFinalResult1!=0 ||intFinalResult1!=null)        
        intFinalResult1= (((intTotalSystemTCV-intTotalPricingTCV)/intTotalSystemTCV)*100);                                
           intFinalResult = math.ceil(intFinalResult1); }
            system.debug('nnnnnnnnn11'+intFinalResult  );
             system.debug('nnnnnnnnn'+(intFinalResult >10) );
          if(intFinalResult!=0 ||intFinalResult!=null)
           if(intFinalResult > 10)   
           { 
               returnFinalList =true;     
           } 
           else  */                 
           insertHeader();
     }
     
     }   return null;        
    }  
    
    public PageReference insertHeader()
    {
    system.debug('revenueheaderlst'+revenueheaderlst);
        try{   
        if(revenueheaderlst!= null)
        {
            system.debug('LISTTTTSIZEEEEE:'+returnRenderedList);
            system.debug('revenueheaderlst:'+revenueheaderlst);
            validate();  
                                   
            //if(hasError==false) 
       
             //     {                 
                   insert revenueheaderlst;
             Map<String,Revenue_Pricing__c> revList=new Map<String,Revenue_Pricing__c>();
            Set <String> valuesSet = new Set<String>();
            system.debug('RevenueList.keySet()'+RevenueList.keySet());
            valuesSet = RevenueList.keySet();
            for(String s1:valuesSet){
                Revenue_Pricing__c rev=new Revenue_Pricing__c();
                rev.RevenuePricingHeader__c=revenueheaderlst[0].Id;
                rev.Name=s1;
                revList.put(rev.Name,rev);    
                system.debug('print revlist values'+ revList); 
                 }
                 
            system.debug('#@#!@#!@#!@#@RevenueList Map ='+RevenueList);            
            if(revList!=null){
                insert revList.values();
                List<Revenue_Pricing_Year__c> revenuelist1 = new List<Revenue_Pricing_Year__c>();
                for(String s2:RevenueList.keyset()){
                    integer col=1;
                    for(String s3:RevenueList.get(s2)){
                        Revenue_Pricing_Year__c revenue = new Revenue_Pricing_Year__c();
                        revenue.Year__c=col;                
                        revenue.Revenue_Pricing__c=revList.get(s2).Id;
                        if(Decimal.valueof(s3)<0||s3==null)
                            revenue.Revenue__c=0;
                        else
                            revenue.Revenue__c=Decimal.valueof(s3);
                        col++;
                        revenuelist1.add(revenue);
                    }
                }
                system.debug('$%@#$@#$!@$@$!@$'+revenuelist1);
                insert revenuelist1;
            }
        //  }
        } 
            }catch(exception e){ 
            if(e.getMessage().contains('MSA/SOW Close Date should be always greater than today'))          
                     ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'MSA/SOW Close Date should be always greater than today date'));        
                    else      
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'' + e.getMessage()));                         
                    }             
     
     returnFinalList=false;
        return null;
    }
     public PageReference opppage()
    {
  
        PageReference ref =  new PageReference('/'+oppId);
        return ref;    }

   
    public PageReference validate()
    {
  
        RevenuePricingHeader__c test = new RevenuePricingHeader__c();    
        new ApexPages.Message(ApexPages.Severity.Info, 'Pricing already exists.');
        return null;
    }


}