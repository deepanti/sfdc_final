public class GPSelectorAccount extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            Account.Name,
                Account.Id, 
                Account.Name, 
                Account.Business_Group__c,
                Account.Business_Segment__r.Name,
                Account.Sub_Business__r.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Account.sObjectType;
    }

    public List<Account> selectById(Set<ID> idSet) {
        return (List<Account>) selectSObjectsById(idSet);
    }
    
    public Account selectById(ID accountId) {
        Set<Id> idSet = new Set<Id> {
            accountId
        };
        return (Account) selectSObjectsById(idSet).get(0);
    }
    public Map<Id,Account> selectaccount(Set<Id> setOfaccountid)
    {
        return new Map<Id,Account>([Select ID,Name,Business_Group__c,Business_Segment__r.Name,
        Business_Segment__r.Id,Sub_Business__r.Name,Sub_Business__r.Id from Account where Id =: setOfaccountid]);
    }
    
    public Account getAccountWithBusiness(ID accountId) {
        return [Select
                Id, 
                Name, 
                Business_Group__c,
                Business_Segment__r.Name,
                Sub_Business__r.Name
                From Account 
                where Id = :accountId];
    } 
    public Account getVerticalSubVertical(ID accountId) {
        return [Select
                Id, 
                Sub_Business__c,
                Business_Group__c,
                Business_Segment__c,
                Sub_Business__r.Name,
                Industry_Vertical__c, 
                Sub_Industry_Vertical__c,
                Business_Segment__r.Name
                From Account 
                where Id = :accountId];
    }
    // Mass Update Change
    public List<Account> getAccountDetailsForMassUpdate(Set<String> setOfAccountIds) {
        String queryStr = 'SELECT Id, Business_Group__c, Business_Segment__c, Business_Segment__r.Name, Sub_Business__c, Sub_Business__r.Name FROM Account WHERE Inactive__c = false AND';
        
        if(setOfAccountIds.size() > 0) {
            queryStr += ' Id =: setOfAccountIds AND';
        }
		
        queryStr = queryStr.removeEnd('AND');
        return Database.query(queryStr);
    }
}