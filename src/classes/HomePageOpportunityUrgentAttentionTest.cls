@istest
public class HomePageOpportunityUrgentAttentionTest {
    static opportunity opp31,opp, opp1, opp2, opp3, opp4, opp5,opp6, opp7, opp8, opp9, opp10, opp11, opp12, opp13, opp14, opp15, opp16,opp21,opp22;
    
    @testSetup
    public static void setupData()
    {
        User testUser=TestDataFactoryUtility.createTestUser('Genpact Super Admin', 'emailTest1xyz@acmexyz.com', 'emailTest1xyz@xyzasdxyz.com');
        insert testUser;
        System.runAs(testUser){
            Account accountobject=TestDataFactoryUtility.createTestAccountRecord();
            accountobject.Name='TEST CUSTOMER 1234';
            insert accountobject;
            List<Non_TS_Ageing_Rules__c> Non_TS_Ageing_Rule_List=new List<Non_TS_Ageing_Rules__c>();
            Non_TS_Ageing_Rules__c Discover_large = new Non_TS_Ageing_Rules__c();
            Discover_large.Name = 'Discover_large';
            Discover_large.Ageing__c = 0;
            
            Non_TS_Ageing_Rules__c Define_large = new Non_TS_Ageing_Rules__c();
            Define_large.Name = 'Define_large';
            Define_large.Ageing__c = 0;
            
            Non_TS_Ageing_Rules__c Discover_Extra_large=new Non_TS_Ageing_Rules__c();
            Discover_Extra_large.Name='Discover_Extra_large';
            Discover_Extra_large.Ageing__c=0;
            
            Non_TS_Ageing_Rules__c Define_Extra_large=new Non_TS_Ageing_Rules__c();
            Define_Extra_large.Name='Define_Extra_large';
            Define_Extra_large.Ageing__c=0;
            
            Non_TS_Ageing_Rule_List.add(Discover_large);
            Non_TS_Ageing_Rule_List.add(Define_large);
            Non_TS_Ageing_Rule_List.add(Discover_Extra_large);
            Non_TS_Ageing_Rule_List.add(Define_Extra_large);
            insert Non_TS_Ageing_Rule_List;
            
            List <Non_Ts_cycle_Time__c> Non_Ts_cycle_Time_List =new List<Non_Ts_cycle_Time__c>();
            Non_Ts_cycle_Time__c setting1 =new Non_Ts_cycle_Time__c();
            setting1.Name='setting1';
            setting1.Stage__c='1. Discover';
            setting1.Name__c='Digital';
            setting1.Type_of_deal_for_non_ts__c='Retail';
            setting1.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting1);
            
            Non_Ts_cycle_Time__c setting2 =new Non_Ts_cycle_Time__c();
            setting2.Name='setting2';
            setting2.Stage__c='1. Discover';
            setting2.Name__c='Digital';
            setting2.Type_of_deal_for_non_ts__c='Medium';
            setting2.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting2);
            
            Non_Ts_cycle_Time__c setting3 =new Non_Ts_cycle_Time__c();
            setting3.Name='setting3';
            setting3.Stage__c='1. Discover';
            setting3.Name__c='Digital';
            setting3.Type_of_deal_for_non_ts__c='Large';
            setting3.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting3);
            
            Non_Ts_cycle_Time__c setting4 =new Non_Ts_cycle_Time__c();
            setting4.Name='setting4';
            setting4.Stage__c='2. Define';
            setting4.Name__c='Digital';
            setting4.Type_of_deal_for_non_ts__c='Retail';
            setting4.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting4);
            
            Non_Ts_cycle_Time__c setting5 =new Non_Ts_cycle_Time__c();
            setting5.Name='setting5';
            setting5.Stage__c='2. Define';
            setting5.Name__c='Digital';
            setting5.Type_of_deal_for_non_ts__c='Medium';
            setting5.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting5);
            
            Non_Ts_cycle_Time__c setting6 =new Non_Ts_cycle_Time__c();
            setting6.Name='setting6';
            setting6.Stage__c='2. Define';
            setting6.Name__c='Digital';
            setting6.Type_of_deal_for_non_ts__c='Large';
            setting6.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting6);
            
            Non_Ts_cycle_Time__c setting7 =new Non_Ts_cycle_Time__c();
            setting7.Name='setting7';
            setting7.Stage__c='1. Discover';
            setting7.Name__c='Analytics';
            setting7.Type_of_deal_for_non_ts__c='Retail';
            setting7.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting7);
            
            Non_Ts_cycle_Time__c setting8 =new Non_Ts_cycle_Time__c();
            setting8.Name='setting8';
            setting8.Stage__c='1. Discover';
            setting8.Name__c='Analytics';
            setting8.Type_of_deal_for_non_ts__c='Medium';
            setting8.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting8);
            
            Non_Ts_cycle_Time__c setting9 =new Non_Ts_cycle_Time__c();
            setting9.Name='setting9';
            setting9.Stage__c='1. Discover';
            setting9.Name__c='Analytics';
            setting9.Type_of_deal_for_non_ts__c='Large';
            setting9.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting9);
            
            
            Non_Ts_cycle_Time__c setting10 =new Non_Ts_cycle_Time__c();
            setting10.Name='setting10';
            setting10.Stage__c='2. Define';
            setting10.Name__c='Analytics';
            setting10.Type_of_deal_for_non_ts__c='Retail';
            setting10.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting10);
            
            Non_Ts_cycle_Time__c setting11 =new Non_Ts_cycle_Time__c();
            setting11.Name='setting11';
            setting11.Stage__c='2. Define';
            setting11.Name__c='Analytics';
            setting11.Type_of_deal_for_non_ts__c='Medium';
            setting11.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting11);
            
            Non_Ts_cycle_Time__c setting12 =new Non_Ts_cycle_Time__c();
            setting12.Name='setting12';
            setting12.Stage__c='2. Define';
            setting12.Name__c='Analytics';
            setting12.Type_of_deal_for_non_ts__c='Large';
            setting12.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting12);
            
            
            
            Non_Ts_cycle_Time__c setting13 =new Non_Ts_cycle_Time__c();
            setting13.Name='setting13';
            setting13.Stage__c='1. Discover';
            setting13.Name__c='consulting';
            setting13.Type_of_deal_for_non_ts__c='Retail';
            setting13.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting13);
            
            Non_Ts_cycle_Time__c setting14 =new Non_Ts_cycle_Time__c();
            setting14.Name='setting14';
            setting14.Stage__c='1. Discover';
            setting14.Name__c='consulting';
            setting14.Type_of_deal_for_non_ts__c='Medium';
            setting14.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting14);
            
            Non_Ts_cycle_Time__c setting15 =new Non_Ts_cycle_Time__c();
            setting15.Name='setting15';
            setting15.Stage__c='1. Discover';
            setting15.Name__c='consulting';
            setting15.Type_of_deal_for_non_ts__c='Large';
            setting15.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting15);
            
            
            Non_Ts_cycle_Time__c setting16 =new Non_Ts_cycle_Time__c();
            setting16.Name='setting16';
            setting16.Stage__c='2. Define';
            setting16.Name__c='consulting';
            setting16.Type_of_deal_for_non_ts__c='Retail';
            setting16.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting16);
            
            Non_Ts_cycle_Time__c setting17 =new Non_Ts_cycle_Time__c();
            setting17.Name='setting17';
            setting17.Stage__c='2. Define';
            setting17.Name__c='consulting';
            setting17.Type_of_deal_for_non_ts__c='Medium';
            setting17.Ageing__c=0;
            
            Non_Ts_cycle_Time_List.add(setting17);
            
            Non_Ts_cycle_Time__c setting18 =new Non_Ts_cycle_Time__c();
            setting18.Name='setting18';
            setting18.Stage__c='2. Define';
            setting18.Name__c='consulting';
            setting18.Type_of_deal_for_non_ts__c='Large';
            setting18.Ageing__c=0;
             
            Non_Ts_cycle_Time_List.add(setting18);
            
            insert Non_Ts_cycle_Time_List;
            
            List<Non_TS_restrcited_access__c> restrictedList=new List<Non_TS_restrcited_access__c>(); 
            
            Non_TS_restrcited_access__c setting19 =new Non_TS_restrcited_access__c();
            setting19.days__c=0;
            setting19.Name='Discover_medium';
			restrictedList.add(setting19);
            
            Non_TS_restrcited_access__c setting20 =new Non_TS_restrcited_access__c();
            setting20.days__c=0;
            setting20.Name='Define_medium';
			restrictedList.add(setting20);
            
            Non_TS_restrcited_access__c setting21 =new Non_TS_restrcited_access__c();
            setting21.days__c=0;
            setting21.Name='On-bid_medium';
			restrictedList.add(setting21);
            
            Non_TS_restrcited_access__c setting22 =new Non_TS_restrcited_access__c();
            setting22.days__c=0;
            setting22.Name='Down Select_medium';
			restrictedList.add(setting22);
            
             Non_TS_restrcited_access__c setting23 =new Non_TS_restrcited_access__c();
            setting23.days__c=0;
            setting23.Name='Discover_large';
			restrictedList.add(setting23);
            
            Non_TS_restrcited_access__c setting24 =new Non_TS_restrcited_access__c();
            setting24.days__c=0;
            setting24.Name='Define_large';
			restrictedList.add(setting24);
            
           
            Non_TS_restrcited_access__c setting25 =new Non_TS_restrcited_access__c();
            setting25.days__c=0;
            setting25.Name='On-bid_large';
			restrictedList.add(setting25);
            
            Non_TS_restrcited_access__c setting26 =new Non_TS_restrcited_access__c();
            setting26.days__c=0;
            setting26.Name='Down Select_large';
			restrictedList.add(setting26);
            
            Non_TS_restrcited_access__c setting27 =new Non_TS_restrcited_access__c();
            setting27.days__c=0;
            setting27.Name='Confirmed_large';
			restrictedList.add(setting27);
            
            Non_TS_restrcited_access__c setting28 =new Non_TS_restrcited_access__c();
            setting28.days__c=0;
            setting28.Name='Discover_extra_large';
			restrictedList.add(setting28);
            
             Non_TS_restrcited_access__c setting29 =new Non_TS_restrcited_access__c();
            setting29.days__c=0;
            setting29.Name='Define_Extra_large';
			restrictedList.add(setting29);
            
             Non_TS_restrcited_access__c setting30 =new Non_TS_restrcited_access__c();
            setting30.days__c=0;
            setting30.Name='On-bid_Extra_large';
			restrictedList.add(setting30);
            
            Non_TS_restrcited_access__c setting31 =new Non_TS_restrcited_access__c();
            setting31.days__c=0;
            setting31.Name='Down Select_Extra_large';
			restrictedList.add(setting31);
            
            Non_TS_restrcited_access__c setting32 =new Non_TS_restrcited_access__c();
            setting32.days__c=0;
            setting32.Name='Confirmed_Extra_large';
			restrictedList.add(setting32);
            
            
            insert restrictedList;
            list<opportunity> oliList= new List<opportunity>();
            opp=new opportunity(name='1234',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                                amount = 2000000);
            
            opp15=new opportunity(name='12348',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                                  amount = 20000000);
            
            opp2=new opportunity(name='1236',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 2000000);
            opp3=new opportunity(name='1237',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 2000000);
            opp5=new opportunity(name='12311',RFXStatus__c='No',StageName='3. On Bid',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 2000000);
            opp7=new opportunity(name='1239',RFXStatus__c='No',StageName='4. Down Select',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 2000000);
            
            opp14=new opportunity(name='12324',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 200000001);
            
            opp8=new opportunity(name='12312',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 6000000);
            opp9=new opportunity(name='12313',RFXStatus__c='No',StageName='3. On Bid',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                 amount = 6000000);
            opp10=new opportunity(name='12314',RFXStatus__c='No',StageName='4. Down Select',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000000);
            opp11=new opportunity(name='12315',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000000);
            opp31=new opportunity(name='12331',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000);
            oliList.add(opp);
            oliList.add(opp2);
            oliList.add(opp3);
            oliList.add(opp5);
            oliList.add(opp7);
            oliList.add(opp8);
            oliList.add(opp9);
            oliList.add(opp10);
            oliList.add(opp11);
            oliList.add(opp14);
            oliList.add(opp15);
            oliList.add(opp31);
            
            insert oliList;
            
            list<opportunity> oliList1= new List<opportunity>();
            Opportunity oppAnalytics=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                     Type_Of_Opportunity__c = 'TS',Amount = 10000,Deal_Nature__c='Analytics');
            Opportunity defineAnalytics=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                        Type_Of_Opportunity__c = 'TS',Amount = 10000,Deal_Nature__c='Analytics');
            Opportunity discoverConsulting=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                           Type_Of_Opportunity__c = 'TS',Amount = 10000,Deal_Nature__c='conslting');
            Opportunity defineConsulting=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                         Type_Of_Opportunity__c = 'TS',Amount = 10000,Deal_Nature__c='conslting');
            Opportunity defineDigital=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                      Type_Of_Opportunity__c = 'TS',Amount = 10000,Deal_Nature__c='Digital');
            Opportunity discoverDigital=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                        Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='Digital');
            
            Opportunity defineDigitalMedium=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                            Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='Digital');
            Opportunity discoverAnalyticsMedium=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                                Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='Analytics');
            Opportunity defineAnalyticsMedium=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                              Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='Analytics');
            Opportunity discoverConsultingMedium=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                                 Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='conslting');
            
            Opportunity defineConsultingMedium=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                               Type_Of_Opportunity__c = 'TS',Amount = 4900000,Deal_Nature__c='conslting');
            
            Opportunity defineDigitalLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                           Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='Digital');
            
            Opportunity discoverDigitalLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                             Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='Digital');
            
            Opportunity discoverAnalyticsLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                               Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='Analytics');
            
            Opportunity defineAnalyticsLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                             Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='Analytics');
            
            Opportunity discoverConsultingLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                                Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='conslting');
            
            Opportunity defineConsultingLarge=new opportunity(name='1238',RFXStatus__c='No',StageName='2. Define',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                              Type_Of_Opportunity__c = 'TS',Amount = 6000000,Deal_Nature__c='conslting');
            Opportunity discoverDigitalSmall=new opportunity(name='1238',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                                        Type_Of_Opportunity__c = 'TS',Amount = 100,Deal_Nature__c='Digital');
            
    
            oliList1.add(oppAnalytics);
            oliList1.add(defineAnalytics);
            oliList1.add(discoverConsulting);
            oliList1.add(defineConsulting);
            oliList1.add(defineDigital);
            oliList1.add(discoverDigital);
            oliList1.add(defineDigitalMedium);
            oliList1.add(discoverAnalyticsMedium);
            oliList1.add(defineAnalyticsMedium);
            oliList1.add(discoverConsultingMedium);
            oliList1.add(defineConsultingMedium);
            oliList1.add(defineDigitalLarge);
            oliList1.add(discoverDigitalLarge);
            oliList1.add(discoverAnalyticsLarge);
            oliList1.add(defineAnalyticsLarge);
            oliList1.add(discoverConsultingLarge);
            oliList1.add(defineConsultingLarge);
            oliList1.add(discoverDigitalSmall);
            insert oliList1;
            
        }
    }
    @istest
    public static void testOpportunityFunctions()
    {
        
        User testUser=[select id,Email,FirstName,LastName from user where email='emailTest1xyz@xyzasdxyz.com'];
        System.runAs(testUser){
            
            Opportunity opp =[select id from opportunity limit 1];
            //sales leader
            HomePageOpportunityUrgentAttention.createOpportunityWrapperList(false,true, -1,0);
            HomePageOpportunityUrgentAttention.createOpportunityWrapperList(false,false, -1,0);
            //seller
            HomePageOpportunityUrgentAttention.createOpportunityWrapperList(true,true, -1,0);
            HomePageOpportunityUrgentAttention.createOpportunityWrapperList(true,false, -1,0);
            
            List<User> userList=new List<User>{testUser};
            EmailTemplate templateid = [SELECT Id FROM EmailTemplate limit 1];
            try
            {
               HomePageOpportunityUrgentAttention.sendFollowUpEmail(userList, 'sfdc.helpdesk@genpact.com', templateid.id,opp.id);
             
            }
            catch(Exception e)
            {
                
            }
            
        }
        
    }
}