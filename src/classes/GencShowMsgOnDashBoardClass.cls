public class GencShowMsgOnDashBoardClass
{   
     public String Msg{get;set;}   
     Public GencShowMsgOnDashBoardClass()
      {     
         Msg = '';  
         Map<String, ShowMsgOnDashBoard__c> SettingMap= ShowMsgOnDashBoard__c.getAll();  
         if(SettingMap.containskey((Userinfo.getuserid().substring(0,15))))  
         {    
                Msg =  SettingMap.get((Userinfo.getuserid().substring(0,15))).Message__c; 
         }  
       }
 }