@isTest
public class CheckAccountExpireTest
{
    static testMethod void CheckAccountTestMethod()      
    {
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
         Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
         Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
         Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');              
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Opportunity  OppObj1 = GEN_Util_Test_Data.CreateOpportunity('TestOpportunity1',oAccount.id,null);
        Opportunity  OppObj2 = GEN_Util_Test_Data.CreateOpportunity('TestOpportunity2',oAccount.id,null);
               String jobId = System.schedule('testCheckAccountExpireScheduler','0 0 0 3 9 ? 2050', new CheckAccountExpireScheduler());
         
       String StrQuery = 'SELECT Id, Name,Type FROM Account';  
       CheckAccountExpire reassign = new CheckAccountExpire(StrQuery);
       ID batchprocessid = Database.executeBatch(reassign); 
    }
}