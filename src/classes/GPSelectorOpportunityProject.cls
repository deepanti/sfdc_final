public without Sharing class GPSelectorOpportunityProject extends fflib_SObjectSelector {
  public List<Schema.SObjectField> getSObjectFieldList() {
       return new List<Schema.SObjectField> {
            GP_Opportunity_Project__c.Name,
            GP_Opportunity_Project__c.GP_Unique_EP_Number__c   
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Opportunity_Project__c.sObjectType;
    }

    public List<GP_Opportunity_Project__c> selectById(Set<ID> idSet) {
        return (List<GP_Opportunity_Project__c>) selectSObjectsById(idSet);
    }
    
     
    
    public List<GP_Opportunity_Project__c> selectOppProject(Set<string> setofOppAutoNumberId) {
        return (List<GP_Opportunity_Project__c>) [Select Id,GP_Probability__c,GP_Opportunity_Id__c from GP_Opportunity_Project__c where GP_Opportunity_Id__c IN:setofOppAutoNumberId];
    }
    
    public List<GP_Opportunity_Project__share> selectOpportunityProjectShareById(Set<ID> setOfOppProjectId) {
                    return (List<GP_Opportunity_Project__share>) [SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId
                    FROM GP_Opportunity_Project__share
                    where ParentId in : setOfOppProjectId];
                    }
    
    public List<GP_Opportunity_Project__c> selectOppProjectNoClosed(Set<string> setofOppAutoNumberId) 
    {
        return (List<GP_Opportunity_Project__c>) [Select Id,GP_Probability__c,GP_Opportunity_Id__c from GP_Opportunity_Project__c where 
        										GP_Opportunity_Id__c IN:setofOppAutoNumberId and GP_Project_Status_Code__c != 'CLOSED'];
    }
    
    
    public List<GP_Opportunity_Project__c> selectOppproductForClassification(List<Id> listOfOppProjectId) {
        return [SELECT
                   Id,GP_TCV__c,
                   GP_Status__c,
                   GP_SDO_Code__c,
                   GP_FP_A_Emp__c,
                   GP_Product_Id__c,
                   GP_StageName__c,
                   GP_Oracle_PID__c,
                   GP_Opportunity__c,
                   GP_Probability__c,
                   GP_Cost_Center__c,
                   GP_Project_Type__c,
                   GP_Project_Name__c,
                   GP_IsSoftDeleted__c,
                   GP_Error_Message__c,
                   GP_Oracle_Status__c,
                   GP_Billing_Entity__c,
                   GP_GPM_Start_Date__c,
                   GP_Opportunity_Id__c,
                   GP_FP_A_Start_Date__c,
                   GP_Sub_Business_L3__c,
                   GP_Project_End_Date__c,
                   GP_Customer_L4_Name__c,
                   GP_Business_Group_L1__c,
                   GP_FP_A_GPM_End_Date__c,
                   GP_EP_Project_Number__c,
                   GP_Project_Abbr_Name__c,
                   GP_Project_Long_Name__c,
                   GP_Pinnacle_Push_Date__c,
                   GP_Oracle_Process_Log__c,
                   GP_Project_Start_Date__c,
                   GP_Probability_Number__c,
                   GP_Sub_Business_L3_Id__c,
                   GP_Oracle_Internal_Id__c,
                   GP_Business_Segment_L2__c,
                   GP_Project_Description__c,
                   GP_Sales_Internal_Oppid__c,
                   GP_Project_Organization__c,
                   GP_Global_Project_Manager__c,
                   GP_Business_Segment_L2_Id__c 
                FROM
                   GP_Opportunity_Project__c 
                WHERE
                   Id in :listOfOppProjectId];
    }
}