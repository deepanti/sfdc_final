public without sharing class GPSelectorRole extends fflib_SObjectSelector {
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Role__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Role__c.sObjectType;
    }
    
    public List<GP_Role__c> selectById(Set<ID> idSet) {
        return (List<GP_Role__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Role__c> selectByDealAccess(Set<string> setOfDeaAccess) {
        return (List<GP_Role__c>) [select id, GP_Deal_Access_Group_Name__c,GP_Business_Name__c,GP_Business_Type__c from GP_Role__c 
                                   where GP_Deal_Access_Group_Name__c in: setOfDeaAccess and
                                   GP_Active__c = true and
                                   GP_Role_Category__c = 'PID Creation'
                                  ];
    }
    
    public List<GP_Role__c> selectBySDO(Set<Id> setofSDOId) {
        return (List<GP_Role__c>) [select id, GP_Business_Name__c,GP_Business_Type__c,GP_Work_Location_SDO_Master__c,GP_Group_Name_for_Read_Access__c from GP_Role__c 
                                   where GP_Work_Location_SDO_Master__c in: setofSDOId and
                                   GP_Active__c = true and
                                   GP_Role_Category__c = 'PID Updation' and
                                   GP_Type_of_Role__c = 'SDO'];
    }
    
    public List<GP_Role__c> selectByHSL(Set<Id> setofHSL) {
        return (List<GP_Role__c>) [select id, GP_Business_Name__c,GP_Business_Type__c,name, GP_HSL_Master__c,GP_Group_Name_for_Read_Access__c from GP_Role__c 
                                   where GP_HSL_Master__c in: setofHSL and
                                   GP_Active__c = true and
                                   GP_Type_of_Role__c = 'HSL Master'];
    }
    
    public List<GP_Role__c> selectByRoleId(Set<Id> Setofid) {
        return (List<GP_Role__c>)[SELECT ID, GP_Work_Location_SDO_Master__c, GP_Work_Location_SDO_Master__r.GP_Business_Name__c,
        								GP_Work_Location_SDO_Master__r.GP_Business_Type__c,
        								GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c,GP_Deal_Access_Group_Name__c,
                                        GP_Customer_L4__c, GP_Customer_L4__r.Name, GP_HSL_Master__r.GP_Unique_Number__c, 
                                        GP_HSL_Master__c, GP_HSL_Master__r.Name,GP_Group_Name_for_Read_Access__c
                                        from GP_Role__c where 
                                        Id in: Setofid];
    }
     
    public List<GP_Role__c> selectByRoleDealAccess(Set<string> setOfDealAccess) {
        return (List<GP_Role__c>)[select id,GP_Business_Name__c,GP_Deal_Access_Group_Name__c,GP_Business_Type__c , GP_Group_Name_for_Read_Access__c
                                                from GP_Role__c where GP_Deal_Access_Group_Name__c in : setOfDealAccess and
                                                GP_Active__c = true
                                               ];
    }   
}