/**
 * Name : Dhruv Singh
 * Created : 08-July-19
 * Email : dhruv.singh@saasfocus.com
 * Description : GPCommonUtil class would keep the generic code used in multiple classes.
 */

// Mobile App Change
public class GPCommonUtil {
    
    // Get Global Value Set.
    public static Map<String,String> getGlobalValues(List<String> keyList) {
        Map<String,String> mapOfKeyValues = new Map<String,String>();
        
        for(String key : keyList) {
            String valueSet = '';            
            GP_Global_Value_Set__c gvs = GP_Global_Value_Set__c.getValues(key);
            Sobject gvsObject = (Sobject) gvs;
            Integer fieldsCount = Integer.valueOf(System.Label.GP_Global_Values_Field_Count);
            
            if(gvsObject != null) {
                for(Integer i = 1; i<= fieldsCount; i++) {
                    if(gvsObject.get('GP_Value_Set_'+i+'__c') != null) {
                        valueSet += (String)gvsObject.get('GP_Value_Set_'+i+'__c');
                    }
                }
            }
            mapOfKeyValues.put(key, valueSet);
        }
        return mapOfKeyValues;
    }
    
    public static void createLWCTestData(Boolean getRelatedData) {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Person_ID__c='9582031';
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Entity_Id__c = '';
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; //objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Billing_Currency__c = 'USD';
        prjObj.GP_Project_Currency__c = 'USD';
        prjObj.GP_TCV__c = 10.00;
        prjObj.CurrencyIsoCode = 'USD';
        prjObj.GP_BPR_Approval_Required__c = false;
        prjObj.GP_PID_Approver_Finance__c = true;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        prjObj.GP_PID_Approver_User__c = objuser.id; 
        prjObj.GP_FP_A_Approver__c = loggedInUser; 
        prjObj.GP_New_SDO_s_MF_User__c = loggedInUser; 
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        prjObj.GP_Project_Short_Name__c = 'Mobile App PID';
        prjObj.Name = 'Mobile App PID';
        prjObj.GP_Project_Long_Name__c = 'Mobile App PID';
        prjObj.GP_Temp_Approval_Status__c = '{"' + loggedInUser + '":{"userId":"' + loggedInUser + '","status":"Pending For Approval","remarks":"Submitting request for PID approval"}}';
        insert prjObj;
                       
        if(getRelatedData) {
            /*GP_Project__c objChildProj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
            objChildProj.Name = 'Child Mobile App';
            objChildProj.GP_Project_Short_Name__c = 'Child Mobile App';
            objChildProj.GP_Project_Long_Name__c = 'Child Mobile App';
            objChildProj.OwnerId=objuser.Id;
            objChildProj.GP_Customer_Hierarchy_L4__c = accobj.id;
            objChildProj.GP_Operating_Unit__c = objpinnacleMaster.Id;
            objChildProj.GP_Project_type__c = 'FTE';
            objChildProj.GP_MOD__c = 'Hard Copy';
            objChildProj.GP_Business_Segment_L2__c = 'test';
            objChildProj.GP_Primary_SDO__c = objSDO.id;
            insert objChildProj;*/
            
            List<GP_Project_Address__c> listOfPAs = new List<GP_Project_Address__c>();
            listOfPAs.add(GPCommonTracker.getProjectAddress(prjObj.Id, 'Direct', null));
            //listOfPAs.add(GPCommonTracker.getProjectAddress(objChildProj.Id, 'Direct', null));
            //listOfPAs.add(GPCommonTracker.getProjectAddress(objChildProj.Id, 'Second', null));
            insert listOfPAs;
            
            List<GP_Resource_Allocation__c> listOfRAs = new List<GP_Resource_Allocation__c>();
            listOfRAs.add(GPCommonTracker.getResourceAllocation(empObj.Id, prjObj.Id));
            //listOfRAs.add(GPCommonTracker.getResourceAllocation(empObj.Id, objChildProj.Id));
            insert listOfRAs;            
            
            GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
            insert objPrjBdgtMaster;
            
            List<GP_Project_Budget__c> listOfPBs = new List<GP_Project_Budget__c>();
            listOfPBs.add(GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id));
            //listOfPBs.add(GPCommonTracker.getProjectBudget(objChildProj.Id, objPrjBdgtMaster.Id));
            insert listOfPBs;
            
            List<GP_Project_Expense__c> listOfPEs = new List<GP_Project_Expense__c>();
            listOfPEs.add(GPCommonTracker.getProjectExpense(prjObj.Id));
            //listOfPEs.add(GPCommonTracker.getProjectExpense(objChildProj.Id));
            insert listOfPEs;
            
            List<GP_Project_Work_Location_SDO__c> listOfPSs = new List<GP_Project_Work_Location_SDO__c>();
            GP_Project_Work_Location_SDO__c objProjectSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
            objProjectSDO.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO');
            listOfPSs.add(objProjectSDO);
            //GP_Project_Work_Location_SDO__c objProjectSDOC = GPCommonTracker.getProjectWorkLocationSDO(objChildProj.Id, objSdo.Id);
            //objProjectSDOC.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO');
            //listOfPSs.add(objProjectSDOC);
            insert listOfPSs;            
            
            List<GP_Project_Work_Location_SDO__c> listOfPWLs = new List<GP_Project_Work_Location_SDO__c>();
            GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
            objProjectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
            listOfPWLs.add(objProjectWorkLocation);
            //GP_Project_Work_Location_SDO__c objProjectWorkLocationC = GPCommonTracker.getProjectWorkLocationSDO(objChildProj.Id, objSdo.Id);
            //objProjectWorkLocationC.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO');
            //listOfPWLs.add(objProjectWorkLocationC);
            insert listOfPWLs;
            
            List<GP_Billing_Milestone__c> listOfBMs = new List<GP_Billing_Milestone__c>();
            listOfBMs.add(GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id));
            //listOfBMs.add(GPCommonTracker.getBillingMilestone(objChildProj.Id, objSdo.Id));
            insert listOfBMs;
            
            Id roleMasterId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
            
            GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterId, 'Cost Charging', 'Billable');
            insert leadershipMaster;
            
            List<GP_Project_Leadership__c> listOfPLs = new List<GP_Project_Leadership__c>();
            GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id, System.today(), null, empObj.Id);
            projectLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
            projectLeadership.GP_Active__c = true;
            listOfPLs.add(projectLeadership);
            
            GP_Project_Leadership__c projectLeadershipNonMK = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id, System.today(), null, empObj.Id);
            projectLeadershipNonMK.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Account Leadership');
            projectLeadershipNonMK.GP_Active__c = true;
            listOfPLs.add(projectLeadershipNonMK);
            
            //GP_Project_Leadership__c projectLeadershipC = GPCommonTracker.getProjectLeadership(objChildProj.Id, leadershipMaster.Id, System.today(), null, empObj.Id);
            //projectLeadershipC.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
            //projectLeadershipC.GP_Active__c = true;
            //listOfPLs.add(projectLeadershipC);
            
            //GP_Project_Leadership__c projectLeadershipNonMKC = GPCommonTracker.getProjectLeadership(objChildProj.Id, leadershipMaster.Id, System.today(), null, empObj.Id);
            //projectLeadershipNonMKC.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Account Leadership');
            //projectLeadershipNonMKC.GP_Active__c = true;
            //listOfPLs.add(projectLeadershipNonMKC);
            insert listOfPLs;
            
            List<GP_Profile_Bill_Rate__c> listOfPBRs = new List<GP_Profile_Bill_Rate__c>();
            GP_Profile_Bill_Rate__c profileBillRate = new GP_Profile_Bill_Rate__c();
            profileBillRate.GP_Project__c = prjObj.Id;
            profileBillRate.GP_Profile__c = 'Manager';
            profileBillRate.GP_Bill_Rate__c = 25.55;
            listOfPBRs.add(profileBillRate);
            
            //GP_Profile_Bill_Rate__c profileBillRateC = new GP_Profile_Bill_Rate__c();
            //profileBillRateC.GP_Project__c = objChildProj.Id;
            //profileBillRateC.GP_Profile__c = 'Manager';
            //profileBillRateC.GP_Bill_Rate__c = 25.55;
            //listOfPBRs.add(profileBillRateC);
            insert listOfPBRs;
            
            List<GP_Project_Document__c> listOfPODs = new List<GP_Project_Document__c>();
            listOfPODs.add(GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'PO Document'));
            //listOfPODs.add(GPCommonTracker.getProjectDocumentWithRecordType(objChildProj.Id, 'PO Document'));
            insert listOfPODs;
            
            List<GP_Project_Document__c> listOfODs = new List<GP_Project_Document__c>();
            listOfODs.add(GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'Other Document'));
            //listOfODs.add(GPCommonTracker.getProjectDocumentWithRecordType(objChildProj.Id, 'Other Document'));
            insert listOfODs;
        }
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj;
    }
    
}