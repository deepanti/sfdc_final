global class GPBatchAlertOnDispacthInfoATTNEmail implements Database.Batchable<sobject>, Database.stateful,Schedulable {
      
    String query = System.Label.GPAlertTodayModifiedPID;
    
    Set<Id> PrjIds=new Set<Id>();
    global Database.QueryLocator start(Database.BatchableContext bc) {    
    return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> listofProjectrecord) {
    
       for(GP_Project__c gpPrj : listofProjectrecord) {
           if(gpPrj.GP_Attn_To_Email__c != null && gpPrj.GP_Attn_To_Email__c.contains('@genpact.com'))
           {
               PrjIds.add(gpPrj.Id);
           }
       }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, CompletedDate, TotalJobItems
                                FROM AsyncApexJob
                                WHERE Id =: bc.getJobId()];
        if(String.isNotBlank(system.label.GP_Email_addresses_for_Resource_Expiry_Alert_Batch_Status)) {
            // Get job details.        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            String toAddresses = system.label.GP_Email_addresses_for_Resource_Expiry_Alert_Batch_Status;
            
            mail.setToAddresses(toAddresses.split(','));
            
            mail.setSubject('Alert For Dispatch Info ATTN To Email batch status : ' + aaj.Status);
            
            if(aaj.NumberOfErrors == 0) {
                mail.setPlainTextBody('Alert For Dispatch Info ATTN To Email batch has run successfully with ' +aaj.TotalJobItems+ ' total jobs on ' +aaj.CompletedDate+ '.');                
            } else {
                mail.setPlainTextBody('Alert For Dispatch Info ATTN To Email batch had ' +aaj.NumberOfErrors+ ' no of error while executing on ' +aaj.CompletedDate+ '.');
            }
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
        
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        List<GP_Project__c> lstproject=new List<GP_Project__c>();
        
        EmailTemplate eTemplate = [SELECT id, DeveloperName, body, subject, htmlvalue
                                   FROM EmailTemplate 
                                   WHERE developername = 'GP_Alert_Template_For_Dispatch_Info_ATTN_To_Email'
                                   LIMIT 1];        
        
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();
        if(PrjIds.size()>0){
            if(eTemplate != null ) {
                if(String.isNotBlank(eTemplate.HtmlValue)) {
    				lstproject = Database.query(System.Label.GPAlertOnDispacthInfoATTNEmailQuery );
                    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();                   
                    
                    String GPEmailDispatchInfo = System.Label.GP_Email_DispatchInfo;                   
                                        
                    email.setToAddresses(GPEmailDispatchInfo.split(','));
                    email.sethtmlBody(eTemplate.HtmlValue);
                    email.setSubject(eTemplate.Subject);                    
                   
                    email.sethtmlBody(email.getHtmlBody().replace('LIST_PID_RECORDS', getAlertDispatchInfo(lstproject)));
                   
                    if (oweaId != null) {
                        email.setOrgWideEmailAddressId(oweaId);
                    }
                    
                    emailNotifications.add(email);
                }
            }
        }

        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
        
    }
    
    private String getAlertDispatchInfo(List<GP_Project__c> lstproject) {
        
        String html = '';
        html += ' <table width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">';
        html += '   <thead>';
        html += '       <tr>';
        html += '           <th bgcolor="#005595" style="color:white">Sr. No</th>';
        html += '           <th bgcolor="#005595" style="color:white">PID No</th>';
        html += '           <th bgcolor="#005595" style="color:white">MOD</th>';
        html += '           <th bgcolor="#005595" style="color:white">Biz Type</th>';
        html += '           <th bgcolor="#005595" style="color:white">Attention To Email ID</th>';
        html += '           <th bgcolor="#005595" style="color:white">PID Creator OHR ID</th>';
        html += '           <th bgcolor="#005595" style="color:white">PID Creator Name</th>';
        html += '           <th bgcolor="#005595" style="color:white">PID Approver OHR ID</th>';
        html += '           <th bgcolor="#005595" style="color:white">PID Approver Name</th>';
        html += '       </tr>';
        html += '   </thead>';
        html += '   <tbody>';
        Integer i=1;
        for(GP_Project__c ra : lstproject) {
            if(ra.GP_Attn_To_Email__c != null && ra.GP_Attn_To_Email__c.contains('@genpact.com'))
            {
                
                html += '       <tr>';
                html += '           <td style="text-align:center;">' + i + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Oracle_PID__c) ? ra.GP_Oracle_PID__c : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_MOD__c) ? ra.GP_MOD__c : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Business_Type__c) ? ra.GP_Business_Type__c : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Attn_To_Email__c) ? ra.GP_Attn_To_Email__c : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Current_Working_User__r.OHR_ID__c) ? ra.GP_Current_Working_User__r.OHR_ID__c : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Current_Working_User__r.Name) ? ra.GP_Current_Working_User__r.Name : '[Not Provided]') + '</td>';
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_PID_Approver_User__r.OHR_ID__c) ? ra.GP_PID_Approver_User__r.OHR_ID__c : '[Not Provided]') + '</td>';            
                html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_PID_Approver_User__r.Name) ? ra.GP_PID_Approver_User__r.Name : '[Not Provided]') + '</td>';
               
                html += '       </tr>';
                i++;
            }
           
        }
        if(i==1)
        {
           
                html += '       <tr>';
                html += '       <td style="text-align:center;" colspan="9">' +'There is no PID with Attain To Email contain @genpact.com '+'</td>';
                html += '       </tr>';
            
        }
        html += ' </tbody></table>';
        
        return html;
    }
     public static void execute(SchedulableContext sc)
    {
        GPBatchAlertOnDispacthInfoATTNEmail obj=new GPBatchAlertOnDispacthInfoATTNEmail();
        Database.executeBatch(obj);
    }

    

}