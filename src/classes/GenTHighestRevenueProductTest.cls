@isTest(SeeAllData=true)
private class GenTHighestRevenueProductTest
{
   
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();        
        oOpportunityProduct.Product_Family_OLI__c = 'IT Managed Services';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.COE__c = 'CMIT';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'ITO';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today();
        oOpportunityProduct.ContractTerminmonths__c = 18;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity.Id;
        oOpportunityProduct.TCVLocal__c =18000;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c=true;
        oOpportunityProduct.HasSchedule__c=true;
        oOpportunityProduct.HasQuantitySchedule__c=false;
        insert oOpportunityProduct;
        //oOpportunity.IT_Service_Product__c=true;
        update oOpportunity;
    
    }

     
}