@isTest
public class GPCmpServiceProjectBdgtandPrcngTracker {
 
    public static GP_Project_Budget__c objPrjBudget;
    public static GP_Project__c prjObj;
    public static GP_Deal__c dealObj;
    public static String jsonresp;

    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        insert prjObj;

        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;

        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;

    }

    @isTest static void testGPCmpServiceProjectandPricingForOMS() {
        fetchData();
        testgetProjectBudgetData();
        testgetProjectBudgetData();
        testsaveProject();
        testdeleteProjectbudgetData();
        testgetOMSProjectBudgetData();
        testgetOMSProjectBudgetDataforCatch();
        testdeleteProjectBudgetRecordforCatch();
        testgetOMSBudgetRecordforCatch();
    }

    @isTest static void testGPCmpServiceProjectandPricing() {
        fetchData();
        dealObj.GP_No_Pricing_in_OMS__c = true;
        update dealObj;
        testgetProjectBudgetData();
        testgetProjectBudgetData();
        testsaveProject();
        testdeleteProjectbudgetData();
        testgetOMSProjectBudgetData();
        testgetOMSProjectBudgetDataforCatch();
        testdeleteProjectBudgetRecordforCatch();
        testgetOMSBudgetRecordforCatch();
    }

    public static void fetchData() {
        objPrjBudget = [select id, Name, GP_Project__c, GP_Budget_Master__c,
            GP_Band__c, GP_Country__c, GP_Effort_Hours__c, GP_Product__c from GP_Project_Budget__c limit 1
        ];
        prjObj = [select id, Name from GP_Project__c limit 1];
        jsonresp = (String) JSON.serialize(new List < GP_Project_Budget__c > { objPrjBudget });
        dealObj = [select id,GP_No_Pricing_in_OMS__c from GP_Deal__c LIMIT 1];
    }

    public static void testgetProjectBudgetData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBudgetandPricing.getProjectBudgetData(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testsaveProject() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBudgetandPricing.saveProjectbudgetData(jsonresp,null);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteProjectbudgetData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBudgetandPricing.deleteProjectbudgetData(objPrjBudget.id);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetOMSProjectBudgetData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBudgetandPricing.getOMSProjectBudgetData(prjObj.id);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetOMSProjectBudgetDataforCatch() {
        GPControllerProjectBudgetandPricing ProjectBudgetandPricing = new GPControllerProjectBudgetandPricing();
        GPAuraResponse returnedResponse = ProjectBudgetandPricing.getBudgetRecord(); 
        System.assertEquals(false, returnedResponse.isSuccess);
    }

    public static void testdeleteProjectBudgetRecordforCatch() {
        GPControllerProjectBudgetandPricing ProjectBudgetandPricing = new GPControllerProjectBudgetandPricing();
        GPAuraResponse returnedResponse = ProjectBudgetandPricing.deleteProjectBudgetRecord(prjObj.id);
        System.assertEquals(false, returnedResponse.isSuccess);
    }

    public static void testgetOMSBudgetRecordforCatch() {
        GPControllerProjectBudgetandPricing ProjectBudgetandPricing = new GPControllerProjectBudgetandPricing();
        GPAuraResponse returnedResponse = ProjectBudgetandPricing.getOMSBudgetRecord();
        System.assertEquals(false, returnedResponse.isSuccess);
    }
}