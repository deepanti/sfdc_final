public class GPCmpServiceProjectDelete {
	@AuraEnabled
    public static GPAuraResponse deleteProjectRecord(Id projectId) {
        GPControllerProjectDelete objGPControllerProjectDelete = new GPControllerProjectDelete(projectId);
        return objGPControllerProjectDelete.deleteProject();
    }
}