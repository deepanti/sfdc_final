public with sharing class GPLWCServicePIDApproverHomePage {    
    @AuraEnabled
    public static GPAuraResponse getAllPendingForApprovalPIDsOfUser() {
        GPLWCControllerPIDApproverHomePage glcpahp = new GPLWCControllerPIDApproverHomePage();
        return glcpahp.getAllPendingForApprovalPIDsOfUser();
    }
}