public class GPDomainProjectWorkLocationSDO extends fflib_SObjectDomain {
    
    public GPDomainProjectWorkLocationSDO(List<GP_Project_Work_Location_SDO__c > sObjectList) {
        super(sObjectList);
    }
    
    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List<SObject> sObjectList) {
            return new GPDomainProjectWorkLocationSDO(sObjectList);
        }
    }
    
    // SObject's used by the logic in this service, listed in dependency order
    private static List<Schema.SObjectType> DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Project_Work_Location_SDO__c.SObjectType
            };
                
    public override void onBeforeInsert(){
         fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
          applyDefaultsOnInsertUpdate(null);
          
        //validateProjectWorkLocationSDORecordAccess(uow, null);
        //  validateProjectworkLOcationSDO(uow,null);
        // uow.commitWork();
    }
    
    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Commented as Task No : GP-725 validateForSameLECodeforBPM();
        // Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, null);
        //uow.commitWork();
    } 
    public override void onAfterDelete() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        // Pankaj 2/4/2018
        //updateAdditionalApproverFieldOndelete();
        //uow.commitWork();
    }
    
    public override void onBeforeUpdate(Map<Id, SObject> oldSObjectMap) {
         //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //validateProjectworkLOcationSDO(oldSObjectMap);
        validateonChangeorDeleteofWorkLocationMaster(oldSObjectMap);
        applyDefaultsOnInsertUpdate(oldSObjectMap);
        setIsUpdateCheckBox();
        setEffectiveEndDateForInActiveWorkLocations(oldSObjectMap);
        //validateProjectWorkLocationSDORecordAccess(uow, oldSObjectMap);
    }
    
    public override void onAfterUpdate(Map<Id, SObject> oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Commented as Task No : GP-725 validateForSameLECodeforBPM();
        // Pankaj 2/4/2018
        // updatePIDApproverFinance(uow, oldSObjectMap);
        // Pankaj 2/4/2018
        //updateAdditionalApproversFieldOnProject(uow, oldSObjectMap);
        
        //uow.commitWork();
    }
    
    public override void onBeforeDelete() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS); 
        validateonChangeorDeleteofWorkLocationMaster(null);
        uow.commitWork();
    }
    
    public override void onValidate() {
        // Validate GP_Project_Work_Location_SDO__c 
        // for(GP_Project_Work_Location_SDO__c  ObjProject: (List<GP_Project_Work_Location_SDO__c >) Records) {}
    }
    
    public override void onApplyDefaults() {}
    
    
    public void applyDefaultsOnInsertUpdate(Map<Id, SOBject> oldSObjectMap){
         for(GP_Project_Work_Location_SDO__c  objPWL: (List<GP_Project_Work_Location_SDO__c >)records) {
            objPWL.GP_BCP_FLAG__c = objPWL.GP_BCP__c == 'Yes'? true:false;
            
            if(oldSObjectMap == null && objPWL.GP_Project_Oracle_PID__c != 'NA' && objPWL.GP_Parent_Project_Work_Location_SDO__c == null){
                objPWL.GP_isUpdated__c = true;
            }
         }
    }
    //Commented as Task No : GP-725
    //public void validateForSameLECodeforBPM(){
    //     id recordTypIdforWL = gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
         
    //     System.System.debug('records >>> ' + records);
    //     set<id> setProjectId = new set<id>();
    //     for(GP_Project_Work_Location_SDO__c  objPWL: (List<GP_Project_Work_Location_SDO__c >)records) {
    //        if(objPWL.GP_Project_Record_Type__c == 'BPM' )
    //        {
    //            setProjectId.add(objPWL.GP_Project__c);
    //        }
    //     }
         
    //     list<GP_Project_Work_Location_SDO__c> lstProkectWorkLocations = GPSelectorProjectWorkLocationSDO.selectProjectWorkLocationforProjectIds(setProjectId);
    //     map<id, list<GP_Project_Work_Location_SDO__c>> mapProjectIdtoPWLList = new map<id, list<GP_Project_Work_Location_SDO__c>>();
         
    //     if(lstProkectWorkLocations != null && lstProkectWorkLocations.size() > 0){
    //         for(GP_Project_Work_Location_SDO__c objExistingPWL : lstProkectWorkLocations)
    //         {
    //            if(mapProjectIdtoPWLList != null && mapProjectIdtoPWLList.get(objExistingPWL.GP_Project__c) == null){
    //                mapProjectIdtoPWLList.put(objExistingPWL.GP_Project__c, new list<GP_Project_Work_Location_SDO__c>());
    //            }
    //            mapProjectIdtoPWLList.get(objExistingPWL.GP_Project__c).add(objExistingPWL);
    //         }
    //     }
         
    //     if(mapProjectIdtoPWLList.size() > 0){
    //        for(id projectId : mapProjectIdtoPWLList.keyset())
    //        {
    //            set<string> setWLLECode = new set<string>();
    //            for(GP_Project_Work_Location_SDO__c  objPWL: mapProjectIdtoPWLList.get(projectId))
    //            {
    //                if(objPWL.GP_Project_Record_Type__c == 'BPM' )
    //                {
    //                    if(objPWL.RecordTypeId == recordTypIdforWL)
    //                    {
    //                        setWLLECode.add(objPWL.GP_Work_Location_LE_Code__c);
    //                    }
    //                }
    //             }
    //             if(setWLLECode.size() > 1 ){
    //                 for(GP_Project_Work_Location_SDO__c objPWL : (list<GP_Project_Work_Location_SDO__c>) records) 
    //                {
    //                    objPWL.adderror('Work Locations for BPM projects should be of same LE code.');
    //                    break;
    //                }
    //            }
    //        }
    //     }
    //}
    
    
    
    /*
    @Author: Amit Prajapati
    @Description: validate for resource allocation before changing / deleting work location
    */
    public void validateonChangeorDeleteofWorkLocationMaster(map<id, Sobject> oldSObjectMap) 
    {
        GP_Project_Work_Location_SDO__c objOldProj;
        set<string> setProjectandWorkLocationMasterKey = new set<string>();
        set<Id> setProjectIds = new set<Id>();
        for(GP_Project_Work_Location_SDO__c objPWL : (list<GP_Project_Work_Location_SDO__c>) records) 
        {
            system.debug('objOldProj______ '+objOldProj);
            system.debug('objPWL_NEW______ '+objPWL);
            if(trigger.isupdate){
                objOldProj = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(objPWL.id);
                // check for Work Location Change
                if(objPWL.GP_Work_Location__c != null && oldSObjectMap != null && objPWL.GP_Work_Location__c != objOldProj.GP_Work_Location__c)
                {
                    setProjectandWorkLocationMasterKey.add(objPWL.GP_Project__c + '@@' + objOldProj.GP_Work_Location__c);
                    setProjectIds.add(objPWL.GP_Project__c);
                }
            }
            // check for project work Location delete
            if(trigger.isDelete)
            {
                setProjectandWorkLocationMasterKey.add(objPWL.GP_Project__c + '@@' + objPWL.GP_Work_Location__c);
                setProjectIds.add(objPWL.GP_Project__c);
            }
        }
        
        
        if(setProjectIds.size() > 0)
        {
            list<GP_Resource_Allocation__c> lstResourceAllocation = new GPSelectorResourceAllocation()
                                                                        .selectResourceAllocationRecordsOfProjectandWorkLocation(setProjectIds, 
                                                                                                                                setProjectandWorkLocationMasterKey);
            
            // If any resource Allocatin found with such projectId@@WorkLocationId combination then show error
            if(lstResourceAllocation.size() > 0){
                for(GP_Project_Work_Location_SDO__c objPWL : (list<GP_Project_Work_Location_SDO__c>) records) 
                {
                    objPWL.adderror('There are resource allocations for work location or Lending SDO being deleted. '+
                                    ' Please update the resource allocation first and then delete the work location.');
                    break;
                }
            }
        }
        
    }
    
    //Method was not called 08052018
    //Description: The method id used to validate if the work location is change or deleted
    //when the resource allocation is added to that particular resource allocation.
    
    //public void validateProjectworkLOcationSDO(Map<Id, SOBject> oldSObjectMap) {
    //    GP_Project_Work_Location_SDO__c objOldProj;
    //    set<ID> setOfWorkLocationSDOId = new set<Id>();
        
    //    for(GP_Project_Work_Location_SDO__c prjWorkLocation : (list<GP_Project_Work_Location_SDO__c>) records)
    //    {
    //        objOldProj = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(prjWorkLocation.id);
    //        if(prjWorkLocation.GP_Project_Record_Type__c != null && 
    //          prjWorkLocation.GP_Project_Record_Type__c.touppercase() != 'CMITS' && 
    //           prjWorkLocation.GP_Project_Record_Type__c.touppercase() != 'OTHER_PBB' &&
    //           prjWorkLocation.GP_Work_Location__c != objOldProj.GP_Work_Location__c &&
    //           objOldProj.GP_Work_Location__c != null)
    //        {
    //            setOfWorkLocationSDOId.add(objOldProj.GP_Work_Location__c);
    //        }
    //    }
        
    //    if(setOfWorkLocationSDOId!= null && setOfWorkLocationSDOId.size() >0)
    //    {
    //        GPSelectorWorkLocationSDOMaster selector = new GPSelectorWorkLocationSDOMaster();
    //        map<string,GP_Work_Location__c> mapofworkLocation = selector.selectResourcFromWorkLocation(setOfWorkLocationSDOId);
            
    //        if(mapofworkLocation!= null && mapofworkLocation.size()>0)
    //        {
    //            for(GP_Project_Work_Location_SDO__c prjWorkLocation : (list<GP_Project_Work_Location_SDO__c>) records)
    //            {
    //                objOldProj = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(prjWorkLocation.id);
                    
    //                if(prjWorkLocation.GP_Project_Record_Type__c != null && 
    //                  prjWorkLocation.GP_Project_Record_Type__c.touppercase() != 'CMITS' && 
    //                   prjWorkLocation.GP_Project_Record_Type__c.touppercase() != 'OTHER_PBB' &&
    //                   prjWorkLocation.GP_Work_Location__c != objOldProj.GP_Work_Location__c &&
    //                   objOldProj.GP_Work_Location__c != null) 
    //                {
    //                    if(mapofworkLocation.containsKey(objOldProj.GP_Work_Location__c) && 
    //                       mapofworkLocation.get(objOldProj.GP_Work_Location__c).Resource_Allocations__r.size() > 0) 
    //                    {
    //                        prjWorkLocation.addError('This worklocation is mapped to resource allocation.');
                            
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
    
    //Method was not called 08052018
    //public void validateProjectWorkLocationSDORecordAccess(fflib_SObjectUnitOfWork uow,Map<Id, SOBject> oldSObjectMap)
    //{
    //    Set<ID> setOfParentProjectID = new Set<ID>();
    //    Map<ID,boolean> mapofIDAndBoolean = new Map<ID,boolean>();
    //    for(GP_Project_Work_Location_SDO__c projectWorkLocationSDO :  (List<GP_Project_Work_Location_SDO__c>) records)
    //    {
    //        setOfParentProjectID.add(projectWorkLocationSDO.GP_Project__c);
    //    }
    //    if(setOfParentProjectID != null && setOfParentProjectID.size()>0)
    //    {
    //        mapofIDAndBoolean = GPServiceProject.validateProjectandChildRecordAccess(setOfParentProjectID);   
    //    }
        
    //    for(GP_Project_Work_Location_SDO__c projectWorkLocationSDO :  (List<GP_Project_Work_Location_SDO__c>) records)
    //    {
    //        if(mapofIDAndBoolean.get(projectWorkLocationSDO.GP_Project__c)==false)
    //        {
    //            projectWorkLocationSDO.addError('You are not a current working user so you are not authorized to work on Project WorklocationSDO.');
    //        }
    //    }
    //}
    
    public void setEffectiveEndDateForInActiveWorkLocations(Map<Id, SObject> oldSObjectMap) {
        Set<Id> setOfWorkLocationId = new Set<Id>();
        List<GP_Project_Work_Location_SDO__c> listOfProjectWorkLocation = (List<GP_Project_Work_Location_SDO__c>)records;
        
        Id projectId = (Id) listOfProjectWorkLocation.get(0).get('GP_Project__c');
        
        GP_Project__c project = [Select Id, GP_EP_Project_Number__c FROM GP_Project__c where Id =: projectId];
        Map<Id, Date> mapOfWorkLocationIdToMaxDate = new Map<Id, Date>();
        Map<String, Id> mapOfEmployeeOHRIdToWorkLocationId = new Map<String, Id>();
        Set<String> setOfOHRId = new Set<String>();
        
        
        for(GP_Resource_Allocation__c resourceAllocation : [SELECT GP_Work_Location__c ,
                                                            GP_Employee__r.GP_Final_OHR__c  
                                                            FROM GP_Resource_Allocation__c
                                                            WHERE GP_Project__c = :projectId]) 
        {
            setOfOHRId.add(resourceAllocation.GP_Employee__r.GP_Final_OHR__c);
            mapOfEmployeeOHRIdToWorkLocationId.put(resourceAllocation.GP_Employee__r.GP_Final_OHR__c, resourceAllocation.GP_Work_Location__c);
        }
        
        for(GP_Timesheet_Max_Date__c timesheetMaxDate : [
            SELECT Id, GP_Employee_Number__c, GP_Person_Id__c, GP_Project_Number__c, GP_Max_Exp_Date__c 
            FROM GP_Timesheet_Max_Date__c
            WHERE GP_Project_Number__c = :project.GP_EP_Project_Number__c
            AND GP_Employee_Number__c in :setOfOHRId
        ])
        {
            Id workLocationId = mapOfEmployeeOHRIdToWorkLocationId.get(timesheetMaxDate.GP_Employee_Number__c);
            Date maxDate = mapOfWorkLocationIdToMaxDate.containsKey(workLocationId) && 
                mapOfWorkLocationIdToMaxDate.get(workLocationId) > timesheetMaxDate.GP_Max_Exp_Date__c ? 
                mapOfWorkLocationIdToMaxDate.get(workLocationId) : timesheetMaxDate.GP_Max_Exp_Date__c;
            
            mapOfWorkLocationIdToMaxDate.put(workLocationId, maxDate);
        }
        
        for(GP_Project_Work_Location_SDO__c  projectWorkLocation : (List<GP_Project_Work_Location_SDO__c>)records) {
            GP_Project_Work_Location_SDO__c  oldProjectWorkLocation = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(projectWorkLocation.Id);
            
            if(!oldProjectWorkLocation.GP_is_Active__c && projectWorkLocation.GP_is_Active__c) {
                if(projectWorkLocation.GP_Project_Record_Type__c == 'BPM') {
                    projectWorkLocation.GP_Effective_End_Date__c = System.today();
                } else {
                    Date nextFriday = getNextFriday();
                    Date timesheetMaxDate = mapOfWorkLocationIdToMaxDate.get(projectWorkLocation.GP_Work_Location__c);
                    Date maxDate = timesheetMaxDate != null && timesheetMaxDate > nextFriday ? timesheetMaxDate : nextFriday;
                    
                    projectWorkLocation.GP_Effective_End_Date__c = maxDate;                    
                }
            } else {
                projectWorkLocation.GP_Effective_End_Date__c = null;
            }
        }
    }
    
    private Date getNextFriday() {        
        Date today = System.today();
        DateTime todayTime = (DateTime) today;
        Integer dayOfWeek = Integer.valueof(todayTime.format('u'));
        Integer daysToAdd = 0;
        
        if(dayOfWeek < 5) {
            daysToAdd = 5 - dayOfWeek;
        } else if(dayOfWeek > 5) {
            daysToAdd = 7 - (dayOfWeek - 5);
        }
        
        return today.addDays(daysToAdd);
    }
    
    public void setIsUpdateCheckBox()
    {
        List<String> lstOfFieldAPINames = new List<String>();
        for(Schema.FieldSetMember fields :Schema.SObjectType.GP_Project_Work_Location_SDO__c.fieldSets.getMap().get('GP_PID_Update_Fields').getFields()){
            lstOfFieldAPINames.add(fields.getFieldPath());
        }
        Set<Id> setOfParentRecords = new Set<Id>();
        for(GP_Project_Work_Location_SDO__c projectWorkLocationSDO :  (List<GP_Project_Work_Location_SDO__c>) records)
        {
            if(projectWorkLocationSDO.GP_Parent_Project_Work_Location_SDO__c != null)
                setOfParentRecords.add(projectWorkLocationSDO.GP_Parent_Project_Work_Location_SDO__c);
        }
        if(setOfParentRecords.size() > 0){
            map<Id,GP_Project_Work_Location_SDO__c> mapOfParentRecords = new map<Id,GP_Project_Work_Location_SDO__c>();
            for(GP_Project_Work_Location_SDO__c projectWL : new GPSelectorProjectWorkLocationSDO().selectProjectWorkLocationAndSDORecords(setOfParentRecords)){
                mapOfParentRecords.put(projectWL.Id,projectWL);
            }
            GPCommon.setIsUpdatedField(records,mapOfParentRecords,lstOfFieldAPINames,'GP_Parent_Project_Work_Location_SDO__c');
            system.debug('records'+records);
        }
    }
    
    
    
    
    /*public void updateAdditionalApproverFieldOnDelete() {
        Id SDORecordTypeId  = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        Set<Id> setOfWorkLocationMasterId = new Set<Id>();
        Set<Id> setOfProjectRecordIds = new Set<Id>();
         for(GP_Project_Work_Location_SDO__c objProjectWorkLocation : (List<GP_Project_Work_Location_SDO__c >)records) {
             //GP_Project_Work_Location_SDO__c objProjectWorkLocation = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(projectWorkLocationId);
            if(SDORecordTypeId == objProjectWorkLocation.RecordTypeId && objProjectWorkLocation.GP_Work_Location__c != null){
                setOfProjectRecordIds.add(objProjectWorkLocation.GP_Project__c);
              }
        }
        if(setOfProjectRecordIds.size() > 0){
            List<GP_Project__c> lstOfProjectRecords = new List<GP_Project__c>();
            Set<Id> setOfProjectsIncluded = new Set<Id>();
            for(GP_Project__c objProject : new GPSelectorProject().getProjectRecordsWithWorkLocation(setOfProjectRecordIds)){
                if(objProject.GP_Project_Work_Location_SDOs__r.size() > 0) 
                {
                    objProject.GP_Additional_Approval_Required__c = false;
                    for(GP_Project_Work_Location_SDO__c objProjectWorkLocation : objProject.GP_Project_Work_Location_SDOs__r){
                        if(objProjectWorkLocation.GP_Work_Location__c != null && objProjectWorkLocation.GP_Work_Location__r.GP_Country__c == 'China')
                        {
                           objProject.GP_Additional_Approval_Required__c = true; 
                        }
                    }
                   lstOfProjectRecords.add(objProject); 
                }
            }
            
            if(lstOfProjectRecords.size() > 0)
                update lstOfProjectRecords;
        }
    }*/
    
   /* public void updateAdditionalApproversFieldOnProject(fflib_SObjectUnitOfWork uow, Map<Id, SOBject> oldSObjectMap) {
        Id SDORecordTypeId  = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        Set<Id> setOfWorkLocationMasterId = new Set<Id>();
        for(GP_Project_Work_Location_SDO__c  objProjectWorkLocation: (List<GP_Project_Work_Location_SDO__c >)records) {
            if(SDORecordTypeId == objProjectWorkLocation.RecordTypeId && objProjectWorkLocation.GP_Work_Location__c != null && 
               (oldSObjectMap != null && oldSObjectMap.get(objProjectWorkLocation.Id).get('GP_Work_Location__c') != objProjectWorkLocation.GP_Work_Location__c)
               ||(oldSObjectMap == null)){
                   setOfWorkLocationMasterId.add(objProjectWorkLocation.GP_Work_Location__c);
               }
        }
        if(setOfWorkLocationMasterId.size() > 0){
            map<Id,GP_Work_Location__c> mapOfWorkLocation = new GPSelectorWorkLocationSDOMaster().selectByRecordId(setOfWorkLocationMasterId);
            List<GP_Project__c> lstOfProjectRecords = new List<GP_Project__c>();
            Set<Id> setOfProjectsIncluded = new Set<Id>();
            for(GP_Project_Work_Location_SDO__c  objProjectWorkLocation: (List<GP_Project_Work_Location_SDO__c >)records) {
                if(SDORecordTypeId == objProjectWorkLocation.RecordTypeId && objProjectWorkLocation.GP_Work_Location__c != null && 
                   (oldSObjectMap != null && oldSObjectMap.get(objProjectWorkLocation.Id).get('GP_Work_Location__c') != objProjectWorkLocation.GP_Work_Location__c)
                   ||(oldSObjectMap == null)){
                       if(!setOfProjectsIncluded.contains(objProjectWorkLocation.GP_Project__c) &&
                          mapOfWorkLocation.get(objProjectWorkLocation.GP_Work_Location__c).GP_Country__c == 'China'){
                              GP_Project__c objProject = new GP_Project__c(id = objProjectWorkLocation.GP_Project__c);
                              objProject.GP_Additional_Approval_Required__c = true;
                              lstOfProjectRecords.add(objProject);
                          }
                   }
            }
            if(lstOfProjectRecords.size() > 0)
                update lstOfProjectRecords;
        }
    } */
    
   /* public void updatePIDApproverFinance(fflib_SObjectUnitOfWork uow, Map<Id, SOBject> oldSObjectMap) {
        
        Set<String> fieldSet = new Set<String>();
        GP_Project_Work_Location_SDO__c oldProjWorkLocation;
        Map<ID,GP_Project__c > mapofIdToobjProject = new Map<Id,GP_Project__c>();
        
        //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
        for(Schema.FieldSetMember fields: Schema.SObjectType.GP_Project_Work_Location_SDO__c.fieldSets.getMap().get('GP_PID_Approver_Finance').getFields()){
            fieldSet.add(fields.getFieldPath());
        }
        
        if(fieldSet == null || fieldSet.size() == 0) {
            return;   
        }
        
        for(GP_Project_Work_Location_SDO__c  objProjectWorkLocation: (List<GP_Project_Work_Location_SDO__c >)records) { 
            
            mapofIdToobjProject.put(objProjectWorkLocation.GP_Project__c, new GP_Project__c(id=objProjectWorkLocation.GP_Project__c, GP_PID_Approver_Finance__c = false));
            
            if(mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_PID_Approver_Finance__c != true) {
                for(string eachField: fieldSet){
                    if(oldSObjectMap != null) {
                        oldProjWorkLocation = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(objProjectWorkLocation.Id);
                        if(objProjectWorkLocation.get(eachField) != oldProjWorkLocation.get(eachField)) {
                            mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_PID_Approver_Finance__c = true;
                        }   
                    } else {
                        mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_PID_Approver_Finance__c = true; 
                    }
                }
                
                // if BCP is changed from 'No' to 'Yes' then FP & A Approver and PID Approver is required
                if(oldSObjectMap != null) {
                    oldProjWorkLocation = (GP_Project_Work_Location_SDO__c)oldSObjectMap.get(objProjectWorkLocation.Id);
                    if(oldProjWorkLocation.get('GP_BCP__c') == 'No' &&
                        objProjectWorkLocation.get('GP_BCP__c') == 'Yes') {
                        mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_PID_Approver_Finance__c = true;
                        mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_FP_And_A_Approval_Required__c = true;
                    }   
                } else {
                    mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_PID_Approver_Finance__c = true; 
                    mapofIdToobjProject.get(objProjectWorkLocation.GP_Project__c).GP_FP_And_A_Approval_Required__c = true;
                }
            }
        }
        
        
        if(mapofIdToobjProject == null || mapofIdToobjProject.size() == 0) {
            return;   
        }
        
        list<GP_Project__c> lstProjecttoUpdate = new list<GP_Project__c>();
        
        for(GP_Project__c objPrj : mapofIdToobjProject.values()) {
            if(objPrj.GP_PID_Approver_Finance__c == true) {
                lstProjecttoUpdate.add(objPrj);
            }
        }
        
        if(lstProjecttoUpdate !=null && lstProjecttoUpdate.size() > 0) {
            uow.registerDirty(lstProjecttoUpdate);
        }
        
    } */
     
    
}