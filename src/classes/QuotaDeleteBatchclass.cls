/*-------------
        Class Description : This Class is a batch class for delete quota attainment purposes.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 11 June 2014
        Last modified date: 3 September 2014
---------------*/
global class QuotaDeleteBatchclass implements Database.Batchable<sObject>
{
    global final String Query;
    //To maintain Quota Year
    String Quota_Year = String.valueof(System.today().year());   
     
    // Constructor    
   global QuotaDeleteBatchclass (String q)
   {   
        //Query=q;
        String StrQuery='select id from QuotaLineOpportunityJunction__c where Quota_header__r.year__c=\''+Quota_Year+'\'';
        system.debug('QuotaDeleteBatchclass StrQuery===='+StrQuery);
        Query=StrQuery;
   }
    
   // Process Launching method.This method gets executed only once at beginning of Batch Process    
   global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
   {    
      return Database.getQueryLocator(query);
   }

   // method to execute logic for each Batch 
   global void execute(Database.BatchableContext BC, List<sObject> scope)           // Execute Logic    
   { 
        system.debug('inside execute method=='+scope.size());
        List<QuotaLineOpportunityJunction__c> del_reclist = new List<QuotaLineOpportunityJunction__c>();
        for(sobject quotalinej : scope)
        {
            system.debug('quotalinej=='+quotalinej);
            QuotaLineOpportunityJunction__c quotalinejobj=(QuotaLineOpportunityJunction__c)quotalinej;
            system.debug('oppobject.QuotaLineOpportunityJunction__c=='+quotalinejobj);    
            del_reclist.add(quotalinejobj);     
         }  
         delete del_reclist;
   }
    // Logic to be Executed at finish
   global void finish(Database.BatchableContext BC)
   {
        QuotaBatchclass reassign = new QuotaBatchclass('');        
       
        ID batchprocessid = Database.executeBatch(reassign,1);   // running a batch with batch size 1
   } 
}