public class GPSelectorWorkLocationSDOMaster extends fflib_SObjectSelector {
    
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Work_Location__c.Name,
                GP_Work_Location__c.GP_Oracle_Id__c
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Work_Location__c.sObjectType;
    }
    
    public List<GP_Work_Location__c> selectById(Set<ID> idSet) {
        return (List<GP_Work_Location__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Work_Location__c> selectByIdofWOrkLocationAndSDO(Set<ID> idSet) {
        return [select id, GP_Status__c,GP_Start_Date_Active__c,GP_End_Date_Active__c, 
                (select id, GP_Is_Closed__c,GP_Start_Date__c,GP_End_Date__c from Projects__r) 
                from GP_Work_Location__c where id IN:idSet];
    }
    
    public List<GP_Work_Location__c> selectByOracleIdofWOrkLocationAndSDO(Set<String> setOfOracleIds) {
        return [select id, GP_Status__c,GP_Start_Date_Active__c,GP_End_Date_Active__c,Name,GP_Oracle_Id__c ,GP_Lending_SDO_Code__c
                from GP_Work_Location__c where GP_Oracle_Id__c IN :setOfOracleIds ];
    }
    
    public map<string, GP_Work_Location__c> selectResourcFromWorkLocation(set<Id> setOfId)
    {
        map<string, GP_Work_Location__c> mapofRecords = new map<string, GP_Work_Location__c> ([select id,
                                                                                               (select Id,name 
                                                                                                from Resource_Allocations__r)
                                                                                               from GP_Work_Location__c 
                                                                                               where Id in:setOfId]);
        return mapofRecords;
        
    }
    
    public map<string, GP_Work_Location__c> selectMapOfWorkLocationToResource(set<Id> setOfId)
    {
        map<string, GP_Work_Location__c> mapofRecords = new map<string, GP_Work_Location__c> ([select id,
                                                                                               (select Id,name 
                                                                                                from Resource_Allocations__r)
                                                                                               from GP_Work_Location__c 
                                                                                               where Id in:setOfId]);
        return mapofRecords;
        
    }
    
    public Static List<GP_Work_Location__c> selectWOrkLocationAndSDOByOracleID(Set<String> setofSDOoracleID) {
        Id SDORecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        return [select id, GP_Oracle_Id__c
                from GP_Work_Location__c 
                where RecordTypeID =: SDORecordTypeId and 
                GP_Oracle_Id__c IN:setofSDOoracleID];
    }
     public Static List<GP_Work_Location__c> selectWOrkLocationByOracleID(Set<String> setofWLoracleID) {
        Id WLRecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        return [select id, GP_Oracle_Id__c,GP_Description__c
                from GP_Work_Location__c 
                where RecordTypeID =: WLRecordTypeId and 
                GP_Oracle_Id__c IN:setofWLoracleID];
    }
    public static List<GP_Work_Location__c> getAddToOppProjectWorkLocations() {
        Id sdoRecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        Id workLocationRecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        
        return [SELECT Id, GP_Oracle_Id__c,
                RecordType.Name FROM GP_Work_Location__c
                WHERE GP_Add_To_Opp_Project__c = true
                AND (RecordTypeID =: sdoRecordTypeId
                     Or RecordTypeID =: workLocationRecordTypeId)];
    }
    public Map<Id,GP_Work_Location__c> selectByRecordId(Set<Id> setOfIds){
        return new Map<Id,GP_Work_Location__c>([Select Id, CurrencyIsoCode,GP_COE_Code__c,GP_Manager_Finance_Employee__c,
                                                GP_Manager_Finance_Employee__r.GP_SFDC_User__c,GP_Country__c
                                                FROM GP_Work_Location__c
                                                WHERE Id in :setOfIds]);
    }


    public static GP_Work_Location__c selectSODById(Id sodId){
        return [SELECT Id, GP_Business_Name__c, GP_Business_Type__c FROM GP_Work_Location__c
                WHERE Id = :sodId];
    }

    public static List<GP_Work_Location__c> selectGlobalSDO() {
        Date today = System.today();
        Id SDORecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        
        list<GP_Work_Location__c> lstOfGlobalSDO = [SELECT Id, 
                                                    Name, 
                                                    GP_Description__c,
                                                    GP_Legacy_Expiration_Date__c, 
                                                    GP_Unique_Name__c,
                                                    GP_Oracle_Id__c,
                                                    GP_Business_Type__c
                                                    FROM GP_Work_Location__c
                                                    where RecordTypeID =: SDORecordTypeId 
                                                    and(GP_Legacy_Expiration_Date__c = null 
                                                    OR GP_Legacy_Expiration_Date__c >= today)
                                                    and GP_Status__c = 'Active and Visible'
                                                   ];  
        return lstOfGlobalSDO;
                                     
    }
}