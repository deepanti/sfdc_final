/**
* @author Anmol.kumar
* @date 22/10/2017
*
* @group ProjectDetails
* @group-content ../../ApexDocContent/ProjectDetails.html
*
* @description Apex Service for project Detail module,
* has aura enabled method to CRUD project Detail data.
*/
public class GPCmpServiceProjectDetail {

    private static final String SUCCESS_LABEL = 'SUCCESS';
    private static final String NONE_LABEL = '--None--';
    
    /**
    * @description Returns All the record types available under a project.
    * @param sObjectName Salesforce API Name
    * 
    * @return GPAuraResponse json of Project leadership data
    * 
    * @example
    * GPCmpServiceProjectDetail.getRecordType(<API name of SFDC Object>);
    */
    @AuraEnabled
    public static GPAuraResponse getRecordType(String sObjectName) {        
        return GPControllerProjectDetail.getRecordType(sObjectName);
    }

    /**
    * @description Saves a given project record after validation
    * @param strProjectRecord Serialized Project record to be saved
    * 
    * @return GPAuraResponse json of Flag wether project record is saved successfully
    * 
    * @example
    * GPCmpServiceProjectDetail.saveProject(<Serialized Project record to be saved>);
    */
    @AuraEnabled
    public static GPAuraResponse saveProject(String strProjectRecord) {
        return GPControllerProjectDetail.saveProject(strProjectRecord);
    }
    
    /**
    * @description Saves a given legacy project record and its deal
    * after validation
    * @param strProjectRecord Serialized Project record to be saved
    * 
    * @return GPAuraResponse json of Flag wether project record is saved successfully
    * 
    * @example
    * GPCmpServiceProjectDetail.saveLegacyProject(<Serialized Project record to be saved>, <Serialized Deal record to be saved>);
    */
    @AuraEnabled
    public static GPAuraResponse saveLegacyProject(String strProjectRecord, String strDealRecord) {
        return GPControllerProjectDetail.saveLegacyProject(strProjectRecord, strDealRecord);
    }

    /**
    * @description Returns project template record
    * @param projectTemplateId SFDC 18/15 digit Project template Id
    * 
    * @return GPAuraResponse Serialized project template record
    * 
    * @example
    * GPCmpServiceProjectDetail.getProjectTemplate(<SFDC 18/15 digit Project template Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectTemplate(String sdoId, String businessGroupL1, String businessSegmentL2, Boolean isIndirect, Boolean isWithoutSFDC, Boolean isInterCOEProject) {
        return GPControllerProjectDetail.getFinalProjectTemplate(sdoId, businessGroupL1, businessSegmentL2, isIndirect, isWithoutSFDC, isInterCOEProject);
    }
    
    /**
    * @description Returns project record
    * @param projectId SFDC 18/15 digit Project Id
    * 
    * @return GPAuraResponse Serialized project record
    * 
    * @example
    * GPCmpServiceProjectDetail.getProjectData(<SFDC 18/15 digit Project Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectData(Id projectId) {
        return GPControllerProjectTemplate.getProjectWithTemplate(projectId);
    }
    
    /**
    * @description Returns Deal record
    * @param dealId SFDC 18/15 digit Deal Id
    * 
    * @return GPAuraResponse Serialized project record
    * 
    * @example
    * GPCmpServiceProjectDetail.getDefaultProjectData(<SFDC 18/15 digit Deal Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getDefaultProjectData(Id dealId) {
        return GPControllerProjectDetail.getDefaultProjectData(dealId);
    }
    
    /**
    * @description Returns Account record
    * @param accountID SFDC 18/15 digit Account Id
    * 
    * @return GPAuraResponse Serialized Account record
    * 
    * @example
    * GPCmpServiceProjectDetail.getAccountRecord(<SFDC 18/15 digit Account Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getAccountRecord(Id accountID) {
        return GPControllerProjectDetail.getAccountRecord(accountID);
    }

    /**
    * @description Returns  status of project and its child record
    * @param projectTemplateId SFDC 18/15 digit Project  Id
    * 
    * @return GPAuraResponse Serialized status of project and its child record
    * 
    * @example
    * GPCmpServiceProjectDetail.getVerticalSubVerticalForAccount(<SFDC 18/15 digit Account Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getVerticalSubVerticalForAccount(Id accountID) {
         return GPControllerProjectDetail.getVerticalSubVerticalForAccount(accountID);
    }
    
    /**
    * @description Returns list of currency for operating unit.
    * @param operatingUnitID SFDC 18/15 digit operating Unit Id
    * 
    * @return GPAuraResponse Serialized List of Currency
    * 
    * @example
    * GPCmpServiceProjectDetail.getOUCurrency(<SFDC 18/15 digit operating unit Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getOUCurrency(Id operatingUnitID) {
        return GPControllerProjectDetail.getOUCurrency(operatingUnitID);
    }
    
    /**
    * @description Returns Deal data.
    *
    * @param billToNameID SFDC 18/15 digit deal Id
    * 
    * @return GPAuraResponse Serialized Deal data
    * 
    * @example
    * GPCmpServiceProjectDetail.getDealData(<dealId>);
    */
    @AuraEnabled
    public static GPAuraResponse getDealData(Id dealId) {
        return GPControllerProjectDetail.getDealData(dealId);
    }  
    
    /**
    * @description Returns Project data.
    *
    * @param billToNameID SFDC 18/15 digit deal Id
    * 
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getProjectsForDeal(<dealId>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectsForDeal(Id dealId) {
        return GPControllerProjectDetail.getProjectsForDeal(dealId);
    }    
    
    /**
    * @description Returns Project data.
    *
    * @param billToNameID SFDC 18/15 digit project Id
    * 
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.cloneBPMProject(<projectId>);
    */
    @AuraEnabled
    public static GPAuraResponse cloneBPMProject(Id projectId, Id dealId) {
        return GPControllerProjectDetail.cloneBPMProject(projectId, dealId);
    } 
    
    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getDealPicklist();
    */
    @AuraEnabled
    public static GPAuraResponse getDealPicklist() {
        return GPControllerProjectDetail.getDealPicklist();
    } 

    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getParentProjectForGroupInvoice(serializedProject);
    */
    @AuraEnabled
    public static GPAuraResponse getParentProjectForGroupInvoice(String serializedProject) {
        return GPControllerProjectDetail.getParentProjectForGroupInvoice(serializedProject);
    } 

    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getIsAvailableForLegacyProjectCreation();
    */
    @AuraEnabled
    public static GPAuraResponse getIsAvailableForLegacyProjectCreation() {
        return GPControllerProjectDetail.getIsAvailableForLegacyProjectCreation();
    } 

    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getFilteredSOD(isLegacyProject);
    */
    @AuraEnabled
    public static GPAuraResponse getFilteredSOD(Boolean isLegacyProject, Boolean isInterCOEProject) {
        return GPControllerProjectDetail.getFilteredSOD(isLegacyProject, isInterCOEProject);
    } 
    
    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getParentProjectInvoice(serializedProject);
    */
    @AuraEnabled
    public static GPAuraResponse getParentProjectInvoice(String serializedProject) {        
        return GPControllerProjectTemplate.getParentProjectInvoice(serializedProject);
    }

    /**
    * @description Returns Picklist for Deal.
    * @return GPAuraResponse Serialized Project data
    * 
    * @example
    * GPCmpServiceProjectDetail.getFilteredListOfTaxCode(operatingUnitOracleID);
    */
    @AuraEnabled
    public static GPAuraResponse getFilteredListOfTaxCode(String operatingUnitOracleID) {        
        return GPControllerProjectDetail.getFilteredListOfTaxCode(operatingUnitOracleID);
    }

    @AuraEnabled
    public static GPAuraResponse getCRNData(String CRNNumberId) {    
        return GPControllerProjectDetail.getCRNData(CRNNumberId);
    }

    @AuraEnabled
    public static GPAuraResponse getPickList(String serializedProjectData, String serializedDealData) {
        return GPControllerProjectTemplate.getPickList(serializedProjectData,serializedDealData);
    }

    @AuraEnabled
    public static GPAuraResponse getSDOData(Id sdoId) {
        return GPControllerProjectDetail.getSDOData(sdoId);
    }

    @AuraEnabled
    public static GPAuraResponse getDefaultOUForIndirectProject() {
        return GPControllerProjectDetail.getDefaultOUForIndirectProject();
    }
//Avinash - Avalara
    @AuraEnabled
    public static GPAuraResponse getBillToAddressForProject(Id pId)
    {        
        GPControllerProjectAddress projectAddressController = new GPControllerProjectAddress(pId);
        return projectAddressController.getProjectAddress();
    }
    
    @AuraEnabled
    public static void SetServiceDeliveryInCanadaFlag(Id ProjectId, Boolean serviceDeliveryFlag)
    {
         List<GP_Project__c> lstPrj=[Select Id,GP_Service_Deliver_in_US_Canada__c,GP_Tax_Exemption_Certificate_Available__c,
                                    GP_Valid_Till__c,GP_Country__c,GP_State__c,GP_City__c,GP_Pincode__c,GP_US_Address__c,
                                    GP_Sub_Category_Of_Services__c from GP_Project__c where Id =: ProjectId];
        if(lstPrj.size()>0)
        {
            lstPrj[0].GP_Service_Deliver_in_US_Canada__c=serviceDeliveryFlag;
            if(!serviceDeliveryFlag)
            {
                lstPrj[0].GP_Tax_Exemption_Certificate_Available__c=serviceDeliveryFlag;  
                lstPrj[0].GP_Valid_Till__c=null;
                lstPrj[0].GP_Sub_Category_Of_Services__c = null;                
                lstPrj[0].GP_Country__c = null;
                lstPrj[0].GP_State__c = null;
                lstPrj[0].GP_City__c = null;
                lstPrj[0].GP_Pincode__c = null;
                lstPrj[0].GP_US_Address__c = null;
            }
        }
        update lstPrj;
    }
	@AuraEnabled
    public static void SetPOFlag(Id ProjectId, Boolean POFlag)
    {
        List<GP_Project__c> lstPrj=[Select Id,GP_Is_PO_Required__c from GP_Project__c where Id =: ProjectId];
        if(lstPrj.size()>0)
        {
           lstPrj[0].GP_Is_PO_Required__c=POFlag;
           update lstPrj;
        }        
    }
    @AuraEnabled
    public static String getOperatingUnitLECode(Id pId)
    {
        List<GP_Project__c> lstGpProject=[Select Id, GP_Operating_Unit__r.GP_LE_Code__c from GP_Project__c where Id =:pId];
        if(lstGpProject.size()>0)
        {return lstGpProject[0].GP_Operating_Unit__r.GP_LE_Code__c;}
        else
        {
            return null;
        }
    }
    @AuraEnabled
    public static String CheckAvalaraLECodeExists(String lecode)
    {
     	String leCodelist;
        for(GP_Avalara_LE_Code__c ap : GP_Avalara_LE_Code__c.getAll().values()) {            
             leCodelist = ap.LE_Code__c; 
        }  
        
        List<String> lstLECode = leCodelist.split(',');  
        if(lstLECode.contains(lecode))
        {
             return 'Y';  
        }
        else
        {
             return 'N';   
        }
       
    }
    @AuraEnabled 
    public static Map<String, List<String>> getDependentMap(sObject objDetail, string contrfieldApiName,string depfieldApiName) {
        
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
        
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        
        Schema.sObjectType objType = objDetail.getSObjectType();
        if (objType==null){
            return objResults;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
                
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objResults;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
            objResults.put(label, new List<String>());
            controllingValues.add(label);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).add(label);
                }
            }
        }
        return objResults;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }
    // Avinash- Avalara Changes end Here
	
	// Gainshare Change: M&A Module
    @AuraEnabled
    public static GPAuraResponse getCRNDataPerCustomerHierarachy(Id accountId) {
		List<GPOptions> listOfCRNNumber = new List<GPOptions>();
        listOfCRNNumber.add(new GPOptions('', '--None--', ''));
        try{
            String activeSignedContractStatus = System.Label.GP_Active_CRN_Status_On_Project;
        	List<String> listOfActiveStatus = activeSignedContractStatus.split(',');
            
            List<GP_Icon_Master__c> listOfIconMaster = [SELECT ID, Name, GP_Type__c FROM GP_Icon_Master__c 
                                                        WHERE GP_Status__c in: listOfActiveStatus 
                                                        AND GP_ACCOUNT_CODE__c =: accountId 
                                                        AND GP_Type__c != 'Dummy' ORDER BY Name];
            
            if(listOfIconMaster == null || listOfIconMaster.isEmpty()) {
                listOfIconMaster = [SELECT ID, Name, GP_Type__c FROM GP_Icon_Master__c 
                                    WHERE GP_Status__c in: listOfActiveStatus 
                                    AND GP_Type__c = 'Dummy' LIMIT 1
                                   ];
            }            
            for(GP_Icon_Master__c iconMaster : listOfIconMaster) {
                listOfCRNNumber.add(new GPOptions(iconMaster.Id, iconMaster.Name, iconMaster.GP_Type__c));
            }
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }        
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfCRNNumber));      
    }
}		

