global class GPBatchClassForPIDInitialDates implements Database.Batchable<sObject> {
	
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select id,
                (Select Id, GP_Project_JSON__c
                 from Project_Version_History__r 
                 where GP_Status__c = 'Approved' Order By CreatedDate ASC Limit 1) 
                from GP_Project__c where z_GP_is_Initial_Date_Captured__c = false]);
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> listProjects) {
        List<GP_Project__c> allPIDs = new List<GP_Project__c>();
        
        for(GP_Project__c pro : listProjects) {
            
            for(GP_Project_Version_History__c pvh : pro.Project_Version_History__r) {
                Map<String,String> mapOfProjectJson = (Map<String,String>)JSON.deserialize(pvh.GP_Project_JSON__c, Map<String,String>.class);
                
                if(mapOfProjectJson != null) {
                    pro.GP_Initial_PID_Creation_DateTime__c = mapOfProjectJson.get('createddate') != null ? Datetime.valueOf(mapOfProjectJson.get('createddate').replace('T', ' ').replace('Z', ' ')) : null;
                    pro.GP_Initial_PID_Submission_DateTime__c = mapOfProjectJson.get('gp_project_submission_date__c') != null ? getDateTime(Date.valueOf(mapOfProjectJson.get('gp_project_submission_date__c'))) : null;
                    pro.GP_Initial_PID_Approval_DateTime__c = mapOfProjectJson.get('gp_approval_datetime__c') != null ? Datetime.valueOf(mapOfProjectJson.get('gp_approval_datetime__c').replace('T', ' ').replace('Z', ' ')) : null;
                    pro.z_GP_is_Initial_Date_Captured__c = true;
                }
            }
            
            allPIDs.add(pro);
            system.debug('==list=='+allPIDs);
        }
        
        if(allPIDs.size() > 0) {
            update allPIDs;
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
    
    private DateTime getDateTime(Date dt) {
        Integer d = dt.day();
        Integer mo = dt.month();
        Integer yr = dt.year();
        
        DateTime daTi = DateTime.newInstance(yr, mo, d);
        
        return daTi;
    }
}