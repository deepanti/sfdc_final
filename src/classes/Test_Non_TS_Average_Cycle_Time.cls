@isTest
public class Test_Non_TS_Average_Cycle_Time {
  public static Opportunity opp, opp2;
    public static Contact oContact;
    public static User u;
    
    public static testMethod void dealcycleStageDiscoverTest(){
        setupTestData();
        //opp.Type_of_deal_for_non_ts__c='Retail';
       // opp.Formula_Hunting_Mining__c='Hunting';
        opp.Deal_Type__c='Sole Sourced';
        singleexecuion.NonTSFlag = true;
        update opp;
    }
    public static testMethod void dealcycleStageDiscoverTest1(){
	 
	  
		
Deal_Cycle__c   DC= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC3= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC4= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC5= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC6= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC7= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC8= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC9= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC10= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC11= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='1. Discover',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC12= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC13= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC14= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC15= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC16= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC17= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Hunting',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC18= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC19= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC20= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Sole Sourced',Cycle_Time__c=400);
Deal_Cycle__c   DC21= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Retail',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC22= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Medium',Deal_Type__c='Competitive',Cycle_Time__c=400);
Deal_Cycle__c   DC23= new Deal_Cycle__c(Transformation_Deal__c='Yes',Hunting_Mining__c='Mining',Stage__c='2. Define',Type_of_deal_for_non_ts__c='Large',Deal_Type__c='Competitive',Cycle_Time__c=400);

        Deal_Cycle__c   DC24= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Retail',Ageing__c=20); 
        Deal_Cycle__c   DC25= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Large',Ageing__c=30);                       
        
        Insert DC;
         Insert DC1;
        Insert DC2;
        Insert DC3;
       Insert DC4;
       Insert DC5;
        Insert DC6;
        Insert DC7;
		 Insert DC8;
        Insert DC9;
       Insert DC10;
       Insert DC11;
        Insert DC12;
        Insert DC13;
		      Insert DC14;
        Insert DC15;
        Insert DC16;
       Insert DC17;
       Insert DC18;
        Insert DC19;
        Insert DC20;
		 Insert DC21;
        Insert DC22;
       Insert DC23;
	    Insert DC24;
       Insert DC25;
          system.debug('=============2===============');
        Double Discover_age_hun_sole_retail_non_ts=DC.Cycle_Time__c/2;
        Double Discover_age_hun_sole_medium_non_ts=DC1.Cycle_Time__c/2;
Double Discover_age_hun_sole_large_non_ts=DC2.Cycle_Time__c/2;
Double Discover_age_hun_comp_retail_non_ts=DC3.Cycle_Time__c/2;
Double Discover_age_hun_comp_medium_non_ts=DC4.Cycle_Time__c/2;
Double Discover_age_hun_comp_large_non_ts=DC5.Cycle_Time__c/2;
        Double Discover_age_min_sole_retail_non_ts=DC6.Cycle_Time__c/2;
        Double Discover_age_min_sole_medium_non_ts=DC7.Cycle_Time__c/2;
Double Discover_age_min_sole_large_non_ts=DC8.Cycle_Time__c/2;
Double Discover_age_min_comp_retail_non_ts=DC9.Cycle_Time__c/2;
Double Discover_age_min_comp_medium_non_ts=DC10.Cycle_Time__c/2;
Double Discover_age_min_comp_large_non_ts=DC11.Cycle_Time__c/2;
        Double Define_age_hun_sole_retail_non_ts=DC.Cycle_Time__c/2;
        Double Define_age_hun_sole_medium_non_ts=DC1.Cycle_Time__c/2;
Double Define_age_hun_sole_large_non_ts=DC2.Cycle_Time__c/2;
Double Define_age_hun_comp_retail_non_ts=DC3.Cycle_Time__c/2;
Double Define_age_hun_comp_medium_non_ts=DC4.Cycle_Time__c/2;
Double Define_age_hun_comp_large_non_ts=DC5.Cycle_Time__c/2;
        Double Define_age_min_sole_retail_non_ts=DC6.Cycle_Time__c/2;
        Double Define_age_min_sole_medium_non_ts=DC7.Cycle_Time__c/2;
Double Define_age_min_sole_large_non_ts=DC8.Cycle_Time__c/2;
Double Define_age_min_comp_retail_non_ts=DC9.Cycle_Time__c/2;
Double Define_age_min_comp_medium_non_ts=DC10.Cycle_Time__c/2;
Double Define_age_min_comp_large_non_ts=DC11.Cycle_Time__c/2;
        
		setupTestData();
		
	}
    private static void setupTestData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        System.runAs(u){
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',ProductionTCV__c = 100000,
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            
            opp2=new opportunity(name='1234',StageName='1. Defined',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive');
            
            //Contact_Role_RV__c contact_role1 = new Contact_Role_RV__c(contact__c = oContact.id, isPrimary__c = true, opportunity__c = opp.ID);
            
            //insert contact_role1;
            
            List<Opportunity> oppList = new List<Opportunity>();
            oppList.add(opp);
            oppList.add(opp2);
            insert oppList;
            
            
        }
    }
	
}