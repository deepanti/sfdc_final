/*
    @Description: Class will be used for handling all IO Type TCV
    @Test Class: TCVAcceleratorCtlrTest
    @Author: Vikas Rajput
*/
public class VIC_TSTCVCalculationCtlr implements VIC_ScorecardCalculationInterface{
    public Decimal pmAcheivmentPercent{get;set;}
    public Decimal opAcheivmentPercent{get;set;}
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    public Map <Id, User_VIC_Role__c> mapUserIdToUserVicRoles{get;set;}
    
    /*
        @Description: Overriding calculate function for getting target payout
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal calculate(Target_Component__c objTargetCmp, List<VIC_Calculation_Matrix_Item__c> lstMatrixItem){
        Set<String> plansRenewalIncentive = new Set<String>{'SL_CP_Enterprise','SL_CP_CMIT'}; //Adding plan for which need a target calculation
        system.debug('targetbonus'+objTargetCmp.Target__r.Target_Bonus__c);
            if(plansRenewalIncentive.contains(objTargetCmp.Target__r.Plan__r.vic_Plan_Code__c)
            && objTargetCmp.Target__r.Target_Bonus__c != null && objTargetCmp.Weightage__c != null){
            Decimal iOTCVTargetedIncentive  = (objTargetCmp.Target__r.Target_Bonus__c*objTargetCmp.Weightage__c)/100;
            //NOT FOR "SL" && "CM/ITO"
                system.debug('mapUserIdToUserVicRoles'+mapUserIdToUserVicRoles);
                system.debug('horizontal'+mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Horizontal__c);
                if(mapUserIdToUserVicRoles.containsKey(objTargetCmp.Target__r.User__c) && 
                !(mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Role__c == 'SL' && 
                  mapUserIdToUserVicRoles.get(objTargetCmp.Target__r.User__c).Master_VIC_Role__r.Horizontal__c == 'CM/ITO')){
                if(opAcheivmentPercent <= Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_OP_Percent').vic_Value__c)){
                    Decimal acheivmentPercent = objTargetCmp.vic_Achievement_Percent__c > 100?100:objTargetCmp.vic_Achievement_Percent__c;
                    return (iOTCVTargetedIncentive*fetchPayout(acheivmentPercent, lstMatrixItem))/100;
                }else{
                    return (iOTCVTargetedIncentive*fetchPayout(objTargetCmp.vic_Achievement_Percent__c, lstMatrixItem))/100;
                }
            }else{    //EXIST FOR "SL" && "CM/ITO"
                system.debug('vicValue'+Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_PM_Percent').vic_Value__c));
                if((pmAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_PM_Percent').vic_Value__c) && 
                        opAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('IO_TCV_OP_Percent').vic_Value__c))){
                    return (iOTCVTargetedIncentive*fetchPayout(objTargetCmp.vic_Achievement_Percent__c, lstMatrixItem))/100;
                }else{
                    Decimal acheivmentPercent = objTargetCmp.vic_Achievement_Percent__c > 100?100:objTargetCmp.vic_Achievement_Percent__c;
                    return (iOTCVTargetedIncentive*fetchPayout(acheivmentPercent, lstMatrixItem))/100;
                }
            } 
        }
        return null;
    } 
    /*
        @Description: It will return additional payout from Calculation Matrix Items
        @Param: NAN
        @Author: Vikas Rajput
        @Note: This function ogic can be same as IO_TCV so it can be moved in VIC_CommonUtil class but avoiding to writing as common
                function because there slightly change in matrix and it can be more change in future
    */
    public Decimal fetchPayout(Decimal achievementParcent, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        //List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem = [select vic_Payout__c,Is_Additional_Payout_Applicable__c,For_Each__c,vic_Start_Value__c,vic_End_Value__c,Additional_Payout__c,vic_Operator__c,vic_TCV_Booking_Type__c  from VIC_Calculation_Matrix_Item__c where vic_VIC_Calculation_Matrix__c =: calcIdARG ORDER BY vic_Start_Value__c ASC];
        //System.debug('----------------------TSTCV-----------------------achievementParcent=='+achievementParcent);
        Decimal totalPayoutValue = 0.00;
        for(VIC_Calculation_Matrix_Item__c objCalMatrixItem : lstCalcMatrixItem){
            if((achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && objCalMatrixItem.vic_End_Value__c != null)&& achievementParcent >= objCalMatrixItem.vic_Start_Value__c && achievementParcent >= objCalMatrixItem.vic_End_Value__c){
                Decimal tempValue = ((objCalMatrixItem.vic_End_Value__c - objCalMatrixItem.vic_Start_Value__c)+1);
                totalPayoutValue = calcAdditionalPayout(tempValue, objCalMatrixItem, totalPayoutValue);
          //      System.debug('----------------------TSTCV-----------------------totalPayoutValue=='+totalPayoutValue);
            }else if(achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && objCalMatrixItem.vic_End_Value__c != null && achievementParcent >= objCalMatrixItem.vic_Start_Value__c && achievementParcent<= objCalMatrixItem.vic_End_Value__c){
                Decimal tempValue = ((achievementParcent - objCalMatrixItem.vic_Start_Value__c)+1);
                //Decimal tempValue = ((objCalMatrixItem.vic_End_Value__c - objCalMatrixItem.vic_Start_Value__c)+1);
                totalPayoutValue = calcAdditionalPayout(tempValue, objCalMatrixItem, totalPayoutValue);
          //      System.debug('----------------------TSTCV-----------------------totalPayoutValue=='+totalPayoutValue);
            }else if((achievementParcent != null && objCalMatrixItem.vic_Start_Value__c != null && achievementParcent >= objCalMatrixItem.vic_Start_Value__c ) && (objCalMatrixItem.vic_End_Value__c == null )){
                Decimal tempValue = ((achievementParcent - objCalMatrixItem.vic_Start_Value__c)+1); //Comment this line if additional payout is not check in VIC Matrix Item
                totalPayoutValue = calcAdditionalPayout(tempValue, objCalMatrixItem,totalPayoutValue);
          //      System.debug('----------------------TSTCV-----------------------3totalPayoutValue=='+totalPayoutValue);
            }
          //  System.debug('----------------------TSTCV-----------------------TOTAL_Payout=='+totalPayoutValue);
        }
        return totalPayoutValue;
    } 
    /*
        @Description: It is helper function for fetchPayout(function of current class)
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Decimal calcAdditionalPayout(Decimal tempValue, VIC_Calculation_Matrix_Item__c objCalMatrixItem, Decimal totalPayout){
        if(objCalMatrixItem.Is_Additional_Payout_Applicable__c && objCalMatrixItem.For_Each__c != null && 
                objCalMatrixItem.Additional_Payout__c != null && objCalMatrixItem.For_Each__c != 0){
            Decimal calcValue = (tempValue.round(System.RoundingMode.DOWN))/objCalMatrixItem.For_Each__c;
            return (totalPayout + (calcValue*objCalMatrixItem.Additional_Payout__c));
        }else{
            return objCalMatrixItem.vic_Payout__c;
        }
    }
}