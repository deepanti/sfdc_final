@isTest(SeeAllData=false)
public class VIC_CalculateKickersIncBatchTest
{
    public static OpportunityLineItem  objOLI;
    public static  user objuser;
    static testMethod void VIC_CalculateKickersIncentiveBatch()
    {
        
        LoadData();
        
        VIC_CalculateKickersIncentiveBatch obj =new VIC_CalculateKickersIncentiveBatch();
        DataBase.executeBatch(obj);
        System.AssertEquals(200,200);
        
    }
    
    
    static testMethod void VIC_CalculateKickersIncentiveBatchN()
    {
        
        LoadData();
       test.starttest();
       system.runas(objuser){
        objOLI.TCV__c=350000;
        
        update objOLI;
        }
       test.stoptest();
        VIC_CalculateKickersIncentiveBatch obj =new VIC_CalculateKickersIncentiveBatch();
        DataBase.executeBatch(obj);
        System.AssertEquals(200,200);
        
    }
    
    static void LoadData()
    {
        
        VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Process_Year__c=2018;
        insert vicInfo;
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
       insert objConga;
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='IT_GRM';   
        planObj1.Is_Active__c = true;
        planObj1.Year__c= String.valueOf(System.today().year());
        insert planobj1;
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
            
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        system.debug('obbbbbjuser'+objuser);
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
        system.debug('masterVICRoles'+masterVICRoleObj);
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
        VIC_Role__c vrole= VIC_CommonTest.getVICRole(masterVICRoleObj.id,planObj1.id);
         insert  vrole;       
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objOpp.vic_Is_Kickers_Calculated__c=false;
        objOpp.OwnerId=objUser.id;
        
        insert objOpp;
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.Product_BD_Rep__c=objuser3.id;
        objOLI.vic_VIC_User_3__c=objuser1.id;
        objOLI.vic_VIC_User_4__c=objuser2.id;
        objOLI.vic_Primary_Sales_Rep_Split__c=100;
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.Contract_Term__c=36;
        objOLI.TCV__c=3500000;
        insert objOLI;
        
        objopp.Number_of_Contract__c=2;    
        objOpp.stagename='6. Signed Deal';
        system.runas(objuser){
        update objOpp;
        }
        system.debug('objOLI'+objOLI);
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Kickers'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
            
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
            
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        
        Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc.Opportunity__c=objOpp.id;
        insert objInc;
        
        
        
        
    }
    
    
    
}