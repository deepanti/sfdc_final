@isTest
public class GPDomainResourceAllocationTracker{
    public static boolean isRun = true;
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    
    @testSetup
    public static Void buildDependencyData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_resource_allocation__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        //prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('OTHER PBB').getRecordTypeId();
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_TCV__c = 234;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Project_type__c = 'Fixed monthly';
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-employee';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = false;
        update empObj;
        
        GP_Resource_Allocation__c objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
     //Author: Anoop Verma
    // Date:1-Jul-2020
    // CR:CRQ000000076988
        objResourceAllocation1.GP_Oracle_Id__c='123457';
        objResourceAllocation1.GP_Oracle_Status__c='S';
        Update objResourceAllocation1;

        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation.GP_Bill_Rate__c = 0;
        objResourceAllocation.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation.GP_Percentage_allocation__c = 1;
        objResourceAllocation.GP_Parent_Resource_Allocation__c = objResourceAllocation1.id;
        insert objResourceAllocation;
        Update objResourceAllocation;
     //Author: Anoop Verma
    // Date:1-Jul-2020
    // CR:CRQ000000076988
        objResourceAllocation.GP_Oracle_Id__c='12345';
        objResourceAllocation.GP_Oracle_Status__c='S';
         Update objResourceAllocation;

        system.debug('objResourceAllocation :'+objResourceAllocation);
            
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        objPrjBdgt.GP_TCV__c = 900000;
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;

        
    //Author: Anoop Verma
    // Date:3-Jul-2020
    // CR:CRQ000000076988

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Oracle_Status__c = 'NA';        
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_is_Send_Notification__c=true;
        insert prjObj;
              
        prjObj.GP_Approval_Status__c='Approved';
        prjObj.GP_Oracle_Status__c = 'S'; 
        update  prjObj;
   
         GP_Resource_Allocation__c objResourceAllocation2 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        
         insert objResourceAllocation2;
         objResourceAllocation2.GP_Oracle_Id__c='12345';
         objResourceAllocation2.GP_Oracle_Status__c='R';
         Update objResourceAllocation;
    }
    
    @isTest
    public static void testGPDomainResourceAll() {
        GPDomainResourceAllocation  obj = new GPDomainResourceAllocation(new List<GP_Resource_Allocation__c >{new GP_Resource_Allocation__c()});
    }
}