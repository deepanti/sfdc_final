/**
 *
 * @group Project Budget And Pricing.
 * @group-content ../../ApexDocContent/ProjectBudgetAndPricing.html.
 *
 * @description Apex controller class having business logics
 *              to fetch, insert, update and delete budget under a project.
 */
public class GPControllerProjectBudgetandPricing {

    private final String IS_BUDGET_NATIVELY_AVAILABLE_LABEL = 'isBudgetNativelyAvailable';
    private final String LIST_OF_PROJECT_BUDGET_RECORD_LABEL = 'listOfProjectbudget';
    private final String LIST_OF_COUNTRY_RECORD_LABEL = 'listOfCountries';
    private final String MAP_OF_BUDGET_RECORD_LABEL = 'mapOfBudgetRecord';
    private final String LIST_OF_PRODUCT_RECORD_LABEL = 'listOfProducts';
    private final String CURRENCY_ISO_CODE_LABEL = 'currencyISOCode';
    private final String LIST_OF_BAND_RECORD_LABEL = 'listOfBands';
    private final String ISUPDATEABLE_LABEL = 'isUpdatable';
    private final String IS_OMS_TYPE_LABEL = 'isOMSType';
    private final String SUCCESS_LABEL = 'SUCCESS';

    private JSONGenerator gen;

    private Map < String, GP_Budget_master__c > mapOfUniqueCombinationToBudget;
    private List < GP_Project_Budget__c > listOfProjectBudget,listOfProjectBudgetToDelete;
    private map < String, String > mapOfUniqueCountries;
    private map < String, String > mapOfUniqueProducts;
    private map < String, String > mapOfUniqueBands;
    private Boolean isBudgetNativelyAvailable;
    private List < GPOptions > lstOfCountries;
    private List < GPOptions > lstOfProducts;
    private List < GPOptions > lstOfBands;
    private GP_Project__c projectObj;
    private String currencyISOCode;
    private Boolean isUpdatable;
    private Id projectId;

    /**
     * @description Parameterized constructor to accept Project Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerProjectBudgetandPricing(<SFDC 18/15 digit project Id>);
     */
    public GPControllerProjectBudgetandPricing(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description Parameterized constructor to accept Project Budget List.
     * @param listOfProjectBudget List Of Project Budget.
     * 
     * @example
     * new GPControllerProjectBudgetandPricing(<List of Project Budget>);
     */
    public GPControllerProjectBudgetandPricing(List < GP_Project_Budget__c > listOfProjectBudget) {
        this.listOfProjectBudget = listOfProjectBudget;
    }
    
    /**
     * @description Parameterized constructor to accept Project Budget List to Insert and delete.
     * @param listOfProjectBudget List Of Project Budget.
     * @param listOfProjectBudgetToDelete List Of Project Budget.
     * 
     * @example
     * new GPControllerProjectBudgetandPricing(<List of Project Budget>,<List of Project Budget>);
     */
    public GPControllerProjectBudgetandPricing(List < GP_Project_Budget__c > listOfProjectBudget,List < GP_Project_Budget__c > listOfProjectBudgetToDelete) {
        this.listOfProjectBudget = listOfProjectBudget;
        this.listOfProjectBudgetToDelete = listOfProjectBudgetToDelete;
    }

    public GPControllerProjectBudgetandPricing() {}

    /**
     * @description method to upsert Project Budget Records.
     * 
     * @example
     * new GPControllerProjectBudgetandPricing(<List Of Project Budget>).saveProjectBudgetRecord();
     */
    public GPAuraResponse saveProjectBudgetRecord() {
        try {
            //GPServiceProjectBudget.validateProjectBudget(listOfProjectBudget);
            upsert listOfProjectBudget;
            
            if(listOfProjectBudgetToDelete != null && listOfProjectBudgetToDelete.size() > 0)
                delete listOfProjectBudgetToDelete;
            
            projectId = listOfProjectBudget[0].GP_Project__c;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJECT BUDGET');
            update project; 
        } catch (Exception ex) {
            
            if(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
            {
                string errorMessage =  GPCommon.parseErrorMessage(ex.getMessage());
                integer occurence = errorMessage.lastIndexOf('[]');
                errorMessage = errorMessage.mid(0, occurence);
                return new GPAuraResponse(false,errorMessage, null);
            }
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfProjectBudget, true));
    }

    /**
     * @description method to delete Project Budget Records.
     * 
     * @example
     * new GPControllerProjectBudgetandPricing(<SFDC 18/15 digit project Id>).deleteProjectBudgetRecord(<SFDC 18/15 digit project budget Id>);
     */
    public GPAuraResponse deleteProjectBudgetRecord(Id projectBudgetId) {
        try {
            GP_Project_Budget__c budget = new GPSelectorProjectBudget().selectProjectBudgetRecord(projectBudgetId);
            projectId = budget.GP_Project__c;
            delete budget;
            
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJECT BUDGET');
            update project; 
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
     * @description method to return Project Budgets.
     *
     * @example
     * new GPControllerProjectBudgetandPricing(<SFDC 18/15 digit project Id>).getBudgetRecord();
     */
    @testVisible
    public GPAuraResponse getBudgetRecord() {
        try {
            if (isBudgetNativelyAvailable()) {
                setBudgetRecord();
                setProjectBudget();
                setJson();
            } else {
                getOMSBudgetRecord();
            }
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description method to return OMS Project Budgets.
     *
     * @example
     * new GPControllerProjectBudgetandPricing(<SFDC 18/15 digit project Id>).getOMSBudgetRecord();
     */
    @testVisible
    public GPAuraResponse getOMSBudgetRecord() {
        try {
            setProjectBudget();
            //Updated by Anmol on 04-04-2018 => Changed from true to false.
            isBudgetNativelyAvailable = false;
            setProject();
            setOMSJson();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description method to set Budget Record and related content for UI.
     *
     */
    private void setBudgetRecord() {
        List < GP_Budget_Master__c > lstOfBudgetMasters = new GPSelectorBudgetMaster().selectAllMasterBudgetRecords();
        mapOfUniqueCombinationToBudget = new Map < String, GP_Budget_master__c > ();
        mapOfUniqueCountries = new map < String, String > ();
        mapOfUniqueProducts = new map < String, String > ();
        mapOfUniqueBands = new map < String, String > ();
        lstOfCountries = new List < GPOptions > ();
        lstOfProducts = new List < GPOptions > ();
        lstOfBands = new List < GPOptions > ();

        for (GP_Budget_Master__c budgetMaster: lstOfBudgetMasters) {
            mapOfUniqueCombinationToBudget.put(budgetMaster.GP_Band__c + '___' +
                budgetMaster.GP_Country__c +
                '___' + budgetMaster.GP_Product__c, budgetMaster);

            if (!mapOfUniqueBands.containsKey(budgetMaster.GP_Band__c)) {
                lstOfBands.add(new GPOptions(budgetMaster.GP_Band__c, budgetMaster.GP_Band__c));
                mapOfUniqueBands.put(budgetMaster.GP_Band__c, budgetMaster.GP_Band__c);
            }

            if (!mapOfUniqueCountries.containsKey(budgetMaster.GP_Country__c)) {
                lstOfCountries.add(new GPOptions(budgetMaster.GP_Country__c, budgetMaster.GP_Country__c));
                mapOfUniqueCountries.put(budgetMaster.GP_Country__c, budgetMaster.GP_Country__c);
            }

            if (!mapOfUniqueProducts.containsKey(budgetMaster.GP_Product__c)) {
                lstOfProducts.add(new GPOptions(budgetMaster.GP_Product__c, budgetMaster.GP_Product__c));
                mapOfUniqueProducts.put(budgetMaster.GP_Product__c, budgetMaster.GP_Product__c);
            }
        }

        //if(lstOfProducts.size() > 1)
        //    lstOfProducts.add(0, new GPOptions('null' ,'--NONE--'));
        //if(lstOfCountries.size() > 1)
        //    lstOfCountries.add(0, new GPOptions('null' , '--NONE--'));
        //if(lstOfBands.size() > 1)
        //    lstOfBands.add(0, new GPOptions('null' , '--NONE--'));

    }

    /**
     * @description Query Project Budget Records.
     *
     */
    private void setProjectBudget() {
        listOfProjectBudget = new GPSelectorProjectBudget().selectProjectBudgetRecords(projectId);
    }

    /**
     * @description Check for OMS type.
     *
     */
    private Boolean isBudgetNativelyAvailable() {
        setProject();
        isBudgetNativelyAvailable = (projectObj.RecordType.Name == 'OTHER PBB' && projectObj.GP_Project_type__c == 'Fixed Price') ||
            (projectObj.RecordType.Name == 'CMITS' && projectObj.GP_Project_type__c == 'Fixed Price' &&
                projectObj.GP_OMS_Deal_ID__c == null) ||
            (projectObj.RecordType.Name == 'CMITS'  && projectObj.GP_Deal__r.GP_No_Pricing_in_OMS__c &&  projectObj.GP_Project_type__c != 'Fixed monthly');
        /*isBudgetNativelyAvailable = !(projectObj.RecordType.Name == 'CMITS' && 
                                      (projectObj.GP_Project_type__c == 'Fixed Price' ||
                                       projectObj.GP_Project_type__c == 'Fixed monthly'));*/
        return isBudgetNativelyAvailable;
    }

    /**
     * @description Query Project Record and associated class members.
     *
     */
    private void setProject() {
        projectObj = new GPSelectorProject().selectProjectRecord(projectId);
        isUpdatable = projectObj.RecordType.Name != 'CMITS' && projectObj.GP_Approval_Status__c == 'Draft' ? true : projectObj.GP_Approval_Status__c == 'Approved' || projectObj.GP_Approval_Status__c == 'Pending For Approval' ? false : true;
        currencyISOCode = projectObj.CurrencyIsoCode;
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (mapOfUniqueCombinationToBudget != null)
            gen.writeObjectField(MAP_OF_BUDGET_RECORD_LABEL, mapOfUniqueCombinationToBudget);

        if (lstOfCountries != null)
            gen.writeObjectField(LIST_OF_COUNTRY_RECORD_LABEL, lstOfCountries);

        if (lstOfProducts != null)
            gen.writeObjectField(LIST_OF_PRODUCT_RECORD_LABEL, lstOfProducts);

        if (lstOfBands != null)
            gen.writeObjectField(LIST_OF_BAND_RECORD_LABEL, lstOfBands);

        setCommonFields();

        gen.writeBooleanField(IS_OMS_TYPE_LABEL, false);
        gen.writeEndObject();
    }

    /**
     * @description Set GPAuraResponse object for OMS type.
     * 
     */
    private void setOMSJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        setCommonFields();
        gen.writeBooleanField(IS_OMS_TYPE_LABEL, true);
        gen.writeEndObject();
    }

    /**
     * @description Set common fields for GPAuraResponse.
     * 
     */
    private void setCommonFields() {
        if (isBudgetNativelyAvailable != null)
            gen.writeBooleanField(IS_BUDGET_NATIVELY_AVAILABLE_LABEL, isBudgetNativelyAvailable);

        if (listOfProjectBudget != null)
            gen.writeObjectField(LIST_OF_PROJECT_BUDGET_RECORD_LABEL, listOfProjectBudget);

        if (currencyISOCode != null)
            gen.writeObjectField(CURRENCY_ISO_CODE_LABEL, currencyISOCode);

        if (isUpdatable != null)
            gen.writeObjectField(ISUPDATEABLE_LABEL, isUpdatable);
    }
}