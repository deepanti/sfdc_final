public class VICServiceHRApproval {

    private final String DEFAULT_YEAR_MISSING_ERROR_LABEL = 'VIC process default year is missing. Please contact your VIC support team.';

    private Map < String, List < Target_Component__c >> mapOfTargetIdVsTargetComponents = new Map < String, List < Target_Component__c >> ();
    private Map < String, List < HRDataWrapper >> mapOfDomicileVsIncentiveData = new Map < String, List < HRDataWrapper >> ();
    public Map < String, Set < String >> mapOfTargetIdsVSAssociatedIncentiveIds = new Map < String, Set < String >> ();
    private Map < String, List < Target__c >> mapOfDomicileVsTargets = new Map < String, List < Target__c >> ();
    private Map < String, List < Target__c >> mapOfOnHoldTargets = new Map < String, List < Target__c >> ();
    private Map < String, List < Target__c >> mapOfUserIdVsTargets = new Map < String, List < Target__c >> ();

    private static Map < String, String > mapDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap();
    private static Map < String, String > mapUserVicRoles = vic_CommonUtil.getUserVICRoleNameinMap();
    private static VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    public String incentiveStatus, leaderName,screenName;

    public VICServiceHRApproval(String incentiveStatus, String leaderName,String screenName) {
        this.incentiveStatus = incentiveStatus;
        this.leaderName = leaderName;
        this.screenName = screenName;
    }

    public Boolean validLoggedinUser() {
        return true;
    }

    public Map < String, List < HRDataWrapper >> getDomicileWiseIncentives() {
        if (processInfo == null || processInfo.VIC_Process_Year__c == null) {
            throw new AuraHandledException(DEFAULT_YEAR_MISSING_ERROR_LABEL);
        }
        setTargetRelatedData();
        setMapOfDomicileVsIncentiveData();
        return mapOfDomicileVsIncentiveData;
    }

    private void setMapOfDomicileVsIncentiveData() {
        system.debug('mapUserVicRoles'+mapUserVicRoles);
        HRDataWrapper incentiveData;
        CalculationWrapper calculationWrapper = new CalculationWrapper();
        Map < String, CalculationWrapper > mapOfCaseIdVsCalculationWrapper = new Map < String, CalculationWrapper > ();
        
        List < HRDataWrapper > lstOfIncentiveDataWrapper;
        for (String domicile: mapOfDomicileVsTargets.KeySet()) {
            lstOfIncentiveDataWrapper = new List < HRDataWrapper > ();
            for (Target__c target: mapOfDomicileVsTargets.get(domicile)) {
                if (!mapUserVicRoles.containsKey(target.User__r.Id))
                    continue;

                incentiveData = new HRDataWrapper();
                incentiveData.IsOnHold = false;
                incentiveData.IsRejected = false;
                incentiveData.UserId = target.User__r.Id;
                incentiveData.UserName = target.User__r.Name;
                incentiveData.VICRole = mapUserVicRoles.containsKey(target.User__r.Id) ? mapUserVicRoles.get(target.User__r.Id) : '-';
                incentiveData.CurrencyISOCode = mapDomicileToISOCode.containsKey(target.Domicile__c) ? mapDomicileToISOCode.get(target.Domicile__c) : '';
                incentiveData.Comments = target.vic_HR_Comments__c;
                if (incentiveStatus == 'HR - Pending' || incentiveStatus == 'VIC Team - Pending')
                    mapOfCaseIdVsCalculationWrapper.put(incentiveStatus, new CalculationWrapper());
                for (Target_Component__c targetComponent: mapOfTargetIdVsTargetComponents.get(target.Id)) {
                    for (Target_Achievement__c incentive: targetComponent.Target_Achievements__r) {
                        if (incentiveStatus == 'HR - Pending' || incentiveStatus == 'VIC Team - Pending')
                            calculationWrapper = mapOfCaseIdVsCalculationWrapper.get(incentiveStatus);
                        else if (incentiveStatus == 'HR - OnHold' && incentive.VIC_Related_Case__c != null) {
                            if (mapOfCaseIdVsCalculationWrapper.containsKey(target.User__r.Id + '@@' + incentive.VIC_Related_Case__r.CaseNumber + '@@' + incentive.VIC_Related_Case__c))
                                calculationWrapper = mapOfCaseIdVsCalculationWrapper.get(target.User__r.Id + '@@' + incentive.VIC_Related_Case__r.CaseNumber + '@@' + incentive.VIC_Related_Case__c);
                            else {
                                mapOfCaseIdVsCalculationWrapper.put(target.User__r.Id + '@@' + incentive.VIC_Related_Case__r.CaseNumber + '@@' + incentive.VIC_Related_Case__c, new CalculationWrapper());
                                calculationWrapper = mapOfCaseIdVsCalculationWrapper.get(target.User__r.Id + '@@' + incentive.VIC_Related_Case__r.CaseNumber + '@@' + incentive.VIC_Related_Case__c);
                            }
                        }
                        if (incentive.vic_Incentive_In_Local_Currency__c == null)
                            incentive.vic_Incentive_In_Local_Currency__c = 0;
                        if (!incentive.Is_Anniversary_Incentive__c && (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_IO') || incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_TS'))) {
                            calculationWrapper.NewBookingAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Renewal')) {
                            calculationWrapper.RenewalAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('New_Logo_Bonus')) {
                            calculationWrapper.NewLogoBonusAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Kickers')) {
                            calculationWrapper.ITKickersAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (incentive.Is_Anniversary_Incentive__c && (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_IO') || incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Profitable_Bookings_TS'))) {
                            calculationWrapper.BDEAnniversaryAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('Discretionary_Payment')) {
                            calculationWrapper.DiscretionaryAmount += incentive.vic_Incentive_In_Local_Currency__c;
                        }
                        /*else if (incentive.vic_Deal_Type__c == 'TS' && incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('TCV_Accelerators')) {
                                                   calculationWrapper.TCVAccleratorTS += incentive.vic_Incentive_In_Local_Currency__c;
                                               } else if (incentive.vic_Deal_Type__c == 'IO' && incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('TCV_Accelerators')) {
                                                   calculationWrapper.TCVAccleratorIO += incentive.vic_Incentive_In_Local_Currency__c;

                                               }*/
                        else if (leaderName == 'SalesLeader' && incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('OP') ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('MBO')) ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('IO_TCV')) ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('TS_TCV'))) {
                            calculationWrapper.ScoreCardIncentive += incentive.vic_Incentive_In_Local_Currency__c;
                        } else if (leaderName == 'SalesRep' && incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('TCV_Accelerators') ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('OP')) ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('PM')) ||
                            (incentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c.equalsIgnoreCase('TCV'))) {
                            calculationWrapper.ScoreCardIncentive += incentive.vic_Incentive_In_Local_Currency__c;
                        }
                        calculationWrapper.setOfIncentiveIds.add(incentive.Id);
                        if (incentiveStatus == 'HR - Pending' || incentiveStatus == 'VIC Team - Pending')
                            mapOfCaseIdVsCalculationWrapper.put(incentiveStatus, calculationWrapper);
                        else if (incentiveStatus == 'HR - OnHold' && incentive.VIC_Related_Case__c != null)
                            mapOfCaseIdVsCalculationWrapper.put(target.User__r.Id + '@@' + incentive.VIC_Related_Case__r.CaseNumber + '@@' + incentive.VIC_Related_Case__c, calculationWrapper);
                    }
                }
                system.debug('mapOfCaseIdVsCalculationWrapper'+mapOfCaseIdVsCalculationWrapper);
                if (incentiveStatus == 'HR - Pending' || incentiveStatus == 'VIC Team - Pending') {
                    calculationWrapper = mapOfCaseIdVsCalculationWrapper.get(incentiveStatus);
                    createIncentiveReecordInstance(incentiveData, calculationWrapper);
                    if (calculationWrapper.setOfIncentiveIds.size() > 0 && (incentiveData.TotalPayoutAmount != 0 || incentiveData.TotalScoreCard != 0))
                        lstOfIncentiveDataWrapper.add(incentiveData);
                } else if (incentiveStatus == 'HR - OnHold') {
                    for (String caseNumber: mapOfCaseIdVsCalculationWrapper.keySet()) {
                        calculationWrapper = mapOfCaseIdVsCalculationWrapper.get(caseNumber);
                        incentiveData = (HRDataWrapper) JSON.deserialize(JSON.serialize(incentiveData), HRDataWrapper.class);
                        createIncentiveReecordInstance(incentiveData, calculationWrapper);
                        incentiveData.CaseNumber = caseNumber.split('@@')[1];
                        incentiveData.CaseId = caseNumber.split('@@')[2];
                        if (calculationWrapper.setOfIncentiveIds.size() > 0 && (incentiveData.TotalPayoutAmount != 0 || incentiveData.TotalScoreCard != 0))
                            lstOfIncentiveDataWrapper.add(incentiveData);
                    }
                }
                mapOfCaseIdVsCalculationWrapper = new Map < String, CalculationWrapper > (); 
            }
            if (!mapOfDomicileVsIncentiveData.containsKey(domicile)) {
                mapOfDomicileVsIncentiveData.put(domicile, new List < HRDataWrapper > ());
            }
            mapOfDomicileVsIncentiveData.get(domicile).addAll(lstOfIncentiveDataWrapper);
        }
        system.debug('mapOfDomicileVsIncentiveData'+mapOfDomicileVsIncentiveData);
    }
    private void createIncentiveReecordInstance(HRDataWrapper incentiveData, CalculationWrapper calculationWrapper) {
        incentiveData.TotalPayoutAmount = calculationWrapper.NewBookingAmount + calculationWrapper.RenewalAmount + calculationWrapper.NewLogoBonusAmount + calculationWrapper.ITKickersAmount + calculationWrapper.BDEAnniversaryAmount + calculationWrapper.DiscretionaryAmount + calculationWrapper.ScoreCardIncentive;
        incentiveData.TotalScoreCard = calculationWrapper.ScoreCardIncentive + calculationWrapper.DiscretionaryAmount;
        incentiveData.BDEAnniversaryAmount = calculationWrapper.BDEAnniversaryAmount;
        incentiveData.DiscretionaryAmount = calculationWrapper.DiscretionaryAmount;
        incentiveData.NewLogoBonusAmount = calculationWrapper.NewLogoBonusAmount;
        incentiveData.NewBookingAmount = calculationWrapper.NewBookingAmount;
        incentiveData.ITKickersAmount = calculationWrapper.ITKickersAmount;
        incentiveData.RenewalAmount = calculationWrapper.RenewalAmount;
        incentiveData.ScoreCardIncentive = calculationWrapper.ScoreCardIncentive;
        incentiveData.TCVAccleratorIO = calculationWrapper.TCVAccleratorIO;
        incentiveData.TCVAccleratorTS = calculationWrapper.TCVAccleratorTS;
        incentiveData.TCVAccleratorSum = calculationWrapper.TCVAccleratorSum;
        incentiveData.setOfAssociatedIncentiveRecords = calculationWrapper.setOfIncentiveIds;
    }
    private void setTargetRelatedData() {
        Date targetDate = Date.newinstance(Integer.ValueOf(processInfo.VIC_Process_Year__c), 1, 1);
        mapOfDomicileVsTargets.put('US', new List < Target__c > ());
        mapOfDomicileVsTargets.put('UK', new List < Target__c > ());
        mapOfDomicileVsTargets.put('OTHERS', new List < Target__c > ());
        Set < Id > setOfPushedTargetIds = new Set < Id > ();

        String US_REGION_DOMICILES = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('US_Region_Domiciles');
        String UK_REGION_DOMICILES = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('UK_Region_Domiciles');

        for (Target_Component__c targetComponent: Database.query(getQuery())) {
            if (!mapOfTargetIdVsTargetComponents.containsKey(targetComponent.Target__c)){
                mapOfTargetIdVsTargetComponents.put(targetComponent.Target__c, new List < Target_Component__c > ());
            }
            mapOfTargetIdVsTargetComponents.get(targetComponent.Target__c).add(targetComponent);

            if (!setOfPushedTargetIds.contains(targetComponent.Target__c)) {
                if (!mapOfUserIdVsTargets.containsKey(targetComponent.Target__r.User__r.Id)){
                    mapOfUserIdVsTargets.put(targetComponent.Target__r.User__r.Id, new List < Target__c > ());
                }
                mapOfUserIdVsTargets.get(targetComponent.Target__r.User__r.Id).add(targetComponent.Target__r);

                if (US_REGION_DOMICILES.ContainsIgnoreCase(targetComponent.Target__r.Domicile__c)) {
                    mapOfDomicileVsTargets.get('US').add(targetComponent.Target__r);
                } else if (UK_REGION_DOMICILES.ContainsIgnoreCase(targetComponent.Target__r.Domicile__c)) {
                    mapOfDomicileVsTargets.get('UK').add(targetComponent.Target__r);
                } else {
                    mapOfDomicileVsTargets.get('OTHERS').add(targetComponent.Target__r);
                }
                setOfPushedTargetIds.add(targetComponent.Target__c);
            }
        }
        system.debug('mapOfTargetIdVsTargetComponents'+mapOfTargetIdVsTargetComponents);
        system.debug('mapOfDomicileVsTargets'+mapOfDomicileVsTargets);
        system.debug('mapOfUserIdVsTargets'+mapOfUserIdVsTargets);
    }

    private String getQuery() {
        String query = 'select Id, Master_Plan_Component__r.vic_Component_Code__c, vic_HR_Pending_Amount__c, Target__r.User__r.Id, Target__c,';
        query += 'Target__r.User__r.Name, Target__r.Domicile__c, Target__r.vic_HR_Comments__c, Target__r.vic_Is_Payment_Hold_By_HR__c,';
        query += ' (Select Id, Is_Anniversary_Incentive__c, vic_Deal_Type__c, vic_Status__c, VIC_Related_Case__c, VIC_Related_Case__r.CaseNumber, vic_Incentive_Type__c, Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,';
        query += ' vic_Incentive_In_Local_Currency__c from Target_Achievements__r where ';
         if (screenName== 'HRApproval')
            query +=' vic_Status__c =: incentiveStatus';
         else if(screenName == 'VICTeamApproval')
            query +=' vic_Status__c =: incentiveStatus';
        
        if(incentiveStatus == 'HR - OnHold')
            query += ' AND VIC_Related_Case__c != null';
        
        query += ')';
        
        query += ' from Target_Component__c where Target_Status__c = \'Active\' AND Target__r.Is_Active__c = TRUE';
        query += ' AND Target__r.User__r.IsActive = true ';
        
        if (leaderName == 'SalesRep')
            query += ' AND Target__r.VIC_Plan_Code__c != \'SL_CP_Enterprise\'';
        else if (leaderName == 'SalesLeader')
            query += ' AND (Target__r.VIC_Plan_Code__c = \'SL_CP_Enterprise\' OR Target__r.VIC_Plan_Code__c = \'SEM\' ) ';
        else if(leaderName == 'Supervisor')
            query += ' AND (Target__r.VIC_Plan_Code__c = \'SL_CP_Enterprise\' OR Target__r.VIC_Plan_Code__c = \'SEM\')';
        query += ' AND Target__r.Start_date__c =: targetDate AND Target__r.Domicile__c != null ';
        
        return query;
    }

    public class HRDataWrapper {
        public Set < String > setOfAssociatedIncentiveRecords;
        public Decimal BDEAnniversaryAmount;
        public Decimal DiscretionaryAmount;
        public Decimal NewLogoBonusAmount;
        public Decimal TotalPayoutAmount;
        public Decimal TCVAccleratorIO;
        public Decimal TCVAccleratorTS;
        public Decimal ScoreCardIncentive;
        public Decimal TotalScoreCard;
        public Decimal TCVAccleratorSum;
        public Decimal NewBookingAmount;
        public Decimal ITKickersAmount;
        public String CurrencyISOCode;
        public Decimal RenewalAmount;
        public Boolean IsRejected;
        public Boolean IsOnHold;
        public String UserName;
        public String Comments;
        public String VICRole;
        public String UserId;
        public String CaseId;
        public String CaseNumber;
    }
    public class CalculationWrapper {
        public Decimal NewBookingAmount;
        public Decimal RenewalAmount;
        public Decimal NewLogoBonusAmount;
        public Decimal ITKickersAmount;
        public Decimal BDEAnniversaryAmount;
        public Decimal DiscretionaryAmount;
        public Decimal TCVAccleratorTS;
        public Decimal TCVAccleratorIO;
        public Decimal TCVAccleratorSum;
        public Decimal ScoreCardIncentive;
        public Set < String > setOfIncentiveIds;
        public CalculationWrapper() {
            NewBookingAmount = 0;
            RenewalAmount = 0;
            NewLogoBonusAmount = 0;
            ITKickersAmount = 0;
            BDEAnniversaryAmount = 0;
            DiscretionaryAmount = 0;
            TCVAccleratorTS = 0;
            TCVAccleratorIO = 0;
            ScoreCardIncentive = 0;
            setOfIncentiveIds = new Set < String > ();
        }
    }
}