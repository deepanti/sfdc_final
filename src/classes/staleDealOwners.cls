global class staleDealOwners {
    private static List<Restricted_Opportunities__c> restricted_Opp_List = new List<Restricted_Opportunities__c>();
    
    private static double Ts_Discover_retail_con;
    private static double Ts_Discover_medium_con;
    private static double Ts_Discover_large_con;
    private static double Ts_Define_retail_con;
    private static double Ts_Define_medium_con;
    private static double Ts_Define_large_con;
    
    private static double Ts_Discover_retail_anal;
    private static double Ts_Discover_medium_anal;
    private static double Ts_Discover_large_anal;
    private static double Ts_Define_retail_anal;
    private static double Ts_Define_medium_anal;
    private static double Ts_Define_large_anal;
    
    private static double Ts_Discover_retail_dig;
    private static double Ts_Discover_medium_dig;
    private static double Ts_Discover_large_dig;
    private static double Ts_Define_retail_dig;
    private static double Ts_Define_medium_dig;
    private static double Ts_Define_large_dig;
     
    public static List<User> addTsDealsUsers(List<Opportunity> oppList, String dealType){
        Set <Id> oppOwnerIdSet=new Set<id>();
        Set<Opportunity> restricted_oppty;
        try{
            if(dealType == 'TS_DEALS'){
            	restricted_oppty = getTsDeals(oppList);    
            }
            else if(dealType == 'NON_TS_DEALS'){
                restricted_oppty = getNonTsDeals(oppList);
            }           
            System.debug('restricted_oppty== '+ restricted_oppty);
            
            for(Opportunity o: restricted_oppty) { oppOwnerIdSet.add(o.OwnerId);
            }
            system.debug('oppOwnerIdSet== ' + oppOwnerIdSet);
            
            List<User> userList = [Select profileId, Has_Stale_Deals__c from User where id IN: oppOwnerIdSet and 
                                   Has_Stale_Deals__c = FALSE and profileId Not IN ('00e9000000125i2AAA','00e90000001aFVIAA2')];
            System.debug('userList in staleDealOwner==  '+userList);
            
            
            for(User uid : userList ){  uid.Has_Stale_Deals__c = True; uid.Old_Profile_Id__c = uid.profileId; uid.profileId = System.label.Profile_Stale_Deal_Owners; //Stale Deals profile
            }
            
            system.debug('restricted_Opp_List=='+restricted_Opp_List);
            insert restricted_Opp_List;
            if(userList.size() > 0){return userList;    
            }
        }
        catch(Exception e){
            System.debug('ERROR=='+e.getLineNumber()+'== '+e.getStackTraceString());
        }          
        return null;
    }
    public static void updateStaleUser(Id uid, String sessionId) {
        
        System.debug('hiii');
        System.debug('old method');
    }
    
    /*  @future(callout=true)
    public static void updateStaleUser(Id uid, String sessionId)
    {
        
    }*/
    
  /*  @Modified By : Neha Pandey
    @TL : Madhuri Sharma
    @Date Modified :7-Mar-2019
    @Description : To Update Profie from StaleDealOwner to Sales Rep New.
  */

   	@future(callout=true)
    public static void updateStaleUser(Set<Id> uid, String sessionId)
    {
       System.debug('inside stale deal future class');
        Id staleProfileId = System.label.Profile_Stale_Deal_Owners;
        
        String pipelinestages= '\'1. Discover\',\'2. Define\',\'3. On bid\',\'4. Down Select\',\'5. Confirmed\'';
        
        String Query = 'Select OwnerId, id, deal_cycle_ageing__c, Name, Opportunity_Source__c, Opportunity_Age__c ,Type_of_deal__c,StageName,Amount,Accountid,QSRM_status2__c,'+
            'Account.Name,Last_stage_change_date__c,Previous_Stage__c,TCV1__c,CloseDate,Transformation_ageing__c,QSRM_Type__c, Deal_Nature__c,'+
            'Move_Deal_Ahead__c,Cycle_Time__c,StallByPassFlag__c,Formula_Hunting_Mining__c,Deal_Type__c,Contract_Status__c, '+
            'Nature_of_Work_highest__c,Discover_MSA_CT_Check__C,Define_MSA_CT_Check__C,Onbid_MSA_CT_Check__c,'+
            'DownSelect_MSA_CT_Check__C,Type_of_deal_for_non_ts__c,Confirmed_MSA_CT_Check__C from opportunity where '+ 
            'stagename IN ('+pipelinestages+') AND Type_Of_Opportunity__c = \'TS\' '+ 
            'and (Transformation_14_days_check_age__c=\'True\' OR StallByPassFlag__c=\'True\') and Ownerid IN : uid ';
        	
        List<Opportunity> ts_oppty  = Database.query(Query);
        	
        Set<opportunity> final_ts_oppty = getTsDeals(ts_oppty);
       
        String Query1 = 'Select OwnerId,id,Name, deal_cycle_ageing__c, Opportunity_Source__c, Opportunity_Age__c ,Type_of_deal__c,StageName,Amount,Accountid,QSRM_status2__c,'+
            'Transformation_deal_ageing_for_non_ts__c,Account.Name,Last_stage_change_date__c,Previous_Stage__c,TCV1__c, Deal_Nature__c, '+
            'CloseDate,Transformation_ageing__c,QSRM_Type__c,Insight__c,CloseDatewithInsight__c,Move_Deal_Ahead__c,Contract_Status__c,'+
            'Cycle_Time__c,Type_of_deal_for_non_ts__c,Formula_Hunting_Mining__c,Deal_Type__c,Nature_of_Work_highest__c,'+
            'NonTsByPassUI__c,NonTsExceptionByPass__c,Discover_MSA_CT_Check__C,Define_MSA_CT_Check__C,Onbid_MSA_CT_Check__c,DownSelect_MSA_CT_Check__C,'+
            'Confirmed_MSA_CT_Check__C from opportunity where stagename IN ('+pipelinestages+') AND (Type_Of_Opportunity__c = \'Non Ts\' OR Type_Of_Opportunity__c = \'Non TS\')'+
            'and Transformation_14_days_check_age__c=\'True\' and Ownerid IN : uid';
       	
        List<Opportunity> non_ts_oppty = Database.query(Query1);
        
        Set<opportunity> final_non_ts_oppty = getNonTsDeals(non_ts_oppty);
        
        System.debug('Final_oppty== '+ final_Ts_oppty.size());
        System.debug('final_non_ts_oppty:== '+ final_non_ts_oppty.size());
    	System.debug('Final_non_ts_oppty:== '+ final_non_ts_oppty.size());
        
        
        List<User> userList = [Select Id,profileId,Has_Stale_Deals__c,Old_Profile_Id__c from User where ID IN: uid];
        System.debug('value of  u==='+userList);
        
        if(final_Ts_oppty.size()==0 && final_non_ts_oppty.size()==0)
        {
            
           	HttpRequest httpRequest = new HttpRequest();
            httpRequest.setMethod('POST');
            System.debug('value of  uid==='+userList[0].Id);
        
            String url = System.label.Update_Stale_User+'/updateProfile?userID='+userList[0].Id+'&ProfileID='+userList[0].Old_Profile_Id__c;
        
            httpRequest.setEndpoint(url);
            System.debug('URL ===   >>>> '+System.label.Update_Stale_User+'/updateProfile?userID='+userList[0].Id+'&ProfileID='+userList[0].Old_Profile_Id__c);
            HttpResponse httpResponse = new Http().send(httpRequest);
			System.debug('httpResponse.getBody()=='+httpResponse.getBody());

            
            /*HTTP h1 = new HTTP();
            HttpRequest request = new HttpRequest();       
            request.setEndpoint(System.label.loginUrl);
            request.setMethod('POST');
            request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            request.setHeader('SOAPAction', '""');
            request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + System.label.StaleUserName+ '</username><password>' +System.label.StalePassword    + '</password></login></Body></Envelope>');
            String SERVER_URL;
            String SESSION_ID;
            
            if(!(Test.isRunningTest()))
            {
                HTTPResponse loginResponse = h1.send(request);     
                Dom.Document doc = loginResponse.getBodyDocument();
                Dom.XMLNode receivedXml= doc.getRootElement();
                SESSION_ID = receivedXml.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/').getChildElement('loginResponse', 'urn:partner.soap.sforce.com').getChildElement('result','urn:partner.soap.sforce.com').getChildElement('sessionId','urn:partner.soap.sforce.com') .getText();  
                
                String remoteURL = System.label.domainUrl +'/services/apexrest/ProfileUpdate?_HttpMethod=PATCH';
                
                HTTPRequest httpRequest = new HTTPRequest();
                httpRequest.setEndpoint(remoteURL);
                httpRequest.setHeader('Authorization', 'OAuth ' + SESSION_ID);
                httpRequest.setHeader('Content-Type', 'application/json');
                httpRequest.setHeader('Accept', 'application/json');
                httpRequest.setMethod('POST');
                System.debug('userId:=='+uid+'==profileId=='+u.Old_Profile_Id__c);
                httpRequest.setBody('{"userId":"'+uid+'","profileId":"'+u.Old_Profile_Id__c+'"}');
                System.debug(httpRequest.getHeader('Authorization'));                
                HTTPResponse httpResponse = new Http().send(httpRequest);
                System.debug('httpRequest.getBody()=='+httpRequest.getBody());
                System.debug(httpResponse.getBody());
            }*/
        }
        System.debug('not redirected to Page==');
       // return null;
    }
    
    //To get all TS deals
    private static Set<Opportunity> getTSDeals(List<Opportunity> ts_oppty){
        dealAgeing();
        Set<Opportunity> final_ts_oppty = new Set<Opportunity>();
         for(opportunity o: ts_oppty)
        {
            System.debug('o.deal_cycle_ageing__c=='+o.deal_cycle_ageing__c);
             System.debug('o.Transformation_ageing__c=='+o.Transformation_ageing__c);
            System.debug('o.StallByPassFlag__c=='+o.StallByPassFlag__c);
            if(o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
            	if(o.Type_of_deal_for_non_ts__c == 'Retail'){
                        if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Discover_retail_dig){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Define_retail_dig){   final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Discover_retail_anal){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled'); 
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Define_retail_anal){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled'); 
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Discover_retail_con){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);   addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Define_retail_con){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                    } else if(o.Type_of_deal_for_non_ts__c == 'Medium'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Discover_medium_dig){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Define_medium_dig){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Discover_medium_anal){if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Define_medium_anal){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Discover_medium_con){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);   addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Define_medium_con){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                        
                    }else if(o.Type_of_deal_for_non_ts__c == 'Large'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Discover_large_dig){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Define_large_dig){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled'); 
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Discover_large_anal){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);  addToRestrictedOpp(o, 'TS', 'Stalled');   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Define_large_anal){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');   
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Discover_large_con){ if(o.QSRM_status2__c==0){ final_ts_oppty.add(o);   addToRestrictedOpp(o, 'TS', 'Stalled');  
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Define_large_con){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stalled');    
                                }
                            }
                        }
                        
                    }    
            }
            
            if(o.CloseDate < System.Today() && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){ final_ts_oppty.add(o); addToRestrictedOpp(o, 'TS', 'Stale');
                
            }
        }
        return final_ts_oppty;
     }
    
    private static Void dealAgeing(){
        for(Non_Ts_cycle_Time__c Dc : Non_Ts_cycle_Time__c.getall().values()){
          if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_dig = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Define_medium_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_dig = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_anal = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Define_medium_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_anal = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_con = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Define_medium_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_con = Dc.Ageing__c;
                }
            }          
        }
    }
    
    //To get all Non-TS deals
    private static Set<Opportunity> getNonTSDeals(List<Opportunity> non_ts_oppty){
        Set<Opportunity> final_non_ts_oppty = new Set<Opportunity>();
        Date today = Date.today();
        
        for(opportunity o: non_ts_oppty)
        {
            
            System.debug('o.Transformation_ageing__c nonTs=='+o.Transformation_ageing__c);
            System.debug('o.Opportunity_Source__c nonTs=='+o.Opportunity_Source__c);
            if(o.NonTsByPassUI__c!='True' && o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
                   if(o.Type_of_deal_for_non_ts__c == 'Medium'){
                       
                       Non_TS_restrcited_access__c discover_medium = Non_TS_restrcited_access__c.getValues('Discover_medium');
                       Non_TS_restrcited_access__c define_medium = Non_TS_restrcited_access__c.getValues('Define_medium');
                       Non_TS_restrcited_access__c onBid_medium = Non_TS_restrcited_access__c.getValues('On-bid_medium');
                       Non_TS_restrcited_access__c downSelect_medium = Non_TS_restrcited_access__c.getValues('Down Select_medium');
                       
                       if(o.stagename == '1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_medium.days__c){
                           if(o.QSRM_status2__c==0 ){
                               final_non_ts_oppty.add(o);     
                               addToRestrictedOpp(o, 'NON-TS', 'Stalled');    
                           }
                       }
                       else if(o.stagename == '2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_medium.days__c){    final_non_ts_oppty.add(o);        addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                       }
                       else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_medium.days__c){    final_non_ts_oppty.add(o);        addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                       }
                       else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_medium.days__c){     final_non_ts_oppty.add(o);        addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                       }
                   }
                   if(o.Type_of_deal_for_non_ts__c == 'Large'){ 
                       
                       Non_TS_restrcited_access__c discover_large = Non_TS_restrcited_access__c.getValues('Discover_large');
                       Non_TS_restrcited_access__c define_large = Non_TS_restrcited_access__c.getValues('Define_large');
                       Non_TS_restrcited_access__c onBid_large = Non_TS_restrcited_access__c.getValues('On-bid_large');
                       Non_TS_restrcited_access__c downSelect_large = Non_TS_restrcited_access__c.getValues('Down Select_large');
                       Non_TS_restrcited_access__c confirmed_large = Non_TS_restrcited_access__c.getValues('Confirmed_large');
                       
                                 
                       if(o.stagename=='1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_large.days__c ){
                           if(o.QSRM_status2__c==0 ){
                               final_non_ts_oppty.add(o);     
                               addToRestrictedOpp(o, 'NON-TS', 'Stalled');  
                           }
                       } 
                       else if(o.stageName == '2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_large.days__c){   final_non_ts_oppty.add(o);        addToRestrictedOpp(o, 'NON-TS', 'Stalled');
                       } 
                       
                       else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_large.days__c){   final_non_ts_oppty.add(o);       addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                       }
                       else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_large.days__c){    final_non_ts_oppty.add(o);        addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                       }
                      	else if(o.StageName == '5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval'))) && o.Transformation_deal_ageing_for_non_ts__c <= confirmed_large.days__c){  final_non_ts_oppty.add(o);       addToRestrictedOpp(o, 'NON-TS', 'Stalled');
                       }
                   }
                   if(o.Type_of_deal_for_non_ts__c == 'Extra Large'){ 
                                Non_TS_restrcited_access__c discover_extra_large = Non_TS_restrcited_access__c.getValues('Discover_extra_large');
                                Non_TS_restrcited_access__c define_extra_large = Non_TS_restrcited_access__c.getValues('Define_Extra_large');
                                Non_TS_restrcited_access__c onBid_extra_large = Non_TS_restrcited_access__c.getValues('On-bid_Extra_large');
                                Non_TS_restrcited_access__c downSelect_extra_large = Non_TS_restrcited_access__c.getValues('Down Select_Extra_large');
                                Non_TS_restrcited_access__c confirmed_extra_large = Non_TS_restrcited_access__c.getValues('Confirmed_Extra_large');
                                                            
                            	if(o.stagename=='1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_extra_large.days__c ){
                                    if(o.QSRM_status2__c==0 ){
                                        final_non_ts_oppty.add(o);     
                                        addToRestrictedOpp(o, 'NON-TS', 'Stalled');    
                                    }                                     
                                 } 
                                 else if(o.stageName == '2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_extra_large.days__c){	final_non_ts_oppty.add(o);     	addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                                 } 
                                else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_extra_large.days__c){ final_non_ts_oppty.add(o);     addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                                }
                                else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_extra_large.days__c){   final_non_ts_oppty.add(o);     addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                                }
                                else if(o.StageName == '5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval'))) && o.Transformation_deal_ageing_for_non_ts__c <= confirmed_extra_large.days__c){ final_non_ts_oppty.add(o);     addToRestrictedOpp(o, 'NON-TS', 'Stalled'); 
                                }
                             }
               }
            if(o.CloseDate < System.Today() && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed'  && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){ final_non_ts_oppty.add(o); addToRestrictedOpp(o, 'NON-TS', 'Stale');
            }
        }  
        return final_non_ts_oppty;
    }
    
    private static void addToRestrictedOpp(Opportunity opp, String ts_Status, String restriction_type){
        Restricted_Opportunities__c res_opp = new Restricted_Opportunities__c();
        res_opp.OpportunityID__c = opp.ID;
        res_opp.ownerID__C = opp.OwnerId;
        res_opp.TS_State__c = ts_Status;
        res_opp.Restriction_Tyepe__c = restriction_type;
        restricted_Opp_List.add(res_opp);
        
    }
}