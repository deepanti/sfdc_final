@isTest
public class GPEmployeeHRMSSyncTracker {
 @isTest
	static void teste(){        
	
        String myJSON = '{ "GP_Employee_HR_History__c":[ { "GP_Asgn_Creation_Date__c":"2015-07-12", "GP_ASGN_EFFECTIVE_END__c":"2015-12-31", "GP_ASGN_EFFECTIVE_START__c":"2015-01-02", "GP_Asgn_Last_Update_Date__c":"2016-04-12", "GP_ASSIGNMENT_STATUS__c":"Active Assignment", "GP_Band__c":"3.3", "GP_Batch_Number__c":"122028095", "GP_Business_Function__c":"Technology", "GP_CC2_Code__c":"180", "GP_CC2_Desc__c":"180-Technology", "GP_COE__c":"9581 - Headstrong (102)", "GP_COE_Code__c":"9581", "GP_Date_Probation_End__c":"", "GP_Designation__c":"Assistant Vice President", "GP_Employee_Business_Group__c":"Firmwide-Firmwide", "GP_Employees__c":"a3K0k000000K7yzEAC", "GP_Employee_Number__c":"400204403", "GP_Employee_Person_Id__c":"2680776", "GP_Employment_Category__c":"Full time", "GP_SDO_OracleId__c":"1733", "GP_Home_Payroll_Location_ID__c":"2853", "GP_Horizontal_Service_Line__c":"Unspecified", "GP_HORIZANTAL_SERVICE_LINE__c":"", "GP_HRM_OHR_ID__c":"302010525", "GP_HRM_PERSON_ID__c":"727013", "GP_Is_Senior_Delivery__c":"", "GP_Legal_Entity_Code__c":"1411", "GP_Legal_Entity_Name__c":"1411 - Headstrong Services India Pvt Ltd", "GP_Location__c":"India, Noida", "GP_Organization__c":"Headstrong Services India Private Limited (D-4 2nd floor)", "GP_Org_Id__c":"42522", "GP_Payroll_Country__c":"India", "GP_Payroll_Name__c":"CMIT-India-Monthly", "GP_Period_Of_Placement_Start__c":"", "GP_Physical_Work_Location__c":"1309111 - 1411-HS Svc I Pvt Ltd: D-4, Sec 59, Noida", "GP_Process__c":"27822700 - Temp CM -Noida D4-2-", "GP_Process_ID__c":"27822700", "GP_Projected_Assignment_End__c":"", "GP_SDO_Name__c":"1733 - HS Mgmt (102.CI)", "GP_SDO_Oracle_Id__c":"1733", "GP_Service_Line__c":"", "GP_Supervisor_Ohrid__c":"703093028", "GP_SUPERVISOR_PERSON_ID__c":"1852033", "GP_TSC_ORG_ID__c":"42522", "GP_Work_Location_Code__c":"1309111", "GP_Attribute1__c":"", "GP_Attribute2__c":"123", "GP_Attribute3__c":"1", "GP_Attribute4__c":"", "GP_Attribute5__c":"", "GP_RESOURCE_ID__c":"" }, { "GP_Asgn_Creation_Date__c":"2015-07-12", "GP_ASGN_EFFECTIVE_END__c":"2015-12-31", "GP_ASGN_EFFECTIVE_START__c":"2015-01-02", "GP_Asgn_Last_Update_Date__c":"qwert", "GP_ASSIGNMENT_STATUS__c":"Active Assignment", "GP_Band__c":"3.4", "GP_Batch_Number__c":"122028095", "GP_Business_Function__c":"Technology", "GP_CC2_Code__c":"180", "GP_CC2_Desc__c":"180-Technology", "GP_COE__c":"9581 - Headstrong (102)", "GP_COE_Code__c":"9581", "GP_Date_Probation_End__c":"", "GP_Designation__c":"Assistant Vice President", "GP_Employee_Business_Group__c":"Firmwide-Firmwide", "GP_Employees__c":"a3K0k000000K7yzEAC", "GP_Employee_Number__c":"400204403", "GP_Employee_Person_Id__c":"2680776", "GP_Employment_Category__c":"Full time", "GP_SDO_OracleId__c":"1733", "GP_Home_Payroll_Location_ID__c":"2853", "GP_Horizontal_Service_Line__c":"Unspecified", "GP_HORIZANTAL_SERVICE_LINE__c":"", "GP_HRM_OHR_ID__c":"302010525", "GP_HRM_PERSON_ID__c":"727013", "GP_Is_Senior_Delivery__c":"", "GP_Legal_Entity_Code__c":"1411", "GP_Legal_Entity_Name__c":"1411 - Headstrong Services India Pvt Ltd", "GP_Location__c":"India, Noida", "GP_Organization__c":"Headstrong Services India Private Limited (D-4 2nd floor)", "GP_Org_Id__c":"42522", "GP_Payroll_Country__c":"India", "GP_Payroll_Name__c":"CMIT-India-Monthly", "GP_Period_Of_Placement_Start__c":"", "GP_Physical_Work_Location__c":"1309111 - 1411-HS Svc I Pvt Ltd: D-4, Sec 59, Noida", "GP_Process__c":"27822700 - Temp CM -Noida D4-2-", "GP_Process_ID__c":"27822700", "GP_Projected_Assignment_End__c":"", "GP_SDO_Name__c":"1733 - HS Mgmt (102.CI)", "GP_SDO_Oracle_Id__c":"1733", "GP_Service_Line__c":"", "GP_Supervisor_Ohrid__c":"703093028", "GP_SUPERVISOR_PERSON_ID__c":"1852033", "GP_TSC_ORG_ID__c":"42522", "GP_Work_Location_Code__c":"1309111", "GP_Attribute1__c":"", "GP_Attribute2__c":"1234", "GP_Attribute3__c":"2", "GP_Attribute4__c":"", "GP_Attribute5__c":"", "GP_RESOURCE_ID__c":"" }]}';
		
        testf(myJSON);
		testf('""');
        
         String myJSONn = '<xml><name>Avinash</name></xml>';
		testf(myJSONn);
	}
    static void testf(String myJSON)
    {
        RestRequest request = new RestRequest();
        RestResponse res = new RestResponse();
        
        request.requestUri ='https://genpact--uat.cs57.my.salesforce.com/services/apexrest/EmployeeMaster';
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueof(myJSON);
        
        RestContext.response = res;
        RestContext.request = request;
        GPEmployeeHRMSSync.doPost();
    }
}