global class GPBatchTerminateWorkflow implements Database.Batchable<sObject>, Database.stateful, schedulable 
{
    
    List<GP_Project__c> finalLstPrj=new List<GP_Project__c>();
    global Database.QueryLocator start(Database.BatchableContext bc)
    {  
        return Database.getQueryLocator(System.Label.GP_Batch_AutoTerminate_WF_Query);
    }
    global void execute(Database.BatchableContext bc , List<GP_Project__c> lstProject)
    {        
        if(lstProject.size()>0)  
        {
            finalLstPrj.addAll(lstProject);
            //SendEmail(lstProject);
        }
    }
    global void finish(Database.BatchableContext bc)
    {    
        
        if(finalLstPrj.size()>0)  
        {          
            
            List<String> ThirtyTwoPidApproversIds=new List<String>();        
            Map<String,Map<String,String>> ThirtyTwoMapApproverAndEmailIds = new Map<String,Map<String,String>>();
            Map<String,List<GP_Project__c>> ThirtyTwoapproverLstProjectMap = new Map<String,List<GP_Project__c>>();        
            for(GP_project__c ThirtyTwo : finalLstPrj)
            { 
                List<String> lstThirtyTwo = ThirtyTwo.GP_Approver_Ids__c.split(';');
                for(String str : lstThirtyTwo)
                {
                    ThirtyTwoPidApproversIds.add(str);
                }
                
            }
            for(String st : ThirtyTwoPidApproversIds)
            {
                List<GP_Project__c> ThirtyTwoLstPrj=new List<GP_Project__c>();
                for(GP_project__c twe : finalLstPrj)
                {   
                    if(twe.GP_Approver_Ids__c.indexof(st) != -1)
                    {
                        ThirtyTwoLstPrj.add(twe);
                    }
                }
                ThirtyTwoapproverLstProjectMap.put(st,ThirtyTwoLstPrj);
            }
            if(ThirtyTwoPidApproversIds.size()>0)
                ThirtyTwoMapApproverAndEmailIds = GetApproverAndSupervisorAndSupEmail(ThirtyTwoPidApproversIds,32);
            
            Map < String, String > mapOfPIDNotificationActive = (Map <String, String> ) JSON.deserialize(System.label.GP_PID_Email_Notofication, Map <String, String>.class);
            system.debug('===mapOfPIDNotificationActive.get(32)==='+mapOfPIDNotificationActive.get('32'));
            if(mapOfPIDNotificationActive.get('32')=='true')
            {
                SendEmail(ThirtyTwoapproverLstProjectMap,ThirtyTwoMapApproverAndEmailIds,32);          
                if(mapOfPIDNotificationActive.get('isDiscard')=='true')
                {
                    GPDiscardWorkflows objDiscard=new GPDiscardWorkflows(finalLstPrj);
                     objDiscard.deleteProject();
                }
            }
        }
    }
    global void execute(SchedulableContext sc)
    {
        GPBatchTerminateWorkflow objTerminatedWF = new GPBatchTerminateWorkflow();
        Database.executebatch(objTerminatedWF);         
    }
    
    public static void SendEmail(Map<String,List<GP_Project__c>> approverAndlstProjectMap, 
                                 Map<String,Map<String,String>> MapApproverAndEmailIds,
                                 Integer DayCount)
    {
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        
        List<GP_Project__c> lstAllTerminatedproject=new List<GP_Project__c>();
        
        // String DeveloperName='';
        //DeveloperName = DayCount != 31 ? 'GP_PID_Approval_Workflow_Alert_For_Days':'GP_PID_Approval_Workflow_Rejected_Alert';
        EmailTemplate eTemplate = [SELECT id, DeveloperName, body, subject, htmlvalue
                                   FROM EmailTemplate 
                                   WHERE developername =: 'GP_PID_Approval_Workflow_Terminated'
                                   LIMIT 1];        
        
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();
        
        if(approverAndlstProjectMap != null)
        {
            for(String key : approverAndlstProjectMap.KeySet())
            {
                if(eTemplate != null)
                {
                    if(String.isNotBlank(eTemplate.HtmlValue) && MapApproverAndEmailIds != null) {
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        Map<String,String> toCCEmailsMap = MapApproverAndEmailIds.get(key);
                        List<String> toAddress=new List<String>();
                        Set<String> ccAddress=new Set<String>();
                        if(toCCEmailsMap != null)
                        {
                            for(String toMailKey : toCCEmailsMap.KeySet())
                            {
                                system.debug('===toMailKey==='+toMailKey + '==='+toCCEmailsMap.get(toMailKey));
                                if(toMailKey != null && toMailKey != '')
                                {
                                    
                                    toAddress.add(toMailKey);
                                    email.setToAddresses(toAddress);  
                                    if(toCCEmailsMap.get(toMailKey) != null && toCCEmailsMap.get(toMailKey) != '')
                                    {                                       
                                        ccAddress = new Set<String>(toCCEmailsMap.get(toMailKey).split(','));
                                    }
                                    for(GP_Project__c pr : approverAndlstProjectMap.get(key))
                                    {
                                        ccAddress.add(pr.GP_Current_Working_User__r.Email);
                                    }                                   
                                    //ccAddress.add('shivani.khullar@genpact.com');
                                    system.debug('===ccAddress==='+ccAddress);
                                    if(ccAddress.size()>0)
                                    {
                                        email.setccAddresses(new List<String>(ccAddress));  
                                    }
                                    //email.setBccAddresses(new String[]{'avinash.narnaware@genpact.com'});
                                    email.sethtmlBody(eTemplate.HtmlValue);
                                    
                                    //auto rejected workflow subject line
                                    if(approverAndlstProjectMap.get(key).size()==1)
                                    {
                                        email.setSubject('Workflow ' + approverAndlstProjectMap.get(key)[0].GP_EP_Project_Number__c + ' - Terminated Due To No Action');   
                                    }
                                    if(approverAndlstProjectMap.get(key).size()> 1)
                                    {
                                        email.setSubject(eTemplate.Subject);   
                                    }
                                    
                                    //email.setSubject(eTemplate.Subject);                    
                                    
                                    //email.sethtmlBody((email.getHtmlBody().replace('Day_Count',String.valueOf(DayCount))).replace('LIST_PID_RECORDS', SetMultipleWorkflowDetails(approverAndlstProjectMap.get(key))));
                                    
                                    email.sethtmlBody((email.getHtmlBody().replace('Team','Approver(s)').replace('Day_Count',String.valueOf(DayCount))));
                                    
                                    Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                                    blob csvBlob = Blob.valueOf(GetAttachmentDetails(approverAndlstProjectMap.get(key)));
                                    
                                    lstAllTerminatedproject.addAll(approverAndlstProjectMap.get(key));
                                    
                                    string csvname= 'Terminated_Workflows'+'.csv';
                                    csvAttc.setFileName(csvname);
                                    csvAttc.setBody(csvBlob);
                                    
                                    email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                                    
                                    if (oweaId != null) {
                                        email.setOrgWideEmailAddressId(oweaId);
                                    }
                                    
                                    emailNotifications.add(email);
                                }
                                
                            }
                        }                        
                        
                    }
                    
                }
            }
            
            if(lstAllTerminatedproject.size()>0 &&  eTemplate != null)
                
            {
                Messaging.SingleEmailMessage emailToDL = new Messaging.SingleEmailMessage();
                String toAddresses = system.label.GP_Genpact_PID_Governance;
                emailToDL.setToAddresses(toAddresses.split(','));
                emailToDL.sethtmlBody(eTemplate.HtmlValue);
                
                //auto rejected workflow subject line
                if(lstAllTerminatedproject.size()==1)
                {
                    emailToDL.setSubject('Workflow ' + lstAllTerminatedproject[0].GP_EP_Project_Number__c + ' - Terminated Due To No Action');   
                }
                if(lstAllTerminatedproject.size()> 1)
                {
                    emailToDL.setSubject(eTemplate.Subject);   
                }
                //email.sethtmlBody((email.getHtmlBody().replace('Day_Count',String.valueOf(DayCount))).replace('LIST_PID_RECORDS', SetMultipleWorkflowDetails(approverAndlstProjectMap.get(key))));
                
                emailToDL.sethtmlBody((emailToDL.getHtmlBody().replace('Day_Count',String.valueOf(DayCount))));
                
                Messaging.EmailFileAttachment csvterminatedAttc = new Messaging.EmailFileAttachment();
                blob csvBlob = Blob.valueOf(GetAttachmentDetails(lstAllTerminatedproject));
                string csvname= 'Terminated_Workflows'+'.csv';
                csvterminatedAttc.setFileName(csvname);
                csvterminatedAttc.setBody(csvBlob);
                
                emailToDL.setFileAttachments(new Messaging.EmailFileAttachment[]{csvterminatedAttc});
                
                if (oweaId != null) {
                    emailToDL.setOrgWideEmailAddressId(oweaId);
                }
                
                emailNotifications.add(emailToDL);
            }
        }
        system.debug('===emailNotifications==='+emailNotifications.size());
        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
        
    }   
    
    
    public static String GetAttachmentDetails(List<GP_Project__c> lstPrj)
    {
        string header = 'SN , Workflow ID , Project Number , Workflow Type, SDO , Project Type , Business Name, Start Date, End Date , Org Name , Workflow Submitted Date , Workflow Status , Workflow Requestor-Name , Workflow Requestor-OHR ID , PID Approver Name , PID Approver OHR ID , Ageing    \n';
        string finalstr = header ;
        Integer i=1;
        for(GP_Project__c prj : lstPrj)
        {
            String ProjectStartDate='';
            if(prj.GP_Start_Date__c != null)
            { ProjectStartDate = String.valueOf(prj.GP_Start_Date__c).split(' ')[0]; } 
            
            String ProjectEndDate='';
            if(prj.GP_End_Date__c != null)
            { ProjectEndDate = String.valueOf(prj.GP_End_Date__c).split(' ')[0]; } 
            
             String PrimarySDOName='';
            if(prj.GP_Primary_SDO__c != null)
            { PrimarySDOName = prj.GP_Primary_SDO__r.Name.replace('–','-'); }
            
            String ProjectOrgName='';
            if(prj.GP_Project_Organization__c != null)
            { ProjectOrgName = prj.GP_Project_Organization__r.Name; }
          
            
            
            string recordString = i +','+ prj.GP_EP_Project_Number__c+','
                + prj.GP_Oracle_PID__c+','
                + (prj.GP_Oracle_PID__c == 'NA' ? 'Creation': 'Modification') +','
                + '"'+ PrimarySDOName +'"' +','
                + prj.GP_Project_type__c+','
                + prj.GP_Business_Name__c+','
                + ProjectStartDate+','
                + ProjectEndDate+','
                + '"'+ ProjectOrgName  +'"' +','                
                + String.ValueOf(prj.GP_Project_Submission_Date__c).split(' ')[0] +','    
                + 'Auto-Terminated' + ','
                + '"'+ prj.GP_Current_Working_User__r.Name +'"' +','
                + prj.GP_Current_Working_User__r.OHR_ID__c + ','
                + '"'+ prj.GP_PID_Approver_User__r.Name   +'"' +',' 
                + prj.GP_PID_Approver_User__r.OHR_ID__c + ','
                + '32' +'\n';
            
            finalstr = finalstr +recordString;
            i++;
        }
        return finalstr;
    }
    public static Map<String,Map<String,String>> GetApproverAndSupervisorAndSupEmail(List<String> pidApproverIds,Integer Days)
    {
        //system.debug('===pidApproverIds==='+pidApproverIds);
        //system.debug('===Days==='+Days);
        Map<String,Map<String,String>> MapApproverAndEmailIds=new Map<String,Map<String,String>>(); 
        
        List<GP_Employee_Master__c> lstEmpMaster=[Select Id,
                                                  GP_OFFICIAL_EMAIL_ADDRESS__c,                                                   
                                                  GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c , 
                                                  GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c, 
                                                  GP_SFDC_User__c 
                                                  from GP_Employee_Master__c                                                  
                                                  Where GP_SFDC_User__c in: pidApproverIds and GP_Person_ID__c != null];
        
        system.debug('===lstEmpMaster==='+lstEmpMaster.size());
        for(GP_Employee_Master__c emp : lstEmpMaster)
        {
            String toemailIds='';
            String ccemailIds='';
            if(emp.GP_OFFICIAL_EMAIL_ADDRESS__c != null &&  Days==32)
            {
                toemailIds = emp.GP_OFFICIAL_EMAIL_ADDRESS__c;
            }
            if(emp.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c  != null && Days==32)
            {
                ccemailIds = emp.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c;
            }
            
            Map<String,String> MapToCcEmailIds=new Map<String,String>();
            if(toemailIds != '' || ccemailIds !='')
            {                
                MapToCcEmailIds.put(toemailIds,ccemailIds);
            }
            
            if(MapToCcEmailIds != null && !MapToCcEmailIds.isEmpty())
            {
                MapApproverAndEmailIds.put(emp.GP_SFDC_User__c, MapToCcEmailIds );
            }
            
        }      
        
        
        return MapApproverAndEmailIds;
    }
}