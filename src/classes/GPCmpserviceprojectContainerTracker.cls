/*=======================================================================================
Description: Common Test Data Class containing all test data for every sObject

=======================================================================================
Version#     Date                           Author                    Description
=======================================================================================
1.0          10-May-2018                   Ved Prakash            Initial Version 
=======================================================================================  
*/
@isTest public class GPCmpserviceprojectContainerTracker {
	public static GP_Project__c prjObj;    
    
    static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
         Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
 
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        GP_Project_Template__c indirectTemp = GPCommonTracker.getProjectTemplate();
        indirectTemp.GP_Active__c = true;
        indirectTemp.Name = 'INDIRECT_INDIRECT';
        indirectTemp.GP_Business_Type__c = 'Indirect';
        indirectTemp.GP_Business_Name__c = 'Indirect';
        indirectTemp.GP_Business_Group_L1__c = 'All';
        indirectTemp.GP_Business_Segment_L2__c = 'All';
        insert indirectTemp;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',indirectTemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert prjObj ;
    }
	
    @isTest public static void testgetProjectContainerStatus()
    {
        setupCommonData();
        GPAuraResponse obj = GPCmpserviceprojectContainer.getProjectContainerStatus(prjObj.id,true);
        obj = GPCmpserviceprojectContainer.getProjectContainerStatus(prjObj.id,false);
        obj = GPCmpserviceprojectContainer.getProjectContainerStatus(null,false);
        
    }
}