@istest
public class CheckRecursiveTest {
    
    private static testmethod void testRecursion(){
        System.assert(checkRecursive.runOnce(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.runOnce(), 'Recursion check failed. Please review checkRecursive recursion logic');
        
        System.assert(checkRecursive.runBeforeDelete(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.runBeforeDelete(), 'Recursion check failed. Please review checkRecursive recursion logic');
   
        System.assert(checkRecursive.afterInsertMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.afterInsertMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
        
        System.assert(checkRecursive.afterUpdateMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.afterUpdateMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
        
        System.assert(checkRecursive.afterDeleteMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.afterDeleteMethod(), 'Recursion check failed. Please review checkRecursive recursion logic');
   
        System.assert(checkRecursive.runOncePinnacle(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.runOncePinnacle(), 'Recursion check failed. Please review checkRecursive recursion logic');
        
        System.assert(checkRecursive.runOnceOliPinnacle(), 'Recursion check failed. Please review checkRecursive recursion logic');
        System.assert(!checkRecursive.runOnceOliPinnacle(), 'Recursion check failed. Please review checkRecursive recursion logic');
   
    }
    
}