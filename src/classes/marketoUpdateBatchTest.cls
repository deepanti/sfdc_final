@isTest
public class marketoUpdateBatchTest {
    public static testMethod void testBatch() {
        
        // Create 200 test accounts - this simulates one execute.  
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount;
        System.runAs(u){
            oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                        oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
        }
        Test.StartTest();
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Account Where name=\'' + oAccount.name+ '\'';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        Test.StopTest();
        
    }
    public static testMethod void testBatch2() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount;
        Contact oContact;   
        System.runAs(u){
            oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                        oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                        'test121@xyz.com','99999999999');
        }
        Test.StartTest();
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Contact Where Id=\'' + oContact.Id+ '\'';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        Test.StopTest();
        
    }
    public static testMethod void testBatch3() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount;
        Contact oContact; 
        Opportunity opp;
        System.runAs(u){
            oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                        oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                        'test121@xyz.com','99999999999');
        }
        Test.StartTest();
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
        
        opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                             Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                             Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        insert opp;
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Opportunity Where name=\'' + opp.name+ '\'';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        Test.StopTest();
        
    }
     public static testMethod void testBatch4() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        Lead Ld;
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
             
        System.runAs(u){
           Ld=new Lead(FirstName='Mukesh',LastName='Kumar',Company='wipro',Title='Manager',Email='Mukesh@gmail.com',Country_Of_Residence__c='Aruba',Status='Raw');
            insert Ld;
        }
        Test.StartTest();
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Lead Where Id=\'' + Ld.Id+ '\'';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        Test.StopTest();
        
    }
    public static testMethod void testBatch5() {
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Campaign camp;
             
        System.runAs(u){
           camp=new Campaign(name='campaign test');
            insert camp;
        }
        Test.StartTest();
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='SELECT Id,Sync_to_Marketo__c FROM Campaign Where Id=\'' + camp.Id+ '\'';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        Test.StopTest();
        
    }
    public static testMethod void testBatch6() {
        
        Test.StartTest();
        try{
        marketoUpdateBatch batctRef=new marketoUpdateBatch();
        batctRef.query='Select Id from OpportunityLineItem Limit 10';
        ID batchprocessid = Database.executeBatch(batctRef,200);
        }
        catch(Exception e)
        {
            system.debug('Error'+e);
        }
        Test.StopTest();
        
    }
}