/**
 * @group ServiceLayer. 
 *
 * @description Service layer for Timesheet Transaction.
 */
public with sharing class GPServiceTimesheetTransaction {

    public class GPServiceTimesheetTransactionException extends Exception {}

    public static final String IS_VALID_PROJECT_TASK_ERROR_LABEL = 'Project Task Selected is not active for the Date;';
    public static final String IS_VALID_FOR_CREATION_LABEL = 'Already a draft entry is there in the system.';
    public static final String IS_SYNCED_IN_ORACLE_LABEL = 'Cant Be updated as locked in Oracle;';
    public static final String IS_VALID_FOR_DURATION_ERROR_LABEL = 'Not Allocated For Duration;';
    public static final String IS_VALID_PROJECT_ERROR_LABEL = 'Project is Closed;';
    public static final String IS_VALID_ASSIGNMENT_ERROR_LABEL = 'Not Assigned;';
    public static final String IS_VALID_ALLOCATION_ERROR_LABEL = 'Not Allocated;';
    public static final String IS_VALID_HOURS_ERROR_LABEL = 'Invalid Hours;';
    public static final String IS_VALID_FOR_DURATION = 'isValidForDuration';
    public static final String IS_VALID_ASSIGNMENT = 'isValidAssignment';
    public static final String IS_VALID_ALLOCATION = 'isValidAllocation';
    public static final String IS_VALID_PROJECT = 'IsValidProject';
    public static final String IS_VALID_HOURS = 'isValidHours';

    public static Map < String, Map < String, GP_Project_Task__c >> mapOfProjectIdVsAssociatedActiveTasks = new Map < String, Map < String, GP_Project_Task__c >> ();
    public static Map < String, Boolean > mapOfUploadTimeSheetIdVsIsValidForDuration = new Map < String, Boolean > ();
    public static Map < String, Boolean > mapOfEmployeeProjectVsIsValidAllocation = new Map < String, Boolean > ();
    public static Map < String, Boolean > mapOfEmployeeIdVsIsValidAssignment = new Map < String, Boolean > ();
    public static Map < String, Boolean > mapOfEmployeeMonthYearIsValidEntry = new map < String, Boolean > ();
    public static Map < String, Boolean > mapOfEmployeeDateVsIsValidHours = new Map < String, Boolean > ();
    public static Map < String, Boolean > mapOfProjectIdVsIsValidProject = new Map < String, Boolean > ();
    public static Map < String, Decimal > mapOfEmployeeDayVsHours = new Map < String, Decimal > ();

    public static List < GP_Timesheet_Entry__c > listOfTimeSheetEntries = new List < GP_Timesheet_Entry__c > ();
    public static Set < String > setOfEmployeeMonthYears = new Set < String > ();
    public static Set < Id > setOfEmployeeId = new Set < Id > ();
    public static Set < String > setOfProjectOraclePId = new Set < String > ();

    /**
     * @description Validates whether the timesheet correction is being done by Time sheet Admin user or Not.
     *
     * @param objLoggedInUserId : Logged In user SFDC Id.
     * 
     * @return BOOLEAN : Return whether logged in User is valid to edit the timesheet data.
     */
    public static Boolean validateLoggedinUser(Id objLoggedInUserId) {
        Map < String, String > mapOfRoleAccessibility = GPCommon.getMapOfScreenAccess(new List<String>{'timesheetcorrection'}, userInfo.getUserId());
       	return mapOfRoleAccessibility != null && mapOfRoleAccessibility.containsKey('timesheetcorrection') && (mapOfRoleAccessibility.get('timesheetcorrection') == 'edit' ||
            mapOfRoleAccessibility.get('timesheetcorrection') == 'modify' || mapOfRoleAccessibility.get('timesheetcorrection') == 'read' ) ? true : false;
        /* GP_Role__c objRole = [SELECT Id FROM GP_Role__c where id in
            (SELECT GP_Role__c FROM GP_User_Role__c where GP_User__c =: objLoggedInUserId and GP_Active__c = true and GP_Role__r.Name = 'TimeSheet Administrator')
        ];
        if (objRole != null)
            return false;

        return true;*/
    }

    /*
     * Description : Validates whether the selected Employee has Allocation 
     *               Status = 'Active Assignment' in the Employee HR History
     *               Object related to Employee Master object.
     */

    //public static Map < String, Boolean > validateActiveAssignment(Set < Id > setOfEmployeeIds) {

    //    Map < String, Boolean > mapOfEmployeeIdVsIsValidAssignment = new Map < String, Boolean > ();
    //    for (GP_Employee_Master__c objEmployeeMaster: [select id, GP_Employee_HR_History__r.GP_Allocation_Status__c from GP_Employee_Master__c
    //            where ID in: setOfEmployeeIds
    //        ]) {
    //        if (objEmployeeMaster.GP_Employee_HR_History__r.GP_Allocation_Status__c == 'Active Assignment')
    //            mapOfEmployeeIdVsIsValidAssignment.put(objEmployeeMaster.Id, true);
    //        else
    //            mapOfEmployeeIdVsIsValidAssignment.put(objEmployeeMaster.Id, false);
    //    }
    //    return mapOfEmployeeIdVsIsValidAssignment;

    //    return null;
    //}


    /**
     * @description Validates whether the employee is allocated in the selected project or not.
     *
     * @param mapOfEmployeeProjectIdVsTimeSheetEntryDate : Map Of Employee-Project Vs its List of timesheet entries date which needs to be validated.
     * 
     * @return Map < String, Boolean > : Map of Employee-Project and its allocation status for the project.
     */
    private static Map < String, Boolean > validateValidEmployee(Map < String, List < Date >> mapOfEmployeeProjectIdVsTimeSheetEntryDate) {
        String employeeProjectKey, key;
        //Manage multiple entries for same combination of date and employee.
        Map < String, Boolean > mapOfEmployeeProjectVsIsValidAllocation = new Map < String, Boolean > ();
        for (GP_Resource_Allocation__c objResourceAllocation: new GPSelectorResourceAllocation().selectResourceAllocationRecordsOfEmployeesAndProjects(setOfEmployeeId, setOfProjectOraclePId)) {

            employeeProjectKey = objResourceAllocation.GP_Employee__c + '@@' + objResourceAllocation.GP_Project__r.GP_Oracle_PID__C;

            if (!mapOfEmployeeProjectIdVsTimeSheetEntryDate.containsKey(employeeProjectKey))
                continue;
            for (Date recordDate: mapOfEmployeeProjectIdVsTimeSheetEntryDate.get(employeeProjectKey)) {

                key = objResourceAllocation.GP_Employee__c + '@@' + objResourceAllocation.GP_Project__r.GP_Oracle_PID__c + '@@' + recordDate;

                if ((objResourceAllocation.GP_Project__r.RecordType.Name != 'Indirect PID' && objResourceAllocation.GP_Project__r.RecordType.Name != 'BPM') &&
                    !(recordDate != null && recordDate <= objResourceAllocation.GP_End_Date__c &&
                        recordDate >= objResourceAllocation.GP_Start_Date__c)) {
                    if (!mapOfEmployeeProjectVsIsValidAllocation.containsKey(key))
                        mapOfEmployeeProjectVsIsValidAllocation.put(key, false);
                } else {
                    if (!mapOfEmployeeProjectVsIsValidAllocation.containsKey(key))
                        mapOfEmployeeProjectVsIsValidAllocation.put(key, true);
                }

            }
        }
        return mapOfEmployeeProjectVsIsValidAllocation;
    }

    /**
     * @description Validates whether the timesheet has been modified for an closed project.
     * 
     * @return Map < String, Boolean > : Map of Project and its status.
     */ //to do change complete..only approved project task should be fetched this also has to be managed.
    private static Map < String, Boolean > validateClosedPID() {

        Map < String, Boolean > mapOfProjectIdVsIsValidProject = new Map < String, Boolean > ();

        for (GP_Project__c objProject: new GPSelectorProject().getProjectAndProjectTasks(setOfProjectOraclePId)) { //[select id,GP_Project_Status__c,GP_Is_Closed__c from GP_Project__c where id in : setOfPrjectId]){
            mapOfProjectIdVsIsValidProject.put(objProject.GP_Oracle_PID__c, !objProject.GP_Is_Closed__c);
        }

        for (GP_Project_Task__c projectTask: new GPSelectorProjectTask().getListOfProjectTaskForSetOfProjectIds(mapOfProjectIdVsIsValidProject.KeySet())) {
            if (!mapOfProjectIdVsAssociatedActiveTasks.containsKey(projectTask.GP_Project_Number__c))
                mapOfProjectIdVsAssociatedActiveTasks.put(projectTask.GP_Project_Number__c, new Map < String, GP_Project_Task__c > ());

            mapOfProjectIdVsAssociatedActiveTasks.get(projectTask.GP_Project_Number__c).put(projectTask.GP_Task_Number__c, projectTask);
        }

        return mapOfProjectIdVsIsValidProject;
    }

    /**
     * @description Validates whether the timesheet has been added or 
     *              modified outside the start date and end date of resource allocation.
     * 
     */
    public static Boolean validateTimsheetAgainstResourceAllocation() {
        //Similar to valid Employee achievable from previous method.
        return true;
    }

    /*
     * Description : Validates the minimum timesheet hour entries 
     *               for employee bassed on employee type i.e Full Time, Contractor
     */

    //public static Map < String, Boolean > validateMinTimeSheetHours(Map < String, Decimal > mapOfEmployeeDayVsHours,
    //    Map < Id, Contact > mapOfEmployee,
    //    Map < String, Integer > mapOfEmployeeTypeVsMinHours,
    //    Map < String, Integer > mapOfEmployeeTypeVsMaxHours) {

    //    //Todo Type of employee specific..
    //    Map < String, Boolean > mapOfUploadTimeSheetIdVsIsValidForDuration = new Map < String, Boolean > ();
    //    Map < String, Decimal > mapOfEmployeeDayVsHoursEnteredInDB = new Map < String, Decimal > ();
    //    for (GP_Timesheet_Entry__c objTimeSheetEntry: [select id, GP_Project__c, GP_Project__r.Name, GP_Actual_Hours__c, GP_Modified_Hours__c,
    //            GP_Ex_Type__c, GP_Date__c, GP_Employee__c
    //            from GP_Timesheet_Entry__c
    //            where GP_Employee__c in: mapOfEmployee.keySet()
    //        ]) {
    //        String Key = objTimeSheetEntry.GP_Employee__c + '@@' + objTimeSheetEntry.GP_Date__c;
    //        if (mapOfEmployeeDayVsHoursEnteredInDB.containsKey(key))
    //            mapOfEmployeeDayVsHoursEnteredInDB.put(Key, mapOfEmployeeDayVsHoursEnteredInDB.get(Key) + objTimeSheetEntry.GP_Actual_Hours__c);
    //    }
    //    system.debug('mapOfEmployeeDayVsHoursEnteredInDB' + mapOfEmployeeDayVsHoursEnteredInDB);
    //    for (String employeeDayCombi: mapOfEmployeeDayVsHours.keySet()) {
    //        if (mapOfEmployeeDayVsHoursEnteredInDB.containsKey(employeeDayCombi))
    //            mapOfEmployeeDayVsHoursEnteredInDB.put(employeeDayCombi, mapOfEmployeeDayVsHoursEnteredInDB.get(employeeDayCombi) +
    //                mapOfEmployeeDayVsHours.get(employeeDayCombi));
    //        else
    //            mapOfEmployeeDayVsHoursEnteredInDB.put(employeeDayCombi, mapOfEmployeeDayVsHours.get(employeeDayCombi));
    //    }
    //    for (String employeeDay: mapOfEmployeeDayVsHoursEnteredInDB.keySet()) {
    //        String empId = employeeDay.split('@@')[0];
    //        Integer minHour = 8; //mapOfEmployeeTypeVsMinHours.get(mapOfEmployee.get(empId).GP_Type__c);
    //        Integer maxHour = 24; //mapOfEmployeeTypeVsMaxHours.get(mapOfEmployee.get(empId).GP_Type__c);
    //        if (mapOfEmployeeDayVsHoursEnteredInDB.get(employeeDay) >= minHour && mapOfEmployeeDayVsHoursEnteredInDB.get(employeeDay) <= maxHour)
    //            mapOfUploadTimeSheetIdVsIsValidForDuration.put(employeeDay, true);
    //    }
    //    return mapOfUploadTimeSheetIdVsIsValidForDuration;

    //    return null;
    //}

    /**
     * @description Create common map for all validation results for the timesheet entry record.
     *
     * @param objTimeSheetEntry : Timesheet Entry that needs to be validated.
     * 
     * @return Map < String, Boolean > : Map of Validation status for the timesheet entry record.
     */
    private static Map < String, Boolean > getValidChecks(GP_Timesheet_Entry__c objTimeSheetEntry) {
        Map < String, Boolean > mapOfChecks = new Map < String, Boolean > ();
        // mapOfChecks.put(IS_VALID_ASSIGNMENT,getIsValid (mapOfEmployeeIdVsIsValidAssignment, 
        //   objTimeSheetEntry.GP_Employee__r.Id));
        system.debug('objTimeSheetEntry.GP_Project__r.RecordType.Name' + objTimeSheetEntry.GP_Project__r.RecordType.Name);
        Boolean isvalidWrtResourceAllocation = (objTimeSheetEntry.GP_Project__r.RecordType.Name != 'Indirect PID' &&
            objTimeSheetEntry.GP_Project__r.RecordType.Name != 'BPM') ? getIsValid(mapOfEmployeeProjectVsIsValidAllocation,
            objTimeSheetEntry.GP_Employee__r.Id +
            '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c + '@@' +
            objTimeSheetEntry.GP_Date__c) : true;
        system.debug('isvalidWrtResourceAllocation' + isvalidWrtResourceAllocation);
        mapOfChecks.put(IS_VALID_ALLOCATION, isvalidWrtResourceAllocation);
        mapOfChecks.put(IS_VALID_PROJECT, getIsValid(mapOfProjectIdVsIsValidProject,
            objTimeSheetEntry.GP_Project_Oracle_PID__c));
        // mapOfChecks.put(IS_VALID_FOR_DURATION,getIsValid (mapOfUploadTimeSheetIdVsIsValidForDuration, 
        //                          objTimeSheetEntry.id));
        //mapOfChecks.put(IS_VALID_HOURS,getIsValid (mapOfEmployeeDateVsIsValidHours, 
        //  objTimeSheetEntry.GP_Employee__c 
        //  + '@@' + objTimeSheetEntry.GP_Date__c));
        DateTime entryDate = objTimeSheetEntry.GP_Date__c;
        String monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);
        String key = objTimeSheetEntry.GP_Employee__r.GP_Person_Id__c + monthYear;

        mapOfChecks.put(IS_VALID_FOR_CREATION_LABEL, getIsValid(mapOfEmployeeMonthYearIsValidEntry, key));

        return mapOfChecks;
    }

    /**
     * @description Consolidated validation result for timehsheet entry records.
     *
     * @param mapOfChecks : Map of all validation results for the timesheet entry records.
     * 
     * @return Boolean : Return whether it fails/pass all the validations.
     */
    private static Boolean isValidForRecordCreation(Map < String, Boolean > mapOfChecks) {
        return (mapOfChecks.get(IS_VALID_ALLOCATION) &&
            mapOfChecks.get(IS_VALID_PROJECT) &&
            mapOfChecks.get(IS_VALID_FOR_CREATION_LABEL));

    }

    /**
     * @description Return Validated list of timesheet entry records for Mass upload timesheet module.
     *
     * @param listOfTimeSheetEntries : List of timesheet entry records that need to be validated.
     * @param mapOfUniqueTimeSheetRecords : Map of unique temporary data.
     * @param setOfErrorIds : Set of Error records of temporary data.
     * @param setOfEmployeeIds : Set of employee Ids for which timesheet entry data has been uploaded.
     * @param setOfProjectIds : Set of project Ids for which timesheet entry data has been uploaded.
     * @param setOfEmployeeMonthYears : Set of employee-month-year for which timesheet entry data has been uploaded.
     * 
     * @return List < GP_Timesheet_Entry__c > : Return List of validated timesheet entry records.
     */
    public static List < GP_Timesheet_Entry__c > getValidatedTimeSheetEntriesLst(List < GP_Timesheet_Entry__c > listOfTimeSheetEntries,
        Map < String, GP_Temporary_Data__c > mapOfUniqueTimeSheetRecords,
        Set < Id > setOfErrorIds,
        Set < Id > setOfEmployeeIds,
        Set < String > setOfProjectOraclePIds,
        Set < String > setOfEmployeeMonthYears) {
        setClassVariables(setOfEmployeeMonthYears, setOfEmployeeIds, setOfProjectOraclePIds, listOfTimeSheetEntries);
        mapOfEmployeeProjectVsIsValidAllocation = getMapOfEmployeeProjectVsIsValidAllocation();
        system.debug('mapOfEmployeeProjectVsIsValidAllocation' + mapOfEmployeeProjectVsIsValidAllocation);
        mapOfProjectIdVsIsValidProject = validateClosedPID();
        system.debug('mapOfProjectIdVsIsValidProject' + mapOfProjectIdVsIsValidProject);
        List < GP_Pinnacle_Master__c > lstOfPinnacleMasterRecord = GPSelectorPinnacleMasters.selectGlobalSettingRecord();
        system.debug('lstOfPinnacleMasterRecord' + lstOfPinnacleMasterRecord);
        mapOfEmployeeMonthYearIsValidEntry = getMapOfEmployeeMonthYearIsValidEntry();
        system.debug('mapOfEmployeeMonthYearIsValidEntry' + mapOfEmployeeMonthYearIsValidEntry);
        List < GP_Timesheet_Entry__c > lstTimeSheetEntries = new List < GP_Timesheet_Entry__c > ();

        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntries) {
            String key = objTimeSheetEntry.GP_Employee__r.GP_Final_OHR__c + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c + '@@';
            key += String.valueOf(Date.valueOf(objTimeSheetEntry.GP_Date__c)) + '@@' + objTimeSheetEntry.GP_Ex_Type__c;
            Map < String, Boolean > mapOfChecks = getValidChecks(objTimeSheetEntry);
            system.debug('mapOfChecks' + mapOfChecks);
            system.debug('mapOfProjectIdVsAssociatedActiveTasks' + mapOfProjectIdVsAssociatedActiveTasks);
            if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                //mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c = '';
                setRemarksField(mapOfUniqueTimeSheetRecords.get(key), mapOfChecks);
            }
            if (isValidForRecordCreation(mapOfChecks)) {
                if (lstOfPinnacleMasterRecord.size() > 0) {
                    if (lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c < objTimeSheetEntry.GP_Date__c) {

                        if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                            setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                            mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c += IS_SYNCED_IN_ORACLE_LABEL;
                            mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                        }
                    } else if ( //objTimeSheetEntry.GP_Employee__r.GP_EMPLOYEE_TYPE__c.equalsIgnoreCase('Full Time') &&
                        !(objTimeSheetEntry.GP_Modified_Hours__c >= lstOfPinnacleMasterRecord[0].GP_Full_Time_Emp_Min_Hrs__c &&
                            objTimeSheetEntry.GP_Modified_Hours__c <= lstOfPinnacleMasterRecord[0].GP_Full_Time_Emp_Max_Hrs__c)) {

                        if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                            setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                            mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                            mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c += IS_VALID_HOURS_ERROR_LABEL;
                        }
                    } else if (objTimeSheetEntry.GP_Employee__r != null && objTimeSheetEntry.GP_Employee__r.GP_EMPLOYEE_TYPE__c != null && objTimeSheetEntry.GP_Employee__r.GP_EMPLOYEE_TYPE__c.equalsIgnoreCase('Contingent Worker') &&
                        !(objTimeSheetEntry.GP_Modified_Hours__c >= lstOfPinnacleMasterRecord[0].GP_Contractor_Emp_Min_Hrs__c &&
                            objTimeSheetEntry.GP_Modified_Hours__c <= lstOfPinnacleMasterRecord[0].GP_Contractor_Emp_Max_Hrs__c)) {

                        if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                            setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                            mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                            mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c += IS_VALID_HOURS_ERROR_LABEL;
                        }

                    } else if (mapOfProjectIdVsAssociatedActiveTasks.containsKey(objTimeSheetEntry.GP_Project_Oracle_PID__c)) {
                        if (mapOfProjectIdVsAssociatedActiveTasks.get(objTimeSheetEntry.GP_Project_Oracle_PID__c).containsKey(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c)) {
                            GP_Project_Task__c projectTask = mapOfProjectIdVsAssociatedActiveTasks.get(objTimeSheetEntry.GP_Project_Oracle_PID__c).get(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c);
                            if (objTimeSheetEntry.GP_Date__c < projectTask.GP_Start_Date__c &&
                                (projectTask.GP_End_Date__c == null || objTimeSheetEntry.GP_Date__c < projectTask.GP_End_Date__c)) {
                                if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                                    setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                                    mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                                    mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c += IS_VALID_PROJECT_TASK_ERROR_LABEL;
                                }
                            } else {
                                lstTimeSheetEntries.add(objTimeSheetEntry);
                            }
                        } else {
                            if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                                setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                                mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                                mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c += IS_VALID_PROJECT_TASK_ERROR_LABEL;
                            }
                        }
                    } else {
                        lstTimeSheetEntries.add(objTimeSheetEntry);
                    }
                } else {

                    if (mapOfUniqueTimeSheetRecords.containsKey(key)) {
                        mapOfUniqueTimeSheetRecords.get(key).GP_Validation_Result__c = 'Global Setting Record not configured!';
                        mapOfUniqueTimeSheetRecords.get(key).GP_Is_Failed__c = true;
                        setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
                    }
                }
            } else {

                if (mapOfUniqueTimeSheetRecords.containsKey(key))
                    setOfErrorIds.add(mapOfUniqueTimeSheetRecords.get(key).id);
            }
        }
        system.debug('mapOfUniqueTimeSheetRecords' + mapOfUniqueTimeSheetRecords);
        return lstTimeSheetEntries;
    }

    /**
     * @description Set Remarks field of tempoarary data(Validation messages).
     *
     * @param objTemporaryTimeSheetEntry : Temporary record which has to be uploaded.
     * @param mapOfChecks : Map of all validation results for the timesheet entry records.
     * 
     */
    private static void setRemarksField(GP_Temporary_Data__c objTemporaryTimeSheetEntry, Map < String, Boolean > mapOfChecks) {
        //if (objTemporaryTimeSheetEntry.GP_Validation_Result__c == null)
        //objTemporaryTimeSheetEntry.GP_Validation_Result__c = '';
        //if (!mapOfChecks.get(IS_VALID_ASSIGNMENT))
        //    objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_ASSIGNMENT_ERROR_LABEL;
        if (!mapOfChecks.get(IS_VALID_ALLOCATION)) {
            objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_ALLOCATION_ERROR_LABEL;
            objTemporaryTimeSheetEntry.GP_Is_Failed__c = true;
        }
        if (!mapOfChecks.get(IS_VALID_PROJECT)) {
            objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_PROJECT_ERROR_LABEL;
            objTemporaryTimeSheetEntry.GP_Is_Failed__c = true;
        }
        if (!mapOfChecks.get(IS_VALID_FOR_CREATION_LABEL)) {
            objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_FOR_CREATION_LABEL;
            objTemporaryTimeSheetEntry.GP_Is_Failed__c = true;
        }

        //if (!mapOfChecks.get(IS_VALID_FOR_DURATION))
        //    objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_FOR_DURATION_ERROR_LABEL;
        //if (!mapOfChecks.get(IS_VALID_HOURS))
        //    objTemporaryTimeSheetEntry.GP_Validation_Result__c += IS_VALID_HOURS_ERROR_LABEL;
    }

    /**
     * @description Get whether the record id valid for creation for corresponding key.
     *
     * @param mapToCheckIsValid : Map for which validation result needs to be returned.
     * @param key : Key for which value needs to be returned.
     * 
     */
    private static Boolean getIsValid(Map < String, Boolean > mapToCheckIsValid, String key) {
        return (mapToCheckIsValid.containsKey(key) ? mapToCheckIsValid.get(key) : false);
    }

    /**
     * @description Get Map of employee-project vs Valid allocation status.
     * 
     * @return Map < String, Boolean > : Return validated map of Employee-Project allocation.
     */
    private static Map < String, Boolean > getMapOfEmployeeProjectVsIsValidAllocation() {
        Map < String, List < Date >> mapOfEmployeeProjectIdVsTimeSheetEntryDate = new Map < String, List < Date >> ();
        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntries) {
            String Key = objTimeSheetEntry.GP_Employee__r.Id + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c;
            if (mapOfEmployeeProjectIdVsTimeSheetEntryDate.containsKey(key))
                mapOfEmployeeProjectIdVsTimeSheetEntryDate.get(Key).add(objTimeSheetEntry.GP_Date__c);
            else
                mapOfEmployeeProjectIdVsTimeSheetEntryDate.put(Key, new List < Date > { objTimeSheetEntry.GP_Date__c });
        }

        return GPServiceTimesheetTransaction.validateValidEmployee(mapOfEmployeeProjectIdVsTimeSheetEntryDate);

    }

    /**
     * @description Get Map of employee-Month-Year vs valid entry.   
     * 
     * @return Map < String, Boolean > : Return validated map of Employee-Month-Year allocation.
     */
    private static Map < String, Boolean > getMapOfEmployeeMonthYearIsValidEntry() {

        Set < String > setOfEmployeeMonthYearTransactionInDB = new Set < String > ();
        for (GP_Timesheet_Transaction__c objTimeSheetTransaction: new GPSelectorTimesheetTransaction().getTimeSheetTransactionEmployeeMonthYear(setOfEmployeeMonthYears)) {
            if (objTimeSheetTransaction.GP_Approval_Status__c != 'Approved')
                setOfEmployeeMonthYearTransactionInDB.add(objTimeSheetTransaction.GP_Employee_Month_Year_Unique__c.toLowerCase());
        }

        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntries) {
            DateTime entryDate = objTimeSheetEntry.GP_Date__c;
            String monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);

            String key = objTimeSheetEntry.GP_Employee__r.GP_Person_Id__c + monthYear;

            if (setOfEmployeeMonthYearTransactionInDB.contains(key))
                mapOfEmployeeMonthYearIsValidEntry.put(key, false);
            else
                mapOfEmployeeMonthYearIsValidEntry.put(key, true);
        }
        return mapOfEmployeeMonthYearIsValidEntry;
    }

    /**
     * @description Set Class Variables.
     *
     * @param setOfEmployeeMonthYears : Set of employee-month-year for which timesheet entry data has been uploaded.
     * @param setOfEmployeeIds : Set of employee Ids for which timesheet entry data has been uploaded.
     * @param setOfProjectIds : Set of project Ids for which timesheet entry data has been uploaded.
     * @param listOfTimeSheetEntriesToBeValidated : List of timesheet entries which need to be validated.
     * 
     */
    private static void setClassVariables(Set < String > setOfEmployeeMonthYear, Set < Id > setOfEmployeeIds, Set < String > setOfProjectOraclePIds, List < GP_Timesheet_Entry__c > listOfTimeSheetEntriesToBeValidated) {
        setOfEmployeeMonthYears = setOfEmployeeMonthYear;
        setOfEmployeeId = setOfEmployeeIds;
        setOfProjectOraclePId = setOfProjectOraclePIds;
        listOfTimeSheetEntries = listOfTimeSheetEntriesToBeValidated;
    }
}