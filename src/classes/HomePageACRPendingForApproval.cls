public class HomePageACRPendingForApproval {
    public class ApprovalDetailsWrapper
    {
        @AuraEnabled public Account_Creation_Request__c ACRRecord;
        @AuraEnabled public Id approvalItemId;
        
    } 
      /****************************************************************************
Method to fetch ACR records Pending For Apporval
****************************************************************************/
    @AuraEnabled
    public static List<ApprovalDetailsWrapper> fetchACRPendingForApporval()
    {
        try{
            
            List<ApprovalDetailsWrapper> approvalWrapper=new List<ApprovalDetailsWrapper>();
            Map<Id,Account_Creation_Request__c> ACRDetailsMap=new Map<Id,Account_Creation_Request__c>();
            Map<Id,Id> pendingACRApprovalIds = new Map<Id,Id>();//keys are id's of ACR record and values are WorkItem Ids
            List<ProcessInstanceWorkItem> approvalItemsLst = new List<ProcessInstanceWorkItem>();
            
            approvalItemsLst = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                WHERE ProcessInstance.Status = 'Pending' AND ActorId =:UserInfo.getUserId()];
            
            
            for (ProcessInstanceWorkItem workItem : approvalItemsLst) 
            {
                if(workItem != null)
                {
                    if(workItem.ProcessInstance.TargetObjectId.getsobjecttype() == getoidSobjectType('Account_Creation_Request__c'))
                        pendingACRApprovalIds.put(workItem.ProcessInstance.TargetObjectId,workItem.id);
                }
                
            }
            
            
            if(pendingACRApprovalIds.size() > 0)
            {
                ACRDetailsMap = new Map<Id,Account_Creation_Request__c>([Select id,Name,Owner.Name,Description__c,CreatedDate
                                                                         From Account_Creation_Request__c Where Id in: pendingACRApprovalIds.keySet()
                                                                        ]);
                for(Account_Creation_Request__c ACRRecord:ACRDetailsMap.values())
                {
                    if(pendingACRApprovalIds.containsKey(ACRRecord.id))
                    {
                        ApprovalDetailsWrapper wrapObj=new ApprovalDetailsWrapper();
                        wrapObj.ACRRecord=ACRRecord;
                        wrapObj.approvalItemId=pendingACRApprovalIds.get(ACRRecord.id);
                        approvalWrapper.add(wrapObj);
                    }
                }
            }
            system.debug('approvalWrapper'+approvalWrapper);
            return approvalWrapper;
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'HomePageACRPendingForApproval', 'fetchACRPendingForApporval','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    /****************************************************************************
Method to fetch sObject Type 
****************************************************************************/
    private static Schema.SObjectType getoidSobjectType(String objectAPIName)
    {
        Map<String, Schema.SObjectType> globalDescription =  Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = globalDescription.get(objectAPIName); 
        return sObjType;
    }
    
    /****************************************************************************
Method to Approve or Reject Pending Items
****************************************************************************/
    @AuraEnabled 
    public static boolean approvePendingItems(Id approvalPendingItemId,String comments,String action)
    {
        try
        {
            system.debug('approvalPendingItemId'+approvalPendingItemId+' comments'+comments+' action'+action);
            // Instantiate the new ProcessWorkitemRequest object and populate it
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments(comments);
            req.setAction(action);
            
            // Use the ID from the newly created item to specify the item to be worked
            req.setWorkitemId(approvalPendingItemId);
            
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
            return result.isSuccess();  
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'HomePageACRPendingForApproval', 'approvePendingItems','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}