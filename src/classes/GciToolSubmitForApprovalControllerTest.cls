@isTest
public class GciToolSubmitForApprovalControllerTest {

     @testSetup
    public static void createTestData()
    {
        test.startTest();
        User testUser=TestDataFactoryUtility.createTestUser('Genpact Super Admin', 'emailxyz12@acmexyz.com', 'emailXyz12@xyzasd.com');
        insert testUser;
        System.runAs(testUser){
            //creating account
            Account acc=TestDataFactoryUtility.createTestAccountRecord();
            insert acc;
            //creating contact
            Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
            insert con;
            //creating opportunity
            List<Opportunity> opp=TestDataFactoryUtility.createTestOpportunitiesRecordsDiscover(acc.Id, 1, system.today(), con);
           
            opp[0].StageName='2. Define';
            opp[0].Assocaie_RS_barometer__c =true;
        opp[0].RevenueStormPP__RevenueStorm_Associate_Pursuit_Profiler__c = true;
        opp[0].Sales_Leader__c =  testUser.id;
              insert opp;
            test.stopTest();
        }
    }
    //testing negative schenarios 
    @isTest  public static void testfunctionsNegative()
    {
        test.startTest();
        try
        {
            GciToolSubmitForApprovalController.submitForApprovalMethod('','');
            
        }
        catch(Exception ex)
        {
            
        }
        test.stopTest();
    }
    //testing positive schenarios 
    @isTest public static void testFunctionsPositive()
    {
        User testUser=[select id from user where email='emailXyz12@xyzasd.com'];
        system.runAs(testUser)
        {
            test.startTest();
            Opportunity  oppRecord=[select id from opportunity];
            try
            {
                GciToolSubmitForApprovalController.submitForApprovalMethod('',oppRecord.id);
            }
            catch(Exception ex)
            {
                
                system.debug(ex.getMessage());
            }
            
           
            test.stopTest();
        }
    }
    
}