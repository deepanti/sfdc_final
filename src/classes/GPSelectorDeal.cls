public without Sharing class GPSelectorDeal extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Deal__c.Name,
            GP_Deal__c.Id, 
            GP_Deal__c.GP_Opportunity_ID__c,
            GP_Deal__c.GP_Opportunity_ID__r.Name,
            GP_Deal__c.GP_Start_Date__c,
            GP_Deal__c.GP_End_Date__c,
            GP_Deal__c.GP_Delivery_Org__c,
            GP_Deal__c.GP_Sharing_Group_Name__c 
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Deal__c.sObjectType;
    }
    public List<GP_Deal__c> selectById(Set<ID> idSet) {
        return (List<GP_Deal__c>) selectSObjectsById(idSet);
    }
    public GP_Deal__c selectById(ID dealId) {
        Set<Id> idSet = new Set<Id> {
            dealId
        };
        return (GP_Deal__c) selectSObjectsById(idSet).get(0);
    }
    public GP_Deal__c selectDealWithOpportunity(Id dealId) {
        return [SELECT  
                Id,
                Name,
                GP_Opportunity_Name__c,
                GP_Account_Name_L4__c,
                GP_Account_Name_L4__r.Name,
                GP_Billing_Type__c,
                GP_Product_Id__c,
                GP_Business_Group_L1__c,
                GP_Business_Segment_L2__c,
                GP_Deal_Type__c,
                GP_Delivery_Org__c,
                GP_End_Date__c,
                GP_Nature_of_Work__c,
                GP_OLI__c,
                GP_OMS_Deal_ID__c,
                GP_Opportunity_ID__c,
                GP_Opportunity_ID__r.Name,
                GP_Product__c,
                GP_Product_Family__c,
                GP_Revenue_Description__c,
                GP_Service_Line_Description__c,
                GP_Start_Date__c,
                GP_Sub_Business_L3__c,
                GP_Sub_Delivery_Org__c,
                GP_Sub_Vertical__c,
                GP_Vertical__c,
                GP_Sales_Opportunity_Id__c,
                GP_OLI_SFDC_Id__c,
                GP_Business_Name__c,
                GP_Service_Line__c
                FROM GP_Deal__c
                WHERE ID = :dealId];
    }    
    
    public List<GP_Deal__c > selectOppDeal(Set<Id> setofOppProjectID) {
        return (List<GP_Deal__c >) [Select ID,GP_OLI_SFDC_Id__c,GP_Sales_Opportunity_Id__c,GP_Opportunity_Project__c from GP_Deal__c where GP_Opportunity_Project__c IN:setofOppProjectID ];
    }
     public List<GP_Deal__c > selectOppDealOnOppId(Set<string> setofOppID) {
        return (List<GP_Deal__c >) [Select ID,GP_OLI_SFDC_Id__c,GP_Sales_Opportunity_Id__c,GP_Opportunity_Project__c from GP_Deal__c where GP_Sales_Opportunity_Id__c IN:setofOppID ];
    }
    
    public List<GP_Deal__c > selectupdateOppDeal(Set<String> setofOLIID) {
        return (List<GP_Deal__c >) [Select Id, GP_Sales_Opportunity_Id__c,GP_OLI_SFDC_Id__c from GP_Deal__c where GP_OLI_SFDC_Id__c IN : setofOLIID ];
    }
    
    public static GP_Deal__c getDealDataForTemplateSelection(Id dealId) {
        return [Select Id, Name,GP_Deal_Type__c, GP_Status__c,
                GP_Project_Count__c, GP_Business_Group_L1__c,
                GP_Business_Segment_L2__c, GP_Opportunity_ID__c, 
                GP_Account_Name_L4__c,GP_Sales_Opportunity_Id__c
                FROM GP_Deal__c 
                WHERE Id = : dealId
                LIMIT 1];
    }
    
    public static GP_Deal__c getDealWithMaxTCVUnderOppProject(Id oppProjectId) {
        return [Select Id, Name, GP_TCV__c,
                GP_Product_Master__c,
                GP_Product_Master__r.GP_Product_Name__c,
                GP_Product_Family1_ID__c,GP_Sales_Opportunity_Id__c,
                GP_Product_Master__r.GP_Product_Family__c,
                GP_Product_Master__r.GP_Service_Line_ID__c,
                GP_Product_Master__r.GP_Service_Line__c,
                GP_Product_Master__r.GP_Nature_of_Work_ID__c,
                GP_Product_Master__r.GP_Nature_of_Work__c
                FROM GP_Deal__c
                where GP_Opportunity_Project__c = :oppProjectId 
                and GP_TCV__c != null  
                Order by GP_TCV__c desc
                LIMIT 1];
    }
    
    public static Map<Id, GP_Deal__c> getDealDetailsForCreateDealBatch(List<Id> listOfDeals) {
        
        return new Map<Id, GP_Deal__c>(
            [Select id, GP_Probability__c,
             GP_Opportunity_Project__c,
             GP_Business_Segment_L2__c,
             GP_Business_Segment_L2_Id__c,
             GP_Sub_Vertical__c,
             GP_Sub_Business_L3__c,
             GP_Business_Group_L1__c,
             GP_Sub_Business_L3_Id__c,
             GP_Sales_Opportunity_Id__c,
             GP_Vertical__c,
             GP_Service_Line_Description__c,
             GP_Sub_Delivery_Org__c,
             GP_Start_Date__c,
             GP_Nature_of_Work__c,
             GP_Delivery_Org__c,
             GP_Account_Name_L4__c,
             GP_Internal_Opportunity_ID__c,
             GP_Deal_Category__c,
             GP_Service_Line__c,
             GP_Opportunity_Name__c,
             GP_Product_Family__c,
             GP_Opportunity_ID__c,
             GP_Product__c,
             GP_Product_Id__c,
             GP_TCV__c,
             GP_End_Date__c,
             Name
             from GP_Deal__c where Id in :listOfDeals]);
    }
	// OMS Change 17-9-19.
    public static List<GP_Deal__c> getDealDataToClonePID(Set<Id> setOfDealIds) {
        return [Select id, GP_OMS_TCV__c, GP_Account_Name_L4__c, GP_Business_Group_L1__c, GP_Business_Segment_L2__c,
                GP_Business_Segment_L2_Id__c, GP_Sub_Business_L3_Id__c, GP_Sub_Business_L3__c, GP_Product__c,
                GP_Product_Id__c, GP_Service_Line__c, GP_Vertical__c, GP_Sub_Vertical__c, GP_Nature_of_Work__c 
                from GP_Deal__c where id =: setOfDealIds];
    }
}