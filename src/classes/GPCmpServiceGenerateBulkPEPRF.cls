public without sharing class GPCmpServiceGenerateBulkPEPRF {
    @AuraEnabled
    public static GPAuraResponse getApprovedPinnaclePIDForDuration(String startDate, String endDate, String selectedStatus, String businessName) {
        Date dtstartDate = Date.valueof(startDate);
        Date dtendDate = Date.valueof(endDate);
        GPControllerGenerateBulkPEPRF reportGeneratorController = new GPControllerGenerateBulkPEPRF(dtstartDate,dtendDate,selectedStatus,businessName);
        return reportGeneratorController.getApprovedPinnaclePIDForDuration();
    }
    
    @AuraEnabled
    public static GPAuraResponse getBusinessNames() {
        GPControllerGenerateBulkPEPRF businessNames = new GPControllerGenerateBulkPEPRF();
        return businessNames.getBusinessNames();
    }
}