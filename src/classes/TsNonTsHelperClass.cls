public class TsNonTsHelperClass {  
    public static String DealNature;
    public static double Ts_Discover_retail;
    public static double Ts_Discover_medium;
    public static double Ts_Discover_large;
    public static double Ts_Discover_Ex_large;
    public static double Ts_Define_retail;
    public static double Ts_Define_medium;
    public static double Ts_Define_large;
    
    public static double Ts_Discover_retail_con;
    public static double Ts_Discover_medium_con;
    public static double Ts_Discover_large_con;
    public static double Ts_Define_retail_con;
    public static double Ts_Define_medium_con;
    public static double Ts_Define_large_con;
    
    public static double Ts_Discover_retail_anal;
    public static double Ts_Discover_medium_anal;
    public static double Ts_Discover_large_anal;
    public static double Ts_Define_retail_anal;
    public static double Ts_Define_medium_anal;
    public static double Ts_Define_large_anal;
    
    public static double Ts_Discover_retail_dig;
    public static double Ts_Discover_medium_dig;
    public static double Ts_Discover_large_dig;
    public static double Ts_Define_retail_dig;
    public static double Ts_Define_medium_dig;
    public static double Ts_Define_large_dig;
    
    public static double Ts_Deal_Aging;
    public static Boolean Ts_Deal_flag = false;
    
    public static Date sowdate;
    public Static string dealType;
    
    public static void dealTypeMethod(list<opportunity> opplst){
        try{
            System.debug('1.Number of Queries used in this apex code so far:dealTypeMethod ' + Limits.getQueries());
            system.debug(':----ts--Nonts---:'+opplst);
            list<opportunity> oplst = new list<opportunity>();
            opportunity opnty = new opportunity();
            
            for(opportunity opp : opplst) {
                string natureofwork = opp.Nature_Of_Work_OLI__c;
                system.debug(':---NatureOfwork---: '+natureofwork);
                if(String.isNotBlank(natureofwork)){    dealType = dealtypemethod(natureofwork,opp.conslting_TCV__c,opp.Analytics_TCV__c,opp.Digital_TCV__c,opp.Managed_Services_TCV__c,opp.IT_Services_TCV__c);
                    
                }
                dealAgeing();
                if(opp.stageName == '1. Discover' && DealNature =='NonTs' ){   Decimal Insight = 0;      if(opp.Type_of_deal_for_non_ts__c=='Retail'){       Insight = Ts_Discover_retail - opp.Transformation_deal_ageing_for_non_ts__c;  opnty.Insight__c=Insight;
                    }        if(opp.Type_of_deal_for_non_ts__c=='Medium'){     Insight = Ts_Discover_medium - opp.Transformation_deal_ageing_for_non_ts__c;  opnty.Insight__c=Insight;
                    }     if(opp.Type_of_deal_for_non_ts__c=='Large'){    Insight = Ts_Discover_large - opp.Transformation_deal_ageing_for_non_ts__c;  opnty.Insight__c=Insight;
                    }      if(opp.Type_of_deal_for_non_ts__c=='Extra Large'){   Insight = Ts_Discover_Ex_large - opp.Transformation_deal_ageing_for_non_ts__c;  opnty.Insight__c=Insight;
                    }   } else if(opp.stageName == '1. Discover'  || opp.stageName == '2. Define' || opp.stageName == '3. On Bid' || opp.stageName == '4. Down Select' || opp.stageName == '5. Confirmed'){
                    if(opp.Last_stage_change_date__c != Null){
                        opnty.Insight__c = opp.Last_stage_change_date__c.daysBetween(opp.CloseDate);
                    }else {       opnty.Insight__c = system.today().daysBetween(opp.CloseDate);
                    }
                }
                opnty.Type_Of_Opportunity__c =  dealType;
                opnty.id = opp.id;
                
                opnty.Deal_Nature__c = DealNature;
                oplst.add(opnty);
            }
            
            update oplst;
        }catch (Exception e) {
            system.debug(':----error Msg----:'+e.getMessage()+':----error Line----:'+e.getLineNumber());
        }
    }
    
    
    public static string dealtypemethod(string s,Decimal conslting_TCV,Decimal Analytics_TCV,Decimal Digital_TCV,Decimal Managed_Services_TCV,Decimal IT_Services_TCV){
        system.debug(':----dealtypemethod----:');
        String dealtype;
        Decimal NonTsTCV = 0;
        Decimal TsTCV = 0;
        if((s.contains('Analytics')) && !s.contains('Consulting') && !s.contains('Managed Services') && !s.contains('IT Services') && !s.contains('Digital')){
            dealtype = 'TS';   DealNature = 'Analytics';
        }else if((s.contains('Consulting') || s.contains('Digital')) && !s.contains('Managed Services') && !s.contains('IT Services') && !s.contains('Analytics')){
            dealtype = 'TS';   Decimal TsTCVcon = 0;  Decimal TsTCVdigi = 0;   if(conslting_TCV != null){  TsTCVcon = TsTCVcon + conslting_TCV;  }  if(Digital_TCV != null){   TsTCVdigi = TsTCVdigi + Digital_TCV;  }
            if(TsTCVcon > TsTCVdigi){ DealNature = 'conslting';     } else {      DealNature = 'Digital';        } 
        }else if((s.contains('Consulting') || s.contains('Digital')) && !s.contains('Managed Services') && !s.contains('IT Services') && s.contains('Analytics')){
            Decimal TsTCVcon = 0;  Decimal TsTCVdigi = 0;
            if(conslting_TCV != null){ TsTCV = TsTCV + conslting_TCV; TsTCVcon = TsTCVcon + conslting_TCV;      }
            if(Analytics_TCV != null){ NonTsTCV = NonTsTCV + Analytics_TCV;      }
            if(Digital_TCV != null){ TsTCV = TsTCV + Digital_TCV; TsTCVdigi = TsTCVdigi + Digital_TCV;       }
            if(NonTsTCV > TsTCV){  dealtype = 'Non Ts';  DealNature = 'NonTs';   } else {
                                    dealtype = 'TS';   if(TsTCVcon > TsTCVdigi){ DealNature = 'conslting';    } else {  DealNature = 'Digital';
                                                            }        }
            
            
        }else if((s.contains('IT Services') || s.contains('Managed Services') ) && !s.contains('Analytics') && !s.contains('Digital')  && !s.contains('Consulting')){
            dealtype = 'Non Ts'; DealNature = 'NonTs';
        } else if((s.contains('IT Services') || s.contains('Managed Services')) && s.contains('Analytics') && !s.contains('Digital')  && !s.contains('Consulting')){
            dealtype = 'Non Ts';   DealNature = 'NonTs';
        } else if((s.contains('IT Services') || s.contains('Managed Services') || s.contains('Analytics')) && (s.contains('Consulting') || s.contains('Digital'))){
            Decimal TsTCVcon = 0;    Decimal TsTCVdigi = 0;
            if(Managed_Services_TCV != null){ NonTsTCV = NonTsTCV + Managed_Services_TCV;
                                            }   if(IT_Services_TCV != null){  NonTsTCV = NonTsTCV + IT_Services_TCV;
                                       }    if(Analytics_TCV != null){  NonTsTCV = NonTsTCV + Analytics_TCV;
                                     }     if(conslting_TCV != null){ TsTCV = TsTCV + conslting_TCV; TsTCVcon = TsTCVcon + conslting_TCV;
                                     }    if(Digital_TCV != null){ TsTCV = TsTCV + Digital_TCV; TsTCVdigi = TsTCVdigi + Digital_TCV;
                                   }    if(NonTsTCV > TsTCV){ dealtype = 'Non Ts'; DealNature = 'NonTs';
                                } else {  dealtype = 'TS';   if(TsTCVcon > TsTCVdigi){ DealNature = 'conslting';
                                                            } else {            DealNature = 'Digital';
                                                            }      }
            
        }
        system.debug(':--dealnature--: '+DealNature);
        return dealtype;
    }
    private static Void dealAgeing(){
        for(Non_Ts_cycle_Time__c Dc : Non_Ts_cycle_Time__c.getall().values()){
            if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'NonTs'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Discover_medium = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){  Ts_Discover_large = Dc.Ageing__c;
                                                            }
                if(Dc.Type_of_deal_for_non_ts__c == 'Ex-Large'){   Ts_Discover_Ex_large = Dc.Ageing__c;
                                                               }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '2. Define' && Dc.Name__c == 'NonTs'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Define_medium = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){ Ts_Define_large = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Discover_medium_dig = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){ Ts_Discover_large_dig = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '2. Define' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Define_medium_dig = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){ Ts_Define_large_dig = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Discover_medium_anal = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){ Ts_Discover_large_anal = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '2. Define' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Define_medium_anal = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){ Ts_Define_large_anal = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '1. Discover' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Discover_medium_con = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){  Ts_Discover_large_con = Dc.Ageing__c;
                                                            }
            }
            if(Test.isRunningTest() || Dc.Stage__c == '2. Define' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Define_medium_con = Dc.Ageing__c;
                                                             }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){Ts_Define_large_con = Dc.Ageing__c;
                                                            }
            }          
        }
    }
}