@istest
public class TestStaleDealsIdentifier {
    
    static TestMethod void Test1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        u.Has_Stale_Deals__c = true;
        update u;
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);   
        oOpportunity.OwnerId = u.id;
        update oOpportunity;
        List<Opportunity> oppList= [select id,ownerid,stagename from Opportunity];
        Test.startTest();
        staledealsIdentifier stale=new staledealsIdentifier('s');
        stale.Query = 'Select id,ownerid,stagename from Opportunity';
        Database.executeBatch(stale);
        //BatchableContext BC = new BatchableContext();
        //stale.execute(BC, oppList);
        oOpportunity.Margin__c=5.0;
        system.runAs(u)
        {
        update oOpportunity;
        }
        Test.stopTest();
        
       
    }
}