// ------------------------------------------------------------------------------------------------ 
// Description: Tracker Class for GPBatchAutoRejectedProject
// ------------------------------------------------------------------------------------------------
//Created By : Ved Prakash
// ------------------------------------------------------------------------------------------------ 
@isTest
public class GPBatchAutoRejectedProjectTracker {
    public static GP_Project__c prjObj; 
    public static GP_Project__c prjObjSeven; 
    public static GP_Project__c prjObjFourteen;
    public static GP_Project__c prjObjTwentyOne; 
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    
    public static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = UserInfo.getUserId();   
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
         GP_Work_Location__c workLocation = GPCommonTracker.getWorkLocation();
        insert workLocation;
        

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; 
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
         prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        prjObj.GP_Project_Submission_Date__c = System.Today().addDays(-Integer.valueof(System.Label.GP_Project_Submission_Valid_days));
        
        insert prjObj;

        //insert record for 7 days
        Date lastSevenWorkingDate;
        Integer WorkingDayCount=0;
        for(Integer i=0;i<=Integer.valueof(System.Label.GP_Project_Submission_Valid_days);i++)
        {
            // workDayCount = GPSendMailNotificationToPIDApprover.getWorkingDays(lastSevendate.addDays(i),System.today());         
            
            Datetime dt = (DateTime)System.today().addDays(-i);
            String dayOfWeek = dt.format('EEEE');                          
            system.debug('==dayOfWeek='+dayOfWeek);
            if(dayOfWeek != 'Sunday' && dayOfWeek != 'Saturday')
            {
                WorkingDayCount = WorkingDayCount+1;
                system.debug('===WorkingDayCount==='+WorkingDayCount);
                if(WorkingDayCount == 7)
                {
                    lastSevenWorkingDate = System.today().addDays(-i);
                    break;
                }    
            }          
            
        }  
        
        prjObjSeven = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUserSeven = userInfo.getUserId();
        
        prjObjSeven.OwnerId = loggedInUserSeven; 
        prjObjSeven.GP_CRN_Number__c = iconMaster.Id;
        prjObjSeven.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjSeven.GP_Primary_SDO__c = objSdo.Id;
        prjObjSeven.GP_Approval_Status__c = 'Pending for Approval';
        prjObjSeven.GP_BPR_Approval_Required__c = true;
        prjObjSeven.GP_PID_Approver_Finance__c = false;
        prjObjSeven.GP_Additional_Approval_Required__c = false;
        prjObjSeven.GP_Controller_Approver_Required__c = false;
        prjObjSeven.GP_FP_And_A_Approval_Required__c = false;
        prjObjSeven.GP_MF_Approver_Required__c = false;
        prjObjSeven.GP_Old_MF_Approver_Required__c = false;
        prjObjSeven.GP_Product_Approval_Required__c = false;
        prjObjSeven.GP_Auto_Approval__c = false;
        prjObjSeven.GP_Current_Working_User__c = loggedInUserSeven; 
        prjObjSeven.GP_Controller_User__c = loggedInUserSeven; 
        prjObjSeven.GP_Approver_Ids__c = loggedInUserSeven;
        prjObjSeven.GP_Project_Submission_Date__c = lastSevenWorkingDate;
        
        insert prjObjSeven;
        
         //insert record for 14 days
        Date lastFourteenWorkingDate;
        Integer WorkingDayFourteenCount=0;
        for(Integer i=0;i<=Integer.valueof(System.Label.GP_Project_Submission_Valid_days);i++)
        {
            // workDayCount = GPSendMailNotificationToPIDApprover.getWorkingDays(lastSevendate.addDays(i),System.today());         
            
            Datetime dt = (DateTime)System.today().addDays(-i);
            String dayOfWeek = dt.format('EEEE');                          
            system.debug('==dayOfWeek='+dayOfWeek);
            if(dayOfWeek != 'Sunday' && dayOfWeek != 'Saturday')
            {
                WorkingDayFourteenCount = WorkingDayFourteenCount+1;
                system.debug('===WorkingDayCount==='+WorkingDayFourteenCount);
                if(WorkingDayFourteenCount == 14)
                {
                    lastFourteenWorkingDate = System.today().addDays(-i);
                    break;
                }    
            }          
            
        }  
        
        prjObjFourteen = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUserFourteen = userInfo.getUserId();
        
        prjObjFourteen.OwnerId = loggedInUserFourteen; 
        prjObjFourteen.GP_CRN_Number__c = iconMaster.Id;
        prjObjFourteen.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjFourteen.GP_Primary_SDO__c = objSdo.Id;
        
        prjObjFourteen.GP_Approval_Status__c = 'Pending for Approval';
        prjObjFourteen.GP_BPR_Approval_Required__c = true;
        prjObjFourteen.GP_PID_Approver_Finance__c = false;
        prjObjFourteen.GP_Additional_Approval_Required__c = false;
        prjObjFourteen.GP_Controller_Approver_Required__c = false;
        prjObjFourteen.GP_FP_And_A_Approval_Required__c = false;
        prjObjFourteen.GP_MF_Approver_Required__c = false;
        prjObjFourteen.GP_Old_MF_Approver_Required__c = false;
        prjObjFourteen.GP_Product_Approval_Required__c = false;
        prjObjFourteen.GP_Auto_Approval__c = false;
        prjObjFourteen.GP_Current_Working_User__c = loggedInUserFourteen; 
        prjObjFourteen.GP_Controller_User__c = loggedInUserFourteen; 
        prjObjFourteen.GP_Approver_Ids__c = loggedInUserFourteen;
        prjObjFourteen.GP_Project_Submission_Date__c = lastFourteenWorkingDate;
        
        insert prjObjFourteen;
        
          //insert record for 21 days
        Date lastTwentyOneWorkingDate;
        Integer WorkingDayTwentyOneCount=0;
        for(Integer i=0;i<=Integer.valueof(System.Label.GP_Project_Submission_Valid_days);i++)
        {
            // workDayCount = GPSendMailNotificationToPIDApprover.getWorkingDays(lastSevendate.addDays(i),System.today());         
            
            Datetime dt = (DateTime)System.today().addDays(-i);
            String dayOfWeek = dt.format('EEEE');                          
            system.debug('==dayOfWeek='+dayOfWeek);
            if(dayOfWeek != 'Sunday' && dayOfWeek != 'Saturday')
            {
                WorkingDayTwentyOneCount = WorkingDayTwentyOneCount+1;
                system.debug('===WorkingDayCount==='+WorkingDayTwentyOneCount);
                if(WorkingDayTwentyOneCount == 21)
                {
                    lastTwentyOneWorkingDate = System.today().addDays(-i);
                    break;
                }    
            }          
            
        }  
        
        prjObjTwentyOne = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUserTwentyOne = userInfo.getUserId();
        
        prjObjTwentyOne.OwnerId = loggedInUserTwentyOne; 
        prjObjTwentyOne.GP_CRN_Number__c = iconMaster.Id;
        prjObjTwentyOne.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjTwentyOne.GP_Primary_SDO__c = objSdo.Id;
        prjObjTwentyOne.GP_Approval_Status__c = 'Pending for Approval';
        prjObjTwentyOne.GP_BPR_Approval_Required__c = true;
        prjObjTwentyOne.GP_PID_Approver_Finance__c = false;
        prjObjTwentyOne.GP_Additional_Approval_Required__c = false;
        prjObjTwentyOne.GP_Controller_Approver_Required__c = false;
        prjObjTwentyOne.GP_FP_And_A_Approval_Required__c = false;
        prjObjTwentyOne.GP_MF_Approver_Required__c = false;
        prjObjTwentyOne.GP_Old_MF_Approver_Required__c = false;
        prjObjTwentyOne.GP_Product_Approval_Required__c = false;
        prjObjTwentyOne.GP_Auto_Approval__c = false;
        prjObjTwentyOne.GP_Current_Working_User__c = loggedInUserTwentyOne; 
        prjObjTwentyOne.GP_Controller_User__c = loggedInUserTwentyOne; 
        prjObjTwentyOne.GP_Approver_Ids__c = loggedInUserTwentyOne;
        prjObjTwentyOne.GP_Project_Submission_Date__c = lastTwentyOneWorkingDate;
        
        insert prjObjTwentyOne;
       
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        String strFieldSetName = '';
        for(Schema.FieldSet f : SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }
    }
    
    static testmethod void testbatch() {
        Test.StartTest();
        setupCommonData();
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment','approver',false);
        
        	GPBatchAutoRejectedProject batcher = new GPBatchAutoRejectedProject();
        	Id batchprocessid = Database.executeBatch(batcher, 1);
        
        Test.StopTest();
    }
}