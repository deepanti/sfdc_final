public class GPProjectTemplateFieldWrapper {
	@AuraEnabled
    public String ApiName{get; set;}
	@AuraEnabled
    public String Label{get; set;}
	@AuraEnabled
    public String sectionName{get; set;}
	@AuraEnabled
    public Boolean isVisible{get; set;}
	@AuraEnabled
    public Boolean isRequired{get; set;}
	@AuraEnabled
    public Boolean isReadOnly{get; set;}
	@AuraEnabled
    public Boolean isSectionVisible{get; set;}
	@AuraEnabled
    public Boolean isreadOnlyAfterApproval{get; set;}
	@AuraEnabled
    public Integer numberOfDefaultRowToDisplay{get; set;}
}