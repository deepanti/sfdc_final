@isTest(seealldata=false)
public class VIC_BatchGenerateBonusMatrixTest {

   
    public static Master_Plan_Component__c masterPlanComponentObj;
   
    public static Target_Achievement__c ObjInc;
    
    public static Target_Component__c targetComponentObj;
    
    public static user ObjUser;
    Public static Master_VIC_Role__c masterVICRoleObj;
    Public static list<id> lstid = new list<id>();
    
    
    static testmethod void Test1(){
        
        LoadData();
        lstid.add(objuser.id);
        
       VIC_BatchGenerateBonusMatrix obj = new VIC_BatchGenerateBonusMatrix(lstid,date.newinstance(2018,1,1),system.today());
       database.executebatch(obj,1);
        System.assertEquals(200,200);
        
    }
    
     static void LoadData()
    {
        
        
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
       
        insert objuser;
         
        
         masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        masterVICRoleObj.Horizontal__c='Enterprise sales';
        masterVICRoleObj.Name='BD Enterprise sales';
        masterVICRoleObj.Role__c='BD';
        insert masterVICRoleObj;

        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        
       
        
        
       
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        
       
        
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';  
        planObj1.Year__c ='2018';
        insert planobj1;
        
        
        
        
       
        
       
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        targetObj.Plan__c=planobj1.id;
        insert targetObj;
        
        masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Discretionary_Payment'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';  
        masterPlanComponentObj.Component_Usage__c='Bonus';  
        insert masterPlanComponentObj;
        
       
        
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
        
        targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.target__r=targetObj;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        
        Target_Achievement__c targetAchievement= vic_commonUtil.initTargetIncentivRecOBJ(targetComponentObj,500,'New','HR - Approved');
        
        insert targetAchievement;
    }

}