/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/26/2018
@Description :this batch is used to update field from Account(Industry Vertical,Archtype) to Contact and Lead Objects Field(Vertical__c,archtype__c)


@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */

Global with sharing class LinkAccountWithAccountCrationRequestHl implements DataBase.Batchable<sObject> {
    
    List<sObject> lstToUpdate=new List<sObject>();
    Map<Id,Account> oldMap=new Map<Id,Account>();
    List<Account> accNewRec=new List<Account>();
    String email='abhishek.maurya@genpact.com';
    public LinkAccountWithAccountCrationRequestHl(List<Account> lstAcc,Map<Id,Account> oldAccMap)
    {
        accNewRec=lstAcc;
        oldMap=oldAccMap;
    }
    
    global Database.querylocator start(Database.BatchableContext BC)
    {
       
        return DataBase.getQueryLocator([SELECT Id,Archetype__r.Name,Industry_Vertical__c,(SELECT Id FROM Contacts),(SELECT Id FROM Leads__r) FROM Account WHERE Id IN : accNewRec]);
       
   
     }
    Global void execute(DataBase.BatchableContext bc,List<Account> accLst)
    {
        
       system.debug('..Inside Batch Execute Method..');
        for(Account acc:accLst) 
        {
            Account oldAccount=oldMap.get(acc.Id);
            if(acc.Archetype__c!=oldAccount.Archetype__c||acc.Industry_Vertical__c!=oldAccount.Industry_Vertical__c)
            {
                for(Contact con:acc.contacts)
                {
                    con.Archetype__c=acc.Archetype__r.Name;
                    con.Vertical__c=acc.Industry_Vertical__c;
                    lstToUpdate.add(con);
                }
                for(Lead ld:acc.leads__r)
                {
                    ld.Archetype__c=acc.Archetype__r.Name;
                    ld.Vertical__c=acc.Industry_Vertical__c;
                    lstToUpdate.add(ld);
                }
            }
        }
        try{
            update lstToUpdate;
        }
        catch(Exception e)
            {
                system.debug('..We have got an Error at..'+String.valueOf(e.getLineNumber())+'..message is..'+e.getMessage());
            }
        
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('batch finished');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process completed for marketo Field update form Account to Lead and Contact Object');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}