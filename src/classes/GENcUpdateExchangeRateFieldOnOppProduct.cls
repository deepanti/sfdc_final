global class GENcUpdateExchangeRateFieldOnOppProduct implements Database.batchable<sObject>, Database.Stateful
{
           global final String StartDate {get; set;}
           global final String EndDate {get; set;}
           global final Integer Probability {get; set;}
           global final String Query; 

               
global GENcUpdateExchangeRateFieldOnOppProduct (String StartDate,String EndDate,integer Probability)
{
               
            StartDate=StartDate;
            EndDate=EndDate;
            Probability=Probability;
            Query = 'SELECT ID,ExchangeRate__c,LocalCurrency__c,TotalContractValue__c,TCVLocal__c FROM OpportunityProduct__c WHERE OpportunityId__r.Probability='+ Probability +'and CreatedDate>='+StartDate +'and CreatedDate<='+EndDate ; 
}

global Database.QueryLocator start(Database.BatchableContext bc)
{    
            System.debug('TestQuery'+Query);
            return database.getQuerylocator(Query);
}
global void execute(Database.batchableContext info, List<OpportunityProduct__c> OppProduct)
{ 
            List<OpportunityProduct__c> OppProductToBeUpadatedList = new List<OpportunityProduct__c>(); 
            Set<OpportunityProduct__c> OppProductToBeUpadatedSet = new Set<OpportunityProduct__c>(); 
            for(OpportunityProduct__c opp : OppProduct)
            {
                for(DatedConversionrate dcr: [SELECT ConversionRate,Id,IsoCode,NextStartDate,StartDate FROM DatedConversionRate WHERE NextStartDate >= today])
                {
                    if (opp.LocalCurrency__c==dcr.IsoCode) 
                    {       
                        opp.ExchangeRate__c = dcr.ConversionRate;
                        if(opp.ExchangeRate__c!=null && opp.ExchangeRate__c!=0)
                        {
                            opp.TotalContractValue__c = opp.TCVLocal__c/opp.ExchangeRate__c;
                        }
                        OppProductToBeUpadatedSet.add(opp);       
                    }
                }       
             }
             OppProductToBeUpadatedList.addall(OppProductToBeUpadatedSet);
             update OppProductToBeUpadatedList;
}
global void finish(Database.batchableContext info)
{        

} 
    
}