@isTest
public class HomePageApprovalControllerTest {
    
    @testSetup
    public static void createData()
    {
        Account accList = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
        insert accList;
        Contact conList = TestDataFactoryUtility.CreateContact('First','Last',accList.id,'Test','Test','fed@gtr.com','Call: 7834234234378342342343');
        insert conList;
        Opportunity oppList = TestDataFactoryUtility.CreateOpportunity('Test',accList.id,conList.id);
        oppList.QSRM_Nature_Type__c ='TS';
        insert oppList;
        List<Product2> pList = TestDataFactoryUtility.createTestProducts(1);
        insert pList;
        test.startTest();
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000, 
            IsActive = true);
        insert standardPrice;
        List<OpportunityLineItem> oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);
        insert oppLineItem;
        User testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','abcdVikasrathi91@bc.com','abcdVikasrathi91@bc.com');
        insert testUser;
        oppList.Assocaie_RS_barometer__c =true;
        oppList.RevenueStormPP__RevenueStorm_Associate_Pursuit_Profiler__c = true;
        oppList.Sales_Leader__c =  testUser.id;
        update oppList;
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(oppList.id);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(UserInfo.getUserId()); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('RS_PP_Tool');
        req1.setSkipEntryCriteria(true);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        test.stopTest();
    }
    public static testMethod void fetchOpportunityPendingForQSRMApprovalTest()
    {
        
        Opportunity opp = [Select  id,QSRM_Required__c,QSRM_Status__c,QSRM_Approved__c,QSRM_Rejected__c,QSRM_status2__c
                           ,Formula_Hunting_Mining__c,Account.Business_Group__c,Opportunity_Source__c from Opportunity Limit 1];
        System.debug('HomePageApprovalController ==Opp='+opp);
        Id qsrmId = [Select Id,Name from RecordType where name = 'TS QSRM'].id;
        Test.startTest();        
        QSRM__c testQSRM = TestDataFactoryUtility.createTestQSRMRecord(opp.Id,qsrmId);
        insert testQSRM;
        System.debug('HomePageApprovalController ==testQSRM='+testQSRM);
        List<HomePageApprovalController.ApprovalDetailsWrapper> wrapperLst = HomePageApprovalController.fetchOpportunityPendingForQSRMApproval();
        System.debug('------------>'+wrapperLst.size());
        //System.assertEquals(1, wrapperLst.size());
            Test.stopTest();
       
    }
    public static testMethod void fetchOpportunityPendingForApporvalTest()
    {
        User salesLeader = [Select id,Name from User where email = 'abcdVikasrathi91@bc.com'];
        
        System.runAs(salesLeader)
        {
            Test.startTest();
            HomePageApprovalController.iterateMethod();
            List<HomePageApprovalController.ApprovalDetailsWrapper> wrapperLst = HomePageApprovalController.fetchOpportunityPendingForApporval();
            //System.assertEquals(1, wrapperLst.size());
            Test.stopTest();
        }
    }
    public static testMethod void approvePendingItemsTest()
    {
        User salesLeader = [Select id,Name from User where email = 'abcdVikasrathi91@bc.com'];
        Id itemPendingForApproval = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                     WHERE ProcessInstance.Status = 'Pending' AND ActorId =:salesLeader.id Limit 1].Id;
        System.runAs(salesLeader)
        {
            Test.startTest();
            Boolean approve = HomePageApprovalController.approvePendingItems(itemPendingForApproval,'Accept','Approve');
           // System.assertEquals(true, approve);            
            Test.stopTest();
        }
    }
    public static testMethod void check()
    {
        Opportunity opp = [Select  id,QSRM_Required__c,QSRM_Status__c,QSRM_Approved__c,QSRM_Rejected__c,QSRM_status2__c
                           ,Formula_Hunting_Mining__c,Account.Business_Group__c,Opportunity_Source__c from Opportunity Limit 1];
        System.debug(opp);
        Id qsrmId = [Select Id,Name from RecordType where name = 'TS QSRM'].id;
        User salesLeader = [Select id,Name from User where email = 'abcdVikasrathi91@bc.com'];
        Test.startTest();        
        QSRM__c testQSRM = TestDataFactoryUtility.createTestQSRMRecord(opp.Id,qsrmId);
        testQSRM.Vertical_Leaders__c = salesLeader.id;
        testQSRM.Service_Line_Leaders__c = salesLeader.id;
        insert testQSRM;
        
        //User usr = [Select Id from User where id =: userid ];
        System.runAs(salesLeader)
        {
            List<HomePageApprovalController.ApprovalDetailsWrapper> wrapperLst = HomePageApprovalController.fetchOpportunityPendingForApporval();
            //System.assertEquals(1, wrapperLst.size());
        }
        Test.stopTest();
    }
}