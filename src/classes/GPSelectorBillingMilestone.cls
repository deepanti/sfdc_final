public class GPSelectorBillingMilestone extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Billing_Milestone__c.Name
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Billing_Milestone__c.sObjectType;
    }

    public List < GP_Billing_Milestone__c > selectById(Set < ID > idSet) {
        return (List < GP_Billing_Milestone__c > ) selectSObjectsById(idSet);
    }

    public List < GP_Billing_Milestone__c > selectProjectBillingMilestoneRecords(Id projectId) {
        return [SELECT Id, Name, GP_Date__c, GP_Amount__c, GP_Work_Location__c, GP_Project__c, GP_Entry_type__c, GP_Milestone_start_date__c,
            GP_Parent_Billing_Milestone__c, GP_Work_Location__r.Name, GP_value__c, GP_Billing_Status__c, GP_Milestone_end_date__c
            FROM GP_Billing_Milestone__c
            where GP_Project__c =: projectId
            ORDER BY GP_Date__c DESC
        ];
    }

    public GP_Billing_Milestone__c selectProjectBillingMilestoneRecord(Id billingMilestoneId) {
        return [Select Id
            from GP_Billing_Milestone__c
            where Id =: billingMilestoneId
        ];
    }

    public List < GP_Billing_Milestone__c > selectProjectBillingMilestones(List < Id > billingMilestoneIds) {
        return [Select Id
            from GP_Billing_Milestone__c
            where Id in: billingMilestoneIds
        ];
    }

    public list < GP_Billing_Milestone__c > selectCurrencyFromProject(set < Id > setOfId) {
        return [SELECT GP_Project__c, GP_Project__r.currencyIsoCode
            FROM GP_Billing_Milestone__c
        ];
    }

    public list < GP_Billing_Milestone__c > queryOnBillingMilestone(set < Id > setOfProjectId) {
        return [select Id, name, CurrencyIsoCode, GP_Project__r.CurrencyIsoCode
            from GP_Billing_Milestone__c where
            GP_Project__c IN: setOfProjectId
        ];
    }

    public List < GP_Billing_Milestone__c > selectProjectBillingMilestoneRecords(Set < Id > lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Billing_Milestone__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Billing_Milestone__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }
}