@isTest(seeAlldata=true)
private class GW1_MakeDSROwnerControllerTracker {

    static testMethod void myUnitTest() 
    {
         user objuser=GW1_CommonTracker.createUser('standt','Testing','standarduserTest@Genpact.com');
         GW1_DSR__c objdsrc =  GW1_CommonTracker.createGW1DSR('abcd');
         GW1_DSR_Team__c objdsrteam= GW1_CommonTracker.createGW1DSRTeam( objdsrc,objuser,true,'RFx Tower Lead');
         
         objdsrteam.GW1_User__c=userInfo.getUserID();
         update objdsrteam;
         ApexPages.StandardController sc = new ApexPages.StandardController(objdsrc);
         GW1_MakeDSROwnerController objController = new GW1_MakeDSROwnerController(sc);
         objController.changeOwner();
        
    }
     static testMethod void myUnitTestUserChange() 
    {
        user objuser=GW1_CommonTracker.createUser('standt','Testing','standarduserTest@Genpact.com');
        GW1_DSR__c objdsrc =  GW1_CommonTracker.createGW1DSR('abcd');
        
        objdsrc.OwnerId = objuser.Id;
        update objdsrc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objdsrc);
        GW1_MakeDSROwnerController objController = new GW1_MakeDSROwnerController(sc);
        objController.changeOwner();
        
    }
    
      static testMethod void IfTowerLeadAlreadyInTeam() 
    {
        user objuser=GW1_CommonTracker.createUser('standt','Testing','standarduserTest@Genpact.com');
        GW1_DSR__c objdsrc =  GW1_CommonTracker.createGW1DSR('abcd');
        GW1_DSR_Team__c objdsrteam= GW1_CommonTracker.createGW1DSRTeam( objdsrc,objuser,true,'RFx Tower Lead');
        objdsrc.OwnerId = objuser.Id;
        update objdsrc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objdsrc);
        GW1_MakeDSROwnerController objController = new GW1_MakeDSROwnerController(sc);
        objController.changeOwner();
        
    }
    static testMethod void IfTowerBidMangerIsAssigned() 
    {
        GW1_Bid_Manager__c objbidmanager=GW1_CommonTracker.createBidManager();
        user objuser=GW1_CommonTracker.createUser('standt','Testing','standarduserTest@Genpact.com');
        GW1_DSR__c objdsrc =  GW1_CommonTracker.createGW1DSR('abcd');
        objdsrc.GW1_Bid_Manager__c=objbidmanager.id;
        update objdsrc;
        GW1_DSR_Team__c objdsrteam= GW1_CommonTracker.createGW1DSRTeam( objdsrc,objuser,true,'RFx Tower Lead');
        objdsrc.OwnerId = objuser.Id;
        update objdsrc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objdsrc);
        GW1_MakeDSROwnerController objController = new GW1_MakeDSROwnerController(sc);
        objController.changeOwner();
        
    }
}