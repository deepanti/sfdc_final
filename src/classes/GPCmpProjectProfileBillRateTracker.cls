@isTest
public class GPCmpProjectProfileBillRateTracker {
    public static GP_Profile_Bill_Rate__c objPrjBillRate;
    public static Id InvalidIdForCoveringCatch;
    public static GP_Project__c prjObj;
    public static String jsonresp;

    @testSetup
    static void setupCommonData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        objuserrole.GP_Role__c = objrole.id;
        insert objuserrole;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        InvalidIdForCoveringCatch = dealObj.id;
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Billing_Currency__c = 'INR';
        insert prjObj;

        GP_Profile_Bill_Rate__c objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Profile__c = 'QA';
        objProfileBillRate.GP_Project__c = prjObj.id;
        objProfileBillRate.GP_Bill_Rate__c = 23;
        insert objProfileBillRate;

        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;

        GP_Project_Expense__c objPrjExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert objPrjExpense;

    }
    
    @isTest
    static void testGPCmpServiceProfileBillRate() {
        fetchData();
        testgetProjectProfileBillRateData();
        testsaveProfileBillRate();
        testdeleteProfileBillRate();
    }

    public static void fetchData() {
        prjObj = [select id, Name, GP_Billing_Currency__c, GP_Bill_Rate_Type__c from GP_Project__c limit 1];
        objPrjBillRate = [select id, Name,GP_Project__c, GP_Bill_Rate__c, GP_Profile__c from GP_Profile_Bill_Rate__c limit 1];
        jsonresp = (String) JSON.serialize(new List < GP_Profile_Bill_Rate__c > { objPrjBillRate });
    }

    public static void testgetProjectProfileBillRateData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectProfileBillRate.getProjectProfileBillRateData(prjObj.Id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetProjectProfileBillRateDataForCatch() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectProfileBillRate.getProjectProfileBillRateData(InvalidIdForCoveringCatch);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testsaveProfileBillRate() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectProfileBillRate.saveProfileBillRate(jsonresp);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteProfileBillRate() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectProfileBillRate.deleteProfileBillRate(objPrjBillRate.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteProfileBillRateForCatch() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectProfileBillRate.deleteProfileBillRate(InvalidIdForCoveringCatch);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
}