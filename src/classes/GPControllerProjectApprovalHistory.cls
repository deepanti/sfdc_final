public with sharing class GPControllerProjectApprovalHistory {
    public ApprovalList aSW ;
    private Id recordId;
    private Id retURLRecordId;
    private String ORG_LNK;
    
   
   public GPAuraResponse getApprovalsHistory(string objectId){
    	recordId = objectId;
    	system.debug('retURLRecordId::'+objectId);
    	ORG_LNK ='';
    	// = new ApprovalList();
         aSW = generateData();
    	
    	system.debug('aSW::'+aSW);
     if(aSW != null){
    	return new GPAuraResponse(true,'Success', setJSON(aSW)); 
     }
     return new GPAuraResponse(false,'Error', 'No data found');
    
    }
    
    public string setJSON(ApprovalList lstProcessinstanceStep){
			return JSON.serialize(lstProcessinstanceStep);
    }
   public  ApprovalList generateData(){
        ApprovalList approvalResultForObject = new ApprovalList();
        List<ApprovalStepWrapper> aSW = new List<ApprovalStepWrapper>();
        String recallApprovalProcessLink;
        Boolean isSubmitForApproval = false;
        for(ProcessInstance pI:getProcessHistory(recordId).values()){
        	isSubmitForApproval = true;
            Map<Id,List<ProcessInstanceHistory>> mapOfProcessNodeIdAndProcessInstanceHistory = new Map<Id,List<ProcessInstanceHistory>>();
            Set<Id> processNodeId= new Set<Id>(); 
            
            for(ProcessInstanceHistory sWI:pI.StepsAndWorkitems){
                if(processNodeId.size() ==0)
                    processNodeId.add(sWI.ProcessNodeId);
                else if(processNodeId.size()>0 && processNodeId.contains(sWI.ProcessNodeId)!= NULL)
                    processNodeId.add(sWI.ProcessNodeId);
            }
            
            for(Id pNId: processNodeId){
                ApprovalStepWrapper aSWr = new ApprovalStepWrapper();
                for(ProcessInstanceHistory sWI:pI.StepsAndWorkitems){
                    if(sWI.processNodeId == pNID){
                        aSWr.listOfSteps.add(sWI);
                    }
                    if(sWI.StepStatus == 'Pending'){
                        aSWr.workItemApproveOrRejectLink = ORG_LNK+'/p/process/ProcessInstanceWorkitemWizardStageManager?id='+sWI.Id;
                        aSWr.workItemReassignLink =  ORG_LNK+'/'+sWI.Id+'/e?et=REASSIGN&retURL=/'+retURLRecordId;
                        recallApprovalProcessLink =  ORG_LNK +'/'+sWI.Id+'/e?et=REMOVE&retURL=/'+retURLRecordId;
                        isSubmitForApproval = false;
                    }
                    
                }
                aSW.add(aSWr);
            }
        }
        if(aSW.size()>0){
        	isSubmitForApproval = true;
        }
        approvalResultForObject.approvals = aSW;
        approvalResultForObject.recordId = recordId;
        approvalResultForObject.isSubmitForApproval = isSubmitForApproval;
        approvalResultForObject.recallApprovalProcessLink = recallApprovalProcessLink;
        return approvalResultForObject;
    }
    private Map<Id,ProcessInstance> getProcessHistory(Id objectId){
       return new Map<Id,ProcessInstance>([SELECT Id, (SELECT ID, ProcessNodeId,
						 StepStatus,Comments,TargetObjectId,ActorId,CreatedById,IsDeleted,IsPending
						,OriginalActorId,ProcessInstanceId,RemindersSent,CreatedDate, Actor.Name,
						OriginalActor.Name , ProcessNode.Name FROM StepsAndWorkitems order by CreatedDate DESC ) 
						FROM ProcessInstance where TargetObjectId =:objectId order by CreatedDate DESC]);
    }
    
    public class ApprovalStepWrapper{
        public String workItemApproveOrRejectLink;
        public String workItemReassignLink ;
        public List<ProcessInstanceHistory> listOfSteps ;
        public ApprovalStepWrapper(){
            listOfSteps = new  List<ProcessInstanceHistory>();
        }
    }
    public class ApprovalList{
        public List<ApprovalStepWrapper> approvals ;
        public String recallApprovalProcessLink ;
        public Boolean isSubmitForApproval ;
        public Id recordId ;
        public ApprovalList(){
            approvals = new List<ApprovalStepWrapper>();
            isSubmitForApproval = true;
        }
    }

}