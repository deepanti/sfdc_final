public class CreateErrorLog {
     
     public static void createErrorRecord(string recordId,string exceptionMessage, 
                                          String exceptionDetails,string stackTrace,
                                          string className,                                          
                                          String methodName,String status,
                                          String errorCode,String lineNo){
         String QueryLimit = '1. SOQL Queries used / SOQL Queries allowed: ' + Limits.getQueries() + '/' + Limits.getLimitQueries();
            String DMLimit = '2. Number of records queried so far /  Number allowed: ' + Limits.getDmlRows() + '/' + Limits.getLimitDmlRows();
            String DMLStat = '3. Number of DML statements used so far / Number allowed: ' +  Limits.getDmlStatements() + '/' + Limits.getLimitDmlStatements();   
            String CPUT = '4. Amount of CPU time (in ms) used so far / CPU usage time (in ms) allowed: ' + Limits.getCpuTime() + '/' + Limits.getLimitCpuTime();
            
         Exception__c newErrorRecord = new Exception__c();
         newErrorRecord.Gov_Limit_Tracking__c = String.format('\n{0}\n{1}\n{2}',new string[]{QueryLimit, DMLimit,DMLStat,CPUT});
         newErrorRecord.RecordId__c = recordId;
         newErrorRecord.Exception_Message__c = exceptionMessage;
         newErrorRecord.Exception_Class__c= className;
         newErrorRecord.Method__c = methodName;
         newErrorRecord.Status__c = status;
         newErrorRecord.Error_Code__c = errorCode;
         newErrorRecord.Exception_Details__c = exceptionDetails;
         newErrorRecord.Stack_Trace__c = stackTrace;
         newErrorRecord.Line_Number__c = lineNo;
         Database.insert(newErrorRecord,false);
     }
}