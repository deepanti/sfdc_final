public class HomePageOpportunityInPipelineController {
    @AuraEnabled(cacheable=true)
    //function to fetch opportunity in pipeline 
    public static List<Opportunity> getOpportunityPipeline(Boolean isSeller)
    { 
        string userId =  userInfo.getUserId().substring(0, 15); 
        try
        {
            
            List<Opportunity> oppList;
            if(!isSeller)
            {
                oppList=[select Amount,CloseDate,Name,StageName,Opportunity_ID__c from Opportunity where (Opportunity.OwnerId=:userId or Opportunity.Account.Sales_Leader_User_id__c=:userId or Opportunity.Account.Client_Partner__c=:userId ) and isClosed=False  and stageName!='Prediscover' limit: Limits.getLimitQueryRows()];    
            }
            else
            {
                oppList=[select Amount,CloseDate,Name,StageName,Opportunity_ID__c from Opportunity where Opportunity.OwnerId=:userId and stageName!='Prediscover' and isClosed=False limit: Limits.getLimitQueryRows()];    
            }
            
            return oppList;
        }
        catch(Exception e)
        {
            throw new AuraHandledException(e.getMessage());    
        }
    }
}