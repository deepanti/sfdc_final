/**
 * @author: Prashant Kumar1
 * @since: 14/06/2018
 * @description:
 * 
*/
public class VIC_KickersIncentiveBatchHelper {
    
    public List<Target_Achievement__c> getIncentiveRecordsForOpportunity(Opportunity opp, 
                                                                         Map<Id, String> userIdVsPlanCode,
                                                                         Map<Id, Target_Component__c> userIdVsTargetComponent,
                                                                         Decimal tcvLimit,
                                                                         Decimal incentiveAmt,
                                                                         Map<Id, User> usersMap)
    {
        List<Target_Achievement__c> incentiveRecords = new List<Target_Achievement__c>();
        //Map<Id, Decimal> userIdvsTotalTCV = new Map<Id, Decimal>();
        Map<Id, TCVOliWrapper> userIdvsTotalTCVOliWrapperObj = new Map<Id, TCVOliWrapper>();
        System.debug('OLI-> ' + opp.OpportunityLineItems);
        //adding total TCV for each user on Opportunity Products of a Opportunity
        for(OpportunityLineItem oppProd: opp.OpportunityLineItems){
            List<Id> userIds = this.getOpportunityProductUsers(oppProd);
            for(Id userId : userIds){
                if(userIdVsPlanCode.get(userId) == 'IT_GRM'){
                    System.Debug('USER---> ' + userId);
                    System.Debug('USER OLI---> ' + oppProd);
                    if((oppProd.Opportunity.OwnerId == userId && oppProd.vic_Sales_Rep_Approval_Status__c != null && oppProd.vic_Sales_Rep_Approval_Status__c.containsIgnoreCase('Approved') ) 
                    || (oppProd.Product_BD_Rep__c != null && oppProd.Product_BD_Rep__c == userId && oppProd.vic_Product_BD_Rep_Approval_Status__c != null && oppProd.vic_Product_BD_Rep_Approval_Status__c.containsIgnoreCase('Approved')) 
                    || (oppProd.vic_VIC_User_3__c != null && oppProd.vic_VIC_User_3__c == userId) 
                    || (oppProd.vic_VIC_User_4__c != null && oppProd.vic_VIC_User_4__c == userId)){
                        System.Debug('***********USER ENTERED***************');
                        if(userIdvsTotalTCVOliWrapperObj.containsKey(userId)){
                            userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue += tcvContribution(oppProd, userId);
                        }else{
                            TCVOliWrapper tcvOliWrapperObj = new TCVOliWrapper(tcvContribution(oppProd, userId), oppProd.Id);
                            userIdvsTotalTCVOliWrapperObj.put(userId, tcvOliWrapperObj);
                        }
                        
                    }
                    
                    
                    /*System.debug('INSIDE IT GRM PLAN');
                    System.debug('USER ID -> ' + userId);
                    if(userIdvsTotalTCVOliWrapperObj.containsKey(userId)){
                        if(
                            (userId == oppProd.Opportunity.Id && oppProd.vic_Sales_Rep_Approval_Status__c != null && oppProd.vic_Sales_Rep_Approval_Status__c == 'Approved') ||
                            (userId == oppProd.Product_BD_Rep__c && oppProd.vic_Product_BD_Rep_Approval_Status__c != null && oppProd.vic_Product_BD_Rep_Approval_Status__c == 'Approved')
                        ){
                            System.debug('ID--> ' + oppProd.id);
                            Decimal totalTCV = 0;
                            Decimal previousTCV = userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue;
                            Decimal tcvOfThisOppProd = oppProd.TCV__c;
                            totalTCV = previousTCV + tcvOfThisOppProd;
                            userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue = totalTCV;
                        }
                    }
                    else{
                        Decimal tcvOfThisOppProd = oppProd.TCV__c;
                        TCVOliWrapper tcvOliWrapperObj = new TCVOliWrapper(tcvOfThisOppProd, oppProd.Id);
                        userIdvsTotalTCVOliWrapperObj.put(userId, tcvOliWrapperObj);
                    } */                   
                }
            }
        }
        System.debug('-----> ' + userIdvsTotalTCVOliWrapperObj);
        String salesCountry = opp.Sales_country__c;
        //get these variable from metadata incentive constants        
        for(Id userId : userIdvsTotalTCVOliWrapperObj.keySet()){
           System.debug('tcvvalue'+userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue);
           System.debug('tcvlimit'+tcvLimit);
            if(userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue > tcvLimit){
                Decimal pppMultiplier = 1;
                //checking whether user met condition for PPP multiplier
                if(VIC_CountryCostTypesCtlr.isQualifiedForPPP(usersMap.get(userId).Domicile__c, salesCountry)){
                    String PPPStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_BDE_Purchase_Power_Parity');
                    pppMultiplier = VIC_GeneralChecks.isDecimal(PPPStr) ? Decimal.valueOf(PPPStr) : 0;
                }
                if(userIdVsTargetComponent.get(userId) != null){                    
                    Target_Achievement__c incentiveRecord = this.createIncentiveRecord(userIdVsTargetComponent.get(userId),
                                                                                       opp, incentiveAmt,
                                                                                       userIdvsTotalTCVOliWrapperObj.get(userId).tcvValue,
                                                                                       pppMultiplier,
                                                                                       userIdvsTotalTCVOliWrapperObj.get(userId).firstOLIId, userId);
                    System.debug(incentiveRecord);
                    if(incentiveRecord != null){
                        incentiveRecords.add(incentiveRecord);
                    }
                }
                else{
                    System.debug('====>> Target Component not available for user: '+ userId);
                }
            }else{                
                System.debug('0.01 INCENTIVE CREATE FOR: '+ userId);
                if(userIdVsTargetComponent.get(userId) != null){                    
                    System.debug('0.01 INCENTIVE CREATE FOR: '+ userId + ' ----------ENTERED--------');
                    Decimal pppMultiplier = 1;
                    if(VIC_CountryCostTypesCtlr.isQualifiedForPPP(usersMap.get(userId).Domicile__c, salesCountry)){
                        String PPPStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_BDE_Purchase_Power_Parity');
                        pppMultiplier = VIC_GeneralChecks.isDecimal(PPPStr) ? Decimal.valueOf(PPPStr) : 0;
                    }
                
                    Target_Component__c targetComponent = userIdVsTargetComponent.get(userId);
                    List<Target_Achievement__c> oldIncentiveRecords = this.getUserOldIncentivesForTargetComponentRelatedToOpportunity(targetComponent, opp, userId);
                    if(oldIncentiveRecords.isEmpty()){
                        Target_Achievement__c incentiveRecord = new Target_Achievement__c();
                        incentiveRecord.vic_is_First_Incentive_Record_For_OLI__c = true;
                        incentiveRecord.Bonus_Amount__c = 0.01;
                        incentiveRecord.vic_Exclude_For_Calculation__c = true;
                        incentiveRecord.Target_Component__c = targetComponent.Id;
                        incentiveRecord.vic_PPP__c = pppMultiplier;
                        incentiveRecord.vic_Incentive_Type__c = 'New';
                        incentiveRecord.Opportunity__c = opp.Id;
                        incentiveRecord.vic_Opportunity_Product_Id__c = userIdvsTotalTCVOliWrapperObj.get(userId).firstOLIId;
                        incentiveRecord.vic_Incentive_Details__c = 'ZERO INCENTIVE RECORD';
                        incentiveRecord.Achievement_Date__c = opp.Actual_Close_Date__c;
                        incentiveRecord.vic_Account__c = opp.AccountId;
                        incentiveRecord.vic_Status__c = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Default_Incentive_Status');
                        incentiveRecords.add(incentiveRecord);
                    }
                }
            }
        }
        
        return incentiveRecords;
    }
    
    private Decimal tcvContribution(OpportunityLineItem oppProd, String userId){
        /*Decimal tcvAmount = 0;
        if(oppProd.Opportunity.OwnerId == userId && oppProd.vic_Primary_Sales_Rep_Split__c != null){
            tcvAmount = oppProd.TCV__c * oppProd.vic_Primary_Sales_Rep_Split__c;
        }else if(oppProd.Product_BD_Rep__c != null && oppProd.Product_BD_Rep__c == userId && oppProd.Opportunity.OwnerId != oppProd.Product_BD_Rep__c && oppProd.vic_Product_BD_Rep_Split__c != null){
            tcvAmount = oppProd.TCV__c * oppProd.vic_Product_BD_Rep_Split__c;
        }else if(oppProd.vic_VIC_User_3__c != null && oppProd.vic_VIC_User_3__c == userId && oppProd.vic_VIC_User_3_Split__c != null){
            tcvAmount = oppProd.TCV__c * oppProd.vic_VIC_User_3_Split__c;
        }else if(oppProd.vic_VIC_User_4__c != null && oppProd.vic_VIC_User_4__c == userId && oppProd.vic_VIC_User_4_Split__c != null){
            tcvAmount = oppProd.TCV__c * oppProd.vic_VIC_User_4_Split__c;
        }
        return tcvAmount;*/
        Decimal tcvAmount = 0;
        if(oppProd.Opportunity.OwnerId == userId && oppProd.vic_Primary_Sales_Rep_Split__c != null){
            Decimal splitValue = oppProd.vic_Primary_Sales_Rep_Split__c;
            tcvAmount = (oppProd.TCV__c * splitValue)/100;
        }else if(oppProd.Product_BD_Rep__c != null && oppProd.Product_BD_Rep__c == userId && oppProd.Opportunity.OwnerId != oppProd.Product_BD_Rep__c && oppProd.vic_Product_BD_Rep_Split__c != null){
            Decimal splitValue = oppProd.vic_Product_BD_Rep_Split__c;
            tcvAmount = (oppProd.TCV__c * splitValue)/100;
        }else if(oppProd.vic_VIC_User_3__c != null && oppProd.vic_VIC_User_3__c == userId && oppProd.vic_VIC_User_3_Split__c != null){
            Decimal splitValue = oppProd.vic_VIC_User_3_Split__c;
            tcvAmount = (oppProd.TCV__c * splitValue)/100;
        }else if(oppProd.vic_VIC_User_4__c != null && oppProd.vic_VIC_User_4__c == userId && oppProd.vic_VIC_User_4_Split__c != null){
            Decimal splitValue = oppProd.vic_VIC_User_4_Split__c;
            tcvAmount = (oppProd.TCV__c * splitValue)/100;
        }
        return tcvAmount;
    }
    
    
    @TestVisible
    private Target_Achievement__c createIncentiveRecord(Target_Component__c targetComponent, Opportunity opp, Decimal incentiveAmt, Decimal totalTcv,
                                                        Decimal pppMultiplier, Id firstOppProdId, String userId)
    {
        System.debug('RP-> incentiveAmt : ' + incentiveAmt);
        System.debug('RP-> pppMultiplier : ' + pppMultiplier);
        System.debug('RP-> totalTcv : ' + totalTcv);
        
        Target_Achievement__c incentiveRecord = new Target_Achievement__c();
        List<Target_Achievement__c> oldIncentiveRecords = this.getUserOldIncentivesForTargetComponentRelatedToOpportunity(targetComponent, opp, userId);
        
        incentiveRecord.vic_PPP__c = pppMultiplier;
        incentiveRecord.Target_Component__c = targetComponent.Id;
        incentiveRecord.Achievement_Date__c = opp.Actual_Close_Date__c;
        incentiveRecord.Opportunity__c = opp.Id;
        incentiveRecord.vic_Opportunity_Product_Id__c = firstOppProdId;
        incentiveRecord.vic_Account__c = opp.AccountId;
        incentiveAmt = incentiveAmt * pppMultiplier;
        incentiveRecord.vic_Status__c = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Default_Incentive_Status');
                        
        if(oldIncentiveRecords.isEmpty()){
            System.debug('----------NO OLD INCENTIVE---------');
            if(incentiveAmt == 0){
                incentiveRecord = null;
            }
            else{
                incentiveRecord.vic_Incentive_Type__c = 'New';
                incentiveRecord.vic_is_First_Incentive_Record_For_OLI__c = true;
                incentiveRecord.Bonus_Amount__c = incentiveAmt;
                incentiveRecord.vic_Incentive_Details__c = 'Total TCV of user is ' + totalTcv 
                    + 'and hence calculated incentive is ' + incentiveAmt +'. ';
            }
            
        }
        else{
            System.debug('----------OLD INCENTIVE FOUND---------');
            Decimal totalPreviouslyAddedBonus = 0;
            for(Target_Achievement__c oldInc: oldIncentiveRecords){
                totalPreviouslyAddedBonus += oldInc.Bonus_Amount__c;
            }
            System.debug('OLD Incentive -> ' + totalPreviouslyAddedBonus);
            if(incentiveAmt - totalPreviouslyAddedBonus == 0){
                incentiveRecord = null;
            }
            else{
                incentiveRecord.vic_Incentive_Type__c = 'Adjustment';
                incentiveRecord.Bonus_Amount__c = incentiveAmt - totalPreviouslyAddedBonus;
                
                incentiveRecord.vic_Incentive_Details__c = 'Total TCV of user is ' + totalTcv + ' and hence calculated incentive is ' 
                    + incentiveAmt 
                    + '. Previously given incentive is ' + totalPreviouslyAddedBonus 
                    + ', so the actual incentive amount is ' + (incentiveAmt - totalPreviouslyAddedBonus) + '.';
            }
        }
        return incentiveRecord;
    }
    
    /*
    * @params: List of UserIds(List<Id>)
    * @return: Map of userId Vs Plan(Map<Id, String>)
    * @description: This method returns the userId vs PlanCodeMap
    * 
    */
    public Map<Id, String> getUsersVsPlanMap(Set<Id> userIds){
        Map<Id, String> usersVsPlanCodes = new Map<Id, String>();
        Map<Id, Id> usersVsMasterVICRole = new Map<Id, Id>();
        Set<Id> masterVICRoles = new Set<Id>();
        Map<Id, String> masterVICRoleVsPlanCode = new Map<Id, String>();
        system.debug('userid'+userIds);
        LIst<User_VIC_Role__c> userVICRoleMappings = [SELECT Id, Master_VIC_Role__c, User__c 
                                                      FROM User_VIC_Role__c 
                                                      WHERE User__c =: userIds 
                                                      AND vic_For_Previous_Year__c = false
                                                      AND User__r.isActive = true
                                                      AND Not_Applicable_for_VIC__c = false];
        
        for(User_VIC_Role__c obj: userVICRoleMappings){
            usersVsMasterVICRole.put(obj.User__c, obj.Master_VIC_Role__c);
            masterVICRoles.add(obj.Master_VIC_Role__c);
        }
             
        //Current year will be taken from custome settings later
        String currentYearStr = String.valueOf(System.today().year());
        
        List<VIC_Role__c> vicRoles = [SELECT Id, Plan__r.vic_Plan_Code__c, Master_VIC_Role__c 
                                      FROM VIC_Role__c 
                                      WHERE Master_VIC_Role__c =:masterVICRoles 
                                      AND Plan__r.Year__c =: currentYearStr 
                                      AND  Plan__r.Is_Active__c = true
                                      AND Plan__r.vic_Plan_Code__c != null];
        
        for(VIC_Role__c obj: vicRoles){
            masterVICRoleVsPlanCode.put(obj.Master_VIC_Role__c, obj.Plan__r.vic_Plan_Code__c);
        }
        
        // creating the required map
        for(Id userId : usersVsMasterVICRole.keySet()){
            usersVsPlanCodes.put(userId, masterVICRoleVsPlanCode.get(usersVsMasterVICRole.get(userId)));
        }
        
        return usersVsPlanCodes;
    }
    
  
   /*
    * @params: Set of userIds for which target components are required (Set<Id>),
    *       Set of Opportunity products ids for which incentive records on target component is queried (Set<Id>)
    * @return: Map of user Id vs List of target components of user (Map<Id, List<Target_Component__c>>)
    * @description: This method fetch target components of user and it's child incentve records filtered with opportunity id
    * 
    */
    public Map<Id, Target_Component__c> getTargetComponents(Set<Id> userIds, Set<Id> oppIds){
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        List<Target_Component__c> targetComponents = [ SELECT Id, Master_Plan_Component__c, Target__c, Target__r.User__c, 
                                                      Target__r.vic_TCV_IO__c, Target__r.vic_TCV_TS__c,
                                                      Master_Plan_Component__r.vic_Component_Code__c,
                                                        (
                                                            SELECT Id, vic_Opportunity_Product_Id__c, Opportunity__c, Bonus_Amount__c,
                                                            Target_Component__r.Target__r.User__c
                                                            FROM Target_Achievements__r
                                                            WHERE Opportunity__c =: oppIds
                                                        )
                                                        FROM Target_Component__c
                                                        WHERE Target__r.User__c =:userIds
                                                        AND Master_Plan_Component__r.vic_Component_Code__c ='Kickers'
                                                        AND Target__r.Start_Date__c =:processingYearStart
                                                    ];
        
        Map<Id, Target_Component__c> userIdVsTargetComponentMap = new Map<Id, Target_Component__c>();
        
        // preparing userIdVsTargetComponentsMap
        for(Target_Component__c obj: targetComponents){
            userIdVsTargetComponentMap.put(obj.Target__r.User__c, obj);
        }
        
        System.debug('userIdVsTargetComponentMap : ' + userIdVsTargetComponentMap);
        return userIdVsTargetComponentMap;
    }
    
    /*
    * @params: Target Component, Opportunity
    * @return: List Of Incentives (List<Target_Achievement__c>)
    * @description: This method returns list of old incentive 
    * records creared for user in this target component filtered by Opportuity Id
    */
    public List<Target_Achievement__c>
        getUserOldIncentivesForTargetComponentRelatedToOpportunity(Target_Component__c targetComponent, Opportunity opp,String userId)
    {
        List<Target_Achievement__c> oldIncentiveRecords = new List<Target_Achievement__c>();
        if(targetComponent != null){
            for(Target_Achievement__c inc: targetComponent.Target_Achievements__r){
                if(inc.Opportunity__c == opp.Id && inc.Target_Component__r.Target__r.User__c == userId){
                    oldIncentiveRecords.add(inc);
                }
            }
        }
        return oldIncentiveRecords;
    }
    
  /*
    * @params: OpprtunityProduct
    * @return: List Of User Ids of OpprtunityProduct
    * @decription: This method returns the list of users involved in sales process of Opportunity product
    * 
    */
    public List<Id> getOpportunityProductUsers(OpportunityLineItem oppProd){
        List<Id> userIds = new List<Id>();
        if(oppProd.Opportunity.OwnerId != null){
            userIds.add(oppProd.Opportunity.OwnerId);
        }
        if(oppProd.Product_BD_Rep__c != null){
            if(oppProd.Product_BD_Rep__c != oppProd.Opportunity.OwnerId){
                userIds.add(oppProd.Product_BD_Rep__c);
            }
        }
        if(oppProd.vic_VIC_User_3__c != null){
            userIds.add(oppProd.vic_VIC_User_3__c);
        }
        if(oppProd.vic_VIC_User_4__c != null){
            userIds.add(oppProd.vic_VIC_User_4__c);
        }
        return userIds;
    }
    
    /*Wrapper class*/
    public class TCVOliWrapper{
        Decimal tcvValue;
        Id firstOLIId;
        TCVOliWrapper(Decimal tcvValue, Id firstOLIId){
            this.tcvValue = tcvValue;
            this.firstOLIId = firstOLIId;
        }
    }
}