global class olddata_splitupdate implements Database.Batchable<sobject> {

    global Database.QueryLocator start(Database.BatchableContext BC) {
    //    string stage='6. Signed Deal';
        date d = Date.newInstance(2018, 1, 1);
        String query = 'Select id,name,TCV1__c From opportunity where TCV1__c>0 and actual_close_date__c > :d limit 100';

    //    string i = '0069000000zw4bB';
    //    String query = 'Select id,name,TCV1__c From opportunity where id =:i';
                if(Test.isRunningTest())
            query = 'Select id,name,TCV1__c From opportunity limit 100';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <opportunity> opp) {
   //    datetime d = Datetime.newInstance(2016, 1, 1);
list<id> oppids = new list<id>();
Map<String, opportunity> oppmap = new Map<String, opportunity>(opp);
for(opportunity objCS : opp)
{
    oppids.add(objCS.id);
}
system.debug('id='+oppids);
//oppids.add([select id from opportunity where id = '0069000000zvSCL'].id);
Map<string, OpportunityTeamMember> opptmvalues = new Map<string, OpportunityTeamMember>();
Map<string, OpportunityTeamMember> opptmbdrepvalues = new Map<string, OpportunityTeamMember>();
list<string> roles = new list<string>{'Opportunity Owner','Super SL','SL'};
list<OpportunitySplit> addsplit = new list<OpportunitySplit>();
list<OpportunitySplit> addsplitbd = new list<OpportunitySplit>();        
OpportunitySplit tempsplit;
OpportunitySplit tempsplita;
decimal a,b,c,d;
string unique,u2,u3;
//************************Checking for bdrep**************************************
Map<string, OpportunityLineItem> oppolivalues = new Map<string, OpportunityLineItem>();
        OpportunityLineItem tempoli = new OpportunityLineItem();
        list<user> actbdreps = new list<user>();
actbdreps = [select id from user where isactive=true and profileid!='00e90000000WFRNAA4'];
for(OpportunityLineItem otm :[select Product_BD_Rep__c,OpportunityId,unitprice from OpportunityLineItem where OpportunityId in :oppids and unitprice>0 and Product_BD_Rep__c in:actbdreps])
{
    unique=string.valueOf(otm.OpportunityId);
    u2=string.valueOf(otm.Product_BD_Rep__c);
    u3=unique + u2;
    if(oppolivalues.containskey(u3))
    {
        tempoli = new OpportunityLineItem();
        system.debug('oppolivalues.get(u3).unitprice=='+oppolivalues.get(u3).unitprice);
        system.debug('otm.UnitPrice=='+otm.UnitPrice);
        tempoli.UnitPrice=oppolivalues.get(u3).unitprice+otm.UnitPrice;
        tempoli.Product_BD_Rep__c=otm.Product_BD_Rep__c;
        tempoli.OpportunityId=otm.OpportunityId;
        oppolivalues.remove(u3);
        oppolivalues.put(u3,tempoli);
    }
    else
    {
        oppolivalues.put(u3,otm);
    }
}
        system.debug('oppolivalues='+oppolivalues);
        //search for bdreps in team members***************************** and remove from bdreps map
/*       
        for(OpportunityTeamMember otm :[select TeamMemberRole,OpportunityId,UserId from OpportunityTeamMember where OpportunityId in :oppids])
{
    unique=string.valueOf(otm.OpportunityId);
    u2=string.valueOf(otm.UserId);
    u3=unique + u2;
    opptmbdrepvalues.put(u3,otm);
    oppolivalues.remove(u3);
}
*/
         Map<string, OpportunitySplit> oppspvaluesa = new Map<string, OpportunitySplit>();
for(OpportunitySplit ots :[select OpportunityId,SplitOwnerId from OpportunitySplit where OpportunityId in :oppids and SplitTypeId = '14990000000fxXIAAY'])
{
    unique=string.valueOf(ots.OpportunityId);
    u2=string.valueOf(ots.SplitOwnerId);
    u3=unique + u2;
    oppspvaluesa.put(u3,ots);
    if(!Test.isRunningTest())
	oppolivalues.remove(u3);
}
 //       system.debug('oppolivalues.size() ='+oppolivalues.size());
 //       system.debug(' oppolivalues = '+oppolivalues);
 //       system.debug(' opptmbdrepvalues = '+opptmbdrepvalues);
        opportunity testopp=new opportunity();
        if(oppolivalues.size()>0||Test.isRunningTest())
            {
            for(string uni : oppolivalues.keyset())
            {
              //  system.debug('key='+uni);
             //  system.debug('OpportunityId = '+opptmvalues.get(uni).OpportunityId);
             //    system.debug('UserId = '+opptmvalues.get(uni).UserId);
                tempsplita=new OpportunitySplit();
             tempsplita.OpportunityId = oppolivalues.get(uni).OpportunityId;
                a=oppmap.get(oppolivalues.get(uni).OpportunityId).TCV1__c; //tcv from opportunity
               // a=testopp.unitprice;
                b= oppolivalues.get(uni).unitprice; //tcv from OLI
                c=(b/a)*100;
                tempsplita.SplitPercentage = c;
                tempsplita.SplitTypeId = '14990000000fxXIAAY';
                tempsplita.SplitOwnerId = oppolivalues.get(uni).Product_BD_Rep__c;
                system.debug('tempsplita = '+tempsplita);
                if(c>0&&c<=100)addsplitbd.add(tempsplita);
            }
  //          system.debug('No of BDRep splits to add = '+addsplitbd.size());
  //          system.debug('Splits to add = '+addsplitbd);
            //try{
             //   List<OpportunitySplit> listStrings = new List<OpportunitySplit>(addsplit);
            //system.debug('listStrings='+listStrings);
             try
             {
                 insert addsplitbd;
            }
            catch(exception e)
            {string allstringa = string.join(addsplitbd,',');
  CreateErrorLog.createErrorRecord(null,e.getMessage(), allstringa, e.getStackTraceString(),'olddata_splitupdate', ' ','Fail','',String.valueOf(e.getLineNumber()));     
    //   system.debug('error e='+e.getmessage());
            }
        }
        ////////////
        
for(OpportunityTeamMember otm :[select TeamMemberRole,OpportunityId,UserId from OpportunityTeamMember where OpportunityId in :oppids and TeamMemberRole IN :roles])
{
    unique=string.valueOf(otm.OpportunityId);
    u2=string.valueOf(otm.UserId);
    u3=unique + u2;
    opptmvalues.put(u3,otm);
}
 system.debug('opptmvalues = '+opptmvalues);
        
Map<string, OpportunitySplit> oppspvalues = new Map<string, OpportunitySplit>();
for(OpportunitySplit ots :[select OpportunityId,SplitOwnerId from OpportunitySplit where OpportunityId in :oppids and SplitTypeId = '14990000000fxXIAAY'])
{
    unique=string.valueOf(ots.OpportunityId);
    u2=string.valueOf(ots.SplitOwnerId);
    u3=unique + u2;
    oppspvalues.put(u3,ots);
}
system.debug('team members = '+ opptmvalues.size() + ' | splits to remove = '+oppspvalues.size());
for(string key:oppspvalues.keyset())
{
    opptmvalues.remove(key);
}

//system.debug('team size = '+opptmvalues.size());
//system.debug('team members = '+opptmvalues);

        
      
if(opptmvalues.size()>0||Test.isRunningTest())
{
for(string uni : opptmvalues.keyset())
{
    tempsplit=new OpportunitySplit();
  //  system.debug('key='+uni);
 //  system.debug('OpportunityId = '+opptmvalues.get(uni).OpportunityId);
 //    system.debug('UserId = '+opptmvalues.get(uni).UserId);
 tempsplit.OpportunityId = opptmvalues.get(uni).OpportunityId;
    tempsplit.SplitPercentage = 100;
    tempsplit.SplitTypeId = '14990000000fxXIAAY';
    tempsplit.SplitOwnerId = opptmvalues.get(uni).UserId;
  addsplit.add(tempsplit);
}
}
//system.debug('No of splits to add = '+addsplit.size());
//system.debug('Splits to add = '+addsplit);
try{
 //   List<OpportunitySplit> listStrings = new List<OpportunitySplit>(addsplit);
//system.debug('listStrings='+listStrings);
   insert addsplit;
}
catch(exception e)
{ 
    string allstring = string.join(addsplit,',');
  CreateErrorLog.createErrorRecord(null,e.getMessage(), allstring, e.getStackTraceString(),'olddata_splitupdate', ' ','Fail','',String.valueOf(e.getLineNumber()));     
    system.debug('error e='+e.getmessage());
}
    }

    global void finish(Database.BatchableContext BC) {
    }
}