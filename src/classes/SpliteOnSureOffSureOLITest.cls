@isTest
public class SpliteOnSureOffSureOLITest
{
    static testMethod void SpliteOnSureOffSureOLITestMethod()      
    {
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
         Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
         Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
         Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');              
                AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
         Opportunity  OppObj = GEN_Util_Test_Data.CreateOpportunity('TestOpportunity',oAccount.id,null);
         Product2 ProductObj  = GEN_Util_Test_Data.CreateProduct('Test_Product');
         OpportunityProduct__c oOpportunityProduct = GEN_Util_Test_Data.CreateOpportunityProduct(OppObj.id,ProductObj.id,12,50421);
         oOpportunityProduct.Mode__c='Onsite & India Offshore';
         update oOpportunityProduct;
         
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,6,12000);
        ProductSchedule__c oProductSchedule1 = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
        
        
     String StrQuery = 'SELECT Id, Name,TCVLocal__c,Mode__c,FTE_OffShore__c,FTE_Onsite__c, (select id,name,RollingYearRevenueLocal__c,OpportunityProduct__c from RevenueSchedule__r),(select id,name,RevenueLocal__c,OpportunityProductId__c,RevenueScheduleId__c from ProductSchedules__r) FROM OpportunityProduct__c WHERE Mode__c=\'Onsite & India Offshore\'';
     SpliteOnSureOffSureOLI reassign = new SpliteOnSureOffSureOLI(StrQuery);
     ID batchprocessid = Database.executeBatch(reassign,1);  
   }
}