public without sharing class GPInputPicklistService  {
    
    private static final String SUCCESS_LABEL = 'SUCCESS';
    
    /*@AuraEnabled
    public static GPAuraResponse getPicklistOptions(String sObjectName, String sObjectFieldName) {
        Map < String, Schema.SObjectType > metaScehma = Schema.getGlobalDescribe();
        List<GPOptions> listOfOptions = new List<GPOptions>();
        Schema.SObjectType sobjectType = metaScehma.get(sObjectName);
        
        List<Schema.PicklistEntry> listOfPickListEntry = sobjectType
            .getDescribe()
            .fields.getMap()
            .get(sObjectFieldName)
            .getDescribe()
            .getPicklistValues();
        
        for (Schema.PicklistEntry picklistEntry: listOfPickListEntry) {
            listOfOptions.add(new GPOptions(picklistEntry.getValue(), picklistEntry.getLabel()));
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfOptions, true));
    }
    */
    @AuraEnabled
    public static List<GPOptions> getPicklistOptions(String sObjectName, String sObjectFieldName) {
        //trim extra spaces
        sObjectName = sObjectName.trim();
        sObjectFieldName = sObjectFieldName.trim();
        
        Map < String, Schema.SObjectType > metaScehma = Schema.getGlobalDescribe();
        List<GPOptions> listOfOptions = new List<GPOptions>();

        Schema.SObjectType sobjectType = metaScehma.get(sObjectName);
        
        List<Schema.PicklistEntry> listOfPickListEntry = sobjectType
            .getDescribe()
            .fields.getMap()
            .get(sObjectFieldName)
            .getDescribe()
            .getPicklistValues();
        
        for (Schema.PicklistEntry picklistEntry: listOfPickListEntry) {
            listOfOptions.add(new GPOptions(picklistEntry.getValue(), picklistEntry.getLabel()));
        }
        
      
        //if(listOfOptions.size() > 1)
        //    listOfOptions.add(0, new GPOptions('--NONE--', '--NONE--'));
        //else
        //    listOfOptions.add(new GPOptions('--NONE--', '--NONE--'));
    
        return listOfOptions;
    }
}