public class OpportunitySurveyListWrapper 
{
    @AuraEnabled
    public List<Opportunity> OpportunityDataList { get;set; }
}