public class OpportunityDetailPageController {
    Public String name{get; set;}
   	public static OpportunityLineItem oppProduct{get;set;}
    public string revenueStartDate{get;set;}
    public List<OppScheduleWrapper> oppLineItemScheduleList{get;set;}
    public string createdDate{get;set;}
    public string lastModifiedDate{get;set;}
    public Boolean showDeleteButton{get;set;}
    
    public OpportunityDetailPageController(ApexPages.StandardController controller){
       String recordID = ApexPages.CurrentPage().getparameters().get('id');
       
        system.debug('recordID=='+recordID);
     	OppProduct = [SELECT ID, Name, Product2ID, Product2.Name, OpportunityId, Opportunity.Name, Delivering_Organisation__c,opportunity.stageName, 
                      Delivery_Location__c, Ebit__c, FTE__c, LastModifiedBy.name, Local_Currency__c, Opportunity_Id__c,lastModifiedDate,
                      Pricer_name__c, Product_BD_Rep__r.Name, Revenue_Start_Date__c, Sub_Delivering_Organisation__c,createdDate,
                      UnitPrice, createdby.name, Contract_Term__c, CYR__c, NYR__c, Revenue_Exchange_Rate__c, Y1_TCV_USD_OLI_CPQ__c,
                      Y10_TCV_USD_OLI_CPQ__c, Y2_TCV_USD_OLI_CPQ__c, Y3_TCV_USD_OLI_CPQ__c, Y4_TCV_USD_OLI_CPQ__c, 
                      Y5_TCV_USD_OLI_CPQ__c, Y6_TCV_USD_OLI_CPQ__c, Y7_TCV_USD_OLI_CPQ__c, Y8_TCV_USD_OLI_CPQ__c, Y9_TCV_USD_OLI_CPQ__c,
                      Y1_AOI_USD_OLI_CPQ__c, Y10_AOI_USD_OLI_CPQ__c, Y2_AOI_USD_OLI_CPQ__c, Y3_AOI_USD_OLI_CPQ__c, Y4_AOI_USD_OLI_CPQ__c, 
                      Y5_AOI_USD_OLI_CPQ__c, Y6_AOI_USD_OLI_CPQ__c, Y7_AOI_USD_OLI_CPQ__c, Y8_AOI_USD_OLI_CPQ__c, Y9_AOI_USD_OLI_CPQ__c, 
                      Total_TCV_Product_CPQ__c, Overall_AOI_CPQ__c, Pricing_Pricer_Name_OLI_CPQ__c, Pricing_Deal_Type_OLI_CPQ__c,
					  EBIT_OLI_CPQ__c, Price_line_Exchg_Rate__c, QSRM_Status__c, opportunity.Deal_Status_In_CPQ__c, 
                      Opportunity.OwnerId, opportunity.product_Count__c FROM OpportunityLineItem WHERE ID =: recordID];
      
        name = oppProduct.Product2.Name + ' for ' + oppProduct.Opportunity.Name;
		
		revenueStartDate = String.valueOf(oppProduct.Revenue_Start_Date__c);
       
        List<OpportunityLineItemSchedule> ScheduleList = [SELECT ID, Revenue, Type, Quantity, Description, ScheduleDate
                       FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId =: recordID ORDER BY ScheduleDate ASC LIMIT 24];
        
        createdDate = oppProduct.createdDate.format('yyyy-MM-dd hh:mm:ss a');
        lastModifiedDate = oppProduct.lastModifiedDate.format('yyyy-MM-dd hh:mm:ss a');
        oppLineItemScheduleList = new List<OppScheduleWrapper>();
        
        for(OpportunityLineItemSchedule oppSchedule :ScheduleList ){
        	  oppLineItemScheduleList.add(new OppScheduleWrapper(oppSchedule));  
        }
        
        ID loggedInUserID = UserInfo.getUserId();
        
        if(loggedInUserID != oppProduct.Opportunity.ownerId && loggedInUserID != oppProduct.Product_BD_Rep__c){
        	showDeleteButton = false;    
        }
        else{
            system.debug('oppProduct.QSRM_Status__c=='+oppProduct.QSRM_Status__c);
            System.debug('oppProduct.opportunity.product_Count__c =='+oppProduct.opportunity.product_Count__c );
            if((oppProduct.QSRM_Status__c!='Your QSRM is submitted for Approval' && oppProduct.QSRM_Status__c != 'Submitted') && (oppProduct.opportunity.StageName =='Prediscover' || oppProduct.opportunity.StageName =='1. Discover'
           		|| oppProduct.opportunity.StageName =='2. Define' || oppProduct.opportunity.StageName =='3. On Bid'
           		||oppProduct.opportunity.StageName =='4. Down Select')){
                    if(oppProduct.opportunity.product_Count__c ==1 && (oppProduct.opportunity.StageName =='2. Define' || oppProduct.opportunity.StageName =='3. On Bid'
           				||oppProduct.opportunity.StageName =='4. Down Select')){
                            System.debug('in product_Count__c ==');
                    	showDeleteButton=false; 	    
                    }
                    else{
                    	System.debug('opp stage=='+oppProduct.opportunity.StageName);
                		showDeleteButton = true;    
                    }
                    
            } 
            else{ 
                System.debug('in else==='); 
                showDeleteButton=false; 
                //errorMessage = 'Post QSRM addition and Down Select stage, Product cannot be deleted.'; 
                
            }
              
        }
         User loggedInUser = [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
                //showError = true;   
                showDeleteButton = true;  
            } 
    }
    
    @RemoteAction
    public static PageReference deleteOppProduct(ID oppProductID, ID opportunityID){
        try{
            OpportunityLineItem oli = [SELECT ID FROM OpportunityLineItem WHERE ID = :oppProductID];
            system.debug('oli=='+oli);
        	DELETE oli;
            PageReference pf = new PageReference('/'+opportunityID);
            pf.setRedirect(true);
            return pf;   
        }
        catch(Exception e){
			System.debug('ERROR==='+e.getMessage()+ '' +e.getLineNumber());
        }
        return null;
    }
    
    public class OppScheduleWrapper{
        public Decimal revenue{get;set;}
        public String scheduleDate{get;set;}
        public OppScheduleWrapper(OpportunityLineItemSchedule oppSchedule){
        	revenue = oppSchedule.Revenue;
            scheduleDate = (String.valueOf(oppSchedule.ScheduleDate)).substring(0,10);
        }
    }
}