@isTest
public class OpportunityLineItemTriggerHelperTest {
    public static Opportunity opp;
    public static User u, u1, u3,u4,u5,u6;
    public static OpportunityLineItem opp_item,opp_item2,opp_item3,opp_item4;
    public static Product2 product_obj;
    public static PricebookEntry entry;
    public static set<id> oppId = new set<id>();
    public static OpportunityLineItemSchedule oliSch;
    
    
    static void setupTestData(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        u4 = GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        u5 =GEN_Util_Test_Data.CreateUser('standarduser2079@testorg.com',p.Id,'standardusertestgen2079@testorg.com' );
        u6 = GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
        
        
            list<MainOpportunityHelperFlag__c> custmSettingList = new list<MainOpportunityHelperFlag__c>();
            MainOpportunityHelperFlag__c setting = new MainOpportunityHelperFlag__c(Name = 'MainOpportunityLineItemTrigger',HelperName__c = true);
            MainOpportunityHelperFlag__c setting1 = new MainOpportunityHelperFlag__c(Name = 'beforeInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting2 = new MainOpportunityHelperFlag__c(Name = 'beforeUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting3 = new MainOpportunityHelperFlag__c(Name = 'beforeDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting4 = new MainOpportunityHelperFlag__c(Name = 'afterInsertMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting5 = new MainOpportunityHelperFlag__c(Name = 'afterUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting6 = new MainOpportunityHelperFlag__c(Name = 'afterDeleteMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting7 = new MainOpportunityHelperFlag__c(Name = 'InsertAdditionalfieldsInOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting8 = new MainOpportunityHelperFlag__c(Name = 'createRevenueSchedule',HelperName__c = true);
            MainOpportunityHelperFlag__c setting9 = new MainOpportunityHelperFlag__c(Name = 'opportunityRestrictMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingq = new MainOpportunityHelperFlag__c(Name = 'insertOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinge = new MainOpportunityHelperFlag__c(Name = 'updateYearlyRevenue',HelperName__c = true);
            MainOpportunityHelperFlag__c settingr = new MainOpportunityHelperFlag__c(Name = 'delteHelperMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingt = new MainOpportunityHelperFlag__c(Name = 'deleteOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinga = new MainOpportunityHelperFlag__c(Name = 'dealTypeMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settings = new MainOpportunityHelperFlag__c(Name = 'updateContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingd = new MainOpportunityHelperFlag__c(Name = 'removeTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingf = new MainOpportunityHelperFlag__c(Name = 'Prediscover_RemoveTasksMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingg = new MainOpportunityHelperFlag__c(Name = 'deleteOppSplitOwneronOppOwnerchange',HelperName__c = true);
            MainOpportunityHelperFlag__c settingh = new MainOpportunityHelperFlag__c(Name = 'deleteContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingupdete = new MainOpportunityHelperFlag__c(Name = 'updateOpportunityFields',HelperName__c = true);
       		custmSettingList.add(setting); custmSettingList.add(setting1); custmSettingList.add(setting2);custmSettingList.add(setting3);
            custmSettingList.add(setting4); custmSettingList.add(setting5); custmSettingList.add(setting6);custmSettingList.add(setting7);
            custmSettingList.add(setting8); custmSettingList.add(setting9); custmSettingList.add(settingq);
            custmSettingList.add(settinge); custmSettingList.add(settingr); custmSettingList.add(settingt);
    		custmSettingList.add(settinga); custmSettingList.add(settings); custmSettingList.add(settingd);custmSettingList.add(settingf);
            custmSettingList.add(settingg); custmSettingList.add(settingh); custmSettingList.add(settingupdete); 
            Insert custmSettingList; 
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        System.runAs(u){
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
            
            Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
      
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
           Test.startTest();
             opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            system.debug('opp value in test class line 41'+opp);
             product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
             entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;           
            
            
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID,
                                                                   vic_VIC_User_4__c = u4.ID
                                                                   ); 
            insert opp_item;
            
            opp_item4 = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID,
                                                                   vic_VIC_User_4__c = u4.ID
                                                                   ); 
            insert opp_item4;
            
            delete opp_item4;
            
                       
            oppId.add(opp_item.id);
            system.debug('Opp Line item test class line 52'+opp_item);
             Test.stopTest();
           oliSch=  new OpportunityLineItemSchedule(scheduleDAte=date.today(),type='revenue',OpportunityLineItemId=opp_item.Id,revenue=400);
            insert oliSch;
            system.debug('id of oliSch'+oliSch.Id);
           
            GP_Sobject_Controller__c obj = new GP_Sobject_Controller__c();
            obj.GP_Enable_Sobject__c = true;
            obj.Name = 'opportunitylineitem';
            insert obj;
            
        }
        
    }
    
    static void setupTestData1(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        u4 = GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        u5 =GEN_Util_Test_Data.CreateUser('standarduser2079@testorg.com',p.Id,'standardusertestgen2079@testorg.com' );
        u6 = GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
        
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        System.runAs(u){
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
            
            Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
      
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
           Test.startTest();
             opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            system.debug('opp value in test class line 41'+opp);
             product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
             entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            
            opp_item2 = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID
                                                                   ); 
            insert opp_item2;
                       
            oppId.add(opp_item2.id);
            system.debug('Opp Line item test class line 52'+opp_item);
             Test.stopTest();
           oliSch=  new OpportunityLineItemSchedule(scheduleDAte=date.today(),type='revenue',OpportunityLineItemId=opp_item2.Id,revenue=400);
            insert oliSch;
           
            system.debug('id of oliSch'+oliSch.Id);
            
            
            list<MainOpportunityHelperFlag__c> custmSettingList = new list<MainOpportunityHelperFlag__c>();
            MainOpportunityHelperFlag__c setting = new MainOpportunityHelperFlag__c(Name = 'MainOpportunityLineItemTrigger',HelperName__c = true);
            MainOpportunityHelperFlag__c setting1 = new MainOpportunityHelperFlag__c(Name = 'beforeInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting2 = new MainOpportunityHelperFlag__c(Name = 'beforeUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting3 = new MainOpportunityHelperFlag__c(Name = 'beforeDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting4 = new MainOpportunityHelperFlag__c(Name = 'afterInsertMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting5 = new MainOpportunityHelperFlag__c(Name = 'afterUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting6 = new MainOpportunityHelperFlag__c(Name = 'afterDeleteMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting7 = new MainOpportunityHelperFlag__c(Name = 'InsertAdditionalfieldsInOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting8 = new MainOpportunityHelperFlag__c(Name = 'createRevenueSchedule',HelperName__c = true);
            MainOpportunityHelperFlag__c setting9 = new MainOpportunityHelperFlag__c(Name = 'opportunityRestrictMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingq = new MainOpportunityHelperFlag__c(Name = 'insertOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinge = new MainOpportunityHelperFlag__c(Name = 'updateYearlyRevenue',HelperName__c = true);
            MainOpportunityHelperFlag__c settingr = new MainOpportunityHelperFlag__c(Name = 'delteHelperMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingt = new MainOpportunityHelperFlag__c(Name = 'deleteOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinga = new MainOpportunityHelperFlag__c(Name = 'dealTypeMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settings = new MainOpportunityHelperFlag__c(Name = 'updateContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingd = new MainOpportunityHelperFlag__c(Name = 'removeTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingf = new MainOpportunityHelperFlag__c(Name = 'Prediscover_RemoveTasksMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingg = new MainOpportunityHelperFlag__c(Name = 'deleteOppSplitOwneronOppOwnerchange',HelperName__c = true);
            MainOpportunityHelperFlag__c settingh = new MainOpportunityHelperFlag__c(Name = 'deleteContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingupdete = new MainOpportunityHelperFlag__c(Name = 'updateOpportunityFields',HelperName__c = true);
       		custmSettingList.add(setting); custmSettingList.add(setting1); custmSettingList.add(setting2);custmSettingList.add(setting3);
            custmSettingList.add(setting4); custmSettingList.add(setting5); custmSettingList.add(setting6);custmSettingList.add(setting7);
            custmSettingList.add(setting8); custmSettingList.add(setting9); custmSettingList.add(settingq);
            custmSettingList.add(settinge); custmSettingList.add(settingr); custmSettingList.add(settingt);
    		custmSettingList.add(settinga); custmSettingList.add(settings); custmSettingList.add(settingd);custmSettingList.add(settingf);
            custmSettingList.add(settingg); custmSettingList.add(settingh); custmSettingList.add(settingupdete); 
            Insert custmSettingList; 
             
        }
        
    }
    
    static void setupTestData2(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        u4 = GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        u5 =GEN_Util_Test_Data.CreateUser('standarduser2079@testorg.com',p.Id,'standardusertestgen2079@testorg.com' );
        u6 = GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
        
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        System.runAs(u){
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
            
            Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
      
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
           Test.startTest();
             opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            system.debug('opp value in test class line 41'+opp);
             product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
             entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            
            
            
            opp_item3 = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_4__c = u1.ID
                                                                   ); 
            insert opp_item3;
                       
            oppId.add(opp_item3.id);
            system.debug('Opp Line item test class line 52'+opp_item);
             Test.stopTest();
           oliSch=  new OpportunityLineItemSchedule(scheduleDAte=date.today(),type='revenue',OpportunityLineItemId=opp_item3.Id,revenue=400);
            insert oliSch;
           
            system.debug('id of oliSch'+oliSch.Id);
            
            
            list<MainOpportunityHelperFlag__c> custmSettingList = new list<MainOpportunityHelperFlag__c>();
            MainOpportunityHelperFlag__c setting = new MainOpportunityHelperFlag__c(Name = 'MainOpportunityLineItemTrigger',HelperName__c = true);
            MainOpportunityHelperFlag__c setting1 = new MainOpportunityHelperFlag__c(Name = 'beforeInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting2 = new MainOpportunityHelperFlag__c(Name = 'beforeUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting3 = new MainOpportunityHelperFlag__c(Name = 'beforeDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting4 = new MainOpportunityHelperFlag__c(Name = 'afterInsertMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting5 = new MainOpportunityHelperFlag__c(Name = 'afterUpdateMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting6 = new MainOpportunityHelperFlag__c(Name = 'afterDeleteMethodOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting7 = new MainOpportunityHelperFlag__c(Name = 'InsertAdditionalfieldsInOli',HelperName__c = true);
            MainOpportunityHelperFlag__c setting8 = new MainOpportunityHelperFlag__c(Name = 'createRevenueSchedule',HelperName__c = true);
            MainOpportunityHelperFlag__c setting9 = new MainOpportunityHelperFlag__c(Name = 'opportunityRestrictMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingq = new MainOpportunityHelperFlag__c(Name = 'insertOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinge = new MainOpportunityHelperFlag__c(Name = 'updateYearlyRevenue',HelperName__c = true);
            MainOpportunityHelperFlag__c settingr = new MainOpportunityHelperFlag__c(Name = 'delteHelperMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingt = new MainOpportunityHelperFlag__c(Name = 'deleteOpportunityTeamMember',HelperName__c = true);
            MainOpportunityHelperFlag__c settinga = new MainOpportunityHelperFlag__c(Name = 'dealTypeMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settings = new MainOpportunityHelperFlag__c(Name = 'updateContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingd = new MainOpportunityHelperFlag__c(Name = 'removeTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingf = new MainOpportunityHelperFlag__c(Name = 'Prediscover_RemoveTasksMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingg = new MainOpportunityHelperFlag__c(Name = 'deleteOppSplitOwneronOppOwnerchange',HelperName__c = true);
            MainOpportunityHelperFlag__c settingh = new MainOpportunityHelperFlag__c(Name = 'deleteContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingupdete = new MainOpportunityHelperFlag__c(Name = 'updateOpportunityFields',HelperName__c = true);
       		custmSettingList.add(setting); custmSettingList.add(setting1); custmSettingList.add(setting2);custmSettingList.add(setting3);
            custmSettingList.add(setting4); custmSettingList.add(setting5); custmSettingList.add(setting6);custmSettingList.add(setting7);
            custmSettingList.add(setting8); custmSettingList.add(setting9); custmSettingList.add(settingq);
            custmSettingList.add(settinge); custmSettingList.add(settingr); custmSettingList.add(settingt);
    		custmSettingList.add(settinga); custmSettingList.add(settings); custmSettingList.add(settingd);custmSettingList.add(settingf);
            custmSettingList.add(settingg); custmSettingList.add(settingh); custmSettingList.add(settingupdete); 
            Insert custmSettingList; 
             
        }
        
    }
    
    static testMethod void insertOpportunityTeamMemberTest(){ 
        setupTestData();
        //test.startTest();
          
            List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
           Map<Id,OpportunityLineItem> mapOLI=new Map<ID,OpportunityLineItem>();
           for(OpportunityLineItem oli:new_OLI_list)
           {
            mapOLI.put(oli.Id,oli);   
           }
          system.debug('test class OLI list ...line 64 '+new_OLI_list);
          OpportunityLineItemTriggerHelper.insertOpportunityTeamMember(new_OLI_list);    
         
       // test.stopTest();
     }
     static testMethod void insertOpportunityTeamMemberTest1(){ 
        setupTestData();
        //test.startTest();
          
         List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
         new_OLI_list.add(opp_item);
         MainOpportunityLineItemTriggerHelper.beforeInsertMethod(new_OLI_list);
         MainOpportunityLineItemTriggerHelper.beforeDeleteMethod();
         MainOpportunityLineItemTriggerHelper.afterUndeleteMethod();
         Map<Id,OpportunityLineItem> mapOLI=new Map<ID,OpportunityLineItem>();
         for(OpportunityLineItem oli:new_OLI_list)
         {
             mapOLI.put(oli.Id,oli);   
         }
         String profileName = 'Genpact super admin';
         system.debug('test class OLI list ...line 64 '+new_OLI_list);
             MainOpportunityLineItemTriggerHelper.afterInsertMethod(new_OLI_list,mapOLI,profileName);
         MainOpportunityLineItemTriggerHelper.afterUpdateMethod(new_OLI_list,mapOLI,profileName);
             MainOpportunityLineItemTriggerHelper.afterDeleteMethod(new_OLI_list,mapOLI); 
         MainOpportunityLineItemTriggerHelper.beforeUpdateMethod(new_OLI_list,mapOLI);
             OpportunityLineItemTriggerHelper.updateRevenueSchedules(opp.Id);
         // test.stopTest();
     }
   @isTest public static void CongaFieldUpdate()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
          
       //test.startTest();
         OpportunityLineItemTriggerHelper.setContractTermField(new_OLI_list);
        
         
     //  test.stopTest();
       
   }
      @isTest public static void CongaFieldUpdate1()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
          
      // test.startTest();
        
         OpportunityLineItemTriggerHelper.setContractTermField(new_OLI_list,opp_item.OpportunityId);
         
       //test.stopTest();
       
   }
     @isTest public static void revenuSch()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
       //test.startTest();
         OpportunityLineItemTriggerHelper.createRevenueSchedule(new_OLI_list); ///oppId
          
      // test.stopTest();
       
   }
      @isTest public static void revenuSch1()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
       //test.startTest();
        
          OpportunityLineItemTriggerHelper.updateAdditionalfieldsInOpportunityLineItems(oppId);
  
      // test.stopTest();
       
   }
      @isTest public static void revenuSch2()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
       //test.startTest();
         
          OpportunityLineItemTriggerHelper.updateYearlyRevenue(oppId);
      // test.stopTest();
       
   }
     @isTest public static void updateOppField()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
      // test.startTest();
          OpportunityLineItemTriggerHelper.InsertAdditionalfieldsInOpportunityLineItems(new_OLI_list);
         
      // test.stopTest();
       
   }
     @isTest public static void updateOppField1()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
       //test.startTest();
     
          OpportunityLineItemTriggerHelper.updateOpportunityFields(new_OLI_list);
       //test.stopTest();
       
   }
      @isTest public static void deleteOppTeam()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
      // test.startTest();
           OpportunityLineItemTriggerHelper.deleteOpportunityTeamMember(new_OLI_list);   
       //test.stopTest();
       
   }
      @isTest public static void updateOppTeam()
   {
       setupTestData();
        List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
             new_OLI_list.add(opp_item);
        Map<Id,OpportunityLineItem> mapOLI=new Map<ID,OpportunityLineItem>();
           for(OpportunityLineItem oli:new_OLI_list)
           {
            mapOLI.put(oli.Id,oli);   
           }
      // test.startTest();
           OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(mapOLI,new_OLI_list);
      // test.stopTest();
       
   }
   
    @isTest public static void updateVICUsers2()
   {
        setupTestData2();
        
            OpportunityLineItem oli = [Select Id,vic_VIC_User_3__c,vic_VIC_User_4__c from
                                        OpportunityLineItem where ID=:opp_item3.Id];
            
            
            Map<Id,OpportunityLineItem> oldOLIMap =new Map<ID,OpportunityLineItem>();
           
            oldOLIMap.put(oli.Id,oli);            
            oli.vic_VIC_User_4__c = u5.Id;
            
            //update oli;
            
            Set<Id> userIds = new Set<Id>();
            userIds.add(oli.vic_VIC_User_4__c);
               
            List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
            new_OLI_list.add(oli);
            
            OpportunityLineItemTriggerHelper.getUserSuperVisors(userIds);
            OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(oldOLIMap,new_OLI_list);
            
   }
   @isTest public static void updateVICUsers1()
   {
        setupTestData1();
        
            OpportunityLineItem oli = [Select Id,vic_VIC_User_3__c,vic_VIC_User_4__c from
                                        OpportunityLineItem where ID=:opp_item2.Id];
            
            system.debug('@@@opp_item@@@'+opp_item2);
            system.debug('@@@oli@@@'+oli);
            Map<Id,OpportunityLineItem> oldOLIMap =new Map<ID,OpportunityLineItem>();
           
            oldOLIMap.put(oli.Id,oli);            
            oli.vic_VIC_User_3__c = u5.Id;
            
            //update oli;
            
            
            List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
            new_OLI_list.add(oli);
            
            
            OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(oldOLIMap,new_OLI_list);
            OpportunityLineItemTriggerHelper.iterateMethod();
           
   }
    public static void testIterate(){
        OpportunityLineItemTriggerHelper.iterateMethod();
    }
   
   @isTest public static void updateVICUsers()
   {
        setupTestData();
            
            OpportunityLineItem oli = [Select Id,vic_VIC_User_3__c,vic_VIC_User_4__c from
                                        OpportunityLineItem where ID=:opp_item.Id];
            
            
            Map<Id,OpportunityLineItem> oldOLIMap =new Map<ID,OpportunityLineItem>();
           
            oldOLIMap.put(oli.Id,oli);            
            oli.vic_VIC_User_3__c = u5.Id;
            oli.vic_VIC_User_4__c = u6.Id;
            
            update oli;
            
              
            List<OpportunityLineItem> new_OLI_list = new List<OpportunityLineItem>();
            new_OLI_list.add(oli);
            
            OpportunityLineItemTriggerHelper.updateOpportunityTeamMember(oldOLIMap,new_OLI_list);
           
   }
    
   
  
  
   
 
}