@isTest
private class GPBatchUpdateCustomerOnPrjAddSchTracker {
	@testSetup
    private static void setUpData() {
        GP_Customer_Master__c customer = new GP_Customer_Master__c();
        customer.Name = 'New Customer';
        customer.GP_Oracle_Customer_Id__c = '1234';
        customer.GP_Is_Dummy__c = true;
        insert customer;
        
        GP_Address__c address = new GP_Address__c();
        address.GP_Address_Line_1__c = 'New Delhi';
        address.GP_Billing_Entity_Id__c ='1234';
        address.GP_Customer_Account_ID__c = '1234';
        address.GP_Site_Use_Code__c = 'SHIP_TO';
        address.GP_Status__c = 'Active';
        insert address;
    }
    
    @isTest
    private static void testUpdateCustomerOfAddressScheduler() {
        Test.StartTest();
        GPBatchUpdateCustomerOnPrjAddSchedular obj = new GPBatchUpdateCustomerOnPrjAddSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Territory Check', sch, obj);
        Test.stopTest();
    }
    
     @isTest
    private static void testUpdateCustomerOfAddressBatch() {
        Test.StartTest();
        GPBatchUpdateCustomeMasterOnPrjAddress batcher = new GPBatchUpdateCustomeMasterOnPrjAddress();
        Id batchprocessid = Database.executeBatch(batcher, 10);
        Test.StopTest();
    }
        
}