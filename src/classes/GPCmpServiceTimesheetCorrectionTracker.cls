@isTest
public class GPCmpServiceTimesheetCorrectionTracker {
    public static GP_Employee_Master__c empObj;
    public static employeeProjectAllocationData objResponseProjectAllocation;
    public static GP_Timesheet_Transaction__c objTimeSheetTransaction;
    public static GP_Project__c objProject;
    public static GP_Project_Task__c objProjectTask;
    
    @testSetup static void setupCommonData() 
    {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Final_OHR__c = '123456778';
        empObj.GP_Person_Id__c = '123456778';
        insert empObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Latest_Open_PA_Period__c = 'Jun-17';
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO'; 
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        timesheettrnsctnObj.GP_Month_Year__c = 'Jun-17';
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        timeshtentryObj.GP_Project_Oracle_PID__c = '4532';
        timeshtentryObj.GP_Project_Task_Oracle_Id__c = 'ORACLE-001';
        insert timeshtentryObj ;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        insert address ;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert resourceAllocationObj; 
        
        GP_Job__c objJob = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJob;
        
        prjObj.GP_Oracle_PID__c='4532';
        prjObj.GP_Is_Closed__c = false;
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Oracle_Status__c = 'S';
        update prjObj;
        
        GP_Timesheet__c timesheetRecord = new GP_Timesheet__c();
        timesheetRecord.GP_Person_Id__c = '123456778';
        timesheetRecord.GP_Employee_Number__c = '123456778';
        timesheetRecord.GP_Project_Number__c = '4532';
        timesheetRecord.GP_Task_Number__c = 'ORACLE-001';
        DateTime entryDate = Date.ValueOf('2018-06-01');
        String monthYear = 'Jun-17';
        timesheetRecord.GP_Month_Year__c = monthYear;
        timesheetRecord.GP_Expenditure_Item_Date__c = system.today().addMonths(3);
        timesheetRecord.GP_Expenditure_Type__c = 'Billable';
        timesheetRecord.GP_Hours__c = 7;
        timesheetRecord.GP_PersonId_MonthYear__c = timesheetRecord.GP_Person_Id__c + monthYear;
        insert timesheetRecord;
        
        GP_Temporary_Data__c objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJob.Id,
                                                                                                  '123456778',
                                                                                                  '4532',
                                                                                                  'ORACLE-001');
        objTemporaryTimeSheet.GP_TSC_Date__c  = system.today().addMonths(3);
        insert objTemporaryTimeSheet;
        
        GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        projectTaskObj1.GP_Active__c = true;
        projectTaskObj1.GP_Task_Number__c = 'ORACLE-001';
        projectTaskObj1.GP_Project_Number__c  = '4532';
        projectTaskObj1.GP_Start_Date__c = system.today().addMonths(3);
        insert projectTaskObj1;
    }     
    
    @isTest static void testGPCmpServiceTimesheetCorrection()
    {
        fetchData();
        testgetIsValidUser();
        testgetTimeSheetEntries();
        testsaveTimeSheetEntries();
        testgetJobId();
        testgetProjectRecord();
        testgetProjectTaskRecord();
        testsaveTimeSheetEntriesWithApproval();
        testgetJobId();
        testgetTimeSheetEntriesNoRecord();
        testtimesheetStatusToApproveOrReject();
        testrecallRequest();
        testTimesheetTransactions();
    }
    
    public static void fetchData() { 
        empObj =[select id from GP_Employee_Master__c limit 1];
        objTimeSheetTransaction = [select id from GP_Timesheet_Transaction__c limit 1];
        objProject = [select id from GP_Project__c limit 1];
        objProjectTask = [select id from GP_Project_Task__c limit 1];
    }
    
    public static void testtimesheetStatusToApproveOrReject()
    {
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.timesheetStatusToApproveOrReject(objTimeSheetTransaction.id,'','');
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testTimesheetTransactions()
    {
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.getTimesheetTransactions('{"GP_Employee__c":"'+empObj.Id+'","GP_Month_Year__c":"Jun-17"}');
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testrecallRequest()
    {
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.recallRequest(objTimeSheetTransaction.id);
        //System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testgetIsValidUser()
    {
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.getIsValidUser(null);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetTimeSheetEntries()
    {
        GPAuraResponse  responseData=GPCmpServiceTimesheetCorrection.getTimeSheetEntries('{"GP_Employee__c":"'+empObj.Id+'","GP_Month_Year__c":"Jun-17"}');
        
        objResponseProjectAllocation = (employeeProjectAllocationData)JSON.deserialize(responseData.response,employeeProjectAllocationData.Class);
        System.assertEquals(true, responseData.isSuccess);
    }
    
    public static void testgetTimeSheetEntriesNoRecord()
    {
        GPAuraResponse  responseData=GPCmpServiceTimesheetCorrection.getTimeSheetEntries('{"GP_Employee__c":"'+empObj.Id+'","GP_Month_Year__c":"Jul-17"}');
        
        objResponseProjectAllocation = (employeeProjectAllocationData)JSON.deserialize(responseData.response,employeeProjectAllocationData.Class);
        System.assertEquals(true, responseData.isSuccess);
    }
    
    public static void testsaveTimeSheetEntries()
    {
        objResponseProjectAllocation.wrapperTimeSheetEntries[0].lstOfTimeSheetEntries[0].GP_Modified_Hours__c  = 8;
        String jsonresp= JSON.serialize(objResponseProjectAllocation.wrapperTimeSheetEntries, true);    
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.saveTimeSheetEntries(jsonresp,false,'Jun-17',objTimeSheetTransaction.Id,empObj.Id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testsaveTimeSheetEntriesWithApproval()
    {
        objResponseProjectAllocation.wrapperTimeSheetEntries[0].lstOfTimeSheetEntries[0].GP_Modified_Hours__c  = 8;
        String jsonresp= JSON.serialize(objResponseProjectAllocation.wrapperTimeSheetEntries, true);    
        GPAuraResponse returnedResponse = GPCmpServiceTimesheetCorrection.saveTimeSheetEntries(jsonresp,true,'Jun-17',objTimeSheetTransaction.Id,empObj.Id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    private static Void testgetJobId(){
        GPCmpServiceTimesheetCorrection.getJobId();
    }   
    
    private static void testgetProjectRecord(){
        GPCmpServiceTimesheetCorrection.getProjectRecord(objProject.Id);
    }
    
    private static void testgetProjectTaskRecord(){
        GPCmpServiceTimesheetCorrection.getProjectTaskRecord(objProjectTask.Id);
    }
    
    @isTest public static void testgetTimeSheetEntries1()
    {
        fetchData();
        testgetIsValidUser();
        
        GP_Timesheet_Entry__c objTimesheet_Entry = [Select Id,GP_Employee__c,GP_Timesheet_Transaction__c from GP_Timesheet_Entry__c limit 1];
        delete objTimesheet_Entry;
        
        GPAuraResponse  responseData=GPCmpServiceTimesheetCorrection.getTimeSheetEntries('{"GP_Employee__c":"'+empObj.Id+'","GP_Month_Year__c":"Jun-17"}');
        
        objResponseProjectAllocation = (employeeProjectAllocationData)JSON.deserialize(responseData.response,employeeProjectAllocationData.Class);
        System.assertEquals(true, responseData.isSuccess);
    }
}