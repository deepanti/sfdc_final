@istest
Public class Tasktest{
    public static Opportunity oppty;
    public static Opportunity opp;
    public static Opportunity opp1;
    public static User u, u1, u3;
    public static OpportunityLineItem opp_item;
    public static OpportunityLineItem opp_item1;
    public   static List<opportunity> updateopplist;
    public static QSRM__c qsrm;


 public static void setupTestData()
{
            
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
              
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
        //System.runAs(u){
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
        Id RecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pre-discover Opportunity').getRecordTypeId();
       
        opp1 =new opportunity(name='123428937892',StageName='Prediscover',CloseDate=system.today()+1, recordTypeId = RecordTypeId1,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id);
            insert opp1;
            
            
           opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,Amount=11000000,GCI_Coach__c='Greg Wilk',
                                Submit_for_SL_approval_on_GCI__c=true,Annuity_Project__c='Project',Margin__c=12,Summary_of_opportunity__c='TestSummary',Contract_type__c='SOW/LOE/Work Order',Number_of_Contract__c=1);
            
            CheckRecursive.run=true;
            insert opp;
           /* ID QSRMrecordTypeId = Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('TS QSRM').getRecordTypeId();
            qsrm = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy');
            insert qsrm; */
    
            Product2 product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
            
            PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,Quantity=1); 
            CheckRecursiveForOLI.runAfter=true; 
            CheckRecursiveForOLI.runBefore=true;
            insert opp_item;
            
            opp_item1 = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u.ID,
                                                                   unitPrice = 100.00, TCV__c = 10000000,Quantity=1); 
            CheckRecursiveForOLI.runAfter=true; 
            CheckRecursiveForOLI.runBefore=true; 
            insert opp_item1;
            
        // QSRM__c oQsrm=GEN_Util_Test_Data.CreateQSRM(opp.Id,Userinfo.getUserId());
         
        }
            
           public static testMethod void TestMethodFinal12(){
                setupTestData();
                test.starttest();
         
         List<opportunity> updateopplist= new list<opportunity>();
             
             opp.StageName = '2. Define';
             updateopplist.add(opp);
             
             opp1.StageName='1. Discover';
             updateopplist.add(opp1);
             
             CheckRecursive.run=true;
             update updateopplist;
        
         test.stoptest();
           }
             
             public static testMethod void TestMethodFinal13(){
                setupTestData();
                test.starttest();
             //opp.amount=11000000;
             opp.StageName='2. Define';
             opp.Submit_for_SL_approval_on_GCI__c=false;
             CheckRecursive.run=true;
             Update opp;
             test.stoptest();
             }
     public static testMethod void TestMethodFinal21(){
                setupTestData();
                test.starttest();
             
             opp.Margin__c=12;
             opp.stageName='3. On Bid';
             CheckRecursive.run=true;
             Update opp;
             test.stoptest();
             }
             
             public static testMethod void TestMethodFinal14(){
                setupTestData();
                test.starttest();
             
             opp.GCI_Coach__c='Andrew Kukielka';
             opp.stageName='3. On Bid';
             CheckRecursive.run=true;
             Update opp;
             test.stoptest();
             }
             public static testMethod void TestMethodFinal15(){
                setupTestData();
                test.starttest();
            
             opp.StageName='4. Down Select';
             CheckRecursive.run=true;
             Update opp;
             test.stoptest();
             }
            
                public static testMethod void TestMethodFinal16(){
                setupTestData();
                test.starttest();
            
             opp1.Contract_type__c='SOW/LOE/Work Order';
             opp1.stageName='5. Confirmed';
             //opp1.Forecast_Category1__c='Commit';
             CheckRecursive.run=true;
             Update opp1;
             
             test.stoptest();
                }
         
         public static testMethod void TestMethodFinal17(){
                setupTestData();
                test.starttest();
            
          
             opp1.stageName='6. Signed';
            
             CheckRecursive.run=true;
             Update opp1;
             
             test.stoptest();
                }
         public static testMethod void TestMethodFinal19(){
                setupTestData();
                test.starttest();
            
          List<Id> OptyIds = new List<Id>();
            OptyIds.add(opp.id);
            TaskCreationHandler.InsertTaskOnDefineStage(OptyIds);
             
             test.stoptest();
                }
         
         

}