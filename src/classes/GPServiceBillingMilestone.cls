/**
 * @group ServiceLayer. 
 *
 * @description Service layer for Project Billing Milestone.
 */
public with sharing class GPServiceBillingMilestone {

    public class GPServiceBillingMilestoneException extends Exception {}

    private static final String ITO_DATE_RANGE_ERROR_LABEL = 'For ITO projects start date and end dates should be between project start and end date.';
    private static final String BILLING_MILESTONE_TOTAL_ERROR_LABEL = 'Billing Milestone sum amount should be equal to project TCV amount.';
    private static final String ITO_DATE_ERROR_LABEL = 'For ITO projects start date and end dates are mandatory';
    private static final String DATE_RANGE_ERROR_LABEL = 'Milestone Date should be within project duration.';
    private static final String PERCENTAGE_ERROR_LABEL = 'Value cannot be greater than 100 and less than 0.';
    private static final String AMOUNT_NEGATIVE_LABEL = 'Amount cannot be less than equal to zero.';
    private static final String AMOUNT_ERROR_LABEL = 'Value cannot be 0.';
	// Gainshare Change
    private static final String BILLING_MILESTONE_GAINSHARE_TOTAL_ERROR_LABEL='Billing Milestone sum amount should be less or equal to project TCV amount.';
    public static Boolean isFormattedLogRequired = true;
    public static Boolean isLogForTemporaryId = true;

    /**
     * @description Validate Project Billing Milestone.
     * 
     * @param listOfBillingMilestone : List of Project Billing Milestone that needs to be validated.
     * @param project : Project Record whose Billing Milestone needs to be validated.
     *
     * @return Map < String, List < String >> : Return Map of Sub Section with List of errors.
     */
    public static Map < String, List < String >> validateBillingMilestone(GP_Project__c project, List < GP_Billing_Milestone__c > listOfBillingMilestone) {
        Map < String, List < String >> mapOfSectionToListOfErrors = new Map < String, List < String >> ();

        Double totalBillingAmount = 0;
        String validationMessage, key;

        if (listOfBillingMilestone != null && listOfBillingMilestone.size() > 0) {
            for (GP_Billing_Milestone__c billingMilestone: listOfBillingMilestone) {
                totalBillingAmount += billingMilestone.GP_Amount__c != null ? billingMilestone.GP_Amount__c : 0;

                if (isFormattedLogRequired) {
                    key = 'Billing Milestone';
                } else if (isLogForTemporaryId) {
                    key = billingMilestone.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = billingMilestone.Id;
                }

                //"Amount cannot be less than equal to zero.",
                /*if (billingMilestone.GP_Amount__c == null || billingMilestone.GP_Amount__c <= 0) {
                    validationMessage = billingMilestone.Name + ': ' + AMOUNT_NEGATIVE_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }*/
                //Added By Insha
                if (billingMilestone.GP_Date__c == null || billingMilestone.GP_Milestone_start_date__c == null ||
                    billingMilestone.GP_Milestone_end_date__c == null) {
                    validationMessage = billingMilestone.Name + ': ' + 'Start Date,End Date,Milestone Date is Required';

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }
                if (billingMilestone.GP_Milestone_start_date__c > billingMilestone.GP_Milestone_end_date__c) {
                    validationMessage = billingMilestone.Name + ': ' + 'Start Date cannot be greater than End Date';

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }

                //Milestone Date should be within project duration."
                /*if (billingMilestone.GP_Date__c == null || billingMilestone.GP_Date__c < project.GP_Start_Date__c ||
                    billingMilestone.GP_Date__c > project.GP_End_Date__c) {
                    validationMessage = billingMilestone.Name + ': ' + DATE_RANGE_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }*/

                //PERCENTAGE_ERROR_LABEL: "Value cannot be greater than 100 and less than 0.
                if (billingMilestone.GP_Value__c == null ||
                    (billingMilestone.GP_Entry_type__c == 'Percent' &&
                        (billingMilestone.GP_Value__c < 0 ||
                            billingMilestone.GP_Value__c > 100))) {

                    validationMessage = billingMilestone.Name + ': ' + PERCENTAGE_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }

                //AMOUNT_ERROR_LABEL: "Value cannot be 0.
                if (billingMilestone.GP_Value__c == null ||
                    billingMilestone.GP_Value__c == 0) {
                    validationMessage = billingMilestone.Name + ': ' + AMOUNT_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }

                Boolean isITOType = project.GP_Delivery_Org__c != null && project.GP_Delivery_Org__c == 'CMITS' &&
                    project.GP_Sub_Delivery_Org__c != null && project.GP_Sub_Delivery_Org__c == 'ITO';
                //For ITO type start date and en date is mandatory.
                /*if (isITOType && (billingMilestone.GP_Milestone_start_date__c == null ||
                        billingMilestone.GP_Milestone_end_date__c == null)) {

                    validationMessage = billingMilestone.Name + ': ' + ITO_DATE_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }*/

                //"ITO Milestone Start Date and End date should be within project duration."
                /* if (isITOType && (billingMilestone.GP_Milestone_start_date__c < project.GP_Start_Date__c ||
                         billingMilestone.GP_Milestone_end_date__c > project.GP_End_Date__c)) {
                     validationMessage = billingMilestone.Name + ': ' + ITO_DATE_RANGE_ERROR_LABEL;

                     if (!mapOfSectionToListOfErrors.containsKey(key)) {
                         mapOfSectionToListOfErrors.put(key, new List < String > ());
                     }

                     mapOfSectionToListOfErrors.get(key).add(validationMessage);
                 }*/
            }
			// Gainshare Change
            // Avinash : Valueshare changes. Value Share -Outcome,Value Share-SLA OD
            if (project.GP_TCV__c < totalBillingAmount 
            && (project.GP_Project_type__c == 'Gainshare' ||
            project.GP_Project_type__c == 'Value Share -Outcome' ||
            project.GP_Project_type__c == 'Value Share-SLA OD')) {

                validationMessage = BILLING_MILESTONE_GAINSHARE_TOTAL_ERROR_LABEL;

                if (isFormattedLogRequired) {
                    key = 'Billing Milestone';
                } else if (isLogForTemporaryId) {
                    key = project.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = project.Id;
                }

                if (!mapOfSectionToListOfErrors.containsKey(key)) {
                    mapOfSectionToListOfErrors.put(key, new List < String > ());
                }

                mapOfSectionToListOfErrors.get(key).add(validationMessage);
            }
			// Gainshare Change
            // Avinash : Valueshare changes. Value Share -Outcome,Value Share-SLA OD
            if (project.GP_TCV__c != totalBillingAmount 
            && project.GP_Project_type__c != 'Gainshare'
            && project.GP_Project_type__c != 'Value Share -Outcome'
            && project.GP_Project_type__c != 'Value Share-SLA OD') {

                validationMessage = BILLING_MILESTONE_TOTAL_ERROR_LABEL;

                if (isFormattedLogRequired) {
                    key = 'Billing Milestone';
                } else if (isLogForTemporaryId) {
                    key = project.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = project.Id;
                }

                if (!mapOfSectionToListOfErrors.containsKey(key)) {
                    mapOfSectionToListOfErrors.put(key, new List < String > ());
                }

                mapOfSectionToListOfErrors.get(key).add(validationMessage);
            }
        }

        return mapOfSectionToListOfErrors;
    }

    /**
     * @description Validate whether billing milestone can be done or not for a particular type of project.
     * 
     * @param project : Project Record whose billing milestone entries have to be validated.
     *
     * @return Boolean : TRUE/FALSE whether billing milestone can be done for the project passed.
     */
    public static Boolean isBillingMilestoneDefinable(GP_Project__c project) {
        return project.RecordType.Name != 'BPM';
    }

    /**
     * @description Validate whether billing milestone can be deleted.
     * 
     * @param billingMilestone : billing milestone entry that has to be validated.
     *
     * @return Boolean : TRUE/FALSE whether billing milestone can be done for the project passed.
     */
    public static Boolean isBillingMilestoneDelitable(GP_Billing_Milestone__c billingMilestone) {
        return billingMilestone.GP_Date__c >= System.today() &&
            billingMilestone.GP_Parent_Billing_Milestone__c != null;
    }

    /**
     * @description Validate whether billing milestone can be edited.
     * 
     * @param billingMilestone : billing milestone entry that has to be validated.
     *
     * @return Boolean : TRUE/FALSE whether billing milestone can be done for the project passed.
     */
    public static Boolean isBillingMilestoneEditable(GP_Billing_Milestone__c billingMilestone) {
        return billingMilestone.GP_Billing_Status__c != 'Billed';
    }
}