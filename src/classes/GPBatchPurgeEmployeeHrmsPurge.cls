global class GPBatchPurgeEmployeeHrmsPurge implements Database.Batchable<sobject>, Database.stateful,Schedulable {
    integer ScopeSize=0;
    global Database.QueryLocator Start(Database.BatchableContext bc)
    {
        return database.getQueryLocator(system.label.GP_Employee_Master_Purge);
    }
    global void Execute(Database.BatchableContext bc, List<sobject> scope)
    {        
        if(scope.size()>0)
        {
            Database.DeleteResult[] drList = Database.delete(scope, false);
            for(Database.DeleteResult dr : drList) {
                if (dr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    ScopeSize = ScopeSize+1;
                }                
            }
        }
    }
    global void finish(database.BatchableContext bc)
    {
        if(ScopeSize>0)
        {
            Messaging.singleEmailMessage Emailwithattch = new Messaging.singleEmailMessage();
            String[] receiverAddr=System.Label.GP_Batch_Job_Email.split(',');
            string[] address = receiverAddr;           
            Emailwithattch.setSubject('Employee Master and HRMS Request and Response are deleteted');        
            Emailwithattch.setToaddresses(address);        
            Emailwithattch.setPlainTextBody('Employee Master and HRMS Request and Response are deleteted. Total Deleted Records = '+ScopeSize); 
            //Emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});        
            //Sends the email   
            Id oweaId = GPCommon.getOrgWideEmailId();
            if (oweaId != null) {
                Emailwithattch.setOrgWideEmailAddressId(oweaId);
            }
            Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Emailwithattch});
        }
    }
    global void Execute(SchedulableContext sc)
    {        
        GPBatchPurgeEmployeeHrmsPurge objBatch=new GPBatchPurgeEmployeeHrmsPurge();
        Database.executeBatch(objBatch);
    }

}