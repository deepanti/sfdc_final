//Test For GenTfieldupdate
@isTest
public class GenTfieldupdateTest
{
    
    public static testMethod void RunTest() 
    { 
        UserRole Obj = new UserRole();
        Obj.Name='TestRole';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id); 
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test1@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        try
        {
            QSRM__c oQSRM=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,Userinfo.getUserId());
            QSRM__c oQSRM1=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,Userinfo.getUserId());
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('Duplicate Name of QSRM') ? true : false;
            //system.assertEquals(expectedExceptionThrown,true);       
        } 
        
    }

}