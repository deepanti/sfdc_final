/**
* @author: Shivam Singh
* @date: March 2018
*/

@isTest
public class vic_UtilTest {
   
    public static Master_VIC_Role__c getMasterVICRole(){
        Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
        masterVICRoleObj.is_Active__c = true;
        return masterVICRoleObj;
    }
    
    public static User_VIC_Role__c getUserVICRole(){
        User_VIC_Role__c userVICRole = new User_VIC_Role__c();
        userVICRole.User__c=UserInfo.getUserId();
        userVICRole.Not_Applicable_for_VIC__c = false;
        return userVICRole;
    }
    
     public static Target__c getTarget(){
        Target__c targetObj = new Target__c();
        targetObj.Start_Date__c = Date.newInstance(2017, 1, 1);
        targetObj.user__c =UserInfo.getUserId();
        return targetObj;
     }
    
    public static Target_Component__c getTargetComponent(){
        Target_Component__c targetComponentObj = new Target_Component__c();
        targetComponentObj.Target_Status__c = 'Active';
        return targetComponentObj;
    }
    
    public static Target_payout__c getTargetPayout(){
        Target_payout__c targetPayout = new Target_payout__c();
        targetPayout.Amount__c = 1000;
        targetPayout.Payout_Date__c = system.today();
        system.assertequals(200,200);
        return targetPayout;
    }
}