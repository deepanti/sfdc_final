global class GPBatchCloseWonOpportunityProject implements Database.Batchable < sObject > , Database.Stateful,schedulable {
    global date dateofOppClosure = System.Today().adddays(-Integer.valueOf(system.label.GP_Close_Opp_Project_After_Won_Day));

    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('dateofOppClosure'+dateofOppClosure);
        return Database.getQueryLocator([Select Id, Name, stagename, Probability, Opportunity_ID__c from Opportunity where stagename = '6. Signed Deal'
            and Actual_Close_Date__c =: dateofOppClosure
        ]);
    }

    global void execute(SchedulableContext sc)
    {
        GPBatchCloseWonOpportunityProject objAutoReject = new GPBatchCloseWonOpportunityProject();
		Database.executebatch(objAutoReject,1); 
 
    }

    global void execute(Database.BatchableContext bc, List < Opportunity > listOpportunity) {

        savepoint sp = database.setSavepoint();
        try {
            set < string > setOppportunityId = new set < string > ();
            map < string, Opportunity > mpOppIdToopp = new map < string, Opportunity > ();
            for (Opportunity objOpp: listOpportunity) {
                setOppportunityId.add(objOpp.Opportunity_ID__c);
                mpOppIdToopp.put(objOpp.Opportunity_ID__c, objOpp);
            }
            if (setOppportunityId.size() > 0) { 
                list < GP_Opportunity_Project__c > lstOpportunityProjectToUpdate = new list < GP_Opportunity_Project__c > ();
                list < GP_Opportunity_Project__c > lstOpportunityProject = new GPSelectorOpportunityProject().selectOppProjectNoClosed(setOppportunityId);

                for (GP_Opportunity_Project__c objOppProject: lstOpportunityProject) {
                    if (mpOppIdToopp.get(objOppProject.GP_Opportunity_Id__c) != null)
                        lstOpportunityProjectToUpdate.add(new GP_Opportunity_Project__c(id = objOppProject.id,
                            GP_Probability__c = mpOppIdToopp.get(objOppProject.GP_Opportunity_Id__c).Probability,
                            GP_StageName__c = mpOppIdToopp.get(objOppProject.GP_Opportunity_Id__c).StageName,
                            GP_Project_Status_Code__c = 'CLOSED',
                            GP_IsUpdated__c = true));
                }

                if (lstOpportunityProjectToUpdate.size() > 0) {
                    update lstOpportunityProjectToUpdate;
                }
            }
        } catch (exception ex) {

            database.rollback(sp);
        }


    }

    global void finish(Database.BatchableContext bc) {}
}