public class GW1_MakeDSROwnerController {
    
    private ApexPages.StandardController oppcontroller;
    private GW1_DSR__c objDSR= new GW1_DSR__c ();
    public string StrError{get;set;}
    public GW1_MakeDSROwnerController(ApexPages.StandardController controller) 
    {
        
       if (!Test.isRunningTest())
        {
            controller.addFields(new List<String> { 'Id',  'name','OwnerId','GW1_Bid_Manager__c' });
        }
        this.oppController = controller;
        objDSR= (GW1_DSR__c ) controller.getRecord();
    }
    
    public pageReference changeOwner()
    {
        Set<id> SetOwnerID = new set<id>();
        List<ConnectApi.BatchInput> batchInputsFeeds = new List<ConnectApi.BatchInput>();  // USed For Feeds
        if (!Test.isRunningTest())
        {
            if(string.valueOf(objDSR.ownerID).startsWith('005'))
            {
                        SetOwnerID.add(objDSR.ownerID);
                        
            }
        }
        
        if( objDSR.GW1_Bid_Manager__c!= null )
        {
           StrError='You cannot change the tower lead as deal manger is alredy assigned, please contact administrator if you want to change.';
            return null;
        }
       
        if(objDSR.id!=null && objDSR.ownerID!=null && objDSR.ownerID!=userInfo.getUserID() && objDSR.GW1_Bid_Manager__c== null )
        {   
            objDSR.ownerID=userInfo.getUserID();
            /*SetOwnerID.add(objDSR.ownerID);
            list<GW1_DSR_Team__c>  lstDSRTeamPresent  = new list<GW1_DSR_Team__c >([select id ,GW1_User__c,GW1_DSR__c ,GW1_Role__c from GW1_DSR_Team__c where  GW1_DSR__c =:objDSR.id]);
            List <GW1_DSR_Team__c> listDSRTeamtoInActive = new list<GW1_DSR_Team__c>();
            list <GW1_DSR_Team__c> lstDSRTeam = new list<GW1_DSR_Team__c>();
            Map<string, GW1_DSR_Team__c>  mapDSRUserIDToDSRTeam = new map<string,GW1_DSR_Team__c>();
            
            for (GW1_DSR_Team__c objDSRTeam :lstDSRTeamPresent)
            {
                string key = objDSRTeam.GW1_DSR__c+'-'+ objDSRTeam.GW1_User__c;
                mapDSRUserIDToDSRTeam.put(key,objDSRTeam);
            }
            
            
          
                                    
            string key =objDSR.id+'-'+objDSR.ownerID;
            // if Tower Lead is already a team member
            if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.get(key)!=null)
             {
                mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c = true;
                lstDSRTeam.add(mapDSRUserIDToDSRTeam.get(key));
                mapDSRUserIDToDSRTeam.remove(key);
                system.debug('key'+mapDSRUserIDToDSRTeam);
                system.debug('mapDSRUserIDToDSRTeam'+mapDSRUserIDToDSRTeam);
                //system.debug('mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c::'+mapDSRUserIDToDSRTeam.get(key).GW1_Is_active__c);
             } 
             else
             {
                GW1_DSR_Team__c objDSRTeam = new GW1_DSR_Team__c();
                objDSRTeam.GW1_User__c =objDSR.ownerID;
                objDSRTeam.GW1_Is_active__c = true;
                objDSRTeam.GW1_DSR__c = objDSR.id;
                objDSRTeam.GW1_Role__c = 'RFx Tower Lead';
                lstDSRTeam.add(objDSRTeam);
             } */
             
            //
            /* if(mapDSRUserIDToDSRTeam!=null && mapDSRUserIDToDSRTeam.size()>0)
                {
                    for(GW1_DSR_Team__c objDSRTeam: mapDSRUserIDToDSRTeam.values() )
                    {
                         objDSRTeam.GW1_Is_active__c = false ;
                         listDSRTeamtoInActive.add(objDSRTeam);
                         system.debug('objDSRTeam::'+objDSRTeam);
                    }
                }
                        
                */      
            // Feeds For Tower Lead 
              /*  ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
                ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

                messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput> ();

                mentionSegmentInput.id = objDSR.ownerID;
                messageBodyInput.messageSegments.add(mentionSegmentInput);

                textSegmentInput.text = ' You have been Assigned RFx Tower Lead for ' + objDSR.Name +' .Kindly assign Deal Manager.';
                messageBodyInput.messageSegments.add(textSegmentInput);

                feedItemInput.body = messageBodyInput;
                feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
                feedItemInput.subjectId = objDSR.id;

                ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);
                batchInputsFeeds.add(batchInput);
                // objDSR.GW1_Is_Tower_Lead_Aligned__c=true;   */
                    oppcontroller.save();
                    
              /*  if (lstDSRTeam != null && lstDSRTeam.size() > 0)
                    {
                        upsert lstDSRTeam;
                    }
                    
                    if(batchInputsFeeds!= null && batchInputsFeeds.size()>0) // Inserting Feeds 
                    {
                        ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputsFeeds);
                    }
                    if (listDSRTeamtoInActive != null && listDSRTeamtoInActive.size() > 0)
                    {
                        update listDSRTeamtoInActive;
                    }
                    
                    // Adding Sharing rule t
                    if(lstDSRTeamPresent!=null && lstDSRTeamPresent.size()>0)
                    {   
                        GW1_DSRTeamTriggerHandler objHandler = new GW1_DSRTeamTriggerHandler();
                        objHandler.addSharingSetting(lstDSRTeamPresent);
                    } */
                   
            }
         return oppcontroller.Cancel();
    }

}