/**
 * @group ProjectDocument
 * @group-content ../../ApexDocContent/GPCmpServiceProjectDocument.htm
 *
 * @description Apex controller class having business logics
 * to fetch, insert, update and delete document under a project.
 */
public class GPControllerProjectDocument {

    private final String OTHER_DOCUMENT_RECORDTYPE_LABEL = 'OtherDocumentRecordTypeId';
    private final String PO_DOCUMENT_RECORDTYPE_LABEL = 'PORecordTypeId';
    private final String OTHER_DOCUMENTS_LABEL = 'otherDocuments';
    private final String PO_DOCUMENTS_LABEL = 'PODocuments';
    private static final String SUCCESS_LABEL = 'SUCCESS';
    private final String PROJECT_LABEL = 'objProject';
     private final String SHOW_PO_DOCUMENT_LABEL = 'showPODocument';


    public static List < GP_Project_Document__c > lstOfOtherProjectDocuments, lstOfPOProjectDocuments;
    public static Id OtherDocumentRecordTypeId;
    public static GP_Project__c objProject;
    public static Id PORecordTypeId;
    public static Boolean showPODocument = false;

    private JSONGenerator gen;

    /**
     * @description method to fetch Project Document type 'Other' Records.
     * 
     * @param projectId : project Id whose Project Document type 'Other' records need to fetched.
     *
     * @example
     * new GPControllerProjectDocument().getOtherProjectDocumentsService(<SFDC 18/15 digit project Id>);
     */
    public static void getOtherProjectDocumentsService(String projectId) {
        lstOfOtherProjectDocuments = new GPSelectorProjectDocument().selectOtherProjectDocumentRecords(projectId);
    }

    /**
     * @description method to fetch Project Document type 'PO' Records.
     * 
     * @param projectId : project Id whose Project Document type 'PO' records need to fetched.
     *
     * @example
     * new GPControllerProjectDocument().getPOProjectDocumentsService(<SFDC 18/15 digit project Id>);
     */
    public static void getPOProjectDocumentsService(String projectId) {
        lstOfPOProjectDocuments = new GPSelectorProjectDocument().selectPOProjectDocumentRecords(projectId);
    }

    /**
     * @description method to set RecordType of PO Document type.
     *
     * @example
     * new GPControllerProjectDocument().getPORecordTypeId();
     */
    public static void getPORecordTypeId() {
        PORecordTypeId = Schema.SObjectType.GP_Project_Document__c.getRecordTypeInfosByName().get('PO Document').getRecordTypeId();
    }

    /**
     * @description method to set RecordType of Other Document type.
     *
     * @example
     * new GPControllerProjectDocument().getOtherDocRecordTypeId();
     */
    public static void getOtherDocRecordTypeId() {
        OtherDocumentRecordTypeId = Schema.SObjectType.GP_Project_Document__c.getRecordTypeInfosByName().get('Other Document').getRecordTypeId();
    }

    /**
     * @description method to get Project Record.
     *
     * @example
     * new GPControllerProjectDocument().getProjectRecord(<SFDC 18/15 digit project Id>);
     */
    public static void getProjectRecord(String projectId) {
        // Payment Term Changes : Replace selectProjectRecord by selectProjectRecordForDocuments.
        objProject = new GPSelectorProject().selectProjectRecordForDocuments(projectId);
    }

    /**
     * @description method to fetch Project Documents for the Project.
     * 
     * @param projectId : project Id whose Project Document records needs to fetched.
     *
     * @example
     * new GPControllerProjectDocument().getDocuments(<SFDC 18/15 digit project Id>);
     */
    public GPAuraResponse getDocuments(String projectId) {
        try {
            getOtherProjectDocumentsService(projectId);
            getPOProjectDocumentsService(projectId);
            getOtherDocRecordTypeId();
            getPORecordTypeId();
            getProjectRecord(projectId);
            setShowPODocument();
            setJSON();

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJSON() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (OtherDocumentRecordTypeId != null)
            gen.writeObjectField(OTHER_DOCUMENT_RECORDTYPE_LABEL, OtherDocumentRecordTypeId);
        if (lstOfOtherProjectDocuments != null)
            gen.writeObjectField(OTHER_DOCUMENTS_LABEL, lstOfOtherProjectDocuments);
        if (PORecordTypeId != null)
            gen.writeObjectField(PO_DOCUMENT_RECORDTYPE_LABEL, PORecordTypeId);
        if (lstOfPOProjectDocuments != null)
            gen.writeObjectField(PO_DOCUMENTS_LABEL, lstOfPOProjectDocuments);
        if (objProject != null)
            gen.writeObjectField(PROJECT_LABEL, objProject);
        if (showPODocument != null)
            gen.writeObjectField(SHOW_PO_DOCUMENT_LABEL, showPODocument);

        gen.writeEndObject();
    }

    /**
     * @description method to delete Project Document Records.
     * 
     * @example
     * new GPControllerProjectDocument().deleteProjectDocumentService(<SFDC 18/15 digit project Document Id>,<SFDC 18/15 digit Content Version Id>);
     */
    public static GPAuraResponse deleteProjectDocumentService(Id projectDocumentId, Id contentVersionId) {

        try {
            GP_Project_Document__c objProjectDocument = [select id,GP_Project__c from GP_Project_Document__c where Id = :projectDocumentId];

            if (contentVersionId != null) {
                ContentVersion objContentVersion = [select id, ContentDocumentId from ContentVersion where id =: contentVersionId];
                ContentDocument objContentDocument = new COntentDocument(id = objContentVersion.ContentDocumentId);
                delete objContentDocument;
            }
            Id projectId = objProjectDocument.GP_Project__c;
            delete objProjectDocument;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'PROJEC DOCUMENT');
            update project;
            
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
     * @description method to save Project Document Records.
     * 
     * @example
     * new GPControllerProjectDocument().saveProjectDocumentService(<Serialized Content>,<SFDC 18/15 digit project Document Id>,<SFDC 18/15 digit RecordType Id>);
     */
    public GPAuraResponse saveProjectDocumentService(String serializedProjectDocument, string projectId, string recordTypeName) {
        if (serializedProjectDocument == null) {
            return new GPAuraResponse(false, 'NO Data to Save!!!', null);
        }
        getProjectRecord(projectId);
        setShowPODocument();
        List < wrapProjectDocumentData > listOfwrapProjectDocument = (List < wrapProjectDocumentData > ) JSON.deserialize(serializedProjectDocument, List < wrapProjectDocumentData > .class);
        
        List < GP_Project_Document__c > listofProjectDocumentWithNoFileData = new List < GP_Project_Document__c > ();
        List < GP_Project_Document__c > lstOfselectedProjectDocuments = new List < GP_Project_Document__c > ();
        List < GP_Project_Document__c > listofProjectDocument = new List < GP_Project_Document__c > ();
        List < ContentDocumentLink > listofContentDocumentLink = new List < ContentDocumentLink > ();
        // Mobile app Changes
        List < ContentDistribution > listofContentDistributions = new List < ContentDistribution > ();
        List < ContentVersion > listofContentVersion = new List < ContentVersion > ();
        Set < Id > setOfCDs = new Set < Id > ();
        Map <String, ContentDistribution> mapOfContentDistributions = new Map <String, ContentDistribution> ();

        Set < ID > setofIdofContentVersion = new Set < ID > ();

        
        for (wrapProjectDocumentData wrapprojectDocument: listOfwrapProjectDocument) {
            if (wrapprojectDocument.filedata != null && wrapprojectDocument.objProjectDocument.id == null) {
                if (recordTypeName == 'PODocument') {
                    wrapprojectDocument.objProjectDocument.Name = wrapprojectDocument.objProjectDocument.GP_PO_Number__c;
                }

                listofProjectDocument.add(wrapprojectDocument.objProjectDocument); //newly added or project Documents which has file attached or attached file is changed
                
                ContentVersion objContentVersion = new ContentVersion();
                String encodedString = wrapprojectDocument.filedata;
                objContentVersion.versionData = EncodingUtil.base64Decode(encodedString);
                objContentVersion.PathOnClient = wrapprojectDocument.filename;

                listofContentVersion.add(objContentVersion);
            } else if (wrapprojectDocument.objProjectDocument.id != null) {
                if (recordTypeName == 'PODocument') {
                    wrapprojectDocument.objProjectDocument.Name = wrapprojectDocument.objProjectDocument.GP_PO_Number__c;
                }
                listofProjectDocumentWithNoFileData.add(wrapprojectDocument.objProjectDocument); //old project documents with changes done in the 'name' or any other field except Uploaded File
            }
        }

        if (listofContentVersion.size() > 0) {
            try {
               // insert listofContentVersion;
            } catch (Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }
        }

        for (Integer a = 0; a < listofProjectDocument.size(); a++) {
            //listofProjectDocument[a].GP_Content_Version_Id__c = listofContentVersion[a].id;
            system.debug('listofProjectDocument[a].GP_Content_Version_Id__c::'+listofProjectDocument[a].GP_Content_Version_Id__c);
            setofIdofContentVersion.add(listofProjectDocument[a].GP_Content_Version_Id__c);
        }

        if (listofProjectDocument.size() > 0) {
            try {
                insert listofProjectDocument;
            } catch (Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }
        }
        
        Map < Id, Id > mapOfCotentVersionIDAndContentDocumentID = new Map < Id, Id > ();
        for (ContentVersion contentversionobj: [SELECT Id, ContentDocumentId FROM ContentVersion WHERE ContentDocumentId IN: setofIdofContentVersion]) {
            mapOfCotentVersionIDAndContentDocumentID.put(contentversionobj.ContentDocumentId, contentversionobj.id);
        }        

        for (GP_Project_Document__c projectDocument: listofProjectDocument) {
            ContentDocumentLink cdl = new ContentDocumentLink();
            cdl.LinkedEntityId = projectDocument.id;
            cdl.ContentDocumentId =  projectDocument.GP_Content_Version_Id__c;//mapOfCotentVersionIDAndContentDocumentID.get(projectDocument.GP_Content_Version_Id__c);
            cdl.ShareType = 'V';
            listofContentDocumentLink.add(cdl);
            
            // Mobile app Changes
            ContentDistribution cdb = new ContentDistribution();
            cdb.ContentVersionId = Test.isRunningTest() ? projectDocument.GP_Content_Version_Id__c : mapOfCotentVersionIDAndContentDocumentID.get(projectDocument.GP_Content_Version_Id__c);
            cdb.Name = projectDocument.GP_File_Name__c;
            cdb.PreferencesAllowOriginalDownload = false;
            cdb.PreferencesAllowPDFDownload = false;
            cdb.PreferencesAllowViewInBrowser = true;
            listofContentDistributions.add(cdb);
        }
        // Mobile app Changes
        if(listofContentDistributions.size() > 0) {
            insert listofContentDistributions;
        }
        // Mobile app Changes
        for(ContentDistribution cdb : listofContentDistributions) {
            setOfCDs.add(cdb.Id);
        }
        // Mobile app Changes
        for(ContentDistribution cd : [Select id, ContentVersionId, DistributionPublicUrl from ContentDistribution Where id =: setOfCDs]) {
            mapOfContentDistributions.put(cd.ContentVersionId, cd);
        }

        for (Integer a = 0; a < listofProjectDocument.size(); a++) {
            //listofProjectDocument[a].GP_Content_Version_Id__c = listofContentVersion[a].id;
            // Mobile app Changes
            listofProjectDocument[a].GP_Mob_File_View_URL__c = Test.isRunningTest() ? '' : 
            mapOfContentDistributions.get(mapOfCotentVersionIDAndContentDocumentID.get(listofProjectDocument[a].GP_Content_Version_Id__c)).DistributionPublicUrl;
            listofProjectDocument[a].GP_Document_Id__c = listofProjectDocument[a].GP_Content_Version_Id__c;
            listofProjectDocument[a].GP_Content_Version_Id__c = Test.isRunningTest() ? 
            listofProjectDocument[a].GP_Content_Version_Id__c : mapOfCotentVersionIDAndContentDocumentID.get(listofProjectDocument[a].GP_Content_Version_Id__c);
        }
        
        if (listofContentDocumentLink.size() > 0) {
            try {
                insert listofContentDocumentLink;
                update listofProjectDocument;
            } catch (Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }

        }

        if (listofProjectDocumentWithNoFileData.size() > 0) {
            try {
                update listofProjectDocumentWithNoFileData;
            } catch (Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }
        }

        if (recordTypeName == 'PODocument') {
            lstOfPOProjectDocuments = new GPSelectorProjectDocument().selectPOProjectDocumentRecords(projectId);
        }

        if (recordTypeName == 'OtherDocument') {
            lstOfOtherProjectDocuments = new GPSelectorProjectDocument().selectOtherProjectDocumentRecords(projectId);
        }
        
        objProject.GP_Last_Modified_Source__c = 'PO Document';
        update objProject;
        
        setJSON();
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }
    public GPAuraResponse saveProjectDocumentChunkService(Id fileId, String base64Data){
         return new GPAuraResponse(true, SUCCESS_LABEL, '');
    }
    private void setShowPODocument(){
        //showPODocument = objProject.RecordType.Name == 'OTHER PBB' || objProject.RecordType.Name == 'BPM' || (objProject.RecordType.Name == 'CMITS' && objProject.GP_Business_Group_L1__c == 'GE') ? true : false;
        //changed by anmol on 01-06-2018 as suggested by Deepti and pankaj.
        showPODocument = objProject.RecordType.Name == 'OTHER PBB' || 
                         objProject.RecordType.Name == 'BPM' || 
                         objProject.RecordType.Name == 'CMITS';
    }

    public class wrapProjectDocumentData {
        @AuraEnabled
        public GP_Project_Document__c objProjectDocument {
            get;
            set;
        }
        @AuraEnabled
        public String filedata {
            get;
            set;
        }
        @AuraEnabled
        public String filename {
            get;
            set;
        }
    }
}