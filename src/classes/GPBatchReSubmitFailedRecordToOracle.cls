global class GPBatchReSubmitFailedRecordToOracle implements Database.Batchable<sObject> {
    // Query all the records with oracle status equal to Failure or ''
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return database.getQueryLocator([select id, GP_Oracle_Re_Submission_Counter__c,GP_Approval_Status__C 
                                         from GP_Project__c 
                                         where GP_Approval_Status__C in ('Approved', 'Closed')
                                         And GP_Oracle_Status__c in ('Failure', '')]);
    }
    
    // increment the Oracle Re submission counter by 1 to resubmit the project to oracle. 
    // The project is submitted to oracle using an outbound message which tracks change in oracle re submission counter.
    global void execute(Database.BatchableContext BC, List<GP_Project__c> listOfFailedProjects) {
        for(GP_Project__c failedProjects : listOfFailedProjects) {
            Decimal resubmissionCounter = failedProjects.GP_Oracle_Re_Submission_Counter__c != null ? failedProjects.GP_Oracle_Re_Submission_Counter__c : 0;
            failedProjects.GP_Oracle_Re_Submission_Counter__c = resubmissionCounter + 1;
        }
        
        update listOfFailedProjects;
    }
    
    global void finish(Database.BatchableContext BC) {}
}