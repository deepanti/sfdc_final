@isTest
public class GPSelectorOpportunityProjectTracker { 
    @isTest
    static void testSelectorOpportunityProject() {
        GPSelectorOpportunityProject opportunityProjectSelector = new GPSelectorOpportunityProject();
        opportunityProjectSelector.selectById(new Set<Id>());
        opportunityProjectSelector.selectOppProject(null);
        opportunityProjectSelector.selectOpportunityProjectShareById(null);
        opportunityProjectSelector.selectOppProjectNoClosed(null);
        opportunityProjectSelector.selectOppproductForClassification(new List<Id>());
    }
}