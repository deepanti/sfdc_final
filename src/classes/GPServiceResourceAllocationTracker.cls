//================================================================================================================
//  Description: Test Class for GPServiceResourceAllocation
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          11-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPServiceResourceAllocationTracker {
    public Static GP_Project_Work_Location_SDO__c objPrjSdo ;
    public Static GP_Project__c parentProject;
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    public static GP_Project__c prjObj;
    Public Static set<ID> setOfID = new set<ID>();
    Public Static Set<String> setOfMonthAndY = new Set<String>();
    Public Static GP_Resource_Allocation__c objResourceAllocation1;
    Public Static GP_Resource_Allocation__c objResourceAllocation2;
    Public Static GP_Employee_Master__c empObj;
    Public Static GP_Work_Location__c objSdo,objSdo1;
    Public Static List<GP_Resource_Allocation__c> listOfResourceAll = new List<GP_Resource_Allocation__c>();
    
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        objpinnacleMaster.GP_Status__c = 'Active';
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        objpinnacleMaster.GP_Oracle_Id__c = 'test';
        insert objpinnacleMaster;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        empObj = GPCommonTracker.getEmployee();
        empObj.GP_EMPLOYEE_TYPE__c = 'Employee';
        empObj.GP_DESIGNATION__c = 'test';
        empObj.GP_EMPOYMENT_CATEGORY__c = 'test';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_Final_OHR__c = '12345';
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_HIRE_Date__c = system.today();
        empObj.GP_Legal_Entity_Code__c = '4141';
        empObj.GP_isActive__c = true;
        empObj.GP_Resource_Id__c ='23';
        insert empObj;
        setOfID.add(empObj.id);
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        timesheettrnsctnObj.GP_Month_Year__c = 'Jan-18';
        insert timesheettrnsctnObj;
        
        
        
        GP_Employee_HR_History__c empHR = new GP_Employee_HR_History__c();
        empHR.GP_Employee_Person_Id__c = '1234567';
        empHR.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empHR.GP_ASSIGNMENT_STATUS__c = 'Active Assignment';
        empHR.GP_Employees__c = empObj.id;
        insert empHR;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        
        objSdo1 = GPCommonTracker.getWorkLocation();
        objSdo1.GP_Status__c = 'Active and Visible';
        objSdo1.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objSdo1;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Group_L1__c = 'GE';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('CMITS').getRecordTypeId();
        prjObj.GP_Parent_Project__c = parentProject.id;
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Approval_Status__c = 'Draft';
        insert prjObj ;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO1;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        timeshtentryObj.GP_Date__c = system.today();
        insert timeshtentryObj ;
        
        /*ProcessInstance objProcessInstance = new ProcessInstance();
objProcessInstance.TargetObjectId = prjObj.id;
//objProcessInstance.ProcessDefinitionId  = '04a90000000H7IUAA0';
objProcessInstance.Status = 'Pending';
insert objProcessInstance;

ProcessInstanceWorkitem objProcessInstanceWorkItem = new ProcessInstanceWorkitem();
objProcessInstanceWorkItem.ProcessInstanceId =objProcessInstance.id;
insert objProcessInstanceWorkItem;*/
        
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id,objSdo.Id);
        insert objPrjBillingMilestone;
        
        objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Employee__c = empObj.id;
        objResourceAllocation1.GP_Work_Location__c = objSdo.id;
        objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
        listOfResourceAll.add(objResourceAllocation1);
        
        GP_Employee_Training__c objEmployeeTraining = new GP_Employee_Training__c();
        objEmployeeTraining.GP_Course_Id__c = 'test'; 
        objEmployeeTraining.GP_Employee_Number__c = '12345';
        objEmployeeTraining.GP_Status_Change_Date__c = system.today().adddays(10);
        insert objEmployeeTraining;
        
        GP_BGC_Detail__c objBGC = new GP_BGC_Detail__c();
        objBGC.GP_Employee_OHR__c = '12345';
        objBGC.GP_Overall_Status__c = 'black';
        insert objBGC;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
         objpinnacleMasterGlobalSetting.GP_LE_Codes_for_BGC_Check__c = '4141';
        insert objpinnacleMasterGlobalSetting ;
        
        GP_Project_Work_Location_SDO__c projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Project__c = prjObj.Id;
        projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Primary__c = true;
        projectWorkLocation.GP_Project__c = prjObj.Id;
        projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        insert projectWorkLocation;
    }
    
    public static void buildDependencyData2() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        objpinnacleMaster.GP_Status__c = 'Active';
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        insert objpinnacleMaster;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        empObj = GPCommonTracker.getEmployee();
        empObj.GP_EMPLOYEE_TYPE__c = 'Employee';
        empObj.GP_DESIGNATION__c = 'test';
        empObj.GP_EMPOYMENT_CATEGORY__c = 'test';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_Final_OHR__c = '12345';
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = true;
        empObj.GP_Resource_Id__c = 'sd';
        insert empObj;
        setOfID.add(empObj.id);
        
        GP_Employee_HR_History__c empHR = new GP_Employee_HR_History__c();
        empHR.GP_Employee_Person_Id__c = '1234567';
        empHR.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empHR.GP_Employees__c = empObj.id;
        empHR.GP_ASGN_EFFECTIVE_START__c = date.newInstance(2018, 02, 02);
        empHR.GP_ASSIGNMENT_STATUS__c = 'Active Assignment';
        insert empHR;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.GP_Business_Name__c='CMITS';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Lending_SDO_Code__c = '1010';
        insert objSdo;
        
        empObj.GP_SDO__c = objSdo.Id;
        update empObj;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Group_L1__c = 'GE';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('BPM').getRecordTypeId();
        prjObj.GP_Parent_Project__c = parentProject.id;
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Approval_Status__c = 'Draft';
        insert prjObj ;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO1;
        
        /*ProcessInstance objProcessInstance = new ProcessInstance();
objProcessInstance.TargetObjectId = prjObj.id;
//objProcessInstance.ProcessDefinitionId  = '04a90000000H7IUAA0';
objProcessInstance.Status = 'Pending';
insert objProcessInstance;

ProcessInstanceWorkitem objProcessInstanceWorkItem = new ProcessInstanceWorkitem();
objProcessInstanceWorkItem.ProcessInstanceId =objProcessInstance.id;
insert objProcessInstanceWorkItem;*/
        
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id,objSdo.Id);
        insert objPrjBillingMilestone;
        
        objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Employee__c = empObj.id;
        objResourceAllocation1.GP_Work_Location__c = objSdo.id;
        objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
        listOfResourceAll.add(objResourceAllocation1);
        
        GP_Employee_Training__c objEmployeeTraining = new GP_Employee_Training__c();
        objEmployeeTraining.GP_Course_Id__c = 'test'; 
        objEmployeeTraining.GP_Employee_Number__c = '12345';
        objEmployeeTraining.GP_Status_Change_Date__c = system.today().adddays(10);
        insert objEmployeeTraining;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
    }
    
    public static void buildDependencyData3() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        objpinnacleMaster.GP_Status__c = 'Active';
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        objpinnacleMaster.GP_Oracle_Id__c = 'test';
        insert objpinnacleMaster;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_EMPLOYEE_TYPE__c = 'Employee';
        empObj.GP_DESIGNATION__c = 'test';
        empObj.GP_EMPOYMENT_CATEGORY__c = 'test';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_Final_OHR__c = '12345';
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_HIRE_Date__c = system.today();
        empObj.GP_isActive__c = true;
         empObj.GP_Resource_Id__c = 's';
        
       
        insert empObj;
        setOfID.add(empObj.id);
        
        GP_Employee_HR_History__c empHR = new GP_Employee_HR_History__c();
        empHR.GP_Employee_Person_Id__c = '1234567';
        empHR.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empHR.GP_Employees__c = empObj.id;
        empHR.GP_ASSIGNMENT_STATUS__c = 'Suspend Assignment';
        insert empHR;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Business_Name__c  = 'CMITS';
        insert objSdo;
        
        empObj.GP_SDO__c = objSdo.id;
        update empObj;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Group_L1__c = 'GE';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('CMITS').getRecordTypeId();
        prjObj.GP_Parent_Project__c = parentProject.id;
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Approval_Status__c = 'Draft';
        insert prjObj ;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO1;
        
        /*ProcessInstance objProcessInstance = new ProcessInstance();
objProcessInstance.TargetObjectId = prjObj.id;
//objProcessInstance.ProcessDefinitionId  = '04a90000000H7IUAA0';
objProcessInstance.Status = 'Pending';
insert objProcessInstance;

ProcessInstanceWorkitem objProcessInstanceWorkItem = new ProcessInstanceWorkitem();
objProcessInstanceWorkItem.ProcessInstanceId =objProcessInstance.id;
insert objProcessInstanceWorkItem;*/
        
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id,objSdo.Id);
        insert objPrjBillingMilestone;
        
        objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Employee__c = empObj.id;
        objResourceAllocation1.GP_Work_Location__c = objSdo.id;
        //objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        //objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_End_Date__c = system.today().adddays(10);
        objResourceAllocation1.GP_Start_Date__c = system.today();
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
        listOfResourceAll.add(objResourceAllocation1);
        
        objResourceAllocation2 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation2.GP_Bill_Rate__c = 0;
        objResourceAllocation2.GP_Employee__c = empObj.id;
        objResourceAllocation2.GP_Work_Location__c = objSdo.id;
        //objResourceAllocation2.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        //objResourceAllocation2.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation2.GP_End_Date__c = system.today().adddays(10);
        objResourceAllocation2.GP_Start_Date__c = system.today();
        objResourceAllocation2.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation2;
        listOfResourceAll.add(objResourceAllocation2);
        
        GP_Employee_Training__c objEmployeeTraining = new GP_Employee_Training__c();
        objEmployeeTraining.GP_Course_Id__c = 'test'; 
        objEmployeeTraining.GP_Employee_Number__c = '12345';
        objEmployeeTraining.GP_Status_Change_Date__c = system.today().adddays(10);
        insert objEmployeeTraining;
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation()
    {
        buildDependencyData();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_Nature_of_Work__c = 'Consulting';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        //-----
        GPServiceResourceAllocation.validateValidEmployeeContract();
        //------
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        //---
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        //-----
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //----
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        //------
        //GPServiceResourceAllocation.validateEachResourceAllocation();
        //------
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation7()
    {
        buildDependencyData();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        //prjObj.GP_Nature_of_Work__c = 'Consulting - Digital';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        //-----
        GPServiceResourceAllocation.validateValidEmployeeContract();
        //------
        GPServiceResourceAllocation.getResourceLendingSDO(empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        //---
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        //-----
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //----
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        //------
        //GPServiceResourceAllocation.validateEachResourceAllocation();
        //------
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        listOfResourceAll[0].GP_Employee__r = new GP_Employee_Master__c();
        listOfResourceAll[0].GP_Employee__r.GP_Final_OHR__c = '12345';
        listOfResourceAll[0].GP_Employee__r.GP_EMPLOYEE_TYPE__c = 'Employee';
        listOfResourceAll[0].GP_Employee__r.GP_isActive__c = true;
        listOfResourceAll[0].GP_OHR__c = '12345';
        GPServiceResourceAllocation.isFormattedLogRequired = false;
        GPServiceResourceAllocation.isLogForTemporaryId = false;
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        GPServiceResourceAllocation.isLogForTemporaryId = true;
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation6()
    {
        buildDependencyData3();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        //prjObj.GP_Nature_of_Work__c = 'Consulting - Digital';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        //-----
        GPServiceResourceAllocation.validateValidEmployeeContract();
        //------
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        //---
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        //-----
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //----
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        //------
        //GPServiceResourceAllocation.validateEachResourceAllocation();
        //------
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation8()
    {
        buildDependencyData3();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_Nature_of_Work__c = 'Consulting';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'BPM';
        prjObj.GP_Vertical__c = 'Manufacturing';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        //-----
        GPServiceResourceAllocation.validateValidEmployeeContract();
        //------
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        //---
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        //-----
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //----
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        //------
        //GPServiceResourceAllocation.validateEachResourceAllocation();
        //------
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation2()
    {
        buildDependencyData2();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_Nature_of_Work__c = 'Consulting';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateValidEmployeeContract();
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation4()
    {
        buildDependencyData2();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        prjObj.GP_Oracle_PID__c = 'test';
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_Nature_of_Work__c = 'Healthcare';
        prjObj.GP_Vertical__c= 'Manufacturing'; 
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.validateValidEmployeeContract();
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
      
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        prjObj.GP_Operating_Unit__r = new GP_Pinnacle_Master__c(); 
        prjObj.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today();
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        prjObj.GP_End_Date__c = system.today().adddays(10);
        listOfResourceAll[0].GP_End_Date__c = system.today().adddays(20);
        listOfResourceAll[0].GP_Percentage_allocation__c = 101;
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        listOfResourceAll[0].GP_Work_Location__r = new  GP_Work_Location__c();
        listOfResourceAll[0].GP_Work_Location__r.GP_Legal_Entity_Code__c = '12345';
        listOfResourceAll[0].GP_Employee__r = new GP_Employee_Master__c();
        listOfResourceAll[0].GP_Employee__r.GP_Legal_Entity_Code__c = '123456';
        listOfResourceAll[0].GP_Employee__r.GP_EMPLOYEE_TYPE__c = 'Employee';
        listOfResourceAll[0].GP_Employee__r.GP_isActive__c = true;
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
    }
    
    @isTest 
    public static void testGPServiceResourceAllocation5()
    {
        buildDependencyData2();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.isFormattedLogRequired = false;
        GPServiceResourceAllocation.isLogForTemporaryId = true;
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.isLogForTemporaryId = false;
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        listOfResourceAll[0].GP_Work_Location__c = null;
        listOfResourceAll[0].GP_Employee__r = new GP_Employee_Master__c();
        listOfResourceAll[0].GP_Employee__r.GP_DESIGNATION__c = null;
        listOfResourceAll[0].GP_Employee__r.GP_EMPLOYEE_TYPE__c = 'Employee';
        listOfResourceAll[0].GP_Employee__r.GP_isActive__c = true;
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_Nature_of_Work__c = 'Consulting';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.validateValidEmployeeContract();
        
        listOfResourceAll[0].GP_SDO__c = objSdo.id;
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        prjObj.GP_Operating_Unit__r = new GP_Pinnacle_Master__c(); 
        prjObj.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today();
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        prjObj.GP_End_Date__c = system.today().adddays(10);
        listOfResourceAll[0].GP_End_Date__c = system.today().adddays(20);
        listOfResourceAll[0].GP_Percentage_allocation__c = 101;
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        listOfResourceAll[0].GP_Work_Location__r = new  GP_Work_Location__c();
        listOfResourceAll[0].GP_Work_Location__r.GP_Legal_Entity_Code__c = '12345';
        listOfResourceAll[0].GP_Employee__r = new GP_Employee_Master__c();
        listOfResourceAll[0].GP_Employee__r.GP_Legal_Entity_Code__c = '123456';
        listOfResourceAll[0].GP_Employee__r.GP_EMPLOYEE_TYPE__c = 'Employee';
        listOfResourceAll[0].GP_Employee__r.GP_isActive__c = true;
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
    }
    
    @isTest  
    public static void testGPServiceResourceAllocation3()
    {
        buildDependencyData();
        GPServiceResourceAllocation.setEmployeeIdSet(setOfID);
        //GPServiceResourceAllocation.setAssociatedProject();
        //GPServiceResourceAllocation.setAssociatedProject(prjObj);
        GPServiceResourceAllocation.validateResourceAllocation(listOfResourceAll,prjObj);
        GPServiceResourceAllocation.validateResourceSDO(prjObj.id);
        setOfMonthAndY.add('Jan-18');
        GPServiceResourceAllocation.validateResourceAllocationWithTimeSheetEntries(setOfID,setOfMonthAndY,listOfResourceAll);
        //GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType = new RecordType();
        prjObj.GP_Deal__r = new GP_Deal__c();
        prjObj.GP_Deal__r.GP_Business_Group_L1__c = 'GE';
        //prjObj.GP_Nature_of_Work__c = 'Consulting - Digital';
        prjObj.GP_Vertical__c = 'Capital Markets';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        prjObj.RecordType.Name = 'CMITS';
        GPServiceResourceAllocation.validateEmployeeAgainstILearn(prjObj,listOfResourceAll);
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        //-----
        GPServiceResourceAllocation.validateValidEmployeeContract();
        //------
        GPServiceResourceAllocation.getResourceLendingSDO( empObj,prjObj);
        GPServiceResourceAllocation.validateEmployeeLendingSDO(prjObj,listOfResourceAll);
        //---
        GPServiceResourceAllocation.validateIsResourceAllocationEditable(prjObj);
        //-----
        GPServiceResourceAllocation.validateIsResourceAllocationDefinable(prjObj);
        //----
        //GPServiceResourceAllocation.customValidationOnResourceAllocation(prjObj,listOfResourceAll);
        //------
        //GPServiceResourceAllocation.validateEachResourceAllocation();
        //------
        GPServiceResourceAllocation.validateBGC(objResourceAllocation1);
        GPServiceResourceAllocation.validateBGC(listOfResourceAll);
        GPServiceResourceAllocation.validateEachResourceAllocation(objResourceAllocation1,new GPResponseWrapper(),prjObj.id);
        GPServiceResourceAllocation.isOverlappingWithSameProject('test',new set<String>{'test'});
    }
    
    public static void fetchData() { 
        objPrjBillingMilestone = [select id,Name from GP_Billing_Milestone__c limit 1]; 
        prjObj = [select id,Name from GP_Project__c limit 1]; 
        jsonresp= (String)JSON.serialize(new List<GP_Billing_Milestone__c>{objPrjBillingMilestone}); 
    }
}