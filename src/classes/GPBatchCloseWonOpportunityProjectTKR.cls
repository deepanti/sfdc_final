//================================================================================================================
//  Description: Test Class for GPBatchCloseWonOpportunityProject
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
private class GPBatchCloseWonOpportunityProjectTKR {
    Public Static List<GP_Project_Address__c> listOfAddress = new List<GP_Project_Address__c>();
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    private Static User objuser;
    
    static void setupCommonData() {
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;

        objuser = GPCommonTracker.getUser(); 
        Profile p = [select id, name from profile where name = 'Genpact Super Admin'
                     limit 1
                    ];
        objuser.ProfileID = p.id;        
        insert objuser;

        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
      system.runAS(objuser)
        {  
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        oppobj.Number_of_Contract__c = 1;
        oppobj.Bypass_Validations__c = true;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-Integer.valueOf(system.label.GP_Close_Opp_Project_After_Won_Day));
        insert oppobj;
        oppobj.StageName = '6. Signed Deal';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-Integer.valueOf(system.label.GP_Close_Opp_Project_After_Won_Day));
        update oppobj;
      system.debug( oppobj.Actual_Close_Date__c );
            
            Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        insert oppproobj;
        }
    }
    
    @isTest
    public static void testGPBatchCloseWonOpportunityProject() {
        
            setupCommonData();
            GPBatchCloseWonOpportunityProject batcher = new GPBatchCloseWonOpportunityProject();
            Id batchprocessid = Database.executeBatch(batcher);
        
    }
    
    
}