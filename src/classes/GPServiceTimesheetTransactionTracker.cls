@isTest
public class GPServiceTimesheetTransactionTracker {
    public static GP_Timesheet_Entry__c timeshtEntryObj,timeshtEntryObj1;
    public static GP_Employee_Master__c empObj,empObj1;
    public static GP_Temporary_Data__c objTemporaryTimeSheet; 
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    
    public static void setupCommonData(){
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_Final_OHR__c = '123456778';
        empObj.GP_Person_Id__c = '123456778';
        insert empObj;
        
        empObj1 = GPCommonTracker.getEmployee();
        empObj1.GP_SFDC_User__c = objuser.id;
        empObj1.GP_Final_OHR__c = '123456788';
        empObj1.GP_Person_Id__c = '123456788';
        insert empObj1;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        objpinnacleMaster = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        DateTime entryDateMaster = system.today();
        String monthYearMaster = entryDateMaster.format('MMM') + '-' +  String.ValueOf(entryDateMaster.year()).substring(2, 4);
        objpinnacleMaster.GP_Latest_Open_PA_Period__c = monthYearMaster;
        objpinnacleMaster.GP_TimeSheet_Financial_Month__c = system.today().addDays(10);
        objpinnacleMaster.GP_Full_Time_Emp_Min_Hrs__c = 8;
        objpinnacleMaster.GP_Full_Time_Emp_Max_Hrs__c = 24;
        objpinnacleMaster.GP_Contractor_Emp_Min_Hrs__c = 5;
        objpinnacleMaster.GP_Contractor_Emp_Max_Hrs__c = 8;
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.Name = 'TimeSheet Administrator';
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = new GP_User_Role__c();
        objuserrole.GP_Active__c = True;
        objuserrole.GP_Role__c = objrole.Id;
        objuserrole.GP_User__c = UserInfo.getUserId();
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        insert prjObj;
        
        GP_Timesheet_Transaction__c  objTimeSheetTransaction = GPCommonTracker.getTimesheetTransaction(empObj);
        objTimeSheetTransaction.GP_Approval_Status__c = 'Approved';
        objTimeSheetTransaction.GP_Oracle_Status__c ='S';
        insert objTimeSheetTransaction;
        
        timeshtEntryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,objTimeSheetTransaction);
        timeshtEntryObj.GP_Date__c = system.today();
        timeshtEntryObj.GP_Project_Oracle_PID__c = '4532';
        timeshtEntryObj.GP_Project_Task_Oracle_Id__c = 'ORACLE-001';
        timeshtEntryObj.GP_Ex_Type__c = 'Billable'; 
        timeshtEntryObj.GP_Modified_Hours__c = 8;
        insert timeshtEntryObj ;
        
        timeshtEntryObj1 = GPCommonTracker.getTimesheetEntry(empObj1,prjObj,objTimeSheetTransaction);
        timeshtEntryObj1.GP_Date__c = system.today();
        timeshtEntryObj1.GP_Project_Oracle_PID__c = '4532';
        timeshtEntryObj1.GP_Project_Task_Oracle_Id__c = 'ORACLE-001';
        timeshtEntryObj1.GP_Ex_Type__c = 'Billable'; 
        timeshtEntryObj1.GP_Modified_Hours__c = 8;
        insert timeshtEntryObj1 ;
        
         GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        projectTaskObj1.GP_Active__c = true;
        projectTaskObj1.GP_Task_Number__c = 'ORACLE-001';
        projectTaskObj1.GP_Project_Number__c  = '4532';
        projectTaskObj1.GP_Start_Date__c = system.today();
        insert projectTaskObj1;
        
        GP_Job__c objJobForTimeSheetUpload = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJobForTimeSheetUpload;
        
        GP_Timesheet__c timesheetRecord = new GP_Timesheet__c();
        timesheetRecord.GP_Person_Id__c = '123456778';
        timesheetRecord.GP_Employee_Number__c = '123456778';
        timesheetRecord.GP_Project_Number__c = '4532';
        timesheetRecord.GP_Task_Number__c = 'ORACLE-001';
        DateTime entryDate = system.today();
        String monthYear = entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        timesheetRecord.GP_Month_Year__c = monthYear;
        timesheetRecord.GP_Expenditure_Item_Date__c = system.today();
        timesheetRecord.GP_Expenditure_Type__c = 'Billable';
        timesheetRecord.GP_Hours__c = 7;
        timesheetRecord.GP_PersonId_MonthYear__c = timesheetRecord.GP_Person_Id__c + monthYear;
        insert timesheetRecord;
        
         GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id, prjObj.id);
        objResourceAllocation.GP_Project__c = prjObj.id;
        objResourceAllocation.GP_Bill_Rate__c = 0;
        objResourceAllocation.GP_Employee__c = empObj.id;
        objResourceAllocation.GP_Work_Location__c = objSdo.id;
        objResourceAllocation.GP_Start_Date__c = system.today();
        objResourceAllocation.GP_End_Date__c = system.today().addMonths(1);
        objResourceAllocation.GP_Percentage_allocation__c = 1;
        objResourceAllocation.GP_SDO__c = objSdo.Id;
        insert objResourceAllocation;
        
        objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJobForTimeSheetUpload.Id,
            '123456778',
            '4532', 
            'ORACLE-001');
        objTemporaryTimeSheet.GP_TSC_Date__c  = system.today();
        insert objTemporaryTimeSheet;
        
        GP_Temporary_Data__c objTemporaryTimeSheet1 = GPCommonTracker.getTemporaryDataForTimeSheet(objJobForTimeSheetUpload.Id,
            '123456788',
            '4532', 
            'ORACLE-001');
        objTemporaryTimeSheet1.GP_TSC_Date__c  = system.today();
        insert objTemporaryTimeSheet1;
    }
    
    @isTest
    public static void testValidateLoggedinUser() {
        setupCommonData();
        GPServiceTimesheetTransaction.validateLoggedinUser(UserInfo.getUserId());
    } 
    
    @isTest
    public static void testgetValidatedTimeSheetEntriesLst1() {
        setupCommonData();
        timeshtEntryObj  = [select id, GP_Project__c,GP_Project__r.Name, GP_Employee__r.GP_EMPLOYEE_TYPE__c,GP_Employee__r.GP_Final_OHR__c,GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c where id =:timeshtEntryObj.Id ];
        List<GP_Timesheet_Entry__c> listOfTimeshtEntry = new List<GP_Timesheet_Entry__c>();
        listOfTimeshtEntry.add(timeshtEntryObj);
        DateTime entryDate = system.today();
        String tempDataKey = empObj.GP_Person_Id__c + '@@' + '4532' + '@@';
        tempDataKey += String.ValueOf(system.today()).split(' ')[0] + '@@' + 'Billable';
        
        String employeemonthYear = empObj.Id + entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        GPServiceTimesheetTransaction.getValidatedTimeSheetEntriesLst(listOfTimeshtEntry,new Map<String,GP_Temporary_Data__c>{tempDataKey => objTemporaryTimeSheet },
            new Set<Id>(),
            new Set<Id>{empObj.Id},
            new Set<String>{'4532'},
            new Set<String>{employeemonthYear});
    } 
    @isTest
    public static void testgetValidatedTimeSheetEntriesLst() {
        setupCommonData();
       List<GP_Timesheet_Entry__c> listOfTimeshtEntry  = [select id, GP_Project__c,GP_Project__r.Name, GP_Employee__r.GP_EMPLOYEE_TYPE__c,GP_Employee__r.GP_Final_OHR__c,GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c where GP_Project_Oracle_PID__c ='4532' ];
        DateTime entryDate = system.today();
        String tempDataKey = empObj.GP_Person_Id__c + '@@' + '4532' + '@@';
        tempDataKey += String.ValueOf(system.today()).split(' ')[0] + '@@' + 'Billable';
        
        String employeemonthYear = empObj1.Id + entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        GPServiceTimesheetTransaction.getValidatedTimeSheetEntriesLst(listOfTimeshtEntry,new Map<String,GP_Temporary_Data__c>{tempDataKey => objTemporaryTimeSheet },
            new Set<Id>(),
            new Set<Id>{empObj1.Id},
            new Set<String>{'4532'},
            new Set<String>{employeemonthYear});
    }
    @isTest
    public static void testgetValidatedTimeSheetEntriesLst3() {
         setupCommonData();
        timeshtEntryObj  = [select id, GP_Project__c,GP_Project__r.Name, GP_Employee__r.GP_EMPLOYEE_TYPE__c,GP_Employee__r.GP_Final_OHR__c,GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c where id =:timeshtEntryObj.Id ];
        List<GP_Timesheet_Entry__c> listOfTimeshtEntry = new List<GP_Timesheet_Entry__c>();
        listOfTimeshtEntry.add(timeshtEntryObj);
        DateTime entryDate = system.today();
        String tempDataKey = empObj.GP_Person_Id__c + '@@' + '4532' + '@@';
        tempDataKey += String.ValueOf(system.today()).split(' ')[0]+ '@@' + 'Billable';
         objpinnacleMaster.GP_TimeSheet_Financial_Month__c = system.today().addDays(-10);
        update objpinnacleMaster;
        String employeemonthYear = empObj.Id + entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        GPServiceTimesheetTransaction.getValidatedTimeSheetEntriesLst(listOfTimeshtEntry,new Map<String,GP_Temporary_Data__c>{tempDataKey => objTemporaryTimeSheet },
            new Set<Id>(),
            new Set<Id>{empObj.Id},
            new Set<String>{'4532'},
            new Set<String>{employeemonthYear});
    } 
    
    @isTest
    public static void testgetValidatedTimeSheetEntriesLst4() {
         setupCommonData();
        timeshtEntryObj  = [select id, GP_Project__c,GP_Project__r.Name, GP_Employee__r.GP_EMPLOYEE_TYPE__c,GP_Employee__r.GP_Final_OHR__c,GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c where id =:timeshtEntryObj.Id ];
        timeshtEntryObj.GP_Modified_Hours__c = 89;
        update timeshtEntryObj;
        List<GP_Timesheet_Entry__c> listOfTimeshtEntry = new List<GP_Timesheet_Entry__c>();
        listOfTimeshtEntry.add(timeshtEntryObj);
        DateTime entryDate = system.today();
        String tempDataKey = empObj.GP_Final_OHR__c + '@@' + '4532' + '@@';
        tempDataKey += String.ValueOf(system.today()).split(' ')[0] + '@@' + 'Billable';
        String employeemonthYear = empObj.Id + entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        GPServiceTimesheetTransaction.getValidatedTimeSheetEntriesLst(listOfTimeshtEntry,new Map<String,GP_Temporary_Data__c>{tempDataKey => objTemporaryTimeSheet },
            new Set<Id>(),
            new Set<Id>{empObj.Id},
            new Set<String>{'4532'},
            new Set<String>{employeemonthYear});
    } 
    
    @isTest
    public static void testValidateTimsheetAgainstResourceAllocation() {
        GPServiceTimesheetTransaction.validateTimsheetAgainstResourceAllocation();
    }
    
}