@isTest
public class AccSeverSideBoardexTest {

    public static Account oAccount;
    public static Contact oContact;
    
     private static void setupTestData(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
         oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
         system.debug('AccountID'+oAccount);
       
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
            
            
        }
    
    @isTest public static void testMethod1()
    {
        
        setupTestData();
        List<Id> idAcc=new List<Id>();
        idAcc.add(oAccount.Id);
        oAccount.SiteClickCount__c = 1;
        update oAccount;
          test.startTest();
           AccServerSideContBoardex.getAccountField(idAcc);
           AccServerSideContBoardex.getClickAccount(oAccount.Id);
           AccServerSideContBoardex.saveClickCount(oAccount.Id);
        test.stopTest();
    }
  
    @isTest public static void testMethod2()
    {
        
        setupTestData();
        oAccount.SiteClickCount__c = null;
        update oAccount;
         test.startTest();
        
           AccServerSideContBoardex.getAccountField(null);
           AccServerSideContBoardex.getClickAccount(null);
           AccServerSideContBoardex.saveClickCount(oAccount.Id);
        test.stopTest();
    }
    
    
}