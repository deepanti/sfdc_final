public with sharing class redirectWLRmain{
    
    Public string opportunityid{get; set;}
    
    
    public redirectWLRmain(ApexPages.StandardController controller) 
    {
        
        if (controller.getrecord().id != NULL) 
        {
                opportunityid = controller.getrecord().id;
        }
        
        
        
    }
    
    
    public pageReference redirection()
    {
                                             if (Userinfo.getUiThemeDisplayed().equals('Theme3'))
                                                {
                                                    PageReference pageRef = new PageReference('/apex/WLR?id=' + opportunityid );
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                }
                                             else if (Userinfo.getUiThemeDisplayed().equals('Theme4t'))
                                                {
                                                    PageReference pageRef = new PageReference('/apex/WLR_mobile?id=' + opportunityid );
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                                
                                                }
                                             else if(Userinfo.getUiThemeDisplayed().equals('Theme4d'))
                                                {    
                                                    PageReference pageRef = new PageReference('/apex/WLR?id=' + opportunityid );
                                                    pageRef.setRedirect(true);
                                                    return pageRef;
     
                                                }       
                                             else
                                                return Null;
        
    
    }

    
}