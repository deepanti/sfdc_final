@isTest
public class GPSelectorWorkLocationSDOMasterTracker {
     
     @isTest
    public static void testgetSObjectType() {
         GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GPSelectorWorkLocationSDOMaster sdomasterSelector = new GPSelectorWorkLocationSDOMaster();
        List<GP_Work_Location__c> listOfWL = sdomasterSelector.selectById(new Set<Id> {sdo.Id});
        sdomasterSelector.selectByIdofWOrkLocationAndSDO(null);
   		sdomasterSelector.selectByOracleIdofWOrkLocationAndSDO(null);
        sdomasterSelector.selectResourcFromWorkLocation(null);
        sdomasterSelector.selectMapOfWorkLocationToResource(null);
        GPSelectorWorkLocationSDOMaster.selectWOrkLocationAndSDOByOracleID(null);
   		GPSelectorWorkLocationSDOMaster.getAddToOppProjectWorkLocations();
   		sdomasterSelector.selectByRecordId(null);
   		GPSelectorWorkLocationSDOMaster.selectSODById(sdo.Id);
        GPSelectorWorkLocationSDOMaster.selectGlobalSDO();
    }
    
    
}