/**
 * @author Anmol.kumar
 * @date 22/10/2017
 *
 * @group ProjectApproval
 * @group-content ../../ApexDocContent/ProjectApproval.html
 *
 * @description Apex Service for project Approval module,
 */
public with sharing class GPServiceStageWiseFieldValidator {
    private static String MANDATORY_FIELD_ERROR_LABEL = 'Some field value are not filled for this stage';
    private static String DEFAULT_STAGE_NAME = 'Cost Charging';
    private static String SUCCESS_LABEL = 'SUCCESS';


    private static final String RESOURCE_ALLOCATION_SECTION_LABEL = 'Resource Allocation',
        BUDGET_AND_EXPENSE_SECTION_LABEL = 'Budget and Expense',
        DOCUMENT_UPLOAD_SECTION_LABEL = 'Document Upload',
        ADDITIONAL_SDO_SECTION_LABEL = 'Additional SDO',
        WORK_LOCATION_SECTION_LABEL = 'Work location',
        PROJECT_DOCUMENT_LABEL = 'Project Document',
        PROJECT_ADDRESS_LABEL = 'Project Address',
        LEADERSHIP_SECTION_LABEL = 'Leadership',
        PROJECT_SECTION_LABEL = 'Project';

    private Map < String, Map < String, List < String >>> mapOfSectionToMapOfSubSectionToListOfError;
    private list < string > lstFields;

    public Boolean isFormattedLogRequired;
    public Boolean isLogForTemporaryId;

    GPWrapFieldValue objResponse;
    private Sobject project;
    private string strJson;
    private list < GP_Project_Leadership__c > listOfProjectLeadership;

    private static List < String > listOfStageName = new List < String > {
        'Cost Charging',
        'Billing Cost Charging',
        'Revenue Cost Charging',
        'Approved'
    };

    private static Map < String, String > mapOfLeadershipCategoryToSectionName = new Map < String, String > {
        'MandatoryKeyMembers' => 'Mandatory Key Members',
        'Regional' => 'Regional Leadership',
        'Account' => 'Account Leadership',
        'HSL' => 'HSL Leadership'
    };

    public GPServiceStageWiseFieldValidator(Sobject project, string strJson) {
        this.project = project;
        this.strJson = strJson;
        GP_Project__c castedProject = (GP_Project__c) project;
        DEFAULT_STAGE_NAME = castedProject.RecordType.Name == 'Indirect PID' ? 'Approved Support' : DEFAULT_STAGE_NAME;

        mapOfSectionToMapOfSubSectionToListOfError = new Map < String, Map < String, List < String >>> ();
        objResponse = new GPWrapFieldValue();
    }

    /**
     * @description sets project leadership.
     * @param listOfProjectLeadership
     * 
     * @return GPServiceStageWiseFieldValidator
     * 
     * @example
     * new GPServiceStageWiseFieldValidator().setProjectLeadership(<list Of ProjectLeadership>);
     */
    public GPServiceStageWiseFieldValidator setProjectLeadership(list < GP_Project_Leadership__c > listOfProjectLeadership) {
        this.listOfProjectLeadership = listOfProjectLeadership;
        return this;
    }

    public GPWrapFieldValue validateFields() {
        String projectStage = getProjectStage();

        updateObjResponse(projectStage);

        if (objResponse.isSuccess) {
            String projectLeadershipStage = getProjectLeadershipStage();
            //updateObjResponse(projectLeadershipStage);
        }

        //ICON status = Singed Contract received should be checked before submission of project. 
        //Means when user submits the project and if ICON status is not Signed Contract Approved then, 
        //Project stage cannot be set to approved.
        GP_Project__c castedProject = (GP_Project__c) project;
        Map < String, Boolean > mapOfStageToIsApproved = (Map < String, Boolean > ) JSON.deserialize(System.label.GP_Approved_Icon_Status, Map < String, Boolean > .class);
        Boolean isRevenueStageValid = false;

        if (mapOfStageToIsApproved.containsKey(castedProject.GP_CRN_Number__r.GP_Status__c) && mapOfStageToIsApproved.get(castedProject.GP_CRN_Number__r.GP_Status__c)) {
            isRevenueStageValid = true;
        }
        if (objResponse.strStageName == 'Approved' && !isRevenueStageValid) {
            objResponse.strStageName = 'Revenue Cost Charging';
            objResponse.isRevenueStageValid = false;
        }

        //in case of support project the stage will always be "Approved Support"
        if (castedProject.RecordType.Name == 'Indirect PID') {
            objResponse.strStageName = 'Approved Support';
            objResponse.isRevenueStageValid = true;
        }

        List < GP_Billing_Milestone__c > lstOfRelatedBillingMilestone = [select id from GP_Billing_Milestone__c where GP_Project__c =: castedProject.Id];
        List < GP_Project_Budget__c > lstOfRelatedBudget = [select id from GP_Project_Budget__c where GP_Project__c =: castedProject.Id];
        
        if ((castedProject.RecordType.Name == 'CMITS' || castedProject.RecordType.Name == 'OTHER PBB') && (castedProject.GP_Project_type__c == 'Fixed Price' || castedProject.GP_Project_type__c == 'Fixed monthly') && lstOfRelatedBillingMilestone.size() == 0) {
            objResponse.strStageName = 'Cost Charging';
            objResponse.isRevenueStageValid = false;
        }
        
        if (((castedProject.RecordType.Name == 'CMITS' && !castedProject.GP_deal__r.GP_No_Pricing_in_OMS__c && 
                    (castedProject.GP_Project_type__c == 'Fixed Price' || castedProject.GP_Project_type__c == 'Fixed monthly')) ||
                 (castedProject.RecordType.Name == 'CMITS' && castedProject.GP_deal__r.GP_No_Pricing_in_OMS__c &&
                (castedProject.GP_Project_type__c == 'Fixed Price' )) || (castedProject.RecordType.Name == 'OTHER PBB' &&
                castedProject.GP_Project_type__c == 'Fixed Price' ) ) && lstOfRelatedBudget.size() == 0) {
            objResponse.strStageName = 'Cost Charging';
            objResponse.isRevenueStageValid = false;
        }

        objResponse.mapOfSectionToMapOfSubSectionToListOfError = mapOfSectionToMapOfSubSectionToListOfError;

        return objResponse;
    }

    private String getProjectLeadershipStage() {
        String projectLeadershipStage = '',
            errorMessage = '';

        Map < String, List < String >> mapOfSubSectionToListOfErrors = new Map < String, List < String >> ();

        Map < String, GP_Project_Leadership__c > mapOfRoleToProjectLeadership = getMapOfRoleToProjectLeadership();
        Map < String, List < String >> mapOfStageNameToListOfRoles = getMapOfStageNameToListOfRoles();

        Boolean isElligibleForCurrentStage = true;

        for (String currentStageName: listOfStageName) {

            isElligibleForCurrentStage = true;

            List < String > listOfRoles = mapOfStageNameToListOfRoles.get(currentStageName);

            if (listOfRoles == null || listOfRoles.isEmpty()) {
                continue;
            }

            if (isNotElligibleForCurrentStage(isElligibleForCurrentStage, currentStageName, listOfRoles)) {
                break;
            }

            for (String roleName: listOfRoles) {

                if (currentStageName == DEFAULT_STAGE_NAME &&
                    !mapOfRoleToProjectLeadership.containsKey(roleName)) {

                    isElligibleForCurrentStage = false;
                    GP_Project_Leadership__c projectLeadership = mapOfRoleToProjectLeadership.get(roleName);
                    if (projectLeadership != null) {
                        String subSectionName = projectLeadership.RecordType.Name != null ? projectLeadership.RecordType.Name : 'Regional/AdditionalAccess';

                        errorMessage = roleName + ' is mandatory for ' + DEFAULT_STAGE_NAME + ' stage';

                        if (!mapOfSubSectionToListOfErrors.containsKey(subSectionName)) {
                            mapOfSubSectionToListOfErrors.put(subSectionName, new List < String > ());
                        }

                        mapOfSubSectionToListOfErrors.get(subSectionName).add(errorMessage);
                    }
                } else if (!mapOfRoleToProjectLeadership.containsKey(roleName)) {
                    isElligibleForCurrentStage = false;
                }
            }

            if (isElligibleForCurrentStage) {
                projectLeadershipStage = currentStageName;
            }
        }

        mapOfSectionToMapOfSubSectionToListOfError.put(LEADERSHIP_SECTION_LABEL, mapOfSubSectionToListOfErrors);
        return projectLeadershipStage;
    }

    private String getProjectStage() {
        Map < String, List < String >> mapOfSubSectionToListOfErrors = new Map < String, List < String >> ();

        if (strJson == null || strJson == '') {
            return DEFAULT_STAGE_NAME;
        }

        Boolean isElligibleForCurrentStage = true;
        lstFields = new List < String > ();
        String strStageName = '';

        Map < string, object > mapOfStageNameToRequiredFields = getMapOfStageNameToRequiredFields();
        Map < string, object > mapOfStageWiseFields;
        List < object > listOfFields;

        if (mapOfStageNameToRequiredFields == null) {
            return DEFAULT_STAGE_NAME;
        }

        Map < String, String > mapOfApiNameToLabel = GPCommon.getMapOfFieldNameToLabel('GP_Project__c');

        for (String currentStageName: listOfStageName) {

            mapOfStageWiseFields = (map < string, object > ) mapOfStageNameToRequiredFields.get(currentStageName);

            if (mapOfStageWiseFields == null) {
                continue;
            }

            listOfFields = (list < object > ) mapOfStageWiseFields.get('fields');

            if (isNotElligibleForCurrentStage(isElligibleForCurrentStage, currentStageName, listOfFields)) {
                break;
            }

            for (object objField: listOfFields) {
                String fieldName = (String) objField;

                //check if the field is actually valid
                if (!isValidField(project, fieldName)) {
                    continue;
                }

                if (project.get(fieldName) == null && currentStageName == DEFAULT_STAGE_NAME) {
                    isElligibleForCurrentStage = false;
                    //lstFields.add(strField);
                    String message = mapOfApiNameToLabel.get(fieldName.toLowercase()) + ' is Mandatory for ' + DEFAULT_STAGE_NAME + ' Stage';
                    lstFields.add(message);
                } else if (project.get(fieldName) == null) {
                    isElligibleForCurrentStage = false;
                }
            }

            if (isElligibleForCurrentStage) {
                strStageName = currentStageName;
            }
        }

        mapOfSubSectionToListOfErrors.put('Project', lstFields);
        mapOfSectionToMapOfSubSectionToListOfError.put('Project', mapOfSubSectionToListOfErrors);

        return strStageName;
    }

    private Boolean isValidField(SObject project, String fieldName) {
        // if GP_Portal_Name__c then it is only applicable for 
        //or(v.projectRecord.GP_MOD__c == 'Upload to Portal', 
        //or(v.projectRecord.GP_MOD__c == 'Email and Upload to Portal', 
        //or(v.projectRecord.GP_MOD__c == 'Email, Hard Copy and Upload to Portal', 
        //or(v.projectRecord.GP_MOD__c == 'Email, Hard Copy, FAX and Upload to Portal', 
        //or(v.projectRecord.GP_MOD__c == 'Fax and Upload to Portal', 
        //v.projectRecord.GP_MOD__c == 'Hard Copy and Upload to Portal'

        if (fieldName == 'GP_Portal_Name__c') {
            String modValue = (String) project.get('GP_MOD__c');

            return modValue == 'Upload to Portal' ||
                modValue == 'Email and Upload to Portal' ||
                modValue == 'Email, Hard Copy and Upload to Portal' ||
                modValue == 'Email, Hard Copy, FAX and Upload to Portal' ||
                modValue == 'Fax and Upload to Portal' ||
                modValue == 'Hard Copy and Upload to Portal';

        } else if (fieldName == 'GP_Customer_Contact_Number__c') {
            String modValue = (String) project.get('GP_MOD__c');

            return modValue == 'Hard Copy' ||
                modValue == 'Email and Hard Copy' ||
                modValue == 'Email, Hard Copy and Upload to Portal' ||
                modValue == 'Email, Hard Copy, FAX and Upload to Portal' ||
                modValue == 'Fax and Hard Copy' ||
                modValue == 'Hard Copy and Upload to Portal';

        } else if (fieldName == 'GP_FAX_Number__c') {
            String modValue = (String) project.get('GP_MOD__c');

            return modValue == 'Email and Fax' ||
                modValue == 'Email, Hard Copy, FAX and Upload to Portal' ||
                modValue == 'Fax' ||
                modValue == 'Fax and Hard Copy' ||
                modValue == 'Fax and Upload to Portal';

        } else if (fieldName == 'GP_Timesheet_Requirement__c') {
            String projectType = (String) project.get('GP_Project_type__c');
            return projectType == 'T&M' ||
                projectType == 'FTE';

        } else if (fieldName == 'GP_Overtime_Billable__c') {
            String projectType = (String) project.get('GP_Project_type__c');
            return projectType == 'T&M';

        } else if (fieldName == 'GP_Holiday_Billable__c') {
            String projectType = (String) project.get('GP_Project_type__c');
            return projectType == 'T&M';

        } else if (fieldName == 'GP_Transition_End_Date__c') {
            Boolean transitionAllowed = (Boolean) project.get('GP_Transition_Allowed__c');
            return transitionAllowed != null && transitionAllowed == true;

        } else if (fieldName == 'GP_Parent_Project_for_Group_Invoice__c') {
            Boolean groupedInvoice = (Boolean) project.get('GP_Grouped_Invoice__c');
            return groupedInvoice != null && groupedInvoice == true;

        } else if (fieldName == 'GP_Daily_Normal_Hours__c') {
            String projectType = (String) project.get('GP_Project_type__c');
            String billRateType = (String) Project.get('GP_Bill_Rate_Type__c');

            return (projectType == 'FTE' || projectType == 'T&M') && (billRateType == 'Daily' || billRateType == 'Weekly');

        }

        return true;
    }

    private Map < String, GP_Project_Leadership__c > getMapOfRoleToProjectLeadership() {
        Map < String, GP_Project_Leadership__c > mapOfRoleToProjectLeadership = new Map < String, GP_Project_Leadership__c > ();
        for (GP_Project_Leadership__c projectLeadership: listOfProjectLeadership) {
            mapOfRoleToProjectLeadership.put(projectLeadership.GP_Leadership_Role_Name__c, projectLeadership);
        }

        return mapOfRoleToProjectLeadership;
    }

    private Map < String, List < String >> getMapOfStageNameToListOfRoles() {
        Map < String, List < String >> mapOfStageNameToListOfRoles = new Map < String, List < String >> ();
        GPSelectorLeadershipMaster leadershipMasterSelector = new GPSelectorLeadershipMaster();

        for (GP_Leadership_Master__c leadershipMaster: leadershipMasterSelector.getRoleMasterLeadership()) {
            if (!mapOfStageNameToListOfRoles.containsKey(leadershipMaster.GP_Mandatory_for_stage__c)) {
                mapOfStageNameToListOfRoles.put(leadershipMaster.GP_Mandatory_for_stage__c, new List < String > ());
            }

            mapOfStageNameToListOfRoles.get(leadershipMaster.GP_Mandatory_for_stage__c).add(leadershipMaster.GP_Leadership_Role__c);
        }

        return mapOfStageNameToListOfRoles;
    }

    private Map < string, object > getMapOfStageNameToRequiredFields() {
        map < string, object > mapOfObjectNameToStage = (map < string, object > ) System.JSON.deserializeUntyped(strJson);
        return (map < string, object > ) mapOfObjectNameToStage.get('Project');
    }

    private void updateObjResponse(String stageName) {
        String oldStageName = objResponse.strStageName;
        String finalStageName;

        if (oldStageName != null && String.isNotEmpty(oldStageName)) {

            Integer indexOfOldStageName = Math.max(listOfStageName.indexOf(oldStageName), 0);
            Integer indexOfNewStageName = Math.max(listOfStageName.indexOf(stageName), 0);
            integer finalIndexOfStageName = Math.min(indexOfOldStageName, indexOfNewStageName);

            finalStageName = listOfStageName.get(finalIndexOfStageName);
        } else {
            finalStageName = stageName;
        }

        objResponse.strStageName = finalStageName;
        objResponse.isSuccess = finalStageName != null;
        objResponse.strMessage = finalStageName != null ? SUCCESS_LABEL : MANDATORY_FIELD_ERROR_LABEL;
    }

    private Boolean isNotElligibleForCurrentStage(Boolean isElligibleForCurrentStage, String currentStageName, List < object > listOfFields) {
        return (!isElligibleForCurrentStage &&
                currentStageName != DEFAULT_STAGE_NAME) ||
            listOfFields == null ||
            listOfFields.isEmpty();
    }
}