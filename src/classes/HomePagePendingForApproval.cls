public class HomePagePendingForApproval {
    public class ApprovalDetailsWrapper
    {
        @AuraEnabled public Opportunity oppRecord;
        @AuraEnabled public Id approvalItemId;
        
    }
    @AuraEnabled
    public static List<ApprovalDetailsWrapper> fetchOpportunityPendingForApporval()
    {
        try{
            
            List<ApprovalDetailsWrapper> approvalWrapper=new List<ApprovalDetailsWrapper>();
            Map<Id,Opportunity> opportunityDetailsMap=new Map<Id,Opportunity>();
            Map<Id,Id> pendingGCIApprovalIds = new Map<Id,Id>();
            List<ProcessInstanceWorkItem> approvalItemsLst = new List<ProcessInstanceWorkItem>();
            
            approvalItemsLst = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                WHERE ProcessInstance.Status = 'Pending' AND ActorId =:UserInfo.getUserId()];
            
            
            for (ProcessInstanceWorkItem workItem : approvalItemsLst) 
            {
                if(workItem != null)
                {
                    if(workItem.ProcessInstance.TargetObjectId.getsobjecttype() == getoidSobjectType('Opportunity'))
                        pendingGCIApprovalIds.put(workItem.ProcessInstance.TargetObjectId,workItem.id);
                }
                
            }
            
            
            if(pendingGCIApprovalIds.size() > 0)
            {
                opportunityDetailsMap = new Map<Id,Opportunity>([Select id,Amount,CloseDate,Name,StageName
                                                                 From Opportunity Where Id in: pendingGCIApprovalIds.keySet()
                                                                 And StageName not in ('6. Signed Deal','7. Lost','8. Dropped')]);
                for(Opportunity oppRecord:opportunityDetailsMap.values())
                {
                    if(pendingGCIApprovalIds.containsKey(oppRecord.id))
                    {
                        ApprovalDetailsWrapper wrapObj=new ApprovalDetailsWrapper();
                        wrapObj.oppRecord=oppRecord;
                        wrapObj.approvalItemId=pendingGCIApprovalIds.get(oppRecord.id);
                        approvalWrapper.add(wrapObj);
                    }
                }
            }
            system.debug('approvalWrapper'+approvalWrapper);
            return approvalWrapper;
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'HomePagePendingForApproval', 'fetchOpportunityPendingForApporval','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    /****************************************************************************
Method to fetch sObject Type 
****************************************************************************/
    private static Schema.SObjectType getoidSobjectType(String objectAPIName)
    {
        Map<String, Schema.SObjectType> globalDescription =  Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = globalDescription.get(objectAPIName); 
        return sObjType;
    }
    
    /****************************************************************************
Method to Approve or Reject Pending Items
****************************************************************************/
    @AuraEnabled 
    public static boolean approvePendingItems(Id approvalPendingItemId,String comments,String action)
    {
        try
        {
            system.debug('approvalPendingItemId'+approvalPendingItemId+' comments'+comments+' action'+action);
            // Instantiate the new ProcessWorkitemRequest object and populate it
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments(comments);
            req.setAction(action);
            
            // Use the ID from the newly created item to specify the item to be worked
            req.setWorkitemId(approvalPendingItemId);
            
            // Submit the request for approval
            Approval.ProcessResult result =  Approval.process(req);
            return result.isSuccess();  
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'HomePagePendingForApproval', 'approvePendingItems','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}