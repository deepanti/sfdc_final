public class GPSelectorEmployeeMaster extends fflib_SObjectSelector {
    public GPSelectorEmployeeMaster() {}
    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Employee_Master__c.Name,
            GP_Employee_Master__c.GP_SFDC_User__c,
            GP_Employee_Master__c.GP_isActive__c,
            GP_Employee_Master__c.GP_Employee_Unique_Name__c
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Employee_Master__c.sObjectType;
    }

    public List < GP_Employee_Master__c > selectById(Set < ID > idSet) {
        return (List < GP_Employee_Master__c > ) selectSObjectsById(idSet);
    }

    public Map < Id, GP_Employee_Master__c > selectEmployeeMasterRecordMap(Set < Id > setOfEmployeeId) {
        return new Map < Id, GP_Employee_Master__c > ([select id, GP_ACTUAL_TERMINATION_Date__c, GP_Contract_Signed_Date__c,
            GP_HIRE_Date__c from GP_Employee_Master__c
            where id in: setOfEmployeeId
        ]);
    }
    public List < GP_Employee_Master__c > selectEmployeeMasterRecords(Set < Id > setOfEmployeeId) {
        return [select id, GP_ACTUAL_TERMINATION_Date__c, 
                        GP_Employee_HR_History__r.GP_Allocation_Status__c, 
                        GP_Contract_Signed_Date__c, GP_EMPOYMENT_CATEGORY__c,
                        GP_Employee_Category__c, 
                        GP_DESIGNATION__c,
                        GP_HIRE_Date__c, 
                        GP_EMPLOYEE_TYPE__c, 
                        GP_Person_ID__c, 
                        GP_isActive__c, 
                        GP_Legal_Entity_Code__c, 
                        GP_Final_OHR__c, 
                        GP_SDO_OracleId__c,
                        GP_Resource_Id__c,
                        GP_SDO__c,
                        GP_SDO__r.GP_Lending_SDO_Code__c
            from GP_Employee_Master__c
            where id in: setOfEmployeeId
        ];
    }

    public GP_Employee_Master__c selectEmployeeMasterRecord(String employeeId) {
        return [select id, GP_EMPLOYEE_TYPE__c, GP_Employee_Category__c, GP_HIRE_Date__c, GP_ACTUAL_TERMINATION_Date__c, GP_Payroll_Country__c,
            GP_SDO__r.Name, GP_isActive__c, GP_Final_OHR__c, GP_Legal_Entity_Code__c,GP_SDO__r.GP_Lending_SDO_Code__c,
            GP_SDO__c, GP_BAND__c, GP_LOCATION__c, GP_Business_Group_NAME__c,GP_SDO__r.GP_Oracle_Id__c,
            GP_Work_Location__r.Name, GP_Legal_Entity_Name__c, GP_OFFICIAL_EMAIL_ADDRESS__c,Name,GP_Employee_Unique_Name__c,
            GP_ORGANIZATION__c
            from GP_Employee_Master__c where id =: employeeId
        ];
    }

    public list < GP_Employee_Master__c > selectUserFromEmployeeMaster(Set < Id > setOfEmployeeId) {
        list < GP_Employee_Master__c > lstOfUsers = [select Id,
            GP_Employee_HR_History__r.Work_Location_SDO_Master__r.GP_User__r.Email,
            GP_Employee_HR_History__r.Work_Location_SDO_Master__r.GP_User__r.Id
            from GP_employee_Master__c where GP_Employee_HR_History__r.Work_Location_SDO_Master__c !=
            null and id in: setOfEmployeeId
        ];
        return lstOfUsers;
    }

    public list < user > getUserByEmployeeId(set < Id > Setofid) {
        list < user > lstOfUser = [select Id, isActive, GP_Person_Id__c From user where id in: Setofid];
        return lstOfUser;

    }
    public List < GP_Employee_Master__c > getEmployeeListForPersonId(Set < String > setOfPersonIds) {
        return [Select ID, GP_Person_ID__c, Name, GP_EMPLOYEE_TYPE__c, GP_Employee_Category__c,
            GP_SDO__r.Name, GP_isActive__c, GP_SFDC_User__c,
            GP_SDO__c, GP_BAND__c, GP_LOCATION__c,
            GP_Work_Location__r.Name, GP_Legal_Entity_Name__c, GP_Legal_Entity_Code__c, GP_OFFICIAL_EMAIL_ADDRESS__c,
            GP_ORGANIZATION__c, GP_Final_OHR__c from GP_Employee_Master__c
            where GP_Person_ID__c IN: setOfPersonIds
        ];

    }
    public List < GP_Employee_Master__c > getEmployeeListForOHRId(Set < String > setOfOHRids) {
        return [Select ID, GP_Person_ID__c, Name, GP_EMPLOYEE_TYPE__c, GP_Employee_Category__c,GP_ACTUAL_TERMINATION_Date__c,
            GP_SDO__r.Name, GP_isActive__c, GP_SFDC_User__c,GP_SDO__r.GP_Lending_SDO_Code__c,GP_SDO__r.GP_Oracle_Id__c,
            GP_SDO__c, GP_BAND__c, GP_LOCATION__c, GP_PHYSICAL_WORK_LOCATION__c,
            GP_Work_Location__r.Name, GP_Legal_Entity_Name__c, GP_Legal_Entity_Code__c, GP_OFFICIAL_EMAIL_ADDRESS__c,
            GP_ORGANIZATION__c, GP_Final_OHR__c from GP_Employee_Master__c
            where GP_Final_OHR__c IN: setOfOHRids and GP_isActive__c = true
        ];

    }
    public List < GP_Employee_Master__c > getActiveEmployeeRecords(String processId) {
        return [Select ID, GP_Person_ID__c, Name, GP_EMPLOYEE_TYPE__c, GP_Employee_Category__c,
            GP_SDO__r.Name, GP_isActive__c,
            GP_SDO__c, GP_BAND__c, GP_LOCATION__c,
            GP_Work_Location__r.Name, GP_Legal_Entity_Name__c, GP_Legal_Entity_Code__c, GP_OFFICIAL_EMAIL_ADDRESS__c,
            GP_ORGANIZATION__c, GP_Final_OHR__c from GP_Employee_Master__c where GP_isActive__c = true
            and GP_Process_ID__c =: processId
        ];

    }
    public Map < Id, GP_Employee_Master__c > selectByRecordId(Set < Id > setOfIds) {
        return new Map < Id, GP_Employee_Master__c > ([Select Id, CurrencyIsoCode, GP_SFDC_User__c, GP_OFFICIAL_EMAIL_ADDRESS__c,
            GP_Org_TSC_Id__c,
            GP_PERSONAL_EMAIL_ADDRESS__c
            FROM GP_Employee_Master__c
            WHERE Id in: setOfIds
        ]);
    }
    public List < GP_Employee_Master__c > selectActiveEmployeeWithSFDCUserIds(Set < Id > setOfIds) {
        return [Select Id, CurrencyIsoCode, GP_SFDC_User__c, GP_Person_ID__c
            FROM GP_Employee_Master__c
            WHERE GP_SFDC_User__c in: setOfIds and GP_isActive__c = true
        ];
    }
}