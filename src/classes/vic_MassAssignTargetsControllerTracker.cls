@istest
private class vic_MassAssignTargetsControllerTracker {
    
    static testMethod void validatevic_MassAssignTargetsController() {
        vic_MassAssignTargetsController.getFinancialYear();
        Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
        masterVICRoleObj.Name='XYZ';
        insert masterVICRoleObj;
        
        User_VIC_Role__c userVICRoleObj = new User_VIC_Role__c();
        userVICRoleObj.Master_VIC_Role__c=masterVICRoleObj.Id;
        userVICRoleObj.User__c=userinfo.getuserid();
        insert userVICRoleObj;
        
        Target__c targetObj = new Target__c();
        targetObj.User__c=userinfo.getuserid();
        targetObj.Start_date__c=system.today();
        insert targetObj;
        
        list<Id> userObj = new list<Id>();
        userObj.add(userinfo.getuserid());
        vic_MassAssignTargetsController.getAllUsers(masterVICRoleObj.Id, '2018');
        vic_MassAssignTargetsController.getVicRole(2018);
        vic_MassAssignTargetsController.getAllPlans(userObj);
    }
    
    @istest
    static void getPlanName_test(){
        Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
        masterVICRoleObj.Name='XYZ';
        insert masterVICRoleObj;
        
        APXTConga4__Conga_Template__c tstCngaTmplt=new APXTConga4__Conga_Template__c();
        tstCngaTmplt.APXTConga4__Name__c='test';
        insert tstCngaTmplt;
        
        Plan__c tstPlan=new Plan__c();
        tstPlan.Name='TestPlan';
        tstPlan.Year__c = '2017' ;
        tstPlan.Conga_Template__c=tstCngaTmplt.Id;
        insert tstPlan;
        
        VIC_Role__c tstVcRole=new VIC_Role__c();
        tstVcRole.Name='tstVcRole';
        tstVcRole.Plan__c=tstPlan.Id;
        //tstVcRole.Role__c='BD';
        tstVcRole.Master_VIC_Role__c=masterVICRoleObj.Id;
        insert tstVcRole;
        
        Master_Plan_Component__c tstPlnCmpnt=new Master_Plan_Component__c();
        tstPlnCmpnt.Name='Test';
        tstPlnCmpnt.Point_type__c='Currency';
        insert tstPlnCmpnt;
        
        Plan_Component__c tstCmpnt=new Plan_Component__c();
        tstCmpnt.Master_Plan_Component__c=tstPlnCmpnt.Id;
        tstCmpnt.Plan__c=tstPlan.Id;
        insert tstCmpnt;
        
        Profile pId=[Select id from Profile where name='System Administrator'];
        
        User u = new User(
            ProfileId = pId.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        insert u;
        
        List<String> lstUsrDtls=new List<String>();
        String concate=u.Id +'-'+'u.Name';
        lstUsrDtls.add(concate);
        
        MasterWrapper lstMstrWrpr=new MasterWrapper();
        lstMstrWrpr.strMasterName=tstPlnCmpnt.Name;
        lstMstrWrpr.strMasterId=tstPlnCmpnt.Id;
        lstMstrWrpr.strMasterType=tstPlnCmpnt.Point_type__c;
        lstMstrWrpr.targetCurrency=2.0;
        lstMstrWrpr.targetPercent=2.0;
        
        List<MasterWrapper> lstwrps=new List<MasterWrapper>(); 
        lstwrps.add(lstMstrWrpr);
        
        WrapperRows lstWrprRows=new WrapperRows();
        lstWrprRows.strUserName=u.lastName;
        lstWrprRows.strUserId=u.Id;
        lstWrprRows.lstMasterCmpName=lstwrps;
        List<WrapperRows> lstwrprrow=new List<WrapperRows>();
        lstwrprrow.add(lstWrprRows);
        
        String strSobjects = ' '; 
        strSobjects += JSON.Serialize(lstwrprrow); 
        system.debug('lstWrprRows' + lstWrprRows);
        
        vic_MassAssignTargetsController.getPlanName(string.valueOf(masterVICRoleObj.Id),lstUsrDtls,'2017');
        try{
            vic_MassAssignTargetsController.getPlanName(string.valueOf(masterVICRoleObj.Id),lstUsrDtls,'1999');
        }
        catch(Exception e){
            
        }
        vic_MassAssignTargetsController.insertTarget(tstPlan.Id,strSobjects,'2018','USD');
        vic_MassAssignTargetsController.getLocalCurrency(tstVcRole.Id);
        vic_MassAssignTargetsController.getEmptyTargetObject();
        
    }
    
     
    
    public class WrapperRows{
        public String strUserName;
        public String strUserId;
        public List<MasterWrapper> lstMasterCmpName;
    }
    
    public class MasterWrapper{
        public String strMasterName;
        public String strMasterId;
        public String strMasterType;
        public Decimal targetCurrency;
        public decimal targetPercent;
    }
}