public class GPDomainProjectLeadership extends fflib_SObjectDomain {

    public GPDomainProjectLeadership(List < GP_Project_Leadership__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainProjectLeadership(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Project_Leadership__c.SObjectType
    };

    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        applyDefaultsOnInsertUpdate(null);
        setIsUpdateCheckBox(null);
        //validateProjectLeadershipRecordAccess(uow, null);
    }

    public override void onAfterInsert() {

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, null);
        updatePIDApproverUserOnProject(uow, null);
        updateApproverIdsOnProject(uow, null);
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        setIsUpdateCheckBox(oldSObjectMap);
        //validateProjectLeadershipRecordAccess(uow, oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, oldSObjectMap);
        updatePIDApproverUserOnProject(uow, oldSObjectMap);
        updateApproverIdsOnProject(uow, oldSObjectMap);
        uow.commitWork();
    }



    public void applyDefaultsOnInsertUpdate(Map < Id, SOBject > oldSObjectMap) {
        for (GP_Project_Leadership__c objPL: (List < GP_Project_Leadership__c > ) records) {

            if (oldSObjectMap == null && objPL.GP_Project_Oracle_PID__c != 'NA' && objPL.GP_Parent_Project_Leadership__c == null) {
                objPL.GP_isUpdated__c = true;
            }
        }
    }

    public void setIsUpdateCheckBox(Map < Id, SOBject > oldSObjectMap) {
        List < String > lstOfFieldAPINames = new List < String > ();
        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Project_Leadership__c.fieldSets.getMap().get('GP_Fields_For_Approval').getFields()) {
            lstOfFieldAPINames.add(fields.getFieldPath());
        }
        Set < Id > setOfParentRecords = new Set < Id > ();

        for (GP_Project_Leadership__c projectLeadership: (List < GP_Project_Leadership__c > ) records) {

            if (projectLeadership.GP_Parent_Project_Leadership__c != null) {
                setOfParentRecords.add(projectLeadership.GP_Parent_Project_Leadership__c);
            }
            if (oldSObjectMap != null && projectLeadership.GP_Active__c == false && oldSObjectMap.get(projectLeadership.id).get('GP_Active__c') == true) {
                projectLeadership.GP_isUpdated__c = true;
            }

        }
        if (setOfParentRecords.size() > 0) {
            map < Id, GP_Project_Leadership__c > mapOfParentRecords = new map < Id, GP_Project_Leadership__c > ();
            for (GP_Project_Leadership__c projectL: new GPSelectorProjectLeadership().selectByProjectLeaderShipId(setOfParentRecords)) {
                mapOfParentRecords.put(projectL.Id, projectL);
            }

            GPCommon.setIsUpdatedField(records, mapOfParentRecords, lstOfFieldAPINames, 'GP_Parent_Project_Leadership__c');

        }
    }

    public override void onBeforeDelete() {
        removeProjectAccess();

    }

    /* public void updatePIDApproverFinance(fflib_SObjectUnitOfWork uow, Map<Id, SOBject> oldSObjectMap) {
        Set<String> fieldSet = new Set<String>();
        GP_Project_Leadership__c oldProjLeadership;
        Map<ID,GP_Project__c > mapofIdToobjProject = new Map<Id,GP_Project__c>();
        Id MandatoryKeyMembersRecordTypeId =  GPCommon.getRecordTypeId('GP_Project_Leadership__c','Mandatory Key Members');
        Id accountRecordTypeId =  GPCommon.getRecordTypeId('GP_Project_Leadership__c','Account Leadership');
        Id hslRecordTypeId =  GPCommon.getRecordTypeId('GP_Project_Leadership__c','HSL Leadership');
        Id bpmRecordTypeId =  GPCommon.getRecordTypeId('GP_Project__c','BPM');
        
                
        //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
        for(Schema.FieldSetMember fields :Schema.SObjectType.GP_Project_Leadership__c.fieldSets.getMap().get('GP_BPR_Approval_Required').getFields()){
            fieldSet.add(fields.getFieldPath());
        }

        if(fieldSet !=null && fieldSet.size()>0)
        {
            for(GP_Project_Leadership__c  projectLeadership : (List<GP_Project_Leadership__c >)records){ 
                
                if(projectLeadership.RecordTypeID == MandatoryKeyMembersRecordTypeId ||
                   projectLeadership.RecordTypeID == accountRecordTypeId ||
                   projectLeadership.RecordTypeID == hslRecordTypeId)
                {
                    mapofIdToobjProject.put(projectLeadership.GP_Project__c, new GP_Project__c(id=projectLeadership.GP_Project__c, GP_BPR_Approval_Required__c = false));
                
                    if(mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_BPR_Approval_Required__c != true) {
                        for(string eachField: fieldSet){

                            if(oldSObjectMap != null)
                            {
                                oldProjLeadership = (GP_Project_Leadership__c)oldSObjectMap.get(projectLeadership.Id);
                                
                                if(projectLeadership.get(eachField) != oldProjLeadership.get(eachField)){
                                    mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_BPR_Approval_Required__c = true;
                                }  

                                // Added by Anmol on 15/01/2018
                                if(oldProjLeadership.get('GP_Leadership_Role_Name__c') == 'FP&A' &&
                                    projectLeadership.get(eachField) != oldProjLeadership.get(eachField)) {
                                    mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_PID_Approver_Finance__c  = true;                            
                                } 
                                
                                if(oldProjLeadership.get('GP_Leadership_Role_Name__c') == 'Billing Spoc' &&
                                    projectLeadership.get(eachField) != oldProjLeadership.get(eachField)) {
                                    
                                    mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_PID_Approver_Finance__c  = true;                            

                                    if(mapofIdToobjProject.get(projectLeadership.GP_Project__c).RecordTypeID == bpmRecordTypeId) {
                                        mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_MF_Approver_Required__c  = true;                            
                                        mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_Controller_Approver_Required__c  = true;                            
                                    } 
                                } 
                            }
                            else
                            {
                                mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_BPR_Approval_Required__c = true; 
                                mapofIdToobjProject.get(projectLeadership.GP_Project__c).GP_PID_Approver_Finance__c  = true; 
                            }                            
                        }
                    }
                }
            }
        }
              
        if(mapofIdToobjProject !=null && mapofIdToobjProject.size()>0)
        {
            list<GP_Project__c> lstProjecttoUpdate = new list<GP_Project__c>();
            for(GP_Project__c objPrj : mapofIdToobjProject.values())
            {
                if(objPrj.GP_BPR_Approval_Required__c == true){
                    lstProjecttoUpdate.add(objPrj);
                }
            }
            if(lstProjecttoUpdate !=null && lstProjecttoUpdate.size()>0)
            {
                uow.registerDirty(lstProjecttoUpdate);
            }
        }   
    } */

    public void updatePIDApproverUserOnProject(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {

        GP_Project_Leadership__c oldProjLeadership;
        Map < ID, string > mapOfPjctIdTOEmpSFDCuserID = new Map < Id, string > ();
        Map < ID, string > mapOfPjctIdTOEmpID = new Map < Id, string > ();
        list < GP_Project__c > lstProjecttoUpdate = new list < GP_Project__c > ();
        Id MandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');

        for (GP_Project_Leadership__c projectLeadership: (List < GP_Project_Leadership__c > ) records) {
            if (projectLeadership.RecordTypeID == MandatoryKeyMembersRecordTypeId && projectLeadership.GP_Leadership_Role_Name__c == 'PID Approver Finance' &&
                projectLeadership.GP_Employee_ID__c != null &&
                projectLeadership.GP_Employee_SFDC_User_ID__c != null) {

                if (oldSObjectMap != null) {
                    oldProjLeadership = (GP_Project_Leadership__c) oldSObjectMap.get(projectLeadership.Id);
                    if (projectLeadership.GP_Employee_ID__c != oldProjLeadership.GP_Employee_ID__c) {
                        mapOfPjctIdTOEmpSFDCuserID.put(projectLeadership.GP_Project__c, projectLeadership.GP_Employee_SFDC_User_ID__c);
                        mapOfPjctIdTOEmpID.put(projectLeadership.GP_Project__c, projectLeadership.GP_Employee_ID__c);
                    }

                } else {
                    mapOfPjctIdTOEmpID.put(projectLeadership.GP_Project__c, projectLeadership.GP_Employee_ID__c);
                    mapOfPjctIdTOEmpSFDCuserID.put(projectLeadership.GP_Project__c, projectLeadership.GP_Employee_SFDC_User_ID__c);
                }
            }
        }

        if (mapOfPjctIdTOEmpSFDCuserID != null && mapOfPjctIdTOEmpSFDCuserID.size() > 0 && mapOfPjctIdTOEmpID != null && mapOfPjctIdTOEmpID.size() > 0) {
            for (GP_Project__c objP: new GPSelectorProject().getProjectDeatil(mapOfPjctIdTOEmpSFDCuserID.keyset())) {

                objP.GP_PID_Approver_User__c = mapOfPjctIdTOEmpSFDCuserID.get(objP.Id);
                objP.GP_Employee_SFDC_ID__c = mapOfPjctIdTOEmpID.get(objP.Id);
                lstProjecttoUpdate.add(objP);
            }
        }

        if (lstProjecttoUpdate != null && lstProjecttoUpdate.size() > 0) {
            uow.registerDirty(lstProjecttoUpdate);
        }
    }


    private void removeProjectAccess() {
        if (records != null && records.size() > 0) {
            set < Id > setOfEmploy = new set < Id > ();
            for (GP_Project_Leadership__c EachLeader: (list < GP_Project_Leadership__c > ) records) {
                if (EachLeader.GP_Employee_ID__c != null) {
                    setOfEmploy.add(EachLeader.GP_Employee_ID__c);
                }
            }

            if (setOfEmploy != null && setOfEmploy.size() > 0) {
                Map < Id, GP_Employee_Master__c > mapOfLeaderShip = new Map < Id, GP_Employee_Master__c > (new GPSelectorEmployeeMaster().selectById((setOfEmploy)));
                for (GP_Project_Leadership__c EachLeader: (list < GP_Project_Leadership__c > ) records) {
                    if (EachLeader.GP_Employee_ID__c != null && mapOfLeaderShip.containsKey(EachLeader.GP_Employee_ID__c) &&
                        mapOfLeaderShip.get(EachLeader.GP_Employee_ID__c).GP_SFDC_User__c != null) {
                        Map < String, Object > mapOfParams = new Map < String, Object > ();
                        mapOfParams.put('strEvent', 'Remove');
                        mapOfParams.put('strProjectId', EachLeader.GP_Project__c);
                        mapOfParams.put('strUserId', mapOfLeaderShip.get(EachLeader.GP_Employee_ID__c).GP_SFDC_User__c);
                        Flow.Interview.GP_ProjectAccess_from_Leadership objFlow = new Flow.Interview.GP_ProjectAccess_from_Leadership(mapOfParams);
                        objFlow.start();
                    }
                }
            }
        }
    }

    //Method no t called 08052018
    // public void validateProjectLeadershipRecordAccess(fflib_SObjectUnitOfWork uow,Map<Id, SOBject> oldSObjectMap)
    //{
    //    Set<ID> setOfParentProjectID = new Set<ID>();
    //    Map<ID,boolean> mapofIDAndBoolean = new Map<ID,boolean>();
    //    for(GP_Project_Leadership__c projectLeadership :  (List<GP_Project_Leadership__c>) records)
    //    {
    //        setOfParentProjectID.add(projectLeadership.GP_Project__c);
    //    }
    //    if(setOfParentProjectID != null && setOfParentProjectID.size()>0)
    //    {
    //        mapofIDAndBoolean = GPServiceProject.validateProjectandChildRecordAccess(setOfParentProjectID);   
    //    }

    //    for(GP_Project_Leadership__c projectLeadership :  (List<GP_Project_Leadership__c>) records)
    //    {
    //        if(mapofIDAndBoolean.get(projectLeadership.GP_Project__c)==false)
    //        {
    //            projectLeadership.addError('You are not a current working users so you are not authorized to work on Project Leadership.');
    //        }
    //    }
    //}

    public void updateApproverIdsOnProject(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
        Set < Id > setOfEmployeeMasterIds = new Set < Id > ();
        Map < Id, GP_Project__c > mapOfProjectRecords = new Map < Id, GP_Project__c > ();
        for (GP_Project_Leadership__c eachProjectLeaderShip: (list < GP_Project_Leadership__c > ) records) {
            if (checkForEmployeeMasterChange(eachProjectLeaderShip, oldSObjectMap) &&
                (eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_FP_A_Approver) ||
                (eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_PID_Approver) ||
                (eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_GPM_Leadesrhip_Role_Code))
                setOfEmployeeMasterIds.add(eachProjectLeaderShip.GP_Employee_ID__c);
        }

        if (setOfEmployeeMasterIds.size() > 0) {
            Map < Id, GP_Employee_Master__c > mapOfIdVsEmployeeMaster = new GPSelectorEmployeeMaster().selectByRecordId(setOfEmployeeMasterIds);

            for (GP_Project_Leadership__c eachProjectLeaderShip: (list < GP_Project_Leadership__c > ) records) {
                GP_Project__c objProject = new GP_Project__c(id = eachProjectLeaderShip.GP_Project__c);
                if (checkForEmployeeMasterChange(eachProjectLeaderShip, oldSObjectMap)) {
                    if (eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_FP_A_Approver) {
                        if (!mapOfProjectRecords.containsKey(objProject.Id)) {
                            objProject.GP_FP_A_Approver__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).GP_SFDC_User__c;
                            mapOfProjectRecords.put(objProject.Id, objProject);
                        } else
                            mapOfProjectRecords.get(objProject.Id).GP_FP_A_Approver__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).GP_SFDC_User__c;

                    } else if ((eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_PID_Approver)) {
                        if (!mapOfProjectRecords.containsKey(objProject.Id)) {
                            objProject.GP_PID_Approver_User__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).GP_SFDC_User__c;
                            mapOfProjectRecords.put(objProject.Id, objProject);
                        } else
                            mapOfProjectRecords.get(objProject.Id).GP_PID_Approver_User__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).GP_SFDC_User__c;

                    } else if ((eachProjectLeaderShip.GP_Leadership_Role_Name__c == System.Label.GP_GPM_Leadesrhip_Role_Code)) {
                        if (!mapOfProjectRecords.containsKey(objProject.Id)) {
                            objProject.GP_GPM_Employee__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).Id;
                            mapOfProjectRecords.put(objProject.Id, objProject);
                        } else
                            mapOfProjectRecords.get(objProject.Id).GP_GPM_Employee__c = mapOfIdVsEmployeeMaster.get(eachProjectLeaderShip.GP_Employee_ID__c).Id;

                    }
                }
            }

            if (mapOfProjectRecords.Values().size() > 0)
                update mapOfProjectRecords.Values();
        }
    }

    private static Boolean checkForEmployeeMasterChange(GP_Project_Leadership__c eachProjectLeaderShip, Map < Id, SOBject > oldSObjectMap) {
        return eachProjectLeaderShip.GP_Employee_ID__c != null && (oldSObjectMap == null ||
            (oldSObjectMap != null && oldSObjectMap.get(eachProjectLeaderShip.Id).get('GP_Employee_ID__c') != eachProjectLeaderShip.GP_Employee_ID__c));
    }
}