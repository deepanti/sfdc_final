/**
* @Description: Class used to calculate kicker incentive 
* @author: Bashim Khan
* @date: May 2018
*/
public class VIC_CalculateKickerIncentivectrl implements VIC_CalculationInterface {
    
    public Decimal calculate(OpportunityLineItem oli, String plancode){
        Decimal incentive = 0;
        Set<String> planCodes = new Set<String>{'IT_GRM'};
        if( planCodes.contains(planCode)  && vic_CommonUtil.getPreCondtionForOLI(oli) 
            && oli.Pricing_Deal_Type_OLI_CPQ__c == VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('OLI_Deal_Type_New_Booking') 
           	&& oli.vic_CPQ_Deal_Status__c == VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('OLI_Deal_Status_Active')){
            	String fixedIncentiveAmountStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Kicker_Component_Fixed_Incentive');
            	incentive = VIC_GeneralChecks.isDecimal(fixedIncentiveAmountStr)?Decimal.valueOf(fixedIncentiveAmountStr):0;
        }
        return incentive;
    }
   
}