public with sharing class ExpertSearchExtension {

    
    
    private String soql {get;set;}
    public String expertname {get;set;}
    public String industryverticalvar{get;set;}
    public String prodfamilyvar {get;set;}
    public String serlinevar {get;set;}
    public String experttypevar{get;set;}
    public String productvar{get;set;}
    public List<Expert__c> experts {get;set;}
    public string geoservedvar{get;set;}
    public string subindustryverticalvar{get;set;}
    public string exptype{get; set;}
    public string expname{get; set;}
    public string IV{get; set;}
    public string PF{get; set;}
    public string SL{get; set;}
    public string PR{get; set;}
    public string Geo{get; set;}
    public string SIV{get; set;}
   public string expid{get; set;}
   public string Opptyid{get; set;}
   public Expert__c exp{get;set;}
  
   
     public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'DESC'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'LastModifiedDate '; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir ; }
    set;
  }
  
   // init the controller and display some sample data when the page loads
  public ExpertSearchExtension(ApexPages.StandardController controller) {
          
          PF='';
          SL='';
          PR='';
   if (ApexPages.currentPage().getParameters().get('expid') != NULL) 
        {
            expid= ApexPages.currentPage().getParameters().get('expid');
        }
        
        if (ApexPages.currentPage().getParameters().get('Opid') != NULL) 
        {
            Opptyid= ApexPages.currentPage().getParameters().get('Opid');
        }
       
        if (ApexPages.currentPage().getParameters().get('exname') != NULL) 
        {
            expname= ApexPages.currentPage().getParameters().get('exname');
        } 
         if (ApexPages.currentPage().getParameters().get('IVval') != NULL) 
        {
            IV= ApexPages.currentPage().getParameters().get('IVval');
        } 
         if (ApexPages.currentPage().getParameters().get('PFval') != NULL) 
        {
            PF= ApexPages.currentPage().getParameters().get('PFval');
        } 
         if (ApexPages.currentPage().getParameters().get('SLval') != NULL) 
        {
            SL= ApexPages.currentPage().getParameters().get('SLval');
        } System.debug('SL'+ SL);
        if (ApexPages.currentPage().getParameters().get('PRval') != NULL) 
        {
            PR= ApexPages.currentPage().getParameters().get('PRval');
        } 
        if (ApexPages.currentPage().getParameters().get('Geoval') != NULL) 
        {
        Geo= ApexPages.currentPage().getParameters().get('Geoval');
        } 
        
        if (ApexPages.currentPage().getParameters().get('SIVval') != NULL) 
        {
        SIV = ApexPages.currentPage().getParameters().get('SIVval');
        } 
        if (ApexPages.currentPage().getParameters().get('extype') != NULL) 
        {
        exptype= ApexPages.currentPage().getParameters().get('extype');
        } 
        
        PF=SPlitingKey(PF);
        SL=SPlitingKey(SL);
        PR=SPlitingKey(PR);
        
       soql = 'select id,name,Expert_Type__c,Product__c,Industrial_Vertical__c,Product_Family__c,Service_Line__c,Geo_Served__c,Sub_Industry_Vertical__c from Expert__c where  (Expert_Type__c=\'External\') AND(Industrial_Vertical__c INCLUDES (\''+ IV +'\')  OR Service_Line__c INCLUDES('+ SL +') OR Product_Family__c INCLUDES('+ PF +') OR Product__c INCLUDES('+ PR +'))';
     System.debug('SLAfter'+ soql);
     runQuery();
  }
  public void toggleSort() {
    // simply toggle the direction 
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }

  
   public void runQuery() {
     
       System.Debug('SOQL: ' + soql);
      
    try{ 
          
       experts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' LIMIT 900');
       
       
       
  
    }catch(DmlException e){
        System.debug('The following exception has occurred: ' + e.getMessage());
    }

    
    }
     
 
  
  public PageReference reset() {
        PageReference pg = new PageReference(System.currentPageReference().getURL());
        pg.setRedirect(true);
        return pg;
    }

       public PageReference runSearch() {
       
      exp = new Expert__c();
      System.debug('Expert name ' + expertname);
    soql = 'select id,name,Expert_Type__c,Product__c,Industrial_Vertical__c,Product_Family__c,Service_Line__c,Geo_Served__c,Sub_Industry_Vertical__c from Expert__c where id!=null';
        
      if (!expertname.equals(''))
      soql += ' and Name LIKE '+ '\'%'+String.escapeSingleQuotes(expertname)+'%' + '\'';
  
     if (!industryverticalvar.equals('none'))
       soql += ' and Industrial_Vertical__c includes ('+ '\''+ industryverticalvar+ '\''+')';

         if (!serlinevar.equals('none'))
      soql += ' and Service_Line__c  includes ('+ '\''+ serlinevar+ '\''+')';
      
     if (!prodfamilyvar.equals('none'))
      soql += ' and Product_Family__c includes ('+ '\''+ prodfamilyvar+ '\''+')';
    
      if (!experttypevar.equals('none'))
      soql += ' and Expert_Type__c LIKE '+ '\'%'+String.escapeSingleQuotes(experttypevar)+'%' + '\'';
      
      if (!productvar.equals('none'))
      soql += ' and Product__c includes ('+ '\''+ productvar+ '\''+')';
    
      if (!geoservedvar.equals('none'))
      soql += ' and Geo_Served__c LIKE '+ '\'%'+String.escapeSingleQuotes(geoservedvar)+'%' + '\'';
      
      if (!subindustryverticalvar.equals('none'))
      soql += ' and Sub_Industry_Vertical__c LIKE '+ '\'%'+String.escapeSingleQuotes(subindustryverticalvar)+'%' + '\'';
    
        
      runQuery();

    return null;
  }

 public List<selectOption> serviceLine{
    get {
      if (serviceLine== null) {

        serviceLine= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Service_Line__c.getDescribe();
        serviceLine.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          serviceLine.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return serviceLine;          
    }
    set;
  }
  public List<selectOption> industryvertical {
    get {
      if (industryvertical == null) {

        industryvertical = new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Industrial_Vertical__c.getDescribe();
        industryvertical.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          industryvertical.add(new selectOption(f.getvalue(),f.getLabel()));

      }
      return industryvertical;          
    }
    set;
  }
  
  public List<selectOption> prodfamily{
    get {
      if (prodfamily == null) {

        prodfamily = new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Product_Family__c.getDescribe();
        prodfamily.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          prodfamily.add(new selectOption(f.getvalue(),f.getlabel()));

      }
      return prodfamily;          
    }
    set;
  }
  public List<selectOption> expertType{
    get {
      if (expertType== null) {

        expertType= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Expert_Type__c.getDescribe();
        expertType.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues()) {
          expertType.add(new selectOption(f.getValue(),f.getLabel()));

      }
      }
      return expertType;          
    }
    set;
  } 
  
   
   public List<selectOption> product{
    get {
      if (product== null) {

        product= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Product__c.getDescribe();
        product.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          product.add(new selectOption(f.getvalue(),f.getLabel()));

      }
      return product;          
    }
    set;
  }
  public List<selectOption> geoserved{
    get {
      if (geoserved== null) {

        geoserved= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Geo_Served__c.getDescribe();
        geoserved.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          geoserved.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return geoserved;          
    }
    set;
    }
    
    public List<selectOption> subindustryvertical{
    get {
      if(subindustryvertical== null) {

        subindustryvertical= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Sub_Industry_Vertical__c.getDescribe();
        subindustryvertical.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          subindustryvertical.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return subindustryvertical;          
    }
    set;
    }
  
  
  Public string SPlitingKey (String key)
{
            String strng='';       
                if(key!= ''){       
                    List<String> Splitstr = key.split(',');
                        for(String str : Splitstr)
                        {
                                String c;
                                
                          c = '\'' + str + '\'' + ',';
                                strng = strng +c;
                        
                        }
                        
                        
                strng=strng.substring(0,(strng.length()-1));
                
                
                }
                else
                {
                    strng='\'None\'';
                }
                System.debug('Where clause' + strng); 
                return strng;
                
                
}   
  
  
  }