@isTest
public class GPCmpServiceProjectLeadershipTracker {
    public static GP_Leadership_Master__c leadershipMaster;
    public static GP_Project__c project;
    public static GP_Project__c parentProject;
    public static GP_Project_Leadership__c projectLeadership;
    
	@testSetup
    public static void buildDependencyData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser(); 
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Project_Template__c objprjtemp = new GP_Project_Template__c();
        objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;

        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project__c parentPrjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentPrjObj.OwnerId = objuser.Id;
        parentPrjObj.GP_Approval_Status__c = 'Approved';
        insert parentPrjObj ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',null,objuser,objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Parent_Project__c = parentPrjObj.Id;
        prjObj.GP_PID_Update_Role__c = objrole.Id;
        prjObj.GP_PID_Creator_Role__c = objrole.Id;
        insert prjObj ;
        
        Business_Segments__c objBS = GPCommonTracker.getBs();
        insert objBS;
        
        Sub_Business__c  objSB = GPCommonTracker.getSB(objBS.Id);
        insert objSB;
        
        Account accountObj = GPCommonTracker.getAccount(objBS.Id,objSB.Id);
        insert accountObj;
        
     	leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Employee_Master__c = empObj.Id;
        leadershipMaster.GP_Mandatory_Service_Line__c = 'service line';
        leadershipMaster.GP_Leadership_Role__c = 'HSL Delivery Head';
        leadershipMaster.RecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        leadershipMaster.GP_Category__c = 'Category';
        insert leadershipMaster;
        
        GP_Leadership_Master__c leadershipMaster1 = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster1.GP_Employee_Master__c = empObj.Id;
        leadershipMaster1.GP_Mandatory_Service_Line__c = 'service line';
        leadershipMaster1.GP_Leadership_Role__c = 'HSL Delivery Head';
        leadershipMaster1.RecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Mandatory Key Members');
        leadershipMaster1.GP_Category__c = 'Category';
        insert leadershipMaster1;
        
        projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        projectLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
        projectLeadership.GP_Active__c = true;
        projectLeadership.GP_Employee_ID__c = empObj.Id;
        insert projectLeadership;
    }
    
    @isTest
    public static void testGPCmpServiceProjectLeadership() {
        fetchData();
        testGetLeadershipData();
        testGetLeadershipDataWithoutProjectId();
        testSaveLeadershipDataWithoutPreviousVersion();
        testSaveLeadershipDataWithoutRecords();
        testdeleteLeadershipData();
        testGPControllerProjectLeadership();
        testGPControllerProjectLeadershipRecrdType();
        testGPControllerProjectLeadershipRecrdType2();
        testGPControllerProjectLeadershipRecrdType3();
        testGPControllerProjectLeadershipRecrdType4();
        
        testgetMapOfRoleToProjectLeadership();
    }
    
    public static void fetchData() {
        project = [Select Id from GP_Project__c where GP_Parent_Project__c = null limit 1];
        parentProject = [Select Id from GP_Project__c where GP_Parent_Project__c != null limit 1];
        projectLeadership = [Select Id, GP_Project__c, GP_Leadership_Role_Name__c from GP_Project_Leadership__c limit 1];
    }
    public static void testGetLeadershipData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.getLeadershipData(project.Id);
         //System.assertEquals(false, returnedResponse.isSuccess);
    }
    public static void testGetLeadershipDataWithoutProjectId() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.getLeadershipData(null);
        //System.assertEquals(false, returnedResponse.isSuccess);
    }
    public static void testSaveLeadershipDataWithoutPreviousVersion() {
		
        List<GP_Project_Leadership__c> listOfProjectLeadership = new List<GP_Project_Leadership__c> {projectLeadership};
            
        String serializedlistOfProjectLeadership =  (String)JSON.serialize(listOfProjectLeadership);  
        GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.saveLeadershipData(serializedlistOfProjectLeadership, project.Id);
        //System.debug('returnedResponse: '+returnedResponse);
        //System.assertEquals(false, returnedResponse.isSuccess);
    }    
    public static void testSaveLeadershipDataWithoutRecords() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.saveLeadershipData(null, null);
        //System.debug('returnedResponse:' + returnedResponse);
      // System.assertEquals(true, returnedResponse.isSuccess);
      //  System.assertEquals('SUCCESS_LABEL', returnedResponse.message);
    }
    
    
    public static void testdeleteLeadershipData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.deleteLeadershipData(projectLeadership.id, project.Id);
        returnedResponse = GPCmpServiceProjectLeadership.deleteLeadershipData(projectLeadership.id, null);
        //System.debug('returnedResponse:' + returnedResponse);
        //System.assertEquals(false, returnedResponse.isSuccess);
    }
      public static void testdeleteLeadershipDataForException() {
        //GPAuraResponse returnedResponse = GPCmpServiceProjectLeadership.deleteLeadershipData('Test');
        //System.debug('returnedResponse:' + returnedResponse);
       // System.assertEquals(, returnedResponse);
    }
    
     public static void testGPControllerProjectLeadership() {
         GPControllerProjectLeadership ProjectLeadership  =  new GPControllerProjectLeadership(parentProject.Id);
        ProjectLeadership.setLeadershipData();
        //System.debug('returnedResponse:' + returnedResponse);
       // System.assertEquals(, returnedResponse);
     }
    public static void testGPControllerProjectLeadershipRecrdType() {
        GPControllerProjectLeadership ProjectLeadershipClass =  new GPControllerProjectLeadership(parentProject.Id);
        ProjectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        ProjectLeadershipClass.setLeadershipData();
        //System.debug('returnedResponse:' + returnedResponse);
        // System.assertEquals(, returnedResponse);
    }
    public static void testGPControllerProjectLeadershipRecrdType2() {
         GPControllerProjectLeadership ProjectLeadershipClass =  new GPControllerProjectLeadership(parentProject.Id);
         ProjectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Regional Leadership').getRecordTypeId();
        ProjectLeadershipClass.setLeadershipData();
        //System.debug('returnedResponse:' + returnedResponse);
       // System.assertEquals(, returnedResponse);
    }
    public static void testGPControllerProjectLeadershipRecrdType3() {
         GPControllerProjectLeadership ProjectLeadershipClass =  new GPControllerProjectLeadership(parentProject.Id);
        ProjectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Account Leadership').getRecordTypeId();
        ProjectLeadershipClass.setLeadershipData();
        //System.debug('returnedResponse:' + returnedResponse);
       // System.assertEquals(, returnedResponse);
    }
    public static void testGPControllerProjectLeadershipRecrdType4() {
         GPControllerProjectLeadership ProjectLeadershipClass =  new GPControllerProjectLeadership(parentProject.Id);
         ProjectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('HSL Leadership').getRecordTypeId();
        ProjectLeadershipClass.setLeadershipData();
        //System.debug('returnedResponse:' + returnedResponse);
       // System.assertEquals(, returnedResponse);
    }
    static void testgetMapOfRoleToProjectLeadership() {
        GPControllerProjectLeadership ProjectLeadershipClass =  new GPControllerProjectLeadership();
        ProjectLeadershipClass.getMapOfRoleToProjectLeadership(new List<GP_Project_leadership__c> {projectLeadership});
    }
}