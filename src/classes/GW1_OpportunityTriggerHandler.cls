// Class is handler class of DSR trigger 
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1        03-10-2016       Rishi Kumar              
// ---------------------------------------------------------------------------------------------



public class GW1_OpportunityTriggerHandler {

	static Boolean mailSentToTowerLead = false;
	public void runTrigger()
	{
		if (trigger.isAfter && trigger.isUpdate)
		{
			onAfterUpdate((list<Opportunity>) trigger.new, (Map<Id, Opportunity>) trigger.OldMap);
		}
        if(trigger.isbefore && trigger.isInsert)
        {
            onBeforeInsert((list<Opportunity>) trigger.new);
        }
	}

   private void onBeforeInsert(list<Opportunity> triggerNew)
   {
       for(Opportunity objopportunity: triggerNew)
       {
          if (!Test.isRunningTest())
        	{
               objopportunity.GW1_DSR_Created__c=false;
               objopportunity.GW1_DSR_Created_Manually__c=false;
               objopportunity.GW1_Dont_Create_DSR__c=false;
               objopportunity.GW1_Include_in_pilot__c=false;
        	}
       }
   }
	private void onAfterUpdate(list<Opportunity> triggerNew, Map<Id, Opportunity> triggerOldMap)
	{
		list<Opportunity> lstOpportunity = new list<Opportunity> ();
		set<id> setOpportunity_Approved = new set<Id>();

		Integer intThresHold = Integer.valueOf(Label.Conslting_DSR_Threshhold);
		if (triggerNew != null && triggerNew.size() > 0)
		{
			for (Opportunity objOpportunity : triggerNew)
			{
				if (objOpportunity.GW1_Dont_Create_DSR__c == false 
					&& objOpportunity.GW1_DSR_Created__c == false
				    && objOpportunity.Approved__c == true 
					&& triggerOldMap != null
                    && objOpportunity.GW1_Include_in_pilot__c==true
				    && triggerOldMap.get(objOpportunity.Id).Approved__c == false)
				{
					lstOpportunity.add(objOpportunity);
				}
				else
				{
					if(objOpportunity.Approved__c == true && triggerOldMap.get(objOpportunity.Id).Approved__c == false && objOpportunity.GW1_DSR_Created__c==true)
					{
						setOpportunity_Approved.add(objOpportunity.id);
					}
				}
			}

			
			try
			{
				if (lstOpportunity.size() > 0)
				   CreateDSRHandler(lstOpportunity);
				
				if(setOpportunity_Approved!=null && setOpportunity_Approved.size()>0)
				{
					updateSubmissionDateOnDSR(setOpportunity_Approved);
				}
			}
			catch(exception e)
			{
				lstOpportunity[0].addError('No delivery product found or consulting product(s) with opportunity value is below ' + intThresHold);
			}

		}
	}



	/*
	  This function is called from page CreateDSR and trigger to create DSR
	  -------------------------------------------------------------------------------------- 
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          03-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
	 */

   
   private void updateSubmissionDateOnDSR( set<id> setOpportunity_Approved) 
   {
   		list<GW1_DSR__c> lstDSRToUpdate = new list<GW1_DSR__c>();
   		if(setOpportunity_Approved!=null && setOpportunity_Approved.size()>0)
   		{
   		    list<Opportunity>	lstopportunity= [ select id ,(Select GW1_Date_of_Submission__c from DSRs__r ) ,(Select Due_date_for_bid_submission__c From QSRMs__r where Status__c='Approved' order by LastModifiedDate limit 1) from opportunity where id in:setOpportunity_Approved];
   			
   			
   			for(opportunity objOpportunity:lstopportunity)
   			{
   				 list<GW1_DSR__c> lstDsr = objOpportunity.DSRs__r;
   				 for(GW1_DSR__c objDsr:lstDsr )
   				 {
   				 	 if(objOpportunity.QSRMs__r.size()>0 )
   				 	 {
   				 	 	if(objOpportunity.QSRMs__r[0].Due_date_for_bid_submission__c!=null)
                        {
                         objDsr.GW1_Date_of_Submission__c=objOpportunity.QSRMs__r[0].Due_date_for_bid_submission__c;
   				 	 		lstDSRToUpdate.add(objDsr);
                        }
   				 	 }
   				 }
   			}
   		
   		}
   		if(lstDSRToUpdate!=null && lstDSRToUpdate.size()>0)
   		{
   			update lstDSRToUpdate;
   		}
   		
   }
 
	public List<GW1_DSR__c> CreateDSRHandler(list<Opportunity> lstOpportunity)
	{

		List<Messaging.SingleEmailMessage> mails2Send = new List<Messaging.SingleEmailMessage> ();
		Set<Id> setOfOpportunityId = new Set<Id> ();
		list<GW1_DSR__c> lstDSRToInsert = new List<GW1_DSR__c> ();
		Set<String> TLEmailId = new Set<String> ();
		map<string, string> mapTowerLead = new map<string, string> ();
		map<string, set<ID>> mapTowerLeadToID = new map<string, set<ID>> ();
		Integer intThresHold = Integer.valueOf(Label.Conslting_DSR_Threshhold);
		String dsrQueueID = String.valueOf(Label.GW1_Tower_Leader_Queue_Id);
        string strProductFamilyName = String.valueOf(Label.GW1_Product_Family);
		list<Opportunity> lstOpportunityToUpdate = new list<Opportunity> ();

		if (lstOpportunity != null && lstOpportunity.size() > 0)
		{
			for (Opportunity objOpportunity : lstOpportunity)
			{
				
                setOfOpportunityId.add(objOpportunity.id);
			}
		}
		Map<Id, opportunity> MapIDToOpp = new Map<Id, opportunity> ();
		if (setOfOpportunityId.size() > 0)
		{
			//Get opportuntiy with product
			MapIDToOpp = new Map<Id, opportunity> ([Select id,Opportunity_ID__c, name, Account.name, Sub_Industry_Vertical__c, Industry_Vertical__c, Service_Line__c,
			                                       Competitor__c, TCV1__c, GW1_DSR_Created__c,
			(Select name, GW1_Product_Family_Name__c,Product_family_Lookup__r.name, Industry_Vertical_temp__c, Nature_Of_Work_Lookup__r.name,
			 Opportunityid__r.Account.name
			 from Opportunity_Products__r),(Select Due_date_for_bid_submission__c From QSRMs__r where Status__c='Approved' order by LastModifiedDate limit 1)
			                                       from Opportunity where id = :setOfOpportunityId]);

			//Get Tower
			list<GW1_Tower_Leader_Mapping__c> lstTowerLeader = [select Id, GW1_Distribution_List__c, GW1_Industry_Vertical__c,
			                                                    GW1_Product_Family__c
			                                                    from GW1_Tower_Leader_Mapping__c limit 999];
			// Mapping all tower lead
			// Mapping is on the basis of vertical & product family 
			if (lstTowerLeader != null && lstTowerLeader.size() > 0)
			{
				for (GW1_Tower_Leader_Mapping__c objTower : lstTowerLeader)
				{
					string key = '';
					if (objTower.GW1_Product_Family__c != null && objTower.GW1_Product_Family__c != '')
					{
						key = objTower.GW1_Product_Family__c.toLowercase();
					}
					if (objTower.GW1_Industry_Vertical__c != null && objTower.GW1_Industry_Vertical__c != '')
					{
						key += objTower.GW1_Industry_Vertical__c.toLowercase();
					}
					System.debug('key::' + key);
					if (mapTowerLead.get(key) == null) // if dosent exist already
					{
						mapTowerLead.put(key, objTower.GW1_Distribution_List__c);
					}
				}
			} //End Mapping

			// looping on all opportunity we got on trigger 
			// for each opportunity we will create a DSR
			for (Opportunity objOpportunity : lstOpportunity)
			{

				integer countOfProduct = 0;
				integer countDeliveryTypeProduct = 0;
				integer countConsultingTypeProduct = 0;

				List<OpportunityProduct__c> lstOpportunityProduct = MapIDToOpp.get(objOpportunity.id).Opportunity_Products__r;
				if (lstOpportunityProduct != null && lstOpportunityProduct.size() > 0)
				{
					//we are now looping on each product of a opportunity
					for (OpportunityProduct__c ObjOppProduct : lstOpportunityProduct)
					{
						if (ObjOppProduct.Nature_Of_Work_Lookup__r.Name != null)
						{
							if (ObjOppProduct.Nature_Of_Work_Lookup__r.Name == 'Delivery')
							{
								countDeliveryTypeProduct++;
							}
							if (ObjOppProduct.Nature_Of_Work_Lookup__r.Name == 'Consulting')
							{
								countConsultingTypeProduct++;
							}
							string productKey = '';
							string key = '';
							if (ObjOppProduct.Product_family_Lookup__c != null && ObjOppProduct.Product_family_Lookup__r.name != '')
							{
								productKey = ObjOppProduct.Product_family_Lookup__r.name.toLowercase();
							}
							if (ObjOppProduct.Industry_Vertical_temp__c!= null &&  ObjOppProduct.Industry_Vertical_temp__c != '' && ObjOppProduct.Product_family_Lookup__r.name.equalsIgnoreCase(strProductFamilyName))
							{
								productKey += ObjOppProduct.Industry_Vertical_temp__c.ToLowercase();
							}
							System.debug('@@productKey::' + productKey);
							if (mapTowerLead.get(productKey) != null)
							{
								TLEmailId.add(mapTowerLead.get(productKey));
							}
						}

					} // END OppProduct Looping

					//Now we are in opportunity loop for each opportunity we will create a dsr
					// DSR will be created only if we have some deleverytype product or if we have only consulting product then opp value must be greater than specified threshold value
					if (countDeliveryTypeProduct > 0 || (countDeliveryTypeProduct == 0 && countConsultingTypeProduct > 0 && objOpportunity.TCV1__c > intThresHold))
					{
						GW1_DSR__c objDSR = new GW1_DSR__c();
						objDSR.GW1_Opportunity__c = objOpportunity.id;
						//objDSR.Name = MapIDToOpp.get(objOpportunity.Id).Account.name.substring(0, 3) + '-';
						//
						
                        if(objOpportunity.name!='' && objOpportunity.name!=null)
                        {
                            string dsrname=objOpportunity.name +'-'+MapIDToOpp.get(objOpportunity.Id).Opportunity_ID__c;
                            if( dsrname.length()>80)
                            {
                                objDSR.Name = objOpportunity.name.substring(0,69) +'-'+MapIDToOpp.get(objOpportunity.Id).Opportunity_ID__c;
                            }
                            else
                        	{
                             objDSR.Name = dsrname;
 
                        	}
                            
                        }
                        
						
						//objDSR.Name += objOpportunity.name.substring(0, 3) + '-';
						//objDSR.Name += System.now().format('MMM') + '-' + System.now().format('YY');
						objDSR.GW1_Service_Line_SAVO__c = objOpportunity.Service_Line__c;
						objDSR.GW1_Named_Competitors__c = objOpportunity.Competitor__c;
						
						if( MapIDToOpp.get(objOpportunity.id).QSRMs__r.size()>0 )
						{
							if(MapIDToOpp.get(objOpportunity.id).QSRMs__r[0].Due_date_for_bid_submission__c!=null)
                            	objDSR.GW1_Date_of_Submission__c=MapIDToOpp.get(objOpportunity.id).QSRMs__r[0].Due_date_for_bid_submission__c;
						}
						
						objDSR.GW1_Account__c = objOpportunity.AccountId;
						objDSR.ownerid = dsrQueueID; //DSR owner will be a Queue
						lstDSRToInsert.add(objDSR); // adding the dsr object to a list to insert later

						//update DSR Created Field to true after creating dsr;
						if (MapIDToOpp.get(objOpportunity.Id) != null)
						{
							Opportunity objOpp = MapIDToOpp.get(objOpportunity.Id);
							objOpp.GW1_DSR_Created__c = true;
							lstOpportunityToUpdate.add(objOpp);
						}
					}
				}

			} //END looping on all opportunity we got on trigger 
		}

		if (lstDSRToInsert != null && lstDSRToInsert.size() > 0 && lstOpportunityToUpdate.size() > 0)
		{
			try
			{
				update lstOpportunityToUpdate;
				insert lstDSRToInsert;
			}
			catch(exception e)
			{
				throw new GW1_DSRException(e.getMessage());
			}
		}
		else
		{
			throw new GW1_DSRException('No delivery product found or consulting product(s) with opportunity value is below ' + intThresHold);
			// this will be caught in GW1_CreateDSRCreateController 
		}


		// Updating each product with inserted DSR id in GW1_DSR__c field

		List<OpportunityProduct__c> listOppProductToUpdate = new List<OpportunityProduct__c> ();
		for (GW1_DSR__c objDSR : lstDSRToInsert)
		{
			for (OpportunityProduct__c objProduct : MapIDToOpp.get(objDSR.GW1_Opportunity__c).Opportunity_Products__r)
			{
				objProduct.GW1_DSR__c = objDSR.id;
				listOppProductToUpdate.add(objProduct);
			}

		}

		if (listOppProductToUpdate != null && listOppProductToUpdate.size() > 0)
		{
			update listOppProductToUpdate;
		}
		if (lstDSRToInsert.size() > 0 && lstOpportunity.size() > 0) //we will not send mail if it is a bulk trigger case
		{
			sendMail(TLEmailId, lstDSRToInsert);
		}

		return lstDSRToInsert;
	}

	/*
	  This function is called from CreateDSRHandler to Send Emails 
	  @pram set<id> setOfUserId-ids of user to send emails to
	  @pram GW1_DSR__c objDSR - Related DSR object which will be used to Send Email 
	  -------------------------------------------------------------------------------------- 
	  Name 								Date 								Version	                    
	  --------------------------------------------------------------------------------------
	  Rishi					          03-10-2016						      1.0	
	  --------------------------------------------------------------------------------------
	 */




	private void sendMail(set<String> TLEmailId, List< GW1_DSR__c> lstDSR)
	{
		
        mailSentToTowerLead=true;
        List<Messaging.SingleEmailMessage> mails2Send = new List<Messaging.SingleEmailMessage> ();
        for(GW1_DSR__c  objDSR: lstDSR)	
        {
            Messaging.SingleEmailMessage EachMail = new Messaging.SingleEmailMessage();
            list<string> ToAdds = new list<string> ();
            ToAdds.addAll(TLEmailId);
           
            //EachMail.setOrgWideEmailAddressId('0D2O000000002JF');
            EachMail.setToAddresses(ToAdds);
            EachMail.setSenderDisplayName('SFDC Helpdesk');
            string strSubject = 'You\'ve been made the tower lead for ' + objDSR.name;
            EachMail.setSubject(strSubject);
            String body = System.Label.Email_template_for_Tower_leader_Assignment;
            if (body.contains('$DSR_Name'))
                body = body.replace('$DSR_Name', objDSR.name);
            if (body.contains('$Baseurl'))
            {
                string Baseurl = URL.getSalesforceBaseUrl().toExternalForm();
                body = body.replace('$Baseurl', Baseurl);
            }
            if (body.contains('$DSR_Link'))
                body = body.replace('$DSR_Link', objDSR.id);
            EachMail.setHtmlBody(body);
            
            mails2Send.add(EachMail);
        }
        try
        {
            system.debug('@mails2Send________' + mails2Send);
            if (mails2Send != null)
                Messaging.sendEmail(mails2Send);
        }
        catch(exception e)
        {
            System.debug('exception in mail sending' + e);
        }
		
	}


}