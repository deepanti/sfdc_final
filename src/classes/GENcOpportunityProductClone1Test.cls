@isTest(SeeAllData=true)
private class GENcOpportunityProductClone1Test { 

    public static testMethod void myUnitTest() {
    test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='standard user']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test111',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
    //    Product_Catalog__c prodcat=new Product_Catalog__c();
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,12,12000);
        RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,13,1,12000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
       
        //me--------------------
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunityProduct);
        GENcOpportunityProductClone1  oGENcOpportunityProduct  = new GENcOpportunityProductClone1(std);
        //me------------------------
        GENcOpportunityProductClone1.ProductScheduleWrapper oProductScheduleWrapper = new GENcOpportunityProductClone1.ProductScheduleWrapper(oProductSchedule,0);
        GENcOpportunityProductClone1.RevenueRollingYearWrapper oRevenueRollingYearWrapper  = new GENcOpportunityProductClone1.RevenueRollingYearWrapper(0);
        //me-----------------------------------------------------
        OpportunityProduct__c opportunityProduct=new OpportunityProduct__c();
        OpportunityProduct__c oppro=[Select  Name,LastModifiedById,Product__r.Name,ExchangeRate__c,OpportunityId__r.StageName,OpportunityId__r.Opportunity_ID__c,OpportunityId__r.Ownerid,OpportunityId__r.Id,OpportunityId__r.AccountId,ACV__c,COE__c,ContractTerminmonths__c,CreatedById,CurrentYearRevenue__c,DeliveryLocation__c,Description__c,NextYearRevenue__c,OpportunityId__c,Product__c,Quantity__c,
                                      Service_Line_Category_OLI__c,Service_Line_OLI__c,Product_Family_OLI__c,RevenueStartDate__c,FTE__c,   EndDate__c,Total_ACV__c,Product_Autonumber__c,SalesExecutive__c,P_L_SUB_BUSINESS__c,SEP__c,TotalContractValue__c,Quarterly_FTE_1st_month__c,FTE_4th_month__c,FTE_7th_month__c,FTE_10th_month__c,TotalPrice__c,
                                      Replicate_Product__c,UnitPrice__c, LocalCurrency__c, OpportunityId__r.Industry_Vertical__c,TCVLocal__c  from OpportunityProduct__c  where id=:oOpportunityProduct.id];
       
          //  opportunityProduct.Name = oppro.Product__r.Name;
                opportunityProduct.Opportunityid__c=oppro.Opportunityid__c;

                
                
                opportunityProduct.COE__c = oppro.COE__c;
                opportunityProduct.P_L_SUB_BUSINESS__c = oppro.P_L_SUB_BUSINESS__c;
                opportunityProduct.LocalCurrency__c = oppro.LocalCurrency__c;
                opportunityProduct.DeliveryLocation__c= oppro.DeliveryLocation__c;
                opportunityProduct.FTE__c= oppro.FTE__c;

        
        //end me------------------
        
        //oGENcOpportunityProduct.getProductShedule();
     //   oGENcOpportunityProduct.getHasAutoPopulateDisabled();
     //   oGENcOpportunityProduct.setHasAutoPopulateDisabled(true);
        //oGENcEditOpportunityProduct.getServiceOptions();
        //oGENcEditOpportunityProduct.addServiceLine();
    //    oGENcOpportunityProduct.getRevenueSchedule(oRevenueSchedule);

       // GENcOpportunityProductClone1.revenueRollingYearToEdit =1;
       // oGENcOpportunityProduct.ProfilesOptionsFetch();
        oGENcOpportunityProduct.getCurrencyIsoCodes();
        oGENcOpportunityProduct.showConvertedValue();
        oGENcOpportunityProduct.convertCurrency();
        oGENcOpportunityProduct.updateSchedule();
        oGENcOpportunityProduct.deleteProductSchedule();
        oGENcOpportunityProduct.validate();
        //oGENcEditOpportunityProduct.getTransitionRevenueUSD();
      // pageReference pg = oGENcOpportunityProduct.saveAndNewLineItem ();
        
      //  oGENcOpportunityProduct.convertUSD(33.12);
      //  oGENcOpportunityProduct.deleteExistingSchedule();
       // oGENcOpportunityProduct.getRevenueRollingYear();
        //oGENcOpportunityProduct.getNoOfInstallment();
       // GENcOpportunityProductClone1.revenueRollingYearIndex = 0;
       // oGENcOpportunityProduct.createSchedule();
        oGENcOpportunityProduct.revenuedate_update();
        oGENcOpportunityProduct.getRevenue(123.32,2);
        //oGENcEditOpportunityProduct.getUnitPrice(); 
        oGENcOpportunityProduct.getSkillSetRendered();
        oGENcOpportunityProduct.setSkillSetRendered(true);
        oGENcOpportunityProduct.saveLineItem();
        oGENcOpportunityProduct.getRollingYearRevenueUSD(oRevenueRollingYearWrapper);
       //end me---------------------------------------------------------------------------------
        test.stoptest();
    
        
    }
   
    public static testMethod void RunTest2()
    {
        test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='standard user']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Pricebook2 oPriceBook = GEN_Util_Test_Data.CreatePricebook2();
        PricebookEntry oPriceBookEntry = GEN_Util_Test_Data.CreatePriceBookEntry(oProduct.Id,oPriceBook.Id);
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test121',oAccount.Id,oContact.Id);
        
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today();
        oOpportunityProduct.ContractTerminmonths__c = 18;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity.Id;
        oOpportunityProduct.TCVLocal__c =18000;
       // oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c=true;
        oOpportunityProduct.HasSchedule__c=true;
        oOpportunityProduct.HasQuantitySchedule__c=false;
        insert oOpportunityProduct;
        
        
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunityProduct);
        GENcOpportunityProductClone1  oGENcOpportunityProduct  = new GENcOpportunityProductClone1(std);
        ApexPages.currentpage().getParameters().put('id',oOpportunityProduct.id);
        oGENcOpportunityProduct.getNoOfInstallment();
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,6,12000);
        RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,6,1,6000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
        
        oGENcOpportunityProduct.theOpportunity = new Opportunity();
        oGENcOpportunityProduct.theOpportunity = oOpportunity;
        
        GENcOpportunityProductClone1.RevenueRollingYearWrapper  oRevenueRollingYearWrapper1  = new GENcOpportunityProductClone1.RevenueRollingYearWrapper(1);
        oRevenueRollingYearWrapper1.revSchedule = oRevenueSchedule;
        GENcOpportunityProductClone1.ProductScheduleWrapper oProductScheduleWrapper= new GENcOpportunityProductClone1.ProductScheduleWrapper(null,1);
        oGENcOpportunityProduct.getRevenueRollingYear();
        oRevenueRollingYearWrapper1.rollYear= 12000;
        GENcOpportunityProductClone1.RevenueRollingYearWrapper  oRevenueRollingYearWrapper2  = new GENcOpportunityProductClone1.RevenueRollingYearWrapper(2);
        oRevenueRollingYearWrapper2.revSchedule = oRevenueSchedule1;
        oRevenueRollingYearWrapper2.rollYear= 6000;
       // oGENcOpportunityProduct.revenueRYWrapperList.add(oRevenueRollingYearWrapper1);
        //oGENcOpportunityProduct.revenueRYWrapperList.add(oRevenueRollingYearWrapper2);
        oRevenueRollingYearWrapper1.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper1.setHasAutoPopulateDisabled(true);
        oRevenueRollingYearWrapper2.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper2.setHasAutoPopulateDisabled(true);
        //oGENcOpportunityProduct.getUnitPrice();
        oGENcOpportunityProduct.ProdObj = new product2();
        oGENcOpportunityProduct.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct.ProdObj.Service_Line__c = '33';
        //oGENcOpportunityProduct.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct.ProdObj.Product_Family__c='IT Managed Services';
        //oGENcOpportunityProduct.disableProduct();
       // oGENcOpportunityProduct.ProfilesOptionsFetch();
        oGENcOpportunityProduct.getautoNumber();
        //oGENcOpportunityProduct.getServiceOptions();
        oGENcOpportunityProduct.getCurrencyIsoCodes();
        oGENcOpportunityProduct.showConvertedValue();
        oGENcOpportunityProduct.convertCurrency();
        oGENcOpportunityProduct.updateSchedule();
        oGENcOpportunityProduct.revenueRollingYearToDelete =1;
        oGENcOpportunityProduct.deleteProductSchedule();
        oGENcOpportunityProduct.revenueRollingYearIndex=1;
        //oGENcOpportunityProduct.createSchedule();
        oGENcOpportunityProduct.revenuedate_update();
        //oGENcOpportunityProduct.getTransitionRevenueUSD();
        oGENcOpportunityProduct.getHasScheculeSectionRendered();
        oGENcOpportunityProduct.setHasScheculeSectionRendered(true);
        oGENcOpportunityProduct.getRevenue(12.00,13.00);
        oGENcOpportunityProduct.getSkillSetRendered();
        oGENcOpportunityProduct.setSkillSetRendered(true);
        //oGENcOpportunityProduct.getPriorityTypeRendered();
        //oGENcOpportunityProduct.setPriorityTypeRendered(true);
        test.stoptest();
    }
   public static testMethod void RunTest3()
    {    
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Profile p = [SELECT Id FROM Profile WHERE Name='standard user']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test1011',oAccount.Id,oContact.Id);
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,24,24000);
        OpportunityTeamMember oOpportunityTeamMember= GEN_Util_Test_Data.CreateOpportunityTeamMember(oOpportunity.id,Userinfo.getUserId());
        ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunityProduct);
        GENcOpportunityProductClone1  oGENcOpportunityProduct1  = new GENcOpportunityProductClone1(std1);
        test.starttest();
        oGENcOpportunityProduct1.theOpportunity = new Opportunity();
         oGENcOpportunityProduct1.opportunityProduct.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct1.theOpportunity = oOpportunity;
        oGENcOpportunityProduct1.ProdObj = new product2();
        oGENcOpportunityProduct1.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct1.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct1.ProdObj.Service_Line__c = '33';
        //oGENcOpportunityProduct1.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct1.ProdObj.Product_Family__c='IT Managed Services';
        //oGENcOpportunityProduct1.disableProduct();
       // oGENcOpportunityProduct1.ProfilesOptionsFetch();
        oGENcOpportunityProduct1.getBDRepOptions();
        oGENcOpportunityProduct1.getCurrencyIsoCodes();
        oGENcOpportunityProduct1.opportunityProduct.LocalCurrency__c='INR';
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        oGENcOpportunityProduct1.opportunityProduct.RevenueStartDate__c = System.today().adddays(20);
        oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c= 18000;
        oGENcOpportunityProduct1.showConvertedValue();
        oGENcOpportunityProduct1.opportunityProduct.ContractTerminmonths__c = 12;
       // oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 18000;
       // oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToEdit=1;
        oGENcOpportunityProduct1.updateSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToDelete=1;
        oGENcOpportunityProduct1.deleteProductSchedule();

        //oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
        //oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 18000;
        //oGENcOpportunityProduct1.createSchedule();
       // oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.opportunityProduct.TransitionRevenueLocal__c=5000;
        oGENcOpportunityProduct1.totalScheduleValue = math.round(oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c);
        oGENcOpportunityProduct1.saveLineItem();
        oGENcOpportunityProduct1.createRevProdList(oGENcOpportunityProduct1.revenueRYWrapperList);
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.saveAndNewLineItem();
       // oGENcOpportunityProduct1.saveandnew();
        oGENcOpportunityProduct1.addServiceLine();
        oGENcOpportunityProduct1.revenuedate_update();
        test.stoptest();
    } 
   public static testMethod void RunTest4()
    {    
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Profile p = [SELECT Id FROM Profile WHERE Name='standard user']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test11100',oAccount.Id,oContact.Id);
        ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunity);
        
        GENcOpportunityProductClone1  oGENcOpportunityProduct1  = new GENcOpportunityProductClone1(std1);
        oGENcOpportunityProduct1.theOpportunity = new Opportunity();
        oGENcOpportunityProduct1.theOpportunity = oOpportunity ;
        oGENcOpportunityProduct1.ProdObj = new product2();
        oGENcOpportunityProduct1.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct1.opportunityProduct.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct1.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct1.ProdObj.Service_Line__c = '33';
       // oGENcOpportunityProduct1.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct1.ProdObj.Product_Family__c='IT Managed Services';
        
        
        oGENcOpportunityProduct1.opportunityProduct.LocalCurrency__c='INR';
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        oGENcOpportunityProduct1.opportunityProduct.RevenueStartDate__c = System.today();
        oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c= 24000;
        oGENcOpportunityProduct1.opportunityProduct.ContractTerminmonths__c = 24;
      //  oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
        
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
      //  oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearIndex =1;
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
       // oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToEdit=1;
        oGENcOpportunityProduct1.updateSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToDelete=1;
        oGENcOpportunityProduct1.deleteProductSchedule();

        //oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
        //oGENcOpportunityProduct1.createSchedule();
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.opportunityProduct.TransitionRevenueLocal__c=5000;
        //oGENcOpportunityProduct1.getTransitionRevenueUSD();
        oGENcOpportunityProduct1.saveLineItem();
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.saveAndNewLineItem();
    }
    
         public static testMethod void RunTest5()
     {
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test1011',oAccount.Id,oContact.Id);
        Product_family__c PF = new Product_family__c(Name='Core Ops');
        Service_Line__c SL= New Service_Line__c(name='Supply MGMT');
        Nature_of_Work__c NOW= New Nature_of_Work__c(Name='Delivery');
        
        Product_family__c PF1 = new Product_family__c(Name='Core Ops');
        Service_Line__c SL1= New Service_Line__c(name='Supply MGMT');
        Nature_of_Work__c NOW1= New Nature_of_Work__c(Name='Delivery');
        
        Product_family__c PF2 = new Product_family__c(Name='Core Ops');
        Service_Line__c SL2= New Service_Line__c(name='Supply MGMT');
        Nature_of_Work__c NOW2= New Nature_of_Work__c(Name='Delivery2');
        Product_Catalogue1__c PC = new Product_Catalogue1__c(Industry_vertical__c = 'BFS',Nature_of_Work__c = NOW1.Id,Product_family1__c=PF1.Id,Service_Line1__c = SL1.Id,Product_new__c=oProduct.id);
        Product_Catalogue1__c PC2 = new Product_Catalogue1__c(Industry_vertical__c = 'BFS',Nature_of_Work__c = NOW2.Id,Product_family1__c=PF2.Id,Service_Line1__c = SL2.Id,Product_new__c=oProduct.id); 
        test.starttest();
        OpportunityTeamMember oOpportunityTeamMember= GEN_Util_Test_Data.CreateOpportunityTeamMember(oOpportunity.id,Userinfo.getUserId());
        //ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunity);
       // GENcOpportunityProductClone1  oGENcOpportunityProduct1  = new GENcOpportunityProductClone1(std1);
        
        ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunity);
        GENcOpportunityProduct  oGENcOpportunityProduct11  = new GENcOpportunityProduct(std1);
        //OpportunityProduct__c oGENcOpportunityProduct2 =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,24,24000);
        
        OpportunityProduct__c oGENcOpportunityProduct2 = new OpportunityProduct__c();
        oGENcOpportunityProduct2.Product_Family_OLI__c = 'Analytics';
        oGENcOpportunityProduct2.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct2.Product_family_Lookup__c =PF1.id;
        oGENcOpportunityProduct2.Service_Line_lookup__c= SL1.id;
        oGENcOpportunityProduct2.Nature_of_Work_lookup__c=NOW1.id;
        oGENcOpportunityProduct2.Product__c = oProduct.Id;
        oGENcOpportunityProduct2.COE__c = 'ANALYTICS';
        oGENcOpportunityProduct2.P_L_SUB_BUSINESS__c = 'Analytics';
        oGENcOpportunityProduct2.DeliveryLocation__c = 'Americas';
        oGENcOpportunityProduct2.SEP__c = 'SEP Opportunity';
        oGENcOpportunityProduct2.LocalCurrency__c = 'INR';
        oGENcOpportunityProduct2.RevenueStartDate__c = System.today();
        oGENcOpportunityProduct2.ContractTerminmonths__c = 18;
        oGENcOpportunityProduct2.SalesExecutive__c = UserInfo.getUserId ();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oGENcOpportunityProduct2.Quarterly_FTE_1st_month__c = 2;
        oGENcOpportunityProduct2.FTE_4th_month__c =2;
        oGENcOpportunityProduct2.FTE_7th_month__c =2;
        oGENcOpportunityProduct2.FTE_10th_month__c =2;
        oGENcOpportunityProduct2.OpportunityId__c = oOpportunity.Id;
        oGENcOpportunityProduct2.TCVLocal__c =18000;
       // oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oGENcOpportunityProduct2.HasRevenueSchedule__c=true;
        oGENcOpportunityProduct2.HasSchedule__c=true;
        oGENcOpportunityProduct2.HasQuantitySchedule__c=false;
        insert oGENcOpportunityProduct2;
        
        
        
        
        ApexPages.StandardController std2 = new ApexPages.StandardController(oGENcOpportunityProduct2);
        GENcOpportunityProductClone1 oGENcOpportunityProduct1  = new GENcOpportunityProductClone1(std2);

        
        oGENcOpportunityProduct1.theOpportunity = new Opportunity();
        oGENcOpportunityProduct1.theOpportunity = oOpportunity ;
        oGENcOpportunityProduct1.ProdObj = new product2();
        oGENcOpportunityProduct1.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct1.opportunityProduct.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct1.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct1.ProdObj.Service_Line__c = '33';
       // oGENcOpportunityProduct1.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct1.ProdObj.Product_Family__c='IT Managed Services';
        
        
        oGENcOpportunityProduct1.opportunityProduct.LocalCurrency__c='INR';
        
        oGENcOpportunityProduct1.opportunityProduct.RevenueStartDate__c = System.today() -2;
        oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c= 24000;
        oGENcOpportunityProduct1.opportunityProduct.ContractTerminmonths__c = 24;
        //oGENcOpportunityProduct2.Product_family_Lookup__c = PF1.id;
        //update oGENcOpportunityProduct2;
        //Product_family__c PFa = new Product_family__c(Name='Core Ops1',Product_Group__c='test');
        //insert PFa;
        //oGENcOpportunityProduct1.oppro.Product_family_Lookup__c=PFa.id;
        //update oGENcOpportunityProduct1.oppro;
        oGENcOpportunityProduct1.ProductFamily_frm_obj=oGENcOpportunityProduct1.oppro.Product_family_Lookup__c;
        
        //oGENcOpportunityProduct1.PFamily_lst();
        
        List<selectOption>ProductFamily_frm_obj_lst=oGENcOpportunityProduct1.ProductFamily_frm_obj_lst;
        //oGENcOpportunityProduct1.ServiceLine_lst();
        oGENcOpportunityProduct1.Serviceline_frm_obj=SL.id;
        List<selectOption>Serviceline_frm_obj_lst=oGENcOpportunityProduct1.Serviceline_frm_obj_lst;
        //oGENcOpportunityProduct1.ProductFamilyFromCatalog();
        oGENcOpportunityProduct1.SelectedProductFamily=NOW.id;
        List<selectOption>ProductFamilyLst=oGENcOpportunityProduct1.ProductFamilyLst;
        //oGENcOpportunityProduct1.ProfilesOptionsFetch();
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        List<selectOption>ProfilesOptions2=oGENcOpportunityProduct1.ProfilesOptions2;
        oGENcOpportunityProduct1.opportunityProduct.Digital_Offerings__c = 'Yes';
        oGENcOpportunityProduct1.opportunityProduct.Analytics__c = 'No';
        
        //oGENcOpportunityProduct1.PFamily_lst();
        oGENcOpportunityProduct1.ProductFamily_frm_obj=PF1.id;
        List<selectOption>ProductFamily_frm_obj_lst1=oGENcOpportunityProduct1.ProductFamily_frm_obj_lst;
        //oGENcOpportunityProduct1.ServiceLine_lst();
        oGENcOpportunityProduct1.Serviceline_frm_obj=SL1.id;
        List<selectOption>Serviceline_frm_obj_lst1=oGENcOpportunityProduct1.Serviceline_frm_obj_lst;
        //oGENcOpportunityProduct1.ProductFamilyFromCatalog();
        oGENcOpportunityProduct1.SelectedProductFamily=NOW1.id;
        List<selectOption>ProductFamilyLst1=oGENcOpportunityProduct1.ProductFamilyLst;
        //oGENcOpportunityProduct1.ProfilesOptionsFetch();
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        List<selectOption>ProfilesOptions21=oGENcOpportunityProduct1.ProfilesOptions2;
        oGENcOpportunityProduct1.opportunityProduct.Digital_Offerings__c = 'Yes';
        oGENcOpportunityProduct1.opportunityProduct.Analytics__c = 'No';
        
        //oGENcOpportunityProduct1.PFamily_lst();
        oGENcOpportunityProduct1.ProductFamily_frm_obj=PF2.id;
        List<selectOption>ProductFamily_frm_obj_lst2=oGENcOpportunityProduct1.ProductFamily_frm_obj_lst;
        //oGENcOpportunityProduct1.ServiceLine_lst();
        oGENcOpportunityProduct1.Serviceline_frm_obj=SL2.id;
        List<selectOption>Serviceline_frm_obj_lst2=oGENcOpportunityProduct1.Serviceline_frm_obj_lst;
        //oGENcOpportunityProduct1.ProductFamilyFromCatalog();
        oGENcOpportunityProduct1.SelectedProductFamily=NOW2.id;
        List<selectOption>ProductFamilyLst2=oGENcOpportunityProduct1.ProductFamilyLst;
        //oGENcOpportunityProduct1.ProfilesOptionsFetch();
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        List<selectOption>ProfilesOptions22=oGENcOpportunityProduct1.ProfilesOptions2;
        oGENcOpportunityProduct1.opportunityProduct.Digital_Offerings__c = 'Yes';
        oGENcOpportunityProduct1.opportunityProduct.Analytics__c = 'No';
        
        oGENcOpportunityProduct1.opportunityProduct.Product_family_Lookup__c=oGENcOpportunityProduct1.ProductFamily_frm_obj;
        oGENcOpportunityProduct1.opportunityProduct.Service_Line_lookup__c=oGENcOpportunityProduct1.Serviceline_frm_obj;
        oGENcOpportunityProduct1.opportunityProduct.Nature_of_Work_lookup__c=oGENcOpportunityProduct1.SelectedProductFamily;
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        oGENcOpportunityProduct1.opportunityProduct.Digital_Offerings__c = 'Yes';
        oGENcOpportunityProduct1.opportunityProduct.Analytics__c = 'No';

         List<String> s= new List<String>();
         s.add('Asset1');
         s.add('Asset2');
        oGencOpportunityProduct1.digitalAsset_lst=s;
        //oGencOpportunityProduct1.DigitalOptionsFetch();
        oGencOpportunityProduct1.showdigitalOptions();
        
         oGENcOpportunityProduct1.revenueRollingYearIndex =1;
       
        oGENcOpportunityProduct1.revenueRollingYearToEdit=1;
        oGENcOpportunityProduct1.updateSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToDelete=1;
        oGENcOpportunityProduct1.deleteProductSchedule();

        //oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
       
        //oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.saveLineItem();
        oGENcOpportunityProduct1.getPricemarginRendered();
        oGENcOpportunityProduct1.setPricemarginRendered(true,true);
        oGENcOpportunityProduct1.tempServiceCategory = 'x';
        oGENcOpportunityProduct1.revenueScheduleList = new List<RevenueSchedule__c>();
        test.stoptest();
     
     
     
     }

    
}