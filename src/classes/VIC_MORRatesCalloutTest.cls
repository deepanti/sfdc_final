/*

* This class is used to prepare testdata for VIC_MORRatesCallout using HttpCalloutMock interface Class

* And test the code coverage of VIC_MORRatesCallout class

* created date - 2018/11/21
* created by - Madhuri Sharma
*  */

@isTest

 

Global class VIC_MORRatesCalloutTest implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest request) {

        // Create a fake response

        HttpResponse response = new HttpResponse();

        response.setHeader('Content-Type', 'application/json');

        response.setBody('{ "Status":"Success","PageSize": 15,"PageNumber": 1,"Pagelimit": 10,"MOR_Rates":[{"FROM_CURRENCY":"USR","TO_CURRENCY":"INR","CONVERSION_DATE":"2018-09-29T00:00:00.000-06:00","CONVERSION_TYPE":"Corporate","CONVERSION_RATE":"0.155149673571144","CREATION_DATE":"16-FEB-06"}]}');

        response.setStatusCode(200);

        return response;

    }


   @isTest static void testMORCallout()

    {
        Test.setMock(HttpCalloutMock.class, new VIC_MORRatesCalloutTest());
        Test.startTest();
        VIC_MORRatesCallout.getMORDetails();
        Test.stopTest();

    }

    @isTest static void testMORException()

    {
        Test.startTest();
        VIC_MORRatesCallout.getMORDetails();
        Test.stopTest();

    }

}