public without Sharing class GPSelectorOMSDetail {
    
    public static List<GP_OMS_Detail__c> getOMSDetailsPerDealIds(Set<Id> setDealIDs) {
        
        return [Select RecordTypeId, RecordType.Name, Name, Id, GP_Work_Location_Name__c, GP_Work_Location_Code__c, GP_Track__c,
                GP_TotalCost__c, GP_TCVInUSD__c, GP_Primary__c, GP_Location__c, GP_Expense_Type__c,
                GP_Expense_Amount__c, GP_Effort_in_Hours__c, GP_Country__c, GP_CostRateUSDPerHour__c,
                GP_Category__c, GP_Band__c, GP_BCP__c, GP_Deal__c, GP_OMS_Deal_ID__c
                From GP_OMS_Detail__c
                WHERE GP_Deal__c in: setDealIDs];
    }
}