@isTest
private class TestProposalTriggers 
{

  static testMethod void testTrigger()
   {
     /*  RMG_Employee_Master__c Empmaster=new RMG_Employee_Master__c(Name='TestEmp'); 
      RMG_Employee_Master__c Empmaster_mngr=new RMG_Employee_Master__c(Name='TestEmp1');
    // Resource_Requirement__c newRRF=new Resource_Requirement__c(Name='123');
     // Resource_Requirement__c newRRF1=new Resource_Requirement__c(Name='12345');
    //  insert newRRF;
     // insert newRRF1;
      RR__c newRR=new RR__c(Name='123',gPAC__c='RMG Employees and CAR Candidates Proposal');
      RR__c newRR1=new RR__c(Name='1231',gPAC__c='CAR Candidates Proposal');
      CAR_Candidate__c newCAR = new CAR_Candidate__c(Name='TestCAR',EDoJ__c=date.parse('12/03/2014')); 
      insert Empmaster;
      insert Empmaster_mngr;
      insert newRR;
       insert newRR1;
      insert newCAR;
      Proposal__c Proposal=new Proposal__c(Status__c='Proposed',RMG_Employee_Code__c=Empmaster.id,Proposal_RR__c=newRR.id,Expected_Billing_Date__c=date.parse('09/01/2014'),Client_Interview_Y_N__c='N',Manager__c=Empmaster_mngr.id);
      Proposal__c Proposal1=new Proposal__c(Status__c='Selected-New',Proposal_RR__c=newRR1.id,CAR_Candidate_Code__c = newCAR.id,Expected_Billing_Date__c=date.parse('09/02/2014'),Client_Interview_Y_N__c='N',Manager__c=Empmaster_mngr.id);
      insert Proposal;
      insert Proposal1;
      update Proposal;
      update Proposal1;
     // update newRRF;
      update newRR;
      update newRR1;
      update Empmaster;
      update newCAR;
      Proposal__c newproposal=[select Name, Status__c from Proposal__c where id = :Proposal.id];
      Proposal__c newproposal1=[select Name, Status__c from Proposal__c where id = :Proposal1.id];
      try 
      { 
      delete newproposal; 
      delete newproposal1;
      }        
      catch (Exception e) 
      {
      System.debug('Proposal has been deleted');
      }
      update newRR;
      if(newproposal.Status__c != Null)
      {
            if(newproposal.Status__c != 'Closed')
      {
      String futureProposalStatus = newproposal.Status__c;      
      System.assertEquals(futureProposalStatus,newproposal.Status__c);
      }
      else if( newproposal.Status__c == 'Selected-New' || newproposal.Status__c == 'Blocked' || newproposal.Status__c == 'Selected-Extended') 
      { 
      Proposal.addError('Employee already selected or blocked. No new Proposal can be created till that block is removed.'); 
      }
      else if(newproposal.Status__c == 'Proposed')
      {
      System.assertEquals('Proposed',newproposal.Status__c);
      }
       
      }
      
      
      Update newRR1;
       if(newproposal1.Status__c != Null)
      {
            if(newproposal1.Status__c != 'Closed')
      {
      String futureProposalStatus = newproposal1.Status__c;      
      System.assertEquals(futureProposalStatus,newproposal1.Status__c);
      }
      else if( newproposal1.Status__c == 'Selected-New' || newproposal1.Status__c == 'Blocked' || newproposal1.Status__c == 'Selected-Extended') 
      { 
      Proposal1.addError('Employee already selected or blocked. No new Proposal can be created till that block is removed.'); 
      }
      else if(newproposal1.Status__c == 'Proposed')
      {
      System.assertEquals('Proposed',newproposal1.Status__c);
      }
     
      }
      
   */         
      
      
      
      
      
      
      
      
      
      
    }
}