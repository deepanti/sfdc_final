global class GPBatchExpiringResouceAllocation implements Database.Batchable<sobject>, Database.stateful {

    global date  forfirstthirtyday = System.Today().addDays(Integer.valueof(system.label.GP_Initial_Days_Prior_Project_End_Date));
    global date  forsecondfifteenday = System.Today().addDays(Integer.valueof(system.label.GP_Second_Prior_Days_Prior_Project_End_Date));
    global date  forthirdsevenday = System.Today().addDays(Integer.valueof(system.label.GP_Third_Prior_Days_Prior_Project_End_Date));
    
    global Database.QueryLocator start(Database.BatchableContext bc) { 

    //Get expiring resource allocations for the project in next 30/15/7 days.
    return Database.getQueryLocator([SELECT id, Name, GP_Oracle_PID__c,
                        GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
                        GP_Current_Working_User__r.Name,
                        GP_Current_Working_User__r.Email, GP_End_Date__c,
                       	GP_PID_Approver_User__r.Email,
                        (SELECT id, GP_Employee__r.GP_FULL_NAME__c,                        
                        GP_Employee__r.GP_Final_OHR__c,
                        GP_End_Date__c 
                        FROM GP_Resource_Allocations__r  
                        WHERE (GP_End_Date__c =: forfirstthirtyday 
                        OR GP_End_Date__c =: forsecondfifteenday
                        OR GP_End_Date__c =: forthirdsevenday))
                        FROM GP_Project__c 
                        WHERE (RecordType.Name = 'OTHER PBB' OR RecordType.Name = 'CMITS') 
                        AND GP_Oracle_Status__c = 'S' 
                        AND GP_Approval_Status__c = 'Approved'
                        AND GP_Project_Status__c != 'CLOSED'
                        AND GP_End_Date__c >= Today]); 
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> listofProjectrecord) {
    
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        
        EmailTemplate eTemplate = [SELECT id, DeveloperName, body, subject, htmlvalue
                                   FROM EmailTemplate 
                                   WHERE developername = 'GP_Email_Template_For_Resource_Allocation_Expiry_Notification'
                                   LIMIT 1];        
        
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();

        for(GP_Project__c project : listofProjectrecord) {
            System.debug('==Size of RAs=='+project.GP_Resource_Allocations__r.size());
            if(eTemplate != null && project.GP_Resource_Allocations__r.size() > 0) {
					                
               
                if(String.isNotBlank(eTemplate.HtmlValue)) {
    
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                    List<String> toEmailAddresses = new List<String>();
                    
                    String GPEmailExpire = System.Label.GP_Email_Expire;
                    
                    Map<String,Object> emailExpMap=(Map<String,Object>)JSON.deserializeUntyped(GPEmailExpire); 
                    
                    if(project.GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null && emailExpMap.get('GPM')=='true'){                        
                        toEmailAddresses.add(project.GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                    }
                    
                    if(project.GP_Current_Working_User__r.Email != null && emailExpMap.get('CurrentWorkingUser')=='true'){                        
                        toEmailAddresses.add(project.GP_Current_Working_User__r.Email);
                    }
                    if(project.GP_PID_Approver_User__r.Email != null && emailExpMap.get('PIDApprover')=='true'){                        
                        toEmailAddresses.add(project.GP_PID_Approver_User__r.Email);
                    }  
                    
                    email.setToAddresses(toEmailAddresses);
                    email.sethtmlBody(eTemplate.HtmlValue);
                    email.setSubject(eTemplate.Subject);
                    
                    email.sethtmlBody(email.getHtmlBody().replace('CURRENT_WORKING_USER', project.GP_Current_Working_User__r.Name));
                    email.sethtmlBody(email.getHtmlBody().replace('LIST_EXPIRED_RESOURCE_ALLOCATION_RECORDS', getExpiredResourceAllocations(project)));
                    email.sethtmlBody(email.getHtmlBody().replace('PID_LINK', '<a href="' + System.Label.GP_Send_Notification_Alert_URL + project.Id +'">'+project.Name+'</a><br/>'));
                    email.setSubject(email.getSubject().replace('ORACLE_PID', project.GP_Oracle_PID__c));
    
                    if (oweaId != null) {
                        email.setOrgWideEmailAddressId(oweaId);
                    }
                    
                    emailNotifications.add(email);
                }
            }
        }

        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
    }

    global void finish(Database.BatchableContext bc) {
        
        if(String.isNotBlank(system.label.GP_Email_addresses_for_Resource_Expiry_Alert_Batch_Status)) {
            // Get job details.
            AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, CompletedDate, TotalJobItems
                                FROM AsyncApexJob
                                WHERE Id =: bc.getJobId()];
        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            String toAddresses = system.label.GP_Email_addresses_for_Resource_Expiry_Alert_Batch_Status;
            
            mail.setToAddresses(toAddresses.split(','));
            
            mail.setSubject('Resource Allocation Expiry Alert batch status : ' + aaj.Status);
            
            if(aaj.NumberOfErrors == 0) {
                mail.setPlainTextBody('Resource Expiry alert batch has run successfully with ' +aaj.TotalJobItems+ ' total jobs on ' +aaj.CompletedDate+ '.');                
            } else {
                mail.setPlainTextBody('Resource Expiry alert batch had ' +aaj.NumberOfErrors+ ' no of error while executing on ' +aaj.CompletedDate+ '.');
            }
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private String getExpiredResourceAllocations(GP_Project__c project) {
        
        String html = '';
        html += ' <table width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">';
        html += '   <thead>';
        html += '       <tr>';
        html += '           <th bgcolor="#005595" style="color:white">Employee Name</th>';
        html += '           <th bgcolor="#005595" style="color:white">Employee OHR</th>';
        html += '           <th bgcolor="#005595" style="color:white">Oracle PID</th>';
        html += '           <th bgcolor="#005595" style="color:white">Allocation End Date</th>';
        html += '           <th bgcolor="#005595" style="color:white">Project End Date</th>';
        html += '           <th bgcolor="#005595" style="color:white">Days In Expiring</th>';
        html += '       </tr>';
        html += '   </thead>';
        html += '   <tbody>';
        
        for(GP_Resource_Allocation__c ra : project.GP_Resource_Allocations__r) {
            
            html += '       <tr>';
            html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Employee__r.GP_FULL_NAME__c) ? ra.GP_Employee__r.GP_FULL_NAME__c : '[Not Provided]') + '</td>';
            html += '           <td style="text-align:center;">' + (String.isNotBlank(ra.GP_Employee__r.GP_Final_OHR__c) ? ra.GP_Employee__r.GP_Final_OHR__c : '[Not Provided]') + '</td>';
            html += '           <td style="text-align:center;">' + (String.isNotBlank(project.GP_Oracle_PID__c) ? project.GP_Oracle_PID__c : '[Not Provided]') + '</td>';
            html += '           <td style="text-align:center;">' + (ra.GP_End_Date__c != null ? string.valueof(ra.GP_End_Date__c).removeEnd('00:00:00') : '[Not Provided]') + '</td>';
            html += '           <td style="text-align:center;">' + (project.GP_End_Date__c != null ? string.valueof(project.GP_End_Date__c).removeEnd('00:00:00') : '[Not Provided]') + '</td>';
            html += '           <td style="text-align:center;">' + ((ra.GP_End_Date__c == forfirstthirtyday) ? system.label.GP_Initial_Days_Prior_Project_End_Date : (ra.GP_End_Date__c == forsecondfifteenday) ? system.label.GP_Second_Prior_Days_Prior_Project_End_Date : (ra.GP_End_Date__c == forthirdsevenday) ? system.label.GP_Third_Prior_Days_Prior_Project_End_Date : '0') + '</td>';
            html += '       </tr>';
        }
        
        html += ' </tbody></table>';
        
        return html;
    }
}