@isTest
public class GPSelectorEmployeeMasterTracker {
    @testSetup
    public static void buildDependencyData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
    }
    
    @isTest
    public static void testSelectEmployeeMasterRecord() {
        GP_Employee_Master__c empObj = [Select Id from GP_Employee_Master__c LIMIT 1];
        
        GPSelectorEmployeeMaster employeeMasterSelector = new GPSelectorEmployeeMaster();
        employeeMasterSelector.selectEmployeeMasterRecordMap(null);
        employeeMasterSelector.selectEmployeeMasterRecord(empObj.Id);
        employeeMasterSelector.selectEmployeeMasterRecords(null);
        employeeMasterSelector.selectUserFromEmployeeMaster(null);
        employeeMasterSelector.getUserByEmployeeId(null);
        employeeMasterSelector.getEmployeeListForPersonId(null);
        employeeMasterSelector.getEmployeeListForOHRId(null);
        employeeMasterSelector.getActiveEmployeeRecords(null);
        employeeMasterSelector.selectByRecordId(null);
        employeeMasterSelector.selectActiveEmployeeWithSFDCUserIds(null);   
        employeeMasterSelector.selectById(new Set<Id> {empObj.Id});
    }
}