@isTest 
public class Test_UpdateWLRReason
{
    static testMethod void TestMethod1()
    {
    // try
    //  {
        system.debug('========test WinLossDroppedSurvey_Extension  calling ======= start====== ');
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];
        User u = new User(alias = 'nkhatri', email='neha1@genpact.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                            localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                            timezonesidkey='Europe/Paris', username='neha1@genpact.com',
                            IsActive = true);    
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        
          System.runAs(u)   
            {
                Account oAccount = new Account();
                oAccount.Name = 'strName';          
                oAccount.Business_group__c= 'ge';
                oAccount.business_segment__c = oBS.ID;
                oAccount.sub_business__c= oSB.ID;
                oAccount.industry_vertical__c= 'BFS';
                oAccount.sub_industry_vertical__c= 'BFS';           
                oAccount.website = 'www.genpact.com';
                oAccount.sic= 'test code';
                oAccount.hunting_mining__c= 'test mining';
                oAccount.TAM__c=5000;
                oAccount.PTAM__c=6000;
                oAccount.Archetype__c=oAT.ID;           
                oAccount.Client_Partner__c=u.id;
                oAccount.Primary_Account_GRM__c=u.Id;
                oAccount.TickerSymbol= 'test ticker';
                oAccount.AnnualRevenue= 50000;          
                oAccount.Ownership= 'test Owner';
                oAccount.Account_headquarters_country__c= 'test Account headquarters';
                         
                Sales_Unit__c su = new Sales_Unit__c(Name='Sales Unit',Sales_Leader__c = u.id);
                insert su;
                
                Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id,Sales_Unit_Group__c='Sales Unit Head');
                insert salesunitobject;
                
                oAccount.Sales_Unit__c = salesunitobject.id;            
                INSERT oAccount;
             
                opportunity Opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='Signed',Competitor__c='Accenture',Deal_Type__c='Competitive',Delivering_Organisation__c='Endeavour',Highest_Revenue_Product__c='IT Services', Nature_of_Work_highest__c = 'Delivery', Highest_ServiceLine__c='Risk', key_win_reasons_and_any_key_learnings__c='aaaa', Reason_1__c ='Partner ally: We had a strong partner ally in the account', Reason_2__c ='Reputation (Buyer perception): Client perceived Genpact as a strong provider in this space', Reason_3__c='Track record: Great performance in current operations');
                insert Opp ;
                                
                 Contact Con =new Contact(firstname='Manoj',lastname='Pandey',accountid=oAccount.Id,Email='test1@gmail.com');
                insert Con;
             
                Contract oContract =new Contract(Accountid=oAccount.id,Opportunity_tagged__c=opp.id,CustomerSignedid=Con.id,Status='Draft');
                insert oContract;
                system.debug('=====oContract===test data==='+oContract);
                Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
                insert attach;
                
               Test.StartTest();    
                 
                ApexPages.StandardController sc = new ApexPages.StandardController(Opp);
                WinLossDroppedSurvey_Extension obj_wlr= new WinLossDroppedSurvey_Extension(sc); // class
                
                OpportunitySurveyListWrapper obj_OpportunitySurveyListWrapper = new OpportunitySurveyListWrapper(); // Wrapper class
                
                WinLossDroppedSurveyController obj_WLDS = new WinLossDroppedSurveyController(); // class        
                WinLossDroppedSurveyController.getDealOutcome();
                WinLossDroppedSurveyController.getOpportunityData(Opp.id);    

                system.debug('=======test====1====='); 
                 
                 // Create an approval for the account and submit
                /*Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
                app.setObjectId(oContract.ID); 
                Approval.ProcessResult AccountRequestResult = Approval.Process(app);
                System.assert(AccountRequestResult.isSuccess());  */
                
                system.debug('=======test====2====');             
                UpdateWLRReason obj_UpdateWLRReason = new UpdateWLRReason(); // class
                UpdateWLRReason.getIncumbent();
                UpdateWLRReason.getinteractions_did_we_have_with_the_prospe();
                UpdateWLRReason.getGenpact_Involvement_in_deal_with_prospec();
                UpdateWLRReason.getClosestCompetitor();
                UpdateWLRReason.getClosestCompetitor1();
                UpdateWLRReason.getLossDealStage();
                UpdateWLRReason.getDealWinner();      
                string conid = oContract.id;
                string SurveyNumber = 'Survey1';
                
                Map<string, string> maps = new Map<string, string>();
                maps.put('Partner ally: We had a strong partner ally in the account', '1');
                maps.put('Reputation (Buyer perception): Client perceived Genpact as a strong provider in this space', '2');
                maps.put('Track record: Great performance in current operations', '3');
                system.debug('=====maps===test===='+maps);      
                UpdateWLRReason.saveWLR(Opp.id, conid, SurveyNumber, maps, Opp) ;                      
                system.debug('========test WinLossDroppedSurvey_Extension  calling ======= end====== ');
              Test.stoptest();
            }
        /*}
        catch(Exception e)
        { 
            system.debug('====test catch=1===='+e.getmessage());
            System.debug('eline:'+e.getLineNumber());
            System.debug('stacktrace:'+e.getStackTraceString());
        }*/
    }
    static testMethod void TestMethod2()
    {
    // try
    //  {
        system.debug('========test WinLossDroppedSurvey_Extension  calling ======= start====== ');
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];
        User u = new User(alias = 'nkhatri', email='neha1@genpact.com',
                            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                            localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                            timezonesidkey='Europe/Paris', username='neha1@genpact.com',
                            IsActive = true);    
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        
          System.runAs(u)   
            {
                Account oAccount = new Account();
                oAccount.Name = 'strName';          
                oAccount.Business_group__c= 'ge';
                oAccount.business_segment__c = oBS.ID;
                oAccount.sub_business__c= oSB.ID;
                oAccount.industry_vertical__c= 'BFS';
                oAccount.sub_industry_vertical__c= 'BFS';           
                oAccount.website = 'www.genpact.com';
                oAccount.sic= 'test code';
                oAccount.hunting_mining__c= 'test mining';
                oAccount.TAM__c=5000;
                oAccount.PTAM__c=6000;
                oAccount.Archetype__c=oAT.ID;           
                oAccount.Client_Partner__c=u.id;
                oAccount.Primary_Account_GRM__c=u.Id;
                oAccount.TickerSymbol= 'test ticker';
                oAccount.AnnualRevenue= 50000;          
                oAccount.Ownership= 'test Owner';
                oAccount.Account_headquarters_country__c= 'test Account headquarters';
                         
                Sales_Unit__c su = new Sales_Unit__c(Name='Sales Unit',Sales_Leader__c = u.id);
                insert su;
                
                Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id,Sales_Unit_Group__c='Sales Unit Head');
                insert salesunitobject;
                
                oAccount.Sales_Unit__c = salesunitobject.id;            
                INSERT oAccount;
             
                opportunity Opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='Signed',Competitor__c='Accenture',Deal_Type__c='Competitive',Delivering_Organisation__c='Endeavour',Highest_Revenue_Product__c='IT Services', Nature_of_Work_highest__c = 'Delivery', Highest_ServiceLine__c='Risk', key_win_reasons_and_any_key_learnings__c='aaaa', Reason_1__c ='Partner ally: We had a strong partner ally in the account', Reason_2__c ='Reputation (Buyer perception): Client perceived Genpact as a strong provider in this space', Reason_3__c='Track record: Great performance in current operations');
                insert Opp ;
                                
                 Contact Con =new Contact(firstname='Manoj',lastname='Pandey',accountid=oAccount.Id,Email='test1@gmail.com');
                insert Con;
             
                Contract oContract =new Contract(Accountid=oAccount.id,Opportunity_tagged__c=opp.id,CustomerSignedid=Con.id,Status='Draft');
                insert oContract;
                system.debug('=====oContract===test data==='+oContract);
                Attachment_Contract__c attach= new Attachment_Contract__c(Contract__c=oContract.id,File_Name__c='test.tes' );
                insert attach;
                
               Test.StartTest();    
                 
                ApexPages.StandardController sc = new ApexPages.StandardController(Opp);
                WinLossDroppedSurvey_Extension obj_wlr= new WinLossDroppedSurvey_Extension(sc); // class
                
                OpportunitySurveyListWrapper obj_OpportunitySurveyListWrapper = new OpportunitySurveyListWrapper(); // Wrapper class
                
                WinLossDroppedSurveyController obj_WLDS = new WinLossDroppedSurveyController(); // class        
                WinLossDroppedSurveyController.getDealOutcome();
                WinLossDroppedSurveyController.getDealOutcome_Signed();
                
                WinLossDroppedSurveyController.getOpportunityData(Opp.id);    

                system.debug('=======test====1====='); 
                 
                 // Create an approval for the account and submit
                /*Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
                app.setObjectId(oContract.ID); 
                Approval.ProcessResult AccountRequestResult = Approval.Process(app);
                System.assert(AccountRequestResult.isSuccess());  */
                
                system.debug('=======test====2====');             
                UpdateWLRReason obj_UpdateWLRReason = new UpdateWLRReason(); // class
                UpdateWLRReason.getIncumbent();
                UpdateWLRReason.getinteractions_did_we_have_with_the_prospe();
                UpdateWLRReason.getGenpact_Involvement_in_deal_with_prospec();
                UpdateWLRReason.getClosestCompetitor();
                UpdateWLRReason.getClosestCompetitor1();
                UpdateWLRReason.getLossDealStage();
                UpdateWLRReason.getDealWinner();      
                string conid = oContract.id;
                string SurveyNumber = 'Survey1';
                
                Map<string, string> maps = new Map<string, string>();
                maps.put('Partner ally: We had a strong partner ally in the account', '1');
                maps.put('Reputation (Buyer perception): Client perceived Genpact as a strong provider in this space', '2');
                //maps.put('Track record: Great performance in current operations', '3');
                system.debug('=====maps===test===='+maps);      
                UpdateWLRReason.saveWLR(Opp.id, conid, SurveyNumber, maps, Opp) ;                      
                system.debug('========test WinLossDroppedSurvey_Extension  calling ======= end====== ');
              Test.stoptest();
            }
        /*}
        catch(Exception e)
        { 
            system.debug('====test catch=1===='+e.getmessage());
            System.debug('eline:'+e.getLineNumber());
            System.debug('stacktrace:'+e.getStackTraceString());
        }*/
    }
}