/*
    @Description: Batch class will be used for handling achievement of target and Calculation of guarenteed Payout, 
                          If there is remaining guarenteed payout then it will add new target incentive.
    @Test Class: VIC_GuaranteeComponentBatchCtlrTest
    @Author: Rishabh Pilani
*/
public class VIC_GuaranteeComponentBatchCtlr implements Database.Batchable<sObject>, Database.Stateful{
    private String strGuaranteedPayout = 'Guarantee';
    Master_Plan_Component__c objMasterGuaranteedPayout = fetchGuaranteedPayout();

    public Database.QueryLocator start(Database.BatchableContext bc){
        
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        Integer processingYear = Integer.ValueOf(vicInfo.VIC_Annual_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        
        return Database.getQueryLocator([SELECT Id,vic_Pending_Guaranteed_Payment__c,vic_Guaranteed_Payout__c,Overall_Achievement__c,
                                            (SELECT id,Name
                                             FROM Target_Components__r 
                                             where Master_Plan_Component__r.vic_Component_Code__c=:strGuaranteedPayout 
                                             LIMIT 1) 
                                        FROM Target__c 
                                        WHERE vic_Pending_Guaranteed_Payment__c  > 0
                                        AND Start_date__c =:targetStartDate
                                        AND Is_Active__c = TRUE ]);
   
    }
    
    public String getIncentiveDetail(Target__c objTarget){
        return 'Guaranteed is ' + objTarget.vic_Guaranteed_Payout__c + ' and the overall achievement of user is ' + objTarget.Overall_Achievement__c + '. So, remaining amount ' + objTarget.vic_Pending_Guaranteed_Payment__c + ' incentive is created.';
    }
    
    public Target_Achievement__c createIncentiveRecord(Target__c objTarget){        
        system.debug('createIncentiveRecord' + objTarget.Target_Components__r);
        return new Target_Achievement__c(
                                            Target_Component__c = objTarget.Target_Components__r!= null && objTarget.Target_Components__r.size() > 0 ? objTarget.Target_Components__r[0].id :null,
                                            Achievement_Date__c = System.Today(),
                                            Bonus_Amount__c = objTarget.vic_Pending_Guaranteed_Payment__c,
                                            vic_Incentive_In_Local_Currency__c = objTarget.vic_Pending_Guaranteed_Payment__c,
                                            vic_Incentive_Type__c = 'Adjustment',
                                            vic_Status__c = 'VIC Team - Pending',
                                            vic_Incentive_Details__c = getIncentiveDetail(objTarget),
                                            VIC_Description__c = objTarget.id
                                        );
    }
    
    public Target_Component__c createTargetComponent(String strTargetId, String strGuaranteeCompId){
        return new Target_Component__c(
            Master_Plan_Component__c = strGuaranteeCompId,
            Target__c = strTargetId,
            Target_Status__c = 'Active'
        );
    }
    
    public void execute(Database.BatchableContext bc, List<Target__c> lstTarget){
        List<Target_Achievement__c> lstTargetAchievement = new List<Target_Achievement__c>();
        List<Target__c> lstPendingTargets = new List<Target__c>();
        for(Target__c t: lstTarget){
            if(t.Target_Components__r != null && t.Target_Components__r.size() > 0){
                lstTargetAchievement.add(createIncentiveRecord(t));
            }else{
                lstPendingTargets.add(t);
            }
        }
        System.debug(lstTargetAchievement);
        System.debug('Incentive Created @ Step 1: ' + lstTargetAchievement.size());
        System.debug('Components needs to be create for ' + lstPendingTargets.size());
        
        if(lstPendingTargets != null && lstPendingTargets.size() > 0){
            Master_Plan_Component__c objMPC = fetchGuaranteedPayout();
            Map<Target_Component__c, Target_Achievement__c> mapCompVsIncentive = new Map<Target_Component__c, Target_Achievement__c>();            
            System.debug('---------------------------------------------------');
            for(Target__c t:lstPendingTargets){
                mapCompVsIncentive.put(createTargetComponent(t.Id,objMPC.id),createIncentiveRecord(t));
            }
            
            List<Target_Achievement__c> lstPendingAch = mapCompVsIncentive.values();
            
            System.debug('---> ' + mapCompVsIncentive);
            insert new List<Target_Component__c>(mapCompVsIncentive.keySet());
            
            SYSTEM.DEBUG('-----FULL MAP------>' + mapCompVsIncentive);
            
            for(Target_Component__c c: mapCompVsIncentive.keySet()){
                for(Target_Achievement__c a:lstPendingAch){
                    if(a.VIC_Description__c != null && a.VIC_Description__c == c.Target__c){
                        a.Target_Component__c = c.Id;
                        a.VIC_Description__c = null;
                        lstTargetAchievement.add(a);
                        break;
                    }
                }
            }
        }
        
        if(lstTargetAchievement.size() > 0){
            insert lstTargetAchievement;
        }
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
    /*
        @Description: It will fetch Master_Plan_Component__c record for 'Guarantee'
        @Return: Master_Plan_Component__c 
        @Author: Vikas Rajput
    */
    public Master_Plan_Component__c fetchGuaranteedPayout(){        
        return [select id,name 
                    from Master_Plan_Component__c 
                    Where vic_Component_Code__c=:strGuaranteedPayout 
                    Limit 1];
    }
    
}