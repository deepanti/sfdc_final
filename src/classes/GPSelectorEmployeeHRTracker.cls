@isTest
public class GPSelectorEmployeeHRTracker {
    static GP_Employee_HR_History__c employeeHRHistory;
    static GP_Employee_Master__c employeeMaster;
    
	@testSetup
    static void buildDependencyData() {
        GP_Employee_Master__c employeeMaster = GPCommonTracker.getEmployee();
        insert employeeMaster;
        
        GP_Employee_HR_History__c employeeHRHistory = GPCommonTracker.getEmployeeHR(employeeMaster.Id);
        insert employeeHRHistory;
    }
    
    @isTest
    static void testEmployeeHrTracker() {
        fetchData();
        GPSelectorEmployeeHR employeeHrSelector = new GPSelectorEmployeeHR();
        employeeHrSelector.selectById(employeeHRHistory.Id);
        employeeHrSelector.selectById(new Set<Id> {employeeHRHistory.Id});
        GPSelectorEmployeeHR.selectInactiveRecordsForAMonthYear(null, null, null);
        GPSelectorEmployeeHR.getEmployeeHRRecordsInDescOrder(null);
        employeeHrSelector.getEmployeeHRBasedOnEmp(null);
        employeeHrSelector.getEmployeeHRRecordsInDescending(null);
    }
    
    static void fetchData() {
        employeeMaster = [Select Id from GP_Employee_Master__c];
        employeeHRHistory = [Select Id from GP_Employee_HR_History__c];
    }
}