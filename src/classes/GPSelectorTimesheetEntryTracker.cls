@isTest
public class GPSelectorTimesheetEntryTracker {
    public static GP_Timesheet_Entry__c timeshtentryObj;
    
    @testSetup static void setupCommonData() 
    {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Person_ID__c  = 'EMP-001';
        insert empObj; 
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        timesheettrnsctnObj.GP_Month_Year__c = 'Jun-2017';
        insert timesheettrnsctnObj;
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,null,timesheettrnsctnObj);
        timeshtentryObj.GP_Project_Oracle_PID__c = 'ORACLE-001';
        timeshtentryObj.GP_Project_Task_Oracle_Id__c = 'ORACLE-001';
        insert timeshtentryObj ;
    }
    
    @isTest
    public static void testselectTimeSheetEntryRecord() {
        timeshtentryObj =[select id from GP_Timesheet_Entry__c limit 1];
        GPSelectorTimesheetEntry selector = new GPSelectorTimesheetEntry();
        //selector.getSObjectFieldList();
        //selector.getSObjectType();
        selector.selectById(new Set<Id>{timeshtentryObj.Id});
        selector.selectTimeSheetEntriesRecord(null,2);
        selector.getTimeSheetEntriesEmployeeMonthYear(new Set<String>());
    }
    
}