//================================================================================================================
//  Description: Test Class for GPServiceBillingMilestone
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
private class GPServiceBillingMilestoneTracker {
    private Static List < GP_Billing_Milestone__c > listOfBillingMilestone = new List < GP_Billing_Milestone__c > ();
    private Static GP_Billing_Milestone__c billingMilestone;
    private Static GP_Work_Location__c objSdo;
    private Static GP_Project__c prjobj;

    @testSetup static void setupCommonData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = new GP_Project__c();
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('CMITS').getRecordTypeId();
        prjObj.GP_Deal__c = dealObj.Id;
        prjObj.GP_PID_Approver_User__c = UserInfo.getUserId();
        prjObj.GP_Project_Template__c = objprjtemp != null ? objprjtemp.Id : null; //lookup  
        prjObj.GP_PID_Creator_Role__c = objrole.Id;
        prjObj.GP_PID_Update_Role__c = objrole.Id;
        //prjobj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjobj.OwnerId = UserInfo.getUserId();
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c = 'Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c = system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        prjobj.GP_Start_Date__c=system.today().adddays(5);
        prjobj.GP_End_Date__c=system.today().adddays(5);
        // Gainshare Change
        prjobj.GP_Project_type__c = 'Gainshare';
        prjobj.GP_TCV__c=1200;
		
        insert prjobj;

        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'INR';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;

        GP_Billing_Milestone__c billingMilestone1 = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone1.GP_Project__c = prjObj.id;
        billingMilestone1.CurrencyIsoCode = 'INR';
        billingMilestone1.GP_Date__c = System.today().adddays(3);
        billingMilestone1.GP_Milestone_start_date__c = System.today().adddays(3);
        insert billingMilestone1;

        GP_Billing_Milestone__c billingMilestone2 = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone2.GP_Project__c = prjObj.id;
        billingMilestone2.CurrencyIsoCode = 'INR';
        billingMilestone2.GP_Date__c = System.today().adddays(3);
        billingMilestone2.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone2.GP_Value__c = 10;
        billingMilestone2.GP_Entry_type__c = 'Percent';
        insert billingMilestone2;
    }

    @isTest
	public static void testGPServiceBillingMilestone() {
		fetchData();
		
		GPServiceBillingMilestone.validateBillingMilestone(prjobj, listOfBillingMilestone);
		//GPServiceBillingMilestone.validateBillingMilestone(prjobj, new List<GP_Billing_Milestone__c>{billingMilestone});
		GPServiceBillingMilestone.isBillingMilestoneDefinable(prjObj);
		GPServiceBillingMilestone.isBillingMilestoneDelitable(listOfBillingMilestone[0]);
		GPServiceBillingMilestone.isBillingMilestoneEditable(listOfBillingMilestone[0]);
		GPServiceBillingMilestone.isFormattedLogRequired = false;
		GPServiceBillingMilestone.validateBillingMilestone(prjobj, listOfBillingMilestone);
	}

    public static void fetchData() {
		// Gainshare Change : GP_Project_type__c
        prjObj = [select id,Name,GP_Delivery_Org__c,GP_Sub_Delivery_Org__c,GP_TCV__c,RecordTypeId,RecordType.Name,GP_Last_Temporary_Record_Id__c,GP_Start_Date__c,GP_Project_type__c from GP_Project__c limit 1];
		listOfBillingMilestone = [select id, Name,GP_Project__c, GP_Date__c,GP_Amount__c,GP_Milestone_start_date__c,GP_Billing_Status__c,
        GP_Milestone_end_date__c,GP_Value__c,GP_Parent_Billing_Milestone__c,GP_Entry_type__c,GP_Last_Temporary_Record_Id__c from GP_Billing_Milestone__c ];
    }


    /*@isTest
    public static void testGPServiceBillingMilestone2() { 
        LoadData();
        insert prjobj;
        billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'MOP';
        billingMilestone.GP_Value__c = 0;
        billingMilestone.GP_Entry_type__c = 'Percent';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;
        listOfBillingMilestone.add(billingMilestone);
        GPServiceBillingMilestone.validateBillingMilestone(prjobj, listOfBillingMilestone);
    }

    @isTest
    public static void testGPServiceBillingMilestone3() {
        LoadData();
        insert prjobj;
        billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'MOP';
        billingMilestone.GP_Value__c = 0;
        billingMilestone.GP_Entry_type__c = 'Percent';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;
        listOfBillingMilestone.add(billingMilestone);
        GPServiceBillingMilestone.isFormattedLogRequired = false;
        GPServiceBillingMilestone.isLogForTemporaryId = true;
        GPServiceBillingMilestone.validateBillingMilestone(prjobj, listOfBillingMilestone);
    }

    @isTest
    public static void testGPServiceBillingMilestone4() {
        LoadData();
        insert prjobj;
        billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'MOP';
        billingMilestone.GP_Value__c = 0;
        billingMilestone.GP_Entry_type__c = 'Percent';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;
        listOfBillingMilestone.add(billingMilestone);
        GPServiceBillingMilestone.isFormattedLogRequired = false;
        GPServiceBillingMilestone.isLogForTemporaryId = False;
        GPServiceBillingMilestone.validateBillingMilestone(prjobj, listOfBillingMilestone);
    }*/

}