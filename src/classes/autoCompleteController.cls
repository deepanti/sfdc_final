global class autoCompleteController 
{
    @RemoteAction
    global static set<String> findSObjects(string obj, string qry, string addFields, string profilename, string fieldname) 
    {
    
        System.Debug('in classsssssssssss');
        /* More than one field can be passed in the addFields parameter
           Split it into an array for later use */
        //List<String> fieldList=new List<String>();
        //if (addFields != '')  
       // fieldList = addFields.split(',');
        
        /* Check whether the object passed is valid */
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) 
        {
            return null;
        }
        
        /* Creating the filter text */
        String filter = ' like \'%' + String.escapeSingleQuotes(qry) + '%\'';
        
        /* Begin building the dynamic soql query */
        String soql = 'SELECT ' + fieldname;
        
        /* If any additional field was passed, adding it to the soql 
        if (fieldList.size()>0) 
        {
            for (String s : fieldList) 
            {
                soql += ', ' + s;
            }
        }
        */
        /* Adding the object and filter by name to the soql */
        soql += ' from ' + obj + ' where ' + fieldname + filter;
      /*  
        if(profilename!='')
        {
            //profile name and the System Administrator are allowed
            soql += ' and Profile.Name like \'%' + String.escapeSingleQuotes(profilename) + '%\'';
            system.debug('Profile:'+profilename+' and SOQL:'+soql);
        }
        */
        /* Adding the filter for additional fields to the soql */
     /*   if (fieldList != null) 
        {
            for (String s : fieldList) 
            {
                soql += ' or ' + s + filter;
            }
        }
*/        
        soql += ' order by ' + fieldname +  ' limit 20';
        
        system.debug('Qry: '+soql);
        
        List<promoter_info__c> L = new List<promoter_info__c>();
        Set<String> res = new Set<String>();
        try 
        {

            L = Database.query(soql);
            
            if(fieldname.equals('Text_Promoter_Name__c'))
            {
                for(Promoter_info__c c: L)
                {
                    IF(!res.contains(c.Text_Promoter_Name__c))
                        res.add(c.Text_Promoter_Name__c);
                }
            }
            else if(fieldname.equals('Job_Title__c'))
            {
                for(Promoter_info__c c: L)
                {
                    IF(!res.contains(c.Job_Title__c))
                        res.add(c.Job_Title__c);
                }
            }
            else if(fieldname.equals('Sub_Service_Line_short__c'))
            {
                for(Promoter_info__c c: L)
                { IF(!res.contains(c.Sub_Service_Line_short__c))
                        res.add(c.Sub_Service_Line_short__c);
                }
            }
            else if(fieldname.equals('Account_Name__c'))
            {
                for(Promoter_info__c c: L)
                {
                    IF(!res.contains(c.Account_Name__c))
                        res.add(c.Account_Name__c);
                }
            }
            else if(fieldname.equals('Unique_about_this_engagement__c'))
            {
                for(Promoter_info__c c: L)
                {
                    IF(!res.contains(c.Unique_about_this_engagement__c))
                        res.add(c.Unique_about_this_engagement__c);
                }
            }

            
            system.debug('L: '+ L);
            system.debug('res: '+ res);
        }
        catch (QueryException e) 
        { system.debug('Query Exception:'+e.getMessage());
            return null;
        }
        return res;
   }
}