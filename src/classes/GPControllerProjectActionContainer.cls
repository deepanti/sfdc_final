/**
 * @author Anmol.kumar
 * @date 22/10/2017
 *
 * @group ProjectTemplate
 * @group-content ../../ApexDocContent/ProjectActionContainere.html
 *
 * @description Apex Service for ProjectAction Container,
 * has aura enabled method to CRUD Project Action Container
 */

public class GPControllerProjectActionContainer {

    private id currentUserId;
    private GP_Project__c objProject;
    private string recordid;
    private string BPMrecordType;
    private Boolean isAdminUser;

    public GPAuraResponse getActionAccess(string strRecordid) {
        BPMrecordType = GPCommon.getRecordTypeId('GP_Project__c', 'BPM');
        recordid = strRecordid;
        setProject(recordid);
        setCurrentUserID();
        setIsAdminUser();

        return new GPAuraResponse(true, '', generateJSON());
    }


    private string generateJSON() {
        JSONGenerator objJSONGen = JSON.createGenerator(false);
        objJSONGen.writeStartObject();
        objJSONGen.writeBooleanField('allowProjectClone', allowProjectClone());
        objJSONGen.writeBooleanField('allowProjectSubmission', allowProjectSubmission());
        objJSONGen.writeBooleanField('allowProjectAssignment', allowProjectAssignment());
        objJSONGen.writeBooleanField('allowProjectApprovereject', allowProjectApprovereject());
        objJSONGen.writeBooleanField('allowProjectClosureSubmission', allowProjectClosureSubmission());
        objJSONGen.writeBooleanField('allowDelete', allowDelete());
        objJSONGen.writeBooleanField('allowrecall', allowRecall());
        objJSONGen.writeBooleanField('allowCloneAndDelete', allowCloneAndDelete());
        objJSONGen.writeEndObject();

        return objJSONGen.getAsString();
    }

    private void setProject(string recordid) {
        objProject = new GPSelectorProject().getProject(recordid);
    }

    private boolean allowCloneAndDelete() {

        string userAccess = GPCommon.getScreenAccess('projectcreation', currentUserId);

        return ((objProject.GP_Approval_Status__c == 'Approved' || objProject.GP_Approval_Status__c == 'Closed') && (userAccess == 'edit' || userAccess == 'modify') &&
                objProject.GP_Oracle_Status__c != '' &&  objProject.GP_Oracle_Status__c != null  && 
                objProject.GP_Oracle_Status__c != 'S' && currentUserId == objProject.GP_Current_Working_User__c);
    }

    private boolean allowProjectClone() {

        string userAccess = GPCommon.getScreenAccess('projectcreation', currentUserId);

        return (objProject.GP_Approval_Status__c == 'Approved' && (userAccess == 'edit' || userAccess == 'modify') && objProject.GP_Oracle_Status__c == 'S' );
    }

    private boolean allowProjectSubmission() {
        return ((objProject.GP_Approval_Status__c == 'Draft' || 
                objProject.GP_Approval_Status__c == 'Rejected') && 
                currentUserId == objProject.GP_Current_Working_User__c);
    }
    private boolean allowProjectAssignment() {
        return (currentUserId == objProject.GP_Current_Working_User__c && 
                    (objProject.GP_Approval_Status__c == 'Draft' || objProject.GP_Approval_Status__c == 'Rejected') &&
                    objProject.RecordTypeId != BPMrecordType) ||
                (isAdminUser && 
                    (objProject.GP_Approval_Status__c == 'Draft' || objProject.GP_Approval_Status__c == 'Rejected'));
    }

    private boolean allowProjectApprovereject() {
        return getPendingWorkItems(recordid);
    }

    private boolean allowProjectClosureSubmission() {
        return ((objProject.GP_Approval_Status__c == 'Draft' || objProject.GP_Approval_Status__c == 'Rejected') && currentUserId == objProject.GP_Current_Working_User__c && objProject.GP_Oracle_PID__c != 'NA');
    }

    private boolean allowDelete() {
        return ((objProject.GP_Approval_Status__c == 'Draft' || 
            objProject.GP_Approval_Status__c == 'Rejected') && 
            (isAdminUser || currentUserId == objProject.GP_Current_Working_User__c)  && 
            String.isEmpty(objProject.GP_Oracle_Status__c)); 
            //|| (objProject.GP_Approval_Status__c != 'Pending for Approval' &&
                //objProject.GP_Approval_Status__c != 'Pending For Closure' && objProject.GP_Oracle_Status__c != 'E')) && currentUserId == objProject.Ownerid) {
    }

    private boolean allowRecall() {
        return ((objProject.GP_Approval_Status__c == 'Pending for Approval' || objProject.GP_Approval_Status__c == 'Pending for Closure') && currentUserId == objProject.GP_Current_Working_User__c);
    }

    private void setCurrentUserID() {
        currentUserId = UserInfo.getUserId();
    }

    private void setIsAdminUser() {
        GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();

        List<GP_User_Role__c> listOfAdminUserRoleForLoggedInUser = userRoleSelector.selectUserofAdminCategory();
        isAdminUser = listOfAdminUserRoleForLoggedInUser != null && listOfAdminUserRoleForLoggedInUser.size() > 0;

    }

    private boolean getPendingWorkItems(string recordid) {
        list < ProcessInstanceWorkitem > lstProcessIntance = [Select p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId, p.ProcessInstanceId, p.OriginalActorId, p.Id, p.ActorId, p.CreatedDate
            From ProcessInstanceWorkitem p where ProcessInstance.Status = 'Pending'
            and ActorId =: currentUserId
            and ProcessInstance.TargetObjectId =: recordid
        ];

        return !lstProcessIntance.isEmpty();
    }

}