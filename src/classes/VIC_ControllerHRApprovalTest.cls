@istest
public class VIC_ControllerHRApprovalTest {
    public static Map < String, Set < String >> mapOfUserVsSetOfIncentiveId = new Map < String, Set < String >> ();
    public static case objcase;
    public static Target_Achievement__c ObjInc;
    public static Target_Achievement__c ObjInc1;
    static  testmethod void VIC_ControllerHRApproval(){
        loadData();
        
       
        
       
        set<string> setOfAssociatedIncentiveRecords = new set<string>();
        //setOfAssociatedIncentiveRecords.add(ObjInc.id);
        setOfAssociatedIncentiveRecords.add(ObjInc1.id);
        VICServiceHRApproval.HRDataWrapper  obj = new VICServiceHRApproval.HRDataWrapper();
        
        obj.IsRejected=true;
        obj.UserId=ObjInc1.Target_Component__r.Target__r.User__C;
        obj.setOfAssociatedIncentiveRecords=setOfAssociatedIncentiveRecords;
        obj.UserName='jhdjh';
        obj.Comments='bkjcsk';
        obj.VICRole='bkjsdncn';
        
        
        
        
        List < VICServiceHRApproval.HRDataWrapper > lst = new List < VICServiceHRApproval.HRDataWrapper >();
        
        lst.add(obj);
        
        string jsonObj=json.serialize(obj);
        string json=json.serialize(lst);
        system.debug('json'+json);
        system.debug('jsonObj'+jsonObj);
        
        VIC_ControllerHRApproval objN = new VIC_ControllerHRApproval('HR - Pending','Test','HRApprovals');
        objN.mapOfUserVsSetOfIncentiveIds=mapOfUserVsSetOfIncentiveId;
        objN.getPendingForApprovalRecords();
        objN.setJson();
        objN.updateStatusOfIncentiveRecords(json,'HRApproval',jsonObj);
        objN.updateIncentives(jsonObj,'HRApproval',jsonObj);
        objN.hack();
        
        VICServiceHRApproval objt = new VICServiceHRApproval('HR - Pending','SalesRep','HRApprovals');
        objt.validLoggedinUser();
        objt.screenName='HRApproval';
       
        VICAuraResponse AuraResponse= new  VICAuraResponse(false,'genpact','vic');
        boolean isSuccess= AuraResponse.getisSuccess;
        string getMessage=AuraResponse.getMessage;
        
       
        system.assertEquals(200,200);
      
        
    }
    static  testmethod void VIC_ControlleNrHRApproval(){
        loadData();
        
       
        VICServiceHRApproval objt = new VICServiceHRApproval('HR - Pending','SalesRep','HRApprovals');
        objt.validLoggedinUser();
        objt.screenName='HRApproval';
        objt.getDomicileWiseIncentives();
        system.assertEquals(200,200);
        
        
        
       
        
      
        
    }
    static void loadData(){
        VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Process_Year__c=2018;
        insert vicInfo;
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='IT_GRM';   
        planObj1.Is_Active__c = true;
        planObj1.Year__c= String.valueOf(System.today().year());
        insert planobj1;
        user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
        
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        system.debug('obbbbbjuser'+objuser);
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
        system.debug('masterVICRoles'+masterVICRoleObj);
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
        VIC_Role__c vrole= VIC_CommonTest.getVICRole(masterVICRoleObj.id,planObj1.id);
        insert  vrole;       
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objOpp.vic_Is_Kickers_Calculated__c=false;
        objOpp.OwnerId=objUser.id;
        
        insert objOpp;
        
        OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.Product_BD_Rep__c=objuser3.id;
        objOLI.vic_VIC_User_3__c=objuser1.id;
        objOLI.vic_VIC_User_4__c=objuser2.id;
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.Contract_Term__c=36;
        objOLI.TCV__c=3500000;
        objOLI.vic_VIC_Grid__c=10;
        insert objOLI;
        
        objopp.Number_of_Contract__c=2;    
        objOpp.stagename='6. Signed Deal';
       
        test.startTest();
        system.runas(objuser){
           update objOpp; 
        }
        
        
        system.debug('objOLI'+objOLI);
        
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        
        
        
        Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='Discretionary_Payment'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
            
        Plan_Component__c PlanComponentObj2 =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj1.id,planobj1.id);
        insert PlanComponentObj2;
            
        Target_Component__c targetComponentObj2=VIC_CommonTest.getTargetComponent();
        targetComponentObj2.Target__C=targetObj.id;
        
        targetComponentObj2.Master_Plan_Component__c=masterPlanComponentObj1.id;
        insert targetComponentObj2;
        
        ObjInc1= VIC_CommonTest.fetchIncentive(targetComponentObj2.id,1000);
        ObjInc1.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc1.Opportunity__c=objOpp.id;
        ObjInc1.vic_Status__c='HR - Pending';
        ObjInc1.vic_VIC_Grid__c='10';
        insert objInc1;
        test.stoptest();    
        
        
    }
    
    
}