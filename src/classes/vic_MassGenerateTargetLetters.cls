public class vic_MassGenerateTargetLetters {
    
    @AuraEnabled
    public static List<String> getFinancialYear(){
        
        List<String> lstFinancialYear=new List<String>();
        
        Integer currentYear = System.Today().year();
        Integer previousYear=currentYear - 1;
        lstFinancialYear.add(String.valueof(currentYear));
        lstFinancialYear.add(String.valueof(previousYear));
        lstFinancialYear.sort();
        return lstFinancialYear;
    }
    
    @AuraEnabled
    public static List<wrapperVicRole> getVicRole(Integer currentFinancialYear){
        List<wrapperVicRole> objWrprLst=new List<wrapperVicRole>();
        List<Master_VIC_Role__c> lst= [select id,name from Master_VIC_Role__c where is_Active__c=true AND Year__c =:String.valueOf(currentFinancialYear) order by name];
        
        for(Master_VIC_Role__c role:lst){
            wrapperVicRole obj=new wrapperVicRole();
            obj.strId=role.Id;
            obj.name=role.name;
            objWrprLst.add(obj);
        }
        
        return objWrprLst;
    }
    /*REFACTORED BY RISHABH PILANI*/        
    @auraenabled
    public static List<Target__c> getTargetRecords(String vicRole, String status, String financialYear){
        List<Target__c> dataSource = new List<Target__c>();
        Set<Id> setUserId = new Set<Id>();
        
        for(User_VIC_Role__c uvr: [SELECT User__c 
                                    FROM User_VIC_Role__c 
                                    WHERE Master_VIC_Role__c= :vicRole 
                                    AND Not_Applicable_for_VIC__c=false 
                                    AND User_VIC_Role__c.User__r.IsActive=true]){
            setUserId.add(uvr.User__c);
        }
        
        Date dtTargetStartDate = Date.newInstance(Integer.valueOf(financialYear), 1, 1);
        
        if(status.equalsIgnoreCase('Draft')){
            dataSource = [SELECT Id,Is_Revised__c,Revised_Target_Letter_shared__c,Status__c,Start_date__c,Plan__r.Name,
                            User__r.Name,User__r.Email 
                            FROM Target__c 
                            WHERE User__c in :setUserId 
                            AND Start_date__c = :dtTargetStartDate
                            AND Status__c = :status
                            LIMIT 100];
        }else if(status.equalsIgnoreCase('Revised')){
            dataSource = [SELECT Id,Is_Revised__c,Revised_Target_Letter_shared__c,Status__c,Start_date__c,Plan__r.Name,
                            User__r.Name,User__r.Email 
                            FROM Target__c 
                            WHERE User__c in :setUserId 
                            AND Start_date__c = :dtTargetStartDate                            
                            AND Is_Revised__c = TRUE
                            AND Revised_Target_Letter_shared__c = FALSE
                            AND Status__c NOT IN ('Exit','On Hold','Not Applicable')
                            LIMIT 100];
        }
        return dataSource;        
    }
    
    /*ADDED BY RISHABH PILANI*/
    @AuraEnabled
    public static String setUpdateTargetRecords(List<Id> targetIds, String strStatus){
        system.debug(targetIds + 'targetIds');
        system.debug(strStatus + 'strStatus');
        if(targetIds !=  null && targetIds.size() > 0){
            try
            {
                List<Target__c> lstTargets = new List<Target__c>();
                for(Id recordId : targetIds){
                    if(strStatus.equalsIgnoreCase('Draft')){
                        lstTargets.add(new Target__c(
                            Id = recordId,
                            Letter_Send_Status__c = TRUE,
                            Status__c='Released'
                        ));
                    }else if(strStatus.equalsIgnoreCase('Revised')){
                        lstTargets.add(new Target__c(
                            Id = recordId,
                            Revised_Target_Letter_shared__c = TRUE,
                            Letter_Send_Status__c = TRUE
                        ));
                    }
                }
                if(lstTargets.size() > 0){
                    update lstTargets;
                }
                return 'SUCCESS';
            }catch(Exception ex){
                throw new AuraHandledException(ex.getMessage());
            }
        }else{
            throw new AuraHandledException('No target records are selected to perform an action.');
        }
    }
      
    
    public class wrapperVicRole{
        
        @auraenabled
        public string strId;
        @auraenabled
        public string name;
        
    }
            
    public class wrapperSessionId{
        
        @auraEnabled
        public  List<String> lstEmail;
        @auraEnabled
        public string strSessionId;
    }   
    
}