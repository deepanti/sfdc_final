/**
* @author Anmol.kumar
* @date 22/10/2017
*
* @group ProjectAssignment
* @group-content ../../ApexDocContent/ProjectAssignment.html
*
* @description Apex Service for ProjectAssignment module,
* has aura enabled method for ProjectAssignment.
*/
public Class GPCmpServiceProjectAssignment {
    /**
    * @description Aura enabled method to get User list to whom project can be assigned.
    * @param Id projectId
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAssignment.getUserlist(<strProjectID>);
    */
    @AuraEnabled
    public static GPAuraResponse getUserlist(string strProjectID) {
        GPControllerProjectAssignment assignmentController = new GPControllerProjectAssignment(strProjectId);
        return assignmentController.getUserlist();
    }

    /**
    * @description Aura enabled method to reassign project to owner.
    * @param Id projectId
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAssignment.assignToOwner(<projectId>);
    */
    @AuraEnabled
    public static GPAuraResponse assignToOwner(string strProjectId) {
        GPControllerProjectAssignment assignmentController = new GPControllerProjectAssignment(strProjectId);
        return assignmentController.assignToOwner();
    }
    
    /**
    * @description Aura enabled method to get Project detail.
    * @param Id strProjectId
    * @param Id strUserID
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAssignment.updateCurrentWorkingUser(<strProjectId>, <strUserID>);
    */
    @AuraEnabled
    public static GPAuraResponse updateCurrentWorkingUser(string strProjectId, string strUserID, Boolean changeOwner) {
        GPControllerProjectAssignment assignmentController = new GPControllerProjectAssignment(strProjectId);
        assignmentController.setChangeOwner(changeOwner);
        return assignmentController.updateCurrentWorkingUser(strUserID);
    }
}