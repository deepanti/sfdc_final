public with sharing class GPSelectorProjectVersionHistory extends fflib_SObjectSelector {
	
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Project_Version_History__c.Name
           
            };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Version_History__c.sObjectType;
    }

    public List<GP_Project_Version_History__c> selectById(Set<ID> idSet) {
        return (List<GP_Project_Version_History__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Project_Version_History__c> selectByVerionDraft(Set<string> setOfParentProjectId) {
        return (List<GP_Project_Version_History__c>) [Select id, name, GP_Status__c, GP_Version_No__c, GP_Project__c from GP_Project_Version_History__c where
        																	GP_Project__c in : setOfParentProjectId order by GP_Version_No__c desc limit 2];
    }
    
    public List<GP_Project_Version_History__c> selectByProject(Set<Id> setOfProjectId) {
        return (List<GP_Project_Version_History__c>) [Select id, name, GP_Status__c, GP_Project__c from GP_Project_Version_History__c where
        																	GP_Project__c in : setOfProjectId];
    }
}