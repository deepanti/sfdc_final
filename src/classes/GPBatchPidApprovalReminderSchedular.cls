global class GPBatchPidApprovalReminderSchedular  implements schedulable {

    global void execute(SchedulableContext sc)
    {
        GPBatchPidApprovalReminder objReminder1 = new GPBatchPidApprovalReminder(1,'1');
		Database.executebatch(objReminder1); 

		GPBatchPidApprovalReminder objReminder2 = new GPBatchPidApprovalReminder(2,'2');
		Database.executebatch(objReminder2);  
		
		GPBatchPidApprovalReminder objReminder3 = new GPBatchPidApprovalReminder(3,'3');
		Database.executebatch(objReminder3);  
    }
}