public class ProductRevenueScheduleTriggerHelper {
    
    public static void insertRevenueScheduleForOLI(List<Product_Revenue_Schedule__c> revenueScheduleList){
        try{
            Set<ID> OLI_Id_List = new Set<ID>();
            List<OpportunityLineItemSchedule> toBeDeletedList = new List<OpportunityLineItemSchedule>();
            List<OpportunityLineItemSchedule> toBeInsertedList = new List<OpportunityLineItemSchedule>();
            
            Map<String, SObjectField> productScheduleFieldMap = SObjectType.Product_Revenue_Schedule__c.fields.getMap();
            System.debug('productScheduleFieldMap=='+productScheduleFieldMap);
            
            Decimal revenue;
            
            for(Product_Revenue_Schedule__c productRevenueSchedule : revenueScheduleList){
                OLI_Id_List.add(productRevenueSchedule.opportunity_Product_Id__c);
            }        
            Map<ID, OpportunityLineItem> OLI_Map = new Map<ID, OpportunityLineItem>([SELECT ID, (SELECT ID FROM OpportunityLineItemSchedules)
                                                                                     FROM OpportunityLineItem WHERE ID IN : OLI_Id_List]);
            
            for(Product_Revenue_Schedule__c productRevenueSchedule : revenueScheduleList){
                Date scheduleDate;
                
                OpportunityLineItem oppProduct = OLI_Map.get(productRevenueSchedule.Opportunity_Product_ID__c);
                toBeDeletedList.addAll(oppProduct.OpportunityLineItemSchedules); 
                
                for(Integer i=1; i<=24; i++){
                    System.debug('value in product schedule =='+productScheduleFieldMap.get('M'+i+'_revenue__c'));
                    revenue = (Decimal)productRevenueSchedule.get(productScheduleFieldMap.get('M'+i+'_revenue__c')); 
                    
                    if(revenue != null && revenue >0){
                        
                        OpportunityLineItemSchedule opp_schedule = new OpportunityLineItemSchedule();
                        opp_schedule.OpportunityLineItemId = oppProduct.Id;
                        opp_schedule.Type = 'Revenue';
                        opp_schedule.revenue = revenue;   
                        
                        if(scheduleDate == NULL){
                            opp_schedule.ScheduleDate = productRevenueSchedule.Revenue_Start_Date__c; 
                            scheduleDate = productRevenueSchedule.Revenue_Start_Date__c.addMonths(1);
                        }
                        else{
                            opp_schedule.ScheduleDate  = scheduleDate;
                            scheduleDate = scheduleDate.addMonths(1);
                        }
                        toBeInsertedList.add(opp_schedule);    
                    }                
                }
                if((Decimal)productRevenueSchedule.get(productScheduleFieldMap.get('M25_revenue__c')) > 0){
                    OpportunityLineItemSchedule opp_schedule = new OpportunityLineItemSchedule();
                    opp_schedule.OpportunityLineItemId = oppProduct.Id;
                    opp_schedule.Type = 'Revenue';
                    opp_schedule.revenue = (Decimal)productRevenueSchedule.get(productScheduleFieldMap.get('M25_revenue__c'));  
                    opp_schedule.ScheduleDate  = scheduleDate;
                    toBeInsertedList.add(opp_schedule); 
                }
            }
            if(toBeInsertedList.size()>0){
                delete toBeDeletedList;
                insert  toBeInsertedList;
                OpportunityLineItemTriggerHelper.updateYearlyRevenue(OLI_Id_List);   
            }    
        }
        catch(Exception e){
            System.debug('ERROR=='+e.getMessage()+' '+e.getLineNumber()+' '+e.getStackTraceString()); 
        }
    }
}