// ------------------------------------------------------------------------------------------------ 
// Description: this class is used for add the project sharing on the respective date
// ------------------------------------------------------------------------------------------------
// Created Date: 2-Nov-2017  Created By: Adarsh  Email: adarsha.nanda@saasfocus.com 
// ------------------------------------------------------------------------------------------------ 
global class GPBatchHandleShareLeadership implements Database.Batchable<sObject>
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([SELECT id, GP_Start_Date__c, GP_End_Date__c, GP_Employee_ID__c, GP_Employee_ID__r.GP_SFDC_User__c, GP_Project__c 
        									from GP_Project_Leadership__c where 
        									GP_Start_Date__c = TODAY or 
        									GP_End_Date__c = TODAY]);
    }
    
    global void execute(Database.BatchableContext BC,list<GP_Project_Leadership__c> lstLeaderShip)
    {
    	if(lstLeaderShip != null && lstLeaderShip.size() > 0)
    	{
    		for(GP_Project_Leadership__c EachLeader : lstLeaderShip)
    		{
    			if(EachLeader.GP_Start_Date__c == system.today())
    			{
    				Map<String, Object> mapOfParams = new Map<String, Object>();
			        mapOfParams.put('strEvent', 'Add');
			        mapOfParams.put('strProjectId', EachLeader.GP_Project__c);
			        mapOfParams.put('strUserId', EachLeader.GP_Employee_ID__r.GP_SFDC_User__c);
			        Flow.Interview.GP_ProjectAccess_from_Leadership objFlow = new Flow.Interview.GP_ProjectAccess_from_Leadership(mapOfParams);
			        objFlow.start(); 
    			}
    			
    			else if(EachLeader.GP_End_Date__c == system.today())
    			{
    				Map<String, Object> mapOfParams = new Map<String, Object>();
			        mapOfParams.put('strEvent', 'Remove');
			        mapOfParams.put('strProjectId', EachLeader.GP_Project__c);
			        mapOfParams.put('strUserId', EachLeader.GP_Employee_ID__r.GP_SFDC_User__c);
			        Flow.Interview.GP_ProjectAccess_from_Leadership objFlow = new Flow.Interview.GP_ProjectAccess_from_Leadership(mapOfParams);
			        objFlow.start(); 
    			}
    		}
    	}
    }
    
    global void finish(Database.BatchableContext BC)
    {

    }
}