public class GPSelectorEmployeeHR extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Employee_HR_History__c.Name,
                GP_Employee_HR_History__c.Id
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Employee_HR_History__c.sObjectType;
    }
    
    public List<GP_Employee_HR_History__c> selectById(Set<ID> idSet) {
        return (List<GP_Employee_HR_History__c>) selectSObjectsById(idSet);
    }
    
    public GP_Employee_HR_History__c selectById(ID masterId) {
        Set<Id> idSet = new Set<Id> {
            masterId
                };
                    return (GP_Employee_HR_History__c) selectSObjectsById(idSet).get(0);
    }
    
    public static List<GP_Employee_HR_History__c> selectInactiveRecordsForAMonthYear(Id employeeId , Date monthStartDate , Date monthEndDate) {
        return [select id,GP_Employees__c,GP_Allocation_Status__c,GP_ASGN_EFFECTIVE_END__c, GP_ASGN_EFFECTIVE_START__c 
                from GP_Employee_HR_History__c where GP_Employees__c =:employeeId and GP_Allocation_Status__c != 'Active Assignment'
                and (GP_ASGN_EFFECTIVE_END__c<= :monthEndDate OR GP_ASGN_EFFECTIVE_START__c>= :monthStartDate)];
    }
    public static List<GP_Employee_HR_History__c> getEmployeeHRRecordsInDescOrder(Set<Id> setOfEmployeeIds) {
        return [select id,GP_Employees__c,GP_Allocation_Status__c,GP_ASGN_EFFECTIVE_END__c, GP_ASGN_EFFECTIVE_START__c 
                from GP_Employee_HR_History__c where GP_Employees__c in :setOfEmployeeIds
                ORDER BY GP_ASGN_EFFECTIVE_END__c DESC NULLS LAST];
    }
    public List<GP_Employee_HR_History__c> getEmployeeHRBasedOnEmp(Set<Id> setOfEmpId){
        return [select id,GP_ASGN_EFFECTIVE_END__c,GP_ASGN_EFFECTIVE_START__c,GP_Org_Id__c,GP_Employees__c,GP_Batch_Number__c  from 
                GP_Employee_HR_History__c where GP_Employees__c =: setOfEmpId];
        
    }
    public List<GP_Employee_HR_History__c> getEmployeeHRRecordsInDescending(Set<Id> setOfEmployeeIds){
        return [select id,GP_Employees__c,GP_ASSIGNMENT_STATUS__c,GP_Allocation_Status__c,GP_ASGN_EFFECTIVE_END__c,
                GP_ASGN_EFFECTIVE_START__c from GP_Employee_HR_History__c where GP_Employees__c in :setOfEmployeeIds
                                                      ORDER BY GP_ASGN_EFFECTIVE_END__c DESC NULLS LAST];
        
    }
}