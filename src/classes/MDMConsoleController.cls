/**********************************************************************************
@Original Author : Iqbal
@Modified By Author : Iqbal
@TL : Madhuri
@date Started : 25 Jan 2019
@Description : To Store user interest/purpose on MDM COnsole object for what he/she wishes to see the MDM report , including Case creation as well
@Parameters : 
@Return type :
**********************************************************************************/



public class MDMConsoleController {
    
    @auraEnabled
    public static Wraperclass RequestReasonList(){
        try{
            Wraperclass objwraper = new Wraperclass();
            
            list<string> RequestReasonPicklistValue = Utility.getPickListValue('MDM_Console__c','Request_Reason__c');
            system.debug(':==='+RequestReasonPicklistValue);
            list<selectOption> wraperList = new list<selectOption>();
            for(string s : RequestReasonPicklistValue){
                selectOption sl = new selectOption();
                sl.label = s;
                sl.value = s;
                wraperList.add(sl);
            }
            objwraper.requestResponseList = wraperList;
            
            list<string> buyinglst = Utility.getPickListValue('Case','Buying_Centre_multi__c');
            list<BuyingOption> bying = new list<BuyingOption>();
            for(string s : buyinglst){
                BuyingOption bo = new BuyingOption();
                bo.label = s;
                bo.value = s;
                bying.add(bo);
            }
            objwraper.buyinghList  = bying;
            
            list<string> levellst = Utility.getPickListValue('Case','Level_Of_Contact__c');
            list<LevelOption> levellts = new list<LevelOption>();
            for(string s : levellst){
                LevelOption lo = new LevelOption();
                lo.label = s;
                lo.value = s;
                levellts.add(lo);
            }
            objwraper.Levellist  = levellts;
            
            list<string> counrtylst = Utility.getPickListValue('Case','Country_Of_Residence__c');
            list<CountryOption> countylts = new list<CountryOption>();
            for(string s : counrtylst){
                CountryOption co = new CountryOption();
                co.label = s;
                co.value = s;
                countylts.add(co);
            }
            objwraper.Countrylist  = countylts;
            
            list<string> industrylst = Utility.getPickListValue('Case','Industry_Vertical_multi__c');
            list<IndustryOption> industrylts = new list<IndustryOption>();
            for(string s : industrylst){
                IndustryOption io = new IndustryOption();
                io.label = s;
                io.value = s;
                industrylts.add(io);
            }
            objwraper.Industrylist  = industrylts;
            
            list<Archetype__c> Archetypelst = [SELECT Name FROM Archetype__c];
            list<ArchetypeOption> Archetypelts = new list<ArchetypeOption>();
            for(Archetype__c s : Archetypelst){
                ArchetypeOption at = new ArchetypeOption();
                at.label = s.Name;
                at.value = s.Name;
                Archetypelts.add(at);
            }
            
            objwraper.ArchetypeList  = Archetypelts;
            
            return objwraper;
           
        }catch(exception e){
            system.debug(':==error '+e.getMessage()+':===error line '+e.getLineNumber());
        }
        return null;
    }
    
    @auraEnabled
    public static List<Account> getAccountId(List<String> AccName){
        List<Account> AccId = [SELECT Id FROM Account WHERE Name =: AccName];
        return AccId;
    }
    
    @auraEnabled
    public static string CreateCaseRecord(String IndustryVertical,String title,String buyingcenter,String levelcontract,String countrySelect,String accountArctye,String validreason,String accLookupvalue,String campaignName){
        try{
            system.debug(':-IndustryVertical-:'+IndustryVertical+':-title-:'+title+':-buyingcenter-:'+buyingcenter+':-levelcontract-:'+levelcontract+':-countrySelect-:'+countrySelect+':-accountArctye-:'+accountArctye+':-validreason-:'+validreason+';=====campaignName===:'+campaignName);                                       
            list<string> industry = IndustryVertical.split(',');
            list<string> buyingcentry = buyingcenter.split(',');
            list<string> countrysel = countrySelect.split(',');
            list<string> accarchtype = accountArctye.split(',');
            list<string> levelcont = levelcontract.split(',');
            String induspick = getmultipicklist(industry);
            String buyingpick = getmultipicklist(buyingcentry);
            String countrypick = getmultipicklist(countrysel);
            String accountarchpick = getmultipicklist(accarchtype);
            String levelcontractpick = getmultipicklist(levelcont);
            
            
            case objcase = new case();
            objcase.Title__c = title;
            system.debug(':==accLookupvalue==:'+accLookupvalue);
            objcase.AccountId = accLookupvalue ;
            objcase.Buying_Centre_multi__c = buyingpick;
            objcase.Level_Of_Contact__c = levelcontractpick;
            objcase.Country_Of_Residence__c = countrypick;
            objcase.Account_Archetype__c = accountarchpick;
            objcase.Please_provide_valid_reason_for_MDMcase__c = validreason;
            objcase.Industry_Vertical_multi__c = induspick;
            objcase.RecordTypeId=label.MDM_Case_Record_Type_Id;
            objcase.Campaign_Name__c= campaignName;
            insert objcase;
            system.debug('Case ID'+objcase.id);
            EmailSender(objcase.id);
            return objcase.id;
            
        }catch (exception e){
            system.debug(':====error msg== :'+e.getMessage()+':=====error line=== :'+e.getLineNumber());
            return null;
        }
    }
    
    @auraEnabled
    public static string getmultipicklist(list<string> pickvalue){
        try{ 
            string picklist;
            for(string s : pickvalue){
                if(picklist != null){
                    picklist = picklist +';'+ s;
                }else{
                    picklist = s;
                }
            }
            return picklist;
        }catch (exception e){
            system.debug(':====error msg== :'+e.getMessage()+':=====error line=== :'+e.getLineNumber());
            return null;
        }   
    }
    
    
    @auraEnabled
    public static void EmailSender(Id CaseId){
        try{
            list<string> email = new list<string>();
            /*list<Case__c> cs = Case__c.getAll().values();
            for(case__c ce: cs)  {
                email.add(ce.PowerBi_Email__c); //PowerBi_Email__c coming from custom Setting
            }*/
            String Addr = label.MDM_Email_To_Address;
			list<string> Add = Addr.split(';');
			for(String s : Add){
			email.add(s);
			}        
            
            EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:'MDMCaseTemplate'];
            List<Messaging.SingleEmailMessage> CaseEmail = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(email);
            mail.setCcAddresses(new String[] { label.MDM_Email_Cc_Address});
            mail.setSenderDisplayName('MDM Team');
            mail.setTreatTargetObjectAsRecipient(false);
            mail.setBccSender(false);
            mail.setTemplateId(et.id);
            mail.setTargetObjectId(label.MDM_Email_TargetObjId);  
            mail.setWhatId(CaseId);
            CaseEmail.add(mail);
            if(CaseEmail != null && CaseEmail.size() > 0){
                system.debug('Email Sent==========:');
                Messaging.sendEmail(CaseEmail); 
                system.debug('CaseEmail After======'+CaseEmail);
            }   
        }catch (exception e){
            system.debug(':====error msg== :'+e.getMessage()+':=====error line=== :'+e.getLineNumber());
            
        }
    }
    
    
    @auraEnabled
    public static boolean CreateRecord(String selectValue,String othersVal){
        
        system.debug(':---value----:'+selectValue+':====othersVal=== :'+othersVal);
        try{
            
            MDM_Console__c objmdm = new MDM_Console__c();
            if(selectValue != null){
                list<string> pickVal = selectValue.split(',');
                string pick;
                for(string s : pickval){
                    if(pick != null){
                        pick = pick + ';' + s;
                    }else{
                        pick = s;
                    }
                }
                objmdm.Request_Reason__c = pick;
            }
            if(othersVal != null){
                objmdm.Other_Request_Reason__c = othersVal;
            }
            if(objmdm != null){
                insert objmdm;
            }
            
            return true;
        }catch(Exception e){
            system.debug(':===error msg === :'+e.getMessage()+':=====error line===== :'+e.getLineNumber());
            return false;
        }
    }
    
    public class Wraperclass{
        @auraEnabled public list<selectOption> requestResponseList;
        @auraEnabled public list<BuyingOption> buyinghList;
        @auraEnabled public list<LevelOption> Levellist;
        @auraEnabled public list<CountryOption> Countrylist;
        @auraEnabled public list<IndustryOption> Industrylist;
        @auraEnabled public list<ArchetypeOption>  ArchetypeList;
    }
    public class ArchetypeOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
    public class SelectOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
    public class IndustryOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
    public class BuyingOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
    public class LevelOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
    public class CountryOption{
        @auraEnabled public string label;
        @auraEnabled public string value;
    }
}