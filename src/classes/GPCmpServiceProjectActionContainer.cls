/**
* @author Pankaj.adhikari
* @date 28/3/2018
*
* @group ProjectActionContainer
* @group-content ../../ApexDocContent/ProjectActionContainer.html
*
* @description Apex Service for Project Action Container module,
* has aura enabled method for Project Action Container.
*/

public class GPCmpServiceProjectActionContainer {
	
    @AuraEnabled
    public static GPAuraResponse getUserAccess(string strRecordId) {
    	GPControllerProjectActionContainer objProjectAction = new GPControllerProjectActionContainer();
        return objProjectAction.getActionAccess(strRecordId);
    }

     /*
     Author :Anoop Verma
     Date: 22 July 2020
     CR:CRQ000000076988
     Description:Block modify button until  Resources are not synced. 
     
     */
     @AuraEnabled
    public static string isResourceSync(String strRecordId)
    {        
       string result= 'Y';
       List<GP_Resource_Allocation__c> lstResourceAllocation = [select ID, GP_Oracle_Id__c, GP_Oracle_Status__c, 
                                        GP_Project__r.GP_Oracle_PID__c from GP_Resource_Allocation__c 
                                        where GP_IsUpdated__c =true and GP_Oracle_Status__c =''  and
										GP_Project__r.id=:strRecordId and GP_Project__r.GP_Oracle_Status__c ='S' and GP_Project__r.GP_Oracle_PID__c!='NA'];
        
        if(lstResourceAllocation.size()>0)
        {
             
            result='N';
            
        }
         return result;
      }
}