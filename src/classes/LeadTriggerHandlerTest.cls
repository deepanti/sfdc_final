/********************************************************************************************************
Name           : LeadTriggerHandlerTest
Created By     : Ryan Macias (The Pedowitz Group)
Created Date   : February 5, 2015
Description    : This class contains unit tests for validating the behavior of Lead Triggers & Classes
               : The @isTest class annotation indicates this class only contains test methods.
               : Classes defined with the @isTest annotation do not count against the organization size 
               : limit for all Apex scripts.

Modified BY    : 
Modified Date  : 
Revisions      : 
*********************************************************************************************************/

@isTest
private class LeadTriggerHandlerTest {

    static testMethod void checkLeadAssignment() {
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    	User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
    	Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
    	Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
    	Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
    	Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');            
        Account newAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test newAccount','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');            
        // Create Test Leads
        List<Lead> testLeads = new List<Lead>{};
        
        for(Integer i = 0; i < 2; i++){
            Lead l = new Lead(lastname='Test1_' + i,firstname='Lead1', company='TestCo', email='testlead1-' + i + '@testco.com',
            status='Engaged', CurrencyIsoCode='USD', Account__c=oAccount.Id, Authority_or_P_to_p__c='Decision Maker', Need__c='Asset optimization');
            testLeads.add(l);
        }
         for(Integer i = 0; i < 2; i++){
            Lead l = new Lead(lastname='Test1_' + i,firstname='Lead1', company='TestCo', email='testlead1-' + i + '@testco.com',
            status='Disqualified',Disqualified_Reason__c='Not Target Account', CurrencyIsoCode='USD', Account__c=oAccount.Id, Authority_or_P_to_p__c='Decision Maker', Need__c='Asset optimization');
            testLeads.add(l);
        }
        insert testLeads;
        
        // Query the database for the inserted test leads
        List<Lead> insertedLeads = [SELECT Id, status, ownerId,Account__c FROM Lead WHERE Id IN :testLeads];
        
        for(Lead leads : insertedLeads){
        	//leads.status='Marketing Qualified';
        	leads.status='Raw';
           
        }
        
        update insertedLeads;
        for(Lead leads : insertedLeads){
        	//leads.status='Marketing Qualified';
        	leads.status='Disqualified';
           
        }
        
        update insertedLeads;
         // Query the database for the udated test leads
        List<Lead> updatedLeads = [SELECT Id, status, ownerId FROM Lead WHERE Id IN :testLeads];
        
        for(Lead leads2 : updatedLeads){
        	//System.assertEquals(oAccount.ownerId, leads2.ownerId);
        }
        
        // Delete test leads for test coverage
        delete updatedLeads;
        undelete updatedLeads;
    }
   
    public static testmethod void updateTest()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    	User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
    	Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
    	Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
    	Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
    	Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');            
        Account newAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test newAccount','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');            
        // Create Test Leads
        final Lead oldLead=new lead();
        
        List<Lead> lstOldLd = new List<Lead>();
        List<Lead> lstNewLd = new List<Lead>();
        
        Map<Id,Lead> oldMpLead=new Map<Id,Lead>();
        Map<Id,Lead> newMpLead=new Map<Id,Lead>();
        
        Lead l = new Lead(lastname='Test1',firstname='Lead1', company='TestCo', email='testlead1'+ '@testco.com',
        status='Engaged', CurrencyIsoCode='USD', Account__c=oAccount.Id, Authority_or_P_to_p__c='Decision Maker', Need__c='Asset optimization'); 
        insert l; 
  
        l.status='Disqualified';
        l.Disqualified_Reason__c='Not Target Account';
        l.Account__c=newAccount.Id;
        oldLead.Id=l.Id;
        
        lstOldLd.add(oldLead);
        oldMpLead.put(oldLead.Id,oldLead);
        
        update l;
        lstNewLd.add(l);
        newMpLead.put(l.Id,l);

        new LeadTriggerHandler().OnBeforeUpdate(lstOldLd, lstNewLd, newMpLead, oldMpLead);
        l.Account__c=NULL;
        update l;
        lstNewLd.add(l);
        newMpLead.put(l.Id,l);
        new LeadTriggerHandler().OnBeforeUpdate(lstOldLd, lstNewLd, newMpLead, oldMpLead);
     
    }
    public static testmethod void exceptionCheck()
    {
       
        List<Lead> lstNewLd;
        
        Map<Id,Lead> oldMpLead;
        
        try{
           new LeadTriggerHandler().accountToLeadFieldUpdate(lstNewLd);
           new LeadTriggerHandler().accountToLeadFieldUpdate(lstNewLd, oldMpLead);
            
        }
        catch(Exception e)
        {
            system.debug('Exception'+e);
        }
    }
    
}