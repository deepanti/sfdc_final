public class ContactLifeCycleHelper {
         
        public static void contactLifeCycleMethod(list<opportunity> oppList,map<id,opportunity> oldOppMap){
            string oppId = string.valueof(oldOppMap.keyset());
            try{
                system.debug(':==oppList==:'+oppList.size());
                set<id> conId = new set<id>();
                Map<Id,List<Opportunity>> conMap = new Map<Id,List<Opportunity>>();
                list<opportunity> newOppList = new list<opportunity>();
                list<opportunity> openOppList = new list<opportunity>();
                list<opportunity> wonOppList = new list<opportunity>();
                list<opportunity> lostOppList = new list<opportunity>();
                for(opportunity opp : oppList){
                    system.debug(':====oppstageName===:'+opp.stageName);
                    system.debug(':====oldOppMap.get(opp.Id).stageName===:'+oldOppMap.get(opp.Id).stageName);
                    system.debug(':===opp.Contact__c===:'+opp.Contact1__c);
                    if(opp.stageName != oldOppMap.get(opp.Id).stageName || test.isRunningTest()){
                        system.debug(':===changestage===:');
                        if(opp.stageName == '6. Signed Deal' || opp.stageName == '7. Lost' || opp.stageName == '8. Dropped'){
                            system.debug(':===close opp changestage===:');
                            system.debug(':===opp.Contact__c===:'+opp.Contact1__c);
                            if(opp.Contact1__c != null ) {
                                conId.add(opp.Contact1__c); 
                                conMap.put(opp.Contact1__c,newOppList);
                                conMap.get(opp.Contact1__c).add(opp);
                            }
                        }  
                    }
                }
                
                system.debug(':==conId--: '+conId);
                if(conId.size() > 0 || test.isRunningTest()){
                    set<string> stageName = new set<string>{'Prediscover', '1. Discover','2. Define','3. On Bid','4. Down Select','5. Confirmed'};
                        
                        Map<Id,List<Opportunity>> openOppMap = new Map<Id,List<Opportunity>>();
                    Map<Id,List<Opportunity>> lostOppMap = new Map<Id,List<Opportunity>>();
                    Map<Id,List<Opportunity>> wonOppMap = new Map<Id,List<Opportunity>>();
                    Set<Id> openOppId = new Set<Id>();
                    Set<Id> wonOppId = new Set<Id>();
                    Set<Id> lostOppId = new Set<Id>();
                    integer limt = Limits.getQueryRows();
                    //integer limt = Limits.getLimitQueryRows();getQueryRows()
                    system.debug(':==limt=='+limt);
                    list<opportunity> oppLst = new list<opportunity>([select Id, stageName,Contact1__c from opportunity where Contact1__c IN : conMap.keySet()]);
                    system.debug(':====oppLst===:'+oppLst.size()+':===oppLst==:'+oppLst);
                    for(opportunity opp : oppLst){
                        if(stageName.contains(opp.stageName)){
                            
                            openOppMap.put(opp.Contact1__c,openOppList);
                            openOppMap.get(opp.Contact1__c).add(opp);
                            openOppId.add(opp.id);
                        }else if(opp.StageName == '7. Lost' || opp.StageName == '8. Dropped'){
                            lostOppMap.put(opp.Contact1__c,lostOppList);
                            lostOppMap.get(opp.Contact1__c).add(opp);
                            lostOppId.add(opp.id);
                        }else if(opp.StageName == '6. Signed Deal'){
                            wonOppMap.put(opp.Contact1__c,wonOppList);
                            wonOppMap.get(opp.Contact1__c).add(opp);
                            wonOppId.add(opp.id);
                        }
                    }
                    list<contact> conlst = new list<contact>();
                    system.debug(':====:'+conMap.keyset());
                    system.debug(':==openOppMap.get(str).size()===:'+openOppMap.size());
                    system.debug(':==wonOppMap.get(str).size()===:'+wonOppMap.size());
                    system.debug(':===lostOppMap.get(str).size()==:'+lostOppMap.size());
                    for(string str : conMap.keyset()){
                        contact con = new contact();
                        con.id = str;
                        system.debug(':==openOppMap.size();;'+openOppMap.size());
                        if(openOppMap.size() > 0 && openOppMap.get(str) != Null  ){
                            system.debug(':==openOppMap.get(str).size()===:'+openOppMap.get(str).size());
                        }
                        
                        
                        if(wonOppMap.size() > 0 &&  wonOppMap.get(str) != null){
                            if(wonOppMap.get(str).size() > 0)
                                con.Status__c ='Closed Won';
                        }else if(openOppMap.size() > 0 && openOppMap.get(str) != null ){
                            
                        
                        }else if(lostOppMap.size() > 0 && lostOppMap.get(str)  != null){
                            if(lostOppMap.get(str).size() > 0)
                                con.Status__c ='Closed Lost';
                        }
                        conlst.add(con);
                    }
                    if(conlst.size() > 0){
                        system.debug(':===conlst==:'+conlst.size()+':====conlst==:'+conlst);
                        update conlst;
                    }
                }
                
            }catch(Exception e){ //ContactLifeCycleHelper.contactLifeCycleMethod
                system.debug(':==Error Message--: '+e.getMessage()+':===Error Line--: '+e.getLineNumber());
                CreateErrorLog.createErrorRecord(oppId,e.getMessage(), '', e.getStackTraceString(),'ContactLifeCycleHelper', 'contactLifeCycleMethod','Fail','',String.valueOf(e.getLineNumber())); 
                
            }
            
        }
    }