//Created BY : mandeep.singh@saasfocus.com
global class GPBatchtoExpiringProjectSchedular implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        GPBatchtoExpiringProject objGPBatchtoExpiringprjct = new GPBatchtoExpiringProject(); //ur batch class
        database.executebatch(objGPBatchtoExpiringprjct,200);
    }
}