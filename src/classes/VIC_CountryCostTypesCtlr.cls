/*
* @author: Prahant Kumar1
* @since: 23/05/2018
* @description: this class contains method to get data 
* related to vic_Country_Cost_Type__mdt
* 
*/
public class VIC_CountryCostTypesCtlr {
    @TestVisible
    private static Map<String, vic_Country_Cost_Type__mdt> lableToMetadataMap = new Map<String, vic_Country_Cost_Type__mdt>();
    static{
        for(vic_Country_Cost_Type__mdt objMetaData: [SELECT Label, vic_Cost_Type__c FROM vic_Country_Cost_Type__mdt]){
            lableToMetadataMap.put(objMetaData.label, objMetaData);
        }
    }
    /*
     * @params: Country(String)
     * @return: isLowCost(Boolean)
     * @description: This methods returns true if Cost_Type__c is 'Low Cost' otherwise false
     * 
    */
    @TestVisible
    private static Boolean isLowCostCountry(String country){
        return lableToMetadataMap.containsKey(country) ? lableToMetadataMap.get(country).vic_Cost_Type__c == 'Low Cost' : false;
    }
    
    /*
     * @params: User Country(String),
     *          Opportunity Country(String)
     * @return: Boolean
     * @description: This method test for whether combination of opportunity
     *               sales country and user domecile country is eligible for PPP multiplier or not
     * 
    */
    public static Boolean isQualifiedForPPP(String userCountry, String oppCountry){
        return isLowCostCountry(userCountry) && !isLowCostCountry(oppCountry);
    }
}