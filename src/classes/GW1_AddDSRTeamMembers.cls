// Class is Controller extention class of vfpage GW1_ADDDSRTeamMember
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1.0        03-10-2016       Rishi Kumar              
// ---------------------------------------------------------------------------------------------
// V 1.1 bootstrap save state 3-29-16
// v update 1.1.1 |bug fixed -after shortlisting, users re-apiering in search result

public class GW1_AddDSRTeamMembers {
    
    public list<WrapperOfUser> shortlistedUserWrapper { get; set; }
    
    Public list<WrapperOfUser> listWrapperOfUser {get;set;}
    public list<User> lstUsers {get;set;}
    public list<String> listA {get;set;}
    public set<String> setA {get;set;}
    public boolean errorList {get; set;}
    public boolean semi {get; set;}
    public boolean caseDetails {get; set;}
    public String caseNo {get; set;}
    public string team { get; set; }
    public string vertical { get; set; }
    public string Geography { get; set; }
    public string searchemployeeID { get; set; }
    public Integer delRowNum { get; set; }
    public string shortlistedUserIndex { get; set; }
    public boolean showFilterError { get; set; }
    public boolean showSaveError { get; set; }
    private list<GW1_DSR_Team__c> listInsertDsr;
    public Id dsrId{get;set;}
    private PageReference cancel;
    public string jsonString {get;set;}
    public List<SelectOption> lstoptionsTeam {get; set;}
    public List<SelectOption> lstGeographyoptions{get; set;}
    public List<SelectOption> lstVerticaloptions{get; set;}
    public List<SelectOption> lstRoleoptions{get; set;}
    
    /*
Constructor of GW1_AddDSRTeamMembers 
--------------------------------------------------------------------------------------
Name            Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi         03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    public GW1_AddDSRTeamMembers(ApexPages.StandardController controller)
    {       
        dsrId = ApexPages.currentPage().getParameters().get('Id');
        shortlistedUserWrapper = new list<WrapperOfUser> ();
        showSaveError = true;
        cancel = controller.cancel();
        System.debug('@@pageRef' + controller);
        GW1_DSR__c objDSR = (GW1_DSR__c) controller.getRecord();
        //vertical = objDSR.GW1_Industry_Vertical__c;
        //Geography = objDSR.GW1_sales_Region__c;
        
        lstoptionsTeam =fetchValues('User','GW1_Team__c','All');
        lstGeographyoptions=fetchValues('User','GW1_Geography__c','All');
        lstVerticaloptions=fetchValues('User','Industry_Vertical__c','All');
        lstRoleoptions=fetchValues('GW1_DSR_Team__c','GW1_Role__c',null);
    }
    /*
Method will fetch all the picl list values
--------------------------------------------------------------------------------------
Name              Date                                              Version                     
--------------------------------------------------------------------------------------
Pankaj             09-5-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/    
    public List<selectOption> fetchValues(String object_name, String field_name,String first_val) 
    {
        List<selectOption> options = new List<selectOption>(); 
        
        Map<String, Schema.SObjectType> schemaInfo =Schema.getGlobalDescribe();
        if (first_val != null) { 
            options.add(new selectOption('', first_val)); 
        }
        Schema.sObjectType sobject_type = schemaInfo.get(object_name); 
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); 
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); 
        List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); 
        
        for (Schema.PicklistEntry a : pick_list_values) 
        { 
            options.add(new selectOption(a.getValue(), a.getLabel())); 
        }
        return options;
    }
    /*
This function quries user ,called on clicking search button
--------------------------------------------------------------------------------------
Name              Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi             03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    
    public void FetchUsers()
    {
        list<Id> listIDToExclude = new list<Id> (); //list of id to exclude
        system.debug('shortlistedUserWrapper::165' + shortlistedUserWrapper);
        if (shortlistedUserWrapper != null && shortlistedUserWrapper.size() > 0)
        {
            for (WrapperOfUser objWUser : shortlistedUserWrapper)
            {
                listIDToExclude.add(objWUser.DSRUser.Id);
            }
        }
        list<GW1_DSR_Team__c> listExistingTeam = [select id, GW1_User__c from GW1_DSR_Team__c where GW1_DSR__r.id = :dsrId ];
        for (GW1_DSR_Team__c eachExistingTeam : listExistingTeam)
        {
            listIDToExclude.add(eachExistingTeam.GW1_User__c);
        }
        
        list<String> listOHRIDToExclude = new list<String> (); //list of id to exclude
        system.debug('shortlistedUserWrapper::165' + shortlistedUserWrapper);
        if (shortlistedUserWrapper != null && shortlistedUserWrapper.size() > 0)
        {
            for (WrapperOfUser objWUser : shortlistedUserWrapper)
            {
                listOHRIDToExclude.add(objWUser.DSRUser.OHR_ID__c);
            }
        }
        /*
list<GW1_DSR_Team__c> listExistingTeam1 = [select id, GW1_User__c from GW1_DSR_Team__c where GW1_DSR__r.id = :dsrId ];
for (GW1_DSR_Team__c eachExistingTeam2 : listExistingTeam1)
{
listOHRIDToExclude.add(eachExistingTeam2.GW1_User__r.OHR_ID__c);
}
*/
        
        
        system.debug('listOHRIDToExclude===='+listOHRIDToExclude);
        
        list<Id> listIDToExcludecase = new list<Id> (); //list of id to exclude for case
        
        
        for (GW1_DSR_Team__c eachExistingTeam1 : listExistingTeam)
        {
            listIDToExcludecase.add(eachExistingTeam1.GW1_User__c);
        }
        string chatterLisence = 'CsnOnly'; //csnOnly is lisence type of chatter user
        string inVar = 'chatter';
        string tempInput = '%' + inVar + '%';
        list<user> queryString = [select name,Email,OHR_ID__c, id, GW1_team__c,Industry_Vertical__c,GW1_Role__c ,GW1_Geography__c   from user where id != :listIDToExclude AND isActive=true AND profile.usertype !=:chatterLisence];
        system.debug('debug 115'+listIDToExclude);
        if(team !=null && team!='' && team.equalsIgnorecase('All'))
        {
            team='';
        }
        if(geography!=null && geography!='' && geography.equalsIgnorecase('All'))
        {
            geography ='';
        }
        if(vertical !=null && vertical!='' && vertical.equalsIgnorecase('All'))
        {
            vertical ='';
        }
        system.debug('debug 128');
        
        //if (team != null && team != ''){
        
        for(user u: queryString)
        {
            //system.debug('debug 134');
            //system.debug(team);
            u.GW1_team__c=team;
        }
        
        //if (geography != null && geography != ''){
        
        for(user u: queryString)
        {
            
            u.GW1_Geography__c =Geography;
        }
        
        /*  if (searchemployeeID != null && searchemployeeID != '')
{
queryString += ' AND (OHR_ID__c = :searchemployeeIDsearchemployeeID )' ;
}
*/
        //if (vertical != null && vertical != ''){
        
        for(user u: queryString)
        {
            u.Industry_Vertical__c =vertical;
        }
        //if (searchemployeeID != null && searchemployeeID != '')        {
        
        String sep =searchemployeeID;
        system.debug('searchemployeeID='+searchemployeeID);
        List<String> sptstg=sep.split(';');
        for(integer i=0;i<sptstg.size()-1;i++){
            if(sptstg.get(i)==',')
            {
                system.debug('found ,');
            }
            
        }
        String sting='';
        String c='';
        //c=c.trim();
        for (String str : sptstg)
        {
            
            
            str=str.trim();
            //String c= '\''+str+'\''+',';
            if(str == '' || str == null)
                c='';
            else
                c= '\''+str+'\''+',';
            system.debug('c='+c);
            sting += c;
            system.debug('sting'+sting);
        }
        
        
        if(sting == '' || sting == null)
            sting='';
        else
            sting=sting.substring(0,(sting.length()-1));
        // sting=sting.trim();
        
        system.debug('sting'+sting);
        /*  if(sep.contains(';')!=true){
system.debug('found ,');
semi=true;
errorList=false;}
else{
system.debug('found1 ,');
semi=false;
// errorList=true;
}
*/
        string query='';
        if(sting == '' || sting == null )
            query ='select name,Email,OHR_ID__c, id, GW1_team__c,Industry_Vertical__c,GW1_Role__c ,GW1_Geography__c from user where id != :listIDToExclude AND isactive=true LIMIT 1000' ;
        else
            query ='select name,Email,OHR_ID__c, id, GW1_team__c,Industry_Vertical__c,GW1_Role__c ,GW1_Geography__c from user where OHR_ID__c in (' + sting +   ') AND id != :listIDToExclude AND isactive=true LIMIT 1000' ;
        
        //  string query ='select name,Email,OHR_ID__c, id, GW1_team__c,Industry_Vertical__c,GW1_Role__c ,GW1_Geography__c from user where OHR_ID__c =:searchemployeeID  ' ;
        lstUsers = new list<User>();
        System.debug('query : ' + query );
        
        lstUsers = database.query(query);
        system.debug('sting'+sting);
        // listA= new List<String>();
        setA= new set<String>();
        list<String> liststing= new List<String>();
        liststing.add(sting);
        system.debug('liststing'+liststing);
        
        /*              String1={1,2,3,4,5,6};
String2={2,3,4};

String[] st = String1.split(' , ');

for(integer i = 0; i<st.size(); i++)
{

if(String2.contains(string.valueof(st[i])))
{

}
}*/
        /*
for(Integer s=0;s< liststing.size();s++)
{   
for(Integer valid=0;valid< lstUsers.size();valid++)
{

system.debug('string.valueof(liststing[s]): ' + string.valueof(liststing[s]));
system.debug('liststing.size()'+liststing.size());
system.debug('lstUsers.size()'+lstUsers.size());
if(!lstUsers[valid].OHR_ID__c.contains(string.valueof(liststing[s])))
{
listA.Add(liststing[s]);
break;
}
}
}
System.debug('listA : ' + listA );
if(listA!=NULL)
{
Case case1=new Case(Subject='test case for invalid ohrid',
Status = 'New',
Origin = 'Phone'
);

System.debug('case1='+case1);
}
*/          
        
        //Set<String> setA = new Set<String>(listA);
        Set<User> setUsers = new Set<User>(lstUsers);
        //  list<User> listOHRID = new List<User>();
        system.debug('setUsers=='+setUsers);
        Set<String> setOHRID = new Set<String>();
        for (Integer a=0; a<setUsers.size(); a++)
        {
            setOHRID.add(lstUsers[a].OHR_ID__c);
        }
        Set<String> setOHRIDToExclude = new Set<String>(listOHRIDToExclude);
        /*
Set<String> setOhId = new Set<String>();
for(Integer b=0; b<setOHRIDToExclude.size(); b++)
{
setOhId.add(listOHRIDToExclude[b]);
}
system.debug('setOhId=='+setOhId);
*/
        for(String expectedUser: sptstg)
        {                
            expectedUser=expectedUser.trim();
            system.debug('expectedUser : ' + expectedUser);
            if(expectedUser!='')
            {
                system.debug('lstUsers.size()=='+lstUsers.size());
                //for(Integer s=0;s< lstUsers.size();s++)
                //{   
                //system.debug('lstUsers[s].OHR_ID__c : ' + lstUsers[s].OHR_ID__c);
                system.debug(setOHRID.contains(expectedUser));
                if(!setOHRID.contains(expectedUser) )
                {
                    setA.Add(expectedUser);
                    system.debug('setOHRIDToExclude===='+setOHRIDToExclude);
                    for(String ohId:setOHRIDToExclude)
                    {
                        
                        //system.debug(setOHRID.contains(expectedUser));
                        if(!setOHRID.contains(ohId))
                        {
                            
                            setA.remove(ohId);
                        }
                    }
                }
                //}
                
            }
            
        }
        
        
        System.debug('listA : ' + listA );
        System.debug('setA : ' + setA );
        /*
if(sep.contains(';')!=true){
system.debug('found ,');
semi=true;
errorList=false;}
else{
system.debug('found1 ,');
semi=false;
// errorList=true;
}
*/
        
        
        if(setA!=NULL && sep.contains(';')==true )
        {
            system.debug('find errorList');
            errorList = true;
            semi=false;
        }
        if(setA.size()<1 || sep.contains(';')==false)
        {
            
            errorList = false;
            semi=false;
        }
        if(setA!=NULL && ((sep.contains(';')==true && sep.contains(',')==true)|| sep.contains(',')==true ))
        {
            errorList = false;
            semi=true;
        }
        if(!setA.isempty() && (sep.contains(';')!=true && sep.contains(',')!=true))
        {
            system.debug('find errorList1');
            errorList = true;
            semi=false;
        }
        
        
        
        // queryString += ' limit 1500';
        /*  System.debug('@@@queryString' + queryString);
lstUsers = new list<User>();
listUser = Database.Query(queryString);
*/
        
        /*  if (listUser.size() == 999)
{
showFilterError = true; //shows error to use atleast some filter as list has limit 10,000 
} 
else
{ */
        
        showFilterError = false;
        listWrapperOfUser = new list<WrapperOfUser> ();
        
        
        /*   WrapperOfUser objWrapper = new WrapperOfUser();
for (user objuser : listUser)
{
objWrapper = new WrapperOfUser(objuser);
listWrapperOfUser.add(objWrapper);
}
*/
        system.debug('shortlistedUserWrapper::216' + shortlistedUserWrapper);
        // }
        
    }
    
    
    /*
This function is called on sortlist user button click  used to shortlist the user 
--------------------------------------------------------------------------------------
Name             Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi             03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    public void shortListUser()
    {
        list<string> listOfShortlistedUserIndex = shortlistedUserIndex.split(';');
        system.debug('shortlistedUserWrapper::' + shortlistedUserWrapper);
        list<User> lstTempUser = new list<User>();
        lstTempUser.addAll(lstUsers );
        if (listOfShortlistedUserIndex != null)
        {
            system.debug(listOfShortlistedUserIndex);
            system.debug('listWrapperOfUser::' + listWrapperOfUser);
            for (string userIndex : listOfShortlistedUserIndex)
            {
                if( lstTempUser!=null && lstTempUser.size()>0 && lstTempUser.size()>= integer.valueOf(userIndex)+ 1)
                {
                    shortlistedUserWrapper.add(new WrapperOfUser(lstTempUser[integer.valueOf(userIndex)]));
                    
                    system.debug('Time 3::'+Limits.getLimitCpuTime());
                }
                
            }
            system.debug('shortlistedUserWrapper::' + shortlistedUserWrapper);
            if (shortlistedUserWrapper.size() > 0)
            {
                FetchUsers();
                
                errorList = false;
                
                
                
            }
            else
            {
                
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please shortlist some users'));
            }
            
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please shortlist some users'));
        }
        
    }
    /*
This function is called from action function deleteRow_AF 
--------------------------------------------------------------------------------------
Name              Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi              03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    
    public PageReference delRow() {
        system.debug('@@@delNum' + delRowNum);
        //shortlistedUser.remove(delRowNum);
        if (shortlistedUserWrapper.size() >= delRowNum + 1)
        {
            lstUsers.add(shortlistedUserWrapper[delRowNum].DSRUser);
            shortlistedUserWrapper.remove(delRowNum);
        }
        
        return null;
    }
    
    /*
this method is called to save record
--------------------------------------------------------------------------------------
Name        Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi         03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    public Void saveAndClose()
    {
        if (shortlistedUserWrapper.size() > 0)
        {
            showSaveError = False;
            try
            {
                jsonString = JSON.serialize(shortlistedUserWrapper);
                // insert listInsertDsr; Commented by pankaj
            }
            catch(exception e)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
                cancel = null;
                showSaveError = true;
            }
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please shortlist some users'));
        }
    }
    public void caseCreation()
    {
        
        
        String stg='';
        String b='';
        for (String la : setA)
        {
            //String c= '\''+str+'\''+',';
            if(b=='' || b==null)
                b='';
            else
                b= '\''+la+'\''+',';
            system.debug('b='+b);
            stg += b;
        }
        system.debug('stg'+stg);
        if (stg=='' || stg==null)
            stg='';
        else
            stg=stg.substring(0,(stg.length()-1));
        
        if(setA!=NULL)
        {
            //errorList = true;
            
            Case case1=new Case(Subject='Please provide Deal Team profile to these users',
                                Status = 'New',
                                Origin = 'DSR page',
                                Description='OHRID are '+stg
                               );
            insert case1;
            caseNo = case1.id;
            System.debug('case1='+case1);
            
        }
        setA.clear();
        if(setA!=null)
        {
            caseDetails=true;
        }
        if(setA.size()<1)
        {
            errorList = false;
            
        }
        
    }    
    /*
wrapperclass to wrap user ,checkbox and DSRRole
--------------------------------------------------------------------------------------
Name            Date                                                            Version                     
--------------------------------------------------------------------------------------
Rishi            03-10-2016                                                  1.0       
--------------------------------------------------------------------------------------
*/
    
    public class WrapperOfUser
    {
        public user DSRUser { get; Private set; }
        //public boolean isChecked { get; set; }
        public string DSRRole { get; set; }
        public WrapperOfUser(user objUser)
        {
            this.DSRUser = objUser;
            //this.isChecked = false;
            if (objUser.GW1_Role__c != null && objUser.GW1_Role__c != '')
            {
                this.DSRRole = objUser.GW1_Role__c;
            }
        }
        public WrapperOfUser()
        {
            
        }
        
    }
}