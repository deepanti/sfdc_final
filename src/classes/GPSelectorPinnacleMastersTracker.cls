@isTest
public class GPSelectorPinnacleMastersTracker {
    static GP_Pinnacle_Master__c pinnacleMaster;
    @testSetup
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        insert project ;
        
        GP_Pinnacle_Master__c hslMaster = GPCommonTracker.GetpinnacleMaster();
        hslMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        insert hslMaster;
        
        GP_Pinnacle_Master__c vslMaster = GPCommonTracker.GetpinnacleMaster();
        vslMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'VSL Master');
        insert vslMaster;
        
        GP_Pinnacle_Master__c subDivisionMaster = GPCommonTracker.GetpinnacleMaster();
        subDivisionMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        insert subDivisionMaster;
        
        GP_Pinnacle_Master__c subDivisionMaster2 = GPCommonTracker.GetpinnacleMaster();
        subDivisionMaster2.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        subDivisionMaster2.GP_Parent_Customer_L2__c = 'Hospitality Properties Trust';
        insert subDivisionMaster2;
        
        GP_Pinnacle_Master__c siloMaster = GPCommonTracker.GetpinnacleMaster();
        siloMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        insert siloMaster;
        
        GP_Pinnacle_Master__c siloMaster2 = GPCommonTracker.GetpinnacleMaster();
        siloMaster2.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        siloMaster2.GP_Parent_Customer_L2__c = 'Hospitality Properties Trust';
        insert siloMaster2;
        
        GP_Pinnacle_Master__c portalMaster = GPCommonTracker.GetpinnacleMaster();
        portalMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Portal Master');
        insert portalMaster;
        
        GP_Pinnacle_Master__c projectOrganizationMaster = GPCommonTracker.GetpinnacleMaster();
        projectOrganizationMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Project Organization');
        insert projectOrganizationMaster;
        
        GP_Pinnacle_Master__c serviceSubCategoryMaster = GPCommonTracker.GetpinnacleMaster();
        serviceSubCategoryMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Service Sub Category');
        serviceSubCategoryMaster.GP_Service_Line__c = '23456';
        insert serviceSubCategoryMaster;
        
        
        GP_Pinnacle_Master__c billingEntity = GPCommonTracker.GetpinnacleMaster();
        billingEntity.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        billingEntity.GP_Status__c = 'Active';
        billingEntity.Name = 'Genpact International Inc.';
        insert billingEntity;
        
        
        GP_Pinnacle_Master__c hSNCategory = GPCommonTracker.GetpinnacleMaster();
        hSNCategory.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSN Category');
        insert hSNCategory;
    }
    @isTest
    public static void testSelectBySetOfId() {
        GPSelectorPinnacleMasters pinnacleMasterSelector = new GPSelectorPinnacleMasters();
        GP_Pinnacle_Master__c expectedPinnacleMaster = [SELECT Id FROM GP_Pinnacle_Master__c LIMIT 1];
        try{
            GP_Pinnacle_Master__c returnedPinnacleMaster = pinnacleMasterSelector.selectById(expectedPinnacleMaster.Id);
        }
        catch(Exception Ex){}
        //System.assertEquals(expectedPinnacleMaster.Id, returnedPinnacleMasters[0].Id);
    }
    @isTest
    public static void testSelectById() {
        GP_Project__c project = [Select Id, GP_Operating_Unit__r.GP_Oracle_Id__c from GP_Project__c limit 1];
        
        GPSelectorPinnacleMasters pinnacleMasterSelector = new GPSelectorPinnacleMasters();
        GP_Pinnacle_Master__c expectedPinnacleMaster = [SELECT Id FROM GP_Pinnacle_Master__c LIMIT 1];
        try{
            list<GP_Pinnacle_Master__c> returnedPinnacleMaster = pinnacleMasterSelector.selectById(new Set<Id> {expectedPinnacleMaster.Id});
        }
        //System.assertEquals(expectedPinnacleMaster.Id, returnedPinnacleMaster.Id);
        catch(Exception Ex){} 
        
        pinnacleMasterSelector.selectByRecordId(null);
        GPSelectorPinnacleMasters.selectGlobalSettingRecord();
        try {
            GPSelectorPinnacleMasters.selectByOperatingUnitId(null);
        } catch(Exception ex) {}
        
        GPSelectorPinnacleMasters.selectBillingEntityRecords(null);
        GPSelectorPinnacleMasters.selectOracleTemplateIdAndMODOracleID(null, null, null, null);
        GPSelectorPinnacleMasters.selectTrainingMasters();
        GPSelectorPinnacleMasters.selectTrainingMastersforCourseId(null);
        GPSelectorPinnacleMasters.selectMODOracleId(null);
        GPSelectorPinnacleMasters.selectRecordsFromOracleIdSet(null);
        GPSelectorPinnacleMasters.getAllPinnacleMasterData(null);
        GPSelectorPinnacleMasters.getTaxCode(project);
        GPSelectorPinnacleMasters.selectByRecordTypeName('HSL Master');
        GPSelectorPinnacleMasters.getTaxCodeRecord(null);
        GPSelectorPinnacleMasters.getGenpactInternationalIncOperatingUnit();
        GPSelectorPinnacleMasters.getTaxMasterCodeRecord(null);
        GPSelectorPinnacleMasters.getAllPinnacleMasterData();
        GPSelectorPinnacleMasters.selectOracleTemplateId(new Set<String>(), new Set<String>());
        GPSelectorPinnacleMasters.selectOracleTemplateId(new Set<String>(), new Set<Id>());
        // HSN Changes
        GPSelectorPinnacleMasters.getGSTEnabledOUs();
    }
}