/**
* @author Anmol.kumar
* @date 22/02/2018
*
* @group OpportunityProjectClassification
* @group-content ../../ApexDocContent/OpportunityProjectClassification.html
*
* @description Apex Service for Opportunity project Classification module,
* has methods to create project Classification data on project approval.
*/
public without sharing class GPServiceOppProjectClassification {
    
    private static final String SUCCESS_LABEL = 'SUCCESS';

    private List<GP_Opp_Project_Default__mdt> listOfOppProjectLeadershipDefaults;
    private List<GP_Project_Classification__c> listOfProjectClassification;
    private List<GP_Opportunity_Project__c> listOfOppProject;
    
    private String oppProjectQueryString;
    private List<Id> listOfOppProjectId;
    private GP_Deal__c dealWithMaxTCV;
    
    
    /**
    * @description Overloaded constructor to accept list of opp project Id.
    *  
    * @param List<Id>  listOfOppProjectId: list of Opportunity Project Id
    * @example
    * new GPServiceOppProjectClassification({listOfOppProjectId});
    */
    public GPServiceOppProjectClassification(List<Id> listOfOppProjectId) {
        this.listOfOppProjectId = listOfOppProjectId;
    }

    /**
    * @description Overloaded constructor to accept list of opp project.
    *  
    * @param List<Id>  listOfOppProject: list of Opportunity Project
    * @example
    * new GPServiceOppProjectClassification({listOfOppProject});
    */
    public GPServiceOppProjectClassification(List<GP_Opportunity_Project__c> listOfOppProject) {
        this.listOfOppProject = listOfOppProject;
    }
    
    /**
    * @description create project classification records.
    *  
    * @example
    * new GPServiceOppProjectClassification({listOfOppProject}).createProjectClassification();
    * 
    * @TODO: Need to update logic of Deal with MAX TCV, Amit sir to confirm the fields.
    */
    public GPResponseWrapper createProjectClassification() {
        listOfProjectClassification = new List<GP_Project_Classification__c>();
        setOpportunityProjectData();
        setDefaultOppProjectClassification();

        for(GP_Opportunity_Project__c oppProject : listOfOppProject) {
            
            // Not required  // Commented by Pankaj
            //setDealWithMaximumTCV(oppProject);
            
            setBusinessHierarchyForPBB(oppProject);
            setPBBAttribute(oppProject);
            setProductFamily(oppProject);

            setProjectBasedBusiness(oppProject);
            setSFDCAttribute(oppProject);
            setDefaultProjectClassificationRecords(oppProject);
        }

        if(listOfProjectClassification == null || listOfProjectClassification.isEmpty()) {
            return new GPResponseWrapper(0, SUCCESS_LABEL);
        }

        try {
            insert listOfProjectClassification;
        } catch(Exception ex) {
            return new GPResponseWrapper(-1, ex.getMessage());
        }
        
        return new GPResponseWrapper(0, SUCCESS_LABEL);
    }
    
    private void setOpportunityProjectData() {
        if(listOfOppProject == null) {
            GPSelectorOpportunityProject oppProjectSelector = new GPSelectorOpportunityProject();
            listOfOppProject = oppProjectSelector.selectOppproductForClassification(listOfOppProjectId);
        }
    }
    
    /*
    private void setDealWithMaximumTCV(GP_Opportunity_Project__c oppProject) {
        /*
        * TODO: Need to update the logic in case of PID Update scenario
        *
        try {
            dealWithMaxTCV = GPSelectorDeal.getDealWithMaxTCVUnderOppProject(oppProject.Id);            
        } catch(Exception ex) {
            System.debug('Error: '+ex.getMessage());
        }
    }*/
    
    /**
    * @description Creates record of opp project classification.
    * Records are created based on records in
    * GP_Opp_Project_Default__mdt - 'Opp Proj Classification' record type
    *  
    * @example
    * new GPServiceOppProjectClassification({listOfOppProjectId}).createProjectClassification();
    */
    private void setDefaultProjectClassificationRecords(GP_Opportunity_Project__c oppProject) {
        for(GP_Opp_Project_Default__mdt defaultOppProjectLeadership :listOfOppProjectLeadershipDefaults) {

            GP_Project_Classification__c defaultProjectClassification = new GP_Project_Classification__c();
            
            defaultProjectClassification.GP_Opp_Project__c = oppProject.Id;
            defaultProjectClassification.GP_CLASS_CODE__c = defaultOppProjectLeadership.GP_CLASSIFICATION_VALUE__c;
            defaultProjectClassification.GP_CLASS_CATEGORY__c = defaultOppProjectLeadership.GP_CLASS_CATEGORY__c;
            defaultProjectClassification.GP_ATTRIBUTE_CATEGORY__c = defaultOppProjectLeadership.GP_CLASS_CATEGORY__c;
            
            defaultProjectClassification.GP_ATTRIBUTE1__c = defaultOppProjectLeadership.GP_ATTRIBUTE1__c;
            defaultProjectClassification.GP_ATTRIBUTE2__c = defaultOppProjectLeadership.GP_ATTRIBUTE2__c;
            defaultProjectClassification.GP_ATTRIBUTE3__c = defaultOppProjectLeadership.GP_ATTRIBUTE3__c;
            defaultProjectClassification.GP_ATTRIBUTE4__c = defaultOppProjectLeadership.GP_ATTRIBUTE4__c;
            defaultProjectClassification.GP_ATTRIBUTE5__c = defaultOppProjectLeadership.GP_ATTRIBUTE5__c;
            defaultProjectClassification.GP_ATTRIBUTE6__c = defaultOppProjectLeadership.GP_ATTRIBUTE6__c;
            defaultProjectClassification.GP_ATTRIBUTE7__c = defaultOppProjectLeadership.GP_ATTRIBUTE7__c;
            defaultProjectClassification.GP_ATTRIBUTE8__c = defaultOppProjectLeadership.GP_ATTRIBUTE8__c;
            defaultProjectClassification.GP_ATTRIBUTE9__c = defaultOppProjectLeadership.GP_ATTRIBUTE9__c;
            defaultProjectClassification.GP_ATTRIBUTE10__c = defaultOppProjectLeadership.GP_ATTRIBUTE10__c;
            defaultProjectClassification.GP_ATTRIBUTE11__c = defaultOppProjectLeadership.GP_ATTRIBUTE11__c;
            defaultProjectClassification.GP_ATTRIBUTE12__c = defaultOppProjectLeadership.GP_ATTRIBUTE12__c;
            defaultProjectClassification.GP_ATTRIBUTE13__c = defaultOppProjectLeadership.GP_ATTRIBUTE13__c;
            defaultProjectClassification.GP_ATTRIBUTE14__c = defaultOppProjectLeadership.GP_ATTRIBUTE14__c;
            defaultProjectClassification.GP_ATTRIBUTE15__c = defaultOppProjectLeadership.GP_ATTRIBUTE15__c;

            defaultProjectClassification.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
            defaultProjectClassification.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
            
            listOfProjectClassification.add(defaultProjectClassification);
            
        }
    }

    private void setBusinessHierarchyForPBB(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c businessHierarchyForPBB = new GP_Project_Classification__c();
        
         string strSbusinessGrupCode = '';
        if(!string.isblank(oppProject.GP_Business_Group_L1__c )){
            if(oppProject.GP_Business_Group_L1__c == 'GE'){
                strSbusinessGrupCode ='001';
            }
            if(oppProject.GP_Business_Group_L1__c == 'Global Clients'){
                strSbusinessGrupCode ='002';
            }
            if(oppProject.GP_Business_Group_L1__c == 'Genpact'){
                strSbusinessGrupCode ='003';
            }
            
        }

        businessHierarchyForPBB.GP_Opp_Project__c = oppProject.Id;
        businessHierarchyForPBB.GP_CLASS_CODE__c = 'Business Hierarchy';
        businessHierarchyForPBB.GP_CLASS_CATEGORY__c = 'Business Hierarchy';
        businessHierarchyForPBB.GP_ATTRIBUTE_CATEGORY__c = 'Business Hierarchy';
        businessHierarchyForPBB.GP_ATTRIBUTE1__c = strSbusinessGrupCode;
        businessHierarchyForPBB.GP_ATTRIBUTE2__c = oppProject.GP_Business_Segment_L2_Id__c;
        businessHierarchyForPBB.GP_ATTRIBUTE3__c = oppProject.GP_Sub_Business_L3_Id__c;
        businessHierarchyForPBB.GP_ATTRIBUTE4__c = oppProject.GP_Customer_L4_Name__c;
        businessHierarchyForPBB.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        businessHierarchyForPBB.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        
        listOfProjectClassification.add(businessHierarchyForPBB);
    }
    
    private void setSFDCAttribute(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c businessHierarchyForPBB = new GP_Project_Classification__c();
        
        businessHierarchyForPBB.GP_Opp_Project__c = oppProject.Id;
        businessHierarchyForPBB.GP_CLASS_CODE__c = 'SFDC Attributes';
        businessHierarchyForPBB.GP_CLASS_CATEGORY__c = 'SFDC Attributes';
        businessHierarchyForPBB.GP_ATTRIBUTE_CATEGORY__c ='SFDC Attributes';
        businessHierarchyForPBB.GP_ATTRIBUTE2__c = oppProject.GP_Opportunity_Id__c ;
        businessHierarchyForPBB.GP_ATTRIBUTE3__c = oppProject.GP_Sales_Internal_Oppid__c;

        businessHierarchyForPBB.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        businessHierarchyForPBB.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        
        listOfProjectClassification.add(businessHierarchyForPBB);
    }
    
    private void setProjectBasedBusiness(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c projectBasedBusiness = new GP_Project_Classification__c();
        
        projectBasedBusiness.GP_Opp_Project__c = oppProject.Id;
        
        //projectBasedBusiness.GP_CLASS_CODE__c = 'Project Based Business';
        //projectBasedBusiness.GP_CLASS_CATEGORY__c = 'Yes';
        
        projectBasedBusiness.GP_CLASS_CATEGORY__c = 'Project Based Business';
        projectBasedBusiness.GP_CLASS_CODE__c = 'Yes';
        projectBasedBusiness.GP_ATTRIBUTE_CATEGORY__c ='Project Based Business';
        projectBasedBusiness.GP_ATTRIBUTE1__c = 'Y';
        projectBasedBusiness.GP_ATTRIBUTE2__c = String.valueOf(oppProject.GP_Pinnacle_Push_Date__c);
        projectBasedBusiness.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        projectBasedBusiness.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        
        listOfProjectClassification.add(projectBasedBusiness);         
    }
    
    private void setProductFamily(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c productFamily = new GP_Project_Classification__c();
        
        productFamily.GP_Opp_Project__c = oppProject.Id;
        productFamily.GP_CLASS_CODE__c = 'Product Family';
        productFamily.GP_CLASS_CATEGORY__c = 'Product Family';
        productFamily.GP_ATTRIBUTE_CATEGORY__c = 'Product Family';
        //if(dealWithMaxTCV != null) {
            
            productFamily.GP_ATTRIBUTE1__c = oppProject.GP_Product_Id__c;
            // Not required :: Other attributes //Pankaj :: Commented 16-March-2018 
            /*productFamily.GP_ATTRIBUTE2__c = dealWithMaxTCV.GP_Product_Master__r.GP_Product_Family__c;
            productFamily.GP_ATTRIBUTE3__c = dealWithMaxTCV.GP_Product_Master__r.GP_Service_Line_ID__c;
            productFamily.GP_ATTRIBUTE4__c = dealWithMaxTCV.GP_Product_Master__r.GP_Service_Line__c;
            
            productFamily.GP_ATTRIBUTE5__c = dealWithMaxTCV.GP_Product_Master__r.GP_Nature_of_Work_ID__c;
            productFamily.GP_ATTRIBUTE6__c = dealWithMaxTCV.GP_Product_Master__r.GP_Nature_of_Work__c;
            productFamily.GP_ATTRIBUTE7__c = dealWithMaxTCV.GP_Product_Master__c;
            productFamily.GP_ATTRIBUTE8__c = dealWithMaxTCV.GP_Product_Master__r.GP_Product_Name__c;*/
        //}
        
        productFamily.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        productFamily.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        
        listOfProjectClassification.add(productFamily);         
    }
   /*
    private void setProductCategory(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c projectCategory = new GP_Project_Classification__c();
        
        projectCategory.GP_Opp_Project__c = oppProject.Id;
        //projectCategory.GP_CLASS_CODE__c = 'Project Category';
        //projectCategory.GP_CLASS_CATEGORY__c = 'Opportunity';
        projectCategory.GP_CLASS_CATEGORY__c = 'Project Category';
        projectCategory.GP_CLASS_CODE__c = 'Opportunity';
        projectCategory.GP_ATTRIBUTE_CATEGORY__c = 'Project Category';
        projectCategory.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        projectCategory.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        listOfProjectClassification.add(projectCategory);         
    }*/
    
    private void setPBBAttribute(GP_Opportunity_Project__c oppProject) {
        GP_Project_Classification__c pbbAttribute = new GP_Project_Classification__c();
        
        pbbAttribute.GP_Opp_Project__c = oppProject.Id;
        pbbAttribute.GP_CLASS_CODE__c = 'PBB Attributes';
        pbbAttribute.GP_CLASS_CATEGORY__c = 'PBB Attributes';
        pbbAttribute.GP_ATTRIBUTE_CATEGORY__c ='PBB Attributes';
        pbbAttribute.GP_ATTRIBUTE1__c = oppProject.GP_EP_Project_Number__c;
        pbbAttribute.GP_ATTRIBUTE3__c = string.valueOf(oppProject.GP_TCV__c); // Should be TCV Added Pankaj 16-Mar-2018
        pbbAttribute.GP_ATTRIBUTE4__c = 'N';
        pbbAttribute.GP_PINNACLE_PUSH_DATE__c = oppProject.GP_Pinnacle_Push_Date__c;
        pbbAttribute.GP_EP_Project_Number__c = oppProject.GP_EP_Project_Number__c;
        
        listOfProjectClassification.add(pbbAttribute); 
    }

    private void setDefaultOppProjectClassification() {
        listOfOppProjectLeadershipDefaults = [Select GP_CLASS_CODE__c,DeveloperName, 
                                                     GP_CLASS_CATEGORY__c, GP_CLASSIFICATION_VALUE__c,
                                                     GP_Attribute1__c, 
                                                     GP_Attribute2__c, 
                                                     GP_Attribute3__c, 
                                                     GP_Attribute4__c, 
                                                     GP_Attribute5__c, 
                                                     GP_Attribute6__c, 
                                                     GP_Attribute7__c, 
                                                     GP_Attribute8__c, 
                                                     GP_Attribute9__c, 
                                                     GP_Attribute10__c, 
                                                     GP_Attribute11__c, 
                                                     GP_Attribute12__c, 
                                                     GP_Attribute13__c, 
                                                     GP_Attribute14__c, 
                                                     GP_Attribute15__c
                                                FROM GP_Opp_Project_Default__mdt
                                                Where GP_Record_Type__c = 'Opp Project Classification'];
    }
}