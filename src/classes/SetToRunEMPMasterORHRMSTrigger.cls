public class SetToRunEMPMasterORHRMSTrigger {
    public static string isRunHrmsTrigger{get;set;}
    public static string isRunEmpMasterTrigger{get;set;}
    public static string RunMapEmployeeWithAssignmentOnBasisOfPersonIdOnce{get;set;}
    public static string isRunWorklocationCodeOnce{get;set;}
    public static string isRunSDOCodeOnce{get;set;}
    public static string isRunSupervisorCodeOnce{get;set;}
    public static string isRunSFDCUserCodeOnce{get;set;}
    public static string isRunEmployeeDetailsOnce{get;set;}
}