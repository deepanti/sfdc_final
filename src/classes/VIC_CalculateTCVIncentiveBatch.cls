public class VIC_CalculateTCVIncentiveBatch implements Database.Batchable<sObject>,Database.Stateful{
    String strNewIncentiveType = 'New';
    String strAdjuctIncentiveType = 'Adjustment';
    String strDisbursed = 'Supervisor Pending';
    VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    Integer currentYear = integer.valueof(processInfo.VIC_Annual_Process_Year__c);
    Date startFYDate = Date.newInstance(currentYear, 1, 1);
    Date endFYDate = Date.newInstance(currentYear, 12, 31);
    String strPlanYear = String.valueOf(currentYear);
    List<Target_Achievement__c> lstTargetIncentive = new List<Target_Achievement__c >();
    Map<String, vic_Incentive_Constant__mdt> mapPMVStrLabelToIncentiveConst = new VIC_IncentiveConstantCtlr().getAllMetaData();
    Map <String, String> mapDomicileTiISOCode = vic_CommonUtil.getDomicileToISOCodeMap();
    Map<String,vic_Currency_Exchange_Rate__c> mapMonthYearISOToCER = vic_CommonUtil.fetchCurrencyExchangeRates();
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        Set<String> setTCVPlans = new Set<String>{'IT_GRM'};
        return Database.getQueryLocator([
            SELECT Id,Target__r.Plan__r.vic_Plan_Code__c,vic_Achievement_Percent__c,Target__r.Target_Bonus__c,Weightage__c,Target__r.VIC_Joining_Date__c,
            Target_In_Currency__c,Target__r.vic_Total_TCV__c,Overall_Achievement__c,Master_Plan_Component__c,
            Master_Plan_Component__r.Name,Target__r.Plan__c,Target__r.User__r.Domicile__c,
            VIC_Total_Achievement_Scoreboard__c,VIC_Total_Achievement_Count__c,
            Target__r.vic_TCV_IO__c,Target__r.vic_TCV_TS__c,
            (SELECT Id,Bonus_Amount__c,vic_Status__c,vic_Incentive_Type__c,vic_Deal_Type__c,vic_Incentive_Details__c,
            vic_PPP__c,Achievement_Date__c,Target_Component__c from Target_Achievements__r) 
            From Target_Component__c where Target__r.Plan__r.vic_Plan_Code__c IN :setTCVPlans AND 
            Target__r.Is_Active__c =:true AND Target_Status__c =:'Active' AND 
            Target__r.User__c != null AND Target__r.User__r.Domicile__c != null AND 
            Master_Plan_Component__r.vic_Component_Code__c =:'TCV' AND Target__r.vic_Total_TCV__c!= null AND 
            Target_In_Currency__c !=null AND Target_In_Currency__c > 0 AND 
            Weightage__c != null AND Target__r.Target_Bonus__c != null AND 
            Target__r.Plan__r.vic_Plan_Code__c != null AND 
            Target__r.Start_date__c >= :startFYDate AND 
            Target__r.Start_date__c <= :endFYDate
        ]);
    }  
    public void execute(Database.BatchableContext bc, List<Target_Component__c> lstTargetCMP){
        Integer intMonth = System.now().month();
        Integer intYear = endFYDate.year();
        Set<Id> setPlanId = new Set<Id>();
        for(Target_Component__c objTC : lstTargetCMP){
            setPlanId.add(objTC.Target__r.Plan__c);
        }
        Map<String,List<VIC_Calculation_Matrix_Item__c>> mapPlanIdToLstOfVICMatrixItem = fetchVICMatrixItemByPlan(setPlanId);
        for(Target_Component__c objTargetCMPITR : lstTargetCMP){
           Decimal dcmTargetBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c;
           if(objTargetCMPITR.Target__r.VIC_Joining_Date__c != null){
                    objTargetCMPITR.Target__r.Target_Bonus__c= vic_CommonUtil.fetchProRatedAmount(objTargetCMPITR.Target__r.Target_Bonus__c,objTargetCMPITR.Target__r.VIC_Joining_Date__c);
           }
            Decimal dcmCalBonousAmount = null;
            if(mapDomicileTiISOCode.containsKey(objTargetCMPITR.Target__r.User__r.Domicile__c) && mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c) != Label.VIC_User_Currency){
                String keyOfMothYearISO = ''+ intMonth + intYear + mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c);
                if(mapMonthYearISOToCER.containsKey(keyOfMothYearISO) && mapMonthYearISOToCER.get(keyOfMothYearISO).vic_Exchange_Rate__c != null && objTargetCMPITR.Target__r.Target_Bonus__c != null){
                    dcmCalBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c/mapMonthYearISOToCER.get(keyOfMothYearISO).vic_Exchange_Rate__c;
                }
            }else if(mapDomicileTiISOCode.containsKey(objTargetCMPITR.Target__r.User__r.Domicile__c) && mapDomicileTiISOCode.get(objTargetCMPITR.Target__r.User__r.Domicile__c) == Label.VIC_User_Currency){
                dcmCalBonousAmount = objTargetCMPITR.Target__r.Target_Bonus__c;
            }
            objTargetCMPITR.Target__r.Target_Bonus__c = dcmCalBonousAmount;
            String strKeyPlanMPC = ID.valueOf(objTargetCMPITR.Target__r.Plan__c)+''+ID.valueOf(objTargetCMPITR.Master_Plan_Component__c);
            if(mapPlanIdToLstOfVICMatrixItem.containsKey(strKeyPlanMPC)){
                String strIncentiveType = (objTargetCMPITR.VIC_Total_Achievement_Count__c == 0)?strNewIncentiveType:strAdjuctIncentiveType;
                VIC_CalculateTCVIncentivectrl objCalcTCVIncentive = new VIC_CalculateTCVIncentivectrl();
                //objCalcTCVIncentive.lstIncentive = objTargetCMPITR.Target_Achievements__r;
                objCalcTCVIncentive.mapValueToObjIncentiveConst = mapPMVStrLabelToIncentiveConst;
                Decimal totalIncentiveAMT = objCalcTCVIncentive.calculate(objTargetCMPITR, mapPlanIdToLstOfVICMatrixItem.get(strKeyPlanMPC));
                Decimal calcPayoutAmount = (totalIncentiveAMT - objTargetCMPITR.VIC_Total_Achievement_Scoreboard__c);
                if(calcPayoutAmount != 0){
                    Target_Achievement__c objTargetIncentive = vic_CommonUtil.initTargetIncentivRecOBJ(objTargetCMPITR, calcPayoutAmount, strIncentiveType, strDisbursed);
                    lstTargetIncentive.add(objTargetIncentive);
                }
            }
            objTargetCMPITR.Target__r.Target_Bonus__c = dcmTargetBonousAmount;
        } 
    }
    public void finish(Database.BatchableContext bc){
        if(!lstTargetIncentive.isEmpty()){
            insert lstTargetIncentive;
        }
        if(!Test.isRunningTest()){
              VIC_CurrencyConversionBatch  CuCvBatch = new VIC_CurrencyConversionBatch(false);
              Database.executeBatch(CuCvBatch, 2);
        }
        
    }
    //Fetching list of VIC_Calculation_Matrix_Item against VIC_Calculation_Matrix
    public Map<String,List<VIC_Calculation_Matrix_Item__c>> fetchVICMatrixItemByPlan(Set<Id> setPlanId){
       Map<String,List<VIC_Calculation_Matrix_Item__c>> mapPlanMPCStrToLstIOTypeVICMatrixItem = new Map<String,List<VIC_Calculation_Matrix_Item__c>>(); //(Plan+MasterPlanCMP) => (List of VIC Matrix Item) for IO type
       Map<String,Id> mapPlanMPCStrToIOVICMatrixId = new Map<String,Id>(); 
       Set<Id> setMatrixId = new Set<Id>();
       for(Plan__c objPlan : [SELECT id,Is_Active__c,vic_Plan_Code__c,Year__c,Conga_Email_Template__c,Conga_Template__c,Conga_Template_For_Bonus__c,
                     (SELECT Id,Master_Plan_Component__c,Master_Plan_Component__r.Name,Plan__c,VIC_IO_Calculation_Matrix_1__c,VIC_TS_Calculation_Matrix_2__c,
                      Master_Plan_Component__r.vic_Component_Code__c,
                      Weightage_Applicable__c from Plan_Components__r where Master_Plan_Component__r.vic_Component_Code__c =:'TCV') from Plan__c where Id IN:setPlanId AND 
                      Year__c =:strPlanYear ]){
            //Here filter data for IO and TS type VIC Matrix
            for(Plan_Component__c objPlanCmp : objPlan.Plan_Components__r){
                    String strKeyPlanMPC = ID.valueOf(objPlan.Id)+''+ID.valueOf(objPlanCmp.Master_Plan_Component__c);
                    mapPlanMPCStrToIOVICMatrixId.put(strKeyPlanMPC,objPlanCmp.VIC_IO_Calculation_Matrix_1__c);
            }
        }
        setMatrixId.addAll(mapPlanMPCStrToIOVICMatrixId.values()); //Adding all VIC Matrix Id To Set
        Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapVICMatrixIDToLstMatrixItem = fetchVICMatrixItemByLstMatrixId(setMatrixId);
         //Setting map of (Plan+MasterPlanCMP) => (List of VIC Matrix Item) for IO type
        for(String everyKey : mapPlanMPCStrToIOVICMatrixId.keySet()){
            ID idVICMatrix = mapPlanMPCStrToIOVICMatrixId.get(everyKey);
            if(mapVICMatrixIDToLstMatrixItem.containsKey(idVICMatrix)){
                mapPlanMPCStrToLstIOTypeVICMatrixItem.put(everyKey,mapVICMatrixIDToLstMatrixItem.get(idVICMatrix));
            }
        }
        return mapPlanMPCStrToLstIOTypeVICMatrixItem;
    }
    
    /*
        @Description: It is initiating map data all target data such as (VIC Matrix Id => List Of VIC Matrix Id Item).      
        @Author: Vikas Rajput
    */
    public Map<Id,List<VIC_Calculation_Matrix_Item__c>> fetchVICMatrixItemByLstMatrixId(Set<Id> setMatrixId){
        Map<Id,List<VIC_Calculation_Matrix_Item__c>> mapVICMatrixIdToLstOfVICMatrixRecItem = new Map<Id,List<VIC_Calculation_Matrix_Item__c>>();
        for(VIC_Calculation_Matrix__c objCalcMatrix : [Select Id,Name,(Select id,Additional_Payout__c,For_Each__c,
                    vic_Component_Type__c,vic_Start_Value__c,vic_End_Value__c,vic_Payout__c,Is_Additional_Payout_Applicable__c 
                    from VIC_Calculation_Matrix_Items__r order by vic_Start_Value__c ASC) from VIC_Calculation_Matrix__c where Id IN:setMatrixId]){
            mapVICMatrixIdToLstOfVICMatrixRecItem.put(objCalcMatrix.Id,objCalcMatrix.VIC_Calculation_Matrix_Items__r);
        }
        return mapVICMatrixIdToLstOfVICMatrixRecItem;
    }
}