/**
* @author Salil.Sharma
* @date 11/01/2018
*
* @group OpportunityProjectLeadership
* @group-content ../../ApexDocContent/OpportunityProjectLeadership.html
*
* @description Apex Service for Opportunity project Leadership module,
* has methods to create Opportunity project Leadership data on project approval.
*/
public without sharing class GPServiceOppProjectLeadership {
    private static final String SUCCESS_LABEL = 'SUCCESS';

    private List<Id> listOfOppProjectId;
    private List<GP_Opportunity_Project__c> listOfOppProject;
    private List<GP_Opp_Project_Leadership__c> listOfOppProjectLeadership;
    private List<GP_Opp_Project_Default__mdt> listOfOppProjectLeadershipDefaults;
	private map<String,id> mpLeaderShiproleToID = new map<String,id>();
	private set<String> setRoleID = new set<String>();
    public GPServiceOppProjectLeadership() {
    }
    
    /**
    * @description Overloaded constructor to accept list of opp project Id.
    *  
    * @param List<Id> listOfOppProjectId
    * @example
	* new GPServiceOppProjectLeadership({listOfOppProjectId}).insertOppProjectLeadership();
    */
    public GPServiceOppProjectLeadership(List<Id> listOfOppProjectId) {
        this.listOfOppProjectId = listOfOppProjectId;
    }

    /**
    * @description Overloaded constructor to accept list of opp project record.
    *  
    * @param List<GP_Opportunity_Project__c> listOfOppProject
    * @example
    * new GPServiceOppProjectLeadership({listOfOppProject});
    */
    public GPServiceOppProjectLeadership(List<GP_Opportunity_Project__c> listOfOppProject) {
        this.listOfOppProject = listOfOppProject;
    }
    
    /**
    * @description Creates record of opp project leadership.
    * Records are created based on records in
    * GP_Opp_Project_Default__mdt - 'Opp Proj Leadership' record type
    *  
    * @example
	* new GPServiceOppProjectLeadership({listOfOppProjectId}).createOppProjectLeadership();
    */
    public GPResponseWrapper createOppProjectLeadership(){
        listOfOppProjectLeadership = new List<GP_Opp_Project_Leadership__c>();
        setDefaultOppProjectLeadership();

        if(listOfOppProject == null) {
            setOpportunityProject();
        }
		
        
        for(GP_Opportunity_Project__c oppProject: listOfOppProject) {
            setOppProjectLeadership(oppProject);
        }
		
		getLeadershipRoles(setRoleID);
		// 
		setLeadershipMaster();
		        
        if(listOfOppProjectLeadership == null || listOfOppProjectLeadership.isEmpty()) {
            return new GPResponseWrapper(0, SUCCESS_LABEL);
        }

        try {
            insert listOfOppProjectLeadership;
        } catch(Exception ex) {
            return new GPResponseWrapper(-1, ex.getMessage());
        }
        
        return new GPResponseWrapper(0, SUCCESS_LABEL);
    }
    
    private void setOpportunityProject() {
        listOfOppProject = GPSelectorOppProjectLeadership.getOppProjectLeadershipUnderOppProject(listOfOppProjectId);
    }
    
    private void setOppProjectLeadership(GP_Opportunity_Project__c oppProject) {

        for(GP_Opp_Project_Default__mdt defaultOppProjectLeadership :listOfOppProjectLeadershipDefaults) {

            GP_Opp_Project_Leadership__c oppFPALeadership = new GP_Opp_Project_Leadership__c();

            oppFPALeadership.GP_Leadership_Role__c = defaultOppProjectLeadership.GP_Leadership_Role_Id__c;
            oppFPALeadership.GP_Emp_Person_Id__c = defaultOppProjectLeadership.GP_Emp_Person_Id__c;
            oppFPALeadership.GP_Start_Date__c = system.today();
            oppFPALeadership.GP_Opportunity_Project__c = oppProject.Id;
            oppFPALeadership.GP_Active__c = true;
			setRoleID.add(defaultOppProjectLeadership.GP_Leadership_Role_Id__c);
            listOfOppProjectLeadership.add(oppFPALeadership);
        }
    }

    private void setDefaultOppProjectLeadership() {
        listOfOppProjectLeadershipDefaults = [Select GP_Leadership_Role_Id__c,
                                                     GP_Emp_Person_Id__c
                                                FROM GP_Opp_Project_Default__mdt
                                                Where GP_Record_Type__c = 'Opp Project Leadership'];
    }
    
    private void getLeadershipRoles(set<String> setRoleID)
    {
    	if(setRoleID == null || setRoleID.size() ==0){
    		return;
    	}
    			
    	list<GP_Leadership_Master__c> lstLeaderShipMaster = new GPSelectorLeadershipMaster().getRoleMasterLeadership(setRoleID);    	
    	for(GP_Leadership_Master__c objLeaderShipMaster:lstLeaderShipMaster){
    			mpLeaderShiproleToID.put(objLeaderShipMaster.GP_Oracle_Id__c,objLeaderShipMaster.id);
    	}
    }
    
    private void setLeadershipMaster()
    {
    	if(mpLeaderShiproleToID == null && mpLeaderShiproleToID.Size() == 0 ){
    		return;
    	}
    	
    	for(GP_Opp_Project_Leadership__c objOppProjLeadership: listOfOppProjectLeadership){
    		if(!String.isBlank(objOppProjLeadership.GP_Leadership_Role__c) && 
    							mpLeaderShiproleToID.get(objOppProjLeadership.GP_Leadership_Role__c) != null){
    			objOppProjLeadership.GP_Leadership_Master__c = mpLeaderShiproleToID.get(objOppProjLeadership.GP_Leadership_Role__c);
    		}
    	}
    }
}