public Class CheckRecursive{
    public static boolean run = true;
    public static boolean runBeforeDelete = true;
    public static boolean afterInsertMethod = true;
    public static boolean afterUpdateMethod = true;
    public static boolean afterDeleteMethod = true;
    public static boolean runOncePinnacle = true;
    public static boolean runOnceOliPinnacle = true;
   
    public static boolean runOnce(){
        if(run){
            run=false;
            return true;
        }
        else{
            return run;
        }
    }
    public static boolean runBeforeDelete(){
        if(runBeforeDelete){
            runBeforeDelete=false;
            return true;
        }
        else{
            return runBeforeDelete;
        }
    }
    public static boolean afterInsertMethod(){
        if(afterInsertMethod){
            afterInsertMethod=false;
            return true;
        }
        else{
            return afterInsertMethod;
        }
    }
    public static boolean afterUpdateMethod(){
        if(afterUpdateMethod){
            afterUpdateMethod=false;
            return true;
        }
        else{
            return afterUpdateMethod;
        }
    }
    public static boolean afterDeleteMethod(){
        if(afterDeleteMethod){
            afterDeleteMethod=false;
            return true;
        }
        else{
            return afterDeleteMethod;
        }
    }
    
    public static boolean runOncePinnacle(){
        if(runOncePinnacle){
            runOncePinnacle = false;
            return true;
        }else{
            return runOncePinnacle;
        }
    }
    
    public static boolean runOnceOliPinnacle(){
        if(runOnceOliPinnacle){
            runOnceOliPinnacle = false;
            return true;
        }else{
            return runOnceOliPinnacle;
        }
    }
}