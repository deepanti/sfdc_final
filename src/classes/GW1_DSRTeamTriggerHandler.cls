// Class is handler class of DSR TEAM  GW1_ADDDSRTeamMember
// --------------------------------------------------------------------------------------------- 
// Version#     Date             Author                  Description
// ---------------------------------------------------------------------------------------------
// v1.0        03-10-2016       Rishi Kumar              
// ---------------------------------------------------------------------------------------------

public class GW1_DSRTeamTriggerHandler
{
	public void runTrigger()
	{
		if (trigger.isAfter && trigger.isInsert)
		{
			onAfterInsert((list<GW1_DSR_Team__c>) trigger.new);
		}

		if (trigger.isDelete)
		{
			onAfterDelete((list<GW1_DSR_Team__c>) trigger.OLD);
		}


		if (trigger.isbefore && trigger.isUpdate)
		{
			onBeforeUpdate((list<GW1_DSR_Team__c>) trigger.NEW, (Map<Id, GW1_DSR_Team__c>) trigger.OldMap);

		}
	}

	private void onBeforeUpdate(list<GW1_DSR_Team__c> lstTriggerNew, Map<Id, GW1_DSR_Team__c> TriggerOldMap) {

		removeInactiveDSRMemberFromChatterGroup(lstTriggerNew, TriggerOldMap);
		addActiveMemberToChatterGroup(lstTriggerNew, TriggerOldMap);
	}

	private void onAfterInsert(list<GW1_DSR_Team__c> lstTriggerNew)
	{
		addChatterMembers(lstTriggerNew);
	}
	private void onAfterDelete(list<GW1_DSR_Team__c> lstTriggerOld)
	{
		removeSharingSetting(lstTriggerOld);
	}


	/*
	  This method will create chatter members and is called after insert of DSR_Team 
	  --------------------------------------------------------------------------------------
	  Name                                Date                                Version                     
	  --------------------------------------------------------------------------------------
	  Rishi                             03-10-2016                              1.0   
	  --------------------------------------------------------------------------------------
	 */
	public void addChatterMembers(list<GW1_DSR_Team__c> lstTriggerNew)
	{
		CreateChatterMember(lstTriggerNew);
		addSharingSetting(lstTriggerNew);
	}


	/*
	  This method will adds sharing record to GW1_DSR__Share for new DSR Team
	  --------------------------------------------------------------------------------------
	  Name                                Date                                Version                     
	  --------------------------------------------------------------------------------------
	  Rishi                             03-10-2016                              1.0   
	  --------------------------------------------------------------------------------------
	 */
	public void addSharingSetting(list<GW1_DSR_Team__c> lstTriggerNew)
	{
		List<GW1_DSR__Share> listShareDSR = new List<GW1_DSR__Share> ();
		set<string > setExistingShareRecord = new set<string>();
		set<id> setParentid = new Set<id>();
	    for(GW1_DSR_Team__c objTeam: lstTriggerNew )
	    {
	    	setParentid.add(objTeam.GW1_DSR__c);
	    }
	    
	    if(setParentid!=null && setParentid.size()>0)
	    {
		    list< GW1_DSR__Share> lstExistingShare =new list<GW1_DSR__Share>([Select parentID,UserOrGroupId  from GW1_DSR__Share Where parentID in :setParentid  ]);
		    
		    for(GW1_DSR__Share objDSRshare:lstExistingShare )
		    {
		    	 string key = objDSRshare.parentID+'-'+objDSRshare.UserOrGroupId;
		    	 setExistingShareRecord.add(key);
		    }
			for (GW1_DSR_Team__c eachTeamMember : lstTriggerNew)
			{
				string key= eachTeamMember.GW1_DSR__c+'-'+eachTeamMember.GW1_User__c;
				if( !setExistingShareRecord.contains(key)) // if sharing already exists 
				{
					if(eachTeamMember.GW1_Is_active__c) 
                    {
                    GW1_DSR__Share DSRShareRecord = new GW1_DSR__Share();
					DSRShareRecord.parentID = eachTeamMember.GW1_DSR__c;
					DSRShareRecord.UserOrGroupId = eachTeamMember.GW1_User__c;
					if (eachTeamMember.GW1_Role__c == 'Deal Manager')
					{
						DSRShareRecord.AccessLevel = 'Edit';
					}
					else
					{
						DSRShareRecord.AccessLevel = 'Read';
					}
					
					//if(eachTeamMember.GW1_Role__c !='RFx Tower Lead')
					 listShareDSR.add(DSRShareRecord);
                   }
				}
			}
			try
			{
				insert listShareDSR;
				System.debug('@@listShareDSR::' + listShareDSR);
			}
			catch(exception e)
			{
				//excepton when no parent group exist 
				//this situation may arrise if there is no group created
				System.debug('@@listShareDSR Error::' + e);
			}
	    }

	}


	/*
	  This method will remove sharing records of user in passed parameter
	  --------------------------------------------------------------------------------------
	  Name                                Date                                Version                     
	  --------------------------------------------------------------------------------------
	  Rishi                             03-10-2016                              1.0   
	  --------------------------------------------------------------------------------------
	 */
	private void removeSharingSetting(list<GW1_DSR_Team__c> lstTriggerNew)
	{

		List<Id> listDeletedRecordId = new List<Id> ();
		List<Id> listDeletedUserId = new List<Id> ();
		for (GW1_DSR_Team__c eachTeamMember : lstTriggerNew)
		{
			listDeletedRecordId.add(eachTeamMember.GW1_DSR__c);
			listDeletedUserId.add(eachTeamMember.GW1_User__c);

		}
		List<GW1_DSR__Share> listSharedDSR = [select id from GW1_DSR__Share where parentID = :listDeletedRecordId AND RowCause = 'Manual' AND UserOrGroupId = :listDeletedUserId];

		delete listSharedDSR;

	}

	/*
	  This method is used to add the members to the chatter group
	  --------------------------------------------------------------------------------------
	  Name                                Date                                Version                     
	  --------------------------------------------------------------------------------------
	  Rishi                             03-10-2016                              1.0   
	  --------------------------------------------------------------------------------------
	 */
	private void CreateChatterMember(list<GW1_DSR_Team__c> lstTriggerNew)
	{
		if (lstTriggerNew != null && lstTriggerNew.size() > 0)
		{
			Set<ID> setDSRId = new Set<ID> ();
			set <id> setchatterGroupId = new set<id>();
			for (GW1_DSR_Team__c eachTeam : lstTriggerNew) //getting all parent DSR id in a set from team 
			{
				if (eachTeam.GW1_DSR__c != null && eachTeam.GW1_Is_active__c)
				{
					setDSRId.add(eachTeam.GW1_DSR__c);
				}
			}
			if (setDSRId.size() > 0)
			{
				// map will hold parent dsr ID and group , we can put dsr id from team and get its curosponding group
				map<ID, GW1_DSR_Group__c> mapDSRIdToDSRGroup = new map<ID, GW1_DSR_Group__c> ();
				for (GW1_DSR_Group__c objGroup :[select id, GW1_DSR__c, GW1_Chatter_Group_Id__c from GW1_DSR_Group__c
				     where GW1_Is_Primary_Group__c = true
				     AND GW1_DSR__c in :setDSRId]) // getting all dsr group whose parent is DSR
				{
					mapDSRIdToDSRGroup.put(objGroup.GW1_DSR__c, objGroup);
					if( objGroup.GW1_Chatter_Group_Id__c!=null && objGroup.GW1_Chatter_Group_Id__c!='')
						setchatterGroupId.add(objGroup.GW1_Chatter_Group_Id__c);
				}
				// Fecting existing chtter members
				map<id, Set<id>> mapGoupidToMeneberID = new map<id, Set<id>> ();
				if(setchatterGroupId!=null && setchatterGroupId.size()>0)
				{
					map<id, CollaborationGroup> mapChattergroup = new map<id, CollaborationGroup> ([select id, ownerid, (Select Id, MemberId From GroupMembers) from CollaborationGroup where id in :setchatterGroupId]);
	
					if (mapChattergroup != null && mapChattergroup.size() > 0)
					{
					// creating map Chatter group Id To Set Of memember Ids
					for (ID objChatterGroupID : mapChattergroup.keyset())
					{
						if (mapGoupidToMeneberID.get(objChatterGroupID) == null)
						mapGoupidToMeneberID.put(objChatterGroupID, new set<id> ());
						for (CollaborationGroupMember objGroupMember : mapChattergroup.get(objChatterGroupID).GroupMembers)
						{
							mapGoupidToMeneberID.get(objChatterGroupID).add(objGroupMember.MemberId); // map Chtter group to its member
						}
	
					}
				  }
			 
				list<CollaborationGroupMember> lstChatterMembr = new list<CollaborationGroupMember> ();
				for (GW1_DSR_Team__c objDSRTeam : lstTriggerNew)
				{
					if ( objDSRTeam.GW1_Is_active__c && objDSRTeam.GW1_User__c != null && objDSRTeam.GW1_DSR_Name__c != null && mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c) != null
					    && mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c).GW1_Chatter_Group_Id__c != null && mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c).GW1_Chatter_Group_Id__c != '')
					{
						// IF User is alresdy a member 
						if(mapGoupidToMeneberID.get(mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c).GW1_Chatter_Group_Id__c )!=null && !mapGoupidToMeneberID.get(mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c).GW1_Chatter_Group_Id__c  ).contains(objDSRTeam.GW1_User__c))
						{
							CollaborationGroupMember grpMr = new CollaborationGroupMember();
							grpMr.memberid = objDSRTeam.GW1_User__c;
						// passing parent object id and retrieving curosponding DSR_Group__c->GW1_Chatter_Group_Id__c 
						// as GW1_Chatter_Group_Id__c is a string type casting it to ID
							grpMr.CollaborationGroupId = (ID) mapDSRIdToDSRGroup.get(objDSRTeam.GW1_DSR__c).GW1_Chatter_Group_Id__c;
							lstChatterMembr.add(grpMr);
						}


					}
				}
				if (lstChatterMembr.size() > 0)
				try
				{
					insert lstChatterMembr;
				}
				catch(exception e)
				{
					System.debug(e);
				}
			}
			}
		}
	}
	
	/*
	  This method will remove those users who are not active 
	  --------------------------------------------------------------------------------------
	  Name                                Date                                Version                     
	  --------------------------------------------------------------------------------------
	  Rishi                             03-10-2016                              1.0   
	  --------------------------------------------------------------------------------------
	 */

	private void removeInactiveDSRMemberFromChatterGroup(list<GW1_DSR_Team__c> lstTriggerNew, Map<Id, GW1_DSR_Team__c> TriggerOldMap)
	{
		set<Id> setCollaborationGroupID = new set<Id> ();
		List<Id> listRemoveUserFromGroup = new List<Id> ();
		list<GW1_DSR_Team__c> inactiveDSRteammember = new list<GW1_DSR_Team__c> ();
		set<String> setDSROldUser = new set<string>();
		for (GW1_DSR_Team__c objTeamMember : lstTriggerNew)
		{
			if (objTeamMember.GW1_Is_active__c == false && TriggerOldMap.get(objTeamMember.Id).GW1_Is_active__c == True) //inactive changes from false to true 
			{
				if(objTeamMember.GW1_Primary_Chatter_ID__c!=null && objTeamMember.GW1_User__c!=null  )
				  setDSROldUser.add(objTeamMember.GW1_Primary_Chatter_ID__c+'-'+objTeamMember.GW1_User__c);
				  
				listRemoveUserFromGroup.add(objTeamMember.GW1_User__c);
				setCollaborationGroupID.add(objTeamMember.GW1_Primary_Chatter_ID__c);
				inactiveDSRteammember.add(objTeamMember);
			}
		}
		if (inactiveDSRteammember != null && inactiveDSRteammember.size() > 0)
		removeSharingSetting(inactiveDSRteammember); //also we need to remove their sharing settings
		List<CollaborationGroupMember> listDeleteMember = new list<CollaborationGroupMember> ();
		//List<CollaborationGroup> listgroup = [select Id, name from CollaborationGroup where id = :setCollaborationGroupID];
		//if (listgroup != null && listgroup.size() > 0)
		listDeleteMember = [select id,memberid,CollaborationGroupId from CollaborationGroupMember where memberid = :listRemoveUserFromGroup AND CollaborationGroupId = :setCollaborationGroupID];
		system.debug('(in class)listDeleteMember ' + listDeleteMember +' '+ listRemoveUserFromGroup +' '+ setCollaborationGroupID);
		//removing cross join case // Added by pankaj
		list<CollaborationGroupMember > lstChatterMemberTodelete = new list<CollaborationGroupMember>();
		
		for (CollaborationGroupMember objMember : listDeleteMember)
		{
			if(objMember.CollaborationGroupId!=null && objMember.memberid!=null )
			 {
			 	string key=objMember.CollaborationGroupId+'-'+objMember.memberid;
				if(setDSROldUser.contains(key))
				{
					lstChatterMemberTodelete.add(objMember);
				} 
			 }
		}

		System.debug('@@@@lstChatterMemberTodelete' + lstChatterMemberTodelete);

		if (lstChatterMemberTodelete != null && lstChatterMemberTodelete.size() > 0)
		{

			try
			{
				delete lstChatterMemberTodelete;
			}
			catch(exception e)
			{
				if (e.getMessage().contains('INVALID_OPERATION, You cannot delete the owner from a group of type collaboration.'))
				{
					lstTriggerNew[0].adderror('Before deactivating this user please transfer chatter group owner to other member');
				}
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------     
	//  Description : This method will add Active member to Chatter Group
	//  
	//------------------------------------------------------------------------------------------------------- 
	// Created :  4-APR-2016  Created by: Pankaj Adhiakri Email:pankaj.adhikari@saasfocus.com
	//-------------------------------------------------------------------------------------------------------   


	private void addActiveMemberToChatterGroup(list<GW1_DSR_Team__c> lstTriggerNew, Map<Id, GW1_DSR_Team__c> TriggerOldMap)
	{
		list<GW1_DSR_Team__c> listADDUserFromGroup = new list<GW1_DSR_Team__c> ();
		for (GW1_DSR_Team__c objTeamMember : lstTriggerNew)
		{
			// if team member is active form inactive 
			if (objTeamMember.GW1_Is_active__c == true && TriggerOldMap.get(objTeamMember.Id).GW1_Is_active__c == false) //inactive changes from false to true 
			{
				listADDUserFromGroup.add(objTeamMember);

			}
		}

		if (listADDUserFromGroup != null && listADDUserFromGroup.size() > 0)
		{
			addChatterMembers(listADDUserFromGroup);
		}
	}
}