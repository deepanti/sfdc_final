public class GPOptions {
    @AuraEnabled
    public String text{get; set;}
    @AuraEnabled
    public String label{get; set;}
    @AuraEnabled
    public String externalParameter{get; set;}
    @AuraEnabled
    public String externalParameter2{get; set;}
    //HSN Changes
    @AuraEnabled
    public String externalParameter3{get; set;}
    
    public GPOptions(String text, String label) {
        this.text = text;
        this.label = label;
    }

    public GPOptions(String text, String label, String externalParameter) {
        this.text = text;
        this.label = label;
        this.externalParameter = externalParameter;
    }

    public GPOptions(String text, String label, String externalParameter, String externalParameter2) {
        this.text = text;
        this.label = label;
        this.externalParameter = externalParameter;
        this.externalParameter2 = externalParameter2;
    }
    
    //HSN Changes
    public GPOptions(String text, String label, String externalParameter, String externalParameter2, String externalParameter3) {
        this.text = text;
        this.label = label;
        this.externalParameter = externalParameter;
        this.externalParameter2 = externalParameter2;        
        this.externalParameter3 = externalParameter3;
    }
}