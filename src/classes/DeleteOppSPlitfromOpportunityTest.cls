@isTest
public class DeleteOppSPlitfromOpportunityTest{
    
    public static Opportunity opp;
    public static Account oAccount;
    public static OpportunitySplit opp_split;
    public static OpportunitySplit opp_split1;
    
    public static User u, u1, u3, u4, u5;
    public static OpportunitySplitType oppSplitType;
    public static OpportunityTeamMember opp_team_member;
    public static OpportunityTeamMember opp_team_member1;
    public static OpportunityLineItem opp_item;
    
    
    
    public static testMethod void CreateOppTeamonOppCreationTest(){  
        setupTestData();
        test.startTest();
        oAccount.Client_Partner__c=u3.id;
        update oAccount;
        List<Opportunity> oppList = new List<Opportunity>();
        List<OpportunityTeamMember> oppTeamMember = new List<OpportunityTeamMember>();
        oppList.add(opp);
        oppTeamMember.add(opp_team_member);
        oppTeamMember.add(opp_team_member1);
        DeleteOppSPlitfromOpportunity.CreateOppTeamonOppCreation(oppList);
        Map<ID, Opportunity> old_opp_map = new Map<ID, Opportunity>();
        old_opp_map.put(opp.ID, opp);
        DeleteOppSPlitfromOpportunity.deleteOppSplitOwneronOppOwnerchange(oppList,old_opp_map);
        test.stoptest();
    }  
    public static testMethod void deleteOppSplitOwneronOppOwnerchangeTest(){  
        setupTestData();
        test.startTest();
        
        List<Opportunity> oppList = new List<Opportunity>();
        opp.ownerID = u1.id;
       
        Map<ID, Opportunity> old_opp_map = new Map<ID, Opportunity>();
        old_opp_map.put(opp.ID, opp);
        system.debug(':--opp owner Id--:'+opp.OwnerId);
        opp.ownerID = u5.id;
        system.debug(':--opp owner Id--:'+opp.OwnerId);
        update opp;
        oppList.add(opp);
        
        DeleteOppSPlitfromOpportunity.deleteOppSplitOwneronOppOwnerchange(oppList,old_opp_map);
        
        test.stoptest();
    }  
    
    
    private static void setupTestData(){
      
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        u4 =GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        u5 =GEN_Util_Test_Data.CreateUser('standarduser2079@testorg.com',p.Id,'standardusertestgen2079@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u3.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        oAccount.Client_Partner__c=u1.id;
        update oAccount;
        
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        System.runAs(u){
            
            opp =new opportunity(name='1234',ownerId = u.id,StageName='1. Discover',CloseDate=system.today()+1,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive',Super_SL_overlay_created__c=False,SL_overlay_created__c=false,Opportunity_Source__c='Ramp up');
            insert opp; 
            
            oppSplitType = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
            opp_split = new OpportunitySplit(SplitPercentage =100,SplitTypeId =oppSplitType.id, SplitOwnerId =u3.id,OpportunityId =opp.id);
           
            opp_split1 = new OpportunitySplit(SplitPercentage =100,SplitTypeId =oppSplitType.id, SplitOwnerId =u1.id,OpportunityId =opp.id);
           
            opp_team_member= new OpportunityTeamMember(OpportunityAccessLevel='Edit',OpportunityId =opp.id,UserId =u3.id,TeamMemberRole ='Super SL');
            
            insert opp_team_member;
             Product2 product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
             Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            
             opp_team_member1= new OpportunityTeamMember(OpportunityAccessLevel='Edit',OpportunityId =opp.id,UserId =u1.id,TeamMemberRole ='SL');
             opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,Quantity = 3); 
          //  insert opp_item; 
        }
    }
    
}