@isTest
public class RestrictedViewProfileTogglingTest {
     @testSetup
    public static void setupData()
    {
        User testUser=TestDataFactoryUtility.createTestUser('Genpact Super Admin', 'email@acme.com', 'email@xyzasd.com');
        insert testUser;
    }
	@istest
    public static void testFunctions()
    {
        test.startTest();
        User testUser=[select id from user where email='email@xyzasd.com'];
        Profile prof=[select id from Profile where name='Genpact Super Admin'];
        System.runAs(testUser){
            RestrictedViewProfileToggling.toggleProfile(testUser.id, prof.id);
        }
        test.stopTest();
     
    }
}