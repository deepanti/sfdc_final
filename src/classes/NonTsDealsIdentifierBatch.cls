global class NonTsDealsIdentifierBatch implements Database.Batchable<sObject>, Database.Stateful {
	
    global Set<User> stale_user_list = new Set<User>();
    
    public NonTsDealsIdentifierBatch(Set<User> user_Set){
        if(user_Set.size() > 0){
            stale_user_list.addAll(user_Set);
        }
    }
    
    
     global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
    {  
        return Database.getQueryLocator([Select ownerid,id,Name,QSRM_status2__c, Opportunity_Source__c, deal_cycle_ageing__c, Type_of_deal_for_non_ts__c,
                                           Opportunity_Age__c ,Type_of_deal__c,TSInights__c ,StageName,Amount,Accountid,
                                           Account.Name,Last_stage_change_date__c,Previous_Stage__c,TCV1__c,CloseDate, 
                                           Transformation_ageing__c,QSRM_Type__c,Move_Deal_Ahead__c,Cycle_Time__c, Contract_Status__c, 
                                           Formula_Hunting_Mining__c,NonTsByPassUI__c,Insight__c,Transformation_deal_ageing_for_non_ts__c,
                                           CloseDatewithInsight__c,Deal_Type__c,Nature_of_Work_highest__c,Discover_MSA_CT_Check__C,
                                           Define_MSA_CT_Check__C,Onbid_MSA_CT_Check__c,DownSelect_MSA_CT_Check__C,Confirmed_MSA_CT_Check__C 
                                           from opportunity where (Type_Of_Opportunity__c = 'Non Ts' OR Type_Of_Opportunity__c = 'Non TS')
                                           and Transformation_14_days_check_age__c='True' and (StageName ='1. Discover' OR StageName = 
                                           '2. Define' OR StageName ='3. On Bid' OR StageName ='4. Down Select' OR StageName ='5. Confirmed')]);
    }
    
     global void execute(Database.BatchableContext BC, List<sObject> oppList)           // Execute Logic    
    {  
        list<User> userList = staleDealOwners.addTsDealsUsers(opplist, 'NON_TS_DEALS');
        System.debug('userList in batch=='+userList);
        if(userList != null){
        	stale_user_list.addAll(userList);     
        }
        System.debug('stale_user_list===='+stale_user_list.size());
    }
    
    global void finish(Database.BatchableContext BC)
    {
        System.debug('stale user size=='+stale_user_list.size());
        update new List<User>(stale_user_list);
    }

}