@isTest(SeeAllData=false)
public class VIC_CalculateUpfrontIncBatchTest{
    public static OpportunityLineItem  objOLI;
    public static list<Target_Component__c> lsttc= new list<Target_Component__c>();
    public static user  objuser;
    
    static testMethod void ValidateVIC_CalculateUpfrontIncentive()
    {
        LoadData();    
        VIC_CalculateUpfrontIncentivesBatch obj =new VIC_CalculateUpfrontIncentivesBatch(false);
        DataBase.executeBatch(obj);
        System.AssertEquals(200,200);
    }
    
    static testMethod void ValidateVIC_CalculateUpfrontIncentiveNew()
    {
        LoadData();    
        
        
               
        system.debug('objOLI'+objOLI.vic_Contract_Term__c);
        VIC_CalculateUpfrontIncentivesBatch obj =new VIC_CalculateUpfrontIncentivesBatch(true);
        DataBase.executeBatch(obj);
        new VIC_UpfrontIncentivesBatchHelper().willIncentiveRecordCreatedForUser(objOLI,objuser.id);
        VIC_UpfrontIncentivesBatchHelper UpfrontIncentivesBatchHelper= new VIC_UpfrontIncentivesBatchHelper();
        UpfrontIncentivesBatchHelper.updateOldTCVForUser(objuser.id,objOLI,500);
        
        System.AssertEquals(200,200);
        Exception ex;
        LoggerUtility.logError('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH );
        LoggerUtility.logAPIError('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH,'','' );
        LoggerUtility.logAPIException('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH,'','', ex);
        LoggerUtility.logMessage('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH,'','', ex);
        LoggerUtility.logException('VIC_CalculateUpfrontIncentivesBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.BATCH,ex);
        LoggerUtility.commitLog();
    }
 
    static void LoadData()
    {
         VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
         vicInfo.VIC_Process_Year__c=2018;
         insert vicInfo;
        
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn11@jdjhdg.com','Genpact Super Admin','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
            
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
            
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
            
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
            
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
            
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';    
        insert planobj1;
                
            
        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser.id;
        objopp.Sales_country__c='Canada';
        
        insert objOpp;
        
        
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_IO_TS__c='IO';
        objOLI.TCV__c=6;
        objOLI.vic_TCV_EBIT_Consideration_ForNPV__c='6';
        objOLI.vic_Contract_Term__c=6;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;  
        objOLI.Y1_AOI_USD_OLI_CPQ__c='5';    
        objOLI.Y2_AOI_USD_OLI_CPQ__c='5';
        objOLI.Y3_AOI_USD_OLI_CPQ__c='5';
        objOLI.Y4_AOI_USD_OLI_CPQ__c='5';
        objOLI.Y5_AOI_USD_OLI_CPQ__c='5';
        objOLI.Y6_AOI_USD_OLI_CPQ__c='5';
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.vic_IO_TS__c ='IO';    
        insert objOLI;
        system.debug('objOLI'+objOLI.vic_Contract_Term__c);
        objOLI=[select id,Opportunity.StageName,vic_is_Eligible_For_NPV_Calculation__c,Product_BD_Rep__c,
                vic_VIC_User_3__c,vic_VIC_User_4__c,
                TCV_EBIT_Consideration_ForNPV_Fx__c ,vic_Contract_Term__c,VIC_NPV__c,Opportunity.ownerid,
                Opportunity.Actual_Close_Date__c,vic_CPQ_Deal_Status__c,vic_Final_Data_Received_From_CPQ__c,
                vic_Sales_Rep_Approval_Status__c,vic_Product_BD_Rep_Approval_Status__c,vic_is_CPQ_Value_Changed__c,
                VIC_IsIncentiveCalculatedForPrimaryRep__c,VIC_IsIncentiveCalculatedForBDRep__c from opportunitylineitem 
                where id=:objOli.id];       
        
       
        objopp.Number_of_Contract__c=2;    
        objOpp.stagename='6. Signed Deal';
        
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        test.starttest();
       
        system.runas(objuser){
       
        update objOpp;
        insert targetObj;
        
        }
        
       
        
        
        test.stoptest();
        
         
        Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='Renewal'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
        
        Master_Plan_Component__c masterPlanComponentObj2=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj2.vic_Component_Code__c='Profitable_Bookings_TS'; 
        masterPlanComponentObj2.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj2;
        
         Master_Plan_Component__c masterPlanComponentObj3=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
         
        masterPlanComponentObj3.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj3;
         Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_IO'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
            
       
        
        
            
        Plan_Component__c PlanComponentObj1 =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj1.id,planobj1.id);
        insert PlanComponentObj1;
        
         Plan_Component__c PlanComponentObj2 =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj2.id,planobj1.id);
        insert PlanComponentObj2;
        
        Plan_Component__c PlanComponentObj3 =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj3;
        
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        lsttc.add(targetComponentObj);
        
        Target_Component__c targetComponentObj1=VIC_CommonTest.getTargetComponent();
        targetComponentObj1.Target__C=targetObj.id;
        targetComponentObj1.Master_Plan_Component__c=masterPlanComponentObj1.id;
        lsttc.add(targetComponentObj1);
        
        Target_Component__c targetComponentObj2=VIC_CommonTest.getTargetComponent();
        targetComponentObj2.Target__C=targetObj.id;
        targetComponentObj2.Master_Plan_Component__c=masterPlanComponentObj2.id;
        lsttc.add(targetComponentObj2);
        
        insert lsttc;
        Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(lsttc[0].id,1000);
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        insert objInc;
        
        

        
       
    }
}