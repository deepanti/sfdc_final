global class GPBatchCreateVersionHistoryDocument implements Database.Batchable<sObject>, Database.AllowsCallouts, schedulable {    
    global void execute(SchedulableContext sc) {
        GPBatchCreateVersionHistoryDocument scheduleCreateDoc = new GPBatchCreateVersionHistoryDocument();
        Database.executebatch(scheduleCreateDoc, 1); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String projectQuery = 'Select id, Name, GP_Project__r.GP_Oracle_PID__c, GP_Oracle_Status__c, GP_Status__c, GP_Version_No__c, GP_Project__c'
                + ' from GP_Project_Version_History__c where GP_Status__c = \'Approved\''
                + ' AND GP_Oracle_Status__c = \'S\''
                + ' AND GP_Content_Version_Document_Id__c = null'
                + ' AND GP_Project__r.GP_Approval_DateTime__c != null'
                + ' AND GP_Project__r.GP_Oracle_Status__c = \'S\''
                + ' AND GP_Project__r.GP_Approval_Status__c = \'Approved\'';
        return Database.getQueryLocator(projectQuery);
    }
    
    global void execute(Database.BatchableContext bc, List <GP_Project_Version_History__c> lstOfVersionHistory) {
        List < ContentVersion > listofContentVersion = new List < ContentVersion > ();
        List < ContentDocumentLink > listofContentDocumentLink = new List < ContentDocumentLink > ();
        Map < Id, ContentVersion > mapOfLinkedEntityIdVsContentversion = new Map< Id, ContentVersion >();
        Set < Id > setOfContentversionIds = new Set< Id >();
        PageReference pageExcel;
        ContentVersion objContentVersion;
        
        for(GP_Project_Version_History__c versionHistoryRecord : lstOfVersionHistory) {
            pageExcel = new PageReference('/apex/GPGenerateProjectFileEXL');
            pageExcel.getParameters().put('id', versionHistoryRecord.GP_Project__c);
            pageExcel.getParameters().put('selectedStatus', 'modify');
            objContentVersion = new ContentVersion();
            
            if(Test.isRunningTest()) {
                objContentVersion.versionData = Blob.valueof('test');  
            } else { 
                objContentVersion.versionData = pageExcel.getContent();
            }
            
            objContentVersion.PathOnClient = versionHistoryRecord.GP_Project__r.GP_Oracle_PID__c + '-' + system.now() + '.xls';
            mapOfLinkedEntityIdVsContentversion.put(versionHistoryRecord.GP_Project__c,objContentVersion);            
            listofContentVersion.add(objContentVersion);    
        }
        
        if (listofContentVersion.size() > 0) {
            insert mapOfLinkedEntityIdVsContentversion.values();
            
            for(ContentVersion contentVersion : listofContentVersion) {
                setOfContentversionIds.add(contentVersion.Id);
            }
            
            Map < Id, Id > mapOfContentVersionIDVsContentDocumentID = new Map < Id, Id > ();
            
            for (ContentVersion contentversionobj: [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: setOfContentversionIds]) {
                mapOfContentVersionIDvsContentDocumentID.put(contentversionobj.Id, contentversionobj.ContentDocumentId);
            }
            
            for(GP_Project_Version_History__c versionHistoryRecord : lstOfVersionHistory) {
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = versionHistoryRecord.id;
                versionHistoryRecord.GP_Content_Version_Document_Id__c = mapOfLinkedEntityIdVsContentversion.get(versionHistoryRecord.GP_Project__c).Id;
                versionHistoryRecord.GP_Document_Creation_Date__c = system.today();
                cdl.ContentDocumentId = mapOfContentVersionIDvsContentDocumentID.get(mapOfLinkedEntityIdVsContentversion.get(versionHistoryRecord.GP_Project__c).Id);
                cdl.ShareType = 'V';
                listofContentVersion.add(objContentVersion);
                listofContentDocumentLink.add(cdl);     
            }
            
            insert listofContentDocumentLink;
            update lstOfVersionHistory;
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
}