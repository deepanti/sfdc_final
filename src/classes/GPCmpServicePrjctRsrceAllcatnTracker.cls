@isTest
public class GPCmpServicePrjctRsrceAllcatnTracker {

    public static GP_Resource_Allocation__c objResourceAllocation;
    public static GP_Employee_Master__c empObj;
    public static GP_Project__c prjObj;
    public static GP_Work_Location__c objSdo;
    public static String jsonresp;

    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-Employee';
        empObj.GP_DESIGNATION__c = 'test';
        empObj.GP_isActive__c = true;
        empObj.GP_Final_OHR__c = '1234567';
        empObj.GP_Resource_Id__c = 'kjadck';
        insert empObj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objPinnacleMaster.GP_Last_Defined_Period_Date__c = system.today();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
		
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        insert parentProject;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('CMITS').getRecordTypeId();
        prjObj.GP_Parent_Project__c = parentProject.id;
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id; 
        prjObj.GP_Operating_Unit__c = objPinnacleMaster.Id;
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_End_Date__c = system.today().addDays(1);
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        
        insert prjObj;

        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id, prjObj.id);
        objResourceAllocation.GP_Project__c = prjObj.id;
        objResourceAllocation.GP_Bill_Rate__c = 0;
        objResourceAllocation.GP_Employee__c = empObj.id;
        objResourceAllocation.GP_Work_Location__c = objSdo.id;
        objResourceAllocation.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation.GP_Percentage_allocation__c = 1;
        objResourceAllocation.GP_SDO__c = objSdo.Id;
        insert objResourceAllocation;
        
		GP_Project_Work_Location_SDO__c projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Project__c = prjObj.Id;
		projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Primary__c = true;
        projectWorkLocation.GP_Project__c = prjObj.Id;
		projectWorkLocation.GP_Work_Location__c = objSdo.Id;
        
        insert projectWorkLocation;
		
        GP_Profile_Bill_Rate__c objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Project__c = prjObj.id;
        insert objProfileBillRate;
    }

    @isTest static void testGPCmpServiceProjectResourceAllocation() {
        fetchData();
        testgetResourceAllocationData();
        testgetResourceAllocationData2();
        testgetLendingSDO();
        testFailTerminationDateSaveResourceAllocationData();
        testgetProcessedCSVRecordsService();
        testSaveResourceAllocationDataInvalid();
        testSaveResourceAllocationData();
        testdeleteResourceAllocationData();
        testdeleteResourceAllocationDataCatch();
        testgetLendingSDOCMITSEmployee();
        //testgetResourceAllocationServiceRelatedDetails();
        //testgetEmployeeHRDataService();
    }

    public static void fetchData() {
        objResourceAllocation = [select id, Name, GP_Employee__c, GP_Start_Date__c, GP_End_Date__c,GP_Project__c from GP_Resource_Allocation__c limit 1];
        prjObj = [select id, Name,GP_Primary_SDO__c from GP_Project__c where GP_Parent_Project__c != null limit 1];
        empObj = [select id from GP_Employee_Master__c limit 1];
        jsonresp = (String) JSON.serialize(objResourceAllocation);
        objSdo = [select id from GP_Work_Location__c where id =:prjObj.GP_Primary_SDO__c];
    }

    /*public static void testgetProjectResourceAllocationData(){
GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getProjectResourceAllocationData(objResourceAllocation.id);
System.assertEquals(true, returnedResponse.isSuccess);
}*/
    public static void testFailTerminationDateSaveResourceAllocationData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.saveResourceAllocationData(jsonresp);
        
    }

    public static void testSaveResourceAllocationData() {
        objResourceAllocation.GP_Start_Date__c = system.today();
        objResourceAllocation.GP_End_Date__c = system.today().addDays(30);
        update objResourceAllocation;
        empObj.GP_HIRE_Date__c = system.today().addDays(-10);
        empObj.GP_ACTUAL_TERMINATION_Date__c = system.today().addDays(40);
        empObj.GP_EMPOYMENT_CATEGORY__c = 'Category';
        empObj.GP_DESIGNATION__c = 'Designation';
        update empObj;
        jsonresp = (String) JSON.serialize(objResourceAllocation);
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.saveResourceAllocationData(jsonresp);
        
    }
    public static void testSaveResourceAllocationDataInvalid() {
        objResourceAllocation.GP_Start_Date__c = system.today();
        objResourceAllocation.GP_End_Date__c = system.today().addDays(30);
        update objResourceAllocation;
        empObj.GP_HIRE_Date__c = system.today().addDays(-10);
        empObj.GP_ACTUAL_TERMINATION_Date__c = system.today().addDays(40);
        update empObj;
        jsonresp = (String) JSON.serialize(objResourceAllocation);
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.saveResourceAllocationData(jsonresp);
        
    }

    public static void testdeleteResourceAllocationData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.deleteResourceAllocationData(objResourceAllocation.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testdeleteResourceAllocationDataCatch() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.deleteResourceAllocationData(null);
        System.assertEquals(false, returnedResponse.isSuccess);
    }

    /*public static void testgetResourceAllocationServiceRelatedDetails(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getResourceAllocationServiceRelatedDetails(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);        
    }*/

    public static void testgetResourceAllocationData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getResourceAllocationData(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetResourceAllocationData2() {
        prjObj.GP_End_Date__c = system.today().addDays(-1);
        update prjObj;
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getResourceAllocationData(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetLendingSDO() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getLendingSDO(empObj.id, prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    public static void testgetLendingSDOCMITSEmployee() {
        objSdo.GP_Business_Name__c = 'CMITS';
        update objSdo;
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getLendingSDO(empObj.id, prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }
    
    public static void testgetProcessedCSVRecordsService() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getProcessedCSVRecordsService((String) JSON.serialize(new List<GP_Resource_Allocation__c>{objResourceAllocation}), prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
        GPAuraResponse returnedResponse2 = GPCmpServiceProjectResourceAllocation.getProcessedCSVRecordsService(jsonresp, prjObj.id);
        System.assertEquals(false, returnedResponse2.isSuccess);
    }

    /*public static void testgetEmployeeHRDataService(){
        GPAuraResponse returnedResponse = GPCmpServiceProjectResourceAllocation.getEmployeeHRDataService(empObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);        
    }*/
}