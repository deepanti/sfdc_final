@isTest
public class GPCmpServiceProjectAddressTracker {

    public static GP_Project__c project;
    public static GP_Customer_Master__c customerMaster;
    public static GP_Project_Address__c projectAddress;
    public static GP_Pinnacle_Master__c billingEntity;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Description__c = 'Sample Description';
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal('CMITS', 'group1', 'segment2');
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate('CMITS', 'group1', 'segment2');
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert prjObj;
        
        
        customerMaster = new GP_Customer_Master__c(Name = 'CVS');
        insert customerMaster;
        
        GP_Address__c address = new GP_Address__c();
        
        address.GP_Address_Line_1__c = 'Dummy Address';
        address.GP_Billing_Entity__c = objpinnacleMaster.Id;
        address.GP_Status__c = 'Active';
        
        insert address;
        
        address =GPCommonTracker.getAddress(customerMaster.Id, objpinnacleMaster.Id, 'SHIP_To');        
        insert address;
        
        address = GPCommonTracker.getAddress(customerMaster.Id, objpinnacleMaster.Id, 'BILL_To');        
        insert address;
        
        
        address = GPCommonTracker.getAddress(customerMaster.Id, objpinnacleMaster.Id, '');        
        insert address;
        
        address = GPCommonTracker.getAddress(customerMaster.Id, objpinnacleMaster.Id, 'BILL_To');        
        address.GP_Customer__c = null;
        insert address;
    }

    @isTest
    public static void testGPCmpServiceProjectAddress() {
        fetchData();
        testGetProjectAddress();
        testsaveProjectAddress();
        testdeleteProjectAddress();
        testgetFilteredListOfAddress();
    }

    public static void fetchData() {
        project = [select id, Name from GP_Project__c limit 1];
        customerMaster = [Select Id from GP_Customer_Master__c limit 1];
        billingEntity = [Select Id from GP_Pinnacle_Master__c limit 1];
    }

    public static void testGetProjectAddress() {
    	GPAuraResponse response = GPCmpServiceProjectAddress.getProjectAddress(null);
    	System.assert(true, response.isSuccess);
    }
    
    public static void testsaveProjectAddress() {
    	GPAuraResponse response = GPCmpServiceProjectAddress.saveProjectAddress(null);
    	System.assert(true, response.isSuccess);
        
        setProjectAddress();
		List<GP_Project_Address__c> listOfProjectAddress = new List<GP_Project_Address__c>();
        listOfProjectAddress.add(projectAddress);
        
    	response = GPCmpServiceProjectAddress.saveProjectAddress(JSON.serialize(listOfProjectAddress));
    	System.assert(true, response.isSuccess);
		
		setProjectAddressNoUSCanada();
        List<GP_Project_Address__c> listOfProjectAddressNonUS = new List<GP_Project_Address__c>();
        listOfProjectAddressNonUS.add(projectAddress);
        
    	response = GPCmpServiceProjectAddress.saveProjectAddress(JSON.serialize(listOfProjectAddressNonUS));
    	System.assert(true, response.isSuccess);
        
        setProjectAddressShipTo();
		List<GP_Project_Address__c> listOfProjectAddressShipTo = new List<GP_Project_Address__c>();
        listOfProjectAddressShipTo.add(projectAddress);
        
    	response = GPCmpServiceProjectAddress.saveProjectAddress(JSON.serialize(listOfProjectAddressShipTo));
    	System.assert(true, response.isSuccess);
        
        setProjectAddressNoUSCanadaShipTo();
        List<GP_Project_Address__c> listOfProjectAddressNonUSShipTo = new List<GP_Project_Address__c>();
        listOfProjectAddressNonUSShipTo.add(projectAddress);
        
    	response = GPCmpServiceProjectAddress.saveProjectAddress(JSON.serialize(listOfProjectAddressNonUSShipTo));
    	System.assert(true, response.isSuccess);
    }
    
    
    public static void setProjectAddress() {
        
        GP_Address__c gpAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpAddr.GP_City__c='CALIFORNIA';
        gpAddr.GP_Country__c='US';
        gpAddr.GP_State__c='CA';
        gpAddr.GP_Site_Use_Code__c='BILL_TO';
        insert gpAddr;
        
      
        
        projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c  = project.Id;
        projectAddress.GP_Bill_To_Address__c = gpAddr.id;
       
        
    }    
    public static void setProjectAddressNoUSCanada() {
        
        GP_Address__c gpAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpAddr.GP_City__c='test';
        gpAddr.GP_Country__c='test';
        gpAddr.GP_State__c='test';
        gpAddr.GP_Site_Use_Code__c='BILL_TO';
        insert gpAddr;
		
		 GP_Address__c gpShipToAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpShipToAddr.GP_City__c='test';
        gpShipToAddr.GP_Country__c='test';
        gpShipToAddr.GP_State__c='test';
        gpShipToAddr.GP_Site_Use_Code__c='SHIP_TO';
        insert gpShipToAddr;
        
        projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c  = project.Id;
        projectAddress.GP_Bill_To_Address__c = gpAddr.id;
        projectAddress.GP_Ship_To_Address__c = gpShipToAddr.id;
        
    }
	public static void setProjectAddressShipTo() {
        
        GP_Address__c gpAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpAddr.GP_City__c='CALIFORNIA';
        gpAddr.GP_Country__c='US';
        gpAddr.GP_State__c='CA';
        gpAddr.GP_Site_Use_Code__c='SHIP_TO';
        insert gpAddr;
        
       
        
        projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c  = project.Id;
        projectAddress.GP_Ship_To_Address__c = gpAddr.id;
        
        
    }    
    public static void setProjectAddressNoUSCanadaShipTo() {
        
        GP_Address__c gpAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpAddr.GP_City__c='test';
        gpAddr.GP_Country__c='test';
        gpAddr.GP_State__c='test';
        gpAddr.GP_Site_Use_Code__c='SHIP_TO';
        insert gpAddr;
        
         GP_Address__c gpBillTiAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpBillTiAddr.GP_City__c='test';
        gpBillTiAddr.GP_Country__c='test';
        gpBillTiAddr.GP_State__c='test';
        gpBillTiAddr.GP_Site_Use_Code__c='BILL_TO';
        insert gpBillTiAddr;
        
        projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c  = project.Id;
        projectAddress.GP_Ship_To_Address__c = gpAddr.id;
        projectAddress.GP_Bill_To_Address__c = gpBillTiAddr.id;
        
    }  
    
    
    public static void testdeleteProjectAddress() {
        projectAddress = [Select Id FROM GP_Project_Address__c LIMIT 1]; 
    	GPAuraResponse response = GPCmpServiceProjectAddress.deleteProjectAddress(null);
    	response = GPCmpServiceProjectAddress.deleteProjectAddress(projectAddress.Id);
    	System.assert(true, response.isSuccess);
    }
    
    public static void testgetFilteredListOfAddress() {
        
    	GPAuraResponse response = GPCmpServiceProjectAddress.getFilteredListOfAddress(null, null);
    	System.assert(true, response.isSuccess);
        
        List<Id> listOfCustomerId = new List<Id> {
          	customerMaster.Id , null 
        };
            
    	response = GPCmpServiceProjectAddress.getFilteredListOfAddress(JSON.serialize(listOfCustomerId), null);
        response = GPCmpServiceProjectAddress.getFilteredListOfAddress(JSON.serialize(listOfCustomerId), billingEntity.Id);
    }    
}