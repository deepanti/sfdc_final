/**
* @Description: Class used to calculate MBO Component incentive 
* @author: Bashim Khan
* @date: May 2018
*/
 public class VIC_CalculateMBOIncentivectrl implements VIC_ScorecardCalculationInterface {
     public Decimal opAcheivmentPercent{get;set;}
     public Decimal calculate(Target_Component__c tc, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixIte){
        Decimal incentive = 0;
        Set<String> plansMBOIncentive = new Set<String>{'SL_CP_Enterprise','SEM','SL_CP_CMIT'};
        if(plansMBOIncentive.contains(tc.Target__r.Plan__r.vic_Plan_Code__c)){
            if(tc.Target__r.Target_Bonus__c !=null && tc.Weightage__c !=null && tc.vic_Achievement_Percent__c !=null){
                Decimal targetedIncentive = tc.Target__r.Target_Bonus__c * tc.Weightage__c/100;
                Decimal maxMBOIncentive =   tc.Target__r.Plan__r.vic_Plan_Code__c=='SEM'?Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Maximum_MBO_Incentive_SEM_Plan')):
                                            Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Maximum_MBO_Incentive_SL_CP_Plan'));
             
                
                if(tc.Target__r.Plan__r.vic_Plan_Code__c == 'SEM' && opAcheivmentPercent > 90){
                    incentive = targetedIncentive * (tc.vic_Achievement_Percent__c<=maxMBOIncentive?tc.vic_Achievement_Percent__c:maxMBOIncentive)/100;
                }else if(tc.Target__r.Plan__r.vic_Plan_Code__c == 'SL_CP_Enterprise' || tc.Target__r.Plan__r.vic_Plan_Code__c == 'SL_CP_CMIT'){
                    incentive = targetedIncentive * (tc.vic_Achievement_Percent__c<=maxMBOIncentive?tc.vic_Achievement_Percent__c:maxMBOIncentive)/100;
                }
            } 
        }
        return incentive;
    }    
    
  

}