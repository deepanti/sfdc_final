/**
 *
 * @group Project Billing Milestone.
 * @group-content ../../ApexDocContent/ProjectBillingMilestone.html.
 *
 * @description Apex Controller for Project Billing Milestone.
 */
public class  GPControllerProjectBillingMilestones {

    private final String LIST_OF_BILLING_MILESTONE_TEMPLATE_LABEL = 'mapOfFieldNameToTemplate';
    private final String LIST_OF_MILESTONE_TYPE_OPTIONS_LABEL = 'listOfMilestoneTypeOptions';
    private final String LIST_OF_BILLING_MILESTONE_LABEL = 'listOfBillingMilestone';
    private final String IS_MILESTONE_DEFINABLE_LABEL = 'isMileStoneDefinable';
    private final String NO_OF_ROWS_TO_BE_DISPLAYED = 'noOfRowsToBeDisplayed';
    private final String PROJECT_TCV_AMOUNT_LABEL = 'projectTCVAmount';
    private final String PROJECT_START_DATE_LABEL = 'projectStartDate';
    private final String MAP_WORKLOCATION_LABEL = 'mapOfWorkLocation';
    private final String CURRENCY_ISO_CODE_LABEL = 'currencyISOCode';
    private final String PROJECT_END_DATE_LABEL = 'projectEnDate';
    private final String IS_PROJECT_ITO_TYPE_LABEL = 'isITOType';
    private final String WORK_LOCATION_LABEL = 'workLocation';
    private final String ISUPDATEABLE_LABEL = 'isUpdatable';
    private final String SUCCESS_LABEL = 'SUCCESS';

    private final GPOptions nullOption = new GPOptions(null, '--Select--');

    private Map < String, GPProjectTemplateFieldWrapper > mapOfBillingMilestoneFieldKeyToFieldTemplate;
    private List < GP_Billing_Milestone__c > listOfBillingMilestone;
    private List < GPOptions > listOfMilestoneTypeOptions;
    private List < GPOptions > relatedWorkLocations;
    private Integer noOfRowsToBeDisplayed = 0;
    private Map < String, Id > mapOfWorkLocation;
    private Boolean isMileStoneDefinable;
    private Double projectTCVAmount;
    private String currencyISOCode;
    private Date projectStartDate;
    private Date projectEndDate;
    private Boolean isUpdatable;
    private JSONGenerator gen;
    private Boolean isITOType;
    private Id projectId;


    /**
     * @description Parameterized constructor to accept Project Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerProjectBillingMilestones(<SFDC 18/15 digit project Id>);
     */
    public GPControllerProjectBillingMilestones(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description Parameterized constructor to accept List of Billing Milestones.
     * @param listOfBillingMilestone : List of Billing Milestones
     * 
     * @example
     * new GPControllerProjectBillingMilestones(<List Of Billing Milestones>);
     */
    public GPControllerProjectBillingMilestones(List < GP_Billing_Milestone__c > listOfBillingMilestone) {
        this.listOfBillingMilestone = listOfBillingMilestone;
    }

    public GPControllerProjectBillingMilestones() {}

    /**
     * @description method to return Billing Milestones.
     *
     * @example
     * new GPControllerProjectBillingMilestones(<SFDC 18/15 digit project Id>).getBillingMilestone();
     */
    public GPAuraResponse getBillingMilestone() {
        try {
            setFieldTemplate();
            setListOfBillingMilestone();
            setBillingMilestoneTypeOptions();
            setRelatedWorkLocation();
            setJson();
            return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    /**
     * @description Query Billing Milestone Records.
     * 
     */ 
    private void setListOfBillingMilestone() {
        if (isMileStoneDefinable)
            listOfBillingMilestone = new GPSelectorBillingMilestone().selectProjectBillingMilestoneRecords(projectId);
    }

    /**
     * @description method to upsert Billing Milestone Records.
     * 
     * @example
     * new GPControllerProjectBillingMilestones(<List Of Billing Milestones>).upsertBillingMilestone();
     */
    public GPAuraResponse upsertBillingMilestone() {
        try {
            upsert listOfBillingMilestone;
            projectId = listOfBillingMilestone[0].GP_Project__c;
            
            listOfBillingMilestone = new GPSelectorBillingMilestone().selectProjectBillingMilestoneRecords(projectId);

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'BILLING MILESTONE');
            update project;

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfBillingMilestone, true));
    }

    /**
     * @description method to delete Billing Milestone Records.
     * 
     * @example
     * new GPControllerProjectBillingMilestones(<SFDC 18/15 digit project Id>).deleteBillingMilestone(<SFDC 18/15 digit billing milestone Id>);
     */
    public GPAuraResponse deleteBillingMilestone(Id billingMilestoneId) {
        try {
            GP_Billing_Milestone__c billingMilestoneToBedeleted = new GPSelectorBillingMilestone().selectProjectBillingMilestoneRecord(billingMilestoneId);
            delete billingMilestoneToBedeleted;
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
     * @description method to delete Billing Milestone Records in Bulk.
     * 
     * @example
     * new GPControllerProjectBillingMilestones(<SFDC 18/15 digit project Id>).deleteBillingMilestoneBulkService(<List of SFDC 18/15 digit billing milestone Ids>);
     */
    public GPAuraResponse deleteBillingMilestoneBulkService(List < Id > billingMilestoneIds) {
        try {
            List < GP_Billing_Milestone__c > billingMilestonesToBedeleted = new GPSelectorBillingMilestone().selectProjectBillingMilestones(billingMilestoneIds);
            delete billingMilestonesToBedeleted;
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
     * @description method to set Billing Milestone Entry Type Options.
     * 
     */
    private void setBillingMilestoneTypeOptions() {
        if (isMileStoneDefinable) {
            Schema.DescribeFieldResult fieldResult = GP_Billing_Milestone__c.GP_Entry_type__c.getDescribe();
            listOfMilestoneTypeOptions = new List < GPOptions > ();
            List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();

            listOfMilestoneTypeOptions.add(new GPOptions('--Select--', null));
            for (Schema.PicklistEntry f: ple) {
                listOfMilestoneTypeOptions.add(new GPOptions(f.getLabel(), f.getValue()));
            }
        }
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if (mapOfBillingMilestoneFieldKeyToFieldTemplate != null)
            gen.writeObjectField(LIST_OF_BILLING_MILESTONE_TEMPLATE_LABEL, mapOfBillingMilestoneFieldKeyToFieldTemplate);
        if (listOfMilestoneTypeOptions != null)
            gen.writeObjectField(LIST_OF_MILESTONE_TYPE_OPTIONS_LABEL, listOfMilestoneTypeOptions);
        if (listOfBillingMilestone != null)
            gen.writeObjectField(LIST_OF_BILLING_MILESTONE_LABEL, listOfBillingMilestone);
        if (isMileStoneDefinable != null)
            gen.writeObjectField(IS_MILESTONE_DEFINABLE_LABEL, isMileStoneDefinable);
        if (noOfRowsToBeDisplayed != null)
            gen.writeObjectField(NO_OF_ROWS_TO_BE_DISPLAYED, noOfRowsToBeDisplayed);
        if (projectTCVAmount != null)
            gen.writeObjectField(PROJECT_TCV_AMOUNT_LABEL, projectTCVAmount);
        if (projectStartDate != null)
            gen.writeObjectField(PROJECT_START_DATE_LABEL, projectStartDate);
        if (relatedWorkLocations != null)
            gen.writeObjectField(WORK_LOCATION_LABEL, relatedWorkLocations);
        if (mapOfWorkLocation != null)
            gen.writeObjectField(MAP_WORKLOCATION_LABEL, mapOfWorkLocation);
        if (currencyISOCode != null)
            gen.writeObjectField(CURRENCY_ISO_CODE_LABEL, currencyISOCode);
        if (projectEndDate != null)
            gen.writeObjectField(PROJECT_END_DATE_LABEL, projectEndDate);
        if (isITOType != null)
            gen.writeObjectField(IS_PROJECT_ITO_TYPE_LABEL, isITOType);
        if (isUpdatable != null)
            gen.writeObjectField(ISUPDATEABLE_LABEL, isUpdatable);
        gen.writeEndObject();
    }

    /**
     * @description Set Billing Milestone Field Map using Project Template.
     * 
     */
    private void setFieldTemplate() {
        mapOfBillingMilestoneFieldKeyToFieldTemplate = new Map < String, GPProjectTemplateFieldWrapper > ();
        GP_Project__c projectObj = getProjectRecord();
        isMileStoneDefinable = projectObj.RecordType.Name != 'BPM' ? true : false;

        if (isMileStoneDefinable) {
            isUpdatable = projectObj.GP_Approval_Status__c == 'Approved' || projectObj.GP_Approval_Status__c == 'Pending For Approval' ? false : true;
            isITOType = projectObj.GP_Delivery_Org__c == 'CMITS' && projectObj.GP_Sub_Delivery_Org__c == 'ITO' ? true : false;
            projectStartDate = projectObj.GP_Start_Date__c;
            currencyISOCode = projectObj.CurrencyIsoCode;
            projectEndDate = projectObj.GP_End_Date__c;
            projectTCVAmount = projectObj.GP_TCV__c;

            String serializedFinalTemplate = getConcatenatedFinalJSON(projectObj);
            String targetKey = 'GP_Billing_Milestone__c___All';
            GPProjectTemplateFieldWrapper billingMilestoneTemplate;

            Map < String, GPProjectTemplateFieldWrapper > mapOfFieldKeyToFieldTemplate = getDeserializedMapOfProjectTemplate(serializedFinalTemplate);

            for (String fieldKey: mapOfFieldKeyToFieldTemplate.keySet()) {

                if (fieldKey.contains(targetKey)) {
                    billingMilestoneTemplate = mapOfFieldKeyToFieldTemplate.get(fieldKey);
                    mapOfBillingMilestoneFieldKeyToFieldTemplate.put(fieldKey, billingMilestoneTemplate);
                    noOfRowsToBeDisplayed = billingMilestoneTemplate.numberOfDefaultRowToDisplay;
                }
            }
        }
    }

    /**
     * @description get Deserialized roject Template Map.
     *
     *@param serializedFinalTemplate : serialized field template.
     * 
     */
    private Map < String, GPProjectTemplateFieldWrapper > getDeserializedMapOfProjectTemplate(String serializedFinalTemplate) {
        return (Map < String, GPProjectTemplateFieldWrapper > ) JSON.deserialize(serializedFinalTemplate, Map < String, GPProjectTemplateFieldWrapper > .class);
    }

    /**
     * @description get Concatenated JSON of project template.
     *
     *@param projectObj : Project Object.
     *
     *@return String concatenated template JSON.
     * 
     */
    private String getConcatenatedFinalJSON(GP_Project__c projectObj) {
        String serializedFinalTemplate = projectObj.GP_Project_Template__r.GP_Final_JSON_1__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_2__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_3__c;

        return serializedFinalTemplate;
    }

    /**
     * @description Return Project Record using project Record Id.
     *
     *@return GP_Project__c Project Record.
     * 
     */
    private GP_Project__c getProjectRecord() {
        return new GPSelectorProject().selectProjectRecord(projectId);
    }

    /**
     * @description Create Work Location picklist.
     * 
     */
    private void setRelatedWorkLocation() {
        if (isMileStoneDefinable) {
            relatedWorkLocations = new List < GPOptions > { nullOption };
            mapOfWorkLocation = new map < String, Id > ();
            for (GP_Project_Work_Location_SDO__c objWorkLocationSDO: new GPSelectorProjectWorkLocationSDO().selectProjectWorkLocationAndSDORecords(projectId)) {
                if (objWorkLocationSDO.GP_Work_Location__r.RecordType.Name == 'Work Location') {
                    mapOfWorkLocation.put(objWorkLocationSDO.GP_Work_Location__r.GP_Oracle_Id__c, objWorkLocationSDO.GP_Work_Location__c);
                    relatedWorkLocations.add(new GPOptions(objWorkLocationSDO.GP_Work_Location__c, objWorkLocationSDO.GP_Work_Location__r.Name));
                }
            }
        }
    }

    /*public class AuraOptions {

        public String label {
            get;
            set;
        }
        public String text {
            get;
            set;
        }

        public AuraOptions(String label, String text) {
            this.label = label;
            this.text = text;
        }

        public AuraOptions(String text) {
            this.label = text;
            this.text = text;
        }
    }*/

    /*public GPAuraResponse getJobId() {
        GPSelectorJob selector = new GPSelectorJob();
        list<GP_job__C> lstOfJobIfExist = selector.SelectJobRecords();
        GP_job__C objJob;
        
        if(lstOfJobIfExist == null || lstOfJobIfExist.size() == 0) {
            objJob = new GP_job__C();
            objJob.GP_Job_Type__c = 'Upload Billing Milestones';
            objJob.OwnerId = userInfo.getUserId();
            insert objJob;
        } else {
            objJob = lstOfJobIfExist[0];
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(objJob, true));
    }*/

    /*public GPAuraResponse getProcessedCSVRecords(List<GP_Billing_Milestone__c> listOfCSVRecordsUploaded){
        system.debug('listOfCSVRecordsUploaded'+listOfCSVRecordsUploaded);
        Set<String> setOfWorkLocationOracleIds = new Set<String>();
        Map<String,String> mapOfOracleIdsWorkLocationIds = new Map<String,String>();
        for(GP_Billing_Milestone__c objBillingMilestone : listOfCSVRecordsUploaded){
            if(objBillingMilestone.GP_Work_Location__c != null){
                setOfWorkLocationOracleIds.add(objBillingMilestone.GP_Work_Location__c);
            }
        }
        for(GP_Work_Location__c objWorkLocation : new GPSelectorWorkLocationSDOMaster().selectByOracleIdofWOrkLocationAndSDO(setOfWorkLocationOracleIds)){
            mapOfOracleIdsWorkLocationIds.put(objWorkLocation.GP_Oracle_Id__c,objWorkLocation.Id);
        }
        
        for(GP_Billing_Milestone__c objBillingMilestone : listOfCSVRecordsUploaded){
            if(mapOfOracleIdsWorkLocationIds.containsKey(objBillingMilestone.GP_Work_Location__c))
                objBillingMilestone.GP_Work_Location__c = mapOfOracleIdsWorkLocationIds.get(objBillingMilestone.GP_Work_Location__c);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfCSVRecordsUploaded, true));
    }*/
}