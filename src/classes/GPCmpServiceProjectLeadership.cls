/**
* @author Anmol.kumar
* @date 11/09/2017
*
* @group ProjectLeadership
* @group-content ../../ApexDocContent/ProjectLeadership.htm
*
* @description Apex Service for project leadership module,
* has aura enabled method to fetch project leadership data.
*/
public class GPCmpServiceProjectLeadership {
    
    /**
    * @description Returns Project leadership data for a given project Id
    * @param projectId Salesforce 18/15 digit Id of project
    * 
    * @return GPAuraResponse json of Project leadership data
    * 
    * @example
    * GPCmpServiceProjectLeadership.getLeadershipData(<18 or 15 digit project Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getLeadershipData(Id projectId) {
        GPControllerProjectLeadership leadershipController = new GPControllerProjectLeadership(projectId);
        return leadershipController.getProjectLeadershipData();
    }
    
    /**
    * @description Saves leadership data
    * @param strListOfProjectLeadership serialized list of project leadership records to be inserted/updated
    * @param projectId Salesforce 18/15 digit Id of project
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully inserted
    * 
    * @example
    * GPCmpServiceProjectLeadership.saveLeadershipData(<Serialized list of project leadership record>, <18 or 15 digit project Id>);
    */
    @AuraEnabled
    public static GPAuraResponse saveLeadershipData(String strListOfProjectLeadership, String projectId) {
        List<GP_Project_Leadership__c> listOfProjectLeadership;
        
        try {
            listOfProjectLeadership = (List<GP_Project_Leadership__c>) JSON.deserialize(strListOfProjectLeadership, List<GP_Project_Leadership__c>.class);
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
        GPControllerProjectLeadership leadershipController = new GPControllerProjectLeadership(projectId, listOfProjectLeadership);
        return leadershipController.upsertProjectLeadership();
    }
    
    /**
    * @description Deletes Project leadership data for a given projectleadership Id
    * @param projectLeadershipId Salesforce 18/15 digit Id of project leadership
    * @param projectId Salesforce 18/15 digit Id of project
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully deleted
    * 
    * @example
    * GPCmpServiceProjectLeadership.deleteLeadershipData(<18 or 15 digit project leadership Id>, <18 or 15 digit project Id>);
    */
    @AuraEnabled
    public static GPAuraResponse deleteLeadershipData(Id projectLeadershipId, Id projectId) {
        GPControllerProjectLeadership leadershipController = new GPControllerProjectLeadership();
        return leadershipController.deleteProjectLeadership(projectLeadershipId, projectId);
    }
}