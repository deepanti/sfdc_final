@isTest public class GPDomainProductTracker {
    
    @isTest private static void testDomainProduct(){
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'Product2';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Product2 objProduct = GPCommonTracker.getProduct();
        objProduct.Nature_of_Work__c = 'Product & Services - Analytics';
        objProduct.Service_Line__c = 'Record to Report';
        objProduct.Industry_Vertical__c = 'Services';
        objProduct.Product_Family__c = 'F&A';
        objProduct.Name = 'NOW change';
        objProduct.IsActive = true;
        insert objProduct;
        
        objProduct.Nature_of_Work__c = 'Consulting - Digital';
        update objProduct;
        
        objProduct.Service_Line__c = 'Regulatory Affairs';
        update objProduct;
        
        objProduct.Product_Family__c = 'Collections';
        update objProduct;
        
        Product2 objProduct1 = GPCommonTracker.getProduct();
        objProduct1.Nature_of_Work__c = 'Product & Services - Analytics';
        objProduct1.Service_Line__c = 'Claims';
        objProduct1.Industry_Vertical__c = 'Services';
        objProduct1.Product_Family__c = 'HRO';
        objProduct1.Name = 'NOW change 2';
        objProduct1.IsActive = true;
        insert objProduct1;
        
        objProduct.IsActive = false;
        update objProduct;
        
        objProduct.Name = 'NOW change 1';
        update objProduct;
        
        
        Nature_of_Work__c objNOW = new Nature_of_Work__c();
        objNOW.Name = 'TestNOW';
        insert objNOW;
        
        Nature_of_Work__c objNOW1 = new Nature_of_Work__c();
        objNOW1.Name = 'TestNOW 1';
        insert objNOW1;
        
        Product_family__c objProductFamily = new Product_family__c();
        objProductFamily.Name = 'TestPF';
        objProductFamily.IsActive__c = True;
        objProductFamily.Product_Group__c = 'Group';
        insert objProductFamily;
        
        Service_Line__c objServiceLine = new Service_Line__c();
        objServiceLine.Name = 'TestSL';
        insert objServiceLine;
        
        Product_Catalogue1__c objProductCatalogue = new Product_Catalogue1__c();
        objProductCatalogue.IsActive__c = true;
        objProductCatalogue.Nature_of_Work__c = objNOW.id;
        objProductCatalogue.Product_family1__c = objProductFamily.id;
        objProductCatalogue.Product_new__c = objProduct.id;
        objProductCatalogue.Service_Line1__c = objServiceLine.Id;
        insert objProductCatalogue;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        GP_Deal__c ObjDeal2  = new GP_Deal__c();
        ObjDeal2.Name = 'TestDeal';
        ObjDeal2.GP_Nature_of_Work1_ID__c = objNOW.Id;
        ObjDeal2.GP_Service_Line1_ID__c = objServiceLine.Id;
        ObjDeal2.GP_Product_Family1_ID__c = objProductFamily.Id;
        ObjDeal2.GP_Product_Id__c = objProduct.Id;
        insert ObjDeal2;
        ObjDeal2.Name = 'TestDeal1';
        update ObjDeal2;
        
        
        
        List<Product2> prods = [Select Id,Nature_of_Work__c,Service_Line__c,Industry_Vertical__c from Product2];
        
        
        
        GPDomainProduct objDomainProduct = new GPDomainProduct(prods);
        GPDomainProduct.Constructor objConstructor = new GPDomainProduct.Constructor();
        objConstructor.construct(prods);
        
        /*for(Product2 obj:prods){
            obj.Nature_of_Work__c = 'Consulting - Digital';
            obj.Service_Line__c = 'Record to Report';
            obj.Industry_Vertical__c = 'Services';
            obj.Product_Family__c = 'F&A';
            obj.Name = 'NOW change';
            obj.IsActive = false;
        }
        
        update prods;*/
    }
}