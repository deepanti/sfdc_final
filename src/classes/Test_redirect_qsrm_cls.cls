@isTest
public class Test_redirect_qsrm_cls {
    static testMethod void test3() 
    {
        list<user> ulist = new list<user>();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User umanager1= new User(Alias = 's1tt', Email='standarduser1man11o@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man11oo@testorg.com');   ulist.add(umanager1);  

        User umanager2= new User(Alias = 'st2t',managerid=umanager1.id, Email='standarduser1man22@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g2', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800220730247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man3422@testorg.com');         ulist.add(umanager2); 
        
         User umanager3= new User(Alias = 'sd3t',managerid=umanager2.id, Email='standarduser1man33@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin3g', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man33@testorg.com');         ulist.add(umanager3);  
        User umanager4= new User(Alias = 'sdstt', managerid=umanager3.id,Email='standarduser1ma445@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin4g', Sales_Leader__c=true,LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man44@testorg.com');         ulist.add(umanager4);   
        User umanager5= new User(Alias = 's5tt', managerid=umanager4.id,Email='standarduser1man5@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g',Sales_Leader__c=true, LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man5@testorg.com');         ulist.add(umanager5);   
        User umanager= new User(Alias = 'stt', Email='standarduser1man@testorg.com', managerid=umanager5.id,EmailEncodingKey='UTF-8', LastName='Testdding', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247', Sales_Leader__c=true,        TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man@testorg.com');         ulist.add(umanager); 
        //User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,managerid=umanager.id,'standardusertestgen2015@testorg.com' );
        User u = new User(Alias = 'stat', Email='standarduser1@testorg.com',managerid=umanager.id,EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', Sales_Leader__c=true,         LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1@testorg.com');         ulist.add(u);  
        insert ulist;   
        
        
        list<Sales_Unit__c> sulist=new List<Sales_Unit__c>();
        Sales_Unit__c SUnit=new Sales_Unit__c(Name='sales Unit Head',Sales_Unit_Group__c='Sales Unit Head',Sales_Leader__c=u.id);   sulist.add(SUnit);
        
        Sales_Unit__c SSub_Unit_IT=new Sales_Unit__c(Name='Sales Sub Unit - IT',Sales_Unit_Group__c='Sales Sub Unit - IT',Sales_Leader__c=umanager.id);     sulist.add(SSub_Unit_IT);
        
        Sales_Unit__c SSub_Unit_Analytics=new Sales_Unit__c(Name='Sales Sub-Unit - Analytics',Sales_Unit_Group__c='Sales Sub-Unit - Analytics',Sales_Leader__c=umanager5.id);   sulist.add(SSub_Unit_Analytics);
        
        Sales_Unit__c SSub_Unit_Vertical=new Sales_Unit__c(Name='Sales Sub Unit - Vertical',Sales_Unit_Group__c='Sales Sub Unit - Vertical',Sales_Leader__c=umanager4.id);  sulist.add(SSub_Unit_Vertical);
        insert sulist;
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(umanager1.id,umanager2.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());

        oAccount.Account_Headquarter_Genpact_Regions__c='AMERICAS';
        oAccount.Account_Headquarters_Country__c='US';
        oAccount.Industry_Vertical__c ='IT Managed Services';
        oAccount.Sales_Unit__c = SUnit.id;
        oAccount.SalesSub_Unit_Vertical__c = SSub_Unit_Vertical.id;
        oAccount.SalesSub_Unit_Analytics__c = SSub_Unit_Analytics.id;
        oAccount.Sales_Sub_Unit_IT__c =  SSub_Unit_IT.id;
        update oAccount;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        oOpportunity.Sales_Country__c = 'Albania';
        oOpportunity.Sales_Region__c ='Europe';
       // update oOpportunity;
        Opportunitycontactrole OCR=new Opportunitycontactrole(Contactid=oContact.id, Isprimary=true,Opportunityid=oOpportunity.id,Role='Decision Maker' );
        Insert OCR;
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.Product_Autonumber__c='OLI3';
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(1);
        oOpportunityProduct.ContractTerminmonths__c = 18;
        oOpportunityProduct.SalesExecutive__c = umanager1.id;
      //  oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today().adddays(1);
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity.Id;
        oOpportunityProduct.TCVLocal__c =18;
       // oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c=true;
        oOpportunityProduct.HasSchedule__c=true;
        oOpportunityProduct.HasQuantitySchedule__c=false;
        oOpportunityProduct.Service_Line_OLI__c='NO Service Line';
        oOpportunityProduct.Product_Family_OLI__c='Analytics';
        insert oOpportunityProduct;
        
        
        //QSRM__c oQsrm=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,Userinfo.getUserId());
        //oQsrm.Status__c='Approved';
        //update oQsrm;
        //oOpportunity.count_no_of_lines__c=1;
        //oOpportunity.QSRM_Approved__c=true;
        //update oOpportunity;
        
        
        oOpportunity.stagename='2. Define'; 
        oOpportunity.Deal_Administrator__c='Analyst/Advisory';
        oOpportunity.Advisor_Firm__c='Alsbridge';
        oOpportunity.Advisor_Name__c='Barbara Deguise';
        //oOpportunity.Analyst_Name__c='sss';
        oOpportunity.Annuity_Project__c='Annuity';
        oOpportunity.Sub_type__c='Fixed Price';
        oOpportunity.Priority_Types__c='2';
        oOpportunity.ownerid=umanager1.id;
        //update oOpportunity;
        
        
        oOpportunity.stagename='3. On Bid'; 
        oOpportunity.Project_Margin__c=12.5;
        oOpportunity.SPOC_s__c='Shared Service';
        oOpportunity.Margin__c=3;
       // update oOpportunity;
        
        
        //RevenuePricingHeader__c CreateRevenuePricingHeader (Id AccountId,Id Userid,Id ContactId,Id OpportunityId)
        //RevenuePricingHeader__c RPH=GEN_Util_Test_Data.CreateRevenuePricingHeader(oAccount.id,umanager1.id,oContact.id,oOpportunity.id);
        
        //oOpportunity.stagename='6. Signed Deal';    
        //oOpportunity.Competitor__c='ADP';
        //oOpportunity.Other_Competitor__c='Shared Service';
        oOpportunity.Pricing_Inserted__c = true;
        oOpportunity.Winner__c='ADP';
        oOpportunity.Other_Winner__c='dd';
        oOpportunity.Win_Loss_Dropped_reason1__c='Stronger transformational story in proposal';
        oOpportunity.Win_Loss_Dropped_reason2__c='Superior capabilities in offerings';
        oOpportunity.Win_Loss_Dropped_reason3__c='Superior capabilities in offerings';
        oOpportunity.What_could_Genpact_have_done_better__c='asdd   ';
        oOpportunity.Contract_type__c='SOW/LOE/Work Order'; 
        update oOpportunity;
        
        QSRM__c oqsrm = new QSRM__c(Opportunity__c =oOpportunity.id );
                     
        QSRM_fieldid__c csetting12=new  QSRM_fieldid__c(); //Custom Setting 
        csetting12.Name='QSRMEntityid';    //Static record 1 of custom setting
        csetting12.Field_ID__c='01I90000001Vn9lEAC';
        insert csetting12;
        
        QSRM_fieldid__c csetting2=new  QSRM_fieldid__c(); //Custom Setting 
        csetting2.Name='QSRMName';    //Static record 1 of custom setting
        csetting2.Field_ID__c='CF00N9000000B9lv5';
        insert csetting2;
        
        ApexPages.currentPage().getParameters().put('CF00N9000000B9lv5','CF00N9000000B9lv5');
        ApexPages.currentPage().getParameters().put('CF00N9000000B9lv5_lkid',oOpportunity.id);
        ApexPages.StandardController scon = new ApexPages.StandardController(oqsrm);
        Redirect_Qsrm_cls clsinst = new Redirect_Qsrm_cls(scon);
        
        Pagereference pg=clsinst.onloadredirect();
        
    }
    
    static testMethod void test4() 
    {
        list<user> ulist = new list<user>();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User umanager1= new User(Alias = 's1tt', Email='standarduser1man11o@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man11oo@testorg.com');   ulist.add(umanager1);  

        User umanager2= new User(Alias = 'st2t',managerid=umanager1.id, Email='standarduser1man22@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g2', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800220730247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man3422@testorg.com');         ulist.add(umanager2); 
        
         User umanager3= new User(Alias = 'sd3t',managerid=umanager2.id, Email='standarduser1man33@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin3g', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man33@testorg.com');         ulist.add(umanager3);  
        User umanager4= new User(Alias = 'sdstt', managerid=umanager3.id,Email='standarduser1ma445@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin4g', Sales_Leader__c=true,LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man44@testorg.com');         ulist.add(umanager4);   
        User umanager5= new User(Alias = 's5tt', managerid=umanager4.id,Email='standarduser1man5@testorg.com',          EmailEncodingKey='UTF-8', LastName='Testddin5g',Sales_Leader__c=true, LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man5@testorg.com');         ulist.add(umanager5);   
        User umanager= new User(Alias = 'stt', Email='standarduser1man@testorg.com', managerid=umanager5.id,EmailEncodingKey='UTF-8', LastName='Testdding', LanguageLocaleKey='en_US',          LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='80022070247', Sales_Leader__c=true,        TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1man@testorg.com');         ulist.add(umanager); 
        //User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,managerid=umanager.id,'standardusertestgen2015@testorg.com' );
        User u = new User(Alias = 'stat', Email='standarduser1@testorg.com',managerid=umanager.id,EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', Sales_Leader__c=true,         LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800070247',         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1@testorg.com');         ulist.add(u);  
        insert ulist;   
        
        
        list<Sales_Unit__c> sulist=new List<Sales_Unit__c>();
        Sales_Unit__c SUnit=new Sales_Unit__c(Name='sales Unit Head',Sales_Unit_Group__c='Sales Unit Head',Sales_Leader__c=u.id);   sulist.add(SUnit);
        
        Sales_Unit__c SSub_Unit_IT=new Sales_Unit__c(Name='Sales Sub Unit - IT',Sales_Unit_Group__c='Sales Sub Unit - IT',Sales_Leader__c=umanager.id);     sulist.add(SSub_Unit_IT);
        
        Sales_Unit__c SSub_Unit_Analytics=new Sales_Unit__c(Name='Sales Sub-Unit - Analytics',Sales_Unit_Group__c='Sales Sub-Unit - Analytics',Sales_Leader__c=umanager5.id);   sulist.add(SSub_Unit_Analytics);
        
        Sales_Unit__c SSub_Unit_Vertical=new Sales_Unit__c(Name='Sales Sub Unit - Vertical',Sales_Unit_Group__c='Sales Sub Unit - Vertical',Sales_Leader__c=umanager4.id);  sulist.add(SSub_Unit_Vertical);
        insert sulist;
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(umanager1.id,umanager2.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());

        oAccount.Account_Headquarter_Genpact_Regions__c='AMERICAS';
        oAccount.Account_Headquarters_Country__c='US';
        oAccount.Industry_Vertical__c ='IT Managed Services';
        oAccount.Sales_Unit__c = SUnit.id;
        oAccount.SalesSub_Unit_Vertical__c = SSub_Unit_Vertical.id;
        oAccount.SalesSub_Unit_Analytics__c = SSub_Unit_Analytics.id;
        oAccount.Sales_Sub_Unit_IT__c =  SSub_Unit_IT.id;
        update oAccount;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        oOpportunity.Sales_Country__c = 'Albania';
        oOpportunity.Sales_Region__c ='Europe';
       // update oOpportunity;
        //Opportunitycontactrole OCR=new Opportunitycontactrole(Contactid=oContact.id, Isprimary=true,Opportunityid=oOpportunity.id,Role='Decision Maker' );
        //Insert OCR;
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.Product_Autonumber__c='OLI3';
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(1);
        oOpportunityProduct.ContractTerminmonths__c = 18;
        oOpportunityProduct.SalesExecutive__c = umanager1.id;
      //  oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today().adddays(1);
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity.Id;
        oOpportunityProduct.TCVLocal__c =18;
       // oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c=true;
        oOpportunityProduct.HasSchedule__c=true;
        oOpportunityProduct.HasQuantitySchedule__c=false;
        oOpportunityProduct.Service_Line_OLI__c='NO Service Line';
        oOpportunityProduct.Product_Family_OLI__c='Analytics';
        insert oOpportunityProduct;
        
        
        //QSRM__c oQsrm=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,Userinfo.getUserId());
        //oQsrm.Status__c='Approved';
        //update oQsrm;
        //oOpportunity.count_no_of_lines__c=1;
        //oOpportunity.QSRM_Approved__c=true;
        //update oOpportunity;
        
        
        oOpportunity.stagename='2. Define'; 
        oOpportunity.Deal_Administrator__c='Analyst/Advisory';
        oOpportunity.Advisor_Firm__c='Alsbridge';
        oOpportunity.Advisor_Name__c='Barbara Deguise';
        //oOpportunity.Analyst_Name__c='sss';
        oOpportunity.Annuity_Project__c='Annuity';
        oOpportunity.Sub_type__c='Fixed Price';
        oOpportunity.Priority_Types__c='2';
        oOpportunity.ownerid=umanager1.id;
        //update oOpportunity;
        
        
        oOpportunity.stagename='3. On Bid'; 
        oOpportunity.Project_Margin__c=12.5;
        oOpportunity.SPOC_s__c='Shared Service';
        oOpportunity.Margin__c=3;
       // update oOpportunity;
        
        
        //RevenuePricingHeader__c CreateRevenuePricingHeader (Id AccountId,Id Userid,Id ContactId,Id OpportunityId)
        //RevenuePricingHeader__c RPH=GEN_Util_Test_Data.CreateRevenuePricingHeader(oAccount.id,umanager1.id,oContact.id,oOpportunity.id);
        
        //oOpportunity.stagename='6. Signed Deal';    
        //oOpportunity.Competitor__c='ADP';
        //oOpportunity.Other_Competitor__c='Shared Service';
        oOpportunity.Pricing_Inserted__c = true;
        oOpportunity.Winner__c='ADP';
        oOpportunity.Other_Winner__c='dd';
        oOpportunity.Win_Loss_Dropped_reason1__c='Stronger transformational story in proposal';
        oOpportunity.Win_Loss_Dropped_reason2__c='Superior capabilities in offerings';
        oOpportunity.Win_Loss_Dropped_reason3__c='Superior capabilities in offerings';
        oOpportunity.What_could_Genpact_have_done_better__c='asdd   ';
        oOpportunity.Contract_type__c='SOW/LOE/Work Order'; 
        update oOpportunity;
        
        QSRM__c oqsrm = new QSRM__c(Opportunity__c =oOpportunity.id );
                     
        QSRM_fieldid__c csetting12=new  QSRM_fieldid__c(); //Custom Setting 
        csetting12.Name='QSRMEntityid';    //Static record 1 of custom setting
        csetting12.Field_ID__c='01I90000001Vn9lEAC';
        insert csetting12;
        
        QSRM_fieldid__c csetting2=new  QSRM_fieldid__c(); //Custom Setting 
        csetting2.Name='QSRMName';    //Static record 1 of custom setting
        csetting2.Field_ID__c='CF00N9000000B9lv5';
        insert csetting2;
        
        ApexPages.currentPage().getParameters().put('CF00N9000000B9lv5','CF00N9000000B9lv5');
        ApexPages.currentPage().getParameters().put('CF00N9000000B9lv5_lkid',oOpportunity.id);
        ApexPages.StandardController scon = new ApexPages.StandardController(oqsrm);
        Redirect_Qsrm_cls clsinst = new Redirect_Qsrm_cls(scon);
        
        Pagereference pg=clsinst.onloadredirect();
        
    }
}