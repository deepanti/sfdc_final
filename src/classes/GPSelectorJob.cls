public class GPSelectorJob extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Job__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Job__c.sObjectType;
    }

    public List < GP_Job__c > selectById(Set < ID > idSet) {
        return (List < GP_Job__c > ) selectSObjectsById(idSet);
    }

    public list < GP_job__C > SelectJobRecords() {
        list < GP_job__C > lstOfJobObj = [SELECT Id, Name, GP_Column_s_To_Update_Upload__c FROM GP_Job__c where ownerId =: userInfo.getUserId()
            And GP_Job_Type__c = 'Upload Billing Milestones'
            And GP_Count_Of_Temporary_Records__c = 0
        ];
        return lstOfJobObj;
    }

    public list < GP_job__C > selectJobRecordsWithoutChildRecords(String jobType) {
        list < GP_job__C > lstOfJobObj = [SELECT Id, Name, GP_Column_s_To_Update_Upload__c FROM GP_Job__c where ownerId =: userInfo.getUserId()
            And GP_Job_Type__c =: jobType
            And GP_Count_Of_Temporary_Records__c = 0
        ];
        return lstOfJobObj;
    }

    public GP_Job__c SelectJobRecord(String jobId) {
        return [SELECT Id, Name, GP_Job_Type__c, GP_Column_s_To_Update_Upload__c FROM GP_Job__c where
            id =: jobId
        ];
    }
}