public class GPSelectorProjectTask extends fflib_SObjectSelector {
    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project_Task__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Project_Task__c.sObjectType;
    }
    
    public List < GP_Project_Task__c > selectById(Set < ID > idSet) {
        return (List < GP_Project_Task__c > ) selectSObjectsById(idSet);
    }
    
    public List < GP_Project_Task__c > getListOfProjectTask(Set < String > setOfProjectTaskNumber) {
        return [SELECT Id, GP_Project__c, GP_Task_Name__c, GP_Project__r.GP_Oracle_PID__c,
                Name, GP_Start_Date__c, GP_End_Date__c,GP_Project_Number__c,
                GP_Task_Number__c, GP_Oracle_Project_Status__c, GP_Active__c FROM GP_Project_Task__c
                where GP_Task_Number__c in: setOfProjectTaskNumber and GP_Active__c = true
                AND(NOT GP_Task_Name__c LIKE '%EXP%')
                AND(NOT GP_Task_Name__c LIKE '%Concur%')
                AND(NOT GP_Task_Name__c LIKE '%Salary%')
                AND(NOT GP_Task_Name__c LIKE '%CTC%')
                AND(NOT GP_Task_Name__c LIKE '%Purchase%')
               ];
    }

    public List < GP_Project_Task__c > getListOfProjectTaskForSetOfProjectIds(Set < String > setOfProjectOracleIds) {
        return [SELECT Id, GP_Project__c, GP_Task_Name__c, GP_Project__r.GP_Oracle_PID__c,
                Name, GP_Start_Date__c, GP_End_Date__c,GP_Project_Number__c,
                GP_Task_Number__c, GP_Oracle_Project_Status__c, GP_Active__c FROM GP_Project_Task__c
                where GP_Project_Number__c in: setOfProjectOracleIds 
                AND GP_Active__c = true
                AND(NOT GP_Task_Name__c LIKE '%EXP %')
                AND(NOT GP_Task_Name__c LIKE '% Concur %')
                AND(NOT GP_Task_Name__c LIKE '% Salary %')
                AND(NOT GP_Task_Name__c LIKE '% CTC %')
                AND(NOT GP_Task_Name__c LIKE '% Purchase %')
               ];
    }
    
    public List < GP_Project_Task__c > getListOfProjectTaskForSetOfProjectIds(Set < String > setOfProjectOracleIds, Date monthStartDate) {
        return [SELECT Id, GP_Project__c, GP_Task_Name__c, GP_Project__r.GP_Oracle_PID__c,
                Name, GP_Start_Date__c, GP_End_Date__c,GP_Project_Number__c,
                GP_Task_Number__c, GP_Oracle_Project_Status__c, GP_Active__c FROM GP_Project_Task__c
                where GP_Project_Number__c in: setOfProjectOracleIds 
                AND GP_Active__c = true
                AND(NOT GP_Task_Name__c LIKE '%EXP%')
                AND(NOT GP_Task_Name__c LIKE '%Concur%')
                AND(NOT GP_Task_Name__c LIKE '%Salary%')
                AND(NOT GP_Task_Name__c LIKE '%CTC%')
                AND(NOT GP_Task_Name__c LIKE '%Purchase%')
                AND((GP_Start_Date__c <=: monthStartDate AND GP_End_Date__c >=: monthStartDate) OR(GP_Start_Date__c <=: monthStartDate AND GP_End_Date__c = null))
               ];
    }
    
    public GP_Project_Task__c getProjectTask(Id projectTaskId) {
        return [SELECT Id, GP_Project__c, GP_Task_Name__c, GP_Project__r.GP_Oracle_PID__c, Name, GP_Start_Date__c, GP_End_Date__c,
                GP_Task_Number__c, GP_Oracle_Project_Status__c, GP_Active__c FROM GP_Project_Task__c
                where id =: projectTaskId
               ];
    }
}