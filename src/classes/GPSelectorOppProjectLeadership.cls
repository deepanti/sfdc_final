public class GPSelectorOppProjectLeadership extends fflib_SObjectDomain {
    public GPSelectorOppProjectLeadership(List<GP_Opp_Project_Leadership__c > sObjectList) {
        super(sObjectList);
    }
                
    public static GP_Opportunity_Project__c getDatafromoppProjectLeadership(){
        
        GP_Opportunity_Project__c opportunityProject = [SELECT Id, GP_FP_A_Emp__c, GP_Global_Project_Manager__c,
                                                        GP_GPM_Start_Date__c, GP_FP_A_Start_Date__c, GP_FP_A_GPM_End_Date__c
                                                        FROM GP_Opportunity_Project__c LIMIT 1];
        return opportunityProject;
    }

    public static List<GP_Opportunity_Project__c> getOppProjectLeadershipUnderOppProject(List<Id> listOfOppProjectId){
        
        return [SELECT Id, GP_FP_A_Emp__c, GP_Global_Project_Manager__c,
                    GP_GPM_Start_Date__c, GP_FP_A_Start_Date__c, GP_FP_A_GPM_End_Date__c
                    FROM GP_Opportunity_Project__c 
                    WHERE Id = :listOfOppProjectId];
    }
    
    public static List<GP_Opp_Project_Leadership__c> getLeaderShipBaseonRole(set<String> setRoleid ,set<String> setOppProjectId){
        
   			return [Select id, GP_Opportunity_Project__c,GP_Start_Date__c,GP_End_Date__c,GP_Leadership_Role__c,GP_Emp_Person_Id__c,GP_Active__c from
   					GP_Opp_Project_Leadership__c where GP_Leadership_Role__c in:setRoleid AND  GP_Opportunity_Project__c in : setOppProjectId 
   					and GP_Active__c = true];
    }
    
}