Public class employeeProjectAllocationData{
    @AuraEnabled
    public Set<String> setOfEmployeeAssociatedProjectId{get;set;}
    @AuraEnabled
    public Decimal minHours{get;set;}
    @AuraEnabled
    public Decimal maxHours{get;set;}
    @AuraEnabled
    public Map<String,List<GP_Resource_Allocation__c>> mapOfwrapperResourceAllocation{get;set;}
    @AuraEnabled
    public List<wrapperTimeSheetEntry> wrapperTimeSheetEntries {get;set;}
    @AuraEnabled
    public Boolean isCompeletlyDisabled {get;set;}
    @AuraEnabled
    public Integer maxDateIndex {get;set;}
    @AuraEnabled
    public String employeeId {get;set;}
    @AuraEnabled
    public String timeSheetTransactionId {get;set;}
    @AuraEnabled
    public List<GP_Employee_HR_History__c> listOfEmployeeHRInactiveRecords {get;set;}
    @AuraEnabled
    public List<GP_Project__c> listOfEmployeeAssociatedProjects {get;set;}
    @AuraEnabled
    public Map<String,Map<Id,GP_Project_Task__c>> mapOfProjectAndAssociatedTasks {get;set;}
}