//@isTest(SeeAllData=true)
@isTest
private class GENcShareLineItemTest
{
   
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
       Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');        
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        //Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];           
        User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800060567',TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen@testorg.com');
        insert u1;
        OpportunityTeamMember oOpportunityTeamMember =new OpportunityTeamMember();
        oOpportunityTeamMember.OpportunityId=oOpportunity.Id;
        oOpportunityTeamMember.UserId =  u1.Id;
        insert oOpportunityTeamMember;
        
        //Create QSRM Test
        Test.starttest();
        QSRM__c oQSRM=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,u.Id);
        
        User u2 = new User(Alias = 'standt', Email='standarduser@testorg.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800060567',TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1@testorg.com');
        insert u2;
        
        QSRM__c oQSRM1=GEN_Util_Test_Data.CreateQSRM(oOpportunity.Id,u2.Id);
        update oQSRM1;
        Test.stoptest();


                
            }
}