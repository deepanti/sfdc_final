@isTest
public class OpportunityConfirmedAddContractTest {
    //creating test data
    @testSetup
    public static void createTestData()
    {
        User testUser=TestDataFactoryUtility.createTestUser('Genpact Super Admin', 'emailxyz12@acmexyz.com', 'emailXyz12@xyzasd.com');
        insert testUser;
        System.runAs(testUser){
            //creating account
            Account acc=TestDataFactoryUtility.createTestAccountRecord();
            insert acc;
            //creating contact
            Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
            insert con;
            //creating opportunity
            List<Opportunity> opp=TestDataFactoryUtility.createTestOpportunitiesRecordsDiscover(acc.Id, 1, system.today(), con);
            insert opp;
            //creating product
            List<Product2> prod=TestDataFactoryUtility.createTestProducts3(1);
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, 
                Product2Id = prod[0].Id,
                UnitPrice = 10, 
                IsActive = true);
            insert standardPrice;
            //creating opportunity lineitem
            List<OpportunityLineItem> oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(prod,opp[0].Id,standardPrice.id);
            // insert oppLineItem;
            
            //updating the stages 
            opp[0].StageName='2. Define';
            //update opp;
            opp[0].StageName='3. On Bid';
            opp[0].Margin__c=1;
            opp[0].Proposed_pricing_model__c='FTE based';
            opp[0].Summary_of_opportunity__c='test';
            opp[0].Current_challenges_client_facing__c ='no challenges';
            opp[0].End_objective_the_client_trying_to_meet__c='value';
            opp[0].Win_Theme__c='win theme';
            //update opp;
            opp[0].StageName='4. Down Select';
            opp[0].W_L_D__c='Signed';
            
            opp[0].StageName='5. Confirmed';
            opp[0].Contract_type__c='MSA or over-arching agreement';
            //creating pricer for contract
            Pricer__c pricerRecord=TestDataFactoryUtility.createTestPricer();
            insert pricerRecord;
            opp[0].Pricer_Name__c=pricerRecord.id;
            
            
            //creating contract record
            Contract conRecord=new Contract(Opportunity_tagged__c=opp[0].Id,AccountId=opp[0].AccountId,
                                            Contract_Language__c='English',Effective_Date_of_Contracts__c=system.today(),
                                            End_Date_of_Contract__c=system.today().addDays(6),Status='Draft',
                                            Customer_Signed_By__c='asd',CustomerSignedDate=system.today());
            insert conRecord;
            test.startTest();
            update opp;
            
            //creating content version
            ContentVersion contentVer=new Contentversion(Title='Test',VersionData=Blob.valueOf('hello'),PathOnClient='Test.pdf');
            insert contentVer;
            
            test.stopTest();
        }
    }
    //testing negative schenarios 
    @isTest  public static void testfunctionsNegative()
    {
        test.startTest();
        try
        {
            OpportunityConfirmedAddContract.getContractInDraft('');
            OpportunityConfirmedAddContract.submitForApproval('');
            
            
        }
        catch(Exception ex)
        {
            
        }
        test.stopTest();
    }
    //testing positive schenarios 
    @isTest public static void testFunctionsPositive()
    {
        User testUser=[select id from user where email='emailXyz12@xyzasd.com'];
        system.runAs(testUser)
        {
            test.startTest();
            Opportunity  oppRecord=[select id from opportunity];
            Contract conRecord=[select id from Contract];
            List<contentVersion> conVer=[select id,ContentDocumentId from contentVersion ];
            List<String> fileId=new List<String>();
            for(Integer i=0;i<conVer.size();i++)
            {
                fileId.add(conVer[i].ContentDocumentId);
            }
            OpportunityConfirmedAddContract.getContractInDraft(oppRecord.Id);
            try
            {
                OpportunityConfirmedAddContract.convertToAttachment(oppRecord.id,conRecord.id,fileId);
                OpportunityConfirmedAddContract.convertToAttachment(oppRecord.id,conRecord.id,new List<String>());
            }
            catch(Exception ex)
            {
                system.debug(ex.getMessage());
            }
            
            try
            {
                 OpportunityConfirmedAddContract.submitForApproval(oppRecord.Id);
                 OpportunityConfirmedAddContract.setContractFile(oppRecord.Id,new List<String>());
            }
            catch(Exception ex)
            {
                system.debug('exeption'+ex.getMessage());
            }
            OpportunityConfirmedAddContract.setContractFile(oppRecord.Id, fileId);
           
            test.stopTest();
        }
    }
    
}