@isTest
public class VIC_GuaranteeComponentBatchCtlrTest{

public static Target__c targetObj;
public static Master_Plan_Component__c masterPlanComponentObj;
public static Target_Component__c targetComponentObj;
 static testmethod void ValidateVIC_GuaranteeComponentBatchCtlr()
 {
    
     LoadData();
     
     VIC_GuaranteeComponentBatchCtlr obj = new VIC_GuaranteeComponentBatchCtlr();
   //  obj.initTargetComponent(targetObj.id,1000,1000,1000,1000,masterPlanComponentObj.id);
     database.executebatch(obj);
     obj.createTargetComponent(targetObj.id,masterPlanComponentObj.id);
     System.assertEquals(200,200); 
     
 }
 static testmethod void ValidateVIC_GuaranteeComponentBatchNCtlr()
 {
    
    
     LoadData();
    
     VIC_GuaranteeComponentBatchCtlr obj = new VIC_GuaranteeComponentBatchCtlr();
  
     database.executebatch(obj);
   System.AssertEquals(200,200);
     
     
 }
 static void LoadData()
{
 VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Annual_Process_Year__c=2018;
        vicInfo.VIC_Process_Year__c=2018;
       insert vicInfo;
user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
insert objuser;

user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='IT_GRM';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;

OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.Product_BD_Rep__c=objuser1.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id;   
objOLI.TCV__c=10000;
objOLI.vic_Primary_Sales_Rep_Split__c=10;
insert objOLI;

system.debug('objOLI'+objOLI);

targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
targetObj.Start_date__c=DATE.newInstance(2018,1,1);
targetObj.vic_Guaranteed_Payout__c=20000;
targetObj.Plan__C= planObj1.id;  
insert targetObj;
targetObj=[select id,vic_Pending_Guaranteed_Payment__c,Start_date__c from target__c where id=:targetObj.id];

 masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Guarantee','Currency');
masterPlanComponentObj.vic_Component_Code__c='Guarantee'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;
    
    masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Guarantee','Currency');
masterPlanComponentObj.vic_Component_Code__c='Guarantee'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;

Master_Plan_Component__c masterPlanComponentObjnew=VIC_CommonTest.fetchMasterPlanComponentData('OP','Currency');
masterPlanComponentObjnew.vic_Component_Code__c='PM'; 
masterPlanComponentObjnew.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObjnew;
 
VIC_Calculation_Matrix__c vicMatrix1=VIC_CommonTest.fetchMatrixComponentData('IO');
insert vicMatrix1;
VIC_Calculation_Matrix__c vicMatrix2=VIC_CommonTest.fetchMatrixComponentData('SEM');
insert vicMatrix2;   
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
PlanComponentObj.VIC_IO_Calculation_Matrix_1__c=vicMatrix1.id;
PlanComponentObj.VIC_TS_Calculation_Matrix_2__c=vicMatrix2.id;    
insert PlanComponentObj;
    

system.debug('objuser'+objuser);    


}    
   


}