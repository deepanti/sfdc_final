@isTest
public class GPSelectorEmployeeTrainingTracker {
	@isTest
    static void testEmployeeTraningTracker() {
        GP_Employee_Training__c employeeTranning = new GP_Employee_Training__c();
        
        insert employeeTranning;
        
        GPSelectorEmployeeTraining employeeTranningSelector = new GPSelectorEmployeeTraining();
        employeeTranningSelector.selectById(employeeTranning.Id);
        employeeTranningSelector.selectById(new Set<Id> {employeeTranning.Id});
        GPSelectorEmployeeTraining.selectEmployeeTrainingRecords(new Set<Id> {employeeTranning.Id});
        GPSelectorEmployeeTraining.selectOldEmpTrainingRecords(null, null);
        GPSelectorEmployeeTraining.selectEmployeeTrainingRecordsWithOHRId(null);
    }
}