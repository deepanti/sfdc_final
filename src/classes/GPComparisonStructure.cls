public class GPComparisonStructure {

    public String destinationValue {get;set;} 
    public String sourceValue {get;set;}  
    public String objectType {get;set;} 
    public String fieldName {get;set;} 
    public String fieldLabel {get;set;} 

    public GPComparisonStructure(String destinationValue, String sourceValue, String objectType, String fieldName , String fieldLabel) {
        this.destinationValue = destinationValue;
        this.sourceValue = sourceValue;
        this.objectType = objectType;
        this.fieldName = fieldName;
        this.fieldLabel = fieldLabel;

    }
    public GPComparisonStructure() {}
}