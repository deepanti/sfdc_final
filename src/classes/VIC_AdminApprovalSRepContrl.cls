Public class VIC_AdminApprovalSRepContrl{
   
static Map<String, String> mapUserVicRoles = vic_CommonUtil.getUserVICRoleNameinMap();
static VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
static Map<String, String> mapDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap();  

@AuraEnabled
public static List<OpportunityLineItem> fetchOLIDataByUser(List<Id> lstUserID){
   return [
            SELECT Id,Name,OpportunityId,Opportunity.OwnerId,Opportunity.TCV1__c,vic_Primary_User__c,Product_BD_Rep__c,vic_Primary_Sales_Rep_Split_TCV__c,vic_Product_BD_Rep_Split_TCV__c,vic_VIC_User_3_Split_TCV__c,vic_VIC_User_4_Split_TCV__c,vic_VIC_User_3__c,vic_VIC_User_4__c,vic_Primary_Sales_Rep_Split__c,vic_Product_BD_Rep_Split__c,vic_VIC_User_3_Split__c,vic_VIC_User_4_Split__c,TCV__c 
            FROM OpportunityLineItem 
            WHERE (Opportunity.OwnerId =:lstUserID OR 
            Product_BD_Rep__c =:lstUserID OR 
            vic_VIC_User_3__c =:lstUserID OR 
            vic_VIC_User_4__c =:lstUserID) AND 
            TCV__c != null AND vic_Final_Data_Received_From_CPQ__c=true and vic_Sales_Rep_Approval_Status__c=:'Approved' and vic_Product_BD_Rep_Approval_Status__c=:'Approved'
     ];
}

@AuraEnabled
public static List <User> fetchUser(String searchText) {
String searchKey = searchText+ '%';
List < User > returnList = new List<User>();
List < User > lstOfUser = [select id, Name from User
                           where Name LIKE: searchKey LIMIT 500];

for (User objUser: lstOfUser) {
returnList.add(objUser);
}
return returnList;
}


@AuraEnabled
public Static list<AdminUserDataWrapper> getAdminUserDatawrapper(){

List<AdminUserDataWrapper> lstReturn = new List<AdminUserDataWrapper>();

if(processInfo != null && processInfo.VIC_Process_Year__c != null){
     
   
    list<id> lstUserId = new list<id>();
  List<Target__c> lstTarget= getTargetsMapDomicileWise(); 
if(lstTarget!=null && lstTarget.size()>0)
{
for(Target__c objTarget:lstTarget){
   lstUserId.add(objTarget.User__c);
} 
}
list<opportunitylineitem> lstoli= fetchOLIDataByUser(lstUserId);
map<id,decimal> mapUseridtoTcv = new map<id,decimal>(); 
map<id,decimal> mapoppidtoTcvbooked = new map<id,decimal>();     
  
system.debug('mapUseridtoTcv'+mapUseridtoTcv);
List<AdminOppWrapper> lstOppWrapper =getoppdatawrapper();
system.debug('lstOppWrapper'+lstOppWrapper);
for(Target__c objTarget:lstTarget){
if(mapUserVicRoles.containskey(objTarget.User__C) && mapUserVicRoles.get(objTarget.User__C)!=null ) {     
AdminUserDataWrapper objData = new AdminUserDataWrapper();

objData.UserId = objTarget.User__r.Id;
objData.UserName = objTarget.User__r.Name;
objData.VICRole = mapUserVicRoles.containsKey(objTarget.User__r.Id) ? mapUserVicRoles.get(objTarget.User__r.Id) : '-';
objData.CurrencyISOCode = mapDomicileToISOCode.containsKey(objTarget.User__r.Id) ? mapDomicileToISOCode.get(objTarget.User__r.Id) : '';
//objData.Comments ='Test';
objData.TargetLet=objTarget.Status__c;
objData.GCIA=objTarget.User__r.GCI_Attended__c;
objData.TcvBooked=0;
objData.TcvValidated=0.0;
objData.lstAdminOPPDVW=lstOppWrapper;
system.debug('lstAdminOPPDVW'+objData.lstAdminOPPDVW);
lstReturn.add(objData);
system.debug('lstReturn'+lstReturn);
}
}



}
return lstReturn;  
}  

public static list<AdminOppWrapper> getoppdatawrapper(){


List<AdminOppWrapper> lstAdminOPPDW= new List<AdminOppWrapper>();
List<AdminOliWrapper> lstAdminOliDW= new List<AdminOliWrapper>();
List<id> lstOliId= new List<id>();
set<id> setoppId = new set<id>();
string uvio='Profitable_Bookings_IO';
string uvts='Profitable_Bookings_TS';
string uvrs='Renewals';
string lgb='New_Logo_Bonus';
string kick='Kickers';
decimal sumOLITcv;
decimal sumOLIVIC;
map<id,decimal> mapoppidtosumOLITcv = new map<id,decimal>();
map<id,decimal> mapoppidtosumOLIVIC = new map<id,decimal>();

list<id> lstUserId = new list<id>();
    List<Target__c> lstTarget= getTargetsMapDomicileWise(); 
for(Target__c objTarget:lstTarget){
   lstUserId.add(objTarget.User__c);
} 
map<id,list<AdminOliWrapper >> mapoppIdtoOli = new map<id, list<AdminOliWrapper>>();
list<opportunitylineitem> lstoli =fetchOLIDataByUser(lstUserId);

for(opportunitylineitem objOli:lstoli){
     
     lstOliId.add(objOli.id);
     
}

map<string,decimal> mapkeytoincsum =calculateincentive(lstOliId);

 
for(opportunitylineitem objOli:[select id,name,TCV__c,opportunityid,opportunity.name,opportunity.Opportunity_ID__c,opportunity.tcv1__C,Revenue_Exchange_Rate__c from opportunitylineitem where vic_Final_Data_Received_From_CPQ__c=true and vic_Sales_Rep_Approval_Status__c=:'Approved' and vic_Product_BD_Rep_Approval_Status__c=:'Approved' and id=:lstOliId]) 
{
    
     setoppId.add(objOli.opportunityid);
     AdminOliWrapper objolidatawrap= new AdminOliWrapper ();
     objolidatawrap.Oppid= objoli.opportunityid;
     objolidatawrap.Oliid= objoli.id;
     objolidatawrap.OliTcv=objoli.TCV__c;
     
     objolidatawrap.upfrontvic=(mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvio)!=null?mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvio):0.0)+(mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvts)!=null?mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvts):0.0);
     objolidatawrap.kickers=(mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+kick)!=null?mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+kick):0.0);
     objolidatawrap.logobonus=(mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+lgb)!=null?mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+lgb):0.0);
     objolidatawrap.renewals=(mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvrs)!=null?mapkeytoincsum.get(string.valueof(id.valueof(objoli.id))+uvrs):0.0);
     objolidatawrap.totalinc=objolidatawrap.upfrontvic+objolidatawrap.logobonus+objolidatawrap.kickers+objolidatawrap.renewals;
     
     lstAdminOliDW.add(objolidatawrap);
    
    
    
    
    
}
  
  for(AdminOliWrapper objOliwrap:lstAdminOliDW)
    {
       if(mapoppIdtoOli.get(objOliwrap.Oppid)==null)
       {
       
         mapoppIdtoOli.put(objOliwrap.Oppid,new list<AdminOliWrapper>());
       }
       mapoppIdtoOli.get(objOliwrap.Oppid).add(objoliwrap);
      
      if(mapoppidtosumOLITcv!=null && mapoppidtosumOLITcv.size()>0 && mapoppidtosumOLITcv.containskey(objOliwrap.Oppid)){
      
          sumOLITcv=mapoppidtosumOLITcv.get(objOliwrap.Oppid);

               if(sumOLITcv!=null){
                
                sumOLITcv=sumOLITcv+objOliwrap.OliTcv;
                mapoppidtosumOLITcv.put(objOliwrap.Oppid,sumOLITcv);

           }

      
      }
      else{
      mapoppidtosumOLITcv.put(objOliwrap.Oppid,objOliwrap.OliTcv);
      
      
      
      
      
      }
      
      if(mapoppidtosumOLIVIC!=null && mapoppidtosumOLIVIC.size()>0 && mapoppidtosumOLIVIC.containskey(objOliwrap.Oppid)){
      
          sumOLIVIC=mapoppidtosumOLIVIC.get(objOliwrap.Oppid);

               if(sumOLIVIC!=null){
                
                sumOLIVIC=sumOLIVIC+objOliwrap.totalinc;
                mapoppidtosumOLIVIC.put(objOliwrap.Oppid,sumOLIVIC);

           }

      
      }
      else{
           mapoppidtosumOLIVIC.put(objOliwrap.Oppid,objOliwrap.totalinc);
      
      }
    
    system.debug('mapoppidtosumOLIVIC'+mapoppidtosumOLIVIC);
    system.debug('mapoppidtosumOLITcv'+mapoppidtosumOLITcv);
      
    
    }
    
   system.debug('setoppId'+setoppId);
   for(opportunity objopp:[select id,Opportunity_ID__c,tcv1__C from opportunity where id in:setoppId]){
   
    AdminOppWrapper objoppdatawrap = new AdminOppWrapper();
    objoppdatawrap.Oppid= objopp.id;
    objoppdatawrap.OppName=objopp.Opportunity_ID__c;
    objoppdatawrap.tcvbooked=objopp.tcv1__C;
    objoppdatawrap.lstOli=mapoppIdtoOli.get(objopp.id);
    objoppdatawrap.totaltvc=mapoppidtosumOLITcv.get(objopp.id);
    objoppdatawrap.totalvicpayable=mapoppidtosumOLIVIC.get(objopp.id);
    system.debug('objoppdatawrap.lstOli'+objoppdatawrap.lstOli);
    lstAdminOPPDW.add(objoppdatawrap);
    }
    
     
    

    

system.debug('lstAdminOPPDW'+lstAdminOPPDW);
return  lstAdminOPPDW;
    
    
}


        public class AdminViewWraper{
        @Auraenabled public String Domicile;
        @Auraenabled public List<AdminUserDataWrapper> lstAdminUDW;
        @Auraenabled public List<AdminOppWrapper> lstAdminOPPDW;
        @Auraenabled public List<AdminOliWrapper> lstAdminOliDW;
        }



        public class AdminUserDataWrapper{
        @Auraenabled public boolean IsSelected;
        @Auraenabled public String UserId;
        @Auraenabled public String UserName;
        @Auraenabled public String VICRole;
        @Auraenabled public String CurrencyISOCode;
        @Auraenabled public String TargetLet;
        @Auraenabled public Decimal TcvBooked;
        @Auraenabled public Decimal TcvValidated;
        @Auraenabled public string  GCIA;
        @Auraenabled public List<AdminOppWrapper> lstAdminOPPDVW;

        }





        public class AdminOppWrapper{
        @Auraenabled public boolean IsOppSelected;
        @Auraenabled public String Oppid;
        @Auraenabled public String OppName;
        @Auraenabled public decimal totaltvc;
        @Auraenabled public decimal totalvicpayable;
        @Auraenabled public decimal tcvbooked;
        @Auraenabled public list<AdminOliWrapper> lstOli;

        }


        public class AdminOliWrapper{
        @Auraenabled public boolean IsOLiSelected;
        @Auraenabled public string Oppid;
        @Auraenabled public String  Oliid;
        @Auraenabled public String  OliName;
        @Auraenabled public decimal OliTcv;
        @AuraEnabled public decimal upfrontvic;
        @AuraEnabled public decimal renewals;
        @AuraEnabled public decimal kickers;
        @AuraEnabled public decimal logobonus;
        @AuraEnabled public decimal totalinc;

        }



public Static List<Target__c> getTargetsMapDomicileWise(){
Date targetDate = date.newinstance(Integer.valueOf(processInfo.VIC_Process_Year__c), 1, 1);
 List<Target__c> lsttarget = new List<Target__c>();


for(Target__c t: [SELECT Id,User__r.Id, User__r.Name, User__r.GCI_Attended__c,Domicile__c,vic_HR_Comments__c,vic_Is_Payment_Hold_By_HR__c,Status__c, 
(Select Id, Master_Plan_Component__r.vic_Component_Code__c, vic_HR_Pending_Amount__c
FROM Target_Components__r) 
FROM Target__c 
WHERE Is_Active__c = TRUE 
AND User__r.IsActive = true
AND Start_date__c=:targetDate
AND Domicile__c != null
Order BY Domicile__c]){
   lsttarget.add(t);

                               
}

return lsttarget;
}

public static  map<string,decimal> calculateincentive(list<id> lstOliid)
{
     map<string,decimal> mapOliidtoIncBm = new map<string,decimal>();
     decimal bonusamountSum;
     string inckey;
     
     for(Target_Achievement__c objInc:[select id,Target_Component__c,vic_Opportunity_Product_Id__c,Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,Bonus_Amount__c from Target_Achievement__c where vic_Opportunity_Product_Id__c in:lstOliid and Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c in('Profitable_Bookings_TS','Profitable_Bookings_IO','New_Logo_Bonus','Kickers')])
     {  
          if(objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c=='Profitable_Bookings_TS' ){
              inckey=objInc.vic_Opportunity_Product_Id__c+objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c;
              if(mapOliidtoIncBm!=null && mapOliidtoIncBm.size()>0 && mapOliidtoIncBm.containskey(objInc.vic_Opportunity_Product_Id__c)){
                  
                   bonusamountSum=mapOliidtoIncBm.get(objInc.vic_Opportunity_Product_Id__c);
                   if(bonusamountSum!=null){
                      
                      bonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                      mapOliidtoIncBm.put(inckey,BonusamountSum);
                   }              
              }
              else{
                   mapOliidtoIncBm.put(inckey,objInc.Bonus_Amount__c);
                }              
          }
          
          
          if(objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c=='Profitable_Bookings_IO'){
              inckey=objInc.vic_Opportunity_Product_Id__c+objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c;
              if(mapOliidtoIncBm!=null && mapOliidtoIncBm.size()>0 && mapOliidtoIncBm.containskey(objInc.vic_Opportunity_Product_Id__c)){
                   bonusamountSum=mapOliidtoIncBm.get(objInc.vic_Opportunity_Product_Id__c);
                   if(bonusamountSum!=null){                      
                      bonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                      mapOliidtoIncBm.put(inckey,BonusamountSum);
                   }
              }
              else{
                   mapOliidtoIncBm.put(inckey,objInc.Bonus_Amount__c);
                }
          }
          if(objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c=='New_Logo_Bonus'){
               inckey=objInc.vic_Opportunity_Product_Id__c+objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c;
              if(mapOliidtoIncBm!=null && mapOliidtoIncBm.size()>0 && mapOliidtoIncBm.containskey(objInc.vic_Opportunity_Product_Id__c)){
                  
                   BonusamountSum=mapOliidtoIncBm.get(objInc.vic_Opportunity_Product_Id__c);
                   if(BonusamountSum!=null){
                      BonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                      mapOliidtoIncBm.put(inckey,BonusamountSum);
                   }
              }
              else{
                   mapOliidtoIncBm.put(inckey,objInc.Bonus_Amount__c);
                }
          }
          if(objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c=='Kickers'){
               inckey=objInc.vic_Opportunity_Product_Id__c+objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c;
              if(mapOliidtoIncBm!=null && mapOliidtoIncBm.size()>0 && mapOliidtoIncBm.containskey(objInc.vic_Opportunity_Product_Id__c)){
                  
                   BonusamountSum=mapOliidtoIncBm.get(objInc.vic_Opportunity_Product_Id__c);
                   if(BonusamountSum!=null){
                       
                      BonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                      mapOliidtoIncBm.put(inckey,BonusamountSum);
                   }
              }
              else{
                   mapOliidtoIncBm.put(inckey,objInc.Bonus_Amount__c);
                }
          }
     }
    return mapOliidtoIncBm;
     
    }

}