// Description : This batch class would create/modify two PIDs per Account on change in respective Account.
// Duration    : This batch class would run every hours daily.
// APC Change
public with sharing class GPBatchAccountAutomation extends GPBatchAccountAutomationHelper
    implements Database.Batchable<sObject>, Database.Stateful, Database.RaisesPlatformEvents, schedulable {

    public String whereClause = System.label.GP_APC_Where_Clause;

    public GPBatchAccountAutomation() {}

    public GPBatchAccountAutomation(String whereClause) {
        this.whereClause = whereClause;
    }
    
    List<Account> listOfAccountsSyncedInOracle = new List<Account>();    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {        
        
        String query = 'SELECT id, RecordTypeId, Name, Description, Sub_Business__c, Sub_Business__r.Name, ';
        query += 'Business_Group__c, Industry_Vertical__c, Sub_Industry_Vertical__c, CreatedDate, GP_Pinnacle_Process_Log__c, ';
        query += 'Sales_Unit__r.Sales_Leader__r.OHR_ID__c, Sales_Unit__r.Sales_Leader__r.IsActive, ';
        query += 'GP_Account_PID__c, GP_Account_BD_Spend__c, Business_Segment__c, Business_Segment__r.Name ';
        query += 'FROM Account ';
        
        if(this.whereClause != 'NA') {
            query += this.whereClause;
        } else {
            query += 'WHERE lastModifiedDate = Today AND Business_Group__c != \'Genpact\' AND Is_This_A_Partner_Account__c != \'Yes\' AND Inactive__c = false';
        }
        
        return Database.getQueryLocator(query);
    }
        
    public void execute(SchedulableContext sc) {
        Database.executeBatch(new GPBatchAccountAutomation(), 1);
    }
    
    public void execute(Database.BatchableContext BC, List<Sobject> listOfAccounts) {
        // TODOS:
        // Savepoint
        // Get PID master data (SDO, Operating Unit, Project Org?, 'GPM, PID, FP&A, Controller Approval user' tagging in PID)
        // Create Indirect PID and deal
        // Create child records (WL, Mandatory key Members, etc)
        
        List<Account> listOfNewAccounts = new List<Account>();
        Set<Id> setOfNewAccountIds = new Set<Id>();
        List<Account> listOfUpdatedAccounts = new List<Account>();
        Set<String> setOfPIDNos = new Set<String>();
        Map<Id, String> mapOfAccIdWithStringValue = new Map<Id, String>();
        Boolean isSuccess = true;
        String errorMessage = '';
        Boolean isUpsert = false;
        
        // Get product master details
        fetchProductMaster(mapOfVerticalWithPMs);
        
        for(Account acc : (List<Account>)listOfAccounts) {
            if(String.isBlank(acc.GP_Account_PID__c) && String.isBlank(acc.GP_Account_BD_Spend__c)) {
                listOfNewAccounts.add(acc);
                setOfNewAccountIds.add(acc.Id);
                mapOfAccIdWithStringValue.put(acc.Id, 'Both');
            } else {
                listOfUpdatedAccounts.add(acc);
                if(String.isNotBlank(acc.GP_Account_PID__c)) setOfPIDNos.add(acc.GP_Account_PID__c);
                if(String.isNotBlank(acc.GP_Account_BD_Spend__c)) setOfPIDNos.add(acc.GP_Account_BD_Spend__c);
                
                if(String.isNotBlank(acc.GP_Account_PID__c) && String.isBlank(acc.GP_Account_BD_Spend__c)) {
                    mapOfAccIdWithStringValue.put(acc.Id, '1');
                    listOfNewAccounts.add(acc);
                	setOfNewAccountIds.add(acc.Id);
                    isUpsert = true;
                }
                
                if(String.isBlank(acc.GP_Account_PID__c) && String.isNotBlank(acc.GP_Account_BD_Spend__c)) {
                    mapOfAccIdWithStringValue.put(acc.Id, '2');
                    listOfNewAccounts.add(acc);
                	setOfNewAccountIds.add(acc.Id);
                    isUpsert = true;
                }
            }
        }
        
        // Rollback if PID creation failed.
        Savepoint sp = Database.setSavepoint();
        
        if(listOfNewAccounts.size() > 0) {
            try {
                // Check already Account BD PID created under the Account.
                List<GP_Project__c> listOfBDPIDs = [Select id, GP_EP_Project_Number__c from GP_Project__c 
                                                    where (GP_Project_Type__c = 'Account BD' OR GP_Project_Type__c = 'Account') 
                                                    AND GP_Customer_Hierarchy_L4__c =: setOfNewAccountIds 
                                                    AND GP_Oracle_PID__c = 'NA'];
                
                if(listOfBDPIDs.size() > 0) {
                    String epNos = '';
                    for(GP_Project__c pid : listOfBDPIDs) {
                        epNos += pid.GP_EP_Project_Number__c + ',';
                    }
                    epNos = epNos.removeEnd(',');
                    errorMessage += 'PIDs with EP numbers '+epNos+' already exists with no Oracle PID generated.';
                    isSuccess = false;
                } else {
                    // call create pid method
                    GPBatchAccountAutomationHelper.GPBatchResponseWrapper response = 
                        createNewPIDsForAccounts(listOfNewAccounts, errorMessage, mapOfAccIdWithStringValue);
                    isSuccess = response.isSuccess;
                    errorMessage = response.message;
                }
                
                if(!isSuccess) {
                    Database.rollback(sp);                    
                }
                
                for(Account acc : listOfNewAccounts) {
                    listOfAccountsSyncedInOracle.add(new Account(Id = acc.Id, GP_Pinnacle_Process_Log__c = errorMessage));
                }
                
            } catch(Exception ex) {
                Database.rollback(sp);
                
                for(Account acc : listOfNewAccounts) {
                    listOfAccountsSyncedInOracle.add(new Account(Id = acc.Id, GP_Pinnacle_Process_Log__c = ex.getMessage()));
                }
            }
        }
        
        if(listOfUpdatedAccounts.size() > 0 && setOfPIDNos.size() > 0 && !isUpsert) {
            try {
                // Check if PID exists and in draft stage.
                List<GP_Project__c> listOfPIDs = [Select Id, GP_Deal__c, GP_Approval_Status__c, GP_Oracle_PID__c 
                                                  from GP_Project__c 
                                                  where GP_Oracle_PID__c =: setOfPIDNos];
                
                Set<Id> setOfDealIds = new Set<Id>();
                
                if(listOfPIDs.size() > 0) {
                    String pidNos = '';
                    for(GP_Project__c pid : listOfPIDs) {
                        if(pid.GP_Approval_Status__c == 'Pending for Approval') {
                            isSuccess = false;
                            pidNos += pid.GP_Oracle_PID__c + ',';
                        }
                        
                        setOfDealIds.add(pid.GP_Deal__c);
                    }
                    
                    if(isSuccess) {
                        // call update pid method
                        GPBatchAccountAutomationHelper.GPBatchResponseWrapper response = 
                        updatePIDsForAccounts(listOfUpdatedAccounts, setOfDealIds);
                        
                        isSuccess = response.isSuccess;
                    	errorMessage = response.message;
                    } else {
                        pidNos = pidNos.removeEnd(',');
                        errorMessage += 'PIDs with Oracle numbers '+pidNos+' are in pending for approval stage.';
                    }
                    
                } else {
                    errorMessage += 'PIDs not found in the system. ';
                    isSuccess = false;
                }
                
                if(!isSuccess) {
                    Database.rollback(sp);
                }
                
                for(Account acc : listOfUpdatedAccounts) {
                    listOfAccountsSyncedInOracle.add(new Account(Id = acc.Id, GP_Pinnacle_Process_Log__c = errorMessage));
                }
                
            } catch(Exception ex) {
                Database.rollback(sp);
                
                for(Account acc : listOfUpdatedAccounts) {
                    listOfAccountsSyncedInOracle.add(new Account(Id = acc.Id, GP_Pinnacle_Process_Log__c = ex.getMessage()));
                }
            }
        }        
    }
    
    public void finish(Database.BatchableContext BC) {
        // Update approval status of all PIDs (Approved)
        // Update account on PID submitted to oracle and creation.
        system.debug('==acc=='+listOfAccountsSyncedInOracle);
        if(listOfAccountsSyncedInOracle.size() > 0) {
            update listOfAccountsSyncedInOracle;
        }        
    }
}