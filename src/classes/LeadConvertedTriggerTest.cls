@isTest
public class LeadConvertedTriggerTest {
	private static Account oAccount;
    public static Contact oContact;
    
    public static testmethod void testLeadConvertedMethod(){
        setupData();
        Lead leadObj = new Lead();
        leadObj.lastName = 'test Lead';
        leadObj.Email = 'abc@xyz.com';
        leadObj.Company = 'genpact';
        leadObj.Status = 'raw';
        leadObj.LeadSource = 'Digital';
        leadObj.Source__c = 'Website';
        insert leadObj;
		
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(leadObj.Id);
        
        LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setAccountId(oAccount.ID);
        lc.setOpportunityName('testOpp');
        lc.setContactId(oContact.ID);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        
    }
    
    private static void setupData(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );       	
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        System.runAs(u){
            
           oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
             oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        }    
    }
    
}