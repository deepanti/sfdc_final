@isTest
private class GPDomainOMSDetailTracker {
    
    @testSetup
    private static void setupCommonData(){
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_oms_detail__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;
        
        GP_OMS_Detail__c omsDetail = GPCommonTracker.getOMSDetail(dealObj);
        omsDetail.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        omsDetail.GP_Record_Type_Name__c = 'WorkLoaction';
        insert omsDetail;
        
        GP_OMS_Detail__c omsDetail2 = GPCommonTracker.getOMSDetail(dealObj);
        omsDetail2.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Expense').getRecordTypeId();
        omsDetail2.GP_Record_Type_Name__c = 'Expenses';
        insert omsDetail2;
        
        GP_OMS_Detail__c omsDetail3 = GPCommonTracker.getOMSDetail(dealObj);
        omsDetail3.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Pricing View').getRecordTypeId();
        omsDetail3.GP_Record_Type_Name__c = 'Budget';
        insert omsDetail3;
    }
    
    @isTest static void testGPDomainOMSDetail() {}
}