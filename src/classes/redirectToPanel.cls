/**
* @author: Ashish Srivastav
* @date: Nov 2018
*/
public class redirectToPanel {
    
	// Variable Declaration
    public static User userDetails;
    public static User_VIC_Role__c userVicRoleMappings;
    public static VIC_Role__c vicRoles;
 	public static Master_VIC_Role__c masterVicRole;
    public static Plan__c planName;
    
    @AuraEnabled
    public static String redirectToUserPanel(){
        try{
            String panelName = '';
            userVicRoleMappings = new User_VIC_Role__c();
            masterVicRole = new Master_VIC_Role__c();
            vicRoles = new VIC_Role__c();
            planName = new Plan__c();
           
            userDetails = [SELECT id,Name,ProfileId FROM User WHERE ID =: UserInfo.getUserId()];
            Profile userProfile = [SELECT Id,Name from Profile Where Id =:userDetails.ProfileId];
            
            System.debug('Profile == >> '+userProfile.Name);
          
            if(userProfile.Name == 'Genpact Sales Rep' || userProfile.Name =='Genpact SL / CP' || userprofile.Name == 'Genpact SL/CP without IP' || userprofile.Name == 'Sales Rep New' || userprofile.Name == 'SL/CP New'){
                userVicRoleMappings = [SELECT Id,Name,Master_VIC_Role__c FROM User_VIC_Role__c WHERE User__c =:userDetails.Id LIMIT 1];
                if(userVicRoleMappings != null){
                    masterVicRole = [Select id,Role__c,Horizontal__c From Master_VIC_Role__c Where Id =:userVicRoleMappings.Master_VIC_Role__c LIMIT 1]; 
                    if(masterVicRole != null){
                        vicRoles = [Select Id,Plan__r.Name,Plan__c From VIC_Role__c Where Master_VIC_Role__c =: masterVicRole.Id LIMIT 1];
                        if(vicRoles != null){
                            planName = [Select id,Name,vic_Plan_Code__c From Plan__c Where Id =: vicRoles.Plan__c];
                            if(planName != null){
                                if(planName.Name == 'SL CP Enterprise' || planName.Name == 'SEM'){
                                    panelName = 'Supervisor';
                                    System.debug('planName == '+planName);
                                }else if(planName.Name == 'BDE' || planName.Name == 'Enterprise GRM' || planName.Name == 'Capital Market GRM' || planName.Name == 'IT GRM'){
                                    panelName = 'Sales rep';
                                    System.debug(panelName);
                                }else{
                                    panelname = 'No Privilege';
                                }
                            }else{  panelname = 'No Privilege';
                            } }else{ panelname = 'No Privilege';
                        } }else{ panelname = 'No Privilege'; } }else{panelname = 'No Privilege'; }
            }else{
               panelname = 'No Privilege';
            }   
                
         return panelName;
        }catch(Exception e){
            return 'No Privilege';
        }

    }
}