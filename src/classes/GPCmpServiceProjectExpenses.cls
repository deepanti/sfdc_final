/**
 * @group ProjectExpense.
 * @group-content ../../ApexDocContent/ProjectExpense.htm.
 *
 * @description Apex service class having Aura enabled methods.
 *              to fetch, insert, update and delete expenses under a project.
 */
public class GPCmpServiceProjectExpenses {

    /**
     * @description Returns Project Expense data for a given project Id.
     * @param projectId Salesforce 18/15 digit Id of project.
     * 
     * @return GPAuraResponse json of Project Expense data.
     * 
     * @example
     * GPCalloutHandlerProjectExpense.getProjectExpenseData(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getProjectExpenseData(Id projectId) {
        GPControllerProjectExpenses expenseController = new GPControllerProjectExpenses(projectId);
        return expenseController.getProjectExpenseRecord();
    }

    /**
     * @description Returns Flag if the data has been successfully upserted.
     * @param serializedListOfProjectExpense Serialized list of project expense.
     * 
     * @return GGPAuraResponse json of Falg whether the records are successfully inserted.
     * 
     * @example
     * GPCmpServiceProjectExpenses.saveProjectExpenseService(<Serialized list of project expense>);
     */
    @AuraEnabled
    public static GPAuraResponse saveProjectExpenseService(String serializedListOfProjectExpense, Decimal nextExpenseCounter, Id projectId) {
        List < GP_Project_Expense__c > listOfProjectExpense = (List < GP_Project_Expense__c > ) JSON.deserialize(serializedListOfProjectExpense, List < GP_Project_Expense__c > .class);
        GPControllerProjectExpenses expenseController = new GPControllerProjectExpenses(listOfProjectExpense, nextExpenseCounter, projectId);
        return expenseController.saveProjectExpense();
    }

    /**
     * @description deletes Project Expense data for a given project expense Id.
     * @param projectExpenseId Salesforce 18/15 digit Id of project expense.
     * 
     * @return GPAuraResponse json of Project Expense data has been deleted.
     * 
     * @example
     * GPCmpServiceProjectExpenses.deleteProjectExpense(<18 or 15 digit project expense Id>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteProjectExpense(Id projectExpenseId) {
        GPControllerProjectExpenses expenseController = new GPControllerProjectExpenses();
        return expenseController.deleteProjectExpense(projectExpenseId);
    }

    /**
     * @description Returns Project Expense data from OMS for a given project Id.
     * @param projectId Salesforce 18/15 digit Id of project.
     * 
     * @return GPAuraResponse json of Project Expense data.
     * 
     * @example
     * GPCalloutHandlerProjectExpense.getProjectExpenseData(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getOMSProjectExpenseData(Id projectId) {
        GPControllerProjectExpenses expenseController = new GPControllerProjectExpenses(projectId);
        return expenseController.getOMSProjectExpenseRecord();
    }
    
}