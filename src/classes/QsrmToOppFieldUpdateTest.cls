@isTest
public class QsrmToOppFieldUpdateTest {

  public static  Opportunity opp;
  public static   User u;
   public static  QSRM__c qsrm;
  @isTest public static void testTrigger()
  {
     setupTestData();
      system.debug('Qsrm'+qsrm);
     delete qsrm;
  }
  public static void setupTestData(){
      
          
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );           
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
       
        System.runAs(u){
         
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
            
            Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
      
             Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            Test.startTest();
            insert opp;
            Test.stopTest();
            system.debug('recordId'+Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('Detail Record Type').getRecordTypeId());
            ID QSRMrecordTypeId = Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('Detail Record Type').getRecordTypeId();
          
           /* qsrm = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id,Status__c='In Approval',recordTypeId=QSRMrecordTypeId);*/
             qsrm = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id);
            insert qsrm;
            system.debug('QSRM=='+qsrm);
        }
    } 
}