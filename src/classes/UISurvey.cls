public class UISurvey {
    @AuraEnabled
    public static user userflagSet()
    {
        try
        {
            
            User u;
            u=[select id,SurveyFilled__c,VisitedHomePage__c,VisitedOpportunityFlow__c from user  where id=:userInfo.getUserId()];
            return u;
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'UISurvey', 'userflagSet','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static void setField() {
        try
            
        {
            
            User u;
            u=[select id,SurveyFilled__c from user  where id=:userInfo.getUserId()];
            u.SurveyFilled__c=true;
            update u;
            if(Test.isRunningTest())
            {
                throw new QueryException();
            }
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'UISurvey', 'setField','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    @AuraEnabled
    public static void trackHomePage()
    {
        try
        {
            
            User u;
            u=[select id,VisitedHomePage__c from user  where id=:userInfo.getUserId()];
            u.VisitedHomePage__c=true;
            update u;
            if(Test.isRunningTest())
            {
                throw new QueryException();
            }
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'UISurvey', 'trackHomePage','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static void trackOpportunityFlow()
    {
        try
        {
            
            User u;
            u=[select id,VisitedOpportunityFlow__c from user  where id=:userInfo.getUserId()];
            u.VisitedOpportunityFlow__c=true;
            update u;
            if(Test.isRunningTest())
            {
                throw new QueryException();
            }
        }
        catch(Exception e)
        {
            CreateErrorLog.createErrorRecord(UserInfo.getUserId(),e.getMessage(), '', e.getStackTraceString(),'UISurvey', 'trackOpportunityFlow','Fail','',String.valueOf(e.getLineNumber()));
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}