 //Author: Anoop Verma
// Date:10-Jul-2020
// CR:CRQ000000076988
// Description:Batch job to  send Email to PID audience after 1 hour if records not processed in Pinnacle.

global class GPBatchResourceSyncAlert implements Database.Batchable<sObject>,Database.Stateful , schedulable {
	Datetime approvaltime=System.Datetime.now().addMinutes(-120);    
    Datetime currenttime=System.Datetime.now().addMinutes(-60);    
    List<GP_Project__c> finalistofPID=new List<GP_Project__c>();
    List<GP_Resource_Allocation__c> listofPIDResources= new List<GP_Resource_Allocation__c>();
    List<GP_Resource_Allocation__c> finalistofPIDResources= new List<GP_Resource_Allocation__c>();        
   
   
  
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select id, GP_Employee__r.name, GP_Employee__r.GP_Final_OHR__c,GP_Start_Date__c,
                                         GP_End_Date__c, GP_Oracle_Status__c, GP_Project__r.ID,
                                         GP_Project__r.GP_Current_Working_User__r.Name,
                                        GP_Project__r.Name,GP_Project__r.GP_Oracle_PID__c, GP_Project__r.GP_Send_Email_Notification_Addresses__c,
										GP_Oracle_Process_Log__c from GP_Resource_Allocation__c Where GP_isupdated__c = true 
										and GP_Project__r.GP_is_Send_Notification__c = true
										and GP_Project__r.GP_Oracle_Status__c ='S'
										and GP_Project__r.GP_Approval_DateTime__c!=null
										and  GP_Project__r.GP_Approval_Status__c in('Approved','Closed')                                        
                                        and GP_Project__r.GP_Approval_DateTime__c!=null
                                        and GP_Project__r.GP_Approval_DateTime__c>=:approvaltime 
                                        and GP_Project__r.GP_Approval_DateTime__c<=:currenttime
                                                   
                                        ]);
    }
  

   
     global void execute(SchedulableContext sc) {
        GPBatchResourceSyncAlert objAutoReject = new GPBatchResourceSyncAlert();
        Database.executebatch(objAutoReject); 
    }

   global void execute(Database.BatchableContext bc, List<GP_Resource_Allocation__c> listofPIDResources) {
           if(listofPIDResources.size()>0) {
               finalistofPIDResources.addAll(listofPIDResources);
             }
              
       
    }

    global void finish(Database.BatchableContext bc) {
     List<GP_Project__c> listOfPids = new List<GP_Project__c>();
     Map<Id, GP_Project__c> mapOfPIDs = new Map<Id, GP_Project__c>();    
     Map<Id, List<GP_Resource_Allocation__c>> mapOfPIDWithRALists = new Map<Id, List<GP_Resource_Allocation__c>>();
     Map<Id, string> mapOfPIdwithEmails = new Map<Id, string>();  
      Map<Id, string> mapOfPIdwithUser = new Map<Id, string>();
     //System.debug('==finallist=='+ finalistofPIDResources.size());
        
      
        
         for(Integer i=0;i<finalistofPIDResources.size();i++)
        {
            List<GP_Resource_Allocation__c> listOfResourceAs;
            GP_Project__c pid =new GP_Project__c(); 
            
            pid.id= finalistofPIDResources[i].GP_Project__r.ID;
            pid.Name= finalistofPIDResources[i].GP_Project__r.Name ;
            pid.GP_Oracle_PID__c= finalistofPIDResources[i].GP_Project__r.GP_Oracle_PID__c;        
            string email= finalistofPIDResources[i].GP_Project__r.GP_Send_Email_Notification_Addresses__c;
            string  currentUserName= finalistofPIDResources[i].GP_Project__r.GP_Current_Working_User__r.Name;
           // System.debug('==finalPID=='+ pid);
            //Start - Avinash : Below code written for getting PID and List of resource in map
            if(mapOfPIDWithRALists.containsKey(finalistofPIDResources[i].GP_Project__r.ID)) {
                 listOfResourceAs=new List<GP_Resource_Allocation__c>();
                listOfResourceAs = mapOfPIDWithRALists.get(finalistofPIDResources[i].GP_Project__r.ID);
                listOfResourceAs.add(finalistofPIDResources[i]);               
                mapOfPIDWithRALists.put(finalistofPIDResources[i].GP_Project__r.ID,listOfResourceAs);
              
            }
            else {           
                 listOfResourceAs=new List<GP_Resource_Allocation__c>();
                listOfResourceAs.add(finalistofPIDResources[i]);                          
                mapOfPIDWithRALists.put(finalistofPIDResources[i].GP_Project__r.ID,listOfResourceAs);
                           
           }
            //End - Avinash : Below code written for getting PID and List of resource in map
           
            
            if(!mapOfPIDs.containsKey(pid.id))
            {
                mapOfPIDs.put(pid.Id, pid);
                mapOfPIdwithEmails.put(pid.Id,email);
                mapOfPIdwithUser.put(pid.Id,currentUserName);
            }
             
        
       

        }
       
        
      
        
         Id oweaId = GPCommon.getOrgWideEmailId();
          List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
            
        if(mapOfPIDWithRALists.keySet().size() > 0) {
                    for(Id pid : mapOfPIDWithRALists.keySet()) {
                        List<GP_Resource_Allocation__c> listOfUpdatedResource = new List<GP_Resource_Allocation__c>();
                        listOfUpdatedResource=mapOfPIDWithRALists.get(pid);                        
                         GP_Project__c p = mapOfPIDs.get(pid);
                                   
                String htmlBody = '';
                if(listOfUpdatedResource.size() > 0) {
                    htmlBody = '<html> <head><style>td {padding-left:5px; text-align:left; border-collapse: collapse;border:solid 1px;}th{text-align:left;padding-left:5px;color:white;background-color: #005595;border-collapse: collapse;border:solid 1px;} </style></head>'
                        + '<tr><td bgcolor="#005595" width="100%" height="30px" color="white" align="left">'
                        + '<b style="color:white;margin-left:10px;">'
                        + '<H4>SFDC-PINNACLE NOTIFICATION!</H4></b>'
                        + '</td></tr><tr><td><div style="color:#333399;margin-left:10px;margin-top:10px">'
                        + 'Dear '+mapOfPIdwithUser.get(p.id)+',</div> '
                        + '<div style="color:#333399;margin-left:10px;margin-top:20px">Find below Status of Resource Allocations Processed in Oracle PA on PID '+ p.GP_Oracle_PID__c +'.For any errors in allocations, do correct the errors and resubmit the PID in Pinnacle SFDC. Until error is not solved, resource will not be allocated on PID in Oracle and will not be able to fill timesheet. <br/> <br/>'
                        + '<table  style="padding-left:10 px;border-collapse: collapse;border:solid 1px; width:100%">'
                        + '<tr><th>Employee Name</th>'
                        + '<th>OHRID</th><th>Oracle Status</th>'
                        + '<th>Oracle Message</th><th> Allocation Start Date</th>'
                        + '<th>Allocation End Date</th></tr>';
                       
                    for(GP_Resource_Allocation__c ra : listOfUpdatedResource) {
                        
                        htmlBody +='</td><td>' + (String.isNotBlank(ra.GP_Employee__r.name) ? ra.GP_Employee__r.name : '')
                            + '</td><td>'+ (String.isNotBlank(ra.GP_Employee__r.GP_Final_OHR__c) ? ra.GP_Employee__r.GP_Final_OHR__c : '')
                            +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Status__c) ? ra.GP_Oracle_Status__c : '')
                            +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Process_Log__c) ? ra.GP_Oracle_Process_Log__c : '') 
                            +'</td><td>'+ (ra.GP_Start_Date__c!=null ? string.valueof(ra.GP_Start_Date__c).removeEnd('00:00:00'):'')
                            +'</td><td>'+(ra.GP_End_Date__c!=null ? string.valueof(ra.GP_End_Date__c).removeEnd('00:00:00'):'')
                            +'</td></tr>';
                    }
                    
                    htmlBody += '</table><br/><br/>';
                    htmlBody += 'Please log on to Pinnacle-SFDC : <a href="' + System.Label.GP_Send_Notification_Alert_URL + p.Id +'">'+p.Name+'</a><br/>';
                    htmlBody += 'This is an Auto Generated Mail. Please do not reply.';
                    htmlBody += '<br/><br/>Thanks';
                    htmlBody += '<br/>Pinnacle SFDC.<br/><br/>';
                }
                
                      
                if(String.isNotBlank(htmlBody)) {
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                   
                    // Get email addresses per PID.
                    List<String> listOfEmailAddress = getEmailAddresses(mapOfPIdwithEmails.get(p.id));
                                      
                    if(!listOfEmailAddress.isEmpty()) {
                       
                        email.setToAddresses(listOfEmailAddress);
                        
                        email.setSubject('Resource Allocation Status on PID '+p.GP_Oracle_PID__c);
                     
                        email.setHtmlBody(htmlBody);
                        
                        if (oweaId != null) {
                            email.setOrgWideEmailAddressId(oweaId);
                        }
                        
                        emailNotifications.add(email);
                    }                
                }
                
                listOfPids.add(new GP_Project__c(Id=p.Id,GP_is_Send_Notification__c=false));

            }
           
        }
        
       
          if(!emailNotifications.isEmpty()) {
                Messaging.sendEmail(emailNotifications);
              
            }
        
        if(!listOfPids.isEmpty()) {
                GPDomainProject.skipRecursiveCall = true;
                update listOfPids;
               
            }
    }
    
     global List<String> getEmailAddresses(String emailAddresses) {
        List<String> listOfEmails = new List<String>();
        List<String> listOfTempEmails = emailAddresses.split(';');
        
        for(String str : listOfTempEmails) {
            if(String.isNotBlank(str)) {
                listOfEmails.add(str);
            }
        }
        
        return listOfEmails;
    }
}