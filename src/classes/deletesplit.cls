global class deletesplit implements Database.Batchable<sobject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        string stage='6. Signed Deal';
        date dd = Date.newInstance(2018, 1, 1);
        String query = 'Select id,name,TCV1__c From opportunity where TCV1__c>0 and actual_close_date__c > :dd and stagename=:stage';
        
        //    string i = '0069000000zw4bB';
        //    String query = 'Select id,name,TCV1__c From opportunity where id =:i';
        if(Test.isRunningTest())
            query = 'Select id,name,TCV1__c From opportunity limit 100';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List <opportunity> opp) {
        //    datetime d = Datetime.newInstance(2016, 1, 1);
        list<id> oppids = new list<id>();
        Map<String, opportunity> oppmap = new Map<String, opportunity>(opp);
        for(opportunity objCS : opp)
        {
            oppids.add(objCS.id);
        }
        list<OpportunitySplit> delsplit = new list<OpportunitySplit>();
        for(OpportunitySplit ots :[select OpportunityId,SplitOwnerId from OpportunitySplit where OpportunityId in :oppids and SplitTypeId = '14990000000fxXIAAY'])
        {
                delsplit.add(ots);
        }
        try
        {
            delete delsplit;
        }
        catch(exception e)
        {string allstringa = string.join(delsplit,',');
         CreateErrorLog.createErrorRecord(null,e.getMessage(), allstringa, e.getStackTraceString(),'olddata_splitupdate', ' ','Fail','',String.valueOf(e.getLineNumber()));     
         //   system.debug('error e='+e.getmessage());
        }
    }  
    global void finish(Database.BatchableContext BC) {
    }
    
}