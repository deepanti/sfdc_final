public with sharing class GPLWCControllerPIDApproverHomePage {

    public GPAuraResponse getAllPendingForApprovalPIDsOfUser() {
        try{
            List<GP_Project__c> listOfPendingPIDs = new List<GP_Project__c>();
            List<ProcessInstanceWorkitem> lstProcessInstanceWorkItems = getPendingWorkItems();
            string strObjectName = 'GP_Project__c';
            Set<id> setTargetObjectId = new Set<id>();

            if(lstProcessInstanceWorkItems != null && !lstProcessInstanceWorkItems.isEmpty()) {
                for(ProcessInstanceWorkitem objPIWI : lstProcessInstanceWorkItems) {
                    if(objPIWI.ProcessInstance.TargetObjectId.getsobjecttype() == getoidSobjectType(strObjectName)) {
                        setTargetObjectId.add(objPIWI.ProcessInstance.TargetObjectId);
                    }
                }

                if(setTargetObjectId.size() > 0) {
                    for(GP_Project__c pid : [SELECT Id, Name, GP_Oracle_PID__c, 
                        GP_EP_Project_Number__c, GP_Current_Working_User__r.Name, GP_Project_Submission_Date__c
                        FROM GP_Project__c WHERE id in: setTargetObjectId 
                        ORDER BY GP_Project_Submission_Date__c DESC]) {
                            listOfPendingPIDs.add(pid);
                    }
                }
            }            
            return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(listOfPendingPIDs));
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    private list<ProcessInstanceWorkitem> getPendingWorkItems() {
        return  [SELECT p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId, p.ProcessInstanceId,
                    p.OriginalActorId,p.Id,p.ActorId , p.CreatedDate
                FROM ProcessInstanceWorkitem p 
                WHERE ProcessInstance.Status = 'Pending' 
                AND ActorId =: Userinfo.getUserId()];
    }

    private Schema.SObjectType getoidSobjectType(String objectAPIName) {
        Map<String, Schema.SObjectType> globalDescription =  Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = globalDescription.get(objectAPIName); 
        return sObjType;
    }
}