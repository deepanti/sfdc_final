//Description : schedular class for GPBatchNotifyProjectUsersCRNStgeUpdte
//Created By : mandeep.singh@saasfous.com
global class GPBatchNotifyProjectUsrCRNStgUpdtSchdulr implements schedulable
{
    global void execute(SchedulableContext sc)
    {
        GPBatchNotifyProjectUsersCRNStgeUpdte objGpnotifyprjct = new GPBatchNotifyProjectUsersCRNStgeUpdte(); //ur batch class
        database.executebatch(objGpnotifyprjct,200);
    }
}