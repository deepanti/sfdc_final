/**
 * @author Anmol.kumar
 * @date 07/09/2017
 *
 * @group ProjecWorklocation
 * @group-content ../../ApexDocContent/ProjecWorklocation.htm
 *
 * @description Apex Controller for project work location module
 */
public class GPControllerProjectWorkLocation {

    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String PROJECT_LABEL = 'project';

    private final String MAP_OF_MASTER_WORKLOCATION_RECORDTYPE_LABEL = 'mapOfMasterWorkLocationRecordType';
    private final String LIST_OF_PROJECT_WORKLOCATION_RECORDTYPE_LABEL = 'projectWorkLocationRecordType';
    private final String LIST_OF_MASTER_WORKLOCATION_RECORDTYPE_LABEL = 'masterWorkLocationRecordType';
    private final String LIST_OF_PROJECT_WORKLOCATION_SDO_LABEL = 'listOfSDOProjectWorklocation';
    private final String LIST_OF_PROJECT_WORKLOCATION_LABEL = 'listOfProjectWorklocation';
    private final String IS_SET_USING_OMS_LABEL = 'isSetUsingOMS';

    private final String PROJECT_WORK_LOCATION_API_NAME = 'GP_Project_Work_Location_SDO__c';

    private final String WORK_LOCATION_RECORDTYPE_NAME = 'Work Location';
    private final String WORK_LOCATION_API_NAME = 'GP_Work_Location__c';

    private JSONGenerator gen;
    private Id projectId;

    private List < GP_Project_Work_Location_SDO__c > listOfSDOWorkLocation;
    private List < GP_Project_Work_Location_SDO__c > listOfSDOWorkLocationToBeDeleted;
    private Map < String, RecordType > mapOfMasterWorkLocationRecordType;
    private List < GP_Project_Work_Location_SDO__c > listOfWorkLocation;
    private List < RecordType > listOfProjectWorkLocationRecordType;
    private List < RecordType > listOfMasterWorkLocationRecordType;
    private GP_Project_Address__c directProjectAddress;
    private GP_Project__c project;
    private Map < String, GP_Work_Location__c > mapOfWorkLocationCodeToWorkLocationMaster = new Map < String, GP_Work_Location__c > ();
    private Set < String > setOfWorkLocationCode = new Set < String > ();
    private List < GP_OMS_Detail__c > listOfOMSDetail;
    private list < GP_Work_Location__c > lstWorkLocationMaster;
    private Boolean isSetUsingOMS = false;
    // HSN changes
    private String hsnCatId, hsnCatName;
    private Boolean isHSNNeeded = true;

    public GPControllerProjectWorkLocation() {}

    public GPControllerProjectWorkLocation(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description Paramiterized constructor
     * @param List<GP_Project_Work_Location_SDO__c> listOfWorkLocation
     * 
     * @example
     * GPCmpServiceProjectWorkLocation.getWorklocationData(<listOfWorkLocation>);
     */
    public GPControllerProjectWorkLocation(List < GP_Project_Work_Location_SDO__c > listOfWorkLocation) {
        this.listOfWorkLocation = listOfWorkLocation;
    }
    // HSN changes
    public GPControllerProjectWorkLocation(List < GP_Project_Work_Location_SDO__c > listOfWorkLocation, String hsnCatId, String hsnCatName, Boolean isHSNNeeded) {
        this.listOfWorkLocation = listOfWorkLocation;
        this.hsnCatId = hsnCatId;
        this.hsnCatName = hsnCatName;
        this.isHSNNeeded = isHSNNeeded;
    }

    public GPControllerProjectWorkLocation(List < GP_Project_Work_Location_SDO__c > listOfWorkLocation, List < GP_Project_Work_Location_SDO__c > listOfSDOWorkLocationToBeDeleted) {
        this.listOfSDOWorkLocationToBeDeleted = listOfSDOWorkLocationToBeDeleted;
        this.listOfWorkLocation = listOfWorkLocation;
    }

    public GPAuraResponse getWorklocation() {

        try {
            setProjectData();
            setListOfWorkLocation();
            setDirectProjectAddress();

            setRecordTypes();
            setJson();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    private void setDirectProjectAddress() {
        try {
            directProjectAddress = GPSelectorProjectAddress.selectDirectProjectAddressUnderProject(projectId);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
        }
    }

    private void setProjectData() {
        project = [SELECT Id,
            Name,
            GP_Deal__c,
            GP_End_Date__c,
            RecordType.Name,
            GP_Start_Date__c,
            GP_Deal_Category__c,
            GP_Bill_To_Country__c,
            GP_Parent_Project__c,
            GP_Oracle_PID__c,
            GP_Operating_Unit__r.GP_Is_GST_Enabled__c
            FROM GP_Project__c
            WHERE ID =: projectId
        ];
    }

    public GPAuraResponse upsertWorklocation() {
        try {
            //GPServiceProjectWorkLocationSDO.validateWorkLocationAndSDO(null, listOfWorkLocation);
            upsert listOfWorkLocation;
            if (listOfSDOWorkLocationToBeDeleted != null && !listOfSDOWorkLocationToBeDeleted.isEmpty()) {
                delete listOfSDOWorkLocationToBeDeleted;
            }

            projectId = listOfWorkLocation[0].GP_Project__c;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'WORK LOCATION');
            
            // Added to update the HSN detail on project if location tagged is GST enabled.
            if(String.isNotBlank(hsnCatId) && String.isNotBlank(hsnCatName)) {
				project.GP_HSN_Category_Id__c = hsnCatId;
            	project.GP_HSN_Category_Name__c = hsnCatName;
            }
            
            // Reset the HSN details.
            if(!isHSNNeeded) {
                project.GP_HSN_Category_Id__c = '';
            	project.GP_HSN_Category_Name__c = '';
            }
            
            update project;
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfWorkLocation, true));
    }

    public GPAuraResponse deleteProjectWorkLocation(Id projectWorkLocationId, Boolean deleteHSNDetails) {
        try {
            GP_Project_Work_Location_SDO__c projectWorkLocationObj = [select id ,GP_Project__c from GP_Project_Work_Location_SDO__c where Id =: projectWorkLocationId];
            projectId = projectWorkLocationObj.GP_Project__c;
            delete projectWorkLocationObj;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'WORKLOCATION');
            
            // Delete the HSN details if GST is not applicable either on OU or WLs.
            if(deleteHSNDetails) {
                project.GP_HSN_Category_Id__c = '';
            	project.GP_HSN_Category_Name__c = '';
            }
            
            update project;
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField(LIST_OF_PROJECT_WORKLOCATION_LABEL, listOfWorkLocation);
        gen.writeObjectField(LIST_OF_PROJECT_WORKLOCATION_SDO_LABEL, listOfSDOWorkLocation);
        gen.writeObjectField(LIST_OF_MASTER_WORKLOCATION_RECORDTYPE_LABEL, listOfMasterWorkLocationRecordType);
        gen.writeObjectField(LIST_OF_PROJECT_WORKLOCATION_RECORDTYPE_LABEL, listOfProjectWorkLocationRecordType);
        gen.writeObjectField(MAP_OF_MASTER_WORKLOCATION_RECORDTYPE_LABEL, mapOfMasterWorkLocationRecordType);
        gen.writeBooleanField(IS_SET_USING_OMS_LABEL, isSetUsingOMS);

        gen.writeBooleanField('isGSTEnabled', project.GP_Operating_Unit__r.GP_Is_GST_Enabled__c);

        if (directProjectAddress != null) {
            gen.writeObjectField('directProjectAddress', directProjectAddress);
        }

        gen.writeObjectField(PROJECT_LABEL, project);
        gen.writeEndObject();
    }

    private void setRecordTypes() {
        mapOfMasterWorkLocationRecordType = new Map < String, RecordType > ();
        listOfMasterWorkLocationRecordType = new List < RecordType > ();
        listOfProjectWorkLocationRecordType = new List < RecordType > ();

        for (RecordType recordTypeObject: [SELECT Id, Name, SobjectType
                FROM RecordType
                WHERE SobjectType =: WORK_LOCATION_API_NAME
                OR SobjectType =: PROJECT_WORK_LOCATION_API_NAME
            ]) {
            if (recordTypeObject.SobjectType == WORK_LOCATION_API_NAME) {
                listOfMasterWorkLocationRecordType.add(recordTypeObject);
                mapOfMasterWorkLocationRecordType.put(recordTypeObject.Name, recordTypeObject);
            } else {
                listOfProjectWorkLocationRecordType.add(recordTypeObject);
            }
        }
    }

    private void setListOfWorkLocation() {
        listOfWorkLocation = new List < GP_Project_Work_Location_SDO__c > ();
        listOfSDOWorkLocation = new List < GP_Project_Work_Location_SDO__c > ();

        for (GP_Project_Work_Location_SDO__c projectWorkLocation: GPSelectorProjectWorkLocationSDO.selectProjectWorkLocationAndSDORecordsUnderProject(projectId)) {
            if (projectWorkLocation.RecordType.Name == WORK_LOCATION_RECORDTYPE_NAME) {
                listOfWorkLocation.add(projectWorkLocation);
            } else {
                listOfSDOWorkLocation.add(projectWorkLocation);
            }
        }

        if (listOfWorkLocation.isEmpty() &&
            project.RecordType.Name == 'CMITS' &&
            (project.GP_Deal_Category__c == 'Sales SFDC' ||
                project.GP_Deal_Category__c == 'Sales SFDC')) {
            setTemporaryWorkLocationFromOMS();
        }
    }

    private void setTemporaryWorkLocationFromOMS() {
        setSetOfWorkLocationCode();
        setListOfWorkLocationMaster();
        setMapOfWorkLocationCodeToWorkLocationMaster();

        for (GP_OMS_Detail__c defaultWorkLocationOMS: listOfOMSDetail) {
            GP_Work_Location__c workLocationMaster = mapOfWorkLocationCodeToWorkLocationMaster.get(defaultWorkLocationOMS.GP_Work_Location_Code__c);

            GP_Project_Work_Location_SDO__c defaultProjectWorkLocation = new GP_Project_Work_Location_SDO__c();

            defaultProjectWorkLocation.GP_BCP_FLAG__c = defaultWorkLocationOMS.GP_BCP__c == 'Yes' ? true : false;
            defaultProjectWorkLocation.GP_BCP__c = (defaultWorkLocationOMS.GP_BCP__c == '' || defaultWorkLocationOMS.GP_BCP__c == null) ? 'No' : defaultWorkLocationOMS.GP_BCP__c;
            defaultProjectWorkLocation.GP_Primary__c = defaultWorkLocationOMS.GP_Primary__c;
            defaultProjectWorkLocation.GP_Project__c = project.Id;
            defaultProjectWorkLocation.GP_Work_Location__c = workLocationMaster.id;
            defaultProjectWorkLocation.GP_Work_Location__r = workLocationMaster;
            defaultProjectWorkLocation.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c','Work Location');

            listOfWorkLocation.add(defaultProjectWorkLocation);
        }

        isSetUsingOMS = (listOfOMSDetail != null && listOfOMSDetail.size() > 0);
    }

    private void setSetOfWorkLocationCode() {
        Id workLocationOMSDealTypeId = GPCommon.getRecordTypeId('GP_OMS_Detail__c', 'Work Location');
        setOfWorkLocationCode = new Set < String > ();

        listOfOMSDetail = [Select GP_Work_Location_Name__c,
            GP_Work_Location_Code__c,
            GP_Primary__c, GP_BCP__c
            From GP_OMS_Detail__c
            WHERE GP_Deal__c =: project.GP_Deal__c
            AND RecordTypeId =: workLocationOMSDealTypeId
        ];


        for (GP_OMS_Detail__c defaultWorkLocationOMS: listOfOMSDetail) {
            setOfWorkLocationCode.add(defaultWorkLocationOMS.GP_Work_Location_Code__c);
        }
    }

    private void setListOfWorkLocationMaster() {
        lstWorkLocationMaster = [select id, Name,
            GP_Oracle_Id__c,
            GP_Legal_Entity_Code__c,
            GP_Legal_Entity_Name__c,
            GP_Country__c
            from GP_Work_Location__c
            where GP_Oracle_Id__c in: setOfWorkLocationCode
        ];
    }

    private void setMapOfWorkLocationCodeToWorkLocationMaster() {
        mapOfWorkLocationCodeToWorkLocationMaster = new Map < String, GP_Work_Location__c > ();

        for (GP_Work_Location__c workLocationMaster: lstWorkLocationMaster) {
            mapOfWorkLocationCodeToWorkLocationMaster.put(workLocationMaster.GP_Oracle_Id__c, workLocationMaster);
        }
    }

    public GPAuraResponse getRelatedDataForWorkLocation(Id workLocationId) {
        GP_Work_Location__c workLocation;

        try {
            workLocation = [SELECT Id,
                GP_Legal_Entity_Code__c,
                GP_Legal_Entity_Name__c,
                GP_Country__c,
                GP_COE_Code__c,
                GP_COE_Name__c
                FROM GP_Work_Location__c
                where Id =: workLocationId
            ];
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(workLocation));
    }
}