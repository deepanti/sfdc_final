@RestResource(urlMapping='/EmployeeHRMS/*')

global with sharing class GPEmployeeHRMSSync {
    
    @HttpPost
    global static void doPost() {
        RestRequest req = RestContext.request;
        EmployeeMasterHRMSAPI__c objReqRes=new 	EmployeeMasterHRMSAPI__c();
        objReqRes.RequestType__c='EmployeeHRMS';
        
        List<GPEmployeeICSResponse> responseList=new List<GPEmployeeICSResponse>(); 
        try{
            if(!String.isBlank(req.requestBody.toString()) && req.requestBody.toString() !='""'){
                Map<String,Object> employeeHRMSDataMap=(Map<String,Object>)JSON.deserializeUntyped(req.requestBody.toString());
                List<object> empHrmLst =(List<object>) employeeHRMSDataMap.get('GP_Employee_HR_History__c');
                system.debug('==size==='+empHrmLst.size());
                List<GP_Employee_HR_History__c> lstEmpHR=new List<GP_Employee_HR_History__c>();                
                List<GP_Employee_Master__c> empMasterLst=new List<GP_Employee_Master__c>();
                Set<String> empMasterPersonIdSet=new Set<String>();
                for(integer j=0; j< empHrmLst.size();j++)
                {
                    map<String,Object> k = (map<String,Object>) empHrmLst[j];
                    empMasterPersonIdSet.add((String)k.get('GP_Employee_Person_Id__c'));
                }
                empMasterLst = [Select Id, GP_Person_ID__c from GP_Employee_Master__c where GP_Person_ID__c in:empMasterPersonIdSet];
                Map<String,Id> mapOfEmployeePerId=new Map<String,Id>();
                for(GP_Employee_Master__c empMst : empMasterLst )
                {
                    mapOfEmployeePerId.put(empMst.GP_Person_ID__c,empMst.Id);
                }
                for(integer i=0; i< empHrmLst.size();i++)
                {
                    map<String,Object> m = (map<String,Object>) empHrmLst[i];
                    try{
                        GP_Employee_HR_History__c  gpEmpHRObj=new GP_Employee_HR_History__c();           
                        gpEmpHRObj.GP_Asgn_Creation_Date__c=ConvertStringToDate((string)m.get('GP_Asgn_Creation_Date__c'));
                        gpEmpHRObj.GP_ASGN_EFFECTIVE_END__c=ConvertStringToDate((string)m.get('GP_ASGN_EFFECTIVE_END__c'));
                        gpEmpHRObj.GP_ASGN_EFFECTIVE_START__c=ConvertStringToDate((string)m.get('GP_ASGN_EFFECTIVE_START__c'));
                        gpEmpHRObj.GP_Asgn_Last_Update_Date__c=ConvertStringToDate((string)m.get('GP_Asgn_Last_Update_Date__c'));
                        gpEmpHRObj.GP_ASSIGNMENT_STATUS__c=(String)m.get('GP_ASSIGNMENT_STATUS__c');
                        gpEmpHRObj.GP_Band__c=(String)m.get('GP_Band__c');
                        gpEmpHRObj.GP_Batch_Number__c=(String)m.get('GP_Batch_Number__c'); 
                        gpEmpHRObj.GP_Business_Function__c=(String)m.get('GP_Business_Function__c');
                        gpEmpHRObj.GP_CC2_Code__c=(String)m.get('GP_CC2_Code__c');
                        gpEmpHRObj.GP_CC2_Desc__c=(String)m.get('GP_CC2_Desc__c');
                        gpEmpHRObj.GP_COE__c=(String)m.get('GP_COE__c');
                        gpEmpHRObj.GP_COE_Code__c=(String)m.get('GP_COE_Code__c');
                        gpEmpHRObj.GP_Date_Probation_End__c=ConvertStringToDate((String)m.get('GP_Date_Probation_End__c'));
                        gpEmpHRObj.GP_Designation__c=(String)m.get('GP_Designation__c');
                        gpEmpHRObj.GP_Employee_Business_Group__c=(String)m.get('GP_Employee_Business_Group__c');
                        gpEmpHRObj.GP_Employees__c=mapOfEmployeePerId.get((String)m.get('GP_Employee_Person_Id__c'));
                        gpEmpHRObj.GP_Employee_Number__c=(String)m.get('GP_Employee_Number__c');
                        gpEmpHRObj.GP_Employee_Person_Id__c=(String)m.get('GP_Employee_Person_Id__c');
                        gpEmpHRObj.GP_SDO_OracleId__c=(String)m.get('GP_SDO_OracleId__c');
                        gpEmpHRObj.GP_Home_Payroll_Location_ID__c=(String)m.get('GP_Home_Payroll_Location_ID__c'); 
                        gpEmpHRObj.GP_Horizontal_Service_Line__c=(String)m.get('GP_Horizontal_Service_Line__c');
                        gpEmpHRObj.GP_HORIZANTAL_SERVICE_LINE__c=(String)m.get('GP_HORIZANTAL_SERVICE_LINE__c');
                        gpEmpHRObj.GP_HRM_OHR_ID__c=(String)m.get('GP_HRM_OHR_ID__c');
                        gpEmpHRObj.GP_HRM_PERSON_ID__c=(String)m.get('GP_HRM_PERSON_ID__c');
                        gpEmpHRObj.GP_Is_Senior_Delivery__c=(String)m.get('GP_Is_Senior_Delivery__c');
                        gpEmpHRObj.GP_Legal_Entity_Code__c=(String)m.get('GP_Legal_Entity_Code__c');
                        gpEmpHRObj.GP_Legal_Entity_Name__c=(String)m.get('GP_Legal_Entity_Name__c');
                        gpEmpHRObj.GP_Location__c=(String)m.get('GP_Location__c');
                        gpEmpHRObj.GP_Organization__c=(String)m.get('GP_Organization__c');
                        gpEmpHRObj.GP_Org_Id__c=(String)m.get('GP_Org_Id__c');
                        gpEmpHRObj.GP_Payroll_Country__c=(String)m.get('GP_Payroll_Country__c');
                        gpEmpHRObj.GP_Payroll_Name__c=(String)m.get('GP_Payroll_Name__c');
                        gpEmpHRObj.GP_Period_Of_Placement_Start__c=ConvertStringToDate((String)m.get('GP_Period_Of_Placement_Start__c')); 
                        gpEmpHRObj.GP_Physical_Work_Location__c=(String)m.get('GP_Physical_Work_Location__c');
                        gpEmpHRObj.GP_Process__c=(String)m.get('GP_Process__c');
                        gpEmpHRObj.GP_Process_ID__c=(String)m.get('GP_Process_ID__c');
                        gpEmpHRObj.GP_Projected_Assignment_End__c=ConvertStringToDate((String)m.get('GP_Projected_Assignment_End__c'));
                        gpEmpHRObj.GP_SDO_Name__c=(String)m.get('GP_SDO_Name__c');
                        gpEmpHRObj.GP_SDO_Oracle_Id__c=(String)m.get('GP_SDO_Oracle_Id__c');
                        
                        gpEmpHRObj.GP_Service_Line__c=(String)m.get('GP_Service_Line__c');
                        gpEmpHRObj.GP_Supervisor_Ohrid__c=(String)m.get('GP_Supervisor_Ohrid__c');
                        gpEmpHRObj.GP_SUPERVISOR_PERSON_ID__c=(String)m.get('GP_SUPERVISOR_PERSON_ID__c');
                        gpEmpHRObj.GP_TSC_ORG_ID__c=(String)m.get('GP_TSC_ORG_ID__c');
                        gpEmpHRObj.GP_Work_Location_Code__c=(String)m.get('GP_Work_Location_Code__c');
                        
                        gpEmpHRObj.GP_Attribute1__c=(String)m.get('GP_Attribute1__c');
                        gpEmpHRObj.GP_Attribute2__c=(String)m.get('GP_Attribute2__c');
                        gpEmpHRObj.GP_Attribute3__c=(String)m.get('GP_Attribute3__c');
                        gpEmpHRObj.GP_Attribute4__c=(String)m.get('GP_Attribute4__c');
                        gpEmpHRObj.GP_Attribute5__c=(String)m.get('GP_Attribute5__c');
                        gpEmpHRObj.GP_RESOURCE_ID__c=(String)m.get('GP_RESOURCE_ID__c');
                        gpEmpHRObj.GP_Seq_Number__c=(String)m.get('GP_Seq_Number__c');
                        gpEmpHRObj.GP_Employment_Category__c=(String)m.get('GP_Employment_Category__c');
                        gpEmpHRObj.GP_ASSIGNMENT_ID__c=(string)m.get('GP_ASSIGNMENT_ID__c');
                        
                        lstEmpHR.add(gpEmpHRObj);
                    }
                    catch(exception ex){                
                        GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
                        objRes.Id=(String)m.get('GP_Attribute2__c');
                        objRes.Status='Fail';
                        objRes.Message=ex.getMessage();
                        objRes.SeqNum=(String)m.get('GP_Seq_Number__c');
                        responseList.add(objRes);
                    }
                } 
                Schema.SObjectField f = GP_Employee_HR_History__c.Fields.GP_Attribute2__c;
                Database.UpsertResult [] srList = Database.upsert(lstEmpHR, f, false); 
                for(Integer index = 0, size = srList.size(); index < size; index++) {
                    GPEmployeeICSResponse resp=new GPEmployeeICSResponse();
                    if(srList[index].isSuccess()) {                
                        //System.debug(lstEmpHR[index].GP_Attribute2__c  +' was created');     
                        resp.Id=lstEmpHR[index].GP_Attribute2__c;
                        resp.SeqNum=lstEmpHR[index].GP_Seq_Number__c;
                        resp.Status='Success';
                    }
                    else if (!srList.get(index).isSuccess()){
                        // DML operation failed
                        Database.Error error = srList.get(index).getErrors().get(0);
                        //String failedDML = error.getMessage();       
                        resp.Id=lstEmpHR[index].GP_Attribute2__c;
                        resp.Status='Fail';
                        resp.SeqNum=lstEmpHR[index].GP_Seq_Number__c;
                        resp.Message=error.getMessage();                 
                    }
                    responseList.add(resp);
                }
                
                String strJSON = '{ "Employee_Assignment_Response":';
                strJSON +=  JSON.serialize(responseList);   
                strJSON +='}';
                //objReqRes.response__c=  strJSON;
                objReqRes.Request_Data__c=String.valueOf(empHrmLst.size());
                upsert objReqRes; 
                AddAttachment('Employee_Assignment_Request',objReqRes.Id,req.requestBody.toString());
            	AddAttachment('Employee_Assignment_Response',objReqRes.Id,strJSON);
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(strJSON); 
            }
            else
            {
                GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
                objRes.Id='';
                objRes.Status='Fail';
                objRes.Message='Blank Request';
                objRes.SeqNum='';
                responseList.add(objRes);
                String strJSON = '{ "Employee_Assignment_Response":';
                strJSON +=  JSON.serialize(responseList);   
                strJSON +='}';
                //objReqRes.response__c=  strJSON;
                objReqRes.Request_Data__c='Blank Request';
                upsert objReqRes; 
                AddAttachment('Employee_Assignment_Request',objReqRes.Id,req.requestBody.toString());
                AddAttachment('Employee_Assignment_Response',objReqRes.Id,strJSON);
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.responseBody = Blob.valueOf(strJSON);
            }
        }
        catch(exception ex)
        {
                     
            GPEmployeeICSResponse objRes=new GPEmployeeICSResponse();
            objRes.Id='';
            objRes.Status='Fail';
            objRes.Message=ex.getMessage();
            objRes.SeqNum='';
            responseList.add(objRes);
            String strJSON = '{ "Employee_Assignment_Response":';
            strJSON +=  JSON.serialize(responseList);   
            strJSON +='}';
            //objReqRes.response__c=  strJSON;
            upsert objReqRes; 
            AddAttachment('Employee_Assignment_Request',objReqRes.Id,req.requestBody.toString());
            AddAttachment('Employee_Assignment_Response',objReqRes.Id,strJSON);
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueOf(strJSON);
        }
        
    }     
    global static void AddAttachment(String Name, String ParentId, String body)
    {
        Attachment attach = new Attachment();
        attach.contentType = 'text/plain';
        attach.name = Name+'_'+system.now(); 
        attach.parentId = ParentId;                
        attach.body = Blob.valueOf(body);
        insert attach;
    }
    global static Date ConvertStringToDate(String MapDate)
    {    if(!String.isBlank(MapDate))
        {
            MapDate = MapDate.replace('4712','4000');//check for infinity value
         }    
        return String.isBlank(MapDate) ? null :Date.valueOf(MapDate);        
    }
    
}