//================================================================================================================
//  Description: Test Class for GPBatchQualifiedDealsNotification
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          09-May-2018             	Ved prakash              Initial Version
//================================================================================================================
@isTest public class GPBatchQualifiedDealsNotificationTracker {
	static void setupCommonData() {
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        insert oppproobj;
        
        GP_Deal__c objDeal = GPCommonTracker.getDeal();
        objDeal.GP_Opportunity_Project__c = oppproobj.id;
        insert objDeal;
      	
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Person_ID__c = 'EMP-002';
        empObj.GP_Final_OHR__c = '303009945';
        empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.Today();
        empObj.GP_isActive__c = true;
        insert empObj; 
    }
    
    @isTest public static void testGPBatchCloseLostOpportunityProject() {
        setupCommonData();
        GPBatchQualifiedDealsNotification batcher = new GPBatchQualifiedDealsNotification();
        Id batchprocessid = Database.executeBatch(batcher);
    }
}