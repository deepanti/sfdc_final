//This is a tracker class for @GPCmpServiceProjectTemplate.
@isTest
public class GPCmpServiceProjectTemplateTracker {
    static GP_Project_Template__c projectTemplate;
    static GP_PROJECT__c project;
    public static GP_Deal__c  dealobj;
    
    //@buildDependencyData Method is used for getting data from the common tracker and inserting in the tracker class.
    @testSetup
    public static void buildDependencyData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Pinnacle_Master__c hslMaster = GPCommonTracker.GetpinnacleMaster();
        hslMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSL Master');
        insert hslMaster;
        
        GP_Pinnacle_Master__c vslMaster = GPCommonTracker.GetpinnacleMaster();
        vslMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'VSL Master');
        insert vslMaster;
        
        GP_Pinnacle_Master__c subDivisionMaster = GPCommonTracker.GetpinnacleMaster();
        subDivisionMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        insert subDivisionMaster;
        
        GP_Pinnacle_Master__c subDivisionMaster2 = GPCommonTracker.GetpinnacleMaster();
        subDivisionMaster2.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Sub Division');
        subDivisionMaster2.GP_Parent_Customer_L2__c = 'Hospitality Properties Trust';
        insert subDivisionMaster2;
        
        GP_Pinnacle_Master__c siloMaster = GPCommonTracker.GetpinnacleMaster();
        siloMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        insert siloMaster;
        
        GP_Pinnacle_Master__c siloMaster2 = GPCommonTracker.GetpinnacleMaster();
        siloMaster2.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Silo');
        siloMaster2.GP_Parent_Customer_L2__c = 'Hospitality Properties Trust';
        insert siloMaster2;
        
        GP_Pinnacle_Master__c portalMaster = GPCommonTracker.GetpinnacleMaster();
        portalMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Portal Master');
        insert portalMaster;
        
        GP_Pinnacle_Master__c projectOrganizationMaster = GPCommonTracker.GetpinnacleMaster();
        projectOrganizationMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Project Organization');
        insert projectOrganizationMaster;
        
        GP_Pinnacle_Master__c serviceSubCategoryMaster = GPCommonTracker.GetpinnacleMaster();
        serviceSubCategoryMaster.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Service Sub Category');
        serviceSubCategoryMaster.GP_Service_Line__c = '23456';
        insert serviceSubCategoryMaster;
        
        
        GP_Pinnacle_Master__c billingEntity = GPCommonTracker.GetpinnacleMaster();
        billingEntity.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'Billing Entity');
        insert billingEntity;
        
        
        GP_Pinnacle_Master__c hSNCategory = GPCommonTracker.GetpinnacleMaster();
        hSNCategory.recordTypeId = GPCommon.getRecordTypeId('GP_Pinnacle_Master__c', 'HSN Category');
        insert hSNCategory;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_Role_Category__c = 'pid creation';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        //objuserrole.Name = 'admin';
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Active__c = false;
        insert objprjtemp;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Business_Segment_L2__c = 'Hospitality Properties Trust';
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        insert timeshtentryObj ;
    }
    
    //The method is used to call all the methods of the tracker class.   
    @isTest
    public static void testProjectTemplateController() {
        fetchData();
        testgetProjectWithTemplateWithoutProjectId();
        testgetProjectWithTemplateWithProjectId();
        testgetFinalProjectTemplatewithtemplateId();
        testgetFinalProjectTemplatewithouttemplateId();
        testgetFinalProjectTemplateWithInvalidTemplateId();
        testgetDeltaProjectTemplateWithPrjTemplateId();
        testgetDeltaProjectTemplateWithInvalidTemplateId();
        testgetDeltaProjectTemplateWithActiveTemplateId();
        testCreateProjectTemplateWithString();
        testgetProjectWithTemplateWithProjectIdWithoutTemplate();
        //testCreateProjectTemplateWithoutString(); 
        testgetProjectWithTemplate();
        testgetProjectTemplate();
        
        Test.startTest();
        
        testcreateProjectTemplate();
        testcreateProjectTemplateWithTemplateId();
        testGetFieldSet();
        testGetFieldSetInvalid();
        testgetStageWiseFieldForProject();
        testgetPickList();
        testgetParentProjectInvoice();
        
        Test.stoptest();
    }
    
    public static void fetchData() {
        projectTemplate = [Select Id, GP_Stage_Wise_Field_Project__c from GP_Project_Template__c limit 1];
        project = [Select Id from GP_Project__c limit 1];

        dealobj = [select id, 
                   Name, 
                   GP_Business_Group_L1__c, 
                   GP_Business_Segment_L2__c, 
                   GP_Deal_Type__c, 
                   GP_Deal_Category__c 
                   from GP_Deal__c  
                   limit 1]; 
    }
    
    public static void testgetProjectWithTemplateWithoutProjectId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getProjectWithTemplate(null);
        System.assertEquals(false, returnedResponse.isSuccess);
        System.assertEquals('Invalid Context', returnedResponse.message);
    }
    
    public static void testgetProjectWithTemplateWithProjectId() {
        //project.GP_Project_Template__c = null;
        //update project;
        User u = [Select Id, Name 
                  from User 
                  Where username = 'newUserPranav@saasforce.com'
                  Limit 1];
        System.debug('u: ' + u);
        System.runAs(u) {
            GPAuraResponse returnedResponse = GPControllerProjectTemplate.getProjectWithTemplate(project.Id);
        }
    }
    
    public static void testgetProjectWithTemplateWithProjectIdWithoutTemplate() {
        project.GP_Project_Template__c = null;
        update project;
        User u = [Select Id, Name 
                  from User 
                  Where username = 'newUserPranav@saasforce.com'
                  Limit 1];
        System.debug('u: ' + u);
        System.runAs(u) {
            GPAuraResponse returnedResponse = GPControllerProjectTemplate.getProjectWithTemplate(project.Id);
        }
        
        //System.assertEquals(false, returnedResponse.isSuccess);
        //System.assertEquals('SUCCESS', returnedResponse.message);
    }
    
    public static void testgetFinalProjectTemplatewithtemplateId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getFinalProjectTemplate(projectTemplate.Id, null);
        returnedResponse = GPControllerProjectTemplate.getFinalProjectTemplate(projectTemplate.Id, JSON.serialize(dealobj));
        //System.assertEquals(True, returnedResponse.isSuccess);
        //System.assertEquals('SUCCESS', returnedResponse.message);
    }
    
    public static void testgetFinalProjectTemplatewithouttemplateId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getFinalProjectTemplate(null, null);
        //System.assertEquals(True, returnedResponse.isSuccess);
        //System.assertEquals('SUCCESS', returnedResponse.message);
    }
    
    public static void testgetFinalProjectTemplateWithInvalidTemplateId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getFinalProjectTemplate(project.Id, null);
        System.assertEquals(false, returnedResponse.isSuccess);
        
        returnedResponse = GPControllerProjectTemplate.getFinalProjectTemplate(project.Id, JSON.serialize(dealobj));
    }
    
    public static void testgetDeltaProjectTemplateWithPrjTemplateId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getDeltaProjectTemplate(ProjectTemplate.id);
        // System.assertEquals(true, returnedResponse.isSuccess);
        //System.assertEquals('SUCCESS', returnedResponse.message);
    }
    
    public static void testgetDeltaProjectTemplateWithActiveTemplateId() {
        ProjectTemplate.GP_Active__c = true;
        update ProjectTemplate;
        
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getDeltaProjectTemplate(ProjectTemplate.id);
        //System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    public static void testgetDeltaProjectTemplateWithInvalidTemplateId() {
        GPAuraResponse returnedResponse = GPControllerProjectTemplate.getDeltaProjectTemplate(project.id);
        System.assertEquals(false, returnedResponse.isSuccess);
        //System.assertEquals('SUCCESS', returnedResponse.message);
    }
    
    public static void gpTestProjectTemplateMethod() {
        GPCmpServiceProjectTemplate.getProjectTemplate(ProjectTemplate.Id);
        GPControllerProjectTemplate.getDeltaProjectTemplate(projectTemplate.Id);
    }
    
    Public Static Void  testCreateProjectTemplateWithString() {
        String serializedTemplateRecord =  (String)JSON.serialize(projectTemplate);
        GPControllerProjectTemplate.createProjectTemplate(serializedTemplateRecord);
    }
    
    
    Public Static Void  testmapOfFieldNameToListOfOptions() {
        GPControllerProjectTemplate.setMapOfFieldNameToOptions();
    }
    Public Static Void  testgetProjectWithTemplate() {
        GPCmpServiceProjectTemplate.getProjectWithTemplate(project.Id);
    }
    
    Public Static Void  testgetProjectTemplate() {
        GPCmpServiceProjectTemplate.getProjectTemplate(ProjectTemplate.Id);
    }
    
    Public Static Void  testcreateProjectTemplate() {
        String serializedTemplateRecord =  (String)JSON.serialize(new GPProjectTemplateWrapper());
        GPCmpServiceProjectTemplate.createProjectTemplate(serializedTemplateRecord);
    }
    
    
    Public Static Void  testcreateProjectTemplateWithTemplateId() {
        GPProjectTemplateWrapper templateWrapper = new GPProjectTemplateWrapper();
        templateWrapper.assesmentTemplateId = projectTemplate.Id;
        String serializedTemplateRecord =  (String)JSON.serialize(templateWrapper);
        GPCmpServiceProjectTemplate.createProjectTemplate(serializedTemplateRecord);
    }
    
    public static void testGetFieldSet() {
        List <String> returnedListOfFields = GPControllerProjectTemplate.getFieldSet('GP_All_Fields', 'GP_Project__c');
        System.assertNotEquals(0, returnedListOfFields.size());
    }
    
    public static void testGetFieldSetInvalid() {
        List <String> returnedListOfFields = GPControllerProjectTemplate.getFieldSet('GP_All_Field', 'GP_Project__c');
        System.assertEquals(null, returnedListOfFields);
    }

    public static void testgetStageWiseFieldForProject() {
        GPCmpServiceProjectTemplate.getStageWiseFieldForProject(project.Id);
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        update dealObj;
        GPCmpServiceProjectTemplate.getStageWiseFieldForProject(project.Id);
        GPCmpServiceProjectTemplate.getStageWiseFieldForProject(null);
    }
    
    static void testgetPickList() {
        String serializedProject = JSON.serialize(project);
        String serializedDeal = JSON.serialize(dealobj);
        GP_Project__c dummyProject = new GP_Project__c();
        String serializedDummyProject = JSON.serialize(dummyProject);
        
        GPControllerProjectTemplate.getPickList(serializedProject, serializedDeal);
        GPControllerProjectTemplate.getPickList(null, null);
        GPControllerProjectTemplate.getPickList(serializedDummyProject, serializedDeal);
    }
    
    static void testgetParentProjectInvoice() {
        String serializedProject = JSON.serialize(project);
        GPControllerProjectTemplate.getParentProjectInvoice(serializedProject);
        GPControllerProjectTemplate.getParentProjectInvoice(null);
    }
}