public class GENcApexScriptForProduct_Schedule
{
    public static void RunScript(List<Product_Schedule_Dummy__c> lstPro_Sch_Dummy)
    {
    
        List<RevenueSchedule__c> lstRevenueSch = new List<RevenueSchedule__c>();
        List<ProductSchedule__c> lstProductSchedule = new List<ProductSchedule__c>();
        List<OpportunityProduct__c> oOPL = new List<OpportunityProduct__c>();
        Map<String,Decimal> oMonthlyMap = new Map<String,Decimal>();
        Map<String,Decimal> MonthlySumOfOLI = new Map<String,Decimal>();
        Map<Id,Boolean> InConsistentDataFlagMap = new Map<Id,Boolean>(); 
        Set<String> s= new Set<String>(); 
        
        //setting the exception on profile getting current user profile in a list
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProfileName = PROFILE[0].Name;
        
        integer LastBucketHaveValue = 0;
        
        //commenting the exception
        //if (MyProfileName !='Genpact Super Admin')
        {
        for(Product_Schedule_Dummy__c oPSD : lstPro_Sch_Dummy)
        {
        s.add(oPSD.Product_Internal_ID__c);
            if(oPSD.Month_1_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'1',oPSD.Month_1_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_1_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_1_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 1;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'1',0);
            
            if(oPSD.Month_2_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'2',oPSD.Month_2_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_2_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_2_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 2;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'2',0);
            
            if(oPSD.Month_3_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'3',oPSD.Month_3_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_3_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_3_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 3;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'3',0);
            
            if(oPSD.Month_4_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'4',oPSD.Month_4_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_4_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_4_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 4;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'4',0);             
            
            if(oPSD.Month_5_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'5',oPSD.Month_5_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_5_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_5_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 5;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'5',0);                 
            
            if(oPSD.Month_6_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'6',oPSD.Month_6_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_6_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_6_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 6;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'6',0);                 
            
            if(oPSD.Month_7_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'7',oPSD.Month_7_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_7_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_7_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 7;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'7',0);                 
            
            if(oPSD.Month_8_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'8',oPSD.Month_8_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_8_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_8_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 8;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'8',0);                 
            
            if(oPSD.Month_9_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'9',oPSD.Month_9_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_9_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_9_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 9;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'9',0);                 
            
            if(oPSD.Month_10_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'10',oPSD.Month_10_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_10_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_10_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 10;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'10',0);                
            
            if(oPSD.Month_11_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'11',oPSD.Month_11_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_11_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_11_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 11;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'11',0);                
           
            if(oPSD.Month_12_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'12',oPSD.Month_12_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_12_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_12_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 12;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'12',0);            
            
            if(oPSD.Month_13_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'13',oPSD.Month_13_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_13_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_13_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 13;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'13',0);                
            
            if(oPSD.Month_14_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'14',oPSD.Month_14_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_14_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_14_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 14;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'14',0);
            
            if(oPSD.Month_15_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'15',oPSD.Month_15_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_15_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_15_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 15;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'15',0);
            
            if(oPSD.Month_16_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'16',oPSD.Month_16_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_16_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_16_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 16;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'16',0);
            
            if(oPSD.Month_17_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'17',oPSD.Month_17_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_17_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_17_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 17;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'17',0);                
            
            if(oPSD.Month_18_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'18',oPSD.Month_18_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_18_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_18_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 18;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'18',0);
            
            if(oPSD.Month_19_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'19',oPSD.Month_19_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_19_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_19_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 19;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'19',0);                
            
            if(oPSD.Month_20_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'20',oPSD.Month_20_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_20_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_20_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 20;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'20',0);    
            
            if(oPSD.Month_21_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'21',oPSD.Month_21_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_21_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_21_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                } 
                LastBucketHaveValue = 21;           
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'21',0);
            
            if(oPSD.Month_22_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'22',oPSD.Month_22_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_22_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_22_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 22;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'22',0);
            
            if(oPSD.Month_23_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'23',oPSD.Month_23_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_23_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_23_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 23;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'23',0);
            
            if(oPSD.Month_241_local__c!=null)
            {
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'24',oPSD.Month_241_local__c);
                if(MonthlySumOfOLI.containskey(oPSD.Product_Internal_ID__c))
                {
                    Decimal TotalTCVInAllMonths = MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c);
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_241_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                else
                {
                    Decimal TotalTCVInAllMonths = 0.0;
                    TotalTCVInAllMonths = TotalTCVInAllMonths + oPSD.Month_241_local__c;
                    MonthlySumOfOLI.put(oPSD.Product_Internal_ID__c,TotalTCVInAllMonths);
                }
                LastBucketHaveValue = 24;
            }
            else
                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+'24',0);
          
            InConsistentDataFlagMap.put(oPSD.Product_Internal_ID__c,false);
            if((oPSD.Data_From__c == 'NS' || oPSD.Data_From__c == 'CMIT') && LastBucketHaveValue < 24)
            {
                if(oPSD.Contract_Term__c < LastBucketHaveValue)
                {
                        InConsistentDataFlagMap.put(oPSD.Product_Internal_ID__c,true);
                        for(integer s2 = integer.valueof(oPSD.Contract_Term__c)+1 ;s2<=LastBucketHaveValue;s2++)
                        {
                            oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+String.valueof(s2),0);
                        }
                }
                else
                {
                    if(oPSD.TCV__c > MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c))
                    {
                        integer Diffrence = 0;
                        integer limitVal = 0;
                        if(oPSD.Contract_Term__c>24)
                        {
                            Diffrence = 24  - LastBucketHaveValue;
                            limitVal = 24;
                        }
                        else
                        {
                            Diffrence = integer.valueof(oPSD.Contract_Term__c) - LastBucketHaveValue;
                            limitVal = integer.valueof(oPSD.Contract_Term__c);
                        }
                        if(Diffrence != 0)
                        {
                            decimal RemaningAmount = (oPSD.TCV__c  - MonthlySumOfOLI.get(oPSD.Product_Internal_ID__c))/Diffrence;
                            for(integer k = LastBucketHaveValue+1; k <=limitVal;k++)
                            {
                                String RuningCounter =  string.valueof(k);
                                oMonthlyMap.put(oPSD.Product_Internal_ID__c.substring(0,15)+RuningCounter,RemaningAmount);
                            }
                        }
                    }
                }
            }
          
        
          
            if(oPSD.Contract_Term__c != null && oPSD.Contract_Term__c <=12 && oPSD.Contract_Term__c > 0 && oPSD.Revenue_start_Date__c!=null)
            {
                RevenueSchedule__c  oRevenueSchedule = new RevenueSchedule__c();    
                
                integer intAmountSum = 0;
                oRevenueSchedule.OpportunityProduct__c =oPSD.Product_Internal_ID__c;
                oRevenueSchedule.InstallmentPeriod__c = 'Monthly';
                oRevenueSchedule.RollingYear__c = 1;
                oRevenueSchedule.NumberOfInstallments__c = oPSD.Contract_Term__c;
                oRevenueSchedule.RollingYearRevenueLocal__c=1;
                oRevenueSchedule.RemainderInstallment__c=0;
                
                
                oRevenueSchedule.StartDate__c = oPSD.Revenue_start_Date__c;
                
                lstRevenueSch.add(oRevenueSchedule);
            }
          if(oPSD.Contract_Term__c != null && oPSD.Contract_Term__c >12 && oPSD.Revenue_start_Date__c!=null)
          {
                RevenueSchedule__c oRevenueSchedule1 = new RevenueSchedule__c();    
                oRevenueSchedule1.OpportunityProduct__c =oPSD.Product_Internal_ID__c;
                oRevenueSchedule1.InstallmentPeriod__c = 'Monthly';
                oRevenueSchedule1.NumberOfInstallments__c = 12;
                oRevenueSchedule1.RemainderInstallment__c=oPSD.Contract_Term__c-12;
                oRevenueSchedule1.RollingYear__c = 1;
                oRevenueSchedule1.RollingYearRevenueLocal__c=1;
                oRevenueSchedule1.StartDate__c = oPSD.Revenue_start_Date__c;
                
                
                lstRevenueSch.add(oRevenueSchedule1);
                RevenueSchedule__c oRevenueSchedule2 = new RevenueSchedule__c();    
                oRevenueSchedule2.OpportunityProduct__c =oPSD.Product_Internal_ID__c;
                oRevenueSchedule2.InstallmentPeriod__c = 'Monthly';
                oRevenueSchedule2.NumberOfInstallments__c = oPSD.Contract_Term__c-12;
                if((oPSD.Contract_Term__c-12) >12)
                oRevenueSchedule2.NumberOfInstallments__c=12;
                oRevenueSchedule2.RollingYearRevenueLocal__c=1;
                
                oRevenueSchedule2.RemainderInstallment__c=0;
                oRevenueSchedule2.RollingYear__c = 2;
                oRevenueSchedule2.StartDate__c = oPSD.Revenue_start_Date__c.addMonths(12);
            
                lstRevenueSch.add(oRevenueSchedule2);
            
          }
        }
     system.debug('@!~#@#!@@#!#!@#!@'+oMonthlyMap);
      if(lstRevenueSch != null && !lstRevenueSch.isEmpty())
      {
            insert lstRevenueSch;
      }
      for(RevenueSchedule__c oRevenueSchedule : lstRevenueSch)
      { 
          integer month=oRevenueSchedule.StartDate__c.month();
          for(integer i=0;i<oRevenueSchedule.NumberOfInstallments__c;i++)
          {
            integer cnt;
            ProductSchedule__c oProductSchedule = new ProductSchedule__c();
            if(oRevenueSchedule.RollingYear__c==1)
            {
              //  oProductSchedule.Next_Year__c = false;
              //  oProductSchedule.Current_Year__c = true;
                cnt=i+1;
            }
            if(oRevenueSchedule.RollingYear__c==2)
            {
              //  oProductSchedule.Next_Year__c = true;
             //   oProductSchedule.Current_Year__c = false;
                cnt=i+13;
            }
            oProductSchedule.RevenueScheduleId__c = oRevenueSchedule.Id;
            oProductSchedule.ScheduleDate__c = oRevenueSchedule.StartDate__c.addMonths(i);
            system.debug('this is with map:'+oMonthlyMap.get(string.valueof(oRevenueSchedule.OpportunityProduct__c).substring(0,15) +string.valueof(cnt)));
            system.debug('this is without map:'+string.valueof(oRevenueSchedule.OpportunityProduct__c).substring(0,15) +string.valueof(cnt));
            oProductSchedule.RevenueLocal__c = oMonthlyMap.get(string.valueof(oRevenueSchedule.OpportunityProduct__c).substring(0,15) +string.valueof(cnt));
            oProductSchedule.OpportunityProductId__c =oRevenueSchedule.OpportunityProduct__c;
            

            lstProductSchedule.add(oProductSchedule);
          }
           
      }
      
      if(lstProductSchedule !=null && !lstProductSchedule.isEmpty())
      {
         insert lstProductSchedule;
      }
      
      MAP<String,Decimal> oRollingYearMap = new MAP<String,Decimal>();
      for(AggregateResult a:[SELECT RevenueScheduleId__c,SUM(RevenueLocal__c)r from ProductSchedule__c where OpportunityProductId__c in:s Group BY RevenueScheduleId__c])
      {
        oRollingYearMap.put(string.valueof(a.get('RevenueScheduleId__c')),(Decimal)(a.get('r')));
      }
      for(integer t=0;t<lstRevenueSch.size();t++)
      {
      if(oRollingYearMap.get(lstRevenueSch[t].id)!=null)
      lstRevenueSch[t].RollingYearRevenueLocal__c=oRollingYearMap.get(lstRevenueSch[t].id);
      }
      update lstRevenueSch;
      
      MAP<String,Decimal> oCYRMap = new MAP<String,Decimal>();
      MAP<String,Decimal> oNYRMap = new MAP<String,Decimal>();
      List<RevenueSchedule__c> rsu = new List<RevenueSchedule__c>();
      List<ProductSchedule__c> psu = new List<ProductSchedule__c>();
      List<OpportunityProduct__c> lst =[Select Id ,CurrentYearRevenueLocal__c,NextYearRevenueLocal__c,HasSchedule__c,TotalContractValue__c,TCVLocal__c,ExchangeRate__c,HasRevenueSchedule__c,HasQuantitySchedule__c,(Select Id ,ScheduleDate__c,RevenueLocal__c from ProductSchedules__r) from OpportunityProduct__c where id in:s];
      List<RevenueSchedule__c> rslst=[Select Id ,RollingYearRevenueLocal__c,RollingYearRevenue__c from RevenueSchedule__c where OpportunityProduct__c in:s];
       set<id> IdAlradyAdded = new set<id>();
       set<id> IdAlradyAddedPS = new set<id>();

      for(OpportunityProduct__c o : lst)
      {
        Decimal intCYR = 0;
        Decimal intNYR = 0;
        Decimal Revenue=0.0;
        Decimal RollingYearRevenue=0.0;
        for(ProductSchedule__c obj : o.ProductSchedules__r)
        {
            if(!(IdAlradyAddedPS.contains(obj.id)))
            {
                IdAlradyAddedPS.add(obj.id);
                if(System.today().year() == obj.ScheduleDate__c.Year() && obj.RevenueLocal__c!=null)
                {
                    intCYR +=obj.RevenueLocal__c;
                }
                if(System.today().year()+1 == obj.ScheduleDate__c.Year() && obj.RevenueLocal__c!=null)
                {
                    intNYR +=obj.RevenueLocal__c;
                }
           if(o.ExchangeRate__c !=0)
            Revenue =obj.RevenueLocal__c/o.ExchangeRate__c;
            
            obj.Revenue__c=Revenue;
            psu.add(obj);
            //system.debug('Ex Rate'+lst[0].ExchangeRate__c+''+'revenue@#'+obj.Revenue__c);
           }
        }
       for(RevenueSchedule__c rsObj : rslst)
        {
            if(!(IdAlradyAdded.contains(rsObj.id)))
            {
                IdAlradyAdded.add(rsObj.id);
                RollingYearRevenue=rsObj.RollingYearRevenueLocal__c/lst[0].ExchangeRate__c;
                rsObj.RollingYearRevenue__c=RollingYearRevenue;
                rsu.add(rsObj);
            }
        }
        
        if(o.TotalContractValue__c!=null && o.TotalContractValue__c!= 0.00)
        o.ExchangeRate__c=o.TCVLocal__c/o.TotalContractValue__c;
        
        o.CurrentYearRevenueLocal__c =intCYR ;
        o.NextYearRevenueLocal__c =intNYR ;
        o.HasQuantitySchedule__c=false;
        o.HasRevenueSchedule__c=true;
        o.HasSchedule__c=true;
        o.InConsistentDataFlag__c = InConsistentDataFlagMap.get(o.id);
        oOPL.add(o);
      }
      if(oOPL !=null && !oOPL.isempty())
      {
        update rsu;
        update psu;
        update oOPL;
      }
      }
    }
}