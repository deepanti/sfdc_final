public class RedirectToWLRFromCon {
    
    public Contract ContractObject;
    Public Opportunity oppty;
    
    public RedirectToWLRFromCon(ApexPages.StandardController controller) 
    {
        
    }   
    
    public pageReference Redirect() {        
    
    try{
        String ErrorStr='';
        String CONID=apexpages.currentpage().getparameters().get('id');
        ContractObject=[select id,name,Opportunity_tagged__c,Number_of_Attachments__c ,Survey_Filled__c,Survey_Initiated_contract__c,Deal_outcome__c from Contract where id = :CONID];
        oppty=[Select id,name,Survey_Initiated__c,Getfeedback_Survey_Name__c from opportunity where id= :ContractObject.Opportunity_tagged__c];
        
         ErrorStr =  '<a href="../' + CONID + '" target="_top"><font color="red">'+ 'Click here to go back.' +'</font></a>';
         system.debug('ContractObject========test==='+ContractObject);
        if(ContractObject.Number_of_Attachments__c  == 0)
                   { 
                       ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please upload the contract under "Attachments" section for ICON team to validate' +' '+ ErrorStr ));
                       //ContractObject.Number_of_Attachments__c .adderror('ERROR:Attachmnet is not uploaded ',false);
                          return null;
                   }
        
        else if(ContractObject.Survey_Filled__c==false && ContractObject.Number_of_Attachments__c  >0)
            {   
                ContractObject.Survey_Initiated_contract__c = true;
                update ContractObject;
                
                                            
                //PageReference pageRef = new PageReference('/apex/WLR?oppid=' + ContractObject.Opportunity_tagged__c + '&conid='+ContractObject.id );
                
                PageReference pageRef = new PageReference('/apex/Win_Loss_Dropped_Surveys?oppid=' + ContractObject.Opportunity_tagged__c + '&conid='+ContractObject.id);
                pageRef.setRedirect(true);
                return pageRef; 
                                                     
                
            }
            
        else if(ContractObject.Survey_Filled__c==true && ContractObject.Deal_outcome__c=='Signed')
        {
            
                    
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            //req.setComments('Submitted for approval. Please approve.');
            req.setObjectId(CONID);
            // submit the approval request for processing
            Approval.ProcessResult result = Approval.process(req);
             
             if(result.isSuccess())
                {
                    PageReference pageRef = new PageReference('/' + ContractObject.id);
                    pageRef.setRedirect(true);
                    return pageRef; 
                }
                else
                    return null;    
            
        
        
        }
        
        else
            {
                ContractObject.Deal_outcome__c.adderror('ERROR:Deal outcome is not Signed.',false);
                          return null; 
            }
    } 
    
    Catch(Exception e)
    {
    
        if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please update mandatory information at opportunity first.'));
         else 
             ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
             
             return null;

    }    
  
    }
    
    
}