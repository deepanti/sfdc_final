// This queueable class is called for GPBatchCreateDeals class to handle the deal update.
public class GPQueueableCallBatchCloneSubmitProject implements Queueable {
    
    Map<Id, GP_Deal__c> mapOfProjectIdToDeal;
    Set<id> setOfProjectIdToClone;
    
    public GPQueueableCallBatchCloneSubmitProject(Set<id> setOfProjectIdToClone, Map<Id, GP_Deal__c> mapOfProjectIdToDeal) {
        this.mapOfProjectIdToDeal = mapOfProjectIdToDeal;
        this.setOfProjectIdToClone = setOfProjectIdToClone;
    }
    
	public void execute(QueueableContext context) {
         Database.executeBatch(new GPBatchCloneSubmitProject(setOfProjectIdToClone, mapOfProjectIdToDeal), 1);
    }
    
} 