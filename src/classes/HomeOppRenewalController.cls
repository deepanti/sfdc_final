public class HomeOppRenewalController {
    class OpportunityWrapper
    {
        @AuraEnabled public Date contractRenewalDate;//contract renewal date 
        @AuraEnabled public Opportunity opportunityDetails;//opportunity Id, other fields + list of products
    }
    @AuraEnabled
    //Function to fetch all the contract opportunity for Renewal
    public static List<OpportunityWrapper> fetchListOpportunity(Integer renewalDuration)
    {
        List<OpportunityWrapper>opportunityWrapperList=new List<OpportunityWrapper>();
        //getting Contract Opportunity pending for Renewal
        //parent change
     
        Date renewal=system.today().addMonths(renewalDuration);
        
        Map<Id,Opportunity> opportunityChildMap= new Map<Id, Opportunity>([select parent__c from opportunity where parent__c!=null]);
        set<Id>opportunityChildSet=new Set<Id>();
        for(Opportunity oppRec:opportunityChildMap.values())
        {
            opportunityChildSet.add(oppRec.parent__c);
        }
        Map<Id,Opportunity> opportunityMap= new Map<Id, Opportunity>
            ([select id,Account.Name,Name,Opportunity_ID__c,(SELECT PricebookEntry.Name,PricebookEntry.Product2Id FROM OpportunityLineItems)
              FROM Opportunity  where   (Account.OwnerId=:userinfo.getUserId() or OwnerId=:userinfo.getUserId()) and  Annuity_Project__c='Annuity' and Type_Of_Opportunity__c='Non Ts' and Id in (select Opportunity_tagged__c from Contract where End_Date_of_Contract__c<=:renewal and End_Date_of_Contract__c>=Today) and id not in :opportunityChildSet]);
        
        for(Contract conRecord:[select Opportunity_tagged__c,End_Date_of_Contract__c from Contract 
                                where  End_Date_of_Contract__c<=:renewal and End_Date_of_Contract__c>=Today
                                and (Opportunity_tagged__r.Account.OwnerId=:userinfo.getUserId() or Opportunity_tagged__r.OwnerId=:userinfo.getUserId()) and Opportunity_tagged__r.Annuity_Project__c='Annuity' and Opportunity_tagged__r.Type_Of_Opportunity__c='Non Ts' and status='Activated' and Opportunity_tagged__c in :opportunityMap.keyset()])
        {
           
            OpportunityWrapper oppWrapRecord=new OpportunityWrapper();
            oppWrapRecord.contractRenewalDate=conRecord.End_Date_of_Contract__c;
            oppWrapRecord.opportunityDetails = opportunityMap.get(conRecord.Opportunity_tagged__c);
            opportunityWrapperList.add(oppWrapRecord);
        }
       
        return opportunityWrapperList; 
    }
    @AuraEnabled
    //Function which sets Hide For Renewal 
    public static void updateHideForRenewal(string oppid) 
    {
        /*Opportunity opp=[select Hide_For_Renewal__c from Opportunity where id=:oppid];
        opp.Hide_For_Renewal__c=true;//setting True so that opportunity cannot be fetched again
        update opp;*/
    }
    
  
    @AuraEnabled
    public static Id createDeepCloneOpportunity(Id oppId,boolean isRenewable,String stage)
    {
         return HomeOppRenewalController.deepCloneOpportunity(oppId,isRenewable,stage);
    }
    private static Id deepCloneOpportunity(Id oppId,boolean isRenewable,String stage)
    {
         Opportunity opportunityRecord=[select parent__c , AccountId,Annuity_Project__c,Target_Source__c,Opportunity_Source__c,Amount,Deal_Type__c,
                                      Closest_Competitor_1__c,Closest_Competitor__c,CloseDate,CreatedDate,Name,Sales_Region__c,Sales_country__c,PriceBook2Id,Contact1__c,
                                      Opportunity_Origination__c,NextStep,Description,Current_challenges_client_facing__c,Role__c,Analyst_Name__c,Summary_of_opportunity__c,
                                        End_objective_the_client_trying_to_meet__c,Win_Theme__c, Deal_Administrator__c, Advisor_Firm__c, Advisor_Name__c,
			                          RFXStatus__c,Margin__c,Proposed_pricing_model__c,RPA_Platform__c,GCI_Coach__c,GCI_Coach_If_Others__c,
                                      Pricer_Name__c,Contract_type__c from opportunity where Id=:oppId];
        System.debug(opportunityRecord);
        Opportunity RenewalOpportunity = opportunityRecord.clone();
        if(isRenewable)
        {
            string newName = opportunityRecord.Name+'_RENEWAL_'+opportunityRecord.CloseDate.format();
            RenewalOpportunity.Name = newName;
            RenewalOpportunity.Opportunity_Source__c = 'Renewal';
            RenewalOpportunity.parent__c=opportunityRecord.Id;//change
            
        }
        else
        {
            RenewalOpportunity.Name = opportunityRecord.Name+'_COPY';
        }
        RenewalOpportunity.StageName = stage;
        RenewalOpportunity.LatestTabId__c = '1';
        if(RenewalOpportunity.CloseDate < date.today())
        {
           
            RenewalOpportunity.CloseDate=date.today().addDays(date.newinstance(opportunityRecord.CreatedDate.year(), opportunityRecord.CreatedDate.month(), opportunityRecord.CreatedDate.day()).daysBetween(RenewalOpportunity.CloseDate));
            
        }
        if(isRenewable)
        {
            RenewalOpportunity.CloseDate=date.today().addDays(180);
        }
                
        insert RenewalOpportunity;
        List<Contact_Role_Rv__c> oppContacts = new List<Contact_Role_Rv__c>();
        for(Contact_Role_Rv__c contact : [Select Id,Contact__c,IsPrimary__c,Opportunity__c,Role__c
                                        from Contact_Role_Rv__c where Opportunity__c =:  oppId])
        {
            if(!contact.IsPrimary__c)
            {
                oppContacts.add(new Contact_Role_Rv__c(
                    Contact__c = contact.contact__c,
                    IsPrimary__c = contact.IsPrimary__c,
                    Opportunity__c = RenewalOpportunity.Id,
                    Role__c = contact.Role__c
                ));
            }            
        }
        if(oppContacts.size() > 0)
        {
            insert oppContacts;    
        }        
        List<OpportunityLineItem> oppItemsList = new List<OpportunityLineItem>();
        for(OpportunityLineItem oli : [Select Id,TCV__c,FTE__c,Type_of_Deal__c,Contract_Term__c,UnitPrice,Sub_Delivering_Organisation__c,
                                       Revenue_Start_Date__c,QSRM_Status__c,Product_BD_Rep_Status__c,Product_BD_Rep__c,Product2Id,
                                       Local_Currency__c,Delivery_Location__c,Delivering_Organisation__c from OpportunityLineItem
                                       where OpportunityId =: oppId])
        {
            oppItemsList.add(oli.clone());
        }
        if(oppItemsList.size() > 0)
        {
            Opportunity oppDetails = [Select CloseDate from Opportunity where Id =: RenewalOpportunity.id];
            System.debug('oppDetails ------------->'+oppDetails);
            for(OpportunityLineItem lineItemRec : oppItemsList)
            {
                lineItemRec.OpportunityId = RenewalOpportunity.Id;
                if(isRenewable)
                {
                    lineItemRec.Type_of_Deal__c = 'Renewal';
                    system.debug(lineItemRec.Type_of_Deal__c + 'type of Deal k2');
                    lineItemRec.Revenue_Start_Date__c = oppDetails.CloseDate.addDays(1);
                    system.debug(lineItemRec.Revenue_Start_Date__c + 'Revenue_Start_Date__c Date');
                    
                }
            }
            system.debug('----------->'+oppItemsList);
            insert oppItemsList;
        }
        return RenewalOpportunity.Id;        
    }
}