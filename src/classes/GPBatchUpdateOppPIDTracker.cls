// BPH Change
@isTest
public class GPBatchUpdateOppPIDTracker {
    @testSetup 
    static void setupCommonData() {
        Business_Segments__c objBS = GPCommonTracker.getBS();
        objBS.For__c = 'GE';
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        accobj.Business_Group__c = 'GE';
        insert accobj;
        
        User objuser = GPCommonTracker.getUser();
        
        Profile p = [select id, name from profile where name = 'Genpact Super Admin'
                     limit 1
                    ];
        objuser.ProfileID = p.id;
        insert objuser;
        
        system.runAS(objuser) {
            Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
            oppobj.Name = 'Test Opportunity';
            oppobj.StageName = 'Prediscover';
            oppobj.AccountId = accobj.id;
            oppobj.Probability = 1;
            oppobj.Actual_Close_Date__c = System.Today().adddays(-10);            
            oppobj.Sales_Leader__c = objuser.id;
            insert oppobj;
            
            oppobj.Bypass_Validations__c = true;
            update oppobj;
            
            Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=: oppobj.id];
            
            GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
            oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
            oppproobj.GP_Probability__c = 70;
            oppproobj.GP_Project_Start_Date__c = system.today();
            insert oppproobj;
            system.debug('oppproobj::'+oppproobj);
        }
    }
    
    @isTest
    static void testBusinessSegmentNameUpdate() {
        Opportunity objOpp = [Select id, AccountId, Opportunity_ID__c from Opportunity where name = 'Test Opportunity' limit 1];        
        
        Account accobj = [Select id, Business_Group__c, Business_Segment__c,
                          Business_Segment__r.Name, Sub_Business__c,
                          Sub_Business__r.Name, Industry_Vertical__c,
                          Sub_Industry_Vertical__c from Account where id =: objOpp.AccountId limit 1];
        
        accobj.Industry_Vertical__c = 'CPG';
        accobj.Sub_Industry_Vertical__c = 'CPG';
        update accobj;
        
        Business_Segments__c objBS = [Select id, Name from Business_Segments__c where id =: accobj.Business_Segment__c limit 1];
        objBS.Name = 'Test 12-05-2020';
        update objBS;
        
        Sub_Business__c objSB = [Select id, Name from Sub_Business__c where id =: accobj.Sub_Business__c limit 1];
        objSB.Name = 'Test 12-05-2020';
        update objSB;
        
        Test.startTest();        
        Id batchprocessid = Database.executeBatch(new GPBatchUpdateOppPIDOnAccountChange(), 1);
        Test.stopTest();
    }
}