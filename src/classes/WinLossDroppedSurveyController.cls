public class WinLossDroppedSurveyController 
{
    @auraEnabled
    public static List<String> getDealOutcome()
    {
        List<String> Opp_DealOutCome_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.W_L_D__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_DealOutCome_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_DealOutCome_list======'+Opp_DealOutCome_list);
        return Opp_DealOutCome_list;
    }
    
    @auraEnabled
    public static List<String> getDealOutcome_Signed()
    {
        List<String> Opp_DealOutCome_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.W_L_D_Signed__c.getDescribe(); // No Signed is there
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_DealOutCome_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_DealOutCome_list======'+Opp_DealOutCome_list);
        return Opp_DealOutCome_list;
    }

    @auraEnabled
    public static OpportunitySurveyListWrapper getOpportunityData(ID opportunityID)  
    {
        try
        {           
            System.debug('opportunityID=========='+opportunityID);            
            List<Opportunity> opportunity_list = [SELECT id, name, StageName, Opportunity_Source__c, Target_Source__c, Annuity_Project__c, Deal_Type__c,W_L_D__c,TCV1__c, Nature_Of_Work_OLI__c, key_win_reasons_and_any_key_learnings__c, Reason_1__c, Reason_2__c, Reason_3__c,Incumbent__c, Incumbent_Others__c, Closest_Competitor__c, Genpact_s_proposed_price_differ__c, Genpact_s_proposed_price_differ_Percent__c, Closest_Competitor_Others__c, Closest_Competitor_1__c, Closest_Competitor_2__c, Competitor_strengths1__c, Competitor_weaknesses1__c, Competitor_strengths2__c, Competitor_weaknesses2__c, Genpact_Involvement_in_deal_with_prospec__c, interactions_did_we_have_with_the_prospe__c, If_advisor_was_involved__c, Loss_Deal_Stage__c,Deal_Winner__c, Deal_Winner_Others__c FROM Opportunity WHERE ID =:opportunityID];
            
            system.debug('opportunity_list==========='+opportunity_list);
            
            if(opportunity_list.size() > 0)
            {  
                OpportunitySurveyListWrapper wrapper = new OpportunitySurveyListWrapper();
                wrapper.OpportunityDataList  = opportunity_list; 
                System.debug('wrapper.OpportunityDataList======'+ wrapper.OpportunityDataList);                
                return wrapper;
            } 
        }
        catch(Exception e){
            System.debug('Error======'+e.getStackTraceString()+ '====='+e.getMessage());
        }
        return NULL;
    }
     
}