/*
    This class is used to cover all the cases of opportunity trigger  
    --------------------------------------------------------------------------------------
    Name                                Date                                Version                     
    --------------------------------------------------------------------------------------
    Jubin                             3/17/2016                               1.0   
    --------------------------------------------------------------------------------------
*/
@istest(seeAllData=False)
public class GW1_OpportunityTriggerHandlertest
{
    /*
        This method is used to cover the DSR creation functionality  
        --------------------------------------------------------------------------------------
        Name                                Date                                Version                     
        --------------------------------------------------------------------------------------
        Jubin                             3/17/2016                               1.0   
        --------------------------------------------------------------------------------------
    */
    static testMethod void ValidateDSRCreation()
    {
     
        
        GW1_CommonTracker.createTriggerCustomSetting();
        Account objAccount = GW1_CommonTracker.createAccount('Account1');
        Opportunity objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', false, false,false, objAccount.Id);
		GW1_CommonTracker.createTL();       
        GW1_CommonTracker.createOppProduct(objOpportunity,'Delivery');
		//GW1_CommonTracker.createOppProduct(objOpportunity,'Consulting');
		QSRM__c objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
		
		
		
        Test.startTest();
        objQSRM.Status__c = 'Approved';
        update objQSRM;
        Test.stopTest();
    }
    
    static testMethod void QSRMapprovedAfterDSrcreate()
    {
     
        
        GW1_CommonTracker.createTriggerCustomSetting();
        Account objAccount = GW1_CommonTracker.createAccount('Account1');
        Opportunity objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', true, false,false, objAccount.Id);
		GW1_CommonTracker.createTL();       
        GW1_CommonTracker.createOppProduct(objOpportunity,'Delivery');
		//GW1_CommonTracker.createOppProduct(objOpportunity,'Consulting');
		QSRM__c objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
		
		
		
        Test.startTest();
        objQSRM.Status__c = 'Approved';
        update objQSRM;
        Test.stopTest();
    }

    static void LoadTestData()
    {
        GW1_CommonTracker.createTriggerCustomSetting();
    }

	static testMethod void validateNoDeliveryProductCase()
	 {
	 //testing case where dsr is not created when we have only consulting product;
     
        GW1_CommonTracker.createTriggerCustomSetting();
        Account objAccount = GW1_CommonTracker.createAccount('Account1');
        Opportunity objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', false, false,false, objAccount.Id);
		GW1_CommonTracker.createTL();       
		GW1_CommonTracker.createOppProduct(objOpportunity,'Consulting');
		QSRM__c objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
		
		
		
          Test.startTest();
        objQSRM.Status__c = 'Approved';
        
		try 
		{	        
		update objQSRM;
		}
		catch (exception e)
		{
		/*
		System.DmlException: Update failed. First exception on row 0 with id a07p0000000RpqPAAS; 
		first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, 
		No delivery product found or consulting product(s) with opportunity value is below 150000: []
		*/
			System.debug(e);
		}
        Test.stopTest();
    }
}