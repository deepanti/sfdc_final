@isTest
public class GPServiceStageWiseFieldValidatorTracker {
	private static list<GP_Leadership_Master__c> listOfLeadershipMaster;
    private static list<GP_Project_Template__c> listOfProjectTemplate;
    private static list<GP_Project__c> listOfProject;
    
    private static GP_Project__c project;
    private static GP_Employee_Master__c employeeMaster;
    private static GP_Leadership_Master__c leadershipMaster;
    private static List<GP_Project_Leadership__c> listOfProjectLeadership;

    @testSetup
    public static void testGPServiceStageWiseFieldValidator() {
        //insert leadership master records.
        insertLeadershipMaster();
        //Insert project
        insertProject();
    }
    
    private static void insertLeadershipMaster() {
        listOfLeadershipMaster = new list<GP_Leadership_Master__c>();
        GP_Leadership_Master__c globalProjectManager = new GP_Leadership_Master__c();
        globalProjectManager.GP_Leadership_Role__c = 'Global Project Manager';
        globalProjectManager.RecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        globalProjectManager.GP_Mandatory_for_stage__c = 'Cost Charging';
        globalProjectManager.GP_PID_Category__c = 'Billable';
        
        listOfLeadershipMaster.add(globalProjectManager);
        insert listOfLeadershipMaster;
    }
    
    private static void insertProject() {
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        
        insert projectTemplate;
        GP_Icon_Master__c crnNumber = new GP_Icon_Master__c();
		insert crnNumber;
        
        listOfProject = new list<GP_Project__c>();
        GP_Project__c project = GPCommonTracker.getProject(dealObj, 'CMITS', projectTemplate, objuser, objrole);
        project.OwnerId = objuser.Id;
        project.GP_CRN_Number__c = crnNumber.Id;
        listOfProject.add(project);
        insert listOfProject;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
    }
    
    @isTest
    public static void validateStageWiseFields() {
        GPWrapFieldValue response; 
         
        String strQuery = 'select GP_CRN_Number__r.GP_Status__c,GP_deal__r.GP_No_Pricing_in_OMS__c,';
        Map<String, Schema.SObjectField> MapOfSchema = Schema.SObjectType.GP_Project__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ', RecordType.Name from GP_Project__c LIMIT 1';
        
        project = Database.query(strQuery);
        
        employeeMaster = [SELECT ID, GP_HIRE_Date__c, GP_ACTUAL_TERMINATION_Date__c From GP_Employee_Master__c LIMIT 1];
        
        GPServiceStageWiseFieldValidator stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, null);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        stageWiseFieldValidator.validateFields();
        
        staticResource stageWiseFieldResource = [SELECT id, Body, Name
                                                 FROM staticResource
                                                 WHERE Name = 'StageWiseField'
                                                ];
        
        String stageWiseField = stageWiseFieldResource.Body.toString();
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        stageWiseFieldValidator.validateFields();
        
        project.GP_Project_Long_Name__c = 'Test Project';
        project.GP_Start_Date__c = System.today();
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        response = stageWiseFieldValidator.validateFields();
    	
        project.GP_MOD__c = 'Email';
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        response = stageWiseFieldValidator.validateFields();
    	
        project.GP_SOW_Start_Date__c = System.today();
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        response = stageWiseFieldValidator.validateFields();
        
        project.GP_SOW_End_Date__c = System.today().addDays(10);
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        response = stageWiseFieldValidator.validateFields();
        
                
        project.RecordType.Name = 'Indirect PID'; 
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(new List<GP_Project_Leadership__c>());
        response = stageWiseFieldValidator.validateFields();

        createProjectLeadership();
        
        stageWiseFieldValidator = new GPServiceStageWiseFieldValidator(project, stageWiseField);
        stageWiseFieldValidator.setProjectLeadership(listOfProjectLeadership);
        response = stageWiseFieldValidator.validateFields();
        
        System.debug('response: ' + response);
    }
    
    
    private static void createProjectLeadership() {
        Date todayDate = System.today();
        listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        List<GP_Leadership_Master__c> listOfLeadershipMaster = new List<GP_Leadership_Master__c>();
        
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        listOfLeadershipMaster.add(leadershipMaster);
        
        leadershipMaster = GPCommonTracker.getLeadershipMaster('Account', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Account';
        
        listOfLeadershipMaster.add(leadershipMaster);

        insert listOfLeadershipMaster;
        
        for(GP_Leadership_Master__c leadershipMaster: listOfLeadershipMaster) {
           
            GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            projectClonedLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
            projectLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
            projectLeadership.Is_Cloned_from_Parent__c = false;
            projectLeadership.GP_Employee_ID__r = employeeMaster;
            projectLeadership.GP_Active__c = false;
            
            GP_Project_Leadership__c projectClonedAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectClonedAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectClonedAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectClonedAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectClonedAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectClonedAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectClonedAccountLeadership.Is_Cloned_from_Parent__c = true;
            projectClonedAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectClonedAccountLeadership.GP_Active__c = false;
            
            
            GP_Project_Leadership__c projectAccountLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
            projectAccountLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
            projectAccountLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Account Leadership');
            projectAccountLeadership.GP_Leadership_Role_Name__c = 'Account Leadership';
            projectAccountLeadership.GP_Pinnacle_End_Date__c = System.today().addDays(-110);
            projectAccountLeadership.GP_Start_Date__c = System.today().addDays(-100);
            projectAccountLeadership.Is_Cloned_from_Parent__c = false;
            projectAccountLeadership.GP_Employee_ID__r = employeeMaster;
            projectAccountLeadership.GP_Active__c = false;
            
            listOfProjectLeadership.add(projectLeadership);
            listOfProjectLeadership.add(projectClonedLeadership);
            
            listOfProjectLeadership.add(projectAccountLeadership);
            listOfProjectLeadership.add(projectClonedAccountLeadership);
        }
        
        insert listOfProjectLeadership;
        /*
        List<GP_Project_Leadership__c> listOfClonedProjectLeadership = new List<GP_Project_Leadership__c>();
        
        for(GP_Project_Leadership__c projectLeadership : listOfProjectLeadership) {
            
            projectLeadership.GP_Parent_Project_Leadership__c = projectLeadership.Id;
            projectLeadership.Id = null;
            projectLeadership.Is_Cloned_from_Parent__c = false;
            
            listOfClonedProjectLeadership.add(projectLeadership);
        }
        
        insert listOfClonedProjectLeadership;*/
    }
}