global class GPScheduleSendNotificationOnPIDSync implements Schedulable {
    
    global void execute(SchedulableContext sc) {
        // TODO
        // Abort this schedule job.
        // Get the syned PIDs.
        // Get the sycned resource allocation records.
        // Send the email notification.
        
        System.abortJob(sc.getTriggerId());
        
        //Integer oracleSyncTime = Integer.valueOf(System.label.GP_Oracle_Sync_Time);
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        List<GP_Project__c> listOfPids = new List<GP_Project__c>();
        
        List<GP_Project__c> listOfProjects = [SELECT id, Name, GP_Oracle_PID__c, GP_EP_Project_Number__c, GP_Current_Working_User__r.Name,
                                              GP_Send_Email_Notification_Addresses__c,
                                              (Select id, GP_Employee__r.name, GP_Employee__r.GP_Final_OHR__c,GP_Start_Date__c,GP_End_Date__c, GP_Oracle_Status__c, 
                                               GP_Oracle_Process_Log__c from GP_Resource_Allocations__r Where GP_isupdated__c = true)
                                              FROM GP_Project__c where  GP_is_Send_Notification__c = true
                                              /*where LastModifiedDate >= : Datetime.now().addMinutes(-oracleSyncTime) 
                                                and GP_Oracle_Status__c ='S' and LastModifiedById =: system.label.GP_Pinnacle_Integration_Use 
                                                and GP_Approval_Status__c ='Approved'*/];
        
        for(GP_Project__c p : listOfProjects) {
            String htmlBody = '';
            if(p.GP_Resource_Allocations__r.size() > 0) {
                htmlBody = '<html><body><table border="1" style="border-collapse: collapse" style=width:100%">'
                    + '<tr><td bgcolor="#005595" width="100%" height="30px" color="white" align="left">'
                    + '<b style="color:white;margin-left:10px;">'
                    + '<H4>SFDC-PINNACLE NOTIFICATION!</H4></b>'
                    + '</td></tr><tr><td><div style="color:#333399;margin-left:10px;margin-top:10px">'
                    + 'Dear '+p.GP_Current_Working_User__r.Name+',</div> '
                    + '<div style="color:#333399;margin-left:10px;margin-top:20px">Find below Status of Resource Allocations Processed in Oracle PA on PID '+ p.GP_Oracle_PID__c +'.For any errors in allocations, do correct the errors and resubmit the PID in Pinnacle SFDC. Until error is not solved, resource will not be allocated on PID in Oracle and will not be able to fill timesheet. <br/> <br/>'
                    + '<table border="1" style="border-collapse: collapse" style="width:100%">'
                    + '<tr><th bgcolor="#005595" style="color:white">EMPLOYEE NAME</th>'
                    + '<th bgcolor="#005595" style="color:white">OHRID</th><th bgcolor="#005595" style="color:white">ORACLE STATUS</th>'
                    + '<th bgcolor="#005595" style="color:white">ORACLE MESSAGE</th><th bgcolor="#005595" style="color:white"> Allocation Start Date</th>'
                    + '<th bgcolor="#005595" style="color:white">Allocation End Date</th></tr>';
                   
                for(GP_Resource_Allocation__c ra : p.GP_Resource_Allocations__r) {
                    htmlBody += '</td><td>' + (String.isNotBlank(ra.GP_Employee__r.name) ? ra.GP_Employee__r.name : '[Not Provided]')
                        + '</td><td>'+ (String.isNotBlank(ra.GP_Employee__r.GP_Final_OHR__c) ? ra.GP_Employee__r.GP_Final_OHR__c : '[Not Provided]')
                        +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Status__c) ? ra.GP_Oracle_Status__c : '[Not Provided]')
                        +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Process_Log__c) ? ra.GP_Oracle_Process_Log__c : '[Not Provided]') 
                        +'</td><td>'+ (ra.GP_Start_Date__c!=null ? string.valueof(ra.GP_Start_Date__c).removeEnd('00:00:00'):'[Not Provided]')
                        +'</td><td>'+(ra.GP_End_Date__c!=null ? string.valueof(ra.GP_End_Date__c).removeEnd('00:00:00'):'[Not Provided]')
                        +'</td></tr>';
                }
                
                htmlBody += '</table><br/><br/>';
                htmlBody +='Please log on to Pinnacle-SFDC : <a href="https://genpact--uat.cs57.my.salesforce.com/'+p.Id+'">'+p.Name+'</a>'+'<br/>';
                htmlBody +='This is an Auto Generated Mail. Please do not reply.';
                htmlBody +='<br/><br/>Thanks';
                htmlBody +='<br/>Pinnacle SFDC.<br/><br/>';
            }
            
            if(String.isNotBlank(htmlBody)) {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                
                // Get email addresses per PID.
                List<String> listOfEmailAddress = getEmailAddresses(p.GP_Send_Email_Notification_Addresses__c);
                // Get org wide email address for Pinnacle System.
                Id oweaId = GPCommon.getOrgWideEmailId();
                
                if(!listOfEmailAddress.isEmpty()) {
                    email.setToAddresses(listOfEmailAddress);
                    email.setSubject('Resource Allocation Status on PID '+p.GP_Oracle_PID__c);
                 
                    email.setHtmlBody(htmlBody);
                    
                    if (oweaId != null) {
                        email.setOrgWideEmailAddressId(oweaId);
                    }
                    
                    emailNotifications.add(email);
                }                
            }
            
            listOfPids.add(new GP_Project__c(Id=p.Id,GP_is_Send_Notification__c=false));
        }
        
        if(!listOfPids.isEmpty()) {
            GPDomainProject.skipRecursiveCall = true;
            update listOfPids;
        }
        
        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
    }
    
    private List<String> getEmailAddresses(String emailAddresses) {
        List<String> listOfEmails = new List<String>();
        List<String> listOfTempEmails = emailAddresses.split(';');
        
        for(String str : listOfTempEmails) {
            if(String.isNotBlank(str)) {
                listOfEmails.add(str);
            }
        }
        
        return listOfEmails;
    }
}