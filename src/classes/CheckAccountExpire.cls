/*
 * To run execute the batch execte the follwing command from developer console
  
 String StrQuery = 'SELECT Id, Name,Type FROM Account';
 CheckAccountExpire reassign = new CheckAccountExpire(StrQuery);

ID batchprocessid = Database.executeBatch(reassign);
 */

public class CheckAccountExpire implements Database.Batchable<sObject>{

   String Query;

   public CheckAccountExpire(String q){

      Query=q;
   }

   public Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   public void execute(Database.BatchableContext BC, List<sObject> scope)
   {
         list<Account> LstOfAcc = new list<account>(); 
         set<Id> AccountIdSet = new Set<Id>();
          map<Id,list<Opportunity>> AccountOppMap = new map<id,list<Opportunity>>();

         for(sobject s : scope)
         {
             Account AccObj = (Account) s;
             AccountIdSet.add(AccObj.id);
         }
        if(AccountIdSet.size()>0)
        {
            list<Opportunity> LstOpp = new list<Opportunity>();
            LstOpp = [select id,name,CloseDate,StageName,TCV1__c,AccountId from Opportunity where accountId in : AccountIdSet];
            if(LstOpp.size()>0)
            {
               for(Opportunity OppObj : LstOpp)
               {
                   if(AccountOppMap.containskey(OppObj.AccountId))
                   {
                      list<Opportunity> TempOppLst = new list<Opportunity>();
                      TempOppLst.addall(AccountOppMap.get(OppObj.AccountId));
                      TempOppLst.add(OppObj);
                      AccountOppMap.put(OppObj.accountid,TempOppLst);
                   }
                   else
                   {
                      list<Opportunity> TempOppLst = new list<Opportunity>();
                      TempOppLst.add(OppObj);
                      AccountOppMap.put(OppObj.accountid,TempOppLst);
                   }
               }
            }
         }
         
        if(AccountOppMap.size()>0)
        {
            for(sobject s : scope)
            {
                Account AccObj = (Account) s;
                boolean AccountNotExpire = false;
                if(AccountOppMap.containskey(AccObj.id))
                {
                    for(Opportunity OppObj : AccountOppMap.get(AccObj.id))
                    {
                        if(OppObj.CloseDate <= System.TODAY() && OppObj.CloseDate >= System.TODAY() - 365 && OppObj.StageName  == '6. Signed Deal' &&  OppObj.TCV1__c > 1)
                        {
                            AccountNotExpire = true;
                            break;
                        }
                    }
                    if(AccountNotExpire)
                        AccObj.Type = 'Client';
                    else
                        AccObj.Type = 'Prospect';
                    LstOfAcc.add(AccObj);
                 }
                 else
                 {
                   AccObj.Type = 'Target';
                    LstOfAcc.add(AccObj);
                  }
  
             }
     update LstOfAcc;

        } 
   }

   public void finish(Database.BatchableContext BC)
   {
       
   }
}