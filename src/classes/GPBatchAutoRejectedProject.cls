global class GPBatchAutoRejectedProject implements Database.Batchable < sObject > , Database.Stateful , schedulable{
    
    /* set < id > setprocessinstance = new set < id > ();
list < Approval.ProcessWorkitemRequest > lstAR = new list < Approval.ProcessWorkitemRequest > ();
map < id, id > mapProcessInstanceworkitem = new map < id, id > (); */
    list < GP_Project__c > updatelstofProject = new list < GP_Project__c > ();
    Date lastSevendate = System.Today().addDays(-Integer.valueof(System.Label.GP_Project_Submission_Valid_days));
    Integer WorkingDayCount=0;
    date lastThirtyOneWorkingDate;
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        //AvinashN - CRQ000000070619 - PID Approval Workflow
        for(Integer i=0;i<=Integer.valueof(System.Label.GP_Project_Submission_Valid_days);i++)
        {
            // workDayCount = GPSendMailNotificationToPIDApprover.getWorkingDays(lastSevendate.addDays(i),System.today());         
            
            Datetime dt = (DateTime)System.today().addDays(-i);
            String dayOfWeek = dt.format('EEEE');                          
            system.debug('==dayOfWeek='+dayOfWeek);
            if(dayOfWeek != 'Sunday' && dayOfWeek != 'Saturday')
            {
                WorkingDayCount = WorkingDayCount+1;
                system.debug('===WorkingDayCount==='+WorkingDayCount);
                if(WorkingDayCount == Integer.valueOf(System.Label.GP_Auto_Reject_Work_Day) )
                {
                    lastThirtyOneWorkingDate = System.today().addDays(-i);
                    break;
                }    
            }          
            
        }        
        //GPSendMailNotificationToPIDApprover.SentEmailNotification(lstofProject);
        //End Here
        System.debug('===lastThirtyOneWorkingDate==='+lastThirtyOneWorkingDate);
        return Database.getQueryLocator([Select Id, Name,
                                         GP_Project_Submission_Date__c, 
                                         GP_Approval_Status__c,
                                         GP_Approver_Ids__c,
                                         GP_Primary_SDO__r.Name,
                                         GP_EP_Project_Number__c,
                                         GP_SDO_Code__c,
                                         GP_Project_type__c,
                                         GP_Business_Type__c,
                                         GP_Business_Name__c,
                                         GP_Start_Date__c,
                                         GP_End_Date__c,
                                         GP_Project_Organization__r.Name,
                                         GP_Current_Working_User__r.Name,
                                         GP_Current_Working_User__r.Email,
                                         GP_Current_Working_User__r.OHR_ID__c,
                                         GP_PID_Approver_User__r.Name,
                                         GP_PID_Approver_User__r.OHR_ID__c,
                                         GP_Oracle_PID__c,
                                         GP_Approval_Requester_Comments__c
                                         From GP_Project__c where GP_Approval_Status__c = 'Pending for Approval'                                         
                                         and GP_Project_Submission_Date__c =: lastThirtyOneWorkingDate
                                        ]);
        
        
    }
    
    global void execute(SchedulableContext sc)
    {
        GPBatchAutoRejectedProject objAutoReject = new GPBatchAutoRejectedProject();
        Database.executebatch(objAutoReject); 
        
    }
    
    global void execute(Database.BatchableContext bc, List < GP_Project__c > lstofProject) {
        
        set < id > setprocessinstance = new set < id > ();
        list < Approval.ProcessWorkitemRequest > lstAR = new list < Approval.ProcessWorkitemRequest > ();
        map < id, list<id>> mapProcessInstanceworkitem = new map < id, list<id> > ();
        savepoint sp = Database.setSavepoint();
        try {
            if (lstofProject != null && lstofProject.size() > 0) {            
                
                lstofProject =  [Select Id, Name, 
                                 GP_Project_Submission_Date__c, 
                                 GP_Approval_Status__c,
                                 GP_Approver_Ids__c,
                                 GP_Primary_SDO__r.Name,
                                 GP_EP_Project_Number__c,
                                 GP_SDO_Code__c,
                                 GP_Project_type__c,
                                 GP_Business_Type__c,
                                 GP_Business_Name__c,
                                 GP_Start_Date__c,
                                 GP_End_Date__c,
                                 GP_Project_Organization__r.Name,
                                 GP_Current_Working_User__r.Name,
                                 GP_Current_Working_User__r.Email,
                                 GP_Current_Working_User__r.OHR_ID__c,
                                 GP_PID_Approver_User__r.Name,
                                 GP_PID_Approver_User__r.OHR_ID__c,
                                 GP_Oracle_PID__c,
                                 GP_Approval_Requester_Comments__c,
                                 (SELECT ID FROM ProcessInstances  where status ='Pending' ORDER BY CreatedDate DESC )
                                 From GP_Project__c where id in: lstofProject];
                
                system.debug('===lstofProject==='+lstofProject);
                
                for (GP_Project__c prj: lstofProject) 
                {
                    for(ProcessInstance objProcessInstance : prj.ProcessInstances)
                    {
                        setprocessinstance.add(objProcessInstance.id);
                    }
                }
            }
            system.debug('===setprocessinstance==='+setprocessinstance.size());
            if (setprocessinstance.size() > 0) {
                for (ProcessInstanceWorkitem obj: [Select Id,ProcessInstance.TargetObjectid, ProcessInstanceId from ProcessInstanceWorkitem 
                                                   where ProcessInstanceId in: setprocessinstance order by Createddate desc]) 
                {
                    if (mapProcessInstanceworkitem.get(obj.ProcessInstance.TargetObjectid) == null) 
                    {
                        mapProcessInstanceworkitem.put(obj.ProcessInstance.TargetObjectid, new list<id>());
                    }
                    mapProcessInstanceworkitem.get(obj.ProcessInstance.TargetObjectid).add(obj.id);
                }
            }
            for (GP_Project__c objProject: lstofProject) {
                
                if (objProject.ProcessInstances != null && mapProcessInstanceworkitem.get(objProject.id) != null) 
                {
                    for(  id objWItemId : mapProcessInstanceworkitem.get(objProject.id )){
                        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                        req.setComments('Rejected by system');
                        req.setAction('Reject');
                        req.setWorkitemId(objWItemId);
                        lstAR.add(req);
                    }
                    objProject.GP_Approval_Status__c = 'Rejected';
                    if(objProject.GP_Band_3_Approver_Status__c == 'Pending'){
                        objProject.GP_Band_3_Approver_Status__c = 'Draft';
                    }
                    objProject.GP_Rejection_Approve_Mode__c = 'System';
                    updatelstofProject.add(objProject);
                }
            }
            system.debug('===lstofProject-updatelstofProject==='+updatelstofProject);
            if (lstAR.size() > 0) {
                
                GPCheckRecursive.ApprovedfromScreen = true;
                list < Approval.ProcessResult > lstresult = Approval.process(lstAR);
            }
            
            if (updatelstofProject != null && updatelstofProject.size() > 0) {
                update updatelstofProject;                
            }
        } catch(Exception ex) {
            database.rollback(sp);
            GPErrorLogUtility.logError('GPBatchAutoRejectedProject', 'execute',ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());            
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
        system.debug('===updatelstofProject==='+updatelstofProject);
        //Avinash - CRQ000000070619 - PID Approval Workflow
        if(System.Label.GP_SendMail_Notification_To_PID_Approver == 'true')
        {
            try{
                if(updatelstofProject.size()>0)
                {
                    GPSendMailNotificationToPIDApprover.SentEmailNotification(updatelstofProject);
                }
                
            }
            catch(exception ex)
            {
                
            }
            try{
                
                GPSendMailNotificationToPIDApprover.SentEmailNotification(null);
            }
            catch(exception ex)
            {
                
            }
        }       
        
    }
    
}