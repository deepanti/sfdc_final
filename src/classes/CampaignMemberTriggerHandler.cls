public class CampaignMemberTriggerHandler {
    /*********************************************************
* Handle all the bussiness logic for after insert/update here
********************************************************/

    public void onAfterInsertforAccountCount(){
        Set<Id> campIds = new Set<Id>();
    Map<Id, set<String>> mapAccountCount = new Map <Id, set<String>>();
    
               List<CampaignMember> camMemLst = (trigger.IsInsert)?Trigger.New:Trigger.Old;
            for(CampaignMember cmItem:camMemLst){
                campIds.add(cmItem.CampaignId);
            }
                
            List<CampaignMember> lstMem = [SELECT Id,CampaignId, Lead.Account__c, Contact.AccountId FROM CampaignMember WHERE CampaignId IN :campIds LIMIT 1000];
            for(CampaignMember cm:lstMem)
            {
                Id memberAccId = (cm.Lead.Account__c != null)?cm.Lead.Account__c:((cm.Contact.AccountId != null)?cm.Contact.AccountId:null);
                    if(memberAccId != null){
                        if(mapAccountCount.containsKey(cm.CampaignId))
                        {
                            Set<String> tempSet = mapAccountCount.get(cm.CampaignId);
                            tempset.add(memberAccId);
                            mapAccountCount.put(cm.CampaignId, tempset);
                        }
                        else{
                            mapAccountCount.put(cm.CampaignId, new Set<String>{memberAccId});
                        }
                    }
            }
            
            if(mapAccountCount.size() != 0)
            {
                List<Campaign> lstCampaign = new List<Campaign>();
                for(Id cmp : mapAccountCount.keyset())
                {
                    Campaign tempCamp = new Campaign(Id=cmp);
                    tempCamp.AccountCount__c = mapAccountCount.get(cmp).size();
                    lstCampaign.add(tempCamp);
                }
                
                if(lstCampaign.size() > 0)
                {
                    update lstCampaign;
                }
            }
                                     
    

        
    }
    
    
    
}