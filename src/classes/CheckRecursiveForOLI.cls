public class CheckRecursiveForOLI 
{
    public static boolean runAfter = true;
    public static boolean runBefore = true;     
    public static boolean runAfter1 = true;
    public static boolean runAfterDelete = true;
    public static boolean runAfterOppTrigger = true;
    public static boolean runAfterOppTrigger1 = true;     
    public static boolean runAfterOppTrigger2 = true;
    public static boolean runAfterOnAcc = true;
    public static boolean dateUpdateFlag = true;
    public static boolean runingTestFlag = true;
    
    public static Boolean runingTestFlag(){
        if(runingTestFlag){
            runingTestFlag = false;
            return true;
        }else{
           return runingTestFlag;
        }
    }
    
    public static Boolean dateUpdateFlag(){
        if(dateUpdateFlag){
            dateUpdateFlag = false;
            return true;
        }else{
            return dateUpdateFlag;
        }
    }
    public static Boolean onAccountTrigger(){
        if(runAfterOnAcc){
            runAfterOnAcc = false;
            return true;
        }else{
            return runAfterOnAcc;
        }
    }
    
    public static Boolean OpptriggerRecursiveMehtod(){
        if(runAfterOppTrigger && runAfterOppTrigger1){
            if(runAfterOppTrigger){
                runAfterOppTrigger = false;
            }
            return true;
        }else if( runAfterOppTrigger1){
            if(runAfterOppTrigger1){
                runAfterOppTrigger1 = false;
            }
            return true;
        }else {
            return false;
        }
    }
     
    public static boolean runOnceAfter()
    {
        if(runAfter){
            runAfter=false;
            return true;
        }
        else{
            return runAfter;
        }
    }
    public static boolean runOnceAfterDelete()
    {
        if(runAfterDelete){
            runAfterDelete=false;
            return true;
        }
        else{
            return runAfterDelete;
        }
    }
    public static boolean runOnceBefore()
    {
        if(runBefore){
            runBefore=false;
            return true;
        }
        else{
            return runBefore;
        }
    }
    public static boolean runOnceAfter_1()
    {
        if(runAfter1){
            runAfter1 =false;
            return true;
        }
        else{
            return runAfter1;
        }
    }
   
}