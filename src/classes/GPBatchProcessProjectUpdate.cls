/**
 * @group Mass Update/Upload. 
 *
 * @description Batch class for Mass Update Project.
 *              Inherits GPBatchMassUpdateAndUploadHelper Class.
 */
global class GPBatchProcessProjectUpdate extends GPBatchMassUpdateAndUploadHelper implements Database.Batchable < sObject > {

    private final String INAVLID_PRIMARY_SDO_ID_ERROR_LABEL = 'Invalid Primary SDO Id';
    private final String INAVLID_SILO_ID_ERROR_LABEL = 'Invalid SILO Oracle Id';
    private final String INAVLID_PORTAL_ID_ERROR_LABEL = 'Invalid Portal Id';
    private final String INAVLID_HSL_ID_ERROR_LABEL = 'Invalid HSL Id';
    // Mass Update Change
    private final String PRODUCT_HIERARCHY_CANT_CHANGE = 'Product hierarchy can\'t be changed for OLI Linked PIDs';
    private final String BUSSINESS_HIERARCHY_CANT_CHANGE = 'Business hierarchy can\'t be changed for OLI Linked PIDs';
    private final String PRODUCT_HIERARCHY_DOESNOT_MATCH = 'Product Hierarchy doesn\'t match';
    private final String BUSSINESS_HIERARCHY_DOESNOT_MATCH = 'Business Hierarchy doesn\'t match';

    /**
     * @description Constructor with jobId whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * 
     */
    global GPBatchProcessProjectUpdate(String jobRecordId) {
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
        jobType = 'Update Project';
    }

    /**
     * @description Batch start method.
     * @param BC : BatchableContext.
     * 
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Update Job Record with 'Pending' Status.
        updateJobRecord('In Progress', 0, ''); // Class member of GPBatchMassUpdateAndUploadHelper.
		// Mass Update Change: added GP_PRJ_Product_Id__c field.
        String query = 'SELECT id, GP_Is_Failed__c,GP_PRJ_Attn_To_Email__c, GP_PRJ_Attn_To_Name__c, GP_PRJ_Billing_Currency__c,';
        query += 'GP_PRJ_Business_Hierarchy_L4__c, GP_PRJ_CC1__c,';
        //query += 'GP_PRJ_Business_Hierarchy__c, GP_PRJ_Business_Hierarchy_L2__c, GP_PRJ_Business_Hierarchy_L3__c, GP_PRJ_Business_Hierarchy_L4__c, GP_PRJ_CC1__c,';
        query += 'GP_PRJ_CC2__c, GP_PRJ_CC3__c, GP_PRJ_Customer_Contact_Number__c,';
        query += 'GP_PRJ_Customer_SPOC_Email__c, GP_PRJ_Customer_SPOC_Name__c, GP_PRJ_End_Date__c,';
        query += 'GP_PRJ_Fax_Number__c, GP_PRJ_HSL__c, GP_PRJ_MOD__c, GP_PRJ_Nature_of_Work__c, GP_PRJ_Portal_Name__c,';
        //query += 'GP_PRJ_Primary_SDO__c, GP_PRJ_Product__c, GP_PRJ_Product_Id__c, GP_PRJ_Project_Category__c,';
        query += 'GP_PRJ_Primary_SDO__c, GP_PRJ_Product__c, GP_PRJ_Project_Category__c,';        
        query += 'GP_PRJ_Project_Description__c, GP_PRJ_Project_Long_Name__c, GP_PRJ_Name__c,';
        query += 'GP_PRJ_Project_Type__c, GP_PRJ_Service_Line__c, GP_PRJ_SILO__c, GP_PRJ_Start_Date__c,';
        query += 'GP_Project__c from GP_Temporary_Data__c where GP_Job_Id__c = \'' + jobId + '\'';

        return Database.getQueryLocator(query);
    }

    /**
     * @description Batch execute method.
     * @param BC : BatchableContext.
     * @param listOfTemporaryRecords : list of temporary records that will be processed.
     * 
     */
    global void execute(Database.BatchableContext BC, List < Sobject > listOfTemporaryRecords) {

        getJobRecord(jobId); // Class member of GPBatchMassUpdateAndUploadHelper.

        Savepoint sp = Database.setSavepoint();

        try {
            // Set Validation configuration for project to validate updated project records.
            projectApprovalValidatorConfig.validateProjectAgainstTemplate = true; // Class member of GPBatchMassUpdateAndUploadHelper.
            projectApprovalValidatorConfig.validateCustomFieldsOnProject = true; // Class member of GPBatchMassUpdateAndUploadHelper.
            validationConfig.validateProject = true; // Class member of GPBatchMassUpdateAndUploadHelper.

            // Create Project Oracle PID set.
            setOracleIdSet(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
            // Query Project Records using project Oracle PID set.
            setListOfProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.

            if (listOfProjectRecords.size() > 0) {
                // Filter the PID's which can't be processed by Mass Update/Upload depening upon their Appproval Status and OMS status.
                setStatusWiseProjectSet(); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Create map of Project Oracle Ids Vs its status of processing for mass upload and update.
                setMapToCheckValidRecordsForProjectStatus(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Filter the list of temporary data depending upon the project's approval status.
                setLstOfValidatedTemporaryData(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Create map of all masters using their oracle PID's.
                setMasterDataMap(); // Class member of GPBatchMassUpdateAndUploadHelper.

                if (lstOfValidatedTemporaryData.size() > 0) {

                    //setProjectQuery();//get project records.
                    //setlistOfProjectRecordsWithChildQueried();

                    // Query all project and associated child records.
                    queryProjectAndChilRecordsUsingOraclePID(setOfOracleIds); // Class member of GPBaseProjectUtil.
                    // Set Map Of project and its Associated child Records.
                    setMapOfProjectChildRecords(); // Class member of GPBaseProjectUtil.

                    //setMasterDataMap();

                    // Merge Temporary record content with queried project records.
                    mergeTemporaryFieldValuesWithProjectRecord(); // Class method.

                    // setCommitedProjectAndChildRecords();

                    // Clone Project and Validate depending upon the Validation Configuration.
                    cloneAndValidateProject(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    // Filter the Project Records which have passed the validations.
                    setListOfValidatedProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.

                    if (listOfValidatedProjectRecords.size() > 0) {
                        // Create map Of Parent Project Id Vs Child Project Id.
                        setMapOfOldProjectIdVsNewProjectId(); // Class member of GPBatchMassUpdateAndUploadHelper.
                        // Clone Project Child Records and Validate depending upon the Validation Configuration.
                        cloneAndValidateProjectChildRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    }
                }

                // Update Job Record and the temporary records with validation result.
                String status = mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0 ? 'Failed' : 'Completed';
                updateJobRecord(status, mapOfInvalidEntries.values().size(), ''); // Class member of GPBatchMassUpdateAndUploadHelper.
            	Database.update(lstOfNewProjectCreated, false); // Class member of GPBatchMassUpdateAndUploadHelper.
                updateJobLineItemRecord(); // Class member of GPBatchMassUpdateAndUploadHelper.
            } else {
                Database.rollback(sp);
                // Update Job Record with Exception Message.
                updateJobRecord('Failed', listOfTemporaryRecords.size(), PROJECT_NOT_FOUND_FOR_ORACLE_PID_ERROR_LABEL); // Class member of GPBatchMassUpdateAndUploadHelper.
            }
        } catch (GPServiceProjectClone.GPServiceProjectCloneException cloneException) {
            Database.rollback(sp);

            if (cloneException.mapOfValidations == null && cloneException.relatedOrcaleId != null) // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.relatedOrcaleId, cloneException.exceptionString); // Class member of GPBatchMassUpdateAndUploadHelper.
            else if(cloneException.relatedOrcaleId == null) // Update Validation Result of record in Invalid Entries map.
				systemThrownExceptionValue = cloneException.exceptionString;
            else // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.mapOfValidations); // Class member of GPBatchMassUpdateAndUploadHelper.

            // Update Job Record and the temporary records with validation result.
            String status = ((mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0) || systemThrownExceptionValue != null || systemThrownExceptionValue != '' ) ? 'Failed' : 'Completed';
            updateJobRecord(status, mapOfInvalidEntries.values().size(), systemThrownExceptionValue); // Class member of GPBatchMassUpdateAndUploadHelper.
        } catch (Exception E) {
            system.debug('Exceptio'+E);
            Database.rollback(sp);
            // Update Job Record with Exception Message. 
            updateJobRecord('Failed', listOfTemporaryRecords.size(), E.getMessage()); // Class member of GPBatchMassUpdateAndUploadHelper.
        }
    }

    /**
     * @description Batch finish method.
     * @param BC : BatchableContext.
     * 
     */
    global void finish(Database.BatchableContext BC) {
        //updateJobRecord('Completed', 0, '');
        System.debug('Success');
    }

    /**
     * @description Merge Temporary record content with queried project records.
     * 
     */
    private void mergeTemporaryFieldValuesWithProjectRecord() {
        List < GP_Project__c > lstOfProjectRecordsCopy = lstProject.clone();
        GP_Temporary_Data__c objUpdatedProjectValues;
        GP_Project__c objProject;
        String returnString;
        Integer count = 0;

        for (integer i = lstProject.size() - 1; i >= 0; i--) {
            objProject = lstProject[i];
            objUpdatedProjectValues = mapOfTemporaryDataWithUniqueKey.get(objProject.GP_Oracle_PID__c);
            returnString = updateValueWithTemporaryRecord(objProject, objUpdatedProjectValues);

            if (returnString != null) {
                lstProject.remove(i);
                // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(objProject.GP_Last_Temporary_Record_Id__c, returnString); // Class member of GPBatchMassUpdateAndUploadHelper.
            }
            //count++;
        }
        //lstProject = listOfProjectRecords;
        /*for(GP_Project__c objProject : lstOfProjectRecordsCopy){
            system.debug('mapOfTemporaryDataWithUniqueKey'+mapOfTemporaryDataWithUniqueKey);
            system.debug('lstOfProjectRecordsCopy'+lstOfProjectRecordsCopy);
            GP_Temporary_Data__c objUpdatedProjectValues = mapOfTemporaryDataWithUniqueKey.get(objProject.GP_Oracle_PID__c);
            String returnString = updateValueWithTemporaryRecord(objProject,objUpdatedProjectValues);
            if(returnString != null){
                //listOfProjectRecords[count] = null;
                listOfProjectRecords.remove(count);
                setExceptionMessageToRelatedRecord(objProject.GP_Last_Temporary_Record_Id__c,returnString);
            }
            count++;
        }*/
    }

    /**
     * @description Update field values with temporary record and Master data Map.
     * @param objProject : project Record.
     * @param objUpdatedProjectValues : temporary data record with new values of project record.
     * 
     */
    private String updateValueWithTemporaryRecord(GP_Project__c objProject, GP_Temporary_Data__c objUpdatedProjectValues) {

        objProject.GP_Last_Temporary_Record_Id__c = objUpdatedProjectValues.Id;
        
        String businessHierarchyKey = checkBussinessHierarchy(objProject, objUpdatedProjectValues);
        String productHierarchyKey = checkProductHierarchy(objProject, objUpdatedProjectValues);
        Boolean isBusinessHierarchyNotMatch = (businessHierarchyKey != null && businessHierarchyKey.equalsIgnoreCase('NoKey')) ? true: false;
        Boolean isProductHierarchyNotMatch = (productHierarchyKey != null && productHierarchyKey.equalsIgnoreCase('NoKey')) ? true : false;

        if (objUpdatedProjectValues.GP_PRJ_SILO__c != null && !mapOfPinnacleMasterOracleIdVsSFDCId.containsKey(objUpdatedProjectValues.GP_PRJ_SILO__c))
            return INAVLID_SILO_ID_ERROR_LABEL;
        else if (objUpdatedProjectValues.GP_PRJ_Primary_SDO__c != null && !mapOfWorkLocationMasterOracleIdVsSFDCId.containsKey(objUpdatedProjectValues.GP_PRJ_Primary_SDO__c))
            return INAVLID_PRIMARY_SDO_ID_ERROR_LABEL;
        else if (objUpdatedProjectValues.GP_PRJ_HSL__c != null && !mapOfPinnacleMasterOracleIdVsSFDCId.containsKey(objUpdatedProjectValues.GP_PRJ_HSL__c))
            return INAVLID_HSL_ID_ERROR_LABEL;
        else if (objUpdatedProjectValues.GP_PRJ_Portal_Name__c != null && !mapOfPinnacleMasterOracleIdVsSFDCId.containsKey(objUpdatedProjectValues.GP_PRJ_Portal_Name__c))
            return INAVLID_PORTAL_ID_ERROR_LABEL;
        // Mass Update Change-30-07-19.
        else if (objProject.GP_Deal_Category__c == 'Sales SFDC' && (String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Nature_of_Work__c) || 
                                                                    String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Service_Line__c) || 
                                                                    String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Product__c)))
                                                                    //String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Product_Id__c)))
            return PRODUCT_HIERARCHY_CANT_CHANGE;
        // Mass Update Change : 12-12-19 : Only L4 be used for Business Hierarchy update.
        else if (objProject.GP_Deal_Category__c == 'Sales SFDC' && String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c))
    		return BUSSINESS_HIERARCHY_CANT_CHANGE;
        else if(isProductHierarchyNotMatch)
        	return PRODUCT_HIERARCHY_DOESNOT_MATCH;
        else if(isBusinessHierarchyNotMatch)
        	return BUSSINESS_HIERARCHY_DOESNOT_MATCH;
        else {
            if (objProject.GP_Deal_Category__c == 'Without SFDC' || objProject.GP_Deal_Category__c == 'Support' || objProject.GP_Deal_Category__c == 'Inter COE') {
                // Mass Update Change : 12-12-19 : Buisness Segment Id will be updated on L4 detail.
                //objProject.GP_Business_Segment_L2_Id__c = objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L2__c;
                //setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Business_Hierarchy_L2__c', objProject, 'GP_Business_Segment_L2_Id__c');

                //objProject.GP_Customer_Hierarchy_L4__c = objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c;
                setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Business_Hierarchy_L4__c', objProject, 'GP_Customer_Hierarchy_L4__c');
				
                // Mass Update Change : 12-12-19 : Sub Buisness Id will be updated on L4 detail.
                //objProject.GP_Sub_Business_L3_Id__c = objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L3__c;
                //setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Business_Hierarchy_L3__c', objProject, 'GP_Sub_Business_L3_Id__c');
				
                // Mass Update Change : 12-12-19 : Sub Buisness Group will be updated on L4 detail.
                //objProject.GP_Business_Group_L1__c = objUpdatedProjectValues.GP_PRJ_Business_Hierarchy__c;
                //setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Business_Hierarchy__c', objProject, 'GP_Business_Group_L1__c');

                //objProject.GP_Nature_of_Work__c = objUpdatedProjectValues.GP_PRJ_Nature_of_Work__c;
                setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Nature_of_Work__c', objProject, 'GP_Nature_of_Work__c');

                //objProject.GP_Service_Line__c = objUpdatedProjectValues.GP_PRJ_Service_Line__c;
                setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Service_Line__c', objProject, 'GP_Service_Line__c');

                //objProject.GP_Product__c = objUpdatedProjectValues.GP_PRJ_Product__c;
                setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Product__c', objProject, 'GP_Product__c');
                
                /*// Mass Update Change : 12-12-19 : User will not provide product id.
                //objProject.GP_Product__c = objUpdatedProjectValues.GP_PRJ_Product__c;
                setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Product_Id__c', objProject, 'GP_Product_Id__c');
                */
                // Mass Update Change : 12-12-19 : Change Business Hierarchy details on L4 id.
                if(businessHierarchyKey != null && !businessHierarchyKey.equalsIgnoreCase('NoKey')) {
                    /*List<Account> listOfAccounts = mapOfAccounts.ContainsKey(businessHierarchyKey) ? mapOfAccounts.get(businessHierarchyKey) : null;
                    
                    if(listOfAccounts != null && listOfAccounts.size() > 0) {
                        Account acc = listOfAccounts[0];
                        objProject.GP_Business_Segment_L2__c = acc.Business_Segment__r.Name;
                        objProject.GP_Sub_Business_L3__c = acc.Sub_Business__r.Name;
                    }*/
                    
                    objProject.GP_Business_Segment_L2__c = mapOfAccounts.get(businessHierarchyKey).Business_Segment__r.Name;
                    objProject.GP_Business_Segment_L2_Id__c = mapOfAccounts.get(businessHierarchyKey).Business_Segment__c;
                    objProject.GP_Sub_Business_L3__c = mapOfAccounts.get(businessHierarchyKey).Sub_Business__r.Name;
                    objProject.GP_Sub_Business_L3_Id__c = mapOfAccounts.get(businessHierarchyKey).Sub_Business__c;
                    objProject.GP_Business_Group_L1__c = mapOfAccounts.get(businessHierarchyKey).Business_Group__c;
                }
                // Mass Update Change
                // Change product Hierarchy details.
                if(productHierarchyKey != null && !productHierarchyKey.equalsIgnoreCase('NoKey')) {
                    List<GP_Product_Master__c> listOfPMs = mapOfProductMasters.ContainsKey(productHierarchyKey) ? mapOfProductMasters.get(productHierarchyKey) : null;
                    
                    if(listOfPMs != null && listOfPMs.size() > 0) {
                        //GP_Product_Master__c pm = listOfPMs[0];
                        for(GP_Product_Master__c pm : listOfPMs) {
                            if(objProject.GP_Vertical__c.equalsIgnoreCase(pm.GP_Industry_Vertical__c)) {
                                objProject.GP_Product_Id__c = pm.GP_Product_ID__c;
                                break;
                            }
                        }
                    }
                }
            }
            
            //objProject.GP_TCV__c = objUpdatedProjectValues.GP_PRJ_TCV__c;
            //objProject.GP_Sub_Vertical__c = objUpdatedProjectValues.GP_PRJ_Sub_Vertical__c;

            //objProject.GP_Primary_SDO__c = mapOfWorkLocationMasterOracleIdVsSFDCId.get(objUpdatedProjectValues.GP_PRJ_Primary_SDO__c);
            setUpdatedValueDependingOnColumnSelectedFromMasters(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Primary_SDO__c', objProject, 'GP_Primary_SDO__c',mapOfWorkLocationMasterOracleIdVsSFDCId);

            //objProject.GP_Portal_Name__c = mapOfPinnacleMasterOracleIdVsSFDCId.get(objUpdatedProjectValues.GP_PRJ_Portal_Name__c);
            setUpdatedValueDependingOnColumnSelectedFromMasters(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Portal_Name__c', objProject, 'GP_Portal_Name__c',mapOfPinnacleMasterOracleIdVsSFDCId);

            //objProject.GP_SILO_Names__c = mapOfPinnacleMasterOracleIdVsSFDCId.get(objUpdatedProjectValues.GP_PRJ_SILO__c);
            setUpdatedValueDependingOnColumnSelectedFromMasters(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_SILO__c', objProject, 'GP_SILO_Names__c',mapOfPinnacleMasterOracleIdVsSFDCId);

            //objProject.GP_Customer_Contact_Number__c = objUpdatedProjectValues.GP_PRJ_Customer_Contact_Number__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Customer_Contact_Number__c', objProject, 'GP_Customer_Contact_Number__c');

            //objProject.GP_HSL__c = mapOfPinnacleMasterOracleIdVsSFDCId.get(objUpdatedProjectValues.GP_PRJ_HSL__c);
            setUpdatedValueDependingOnColumnSelectedFromMasters(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_HSL__c', objProject, 'GP_HSL__c',mapOfPinnacleMasterOracleIdVsSFDCId);

            //objProject.GP_Project_Description__c = objUpdatedProjectValues.GP_PRJ_Project_Description__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Project_Description__c', objProject, 'GP_Project_Description__c');

            //objProject.GP_Customer_SPOC_Email__c = objUpdatedProjectValues.GP_PRJ_Customer_SPOC_Email__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Customer_SPOC_Email__c', objProject, 'GP_Customer_SPOC_Email__c');

            //objProject.GP_Customer_SPOC_Name__c = objUpdatedProjectValues.GP_PRJ_Customer_SPOC_Name__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Customer_SPOC_Name__c', objProject, 'GP_Customer_SPOC_Name__c');

            //objProject.GP_Project_Long_Name__c = objUpdatedProjectValues.GP_PRJ_Project_Long_Name__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Project_Long_Name__c', objProject, 'GP_Project_Long_Name__c');

            //objProject.GP_Project_Category__c = objUpdatedProjectValues.GP_PRJ_Project_Category__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Project_Category__c', objProject, 'GP_Project_Category__c');

            //objProject.GP_Billing_Currency__c = objUpdatedProjectValues.GP_PRJ_Billing_Currency__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Billing_Currency__c', objProject, 'GP_Billing_Currency__c');

            //objProject.GP_Attn_To_Email__c = objUpdatedProjectValues.GP_PRJ_Attn_To_Email__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Attn_To_Email__c', objProject, 'GP_Attn_To_Email__c');
			
            // Mass Update Change : 12-12-19 : no need to update project type.
            //objProject.GP_Project_type__c = objUpdatedProjectValues.GP_PRJ_Project_Type__c;
            //setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Project_Type__c', objProject, 'GP_Project_type__c');

            //objProject.GP_Attn_To_Name__c = objUpdatedProjectValues.GP_PRJ_Attn_To_Name__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Attn_To_Name__c', objProject, 'GP_Attn_To_Name__c');

            //objProject.GP_Start_Date__c = objUpdatedProjectValues.GP_PRJ_Start_Date__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Start_Date__c', objProject, 'GP_Start_Date__c');

            //objProject.GP_FAX_Number__c = objUpdatedProjectValues.GP_PRJ_Fax_Number__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Fax_Number__c', objProject, 'GP_FAX_Number__c');

            //objProject.GP_End_Date__c = objUpdatedProjectValues.GP_PRJ_End_Date__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_End_Date__c', objProject, 'GP_End_Date__c');

            //objProject.GP_Last_Temporary_Record_Id__c = objUpdatedProjectValues.Id;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'Id', objProject, 'GP_Last_Temporary_Record_Id__c');

            //objProject.GP_MOD__c = objUpdatedProjectValues.GP_PRJ_MOD__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_MOD__c', objProject, 'GP_MOD__c');

            //objProject.GP_CC3__c = objUpdatedProjectValues.GP_PRJ_CC3__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_CC3__c', objProject, 'GP_CC3__c');

            //objProject.GP_CC2__c = objUpdatedProjectValues.GP_PRJ_CC2__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_CC2__c', objProject, 'GP_CC2__c');

            //objProject.GP_CC1__c = objUpdatedProjectValues.GP_PRJ_CC1__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_CC1__c', objProject, 'GP_CC1__c');

            //objProject.Name = objUpdatedProjectValues.GP_PRJ_Name__c;
            setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectValues, 'GP_PRJ_Name__c', objProject, 'Name');


            //objProject.GP_Project_Organization__c = objUpdatedProjectValues.GP_PRJ_Project_Organization__c;
            //objProject.GP_Customer_Hierarchy_L4__c = objUpdatedProjectValues.GP_PRJ_Customer_Hierarchy_L4__c;
            //objProject.GP_Project_Currency__c = objUpdatedProjectValues.GP_PRJ_Project_Currency__c;
            //objProject.GP_Product_Family__c = objUpdatedProjectValues.GP_PRJ_Product_Family__c;
            return null;
        }
    }
    // Mass Update Change
    private String checkProductHierarchy(GP_Project__c objProject, GP_Temporary_Data__c objUpdatedProjectValues) {
        String key = '';
        
        if(String.isBlank(objUpdatedProjectValues.GP_PRJ_Product__c) && 
           //String.isBlank(objUpdatedProjectValues.GP_PRJ_Product_Id__c) && 
           String.isBlank(objUpdatedProjectValues.GP_PRJ_Nature_of_Work__c) && 
           String.isBlank(objUpdatedProjectValues.GP_PRJ_Service_Line__c)) 
        {
            return null;
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Product__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Product__c + '_';
        } else {
            key += objProject.GP_Product__c + '_';
        }
        
        /*if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Product_Id__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Product_Id__c + '_';
        } else {
            key += objProject.GP_Product_Id__c + '_';
        }*/
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Service_Line__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Service_Line__c + '_';
        } else {
            key += objProject.GP_Service_Line__c + '_';
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Nature_of_Work__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Nature_of_Work__c + '_';
        } else {
            key += objProject.GP_Nature_of_Work__c + '_';
        }
        key = key.removeEnd('_');
        
        System.debug('==key @@ in Update PID Batch=='+ key);
        
        if(mapOfProductMasters.containsKey(key)) return key; else return 'NoKey';
    }
    // Mass Update Change
    private String checkBussinessHierarchy(GP_Project__c objProject, GP_Temporary_Data__c objUpdatedProjectValues) {
        /*String key = '';
        
        if(String.isBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy__c) &&
          String.isBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L2__c) &&
          String.isBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L3__c) &&
          String.isBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c)) 
        {
            return null;
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Business_Hierarchy__c + '_';
        } else {
            key += objProject.GP_Business_Group_L1__c + '_';
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L2__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L2__c + '_';
        } else {
            key += objProject.GP_Business_Segment_L2_Id__c + '_';
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L3__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L3__c + '_';
        } else {
            key += objProject.GP_Sub_Business_L3_Id__c + '_';
        }
        
        if(String.isNotBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c)) {
            key += objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c + '_';
        } else {
            key += objProject.GP_Customer_Hierarchy_L4__c + '_';
        }
        
        key = key.removeEnd('_');
        
        System.debug('==key @@ in Update PID Batch for Accounts=='+ key);
        System.debug('==map Of Accounts=='+ mapOfAccounts);
        
        if(Test.isRunningTest()) return null;
        
        if(mapOfAccounts.containsKey(key)) return key; else return 'NoKey';*/
        
        if(String.isBlank(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c)) {
            return null;
        }
        
        if(mapOfAccounts.containsKey(objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c)) return objUpdatedProjectValues.GP_PRJ_Business_Hierarchy_L4__c; else return 'NoKey';
    }
}