@isTest
public class GPBatchProcessTimesheetUploadTracker {
public static GP_Job__c objJob;
    public static employeeProjectAllocationData objResponseProjectAllocation;
    public static GP_Timesheet_Transaction__c objTimeSheetTransaction;
    
    @testSetup static void setupCommonData()
    {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMasterGlobalSetting ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO'; 
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        timesheettrnsctnObj.GP_Month_Year__c = 'May-18';
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Is_Closed__c = false;
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Oracle_Status__c = 'S';
        insert prjObj ;
        prjObj.GP_Is_Closed__c = false;
        prjObj.GP_Oracle_PID__c='ORACLE-001';
        update prjObj;
        
        GP_Project_Task__c objPrjTask = GPCommonTracker.getProjectTask(prjObj);
        objPrjTask.GP_SOA_External_ID__c = 'test';
        objPrjTask.GP_Start_Date__c = system.today().adddays(5);
        objPrjTask.GP_End_Date__c = system.today().adddays(30);
        objPrjTask.GP_Task_Number__c = 'ORACLETK-01';
        objPrjTask.GP_Project_Number__c = 'ORACLE-001';
        insert objPrjTask;
        
        GP_Timesheet__c objTimeSheet = new GP_Timesheet__c();
        objTimeSheet.GP_PersonId_MonthYear__c = 'EMP-001May-18';
        objTimeSheet.GP_Expenditure_Item_Date__c = system.today().adddays(20);
        objTimeSheet.GP_Person_Id__c = 'EMP-001';
        objTimeSheet.GP_Project_Number__c = 'ORACLE-001';
        objTimeSheet.GP_Task_Number__c = 'ORACLETK-01';
        insert objTimeSheet;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        timeshtentryObj.GP_Project__c = prjObj.id;
        timeshtentryObj.GP_Project_Oracle_PID__c = 'ORACLE-001';
        timeshtentryObj.GP_Project_Task_Oracle_Id__c = 'ORACLETK-01';
        insert timeshtentryObj ;
        
        GP_Timesheet_Entry__c listoftimesentry = [Select id, GP_Employee_Month_Year_Unique__c,GP_Project__c,
                                                  GP_Project__r.GP_Is_Closed__c from GP_Timesheet_Entry__c 
                                                  where id=: timeshtentryObj.id];
        
        system.debug('@@@@listoftimesentry'+listoftimesentry);
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        insert address ;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert resourceAllocationObj; 
        
        GP_Job__c objJob = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJob;
        
        
        
        GP_Temporary_Data__c objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJob.Id,
                                                                                                 'EMP-001',
                                                                                                 'ORACLE-001',
                                                                                                 'ORACLETK-01');
        objTemporaryTimeSheet.GP_TSC_Project_Task__c = 'ORACLETK-01@@ORACLE-001';
        objTemporaryTimeSheet.GP_TSC_Date__c = system.today().adddays(20);
        insert objTemporaryTimeSheet;
    }     
    @isTest static void testGPBatchProcessTimesheetUpload()
    {
        fetchData();
        invokeBatch();
    }
     public static void fetchData() { 
        objJob =[select id from GP_Job__c limit 1];
        //objTimeSheetTransaction = [select id from GP_Timesheet_Transaction__c limit 1];
    }
    public static void invokeBatch(){
        Database.executeBatch(new GPBatchProcessTimesheetUpload(objJob.Id), 200);
    }
}