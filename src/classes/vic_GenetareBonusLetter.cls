/**
* @author: Shivam Singh
* @date: March 2018
*/
public class vic_GenetareBonusLetter implements VIC_ObjectInterface{
    /**
* @author: Shivam Singh
* @date: March 2018
*/
    @AuraEnabled
    public static List<String> getFinancialYear(){
        return vic_CommonUtil.getFinancialYearForDisplay();
    }
    
    @AuraEnabled
    public static list<String> getYearForDisplay(){
        return vic_CommonUtil.getYearForDisplay();   
    }
    
    @AuraEnabled
    public static String getCurrentYear(){
        return String.valueof(system.today().year());
    }
    
    @AuraEnabled
    public static List<wrapperVicRole> getVicRole(){
        List<wrapperVicRole> objWrprLst=new List<wrapperVicRole>();
        List<Master_VIC_Role__c> masterVICRoleObj= vic_DataUtil.getActiveMasterVICRole();
        
        if(!masterVICRoleObj.isEmpty()){
            for(Master_VIC_Role__c role : masterVICRoleObj){
                wrapperVicRole wrapperVicRoleobj=new wrapperVicRole();
                wrapperVicRoleobj.strId=role.Id;
                wrapperVicRoleobj.name=role.name;
                objWrprLst.add(wrapperVicRoleobj);
            }
        }
        return objWrprLst;
    }
    
    
    @auraenabled
    public static String getSearchItems(String vicRole, integer selectedYear, string domicile, string frequency, string month, string qtr, Integer ytdYear){
        try{
            System.debug('vicRole===' + vicRole);
            Date dtStartDate = Date.newInstance(selectedYear, 1, 1);
            Date dtEndDate;
            if(frequency == 'Monthly YTD'){
                String[] strMonthArr = month.split('-');
                dtEndDate = Date.newInstance(ytdYear, integer.valueOf(strMonthArr[0]), integer.valueOf(strMonthArr[1]));
            }else if(frequency == 'Yearly'){
                dtEndDate = Date.newInstance(selectedYear+2, 12, 31);
            }else{
                dtEndDate = Date.newInstance(selectedYear, 12, 31);
            }
            
            List<User_VIC_Role__c> lstVicRole=new List<User_VIC_Role__c>();
            
            if(domicile == '' || domicile == null){
                if(vicRole == 'All VIC Role'){
                    lstVicRole= vic_DataUtil.getAllActiveUserVICRole();
                }else{
                    lstVicRole= vic_DataUtil.getActiveUserVICRoleByVicRole(vicRole);
                }
            }else{
                if(vicRole == 'All VIC Role'){
                    lstVicRole= vic_DataUtil.getAllActiveUserVICRoleAndDomicile(domicile); 
                }else{
                    lstVicRole= vic_DataUtil.getActiveUserVICRoleByVicRoleAndDomicile(vicRole, domicile); 
                }
            }
            
            Set<Id> setUserId = new Set<Id>();
            string strMsg = '';
            if(!lstVicRole.isEmpty() ){
                for(User_VIC_Role__c role : lstVicRole){
                    setUserId.add(role.User__c);
                    strMsg += role.User__c + ',';
                }
            }
            system.debug('setUserId'+setUserId);
            system.debug('dtStartDate'+dtStartDate);
            Set<Id> setTargetId = new Set<Id>();
            /*for(Target_payout__c p : [SELECT ID,Component__r.Target__c
                                      FROM Target_payout__c
                                      WHERE Component__r.Target__r.User__c IN :setUserId
                                      AND Component__r.Target__r.Start_Date__c =: dtStartDate
                                      AND Payout_Date__c <=:dtEndDate])
            {
              setTargetId.add(p.Component__r.Target__c);
            }*/
            
            for(Target_achievement__c p : [SELECT ID,Target_Component__r.Target__c
                                      FROM Target_achievement__c
                                      WHERE Target_Component__r.Target__r.User__c IN :setUserId
                                      AND Target_Component__r.Target__r.Start_Date__c =: dtStartDate
                                      AND vic_HR_Approval_Date__c <=:dtEndDate])
            {
              setTargetId.add(p.Target_Component__r.Target__c);
            }
            List<searchItem> lstSearchItem = new List<searchItem>();

            if(!setTargetId.isEmpty()){
                for(Target__c targetObj : [SELECT Id, User__r.Supervisor__r.Name,
                                           Start_date__c,
                                           Plan__r.Name,User__r.Name,
                                           Overall_Achievement__c,
                                           Overall_Payouts_Amount__c,
                                           Master_VIC_Role__c,
                                           Master_VIC_Role__r.Name
                                           FROM Target__c
                                           WHERE ID IN:setTargetId
                                           AND Bonus_Status__c NOT IN ('Exit','On Hold','Not Applicable') 
                                           AND Overall_Achievement__c > 0]){
                                               lstSearchItem.add(new searchItem(targetObj));
                                           }
            }
            
            return JSON.serialize(lstSearchItem);
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
    }
    
    
    @auraEnabled
    public static String getCongaUrlForBonusLetter(String bonusFilterStartDate, String bonusFilterEndDate, String strOfSelectedRecords, String vicRole, String year){
        try{
        List<Id> listofUserIdForSelectedVICRole = new  List<Id>();
        List<Id> listofTargetId = new List<Id>();
        List<searchItem> lstOfSelectedRecords = (List<searchItem>)JSON.deserialize(strOfSelectedRecords, List<searchItem>.class);
        if(lstOfSelectedRecords == null) {
            throw new VIC_Exception('Please select records for processing bonus letter.');
            //throw new VIC_Exception('COUNT-->' + lstOfSelectedRecords.size());
        }
        String masterVICRole;
        Map<String,String> mapVICRole = new Map<String,String>();
        system.debug('vicRole'+vicRole);
        system.debug('lstOfSelectedRecords'+lstOfSelectedRecords);    
        if(vicRole!='All VIC Role'){
            masterVICRole = [SELECT Id,Name FROM Master_VIC_Role__c WHERE id= : vicRole].Name;
        }else{
            Set<Id> setUserId = new Set<Id>();
            for(searchItem s:lstOfSelectedRecords){
                setUserId.add(s.objtarget.User__c);
            }
            System.debug('setUserId===>' + setUserId);
            Boolean isPreviousYearBonus = false;
            if(year != null && Integer.valueOf(year) < System.Today().Year()){
                isPreviousYearBonus = true;
            }
            List<User_VIC_Role__c> lstUserVicRole = new List<User_VIC_Role__c>();
            System.debug('isPreviousYearBonus ==>'+ isPreviousYearBonus);
            if(isPreviousYearBonus){
                lstUserVicRole = [SELECT User__c,Master_VIC_Role__c, Master_VIC_Role__r.Name
                                    FROM User_VIC_Role__c
                                    WHERE User__c IN :setUserId
                                    ORDER BY vic_For_Previous_Year__c ASC];
            }else{
                lstUserVicRole = [SELECT User__c,Master_VIC_Role__c, Master_VIC_Role__r.Name
                                    FROM User_VIC_Role__c
                                    WHERE User__c IN :setUserId
                                    AND vic_For_Previous_Year__c = false];
            }
            System.debug('lstUserVicRole===>' + lstUserVicRole);
            for(User_VIC_Role__c r :lstUserVicRole)
            {
                mapVICRole.put(r.User__c, r.Master_VIC_Role__r.Name);
            }
        }
        
        
        System.debug('bonusFilterStartDate===' + bonusFilterStartDate);
        System.debug('bonusFilterEndDate===' + bonusFilterEndDate);
        System.debug('lstOfSelectedRecords===' + lstOfSelectedRecords);
        System.debug('masterVICRole===' + masterVICRole);
        System.debug('year===' + year);
        System.debug('mapVICRole===' + mapVICRole);
        // call the method to get the VIC Role for which Generate Bonus Matrix call.
        List<String> listOfVICRole = getListOfVICRole();
        System.debug('listOfVICRole===' + listOfVICRole);
        
        if(vicRole == 'All VIC Role') {
            for(searchItem obj : lstOfSelectedRecords) {
                System.debug('obj=='+ obj);
                if(obj.objtarget != null) {
                system.debug('mapVICRole'+obj.objtarget.User__r.Id);
                    String strVicRole = mapVICRole.containsKey(obj.objtarget.User__r.Id) ? mapVICRole.get(obj.objtarget.User__r.Id):'';
                    System.debug('listOfVICRole=='+listOfVICRole);
                    System.debug('true0=='+ strVicRole);
                     System.debug('true1=='+listOfVICRole.contains(strVicRole));
                    if(listOfVICRole.contains(strVicRole)) {
                     System.debug('true=='+ obj);
                        listofUserIdForSelectedVICRole.add(obj.objtarget.User__r.Id);
                    } else {
                        listofTargetId.add(obj.objtarget.Id);
                    }
                } 
            }
            System.debug('listofTargetId===' + listofTargetId);
            System.debug('listofUserIdForSelectedVICRole===' + listofUserIdForSelectedVICRole);
            
            if(listofUserIdForSelectedVICRole != null && listofUserIdForSelectedVICRole.size() > 0) {
                //calling the VIC_BatchGenerateBonusMatrix to generate the PDF.
                system.debug('1');
                return(callBatchGenerateBonusMatrix(listofUserIdForSelectedVICRole, year,Date.valueof(bonusFilterStartDate),Date.valueof(bonusFilterEndDate)));
            } 
            else {
            system.debug('2');
                return(generateBonusLetter(listofTargetId,  bonusFilterStartDate, bonusFilterEndDate));
            }
        } else {
             
             if(listOfVICRole.contains(masterVICRole)) {
                for(searchItem obj : lstOfSelectedRecords) {
                     system.debug('3');
                    listofUserIdForSelectedVICRole.add(obj.objTarget.User__r.Id);
                }
                
                return callBatchGenerateBonusMatrix(listofUserIdForSelectedVICRole, year,Date.valueof(bonusFilterStartDate),Date.valueof(bonusFilterEndDate));
            } else {
                 for(searchItem obj : lstOfSelectedRecords) {
                    listofTargetId.add(obj.objTarget.Id);
                }
                
                return generateBonusLetter(listofTargetId,  bonusFilterStartDate, bonusFilterEndDate);
            }
        }
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }
        
    }    
    
    
    
    
    private static String generateBonusLetter(List<Id> listofTargetId, String bonusFilterStartDate, String bonusFilterEndDate){
        try{
            Date bonusStartDate = null;
            Date bonusEndDate = null;
            
            if(bonusFilterStartDate != null && bonusFilterStartDate != ''){
                bonusStartDate = Date.valueOf(bonusFilterStartDate);
            }    
            if(bonusFilterEndDate != null && bonusFilterEndDate != ''){
                bonusEndDate = Date.valueOf(bonusFilterEndDate);
            }
            if(listofTargetId != null) {
                List<Target__c> lstSelectedUsr = [SELECT Id,Bonus_Status__c,
                                                  Generate_Bonus_Letter__c,
                                                  Bonus_Filter_Start_Date__c,
                                                  Bonus_Filter_End_Date__c,
                                                  Master_VIC_Role__c,
                                                  Master_VIC_Role__r.Name
                                                  FROM Target__c
                                                  WHERE id IN : listofTargetId];
                System.debug('lstSelectedUsr==' + lstSelectedUsr);
                if(!lstSelectedUsr.isEmpty()){
                    for(Target__c allTrgt : lstSelectedUsr){
                        allTrgt.Generate_Bonus_Letter__c=true;
                        allTrgt.Bonus_Status__c = 'Released';
                        if(bonusStartDate != null){
                            allTrgt.Bonus_Filter_Start_Date__c=bonusStartDate;
                        }    
                        if(bonusEndDate != null){
                            allTrgt.Bonus_Filter_End_Date__c=bonusEndDate;
                        }   
                    }      
                }
                System.debug('updatedlstSelectedUsr0==='+ lstSelectedUsr);
                if(!lstSelectedUsr.isEmpty()){
                    update lstSelectedUsr;
                    System.debug('updatedlstSelectedUsr==='+ lstSelectedUsr);
                    return 'SUCCESS';
                }
               
            }
        }
        catch(Exception ex){
            return ex.getMessage();
        }  
        return null;
    }
    
    
    /**
@Author:        Dheeraj Chawla
@Date:          03/08/2018
@description:   To get the list of VIC role from the custom label.
**/
    
    private static List<String> getListOfVICRole() {
        String VIC_Role = Label.Bonus_Matrix_VIC_Role;
        List<String> listOfVICRole = new List<String>();
        for(String str : VIC_Role.split(',')){
            listOfVICRole.add(str.trim());
        }        
        if(listOfVICRole.isEmpty()) {
            return null;
        }
        return listOfVICRole;
    }
    /**
@Author:        Dheeraj Chawla
@Date:          03/08/2018
@description:   To call the VIC_BatchGenerateBonusMatrix to generate the PDF.
**/
    private static String callBatchGenerateBonusMatrix(List<id> listofUserIdForSelectedVICRole, String year,date startDate, date endDate) {
        System.debug('listofUserIdForSelectedVICRole==>' + listofUserIdForSelectedVICRole);
        VIC_BatchGenerateBonusMatrix objBatchInstance = new VIC_BatchGenerateBonusMatrix(listofUserIdForSelectedVICRole, startDate, endDate);
        ID batchprocessid = Database.executeBatch(objBatchInstance, 1);
        return 'Queued';
    }
    
    
    public class wrapperVicRole{
        
        @auraenabled
        public string strId;
        @auraenabled
        public string name;
        
    }
    
    public class searchItem{
        
        @auraenabled
        public Boolean isSelected;
        @auraenabled
        public String strTargetID;
        @auraenabled
        public String strUserName;
        @auraenabled
        public String strPlanName;
        @auraenabled
        public String strSupervisorName;
        @auraenabled
        public Decimal decTotalBonus;
        @auraenabled
        public Decimal decTotalPayout;
        @AuraEnabled
        public Target__c objtarget;
        
        
        public searchItem(Target__c targetObj){
            this.isSelected = false;
            this.strTargetID = targetObj.id;
            this.strUserName = targetObj.User__r.Name;
            this.strPlanName = targetObj.Plan__r.Name;
            this.strSupervisorName = targetObj.User__r.Supervisor__r.Name;
            this.decTotalBonus = targetObj.Overall_Achievement__c;
            this.decTotalPayout = targetObj.Overall_Achievement__c;
            this.objtarget = targetObj;
        }
        
    }
    
}