// Description : This batch helper class would create/modify two PIDs per Account on change in respective Account. 
// APC Change
public virtual with sharing class GPBatchAccountAutomationHelper extends GPBaseProjectUtil {

    public Id indirectRecordTypeId;
    public Id wlRecordTypeId;
    public Id mandatoryKeyMemberRecordTypeId;
    public List<String> listOfAccountFieldsToValidate = new List<String>();
    public Map<String,String> mapOfAPCSettingNames = (Map<String,String>)JSON.deserialize(System.Label.GP_APC_Custom_Setting_Names, Map<String,String>.class);        
    public Map<String,String> mapOfAPCSettingDetails = GPCommonUtil.getGlobalValues(mapOfAPCSettingNames.Values());
    public Map<String,GP_Product_Master__c> mapOfVerticalWithPMs = new Map<String,GP_Product_Master__c>();
    public String gpmOHR;
    
    public GPBatchAccountAutomationHelper() {
        this.indirectRecordTypeId = GPCommon.getRecordTypeId('GP_Project__c', 'Indirect PID');
        this.wlRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        this.mandatoryKeyMemberRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        this.gpmOHR = System.Label.GP_Default_Sales_Leader_OHR != 'NA' ? System.Label.GP_Default_Sales_Leader_OHR : '';
    }    

    public void fetchProductMaster(Map<String,GP_Product_Master__c> mapOfVerticalWithPMs) {
        for(GP_Product_Master__c pm : [select id, Name, GP_Service_Line__c, GP_Nature_of_Work__c, GP_Product_Name__c, GP_Industry_Vertical__c, GP_Product_ID__c 
        from GP_Product_Master__c where GP_Product_Name__c = 'Consulting - Others' 
        and GP_Is_Active__c = true and GP_Service_Line__c != 'Underwriting']) {
            mapOfVerticalWithPMs.put(pm.GP_Industry_Vertical__c, pm);
        }
    }
    
    public GPBatchAccountAutomationHelper.GPBatchResponseWrapper createNewPIDsForAccounts(List<Account> listOfNewAccounts, String errorMessage, Map<Id, String> mapOfAccIdWithStringValue) {
        
        Map<Id,Id> mapOfAccIdWithDealIds = new Map<Id,Id>();
        Map<Id,Boolean> mapAccIdWithValidationStatus = new Map<Id,Boolean>();
        List<GP_Project__c> listOfPIDs = new List<GP_Project__c>();
        Boolean isSuccess = false;
        
        // upsert indirect deals
        errorMessage += upsertPIDDeals(listOfNewAccounts, mapOfAccIdWithDealIds, errorMessage, mapAccIdWithValidationStatus, mapOfVerticalWithPMs);
        System.debug('==createNewPIDsForAccounts=='+ mapOfAccIdWithDealIds);
        // Create Indirect PIDs
        if(mapOfAccIdWithDealIds.keySet().size() > 0) {
            errorMessage += createAccountPIDs(mapOfAPCSettingNames, mapOfAPCSettingDetails, listOfPIDs, listOfNewAccounts, errorMessage, mapOfAccIdWithStringValue,
            mapOfVerticalWithPMs, mapAccIdWithValidationStatus);
        }
        
        // Tag Deals to PIDs
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Deal__c = mapOfAccIdWithDealIds.get(pid.GP_Customer_Hierarchy_L4__c);
        }
        // Create new PIDs for new accounts
        if(listOfPIDs.size() > 0) {
            insert listOfPIDs;
        }

        // Create child records
        errorMessage += createChildRecords(mapOfAPCSettingNames, mapOfAPCSettingDetails, listOfPIDs, listOfNewAccounts, mapAccIdWithValidationStatus, 
        errorMessage);
        
        // Approve all PIDs
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Approval_Status__c = 'Approved';
            pid.GP_Approval_DateTime__c = DateTime.now();
            pid.GP_Initial_PID_Approval_DateTime__c = DateTime.now();
            pid.GP_Project_Status__c = '1008';
        }
        
        if(listOfPIDs.size() > 0) {
            update listOfPIDs;
        }
        
        if(String.isNotBlank(errorMessage)) {
            isSuccess = false;
        } else {
            errorMessage = 'Data Synced Successfully.';
            isSuccess = true;
        }
        
        return new GPBatchAccountAutomationHelper.GPBatchResponseWrapper(isSuccess, errorMessage);
    }

    public String createAccountPIDs(Map<String,String> mapOfAPCSettingNames, Map<String,String> mapOfAPCSettingDetails, 
    List<GP_Project__c> listOfPIDs, List<Account> listOfAccounts, String errorMessage, Map<Id, String> mapOfAccIdWithStringValue, 
    Map<String,GP_Product_Master__c> mapOfVerticalWithPMs, Map<Id,Boolean> mapAccIdWithValidationStatus) {        
        
        Map<String, Map<String, String>> mapOfSDOS = (Map<String,Map<String, String>>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('1')), Map<String,Map<String, String>>.class);
        Map<String, String> mapOfOUS = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('3')), Map<String,String>.class);
        Map<String, String> mapOfPTS = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('6')), Map<String,String>.class);
        Map<String, String> mapOfPOS = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('7')), Map<String,String>.class);
        
        String PTId = mapOfPTS.values().get(0);
        String OUDetails = mapOfOUS.values().get(0);
        String poId = mapOfPOS.values().get(0);        

        for(Account acc : listOfAccounts) {
            // No need to check vertical as already done in Deal validation
            if(String.isNotBlank(acc.Industry_Vertical__c)) {
                if(mapOfVerticalWithPMs.containsKey(acc.Industry_Vertical__c)) {
                    //Map<String,String> mapOfSDOViaSettings = (Map<String,String>)JSON.deserialize(mapOfSDOS.get(acc.Industry_Vertical__c), Map<String,String>.class);
                    String SDODetail = mapOfSDOS.get(acc.Industry_Vertical__c).values().get(0);

                    // Continue further if no account found
                    if(!mapAccIdWithValidationStatus.containsKey(acc.Id)) {
                        continue;
                    }
                    
                    if(mapOfAccIdWithStringValue.containskey(acc.Id)) {
                        if(mapOfAccIdWithStringValue.get(acc.Id) == 'Both' || mapOfAccIdWithStringValue.get(acc.Id) == '1') {
                            GP_Project__c pid1 = createPIDRecord('first', acc, SDODetail, OUDetails, PTId, poId,
                            mapOfVerticalWithPMs.get(acc.Industry_Vertical__c), mapAccIdWithValidationStatus.get(acc.Id));
                            
                            if(pid1 != null) {
                                listOfPIDs.add(pid1);                        
                            }
                        }                        
                        
                        if(mapOfAccIdWithStringValue.get(acc.Id) == 'Both' || mapOfAccIdWithStringValue.get(acc.Id) == '2') {
                            GP_Project__c pid2 = createPIDRecord('Second', acc, SDODetail, OUDetails, PTId, poId,
                            mapOfVerticalWithPMs.get(acc.Industry_Vertical__c), mapAccIdWithValidationStatus.get(acc.Id));
                            
                            if(pid2 != null) {
                                listOfPIDs.add(pid2);
                            }
                        }
                    }
                } else {
                    errorMessage += 'Product Master not found for the vertical. ';
                }
                
            } else {
                errorMessage += 'Industrial Vertical not defined. ';
            }            
        }
        
        return errorMessage;
    }

    private GP_Project__c createPIDRecord(String PIDSeq, Account acc, String SDODetail, String OUDetails, String PTId, String poId, GP_Product_Master__c pm, Boolean isAccountValid) {
        GP_Project__c pid = new GP_Project__c();

        // PID details
        if(PIDSeq.equalsIgnoreCase('first')) {
            String pidName = 'ACNT - BD - ' + acc.Name;
            String pidLongName = 'Business Dev PID for ' + acc.Name;
            pid.Name = pidName.length() >= 30 ? pidName.Left(30) : pidName;
            pid.GP_Project_Long_Name__c = pidLongName.length() >= 60 ? pidLongName.left(60) : pidLongName;
            pid.GP_Project_Description__c = 'Business Dev PID for ' + acc.Name;
            pid.GP_Clone_Source__c = 'First Account PID';
            pid.GP_Project_type__c = 'Account BD';
        } else {
            String pidName2 = 'ACNT - IO/MS - ' + acc.Name;
            String pidLongName2 = 'IO/MS Support PID for ' + acc.Name;
            pid.Name = pidName2.length() >= 30 ? pidName2.Left(30) : pidName2;
            pid.GP_Project_Long_Name__c = pidLongName2.length() >= 60 ? pidLongName2.left(60) : pidLongName2;
            pid.GP_Project_Description__c = 'IO/MS Support PID for ' + acc.Name;
            pid.GP_Clone_Source__c = 'Second Account PID';
            pid.GP_Project_type__c = 'Account';
        }
        
        pid.recordTypeId = this.indirectRecordTypeId;
        pid.GP_Project_Template__c = PTId;
        pid.GP_Primary_SDO__c = SDODetail;
        pid.GP_Operating_Unit__c = OUDetails;
        pid.OwnerId = UserInfo.getUserId();
        pid.GP_Current_Working_User__c = UserInfo.getUserId();
        // BH
        pid.GP_Business_Group_L1__c = acc.Business_Group__c;
        pid.GP_Business_Segment_L2_Id__c = acc.Business_Segment__c;        
        pid.GP_Business_Segment_L2__c = acc.Business_Segment__r.Name;
        pid.GP_Sub_Business_L3_Id__c = acc.Sub_Business__c;
        pid.GP_Sub_Business_L3__c = acc.Sub_Business__r.Name;
        pid.GP_Customer_Hierarchy_L4__c = acc.Id;
        // PH
        pid.GP_Vertical__c = pm.GP_Industry_Vertical__c;
        pid.GP_Sub_Vertical__c = acc.Sub_Industry_Vertical__c;
        pid.GP_Nature_of_Work__c = pm.GP_Nature_of_Work__c;
        pid.GP_Service_Line__c = pm.GP_Service_Line__c;
        pid.GP_Product__c = pm.GP_Product_Name__c;
        pid.GP_Product_Id__c = pm.GP_Product_ID__c;
        // Other info
        pid.GP_Start_Date__c = Date.today();
        pid.GP_Business_Name__c = 'Indirect';
        pid.GP_Business_Type__c = 'Indirect';        
        pid.GP_Project_Category__c = 'Account';
        pid.GP_Billing_Currency__c = 'USD';
        pid.GP_Project_Currency__c = 'USD';
        pid.GP_Bill_Rate_Type__c = 'Hourly';
		pid.GP_Initial_PID_Submission_DateTime__c = DateTime.now();
        // Corporate Id as Project Org
        pid.GP_Project_Organization__c = poId;
        pid.GP_Approval_Status__c = 'Draft';

        return pid;
    }

    public String upsertPIDDeals(List<Account> listOfAccounts, Map<Id,Id> mapOfAccIdWithDealIds, String errorMessage,
    Map<Id,Boolean> mapAccIdWithValidationStatus, Map<String,GP_Product_Master__c> mapOfVerticalWithPMs) {

        List<Account> listOfValidAccounts = new List<Account>();
        List<GP_Deal__c> listOfDealsToUpsert = new List<GP_Deal__c>();
        Set<String> setOfPIDsOfAccounts = new Set<String>();

        for(Account acc : listOfAccounts) {
            if(!mapOfVerticalWithPMs.containsKey(acc.Industry_Vertical__c)) {
                errorMessage += 'Product Master not found for the vertical. ';
            } else {
                listOfValidAccounts.add(acc);
                mapAccIdWithValidationStatus.put(acc.Id, true);
            }
        }
        // On Account modification
        if(listOfValidAccounts.size() > 0) {
            // Determine which accounts have PIDs already created in the system.
            for(Account acc : listOfValidAccounts) {
                if(String.isNotBlank(acc.GP_Account_PID__c) && String.isNotBlank(acc.GP_Account_BD_Spend__c)) {
                    setOfPIDsOfAccounts.add(acc.GP_Account_PID__c);
                    setOfPIDsOfAccounts.add(acc.GP_Account_BD_Spend__c);
                } else if(String.isNotBlank(acc.GP_Account_PID__c)) {
                    setOfPIDsOfAccounts.add(acc.GP_Account_PID__c);
                } else if(String.isNotBlank(acc.GP_Account_BD_Spend__c)) {
                    setOfPIDsOfAccounts.add(acc.GP_Account_BD_Spend__c);
                }
            }

            if(setOfPIDsOfAccounts.size() > 0) {
                // Query the deal ids from PIDs.
                for(GP_Project__c pid : [Select id, GP_Deal__c, GP_Customer_Hierarchy_L4__c from GP_Project__c where GP_Oracle_PID__c =: setOfPIDsOfAccounts 
                AND GP_Oracle_Status__c = 'S' AND GP_Approval_Status__c = 'Approved' AND GP_Parent_Project__c = null 
                AND RecordTypeId =: this.indirectRecordTypeId]) {
                    mapOfAccIdWithDealIds.put(pid.GP_Customer_Hierarchy_L4__c, pid.GP_Deal__c);
                }
            }
        }

        if(listOfValidAccounts.size() > 0) {
            for(Account acc : listOfValidAccounts) {
                String dealId = '';
                if(mapOfAccIdWithDealIds.keySet().size() > 0 && mapOfAccIdWithDealIds.containsKey(acc.Id)) {
                    dealId = mapOfAccIdWithDealIds.get(acc.Id);
                }

                if(mapOfVerticalWithPMs.containsKey(acc.Industry_Vertical__c)) {
                    listOfDealsToUpsert.add(dealRecordObj(acc, dealId, mapOfVerticalWithPMs.get(acc.Industry_Vertical__c)));
                }
            }
        }

        if(listOfDealsToUpsert.size() > 0) {
            upsert listOfDealsToUpsert;
        }

        for(GP_Deal__c d : listOfDealsToUpsert) {
            mapOfAccIdWithDealIds.put(d.GP_Account_Name_L4__c, d.Id);
        }
        
        return errorMessage;
    }

    private GP_Deal__c dealRecordObj(Account acc, String dealId, GP_Product_Master__c pm) {
        
        GP_Deal__c deal = new GP_Deal__c();

        if(String.isNotBlank(dealId)) {
            deal.id = dealId;
        }
        deal.Name = acc.Name;
        // BH
        deal.GP_Business_Group_L1__c = acc.Business_Group__c;
        deal.GP_Business_Segment_L2__c = acc.Business_Segment__r.Name;
        deal.GP_Business_Segment_L2_Id__c = acc.Business_Segment__c;
        deal.GP_Sub_Business_L3__c = acc.Sub_Business__r.Name;
        deal.GP_Sub_Business_L3_Id__c = acc.Sub_Business__c;
        deal.GP_Account_Name_L4__c = acc.Id;
        // PH
        deal.GP_Nature_of_Work__c = pm.GP_Nature_of_Work__c;
        deal.GP_Service_Line__c = pm.GP_Service_Line__c;
        deal.GP_Vertical__c = pm.GP_Industry_Vertical__c;
        deal.GP_Sub_Vertical__c = acc.Sub_Industry_Vertical__c;
        deal.GP_Product__c = pm.GP_Product_Name__c;
        deal.GP_Product_Id__c = pm.GP_Product_ID__c;
        // Other info
        deal.GP_Status__c = 'Qualified';
        deal.GP_Deal_Category__c = 'Support';
        deal.GP_Is_Created_per_Account__c = true;
        deal.GP_Sales_Leader_OHR__c = String.isNotBlank(this.gpmOHR) ? this.gpmOHR : acc.Sales_Unit__r.Sales_Leader__r.OHR_ID__c;

        if(String.isBlank(dealId)) {
            deal.GP_Start_Date__c = Date.today();
        }

        return deal;
    }
    
    public String createChildRecords(Map<String,String> mapOfAPCSettingNames, Map<String,String> mapOfAPCSettingDetails, 
    List<GP_Project__c> listOfPIDs, List<Account> listOfAccounts, Map<Id,Boolean> mapAccIdWithValidationStatus,
    String errorMessage) {

        Set<String> setOfLeadershipOHRs = new Set<String>();
        Map<Id, Id> mapOfPIDWithAccId = new Map<Id, Id>();
        Map<String, GP_Employee_Master__c> mapOfEmpIdWithEmpRecs = new Map<String, GP_Employee_Master__c>();
        Map<String, GP_Leadership_Master__c> mapOfLMIdWithLMRecs = new Map<String, GP_Leadership_Master__c>();
        Map<Id, Account> mapOfAccIdWithAccs = new Map<Id, Account>();
        List<GP_Project_Work_Location_SDO__c> listOfWLs = new List<GP_Project_Work_Location_SDO__c>();
        List<GP_Project_Leadership__c> listOfPLs = new List<GP_Project_Leadership__c>();

        Map<String, String> mapOfFPnAs = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('2')), Map<String,String>.class);
        Map<String, String> mapOfPIDApprovers = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('4')), Map<String,String>.class);
        Map<String, String> mapOfWLs = (Map<String,String>)JSON.deserialize(mapOfAPCSettingDetails.get(mapOfAPCSettingNames.get('5')), Map<String,String>.class);

        String defaultWorkLocationId = mapOfWLs.values().get(0);

        for(Account acc : listOfAccounts) {
            if(mapAccIdWithValidationStatus.containsKey(acc.Id) && mapAccIdWithValidationStatus.get(acc.Id)) {
                if(String.isNotBlank(this.gpmOHR) || 
                   acc.Sales_Unit__r.Sales_Leader__r.IsActive) {
                    String salesLeaderOHR = String.isNotBlank(this.gpmOHR) ? 
                        this.gpmOHR : acc.Sales_Unit__r.Sales_Leader__r.OHR_ID__c;
                    setOfLeadershipOHRs.add(salesLeaderOHR);
                } else {
                    errorMessage += 'Account GPM is not an active user. ';
                    break;
                }

                if(mapOfFPnAs.containsKey(acc.Industry_Vertical__c)) {
                    setOfLeadershipOHRs.add(mapOfFPnAs.get(acc.Industry_Vertical__c));
                }

                if(mapOfPIDApprovers.containsKey(acc.Industry_Vertical__c)) {
                    setOfLeadershipOHRs.add(mapOfPIDApprovers.get(acc.Industry_Vertical__c));
                }

                mapOfAccIdWithAccs.put(acc.Id, acc);
            }
        }

        if(setOfLeadershipOHRs.size() > 0) {
            for(GP_Employee_Master__c em : [Select id, GP_Final_OHR__c from GP_Employee_Master__c where GP_isActive__c = true 
            AND GP_Final_OHR__c =: setOfLeadershipOHRs]) {
                mapOfEmpIdWithEmpRecs.put(em.GP_Final_OHR__c, em);
            }
            
            for(GP_Leadership_Master__c lm : [Select id, GP_Leadership_Role__c, GP_Employee_Field_Type__c from GP_Leadership_Master__c where GP_PID_Category__c = 'Support' 
            AND GP_Category__c = 'MandatoryKeyMembers']) {
                mapOfLMIdWithLMRecs.put(lm.GP_Leadership_Role__c, lm);
            }
            
            // Create Wls
            for(GP_Project__c pid : listOfPIDs) {
                if(String.isNotBlank(pid.GP_Customer_Hierarchy_L4__c)) {
                    mapOfPIDWithAccId.put(pid.Id, pid.GP_Customer_Hierarchy_L4__c);
                }
                listOfWLs.add(createWLObjects(pid, defaultWorkLocationId));
                System.debug('==22=='+ mapOfAccIdWithAccs);
                if(mapOfAccIdWithAccs.containsKey(pid.GP_Customer_Hierarchy_L4__c)) {
                    Account acc = mapOfAccIdWithAccs.get(pid.GP_Customer_Hierarchy_L4__c);
                    Boolean isValid = true;
    
                    String vertical = acc.Industry_Vertical__c;
                    String gpmOHR = String.isNotBlank(this.gpmOHR) ? this.gpmOHR : acc.Sales_Unit__r.Sales_Leader__r.OHR_ID__c;
                    // GPM validation
                    if(!mapOfEmpIdWithEmpRecs.containsKey(gpmOHR)) {
                        errorMessage += 'GPM is not an active employee. ';
                        //isSuccess = false;
                        isValid = false;
                    }
                    // FP&A validation
                    if(mapOfFPnAs.containsKey(vertical) && !mapOfEmpIdWithEmpRecs.containsKey(mapOfFPnAs.get(vertical))) {
                        errorMessage += 'FP&A is not an active employee. ';
                        //isSuccess = false;
                        isValid = false;
                    }
                    // PID Approver validation
                    if(mapOfPIDApprovers.containsKey(vertical) && !mapOfEmpIdWithEmpRecs.containsKey(mapOfPIDApprovers.get(vertical))) {
                        errorMessage += 'PID Approver is not an active employee. ';
                        //isSuccess = false;
                        isValid = false;
                    }
                    if(isValid) {
                        listOfPLs.add(createPLObjects(pid, mapOfEmpIdWithEmpRecs.get(gpmOHR).Id, mapOfLMIdWithLMRecs.get('Global Project Manager'), 'PROJECT MANAGER'));
                        listOfPLs.add(createPLObjects(pid, mapOfEmpIdWithEmpRecs.get(mapOfFPnAs.get(vertical)).Id, mapOfLMIdWithLMRecs.get('FP&A'), '1003'));
                        listOfPLs.add(createPLObjects(pid, mapOfEmpIdWithEmpRecs.get(mapOfPIDApprovers.get(vertical)).Id, mapOfLMIdWithLMRecs.get('PID Approver Finance'), '1000'));
                    } else {
                        break;
                    }
                }
            }
        }

        if(listOfWLs.size() > 0) {
            insert listOfWLs;
        }

        if(listOfPLs.size() > 0) {
            insert listOfPLs;
        }
        
        return errorMessage;
    }

    private GP_Project_Work_Location_SDO__c createWLObjects(GP_Project__c pid, String defaultWorkLocationId) {
        GP_Project_Work_Location_SDO__c pwlo = new GP_Project_Work_Location_SDO__c();

        pwlo.GP_Primary__c = false;
        pwlo.GP_Project__c = pid.Id;
        pwlo.GP_Work_Location__c = defaultWorkLocationId;
        pwlo.GP_BCP__c = 'No';
        pwlo.RecordTypeId = this.wlRecordTypeId;
        
        return pwlo;
    }
    
    private GP_Project_Leadership__c createPLObjects(GP_Project__c pid, String EmployeeMasterId, 
    GP_Leadership_Master__c leadershipMaster, String leadershipRole) {
        GP_Project_Leadership__c pl = new GP_Project_Leadership__c();
        
        pl.GP_Project__c = pid.Id;
        pl.GP_Leadership_Master__c = leadershipMaster.Id;
        pl.GP_Employee_ID__c = EmployeeMasterId;
        pl.GP_Active__c = true;
        pl.GP_Employee_Field_Type__c = leadershipMaster.GP_Employee_Field_Type__c;
        pl.GP_Leadership_Role__c = leadershipRole;
        pl.GP_Leadership_Role_Name__c = leadershipMaster.GP_Leadership_Role__c;
        pl.RecordTypeId = this.mandatoryKeyMemberRecordTypeId;
        pl.GP_Start_Date__c = Date.today();
        
        return pl;
    }
    
    public GPBatchAccountAutomationHelper.GPBatchResponseWrapper updatePIDsForAccounts(List<Account> listOfUpdatedAccounts, Set<Id> setOfDealIds) {
        
        Map<Id,Id> mapOfAccIdWithDealIds = new Map<Id,Id>();
        Map<Id,Boolean> mapAccIdWithValidationStatus = new Map<Id,Boolean>();
        String errorMessage = ''; 
        Boolean isSuccess;

        // See if any update in account data
        GPBatchAccountAutomationHelper.GPBatchResponseWrapper rw = checkForUpdates(listOfUpdatedAccounts, setOfDealIds);        
        
        if(rw.isSuccess) {
            errorMessage += upsertPIDDeals(listOfUpdatedAccounts, mapOfAccIdWithDealIds, errorMessage, mapAccIdWithValidationStatus, 
            mapOfVerticalWithPMs);

            if(String.isNotBlank(errorMessage)) {
                isSuccess = false;
            } else {
                errorMessage = 'Data Synced Successfully. ';
                isSuccess = true;
            }
        } else {
            errorMessage = 'No update found in account. ';
            isSuccess = true;
        }
        
        return new GPBatchAccountAutomationHelper.GPBatchResponseWrapper(isSuccess, errorMessage);
    }

    private GPBatchAccountAutomationHelper.GPBatchResponseWrapper checkForUpdates(List<Account> listOfUpdatedAccounts, Set<Id> setOfDealIds) {
        Boolean isUpdated = false;
        String message = '';
        Map<Id, GP_Deal__c> mapOfAccIdWithDealObj = new Map<Id, GP_Deal__c>();

        for(GP_Deal__c deal : [Select id, GP_Business_Group_L1__c, GP_Business_Segment_L2__c, GP_Sub_Business_L3__c, GP_Business_Segment_L2_Id__c,
        GP_Sub_Business_L3_Id__c, GP_Account_Name_L4__c, GP_Vertical__c, GP_Sub_Vertical__c, GP_Sales_Leader_OHR__c 
        from GP_Deal__c where id =: setOfDealIds]) {
            mapOfAccIdWithDealObj.put(deal.GP_Account_Name_L4__c, deal);
        }

        for(Account acc : listOfUpdatedAccounts) {
            if(mapOfAccIdWithDealObj.containsKey(acc.Id)) {
                if(!acc.Business_Group__c.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Business_Group_L1__c)) isUpdated = true;
                if(!acc.Business_Segment__r.Name.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Business_Segment_L2__c)) isUpdated = true;
                if(acc.Business_Segment__c != mapOfAccIdWithDealObj.get(acc.Id).GP_Business_Segment_L2_Id__c) isUpdated = true;
                if(acc.Sub_Business__c != mapOfAccIdWithDealObj.get(acc.Id).GP_Sub_Business_L3_Id__c) isUpdated = true;
                if(!acc.Sub_Business__r.Name.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Sub_Business_L3__c)) isUpdated = true;
                if(!acc.Industry_Vertical__c.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Vertical__c)) isUpdated = true;
                if(!acc.Sub_Industry_Vertical__c.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Sub_Vertical__c)) isUpdated = true;
                if(String.isBlank(this.gpmOHR) && !acc.Sales_Unit__r.Sales_Leader__r.OHR_ID__c.equalsIgnoreCase(mapOfAccIdWithDealObj.get(acc.Id).GP_Sales_Leader_OHR__c)) isUpdated = true;
            }
        }

        return new GPBatchAccountAutomationHelper.GPBatchResponseWrapper(isUpdated, message);
    }
    
    public class GPBatchResponseWrapper {
        public boolean isSuccess;
        public String message;
        
        public GPBatchResponseWrapper(boolean isSuccess, String message) {
            this.isSuccess = isSuccess;
            this.message = message;
        }
    }
}