/**
* @group TimesheetCorrection. 
*
* @description Wrapper Class for timesheet entries, which will be displayed on timesheet correction screen.
*/
public class wrapperTimeSheetEntry{
    @AuraEnabled
    public List<GP_Timesheet_Entry__c> lstOfTimeSheetEntries{get;set;}
    @AuraEnabled
    public String timeSheetTransactionId{get;set;}
    @AuraEnabled
    public String projectName{get;set;}
    @AuraEnabled
    public boolean newRecord{get;set;}
    @AuraEnabled
    public String taskName{get;set;}
    @AuraEnabled
    public String exType{get;set;}
    @AuraEnabled
    public String projectId{get;set;}
    @AuraEnabled
    public String projectRecordTypeName{get;set;}
    @AuraEnabled
    public String taskId{get;set;}
    @AuraEnabled
    public Boolean isDefault {get;set;}
    @AuraEnabled
    public GP_Project_Task__c projectTask{get;set;}
    
    public wrapperTimeSheetEntry(Integer noOfDays, GP_Timesheet_Entry__c objTimeSheetEntryRecord,
                                 Id timeSheetTransactionId,
                                 Id customerId, Integer year, Integer month)
    {
        lstOfTimeSheetEntries = new List<GP_Timesheet_Entry__c>();
        Integer correctedYearValue;
        
        for(Integer day = 0; day < noOfDays; day++)
        {
            GP_Timesheet_Entry__c objTimeSheetEntry = new GP_Timesheet_Entry__c();
            //objTimeSheetEntry.GP_Project__c = objTimeSheetEntryRecord.GP_Project__c;
            objTimeSheetEntry.GP_Project_Oracle_PID__c = objTimeSheetEntryRecord.GP_Project_Oracle_PID__c;
            //objTimeSheetEntry.GP_Project_Task__c = objTimeSheetEntryRecord.GP_Project_Task__c;
            objTimeSheetEntry.GP_Project_Task_Oracle_Id__c = objTimeSheetEntryRecord.GP_Project_Task_Oracle_Id__c;
            objTimeSheetEntry.GP_Ex_Type__c = objTimeSheetEntryRecord.GP_Ex_Type__c;
            objTimeSheetEntry.GP_Timesheet_Transaction__c = timeSheetTransactionId;
            objTimeSheetEntry.GP_Employee__c = customerId;
            correctedYearValue = year;
            objTimeSheetEntry.GP_Date__c = Date.newInstance(correctedYearValue,month, day+1);
            objTimeSheetEntry.GP_Modified_Hours__c = 0;

            lstOfTimeSheetEntries.add(objTimeSheetEntry);
        }
    }
}