public class GPDomainEmployeeHRHistory extends fflib_SObjectDomain {

    public GPDomainEmployeeHRHistory(List < GP_Employee_HR_History__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainEmployeeHRHistory(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {};

    public override void onBeforeInsert() {
        // fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        autoPopulateRelatedEmployee(null);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //commented 12052018 upsertEmployeeHR(oldSObjectMap);
        //uow.commitWork();
        
       PopulateValueOnEmployeeFromLatestAssignment();
        
    }
   
    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        autoPopulateRelatedEmployee(oldSObjectMap);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //commented 12052018 upsertEmployeeHR(null);
        //uow.commitWork();
      PopulateValueOnEmployeeFromLatestAssignment();
    }
	public void PopulateValueOnEmployeeFromLatestAssignment()
    {
         list < GP_Employee_HR_History__c > lstEmpHistory = (list < GP_Employee_HR_History__c >) records;
       
        if(lstEmpHistory.size()>0){
            Set<String> personId=new Set<String>();
            for(GP_Employee_HR_History__c empHR : lstEmpHistory)
            {
                personId.add(empHR.GP_Employee_Person_Id__c);
            }  
           system.debug('===personId==='+personId);
           list < GP_Employee_HR_History__c > lstEmpHRHistory = [Select Id,
                                                                Name,
                                                                GP_Asgn_Creation_Date__c,
                                                                GP_ASGN_EFFECTIVE_END__c,
                                                                GP_ASGN_EFFECTIVE_START__c,
                                                                GP_Asgn_Last_Update_Date__c,
                                                                GP_ASSIGNMENT_STATUS__c,
                                                                GP_Band__c,
                                                                GP_Batch_Number__c,
                                                                GP_Business_Function__c,
                                                                GP_CC2_Code__c,
                                                                GP_CC2_Desc__c,
                                                                GP_COE__c,
                                                                GP_COE_Code__c,
                                                                GP_Date_Probation_End__c,
                                                                GP_Designation__c,
                                                                GP_Employee_Business_Group__c,
                                                                GP_Employees__c,
                                                                GP_Employee_Number__c,
                                                                GP_Employee_Person_Id__c,
                                                                GP_SDO_OracleId__c,
                                                                GP_Home_Payroll_Location_ID__c,
                                                                GP_Horizontal_Service_Line__c,
                                                                GP_HORIZANTAL_SERVICE_LINE__c,
                                                                GP_HRM_OHR_ID__c,
                                                                GP_HRM_PERSON_ID__c,
                                                                GP_Is_Senior_Delivery__c,
                                                                GP_Legal_Entity_Code__c,
                                                                GP_Legal_Entity_Name__c,
                                                                GP_Location__c,
                                                                GP_Organization__c,
                                                                GP_Org_Id__c,
                                                                GP_Payroll_Country__c,
                                                                GP_Payroll_Name__c,
                                                                GP_Period_Of_Placement_Start__c,
                                                                GP_Physical_Work_Location__c,
                                                                GP_Process__c,
                                                                GP_Process_ID__c,
                                                                GP_Projected_Assignment_End__c,
                                                                GP_SDO_Name__c,
                                                                GP_SDO_Oracle_Id__c,
                                                                GP_Service_Line__c,
                                                                GP_Supervisor_Ohrid__c,
                                                                GP_SUPERVISOR_PERSON_ID__c,
                                                                GP_TSC_ORG_ID__c,
                                                                GP_Work_Location_Code__c,
                                                                GP_Attribute1__c,
                                                                GP_Attribute2__c,
                                                                GP_Attribute3__c,
                                                                GP_Attribute4__c,
                                                                GP_Attribute5__c,
                                                                GP_RESOURCE_ID__c,
                                                                GP_Seq_Number__c,
                                                                GP_Employment_Category__c,
                                                                 GP_Supervisor_Employee__c                                                                 
                                                                from GP_Employee_HR_History__c
                                                                where GP_Employee_Person_Id__c in: personId and GP_Attribute2__c != null ];
            system.debug('===lstEmpHRHistory==='+lstEmpHRHistory.size());
            Map<String,List<GP_Employee_HR_History__c>> mapUniquePersonId=new Map<String,List<GP_Employee_HR_History__c>>();
            
            for(String st: personId)
            {
                List<GP_Employee_HR_History__c> segregateEmployeeHRMaster=new List<GP_Employee_HR_History__c>();
                for(GP_Employee_HR_History__c empHRMst:lstEmpHRHistory)
                {                 
                    if(st==empHRMst.GP_Employee_Person_Id__c){                    
                        segregateEmployeeHRMaster.add(empHRMst);
                    }
                }
				if(segregateEmployeeHRMaster.size()>0){
                mapUniquePersonId.put(st,segregateEmployeeHRMaster);
				}
            }
            system.debug('===mapUniquePersonId==='+mapUniquePersonId.keySet());            
            List<GP_Employee_HR_History__c> EmployeeHRGreatesAsgEFFDate=new List<GP_Employee_HR_History__c>();
            for(String key:mapUniquePersonId.KeySet())
            {
                List<GP_Employee_HR_History__c> lstEmpHRDataByKey=new  List<GP_Employee_HR_History__c>();
                lstEmpHRDataByKey=mapUniquePersonId.get(key);
                GP_Employee_HR_History__c actEmpHR=new GP_Employee_HR_History__c();
                system.debug('===lstEmpHRDataByKey==='+lstEmpHRDataByKey.size());
               /* if(lstEmpHRDataByKey.size()==1)
                {
                    actEmpHR=lstEmpHRDataByKey[0];
                }*/
                actEmpHR=lstEmpHRDataByKey[0];
                for(Integer i=0; i < lstEmpHRDataByKey.size()-1;i++)
                {        
                    system.debug('===I==='+i);
                    if(actEmpHR.GP_Employee_Person_Id__c == lstEmpHRDataByKey[i+1].GP_Employee_Person_Id__c )  
                    {
                        //system.debug('===lstEmpHRDataByKey[i].GP_ASGN_EFFECTIVE_START__c==='+actEmpHR.GP_ASGN_EFFECTIVE_START__c);
                        //system.debug('===lstEmpHRDataByKey[i+1].GP_ASGN_EFFECTIVE_START__c==='+lstEmpHRDataByKey[i+1].GP_ASGN_EFFECTIVE_START__c);
                        if(actEmpHR.GP_ASGN_EFFECTIVE_START__c > lstEmpHRDataByKey[i+1].GP_ASGN_EFFECTIVE_START__c)
                        {                  
                            actEmpHR = actEmpHR;                 
                        }
                        else{
                            actEmpHR = lstEmpHRDataByKey[i+1];
                        }
                    }               
                }
                system.debug('====actEmpHR==='+actEmpHR);
                EmployeeHRGreatesAsgEFFDate.add(actEmpHR);
                
            }
            List<GP_Employee_Master__c> empMasterList=[Select Id,Name, GP_Assignment_Creation_Date__c ,
                                                       GP_Person_ID__c,
                                                       GP_ASGN_Last_Update_Date__c ,
                                                       GP_Assignment_Status__c ,
                                                       GP_BAND__c,
                                                       GP_BUSINESS_FUNCTION__c,
                                                       GP_CC2_Code__c ,
                                                       GP_CC2_Desc__c ,
                                                       GP_COE__c ,
                                                       GP_COE_Code__c,
                                                       GP_Cost_Rate__c,
                                                       GP_DATE_PROBATION_END__c,
                                                       GP_DESIGNATION__c,
                                                       GP_Employment_Category__c,
                                                       GP_Home_Payroll_Location_ID__c,
                                                       GP_HORIZANTAL_SERVICE_LINE__c,
                                                       GP_HRM_OHR_ID__c,
                                                       GP_HRM_PERSON_ID__c ,
                                                       GP_IS_SENIOR_DELIVERY__c,
                                                       GP_Legal_Entity_Code__c,
                                                       GP_Legal_Entity_Name__c, 
                                                       GP_LOCATION__c,  
                                                       GP_ORGANIZATION__c,  
                                                       GP_Organization_ID__c,  
                                                       GP_Org_TSC_Id__c,  
                                                       GP_Payroll_Country__c ,                                                         
                                                       GP_Period_Of_Placement_Start__c,  
                                                       GP_PHYSICAL_WORK_LOCATION__c,  
                                                       GP_Process_ID__c,  
                                                       GP_Process_Name__c,  
                                                       GP_Projected_Assignment_End__c,  
                                                       GP_Resource_Id__c,  
                                                       GP_SDO__c, 
                                                       GP_SDO_Name__c,  
                                                       GP_SDO_OracleId__c,  
                                                       GP_SERVICE_LINE__c,          
                                                       GP_Supervisor_Employee__c,  
                                                       GP_SUPERVISOR_OHRID__c,  
                                                       GP_SUPERVISOR_PERSON_ID__c,  
                                                       GP_Work_Location__c,  
                                                       GP_Employee_HR_History__c
                                                       from GP_Employee_Master__c where GP_Person_ID__c in: personId];
            if(empMasterList.size()>0 && EmployeeHRGreatesAsgEFFDate.size()>0 )
            {
                for(Integer i=0;i<empMasterList.size();i++)
                {          
                    for(GP_Employee_HR_History__c empHr : EmployeeHRGreatesAsgEFFDate)
                    {
                    if(empMasterList[i].GP_Person_Id__C == empHr.GP_Employee_Person_Id__c)
                    {
                        
                        empMasterList[i].GP_Assignment_Creation_Date__c = empHr.GP_Asgn_Creation_Date__c;
                        empMasterList[i].GP_ASGN_Last_Update_Date__c = empHr.GP_ASGN_Last_Update_Date__c;
                        empMasterList[i].GP_Assignment_Status__c = empHr.GP_Assignment_Status__c;
                        empMasterList[i].GP_BAND__c = empHr.GP_BAND__c;
                        empMasterList[i].GP_BUSINESS_FUNCTION__c = empHr.GP_BUSINESS_FUNCTION__c;
                        empMasterList[i].GP_CC2_Code__c = empHr.GP_CC2_Code__c;
                        empMasterList[i].GP_CC2_Desc__c = empHr.GP_CC2_Desc__c;
                        empMasterList[i].GP_COE__c = empHr.GP_COE__c;
                        empMasterList[i].GP_COE_Code__c = empHr.GP_COE_Code__c;
                        empMasterList[i].GP_Cost_Rate__c =  empHr.GP_Attribute1__c != null ? Decimal.valueOf(empHr.GP_Attribute1__c): null;
                        empMasterList[i].GP_DATE_PROBATION_END__c = empHr.GP_DATE_PROBATION_END__c;
                        
                        empMasterList[i].GP_DESIGNATION__c = empHr.GP_DESIGNATION__c;
                       
                        empMasterList[i].GP_Employment_Category__c = empHr.GP_Employment_Category__c;
                        empMasterList[i].GP_Home_Payroll_Location_ID__c = empHr.GP_Home_Payroll_Location_ID__c;
                        empMasterList[i].GP_HORIZANTAL_SERVICE_LINE__c = empHr.GP_HORIZANTAL_SERVICE_LINE__c;
                        empMasterList[i].GP_HRM_OHR_ID__c =empHr.GP_HRM_OHR_ID__c;
                        empMasterList[i].GP_HRM_PERSON_ID__c = empHr.GP_HRM_PERSON_ID__c;
                        
                        
                        empMasterList[i].GP_IS_SENIOR_DELIVERY__c = empHr.GP_IS_SENIOR_DELIVERY__c;
                        empMasterList[i].GP_Legal_Entity_Code__c = empHr.GP_Legal_Entity_Code__c;
                        empMasterList[i].GP_Legal_Entity_Name__c =empHr.GP_Legal_Entity_Name__c;
                        empMasterList[i].GP_LOCATION__c = empHr.GP_LOCATION__c;
                        
                        
                        empMasterList[i].GP_ORGANIZATION__c = empHr.GP_ORGANIZATION__c;
                        empMasterList[i].GP_Organization_ID__c = empHr.GP_Org_ID__c;
                        empMasterList[i].GP_Org_TSC_Id__c =empHr.GP_Org_ID__c;
                        empMasterList[i].GP_Payroll_Country__c = empHr.GP_Payroll_Country__c;                        
                        empMasterList[i].GP_Period_Of_Placement_Start__c = empHr.GP_Period_Of_Placement_Start__c;
                        empMasterList[i].GP_PHYSICAL_WORK_LOCATION__c = empHr.GP_PHYSICAL_WORK_LOCATION__c;
                        empMasterList[i].GP_Process_ID__c =empHr.GP_Process_ID__c;
                        empMasterList[i].GP_Process_Name__c = empHr.GP_Process__c;
                        
                        
                        empMasterList[i].GP_Projected_Assignment_End__c = empHr.GP_Projected_Assignment_End__c;
                        empMasterList[i].GP_Resource_Id__c = empHr.GP_Resource_Id__c;                        
                        empMasterList[i].GP_SDO_Name__c = empHr.GP_SDO_Name__c;
                        empMasterList[i].GP_Work_Location_Code__c = empHr.GP_Work_Location_Code__c;                        
                        empMasterList[i].GP_SDO_OracleId__c = empHr.GP_SDO_OracleId__c;
                        empMasterList[i].GP_SERVICE_LINE__c = empHr.GP_SERVICE_LINE__c;           
                        empMasterList[i].GP_Supervisor_Employee__c = empHr.GP_Supervisor_Employee__c;
                        
                       
                        empMasterList[i].GP_SUPERVISOR_OHRID__c = empHr.GP_SUPERVISOR_OHRID__c;
                        empMasterList[i].GP_SUPERVISOR_PERSON_ID__c = empHr.GP_SUPERVISOR_PERSON_ID__c;                        
                        empMasterList[i].GP_Employee_HR_History__c= empHr.Id;
                    }
                    }
                }
			}
            if(empMasterList.size()>0){           
            SetToRunEMPMasterORHRMSTrigger.RunMapEmployeeWithAssignmentOnBasisOfPersonIdOnce='Y';
            update empMasterList;
            }
        }
    }
    //Description : For updating the supervisior lookup on the employeeHRHistory object whenever supervisor
    // person's id is entered    
    //mandeep.singh@saasfocus.com
    ////commented 12052018
   // public void upsertEmployeeHR(Map < Id, SOBject > oldSObjectMap) {
//         List < GP_Employee_HR_History__c > listEmployeeHR = new List < GP_Employee_HR_History__c > ();
//         GP_Employee_HR_History__c oldEmpHrHistory;

//         for (GP_Employee_HR_History__c empHR: (list < GP_Employee_HR_History__c > ) records) {
//             if (empHR != null) {
//                 GP_Employee_HR_History__c empHRnew = new GP_Employee_HR_History__c();
//                 empHRnew.id = empHR.Id;

//                 if (oldSObjectMap != null)
//                     oldEmpHrHistory = (GP_Employee_HR_History__c) oldSObjectMap.get(empHR.Id);
//                 else
//                     oldEmpHrHistory = null;

//                 if (empHR.GP_SUPERVISOR_PERSON_ID__c != null &&
//                     (oldEmpHrHistory != null && empHR.GP_SUPERVISOR_PERSON_ID__c != oldEmpHrHistory.GP_SUPERVISOR_PERSON_ID__c) ||
//                     oldSObjectMap == null) {
//                     GP_Employee_Master__c supervisorEmp = new GP_Employee_Master__c(GP_Person_ID__c = empHR.GP_SUPERVISOR_PERSON_ID__c);
//                     empHRnew.GP_Supervisor_Employee__r = supervisorEmp;
//                     listEmployeeHR.add(empHRnew);
//                 } else if (empHR.GP_SUPERVISOR_PERSON_ID__c == null &&
//                     (oldEmpHrHistory != null && empHR.GP_SUPERVISOR_PERSON_ID__c != oldEmpHrHistory.GP_SUPERVISOR_PERSON_ID__c)) {
//                     empHRnew.GP_Supervisor_Employee__c = null;
//                     listEmployeeHR.add(empHRnew);
//                 }
//             }
//         }
//         Database.upsert(listEmployeeHR, false);
//     }

    //Description : For auto populating the related employeee lookup whenever the user enters the employee person ID in the 
    //employee person ID field 
    //mandeep.singh@saasfocus.com
    public void autoPopulateRelatedEmployee(Map < Id, SObject > oldSObjectMap) {
        map < String, GP_Employee_Master__c > mapofPersonIDandEmployee = new map < String, GP_Employee_Master__c > ();
        Set < String > setofPersonID = new Set < String > ();
        GP_Employee_HR_History__c oldEmpHr;

        for (GP_Employee_HR_History__c empHR: (list < GP_Employee_HR_History__c > ) records) {
            if (empHR != null) {

                if (oldSObjectMap != null)
                    oldEmpHr = (GP_Employee_HR_History__c) oldSObjectMap.get(empHR.Id);
                else
                    oldEmpHr = null;

                if (empHR.GP_Employee_Person_Id__c != null &&
                    ((oldEmpHr != null && empHR.GP_Employee_Person_Id__c != oldEmpHr.GP_Employee_Person_Id__c) ||
                        oldSObjectMap == null)) {
                    setofPersonID.add(empHR.GP_Employee_Person_Id__c);
                } else if (empHR.GP_Employee_Person_Id__c == null &&
                    (oldEmpHr != null && empHR.GP_Employee_Person_Id__c != oldEmpHr.GP_Employee_Person_Id__c)) {
                    empHR.GP_Employees__c = null;
                }
            }
        }

        if (setofPersonID.size() > 0) {
            for (GP_Employee_Master__c emp: (new GPSelectorEmployeeMaster()).getEmployeeListForPersonId(setofPersonID)) {
                mapofPersonIDandEmployee.put(emp.GP_Person_ID__c, emp);
            }
            for (GP_Employee_HR_History__c empHR: (list < GP_Employee_HR_History__c > ) records) {

                if (oldSObjectMap != null)
                    oldEmpHr = (GP_Employee_HR_History__c) oldSObjectMap.get(empHR.Id);
                else
                    oldEmpHr = null;

                if (empHR.GP_Employee_Person_Id__c != null &&
                    ((oldEmpHr != null && empHR.GP_Employee_Person_Id__c != oldEmpHr.GP_Employee_Person_Id__c) ||
                        oldSObjectMap == null)) {

                    if (mapofPersonIDandEmployee != null && mapofPersonIDandEmployee.size() > 0 &&
                        mapofPersonIDandEmployee.get(empHR.GP_Employee_Person_Id__c) != null) {
                        empHR.GP_Employees__c = mapofPersonIDandEmployee.get(empHR.GP_Employee_Person_Id__c).id;
                    } else {
                        empHR.GP_Employees__c = null;
                    }
                }
            }
        }
    }
}