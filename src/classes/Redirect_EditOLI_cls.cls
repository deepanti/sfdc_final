public class Redirect_EditOLI_cls
{
@testVisible
PageReference ReturnPage {get;set;} 
 public Opportunity theOpportunity {get; set;}
    public OpportunityProduct__c opportunityProduct {get; set;}   
    public String oppid {get; set;}
    public String showError {get; set;}

    public Redirect_EditOLI_cls(ApexPages.StandardController controller)
    { 
        opportunityProduct = (OpportunityProduct__c)controller.getRecord();   
    
        opportunityProduct = [Select Name,LastModifiedById,Product__r.Name,ExchangeRate__c,OpportunityId__r.StageName,OpportunityId__r.Opportunity_ID__c,OpportunityId__r.Ownerid,OpportunityId__r.Id,OpportunityId__r.AccountId,ACV__c,COE__c,ContractTerminmonths__c,CreatedById,CurrentYearRevenue__c,DeliveryLocation__c,Description__c,NextYearRevenue__c,OpportunityId__c,Product__c,Quantity__c,
                          Service_Line_Category_OLI__c,Service_Line_OLI__c,Product_Family_OLI__c,RevenueStartDate__c,FTE__c,   EndDate__c,Total_ACV__c,Product_Autonumber__c,SalesExecutive__c,P_L_SUB_BUSINESS__c,SEP__c,TotalContractValue__c,Quarterly_FTE_1st_month__c,FTE_4th_month__c,FTE_7th_month__c,FTE_10th_month__c,TotalPrice__c,
                          Replicate_Product__c,UnitPrice__c, LocalCurrency__c, OpportunityId__r.Industry_Vertical__c,TCVLocal__c,OpportunityID__r.QSRM_status__c,Digital_Offerings__c,Product_family_Lookup__c ,Service_Line_lookup__c ,Nature_of_Work_lookup__c,Product_family_Lookup__r.name ,Service_Line_lookup__r.name ,Nature_of_Work_lookup__r.name,Analytics__c   from OpportunityProduct__c where Id =: controller.getRecord().Id];
   
        theOpportunity = [select Id, Name, Roll_up_QSRM_App_Rej__c, Approved__c, QSRM_Status__c, account.Industry_Vertical__c, Revenue_Start_Date__c,End_Date__c, StageName,Pricebook2Id,Contract_Term_in_months__c,Margin__c,Industry_Vertical__c,CloseDate,Project_Margin__c,Priority_Types__c, Pricebook2.Name  from Opportunity where Id =:opportunityProduct.OpportunityId__c];

        if( theOpportunity.QSRM_Status__c=='Your QSRM is submitted for Approval')
        {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'Since QSRM is in Approval status,you cannot edit the deal'));     
            oppid = opportunityProduct.OpportunityId__c;
            showError = 'True';
        }
    }
}