@isTest
private class GENT_OpportunityUpdatePrediscoverTest
{
    /*public static testMethod void RunTest()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Pre_discover__c oPre_Discover = GEN_Util_Test_Data.CreatePre_discover('Test');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        User u2 =GEN_Util_Test_Data.CreateUser('stfsdfsdfsd5@testorg.com',p.Id,'stfsdfsdfsd5@testorg.com' );
        
        AccountArchetype__c TemoObj = GEN_Util_Test_Data.createNewAccountArchetype(u2.id,oAccount.id,oAT.id);
        TemoObj.Team_Member_Role__c='GRM';
        update TemoObj;
        
        oOpportunity.stageName='2. Define';
        Update  oOpportunity;
    }
    */
    Static testmethod void runtest2()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        
        
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='',Transformation_stale_deal_check__c=true,Nature_of_Work_highest__c = 'Consulting');
        insert opp;
        
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        Nature_of_Work__c now = new Nature_of_Work__c(Name='Consulting');
        insert now;
        OpportunityProduct__c OLi=new OpportunityProduct__c(tcvlocal__c = 1000000,Nature_of_Work_lookup__c = now.id,Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Analytics',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        //opp.Nature_of_Work_highest__c='Delivery';
        opp.Transformation_stale_deal_check__c=true;
        
        opp.stagename='2. Define';
        Update opp;
        
        //opp.stagename='3. On Bid';
       // update opp;
        
        Deal_Cycle__c   DC= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='1. Discover',Type_of_Deal__c='Retail',Ageing__c=30);
        Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='1. Discover',Type_of_Deal__c='Large',Ageing__c=45);            
        Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Retail',Ageing__c=20); 
        Deal_Cycle__c   DC3= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Large',Ageing__c=30);                       
        
        Insert DC;
        Insert DC1;
        Insert DC2;
        Insert DC3;
        
    }
    
    /*
    public static testMethod void testMethodOne(){
        
        //create product
        Product2 oProduct = new Product2();
        oProduct.Name='Additional Client Licenses';
        oProduct.IsActive=true;
        insert oProduct;
        
        
        
        // create business segment
        Business_Segments__c oBS = new Business_Segments__c();
        oBS.Name = 'ABC';
        oBS.For__c = 'Genpact';
        insert oBS;
        
        Archetype__c oArchtype = new Archetype__c();
        oArchtype.Name='ABC';
        insert oArchtype;
        
        // Create Sub_Business__c
        
        Sub_Business__c oSB = new Sub_Business__c();
        oSB.name ='ABC';
        oSB.for__c = 'Genpact';
        oSB.Business_Segment__c = oBS.Id;
        insert oSB;  
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        UserRole ur = new UserRole(Name = 'Shared Services');
        User oUser = new User();
        oUser.Alias = 'Test';
        oUser.Email = 'aa@a.com';
        oUser.EmailEncodingKey='UTF-8';
        oUser.LastName='Test';
        oUser.LanguageLocaleKey='en_US';
        oUser.LocaleSidKey='en_US';
        Ouser.ProfileId=p.Id;
        Ouser.CommunityNickname='test';
        oUser.OHR_ID__c='700060235';
        oUser.TimeZoneSidKey='America/Los_Angeles';
        oUser.UserName='aa@a.com.a';
        oUser.UserRoleId =ur.Id;
        insert oUser ;
        
        User oUser1 = new User();
        oUser1.Alias = 'Test1';
        oUser1.Email = 'aa1@a.com';
        oUser1.EmailEncodingKey='UTF-8';
        oUser1.LastName='Test';
        oUser1.LanguageLocaleKey='en_US';
        oUser1.LocaleSidKey='en_US';
        Ouser1.ProfileId=p.Id;
        Ouser1.CommunityNickname='test1';
        oUser1.OHR_ID__c='700060234';
        oUser1.TimeZoneSidKey='America/Los_Angeles';
        oUser1.UserRoleId =ur.Id;
        oUser1.UserName='aa1@a.com.a';
        insert oUser1 ;
        
        Sales_Unit__c SalesUnitObj = new Sales_Unit__c();
        SalesUnitObj.Name = 'ABC';
        SalesUnitObj.Sales_Leader__c = oUser1.Id;
        SalesUnitObj.Sales_Unit_Group__c = 'Sales Unit Head';     
        insert SalesUnitObj;
        
        //create account
        Account oAccount = new Account();
        oAccount.Name = 'ABC';
        oAccount.AccountNumber = '123';
        oAccount.Business_group__c= 'Genpact'; // picklist field
        oAccount.business_segment__c = oBS.Id;
        oAccount.sub_business__c= oSB.Id;
        oAccount.Sales_Unit__c=SalesUnitObj.Id;
        oAccount.industry_vertical__c= 'BFS';
        oAccount.sub_industry_vertical__c= 'BFS';
        //oAccount.Account_classification__c= strAccount_classification;
        //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
        oAccount.website = 'www.genpact.com';
        oAccount.sic= 'test code';
        oAccount.hunting_mining__c= 'test mining';
        oAccount.TAM__c=5000;
        oAccount.PTAM__c=6000;
        oAccount.Archetype__c=oArchtype.Id;
        //oAccount.DUNS_Number__c=454554;    
        oAccount.Client_Partner__c=oUser.Id;
        oAccount.Primary_Account_GRM__c=oUser1.Id;
        oAccount.TickerSymbol= 'test ticker';
        oAccount.AnnualRevenue= 50000;
        oAccount.ownerid=oUser1.Id;
        oAccount.Ownership= 'test Owner';
        oAccount.Account_headquarters_country__c= 'test Account headquarters';
        oAccount.Account_Owner_from_ACR__c=oUser.Id;
        //oAccount.base_country__c= 'test base country';
        insert oAccount;
        
        Account oAccount1 = new Account();
        oAccount1.Name = 'ABCD';
        oAccount1.AccountNumber = '1234';
        oAccount1.Business_group__c= 'Genpact'; // picklist field
        oAccount1.business_segment__c = oBS.Id;
        oAccount1.sub_business__c= oSB.Id;
        oAccount1.industry_vertical__c= 'BFS';
        oAccount1.sub_industry_vertical__c= 'BFS';
        //oAccount.Account_classification__c= strAccount_classification;
        //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
        oAccount1.website = 'www.genpact.com';
        oAccount1.sic= 'test code';
        oAccount1.hunting_mining__c= 'test mining';
        oAccount1.TAM__c=5000;
        oAccount1.PTAM__c=6000;
        oAccount1.Archetype__c=oArchtype.Id;
        //oAccount.DUNS_Number__c=454554;    
        oAccount1.Client_Partner__c=oUser.Id;
        oAccount1.Primary_Account_GRM__c=oUser1.Id;
        oAccount1.TickerSymbol= 'test ticker';
        oAccount1.AnnualRevenue= 50000;
        oAccount1.ownerid=oUser1.Id;
        oAccount1.Ownership= 'test Owner';
        oAccount1.Account_headquarters_country__c= 'test Account headquarters';
        oAccount1.Account_Owner_from_ACR__c=oUser.Id;
        //oAccount.base_country__c= 'test base country';
        insert oAccount1;
        
        Contact oContact = new Contact();
        oContact.FirstName = 'ABC';
        oContact.LastName = 'XYZ';
        oContact.AccountId = oAccount.id;
        oContact.Title = 'Mr';
        oContact.LeadSource = 'Email';
        oContact.Email = 'aa@a.com';  
        oContact.Phone = '1212121212';
        insert oContact;
        
        Opportunity oOpportunity1 = new Opportunity();
        oOpportunity1.Name = 'ABC';
        oOpportunity1.AccountId = oAccount.id;
        oOpportunity1.StageName = '1. Discover';
        oOpportunity1.Target_Source__c = 'Consulting Pull Through';
        oOpportunity1.Opportunity_Origination__c='Proactive';
        oOpportunity1.Sales_Region__c='India';
        oOpportunity1.Sales_country__c = 'India';
        oOpportunity1.Deal_Type__c = 'Sole Sourced';
        oOpportunity1.Sales_Leader__c = Null;
        //oOpportunity1.Is_RS_parameter_approve__c = False;
        oOpportunity1.CloseDate = System.today().adddays(1);
        oOpportunity1.Revenue_Start_Date__c = System.today().adddays(1);
        oOpportunity1.Contract_Term_in_months__c=36;
        oOpportunity1.Advisor_Firm__c = 'Trestle';
        oOpportunity1.Contact__c = oContact.id;
        oOpportunity1.NextStep='b';
        oOpportunity1.OwnerId=oUser1.Id;
        system.debug('Opportunity owner before insert'+oOpportunity1.Owner);
        insert oOpportunity1;
        
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c='1';
        oOpportunityProduct.Service_line_OLI__c='11';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = 'Doc Management';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.COE__c = 'IMS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Auto';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(4);
        oOpportunityProduct.ContractTerminmonths__c = 12;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity1.Id;
        oOpportunityProduct.TCVLocal__c =2  ;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
        
        oOpportunity1.Name = 'ABC';
        oOpportunity1.AccountId = oAccount.id;
        // oOpportunity1.StageName = '2. Define';
        
        oOpportunity1.Target_Source__c = 'Consulting Pull Through';
        oOpportunity1.Opportunity_Origination__c='Proactive';
        oOpportunity1.Sales_Region__c='India';
        oOpportunity1.Sales_country__c = 'India';
        oOpportunity1.Deal_Type__c = 'Sole Sourced';
        oOpportunity1.Sales_Leader__c = Null;
        //oOpportunity1.Is_RS_parameter_approve__c = False;
        oOpportunity1.CloseDate = System.today().adddays(1);
        oOpportunity1.Revenue_Start_Date__c = System.today().adddays(4);
        oOpportunity1.Contract_Term_in_months__c=36;
        
        
        oOpportunity1.Advisor_Firm__c = 'Trestle';
        oOpportunity1.Contact__c = oContact.id;
        oOpportunity1.NextStep='b';
        oOpportunity1.OwnerId=oUser.Id;
        system.debug('Opportunity owner before update'+oOpportunity1.Owner);
        update oOpportunity1;
        
        
        system.debug('Revenue111'+oOpportunity1.Revenue_Start_Date__c);
        task t = new task();    
        t.OwnerId = UserInfo.getUserId();
        t.Subject='Next Step Note';
        t.Status='Not Started';
        t.Priority='Normal';
        insert t;
        t.WhatId=oOpportunity1.Id;
        t.Status='Completed';
        
        update t;
        
    } 

    public static testMethod void testMethod2(){
        
        //create product
        Product2 oProduct = new Product2();
        oProduct.Name='Additional Client Licenses';
        oProduct.IsActive=true;
        insert oProduct;
        
        
        
        // create business segment
        Business_Segments__c oBS = new Business_Segments__c();
        oBS.Name = 'ABC';
        oBS.For__c = 'Genpact';
        insert oBS;
        
        Archetype__c oArchtype = new Archetype__c();
        oArchtype.Name='ABC';
        insert oArchtype;
        
        // Create Sub_Business__c
        
        Sub_Business__c oSB = new Sub_Business__c();
        oSB.name ='ABC';
        oSB.for__c = 'Genpact';
        oSB.Business_Segment__c = oBS.Id;
        insert oSB;  
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        UserRole ur = new UserRole(Name = 'Shared Services');
        User oUser = new User();
        oUser.Alias = 'Test';
        oUser.Email = 'aa@a.com';
        oUser.EmailEncodingKey='UTF-8';
        oUser.LastName='Test';
        oUser.LanguageLocaleKey='en_US';
        oUser.LocaleSidKey='en_US';
        Ouser.ProfileId=p.Id;
        Ouser.CommunityNickname='test';
        oUser.OHR_ID__c='700060235';
        oUser.TimeZoneSidKey='America/Los_Angeles';
        oUser.UserName='aa@a.com.a';
        oUser.UserRoleId =ur.Id;
        insert oUser ;
        
        User oUser1 = new User();
        oUser1.Alias = 'Test1';
        oUser1.Email = 'aa1@a.com';
        oUser1.EmailEncodingKey='UTF-8';
        oUser1.LastName='Test';
        oUser1.LanguageLocaleKey='en_US';
        oUser1.LocaleSidKey='en_US';
        Ouser1.ProfileId=p.Id;
        Ouser1.CommunityNickname='test1';
        oUser1.OHR_ID__c='700060234';
        oUser1.TimeZoneSidKey='America/Los_Angeles';
        oUser1.UserRoleId =ur.Id;
        oUser1.UserName='aa1@a.com.a';
        insert oUser1 ;
        
        Sales_Unit__c SalesUnitObj = new Sales_Unit__c();
        SalesUnitObj.Name = 'ABC';
        SalesUnitObj.Sales_Leader__c = oUser1.Id;
        SalesUnitObj.Sales_Unit_Group__c = 'Sales Unit Head';     
        insert SalesUnitObj;
        
        //create account
        Account oAccount = new Account();
        oAccount.Name = 'ABC';
        oAccount.AccountNumber = '123';
        oAccount.Business_group__c= 'Genpact'; // picklist field
        oAccount.business_segment__c = oBS.Id;
        oAccount.sub_business__c= oSB.Id;
        oAccount.Sales_Unit__c=SalesUnitObj.Id;
        oAccount.industry_vertical__c= 'BFS';
        oAccount.sub_industry_vertical__c= 'BFS';
        //oAccount.Account_classification__c= strAccount_classification;
        //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
        oAccount.website = 'www.genpact.com';
        oAccount.sic= 'test code';
        oAccount.hunting_mining__c= 'test mining';
        oAccount.TAM__c=5000;
        oAccount.PTAM__c=6000;
        oAccount.Archetype__c=oArchtype.Id;
        //oAccount.DUNS_Number__c=454554;    
        oAccount.Client_Partner__c=oUser.Id;
        oAccount.Primary_Account_GRM__c=oUser1.Id;
        oAccount.TickerSymbol= 'test ticker';
        oAccount.AnnualRevenue= 50000;
        oAccount.ownerid=oUser1.Id;
        oAccount.Ownership= 'test Owner';
        oAccount.Account_headquarters_country__c= 'test Account headquarters';
        oAccount.Account_Owner_from_ACR__c=oUser.Id;
        //oAccount.base_country__c= 'test base country';
        insert oAccount;
        
        Account oAccount1 = new Account();
        oAccount1.Name = 'ABCD';
        oAccount1.AccountNumber = '1234';
        oAccount1.Business_group__c= 'Genpact'; // picklist field
        oAccount1.business_segment__c = oBS.Id;
        oAccount1.sub_business__c= oSB.Id;
        oAccount1.industry_vertical__c= 'BFS';
        oAccount1.sub_industry_vertical__c= 'BFS';
        //oAccount.Account_classification__c= strAccount_classification;
        //oAccount.forbes_2000_company_name__c= strForbes_Company_Name;
        oAccount1.website = 'www.genpact.com';
        oAccount1.sic= 'test code';
        oAccount1.hunting_mining__c= 'test mining';
        oAccount1.TAM__c=5000;
        oAccount1.PTAM__c=6000;
        oAccount1.Archetype__c=oArchtype.Id;
        //oAccount.DUNS_Number__c=454554;    
        oAccount1.Client_Partner__c=oUser.Id;
        oAccount1.Primary_Account_GRM__c=oUser1.Id;
        oAccount1.TickerSymbol= 'test ticker';
        oAccount1.AnnualRevenue= 50000;
        oAccount1.ownerid=oUser1.Id;
        oAccount1.Ownership= 'test Owner';
        oAccount1.Account_headquarters_country__c= 'test Account headquarters';
        oAccount1.Account_Owner_from_ACR__c=oUser.Id;
        //oAccount.base_country__c= 'test base country';
        insert oAccount1;
        
        Contact oContact = new Contact();
        oContact.FirstName = 'ABC';
        oContact.LastName = 'XYZ';
        oContact.AccountId = oAccount.id;
        oContact.Title = 'Mr';
        oContact.LeadSource = 'Email';
        oContact.Email = 'aa@a.com';  
        oContact.Phone = '1212121212';
        insert oContact;
        
        Opportunity oOpportunity1 = new Opportunity();
        oOpportunity1.Name = 'ABC';
        oOpportunity1.AccountId = oAccount.id;
        oOpportunity1.StageName = 'Prediscover';
        oOpportunity1.Target_Source__c = 'Consulting Pull Through';
        oOpportunity1.Opportunity_Origination__c='Proactive';
        oOpportunity1.Sales_Region__c='India';
        oOpportunity1.Sales_country__c = 'India';
        oOpportunity1.Deal_Type__c = 'Sole Sourced';
        oOpportunity1.Sales_Leader__c = Null;
        //oOpportunity1.Is_RS_parameter_approve__c = False;
        oOpportunity1.CloseDate = System.today().adddays(1);
        oOpportunity1.Revenue_Start_Date__c = System.today().adddays(1);
        oOpportunity1.Contract_Term_in_months__c=36;
        oOpportunity1.Advisor_Firm__c = 'Trestle';
        oOpportunity1.Contact__c = oContact.id;
        oOpportunity1.NextStep='b';
        oOpportunity1.OwnerId=oUser1.Id;
        system.debug('Opportunity owner before insert'+oOpportunity1.Owner);
        insert oOpportunity1;
        Nature_of_work__c now = new Nature_of_work__c(Name='Consulting');
        insert now;
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c='1';
        oOpportunityProduct.Nature_of_Work_lookup__c = now.id;
        oOpportunityProduct.Service_line_OLI__c='11';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = 'Doc Management';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.COE__c = 'IMS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Auto';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(4);
        oOpportunityProduct.ContractTerminmonths__c = 12;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity1.Id;
        oOpportunityProduct.TCVLocal__c =2  ;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
        
        oOpportunity1.Name = 'ABC';
        oOpportunity1.AccountId = oAccount.id;
        oOpportunity1.StageName = '1. Discover';
        
        oOpportunity1.Target_Source__c = 'Consulting Pull Through';
        oOpportunity1.Opportunity_Origination__c='Proactive';
        oOpportunity1.Sales_Region__c='India';
        oOpportunity1.Sales_country__c = 'India';
        oOpportunity1.Deal_Type__c = 'Sole Sourced';
        oOpportunity1.Sales_Leader__c = Null;
        //oOpportunity1.Is_RS_parameter_approve__c = False;
        oOpportunity1.CloseDate = System.today().adddays(1);
        oOpportunity1.Revenue_Start_Date__c = System.today().adddays(4);
        oOpportunity1.Contract_Term_in_months__c=36;
        
        
        oOpportunity1.Advisor_Firm__c = 'Trestle';
        oOpportunity1.Contact__c = oContact.id;
        oOpportunity1.NextStep='b';
        oOpportunity1.OwnerId=oUser.Id;
        oOpportunity1.Transformation_stale_deal_check__c = true;
        system.debug('Opportunity owner before update'+oOpportunity1.Owner);
        Test.startTest();
        update oOpportunity1;
        Test.stopTest();
        
    }
    */
}