global class GPBatchQualifiedDealsNotification implements Database.Batchable < sObject > , Database.Stateful {
    
    global List < GP_Employee_Master__c > oppProjectLeadershipEmployees = new List < GP_Employee_Master__c > ();
    global List < GP_Opportunity_Project__c > oppProjects = new List < GP_Opportunity_Project__c > ();

    global Messaging.SingleEmailMessage message;
    global List < Messaging.SingleEmailMessage > EmaiTolist;
    global List < String > listofemails;
    
    EmailTemplate EmpTemplate = [Select id, DeveloperName, body, subject, htmlvalue
        from EmailTemplate where
        developername = 'GP_Email_Template_for_Opp_85'
        limit 1
    ];

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([select id, Name, GP_Opportunity_Name__c, 
                                        GP_Qualified_Notification_Sent__c, GP_Oracle_PID__c, GP_Opportunity_Id__c,
            (select id, GP_Delivery_Org__c, GP_Sub_Delivery_Org__c, 
             GP_Nature_of_Work__c, GP_OMS_Deal_ID__c from Deals__r) 
            from GP_Opportunity_Project__c where GP_Qualified_Notification_Sent__c = false]);
    }

    global void execute(Database.BatchableContext bc, List < GP_Opportunity_Project__c > oppProjectRecords) {
        if (oppProjectRecords != null && oppProjectRecords.size() > 0) {
            Set < String > empPersonIds = new Set < String > ();
            oppProjects = oppProjectRecords;

            for (GP_Opp_Project_Default__mdt oppProjectLeadership: [select GP_Emp_Person_Id__c from GP_Opp_Project_Default__mdt]) {
                empPersonIds.add(oppProjectLeadership.GP_Emp_Person_Id__c);
            }
            if (empPersonIds.size() > 0) {
                oppProjectLeadershipEmployees = new GPSelectorEmployeeMaster().getEmployeeListForOHRId(empPersonIds);
            }
        }

    }
    global void finish(Database.BatchableContext bc) {
        if (oppProjectLeadershipEmployees != null && oppProjectLeadershipEmployees.size() > 0) {
            try {
                getEmailbody();
                //system.debug('@EmaiTolist_____' + EmaiTolist);
                if (EmaiTolist != null && EmaiTolist.size() > 0 && GPCommon.allowSendingMail()) {
                    Messaging.SendEmailResult[] EHRBP = Messaging.sendEmail(EmaiTolist);
                }
            } catch (Exception ex) {
                system.debug('@Exception :: ' + ex.getMessage());
            }
        }
    }
    public void getEmailbody() {

        EmaiTolist = new list < Messaging.SingleEmailMessage > ();
        Id orgWideEmailId = GPCommon.getOrgWideEmailId();
        for (GP_Employee_Master__c employee: oppProjectLeadershipEmployees) {
            GPCommon objcommon = new GPCommon();

            message = objcommon.notificationThroughEmailForBatch(EmpTemplate, employee, 'GP_Field_Set_Emp_Batch_For_Email_Notific', 'GP_Employee_Master__c');

            message.setHtmlBody(message.getHtmlBody().replace('LIST_QUALIFIED_DEAL_RECORDS', createProjectTableString(oppProjects)));

            listofemails = new list < string > ();
            //system.debug('message::' + message.getHtmlBody());
            if (employee.GP_ACTUAL_TERMINATION_Date__c != null) {
                if (employee.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                    listofemails.add(employee.GP_OFFICIAL_EMAIL_ADDRESS__c);
                    //system.debug('email address of emp@@' + employee.GP_OFFICIAL_EMAIL_ADDRESS__c);
                }
                if (listofemails != null && listofemails.size() > 0) {
                    message.setToAddresses(listofemails);
                }
                if (message != null) {
                    // since a single settargetobjectid can only be added in a message so 
                    // they are added individualy in the individual message above
                    message.settargetobjectid(null);
                    if(orgWideEmailId != null)
                    	message.setOrgWideEmailAddressId(orgWideEmailId);
                    EmaiTolist.add(message);
                }
            }
        }

    }
    private String createProjectTableString(List < GP_Opportunity_Project__c > oppProjects) {
        String html = '';
        if (oppProjects.size() > 0) {
            html += ' <table  width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">';
            html += '   <thead>';
            html += '       <tr>';
            html += '           <th bgcolor="#005595" style="color:white">Opp ID</th>';
            html += '           <th bgcolor="#005595" style="color:white">Deal ID</th>';
            html += '           <th bgcolor="#005595" style="color:white">Opportunity Name/s</th>';
            html += '           <th bgcolor="#005595" style="color:white" >Delivery Org</th>';
            html += '           <th bgcolor="#005595" style="color:white" >Sub Delivery Org</th>';
            html += '           <th bgcolor="#005595" style="color:white" >Nature of Work</th>';
            html += '       </tr>';
            html += '   </thead>';
            html += '   <tbody>';
            for (GP_Opportunity_Project__c oppProject: oppProjects) {
                for (GP_Deal__c deal: oppProject.Deals__r) {
                    html += '       <tr>';
                    html += '           <td>' + oppProject.GP_Opportunity_Id__c + '</td>';
                    html += '           <td>' + deal.GP_OMS_Deal_ID__c + '</td>';
                    html += '           <td>' + oppProject.GP_Opportunity_Name__c + '</td>';
                    html += '           <td>' + deal.GP_Delivery_Org__c + '</td>';
                    html += '           <td>' + deal.GP_Sub_Delivery_Org__c + '</td>';
                    html += '           <td>' + deal.GP_Nature_of_Work__c + '</td>';
                    html += '       </tr>';
                }
            }
            html += ' </tbody></table>';
        }
        return html;
    }

}