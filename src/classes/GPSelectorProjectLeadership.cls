public class GPSelectorProjectLeadership extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Project_Leadership__c.Name
           
            };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Leadership__c.sObjectType;
    }

    public List<GP_Project_Leadership__c> selectById(Set<ID> idSet) {
        return (List<GP_Project_Leadership__c>) selectSObjectsById(idSet);
    }
    
    public GP_Project_Leadership__c selectById(ID leadershipId) {
        Set<Id> idSet = new Set<Id> {leadershipId};
        List<GP_Project_Leadership__c> listOfProjectLeadership = (List<GP_Project_Leadership__c>) selectSObjectsById(idSet);
        return listOfProjectLeadership.get(0);
    }
    
    public List<GP_Project_Leadership__c> selectByProjectId(ID projectId) {
        return [SELECT Id, 
                Name, 
                GP_Project__c, 
                GP_Project__r.Name, 
                GP_Leadership_Master__c,
                GP_Active__c,
                Is_Cloned_from_Parent__c,
                GP_Leadership_Master__r.Name,    
                GP_Employee_ID__c,  
                GP_Employee_ID__r.Name,
                GP_End_Date__c, 
                GP_Pinnacle_End_Date__c, 
                GP_Start_Date__c, 
                RecordTypeId, 
                RecordType.Name, 
                GP_Parent_Project_Leadership__c,
                GP_Leadership_Role__c, 
                GP_Leadership_Role_Name__c,
                GP_Type_of_Leadership__c
                FROM GP_Project_Leadership__c
                where GP_Project__c = :projectId
                ORDER By GP_Start_Date__c ASC];
    }
    
     public List<GP_Project_Leadership__c> selectByProjectLeaderShipId(set<Id> setPLId) 
     {
        return [SELECT Id, Name, GP_Project__c, GP_Project__r.Name, GP_Leadership_Master__c,
                GP_Leadership_Master__r.Name,    GP_Employee_ID__c,  GP_Employee_ID__r.Name,
                GP_End_Date__c, GP_Pinnacle_End_Date__c, GP_Start_Date__c, RecordTypeId, RecordType.Name, GP_Parent_Project_Leadership__c,
                GP_Leadership_Role__c, GP_Leadership_Role_Name__c
                FROM GP_Project_Leadership__c
                where id = :setPLId];
    }

    public List<GP_Project_Leadership__c> selectActiveLeadershipByProjectId(ID projectId) {
        return [SELECT Id, 
                        Name, 
                        RecordTypeId, 
                        GP_Project__c, 
                        GP_End_Date__c, 
                        RecordType.Name, 
                        GP_Start_Date__c,
                        GP_Employee_ID__c,  
                        GP_Project__r.Name, 
                        GP_Leadership_Role__c, 
                        GP_Employee_ID__r.Name,
                        GP_Leadership_Master__c,
                        GP_Pinnacle_End_Date__c,
                        GP_Type_of_Leadership__c,
                        GP_Employee_Field_Type__c,
                        GP_Leadership_Role_Name__c,
                        GP_Leadership_Master__r.Name,
                        GP_Parent_Project_Leadership__c,
                        GP_Editable_after_PID_Creation__c,    
                        GP_Employee_ID__r.GP_Employee_Unique_Name__c 
                FROM GP_Project_Leadership__c
                where GP_Project__c = :projectId
                and GP_Active__c = true];
    }
    
    public List<GP_Project_Leadership__c> selectByListOfProjectId(List<Id> listOfProjectId) {
        return [SELECT Id, 
                GP_Employee_ID__c, 
                GP_Employee_ID__r.Name,
                GP_Employee_Field_Type__c,
                GP_Leadership_Role__c, 
                GP_Start_Date__c,
                RecordType.Name,
                GP_End_Date__c, 
                GP_Project__c, 
                GP_Pinnacle_End_Date__c,
                GP_Type_of_Leadership__c,
                GP_Leadership_Role_Name__c,
                GP_Last_Temporary_Record_Id__c
                FROM GP_Project_Leadership__c
                WHERE GP_Project__c in :listOfProjectId
                and GP_Active__c = true];
    }
}