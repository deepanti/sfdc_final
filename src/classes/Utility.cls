/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────
* This Class Use for Dynamic get the vlaue
* 
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author         Vikas Chand   <vikas.chand@genpact.com>
* Version 1
* Created Date : 9/26/2018
* ──────────────────────────────────────────────────────────────────────────────────────────────────
*/

public class Utility {
    /*This method use for get the dynamic picklist value send the object Name and field Name*/
    public static List<String> getPickListValue(string  sObjectName, String fieldName) {
        try{
            List<String> picklistValue = new List<String>();
            Map <String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
			Schema.SObjectType objType = gd.get(sObjectName);
            Schema.DescribeSObjectResult objDescribe = objType.getDescribe();  
            map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
            list<Schema.PicklistEntry> values =  fieldMap.get(fieldName).getDescribe().getPickListValues();
            for( Schema.PicklistEntry f : values)
            {
                picklistValue.add(f.getValue());
            }    
            return picklistValue; 
        } catch(Exception e){ 
            system.debug('ERROR=='+e.getStackTraceString()+'  '+e.getMessage()); 
        } 
        return NULL;
    }
    
}