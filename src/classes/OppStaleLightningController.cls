public class OppStaleLightningController {
    
    /*******************************************************************
MethodName: getLeadRecord()
Purpose:    Retrieves max of 50 leads to display it in component.                                                       
Parameters: None
Returns: List of Leads(50)
Throws [Exceptions]: None                                                        
********************************************************************/
    
    @AuraEnabled
    public static List<Opportunity> getOppRecord() {
        String query;
        String today_date;
        today_date = System.now().format('yyyy-MM-dd');
        system.debug('today_date=='+today_date);
        String pipelinestages= '\'1. Discover\',\'2. Define\',\'3. On bid\',\'4. Down Select\',\'5. Confirmed\'';
        Query='select id,ownerid,stagename,closeDate,revenue_start_date__c,name,owner.name,TCV1__c from Opportunity where stagename IN ('+pipelinestages+') AND closeDate < LAST_N_DAYS:1 AND ownerid = \''+ UserInfo.getUserId() +'\' order by closeDate asc';
        List<Opportunity> oppList=Database.query(Query);
        //return [SELECT id, name,closedate,revenue_start_date__c,Owner.name,StageName FROM Opportunity LIMIT 50];
        return oppList;
    }
    
    
    
    /*******************************************************************
MethodName: massUpdateLeads(String recIds,String Status)
Purpose:    Updates the Status for the selected leads based on the parameters.
Due to limitations of Lightning component on passing Array as parameters
recIds is deserialized and added to list and later used to update the status 
which is available as an another parameter.
Parameters: String recIds,String Status
Returns: List of Leads(50)
Throws [Exceptions]: None                                                        
********************************************************************/
    @AuraEnabled
    public static List<Opportunity> massUpdateOpps(String recIds,String Status) {
        
        system.debug('recIds: '+ recIds);
        string [] values = (String[])System.JSON.deserialize(recIds,String[].class);
        List<Opportunity> opplistToUpdate = new List<Opportunity> ();
        for(Opportunity o : [SELECT Id,Owner.name,Move_Deal_Ahead__c,StageName from Opportunity where id IN:values]){
            o.Move_Deal_Ahead__c = Status;
            opplistToUpdate.add(o);   
        }
        try{
            update opplistToUpdate;
        }
        catch(DMLException e)
        {	
            throw new AuraHandledException('There is an error with one or more Opportunity.');
        }
            String query;
            String today_date;
            today_date = System.now().format('yyyy-MM-dd');
            system.debug('today_date=='+today_date);
            String pipelinestages= '\'1. Discover\',\'2. Define\',\'3. On bid\',\'4. Down Select\',\'5. Confirmed\'';
            Query='select id,ownerid,stagename,closeDate,revenue_start_date__c,name,owner.name,TCV1__c from Opportunity where stagename IN ('+pipelinestages+') AND closeDate < LAST_N_DAYS:1 AND ownerid = \''+ UserInfo.getUserId() +'\'';
            List<Opportunity> oppList=Database.query(Query);
            //return [SELECT id, name,closedate,revenue_start_date__c,Owner.name,StageName FROM Opportunity LIMIT 50];
            return oppList;
        
    }
        
}