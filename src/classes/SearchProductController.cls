public class SearchProductController {
    
   
   @auraEnabled
  	public static ProductListWrapper getProducts(ID opportunityID){
        try{
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
            String industryVertical = opportunity_list[0].Industry_Vertical__c;
            
            if(opportunity_list.size() > 0){  
                System.debug('custom label=='+label.Product_Nature_of_Work);
                 Map<ID, Product2> product_map= new Map<ID, Product2>([SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c FROM Product2 
                                                                      WHERE Industry_Vertical__c = :industryVertical 
                                                                      AND Nature_of_Work__c != :label.Product_Nature_of_Work
                                                                      AND isActive = true]);              
              
                System.debug('total_product_map.keySet()=='+product_map.size());
                
                List<Product2> product_list = getProductsHavingPriceBookEntry(product_map);
                
                ProductListWrapper wrapper = new ProductListWrapper();                
                
                wrapper.total = product_list.size();
                system.debug('wrapper.total=='+wrapper.total);
                wrapper.productList = product_list; 
                System.debug('wrapper.productList=='+wrapper.productList);
                
                return wrapper;
            }
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
        }
        return NULL;
    }   
    
    @auraEnabled
    public static List<String> getProductFields(){
        try{
            return new List<String>(getProductFieldMap().keyset());   
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
    @auraEnabled
    public static ProductListWrapper getSearchedProduct(ID opportunityID, String searchFilter, String keyword){
        
        try{
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
        	String industryVertical = opportunity_list[0].Industry_Vertical__c;
            List<Product2> product_list = new List<Product2>();
             List<Product2> proList;            
           
            System.debug('keyword=='+keyword);
            
            if(opportunity_list.size() > 0){
                if(searchFilter == 'BY_KEYWORD'){
                    System.debug('in keyword search');
                    proList= [SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c
                                              FROM Product2 WHERE Industry_Vertical__c =
                                              :industryVertical AND (Name LIKE:'%'+keyword+'%' 
                                   				OR Nature_of_Work__c LIKE:'%'+keyword+'%'OR Service_Line__c LIKE:'%'+keyword+'%') 
                             					AND Nature_of_Work__c != :label.Product_Nature_of_Work AND isActive = true];    
                    system.debug('product_list in search product=='+product_list);    
                }
                if(searchFilter == 'BY_FILED_FILTER'){          
                   
                    String Query = 'SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c'+
                                   ' FROM Product2 WHERE Industry_Vertical__c = \''+industryVertical
                                    +'\' AND '+getQueryString(keyword) + ' AND Nature_of_Work__c != \''
                        			+label.Product_Nature_of_Work + '\' AND isActive ='+true ;
                    System.debug('Query===1==='+Query);
                    
                    proList = Database.query(query);
                 }
                product_list = getProductsHavingPriceBookEntry(new Map<ID, Product2>(proList)); 
               	ProductListWrapper wrapper = new ProductListWrapper();
                wrapper.total = product_list.size();
                wrapper.productList = product_list;
                return wrapper;
            }    
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
        
    }
    
    @auraEnabled
    public static ProductListWrapper getSearchedProduct2(ID opportunityID, String searchFilter, String keyword, String filterList){
        try{
            List<Opportunity> opportunity_list = [SELECT Industry_Vertical__c FROM Opportunity WHERE ID =:opportunityID];
           	String industryVertical = opportunity_list[0].Industry_Vertical__c;
            List<Product2> product_list = new List<Product2>();            
           
            
           String Query = 'SELECT ID, Name, Nature_of_Work__c, Service_Line__c, Industry_Vertical__c'+
                ' FROM Product2 WHERE Industry_Vertical__c = \''+opportunity_list[0].Industry_Vertical__c
                +'\' AND '+getqueryString(filterList) +' AND (Name LIKE \'%'+keyword+'%\' OR Nature_of_Work__c LIKE \'%'+keyword+'%\' '+
                ' OR Service_Line__c LIKE \'%'+keyword+'%\') AND Nature_of_Work__c != \''+label.Product_Nature_of_Work
                +'\'AND isActive = '+true;
            
            System.debug('Query===2==='+Query);
            
            List<Product2> proList = Database.query(query);
            
            product_list = getProductsHavingPriceBookEntry(new Map<ID, Product2>(proList));
            
            ProductListWrapper wrapper = new ProductListWrapper();
            wrapper.total = product_list.size();
            wrapper.productList = product_list;
            return wrapper;
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
   /* @auraEnabled
    public static User getLoggedInUserDetail (String opportunityID){
        try{
           return [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
           } 
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    } */
    
    @auraEnabled
    public static DateWrapper getOpportunityClosedDate (String opportunityID){
        try{
            DateWrapper wrapper = new DateWrapper();
            system.debug('recordId=='+opportunityID);
            List<Opportunity> opportunity_list = [SELECT CloseDate FROM Opportunity WHERE ID =:opportunityID];
            if(opportunity_list.size() > 0){
                wrapper.closedDate = opportunity_list[0].CloseDate;
            }
            User loggedInUser = [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            System.debug('loggedInUser=='+loggedInUser);
            wrapper.loggedInUser = loggedInUser;
                return wrapper;   
        } 
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }  
    
    
    /* @auraEnabled
    public static Date getOpportunityClosedDate (String opportunityID){
        try{
           List<Opportunity> opportunity_list = [SELECT CloseDate FROM Opportunity WHERE ID =:opportunityID];
            if(opportunity_list.size() > 0){
                return opportunity_list[0].CloseDate;
            }
        } 
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }*/  
    
    private static Map<String,String> getOperatorMap(){
        Map<String, String> operator_map = new Map<String, String>();
        operator_map.put('equals', '=');
        operator_map.put('not equal to', '!=');
        operator_map.put('less than', '<');
        operator_map.put('greater than', '>');
        operator_map.put('less or equal', '<=');
        operator_map.put('greater or equal', '>=');
        operator_map.put('contains', 'LIKE');
        operator_map.put('does not contain', 'NOT_CONTAIN');
        operator_map.put('starts with', 'STARTS_WITH');
        return operator_map;
    }  
    
    private static Map<String, Schema.SObjectField> getProductFieldMap(){
        try{
            Map<String, Schema.SObjectField> product_field_map = new Map<String, Schema.SObjectField>();
        
            Map<String, Schema.SObjectField> field_map = Schema.getGlobalDescribe().get('Product2').getDescribe()
                                                        .fields.getMap();
            List<String> product_field_list = new List<String>();
            
            for(String field_name : field_map.keyset()){
                if(String.valueOf(field_map.get(field_name)).equals('Name') 
                   || String.valueOf(field_map.get(field_name)).equals('Nature_of_Work__c')
                    || String.valueOf(field_map.get(field_name)).equals('Service_Line__c')){
                    product_field_list.add(field_map.get(field_name).getDescribe().getLabel());  
                    product_field_map.put(field_map.get(field_name).getDescribe().getLabel(), field_map.get(field_name));
                }   
            }
            return product_field_map;  
        }
       catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
    private static String getQueryString(String filterList){
        try{
            List<FilterWrapper> filter_list = (List<FilterWrapper>)JSON.deserialize(filterList, List<FilterWrapper>.class);
            System.debug('filter_list=='+filter_list);
            
            Map<String, String> operator_map = getOperatorMap();
            Map<String, Schema.SObjectField> product_field_map = getProductFieldMap();
            
            String query_string = NULL;
            
            For(FilterWrapper filter : filter_list){
                if(operator_map.get(filter.operator) == 'LIKE'){
                    if(query_string == NULL){
                        query_string = product_field_map.get(filter.fieldName)+' '+
                            operator_map.get(filter.operator)+' \'%'+filter.value+'%\'';       
                    }
                    else{
                        query_string = query_string +' AND '+product_field_map.get(filter.fieldName)+' '+
                            operator_map.get(filter.operator)+' \'%'+filter.value+'%\'';       
                    }
                }   
                else if(operator_map.get(filter.operator) == 'NOT_CONTAIN'){
                    if(query_string == NULL){
                        query_string = '(NOT '+ product_field_map.get(filter.fieldName) +
                            ' LIKE \'%'+filter.value+'%\')';  
                    }
                    else{
                        query_string = query_string + ' AND (NOT '+ product_field_map.get(filter.fieldName) +
                            ' LIKE \'%'+filter.value+'%\')';      
                    }
                    
                }
                else if(operator_map.get(filter.operator) == 'STARTS_WITH'){
                    if(query_string == NULL){
                        query_string = product_field_map.get(filter.fieldName)+' LIKE \''+filter.value+'%\'';          
                    }
                    else{
                        query_string = query_string +' AND '+product_field_map.get(filter.fieldName)+' LIKE \''+filter.value+'%\'';          
                    }
                    
                }
                else{
                    if(query_string == NULL){
                        query_string = product_field_map.get(filter.fieldName) + ' '+
                            operator_map.get(filter.operator)+' \''+filter.value+'\'';   
                    }
                    else{
                        query_string = query_string + ' AND '+product_field_map.get(filter.fieldName) + ' '+
                            operator_map.get(filter.operator)+' \''+filter.value+'\'';      
                    }
                    
                }
            }
            return query_string;   
            
        }
        catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
    private static List<Product2> getProductsHavingPriceBookEntry(Map<ID, Product2>product_map){
        try{
        	List<Product2> product_list = new List<Product2>();
    		List<PricebookEntry> priceBookEntryList = [SELECT Id, Product2Id FROM PricebookEntry 
                                                       Where Product2ID =:product_map.keySet()];
            System.debug('priceBookEntryList=='+priceBookEntryList);
        	for(PricebookEntry priceEntry : priceBookEntryList){
            	if(product_map.containsKey(priceEntry.Product2ID)){
            		product_list.add(product_map.get(priceEntry.Product2ID));    
            	}	    
        	}
    	return product_list;       
        }
         catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+'::'+e.getMessage());
        }
        return NULL;
    }
    
     public class FilterWrapper{
        public string fieldName;
        public string operator;
        public string value;
    } 
    
     public class DateWrapper{
        @auraenabled
        public Date closedDate;
        @auraEnabled
        public User loggedInUser;
     }
    
}