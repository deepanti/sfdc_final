public class GPDiscardWorkflows {

    Set<Id> prjIds = new Set<Id>();
    private static final String SUCCESS_LABEL = 'SUCCESS';
    
     public GPDiscardWorkflows(List<GP_Project__c> lstPrj){
       for(GP_Project__c prj : lstPrj)
       {
           prjIds.add(prj.Id);
       }
        system.debug('===prjIds==='+prjIds);
    } 
    
    public GPAuraResponse deleteProject() {
        // SObject's used by the logic in this service, listed in dependency order
        
        savepoint sp= database.setSavepoint();
        try {
            
            
            list<GP_Project__c> lstProject = [SELECT Id, GP_Deal__c, RecordType.Name, GP_Opportunity_project__c,GP_Deal__r.GP_Project_Count__c
                                              FROM GP_Project__c 
                                              WHERE Id in : prjIds];
            system.debug('===lstProject==='+lstProject);
            filterBPMProjectsToClearDealvalues(lstProject);
            
            list<GP_Project_Version_History__c> lstProjectVersionHistory = [SELECT Id 
                                                                            FROM GP_Project_Version_History__c 
                                                                            WHERE GP_Project__c in : prjIds ];
            list<GP_Project_Classification__c> lstProjectClassification = [SELECT Id 
                                                                           FROM GP_Project_Classification__c 
                                                                           WHERE GP_Project__c in : prjIds ];
            list<GP_Project_Task__c> lstProjectTask = [SELECT Id 
                                                       FROM GP_Project_Task__c 
                                                       WHERE GP_Project__c in : prjIds];
            list<GP_Timesheet_Entry__c> lstTimesheetEntry = [SELECT Id 
                                                             FROM GP_Timesheet_Entry__c 
                                                             WHERE GP_Project__c in : prjIds ];
            
            system.debug('===lstProjectVersionHistory==='+lstProjectVersionHistory);
            system.debug('===lstProjectClassification==='+lstProjectClassification);
            system.debug('===lstProjectTask==='+lstProjectTask);
            system.debug('===lstTimesheetEntry==='+lstTimesheetEntry);
            
            if(lstProjectVersionHistory != null && lstProjectVersionHistory.size() > 0) {
                //delete (lstProjectVersionHistory); 
            }
            
            if(lstProjectClassification != null && lstProjectClassification.size() > 0) {
                delete (lstProjectClassification); 
            }
            //Todo- There will not be reference of project task with project. hence this can be removed
            if(lstProjectTask != null && lstProjectTask.size() > 0) {
                delete (lstProjectTask); 
            }
            
            //Todo- There will not be reference of Timesheet Entry with project. hence this can be removed
            if(lstTimesheetEntry != null && lstTimesheetEntry.size() > 0) {
                //delete (lstTimesheetEntry); 
            }
            if(lstProject != null && lstProject.size() > 0) {
                //uow.registerDeleted(lstProject);
                delete lstProject;
            }
            updateProjectCountOnOppProject(lstProject);
            
            
        } catch(Exception ex) {
            database.rollback(sp);
            return new GPAuraResponse(false, ex.getMessage(), null);            
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }
    private void filterBPMProjectsToClearDealvalues(List<GP_Project__c> lstProject){
        List<GP_Deal__c> BPMDeals = new List<GP_Deal__c>();
        for(GP_Project__c project: lstProject){
            if(project.RecordType.Name == 'BPM' && project.GP_Deal__r.GP_Project_Count__c == 1){
                
            }
        }
    }
    
    private void updateProjectCountOnOppProject(List<GP_Project__c> lstProject) {
        
        List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
            GP_Deal__c.SObjectType,
                GP_Opportunity_Project__c.SobjectType
                };
                    
                    fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        List<GP_Deal__c> listOfDealToBeUpdated = new List<GP_Deal__c>();
        Set<Id> setOfDealId = new Set<Id>();
        
        for (GP_Project__c project: lstProject) {
            setOfDealId.add(project.GP_Deal__c);
        }
        
        List<GP_Deal__c> listOfDeal = [SELECT Id, GP_Business_Name__c,GP_Business_Type__c,
                                       GP_Project_Count__c, 
                                       (Select Id 
                                        from GP_Projects__r 
                                        where GP_Parent_Project__c = null) 
                                       FROM GP_Deal__c
                                       WHERE Id in :setOfDealId];
        
        System.System.debug('listOfDeal >>> ' + listOfDeal);
        
        for(GP_Deal__c deal : listOfDeal) {
            
            Integer updatedProjectCount = deal.GP_Projects__r.size();
            
            if(updatedProjectCount != deal.GP_Project_Count__c) {
                deal.GP_Project_Count__c = updatedProjectCount; 
                deal.GP_Current_Project_Status__c ='Approved';
                if(updatedProjectCount == 0 ){
                    deal.GP_Current_Project_Status__c ='NA';
                }
                if(updatedProjectCount == 0  && deal.GP_Business_Type__c == 'BPM' && deal.GP_Business_Name__c == 'BPM'){
                    deal.GP_Business_Type__c = null;
                    deal.GP_Business_Name__c = null;
                }
                listOfDealToBeUpdated.add(deal);
            }
        }
        System.System.debug('listOfDealToBeUpdated >>> ' + listOfDealToBeUpdated);
        uow.registerDirty(listOfDealToBeUpdated);
        
        updateProjectCountOnOppProject(lstProject,uow);
        uow.commitWork();
    }
    
    private void updateProjectCountOnOppProject( List<GP_Project__c> lstProject,fflib_SObjectUnitOfWork uow) {
        system.debug('===updateProjectCountOnOppProject==='+lstProject );
        List<GP_Opportunity_Project__c> listOfOppProjectToBeUpdated = new List<GP_Opportunity_Project__c>();
        Set<Id> setOfOppProjectId = new Set<Id>();
        
        for (GP_Project__c ObjProject: lstProject) {
            setOfOppProjectId.add(ObjProject.GP_Opportunity_Project__c);
        }
        
        List<GP_Opportunity_Project__c> listOfOppProject = [SELECT Id, 
                                                            GP_Project_Count__c, 
                                                            (Select Id 
                                                             from Projects__r 
                                                             where GP_Parent_Project__c = null) 
                                                            FROM GP_Opportunity_Project__c
                                                            WHERE Id in :setOfOppProjectId];
        system.debug('===listOfOppProject==='+listOfOppProject );
        for(GP_Opportunity_Project__c oppProject : listOfOppProject) {
            
            Integer updatedProjecctCount = oppProject.Projects__r.size();
            system.debug('===updatedProjecctCount==='+updatedProjecctCount );
            if(updatedProjecctCount != oppProject.GP_Project_Count__c) {
                oppProject.GP_Project_Count__c = updatedProjecctCount; 
                listOfOppProjectToBeUpdated.add(oppProject);
            }
        }
        
        uow.registerDirty(listOfOppProjectToBeUpdated);
    }
    
}