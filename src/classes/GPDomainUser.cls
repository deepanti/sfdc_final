public class GPDomainUser extends fflib_SObjectDomain {
    public GPDomainUser(List < User > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainUser(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        User.SObjectType
    };

    public override void onBeforeInsert() {}

    public override void onAfterInsert() {
        TagSFDCUserOnEmployeeMaster();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {}

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        inactiveUserRole(oldSObjectMap);
        TagSFDCUserOnEmployeeMaster();
    }
    
    public void TagSFDCUserOnEmployeeMaster()
    {
        try{
            SetToRunEMPMasterORHRMSTrigger.isRunEmpMasterTrigger='N';
            set<String> setofFedId = new Set<String>();
            set<Id> setofId = new Set<Id>();
            Map<String,String> MapUserIdFed=new Map<String,String>();
            List<GP_Employee_Master__c> lstUpdatedEmployee = new List<GP_Employee_Master__c>();
            for(User objUser: (List<User>)records) {
                system.debug('===objUser.Id==='+objUser.Id);
                setofId.add(objUser.Id);
                setofFedId.add(objUser.FederationIdentifier);               
            }    
            
            if(setofId.size()>0)
            {
               
                for(User objUser  : [Select Id,FederationIdentifier,EmployeeNumber  
                                     from User where IsActive =true and FederationIdentifier !=null 
                                     and (not FederationIdentifier   like '9%') and Id in: setofId])
                {
                    MapUserIdFed.put(objUser.FederationIdentifier, objUser.Id);
                }
                
                    
                List<GP_Employee_Master__c> lstEmployee =  [Select Id , GP_SFDC_User__c,GP_Final_OHR__c 
                                                            from GP_Employee_Master__c 
                                                            where  GP_Final_OHR__c in:setofFedId
                                                            and GP_isActive__c = true and GP_SFDC_User__c = null];
                if(lstEmployee.size()>0)
                {
                    for(GP_Employee_Master__c emp:lstEmployee  )
                    {
                        emp.GP_SFDC_User__c=MapUserIdFed.get(emp.GP_Final_OHR__c);
                        lstUpdatedEmployee.add(emp);
                    }
                }
            }
            if(lstUpdatedEmployee.size()>0)
            {
                update lstUpdatedEmployee;
            }
        }
        catch(Exception ex)
        {
            
        }
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: Method is used to Inactive all the user role when User gets inactive       
    // ------------------------------------------------------------------------------------------------
    // Input Param:: fflib_SObjectUnitOfWork uow, Map<id, User> oldSObjectMap
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::29-OCT-2017  Created By::Salil  Email:: Salil.Sharma@saasfocus.com 
    // ------------------------------------------------------------------------------------------------ 
	private void inactiveUserRole(Map<Id, SObject> oldSObjectMap) {
		set<ID> setofId = new Set<ID>();
		for(User objUser: (List<User>)records) {
			User objOldGU = (User) oldSObjectMap.get(objUser.Id);

			if (!objUser.IsActive && objOldGU.IsActive) {
				setofId.add(objUser.id);
			}
		}
		if (setofId != null && setofId.size() > 0) {
			GPServiceUser.DeactivateUserRole(setofId);
		}
	}
}