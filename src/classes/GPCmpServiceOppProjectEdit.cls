public class GPCmpServiceOppProjectEdit {
	@AuraEnabled
    public static GPAuraResponse getOppProjectData(Id oppProjectId) {
        GPControllerOppProjectEdit oppProjectEditController = new GpControllerOppProjectEdit(oppProjectId);
        return oppProjectEditController.getOppProjectData();
    }
    
	@AuraEnabled
    public static GPAuraResponse saveOppProject(String serializedOppProject) {
        
        System.debug('serializedOppProject: '+ serializedOppProject);
        
        GP_Opportunity_Project__c oppProject = (GP_Opportunity_Project__c) JSON.deserialize(serializedOppProject, GP_Opportunity_Project__c.class);
        
        System.debug('oppProject: '+ oppProject);
        
        GPControllerOppProjectEdit oppProjectEditController = new GpControllerOppProjectEdit(oppProject);
        return oppProjectEditController.saveOppProjectData();
    }
    
}