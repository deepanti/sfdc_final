@istest

public class GW1_DSRGroupTriggerHandlertest
{
    public static testmethod void test1()
    {
	     
	     GW1_CommonTracker.createTriggerCustomSetting();
         Account objAccount = GW1_CommonTracker.createAccount('Account1');
         Opportunity objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', false, false,false, objAccount.Id);
	     
	     GW1_DSR__c objdsrc =  GW1_CommonTracker.createGW1DSR('abcd');
	     objdsrc.GW1_Opportunity__c=objOpportunity.id;
	     update objdsrc;
	     GW1_DSR_Group__c objdsrgroup = GW1_CommonTracker.createGW1DSRGroup('abcd',objdsrc,true);
	     user objuser=GW1_CommonTracker.createUser('standt','Testing','standarduserTest@Genpact.com');
	     GW1_DSR_Team__c objDsrTeam= GW1_CommonTracker.createGW1DSRTeam(objdsrc,objuser,true,'Head');
	     objdsrgroup.ownerid=objuser.id;
	     update objdsrgroup;
    
    }
}