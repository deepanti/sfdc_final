/**
 * @group Mass Update/Upload. 
 * @group-content ../../ApexDocContent/MassUploadAndUpdate.html
 *
 * @description Apex Service for MassUploadAndUpdate module,
 *              has aura enabled method for MassUploadAndUpdate.
 */
public class GpCmpServiceMassUploadAndUpdate {

    /**
     * @description Aura enabled method to insert Temporary Records.
     * @param String serializedTemporaryData
     * 
     * @return GPAuraResponse
     *  
     * @example
     * GpCmpServiceMassUploadAndUpdate.insertTemporaryRecordsService(<serializedTemporaryData>, JobType);
     */
    @AuraEnabled
    public static GPAuraResponse insertTemporaryRecordsService(String serializedTemporaryData, string JobType, String fileName, String jobId) {
        GPControllerMassUploadAndUpdate massUploadController = new GPControllerMassUploadAndUpdate(jobId);
        try {
            List < GP_Temporary_Data__c > listOfTemporaryData = (List < GP_Temporary_Data__c > ) JSON.deserialize(serializedTemporaryData, List < GP_Temporary_Data__c > .class);
            return massUploadController.insertTemporaryRecords(listOfTemporaryData, JobType, fileName);
        } catch (Exception e) {
            return new GPAuraResponse(false, 'Please enter the data correctly as per the given instruction', null);
        }
    }

    /**
     * @description Aura enabled method to get Job Configuration.
     * @param String jobId
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GpCmpServiceMassUploadAndUpdate.getJobConfigurationDataService(<jobId>);
     */
    @AuraEnabled
    public static GPAuraResponse getJobConfigurationDataService(String jobId) {
        GPControllerMassUploadAndUpdate massUploadController = new GPControllerMassUploadAndUpdate(jobId);
        return massUploadController.getJobConfigurationData();
    }
}