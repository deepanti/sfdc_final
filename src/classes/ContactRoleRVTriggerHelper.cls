public class ContactRoleRVTriggerHelper 
{
   private static Map<Id, ID> primary_contact_ID_map;
   static Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
    
    //This method is used for inserting contact role and RevenueStrom barometer
    public static void insertContactRoleAndRevenueStormBarometer(List<Contact_Role_RV__c> contact_role_RV_list)
    {
        List<Sobject> sObject_list = new List<SObject>();
        Set<ID> opportunityIDs = new Set<ID>();
        primary_contact_ID_map = new Map<ID, ID>();
        
        try{ 
            List<String> contactId_list = new List<String>();
            
            for(Contact_Role_RV__c contact_role : contact_role_RV_list){
                contactId_list.add(contact_role.Contact__c);
                opportunityIDs.add(contact_role.Opportunity__c);
            } 
            
            Map<ID, Contact> contact_map = getContactMap(contactId_list);
            
            List<OpportunityContactRole> oppcontactroleList = [SELECT id, contactID, isPrimary, role, opportunityId from OpportunityContactRole 
                                                              WHERE opportunityId = : opportunityIDs];
            
            for(Contact_Role_RV__c contact_role_RV : contact_role_RV_list){
                
                // For Insertion into Opportunity Contact Role 
                if(contact_role_RV.isPrimary__c){
                    primary_contact_ID_map.put(contact_role_RV.opportunity__c, contact_role_RV.ID);
                 }                
                OpportunityContactRole opportunity_contact_role = new OpportunityContactRole();
                opportunity_contact_role.ContactId = contact_role_RV.contact__c;
                opportunity_contact_role.OpportunityId = contact_role_RV.Opportunity__c;
                opportunity_contact_role.role = contact_role_RV.Role__c;
                opportunity_contact_role.IsPrimary = contact_role_RV.IsPrimary__c;
                if(!OppcontactRoleIsExist(oppcontactroleList, opportunity_contact_role)){
                	sObject_list.add(opportunity_contact_role);    
                }
                
                
                //For Insertion into RevenueStorm relationship Barometer.                
                Contact contact_obj = contact_Map.get(contact_role_RV.contact__c);
                
                RevenueStormRB__RevenueStorm_Relationship_Barometer__c revenueStorm_barometer = 
                        getRevenueStrom_barometer_obj(new RevenueStormRB__RevenueStorm_Relationship_Barometer__c(), contact_obj);
                
                revenueStorm_barometer.RevenueStormRB__Contact__c = contact_role_RV.contact__c;
                revenueStorm_barometer.RevenueStormRB__Opportunity__c = contact_role_RV.Opportunity__c;
                sObject_list.add(revenueStorm_barometer);
            } 
            System.debug('sObject_list=='+sObject_list);
            //if(CheckRecursiveForContactRole.runOnceAfter()){
                System.debug('sObject_list 1=='+sObject_list);
                Insert sObject_list;
                if(!primary_contact_ID_map.isEmpty()){
                    SetPrimaryContactRole(primary_contact_ID_map);     
                }                  
            //}            
        }
        catch(Exception e){
            System.debug(e.getLineNumber()+'::'+e.getMessage());
        }        
    }
    
    //This method is used for Updating contact role and RevenueStrom barometer
    public static void updateContactRoleAndRevenueStormBarometer(Map<Id,Contact_Role_RV__c> updated_contact_role_RV,
                                                                List<Contact_Role_RV__c> old_contact_role_RV){
       	List<String> opportunityId_list = new List<String>();
        List<String> contactId_list = new List<String>();
        List<String> contact_list = new List<String>();
                                                                    
        List<Sobject> sObject_list = new List<SObject>();
        primary_contact_ID_map = new Map<ID, ID>();
        try{
            for(Contact_Role_RV__c contact_role : old_contact_role_RV){
                contactId_list.add(contact_role.Contact__c);
                opportunityId_list.add(contact_role.Opportunity__c);    
            }                    
           
            // For Updation into Opportunity Contact Role  
            for(opportunityContactRole oppor_contact_role : [SELECT Id, contactId, opportunityId, role, isPrimary
                                                                       FROM Opportunitycontactrole WHERE 
                                                                       contactID IN :contactID_list 
                                                                        AND opportunityID IN :opportunityId_list]){
                
                for(Contact_Role_RV__c contact_role_RV : old_contact_role_RV){
                    if(oppor_contact_role.contactId == contact_role_RV.Contact__c && 
                       oppor_contact_role.opportunityID == contact_role_RV.opportunity__c){
                           Contact_Role_RV__c updated_contact_role = updated_contact_role_RV.get(contact_role_RV.ID);
                           if(updated_contact_role.isPrimary__c){
                              primary_contact_ID_map.put(updated_contact_role.opportunity__c, updated_contact_role.ID);
                           }
                            oppor_contact_role.contactId = updated_contact_role.Contact__c;
                            oppor_contact_role.role = updated_contact_role.role__c;
                            oppor_contact_role.isPrimary = updated_contact_role.isPrimary__c;
                            sObject_list.add(oppor_contact_role);
                        }   
                    }    
                }
            
            //For updation into Opportunity
           Map<ID, Opportunity> related_opportunity_list = new Map<ID, Opportunity>([SELECt ID, contact1__c, role__c FROM Opportunity
                                                            WHERE ID IN :opportunityId_list]);
            system.debug('related_opportunity_list=='+related_opportunity_list);
             
            if(related_opportunity_list.size() > 0){
            	for(Contact_Role_RV__c contact_role_RV : old_contact_role_RV){
              		Opportunity opp =  related_opportunity_list.get(contact_role_RV.Opportunity__c); 
                    if(opp != null){
                        if(opp.contact1__c == contact_role_RV.Contact__c){                      
                            Contact_Role_RV__c updated_contact_role = updated_contact_role_RV.get(contact_role_RV.ID);                      
                            if(contact_role_RV.Contact__c != updated_contact_role.Contact__c 
                               || contact_role_RV.role__c!= updated_contact_role.role__c){
                                   
                                   opp.contact1__c = updated_contact_role.Contact__c;
                                   opp.role__c = updated_contact_role.role__c;
                                   sObject_list.add(opp);
                               }                  	                       
                        }    
                    }
                   
              	}     
            }              
            
            List<Contact_Role_RV__c> updated_contact_role_Rv_list = updated_contact_role_RV.values();
            
            for(Contact_Role_RV__c contact_role : updated_contact_role_Rv_list){
                contact_list.add(contact_role.Contact__c);
            }    
            
            Map<ID, Contact> contact_map = getContactMap(contact_list);  
             
             // For Updation into RevenueStormRB__RevenueStorm_Relationship_Barometer Object 
            for(RevenueStormRB__RevenueStorm_Relationship_Barometer__c revenueStorm_barometer : [SELECT Id, 
                                                        RevenueStormRB__Contact__c, RevenueStormRB__Opportunity__c 
                                                        FROM RevenueStormRB__RevenueStorm_Relationship_Barometer__c 
                                                        WHERE RevenueStormRB__Contact__c IN :contactID_list AND 
                                                        RevenueStormRB__Opportunity__c IN :opportunityId_list]){
                
                for(Contact_Role_RV__c contact_role_RV : old_contact_role_RV){
                    if(revenueStorm_barometer.RevenueStormRB__Contact__c == contact_role_RV.Contact__c && 
                        revenueStorm_barometer.RevenueStormRB__Opportunity__c == contact_role_RV.opportunity__c){
                            
                        //Get updated Contact role object
                        Contact_Role_RV__c updated_contact_role = updated_contact_role_RV.get(contact_role_RV.ID);
                        Contact contact_obj = contact_Map.get(updated_contact_role.contact__c);
                            
                        //Get updated RevenueStorm Barometer object
                        revenueStorm_barometer = getRevenueStrom_barometer_obj(revenueStorm_barometer, contact_obj);
                            
                        revenueStorm_barometer.RevenueStormRB__Contact__c = updated_contact_role.contact__c;
                        revenueStorm_barometer.RevenueStormRB__Opportunity__c = updated_contact_role.Opportunity__c;
                    	sObject_list.add(revenueStorm_barometer);                       
                   }
                }                 
            }
            Update sObject_list;
            //if(CheckRecursiveForContactRole.runOnceAfter()){               
                if(!primary_contact_ID_map.isEmpty()){
                    System.debug('primary_contact_ID_map=='+primary_contact_ID_map);
                    SetPrimaryContactRole(primary_contact_ID_map);     
                }                  
           // }           
        }
        catch(Exception e){
            System.debug('error is=='+e.getLineNumber()+'::'+e.getMessage());
        }
        
    }
    
    //This method is used for Deleting contact role and RevenueStrom barometer
    public static void deleteContactRoleAndRevenueStormBarometer(List<Contact_Role_RV__c> contact_role_RV_list){
        List<String> opportunityId_list = new List<String>();
        List<String> contactId_list = new List<String>();
        
        List<Sobject> sObject_list = new List<SObject>();
            
        try{
            for(Contact_Role_RV__c contact_role : contact_role_RV_list){
                contactId_list.add(contact_role.Contact__c);
                opportunityId_list.add(contact_role.Opportunity__c);    
            }
            
            // For Deletion into Opportunity Contact Role 
            sObject_list.addAll([SELECT Id FROM Opportunitycontactrole WHERE contactID IN :contactID_list 
                    AND opportunityID IN :opportunityId_list]);
            
            // For Deletion into RevenueStorm Relationship barometer object 
            sObject_list.addAll([SELECT Id FROM RevenueStormRB__RevenueStorm_Relationship_Barometer__c WHERE 
                    RevenueStormRB__Contact__c IN :contactID_list 
                    AND RevenueStormRB__Opportunity__c IN :opportunityId_list]);
            
            Delete sObject_list;
        }
        catch(Exception e){
            System.debug(e.getLineNumber()+'::'+e.getMessage());
        }
    }
    
    public static void checkDuplicateContact(List<Contact_Role_RV__c> new_Contact_Role_List){
        List<ID> contactIDs = new List<ID>();        
        List<ID> opportunityIDs = new List<ID>();
        
        for(Contact_Role_RV__c contact_role : new_Contact_Role_List){
            contactIDs.add(contact_role.contact__c);
            opportunityIDs.add(contact_role.Opportunity__c);
        }
        List<Contact_Role_RV__c> existing_contact_role_list = [SELECT Id, Contact__r.Name, Contact__c, Opportunity__c from Contact_role_rv__c WHERE
                                                         contact__c IN :contactIDs AND Opportunity__c IN :opportunityIDs];
     	if(existing_contact_role_list.size()>0){
            for(Contact_Role_RV__c contact_role : new_Contact_Role_List){ 
                system.debug('contact_role in duplicate=='+contact_role);
                for(Contact_Role_Rv__c existing_contact_Role : existing_contact_role_list){
                    system.debug('existing_contact_Role in duplicate=='+existing_contact_Role);
                    if(contact_role.Contact__c == existing_contact_Role.contact__c 
                       && contact_role.Opportunity__c == existing_contact_Role.Opportunity__c
                      	&& contact_role.Id != existing_contact_role.Id){
                        contact_role.addError(existing_contact_Role.contact__r.Name+' is already a contact Role ');
                        break;
                    }
                }
            }
        }
    }
    
    public static void restrictPrimaryContact(List<Contact_Role_RV__c> old_Contact_Role_List){
       
        for(Contact_Role_RV__c contact_role : old_Contact_Role_List){
            if(contact_role.IsPrimary__c){
            	contact_role.addError('Primary Contact Role can not be deleted');  
                break;
            }    
        }    
    }
    
    //This method is used to update all the contact role RV records if primay contact has changed.
    private static void SetPrimaryContactRole(Map<Id, Id> primary_contact_map){
        List<Contact_Role_RV__c> contact_role_rv_list = [SELECT ID, isPrimary__c FROM Contact_Role_RV__c
                                                             WHERE Id != :primary_contact_ID_map.values() 
                                                        	AND opportunity__c IN :primary_contact_map.keySet()];
        for(Contact_Role_RV__c contact_role_RV : contact_role_rv_list){
            contact_role_RV.IsPrimary__c = false;
        }  
            System.debug('contact_role_rv_list in primary=='+contact_role_rv_list);
        update contact_role_rv_list;    
    }
    
    // This method is used to get the Map of contacts having values of Contact Evaluator.
    private static Map<ID, Contact> getContactMap(List<ID> contactIDs){
        return new Map<ID, Contact>([SELECT Id, Name, RevenueStormRB__RevenueStorm_Political_Question_5__c,
                  RevenueStormRB__RevenueStorm_Political_Question_8__c, RevenueStormRB__RevenueStorm_Relationship_Question_8__c,
                  RevenueStormRB__RevenueStorm_Political_Question_3__c, RevenueStormRB__RevenueStorm_Political_Question_2__c,
                  RevenueStormRB__RevenueStorm_Political_Question_7__c, RevenueStormRB__RevenueStorm_Relationship_Question_1__c,
                  RevenueStormRB__RevenueStorm_Political_Question_1__c, RevenueStormRB__RevenueStorm_Relationship_Question_7__c,
                  RevenueStormRB__RevenueStorm_Relationship_Question_5__c, RevenueStormRB__RevenueStorm_Political_Total__c,
                  RevenueStormRB__RevenueStorm_Relationship_Question_6__c, RevenueStormRB__RevenueStorm_Relationship_Question_3__c,
                  RevenueStormRB__RevenueStorm_Relationship_Category__c, RevenueStormRB__RevenueStorm_Relationship_Total__c,
                  RevenueStormRB__RevenueStorm_Relationship_Question_9__c, RevenueStormRB__RevenueStorm_Relationship_Question_4__c,
                  RevenueStormRB__RevenueStorm_Political_Question_6__c, RevenueStormRB__RevenueStorm_Political_Question_4__c,
                  RevenueStormRB__RevenueStorm_Relationship_Question_2__c, RevenueStormRB__RevenueStorm_Political_Label__c
                  FROM CONTACT WHERE ID IN :contactIDs]);         
    }
    
    //This method is used to build the RevenueStormBarometer Objects.
    public static RevenueStormRB__RevenueStorm_Relationship_Barometer__c getRevenueStrom_barometer_obj(
                    RevenueStormRB__RevenueStorm_Relationship_Barometer__c revenueStorm_barometer, 
                    Contact contact_obj){
        
        revenueStorm_barometer.Name = contact_obj.Name;               
        revenueStorm_barometer.RevenueStormRB__Political_Question_1__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_1__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_2__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_2__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_3__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_3__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_4__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_4__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_5__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_5__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_6__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_6__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_7__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_7__c;
        revenueStorm_barometer.RevenueStormRB__Political_Question_8__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Question_8__c;
        revenueStorm_barometer.RevenueStormRB__Political_Label__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Label__c;
        revenueStorm_barometer.RevenueStormRB__Political_Total__c = contact_obj.RevenueStormRB__RevenueStorm_Political_Total__c;
        
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_1__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_1__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_2__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_2__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_3__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_3__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_4__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_4__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_5__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_5__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_6__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_6__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_7__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_7__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_8__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_8__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Question_9__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Question_9__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Category__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Category__c;
        revenueStorm_barometer.RevenueStormRB__Relationship_Total__c = contact_obj.RevenueStormRB__RevenueStorm_Relationship_Total__c;
        return revenueStorm_barometer;
    }
    
    public static boolean OppcontactRoleIsExist(List<OpportunityContactRole>oppContactRoleList, OpportunityContactRole opportunity_contact_role){
        
        for(OpportunityContactRole oppRole : oppContactRoleList){
            if(opportunity_contact_role.ContactId == oppRole.ContactId 
               && opportunity_contact_role.OpportunityId == oppRole.opportunityId){
                return true;
            }
        }
        return false;
    }
}