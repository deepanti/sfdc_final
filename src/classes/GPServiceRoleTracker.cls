@isTest
public class GPServiceRoleTracker {
    @isTest
    public static void testInsertGroup() {  
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        objrole.GP_Group_Name_for_Read_Access__c = 'Test Group';
        insert objrole;
        
        GP_Role__c objrole2 = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole2.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole2.GP_HSL_Master__c = null;
        objrole2.GP_Role_Category__c = 'PID Creation';
        objrole2.GP_Group_Name_for_Read_Access__c = 'Test Group';
        insert objrole2;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        GP_User_Role__c objuserrole2 = GPCommonTracker.getUserRole(objrole2, objuser);
        objuserrole2.GP_Active__c = true;
        insert objuserrole2;
        
        objuserrole2 = GPCommonTracker.getUserRole(objrole2, objuser);
        objuserrole2.GP_Active__c = true;
        objuserrole2.GP_User__c = UserInfo.getUserId();
        insert objuserrole2;
        
        GPServiceRole roleService = new GPServiceRole();
        GPServiceRole.insertGroup(new Set<Id> {objrole.Id});
        
        roleService.getUserEmailsForRole(new Set<Id> {objrole.Id, objrole2.Id});
    }
}