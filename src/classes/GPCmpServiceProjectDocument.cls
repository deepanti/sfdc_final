/**
 * @author Anmol.kumar
 * @date 13/09/2017
 *
 * @group GPCmpServiceProjectDocument
 * @group-content ../../ApexDocContent/GPCmpServiceProjectDocument.htm
 *
 * @description Apex Service for GPCmpServiceProjectDocument module,
 * has aura enabled method to fetch, update/create and delete project document data.
 */
public class GPCmpServiceProjectDocument {

    /**
     * @description Returns  project document record
     * @param projectId SFDC 18/15 digit Project  Id
     * 
     * @return GPAuraResponse  json of Project document data
     * 
     * @example
     * GPCmpServiceProjectDocument.getProjectDocuments(<SFDC 18/15 digit Project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getProjectDocuments(Id projectId) {
        GPControllerProjectDocument projectDocumentsController = new GPControllerProjectDocument();
        return projectDocumentsController.getDocuments(projectId);
    }

    /**
     * @description deletes project document record
     * @param Id projectDocumentId
     * @param Id contentVersionId
     * 
     * @return GPAuraResponse Serialized status of project and its child record
     * 
     * @example
     * GPCmpServiceProjectDocument.deleteProjectDocument(<SFDC 18/15 digit Project Document Id>, <contentVersionId>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteProjectDocument(Id projectDocumentId, Id contentVersionId) {
        return GPControllerProjectDocument.deleteProjectDocumentService(projectDocumentId, contentVersionId);
    }

    /**
     * @description Saves project document record
     * @param String serializedProjectDocument
     * @param String projectId
     * @param String recordTypeName
     * 
     * @return GPAuraResponse Saves project document record
     * 
     * @example
     * GPCmpServiceProjectDocument.saveProjectDocument(<serializedProjectDocument>, <projectId>, <recordTypeName>);
     */
    @AuraEnabled
    public static GPAuraResponse saveProjectDocument(String serializedProjectDocument, String projectId, String recordTypeName) {
        GPControllerProjectDocument projectDocumentsController = new GPControllerProjectDocument();
        return projectDocumentsController.saveProjectDocumentService(serializedProjectDocument, projectId, recordTypeName);
    }
}