@isTest
public class GPServiceProjectTracker {
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c project = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;

    private static GP_Employee_Master__c employeeMaster;    
    private static List<GP_Project_Leadership__c> listOfProjectLeadership;
    private static GP_Leadership_Master__c ledershipMaster;
    
    @testSetup
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        // HSN Changes: Billing Entity
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        objpinnacleMaster.GP_Is_GST_Enabled__c = true;
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        system.debug('==OU Id=='+ objpinnacleMaster.Id);
        system.debug('==OU is GST=='+ objpinnacleMaster.GP_Is_GST_Enabled__c);
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        // HSN Changes
        parentProject.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert parentProject;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.Name = 'Test PID';
        // HSN Changes
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Operating_Unit__c = objpinnacleMaster.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        // Gainshare Change
        project.GP_Project_type__c = 'Gainshare';
        project.GP_Oracle_PID__c = 'NA';
        
        insert project ;
        
        /*
        ProcessInstance objProcessInstance = new ProcessInstance();
        objProcessInstance.TargetObjectId = project.id;
        objProcessInstance.ProcessDefinitionId  = '04a90000000H7IUAA0';
        objProcessInstance.Status = 'Pending';
        insert objProcessInstance;
        
        ProcessInstanceWorkitem objProcessInstanceWorkItem = new ProcessInstanceWorkitem();
        objProcessInstanceWorkItem.ProcessInstanceId = objProcessInstance.id;
        insert objProcessInstanceWorkItem;*/
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(project.Id, sdo.Id);
        insert objPrjBillingMilestone;
        
    }
    @isTest 
    public static void testGPServiceProject() {
        fetchData();
        createProjectLeadership();
        testvalidateLoggedInUser();
        testvalidateProjectStages();
        testvalidateApprovalStatus();
        testvalidateProjectPricingStatus();
        testvalidateApprovalStatusForClosure();
        testcloneProject();
        testvalidateMakerChecker1();
        testvalidateLoggedinUserApprover();
        
        testupdateEmployeeEndDateInOldVersion();
        testprojectCloserNotification();
        testvalidateProjectandChildRecordAccess();
        testvalidateProjectAgainstTemplate();
        testisValidField();
        testcustomValidateProject();
        testupdateOracleTemplateIdOnProject();
    }
    
    static void testupdateEmployeeEndDateInOldVersion() {
        GP_Project__c clonedProject = project.clone();
        clonedProject.GP_Parent_Project__c = project.Id;
        insert clonedProject;
        
        GP_Project_Version_History__c versionHistory = new GP_Project_Version_History__c();
        versionHistory.GP_Project_JSON__c = JSON.serialize(project);
        versionHistory.GP_Project__c = project.Id;
        versionHistory.GP_Status__c = 'Approved';
        insert versionHistory;
        
        GPServiceProject.updateEmployeeEndDateInOldVersion(clonedProject); 
        clonedProject.GP_GPM_Start_Date__c = System.today();
        GPServiceProject.updateEmployeeEndDateInOldVersion(clonedProject); 
    }
    
    static void fetchData() { 
        objPrjBillingMilestone = [select id,Name from GP_Billing_Milestone__c limit 1];
        // ECR-HSN Changes
    	// Added GP_Oracle_PID__c and GP_Parent_Project__c
        project = [select id, Name, GP_Project_Template__c, GP_Primary_SDO__c, GP_Project_Stages_for_Approver__c, 
                   GP_Approval_Status__c, GP_GPM_Start_Date__c, GP_Project_type__c, GP_is_GST_Enabled_OU__c,
                   GP_Operating_Unit__c, GP_Operating_Unit__r.GP_Is_GST_Enabled__c, GP_End_Date__c,
                   GP_Oracle_PID__c, GP_Parent_Project__c
                  from GP_Project__c where Name = 'Test PID' limit 1];
        
        System.debug('==project1=='+ project.GP_is_GST_Enabled_OU__c);
        System.debug('==project2=='+ project.GP_Operating_Unit__r.GP_Is_GST_Enabled__c);
        
        
        employeeMaster = [SELECT ID From GP_Employee_Master__c LIMIT 1];
        jsonresp= (String)JSON.serialize(new List<GP_Billing_Milestone__c>{objPrjBillingMilestone}); 
    }
    
    static void testcustomValidateProject() {
        Map<String, String> mapOfProjectFieldNameToLabel = GPCommon.getMapOfFieldNameToLabel('GP_Project__c');
        
        //System.debug('==project3=='+ project);
        
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);
        project.GP_T_M_With_Cap__c = true;
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);
        
        project.Name = 'sdfhjkmnbvcxxhjkkjhgfdsdfghjhgfdcvbnmnbvsdfvbqwertyuioplkjbvcxcvbnmlyfdfghjmnbvcfghjklmbvcghjklkjhvc';
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);
        
        project.GP_Project_type__c = 'Fixed Price';
        project.GP_Overtime_Billable__c = true;
        project.GP_Timesheet_Requirement__c = 'yes';
        
        GPServiceProject.isFormattedLogRequired = true;
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);        
        
        project.GP_Project_type__c = 'T&M';
        project.GP_Bill_Rate_Type__c = null;
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);
        
		Date todayDate = System.today();
        
        project.GP_Start_Date__c = todayDate.addDays(10);
        project.GP_End_Date__c = todayDate;        
        
        project.GP_SOW_Start_Date__c = todayDate.addDays(10);
        project.GP_SOW_End_Date__c = todayDate;
                
        project.GP_Contract_Start_Date__c = todayDate.addDays(10);
        project.GP_Contract_End_Date__c = todayDate;
        
        project.GP_Transition_End_Date__c = todayDate;
        project.GP_Shift_Start_Date__c = todayDate;
        
        project.GP_Service_Deliver_in_US_Canada__c = true;
        
        GPServiceProject.isFormattedLogRequired = false;
        GPServiceProject.isLogForTemporaryId = true;        
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);        
        
        project.GP_SOW_End_Date__c = todayDate.addDays(10);
        project.GP_Shift_Start_Date__c = todayDate.addDays(10);
        project.GP_Tax_Exemption_Certificate_Available__c = true;
        GPServiceProject.isLogForTemporaryId = false;
        GPServiceProject.customValidateProject(project, mapOfProjectFieldNameToLabel);  
        
    }
    
    static void testisValidField() {
        GPServiceProject.isValidField('GP_Project__c', 'invalid field');
        GPServiceProject.isValidField('GP_Project__c', 'Name'); 
        GPServiceProject.isValidField('invalid Object', 'Name'); 
    }
    
    static void testvalidateProjectAgainstTemplate() {
        
        Map<String, String> mapOfProjectFieldNameToLabel = GPCommon.getMapOfFieldNameToLabel('GP_Project__c');
        GP_Project_Template__c projectTemplate = [Select Id, GP_Final_JSON_1__c, GP_Final_JSON_2__c, GP_Final_JSON_3__c
                                                 FROM GP_Project_Template__c where Id =:project.GP_Project_Template__c];
        GPServiceProject.isFormattedLogRequired = true;
        GPServiceProject.validateProjectAgainstTemplate(project, projectTemplate, mapOfProjectFieldNameToLabel); 
        
        GPServiceProject.isFormattedLogRequired = false;  
        GPServiceProject.isLogForTemporaryId = true;
        GPServiceProject.validateProjectAgainstTemplate(project, projectTemplate, mapOfProjectFieldNameToLabel);  
        GPServiceProject.isLogForTemporaryId = false;
        GPServiceProject.validateProjectAgainstTemplate(project, projectTemplate, mapOfProjectFieldNameToLabel);  
    }
    
    static void testvalidateProjectandChildRecordAccess() {
        Set<Id> setOfProjectId = new Set<Id> {
            project.Id
        };
        GPServiceProject.validateProjectandChildRecordAccess(setOfProjectId);         
    }
    
    static void testprojectCloserNotification() {
        List<Id> listOfProjectId = new List<Id> {
            project.Id
        };
        GPServiceProject.projectCloserNotification(listOfProjectId);        
    }
    
    static void testvalidateApprovalStatusForClosure() {
        GPServiceProject.validateApprovalStatusForClosure(project);
        project.GP_Approval_Status__c = 'Pending For Closure';
        GPServiceProject.validateApprovalStatusForClosure(project);      
    }
    
    static void testvalidateApprovalStatus() {
        GPServiceProject.validateApprovalStatus(project);
        project.GP_Approval_Status__c = 'Pending for Approval';
        GPServiceProject.validateApprovalStatus(project);    
    }
    
    static void testvalidateProjectStages() {
        GPServiceProject.validateProjectStages(project);
        project.GP_Project_Stages_for_Approver__c = 'Draft';
        GPServiceProject.validateProjectStages(project);
    }
    
    static void testvalidateLoggedInUser() {
        GPServiceProject.validateLoggedInUser(project);
        system.assert(True, 'CheckLoggedInUser');
    }
    
    static void testvalidateProjectPricingStatus() {
        GPServiceProject.validateProjectPricingStatus(project);
    }
    
    static void testcloneProject() {
        //string str = GPServiceProject.cloneProject(project.id,'Manuel',false);
    }
    
    static void testvalidateMakerChecker1() {
        //GPServiceProject.validateMakerChecker1(project);
    }
    
    static void testvalidateLoggedinUserApprover() {
        GPServiceProject.validateLoggedinUserApprover(project);
    }
    
    static void testupdateOracleTemplateIdOnProject() {
        GPServiceProject.updateOracleTemplateIdOnProject(null);
    }
 
    private static void createProjectLeadership() {
        Date todayDate = System.today();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        ledershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        ledershipMaster.GP_Category__c = 'Mandatory Key Members';
        insert ledershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(project.Id, ledershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
        projectLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
        
        List<GP_Project_Leadership__c> listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        listOfProjectLeadership.add(projectLeadership);
        
        insert listOfProjectLeadership;
    }
}