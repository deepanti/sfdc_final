@isTest
private class Testupdatecurrentallocationstatus 
{

   static testMethod void testTrigger()
   {
       RMG_Employee_Master__c rmg = new RMG_Employee_Master__c(Name='Test'); 
      insert rmg;
      RMG_Employee_Master__c rmg_1 = new RMG_Employee_Master__c(Name='Test1'); 
      insert rmg_1;
      RMG_Employee_Master__c rmg_2 = new RMG_Employee_Master__c(Name='Test2'); 
      insert rmg_2;
      
      RMG_Employee_Allocation__c rmgallo = new RMG_Employee_Allocation__c(
                                   Name='Test', Client__c='Non-Bench',Status__c = 'Current',is_Zero_PRF__c='1',Start_Date__c = date.parse('05/05/2016'), End_Date__c = date.parse('08/25/2017'),
                                  RMG_Employee_Code__c=rmg.id);
                                     
                                     
                                     
       RMG_Employee_Allocation__c rmgallo_1 = new RMG_Employee_Allocation__c(
                                     Name='Test1', Client__c='Bench',Status__c = 'Current',Start_Date__c = date.parse('04/05/2016'), End_Date__c = date.parse('11/25/2017'),
                                     RMG_Employee_Code__c=rmg_1.id); 
                                     
        RMG_Employee_Allocation__c rmgallo_2 = new RMG_Employee_Allocation__c(
                                     Name='Test2', Client__c='Non-Bench',Status__c = 'Current',is_Zero_PRF__c='0',Start_Date__c = date.parse('04/05/2016'), End_Date__c = date.parse('11/25/2017'),
                                     RMG_Employee_Code__c=rmg_2.id);                                                              
     
       insert rmgallo_1;
    
      
      update rmgallo_1;
      
       insert rmgallo;
      
       update rmgallo;
         insert rmgallo_2;
      
       update rmgallo_2;
      
     RMG_Employee_Allocation__c newrmg=[select Name, Client__c,Status__c,is_Zero_PRF__c,Active__c from RMG_Employee_Allocation__c where id = :rmgallo.id];
      RMG_Employee_Allocation__c newrmg_1=[select Name, Client__c,Status__c,is_Zero_PRF__c,Active__c from RMG_Employee_Allocation__c where id = :rmgallo_1.id];
        RMG_Employee_Allocation__c newrmg_2=[select Name, Client__c,Status__c,is_Zero_PRF__c,Active__c from RMG_Employee_Allocation__c where id = :rmgallo_2.id];
      
      
      
      //System.assertEquals(rmg.Allocation_Status__c,null);
      
      
      }
      }