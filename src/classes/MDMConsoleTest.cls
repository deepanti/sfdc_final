/**********************************************************************************
@Original Author : Iqbal
@Modified By Author : Iqbal
@TL : Madhuri
@date Started : 26 Feb 2019
@Description : Test class to cover "MDMConsoleController" and "DynamicLookUpController" classes.
@Parameters : 
@Return type :
**********************************************************************************/


@isTest
public class MDMConsoleTest {
    public static Account oAccount;
    public static Contact oContact;
    public static Case oCase;
    public static String IndustryVertical = 'Manufacturing';
    public static String title = 'Test';
    public static String buyingcenter = 'Digital';
    public static String levelcontract = 'CXO';
    public static String countrySelect = 'Albania';
    public static String accountArctye = 'test';
    public static String validreason = 'Test';
    public static String campaignName = 'Test';
    
    private static void setupTestData(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        system.debug('AccountID'+oAccount);
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        Id RecordTypeIdContact = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MDM Console Report').getRecordTypeId();
        
        oCase = new Case(Title__c= 'Test' ,AccountId=oAccount.Id,Buying_Centre_multi__c='Digital',Level_Of_Contact__c='CXO',Country_Of_Residence__c='Albania',
                         Account_Archetype__c=oAT.Id,Please_provide_valid_reason_for_MDMcase__c='Test',Industry_Vertical_multi__c='Manufacturing',
                         RecordTypeId=RecordTypeIdContact,Campaign_Name__c='Test');
        insert oCase;
        
        /*Case__c oC = new Case__c();
        oC.PowerBi_Email__c='test@test.com';
        oC.Name = 'Test';
        insert oC;*/
        
    }
    
    //Test Method to cover DynamicLookupController Class
    @isTest public static void testMethod1()
    {
        String searchKeyWord = 'test';
        String ObjectName = 'Account';
        test.startTest();
        DynamicLookUpController.fetchLookUpValues(searchKeyWord,ObjectName);
        test.stopTest();
        
    }
    @isTest public static void testMethod2()
    {
        test.startTest();
        setupTestData();
        
        MDMConsoleController.RequestReasonList();
        
        list<string> accNam = new list<string>();
        accNam.add('Test Account');
        MDMConsoleController.getAccountId(accNam);
        
        list<string> Pickval = new list<string>();
        Pickval.add('Test1');
        Pickval.add('Test2');
        MDMConsoleController.getmultipicklist(Pickval);
        
        MDMConsoleController.CreateCaseRecord(IndustryVertical,title,buyingcenter,levelcontract,countrySelect,accountArctye,validreason,oAccount.Id,campaignName);
        
        MDMConsoleController.EmailSender(oCase.Id);
        
        MDMConsoleController.CreateRecord('Inside Sales,test','othertest');
        
        test.stopTest();
        
    }
    @isTest public static void testMethod3()
    { 
     test.startTest();
     setupTestData();
     
     MDMConsoleController.EmailSender(null);
     
     MDMConsoleController.getmultipicklist(null);
     
     MDMConsoleController.CreateRecord(null,null);
     
     MDMConsoleController.CreateCaseRecord(null,title,buyingcenter,levelcontract,countrySelect,accountArctye,validreason,oAccount.Id,campaignName);
     
     test.stopTest();
    }

    
}