global class GENcUpdateCurrentYearonHeader implements Database.batchable<sObject>, Database.Stateful
{
    Map<string, AggregateResult> oppProductAggregateResultMap = new Map<string, AggregateResult>();
    string Query ='select id, Name,count_no_of_lines__c, ProductionTCV__c, ProductionACV__c,ProductionCYR__c, ProductionNYR__c, (select Id, name, TotalContractValue__c, ACV__c, CurrentYearRevenue__c, NextYearRevenue__c from Opportunity_Products__r)from Opportunity';
    
    global Database.QueryLocator start(Database.BatchableContext bc)
     {    
       return database.getQuerylocator(Query);
     }
    global void execute(Database.batchableContext info, List<Opportunity> oppWithProducts)
    {
        List<AggregateResult> aggResult = [Select  opportunityId__c, Max(RevenueStartDate__c) maxRevenueStartDate, min(ContractTermInMonths__c) minContractTerm FROM OpportunityProduct__c group by opportunityId__c];
        for(AggregateResult agg: aggResult ){
            oppProductAggregateResultMap.put(String.valueOf(agg.get('opportunityId__c')), agg);
        }
        GENcCalculateProductLineItemRollUp.updateOpportunityRollUpInfo(oppWithProducts, oppProductAggregateResultMap);
    }
    global void finish(Database.batchableContext info)
    {        

    } 
    
}