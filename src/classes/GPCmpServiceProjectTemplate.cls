/**
* @author Anmol.kumar
* @date 22/10/2017
*
* @group ProjectTemplate
* @group-content ../../ApexDocContent/ProjectTemplate.html
*
* @description Apex Service for project Template module,
* has aura enabled method to CRUD project Template data.
*/
public class GPCmpServiceProjectTemplate {   

    /**
    * @description Returns Project template with project for project Id.
    * @param Id projectId
    * 
    * @return GPAuraResponse json of Project + Template data
    * 
    * @example
    * GPCmpServiceProjectTemplate.getProjectWithTemplate(<projectId>);
    */     
    @AuraEnabled
    public static GPAuraResponse getProjectWithTemplate(Id projectId) {
        return GPControllerProjectTemplate.getProjectWithTemplate(projectId);
    }
    
    /**
    * @description Returns project template record for template Id.
    * @param Id projectTemplateId
    * 
    * @return GPAuraResponse json of Project template data
    * 
    * @example
    * GPCmpServiceProjectTemplate.getProjectTemplate(<projectTemplateId>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectTemplate(Id projectTemplateId) {
       return GPControllerProjectTemplate.getDeltaProjectTemplate(projectTemplateId); // return GPControllerProjectTemplate.getProjectTemplate(projectTemplateId, lstOfDeltaProjectTemplateFields);
    }
    
    /**
    * @description creates project template record.
    * @param String serializedTemplateRecord
    * 
    * @return GPAuraResponse json Status if project template is saved/updated successfully.
    * 
    * @example
    * GPCmpServiceProjectTemplate.createProjectTemplate(<serializedTemplateRecord>);
    */
    @AuraEnabled
    public static GPAuraResponse createProjectTemplate(String serializedTemplateRecord) {
        return GPControllerProjectTemplate.createProjectTemplate(serializedTemplateRecord);
    }

    @AuraEnabled
    public static GPAuraResponse getStageWiseFieldForProject(Id projectId) {
        return GPControllerProjectTemplate.getStageWiseFieldForProject(projectId);
    }
}