global class GPBatchSendReportForAutoRejectedWF implements Database.Batchable<sObject>, Database.stateful, schedulable 
{
    static string header = 'SN , Workflow ID , Project Number , Workflow Type, SDO , Project Type , Business Name, Start Date, End Date , Org Name , Workflow Submitted Date , Workflow Status , Workflow Requestor-Name , Workflow Requestor-OHR ID , PID Approver Name , PID Approver OHR ID , Ageing    \n';
    //'SN , WF , PID Number , Workflow Type , SDO , Project Type , Business Name, Start Date, End Date , 
    //Org Name , Project Submission Date , PID Creator OHR ID , PID Creator Name , PID Approver OHR ID , 
    //PID Approver Name , Requestor Comments , Workflow Status    \n';
    static string finalstr = header ;
    List<GP_Project__c> finalLstPrj=new List<GP_Project__c>();
    global Database.QueryLocator start(Database.BatchableContext bc)
    {  
        return Database.getQueryLocator(System.Label.GP_Batch_AutoReject_WF_Query);
    }
    global void execute(Database.BatchableContext bc , List<GP_Project__c> lstProject)
    {        
        if(lstProject.size()>0)  
        {
            finalLstPrj.addAll(lstProject);
            //SendEmail(lstProject);
        }
    }
    global void finish(Database.BatchableContext bc)
    {     
        if(finalLstPrj.size()>0)  
        {          
            SendEmail(finalLstPrj);
        }
    }
    global void execute(SchedulableContext sc)
    {
        GPBatchSendReportForAutoRejectedWF objAutoReject = new GPBatchSendReportForAutoRejectedWF();
        Database.executebatch(objAutoReject);         
    }
    public static void SendEmail(List<GP_Project__c> lstPrj)
    {
        system.debug('===lstPrj==='+lstPrj.size());
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        // List<GP_Project__c> lstproject=new List<GP_Project__c>();
        //String DeveloperName='';
        // DeveloperName = DayCount != 31 ? 'GP_PID_Send_Workflow_Report_For_AutoReject':'GP_PID_Approval_Workflow_Alert';
        EmailTemplate eTemplate = [SELECT id, DeveloperName, body, subject, htmlvalue
                                   FROM EmailTemplate 
                                   WHERE developername =: 'GP_PID_Send_Workflow_Report_For_AutoReject'
                                   LIMIT 1];        
        
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();       
        if(eTemplate != null && lstPrj.size()>0)
        {
            
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(GetAttachmentDetails(lstPrj));
            string csvname= 'AutoRejectedWorkflows_'+String.valueOf(System.today())+'.csv';
            csvAttc.setFileName(csvname);
            csvAttc.setBody(csvBlob);
            
            List<String> toAddress=new List<String>();
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();            
            String toAddresses = system.label.GP_Genpact_PID_Governance;
            email.setToAddresses(toAddresses.split(','));
            email.sethtmlBody(eTemplate.HtmlValue);
            email.setSubject(eTemplate.Subject.replace('Date',String.valueOf(System.today())));
            //email.sethtmlBody(email.getHtmlBody().replace('LIST_PID_RECORDS', SetMultipleWorkflowDetails(lstPrj)));  
            email.sethtmlBody(email.getHtmlBody()); 
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            if (oweaId != null) {
                email.setOrgWideEmailAddressId(oweaId);
            }            
            emailNotifications.add(email);
        }
        system.debug('===emailNotifications==='+emailNotifications.size());
        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
        
    }   
    
    public static String GetAttachmentDetails(List<GP_Project__c> lstPrj)
    {
        Integer i=1;
        for(GP_Project__c prj : lstPrj)
        {
            String ProjectStartDate='';
            if(prj.GP_Start_Date__c != null)
            { ProjectStartDate = String.valueOf(prj.GP_Start_Date__c).split(' ')[0]; } 
            
            String ProjectEndDate='';
            if(prj.GP_End_Date__c != null)
            { ProjectEndDate = String.valueOf(prj.GP_End_Date__c).split(' ')[0]; } 
            
             String PrimarySDOName='';
            if(prj.GP_Primary_SDO__c != null)
            { PrimarySDOName = prj.GP_Primary_SDO__r.Name.replace('–','-'); }
            
            String ProjectOrgName='';
            if(prj.GP_Project_Organization__c != null)
            { ProjectOrgName = prj.GP_Project_Organization__r.Name; }
          
            
            
            string recordString = i +','+ prj.GP_EP_Project_Number__c+','
                + prj.GP_Oracle_PID__c+','
                + (prj.GP_Oracle_PID__c == 'NA' ? 'Creation': 'Modification') +','
                + '"'+ PrimarySDOName +'"' +','
                + prj.GP_Project_type__c+','
                + prj.GP_Business_Name__c+','
                + ProjectStartDate+','
                + ProjectEndDate+','
                + '"'+ ProjectOrgName  +'"' +','                
                + String.ValueOf(prj.GP_Project_Submission_Date__c).split(' ')[0] +','    
                + prj.GP_Approval_Status__c + ','
                + '"'+ prj.GP_Current_Working_User__r.Name +'"' +','
                + prj.GP_Current_Working_User__r.OHR_ID__c + ','
                + '"'+ prj.GP_PID_Approver_User__r.Name   +'"' +',' 
                + prj.GP_PID_Approver_User__r.OHR_ID__c + ','
                + '31' +'\n';
            
            finalstr = finalstr +recordString;
            i++;
        }
        return finalstr;
    }
  
}