public class DeleteOppSPlitfromOpportunity {
    
    public static List<OpportunitySplit> opp_Split_List ;
    public static List<Sales_Unit__c> salesUnitList;
    public static List<OpportunityTeamMember> holdOppTeam;
    static Boolean splitFlag = true;
    
    
    public static void CreateOppTeamonOppCreation(List<Opportunity> oppList) {
        Set<ID> opids=new Set<Id>();
        try{
            List <OpportunitySplit> SplitToInsertI = new List<OpportunitySplit>();
            map<id,user> umap = new map<id,user>([Select id from User where IsActive = true]);
            Id opportunity_split_type = system.Label.OpportunitySplitType;
            Set<Id> oppOwnerId = new Set<Id>();
            
            for(Opportunity opty:oppList){
                opids.add(opty.id);
                oppOwnerId.add(opty.OwnerId);
                if(splitFlag){
                    if(umap.containsKey(opty.OwnerID)){
                    OpportunitySplit oppty_split = new OpportunitySplit(); 
                    oppty_split.SplitPercentage = 100; 
                    oppty_split.SplitTypeId = opportunity_split_type; 
                    oppty_split.SplitOwnerId = opty.OwnerID; 
                    oppty_split.OpportunityId = opty.Id; 
                    SplitToInsertI.add(oppty_split);
                    splitFlag = false;
                    }
                }
            }
            
            //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
            Map<Id,Id> userSuperVisiorMap = new Map<Id,Id>();
            userSuperVisiorMap=getUserSuperVisors(oppOwnerId);

            List<Opportunity> opptlist=[Select id,OwnerId, Account.Client_Partner__c,Opportunity_Source__c,account.SL_ID_18_digit__c, Amount,StageName,Account.Sales_Unit__c,(SELECT Id from OpportunityLineItems) from Opportunity where Id In:opids AND Isclosed=false];
            list<opportunityLineItem> oppItem = new list<opportunityLineItem>();
            List<OpportunityTeamMember> OTM_list = new  List<OpportunityTeamMember>();
            List<OpportunityShare> oppShareList = new List <OpportunityShare>();
            
            For(Opportunity opp : opptList) {
                oppItem = opp.OpportunityLineItems;
                 //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access
                if(userSuperVisiorMap != null && userSuperVisiorMap.size() > 0 && userSuperVisiorMap.get(opp.OwnerId) != null){
                    OpportunityShare oppShare = new OpportunityShare(); oppShare.OpportunityAccessLevel = 'Read';  oppShare.OpportunityId = opp.Id; oppShare.UserOrGroupId = userSuperVisiorMap.get(opp.OwnerId);  oppShareList.add(oppShare);
                }
                if(opp.stagename=='1. Discover' && opp.Account.Client_Partner__c!= opp.Account.SL_ID_18_digit__c && opp.Account.Client_Partner__c!=NULL)
                {
                    if(Opp.OwnerID!=opp.Account.Client_Partner__c && umap.containsKey(opp.Account.Client_Partner__c)){
                        OpportunityTeamMember opp_team_member = new OpportunityTeamMember();
                        opp_team_member.OpportunityAccessLevel = 'Edit';
                        opp_team_member.OpportunityId = opp.id;
                        opp_team_member.UserId = opp.Account.Client_Partner__c;
                        opp_team_member.TeamMemberRole = 'Super SL';
                        OTM_list.add(opp_team_member); 
                        
                        OpportunitySplit oppty_split = new OpportunitySplit(); 
                        oppty_split.SplitPercentage = 100; 
                        oppty_split.SplitTypeId = opportunity_split_type; 
                        oppty_split.SplitOwnerId = opp.Account.Client_Partner__c; 
                        oppty_split.OpportunityId = opp.Id; 
                        SplitToInsertI.add(oppty_split); 
                    }
                    if(opp.Account.SL_ID_18_digit__c != null) {               
                        if(Opp.OwnerID!=opp.Account.SL_ID_18_digit__c && umap.containsKey(opp.Account.SL_ID_18_digit__c)) {    
                            OpportunityTeamMember opp_team_member = new OpportunityTeamMember();
                            opp_team_member.OpportunityAccessLevel = 'Edit';
                            opp_team_member.OpportunityId = opp.id;
                            opp_team_member.UserId = opp.Account.SL_ID_18_digit__c;
                            opp_team_member.TeamMemberRole = 'SL';
                            OTM_list.add(opp_team_member); 
                            
                            OpportunitySplit oppty_split = new OpportunitySplit(); 
                            oppty_split.SplitPercentage = 100; 
                            oppty_split.SplitTypeId = opportunity_split_type; 
                            oppty_split.SplitOwnerId = opp.Account.SL_ID_18_digit__c; 
                            oppty_split.OpportunityId = opp.Id; 
                            SplitToInsertI.add(oppty_split);
                        }    
                    }
                } 
              if(opp.stagename=='1. Discover' && opp.Account.Client_Partner__c == opp.Account.SL_ID_18_digit__c) { 
                    if(Opp.OwnerID!=opp.Account.Client_Partner__c && umap.containsKey(opp.Account.Client_Partner__c)) {
                        OpportunityTeamMember opp_team_member = new OpportunityTeamMember();
                        opp_team_member.OpportunityAccessLevel = 'Edit';
                        opp_team_member.OpportunityId = opp.id;
                        opp_team_member.UserId = opp.Account.Client_Partner__c;
                        opp_team_member.TeamMemberRole = 'Super SL';
                        OTM_list.add(opp_team_member);   
                        
                        OpportunitySplit oppty_split = new OpportunitySplit(); 
                        oppty_split.SplitPercentage = 100; 
                        oppty_split.SplitTypeId = opportunity_split_type; 
                        oppty_split.SplitOwnerId = opp.Account.Client_Partner__c; 
                        oppty_split.OpportunityId = opp.Id; 
                        SplitToInsertI.add(oppty_split);
                    }
                    
                }
            }
            if(Label.debugclass == 'DeleteOppSPlitfromOpportunity'){
                system.debug('==SplitToInsertI=='+SplitToInsertI);
                system.debug('==SplitToInsertI=='+SplitToInsertI);
            }
            if(OTM_List.size() > 0){
                insert OTM_List; 
            }
            if(SplitToInsertI.size() > 0 ){
                insert SplitToInsertI;
            }
            if(oppShareList != null && oppShareList.size() > 0){
                insert oppShareList ;
            }
        } catch (Exception e) { CreateErrorLog.createErrorRecord(String.valueOf(opids),e.getMessage(), '', e.getStackTraceString(),'DeleteOppSPlitfromOpportunity', 'CreateOppTeamonOppCreation','Fail','',String.valueOf(e.getLineNumber())); 

            System.debug(':---Error---: >'+e.getMessage() + ':--Error Line Number--:  >'+e.getLineNumber());
        }
    }
    public static void holdOpportunityTeam(List<Opportunity> oppList,Map<ID, Opportunity> old_opp_map){
        set<id> OppId = new set<id>();
        try{
            holdOppTeam = new list<OpportunityTeamMember>();
        Boolean ownerChangeFlag = false;
        String oppOwner;
        for(Opportunity opp : oppList){
            OppId.add(opp.Id);
            if (old_opp_map.get(Opp.id).OwnerId != Opp.OwnerId) {
                ownerChangeFlag = true; oppOwner = old_opp_map.get(Opp.id).OwnerId;
            }
        }
        if(ownerChangeFlag){
            holdOppTeam = [SELECT id, UserId, OpportunityId, OpportunityAccessLevel, TeamMemberRole FROM OpportunityTeamMember WHERE opportunityId =:old_opp_map.keySet() AND UserId !=: oppOwner ];
          if(Label.debugclass == 'DeleteOppSPlitfromOpportunity'){
            system.debug(':---holdOppTeam---:'+holdOppTeam);
          }
        }
        }catch (Exception e) {
            System.debug(':---Error---: >'+e.getMessage() + ':--Error Line Number--:  >'+e.getLineNumber());
            CreateErrorLog.createErrorRecord(String.valueOf(OppId),e.getMessage(), '', e.getStackTraceString(),'DeleteOppSPlitfromOpportunity', 'holdOpportunityTeam','Fail','',String.valueOf(e.getLineNumber())); 
        }  
    }
    
    public static void deleteOppSplitOwneronOppOwnerchange(List<Opportunity> oppList,Map<ID, Opportunity> old_opp_map){ 
       Set<ID> oppId = new Set<Id>();
        try{
            Set<ID> oppOwnerIds = new Set<Id>();
            map<id,user> umap = new map<id,user>([Select id from User where IsActive = true]);
            Set<ID> salesLeaderIds = New set<Id>();
            Set<ID> userId = New set<Id>(); 
            Boolean UpdateOwner = False;
            Set<Id> newOppOwners = new Set<Id>();
            Set<Id> oldOppOwners = new Set<Id>();
            string oppTeam;
            for(opportunity opp : oppList){
                oppOwnerIds.add(opp.OwnerId);
                oppId.add(opp.Id);
         
               if(opp.OwnerId != old_opp_map.get(opp.Id).OwnerId){  UpdateOwner = true; 
                    //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
                    newOppOwners.add(opp.OwnerID);    oldOppOwners.add(old_opp_map.get(opp.Id).OwnerId);
                }
            }
           
            //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
            Map<Id,Id> userSuperVisiorMap = new Map<Id,Id>();
            userSuperVisiorMap=getUserSuperVisors(newOppOwners);
            
          
            if(true){
                List<OpportunityTeamMember> insertTeamMember = new list<OpportunityTeamMember>();
                List <OpportunitySplit> splitToInsert = new List<OpportunitySplit>();
                Id opportunity_split_type = system.Label.OpportunitySplitType;
                list<OpportunitySplit>  oppSplitLst = [SELECT id FROM OpportunitySplit WHERE OpportunityID IN :old_opp_map.keyset() AND splitTypeId =: opportunity_split_type]; 
                system.debug(':---oppSplitLst--> '+oppSplitLst);
                if(oppSplitLst.size() >0 ) { 
                    delete oppSplitLst;
                }
                if(holdOppTeam != null){
                    if(holdOppTeam.size() > 0){
                        if(Label.debugclass == 'DeleteOppSPlitfromOpportunity'){
                            system.debug(':--test insertTeamMember --:'+holdOppTeam);
                        }
                        for(OpportunityTeamMember oppTem: holdOppTeam){  
                            OpportunityTeamMember opp_team_member = new OpportunityTeamMember();    
                            opp_team_member.OpportunityAccessLevel = oppTem.OpportunityAccessLevel;   
                            opp_team_member.OpportunityId = oppTem.OpportunityId;  
                            opp_team_member.UserId = oppTem.UserId; 
                            opp_team_member.TeamMemberRole = oppTem.TeamMemberRole; 
                            insertTeamMember.add(opp_team_member);    
                            userId.add(oppTem.UserId);
                        }
                    }
                }
                system.debug(':--insertTeamMember insertTeamMember --:'+insertTeamMember);
                Map<id,opportunity> oppMap =new Map<id,opportunity>([SELECT id,OwnerId, amount, AccountId, Account.Client_Partner__c,StageName,account.Sales_Unit__c,account.Sales_Leader_User_Id__c  FROM Opportunity WHERE Id IN :OppId AND IsClosed = False]);
              
                List<OpportunityShare> oppShareList = new List <OpportunityShare>();
                for(opportunity opp : oppMap.values()){
                    salesLeaderIds.add(opp.OwnerId);
                    salesLeaderIds.add(opp.Account.Client_Partner__c);
                    salesLeaderIds.add(opp.Account.Sales_Leader_User_Id__c);
                    
                    //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
                    if(userSuperVisiorMap != null && userSuperVisiorMap.size() > 0 && userSuperVisiorMap.get(opp.OwnerId) != null){
                        OpportunityShare oppShare = new OpportunityShare(); 
                        oppShare.OpportunityAccessLevel = 'Read'; 
                        oppShare.OpportunityId = opp.Id; 
                        oppShare.UserOrGroupId = userSuperVisiorMap.get(opp.OwnerId); 
                        oppShareList.add(oppShare);
                    }
                    
                    if(splitFlag){
                        OpportunitySplit oppty_splitownr = new OpportunitySplit();   
                        oppty_splitownr.SplitPercentage = 100;    
                        oppty_splitownr.SplitTypeId = opportunity_split_type;  
                        oppty_splitownr.SplitOwnerId = opp.OwnerId;  
                        oppty_splitownr.OpportunityId = opp.id;   
                        splitToInsert.add(oppty_splitownr);  
                        splitFlag = false;
                    }
                    if(opp.OwnerId != opp.Account.Client_Partner__c && umap.containsKey(opp.Account.Client_Partner__c)) {
                            
                            OpportunitySplit oppty_split = new OpportunitySplit();  
                            oppty_split.SplitPercentage = 100; 
                            oppty_split.SplitTypeId = opportunity_split_type; 
                            oppty_split.SplitOwnerId = opp.Account.Client_Partner__c;
                            oppty_split.OpportunityId = opp.id;
                            splitToInsert.add(oppty_split);
                    
                        if(!userId.contains(opp.Account.Client_Partner__c) && umap.containsKey(opp.Account.Client_Partner__c)){
                            
                            OpportunityTeamMember opp_team_member = new OpportunityTeamMember();  
                            opp_team_member.OpportunityAccessLevel = 'Edit'; 
                            opp_team_member.OpportunityId = opp.Id;
                            opp_team_member.UserId = opp.Account.Client_Partner__c;
                            opp_team_member.TeamMemberRole = 'Super SL';
                            insertTeamMember.add(opp_team_member);
                        }
                    }
                    if(opp.OwnerId != opp.Account.Sales_Leader_User_Id__c && opp.Account.Sales_Leader_User_Id__c != opp.Account.Client_Partner__c) {
                        if(umap.containsKey(opp.Account.Sales_Leader_User_Id__c)){
                            OpportunitySplit oppty_split1 = new OpportunitySplit();  
                            oppty_split1.SplitPercentage = 100; 
                            oppty_split1.SplitTypeId = opportunity_split_type; 
                            oppty_split1.SplitOwnerId = opp.Account.Sales_Leader_User_Id__c;
                            oppty_split1.OpportunityId = opp.id;
                            splitToInsert.add(oppty_split1);
                        }
                        if(!userId.contains(opp.Account.Sales_Leader_User_Id__c) && umap.containsKey(opp.Account.Sales_Leader_User_Id__c)){
                            OpportunityTeamMember opp_team_member1 = new OpportunityTeamMember();  
                            opp_team_member1.OpportunityAccessLevel = 'Edit'; 
                            opp_team_member1.OpportunityId = opp.Id;
                            opp_team_member1.UserId = opp.Account.Sales_Leader_User_Id__c;
                            opp_team_member1.TeamMemberRole = 'SL';
                            insertTeamMember.add(opp_team_member1);
                        }
                    }  
                } 
             
                
                if(insertTeamMember.size() > 0){
                    insert insertTeamMember;
                }
                if(splitToInsert.size() > 0){
                    insert splitToInsert;
                }
                
                
                //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
                if(oldOppOwners != null && oldOppOwners.size() > 0){
                    Map<Id,Id> oldUserSuperVisiorMap = new Map<Id,Id>();
                    oldUserSuperVisiorMap=getUserSuperVisors(oldOppOwners);
                    
                    if(oldUserSuperVisiorMap != null && oldUserSuperVisiorMap.size() > 0 && oldUserSuperVisiorMap.values() != null &&
                        oldUserSuperVisiorMap.values().size() > 0 ){
                        
                        List<OpportunityShare> existingOppShareList = new List<OpportunityShare>();
                                        
                        existingOppShareList = [Select Id,OpportunityId,UserOrGroupId,OpportunityAccessLevel from OpportunityShare   where OpportunityId IN:old_opp_map.keyset() and    UserOrGroupId IN:oldUserSuperVisiorMap.values() and 
                                                RowCause = 'Manual'];
                        
                        System.debug('\n\n ----->>> Oppn share List '+existingOppShareList);
                        
                        if(existingOppShareList != null && existingOppShareList.size() > 0){                            
                                delete existingOppShareList;
                        }
                    }
                }
                if(oppShareList != null && oppShareList.size() > 0){
                    insert oppShareList;
                }
            }
        } catch (Exception e) {
             System.debug(':---Error---: >'+e.getMessage() + ':--Error Line Number--:  >'+e.getLineNumber());
            CreateErrorLog.createErrorRecord(String.valueOf(oppId),e.getMessage(), '', e.getStackTraceString(),'DeleteOppSPlitfromOpportunity', 'deleteOppSplitOwneronOppOwnerchange','Fail','',String.valueOf(e.getLineNumber())); 
          
        }
    }
    
    //Added By Madhuri - Date : 18/09/2018 - VIC Opportunity Owner Supervisor access 
    public static Map<Id,Id> getUserSuperVisors(Set<Id> userIds){
            
        List<User> userList = new List<User>();
        userList = [Select Id,name,Supervisor__c from User where Id IN:userIds];
        Map<Id,Id> userSuperVisiorMap = new Map<Id,Id>();
        if(Label.debugclass == 'DeleteOppSPlitfromOpportunity'){
            System.debug('\n\n ----------->>> userList : '+userList );
        }
        if(userList != null && userList.size() > 0){
            for(User u:userList){
                
                System.debug('\n\n ----------->>> Supervisor: '+u.Supervisor__c );
                if(u.Supervisor__c != null){
                    userSuperVisiorMap.put(u.Id,u.Supervisor__c);
                }
            }
        }        
        return userSuperVisiorMap;
    }
}