@isTest
public class GPSelectorProjectTracker {
    public Static GP_Project__c Project;
    
    @testSetup
    public static void buildDependencyData() {
GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Primary_SDO__c = sdo.Id;
        insert parentProject;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Primary_SDO__c = sdo.Id;
        insert project ;
        
        GP_Project_Address__c directAddress = GPCommonTracker.getProjectAddress(project.Id, null, null);
        insert directAddress;
    }
    
    @isTest
    public static void testgetSObjectType() {
       GPSelectorProject ProjectSelector = new GPSelectorProject();
        Schema.SObjectType returnedType = ProjectSelector.getSObjectType();
        Schema.SObjectType expectedType = GP_Project__c.sObjectType;
        
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectProjectShareById() {
        GPSelectorProject ProjectSelector = new GPSelectorProject();
        Project = [Select Id from GP_Project__c Limit 1];
        GP_Project_Address__c directAddress = [Select Id, GP_Bill_to_Address__c, GP_Customer_Name__c, GP_Ship_to_Address__c 
                                               from GP_Project_Address__c LIMIT 1];
        
        list<GP_Project__share> returnedProjectShare = ProjectSelector.selectProjectShareById(new Set<Id> {
            Project.Id
        });
        
        //System.assertEquals(returnedProjectShare.ParentId, Project.id);
        List<GP_Project__c> listOfReturnedProjects = ProjectSelector.selectById(new Set<Id> {
            Project.Id
             });
        
        System.assertEquals(listOfReturnedProjects.size(), 1);

        try{
         
            ProjectSelector.selectProjectShareById(null);  
        } catch(Exception ex) {}
        try{
            ProjectSelector.selectById(null);  
        } catch(Exception ex) {}
        
        
        try{
            ProjectSelector.selectProjectShareById(new Set<Id>());
            ProjectSelector.selectProjectShareById(new Set<Id>(), new Set<Id>()); 
            ProjectSelector.selectManualProjectShareById(new Set<Id>());
        } catch(Exception ex) {}
        
        try{
            ProjectSelector.selectProjectRecord(Project.Id);  
        } catch(Exception ex) {}
        
        
        ProjectSelector.getProjectWithRelatedData(Project.Id);  
        ProjectSelector.getProjectDeatil(new Set<Id>());
        ProjectSelector.getProjectAndProjectTasks(null);
        ProjectSelector.getProjectFieldDetail(project.Id);
        ProjectSelector.getApprovedProjectApprovedFieldDetail(null);
        ProjectSelector.getDeal(null);
        ProjectSelector.getProjectID(null);
        ProjectSelector.getProjectsWithPendingApproval(null);
        ProjectSelector.getProjectsForClosureNotification(new List<Id> {project.Id});
        GPSelectorProject.getApprovedProjectUnderDeal(null);
        ProjectSelector.getProjectPrimarySDO(project.Id);
        ProjectSelector.getProject(project.Id);
        ProjectSelector.getProject(new Set<Id>());
        ProjectSelector.getProjectsWithBillingEntityDetails(new Set<String>());
        ProjectSelector.getProjectsWithBillingEntityDetails(new Set<Id>());
        ProjectSelector.getProjectMap(null);
        ProjectSelector.getProjectRecordForClosure(project.Id);
        ProjectSelector.getProjectProcessInstanceForClosure(project.Id);
        ProjectSelector.getProjectListForOracleId(null);
        ProjectSelector.getBillingCurrencyFromProject(null);
        ProjectSelector.getProjectRecordsWithWorkLocation(null);
        ProjectSelector.getProjectRecordsWithPODocument(null);
        ProjectSelector.getParentProjectDetails(new Set<Id> {project.Id}, new Set<String> {'Name'});
        GPSelectorProject.getParentProjectForGroupInvoice(directAddress);
    }
    
}