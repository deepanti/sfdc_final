@isTest
public class GPSelectorJobTracker {
	@isTest
    static void testJobSelector() {
        GP_job__c job = new GP_job__c();
        job.GP_Job_Type__c = 'Upload Time Sheet Entries';
        job.GP_Column_s_To_Update_Upload__c = 'GP_Project__c;GP_TSC_Ex_Type__c;GP_PL_Employee_OHR__c;GP_TSC_Modified_Hours__c;GP_TSC_Project_Task__c;GP_TSC_Date__c';
        job.OwnerId = userInfo.getUserId();
        insert job;
        
        GPSelectorJob jobSelector = new GPSelectorJob();
        jobSelector.selectById(new Set<Id>());
    	jobSelector.SelectJobRecord(job.Id);
        jobSelector.selectJobRecordsWithoutChildRecords(null);
        jobSelector.SelectJobRecords();
    }
}