@isTest
public class GPDomainUserRoleTracker {
    public static GP_Role__c objrole;
    public static GP_User_Role__c userRole;
    public static user Usr;
    public Static Account objAccount;
    
    
    @testSetup
    public static void buildDependencyData() 
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_user_role__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        Insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        Insert objSB;
        
        GP_Work_Location__c objSdo1 = GPCommonTracker.getWorkLocation();
        objSdo1.GP_Status__c = 'Active and Visible';
        objSdo1.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo1;
        
        Account objAccount = GPCommonTracker.getAccount(objBS.Id,objSB.Id);
        insert objAccount;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        objuser.username = 'test555555555@jkj.ccc';
        objuser.lastname = 'asassas';
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        try{
        delete objuserrole;
        } 
        catch (Exception e) {
        }
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Approval_Status__c = 'Approved';
        //insert prjObj ;
        
        //GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        //insert timeshtentryObj ;
        
       /* objrole.GP_Active__c = true;
        update objrole;
        
        objrole.GP_Active__c = false;
        update objrole;*/
    }
    
     @isTest
    public static void testGPDomainUserRole() {
        
    }
}