@isTest 
public class GPServiceProjectClassificationTracker { 
    
    public static GP_Project__c project;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        
        insert dealObj;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        objprjtemp.GP_Business_Group_L1__c = 'group1';
        objprjtemp.GP_Business_Segment_L2__c = 'segment2';
        insert objprjtemp;
        
        GP_Project__c parentProjectObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        parentProjectObj.OwnerId = objuser.Id;
        parentProjectObj.GP_CRN_Number__c = iconMaster.Id;
        parentProjectObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        parentProjectObj.GP_Project_Type__c = 'Fixed monthly';
        parentProjectObj.GP_HSN_Category_Id__c = '123456';
        parentProjectObj.GP_Sales_Opportunity_Id__c = '234567';
        parentProjectObj.GP_Oracle_PID__c = '123456';
        parentProjectObj.GP_Business_Group_L1__c = 'GE';
        insert parentProjectObj;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Project_Type__c = 'Fixed monthly';
        prjObj.GP_HSN_Category_Id__c = '123456';
        prjObj.GP_Sales_Opportunity_Id__c = '234567';
        prjObj.GP_Parent_Project__c = parentProjectObj.Id;
        insert prjObj;
    }
    
    @isTest
    public static void testGPCmpServiceProjectAddress() {
        fetchData();
        testCreateProjectClassification();
    }
    
    private static void testCreateProjectClassification() {
        GPServiceProjectClassification projectClassificationService = new GPServiceProjectClassification(project.Id);
        projectClassificationService = new GPServiceProjectClassification(new Set<Id> {project.Id});
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
        
        project.GP_Oracle_PID__c = '123456';
        project.GP_Business_Group_L1__c = 'Genpact';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
        
        project.GP_Business_Group_L1__c = 'Global Clients';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
        
        project.GP_Business_Group_L1__c = 'GE';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
        
        project.GP_HSN_Category_Id__c = '';
        project.GP_Sales_Opportunity_Id__c = '';
        project.GP_PBB_Flag__c = 'Yes';
        project.GP_Invoice_Frequency__c  = 'Quarterly';
        project.GP_Oracle_PID__c = 'NA';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();        
        
        project.GP_Project_type__c = 'T&M';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
        // Gainshare Change
        project.GP_Project_type__c = 'Gainshare';
        projectClassificationService = new GPServiceProjectClassification(new List<GP_Project__c> {project});
        projectClassificationService.createProjectClassification();
    }
    
    private static void fetchData() {
        //project = [select id, Name from GP_Project__c limit 1];
        
        Map<String, Schema.SObjectField> MapOfSchema = Schema.SObjectType.GP_Project__c.fields.getMap();
        
        String projectQueryString = 'Select ';
        
        for(String fieldName : MapOfSchema.keySet()) {
            projectQueryString += fieldName + ', ';
        }
        
        projectQueryString += 'RecordType.Name,GP_HSL__r.GP_Oracle_Id__c,CreatedBy.GP_Person_Id__c, GP_Project_Template__r.GP_Final_JSON_1__c, GP_Project_Template__r.GP_Final_JSON_2__c, GP_Project_Template__r.GP_Final_JSON_3__c, ';
        projectQueryString += 'GP_Collector__r.GP_Final_OHR__c,GP_Collector__r.GP_Person_ID__c, GP_GOL__r.GP_Final_OHR__c,GP_GOL__r.GP_Person_ID__c,GP_GRM__r.GP_Person_ID__c, GP_GRM__r.GP_Final_OHR__c, ';
        projectQueryString += 'GP_Deal__r.GP_Service_Line1_ID__c, GP_Deal__r.GP_Sales_Opportunity_Id__c, GP_Deal__r.GP_Internal_Opportunity_ID__c, GP_Primary_SDO__r.GP_Oracle_Id__c, ';
        projectQueryString += 'GP_Deal__r.GP_Business_Segment_L2_Id__c, GP_Deal__r.GP_Sub_Business_L3_Id__c, ';
        projectQueryString += '(select id, SubmittedById from ProcessInstances ORDER BY CreatedDate DESC), ';
        
        projectQueryString += '(SELECT Id, GP_Project__c, GP_Work_Location__c, GP_Primary__c, RecordType.Name, GP_BCP_FLAG__c,GP_IsUpdated__c, ';
        projectQueryString += 'GP_Work_Location__r.GP_Start_Date_Active__c, GP_Work_Location__r.GP_End_Date_Active__c,';
        projectQueryString += 'GP_Work_Location__r.GP_Status__c, GP_Work_Location__r.RecordType.Name,';
        projectQueryString += 'GP_Work_Location__r.GP_Billable__c, GP_Work_Location__r.GP_Hide_For_BPM__c,';
        projectQueryString += 'GP_Work_Location__r.GP_Country__c, GP_Work_Location__r.Name, GP_Work_Location__r.GP_Oracle_Id__c, GP_Project_Record_Type__c ';
        projectQueryString +=  'FROM GP_Project_Work_Location_SDOs__r) ';
        
        projectQueryString += 'from GP_Project__c where GP_Parent_Project__c != null';
        project = Database.query(projectQueryString);
    }
}