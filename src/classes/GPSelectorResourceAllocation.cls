public class GPSelectorResourceAllocation extends fflib_SObjectSelector {
    
    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Resource_Allocation__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Resource_Allocation__c.sObjectType;
    }
    
    public List < GP_Resource_Allocation__c > selectById(Set < ID > idSet) {
        return (List < GP_Resource_Allocation__c > ) selectSObjectsById(idSet);
    }
    
    public GP_Resource_Allocation__c selectResourceAllocationRecord(Id resourceAllocationId) {
        return [Select Id, Name, GP_Project__c, GP_Employee__c, GP_Employee__r.Name, GP_Project__r.GP_Project_type__c,GP_Bill_Rate__c,GP_Committed_SOW__c,
                GP_Work_Location__r.Name, GP_SDO__r.Name, GP_Employee_HR_History__c, GP_Percentage_allocation__c,GP_COST_CENTER__c,GP_Interviewed_by_Customer__c,
                GP_Work_Location__c, GP_Start_Date__c, GP_End_Date__c, GP_Project__r.RecordType.Name,GP_Employee__r.GP_Payroll_Country__c,
                GP_SDO__c, GP_Project__r.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c,GP_Employee__r.GP_ACTUAL_TERMINATION_Date__c,
                GP_Employee__r.GP_Final_OHR__c, GP_Profile__c, GP_Parent_Resource_Allocation__c,GP_MS_Payment_Mode__c,GP_Oracle_Id__c,
                GP_Employee__r.GP_EMPLOYEE_TYPE__c, GP_Employee__r.GP_Employee_Category__c,GP_Shadow_type__c,GP_CM_Address__c,GP_CM_Id__c,
                GP_Employee__r.GP_SDO__r.Name, GP_Employee__r.GP_isActive__c,GP_Employee__r.GP_Business_Group_NAME__c,GP_Employee__r.GP_HIRE_Date__c,
                GP_Employee__r.GP_SDO__c, GP_Employee__r.GP_BAND__c, GP_Employee__r.GP_LOCATION__c,GP_MS_MANAGER__c,GP_Oracle_Status__c,
                GP_Employee__r.GP_Work_Location__r.Name, GP_Employee__r.GP_Legal_Entity_Name__c, GP_Employee__r.GP_Legal_Entity_Code__c, GP_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
                GP_Employee__r.GP_ORGANIZATION__c, GP_SDO__r.GP_Oracle_Id__c,GP_Employee__r.GP_Employee_Unique_Name__c,GP_MS_Address__c,
                GP_Allocation_Type__c, GP_Project__r.Name,GP_MS_Attention_To__c,GP_MS_BusinessUnit_Manager__c,GP_MS_MER_No__c,GP_MS_ID__c,
                GP_Employee__r.GP_Employee_HR_History__r.Work_Location_SDO_Master__c, GP_Project__r.GP_Oracle_PID__c,GP_MS_Email_Id__c,GP_Oracle_Process_Log__c
                from GP_Resource_Allocation__c
                where Id =: resourceAllocationId and GP_Employee__c != null and GP_Project__c != null
               ];
    }
    
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsOfEmployees(Set < Id > setOfEmployeeId) {
        return [SELECT Id, Name, GP_Project__c, GP_Employee__c,
                GP_Employee__r.Name, GP_Work_Location__r.Name,
                GP_SDO__r.Name, GP_Employee_HR_History__c,
                GP_Work_Location__c, GP_Start_Date__c,
                GP_End_Date__c, GP_Percentage_allocation__c,
                GP_SDO__c, GP_Bill_Rate__c, GP_Profile__c,GP_Shadow_type__c,
                GP_Allocation_Type__c, GP_Project__r.Name, GP_Employee__r.GP_Employee_Unique_Name__c
                FROM GP_Resource_Allocation__c
                where GP_Employee__c in: setOfEmployeeId
                AND(GP_Project__r.RecordType.Name = 'CMITS'
                    OR GP_Project__r.RecordType.Name = 'OTHER PBB')
               ];
    }
    
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsForAMonth(String employeeId, Integer month) {
        return [select id, GP_Project__c,GP_Project__r.GP_Oracle_PID__c, GP_Oracle_Process_Log__c, GP_End_Date__c, GP_Start_Date__c, GP_Profile__c, GP_Shadow_type__c,
                GP_Employee__c from GP_Resource_Allocation__c where
                GP_Employee__c =: employeeId and GP_Project__c != null and GP_Project__r.GP_Approval_Status__c = 'Approved' 
                and GP_Project__r.GP_Oracle_Status__c = 'S' 
                and(CALENDAR_MONTH(GP_Start_Date__c) =: month OR CALENDAR_MONTH(GP_End_Date__c) =: month)
               ];
    }
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsForAMonth(String employeeId, Date monthStartDate, Date monthEndDate) {
        return [select id, GP_Project__c, GP_Project__r.Name,GP_Project__r.RecordType.Name,GP_Project__r.GP_Oracle_PID__c,GP_Shadow_type__c,
                GP_End_Date__c, GP_Start_Date__c, GP_Profile__c, GP_Employee__c from GP_Resource_Allocation__c where
                GP_Employee__c =: employeeId and GP_Project__c != null and GP_Project__r.GP_Approval_Status__c = 'Approved' 
                and GP_Project__r.GP_Oracle_Status__c = 'S' AND((GP_Start_Date__c >= :monthStartDate AND
                                                                 GP_Start_Date__c <= :monthEndDate) OR
                                                                (GP_End_Date__c >= :monthStartDate AND
                                                                 GP_End_Date__c <= :monthEndDate) OR
                                                                (GP_Start_Date__c <= :monthStartDate AND
                                                                 GP_End_Date__c >= :monthEndDate))
               ];
    }
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsForAProject(String projectId) {
        return [SELECT Id, GP_Employee__c, GP_Employee__r.Name, GP_Project__c, GP_Project__r.Name, GP_Committed_SOW__c,GP_Oracle_Status__c,
                GP_Start_Date__c, GP_End_Date__c, GP_Work_Location__r.Name, GP_Oracle_Id__c,GP_Employee__r.GP_Payroll_Country__c,
                GP_Allocation_Type__c, GP_Bill_Rate__c, GP_SDO__r.Name, GP_Employee__r.GP_Employee_Unique_Name__c,GP_MS_MANAGER__c,
                GP_Employee__r.GP_Final_OHR__c, GP_Profile__c, GP_Parent_Resource_Allocation__c,GP_Employee__r.GP_ACTUAL_TERMINATION_Date__c,
                GP_Employee__r.GP_EMPLOYEE_TYPE__c, GP_Employee__r.GP_Employee_Category__c,GP_Employee__r.GP_HIRE_Date__c,GP_MS_Payment_Mode__c,
                GP_Employee__r.GP_SDO__r.Name, GP_Employee__r.GP_isActive__c,GP_Employee__r.GP_Business_Group_NAME__c,GP_CM_Address__c,GP_CM_Id__c,
                GP_Employee__r.GP_SDO__c, GP_Employee__r.GP_BAND__c, GP_Employee__r.GP_LOCATION__c,GP_MS_Address__c,GP_MS_Email_Id__c,GP_MS_ID__c,
                GP_Employee__r.GP_Work_Location__r.Name, GP_Employee__r.GP_Legal_Entity_Name__c, GP_Employee__r.GP_Legal_Entity_Code__c, 
                GP_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c, GP_MS_Attention_To__c,GP_MS_BusinessUnit_Manager__c,GP_MS_MER_No__c,
                GP_Employee__r.GP_ORGANIZATION__c, GP_SDO__r.GP_Oracle_Id__c,GP_Shadow_type__c,GP_COST_CENTER__c,GP_Interviewed_by_Customer__c,
                GP_Percentage_allocation__c,GP_Oracle_Process_Log__c
                FROM GP_Resource_Allocation__c
                WHERE GP_Project__c =: projectId ORDER By LastModifiedDate DESC
               ];
    }
    
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsOfEmployeesAndProjects(Set < Id > setOfEmployeeId, Set < String > setOfProjectOraclePId) {
        return [select id, GP_Employee__c, GP_End_Date__c, GP_Start_Date__c, GP_Project__c,GP_Project__r.GP_Oracle_PID__c,
                GP_Project__r.RecordType.Name, GP_Project__r.GP_Is_Closed__c,GP_Shadow_type__c, GP_Oracle_Process_Log__c
                from GP_Resource_Allocation__c where
                GP_Employee__c in: setOfEmployeeId and GP_Project__r.GP_Oracle_PID__c in: setOfProjectOraclePId 
                and GP_Project__r.GP_Approval_Status__c = 'Approved' and GP_Project__r.GP_Oracle_Status__c = 'S'
               ];
    }
    public Map < Id, GP_Resource_Allocation__c > selectResourceAllocationRecords(Set < Id > setOfResourceAllocationIds) {
        return new Map < Id, GP_Resource_Allocation__c > ([SELECT Id, GP_Employee__c, GP_Employee__r.Name, GP_Project__c, GP_Project__r.Name,
                                                           GP_Start_Date__c, GP_End_Date__c, GP_Work_Location__r.Name,
                                                           GP_Allocation_Type__c, GP_Bill_Rate__c, GP_SDO__r.Name,GP_Shadow_type__c,
                                                           GP_Employee__r.GP_Final_OHR__c, GP_Profile__c, GP_Parent_Resource_Allocation__c,
                                                           GP_Percentage_allocation__c
                                                           FROM GP_Resource_Allocation__c
                                                           WHERE Id in: setOfResourceAllocationIds
                                                          ]);
    }
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsOfProject(Set < Id > setOfProjectIds) {
        return [SELECT Id, GP_Employee__c, GP_Employee__r.Name, GP_Project__c, GP_Project__r.Name,
                GP_Start_Date__c, GP_End_Date__c, GP_Work_Location__r.Name,GP_Shadow_type__c,
                GP_Allocation_Type__c, GP_Bill_Rate__c, GP_SDO__r.Name, GP_Employee__r.GP_Employee_Unique_Name__c,
                GP_Employee__r.GP_Final_OHR__c, GP_Profile__c, GP_Parent_Resource_Allocation__c,
                GP_Percentage_allocation__c
                FROM GP_Resource_Allocation__c
                WHERE GP_Project__c in: setOfProjectIds
               ];
    }
    public List < GP_Resource_Allocation__c > selectProjectResourceAllocationRecords(Set < Id > lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Resource_Allocation__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }
    
    
    public List < GP_Resource_Allocation__c > selectResourceAllocationRecordsOfProjectandWorkLocation(Set < Id > setOfProjectIds, set < string > setProjectandWorkLocationMasterKey) {
        return [SELECT Id, GP_Project__c, GP_Work_Location__c,GP_Shadow_type__c
                FROM GP_Resource_Allocation__c
                WHERE GP_Project__c in: setOfProjectIds
                and GP_Project_and_Work_Location_ID__c in: setProjectandWorkLocationMasterKey 
               ];
    }
    
    public List < GP_Resource_Allocation__c > selectActiveResourceRecords(Id projectId) {
        return [SELECT Id, GP_Project__c, GP_Work_Location__c,GP_Shadow_type__c
                FROM GP_Resource_Allocation__c
                WHERE GP_Project__c = :projectId
                AND GP_End_Date__c > TODAY
               ];
    }
}