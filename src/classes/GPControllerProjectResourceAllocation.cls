/**
 * @group ProjectResourceAllocation
 * @group-content ../../ApexDocContent/ProjectResourceAllocation.htm
 *
 * @description Apex controller class having business logics
 * to fetch, insert, update and delete resource to be allocated under a project.
 */
public class GPControllerProjectResourceAllocation {

    private final String RESOURCE_TOTAL_COST_EXCEPTION_LABEL = 'Please update the allocations or modify the budget accordingly';
    private final String BUDGET_EXCEEDED_ERROR_LABEL = 'Resource Total Cost exceeds the Budget of Project!';
    private final String IS_RESOURCE_ALLOCATION_DEFINABLE_LABEL = 'isResourceAllocationDefinable';
    private final String LIST_OF_RESOURCE_ALLOCATION_TEMPLATE_LABEL = 'mapOfFieldNameToTemplate';
    private final String EMPLOYEE_RESOURCE_ALLOCATION_LABEL = 'employeeResourceAllocation';
    private final String RESOURCE_LENDING_SDO_CODE_LABEL = 'resourceLendingSDOOracleCode';
    private final String MAP_OF_EMPLOYEE_LENDING_LABEL = 'mapOfEmployeeVsLendingSDO';
    private final String EMPLOYEE_TIME_SHEET_ENTRY_LABEL = 'employeeTimeSheetEntry';
    private final String RESOURCE_LENDING_SDO_NAME_LABEL = 'resourceLendingSDOName';
    private final String LIST_RESOURCE_ALLOCATION_LABEL = 'lstresourceAllocation';
    private final String RESOURCE_TEMPLATE_LABEL = 'resourceAllocationTemplate';
    private final String NO_OF_ROWS_TO_BE_DISPLAYED = 'noOfRowsToBeDisplayed';
    private final String RESOURCE_LENDING_SDO_LABEL = 'resourceLendingSDO';
    private final String RESOURCE_ALLOCATION_LABEL = 'resourceAllocation';
    private final String MAP_OF_WORKLOCATION_LABEL = 'mapOfWorkLocation';
    private final String LIST_OF_VISIBLEFIELDS = 'listOfVisibleFields';
    private final String PROFILE_BILL_RATE_LABEL = 'profileBillRate';
    private final String CURRENCY_ISO_CODE_LABEL = 'currencyISOCode';
    private final String WORK_LOCATION_LABEL = 'workLocation';
    private final String EMPLOYEE_LABEL = 'objEmployeeMaster';
    private final String IS_UPDATABLE_LABEL = 'isUpdatable';
    private final String MAX_END_DATE_LABEL = 'maxEndDate';
    private final String LENDING_SDO_LABEL = 'lendingSDO';
    private final String BILL_RATE_LABEL = 'billRateType';
    private final String IS_PID_CREATED = 'isPIDCreated';
    private final String CSV_DATA_LABEL = 'CSVData';
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String WARNING_LABEL = 'WARNING';
    private final String PROJECT_LABEL = 'project';

    private final GPOptions nullOption = new GPOptions(null, '--Select--');

    private Map < String, GPProjectTemplateFieldWrapper > mapOfResourceAllocationFieldKeyToFieldTemplate;
    private Map < String, GP_Timesheet_Entry__c > mapOfMonthYearVsEmployeeTimeSheetRecords;
    private List < GP_Resource_Allocation__c > lstOfEmployeeResourceAllocationRecords;
    private Map < Id, GP_Profile_Bill_Rate__c > mapOfProfileBillRateRecords;
    private List < GP_Resource_Allocation__c > listOfResourceAllocation;
    private List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded;
    private Map < String, List < String >> mapOfEmployeeVsLendingSDO;
    private GP_Resource_Allocation__c resourceAllocation;
    private Map < String, Decimal > mapOfProfileBillRates;
    private GP_Employee_Master__c objEmployeeMaster;
    private List < GPOptions > relatedWorkLocations;
    private Map < Id, String > mapOfWorkLocation;
    private List < GPOptions > relatedSDOs;
    private GP_Project__c objProject;


    private GPProjectTemplateFieldWrapper resourceAllocationTemplate;
    private list < GPProjectTemplateFieldWrapper > lstFieldvisible;
    private Boolean isResourceAllocationDefinable = true;
    private static String resourceLendingSDOOracleCode;
    private static String resourceLendingSDOName;
    private Integer noOfRowsToBeDisplayed = 0;
    private static Id resourceLendingSDO;
    private String currencyISOCode;
    private String billRateType;
    private Id resourceAllocationId;
    private Boolean isPIDCreated;
    private Boolean isUpdatable;
    private JSONGenerator gen;
    private Date maxEndDate;
    private Id projectId;

    private Set < String > setOfBusinessGroupsToConsider = new Set < String > { 'CPG', 'Healthcare', 'High Tech', 'Insurance', 'Life Sciences', 'Retail' };

    /**
     * @description Parameterized constructor to accpet resource allocation Id and project Id.
     * @param resourceAllocationId Resource allocation Id.
     * @param projectId Project Id.
     * 
     * @example
     * new GPControllerProjectResourceAllocation(<SFDC 18/15 digit resourceallocation Id>, <SFDC 18/15 digit project Id>);
     */
    public GPControllerProjectResourceAllocation(Id resourceAllocationId, Id projectId) {
        this.resourceAllocationId = resourceAllocationId;
        this.projectId = projectId;
    }

    public GPControllerProjectResourceAllocation() {}

    /**
     * @description Parameterized constructor to accpet resource allocation.
     * @param resourceAllocation Resource allocation Record.
     * 
     * @example
     * new GPControllerProjectResourceAllocation(<Resource Allocation Record>);
     */
    public GPControllerProjectResourceAllocation(GP_Resource_Allocation__c resourceAllocation) {
        this.resourceAllocation = resourceAllocation;
    }

    /**
     * @description method to delete Project Resource Allocation Records.
     * 
     * @example
     * new GPControllerProjectResourceAllocation(<SFDC 18/15 digit resourceallocation Id>, <SFDC 18/15 digit project Id>).deleteResourceAllocationRecord(<SFDC 18/15 digit resource allocation Id>);
     */
    public GPAuraResponse deleteResourceAllocationRecord(Id resourceAllocationId) {
        try {
            GP_Resource_Allocation__c resourceAllocationToBeDeleted = [select id,GP_Project__c from GP_Resource_Allocation__c where Id = :resourceAllocationId]; //GPSelectorResourceAllocation().selectResourceAllocationRecord(resourceAllocationId);
			projectId = resourceAllocationToBeDeleted.GP_Project__c;
			delete resourceAllocationToBeDeleted;
            
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'RESOURCE ALLOCATION');
            update project; 
            
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
     * @description method to upsert Resource Allocation Records.
     * 
     * @example
     * new GPControllerProjectResourceAllocation(<Resource Allocation Record>).upsertResourceAllocationRecord();
     */
    public GPAuraResponse upsertResourceAllocationRecord() {
        try {
            return validateRecord();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
		GP_Resource_Allocation__c resourceRecord = new GPSelectorResourceAllocation().selectResourceAllocationRecord(resourceAllocation.Id);
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(resourceRecord));
    }

    /**
     * @description method to validate Resource Allocation Record and upsert the same.
     * 
     */
    private GPAuraResponse validateRecord() {
        GPResponseWrapper responseObject = new GPResponseWrapper();
        responseObject.code = 0;
        String validationMessage = GPServiceResourceAllocation.validateEachResourceAllocation(resourceAllocation, responseObject, resourceAllocation.GP_Project__c);
        system.debug('validationMessage'+validationMessage);
        if (responseObject.code == 0) {
            if (validationMessage.contains(RESOURCE_TOTAL_COST_EXCEPTION_LABEL))
                return new GPAuraResponse(false, GPCommon.parseErrorMessage(validationMessage), null);
            return new GPAuraResponse(false, validationMessage, null);
        } else if (responseObject.code == 1) {
            resourceAllocation = getFieldValuesForResource(resourceAllocation.Id);
            insertProjectWorkLocationIfRequired();
            projectId = resourceAllocation.GP_Project__c;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'RESOURCE ALLOCATION');
            update project;
            gen = JSON.createGenerator(true);
             gen.writeStartObject();

            if (resourceAllocation != null)
                gen.writeObjectField('resourceAllocation', resourceAllocation);
            if(responseObject.message != null)
                gen.writeObjectField('percentage', responseObject.message);

             gen.writeEndObject();
            return new GPAuraResponse(true, WARNING_LABEL, gen.getAsString());
        } else {

            resourceAllocation = getFieldValuesForResource(resourceAllocation.Id);
			projectId = resourceAllocation.GP_Project__c;

            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'RESOURCE ALLOCATION');
            update project;
            insertProjectWorkLocationIfRequired();
            return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(resourceAllocation, true));
        }
    }

    /**
     * @description method to create lending SDO record if doesn't exist in project SDO.
     *
     */
    private void insertProjectWorkLocationIfRequired() {
        Set < Id > setOfProjectLendingSDO = new Set < Id > ();
        objProject = getProjectRecord(resourceAllocation.GP_Project__c);
        setOfProjectLendingSDO.add(objProject.GP_Primary_SDO__c);
        for (GP_Project_Work_Location_SDO__c projectSDO: GPSelectorProjectWorkLocationSDO.selectProjectSDOforProjectIds(new Set < Id > { objProject.Id })) {
            setOfProjectLendingSDO.add(projectSDO.GP_Work_Location__c);
        }
        if (!setOfProjectLendingSDO.contains(resourceAllocation.GP_SDO__c)) {
            GP_Project_Work_Location_SDO__c projectLendingSDO = new GP_Project_Work_Location_SDO__c();
            projectLendingSDO.GP_Project__c = objProject.Id;
            projectLendingSDO.GP_Work_Location__c = resourceAllocation.GP_SDO__c;
            projectLendingSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
            insert projectLendingSDO;
        }
    }

    /**
     * @description method to fetch Resource Allocation Records.
     * 
     * @param projectId : project Id whose resource allocation records need to fetched.
     *
     * @example
     * new GPControllerProjectResourceAllocation(<Resource Allocation Record>).getResourceAllocationData(<SFDC 18/15 digit project Id>);
     */
    public GPAuraResponse getResourceAllocationData(Id projectId) {
        objProject = getProjectRecord(projectId);
        isResourceAllocationDefinable = objProject.RecordType.Name != 'BPM' ? true : false;
        if (!isResourceAllocationDefinable) {
            setJson();
        } else {
            try {
                listOfResourceAllocation = new GPSelectorResourceAllocation().selectResourceAllocationRecordsForAProject(projectId);
                setProjectRelatedContent();
                setFieldTemplate();
                setRelatedWorkLocationAndSDOs();
                setProfileBillRate();
                setJson();
            } catch (Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }
        }
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    /**
     * @description query project and set associated class members.
     * 
     */
    private void setProjectRelatedContent() {
        objProject = getProjectRecord(projectId);
        relatedSDOs = new List < GPOptions > { nullOption };

        if (objProject.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c != null &&
            objProject.GP_End_Date__c != null &&
            objProject.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c < objProject.GP_End_Date__c)
            maxEndDate = objProject.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c;
        else if (objProject.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c != null && objProject.GP_End_Date__c != null &&
            objProject.GP_Operating_Unit__r.GP_Last_Defined_Period_Date__c > objProject.GP_End_Date__c)
            maxEndDate = objProject.GP_End_Date__c;

        if (objProject.GP_Primary_SDO__c != null)
            relatedSDOs.add(new GPOptions(objProject.GP_Primary_SDO__c, objProject.GP_Primary_SDO__r.Name));

        isPIDCreated = objProject.GP_Oracle_PID__c != null ? true : false;
        currencyISOCode = objProject.CurrencyIsoCode;
        billRateType = objProject.GP_Bill_Rate_Type__c;

    }

    /**
     * @description Create worklocation picklist for UI.
     * 
     */
    private void setRelatedWorkLocationAndSDOs() {
        relatedWorkLocations = new List < GPOptions > { nullOption };
        mapOfWorkLocation = new map < Id, String > ();

        for (GP_Project_Work_Location_SDO__c objWorkLocationSDO: new GPSelectorProjectWorkLocationSDO().selectProjectWorkLocationAndSDORecords(projectId)) {
            if (objWorkLocationSDO.GP_Work_Location__r.RecordType.Name == 'Work Location') {
                relatedWorkLocations.add(new GPOptions(objWorkLocationSDO.GP_Work_Location__c, objWorkLocationSDO.GP_Work_Location__r.Name));
                mapOfWorkLocation.put(objWorkLocationSDO.GP_Work_Location__c, objWorkLocationSDO.GP_Work_Location__r.GP_Legal_Entity_Code__c);
            } else
                relatedSDOs.add(new GPOptions(objWorkLocationSDO.GP_Work_Location__c, objWorkLocationSDO.GP_Work_Location__r.Name));
        }
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (mapOfResourceAllocationFieldKeyToFieldTemplate != null)
            gen.writeObjectField(LIST_OF_RESOURCE_ALLOCATION_TEMPLATE_LABEL, mapOfResourceAllocationFieldKeyToFieldTemplate);
        if (isResourceAllocationDefinable != null)
            gen.writeObjectField(IS_RESOURCE_ALLOCATION_DEFINABLE_LABEL, isResourceAllocationDefinable);
        if (resourceLendingSDOOracleCode != null)
            gen.writeObjectField(RESOURCE_LENDING_SDO_CODE_LABEL, resourceLendingSDOOracleCode);
        if (mapOfEmployeeVsLendingSDO != null)
            gen.writeObjectField(MAP_OF_EMPLOYEE_LENDING_LABEL, mapOfEmployeeVsLendingSDO);
        if (listOfResourceAllocation != null)
            gen.writeObjectField(LIST_RESOURCE_ALLOCATION_LABEL, listOfResourceAllocation);
        if (resourceLendingSDOName != null)
            gen.writeObjectField(RESOURCE_LENDING_SDO_NAME_LABEL, resourceLendingSDOName);
        if (resourceAllocationTemplate != null)
            gen.writeObjectField(RESOURCE_TEMPLATE_LABEL, resourceAllocationTemplate);
        if (noOfRowsToBeDisplayed != null)
            gen.writeObjectField(NO_OF_ROWS_TO_BE_DISPLAYED, noOfRowsToBeDisplayed);
        if (mapOfProfileBillRates != null)
            gen.writeObjectField(PROFILE_BILL_RATE_LABEL, mapOfProfileBillRates);
        if (resourceLendingSDO != null)
            gen.writeObjectField(RESOURCE_LENDING_SDO_LABEL, resourceLendingSDO);
        if (resourceLendingSDO != null)
            gen.writeObjectField(RESOURCE_LENDING_SDO_LABEL, resourceLendingSDO);
        if (resourceAllocation != null)
            gen.writeObjectField(RESOURCE_ALLOCATION_LABEL, resourceAllocation);
        if (mapOfWorkLocation != null)
            gen.writeObjectField(MAP_OF_WORKLOCATION_LABEL, mapOfWorkLocation);
        if (relatedWorkLocations != null)
            gen.writeObjectField(WORK_LOCATION_LABEL, relatedWorkLocations);
        if (listOfCSVRecordsUploaded != null)
            gen.writeObjectField(CSV_DATA_LABEL, listOfCSVRecordsUploaded);
        if (currencyISOCode != null)
            gen.writeObjectField(CURRENCY_ISO_CODE_LABEL, currencyISOCode);
        if (lstFieldvisible != null && lstFieldvisible.size() > 0)
            gen.writeObjectField(LIST_OF_VISIBLEFIELDS, lstFieldvisible);
        if (objEmployeeMaster != null)
            gen.writeObjectField(EMPLOYEE_LABEL, objEmployeeMaster);
        if (isUpdatable != null)
            gen.writeObjectField(IS_UPDATABLE_LABEL, isUpdatable);
        if (relatedSDOs != null)
            gen.writeObjectField(LENDING_SDO_LABEL, relatedSDOs);
        if (maxEndDate != null)
            gen.writeObjectField(MAX_END_DATE_LABEL, maxEndDate);
        if (isPIDCreated != null)
            gen.writeObjectField(IS_PID_CREATED, isPIDCreated);
        if (objProject != null)
            gen.writeObjectField(PROJECT_LABEL, objProject);
        if (billRateType != null)
            gen.writeObjectField(BILL_RATE_LABEL, billRateType);

        gen.writeEndObject();
    }

    /**
     * @description Set Profile Bill Rate picklist.
     * 
     */
    private void setProfileBillRate() {
        mapOfProfileBillRates = new map < String, Decimal > ();

        for (GP_Profile_Bill_Rate__c objProfileBillRate: new GPSelectorProfileBillRate().getMapOfProjectRelatedProfileBillRates(projectId)) {
            mapOfProfileBillRates.put(objProfileBillRate.GP_Profile__c, objProfileBillRate.GP_Bill_Rate__c);
        }
    }

    /**
     * @description Set Field map to show on UI depending upon project template.
     * 
     */
    private void setFieldTemplate() {
        mapOfResourceAllocationFieldKeyToFieldTemplate = new Map < String, GPProjectTemplateFieldWrapper > ();
        lstFieldvisible = new list < GPProjectTemplateFieldWrapper > ();
        String serializedFinalTemplate = getConcatenatedFinalJSON(objProject);
        String targetKey = 'ProjectResourceAllocation___Default';

        Map < String, GPProjectTemplateFieldWrapper > mapOfFieldKeyToFieldTemplate = getDeserializedMapOfProjectTemplate(serializedFinalTemplate);

        for (String fieldKey: mapOfFieldKeyToFieldTemplate.keySet()) {

            if (fieldKey.contains(targetKey)) {
                resourceAllocationTemplate = mapOfFieldKeyToFieldTemplate.get(fieldKey);
                mapOfResourceAllocationFieldKeyToFieldTemplate.put(fieldKey, resourceAllocationTemplate);
                noOfRowsToBeDisplayed = resourceAllocationTemplate.numberOfDefaultRowToDisplay;
                if (resourceAllocationTemplate.isVisible)
                    lstFieldvisible.add(resourceAllocationTemplate);
            }
        }
    }

    /**
     * @description get concatenated serialosed JSON of project template.
     * 
     */
    private String getConcatenatedFinalJSON(GP_Project__c projectObj) {
        String serializedFinalTemplate = projectObj.GP_Project_Template__r.GP_Final_JSON_1__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_2__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_3__c;

        return serializedFinalTemplate;
    }

    /**
     * @description get deserialised project template.
     * 
     */
    private Map < String, GPProjectTemplateFieldWrapper > getDeserializedMapOfProjectTemplate(String serializedFinalTemplate) {
        return (Map < String, GPProjectTemplateFieldWrapper > ) JSON.deserialize(serializedFinalTemplate, Map < String, GPProjectTemplateFieldWrapper > .class);
    }

    /**
     * @description get resource lending SDO.
     * 
     */
    public void setResourceLendingSDO(GP_Employee_Master__c objEmployeeMaster) {
        String lendingSDOCode;
        if (objEmployeeMaster != null) {
            // Product Hierarchy Changes.
            if (objProject.GP_Nature_of_Work__c != null && objProject.GP_Nature_of_Work__c.containsIgnoreCase('Consulting')) {
                //mapping
                lendingSDOCode = objEmployeeMaster.GP_SDO__c != null ? objEmployeeMaster.GP_SDO__r.GP_Lending_SDO_Code__c : null;
                setLendingSDOUsingWorkLocationMaster(lendingSDOCode);
            } else if (objEmployeeMaster.GP_Employee_Category__c == 'CMITS') {
                system.debug('Check the value from lending Sdo'+objProject.GP_Primary_SDO__r.GP_Business_Name__c);
                if (objProject.RecordType.Name == 'CMITS' || objProject.GP_Primary_SDO__r.GP_Business_Name__c=='CMITS') {
                    
                    resourceLendingSDO = objProject.GP_Primary_SDO__c;
                    resourceLendingSDOName = objProject.GP_Primary_SDO__r.Name;
                    resourceLendingSDOOracleCode = objProject.GP_Primary_SDO__r.GP_Oracle_Id__c;
                } else {
                    //mapping...
                    if (objProject.GP_Vertical__c != 'Capital Markets') {
                        String query = 'select GP_Business_Group__c,';
                        query += 'GP_Industry_Vertical__c, GP_SDO_Oracle_Id__c from GP_Employee_Lending_SDO__mdt';
                        query += ' where GP_Industry_Vertical__c =\'' + objProject.GP_Vertical__c + '\'';
                        if (!setOfBusinessGroupsToConsider.contains(objProject.GP_Vertical__c))
                            query += 'and GP_Business_Group__c =\'' + objProject.GP_Business_Group_L1__c + '\'';

                        List < GP_Employee_Lending_SDO__mdt > lendingSDOMetadata = Database.Query(query);

                        lendingSDOCode = lendingSDOMetadata.size() > 0 ? lendingSDOMetadata[0].GP_SDO_Oracle_Id__c : null;
                        setLendingSDOUsingWorkLocationMaster(lendingSDOCode);
                    } else {
                        lendingSDOCode = objProject.GP_Customer_Hierarchy_L4__c != null && objProject.GP_Customer_Hierarchy_L4__r.GP_Mapped_SDO_for_Pinnacle__c != '' && objProject.GP_Customer_Hierarchy_L4__r.GP_Mapped_SDO_for_Pinnacle__c != null ?
                            objProject.GP_Customer_Hierarchy_L4__r.GP_Mapped_SDO_for_Pinnacle__c : System.Label.GP_Global_Lending_SDO_Oracle_ID;
                        setLendingSDOUsingWorkLocationMaster(lendingSDOCode);
                    }
                }
            } else {
                resourceLendingSDO = objEmployeeMaster.GP_SDO__c != null ? objEmployeeMaster.GP_SDO__c : null;
                resourceLendingSDOName = objEmployeeMaster.GP_SDO__c != null ? objEmployeeMaster.GP_SDO__r.Name : null;
                resourceLendingSDOOracleCode = objEmployeeMaster.GP_SDO__r.GP_Oracle_Id__c;
            }

        }
    }
    private void setLendingSDOUsingWorkLocationMaster(String lendingSDOCode) {
        if (lendingSDOCode != null) {
            List < GP_Work_Location__c > workLocationMaster = new GPSelectorWorkLocationSDOMaster().selectByOracleIdofWOrkLocationAndSDO(new Set < String > {
                lendingSDOCode
            });
            if (workLocationMaster.size() > 0) {
                resourceLendingSDO = workLocationMaster[0].Id;
                resourceLendingSDOName = workLocationMaster[0].Name;
                resourceLendingSDOOracleCode = workLocationMaster[0].GP_Oracle_Id__c;
            }
        }
    }
    /**
     * @description Set Lending SDO picklist.
     * 
     */
    public GPAuraResponse getLendingSDOToSelect(String employeeId, String projectId) {
        objProject = getProjectRecord(projectId);
        objEmployeeMaster = new GPSelectorEmployeeMaster().selectEmployeeMasterRecord(employeeId);
        setResourceLendingSDO(objEmployeeMaster);
        setJson();
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    /**
     * @description query project record.
     * 
     */
    private static GP_Project__c getProjectRecord(Id projectId) {
        return new GPSelectorProject().selectProjectRecord(projectId);
    }

    /**
     * @description query resource allocation record.
     * 
     */
    private GP_Resource_Allocation__c getFieldValuesForResource(Id resourceAllocationId) {
        return new GPSelectorResourceAllocation().selectResourceAllocationRecord(resourceAllocationId);
    }

    /**
     * @description get processed mass upload resource allocation list.
     * 
     */
    public GPAuraResponse getProcessedCSVRecords(List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded, String projectId) {
        this.listOfCSVRecordsUploaded = listOfCSVRecordsUploaded;
        this.projectId = projectId;
        objProject = getProjectRecord(projectId);

        Map < String, GP_Employee_Master__c > mapOfPersonIDVsEmployeeRecord = new Map < String, GP_Employee_Master__c > ();
        Map < String, String > mapOfOracleIdsWorkLocationIds = new Map < String, String > ();
        Map < String, String > mapOfOracleIdsSDOIds = new Map < String, String > ();
        Set < String > setOfWorkLocationOracleIds = new Set < String > ();
        //Set < String > setOfEmployeePersonIds = new Set < String > ();
        Set < String > setOfEmployeeOHRIds = new Set < String > ();
        Set < String > setOfSDOOracleIds = new Set < String > ();

        mapOfEmployeeVsLendingSDO = new Map < String, List < String >> ();

        getAllExternalIdsSet(listOfCSVRecordsUploaded, setOfWorkLocationOracleIds, /*setOfEmployeePersonIds*/ setOfEmployeeOHRIds, setOfSDOOracleIds);
        setMapOfWorkLocationAndSDO(listOfCSVRecordsUploaded, mapOfOracleIdsWorkLocationIds, mapOfOracleIdsSDOIds, /*setOfEmployeePersonIds*/ setOfEmployeeOHRIds);
        setMapOfEmployee(listOfCSVRecordsUploaded, /*setOfEmployeePersonIds*/ setOfEmployeeOHRIds, mapOfPersonIDVsEmployeeRecord);
        setCSVRecordsWithLookups(listOfCSVRecordsUploaded, mapOfOracleIdsWorkLocationIds, mapOfOracleIdsSDOIds, mapOfPersonIDVsEmployeeRecord);

        setJson();
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description query masters for mass upload.
     * 
     */
    private void getAllExternalIdsSet(List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded, Set < String > setOfWorkLocationOracleIds,
        Set < String > setOfEmployeeOHRIds, Set < String > setOfSDOOracleIds) {

        for (GP_Resource_Allocation__c objResourceAllocation: listOfCSVRecordsUploaded) {
            if (objResourceAllocation.GP_Work_Location__c != null) {
                setOfWorkLocationOracleIds.add(objResourceAllocation.GP_Work_Location__c);
            }
            if (objResourceAllocation.GP_SDO__c != null) {
                setOfSDOOracleIds.add(objResourceAllocation.GP_SDO__c);
            }
            if (objResourceAllocation.GP_Employee__c != null) {
                //setOfEmployeePersonIds.add(objResourceAllocation.GP_Employee__c);
                setOfEmployeeOHRIds.add(objResourceAllocation.GP_Employee__c);
            }
        }
    }

    /**
     * @description set map of worklocation master.
     * 
     */
    private void setMapOfWorkLocationAndSDO(List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded, Map < String, String > mapOfOracleIdsWorkLocationIds,
        Map < String, String > mapOfOracleIdsSDOIds, Set < String > setOfEmployeePersonIds) {

        for (GP_Project_Work_Location_SDO__c objWorkLocationSDO: new GPSelectorProjectWorkLocationSDO().selectProjectWorkLocationAndSDORecords(projectId)) {
            if (objWorkLocationSDO.GP_Work_Location__r.RecordType.Name == 'Work Location') {
                mapOfOracleIdsWorkLocationIds.put(objWorkLocationSDO.GP_Work_Location__r.GP_Oracle_Id__c, objWorkLocationSDO.GP_Work_Location__c);
            } else {
                mapOfOracleIdsSDOIds.put(objWorkLocationSDO.GP_Work_Location__r.GP_Oracle_Id__c, objWorkLocationSDO.GP_Work_Location__c);

            }
        }
        objProject = getProjectRecord(projectId);
        if (objProject.GP_Primary_SDO__c != null && objProject.GP_Primary_SDO__r.GP_Oracle_Id__c != null)
            mapOfOracleIdsSDOIds.put(objProject.GP_Primary_SDO__r.GP_Oracle_Id__c, objProject.GP_Primary_SDO__c);
    }

    /**
     * @description set map of employee master.
     * 
     */
    private void setMapOfEmployee(List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded, Set < String > setOfEmployeeOHRIds,
        Map < String, GP_Employee_Master__c > mapOfPersonIDVsEmployeeRecord) {

        for (GP_Employee_Master__c objEmployee: new GPSelectorEmployeeMaster().getEmployeeListForOHRId(setOfEmployeeOHRIds)) {
            //mapOfPersonIDVsEmployeeRecord.put(objEmployee.GP_Person_ID__c, objEmployee);
            mapOfPersonIDVsEmployeeRecord.put(objEmployee.GP_Final_OHR__c, objEmployee);

            setResourceLendingSDO(objEmployee);
            mapOfEmployeeVsLendingSDO.put(objEmployee.Id, new List < String > { resourceLendingSDO, resourceLendingSDOName, resourceLendingSDOOracleCode });
        }
    }

    /**
     * @description change Oracle Id to lookup Id for mass update.
     * 
     */
    private void setCSVRecordsWithLookups(List < GP_Resource_Allocation__c > listOfCSVRecordsUploaded, Map < String, String > mapOfOracleIdsWorkLocationIds,
        Map < String, String > mapOfOracleIdsSDOIds, Map < String, GP_Employee_Master__c > mapOfPersonIDVsEmployeeRecord) {

        for (GP_Resource_Allocation__c objResourceAllocation: listOfCSVRecordsUploaded) {
            if (mapOfOracleIdsWorkLocationIds.containsKey(objResourceAllocation.GP_Work_Location__c))
                objResourceAllocation.GP_Work_Location__c = mapOfOracleIdsWorkLocationIds.get(objResourceAllocation.GP_Work_Location__c);
            else
                objResourceAllocation.GP_Work_Location__c = null;
            if (mapOfOracleIdsSDOIds.containsKey(objResourceAllocation.GP_SDO__c))
                objResourceAllocation.GP_SDO__c = mapOfOracleIdsSDOIds.get(objResourceAllocation.GP_SDO__c);
            else
                objResourceAllocation.GP_SDO__c = null;
            if (mapOfPersonIDVsEmployeeRecord.containsKey(objResourceAllocation.GP_Employee__c)) {
                objResourceAllocation.GP_Employee__r = mapOfPersonIDVsEmployeeRecord.get(objResourceAllocation.GP_Employee__c);
                objResourceAllocation.GP_Employee__c = mapOfPersonIDVsEmployeeRecord.get(objResourceAllocation.GP_Employee__c).Id;
            } else {
                objResourceAllocation.GP_Employee__r = null;
                objResourceAllocation.GP_Employee__c = null;
            }
        }
    }
}