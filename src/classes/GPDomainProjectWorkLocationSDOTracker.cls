@isTest
public class GPDomainProjectWorkLocationSDOTracker {
    public static boolean isRun = true;
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj;
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO;
    public static String jsonresp;
    
    public static Void buildDependencyData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_work_location_sdo__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.GP_Legal_Entity_Code__c = '12345';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Work_Location__c objSdo2 = GPCommonTracker.getWorkLocation();
        objSdo2.GP_Status__c = 'Active and Visible';
        objSdo2.GP_Legal_Entity_Code__c = '12346';
        objSdo2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo2;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        //prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('BPM').getRecordTypeId();
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Primary_SDO__c = objSdo.id;
        prjObj.GP_Is_Closed__c = true;
        insert prjObj;
        
        objSdo.GP_Status__c = 'Inactive';
        objSdo.GP_Start_Date_Active__c = Date.newInstance(2017, 5, 20);
        objSdo.GP_End_Date_Active__c = Date.newInstance(2017, 6, 20);
        update objSdo;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-employee';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = false;
        update empObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        //objProjectWorkLocationSDO1.recordtypeId = gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        insert objProjectWorkLocationSDO1;
        
        objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.recordtypeId = gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location');
        objProjectWorkLocationSDO.GP_Parent_Project_Work_Location_SDO__c = objProjectWorkLocationSDO1.id;
        insert objProjectWorkLocationSDO;
        objProjectWorkLocationSDO.GP_Work_Location__c = objSdo2.id;
        objProjectWorkLocationSDO.GP_is_Active__c = true;
        Update objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
    }
  
     @isTest
    public static void testGPDomainProjectWorkLocationSDO() {
        buildDependencyData();
        delete objProjectWorkLocationSDO;
    }
}