@istest
public class Test_updateOPP {

    
    static testMethod void method1()
    {
          Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
            insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
      
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,Number_of_Contract__c=0);
        insert opp;
        
        opportunity opp1=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,Number_of_Contract__c=0);
        insert opp1;
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
         insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        
        
        Contract oContract2 =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp1.id,CustomerSignedid=oContact.id,Status='Draft');
        insert oContract2;
        
        Contract oContract3 =new Contract(ID=oContract2.id,Accountid=accountobject.id,Opportunity_tagged__c=opp1.id,CustomerSignedid=oContact.id,Status='In Approval Process');
        update oContract3;
        
        
        system.assertEquals( opp1.Number_of_Contract__c,0);
        
        test.stopTest();
       
       
        
    }
    
    static testMethod void method2()
    {
          Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
            insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
      
       
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
        insert opp;
        
      
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
         insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        
        Contract oContract =new Contract(Accountid=accountobject.id,Opportunity_tagged__c=opp.id,CustomerSignedid=oContact.id,Status='Draft');
        insert oContract;
        
        Contract c=[select Status,id from contract where id =:oContract.id ];
        c.Status='Activated';
        update c;
        
        
        
        //system.assertEquals( opp.Number_of_Contract__c,1);
        
        
        test.stopTest();
       
       
        
    }

}