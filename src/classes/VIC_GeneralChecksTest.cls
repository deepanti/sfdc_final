@isTest
public class VIC_GeneralChecksTest 
{
  static testmethod void ValidateVIC_GeneralChecks()
  {
      VIC_GeneralChecks obj = new VIC_GeneralChecks();
      VIC_GeneralChecks.CheckNull(1);
      VIC_GeneralChecks.CheckNull(1.0);
      VIC_GeneralChecks.CheckNull('1.0');
      VIC_GeneralChecks.IsNotNull(1.0);
      VIC_GeneralChecks.IsNotNull(system.now());
      VIC_GeneralChecks.IsNotNull(system.today());
      VIC_GeneralChecks.IsNotNull('test');
      VIC_GeneralChecks.IsNotNull(1);
      contact objc= new contact();
      list<contact> lstcontact = new list<contact>();
      lstcontact.add(objc);
      VIC_GeneralChecks.IsNotNull(objc);
      VIC_GeneralChecks.IsNotNull(lstcontact);
      VIC_GeneralChecks.IsNotNullGreaterThenZero(1.0);
      VIC_GeneralChecks.IsNotNullGreaterThenZero(1);
      VIC_GeneralChecks.IsNotNullAndBlank('1');
      VIC_GeneralChecks.calcValueAsPercentage(10.0,10.0);
      VIC_GeneralChecks.GetPercentageOfResult(10.0,10.0);
      VIC_GeneralChecks.isDecimal('1');
      System.AssertEquals(200,200);

      
  }
}