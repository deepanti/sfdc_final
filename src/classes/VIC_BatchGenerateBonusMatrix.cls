/**
    @Author:        Dheeraj Chawla
    @Date:          31/07/2018
    @description:   To call the batch class for user according to VIC role 
                    and insert the pdf under the Target object.
**/
public class VIC_BatchGenerateBonusMatrix implements Database.Batchable<SObject> {
    public String year;
    public List<String> listOfUserId;
    public Date startDate;
    public Date endDate;
    String query;
    
    public VIC_BatchGenerateBonusMatrix(List<Id> listofUserIdForSelectedVICRole, Date startDate, Date endDate) {
        this.listOfUserId = listofUserIdForSelectedVICRole;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    public Database.QueryLocator start(Database.BatchableContext start) {
        System.debug('Start Date-> '+startDate);
        System.debug('listOfUserId-> '+listOfUserId);
        return Database.getQueryLocator([SELECT Id, User__c,Generate_Bonus_Letter__c
                                            FROM Target__c
                                            WHERE Start_date__c=:startDate
                                            AND Is_Active__c = TRUE
                                            AND User__c IN :listOfUserId]);
    }
    
    public void execute(Database.BatchableContext context, List<Target__c> scopes) {
        System.debug('scopes-> ' + scopes);
         Blob data;
        //BATCH SIZE MUST BE 1        
        for(Target__c objTarget: scopes) {
            PageReference pr = new PageReference('/apex/VIC_GenerateBonusMatrix');
            pr.getParameters().put('startdate', String.valueOf(startDate));
            pr.getParameters().put('endDate', String.valueOf(endDate));
            pr.getParameters().put('userId', objTarget.User__c);
           if(!test.isrunningtest()){
              data = pr.getContent();
             }
             else{
               data=blob.valueof('data to be inserted.');
             }
             
            
            
            Attachment objAttachment = new Attachment();
            objAttachment.Name = 'Bonus_Report' + System.now().format('MM-dd-yyyy-HHmmss') + '.pdf';
            objAttachment.Body = data;
            objAttachment.ParentId  = objTarget.Id;
            objAttachment.IsPrivate = FALSE;
            INSERT objAttachment;
            
            objTarget.Generate_Bonus_Letter__c = true;
            UPDATE objTarget;
            break;
        }
    }
        
    public void finish(Database.BatchableContext context) {
        
    }
    
}