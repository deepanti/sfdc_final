public class GPSelectorProject extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project__c.Name,
            GP_Project__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project__c.sObjectType;
    }

    public List < GP_Project__c > selectById(Set < ID > idSet) {
        return (List < GP_Project__c > ) selectSObjectsById(idSet);
    }

    public List < GP_Project__share > selectProjectShareById(Set < ID > setOfProjectId) {
        return (List < GP_Project__share > )[SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId FROM GP_Project__share where ParentId in: setOfProjectId];
    }

    public List < GP_Project__share > selectProjectShareById(Set < ID > setOfProjectId, Set < ID > setUserID) {
        return (List < GP_Project__share > )[SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId FROM GP_Project__share where ParentId in: setOfProjectId AND UserOrGroupId in: setUserID];
    }

    public List < GP_Project__share > selectManualProjectShareById(Set < ID > setOfProjectId) {
        return (List < GP_Project__share > )[SELECT Id, UserOrGroupId, RowCause, AccessLevel, ParentId FROM GP_Project__share where ParentId in: setOfProjectId and RowCause = 'Manual'];
    }
    
    // T7 Change
    public GP_Project__c selectProjectRecord(Id projectId) {
        return [SELECT id,
            recordtype.name,
            GP_CRN_Number__r.GP_Type__c,
            GP_CRN_Number__c,
            name,GP_Is_PO_Required__c,
            gp_oms_deal_id__c,
            gp_deal__r.gp_no_pricing_in_oms__c,
            gp_project_template__r.gp_final_json_1__c,
            gp_bill_rate_type__c,
            gp_project_template__r.gp_final_json_2__c,
            gp_operating_unit__c,
            gp_operating_unit__r.name,
            gp_operating_unit__r.GP_LE_Code__c,//Avalara check for LE Code
            gp_project_template__r.gp_final_json_3__c,
            gp_temp_approval_status__c,
            gp_primary_sdo__c,
            gp_service_line__c,
            ownerid,
            gp_parent_customer_sub_division__c,
            gp_nature_of_work__c,
            gp_project_type__c,
            gp_approval_status__c,
            gp_vertical__c,
            gp_business_group_l1__c,
            gp_customer_hierarchy_l4__c,
            gp_start_date__c,
            gp_customer_hierarchy_l4__r.gp_mapped_sdo_for_pinnacle__c,
            gp_hsl__c,
            gp_hsl__r.parent_hsl__c,
            gp_project_status__c,
            gp_operating_unit__r.gp_last_defined_period_date__c,
            gp_band_3_approver_status__c,
            gp_tcv__c,
            gp_end_date__c,
            gp_business_segment_l2__c,
            gp_primary_sdo__r.gp_ischina__c,
            gp_additional_approval_required__c,
            gp_sub_delivery_org__c,
            gp_delivery_org__c,
            gp_primary_sdo__r.name,GP_Primary_SDO__r.GP_Business_Name__c,
            currencyisocode,
            gp_oracle_pid__c,
            gp_primary_sdo__r.gp_oracle_id__c,
            gp_customer_master__r.name,
            gp_next_expense_counter__c,
            GP_Last_Modified_Source__c,
            GP_Approver_Ids__c
            //GP_is_Billing_Stage_Passed__c, // T7 Change
            //gp_project_template__r.GP_Custom_Template_Identifier__c, // T7 Change
            //GP_Approval_Error__c, // T7 Change
            //GP_Delivery_Start_Date__c // T7 Change
            FROM gp_project__c
            WHERE id =: projectId
        ];
    }
    
    // Payment Term changes
    public GP_Project__c selectProjectRecordForDocuments(Id projectId) {
        return [SELECT id,
            recordtype.name,
            GP_Last_Modified_Source__c,
            GP_Project_Status__c,
            GP_Is_Customer_PO_Available__c,
            GP_Payment_Terms__c
            FROM gp_project__c
            WHERE id =: projectId
        ];
    }

    public GP_Project__c getProjectWithRelatedData(Id projectId) {
        return [SELECT Id, GP_Approval_Status__c, GP_Oracle_PID__c,
            RecordType.Name, GP_Project_type__c, GP_Approval_Error__c,
            GP_CRN_Number__r.GP_Type__c,GP_Current_Working_User__c,
            GP_CRN_Number__c,
            //(SELECT Id FROM GP_Project_Work_Location_SDOs__r),
            (Select Id from Project_Leaderships__r),
            (Select Id from GP_Project_Expenses__r)
            //(Select Id from GP_Project_Budgets__r),
            //(Select Id from GP_Project_Documents__r)
            //(Select Id from GP_Billing_Milestones__r),
            //(Select Id from GP_Resource_Allocations__r)
            FROM GP_Project__c
            Where Id =: projectId
        ];
    }

    public List < GP_Project__c > getProjectDeatil(set < Id > setofPrjID) {
        return [Select ID, GP_CRN_Status__c, GP_Is_Closed__c,
            GP_CRN_Number__r.GP_Type__c,
            GP_CRN_Number__c, GP_Contract_End_Date__c,
            GP_Contract_Start_Date__c,
            GP_Employee_SFDC_ID__c, GP_PID_Approver_User__c
            from GP_Project__c
            where ID in: setofPrjID
        ];
    }
    public List < GP_Project__c > getProjectAndProjectTasks(set < String > setOfProjectOraclePId) {
        return [Select ID, GP_CRN_Status__c, GP_Is_Closed__c,
            GP_CRN_Number__c, GP_Contract_End_Date__c,
            GP_Contract_Start_Date__c,GP_Oracle_PID__c,
            GP_Employee_SFDC_ID__c, GP_PID_Approver_User__c,
            GP_CRN_Number__r.GP_Type__c
            /*(select id, GP_Active__c, GP_Start_Date__c, GP_End_Date__c from Project_Tasks__r where GP_Active__c = true
                AND (NOT GP_Task_Name__c  LIKE '% EXP %')
                AND (NOT GP_Task_Name__c  LIKE '% Concur %')
                AND (NOT GP_Task_Name__c  LIKE '% Salary %')
                AND (NOT GP_Task_Name__c  LIKE '% CTC %')
                AND (NOT GP_Task_Name__c  LIKE '% Purchase %')
            )*/
        from GP_Project__c
            where GP_Oracle_PID__c in: setOfProjectOraclePId
                and GP_Oracle_Status__c = 'S' and
                GP_Approval_Status__c = 'Approved'
        ];
    }

    public GP_Project__c getProjectFieldDetail(Id ProjID) {
        return [Select ID, 
                       Name, 
                       RecordType.Name,
                       GP_End_Date__c,
                       GP_Start_Date__c,
                       // Mobile App : EP Project Number
                       GP_EP_Project_Number__c,
                       GP_Oracle_PID__c, 
                       GP_Project_type__c, 
                       GP_Oracle_Status__c,
                       GP_Approval_Error__c,
                       GP_Bill_Rate_Type__c, 
                       GP_Approval_Status__c, 
                       GP_Primary_SDO__r.Name, 
                       GP_GPM_Employee__r.Name, 
                       GP_Customer_Master__r.Name,
                       GP_CRN_Number__r.GP_Type__c,
                       GP_CRN_Number__c,
                       GP_Resource_Allocation_Failure_Count__c,
                       GP_Current_Working_User__r.Name,
                       toLabel(GP_Project_Status__c),
                       GP_Blank_Fields_for_Approval__c, 
                       GP_Primary_SDO__r.GP_Oracle_Id__c
                from GP_Project__c
                where ID =: ProjID
        ];
    }

    public List < GP_Project__c > getApprovedProjectApprovedFieldDetail(set < Id > setofPrjID) {
        return [Select Id, GP_Approval_Status__c, GP_Parent_Project__c,
            GP_CRN_Number__r.GP_Type__c,
            GP_CRN_Number__c
            from GP_Project__c
            where GP_CRN_Number__c IN: setofPrjID
            and GP_Is_Closed__c = false
        ];
    }

    public List < GP_Deal__c > getDeal(set < String > setOfDealId) {
        list < GP_Deal__c > lstOfDeal = [Select ID, GP_Expense_Form_OMS__c, GP_Budget_From_OMS__c
            from GP_Deal__c
            where ID =: setOfDealId
        ];
        return lstOfDeal;
    }

    public list < GP_Project__c > getProjectID(set < Id > setOfProjectId) {

        list < GP_Project__c > lstOfProject = [Select ID, GP_CRN_Status__c,
            GP_Contract_Start_Date__c,
            GP_Contract_End_Date__c,
            GP_CRN_Number__r.GP_Type__c,
            GP_CRN_Number__c
            from GP_Project__c
            where ID IN: setofProjectID
        ];
        return lstOfProject;
    }

    public list < GP_Project__c > getProjectsWithPendingApproval(set < Id > setOfProjectId) {
        Id MandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        list < GP_Project__c > lstOfProject = [Select ID, GP_CRN_Status__c, GP_Next_CRN_Stage_Date__c,
            GP_CRN_Number__r.GP_Type__c,
            GP_CRN_Number__c,
            (select id, GP_Employee_ID__r.GP_SFDC_User__c,
                GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c from Project_Leaderships__r where RecordTypeID =: MandatoryKeyMembersRecordTypeId and(GP_Type_of_Leadership__c =: 'Billing Approver'
                    or GP_Type_of_Leadership__c =: 'Billing Spoc'
                    or GP_Type_of_Leadership__c =: 'FP&A'))
            from GP_Project__c
            where ID IN: setOfProjectId
        ];

        return lstOfProject;
    }
    public list < GP_Project__c > getProjectsForClosureNotification(list < Id > lstprojectID) {
        Id MandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        list < GP_Project__c > lstOfProject = [Select id, name, GP_Oracle_PID__c, Ownerid, GP_PID_Approver_User__c,
            GP_GPM_Employee__c, GP_GPM_Employee__r.GP_SFDC_User__c,
            GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
            (Select id, GP_Employee_ID__r.GP_SFDC_User__c,
                GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c From Project_Leaderships__r Where(RecordTypeID =: MandatoryKeyMembersRecordTypeId and(GP_Type_of_Leadership__c =: 'FP&A'
                    or GP_Type_of_Leadership__c =: 'PID Approver Finance')))
            From GP_Project__c Where ID IN: lstprojectID
        ];

        return lstOfProject;
    }



    public static List < GP_Project__c > getApprovedProjectUnderDeal(Id dealId) {
        return [Select Id, Name, GP_EP_Project_Number__c,GP_Oracle_PID__c
            FROM GP_Project__c
            WHERE GP_Deal__c =: dealId
            AND GP_Approval_Status__c = 'Approved' 
            AND GP_Oracle_Status__c = 'S'
        ];
    }

    public GP_Project__c getProjectPrimarySDO(Id strProjectID) {
        return [Select Id, GP_Primary_SDO__c,
            ownerId, GP_Current_Working_User__c
            from GP_Project__c
            where ID =: strProjectID
            and GP_Primary_SDO__c != null
        ];
    }

    public GP_Project__c getProject(Id strProjectID) {
        return [Select Id, GP_Current_Working_User__c, RecordTypeId, RecordType.Name,GP_Parent_Project__c,
            GP_Deal__c, GP_Deal__r.GP_Business_Group_L1__c, GP_Oracle_PID__c,GP_Oracle_Status__c,
            OwnerID, GP_Approval_Status__c
            from GP_Project__c
            where ID =: strProjectID
        ];
    }
    public list < GP_Project__c > getProject(set < ID > strProjectID) {
        return [Select Id, GP_Current_Working_User__c, RecordType.Name,
            OwnerID, GP_Approval_Status__c, GP_Oracle_PID__c
            from GP_Project__c
            where ID =: strProjectID
        ];
    }
    public list < GP_Project__c > getProjectsWithBillingEntityDetails(set < String > setOfProjectOraclePIds) {
        list < GP_Project__c > lstOfProject = [Select ID, GP_Operating_Unit__c,GP_Oracle_PID__c, GP_Operating_Unit__r.GP_Latest_Open_PA_Period__c, GP_PID_Approver_User__c
            from GP_Project__c
            where GP_Oracle_PID__c IN: setOfProjectOraclePIds
            and GP_Operating_Unit__c != null
        ];

        return lstOfProject;
    }
    public list < GP_Project__c > getProjectsWithBillingEntityDetails(set < Id > setOfProjectIds) {
        list < GP_Project__c > lstOfProject = [Select ID, GP_Operating_Unit__c, GP_Operating_Unit__r.GP_Latest_Open_PA_Period__c, GP_PID_Approver_User__c
            from GP_Project__c
            where Id IN: setOfProjectIds
            and GP_Operating_Unit__c != null
        ];

        return lstOfProject;
    }
    public map < Id, GP_Project__c > getProjectMap(set < Id > setOfProjectId) {
        return new Map < Id, GP_Project__c > ([Select ID, GP_Operating_Unit__c, GP_Total_TCV__c,
            GP_Operating_Unit__r.GP_Latest_Open_PA_Period__c, GP_Total_Effort_Hours__c,GP_Oracle_PID__c,
            RecordType.Name, GP_Project_type__c
            from GP_Project__c
            where ID IN: setOfProjectId
        ]);

    }
    public List < GP_Project__c > getProjectListForOracleId(Set < String > setOfProjectOracleIds) {
        return [Select ID, RecordTypeId,GP_Oracle_PID__c, GP_Oracle_Status__c, GP_is_Closed__c, GP_Current_Working_User__c, Name, GP_Approval_Status__c, GP_OMS_Status__c, RecordType.Name from GP_Project__c
            where GP_Oracle_PID__c IN: setOfProjectOracleIds  ORDER BY LastModifiedDate Desc
        ];

    }
    public List < GP_project__c > getBillingCurrencyFromProject(Id projectId) {
        return [SELECT GP_Billing_Currency__c From GP_project__c where Id =: projectId];

    }
    public List < GP_project__c > getProjectRecordsWithWorkLocation(Set < Id > setOfProjectId) {
        return [SELECT id, GP_Additional_Approval_Required__c,
            (Select Id, GP_Work_Location__c, GP_Work_Location__r.GP_Country__c from GP_Project_Work_Location_SDOs__r)
            From GP_project__c where Id in: setOfProjectId
        ];

    }

    public List < GP_project__c > getProjectRecordsWithPODocument(Set < Id > setOfProjectId) {

        ID PODocumentID = GPCommon.getRecordTypeId('GP_Project_Document__c', 'PO Document');

        return [select GP_Is_PO_Required__c, id, (Select id from GP_Project_Documents__r Where recordtypeid =: PODocumentID)
            from GP_Project__c where id in: setOfProjectId
        ];

    }

    public GP_project__c getProjectRecordForClosure(String projectId) {
        return [SELECT id, GP_Primary_SDO__r.GP_ischina__c,RecordType.Name, GP_End_Date__c, GP_SOW_End_Date__c, GP_Oracle_PID__c, GP_Controller_User__c,
            GP_Validation_String_For_PID_Closure__c, GP_Approval_Status__c,
            (select id from GP_Project_Documents__r where GP_Type__c = 'Project Closure Signoff') FROM GP_Project__c
            where id =: projectId
        ];

    }
    public GP_project__c getProjectProcessInstanceForClosure(String projectId) {
        return [SELECT id, RecordType.Name, GP_End_Date__c, GP_SOW_End_Date__c, GP_Oracle_PID__c, GP_Controller_User__c,
            GP_Validation_String_For_PID_Closure__c, GP_Approval_Status__c,
            (select id, SubmittedById from ProcessInstances ORDER BY CreatedDate DESC) FROM GP_Project__c
            where id =: projectId
        ];

    }

    public list < GP_project__c > getParentProjectDetails(set < id > projectId, set < string > setfields) {
        string strQuery = 'Select id';
        for (String strFields: setfields) {
            strQuery += ',' + strFields;
        }

        strQuery += ' from GP_project__c where id in : projectId';

        return database.query(strQuery);
    }

    public static list < GP_project__c > getParentProjectForGroupInvoice(GP_Project_Address__c directAddress) {
        return [SELECT Id, Name, GP_Oracle_PID__c
            FROM GP_Project__c
            where GP_Customer_Master__c =: directAddress.GP_Customer_Name__c
            and GP_Bill_to_Address__c =: directAddress.GP_Bill_to_Address__c
            and GP_Ship_to_Address__c =: directAddress.GP_Ship_to_Address__c
            and GP_Oracle_PID__c != null
        ];
    }
    
    public static list < GP_project__c > getDuplicateProject(set<string> setOfLongName, set<string> setOfShortName) {
        return [SELECT Id, Name, GP_Oracle_PID__c, GP_Project_Long_Name__c, GP_Project_Short_Name__c
            FROM GP_Project__c
            where GP_Project_Long_Name__c in: setOfLongName
            or GP_Project_Short_Name__c in: setOfShortName
        ];
    }
    
    public static List<GP_Project__c> getAllRejectedProjects() {
        return [SELECT Id, Name, GP_Auto_Reject_Date__c, GP_User_Requested_Status__c, GP_Oracle_PID__c
            FROM GP_Project__c
            where GP_Approval_Status__c = 'Rejected' AND GP_Auto_Reject_Date__c != null];
    }
}