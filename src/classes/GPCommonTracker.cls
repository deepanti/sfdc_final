/*=======================================================================================
Description: Common Test Data Class containing all test data for every sObject

=======================================================================================
Version#     Date                           Author                    Description
=======================================================================================
1.0          31-OCt-2017                   Mandeep Singh Chauhan            Initial Version 
=======================================================================================  
*/


public with sharing class GPCommonTracker {
    
    public static Map < String, String > mapOfJobTypeVsColumToBeUpdated = new Map < String, String > {
        'Upload Time Sheet Entries' => 'GP_PL_Employee_OHR__c;GP_TSC_Ex_Type__c;GP_TSC_Modified_Hours__c;GP_Project__c;GP_TSC_Project_Task__c;GP_TSC_Date__c',
            'Update Project Work Location & SDOs' => 'GP_PWL_BCP_Requirement__c;GP_PWL_Primary__c;GP_Project__c;GP_PWL_Work_Location__c',
            'Update Project Leadership' => 'GP_PL_Employee_OHR__c;GP_PL_End_Date__c;GP_PL_Leadership_Role__c;GP_PL_Start_Date__c;GP_Project__c',
            'Update Project' => 'GP_PRJ_Attn_To_Email__c;GP_PRJ_Attn_To_Name__c;GP_PRJ_Billing_Currency__c;GP_PRJ_Business_Hierarchy__c;GP_PRJ_Business_Hierarchy_L2__c;GP_PRJ_Business_Hierarchy_L3__c;GP_PRJ_Business_Hierarchy_L4__c;GP_PRJ_CC1__c;GP_PRJ_CC2__c;GP_PRJ_CC3__c;GP_PRJ_Customer_Contact_Number__c;GP_PRJ_Customer_SPOC_Email__c;GP_PRJ_Customer_SPOC_Name__c;GP_PRJ_Fax_Number__c;GP_PRJ_HSL__c;GP_PRJ_MOD__c;GP_PRJ_Nature_of_Work__c;GP_PRJ_Portal_Name__c;GP_PRJ_Primary_SDO__c;GP_PRJ_Product__c;GP_PRJ_Project_Category__c;GP_PRJ_Project_Description__c;GP_PRJ_Project_Long_Name__c;GP_PRJ_Name__c;GP_PRJ_Project_Type__c;GP_PRJ_Service_Line__c;GP_PRJ_SILO__c;GP_PRJ_Start_Date__c;GP_PRJ_End_Date__c;GP_Project__c'
            };
                
                public static GP_Employee_Master__c getEmployee() {
                    GP_Employee_Master__c empObj = new GP_Employee_Master__c();
                    empObj.Name = 'Testemployee';
                    //empObj.GP_Resource_Allocation__c = resourseallobj.Id;
                    //empObj.GP_SFDC_User__c = objUser.Id;   
                    empObj.GP_EMPLOYEE_TYPE__c = 'Employee';
                    empObj.GP_Person_ID__c = 'EMP-001';
                    empObj.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test@test.com';
                    return empObj;
                }
    
    public static GP_Icon_Master__c getIconMaster() {
        GP_Icon_Master__c iconMaster = new GP_Icon_Master__c();
        return iconMaster;
    }
    
    public static Business_Segments__c getBS() {
        Business_Segments__c objBS = new Business_Segments__c();
        objBS.Name = 'Test';
        return objBS;
        
    }
    public static Sub_Business__c getSB(Id bsId) {
        Sub_Business__c objSB = new Sub_Business__c();
        objSB.name = 'Test';
        objSB.Business_Segment__c = bsId;
        return objSB;
    }
    public Static Account getAccount(Id bsId, Id sbId) {
        Account accobj = new Account();
        accobj.name = 'TestAccount';
        accobj.Business_Segment__c = bsId;
        accobj.Sub_Business__c = sbId;
        accobj.Client_Partner__c = UserInfo.getUserId();
        return accobj;
    }
    
    public static Product2 getProduct() {
        Product2 prodobj = new Product2();
        //oProduct.Name='Additional Client Licenses';
        prodobj.Name = 'test';
        prodobj.IsActive = true;
        return prodobj;
    }
    public static Opportunity getOpportunity(Id accId) {
        Opportunity oppobj = new Opportunity();
        oppobj.Name = 'test';
        oppobj.AccountId = accId;
        oppobj.StageName = '1. Discover';
        oppobj.Target_Source__c = 'Consulting Pull Through';
        oppobj.Opportunity_Origination__c = 'Proactive';
        oppobj.Sales_Region__c = 'India';
        oppobj.Sales_country__c = 'India';
        oppobj.Deal_Type__c = 'Sole Sourced';
        oppobj.CloseDate = System.today().adddays(1);
        oppobj.Revenue_Start_Date__c = System.today().adddays(1);
        oppobj.Contract_Term_in_months__c = 36;
        oppobj.Revenue_Product__c = null;
        oppobj.CMITs_Check__c = False;
        oppobj.Advisor_Firm__c = 'Trestle';
        oppobj.Advisor_Name__c = 'Bernhard Janischowsky';
        
        oppobj.Annuity_Project__c = 'Project';
        oppobj.Deal_Type__c = 'Sole Sourced';
        oppobj.Opportunity_Source__c ='Renewal';
        
        oppobj.Win_Loss_Dropped_reason1__c = 'test';
        oppobj.Win_Loss_Dropped_reason2__c = 'test';
        oppobj.Win_Loss_Dropped_reason3__c = 'test';
        oppobj.SPOC_s__c = 'Nitesh Aggarwal';
        
        return oppobj;
    }
    
    //Create the OpportunityProduct__c
    public static OpportunityProduct__c getOpportunityProduct(Id ProdId, Id OppId) {
        OpportunityProduct__c objoppprod = new OpportunityProduct__c();
        objoppprod.Service_Line_Category_Oli__c = '1';
        objoppprod.Service_line_OLI__c = '11';
        objoppprod.Product_Autonumber__c = 'OLI';
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        objoppprod.Product_Family_OLI__c = 'Doc Management';
        objoppprod.Product__c = ProdId;
        objoppprod.COE__c = 'IMS';
        objoppprod.P_L_SUB_BUSINESS__c = 'Auto';
        objoppprod.DeliveryLocation__c = 'Americas';
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        objoppprod.LocalCurrency__c = 'INR';
        objoppprod.RevenueStartDate__c = System.today().adddays(1);
        //objoppprod.ContractTerminmonths__c = intContractTerminmonths;
        objoppprod.SalesExecutive__c = UserInfo.getUserId();
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        objoppprod.Quarterly_FTE_1st_month__c = 2;
        objoppprod.FTE_4th_month__c = 2;
        objoppprod.FTE_7th_month__c = 2;
        objoppprod.FTE_10th_month__c = 2;
        objoppprod.OpportunityId__c = OppId;
        //objoppprod.TCVLocal__c =DecTCV_Local   ;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        return objoppprod;
    }
    
    public static GP_Opportunity_Project__c getoppproject(Id accId) {
        GP_Opportunity_Project__c objopppro = new GP_Opportunity_Project__c();
        objopppro.Name = 'Test';
        objopppro.GP_Customer_L4_Name__c = accId;
        return objopppro;
        
    }
    
    public static GP_Work_Location__c getWorkLocation() {
        GP_Work_Location__c objSDO = new GP_Work_Location__c();
        objSDO.name = 'TestWL & SDO Master';
        objSDO.GP_Oracle_Id__c = 'oracle';
        objSDO.GP_Status__c = 'No';
        objSDO.GP_Description__c = 'Description';
        return objSDO;
    }
    
    public static GP_Project_Expense__c getProjectExpense(Id projectId) {
        GP_Project_Expense__c projectExpense = new GP_Project_Expense__c();
        projectExpense.GP_Project__c = projectId;
        return projectExpense;
    }
    
    public static GP_Pinnacle_Master__c GetpinnacleMaster() {
        GP_Pinnacle_Master__c objpinnacleMaster = new GP_Pinnacle_Master__c();
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('HSL Master').getRecordTypeId();
        objpinnacleMaster.Name = 'HSL';
        // objpinnacleMaster.GP_Company__c = 'Saasfocus.inc';
        objpinnacleMaster.GP_Description__c = 'Test Description';
        objpinnacleMaster.GP_Status__c = 'Active';
        return objpinnacleMaster;
    }
    public static GP_Pinnacle_Master__c GetGlobalSettingspinnacleMaster() {
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = new GP_Pinnacle_Master__c();
        objpinnacleMasterGlobalSetting.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Global Settings').getRecordTypeId();
        objpinnacleMasterGlobalSetting.Name = 'Global Setting';
        objpinnacleMasterGlobalSetting.GP_TimeSheet_Financial_Month__c = Date.newInstance(2018, 6, 17);
        objpinnacleMasterGlobalSetting.GP_Description__c = 'Desc';
        objpinnacleMasterGlobalSetting.GP_Full_Time_Emp_Min_Hrs__c = 8;
        objpinnacleMasterGlobalSetting.GP_Full_Time_Emp_Max_Hrs__c = 24;
        objpinnacleMasterGlobalSetting.GP_Contractor_Emp_Min_Hrs__c = 5;
        objpinnacleMasterGlobalSetting.GP_Contractor_Emp_Max_Hrs__c = 8;
        return objpinnacleMasterGlobalSetting;
    }
    public static GP_Role__c getRole(GP_Work_Location__c objSDO, GP_Pinnacle_Master__c objpinnacleMaster) {
        GP_Role__c objrole = new GP_Role__c();
        objrole.Name = 'Testrole';
        objrole.GP_Active__c = True;
        objrole.GP_HSL_Master__c = objpinnacleMaster.id;
        //objrole.GP_Delivery_Org__c= 'xyz';
        //objrole.GP_Work_Location_SDO_Master__c = objSDO.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        
        //objrole.GP_Role_Category__c = 'PID Creation';
        return objrole;
    }
    
    public static GP_User_Role__c getUserRole(GP_Role__c objrole, User objuser) {
        GP_User_Role__c objuserrole = new GP_User_Role__c();
        //objuserrole.Name = 'PID Creation';(Auto number)
        objuserrole.GP_Active__c = True;
        objuserrole.GP_Role__c = objrole.Id;
        objuserrole.GP_User__c = objuser.Id;
        return objuserrole;
    }
    
    //public static GP_Employee_Type_Hours__c getEmployeeHoursCustomSetting() {
    //    GP_Employee_Type_Hours__c employeeHours = new GP_Employee_Type_Hours__c();
    //    employeeHours.GP_Maximum_Hours__c = 24;
    //    //empObj.GP_Resource_Allocation__c = resourseallobj.Id;
    //    //empObj.GP_SFDC_User__c = objUser.Id;   
    //    employeeHours.Name = 'Full Time';
    //    employeeHours.GP_Minimum_Hours__c = 8;
    //    return employeeHours;
    //}
    
    public static GP_Deal__c getDeal() {
        GP_Deal__c dealObj = new GP_Deal__c();
        dealObj.CurrencyIsoCode = 'INR';
        dealObj.Name = 'testdeal';
        return dealObj;
    }
    
    public static GP_Deal__c getDeal(String dealType, String businessGroupL1, String businessSegmentL2) {
        GP_Deal__c dealObj = new GP_Deal__c();
        
        dealObj.Name = 'testdeal';
        dealObj.CurrencyIsoCode = 'INR';
        dealObj.GP_Deal_Type__c = dealType;
        dealObj.GP_Business_Group_L1__c = businessGroupL1;
        dealObj.GP_Business_Segment_L2__c = businessSegmentL2;
        
        return dealObj;
    }
    
    public static GP_Project__c getProject(GP_Deal__c dealObj, String RecordTypeName, GP_Project_Template__c objprjtemp, User objuser, GP_Role__c objrole) {
        GP_Project__c prjObj = new GP_Project__c();
        prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get(RecordTypeName).getRecordTypeId();
        prjObj.GP_Deal__c = dealObj.Id;
        prjObj.GP_PID_Approver_User__c = objuser.Id;
        prjObj.GP_Project_Template__c = objprjtemp != null ? objprjtemp.Id : null; //lookup  
        prjObj.GP_PID_Creator_Role__c = objrole.Id;
        prjObj.GP_PID_Update_Role__c = objrole.Id;
        return prjObj;
    }
    
    
    public static GP_Project_Task__c getProjectTask(GP_Project__c prjObj) {
        GP_Project_Task__c prjTskObj = new GP_Project_Task__c();
        prjTskObj.Name = 'testtask';
        prjTskObj.GP_Active__c = True;
        prjTskObj.GP_Project__c = prjObj.Id;
        //prjTskObj.GP_Start_Date__c = ;
        //prjTskObj.GP_Task_Master__c = taskmasObj.Id;   
        return prjTskObj;
    }
    
    public static GP_Timesheet_Entry__c getTimesheetEntry(GP_Employee_Master__c empObj, GP_Project__c prjObj, GP_Timesheet_Transaction__c timesheettrnsctnObj) {
        GP_Timesheet_Entry__c timeshtentryObj = new GP_Timesheet_Entry__c();
        //timeshtentryObj.Name = 'testtimesheet';
        timeshtentryObj.GP_Employee__c = empObj.Id;
        //timeshtentryObj.GP_Project__c = prjObj.Id;
        //timeshtentryObj.GP_Task_Master__c = taskmasObj.Id;   
        timeshtentryObj.GP_Timesheet_Transaction__c = timesheettrnsctnObj.Id;
        timeshtentryObj.GP_Actual_Hours__c = 7;
        timeshtentryObj.GP_Date__c = Date.newInstance(2017, 6, 17);
        return timeshtentryObj;
    }
    
    public static GP_Resource_Allocation__c getResourceAllocation(Id empObjId, Id prjObjId) {
        GP_Resource_Allocation__c resourceAllocationObj = new GP_Resource_Allocation__c();
        resourceAllocationObj.Name = 'testtimesheet';
        resourceAllocationObj.GP_Employee__c = empObjId;
        resourceAllocationObj.GP_Project__c = prjObjId;
        resourceAllocationObj.GP_Start_Date__c = Date.newInstance(2017, 6, 1);
        resourceAllocationObj.GP_End_Date__c = Date.newInstance(2017, 6, 20);
        return resourceAllocationObj;
    }
    
    /* public static GP_Task_Master__c createTaskMaster()
{
GP_Task_Master__c taskmasObj = new GP_Task_Master__c();
taskmasObj.Name = 'testtaskmaster';               
return taskmasObj;
}*/
    
    public static GP_Timesheet_Transaction__c getTimesheetTransaction(GP_Employee_Master__c empObj) {
        GP_Timesheet_Transaction__c timesheettrnsctnObj = new GP_Timesheet_Transaction__c();
        //timesheettrnsctnObj.Name = 'testtransaction';
        timesheettrnsctnObj.GP_Employee__c = empObj.Id;
        //timesheettrnsctnObj.GP_Task_Master__c = taskmasObj.Id;
        timesheettrnsctnObj.GP_Month_Year__c = 'june-2017';
        return timesheettrnsctnObj;
    }
    public static User getUser() {
        User objuser = new User();
        Profile p = [select id, name from profile where name = 'System Administrator'
                     limit 1
                    ];
        
        objuser.profileId = p.Id;
        objuser.username = 'newUserPranav@saasforce.com';
        objuser.email = 'pb@ff.com';
        objuser.emailencodingkey = 'UTF-8';
        objuser.localesidkey = 'en_US';
        objuser.languagelocalekey = 'en_US';
        objuser.timezonesidkey = 'America/Los_Angeles';
        objuser.alias = 'nuser';
        objuser.lastname = 'User Last Name';
        objuser.isActive = true;
        return objuser;
    }
    public static GP_Address__c getAddress() {
        GP_Address__c address = new GP_Address__c();
        address.GP_City__c = 'Gurgaon';
        return address;
    }
    
    public static GP_Address__c getAddress(Id customerId, Id billingEntityId, String siteUseCode) {
        GP_Address__c address = new GP_Address__c();
        address.GP_City__c = 'Gurgaon';
        address.GP_Address_Line_1__c = 'Dummy Address';
        address.GP_Customer__c = customerId;
        address.GP_Billing_Entity__c = billingEntityId;
        address.GP_Status__c = 'Active';
        address.GP_Site_Use_Code__c = siteUseCode;
        return address;
    }
    
    public static GP_Budget_Master__c getBudgetMaster() {
        GP_Budget_Master__c budgetMaster = new GP_Budget_Master__c();
        budgetMaster.GP_Band__c = 'Band';
        budgetMaster.GP_Country__c = 'Country';
        budgetMaster.GP_Product__c = 'Product';
        budgetMaster.GP_SOA_External_ID__c = 'extId';
        return budgetMaster;
    }
    
    public static GP_Project_Budget__c getProjectBudget(Id projectId, Id budgetId) {
        GP_Project_Budget__c projectBudget = new GP_Project_Budget__c();
        projectBudget.GP_Project__c = projectId;
        projectBudget.GP_Budget_Master__c = budgetId;
        projectBudget.GP_Band__c = 'Band';
        projectBudget.GP_Country__c = 'Country';
        projectBudget.GP_Effort_Hours__c = 10;
        projectBudget.GP_Product__c = 'Product';
        return projectBudget;
    }
    
    public static GP_Project_Template__c getProjectTemplate() {
        List < staticResource > ListOfTemplate = [SELECT id, Body, Name
                                                  FROM staticResource
                                                  WHERE Name = 'baseProjectTemplate'
                                                  OR Name = 'FinalProjectTemplate'
                                                  OR Name = 'StageWiseField'
                                                 ];
        staticResource baseTemplateData = getStaticResourceByName('baseProjectTemplate', ListOfTemplate);
        staticResource finalTemplateData = getStaticResourceByName('FinalProjectTemplate', ListOfTemplate);
        staticResource stageWiseTemplateData = getStaticResourceByName('StageWiseField', ListOfTemplate);
        
        String serializedTemplateRecord = baseTemplateData.body.toString();
        
        String defaultJSON1 = serializedTemplateRecord;
        
        String serializedFinalTemplateRecord = finalTemplateData.body.toString();
        String serializedStageWiseFieldRecord = stageWiseTemplateData.body.toString();
        
        serializedFinalTemplateRecord = serializedFinalTemplateRecord.substring(1, serializedFinalTemplateRecord.length() - 1);
        //serializedStageWiseFieldRecord = serializedStageWiseFieldRecord.substring(1, serializedStageWiseFieldRecord.length() - 1);

        System.debug('serializedStageWiseFieldRecord: ' + serializedStageWiseFieldRecord);
        
        String finalJson1 = serializedFinalTemplateRecord.substring(0, 5);
        String finalJson2 = serializedFinalTemplateRecord.substring(5, 10);
        String finalJson3 = serializedFinalTemplateRecord.substring(10);
        
        
        GP_Project_Template__c projectTemplate = new GP_Project_Template__c(GP_Default_JSON_1__c = defaultJSON1,
                                                                            GP_Final_JSON_1__c = serializedFinalTemplateRecord, 
                                                                            GP_Final_JSON_2__c = finalJson2, 
                                                                            GP_Final_JSON_3__c = finalJson3,
                                                                            GP_Stage_Wise_Field_Project__c = serializedStageWiseFieldRecord);
        return projectTemplate;
    }
    
    public static GP_Project_Template__c getProjectTemplate(String type, String businessGroupL1, String businessSegmentL2) {
        List < staticResource > ListOfTemplate = [SELECT id, Body, Name
                                                  FROM staticResource
                                                  WHERE Name = 'baseProjectTemplate'
                                                  OR Name = 'FinalProjectTemplate'
                                                  OR Name = 'StageWiseField'
                                                 ];
        staticResource baseTemplateData = getStaticResourceByName('baseProjectTemplate', ListOfTemplate);
        staticResource finalTemplateData = getStaticResourceByName('FinalProjectTemplate', ListOfTemplate);
        staticResource stageWiseTemplateData = getStaticResourceByName('StageWiseField', ListOfTemplate);
        
        String serializedTemplateRecord = baseTemplateData.body.toString();
        
        String defaultJSON1 = serializedTemplateRecord;
        
        
        String serializedFinalTemplateRecord = finalTemplateData.body.toString();
        String serializedStageWiseFieldRecord = stageWiseTemplateData.body.toString();
        
        serializedFinalTemplateRecord = serializedFinalTemplateRecord.substring(1, serializedFinalTemplateRecord.length() - 1);
        serializedStageWiseFieldRecord = serializedStageWiseFieldRecord.substring(1, serializedStageWiseFieldRecord.length() - 1);
        
        String finalJson1 = serializedFinalTemplateRecord.substring(0, 5);
        String finalJson2 = serializedFinalTemplateRecord.substring(5, 10);
        String finalJson3 = serializedFinalTemplateRecord.substring(10);
        
        GP_Project_Template__c projectTemplate = new GP_Project_Template__c(GP_Default_JSON_1__c = defaultJSON1,
                                                                            GP_Type__c = type,
                                                                            GP_Active__c = true,
                                                                            GP_Business_Group_L1__c = businessGroupL1,
                                                                            GP_Business_Segment_L2__c = businessSegmentL2,
                                                                            GP_Final_JSON_1__c = finalJson1,
                                                                            GP_Final_JSON_2__c = finalJson2,
                                                                            GP_Final_JSON_3__c = finalJson3,
                                                                            GP_Stage_Wise_Field_Project__c = serializedStageWiseFieldRecord);
        return projectTemplate;
    }
    
    public static staticResource getStaticResourceByName(String resourceName, List < staticResource > listOfStaticResource) {
        for (staticResource resource: listOfStaticResource) {
            if (resource.Name == resourceName)
                return resource;
        }
        return null;
    }
    
    public static GP_Leadership_Master__c getLeadership(Id accountId, String status) {
        GP_Leadership_Master__c leadershipMaster = new GP_Leadership_Master__c();
        
        leadershipMaster.GP_Account__c = accountId;
        leadershipMaster.GP_Status__c = status;
        
        return leadershipMaster;
    }
    
    public static GP_Project_Leadership__c getProjectLeadership(Id projectId, Id leadershipMasterId) {
        GP_Project_Leadership__c projectLeadership = new GP_Project_Leadership__c();
        projectLeadership.GP_Project__c = projectId;
        projectLeadership.GP_Leadership_Master__c = leadershipMasterId;
        return projectLeadership;
    }
    
    public static GP_Billing_Milestone__c getBillingMilestone(Id projectId, Id workLocationId) {
        GP_Billing_Milestone__c billingMilestone = new GP_Billing_Milestone__c();
        billingMilestone.GP_Project__c = projectId;
        billingMilestone.GP_Work_Location__c = workLocationId;
        
        return billingMilestone;
    }
    public static GP_Project_Work_Location_SDO__c getProjectWorkLocationSDO(Id projectId, Id workLocationId) {
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = new GP_Project_Work_Location_SDO__c();
        objProjectWorkLocationSDO.GP_Project__c = projectId;
        objProjectWorkLocationSDO.GP_Work_Location__c = workLocationId;
        return objProjectWorkLocationSDO;
    }
    public static GP_Project_Document__c getProjectDocument(Id projectId) {
        GP_Project_Document__c objProjDoc = new GP_Project_Document__c();
        objProjDoc.GP_Project__c = projectId;
        return objProjDoc;
    }
    public static GP_Project_Document__c getProjectDocumentWithRecordType(Id projectId, String recordTypeName) {
        GP_Project_Document__c objProjDoc = new GP_Project_Document__c();
        objProjDoc.GP_Project__c = projectId;
        objProjDoc.RecordTypeId = Schema.SObjectType.GP_Project_Document__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        return objProjDoc;
    }
    public static GP_Job__c getJobRecord(String jobType) {
        GP_Job__c objJob = new GP_Job__c();
        objJob.GP_Job_Type__c = jobType;
        objJob.GP_Column_s_To_Update_Upload__c = mapOfJobTypeVsColumToBeUpdated.get(jobType);
        return objJob;
    }
    public static GP_Employee_HR_History__c getEmployeeHR(String empId) {
        GP_Employee_HR_History__c empHR = new GP_Employee_HR_History__c();
        empHR.GP_Employee_Person_Id__c = '1234567';
        empHR.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empHR.GP_Employees__c = empId;
        empHR.GP_ASGN_EFFECTIVE_START__c = system.today().addDays(-1);
        empHR.GP_ASGN_EFFECTIVE_END__c = system.today().addDays(30);
        return empHR;
    }
    public static GP_Temporary_Data__c getTemporaryDataForTimeSheet(Id jobId,
                                                                    String employeePersonId,
                                                                    String projectOHRId,
                                                                    String projectTaskOHRId) {
                                                                        GP_Temporary_Data__c objTemporaryData = new GP_Temporary_Data__c();
                                                                        objTemporaryData.GP_Employee__c = employeePersonId;
                                                                        objTemporaryData.GP_Project__c = projectOHRId;
                                                                        objTemporaryData.GP_TSC_Project_Task__c = projectTaskOHRId;
                                                                        objTemporaryData.GP_TSC_Date__c = system.today();
                                                                        objTemporaryData.GP_TSC_Ex_Type__c = 'Billable';
                                                                        objTemporaryData.GP_Job_Id__c = jobId;
                                                                        objTemporaryData.GP_TSC_Modified_Hours__c = 8;
                                                                        DateTime entryDate = system.today();
                                                                        String monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);
                                                                        
                                                                        objTemporaryData.GP_TSC_Month_Year__c = monthYear;
                                                                        
                                                                        return objTemporaryData;
                                                                    }
    public static GP_Temporary_Data__c getTemporaryDataForProject(Id jobId,
                                                                  String projectOHRId) {
                                                                      GP_Temporary_Data__c objTemporaryData = new GP_Temporary_Data__c();
                                                                      objTemporaryData.GP_PRJ_Name__c = 'NAME';
                                                                      objTemporaryData.GP_Project__c = projectOHRId;
                                                                      objTemporaryData.GP_Job_Id__c = jobId;
                                                                      return objTemporaryData;
                                                                  }
    public static GP_Temporary_Data__c getTemporaryDataForProjectLeaderShip(Id jobId,
                                                                            String projectOHRId,
                                                                            String roleName) {
                                                                                GP_Temporary_Data__c objTemporaryData = new GP_Temporary_Data__c();
                                                                                objTemporaryData.GP_PL_Leadership_Role__c = roleName;
                                                                                objTemporaryData.GP_Project__c = projectOHRId;
                                                                                objTemporaryData.GP_PL_End_Date__c = system.today().addDays(20);
                                                                                objTemporaryData.GP_Job_Id__c = jobId;
                                                                                return objTemporaryData;
                                                                            }
    public static GP_Temporary_Data__c getTemporaryDataForProjectWorkLocation(Id jobId,
                                                                              String projectOHRId,
                                                                              String workLocationOracleId) {
                                                                                  GP_Temporary_Data__c objTemporaryData = new GP_Temporary_Data__c();
                                                                                  objTemporaryData.GP_PWL_Work_Location__c = workLocationOracleId;
                                                                                  objTemporaryData.GP_Job_Id__c = jobId;
                                                                                  objTemporaryData.GP_PWL_BCP_Requirement__c = 'YES';
                                                                                  objTemporaryData.GP_Project__c = projectOHRId;
                                                                                  return objTemporaryData;
                                                                              }
    public static GP_Opp_Project_Leadership__c getOppProjectLeadership() {
        
        GP_Opp_Project_Leadership__c oppProjectleadership = new GP_Opp_Project_Leadership__c();
        // oppProjectleadership.Name = 'Test';
        return oppProjectleadership;
        
    }
    public static GP_Project_Version_History__c getProjectVersionHistory(String projectId) {
        
        GP_Project_Version_History__c projectVersionHistory = new GP_Project_Version_History__c();
        projectVersionHistory.GP_Project__c=projectId;
        return projectVersionHistory;
        
    }
    public static GP_Project_Classification__c getProjectClassification(String projectId) {
        
        GP_Project_Classification__c projectClassification = new GP_Project_Classification__c();
        projectClassification.GP_Project__c=projectId;
        return projectClassification;
        
    }
    
    public static GP_Project_Version_Line_Item_History__c getProjectVersionLineItemHistory(String projectVersionHistoryId) {
        
        GP_Project_Version_Line_Item_History__c projectVersionLineItemHistory = new GP_Project_Version_Line_Item_History__c();
        projectVersionLineItemHistory.GP_Project_Version_History__c = projectVersionHistoryId;
        return projectVersionLineItemHistory;
        
    }
    
    public static GP_Project_Address__c getProjectAddress(Id projectId, String relationship, Id customerId) {
        GP_Project_Address__c projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c = projectId;
        projectAddress.GP_Relationship__c = relationship;
        projectAddress.GP_Customer_Name__c = customerId;
        return projectAddress;
    }
    
    public static GP_Project_Leadership__c getProjectLeadership(Id projectId, Id leadershipMasterId, Date startDate, Date endDate, Id employeeId) {
        GP_Project_Leadership__c projectLeadership = new GP_Project_Leadership__c();
        projectLeadership.GP_Project__c = projectId;
        projectLeadership.GP_Leadership_Master__c = leadershipMasterId;
        projectLeadership.GP_Start_Date__c = startDate;
        projectLeadership.GP_End_Date__c = endDate;
        projectLeadership.GP_Employee_ID__c = employeeId;
        return projectLeadership;
    }
    
    public static GP_Leadership_Master__c getLeadershipMaster(String leadershipRole, Id recordTypeId, String mandatoryForStage, String pIDCategory) {
        GP_Leadership_Master__c leadershipMaster = new GP_Leadership_Master__c();
        leadershipMaster.GP_Leadership_Role__c = leadershipRole;
        leadershipMaster.RecordTypeId = recordTypeId;
        leadershipMaster.GP_Mandatory_for_stage__c = mandatoryForStage;
        leadershipMaster.GP_PID_Category__c = pIDCategory;
        
        return leadershipMaster;
    }
    
    public static GP_Project_Work_Location_SDO__c getProjectWorkLocation(Id projectId, Id workLocationId, Boolean isPrimary) {
        GP_Project_Work_Location_SDO__c projectWorkLocation = new GP_Project_Work_Location_SDO__c();
        projectWorkLocation.GP_Project__c = projectId;
        projectWorkLocation.GP_Work_Location__c = workLocationId;
        projectWorkLocation.GP_Primary__c = isPrimary;
        
        return projectWorkLocation;
    }
    
    public static GP_OMS_Detail__c getOMSDetail(GP_Deal__c deal) {
        GP_OMS_Detail__c omsDetail = new GP_OMS_Detail__c();
        omsDetail.GP_OMS_Deal_ID__c = '1245';
        omsDetail.GP_Work_Location_Code__c = '9245';
        omsDetail.GP_Work_Location_Name__c = 'WLName';
        omsDetail.GP_Band__c ='Band';
        omsDetail.GP_Deal__c = deal.Id;
        return omsDetail;
    }
}