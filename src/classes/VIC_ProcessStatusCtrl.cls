public class VIC_ProcessStatusCtrl {
    
    @AuraEnabled
    public static list<String> getFinancialYear(){
        return vic_CommonUtil.getYearForDisplay();   
    }
    
    @AuraEnabled
    public static String getCurrentVICYear(){
        VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();        
        return String.valueOf(Integer.ValueOf(processInfo.VIC_Process_Year__c));   
    }
    @AuraEnabled
    public static String getCurrentAnnualYear(){
        VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();        
        return String.valueOf(Integer.ValueOf(processInfo.VIC_Annual_Process_Year__c));   
    }
    @AuraEnabled
    public static boolean setVICProcessYear(Integer dYear){
        
        try{
            VIC_Process_Information__c processInfo = [Select Id, VIC_Process_Year__c 
                                                      FROM VIC_Process_Information__c
                                                      LIMIT 1];
            
            processInfo.VIC_Process_Year__c  = dYear;
            update processInfo;
            return true;
        }catch(Exception ex){
            
            throw new AuraHandledException(ex.getMessage());
        }        
    }
    
    @AuraEnabled
    public static boolean setAnnualProcessYear(Integer dYear){
        try{
            VIC_Process_Information__c processInfo = [Select Id,VIC_Annual_Process_Year__c
                                                      FROM VIC_Process_Information__c
                                                      LIMIT 1];
            processInfo.VIC_Annual_Process_Year__c= dYear;
            update processInfo;
            return true;
        }catch(Exception ex){
            throw new AuraHandledException(ex.getMessage());
        }        
    }
    
    @AuraEnabled
    public static void saveHRDay(Integer Dday){
        
        VIC_Process_Information__c updateProcess = [Select Id,VIC_Annual_Process_Year__c,VIC_Calculate_Sales_Leader_Day__c,
                                                    VIC_Calculate_HR_Day__c
                                                    FROM VIC_Process_Information__c LIMIT 1];
        
        
        updateProcess.VIC_Calculate_HR_Day__c = Dday;
        update updateProcess;
        
    }       
    
    
    @AuraEnabled
    public static void saveSLDay(Integer slDay){
        
        VIC_Process_Information__c updateProcess = [Select Id,VIC_Annual_Process_Year__c,VIC_Calculate_Sales_Leader_Day__c,
                                                    VIC_Calculate_HR_Day__c
                                                    FROM VIC_Process_Information__c LIMIT 1];
        
        
        updateProcess.VIC_Calculate_Sales_Leader_Day__c = slDay;
        update updateProcess;
        
    }
    
    @AuraEnabled
    public static string getJobDate(String jobName){
        
        return (VIC_Process_Status_Job__c.getValues(jobName).Notification_Job_Run_on__c);
    }
  
   @AuraEnabled
    public static decimal getJobTime(String jobName){
        
        return (VIC_Process_Status_Job__c.getValues(jobName).Notification_Job_Running_Time__c);
    }  
    @AuraEnabled
    public static boolean getRunningStatusOfJob(String jobName){
        
        return (VIC_Process_Status_Job__c.getValues(jobName)!=null && VIC_Process_Status_Job__c.getValues(jobName).VIC_Is_Running__c);
    }
    @AuraEnabled
    public static date getlastdeactivatedOfJob(String jobName){
        date dtActivated=VIC_Process_Status_Job__c.getValues(jobName).VIC_Start_on__c;
        
        return dtActivated;
    }
    
    @AuraEnabled
    public static void enaqueSchedulerJob(String jobName, Boolean status){
        VIC_ScheduleUpfontVICBatch sch2 = new VIC_ScheduleUpfontVICBatch(); //Schedule apex class name
        String sch = '0 0 * * * ?'; //schedule interval time
        Id schid;
        VIC_Process_Status_Job__c jobRecord = new VIC_Process_Status_Job__c();
        if(VIC_Process_Status_Job__c.getValues(jobName)==null){
            jobRecord.name = jobName;
            schid = system.schedule('VIC Upfront Batch Job', sch, sch2);
            jobRecord.VIC_Job_Name__c = jobName;
            jobRecord.VIC_Job_ID__c = schid;
            jobRecord.VIC_Start_on__c = system.today();
            jobRecord.VIC_Is_Running__c = status;
            insert jobRecord;
        }
        if(!status){
            jobRecord = VIC_Process_Status_Job__c.getValues(jobName);
            jobRecord.VIC_Is_Running__c = false;
            jobRecord.VIC_Last_Deactivated_on__c = system.today();
            jobRecord.VIC_Start_on__c=null;
            update jobRecord;
            System.abortJob(jobRecord.VIC_Job_ID__c);
        }
        else if(status && !VIC_Process_Status_Job__c.getValues(jobName).VIC_Is_Running__c){
            jobRecord = VIC_Process_Status_Job__c.getValues(jobName);
            jobRecord.VIC_Is_Running__c = true;
            jobRecord.VIC_Start_on__c = system.today();
            schid = system.schedule('VIC Upfront Batch Job', sch, sch2);
            jobRecord.VIC_Job_ID__c = schid;
            update jobRecord;
        }
        
    }
    
    
    @AuraEnabled
    public static void enaqueSendSchedulerJob(String jobName, Boolean status,string day,decimal tme){
        VIC_ScheduleSendNotToSupnHrBatch  sch2 = new VIC_ScheduleSendNotToSupnHrBatch ();
        
        string cron='0 0 '+tme+' ? * '+day+' *';
        system.debug('cron'+cron);
        Id schid;
        
        VIC_Process_Status_Job__c jobRecord = new VIC_Process_Status_Job__c();
        if(VIC_Process_Status_Job__c.getValues(jobName)==null){
            jobRecord.name = jobName;
            schid = system.schedule('VIC Incentive Notification', cron, sch2);
            jobRecord.VIC_Job_Name__c = jobName;
            jobRecord.VIC_Job_ID__c = schid;
            jobRecord.VIC_Start_on__c = system.today();
            jobRecord.VIC_Is_Running__c = status;
            jobRecord.Notification_Job_Run_on__c=day;
            jobRecord.Notification_Job_Running_Time__c=tme;
            insert jobRecord;
        }
        if(!status){
            
            jobRecord = VIC_Process_Status_Job__c.getValues(jobName);
            
            CronTrigger ct = [SELECT Id, TimesTriggered, NextFireTime
                              FROM CronTrigger 
                              WHERE Id =:jobRecord.VIC_Job_Id__c];
            System.abortJob(ct.id);
            
            jobRecord.VIC_Job_ID__c ='' ;
            jobRecord.VIC_Start_on__c= null ;
            
            jobRecord.VIC_Is_Running__c = false;
            jobRecord.VIC_Last_Deactivated_on__c = system.today();
            jobRecord.Notification_Job_Run_on__c='';
            jobRecord.Notification_Job_Running_Time__c=null;
            
            update jobRecord;
            
        }
        else if(status && !VIC_Process_Status_Job__c.getValues(jobName).VIC_Is_Running__c ){
            jobRecord = VIC_Process_Status_Job__c.getValues(jobName);
            
            jobRecord.VIC_Is_Running__c = true;
            jobRecord.VIC_Start_on__c = system.today();
            schid = system.schedule('VIC Incentive Notification', cron, sch2);
            jobRecord.VIC_Job_ID__c = schid;
            jobRecord.Notification_Job_Run_on__c=day;
            jobRecord.Notification_Job_Running_Time__c=tme;
            update jobRecord;
        }
        if(status && VIC_Process_Status_Job__c.getValues(jobName).VIC_Is_Running__c){
            jobRecord = VIC_Process_Status_Job__c.getValues(jobName);
            CronTrigger ct = [SELECT Id, TimesTriggered, NextFireTime
                              FROM CronTrigger 
                              WHERE Id =:jobRecord.VIC_Job_Id__c];
            System.abortJob(ct.id);
            jobRecord.VIC_Start_on__c = system.today();
            schid = system.schedule('VIC Incentive Notification', cron, sch2);
            jobRecord.VIC_Job_ID__c = schid;
            jobRecord.Notification_Job_Run_on__c=day;
            jobRecord.Notification_Job_Running_Time__c=tme;
            update jobRecord; 
            
        }
        
        
        
        
    }
    
    
    @AuraEnabled
    public static void runUpfrontBatchServerController(){
        Boolean isAnniversaryCalculation = false;
        VIC_CalculateUpfrontIncentivesBatch upfrontBatch = new VIC_CalculateUpfrontIncentivesBatch(isAnniversaryCalculation);
        Database.executeBatch(upfrontBatch, 1);
    }
    
    @AuraEnabled
    public static void runUpfrontBatchAnnualServerController(){
        Boolean isAnniversaryCalculation = true;
        VIC_CalculateUpfrontIncentivesBatch upfrontBatch = new VIC_CalculateUpfrontIncentivesBatch(isAnniversaryCalculation);
        Database.executeBatch(upfrontBatch, 1);
    }
    
    @AuraEnabled
    public static void runUpTCVBatchServerController(){
        VIC_CalculateTCVIncentiveBatch  tcvBatch = new VIC_CalculateTCVIncentiveBatch();
        Database.executeBatch(tcvBatch, 2);
    } 
    
    @AuraEnabled
    public static void runCalculateGuaranteedAmountController(){
        VIC_GuaranteeComponentBatchCtlr obj = new VIC_GuaranteeComponentBatchCtlr();
        Database.executeBatch(obj);
    }
    
    @AuraEnabled
    public static void runCurrConvUpfrController(){
        boolean isUpfrontCalculation=true;
        VIC_CurrencyConversionBatch  CuCvBatch = new VIC_CurrencyConversionBatch(isUpfrontCalculation);
        Database.executeBatch(CuCvBatch, 2);
    }
    
    @AuraEnabled
    public static void runCurrConvAnnualController(){
        boolean isUpfrontCalculation=false;
        VIC_CurrencyConversionBatch  CuCvBatch = new VIC_CurrencyConversionBatch(isUpfrontCalculation);
        Database.executeBatch(CuCvBatch, 2);
    }   
    
    
    @AuraEnabled
    public static void runUpScoreCardBatchServerController(){
        VIC_ScorecardBatch  scoreBatch = new VIC_ScorecardBatch();
        Database.executeBatch(scoreBatch,10);
    } 
    
    @AuraEnabled
    public static void runUpTCVAcceleratorIOTSBatchServerController(){
        VIC_TcvAcceleratorBatch tcvIOTSBatch = new VIC_TcvAcceleratorBatch();
        Database.executeBatch(tcvIOTSBatch, 2);
    } 
    
    
}