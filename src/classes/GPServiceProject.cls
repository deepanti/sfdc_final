public with sharing class GPServiceProject {
    public class GPServiceProjectException extends Exception {}
    public static Boolean skipParentTagging;
    public static Boolean isFormattedLogRequired;
    public static Boolean isLogForTemporaryId;
    private static Map <String, GPProjectTemplateFieldWrapper> mapOfFieldNameToTemplate;

    public static list < GP_Project__c > lstProject = new list < GP_Project__c >();
    private static Map<String, List <String>> mapOfSubSectionToListOfErrors;

    //used on the GPControllerProjectSubmission for validate Logged In User
    public static boolean validateLoggedInUser(GP_Project__c projectObj) {
        list < GP_User_Role__c > lstUserRole = [select id, GP_Active__c, GP_Role__c, GP_Role__r.Name, GP_User__c 
                                                from GP_User_Role__c 
                                                where GP_Role__r.GP_Role_Category__c = 'PID Creation'
                                                and GP_Active__c = true
                                                and GP_User__c =: userInfo.getUserId()
                                                and GP_Role__r.GP_Active__c = true
                                                and GP_Role__r.GP_Work_Location_SDO_Master__c =: projectObj.GP_Primary_SDO__c
                                                and GP_Role__r.GP_Type_of_Role__c = 'SDO'
                                               ];
        
        if (lstUserRole != null && lstUserRole.size() > 0) {
            return true;
        }
        return false;
    }
    
    
    //used on the GPControllerProjectSubmission for validate Project Pricing Status
    public static boolean validateProjectPricingStatus(GP_Project__c objProject) {
        Id strCMITSRecordType = GPCommon.getRecordTypeId('GP_Project__c', 'CMITS');
        if (objProject != null && objProject.RecordTypeId == strCMITSRecordType) {
            if (objProject.GP_Pricing_Status__c != 'Approved') {
                return false;
            }
        }
        return true;
    }
    
    //used on the GPControllerProjectApproval for validate Project Stages 
    public static boolean validateProjectStages(GP_Project__c objProject) {
        if (objProject.GP_Project_Stages_for_Approver__c != null && objProject.GP_Project_Stages_for_Approver__c != '')
            return true;
        else
            return false;
    }
    
    //used on the GPControllerProjectApproval for validate Approval Status
    public static boolean validateApprovalStatus(GP_Project__c objProject) {
        if (objProject.GP_Approval_Status__c == 'Pending for Approval')
            return true;
        else
            return false;
    }
    
    public static boolean validateApprovalStatusForClosure(GP_Project__c objProject) {
        if (objProject.GP_Approval_Status__c == 'Pending For Closure')
            return true;
        else
            return false;
    }
    
    //used on the GPControllerProjectApproval for validate Logged in User
    public static boolean validateLoggedinUserApprover(GP_Project__c objProject) {
        List < ProcessInstanceWorkitem > lstWorkItems = [SELECT Id, ProcessInstanceId, ProcessInstance.Status, ActorId
                                                         FROM ProcessInstanceWorkitem WHERE
                                                         ProcessInstance.TargetObjectId =: objProject.Id and
                                                         ProcessInstance.Status =: 'Pending'
                                                        ];
        if (lstWorkItems != null && lstWorkItems.size() > 0) {
            set < Id > setOfGroupId = new set < Id > ();
            for (ProcessInstanceWorkitem EachWorkItem: lstWorkItems) {
                if (string.valueOf(EachWorkItem.ActorId).startsWith('005')) {
                    if (EachWorkItem.ActorId == UserInfo.getUserId()) {
                        return true;
                    }
                } else if (string.valueOf(EachWorkItem.ActorId).startsWith('00G')) {
                    setOfGroupId.add(EachWorkItem.ActorId);
                }
            }
            
            if (setOfGroupId != null && setOfGroupId.size() > 0) {
                list < GroupMember > lstMembers = [select id, UserOrGroupId, GroupId from GroupMember where
                                                   GroupId in: setOfGroupId and
                                                   UserOrGroupId =: UserInfo.getUserId()
                                                  ];
                if (lstMembers != null && lstMembers.size() > 0) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static void updateEmployeeEndDateInOldVersion(GP_Project__c project) {
        
        Id projectId = project.Id;
        GP_Project_Version_History__c projectVersion;
        GP_Project__c projectObj, oldProject;
        
        try {
            projectObj = [SELECT Id, GP_Parent_Project__c
                          FROM GP_Project__c
                          WHERE Id =: projectId
                         ];
            
            //Select the version of latest previously created project.
            projectVersion = [SELECT Id, GP_Version_No__c, GP_Project_JSON__c
                              FROM GP_Project_Version_History__c
                              WHERE GP_Project__c =: projectObj.GP_Parent_Project__c
                              AND GP_Status__c = 'Approved'
                              ORDER BY GP_Version_No__c Desc
                              LIMIT 1
                             ];
            
            oldProject = (GP_Project__c) JSON.deserialize(projectVersion.GP_Project_JSON__c, GP_Project__c.class);
            oldProject.GP_GPM_Start_Date__c = project.GP_GPM_Start_Date__c.addDays(-1);
            
            projectVersion.GP_Project_JSON__c = JSON.serialize(oldProject, false);
            
            update projectVersion;
        } catch (Exception ex) {
            return;
        }
    }
    
    //For sending the notification to the Creator user, approver, Project manager and FP&A when the project is marked as isClosed 
    //mandeep.singh@saasfocus.com
    @InvocableMethod(label = 'Project Closure Notifications'
                     description = 'If project closer approval is marked as ‘Approved’, the project stage will be marked as Closed and email notification will be sent to Creator user, approver, Project manager and FP&A with Oracle PID number..')
    public static void projectCloserNotification(list < ID > lstprojectID) {
        Messaging.SingleEmailMessage message;
        list < Messaging.SingleEmailMessage > EmaiTolist = new list < Messaging.SingleEmailMessage >();
        list < String > listofemails;
        Id MandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        EmailTemplate EmpTemplate;
        try {
            EmpTemplate = [Select id, DeveloperName, body, subject, htmlvalue
                           from EmailTemplate where
                           developername = 'GP_Email_Template_For_Project_Closer_Notification'
                           limit 1
                          ];
        } catch (Exception ex) {
            System.debug('Exc' + ex.getMessage());
        }
        if (lstprojectID.size() > 0) {
            Id orgWideEmailId = GPCommon.getOrgWideEmailId();
            for (GP_Project__c prj: new GPSelectorProject().getProjectsForClosureNotification(lstprojectID)) {
                if (prj.GP_Oracle_PID__c != null && prj.name != null && EmpTemplate != null) {
                    listofemails = new list < string > ();
                    GPCommon objcommon = new GPCommon();
                    if (EmpTemplate != null) {
                        message = objcommon.notificationThroughEmailForBatch(EmpTemplate, prj, 'GP_Field_Set_For_Batch_For_Email_Notific', 'GP_Project__c');
                    }
                    if (prj.GP_GPM_Employee__r != null && prj.GP_GPM_Employee__r.GP_SFDC_User__c != null) {
                        message.settargetobjectid(prj.GP_GPM_Employee__r.GP_SFDC_User__c);
                        EmaiTolist.add(message);
                    } else if (prj.GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                        listofemails.add(prj.GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                    }
                    
                    if (prj.Ownerid != null) {
                        message.settargetobjectid(prj.Ownerid);
                        EmaiTolist.add(message);
                    }
                    
                    for (GP_Project_Leadership__c prjleadobj: prj.Project_Leaderships__r) {
                        system.debug('prjleadobj@@@@@@' + prjleadobj);
                        if (prjleadobj.GP_Employee_ID__r != null && prjleadobj.GP_Employee_ID__r.GP_SFDC_User__c != null) {
                            system.debug('mail sent to id@@@@@@' + prjleadobj.GP_Employee_ID__r.GP_SFDC_User__c);
                            message.settargetobjectid(prjleadobj.GP_Employee_ID__r.GP_SFDC_User__c);
                            EmaiTolist.add(message);
                        } else if (prjleadobj.GP_Employee_ID__r != null && prjleadobj.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                            system.debug('mail sent to Email Addresssssssssssssss@@@@@@' + prjleadobj.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                            listofemails.add(prjleadobj.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                        }
                    }
                    if (listofemails != null && listofemails.size() > 0) {
                        message.setToAddresses(listofemails);
                    }
                    if (message != null) {
                        message.settargetobjectid(null);
                        if(orgWideEmailId != null)
                            message.setOrgWideEmailAddressId(orgWideEmailId);
                        EmaiTolist.add(message);
                    }
                }
            }
            try {
                if (EmaiTolist != null && EmaiTolist.size() > 0 && GPCommon.allowSendingMail()) {
                    Messaging.SendEmailResult[] EHRBP = Messaging.sendEmail(EmaiTolist);
                }
            } catch (Exception ex) {
                System.debug('Exc' + ex.getMessage());
            }
        }
    }
    
    //Validate Project and Child Record Insert and Update Permission based on Current Working User 
    public static map <Id, Boolean> validateProjectandChildRecordAccess(set <ID> setofProjectID) {
        Id loginUserID = UserInfo.getUserId();
        Map < Id, Boolean > mapofUserIDToAccess = new Map < Id, Boolean > ();
        Map < Id, Boolean > mapofProjectIDToAccess = new Map < Id, Boolean > ();
        
        // Get Admin Role Users from User Role where Type of Role is Admin
        for (GP_User_Role__c objUR: new GPSelectorUserRole().selectUserfromAdminUserRole()) {
            mapofUserIDToAccess.put(objUR.GP_User__c, true);
        }
        
        // Get System Admin Users
        for (User objUser: new GPSelectorUser().getUserDetail()) {
            //mapofUserIDToAccess.put(objUser.Id, true);
        }
        if (setofProjectID != null) {
            if (!mapofUserIDToAccess.containsKey(loginUserID)) {
                for (GP_Project__c objProj: new GPSelectorProject().getProject(setofProjectID)) {
                    
                    if (objProj.GP_Current_Working_User__c != null && objProj.GP_Current_Working_User__c != loginUserID) {
                        mapofProjectIDToAccess.put(objProj.ID, false);
                    } else {
                        mapofProjectIDToAccess.put(objProj.ID, true);
                    }
                }
            }
        }
        
        return mapofProjectIDToAccess;
    }
    
    public static Map<String, List <String>> validateProjectAgainstTemplate(GP_Project__c project, GP_Project_Template__c projectTemplate, Map<String, String> mapOfApiNameToLabel) {
        mapOfSubSectionToListOfErrors = new Map<String, List <String>>();

        if (projectTemplate == null ||
            (projectTemplate.GP_Final_JSON_1__c == null &&
             projectTemplate.GP_Final_JSON_2__c == null &&
             projectTemplate.GP_Final_JSON_3__c == null)) {
                 return null;
             }
        
        String serializedFinalTemplate = '';
        List <String> listOfErrors = new List <String> ();
        
        if (projectTemplate.GP_Final_JSON_1__c != null)
            serializedFinalTemplate += projectTemplate.GP_Final_JSON_1__c;
        if (projectTemplate.GP_Final_JSON_2__c != null)
            serializedFinalTemplate += projectTemplate.GP_Final_JSON_2__c;
        if (projectTemplate.GP_Final_JSON_3__c != null)
            serializedFinalTemplate += projectTemplate.GP_Final_JSON_3__c;
        
        mapOfFieldNameToTemplate = (Map <String, GPProjectTemplateFieldWrapper> ) JSON.deserialize(serializedFinalTemplate, Map <String, GPProjectTemplateFieldWrapper>.class);
        Set<String> projectFields = Schema.SObjectType.GP_Project__c.fields.getMap().keySet();
        
        String validationMessage = '';
        String key, fieldName;

        //iterate through all the fields of template and validate if required fields are available.
        for (String fieldKey: mapOfFieldNameToTemplate.keySet()) {
            //only validate fields of project object
            if (fieldKey.split('___')[0] != 'Project')
                continue;
            
            fieldName = fieldKey.split('___')[2];
            
            if (mapOfFieldNameToTemplate.get(fieldKey).isSectionVisible && 
                mapOfFieldNameToTemplate.get(fieldKey).isRequired && 
                isValidField('GP_Project__c', fieldName) && 
                project.get(fieldName) == null) {
                    if (isFormattedLogRequired) {
                        key = mapOfFieldNameToTemplate.get(fieldKey).sectionName;
                    } else if (isLogForTemporaryId) {
                        key = project.GP_Last_Temporary_Record_Id__c;
                    } else if (!isLogForTemporaryId) {
                        key = project.Id;
                    }
                    if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
                        mapOfSubSectionToListOfErrors.put(key, new List < String > ());
                    }

                    validationMessage = mapOfApiNameToLabel.get(fieldName.toLowerCase()) + ' is a mandatory field.'; 
                    mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
                    
            }
        }
        return mapOfSubSectionToListOfErrors;
    }
    
    @testVisible
    private static Boolean isValidField(String sObjectName, String fieldName) {
        try {
            SObject so = Schema.getGlobalDescribe().get(sObjectName).newSObject();
            return so.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName);
        }
        catch(Exception ex) {}
         
        return false;
    }

    public static Map<String, List <String>> customValidateProject(GP_Project__c project, Map<String, String> mapOfApiNameToLabel) {
        /*
        * if Project type is T&M then GP_T_M_With_Cap__c is Mandatory.
        * if Project GP_T_M_With_Cap__c is true then GP_TCV__c is Mandatory.
        * Project Name length cannot be greater than 30 character.
        * Project GP_Project_type__c = 'T&M' then GP_Overtime_Billable__c is not applicable.
        * Project GP_Project_type__c = 'T&M' then GP_Timesheet_Requirement__c is not applicable.
        * Project GP_Project_type__c = 'T&M' then GP_Bill_Rate_Type__c is mandatory.
        * GPM start date should be between project start and end date.
        * project start date should be less than end date
        * SOW start date should be less than end date
        * Contract start date should be less than end date
        * if Us tax is true then all the fields under it are mandatory:
        * "GP_State__c",
        * "GP_City__c",
        * "GP_Sub_Category_Of_Services__c",
        * "GP_Tax_Exemption_Certificate_Available__c",
        * "GP_Valid_Till__c",
        * "GP_Pincode__c"
        * */

        mapOfSubSectionToListOfErrors = new Map<String, List <String>>();

        List<String> listOfErrors = new List<String>();
        
        //if Project type is T&M then GP_T_M_With_Cap__c is Mandatory.
        //if (project.GP_Project_type__c == 'T&M' && project.GP_T_M_With_Cap__c == false) {
        //    listOfErrors.add('T & M With cap is Mandatory for T&M billing type.');
        //}
        
        //if Project GP_T_M_With_Cap__c is true then GP_TCV__c is Mandatory.
        String key, validationMessage, fieldName;

        if (project.RecordType.Name != 'BPM' && project.GP_T_M_With_Cap__c && project.GP_TCV__c == null) {
            fieldName = 'GP_TCV__c';
            validationMessage = 'TCV is mandatory if T & M With Cap is true.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }

        if (project.RecordType.Name == 'CMITS' && !project.GP_Deal__r.GP_No_Pricing_in_OMS__c && project.GP_OMS_Deal_ID__c == null ) {
            fieldName = 'GP_OMS_Deal_ID__c';
            validationMessage = 'OMS Deal Id is mandatory when no Pricing in OMS available.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }

        if (project.RecordType.Name == 'CMITS' && !project.GP_Deal__r.GP_No_Pricing_in_OMS__c && project.GP_Revenue_Description__c == null ) {
            fieldName = 'GP_Revenue_Description__c';
            validationMessage = 'Revenue Description is mandatory when no Pricing in OMS available.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }
        
        //Project Name length cannot be greater than 30 character.
        if(project.Name !=null && project.Name.length() > 30) {
            fieldName = 'Name';
            validationMessage = 'Project name cannot be greater than 30 character.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        } else if(project.Name != null && project.Name.contains(System.Label.GP_BPM_Clone_Project_Name_Prefix)){
            fieldName = 'Name';
            validationMessage = 'Please change Project Name.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }

        if(project.GP_Project_Long_Name__c != null && project.GP_Project_Long_Name__c.contains(System.Label.GP_BPM_Clone_Project_Name_Prefix)){
            fieldName = 'GP_Project_Long_Name__c';
            validationMessage = 'Please change Project Long Name.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }
        
        //if Project GP_Project_type__c = 'T&M' then GP_Overtime_Billable__c is not applicable.
        if(project.GP_Project_type__c != 'T&M' && project.GP_Project_type__c != 'FTE'  && project.GP_Overtime_Billable__c == true) {
            fieldName = 'GP_Overtime_Billable__c';
            validationMessage = 'Overtime billable is only applicable for T&M and FTE projects.';
            addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }
        
        //if Project GP_Project_type__c = 'T&M' then GP_Timesheet_Requirement__c is not applicable.
        if(project.GP_Project_type__c != 'T&M' && project.GP_Project_type__c != 'FTE'  && project.GP_Timesheet_Requirement__c != null) {
            fieldName = 'GP_Timesheet_Requirement__c';
            validationMessage = 'Timesheet requirement is only applicable for T&M and FTE projects.';
            addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }
        
        //if Project GP_Project_type__c = 'T&M' then GP_Bill_Rate_Type__c is mandatory..
        if(project.GP_Project_type__c == 'T&M'  && project.GP_Bill_Rate_Type__c == null) {
            fieldName = 'GP_Bill_Rate_Type__c';
            validationMessage = 'If Project type is T&M then Bill rate type is Mandatory.';
            addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }
        
        //GPM start date should be between project start and end date.
        //if (project.GP_GPM_Start_Date__c < project.GP_Start_Date__c || project.GP_GPM_Start_Date__c >= project.GP_End_Date__c) {
        //    listOfErrors.add('If Project type is T&M then Bill rate type is Mandatory.');    
        //}
        
        //project start date should be less than end date
        if (project.GP_Start_Date__c != null&&
            project.GP_End_Date__c != null &&
            project.GP_Start_Date__c > project.GP_End_Date__c) {
                fieldName = 'GP_Start_Date__c';
                validationMessage = 'Project Start Date should be less than end date.';
                addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }
        
        //SOW start date should be less than end date
        if (project.GP_SOW_Start_Date__c != null &&
            project.GP_SOW_End_Date__c != null &&
            project.GP_SOW_Start_Date__c > project.GP_SOW_End_Date__c) {
                fieldName = 'GP_SOW_Start_Date__c';
                validationMessage = 'SOW Start Date should be less than end date.';
                addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }

        if (project.GP_Shift_Start_Date__c != null &&
            (project.GP_Shift_Start_Date__c > project.GP_End_Date__c ||
            project.GP_Shift_Start_Date__c < project.GP_Start_Date__c)) {
                fieldName = 'GP_Shift_Start_Date__c';
                validationMessage = 'Shift Start Date must lie in between project duration.';
                addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }

        if (project.GP_Transition_End_Date__c != null &&
            (project.GP_Transition_End_Date__c > project.GP_End_Date__c ||
            project.GP_Transition_End_Date__c < project.GP_Start_Date__c)) {
                fieldName = 'GP_Transition_End_Date__c';
                validationMessage = 'Transition End Date must lie in between project duration.';
                addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }

        //Contract start date should be less than end date
        if (project.GP_Contract_Start_Date__c != null &&
            project.GP_Contract_End_Date__c != null &&
            project.GP_Contract_Start_Date__c > project.GP_Contract_End_Date__c) {
                fieldName = 'GP_Contract_Start_Date__c';
                validationMessage = 'Contract Start Date should be less than end date.';
                addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        }

        //Project date should be between Contract date.
        //if (project.GP_Contract_Start_Date__c != null &&
        //    project.GP_Contract_Start_Date__c > project.GP_Start_Date__c) {
        //        fieldName = 'GP_Start_Date__c';
        //        validationMessage = 'Project date should be between Contract date.';
        //        addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        //}

        //if (project.GP_Contract_End_Date__c != null &&
        //    project.GP_Contract_End_Date__c < project.GP_Start_Date__c) {
        //        fieldName = 'GP_Start_Date__c';
        //        validationMessage = 'Project date should be between Contract date.';
        //        addErrorInSubSection(project, 'Project_Info', fieldName, validationMessage);
        //}
		//GP_Sub_Category_Of_Services__c is mandatory..
        		        
        /* String billableProjectType = System.Label.GP_Billable_Project_Type_Values;
        if((project.RecordType.Name == 'Indirect PID' ||
            (String.isNotBlank(project.GP_Project_type__c) && 
             !billableProjectType.containsIgnoreCase(project.GP_Project_type__c)))
            && project.GP_Sub_Category_Of_Services__c == null) {
            fieldName = 'GP_Sub_Category_Of_Services__c';
            validationMessage = 'Sub Category Of Services is mandatory.';
            addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }*/
         //<!--Avinash: GE Divine code changes -->
        if(String.isNotBlank(project.GP_Project_type__c)
            && project.GP_Divine__c == null && project.GP_Business_Group_L1__c == 'GE') {
            fieldName = 'GP_Divine__c';
            validationMessage = 'GE classification is mandatory.';
            addErrorInSubSection(project, 'OtherClassification', fieldName, validationMessage);
        }
        
        //validate if Us tax is true then all the fields under it are mandatory
        if (project.GP_Service_Deliver_in_US_Canada__c) {
            List<String> listOfFieldUnderUsTax = new List<String> {
                'gp_state__c', 
                'gp_city__c',
                'gp_sub_category_of_services__c',
                'gp_tax_exemption_certificate_available__c',
                'gp_pincode__c',
                //'gp_services_to_be_delivered_from__c',
                'gp_country__c',
                'gp_us_address__c'
            };
                    
            for(String usTaxFieldName : listOfFieldUnderUsTax) {
                if(project.get(usTaxFieldName) == null) {
                    validationMessage = mapOfApiNameToLabel.get(usTaxFieldName) + ' is mandatory.'; 
                    addErrorInSubSection(project, 'OtherClassification', usTaxFieldName, validationMessage);
                }
            }

            if (project.GP_Tax_Exemption_Certificate_Available__c && project.GP_Valid_Till__c == null) {
                validationMessage = mapOfApiNameToLabel.get('gp_valid_till__c') + ' is mandatory.'; 
                addErrorInSubSection(project, 'OtherClassification', 'GP_Valid_Till__c', validationMessage);
            }
            
        }
        
        //Added lines to make HSN category mandatory.
        //System.debug('==is GST Enabled=='+ project.GP_is_GST_Enabled_OU__c + '==OU==' + project.GP_Operating_Unit__c);
        // ECR-HSN Changes
        String billable = System.Label.GP_Billable_Project_Type_Values;
        
       //project.GP_Oracle_PID__c == 'NA' && 
        	//String.isBlank(project.GP_Parent_Project__c) &&
        if ((project.RecordType.Name != 'Indirect PID' ||
            (String.isNotBlank(project.GP_Project_type__c) && 
             billable.containsIgnoreCase(project.GP_Project_type__c))) &&
            String.isBlank(project.GP_HSN_Category_ID__c) &&
           	project.GP_Operating_Unit__r.GP_Is_GST_Enabled__c) {
                		
            fieldName = 'GP_HSN_Category_ID__c';
            validationMessage = 'HSN Category is mandatory.';
            addErrorInSubSection(project, 'OtherClassification', 'GP_HSN_Category_ID__c', validationMessage);
        }

        //ICON status = Singed Contract received should be checked before submission of project. 
        //Means when user submits the project and if ICON status is not Signed Contract Approved then, 
        //Project stage cannot be set to approved.
        String activeSignedContractStatus = System.Label.GP_Active_CRN_Status_On_Project;
        Set<String> listOfActiveStatus = new Set<String>();
        listOfActiveStatus.addAll(activeSignedContractStatus.split(','));
        system.debug('project-----'+project.GP_CRN_Number__r);
        system.debug('project-----'+project.GP_CRN_Number__c);
        if (project.GP_CRN_Number__c != null && !listOfActiveStatus.contains(project.GP_CRN_Number__r.GP_Status__c)) {  
            addErrorInSubSection(project, 'Project_Info', 'GP_CRN_Number__c', 'CRN should have an active status.');
        }
        
        return mapOfSubSectionToListOfErrors;
    }
    
    private static void addErrorInSubSection(GP_Project__c project, String sectionName, String fieldName, String validationMessage) {
        String key;
        if (isFormattedLogRequired) {
            key = sectionName;
        } else if (isLogForTemporaryId) {
            key = project.GP_Last_Temporary_Record_Id__c;
        } else if (!isLogForTemporaryId) {
            key = project.Id;
        }
        if (!mapOfSubSectionToListOfErrors.containsKey(key)) {
            mapOfSubSectionToListOfErrors.put(key, new List<String>());
        }

        mapOfSubSectionToListOfErrors.get(key).add(validationMessage);
    }

    public static void updateOracleTemplateIdOnProject(List<GP_Project__c> listOfProject) {
        /*Map<String, GP_Pinnacle_Master__c> mapOfKeyToOracleTemplate = new Map<String, GP_Pinnacle_Master__c>();
        Set<String> SetOfProjectType = new Set<String>();
        Set<Id> setOfOperatingUnit = nw Set<Id>();

        for(GP_Project__c project : listOfProject) {
        SetOfProjectType.add(project.GP_Project_type__c);
        setOfOperatingUnit.add(project.GP_Operating_Unit__c);
        } 

        try {
        for(GP_Pinnacle_Master__c oracleTemplate = GPSelectorPinnacleMasters.selectOracleTemplateId(SetOfProjectType, setOfOperatingUnit)) {
        String key = oracleTemplate.GP_Project_Type__c + '___' + oracleTemplate.GP_Entity_Id__c; 
        mapOfKeyToOracleTemplate.put(key, oracleTemplate);
        }

        for(GP_Project__c project : listOfProject) {
        String key = project.GP_Project_Type__c + '___' + project.GP_Operating_Unit__c; 
        GP_Pinnacle_Master__c oracleTemplate = mapOfKeyToOracleTemplate.get(key);
        SetOfProjectType.add(project.GP_Project_type__c);
        setOfOperatingUnit.add(project.GP_Operating_Unit__c);
        } 

        } catch(Exception ex) {
        return;
        }

        project.GP_Oracle_Template_Id__c = oracleTemplate.GP_Oracle_Id__c;*/
    }
}