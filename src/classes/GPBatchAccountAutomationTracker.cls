/**
	Description  : This test class will cover code for batch class GPBatchAccountAutomation and helper class GPBatchAccountAutomationHelper.
	Created by   : Dhruv Singh.
	OHR 	     : 703248825.
	Created On   : 14-10-19.  
*/
@isTest
public class GPBatchAccountAutomationTracker {
	@TestSetup
    public static void testData() {
        // Product Master.
        GP_Product_Master__c pm = new GP_Product_Master__c();
        pm.Name = 'Consulting - Others';
        pm.GP_Product_Name__c = 'Consulting - Others';
        pm.GP_Product_ID__c = '123456';
        pm.GP_Industry_Vertical__c = 'BFS';
        pm.GP_Service_Line__c = 'serviceLine';
        pm.GP_Nature_of_Work__c = 'now';
        pm.GP_Is_Active__c = true;
        insert pm;
        
        // SDO.
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Business_Type__c =  'PBB';
        insert objSdo;
        
        // WT.
        GP_Work_Location__c objWT = GPCommonTracker.getWorkLocation();
        objWT.GP_Status__c = 'Active and Visible';
        objWT.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        objWT.GP_Oracle_Id__c = 'oraclewt';
        insert objWT;
        
        // OU.
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        objpinnacleMaster.GP_Oracle_Id__c = '10001';
        insert objpinnacleMaster;
        
        // PO
        GP_Pinnacle_Master__c objpinnacleMasterPO = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMasterPO.GP_Oracle_Id__c = '10002';
        objpinnacleMasterPO.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Project Organization').getRecordTypeId();
        objpinnacleMasterPO.Name = 'po';
        insert objpinnacleMasterPO;
        
        // Employee Masters
        List<GP_Employee_Master__c> listOfEMs = new List<GP_Employee_Master__c>();
        // FPnA
        GP_Employee_Master__c empObjFPnA = GPCommonTracker.getEmployee();
        //empObjFPnA.GP_isActive__c = true;
        empObjFPnA.GP_Person_ID__c = 'EMP-002';
        empObjFPnA.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test1@test.com';
        empObjFPnA.GP_Final_OHR__c = '10000001';
        //insert empObjFPnA;
        //empObjFPnA.GP_Is_Active__c = true;
        listOfEMs.add(empObjFPnA);
        // PID
        GP_Employee_Master__c empObjPID = GPCommonTracker.getEmployee();
        //empObjPID.GP_isActive__c = true;
        empObjPID.GP_Person_ID__c = 'EMP-003';
        empObjPID.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test2@test.com';
        empObjPID.GP_Final_OHR__c = '10000002';
        //insert empObjPID;
        //empObjPID.GP_Is_Active__c = true;
        listOfEMs.add(empObjPID);
        // GPM
        GP_Employee_Master__c empObjGPM = GPCommonTracker.getEmployee();
        //empObjGPM.GP_isActive__c = true;
        empObjGPM.GP_Person_ID__c = 'EMP-004';
        empObjGPM.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test3@test.com';
        empObjGPM.GP_Final_OHR__c = '10000003';
        //insert empObjGPM;
        //empObjGPM.GP_Is_Active__c = true;
        listOfEMs.add(empObjGPM);        
        insert listOfEMs;
        
        // PT
        GP_Project_Template__c pt = new GP_Project_Template__c();
        pt.Name = 'INDIRECT-INDIRECT';
        pt.GP_Active__c = true;
        pt.GP_Business_Group_L1__c = 'All';
        pt.GP_Business_Segment_L2__c = 'All';
        pt.GP_Business_Name__c = 'Indirect';
        pt.GP_Business_Type__c = 'Indirect';
        insert pt;
        
        // Global values data.
        List<GP_Global_Value_Set__c> listOfGVSs = new List<GP_Global_Value_Set__c>();
        
        GP_Global_Value_Set__c gvsSDO = new GP_Global_Value_Set__c();
        gvsSDO.Name = 'APC_Consulting_SDOs';
        gvsSDO.GP_Description__c = 'Vertical : SDO data.';
        gvsSDO.GP_Value_Set_1__c = '{"BFS":{"oracle":"'+objSdo.Id+'"}}';
        listOfGVSs.add(gvsSDO);
        
		GP_Global_Value_Set__c gvsFPnA = new GP_Global_Value_Set__c();
        gvsFPnA.Name = 'APC_FP&A_Approvers';
        gvsFPnA.GP_Description__c = 'Vertical : FP&A OHR.';
        gvsFPnA.GP_Value_Set_1__c = '{"BFS":"10000001"}';
        listOfGVSs.add(gvsFPnA);

		GP_Global_Value_Set__c gvsOU = new GP_Global_Value_Set__c();
        gvsOU.Name = 'APC_Operating_Units';
        gvsOU.GP_Description__c = 'Oracle Id : recordId of OU.';
        gvsOU.GP_Value_Set_1__c = '{"10001":"'+objpinnacleMaster.Id+'"}';
        listOfGVSs.add(gvsOU);

		GP_Global_Value_Set__c gvsPID = new GP_Global_Value_Set__c();
        gvsPID.Name = 'APC_PID_Approver';
        gvsPID.GP_Description__c = 'Vertical : PID Approver OHR.';
        gvsPID.GP_Value_Set_1__c = '{"BFS":"10000002"}';
        listOfGVSs.add(gvsPID);

		GP_Global_Value_Set__c gvsPO = new GP_Global_Value_Set__c();
        gvsPO.Name = 'APC_Project_Organization';
        gvsPO.GP_Description__c = 'Name : Id.';
        gvsPO.GP_Value_Set_1__c = '{"po":"'+objpinnacleMasterPO.Id+'"}';
        listOfGVSs.add(gvsPO);

		GP_Global_Value_Set__c gvsPT = new GP_Global_Value_Set__c();
        gvsPT.Name = 'APC_Project_Template';
        gvsPT.GP_Description__c = 'Business Name : PT Id.';
        gvsPT.GP_Value_Set_1__c = '{"Indirect":"'+pt.Id+'"}';
        listOfGVSs.add(gvsPT);
        
        GP_Global_Value_Set__c gvsWT = new GP_Global_Value_Set__c();
        gvsWT.Name = 'APC_Work_Locations';
        gvsWT.GP_Description__c = 'Oracle Id : WL recordId.';
        gvsWT.GP_Value_Set_1__c = '{"oraclewt":"'+objWT.Id+'"}';
        listOfGVSs.add(gvsWT);

		insert listOfGVSs;
        
        // Business hierarchy
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        BSobj.For__c = 'GE';
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        accobj.Name = 'Test Account Automation';
        accobj.Business_Group__c = 'GE';
        accobj.Industry_Vertical__c = 'BFS';
        accobj.Sub_Industry_Vertical__c = 'BFS';
        insert accobj ;        
        
        // PID data
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Support';
        dealObj.GP_Deal_Category__c = 'Support';
        dealObj.GP_Status__c = 'Qualified';
        insert dealObj;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c  = null;
        insert objrole;
        
        GP_Project__c prjObjAccount = GPCommonTracker.getProject(dealObj,'Indirect PID',pt,objuser,objrole);
        prjObjAccount.Name = 'ACNT - IO/MS - Test';
        prjObjAccount.GP_Deal__c = dealObj.id;
        prjObjAccount.GP_Primary_SDO__c = objSdo.id;
        prjObjAccount.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjAccount.GP_CRN_Status__c = 'Test';
        prjObjAccount.GP_MOD__c = 'Hard Copy';
        prjObjAccount.GP_Project_type__c = 'Account';
        prjObjAccount.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObjAccount.GP_Oracle_Internal_Id__c = '011';
        prjObjAccount.GP_Oracle_PID__c = 'NA';//850040951
        prjObjAccount.GP_Approval_Status__c = 'Approved';
        prjObjAccount.GP_Oracle_Status__c = 'S';
        insert prjObjAccount;
        
        GP_Project__c prjObjAccountBD = GPCommonTracker.getProject(dealObj,'Indirect PID',pt,objuser,objrole);
        prjObjAccountBD.Name = 'ACNT - BD - Test';
        prjObjAccountBD.GP_Deal__c = dealObj.id;
        prjObjAccountBD.GP_Primary_SDO__c = objSdo.id;
        prjObjAccountBD.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjAccountBD.GP_CRN_Status__c = 'Test';
        prjObjAccountBD.GP_MOD__c = 'Hard Copy';
        prjObjAccountBD.GP_Project_type__c = 'Account BD';
        prjObjAccountBD.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObjAccountBD.GP_Oracle_Internal_Id__c = '012';
        prjObjAccountBD.GP_Oracle_PID__c = 'NA';//703248825
        prjObjAccountBD.GP_Approval_Status__c = 'Approved';
        prjObjAccountBD.GP_Oracle_Status__c = 'S';
        insert prjObjAccountBD;
    }
    
    @isTest
    public static void onAccountCreation() {
        List<Account> lstOfAccs = [Select Id from Account where name = 'Test Account Automation'];
        
		Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void accountWithOnePID() {
        List<Account> lstOfAccs = [Select Id, GP_Account_PID__c from Account where name = 'Test Account Automation'];
        /*List<GP_Project__c> listOfPIDs = [Select Id, GP_Oracle_PID__c from GP_Project__c where GP_Project_Type__c = 'Account'];
        
        listOfPIDs[0].GP_Oracle_PID__c = '850040951';        
        update listOfPIDs[0];*/
        
        lstOfAccs[0].GP_Account_PID__c = '850040951';
        update lstOfAccs[0];
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void accountWithBDPID() {
        List<Account> lstOfAccs = [Select Id, GP_Account_BD_Spend__c from Account where name = 'Test Account Automation'];
        
        lstOfAccs[0].GP_Account_BD_Spend__c = '703248825';
        update lstOfAccs[0];
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void onAccountUpdate() {
        List<Account> lstOfAccs = [Select Id, Industry_Vertical__c, Sub_Industry_Vertical__c, GP_Account_PID__c, GP_Account_BD_Spend__c
                                   from Account where name = 'Test Account Automation'];
        
        lstOfAccs[0].Industry_Vertical__c = 'CPG';
        lstOfAccs[0].Sub_Industry_Vertical__c = 'CPG';
        lstOfAccs[0].GP_Account_PID__c = '850040951';
        lstOfAccs[0].GP_Account_BD_Spend__c = '703248825';
        update lstOfAccs[0];
        
        List<GP_Project__c> listOfPIDs = [Select Id, GP_Oracle_PID__c, GP_Project_Type__c 
                                          from GP_Project__c 
                                          where GP_Project_Type__c = 'Account' OR GP_Project_Type__c = 'Account BD'];
        
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Customer_Hierarchy_L4__c = lstOfAccs[0].Id;
            if(pid.GP_Project_Type__c == 'Account') {
                pid.GP_Oracle_PID__c = '850040951';
            } else if(pid.GP_Project_Type__c == 'Account BD') {
                pid.GP_Oracle_PID__c = '703248825';
            }
        }
        
        if(listOfPIDs.size() > 0)
        	update listOfPIDs;
        
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }    
    
    @isTest
    public static void onAccountNoUpdateFound() {
        List<Account> lstOfAccs = [Select Id, GP_Account_PID__c, GP_Account_BD_Spend__c from Account where name = 'Test Account Automation'];
        
        lstOfAccs[0].GP_Account_PID__c = '850040951';
        lstOfAccs[0].GP_Account_BD_Spend__c = '703248825';
        update lstOfAccs[0];
        
        List<GP_Project__c> listOfPIDs = [Select Id, GP_Oracle_PID__c, GP_Project_Type__c 
                                          from GP_Project__c 
                                          where GP_Project_Type__c = 'Account' OR GP_Project_Type__c = 'Account BD'];
        
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Customer_Hierarchy_L4__c = lstOfAccs[0].Id;
            if(pid.GP_Project_Type__c == 'Account') {
                pid.GP_Oracle_PID__c = '850040951';
            } else if(pid.GP_Project_Type__c == 'Account BD') {
                pid.GP_Oracle_PID__c = '703248825';
            }
        }
        
        if(listOfPIDs.size() > 0)
        	update listOfPIDs;
        
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void handleException1() {
        // Change in PM, or GVS values.
        List<GP_Product_Master__c> listOfPMs = [Select id from GP_Product_Master__c where GP_Product_Id__c = '123456'];
        listOfPMs[0].GP_Industry_Vertical__c = 'CPG';
        
        update listOfPMs[0];
        
        List<Account> lstOfAccs = [Select Id from Account where name = 'Test Account Automation'];
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void handlePIDsNotFound() {
        List<Account> lstOfAccs = [Select Id, GP_Account_PID__c, GP_Account_BD_Spend__c from Account where name = 'Test Account Automation'];
        
        lstOfAccs[0].GP_Account_PID__c = '850040951';
        lstOfAccs[0].GP_Account_BD_Spend__c = '703248825';
        update lstOfAccs[0];
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void handlePendingForApprovalPIDs() {
        List<Account> lstOfAccs = [Select Id, GP_Account_PID__c, GP_Account_BD_Spend__c from Account where name = 'Test Account Automation'];
        
        lstOfAccs[0].GP_Account_PID__c = '850040951';
        lstOfAccs[0].GP_Account_BD_Spend__c = '703248825';
        update lstOfAccs[0];
        
        List<GP_Project__c> listOfPIDs = [Select Id, GP_Oracle_PID__c, GP_Project_Type__c 
                                          from GP_Project__c 
                                          where GP_Project_Type__c = 'Account' OR GP_Project_Type__c = 'Account BD'];
        
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Customer_Hierarchy_L4__c = lstOfAccs[0].Id;
            pid.GP_Approval_Status__c = 'Pending for Approval';
            if(pid.GP_Project_Type__c == 'Account') {
                pid.GP_Oracle_PID__c = '850040951';
            } else if(pid.GP_Project_Type__c == 'Account BD') {
                pid.GP_Oracle_PID__c = '703248825';
            }
        }
        
        if(listOfPIDs.size() > 0)
        	update listOfPIDs;
        
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
    
    @isTest
    public static void forWorkflowExistExecption() {
        List<Account> lstOfAccs = [Select Id from Account where name = 'Test Account Automation'];       
        
        List<GP_Project__c> listOfPIDs = [Select Id, GP_Oracle_PID__c, GP_Project_Type__c 
                                          from GP_Project__c 
                                          where GP_Project_Type__c = 'Account' OR GP_Project_Type__c = 'Account BD'];
        
        for(GP_Project__c pid : listOfPIDs) {
            pid.GP_Customer_Hierarchy_L4__c = lstOfAccs[0].Id;
        }
        
        if(listOfPIDs.size() > 0)
        	update listOfPIDs;
        
        
        Test.startTest();
        Database.executeBatch(new GPBatchAccountAutomation('Where id = \''+lstOfAccs[0].Id+'\''), 1);
        Test.stopTest();
    }
}