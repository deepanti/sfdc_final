public class AsynApexJobDetails {
    //Avinash N	
	
    Public string JobId{get;set;}
    Public string ApexClassName{get;set;}
    Public string JobType{get;set;}
    Public string Status{get;set;}
    Public string NumberOfErrors{get;set;}
    Public string TotalJobItems{get;set;}
    Public string CreatedDate{get;set;}
    Public string CompletedDate{get;set;}	
    Public string CreatedBy{get;set;}
    
}