public with sharing class GPDomainOMSDetail extends fflib_SObjectDomain {

    public GPDomainOMSDetail(List < GP_Deal__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainOMSDetail(sObjectList);
        }
    }

    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_OMS_Detail__c.SObjectType,
            GP_Deal__share.SObjectType,
            GP_Role__c.SObjectType
    };

    public override void onBeforeInsert() {
        updateRecordType();
        updateDealonOMSDetail();
    }

    //public override void onAfterInsert() {
    //    fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
    //    updateDealonOMSDetail(null, uow);
    //    uow.commitWork();
    //}

    private void updateRecordType() {
        Id workLocationRecordTypeID = GPCommon.getRecordTypeId('GP_OMS_Detail__c', 'Work Location');
        Id ExpenseRecordTypeID = GPCommon.getRecordTypeId('GP_OMS_Detail__c', 'Expense');
        Id PricingViewRecordTypeID = GPCommon.getRecordTypeId('GP_OMS_Detail__c', 'Pricing View');
        for (GP_OMS_Detail__c objOMSDetail: (list < GP_OMS_Detail__c > ) records) {
            if (objOMSDetail.GP_Record_Type_Name__c == 'WorkLoaction') {
                objOMSDetail.RecordtypeId = workLocationRecordTypeID;
            }
            if (objOMSDetail.GP_Record_Type_Name__c == 'Expenses') {
                objOMSDetail.RecordtypeId = ExpenseRecordTypeID;
            }
            if (objOMSDetail.GP_Record_Type_Name__c == 'Budget') {
                objOMSDetail.RecordtypeId = PricingViewRecordTypeID;
            }
        }
    }

    private void updateDealonOMSDetail() {

        list < GP_OMS_Detail__c > lstOMSDetails = new list < GP_OMS_Detail__c > ();
        Set < String > setOfOMSDealIds = new Set < String > ();
        for (GP_OMS_Detail__c objOMSDetail: (list < GP_OMS_Detail__c > ) records) {
            if (!string.isBlank(objOMSDetail.GP_OMS_Deal_ID__c)) {
                setOfOMSDealIds.add(objOMSDetail.GP_OMS_Deal_ID__c);
            }
        }

        if (setOfOMSDealIds.size() > 0) {
            Map < String, GP_Deal__c > mapOfDeal = new Map < String, GP_Deal__c > ();
            for (GP_Deal__c deal: [select GP_OMS_Deal_ID__c, Id from GP_Deal__c where GP_OMS_Deal_ID__c in: setOfOMSDealIds]) {
                mapOfDeal.put(deal.GP_OMS_Deal_ID__c, deal);
            }
            for (GP_OMS_Detail__c objOMSDetail: (list < GP_OMS_Detail__c > ) records) {
                if (!string.isBlank(objOMSDetail.GP_OMS_Deal_ID__c) && mapOfDeal.containsKey(objOMSDetail.GP_OMS_Deal_ID__c)) {
                    objOMSDetail.GP_Deal__c = mapOfDeal.get(objOMSDetail.GP_OMS_Deal_ID__c).Id;
                }
            }
        }
    }
}