public class UpdateUserProfile {

    public static void updateProfile(){
        try{
           
            String userID = ApexPages.currentPage().getParameters().get('userID');
            String profileID = ApexPages.currentPage().getParameters().get('ProfileID');
             system.debug('userID=='+userID);
            system.debug('profileID=='+userID);
            update new User(Id=userID,ProfileId = profileID,Has_Stale_Deals__c=false,Old_Profile_Id__c='');
        }
        catch(Exception e){
            System.debug('Exception in Updating Profile=='+e.getStackTraceString());
        }        
    }
}