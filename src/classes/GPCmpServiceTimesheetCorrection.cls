/**
 * @group TimeSheet Correction
 * @group-content ../../ApexDocContent/TimeSheetCorrection.htm
 *
 * @description Apex service class having Aura enabled methods
 *              to fetch, insert, update timesheet records.
 */
public class GPCmpServiceTimesheetCorrection {

    /**
     * @description Returns whether logged in user is Valid to view the page.
     * 
     * @return GPAuraResponse json flag to view the tab.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getIsValidUser();
     */
    @AuraEnabled
    public static GPAuraResponse getIsValidUser(String timesheetTransactionId) {
        Id loggedInUserId = UserInfo.getUserId();
        List < GP_Pinnacle_Master__c > lstOfPinnacleMasterRecord = GPSelectorPinnacleMasters.selectGlobalSettingRecord();
        Boolean isValidUser = GPServiceTimesheetTransaction.validateLoggedinUser(loggedInUserId);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();

        gen.writeObjectField('isValidUser', isValidUser);
        gen.writeObjectField('monthYearDuration', Integer.ValueOf(System.Label.GP_Previous_Month_for_TimeSheet_Correction));
        if (lstOfPinnacleMasterRecord.size() > 0 && lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c != null) {
            gen.writeObjectField('financialMonth', lstOfPinnacleMasterRecord[0].GP_TimeSheet_Financial_Month__c);
        } else {
            return new GPAuraResponse(false, 'Global Setting for financial month is not defined.Please contact your administartor.', null);
        }
        if (timesheetTransactionId != null) {
            GP_Timesheet_Transaction__c transactionRecord = [select Id, GP_Employee_Month_Year_Unique__c, GP_Employee__r.Name, CreatedDate, LastModifiedDate, GP_Oracle_Process_Log__c,
                GP_Approval_Status__c, Name, GP_Approver__r.Name, GP_Employee__c,
                GP_Oracle_Status__c, Owner.Name, GP_Approver__r.OHR_ID__c, GP_Month_Year__c,
                GP_Employee__r.GP_Employee_Unique_Name__c from GP_Timesheet_Transaction__c where id =: timesheetTransactionId
            ];
            if (transactionRecord != null)
                gen.writeObjectField('timesheetRecord', transactionRecord);
        }
        gen.writeEndObject();
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    /**
     * @description Returns Timesheet data for a given employee-month-year.
     * @param objJSONTimeSheetTransaction serialized timesheet transaction object.
     * 
     * @return GPAuraResponse json of TimeSheet Entries.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getTimesheetTransactions(<Serialized Timesheet Transaction Record>);
     */
    @AuraEnabled
    public static GPAuraResponse getTimesheetTransactions(String objJSONTimeSheetTransaction) {
        return GPControllerTimesheetCorrection.getTimesheetTransactions(objJSONTimeSheetTransaction);

    }

    /**
     * @description Returns Timesheet data for a given employee-month-year.
     * @param objJSONTimeSheetTransaction serialized timesheet transaction object.
     * 
     * @return GPAuraResponse json of TimeSheet Entries.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getTimeSheetEntries(<Serialized Timesheet Transaction Record>);
     */
    @AuraEnabled
    public static GPAuraResponse getTimeSheetEntries(String objJSONTimeSheetTransaction) {
        return GPControllerTimesheetCorrection.getTimeSheetEntries(objJSONTimeSheetTransaction);

    }

    /**
     * @description Save Timesheet data for a given employee-month-year.
     * @param lstJSONTimeSheetEntries serialized timesheet entries.
     * @param isForApproval Submittd for approval.
     * @param monthYear Month-year value whose timesheet entries need to be created.
     * @param timeSheetTransactionId timsheet transaction Id corresponding to whom entries will be inserted.
     * @param employeeId employee for whom timesheet correction is done.
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully inserted
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.saveTimeSheetEntries(<lstJSONTimeSheetEntries>,<isForApproval>,<monthYear>,
     *                                                    <timeSheetTransactionId>,<employeeId>);
     */
    @AuraEnabled
    public static GPAuraResponse saveTimeSheetEntries(String lstJSONTimeSheetEntries, Boolean isForApproval, String monthYear,
        String timeSheetTransactionId, String employeeId) {
        return GPControllerTimesheetCorrection.saveTimeSheetEntries(lstJSONTimeSheetEntries, isForApproval, monthYear, timeSheetTransactionId, employeeId);
    }

    /**
     * @description Returns Job record Id for Mass Upload.
     * 
     * @return GPAuraResponse json of Job Record.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getJobId();
     */
    @AuraEnabled
    public static GPAuraResponse getJobId() {
        return GPControllerTimesheetCorrection.getJobId();
    }

    /**
     * @description Returns Project Record.
     * 
     * @return GPAuraResponse json of project Record.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getProjectRecord();
     */
    @AuraEnabled
    public static GPAuraResponse getProjectRecord(Id projectId) {
        GP_Project__c project = new GPSelectorProject().selectProjectRecord(projectId);
        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(project));
    }

    /**
     * @description Returns Project Task Record.
     * 
     * @return GPAuraResponse json of project task Record.
     * 
     * @example
     * GPCmpServiceTimesheetCorrection.getProjectTaskRecord();
     */
    @AuraEnabled
    public static GPAuraResponse getProjectTaskRecord(Id projectTaskId) {
        GP_Project_Task__c projectTask = new GPSelectorProjectTask().getProjectTask(projectTaskId);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (projectTask != null)
            gen.writeObjectField('projectTask', projectTask);

        gen.writeEndObject();
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    @AuraEnabled
    public static GPAuraResponse timesheetStatusToApproveOrReject(Id timesheetTransactionId, String strComment,String action) {
        GPControllerTimesheetCorrection objController = new GPControllerTimesheetCorrection();
        return objController.timesheetStatusChange(timesheetTransactionId, strComment, action);
    }
    
    @AuraEnabled
    public static GPAuraResponse recallRequest(Id timesheetTransactionId) {
        GPControllerTimesheetCorrection objController = new GPControllerTimesheetCorrection();
        return objController.recallApprovalProcess(timesheetTransactionId);
    }
}