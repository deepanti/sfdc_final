public class GPDomainBillingMilestone extends fflib_SObjectDomain {

    public GPDomainBillingMilestone(List < GP_Billing_Milestone__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainBillingMilestone(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Billing_Milestone__c.SObjectType
    };

    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //validateBillingMilestoneRecordAccess(uow,null);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updatePIDApproverFinance(uow, null);
        // onFirstBillingMilestoneInsert(uow,null); Commented By Insha
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        setIsUpdateCheckBox();
        //validateBillingMilestoneRecordAccess(uow, oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updatePIDApproverFinance(uow, oldSObjectMap);
        // Code is moved to GPServiceProjectApproval  // Pankaj 2/4/2018
        //updateProjectApproverFields(uow, oldSObjectMap);
        //uow.commitWork();

    }
    /*public override void onValidate() {
// Validate GP_Billing_Milestone__c 
//for(GP_Billing_Milestone__c  ObjProject : (List<GP_Billing_Milestone__c >) Records) {
//  }
}

public override void onApplyDefaults() {

// Apply defaults to GP_Billing_Milestone__c 
//for(GP_Billing_Milestone__c  ObjProject : (List<GP_Billing_Milestone__c >) Records) { 
//}               
}*/

    /* public void updateProjectApproverFields(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
         Set < String > fieldSetForApproval = new Set < String > ();

         for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Billing_Milestone__c.fieldSets.getMap().get('GP_Fields_For_Approval_CMITS').getFields()) {
             fieldSetForApproval.add(fields.getFieldPath());
         }

         GPCommon.setProjectApproverFieldAndAutoApprovalForRelatedRecords(fieldSetForApproval, null, records, 'GP_Parent_Billing_Milestone__c',
             'GP_Billing_Milestone__c', 'GP_PID_Approver_Finance__c');
     } */

    /* public void updatePIDApproverFinance(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
         Set < String > fieldSet = new Set < String > ();
         GP_Billing_Milestone__c oldProjBudget;
         Map < ID, GP_Project__c > mapofIdToobjProject = new Map < Id, GP_Project__c > ();

         //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
         for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Billing_Milestone__c.fieldSets.getMap().get('GP_PID_Approver_Finance').getFields()) {
             fieldSet.add(fields.getFieldPath());
         }
         if (fieldSet != null && fieldSet.size() > 0) {
             for (GP_Billing_Milestone__c objPB: (List < GP_Billing_Milestone__c > ) records) {

                 mapofIdToobjProject.put(objPB.GP_Project__c, new GP_Project__c(id = objPB.GP_Project__c, GP_PID_Approver_Finance__c = false));

                 if (mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c != true) {
                     for (string eachField: fieldSet) {
                         if (oldSObjectMap != null) {
                             oldProjBudget = (GP_Billing_Milestone__c) oldSObjectMap.get(objPB.Id);
                             if (objPB.get(eachField) != oldProjBudget.get(eachField)) {
                                 mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true;
                             }
                         } else {
                             mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true;
                         }
                     }
                 }
             }
         }

         if (mapofIdToobjProject != null && mapofIdToobjProject.size() > 0) {
             list < GP_Project__c > lstProjecttoUpdate = new list < GP_Project__c > ();
             for (GP_Project__c objPrj: mapofIdToobjProject.values()) {
                 if (objPrj.GP_PID_Approver_Finance__c == true) {
                     lstProjecttoUpdate.add(objPrj);
                 }
             }
             if (lstProjecttoUpdate != null && lstProjecttoUpdate.size() > 0) {
                 uow.registerDirty(lstProjecttoUpdate);
             }
         }
     }*/
    // method to change the currency of the billing milestone record based on the currency of it's project
    // @method is called from onafterInsert


    //Method commented as was not called 08052018
    //public void onFirstBillingMilestoneInsert(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
    //    set < Id > setofid = new set < Id > ();
    //    map < Id, string > mapofIdtoCurrency = new map < Id, string > ();
    //    list < GP_Billing_Milestone__c > lstOfbllngMilstoneForProject;
    //    list < GP_Billing_Milestone__c > lstOfBllngMilestone = new list < GP_Billing_Milestone__c > ();
    //    for (GP_Billing_Milestone__c objBllngMlstone: (list < GP_Billing_Milestone__c > ) records) {
    //        if (objBllngMlstone != null) {
    //            setofid.add(objBllngMlstone.id);
    //        }
    //    }
    //    // system.debug('setofid____'+ setofid);
    //    if (setofid != null && setofid.size() > 0) {
    //        GPSelectorBillingMilestone selector = new GPSelectorBillingMilestone();
    //        lstOfbllngMilstoneForProject = selector.selectCurrencyFromProject(setofid);
    //    }
    //    // system.debug('lstOfbllngMilstoneForProject___'+lstOfbllngMilstoneForProject);
    //    if (lstOfbllngMilstoneForProject != null && lstOfbllngMilstoneForProject.size() > 0) {
    //        for (GP_Billing_Milestone__c bllngMilstone: lstOfbllngMilstoneForProject) {
    //            mapofIdtoCurrency.put(bllngMilstone.GP_Project__c, bllngMilstone.GP_Project__r.currencyIsoCode);

    //        }
    //    }
    //    //  system.debug('mapofIdtoCurrency_____'+ mapofIdtoCurrency); 
    //    for (GP_Billing_Milestone__c bllngMilstone: lstOfbllngMilstoneForProject) {
    //        bllngMilstone.CurrencyIsoCode = mapofIdtoCurrency.get(bllngMilstone.GP_Project__c);
    //        lstOfBllngMilestone.add(bllngMilstone);
    //    }

    //    if (lstOfBllngMilestone != null && lstOfBllngMilestone.size() > 0) {
    //        //update lstOfBllngMilestone;
    //        uow.registerDirty(lstOfBllngMilestone);
    //    }
    //}

    //Method commented as was not called 08052018
    //public void validateBillingMilestoneRecordAccess(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
    //    Set < ID > setOfParentProjectID = new Set < ID > ();
    //    Map < ID, boolean > mapofIDAndBoolean = new Map < ID, boolean > ();
    //    for (GP_Billing_Milestone__c billingMilestone: (List < GP_Billing_Milestone__c > ) records) {
    //        setOfParentProjectID.add(billingMilestone.GP_Project__c);
    //    }
    //    if (setOfParentProjectID != null && setOfParentProjectID.size() > 0) {
    //        mapofIDAndBoolean = GPServiceProject.validateProjectandChildRecordAccess(setOfParentProjectID);
    //    }

    //    for (GP_Billing_Milestone__c billingMilestone: (List < GP_Billing_Milestone__c > ) records) {
    //        if (mapofIDAndBoolean.get(billingMilestone.GP_Project__c) == false) {
    //            billingMilestone.addError('You are not a current working users so you are not authorized to work on Billing Milestone.');
    //        }
    //    }
    //}

    public void setIsUpdateCheckBox() {
            List < String > lstOfFieldAPINames = new List < String > ();
            for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Billing_Milestone__c.fieldSets.getMap().get('GP_PID_Update_Fields').getFields()) {
                lstOfFieldAPINames.add(fields.getFieldPath());
            }
            
            Set < Id > setOfParentRecords = new Set < Id > ();
            for (GP_Billing_Milestone__c projectBillingMilestone: (List < GP_Billing_Milestone__c > ) records) {
                if (projectBillingMilestone.GP_Parent_Billing_Milestone__c != null)
                    setOfParentRecords.add(projectBillingMilestone.GP_Parent_Billing_Milestone__c);
            }

            if (setOfParentRecords.size() > 0) {
                map < Id, GP_Billing_Milestone__c > mapOfParentRecords = new map < Id, GP_Billing_Milestone__c > ();
                for (GP_Billing_Milestone__c projectBillingMilestone: new GPSelectorBillingMilestone().selectProjectBillingMilestoneRecords(setOfParentRecords)) {
                    mapOfParentRecords.put(projectBillingMilestone.Id, projectBillingMilestone);
                }

                GPCommon.setIsUpdatedField(records, mapOfParentRecords, lstOfFieldAPINames, 'GP_Parent_Billing_Milestone__c');
        }
    }
}