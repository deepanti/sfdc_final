public with sharing class GPCmpServiceProjectApprovalHistory {
	
	@AuraEnabled
    public static GPAuraResponse getApprovalsHistory(string objectId) {
    	GPControllerProjectApprovalHistory objProjectApprovalHistory = new GPControllerProjectApprovalHistory();
    	return objProjectApprovalHistory.getApprovalsHistory(objectId);
    }
    
}