public class GPControllerProjectContainer {
    
    private static final String SUCCESS_LABEL = 'SUCCESS';
    private static final String UNTOUCHED_STATUS_LABEL = 'unTouched';
    private static final String DIRTY_STATUS_LABEL = 'dirty';
    private static final String APPROVED_STATUS_LABEL = 'approved';

    private Id projectId;
    private JSONGenerator gen;
    private GP_Project__c project;
    
    private String projectStatus = APPROVED_STATUS_LABEL;
    private String workLocationStatus = APPROVED_STATUS_LABEL;
    private String leadershipStatus = APPROVED_STATUS_LABEL;
    private String billingMilestoneStatus = APPROVED_STATUS_LABEL;
    private String budgetStatus = APPROVED_STATUS_LABEL;
    private String expenseStatus = APPROVED_STATUS_LABEL;
    private String documentUploadStatus = APPROVED_STATUS_LABEL;
    private String resourceAllocationStatus = APPROVED_STATUS_LABEL;
    private String profileBillRateStatus = APPROVED_STATUS_LABEL;//amitppt//07042018
    private Map < String, String > mapOfRoleAccessibility;
    private Map < String, String > mapOfReadRoleAccessibility = new Map<String,String>{
        'projectcreation' => 'read',
                'projectworklocation' => 'read',
                'additionalsdo' => 'read',
                'projectleaderships' => 'read',
                'uploaddocuments' => 'read',
                'resourceallocation' => 'read',
                'billingmilestone' => 'read',
                'projectbudgetpricingview' => 'read',
                'profilebillrate' => 'read',
                'projectexpenses' => 'read',
                'invoicedetails' => 'read',
                'timesheetcorrection' => 'read'
            };

    public GPControllerProjectContainer(Id projectId) {
        this.projectId = projectId;
    }
    
    public GPAuraResponse getProjectChildStatus(Boolean validateApproval) {
        System.System.debug('validateApproval >>> ' + validateApproval);
        try {
            if(validateApproval) {
                GPControllerProjectApprovalValidator validator = new GPControllerProjectApprovalValidator(projectId);
                validator.validateProject();
            }
            setProject();
            setProjectContainerStatus();
            setRoleAccessibility();
            setJson();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    
    private void setProject() {
        GPSelectorProject projectSelector = new GPSelectorProject();
        project = projectSelector.getProjectWithRelatedData(projectId);
    }
    
    private void setRoleAccessibility() {
        mapOfRoleAccessibility = new Map < String, String > ();
        List < String > listOfScreens = new List < String > {
            'projectcreation',
                'projectworklocation',
                'additionalsdo',
                'projectleaderships',
                'uploaddocuments',
                'resourceallocation',
                'billingmilestone',
                'projectbudgetpricingview',
                'profilebillrate',
                'projectexpenses',
                'invoicedetails',
                'timesheetcorrection'
                };
        mapOfRoleAccessibility = GPCommon.getMapOfScreenAccess(listOfScreens, userInfo.getUserId());
        if(project.GP_Current_Working_User__c != UserInfo.getUserId() && mapOfRoleAccessibility.get('projectcreation') == 'edit'){
            mapOfRoleAccessibility = mapOfReadRoleAccessibility;
        }
        system.debug('mapOfRoleAccessibility'+mapOfRoleAccessibility);
    }
    
    private void setProjectContainerStatus() {
        projectStatus = project.GP_Approval_Status__c != null ? project.GP_Approval_Status__c : '';
        
        if (projectStatus != 'Approved') {
            workLocationStatus = getProgressStatus(project.GP_Project_Work_Location_SDOs__r);
            leadershipStatus = getProgressStatus(project.Project_Leaderships__r);
            billingMilestoneStatus = getProgressStatus(project.GP_Billing_Milestones__r);
            budgetStatus = getProgressStatus(project.GP_Project_Budgets__r);
            expenseStatus = getProgressStatus(project.GP_Project_Expenses__r);
            documentUploadStatus = getProgressStatus(project.GP_Project_Documents__r);
            resourceAllocationStatus = getProgressStatus(project.GP_Resource_Allocations__r);
            profileBillRateStatus = getProgressStatus(project.Profile_Bill_Rates__r);////amitppt//07042018
        }
    }
    
    private String getProgressStatus(List < Sobject > listOfObject) {
        Boolean isListNotEmpty = isListNotEmpty(listOfObject);
        return isListNotEmpty ? DIRTY_STATUS_LABEL : UNTOUCHED_STATUS_LABEL;
    }
    
     private Boolean isListNotEmpty(List < Sobject > listOfObject) {
        return listOfObject != null && listOfObject.size() > 0;
    }
    
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('projectStatus', projectStatus);
        gen.writeObjectField('project', project);
        gen.writeStringField('workLocationStatus', workLocationStatus);
        gen.writeStringField('leadershipStatus', leadershipStatus);
        gen.writeStringField('expenseStatus', expenseStatus);
        gen.writeStringField('budgetStatus', budgetStatus);
        gen.writeStringField('documentUploadStatus', documentUploadStatus);
        gen.writeStringField('billingMilestoneStatus', billingMilestoneStatus);
        gen.writeStringField('resourceAllocationStatus', resourceAllocationStatus);
        gen.writeObjectField('mapOfRoleAccessibility', mapOfRoleAccessibility);
        gen.writeEndObject();
    }
}