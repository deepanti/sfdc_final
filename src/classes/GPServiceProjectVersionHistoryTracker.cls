@isTest
public class GPServiceProjectVersionHistoryTracker{
    
    public static testMethod void validateApprovedProjectVersionHistory(){
    
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c parentPrjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentPrjObj.OwnerId = objuser.Id;
        parentPrjObj.GP_Approval_Status__c = 'Draft';
        parentPrjObj.GP_Oracle_PID__c = '1234';
        parentPrjObj.GP_Oracle_Status__c = 'S';
        
        insert parentPrjObj ;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(parentPrjObj.Id);
        insert objDoc;
        
        GP_Project_Version_History__c projectVersionHistory = GPCommonTracker.getProjectVersionHistory(parentPrjObj.Id);
        projectVersionHistory.GP_Status__c = 'Draft';
        insert projectVersionHistory;

        GP_Project_Classification__c projectClassification = GPCommonTracker.getProjectClassification(parentPrjObj.Id);
        insert projectClassification;
        
        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id, parentPrjObj.id);
        insert objResourceAllocation;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(parentPrjObj.Id);
        insert projectExpense;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Parent_Project__c = parentPrjObj.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Oracle_PID__c = '1234';
        prjObj.GP_Oracle_Status__c = '';
        insert prjObj;
        
        GP_Project_Document__c objDoc1 = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc1;
        
        GP_Project_Version_History__c projectVersionHistorys = GPCommonTracker.getProjectVersionHistory(prjObj.Id);
        projectVersionHistorys.GP_Status__c = 'Draft';
        insert projectVersionHistorys;

        GP_Project_Classification__c projectClassifications = GPCommonTracker.getProjectClassification(prjObj.Id);
        insert projectClassifications;
        
        GP_Resource_Allocation__c objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id, prjObj.id);
        insert objResourceAllocation1;
        
        GP_Project_Expense__c projectExpense1 = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense1;
        
        GP_Billing_Milestone__c  billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Last_Temporary_Record_Id__c = prjObj.Id;
        insert billingMilestone;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Profile_Bill_Rate__c objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Profile__c = 'QA';
        objProfileBillRate.GP_Project__c = prjObj.id;
        objProfileBillRate.GP_Bill_Rate__c = 23;
        insert objProfileBillRate;
        
        List<GP_Project_Work_Location_SDO__c> lstPWLSDO = new List<GP_Project_Work_Location_SDO__c>();
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId(); 
        lstPWLSDO.add(objProjectWorkLocationSDO);
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocation.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        lstPWLSDO.add(objProjectWorkLocation);
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocation1.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        lstPWLSDO.add(objProjectWorkLocation1);
        insert lstPWLSDO;
        
        Account accountObj = GPCommonTracker.getAccount(objBS.Id, objSB.Id);
        insert accountObj;

        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Leadership_Role__c = 'Billing SPOC';
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.GP_Leadership_Role_Name__c = 'Billing SPOC';
        insert projectLeadership;
        
        /* This is required for covering code of function name "generateJSONForProcessInstance" of class "GPServiceProjectVersionHistory"
        ProcessInstance objProcessInstance = new ProcessInstance();
        objProcessInstance.TargetObjectId = prjObj.id;
        objProcessInstance.ProcessDefinitionId  = '04a90000000H7IUAA0';
        objProcessInstance.Status = 'Pending';
        insert objProcessInstance;
        
        ProcessInstanceWorkitem objProcessInstanceWorkItem = new ProcessInstanceWorkitem();
        objProcessInstanceWorkItem.ProcessInstanceId =objProcessInstance.id;
        insert objProcessInstanceWorkItem;
        */
        
        GP_Project__c prjObj1 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj1.OwnerId = objuser.Id;
        prjObj1.GP_CRN_Number__c = iconMaster.Id;
        prjObj1.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj1.GP_Parent_Project__c = prjObj.Id;
        prjObj1.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj1.GP_Approval_Status__c = 'Draft';
        prjObj1.GP_Oracle_PID__c = '1234';
        prjObj1.GP_Oracle_Status__c = 'S';
        insert prjObj1;
        
        GP_Project_Document__c objDoc2 = GPCommonTracker.getProjectDocument(prjObj1.Id);
        insert objDoc2;
        
        GP_Project_Version_History__c projectVersionHistory1 = GPCommonTracker.getProjectVersionHistory(prjObj1.Id);
        projectVersionHistory1.GP_Status__c = 'Draft';
        insert projectVersionHistory1;

        GP_Project_Classification__c projectClassification1 = GPCommonTracker.getProjectClassification(prjObj1.Id);
        insert projectClassification1;
        
        GP_Project__c prjObj2 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj2.OwnerId = objuser.Id;
        prjObj2.GP_CRN_Number__c = iconMaster.Id;
        prjObj2.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj2.GP_Parent_Project__c = prjObj1.Id;
        prjObj2.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj2.GP_Approval_Status__c = 'Draft';
        prjObj2.GP_Oracle_PID__c = '1234';
        prjObj2.GP_Oracle_Status__c = 'S';
        prjObj2.GP_Create_Version_History__c = true;
        insert prjObj2;
        
        GP_Project_Document__c objDoc3 = GPCommonTracker.getProjectDocument(prjObj2.Id);
        insert objDoc3;
        
        GP_Project_Version_History__c projectVersionHistory2 = GPCommonTracker.getProjectVersionHistory(prjObj2.Id);
        projectVersionHistory2.GP_Status__c = 'Draft';
        insert projectVersionHistory2;

        GP_Project_Classification__c projectClassification2 = GPCommonTracker.getProjectClassification(prjObj2.Id);
        insert projectClassification2;
        
        List < GP_Project__c > records = new List < GP_Project__c >();
        records.add(prjObj1);
        records.add(prjObj );
        records.add(prjObj1);
        String strrecords = JSON.serialize(records);
        
        prjObj.GP_Approval_Status__c = 'Draft';
        map<id, Sobject> oldSObjectMap = new  map<id, Sobject>();
        oldSObjectMap.put(prjObj1.Id, prjObj1);
        oldSObjectMap.put(prjObj.Id, prjObj);
        oldSObjectMap.put(prjObj1.Id, prjObj1);
        String stroldSObjectMap = JSON.serialize(oldSObjectMap);
        GPServiceProjectVersionHistory.createApprovedProjectVersionHistory(stroldSObjectMap, strrecords);
        Database.executeBatch(new GPBatchToCreateVersionHistory(),1);

        
    }
    
    public static testMethod void validateJSONForProcessInstance(){
        /* No use of code untill blocker of this function "generateJSONForProcessInstance" call is resolved.
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c parentPrjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        parentPrjObj.OwnerId = objuser.Id;
        parentPrjObj.GP_Approval_Status__c = 'Draft';
        parentPrjObj.GP_Oracle_PID__c = '1234';
        parentPrjObj.GP_Oracle_Status__c = 'S';
        insert parentPrjObj ;
        
        Map <Id, ProcessInstance > mapOfProcessInstance = new Map <Id, ProcessInstance >();
        List<ProcessInstance> processInstances = [SELECT Id, (SELECT Id,ProcessNodeId,ProcessInstanceId,Comments,TargetObjectId FROM StepsAndWorkitems ) FROM ProcessInstance];
        for(ProcessInstance objProInstance : processInstances){
            mapOfProcessInstance.put(objProInstance.Id, objProInstance);
        }
        //"generateJSONForProcessInstance" is not visible
        //GPServiceProjectVersionHistory.generateJSONForProcessInstance(parentPrjObj, mapOfProcessInstance);
        */
    }

}