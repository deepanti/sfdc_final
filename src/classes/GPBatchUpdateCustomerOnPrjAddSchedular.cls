global class GPBatchUpdateCustomerOnPrjAddSchedular implements schedulable {
    global void execute(SchedulableContext sc) {
        GPBatchUpdateCustomeMasterOnPrjAddress updateCustomerOnAddress = new GPBatchUpdateCustomeMasterOnPrjAddress(); //ur batch class
        database.executebatch(updateCustomerOnAddress, 200);
    }
}