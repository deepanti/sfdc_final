/*This test class is used to test MyRedirectController Class
Version 1.0, Created By: Ankit Nagpal, Created date: 27th,July,2015, Deployment date: 31th, July,2015*/
@isTest
public class Test_MyRedirectController {
    public static testMethod void RunTest()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.Id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Contact oContact = GEN_Util_Test_Data.CreateContact('Ankit','Nagpal',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        
        //create opportunity with survey filled -survey already taken
        Test_MyRedirectController new1=new Test_MyRedirectController();
        Opportunity oOpportunity = new1.CreateOpportunity('Test',oAccount.Id,oContact.Id,'Genpact Sales Rep');
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c OOpportunityProduct=  new1.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,1200000);
        
        ApexPages.StandardController sc = new ApexPages.standardController(oOpportunity);
        MyRedirectController rc = new MyRedirectController(sc);
        Opportunity Opp = [SELECT Id From Opportunity where ID=:oOpportunity.Id];
        system.currentPageReference().getParameters().put('Id', Opp.Id);
        PageReference pg1 = rc.doRedirect();
        //test opportunity when survey is not yet taken
        oOpportunity.Survey_Name__c = '';
        update oOpportunity;
        PageReference pg2 = rc.doRedirect();         
    }
    
    //create Opportunity
    public Opportunity CreateOpportunity(String strName,Id AccountId,Id ContactId, String surveyName)
    {
        Opportunity oOpportunity = new Opportunity();
        oOpportunity.Name = strName;
        oOpportunity.AccountId = AccountId;
        oOpportunity.StageName = '1. Discover';
        oOpportunity.Survey_Name__c = surveyName;
        oOpportunity.Target_Source__c = 'Consulting Pull Through';
        oOpportunity.Opportunity_Origination__c='Proactive';
        oOpportunity.Sales_Region__c='Asia';
        oOpportunity.Sales_country__c = 'India';
        oOpportunity.Deal_Type__c = 'Sole Sourced';
        oOpportunity.Contract_type__c = 'MSA or over-arching agreement';
        oOpportunity.Deal_Administrator__c = 'Analyst/Advisory Firm';
        oOpportunity.CloseDate = System.today().adddays(1);
        oOpportunity.Revenue_Start_Date__c = System.today().adddays(1);
        oOpportunity.Contract_Term_in_months__c=36;
        oOpportunity.Revenue_Product__c=null;
        oOpportunity.CMITs_Check__c=False;
        oOpportunity.Advisor_Firm__c = 'Trestle';
        oOpportunity.Contact__c = ContactId;
        oOpportunity.Advisor_Name__c = 'Bernhard Janischowsky';
        oOpportunity.Annuity_Project__c = 'Project';
        oOpportunity.Deal_Type__c = 'Sole Sourced';
        //oOpportunity.Category_1__c = 'Relationships / Connections';
        //oOpportunity.Win_Loss_drop_reason_1__c = 'Cultural Alignment';
        //oOpportunity.Information_Sorce_1__c = 'Customer feedback - formal';
        oOpportunity.Win_Loss_Dropped_reason1__c='test';
        oOpportunity.Win_Loss_Dropped_reason2__c='test';
        oOpportunity.Win_Loss_Dropped_reason3__c='test';
        oOpportunity.SPOC_s__c='Nitesh Aggarwal';
        
        insert oOpportunity;
        return oOpportunity;
    }
    //Create the OpportunityProduct__c
    public OpportunityProduct__c CreateOpportunityProduct(Id OpportunityId,Id ProductId,integer intContractTerminmonths,integer DecTCV_Local)
    {
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c='1';
        oOpportunityProduct.Service_line_OLI__c='11';
        oOpportunityProduct.Product_Autonumber__c='OLI';
        oOpportunityProduct.Product_Family_OLI__c = 'Doc Management';
        oOpportunityProduct.Product__c = ProductId;
        oOpportunityProduct.COE__c = 'IMS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Auto';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.LocalCurrency__c = 'USD';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(1);
        oOpportunityProduct.ContractTerminmonths__c = intContractTerminmonths;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = OpportunityId;
        oOpportunityProduct.TCVLocal__c =DecTCV_Local;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
        return oOpportunityProduct;
    } 
}