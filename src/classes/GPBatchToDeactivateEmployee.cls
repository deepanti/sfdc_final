global class GPBatchToDeactivateEmployee implements  Database.Batchable<sObject>, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {        
        return Database.getQueryLocator([Select Id, Name, GP_EMPLOYEE_TYPE__c
                                         From GP_Employee_Master__c 
                                         where GP_ACTUAL_TERMINATION_Date__c <= :system.today() AND
                                         GP_isActive__c = true AND GP_EMPLOYEE_TYPE__c != 'Ex-Employee']) ;      
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Employee_Master__c> lstOfEmployees){
        if(lstOfEmployees != null && lstOfEmployees.size() > 0) {
            for(GP_Employee_Master__c employeeMaster : lstOfEmployees) {
                employeeMaster.GP_EMPLOYEE_TYPE__c = 'Ex-Employee';
            }
            update lstOfEmployees;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        
    }    
    
}