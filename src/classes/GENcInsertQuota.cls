/*-------------
        Class Description : Class is being used in trigger on User on Automatic create Quota for Current Year for user.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 4 June 2014  
        Last modified date: 14 August 2014
---------------*/
global class GENcInsertQuota {
    /*Used in a Apex Trigger*/
    @future
    public static void addQuotaheader(Set<Id> oUserId) 
    {
        addQuotaheadermeth(oUserId);
    }
    
    /*Used in a Batch Class*/
    public static void addQuotaheader1(Set<Id> oUserId) 
    {
       addQuotaheadermeth(oUserId);
    }
    
    public static void addQuotaheadermeth(Set<Id> oUserId)
    {
        Map<String, Quota_header__c> oQuotaHeaderMap = new Map<String, Quota_header__c>(); //Map to check Duplicacy
        
        List<Quota_header__c> oQuotaHeaderToInsert = new List<Quota_header__c>();   // list for upsertion
        
        for (Quota_header__c oQuotahdr:[Select id,Sales_Person__c,year__c From Quota_header__c where Sales_Person__c in:oUserId]) 
            oQuotaHeaderMap.put(oQuotahdr.Sales_Person__c+oQuotahdr.year__c,oQuotahdr);
        
        for (User oUsr: [Select id,Sales_Leader__c,profile.name,FirstName,Lastname from User where isActive = true AND id in :oUserId])
        {
            if(oQuotaHeaderMap.get(oUsr.id+String.valueof(System.today().year()))== null)
            {
                Quota_header__c oQH = new Quota_header__c();
                oQH.Sales_Person__c = oUsr.id;
                oQH.ownerid = oUsr.id;
                oQH.Name= oUsr.FirstName+ ' ' +oUsr.LastName;
                oQH.year__c=String.valueof(System.today().year());
                oQuotaHeaderToInsert.add(oQH);
            }
            else
             {
                Quota_header__c oQH = new Quota_header__c();
                oQH.id = oQuotaHeaderMap.get(oUsr.id+String.valueof(System.today().year())).id;
                oQH.Sales_Person__c = oUsr.id;
                oQH.Name= oUsr.FirstName+ ' ' +oUsr.LastName;
                oQH.ownerid = oUsr.id;
                oQH.year__c=String.valueof(System.today().year());
                oQuotaHeaderToInsert.add(oQH);
             }   
        }
        //upsert oQuotaHeaderToInsert;
        Database.UpsertResult[] sr = Database.upsert(oQuotaHeaderToInsert, false);
    }
}