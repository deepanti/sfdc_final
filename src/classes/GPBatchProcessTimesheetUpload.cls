/**
 * @group Mass Update/Upload. 
 *
 * @description Batch class for Mass Update Timesheet.
 */
global class GPBatchProcessTimesheetUpload implements Database.Batchable < sObject > {

    public static Map < String,
    List < GP_Timesheet_Entry__c >> mapOfTimeSheetTransactionIdVsTimeSheetEntries = new Map < String,
    List < GP_Timesheet_Entry__c >> ();
    public static Map < String,
    GP_Timesheet_Transaction__c > mapOfMonthYearVsTimeSheetTransaction = new Map < String,
    GP_Timesheet_Transaction__c > ();
    public static Map < String,
    GP_Temporary_Data__c > mapOfTemporaryData = new Map < String,
    GP_Temporary_Data__c > ();
    public static Map < String,
    GP_Employee_Master__c > mapOfEmployee = new Map < String,
    GP_Employee_Master__c > ();
    public static Map < String,
    GP_Project__c > mapOfProject = new Map < String,
    GP_Project__c > ();
    public static Map < String,
    GP_Project_Task__c > mapOfProjectTasks = new Map < String,
    GP_Project_Task__c > ();
    public static List < GP_Employee_Master__c > lstOfEmployee = new List < GP_Employee_Master__c > ();
    public static Set < String > setOfEmployeeMonthYears = new Set < String > ();
    public static Set < Id > setOfUploadTimeSheetEntriesIds = new Set < Id > ();
    public static Set < String > setOfMonthYears = new Set < String > ();
    public static Set < Id > setOfErrorIds = new Set < Id > ();
    public static Set < String > setOfProjectOraclePIds = new Set < String > ();
    public static Set < Id > setOfEmployeeIds = new Set < Id > ();
    public static Map < String,
    Set < String >> mapOfMonthYearVSAssociatedProjectIds = new Map < String,
    Set < String >> ();
    public static Set < String > setOfProjectIdsForApproval = new Set < String > ();
    public static Map < Id,
    List < GP_Timesheet_Entry__c >> mapOfTimeSheetTransactionIdVsTimesheetEntriesForApproval = new Map < Id,
    List < GP_Timesheet_Entry__c >> ();
    String jobId;

    /**
     * @description Constructor with jobId whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * 
     */
    public GPBatchProcessTimesheetUpload(String jobRecordId) {
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
    }

    /**
     * @description Batch start method.
     * @param BC : BatchableContext.
     * 
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {

        // Update Job Record with 'Pending' Status.
        updateJobRecord('In Progress', 0, '');

        String query = 'SELECT id, GP_Is_Failed__c,GP_TSC_Ex_Type__c, GP_TSC_Modified_Hours__c, GP_TSC_Project_Task__c, GP_TSC_Date__c,';
        query += 'GP_TSC_Transaction__c, GP_TSC_Unique_TimeSheet_Record__c, GP_Validation_Result__c,';
        query += 'GP_Project__c, GP_Employee__c, GP_TSC_Month_Year__c from GP_Temporary_Data__c where GP_Job_Id__c = \'' + jobId + '\'';

        return Database.getQueryLocator(Query);
    }

    /**
     * @description Batch execute method.
     * @param BC : BatchableContext.
     * @param listOfTemporaryTimeSheetEntries : list of temporary records that will be processed.
     * 
     */
    global void execute(Database.BatchableContext BC, List < Sobject > listOfTemporaryTimeSheetEntries) {
        Savepoint sp = Database.setSavepoint();

        // Create new and merge with old approved Timesheet records.
        List < GP_Timesheet_Entry__c > lstTimeSheetEntries = GPHelperTimesheet.getAllTimeSheetEntriesForBatch(listOfTemporaryTimeSheetEntries,
            mapOfEmployee,
            mapOfProject,
            mapOfProjectTasks,
            mapOfTemporaryData);

        getMapOfTemporaryData(listOfTemporaryTimeSheetEntries);

        // Query timesheet Transactions entered in Pinnacle.
        List < GP_Timesheet_Transaction__c > lstTimeSheetTransactionInDB = new GPSelectorTimesheetTransaction().selectTimeSheetTransactionRecordsForAMonthYear(setOfMonthYears);

        setMapOfMonthYearVsTimeSheetTransaction(lstTimeSheetTransactionInDB);
        filterTimeSheetEntriesWrtApprovalStatus(lstTimeSheetEntries);

        lstOfEmployee = mapOfEmployee.Values();

        //setProjectIdAndEmployeeIdSet(mapOfProject,mapOfEmployee);

        // Get Validated list of timesheet Entries Records.
        List < GP_Timesheet_Entry__c > lstOfValidatedTimeSheetEntries = GPServiceTimesheetTransaction.getValidatedTimeSheetEntriesLst(lstTimeSheetEntries,
            mapOfTemporaryData,
            setOfErrorIds,
            setOfEmployeeIds,
            setOfProjectOraclePIds,
            setOfEmployeeMonthYears);
        system.debug('lstOfValidatedTimeSheetEntries' + lstOfValidatedTimeSheetEntries);

        if (checkIfBatchNeedsToBeProcessedFurther()) {

            // Get timesheet Transaction to insert in pinnacle SFDC.
            List < GP_Timesheet_Transaction__c > lstTimeSheetTransactions = getTimeSheetTransactionLst(lstOfValidatedTimeSheetEntries);
            system.debug('lstTimeSheetTransactions' + lstTimeSheetTransactions);

            try {
                if (lstTimeSheetTransactions != null && lstTimeSheetTransactions.size() > 0) {
                    Database.upsert(lstTimeSheetTransactions, false);
                    setMapOfMonthYearVsTimeSheetTransaction(lstTimeSheetTransactions);
                }

                // Set lookup of insered timesheet transactions in timesheet entries.
                setTimeSheetTransactionToEntries(lstOfValidatedTimeSheetEntries);
                system.debug('lstOfValidatedTimeSheetEntries' + lstOfValidatedTimeSheetEntries);

                if (lstOfValidatedTimeSheetEntries.size() > 0) {
                    Database.upsert(lstOfValidatedTimeSheetEntries, false);
                    processRecordsForApproval();
                    //if (isForApproval) {
                    //    Boolean isApprovalRequired = GPCommon.getisApprovalRequired(setOfProjectIds,
                    //        GPCommon.mapOfMonthNameToNumber.get(monthYear[0]),
                    //        Integer.ValueOf(monthYear[1]));
                    //    if (isApprovalRequired) {
                    //        GP_Timesheet_Transaction__c timeSheetTransaction = GPCommon.processApproval(timeSheetEntriesToBeUpserted, timeSheetTransactionId);
                    //        upsert timeSheetTransaction;
                    //    }
                    //}
                }
                system.debug('mapOfTemporaryData' + mapOfTemporaryData);
                //Update Valiation results in tempoarary Records.
                update mapOfTemporaryData.values();

                // GP_Timesheet_Transaction__c timeSheetTransaction = GPCommon.processApproval(timeSheetEntriesToBeUpserted,timeSheetTransactionId);
                //upsert timeSheetTransaction;

                //Update Job Record depending upon the result of processing
                String status = setOfErrorIds.size() > 0 ? 'Failed' : 'Completed';
                updateJobRecord(status, setOfErrorIds.size(), '');
            } catch (Exception E) {
                Database.rollback(sp);
                updateJobRecord('Failed', listOfTemporaryTimeSheetEntries.size(), E.getMessage());
            }
        } else {
            update mapOfTemporaryData.values();
            String status = setOfErrorIds.size() > 0 ? 'Failed' : 'Completed';
            updateJobRecord(status, setOfErrorIds.size(), '');
        }
    }

    private static Boolean checkIfBatchNeedsToBeProcessedFurther() {
        for (GP_Temporary_Data__c tempData: mapOfTemporaryData.Values()) {
            if (tempData.GP_Validation_Result__c != '' && tempData.GP_Validation_Result__c != null) {
                return false;
            }
        }
        return true;
    }

    /**
     * @description Create temporary data map for uniquely identifying temporary records in order to set validation strings.
     * @param listOfTemporaryTimeSheetEntries : list of temporary records that will be processed.
     * 
     */
    public static void getMapOfTemporaryData(List < GP_Temporary_Data__c > listOfTemporaryTimeSheetEntries) {
        String key;

        for (GP_Temporary_Data__c objTemporaryTimeSheet: listOfTemporaryTimeSheetEntries) {
            //objTemporaryTimeSheet.GP_Validation_Result__c = '';
            key = objTemporaryTimeSheet.GP_Employee__c + '@@';
            key += objTemporaryTimeSheet.GP_Project__c + '@@';
            key += String.ValueOf(Date.ValueOf(objTemporaryTimeSheet.GP_TSC_Date__c)) + '@@' + objTemporaryTimeSheet.GP_TSC_Ex_Type__c;
            if (!mapOfEmployee.containsKey(objTemporaryTimeSheet.GP_Employee__c)){
                objTemporaryTimeSheet.GP_Validation_Result__c = 'Invalid Employee Number';
                objTemporaryTimeSheet.GP_Is_Failed__c = true;
            }
            else if (!mapOfProject.containsKey(objTemporaryTimeSheet.GP_Project__c)){
                objTemporaryTimeSheet.GP_Validation_Result__c = 'Invalid Project Number';
                objTemporaryTimeSheet.GP_Is_Failed__c = true;
            }
            else if (!mapOfProjectTasks.containsKey(objTemporaryTimeSheet.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheet.GP_Project__c)){
                objTemporaryTimeSheet.GP_Validation_Result__c = 'Invalid Project Task Number';
                objTemporaryTimeSheet.GP_Is_Failed__c = true;
            }
            else {
                setOfEmployeeMonthYears.add(objTemporaryTimeSheet.GP_Employee__c + objTemporaryTimeSheet.GP_TSC_Month_Year__c);
                setOfEmployeeIds.add(mapOfEmployee.get(objTemporaryTimeSheet.GP_Employee__c).Id);
                setOfProjectOraclePIds.add(mapOfProject.get(objTemporaryTimeSheet.GP_Project__c).GP_Oracle_PID__c);
                setOfUploadTimeSheetEntriesIds.add(objTemporaryTimeSheet.id);
                //String Key = objTemporaryTimeSheet.GP_Employee__c + '@@' + objTemporaryTimeSheet.GP_TSC_Date__c;
                if (objTemporaryTimeSheet.GP_TSC_Month_Year__c != null && objTemporaryTimeSheet.GP_TSC_Month_Year__c != '')
                    setOfMonthYears.add(objTemporaryTimeSheet.GP_TSC_Month_Year__c);
            }
            mapOfTemporaryData.put(key, objTemporaryTimeSheet);
            if (objTemporaryTimeSheet.GP_Validation_Result__c != '')
                setOfErrorIds.add(objTemporaryTimeSheet.id);
        }
    }

    /**
     * @description Batch finish method.
     * @param BC : BatchableContext.
     * 
     */
    global void finish(Database.BatchableContext BC) {
        //updateJobRecord('Completed', 0, '');
        System.debug('Success');

    }

    /**
     * @description Create timesheet Transaction list to insert in pinnacle SFDC.
     * @param listOfTimeSheetEntries : list of timesheet entry records whose timesheet transaction will be created.
     * @return List<GP_Timesheet_Transaction__c> : TimeSheet Transaction List to insert.
     * 
     */
    public static List < GP_Timesheet_Transaction__c > getTimeSheetTransactionLst(List < GP_Timesheet_Entry__c > listOfTimeSheetEntries) {
        List < GP_Timesheet_Transaction__c > lstTimeSheetTransactions = new List < GP_Timesheet_Transaction__c > ();
        GP_Timesheet_Transaction__c objTimeSheetTransaction;
        String monthYear, key;
        DateTime entryDate;

        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntries) {
            entryDate = objTimeSheetEntry.GP_Date__c;
            monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);
            key = objTimeSheetEntry.GP_Employee__r.Id + '@@' + monthYear;

            if (!mapOfMonthYearVsTimeSheetTransaction.containsKey(key)) {
                objTimeSheetTransaction = new GP_Timesheet_Transaction__c();
                objTimeSheetTransaction.GP_Employee__c = objTimeSheetEntry.GP_Employee__r.Id;
                objTimeSheetTransaction.GP_Month_Year__c = monthYear;

                mapOfMonthYearVsTimeSheetTransaction.put(key, objTimeSheetTransaction);
                lstTimeSheetTransactions.add(objTimeSheetTransaction);
            }

        }

        return lstTimeSheetTransactions;
    }

    /**
     * @description Create map of timesheet transaction with unique key as employee id + '@@" + monthyear.
     * @param lstTimeSheetTransactions : list of timesheet transacton records.
     * 
     */
    public static void setMapOfMonthYearVsTimeSheetTransaction(List < GP_Timesheet_Transaction__c > lstTimeSheetTransactions) {
        String key;
        for (GP_Timesheet_Transaction__c objTimeSheetTransaction: lstTimeSheetTransactions) {
            //if (objTimeSheetTransaction.GP_Approval_Status__c == 'Approved') 
            {
                key = objTimeSheetTransaction.GP_Employee__c + '@@' + objTimeSheetTransaction.GP_Month_Year__c;
                mapOfMonthYearVsTimeSheetTransaction.put(key, objTimeSheetTransaction);
            }
        }
    }

    /**
     * @description Update Approver Lookup on timesheet Trasaction.
     * @return List<GP_Timesheet_Transaction__c> : list of timesheet transacton records.
     * 
     */
    public static List < GP_Timesheet_Transaction__c > updateApproverIds() {
        List < GP_Timesheet_Transaction__c > listOfTimeSheetTransactions = new List < GP_Timesheet_Transaction__c > ();

        for (String timeSheetTransactionRecordId: mapOfTimeSheetTransactionIdVsTimeSheetEntries.keySet()) {
            listOfTimeSheetTransactions.add(GPCommon.processApproval(mapOfTimeSheetTransactionIdVsTimeSheetEntries.get(timeSheetTransactionRecordId),
                timeSheetTransactionRecordId));
        }

        return listOfTimeSheetTransactions;
    }

    /**
     * @description Filter the Temporary records that are locked in pinnacle SFDC.
     * @param listOfTimeSheetEntries : list of timesheet Entry records.
     * 
     */
    public static void filterTimeSheetEntriesWrtApprovalStatus(List < GP_Timesheet_Entry__c > listOfTimeSheetEntries) {
        GP_Timesheet_Entry__c timesheetEntry;
        String tempDataKey, monthYear, key;
        DateTime entryDate;
        Integer count = 0;
        //List<GP_Timesheet_Entry__c> listOfTimeSheetEntriesCopy = listOfTimeSheetEntries.clone();

        for (Integer i = listOfTimeSheetEntries.size() - 1; i >= 0; i--) {
            timesheetEntry = listOfTimeSheetEntries[i];
            entryDate = timesheetEntry.GP_Date__c;
            monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);
            key = timesheetEntry.GP_Employee__r.Id + '@@' + monthYear;

            if (mapOfMonthYearVsTimeSheetTransaction.containsKey(key) && mapOfMonthYearVsTimeSheetTransaction.get(key).GP_Approval_Status__c != 'Approved') {
                tempDataKey = timesheetEntry.GP_Employee__r.GP_Final_OHR__c + '@@' + timesheetEntry.GP_Project_Oracle_PID__c + '@@';
                tempDataKey += String.valueOf(Date.valueOf(timesheetEntry.GP_Date__c)) + '@@' + timesheetEntry.GP_Ex_Type__c;

                if (mapOfTemporaryData.containsKey(tempDataKey)) {
                    mapOfTemporaryData.get(tempDataKey).GP_Validation_Result__c += 'Timesheet is Locked cant be updated from Mass Update.';
                    mapOfTemporaryData.get(tempDataKey).GP_Is_Failed__c = true;
                    setOfErrorIds.add(mapOfTemporaryData.get(tempDataKey).id);
                }
                system.debug('mapOfTemporaryData' + mapOfTemporaryData);
                //listOfTimeSheetEntries.remove(i);
            }
        }
        /*for(GP_Timesheet_Entry__c timesheetEntry : listOfTimeSheetEntriesCopy){
            DateTime entryDate = timesheetEntry.GP_Date__c;
            String monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2,4);
            String key = timesheetEntry.GP_Employee__r.Id + '@@' + monthYear.toLowerCase();
            system.debug('key'+key);
            if(mapOfMonthYearVsTimeSheetTransaction.containsKey(key) && mapOfMonthYearVsTimeSheetTransaction.get(key).GP_Approval_Status__c == 'Draft')
            {
                String tempDataKey = timesheetEntry.GP_Employee__r.Id +'@@'+ timesheetEntry.GP_Project__r.Id+'@@'; 
                tempDataKey += String.valueOf(Date.valueOf(timesheetEntry.GP_Date__c)) +'@@'+ timesheetEntry.GP_Ex_Type__c;
                system.debug('tempDataKey'+tempDataKey);
                if(mapOfTemporaryData.containsKey(tempDataKey)){
                    mapOfTemporaryData.get(tempDataKey).GP_Validation_Result__c = 'Timesheet is Locked cant be updated from Mass Update.';
                }
                listOfTimeSheetEntries.remove(count);
                setOfErrorIds.add(mapOfTemporaryData.get(tempDataKey).id);
            }
            count++;
        } */
    }

    /**
     * @description Set lookup value in timesheet entry records.
     * @param listOfTimeSheetEntries : list of timesheet Entry records.
     * 
     */
    public static void setTimeSheetTransactionToEntries(List < GP_Timesheet_Entry__c > listOfTimeSheetEntries) {
        String monthYear, key;
        DateTime entryDate;

        //List<GP_Timesheet_Entry__c> listOfTimeSheetEntriesWithTransaction = new List<GP_Timesheet_Entry__c>();
        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntries) {
            entryDate = objTimeSheetEntry.GP_Date__c;
            monthYear = entryDate.format('MMM') + '-' + String.ValueOf(entryDate.year()).substring(2, 4);
            key = objTimeSheetEntry.GP_Employee__r.Id + '@@' + monthYear;

            objTimeSheetEntry.GP_Timesheet_Transaction__c = mapOfMonthYearVsTimeSheetTransaction.get(key).Id;
            if (!mapOfMonthYearVSAssociatedProjectIds.containsKey(monthYear))
                mapOfMonthYearVSAssociatedProjectIds.put(monthYear, new Set < String > ());
            mapOfMonthYearVSAssociatedProjectIds.get(monthYear).add(objTimeSheetEntry.GP_Project_Oracle_PID__c);
            setOfProjectIdsForApproval.add(objTimeSheetEntry.GP_Project_Oracle_PID__c);
            if (!mapOfTimeSheetTransactionIdVsTimesheetEntriesForApproval.containsKey(objTimeSheetEntry.GP_Timesheet_Transaction__c))
                mapOfTimeSheetTransactionIdVsTimesheetEntriesForApproval.put(objTimeSheetEntry.GP_Timesheet_Transaction__c, new List < GP_Timesheet_Entry__c > ());
            mapOfTimeSheetTransactionIdVsTimesheetEntriesForApproval.get(objTimeSheetEntry.GP_Timesheet_Transaction__c).add(objTimeSheetEntry);
            //listOfTimeSheetEntriesWithTransaction.add(objTimeSheetEntry);
        }
        //return listOfTimeSheetEntriesWithTransaction;
    }

    /*public static void setProjectIdAndEmployeeIdSet(Map<String,GP_Project__c> mapOfProject,Map<String,GP_Employee_Master__c> mapOfEmployee){
for(GP_Project__c objProject : mapOfProject.values()){
setOfProjectIds.add(objProject.Id);
}
for(GP_Employee_Master__c objEmployee : mapOfEmployee.values()){
setOfEmployeeIds.add(objEmployee.Id);
}
}*/

    /**
     * @description Update Job Record.
     * @param status : Job Status.
     * @param noOfUnsuccessfulRecords : Failed Records count.
     * @param exceptionString : Gloabl error in Job.
     * 
     */
    private void updateJobRecord(String status, Integer noOfUnsuccessfulRecords, String exceptionString) {
        GP_Job__c jobRecord = new GP_Job__c(Id = jobId);
        jobRecord.GP_Status__c = status;
        jobRecord.GP_No_of_Unsuccessful_Records__c = noOfUnsuccessfulRecords;
        jobRecord.GP_Error_Description__c = exceptionString;
        if (status == 'In Progress')
            jobRecord.GP_Job_Process_Started_On__c = system.now();
        else if (status == 'Completed')
            jobRecord.GP_Job_Process_Completed_On__c = system.now();
        update jobRecord;
    }

    /**
     * @description Process For Approval.
     */
    private void processRecordsForApproval() {
        Map < String, Boolean > mapOfAprovalRequired = GPCommon.getisApprovalRequiredBulk(mapOfMonthYearVSAssociatedProjectIds, setOfProjectIdsForApproval);
        GPCommon.processApprovalForBulk(mapOfTimeSheetTransactionIdVsTimesheetEntriesForApproval);
    }
}