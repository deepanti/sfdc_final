@isTest
public class ContactRoleRVTriggerHelperTest {
    public static Opportunity opp;
    public static Contact oContact;
    public static Contact oContact1;
    public static Contact_Role_RV__c contact_role_RV;
    public static User u;
    public static Account oAccount;
    public static OpportunityContactRole opprole;
    
   public static testMethod void insertContactRoleAndRevenueStormBarometerTest(){  
        setupTestData(false);
    }    
    
    public static testMethod void updateContactRoleForDiscoverOpportunityTest(){   
        setupTestData(false);
        System.runAs(u){
            Test.startTest();           
            Contact_Role_Rv__c contact_role = new Contact_role_RV__c();
            contact_role.Contact__c = oContact1.ID;
            contact_role.Opportunity__c = opp.ID;
            contact_role.IsPrimary__c = true;
            insert contact_role;   
            List<Contact_Role_RV__c> old_contact_role_RV = new List<Contact_Role_Rv__c>();
            old_contact_role_RV.add(contact_role);
           
             //contact_role.Contact__c = oContact.ID;
            contact_role.Role__c = 'Influencer';           
            update contact_role;
            
            Map<ID, Contact_Role_Rv__c> contact_role_map = new Map<ID, Contact_Role_Rv__c>();
            contact_role_map.put(contact_role.Id, contact_role);
            
            ContactRoleRVTriggerHelper.updateContactRoleAndRevenueStormBarometer(contact_role_map, old_contact_role_RV);
            Test.stopTest();
        }      	
    }
    
    public static testMethod void deleteContactRoleAndRevenueStormBarometerTest(){   
        setupTestData(false);
        System.runas(u){
             Contact_Role_Rv__c contact_role = [SELECT Id, Contact__c, role__c FROM Contact_Role_Rv__c WHERE Contact__c =:oContact.ID limit 1];
            delete contact_role;  
        }       
    }

    public static testMethod void checkDuplicateContacttest(){
        setupTestData(false);
        System.runAs(u){
            Test.startTest();           
            Contact_Role_Rv__c contact_role = new Contact_role_RV__c();
            contact_role.Contact__c = oContact1.ID;
            contact_role.Opportunity__c = opp.ID;
            contact_role.IsPrimary__c = false;
            insert contact_role;   
            List<Contact_Role_RV__c> contact_role_RV_list = new List<Contact_Role_Rv__c>();
           	contact_role_RV_list.add(contact_role);
            ContactRoleRVTriggerHelper.checkDuplicateContact(contact_role_RV_list);
        }
    }
    
     public static testMethod void restrictPrimaryContacttest(){
        setupTestData(false);
        System.runAs(u){
            Test.startTest();           
            Contact_Role_Rv__c contact_role = new Contact_role_RV__c();
            contact_role.Contact__c = oContact1.ID;
            contact_role.Opportunity__c = opp.ID;
            contact_role.IsPrimary__c = true;
            insert contact_role;   
            List<Contact_Role_RV__c> contact_role_RV_list = new List<Contact_Role_Rv__c>();
           	contact_role_RV_list.add(contact_role);
            ContactRoleRVTriggerHelper.restrictPrimaryContact(contact_role_RV_list);
        }
    }
    
     /*public static testMethod void OppcontactRoleIsExisttest(){
        setupTestData(false);
        System.runAs(u){
            Test.startTest();         
           List<OpportunityContactRole> contact_role_list = new List<OpportunityContactRole>();
           	contact_role_list.add(oppRole);
            ContactRoleRVTriggerHelper.OppcontactRoleIsExist(contact_role_list, oppRole);
        }
    }*/
    
    
    private static void setupTestData(Boolean is_discover_opportunity){
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        oContact1 = GEN_Util_Test_Data.CreateContact('test','contact',oAccount.Id,'test','Cross-Sell',
                                                    'test12@xyz.com','99999999990');
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        System.runAs(u){
            if(!is_discover_opportunity){
                Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Define Opportunity').getRecordTypeId();
                opp=new opportunity(name='1234',StageName='2. Define',CloseDate=system.today(), recordTypeId = recordTypeID,
                                    Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Competitor__c='Accenture',Deal_Type__c='Competitive',
                                   contact1__c =oContact1.ID, role__c = 'Other');
                insert opp;
                
                contact_role_RV = new Contact_role_RV__c();
                contact_role_RV.Contact__c = oContact.ID;
                contact_role_RV.Opportunity__c = opp.ID;
                contact_role_RV.IsPrimary__c = false;
                insert contact_Role_RV;   
                
                OpportunityContactRole opp_contact_role = new OpportunityContactRole();
                opp_contact_role.OpportunityId = opp.ID;
                opp_contact_role.ContactId = oContact.ID;
                opp_contact_role.IsPrimary = false;
                insert opp_contact_role;
                
                RevenueStormRB__RevenueStorm_Relationship_Barometer__c revenueStorm_barometer = 
                    ContactRoleRVTriggerHelper.getRevenueStrom_barometer_obj(new RevenueStormRB__RevenueStorm_Relationship_Barometer__c(), oContact);
                
                revenueStorm_barometer.RevenueStormRB__Contact__c = contact_role_RV.contact__c;
                revenueStorm_barometer.RevenueStormRB__Opportunity__c = contact_role_RV.Opportunity__c;
                insert revenueStorm_barometer;    
            }
            else{
            	Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
                opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(), recordTypeId = recordTypeID,
                                    Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Competitor__c='Accenture',
                                    Deal_Type__c='Competitive', contact1__c =oContact1.ID, role__c = 'other' );
                insert opp; 
                
                opprole = new OpportunityContactRole(opportunityID = opp.Id, contactId = oContact.ID,
                                                                           role= 'Other', isPrimary=true);
                insert opprole;
            }
        }      	
    }   
}