@isTest
public class RestrictOpportunityDeleteTest{

static testMethod void Test1()
{
ID PID= userinfo.getProfileId();
    //Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId =PID, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser112016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
            insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id,Inactive__c=false,Business_Group__c='GE');
        insert accountobject;
      
       
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
         insert opp;
try{            

                delete opp;
                
                                  
            }catch(Exception e) {
                system.assert(e.getMessage().contains('You cannot delete an Opportunity.Please contact your Salesforce.com Administrator for assistance.'));
            }
  }
  
  static testMethod void Test2()
{
ID PID= '00e9000000125i2';
    //Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId =PID, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser112016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
            insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id,Inactive__c=false,Business_Group__c='GE');
        insert accountobject;
      
       
        opportunity opp1=new opportunity(name='456',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
         insert opp1;
try{            

                delete opp1;
                
                                  
            }catch(Exception e) {
                system.assert(e.getMessage().contains(''));
            }
  }

  }