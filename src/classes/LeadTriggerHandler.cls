/********************************************************************************************************
Name           : LeadTriggerHandler
Created By     : Ryan Macias (The Pedowitz Group)
Created Date   : February 5, 2015
Description    : Class that handles all events from the universal Lead Trigger
: including lead assignment when lead status hits MQL 

Modified BY    : Abhishek Maurya
Modified Date  : 9/19/2018
Revisions      : 
*********************************************************************************************************/

public with sharing class LeadTriggerHandler {
    
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    public LeadTriggerHandler()
    {
        
    }
    public LeadTriggerHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
    public void OnBeforeInsert(Lead[] newLeads){
        // EXECUTE BEFORE INSERT LOGIC
        //Modified by Abhishek Maurya
        //this code is use to update standard status field value to custom status field
        //Loop through all the Lead record and set the field values
        for(Lead ld: newLeads)
        {
            if(ld.Lifecycle_Status__c=='Disqualified')
            {
                ld.Status=ld.Lifecycle_Status__c;
                ld.Disqualified_Reason__c=ld.Disqualified_ReasonLS__c; 
            }
            else
            {  
                
                ld.Status=ld.Lifecycle_Status__c;
                ld.Disqualified_Reason__c=''; 
                
            }
            accountToLeadFieldUpdate(newLeads);
        }
        
    }                           
    
    
    public void OnAfterInsert(Lead[] newLeads){
        // EXECUTE AFTER INSERT LOGIC
    }
    
    public void OnBeforeUpdate(Lead[] oldLeads, Lead[] updatedLeads, Map<ID, Lead> leadsNewMap, Map<ID, Lead> leadsOldMap){
        
        // List to store leads to process
        // Lead[] LeadUpdateList = new List<Lead>();
        
        // Loop through leads and Check if the status has changed to MQL
        for(Lead l : updatedLeads){
            Lead oldLead = leadsOldMap.get(l.Id);
            
            /*  if (l.Status == 'Marketing Qualified' && l.Account__c != null) {
LeadUpdateList.add(l);
}*/
            
            //check if status field change or not(If changed proceed further and update the statu field to status)
            if(l.Status!=oldLead.status||l.Disqualified_Reason__c!=oldLead.Disqualified_Reason__c)
            {
                if(l.status=='Disqualified')
                {
                    l.Lifecycle_Status__c=l.Status;
                    l.Disqualified_ReasonLS__c=l.Disqualified_Reason__c; 
                }
                else
                {   
                    
                    
                    l.Lifecycle_Status__c=l.Status;
                    l.Disqualified_ReasonLS__c=''; 
                    
                }
                
            }
            //check if status field change or not(If changed proceed further and update the statu__C field to status)
            if(l.Lifecycle_Status__c!=oldLead.Lifecycle_Status__c||l.Disqualified_ReasonLS__c!=oldLead.Disqualified_ReasonLS__c)
            {
                if(l.Lifecycle_Status__c=='Disqualified')
                {
                    l.Status=l.Lifecycle_Status__c;
                    l.Disqualified_Reason__c=l.Disqualified_ReasonLS__c; 
                }
                else
                {   
                    
                    
                    l.status=l.Lifecycle_Status__c;
                    l.Disqualified_Reason__c=''; 
                    
                }
            } 
        }
        accountToLeadFieldUpdate(updatedLeads,leadsOldMap);
        // system.debug('Leads in List = ' + LeadUpdateList);
        // Check for Leads to process 
        /* if (LeadUpdateList.size()>0){
assignLeadOwner(LeadUpdateList);
}*/
    }
    
    public void OnAfterUpdate(Lead[] oldLeads, Lead[] updatedLeads, Map<ID, Lead> leadMap){
        // AFTER UPDATE LOGIC
    }
    
    public void OnBeforeDelete(Lead[] leadsToDelete, Map<ID, Lead> leadMap){
        // BEFORE DELETE LOGIC
    }
    
    public void OnAfterDelete(Lead[] deletedLeads, Map<ID, Lead> leadMap){
        // AFTER DELETE LOGIC
    }
    
    public void OnUndelete(Lead[] restoredLeads){
        // AFTER UNDELETE LOGIC
    }
    
    /* private void assignLeadOwner(Lead[] leadsToAssign) {

//String to store leads to process
List<id> AccountIDs=new list<id>();

// Loop through leads and check logic
for(Lead l : leadsToAssign){
AccountIDs.add(l.Account__c);
}

//Create a list of Accounts from query results matching lead website
List<Account> accounts=[Select Id, OwnerId FROM Account WHERE Id IN: AccountIDs];

//Create our map to store IDs
Map<Id, Id> acctOwner=new Map<Id, Id>();

for(Account a:accounts){
acctOwner.put(a.Id, a.OwnerId);
}

// Loop through Leads and assign owner
for(Lead leads : leadsToAssign){
leads.ownerId=acctOwner.get(leads.Account__c);
}
}*/
     //this method used to update field from Account object to Lead (Archtype__c and vertical__C) 
    public void accountToLeadFieldUpdate(Lead[] newLead)
    {
        
        try{ for(Lead ld:newLead)
        {
            if(ld.Account__c!=NULL)
            {
                ld.Archetype__c=ld.FORMULA_AccountArchetype__c;
                ld.Vertical__c=ld.AccountIndustryVertical__c;
            }
        }}
        catch(Exception e)
        {
            system.debug('...Line Number..'+String.valueOf(e.getLineNumber())+'...Error Message...'+e.getMessage());
        }
        
    }
     //this method used to update field from Account object to Lead (Archtype__c and vertical__C) 
    public void accountToLeadFieldUpdate(Lead[] newLead,Map<ID, Lead> leadsOldMap)
    {
       
        try
        {
            for(Lead ld:newLead)
            {
                Lead oldLead = leadsOldMap.get(ld.Id);
                if(ld.Account__c!=NULL&&ld.Account__c!=oldLead.Account__c)
                {
                    system.debug('..field change is working fine.. Line 178..');
                    ld.Archetype__c=ld.FORMULA_AccountArchetype__c;
                    ld.Vertical__c=ld.AccountIndustryVertical__c;
                    
                }
                else if(oldLead.Account__c==NULL && ld.Account__c!=NULL)
                {
                    ld.Archetype__c=ld.FORMULA_AccountArchetype__c;
                    ld.Vertical__c=ld.AccountIndustryVertical__c;   
                }
                else if(oldLead.Account__c!=NULL && ld.Account__c==NULL)
                {
                    ld.Archetype__c='';
                    ld.Vertical__c='';  
                }
                else 
                {
                    system.debug('No field Assingment has been done');
                }
            }
        }
        catch(Exception e)
        {
            system.debug('...Line Number..'+String.valueOf(e.getLineNumber())+'...Error Message...'+e.getMessage());
        }
        
    }
}