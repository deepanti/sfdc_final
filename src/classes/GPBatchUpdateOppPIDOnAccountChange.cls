/*
 * Created By    : Dhruv Singh.
 * Creation Date : 14-02-2020.
 * CR No         : 
 * Subject       : Flow mode to batch mode for Opp PID update on Account updation.
 * Description   : Replace Opp PID updation on process builder named 'GPUpdateDealonAccount' with this batch.
 * 				   Before 101 SOQL limit use to reach on account update via process builder due to multiple 
 * 				   project classification records.
 * */
// BPH Change
global class GPBatchUpdateOppPIDOnAccountChange implements Database.Batchable<SOBJECT>, schedulable {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        Integer ndays = Integer.valueOf(Label.GP_Last_N_Days_for_Account_Update);
        Date lastNDays = Date.today();
        if(ndays > 0) {
            lastNDays = lastNDays.addDays(-ndays);
        }        
        return Database.getQueryLocator([SELECT id, Opportunity_ID__c,
                                         Account.Business_Group__c, Account.Business_Segment__c, 
                                         Account.Business_Segment__r.Name, Account.Sub_Business__c, 
                                         Account.Sub_Business__r.Name, Account.Industry_Vertical__c, 
                                         Account.Sub_Industry_Vertical__c 
                                         FROM Opportunity 
                                         WHERE AccountId != null
                                         AND (Account.LastModifiedDate >=: lastNDays 
                                              OR Account.Business_Segment__r.LastModifiedDate >=: lastNDays
                                              OR Account.Sub_Business__r.LastModifiedDate >=: lastNDays)
                                         AND Account.Inactive__c = false 
                                         AND Account.Is_This_A_Partner_Account__c != 'Yes' 
                                         AND Account.Business_Group__c != 'Genpact'
                                         AND StageName != '7. Lost' 
                                         AND StageName != '8. Dropped']);
    }
    
    global void execute(SchedulableContext sc) {
        Database.executebatch(new GPBatchUpdateOppPIDOnAccountChange(), 1);
    }
    
    global void execute(Database.BatchableContext bc, List<SOBJECT> lstOfOpps) {
        
        try {
            Set<String> setofOppIds = new Set<String>();
            Map<String,List<GP_Opportunity_Project__c>> mapOfOppIdWithOppPIds = 
                new Map<String,List<GP_Opportunity_Project__c>>();            
            List<GP_Opportunity_Project__c> lstOfOppProjects = new List<GP_Opportunity_Project__c>();
            
            for(Opportunity opp : (List<Opportunity>) lstOfOpps) {
                setofOppIds.add(opp.Opportunity_ID__c);
            }
            
            for(GP_Opportunity_Project__c oppPid : [SELECT id, GP_Business_Group_L1__c, GP_Business_Segment_L2__c, 
                                                    GP_Business_Segment_L2_Id__c, GP_Sub_Business_L3__c, 
                                                    GP_Sub_Business_L3_Id__c, GP_Vertical__c, GP_Sub_Vertical__c,
                                                    GP_Opportunity_Id__c
                                                    FROM GP_Opportunity_Project__c 
                                                    WHERE GP_Opportunity_Id__c in : setofOppIds
                                                    AND GP_StageName__c != '7. Lost' 
                                                    AND GP_StageName__c != '8. Dropped']) 
            {
                if(!mapOfOppIdWithOppPIds.containsKey(oppPid.GP_Opportunity_Id__c)) {
                    mapOfOppIdWithOppPIds.put(oppPid.GP_Opportunity_Id__c, new List<GP_Opportunity_Project__c>());
                }
                mapOfOppIdWithOppPIds.get(oppPid.GP_Opportunity_Id__c).add(oppPid);
            }
            
            for(Opportunity opp : (List<Opportunity>) lstOfOpps) {
                if(mapOfOppIdWithOppPIds.containsKey(opp.Opportunity_ID__c) 
                   && mapOfOppIdWithOppPIds.get(opp.Opportunity_ID__c) != null) {
                    for(GP_Opportunity_Project__c oPID : mapOfOppIdWithOppPIds.get(opp.Opportunity_ID__c)) {
                        if(isOppPIDDirty(opp, oPID)) {
                            oPID.GP_Business_Group_L1__c = opp.Account.Business_Group__c;
                            oPID.GP_Business_Segment_L2__c = opp.Account.Business_Segment__r.Name;
                            oPID.GP_Business_Segment_L2_Id__c = opp.Account.Business_Segment__c;
                            oPID.GP_Sub_Business_L3__c = opp.Account.Sub_Business__r.Name;
                            oPID.GP_Sub_Business_L3_Id__c = opp.Account.Business_Segment__c;
                            oPID.GP_Vertical__c = opp.Account.Industry_Vertical__c;
                            oPID.GP_Sub_Vertical__c = opp.Account.Sub_Industry_Vertical__c;
                            lstOfOppProjects.add(oPID);
                        }
                    }
                }
            }
            
            if(lstOfOppProjects.size() > 0) {
                update lstOfOppProjects;
            }
        } catch(Exception ex) {
            GPErrorLogUtility.logError('GPBatchUpsertOppPIDOnAccountChange', 'execute',ex, ex.getMessage(), 
                                       null, null, null, null, ex.getStackTraceString());
        }
    }
    
    global void finish(Database.BatchableContext bc) {}
    
    private boolean isOppPIDDirty(Opportunity opp, GP_Opportunity_Project__c oPID) {
        Boolean isChanged = false;
        if(oPID.GP_Business_Group_L1__c != opp.Account.Business_Group__c) {
            isChanged = true;
        }
        if(oPID.GP_Business_Segment_L2__c != opp.Account.Business_Segment__r.Name) {
            isChanged = true;
        }
        if(oPID.GP_Business_Segment_L2_Id__c != opp.Account.Business_Segment__c) {
            isChanged = true;
        }
        if(oPID.GP_Sub_Business_L3__c != opp.Account.Sub_Business__r.Name) {
            isChanged = true;
        }
        if(oPID.GP_Sub_Business_L3_Id__c != opp.Account.Business_Segment__c) {
            isChanged = true;
        }
        if(oPID.GP_Vertical__c != opp.Account.Industry_Vertical__c) {
            isChanged = true;
        }
        if(oPID.GP_Sub_Vertical__c != opp.Account.Sub_Industry_Vertical__c) {
            isChanged = true;
        }        
        return isChanged;
    }
}