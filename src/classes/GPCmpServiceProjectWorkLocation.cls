/**
* @author Anmol.kumar
* @date 07/09/2017
*
* @group ProjecWorklocation
* @group-content ../../ApexDocContent/ProjecWorklocation.htm
*
* @description Apex Service for project work location module,
* has aura enabled method to fetch, update/create and delete Project Work Location data.
*/
public class GPCmpServiceProjectWorkLocation {

    /**
    * @description Returns Project worklocation data for a given project Id
    * @param projectId Salesforce 18/15 digit Id of project
    * 
    * @return GPAuraResponse json of Project worklocation data
    * 
    * @example
    * GPCmpServiceProjectWorkLocation.getWorklocationData(<18 or 15 digit project Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getWorklocationData(Id projectId) {
        GPControllerProjectWorkLocation workLocationController = new GPControllerProjectWorkLocation(projectId);
        return workLocationController.getWorklocation();
    }
    
    /**
    * @description Saves project worklocation data
    * @param strListOfWorklocation serialized list of project worklocation records to be inserted/updated
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully inserted
    * 
    * @example
    * GPCmpServiceProjectWorkLocation.saveWorklocationData(<Serialized list of project worklocation record>);
    */
    // HSN changes
    @AuraEnabled
    public static GPAuraResponse saveWorklocationData(String strListOfWorklocation, String hsnCatId, String hsnCatName, Boolean isHSNNeeded) {
        List<GP_Project_Work_Location_SDO__c> listOfWorkLocation = (List<GP_Project_Work_Location_SDO__c>) JSON.deserialize(strListOfWorklocation, List<GP_Project_Work_Location_SDO__c>.class);
         		
        GPControllerProjectWorkLocation workLocationController = new GPControllerProjectWorkLocation(listOfWorkLocation, hsnCatId, hsnCatName, isHSNNeeded);
        return workLocationController.upsertWorklocation();
    }
    
    @AuraEnabled
    public static GPAuraResponse updateWorklocationData(String strListOfWorklocationToBeSaved, String strListOfWorklocationToBeDeleted) {
        List<GP_Project_Work_Location_SDO__c> listOfWorkLocationToBeDeleted = (List<GP_Project_Work_Location_SDO__c>) JSON.deserialize(strListOfWorklocationToBeDeleted, List<GP_Project_Work_Location_SDO__c>.class);
        List<GP_Project_Work_Location_SDO__c> listOfWorkLocationToBeSaved = (List<GP_Project_Work_Location_SDO__c>) JSON.deserialize(strListOfWorklocationToBeSaved, List<GP_Project_Work_Location_SDO__c>.class);
        GPControllerProjectWorkLocation workLocationController = new GPControllerProjectWorkLocation(listOfWorkLocationToBeSaved, listOfWorkLocationToBeDeleted);
        return workLocationController.upsertWorklocation();
    }
    
    /**
    * @description Deletes Project worklocation data for a given projectWorkLocationId Id
    * @param projectWorkLocationId Salesforce 18/15 digit Id of project worklocation
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully deleted
    * 
    * @example
    * GPCmpServiceProjectWorkLocation.deleteProjectWorkLocation(<18 or 15 digit project worklocation Id>);
    */
    // HSN changes
    @AuraEnabled
    public static GPAuraResponse deleteProjectWorkLocation(Id projectWorkLocationId, Boolean deleteHSNDetails) {
        GPControllerProjectWorkLocation workLocationController = new GPControllerProjectWorkLocation();
        return workLocationController.deleteProjectWorkLocation(projectWorkLocationId, deleteHSNDetails);
    }

    /**
    * @description returns related data for worklocation to be displayed as read only fields in UI
    * @param workLocationId Salesforce 18/15 digit Id of worklocation master
    * 
    * @return GPAuraResponse json of Worklocation record
    * 
    * @example
    * GPCmpServiceProjectWorkLocation.getRelatedDataForWorkLocation(<18 or 15 digit worklocation Id>);
    */
    @AuraEnabled
    public static GPAuraResponse getRelatedDataForWorkLocation(Id workLocationId) {
        GPControllerProjectWorkLocation workLocationController = new GPControllerProjectWorkLocation();
        return workLocationController.getRelatedDataForWorkLocation(workLocationId);
    }
}