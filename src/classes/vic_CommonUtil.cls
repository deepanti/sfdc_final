/**
 * @author: Shivam Singh
 * @date: March 2018
 */
public class vic_CommonUtil {
    static String strGRM = 'GRM';
    static String strBD = 'BD';
    static String strITO = 'ITO';
    static String strEnterpriseSales = 'Enterprise_sales';
    //static String strOppStage = 'Opportunity_Eligible_Stage_For_VIC';
    static String strITService = 'IT_Services';
    static String strNONITService = 'Non_IT_Services';
    public static String strOpportunityStage;
    public static VIC_Process_Information__c processInfo;
    @TestVisible
    public static Map < String, vic_Incentive_Constant__mdt > mapValueToObjIncentiveConst = new Map < String, vic_Incentive_Constant__mdt > ();
    public Map < String, vic_Incentive_Constant__mdt > mapDevNameToObjIncentiveConst = new Map < String, vic_Incentive_Constant__mdt > ();
    /*
    static{
        for(vic_Incentive_Constant__mdt objDTS : [SELECT DeveloperName,Label,vic_Value__c FROM vic_Incentive_Constant__mdt]){
            mapValueToObjIncentiveConst.put(objDTS.DeveloperName, objDTS);
        }
        if(mapValueToObjIncentiveConst.containsKey(strOppStage)){
            strOpportunityStage = mapValueToObjIncentiveConst.get(strOppStage).vic_Value__c;
        }
        //initSplitPercentage();
    }
     public vic_CommonUtil(){
         for(vic_Incentive_Constant__mdt objDTS : [SELECT DeveloperName,Label,vic_Value__c FROM vic_Incentive_Constant__mdt]){
            mapDevNameToObjIncentiveConst.put(objDTS.DeveloperName, objDTS);
        }
     }
     */
    /*
     * @Description: It will map the key to split percentage
     * @author: Vikas Rajput
     * @Param: NAN
     */
    public Map < String, VIC_Split_Percentage__mdt > initSplitPercentage() {
        Map < String, VIC_Split_Percentage__mdt > mapKeyToSplitPercent = new Map < String, VIC_Split_Percentage__mdt > ();
        for (VIC_Split_Percentage__mdt objSplitPercentITR: [SELECT DeveloperName, Label, vic_Split_Value__c FROM VIC_Split_Percentage__mdt]) {
            mapKeyToSplitPercent.put(objSplitPercentITR.DeveloperName, objSplitPercentITR);
        }
        return mapKeyToSplitPercent;
    }

    /**
     * @author: Shivam Singh
     * @date: March 2018
     */
    public static List < String > getFinancialYearForDisplay() {
        List < String > lstFinancialYear = new List < String > ();
        Integer currentYear = System.Today().year();
        Integer previousYear = currentYear - 1;
        lstFinancialYear.add(String.valueof(currentYear));
        lstFinancialYear.add(String.valueof(previousYear));
        lstFinancialYear.sort();
        return lstFinancialYear;
    }

    /**
     * @author: Shivam Singh
     * @date: March 2018
     */
    public static list < String > getYearForDisplay() {
        list < String > financialYearValueList = new list < String > ();
        Date currentDate = system.today();
        Date twoYearAfterCurrentDate = currentDate.addYears(2);
        Date oneYearAfterCurrentDate = currentDate.addYears(1);
        Date oneYearBeforeCurrentDate = currentDate.addYears(-1);
        financialYearValueList.add(string.valueof((currentDate.addYears(-1)).year()));
        financialYearValueList.add(string.valueof(currentDate.year()));
        financialYearValueList.add(string.valueof(oneYearAfterCurrentDate.year()));
        financialYearValueList.add(string.valueof(twoYearAfterCurrentDate.year()));
        //financialYearValueList.sort();
        return financialYearValueList;
    }
    /**
     * @author: Bashim Khan
     * @date: May 2018
     */
    public static string getPlanCodeOfUser(Id usrId) {
        VIC_Role__c vicrole = [select Plan__r.vic_Plan_Code__c from VIC_Role__c
            where Master_VIC_Role__c IN(select Master_VIC_Role__c from User_VIC_Role__c where vic_For_Previous_Year__c = false and User__r.isactive = true and User__c =: usrId) limit 1
        ];
        return (vicrole == null ? '' : vicrole.Plan__r.vic_Plan_Code__c);
    }
    /**
     * @author: Bashim Khan
     * @date: May 2018
     */
    public static List < Plan_Component__c > getPlanComponentsOfUserCategoryBased(Id usrId, String componentCategory) {
        List < Plan_Component__c > lstPlanComponents = new List < Plan_Component__c > ();
        VIC_Role__c vicrole = [select Plan__c, Plan__r.vic_Plan_Code__c from VIC_Role__c
            where Master_VIC_Role__c IN(select Master_VIC_Role__c from User_VIC_Role__c where vic_For_Previous_Year__c = false and User__r.isactive = true and User__c =: usrId) limit 1
        ];
        if (vicrole != null) {
            lstPlanComponents = [select id, Master_Plan_Component__c, Plan__c, Weightage_Applicable__c from Plan_Component__c
                where Plan__c =: vicrole.Plan__c and
                Master_Plan_Component__r.VIC_Component_Category__c =: componentCategory
            ];
        }
        return lstPlanComponents;
    }


    public static List < Plan_Component__c > getPlanComponentsOfUser(Id usrId) {
        List < Plan_Component__c > lstPlanComponents = new List < Plan_Component__c > ();
        VIC_Role__c vicrole = [select Plan__c, Plan__r.vic_Plan_Code__c from VIC_Role__c
            where Master_VIC_Role__c IN(select Master_VIC_Role__c from User_VIC_Role__c where vic_For_Previous_Year__c = false and User__r.isactive = true and User__c =: usrId) limit 1
        ];
        if (vicrole != null) {
            lstPlanComponents = [select id, Master_Plan_Component__c, Plan__c, Weightage_Applicable__c from Plan_Component__c
                where Plan__c =: vicrole.Plan__c
            ];
        }
        return lstPlanComponents;
    }

    /**
     * @author: Bashim Khan
     * @date: May 2018
     */

    public static string getCurrentYear() {

        return String.valueOf(System.Today().year());
    }

    /**
     * @Description: For any changes in this function, Need to do same change in this function also name as "getPreCondtionForOLI" 
     * @author: Bashim Khan
     * @date: May 2018
     */
    public static boolean getPreCondtionForOLI(OpportunityLineItem oli) {
        return (oli.vic_Final_Data_Received_From_CPQ__c && oli.Opportunity.StageName == strOpportunityStage);
    }

    /*
     * @Description: It will be use in VIC_OpportunityLineItemTriggerHandler for identifying eligible Opportunity Line Item
     * @author: Vikas Rajput
     * @Param: OpportunityLineItem , Opportunity (That belong to same opporutnity lime item)
     */
    public static boolean getPreCondtionForOLI(OpportunityLineItem oli, Opportunity objOppArg, String strOppStage) {
        return (oli.vic_Final_Data_Received_From_CPQ__c && strOppStage.equals(objOppArg.StageName));
    }
    /*
        @Description: Fetching Incentive record by updated status;
        @Author: Vikas Rajput
    */
    public static List<Target_Achievement__c> fetchAllIncetiveByChangeStatus(List<Id> lstIncentiveId, String strStatus){
        List<Target_Achievement__c> lstIncentive = [SELECT Id,vic_Incentive_Type__c,
                                                    Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,
                                                    vic_Status__c,Achievement_Date__c FROM Target_Achievement__c WHERE 
                                                    Id IN:lstIncentiveId];
        for(Target_Achievement__c  eachIncetive : lstIncentive){
            eachIncetive.vic_Status__c = strStatus;
        }
        return lstIncentive;
    }

    /*
        @Description: Fetching and init the Incentive record;
        @Author: Vikas Rajput
    */
    public static Target_Achievement__c initTargetIncentivRecOBJ(Target_Component__c objTargetComponent, Decimal dcmBonousAmt, String strIncentiveTypeARG, String strStatus) {
        Target_Achievement__c objTargetAchievm = new Target_Achievement__c();
        objTargetAchievm.Bonus_Amount__c = dcmBonousAmt;
        objTargetAchievm.Achievement_Date__c = System.Today();
        objTargetAchievm.vic_Incentive_Type__c = strIncentiveTypeARG;
        objTargetAchievm.vic_Incentive_Details__c = fetchIncentiveDetails(dcmBonousAmt, objTargetComponent.VIC_Total_Achievement_Scoreboard__c);
        objTargetAchievm.vic_Status__c = strStatus;
        objTargetAchievm.Target_Component__c = objTargetComponent.Id;
        return objTargetAchievm;
    }

    //Creating  incentive note here
    public static string fetchIncentiveDetails(Decimal calculatedPayoutIncentive, Decimal totalIncentive) {
        if (calculatedPayoutIncentive != null && totalIncentive != null) {
            return 'PayoutIncentive = ' + calculatedPayoutIncentive.setScale(2) + 'TotalIncentive = ' + totalIncentive.setScale(2) + ' Remaining = ' + (calculatedPayoutIncentive - totalIncentive).setScale(2);
        }
        return null;
    }

    /*
        @Author: Rishabh Pilani
        @Description: Use to get VIC Process custom setting information.
    */
    public static VIC_Process_Information__c getVICProcessInfo() {
        VIC_Process_Information__c eParams = VIC_Process_Information__c.getInstance();
        if (eParams == null) {
            eParams = VIC_Process_Information__c.getOrgDefaults();
        }
        return eParams;
    }

    /*
        Author: Rishabh Pilani
        Description: Used to get user vic role mapping in map
    */
    public static Map < String, String > getUserVICRoleNameinMap() {
        Map < String, String > mapUserVicRoles = new Map < String, String > ();
        For(User_VIC_Role__c v: [SELECT Id, Master_VIC_Role__c, User__r.Id, Master_VIC_Role__r.Name
            FROM User_VIC_Role__c
            WHERE vic_For_Previous_Year__c = false
            AND User__r.isActive = true
            AND Not_Applicable_for_VIC__c = false
        ]) {
            mapUserVicRoles.put(v.User__r.Id, v.Master_VIC_Role__r.Name);
        }
        return mapUserVicRoles;
    }

    /*
        Author: Rishabh Pilani
        Description: Used to get Local curency ISO code in map
    */
    public static Map < String, String > getDomicileToISOCodeMap() {
        Map < String, String > mapDomicileToISO = new Map < String, String > ();
        for (Country_ISO_Code__mdt currencyCode: [SELECT Label, DeveloperName, vic_ISO_Code__c
                FROM Country_ISO_Code__mdt
            ]){
            mapDomicileToISO.put(currencyCode.Label, currencyCode.vic_ISO_Code__c);
        }
        return mapDomicileToISO;
    }
    

    public static Case createCase(String status, String priority, Id caseAssignedTo, String description, ID idOfOLI, ID idOfOpportunity) {
        ID RecordTypeIdCase = Schema.SObjectType.case.getRecordTypeInfosByName().get('VIC Cases').getRecordTypeId();
        Case caseRaised = new Case();
        caseRaised.RecordTypeId = RecordTypeIdCase;
        caseRaised.Status = status;
        caseRaised.Priority = priority;
        caseRaised.VIC_Case_Created_For__c = caseAssignedTo;
        //caseRaised.VIC_Case_Created_By__c = UserInfo.getUserId();
        caseRaised.Subject = 'SL - VIC TEAM QUERY';
        caseRaised.Description = description;
        caseRaised.Opportunity__c = idOfOpportunity;
        caseRaised.Opportunity_Product_ID__c = idOfOLI;
        return caseRaised;
    }
    
    public static Date CheckDates(integer n) {
          date tddate = system.today();
          date ddate = tddate.adddays(n);
          integer d = tddate.daysBetween(ddate);
          integer mod = math.mod(d, 7);
          date AuSwdt;
          if (mod == 3 || mod == 4 || mod == 5 || mod == 6){
              AuSwdt = system.today().adddays(n + 2);
            } 
          else{
           AuSwdt = system.today().adddays(n);    
          }
          return AuSwdt;
     }
    
     /*
        Author: Rishabh Pilani
        Description: Used to get user vic role mapping in map
    */
    public static Map <Id, User_VIC_Role__c> getUserVICRoleinMap() {
        Map <Id, User_VIC_Role__c> mapUserVicRoles = new Map <Id, User_VIC_Role__c> ();
        For(User_VIC_Role__c v: [SELECT Id, Master_VIC_Role__c, User__r.Id, Master_VIC_Role__r.Name,
            Master_VIC_Role__r.Role__c,Master_VIC_Role__r.Horizontal__c 
            FROM User_VIC_Role__c
            WHERE vic_For_Previous_Year__c = false
            AND User__r.isActive = true
            AND Not_Applicable_for_VIC__c = false
        ]) {
            mapUserVicRoles.put(v.User__r.Id, v);
        }
        return mapUserVicRoles;
    }
    
    /*
        @Author: Vikas Rajput
        @Description: Returned currency with exchange rate
    */
    public static Map<String,vic_Currency_Exchange_Rate__c> fetchCurrencyExchangeRates(){
        Map<String,vic_Currency_Exchange_Rate__c> mapStrKeyToObjCER = new Map<String,vic_Currency_Exchange_Rate__c>();
        for(vic_Currency_Exchange_Rate__c eachCurrExchRate : [SELECT Id,vic_Exchange_Rate__c,vic_ISO_Code__c,vic_Month__c,Unique_Id__c,vic_Year__c 
                    FROM vic_Currency_Exchange_Rate__c]){
            String strKey = ''+eachCurrExchRate.vic_Month__c + eachCurrExchRate.vic_Year__c + eachCurrExchRate.vic_ISO_Code__c;
            mapStrKeyToObjCER.put(strKey , eachCurrExchRate);
        }
        return mapStrKeyToObjCER;
    }
    
     Public static decimal fetchProRatedAmount(decimal amount,date joinDt){
       processInfo=getVICProcessInfo();
       Integer currentYear = integer.valueof(processInfo.VIC_Annual_Process_Year__c);
      
       Integer joinYr;
       if(joinDt!=null){
           joinYr = integer.valueof(joinDt.year());
        }
       
       Date endFYDate=Date.newInstance(currentYear, 12, 31);
       decimal proRatedAmt,datediff;
       if(joinYr!=null &&  currentYear!=null && currentYear==joinYr){
          datediff = (joinDt.daysBetween(endFYDate)+1);
          proRatedAmt = (amount/365)*datediff;
       }else{
           proRatedAmt = amount;
       }
      return proRatedAmt;
    }

    
    
}