public class CheckRecursiveForContactRole {
	public static boolean runBefore = true;
    public static boolean runAfter = true;
    public static boolean runOnceBefore(){
        if(runBefore){
            runBefore=false;
            return true;
        }
        else{
            return runBefore;
        }
    }
    
    public static boolean runOnceAfter(){
        if(runAfter){
            runAfter=false;
            return true;
        }
        else{
            return runAfter;
        }
    }
}