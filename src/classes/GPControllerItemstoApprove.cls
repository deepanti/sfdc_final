/**
* @author Pankaj.adhikari
* @date 22/10/2017
*
* @group ItemstoApprove 
* @group-content ../../ApexDocContent/ItemstoApprove.html
*
* @description Apex Controller for Items to Approve module,
* has aura enabled method for Items To Approve.
*/

public without sharing class GPControllerItemstoApprove {
   /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */  
    public list<PendingWorkItems> fetchPendingApproval(string strObjectName,string strFieldSetName)
    {
        list<ProcessInstanceWorkitem> lstProcessInstanceWorkItems = getPendingWorkItems();
        list<ProcessInstanceWorkitem> lstPendingInstanceWorkItems = new list<ProcessInstanceWorkitem>();
        set<id> setTargetObjectId = new set<id>();
        map<id,Sobject> mpIdtoRelatedtoDetails = new map <id,Sobject>();
        if(lstProcessInstanceWorkItems != null && !lstProcessInstanceWorkItems.isEmpty())
        {
            for(ProcessInstanceWorkitem objPIWI : lstProcessInstanceWorkItems)
            {
                if(!String.isBlank(strObjectName))
                {
                    if(objPIWI.ProcessInstance.TargetObjectId.getsobjecttype() == getoidSobjectType(strObjectName))
                    {
                        setTargetObjectId.add(objPIWI.ProcessInstance.TargetObjectId);
                        lstPendingInstanceWorkItems.add(objPIWI);
                    }
                }
            }
            
            if(setTargetObjectId != null && !String.IsBlank(strObjectName))
            {
                mpIdtoRelatedtoDetails = getRelatedtoDetails(setTargetObjectId,strObjectName,strFieldSetName);
                system.debug('mpIdtoRelatedtoDetails::'+mpIdtoRelatedtoDetails);
            }
            
            return createResponseJSON(lstPendingInstanceWorkItems,mpIdtoRelatedtoDetails,strObjectName);
        }
        return null;
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    public GPAuraResponse getPendingApproval(string strObjectName,string strFieldSetName)
    {
        list<PendingWorkItems> lstPendingWorkItems = fetchPendingApproval(strObjectName,strFieldSetName);
        string strfieldSet = getFieldsSet(strObjectName,strFieldSetName );
        
        JSONGenerator objJSONGen = JSON.createGenerator(false);
        objJSONGen.writeStartArray();
            objJSONGen.writeStartObject();
                objJSONGen.writeStringField('Pending_WI',JSON.serialize(lstPendingWorkItems));
            objJSONGen.writeEndObject();
            objJSONGen.writeStartObject();
                objJSONGen.writeStringField('Field_Set',strfieldSet);
            objJSONGen.writeEndObject();
        objJSONGen.writeEndArray();
        
        return new GPAuraResponse(true,'',objJSONGen.getAsString());
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    private list<PendingWorkItems> createResponseJSON(list<ProcessInstanceWorkitem> lstPendingInstanceWorkItems , map<id,Sobject> mpIdtoRelatedtoDetails ,
                                                                string strObjectName )
    {
        list<PendingWorkItems> lstPendingWorkItems = new list<PendingWorkItems>();
        for(ProcessInstanceWorkitem objPIWI : lstPendingInstanceWorkItems)
        {
            PendingWorkItems objPendingWI ; 
            if(mpIdtoRelatedtoDetails.get(objPIWI.ProcessInstance.TargetObjectId) != null)
            {
                if(strObjectName=='GP_Project__c' )
                {
                  string strProjectStage = (string)mpIdtoRelatedtoDetails.get(objPIWI.ProcessInstance.TargetObjectId).get('GP_Project_Stages_for_Approver__c');
                  system.debug('strProjectStage::'+strProjectStage);
                  //objPendingWI = new PendingWorkItems(false,objPIWI,mpIdtoRelatedtoDetails.get(objPIWI.ProcessInstance.TargetObjectId),
                  //                 strProjectStage.split(';'));
                    objPendingWI = new PendingWorkItems(false,objPIWI,mpIdtoRelatedtoDetails.get(objPIWI.ProcessInstance.TargetObjectId));
                }
                else
                {
                    objPendingWI = new PendingWorkItems(false,objPIWI,mpIdtoRelatedtoDetails.get(objPIWI.ProcessInstance.TargetObjectId));
                }
            }else
            {
                objPendingWI = new PendingWorkItems(false,objPIWI,null);
            }
            lstPendingWorkItems.add(objPendingWI);
        }
        if(!lstPendingWorkItems.isEmpty())
        {
            return lstPendingWorkItems ;    
        }
        
        return null ;
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    private map<id,Sobject>  getRelatedtoDetails(set<id> setTargetObjectId,string strObjectName,string strFieldSetName )
    {
        map<id,Sobject> mpidSobject = new map<id,Sobject>();
        if(!String.IsBlank(strObjectName))
        {
            string strQuery ='Select ';
            
            if(strObjectName =='GP_Project__c' ) 
            {
                strQuery+='GP_Current_Working_User__r.name,GP_Project_Stages_for_Approver__c,GP_Temp_Approval_Status__c,GP_Approval_Status__c,GP_Band_3_Approver_Status__c,GP_Additional_Approval_Required__c,GP_Primary_SDO__r.GP_isChina__c,GP_PID_Approver_User__c,';
            }
            
            if(!String.IsBlank(strFieldSetName))
            {
                for(Schema.FieldSetMember objFSM: getFieldSet(strFieldSetName,strObjectName))
                {
                    strQuery += objFSM.getFieldPath()+',';
                }
            }
            
            strQuery+='id from '+ strObjectName +' Where id in: setTargetObjectId';
            system.debug(' strQuery'+strQuery);
            system.debug(' strFieldSetName'+strFieldSetName);
            system.debug(' strObjectName'+strObjectName);
            
            for( Sobject objSobject:DataBase.query(strQuery))
            {
                mpidSobject.put(objSobject.id,objSobject);
            }
            
            return mpidSobject;
        }
        return null;
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    private list<ProcessInstanceWorkitem> getPendingWorkItems()
    {
        return  [Select p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId, p.ProcessInstanceId,p.OriginalActorId,p.Id,p.ActorId , p.CreatedDate
                                                From ProcessInstanceWorkitem p where ProcessInstance.Status ='Pending' and ActorId =:Userinfo.getUserId() ];
        
        
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    public GPAuraResponse approvePendingItems(String strPendingWorkItems)
    {
        Set<id> setChinaSDOId = new set<id>();
        if(!string.isBlank(strPendingWorkItems))
        {
            list<GPControllerItemstoApprove.PendingWorkItems> lstPendingWorkItems = 
                    (list<GPControllerItemstoApprove.PendingWorkItems>)JSON.deserialize(strPendingWorkItems,list<GPControllerItemstoApprove.PendingWorkItems>.class);
            try
            {
                List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
                list<GP_Project__c> lstProjectToUpdate = new list<GP_Project__c>();
                for(PendingWorkItems objPWI: lstPendingWorkItems)
                {
                    if(objPWI.isSelected)
                    {
                        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                        req.setWorkitemId(objPWI.objPIWI.Id);
                        req.setAction('Approve');
                        req.setComments(objPWI.strRemarks);
                        requests.add(req);
                        
                        if( objPWI.objPIWI.ProcessInstance.TargetObjectId.getsObjectType() == GP_Project__c.sobjecttype )
                        {
                            
                            if(objPWI.objRelatedto.get('GP_Band_3_Approver_Status__c') == 'Pending' && 
                                    objPWI.objRelatedto.get('GP_Additional_Approval_Required__c') == true && 
                                    objPWI.objRelatedto.get('GP_Primary_SDO__r.GP_isChina__c') == true )
                            {
                                setChinaSDOId.add((Id)objPWI.objRelatedto.get('Id'));
                            }
                            else{
                                system.debug('temp :'+(String)objPWI.objRelatedto.get('GP_Temp_Approval_Status__c') );
                                if(objPWI.objRelatedto.get('GP_Approval_Status__c')=='Pending for Approval'){
                                GP_Project__c objProject = new GP_Project__c(id =objPWI.objPIWI.ProcessInstance.TargetObjectId ,
                                                            GP_Temp_Approval_Status__c = (String)objPWI.objRelatedto.get('GP_Temp_Approval_Status__c') );
                                GPServiceProjectApproval.setApprovalStageForProject(objProject,'Approve',objPWI.strRemarks );
                                lstProjectToUpdate.add(objProject);
                            }
                        }
                    }
                }
                }
                if(requests.size()>0)
                {
                    Approval.ProcessResult[] processResults = Approval.process(requests);
                    
                    if(setChinaSDOId.size()>0){ 
                        new GPServiceSubmitforApproval(setChinaSDOId).submitforApproval();
                    }
                    
                    if(!lstProjectToUpdate.isEmpty())
                    {
                        update lstProjectToUpdate ;
                    }
                    return new GPAuraResponse(true,'Record Approved','');
                }
                
            }catch(exception ex)
            {
                system.debug('ex.getMessage()::'+ex.getMessage());
                return new GPAuraResponse(false,ex.getMessage(),null);
            }
        }
        return new GPAuraResponse(false,'Please select the record to aaprove.','');
    }
    
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    public GPAuraResponse rejectPendingItems(String strPendingWorkItems)
    {
        if(!string.isBlank(strPendingWorkItems))
        {
            list<GPControllerItemstoApprove.PendingWorkItems> lstPendingWorkItems = (list<GPControllerItemstoApprove.PendingWorkItems>)JSON.deserialize(strPendingWorkItems,list<GPControllerItemstoApprove.PendingWorkItems>.class);
            try
            {
                List<Approval.ProcessWorkitemRequest> requests = new List<Approval.ProcessWorkitemRequest>();
                list<GP_Project__c> lstProjectToUpdate = new list<GP_Project__c>();
                for(PendingWorkItems objPWI: lstPendingWorkItems)
                {
                    if(objPWI.isSelected)
                    {
                        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                        req.setWorkitemId(objPWI.objPIWI.Id);
                        req.setAction('Reject');
                        req.setComments(objPWI.strRemarks);
                        requests.add(req);
                        if(objPWI.objRelatedto != null &&  objPWI.objPIWI.ProcessInstance.TargetObjectId.getsObjectType() == GP_Project__c.sobjecttype )
                        {
                            if(objPWI.objRelatedto.get('GP_Band_3_Approver_Status__c') == 'Pending' && 
                                    objPWI.objRelatedto.get('GP_Additional_Approval_Required__c') == true && 
                                    objPWI.objRelatedto.get('GP_Primary_SDO__r.GP_isChina__c') == true )
                            {
                                //setChinaSDOId.add((Id)objPWI.objRelatedto.get('Id'));
                            }
                            else
                            {
                                if(objPWI.objRelatedto.get('GP_Approval_Status__c')=='Pending for Approval'){
                                GP_Project__c objProject = new GP_Project__c(id =objPWI.objPIWI.ProcessInstance.TargetObjectId ,
                                                            GP_Temp_Approval_Status__c = (String)objPWI.objRelatedto.get('GP_Temp_Approval_Status__c'));
                                GPServiceProjectApproval.setApprovalStageForProject(objProject,'Reject',objPWI.strRemarks );
                                lstProjectToUpdate.add(objProject);
                            }
                          }
                        }
                    }
                }
                if(requests.size()>0)
                {
                    Approval.ProcessResult[] processResults = Approval.process(requests);
                    
                    if(!lstProjectToUpdate.isEmpty())
                    {
                        update lstProjectToUpdate ;
                    }
                    return new GPAuraResponse(true,'Record Rejected','');


                }
                
            }catch(exception ex)
            {
                system.debug('ex.getMessage()::'+ex.getMessage());
                return new GPAuraResponse(false,ex.getMessage(),null);
            }
        }
        return new GPAuraResponse(false,'Please select the record to aaprove.','');
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    private Schema.SObjectType getoidSobjectType(String objectAPIName)
    {
        Map<String, Schema.SObjectType> globalDescription =  Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = globalDescription.get(objectAPIName); 
        return sObjType;
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    @TestVisible
    public class PendingWorkItems
    {
        @AuraEnabled
        public boolean isSelected;
        @AuraEnabled
        public ProcessInstanceWorkitem objPIWI;
        @AuraEnabled
        public id relatedToid ;
        @AuraEnabled
        public Sobject objRelatedto ; 
        @AuraEnabled
        public String strRemarks ; 
        @AuraEnabled
        public String strProjectstage; 
         @AuraEnabled
        public list<String> lstProjectStage; 
        @TestVisible
        PendingWorkItems (boolean isSelected, ProcessInstanceWorkitem objPIWI,Sobject objRelatedto )
        {
            this.isSelected = isSelected;
            this.objPIWI = objPIWI;
            this.objRelatedto = objRelatedto;
            this.strRemarks ='';
        }
        PendingWorkItems (boolean isSelected, ProcessInstanceWorkitem objPIWI,Sobject objRelatedto,list<String> lstProjectStage )
        {
            this.isSelected = isSelected;
            this.objPIWI = objPIWI;
            this.objRelatedto = objRelatedto;
            this.strRemarks ='';
            this.lstProjectStage = lstProjectStage;
            this.strProjectstage='';
        }
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    public String getFieldsSet(string strObjectName, String fieldSetName ) 
    {
        JSONGenerator objJSONGen = JSON.createGenerator(false);
        objJSONGen.writeStartArray();
        for(Schema.FieldSetMember objFSM: getFieldSet(fieldSetName,strObjectName))
            {
                objJSONGen.writeStartObject();
                objJSONGen.writeStringField('field_Label',objFSM.label);
                objJSONGen.writeStringField('field_Name',objFSM.getFieldPath());
                objJSONGen.writeEndObject();
            }
        objJSONGen.writeEndArray(); 
        return objJSONGen.getAsString();
    }
     /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    public static List<Schema.FieldSetMember> getFieldSet(String fieldSetName, String ObjectName)
    {
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        if(fieldSetObj != null)
        {
            return fieldSetObj.getFields(); 
        }
        else
        {
            return null;
        }
    }
}