public without sharing class GPDomainProduct extends fflib_SObjectDomain {
    public GPDomainProduct(List < Product2 > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainProduct(sObjectList);
        }
    }

    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {        
        GP_Product_Master__c.SobjectType,
        GP_Deal__c.SObjectType
    };
        
    public override void onAfterInsert() {        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
    	syncProductMasterOnProduct2Change(uow, null);
        uow.commitWork();
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        syncProductMasterOnProduct2Change(uow, oldSObjectMap);
        //copyProductHierarchyToDeals(uow, oldSObjectMap);        
        uow.commitWork();
    }
    
    public void syncProductMasterOnProduct2Change(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
        
        try {
            Set<Id> setOfProductIds = new Set<Id>();
            Set<Id> setOfProductIdForUpdate = new Set<Id>();
            Map < Id, Product2 > mapOfProduct = new Map < Id, Product2 > ();
            List<GP_Product_Master__c> listOfProductMasters = new List<GP_Product_Master__c>();
            
            for (Product2 product: (List < Product2 >) records) {
                
                Product2 oldProduct;
                if(oldSObjectMap != null) {
                    oldProduct = (Product2) oldSObjectMap.get(product.Id);
                }                
                
                // Have to remove oldProduct check in if cond.
                if(oldSObjectMap == null || 
                (product.Nature_of_Work__c != oldProduct.Nature_of_Work__c || 
                    product.Service_Line__c != product.Service_Line__c ||
                    product.Industry_Vertical__c != oldProduct.Industry_Vertical__c ||
                    product.Product_Family__c != oldProduct.Product_Family__c ||
                    product.Name != oldProduct.Name ||
                    product.IsActive != oldProduct.IsActive
                )) {
                    setOfProductIds.add(product.Id);
                    mapOfProduct.put(product.Id, product);
                }
            }
            system.debug('==setOfProductIds=='+setOfProductIds);
            if(setOfProductIds != null && setOfProductIds.size() > 0) {
                for(GP_Product_Master__c pm : [Select Id, GP_Product_ID__c, GP_Is_Active__c, 
                                            GP_Nature_of_Work__c, GP_Service_Line__c, 
                                            GP_Product_Name__c from GP_Product_Master__c 
                                            Where GP_Product_ID__c IN: setOfProductIds]) {
                    if(mapOfProduct.containsKey(pm.GP_Product_ID__c)) {
                        GP_Product_Master__c pmObj = new GP_Product_Master__c();
                        pmObj.Id = pm.Id;
                        pmObj.GP_Industry_Vertical__c = mapOfProduct.get(pm.GP_Product_ID__c).Industry_Vertical__c;
                        pmObj.GP_Is_Active__c = mapOfProduct.get(pm.GP_Product_ID__c).IsActive;
                        pmObj.GP_Service_Line__c = mapOfProduct.get(pm.GP_Product_ID__c).Service_Line__c;
                        pmObj.GP_Nature_of_Work__c = mapOfProduct.get(pm.GP_Product_ID__c).Nature_of_Work__c;
                        pmObj.GP_Product_Name__c = mapOfProduct.get(pm.GP_Product_ID__c).Name;
                        pmObj.Name = mapOfProduct.get(pm.GP_Product_ID__c).Name.length() > 80 ? 
                                    mapOfProduct.get(pm.GP_Product_ID__c).Name.substring(0, 80) : 
                                    mapOfProduct.get(pm.GP_Product_ID__c).Name;
                        
                        listOfProductMasters.add(pmObj);
                        setOfProductIdForUpdate.add(pm.GP_Product_ID__c);
                    }
                }
                System.debug('==listOfProductMasters==BEFORE=='+listOfProductMasters);
                for(Id productId : mapOfProduct.keySet()) {
                    if(!(setOfProductIdForUpdate.size() > 0 && setOfProductIdForUpdate.contains(productId))) {
                        GP_Product_Master__c pmObj = new GP_Product_Master__c();
                        pmObj.GP_Industry_Vertical__c = mapOfProduct.get(productId).Industry_Vertical__c;
                        pmObj.GP_Is_Active__c = mapOfProduct.get(productId).IsActive;
                        pmObj.GP_Service_Line__c = mapOfProduct.get(productId).Service_Line__c;
                        pmObj.GP_Nature_of_Work__c = mapOfProduct.get(productId).Nature_of_Work__c;
                        pmObj.GP_Product_Name__c = mapOfProduct.get(productId).Name;
                        pmObj.Name = mapOfProduct.get(productId).Name.length() > 80 ? mapOfProduct.get(productId).Name.substring(0, 80) : mapOfProduct.get(productId).Name;
                        pmObj.GP_Product_ID__c = mapOfProduct.get(productId).Id;
                        pmObj.GP_Product_Family__c = mapOfProduct.get(productId).Product_Family__c;
                        
                        listOfProductMasters.add(pmObj);
                    }
                }
                System.debug('==listOfProductMasters==AFTER=='+listOfProductMasters.size());
                if (!listOfProductMasters.isEmpty() && listOfProductMasters.size() < 10000) {
                    upsert listOfProductMasters;
                } else {
                    GPErrorLogUtility.logInfo('GPDomainProduct', 'syncProductMasterOnProduct2Change', 'Line no : 96.', null, null, null, null, 'LIMIT: More than 10000 records for DML.');
                }
            }
        } catch(Exception ex) {
            GPErrorLogUtility.logError('GPDomainProduct', 'Product Master Upsert.', ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());
        }
    }

    /*public void copyProductHierarchyToDeals(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {
        Set < Id > setOfProductIds = new Set < Id > ();
        Map < Id, Product2 > mapOfProduct = new Map < Id, Product2 > ();
        List < GP_Deal__c > lstUpdatedProductHierarchyDeals = new List < GP_Deal__c > ();
        Product2 oldProduct;
        for (Product2 product: (List < Product2 > ) records) {
            oldProduct = (Product2) oldSObjectMap.get(product.Id);
            if (product.Nature_of_Work__c != oldProduct.Nature_of_Work__c || product.Service_Line__c != product.Service_Line__c ||
                product.Industry_Vertical__c != oldProduct.Industry_Vertical__c) {
                setOfProductIds.add(product.Id);
                mapOfProduct.put(product.Id, product);
            }
        }
        if (setOfProductIds.size() > 0) {
            for (GP_Deal__c deal: [select id, GP_Product_Id__c, GP_Nature_of_Work__c, GP_Service_Line__c, GP_Vertical__c
                    from GP_Deal__c where GP_Product_Id__c in: setOfProductIds
                ]) {
                if (mapOfProduct.containsKey(deal.GP_Product_Id__c)) {
                    deal.GP_Nature_of_Work__c = mapOfProduct.get(deal.GP_Product_Id__c).Nature_of_Work__c;
                    deal.GP_Service_Line__c = mapOfProduct.get(deal.GP_Product_Id__c).Service_Line__c;
                    deal.GP_Vertical__c = mapOfProduct.get(deal.GP_Product_Id__c).Industry_Vertical__c;
                    lstUpdatedProductHierarchyDeals.add(deal);
                }
            }

            if (!lstUpdatedProductHierarchyDeals.isEmpty()) {
                uow.registerDirty(lstUpdatedProductHierarchyDeals);
            }
        }
    }*/    
}