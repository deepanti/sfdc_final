/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing InsideSalesAssignCampaign class
**************************************************************************************************************************/
@isTest (SeeAllData = false)
public class InsideSalesAssignCampaignTest {
    public static User u;
    public static User u1;
    public static List<Id> idsList;    
    public static Campaign insertedCampaign;
    public static Account acc;
    public static Lead lead1;
    public static Lead lead2;
    public static Contact con1;
    public static Contact con2;
    public static CampaignMember campLead1Mem;
    public static CampaignMember campLead2Mem;
    public static CampaignMember campCon1Mem;
    public static CampaignMember campCon2Mem;
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    public static void setUpData()
    {	
        RecordType contactCreation = [select id from recordType where sObjectType = 'Contact' And DeveloperName = 'Default_Record_Type' limit 1];
        
        u = TestDataFactoryUtility.createTestUser('System Administrator','a4c@bc.com','a4c@bc.com');
        //u1 = TestDataFactoryUtility.createTestUser('Genpact Insight Sales ','a40c@bc.com','a40c@bc.com');
        idsList = new List<Id>();
        //idsList.add(u1.Id);
        //idsList.add(u.Id);
        insertedCampaign = new Campaign(Name='CampaignTest',Status='Launched',Channel__c='Telenurturing',AssignedTo__c='');
        insert insertedCampaign;
        acc = TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        con1 = new Contact(lastname='abc',firstname='pqr',email='lmntest@gmail.com',AccountId=acc.Id,Title='Manager',Country_of_Residence__c='India',
                           RecordTypeId = contactCreation.Id);
        insert con1;
        con2 = new Contact(lastname='hehe',firstname='pqr',email='hehetest@gmail.com',AccountId=acc.Id,Title='Manager',Country_of_Residence__c='India',
                           RecordTypeId = contactCreation.Id);
        insert con2;
        lead1 = new Lead(lastname='abc',company='abc',Account__c=acc.Id,firstname='uuu',Title='Manager',email='xfytest@gmail.com',Status='Raw',Country_Of_Residence__c='India');
        insert lead1;        
        lead2 = new Lead(lastname='abcl',company='abcg',firstname='uuul',Title='Managerl',email='xfytlest@gmail.com',Status='Raw',Country_Of_Residence__c='India');
        insert lead2;
        campLead1Mem = new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=lead1.Id,ContactId=null);
        insert campLead1Mem;
        campLead2Mem =new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=lead2.Id,ContactId=null);
        insert campLead2Mem;
        campCon1Mem = new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=null,ContactId=con1.Id);
        insert campCon1Mem;
        campCon2Mem = new CampaignMember(CampaignId=insertedCampaign.Id,LeadId=null,ContactId=con2.Id);
        insert campCon2Mem;
    }
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing InsideSalesAssignCampaign.getCampaignMembers mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void getCampaignMembers()
    {
        setUpData();
        System.runAs(u){
            Test.startTest();
            List<InsideSalesAssignCampaign.AccountWrapper> wrapList = InsideSalesAssignCampaign.getCampaignMembers(insertedCampaign.Id);
            Test.stopTest();
        }
    }
     /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing InsideSalesAssignCampaign.fetchCampaign mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void fetchCampaign()
    {
        setUpData();
        System.runAs(u){
            Test.startTest();
            insertedCampaign  = InsideSalesAssignCampaign.fetchCampaign(insertedCampaign.Id);
            system.assertEquals('CampaignTest',insertedCampaign.Name);
            Test.stopTest();
        }
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing InsideSalesAssignCampaign.fetchCampaign mtd if no campaign id is passed
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void fetchCampaignNull()
    {
        setUpData();
        System.runAs(u){
            Test.startTest();
            insertedCampaign  = InsideSalesAssignCampaign.fetchCampaign('habc');
            system.assertEquals(null,insertedCampaign);
            Test.stopTest();
        }
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing InsideSalesAssignCampaign.saveCallersToCampaignMembers mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void saveCallersToCampaignMembers()
    {
        setUpData();
        System.runAs(u){
            Test.startTest();
            InsideSalesAssignCampaign.saveCallersToCampaignMembers(JSON.serialize(InsideSalesAssignCampaign.getCampaignMembers(insertedCampaign.Id)));
            system.AssertNotEquals(null,insertedCampaign.AssignedTo__c);
            Test.stopTest();
        }
    }
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing InsideSalesAssignCampaign.getCallersToSkipInPlay mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void getCallersToSkipInPlay()
    {
        setUpData();
        Test.startTest();
        //idsList = InsideSalesAssignCampaign.getCallersToSkipInPlay();
        Test.stopTest();
        
    }
}