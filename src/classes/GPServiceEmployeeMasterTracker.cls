//================================================================================================================
//  Description: Test Class for GPServiceEmployeeMaster
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPServiceEmployeeMasterTracker {
    public static set<id> setOfId = new set<id>();
    public static set<String> setOfOHRId = new set<String>();
    public static void setupCommonData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_Final_OHR__c = '123456';
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = true;
        insert empObj;
        setOfId.add(objuser.id);
        setOfOHRId.add('123456');
    }
    @isTest
    public static void testGPServiceEmployeeMaster() {
        setupCommonData(); 
        GPServiceEmployeeMaster.ActivateUser(setOfId);
    }
    
     @isTest
    public static void testGPServiceEmployeeMaster2() {
        setupCommonData(); 
        GPServiceEmployeeMaster.inActivateUser(setOfId);
    }
    
     @isTest
    public static void testGPServiceEmployeeMaster3() {
        setupCommonData(); 
        GPServiceEmployeeMaster.getMapOHRtoPersonId(setOfOHRId);
    }
}