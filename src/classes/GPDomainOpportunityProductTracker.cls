@isTest
public class GPDomainOpportunityProductTracker {
    
    public static OpportunityLineItem oli;
    
    static void setupCommonData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'opportunitylineitem';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
         Sales_Unit__c salesunittosharerecord=new Sales_Unit__c();
        
      salesunittosharerecord.Sales_Leader__c=Userinfo.getUserId();
        salesunittosharerecord.name='TESTING SALES';
        insert salesunittosharerecord;
        Business_Segments__c BSobj = GPCommonTracker.getBS();
       
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
          user    objuser = GPCommonTracker.getUser(); 
        Profile p = [select id, name from profile where name = 'Genpact Super Admin'
                     limit 1
                    ];
        objuser.ProfileID = p.id;        
        insert objuser;
   
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
          accobj.Sales_Unit__c=salesunittosharerecord.id;
        insert accobj ;
           
       
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.AccountId = accobj.id;
        oppobj.Probability = 40;
        oppobj.Sales_Leader__c=objuser.id;
        insert oppobj;
        singleexecuion.bool_ShareOLIWithAccountGRMAndOppotunityOwner=true;
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        insert oppproobj;
        
        Product2 prodobj = GPCommonTracker.getProduct();
        insert prodobj;
        
       
        
        OpportunityProduct__c oppprodobj = GPCommonTracker.getOpportunityProduct(prodobj.id,oppobj.id);
        insert oppprodobj; 
       
        GP_Deal__c Dealobj = GPCommonTracker.getDeal();
        Dealobj.GP_OLI__c = oppprodobj.ID;
        Dealobj.GP_Probability__c=oppobj.Probability;
        insert Dealobj;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        oli = new OpportunityLineItem(
            OpportunityId = oppobj.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 8 * pbEntry.UnitPrice
            
            
        );
      // oli.Opportunity.Probability=oppobj.Probability;
        insert oli;
        
    } 
    
    @isTest
    public static void testGPDomainOpportunityDetail()
    {
    
        setupCommonData();
        
        oli.Quantity = 6;
     
        update oli;
        
    // delete oli;
      
    }
    
    @isTest public static void testGPDomainOpportunityProduct() {
        GPDomainOpportunityProduct obj = new GPDomainOpportunityProduct(new List<OpportunityLineItem>{new OpportunityLineItem()});
    }
        
}