/*
    @Author: Rajan Saini
    @Company: SaaSFocus
    @Description: Test class for the controller of VIC_CalculateMBOIncentivectrl
*/

@isTest
 public class VIC_CalculateMBOIncentivectrlTest{
   
      static testmethod void validatemethod(){
    
        APXTConga4__Conga_Template__c obj= new VIC_CommonTest().initAllcongaData('GRM - CM VIC Letter');
         insert obj;
        
         List<Plan__c> plan = new VIC_CommonTest().initAllPlanData(obj.ID);
         
         insert plan;
        
        List<Master_Plan_Component__c >lstMaster= new VIC_CommonTest().initAllMasterPlanComponent();
        insert lstMaster;
        List<Plan_Component__c> planComponent= new VIC_CommonTest().initAllPlanComponent(plan,lstMaster);
        
        VIC_Calculation_Matrix__c objMatrix =  VIC_CommonTest.fetchMatrixComponentData('SL CP Enterprise');
        insert objMatrix;
        
        List<VIC_Calculation_Matrix_Item__c> calcMatrix = new VIC_CommonTest().fetchListMatrixItemMBOData(objMatrix.Name);
        
        Target__c target = new Target__c();
        target.Target_Bonus__c = 5000;
          target.Plan__r = plan[3];
        target.Plan__c = plan[3].id;
        
        insert target;
        
          Target__c target1 = new Target__c();
        target1.Target_Bonus__c = 5000;
          target1.Plan__r = plan[2];
        target1.Plan__c = plan[2].id;
        
        insert target1;
          
        Target_Component__c  targetComp = new Target_Component__c();
        targetComp.Weightage__c =10;
        targetComp.vic_Achievement_Percent__c =100;
        targetComp.Target_Status__c = 'Active';
        targetComp.Target_In_Currency__c =1000;
        targetComp.Is_Weightage_Applicable__c = true;
        targetComp.Target__c = target.id;
         targetComp.Target__r = target;
        insert targetComp;
          
          Target_Component__c  targetComp1 = new Target_Component__c();
        targetComp1.Weightage__c =10;
        targetComp1.vic_Achievement_Percent__c =100;
        targetComp1.Target_Status__c = 'Active';
        targetComp1.Target_In_Currency__c =1000;
        targetComp1.Is_Weightage_Applicable__c = true;
        targetComp1.Target__c = target1.id;
         targetComp1.Target__r = target1;
        insert targetComp1;
        

        Test.StartTest(); 
        VIC_CalculateMBOIncentivectrl objctrl = new VIC_CalculateMBOIncentivectrl();
        objctrl.opAcheivmentPercent=100.0;
        objctrl.calculate(targetComp, null);
        objctrl.calculate(targetComp1, null);
          System.AssertEquals(200,200);
        Test.StopTest();
         
       
  }


}