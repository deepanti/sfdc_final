/**
* @author Anmol.kumar
* @date 13/09/2017
*
* @group projectContainer
* @group-content ../../ApexDocContent/projectContainer.htm
*
* @description Apex Service for projectContainer module,
* has aura enabled method to fetch, update/create and delete project budget data.
*/
public class GPCmpserviceprojectContainer {
	
    /**
    * @description Returns  status of project and its child record
    * @param projectId SFDC 18/15 digit Project  Id
    * @param Boolean validateApproval
    * 
    * @return GPAuraResponse Serialized status of project and its child record
    * 
    * @example
    * GPCmpserviceprojectContainer.getProjectContainerStatus(<SFDC 18/15 digit Project Id>, <validateApproval>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectContainerStatus(Id projectId, Boolean validateApproval) {
        GPControllerProjectContainer projectDetailobj = new GPControllerProjectContainer(projectId);
        return projectDetailobj.getProjectChildStatus(validateApproval);
    }
}