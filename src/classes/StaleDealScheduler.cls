global class StaleDealScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {  
        Delete [Select ID from Restricted_Opportunities__c];
        //staleDealsIdentifier reassign = new staleDealsIdentifier('');        
		//ID batchprocessid = Database.executeBatch(reassign,100);
		TsDealsIdentifierBatch reassign = new TsDealsIdentifierBatch();        
		ID batchprocessid = Database.executeBatch(reassign,100);
    }
}