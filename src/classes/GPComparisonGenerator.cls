public class GPComparisonGenerator {

    //SObject destinationObject, sourceObject;
    Map<string,string> destinationObject,sourceObject;
    Set < String > fieldSet;
    String objectType;
    map<string,string> mapApiNameToLabel ;
    map<string,Schema.DisplayType> mapFieldToType ;
    
    public GPComparisonGenerator setDestinationObjectList(Map<string,string> destinationObject) {
        this.destinationObject = destinationObject;
        return this;
    }

    public GPComparisonGenerator setSourceObjectList(Map<string,string> sourceObject) {
        this.sourceObject = sourceObject;
        return this;
    }

    public GPComparisonGenerator setFieldSet(Set < String > fieldSet) {
        this.fieldSet = fieldSet;
        return this;
    }

    public GPComparisonGenerator setObjectType(String objectType) {
        this.objectType = objectType;
        return this;
    } 

    public GPComparisonGenerator sefieldLabelMap(map<string,string> mapApiNameToLabel) {
        this.mapApiNameToLabel = mapApiNameToLabel;
        return this;
    }
    public GPComparisonGenerator sefieldTypeMap(map<string,Schema.DisplayType> mapFieldToType) {
        this.mapFieldToType = mapFieldToType;
        return this;
    }

    public List < GPComparisonStructure > generateComparisonResult() {

        List < GPComparisonStructure > comparisonResult = new List < GPComparisonStructure > ();
        GPComparisonStructure compareresult;
        String updatedValue;
        String fieldLabel;

        for (String strfield: fieldSet) {
              string field = strfield.tolowercase();
            if (sourceObject.get(field) != null || (destinationObject != null && destinationObject.get(field) != null)) {
                
                updatedValue =  (destinationObject != null && (sourceObject.get(field) != destinationObject.get(field)) )? String.ValueOf(destinationObject.get(field)) : '';
                
                fieldLabel = mapApiNameToLabel != null && mapApiNameToLabel.get(field.tolowercase()) != null ? mapApiNameToLabel.get(field.tolowercase()): '' ;
                
                if(mapFieldToType != null && mapFieldToType.get(field.tolowercase()) != null &&
                  	mapFieldToType.get(field.tolowercase()) == Schema.DisplayType.DATE ){
                 	
                    compareresult = new GPComparisonStructure(formateddate(updatedValue), formateddate(String.valueOf(sourceObject.get(field))), objectType, field, fieldLabel);
                }
                else
                {
                   compareresult = new GPComparisonStructure(updatedValue, String.valueOf(sourceObject.get(field)), objectType, field, fieldLabel); 
                }
                
                comparisonResult.add(compareresult);
                if(mapFieldToType != null && mapFieldToType.get(field.tolowercase()) != null){
                    if(mapFieldToType.get(field.tolowercase()) == Schema.DisplayType.REFERENCE){
                        if (field.contains('__c')) {
                                fieldLabel = mapApiNameToLabel != null && mapApiNameToLabel.get(field.tolowercase()) != null ? mapApiNameToLabel.get(field.tolowercase()): '' ;
                                field = field.replace('__c', '__r');
                                field +='.name';
								updatedValue =  (destinationObject != null && (sourceObject.get(field) != destinationObject.get(field)) )? String.ValueOf(destinationObject.get(field)) : '';
                                compareresult = new GPComparisonStructure(updatedValue, String.valueOf(sourceObject.get(field)), objectType, field, fieldLabel);
                                system.debug(compareresult);
                                comparisonResult.add(compareresult);
                         }
                    }
                }
            }
            else if(sourceObject.get(field) == null)
            {
                
                
                fieldLabel = mapApiNameToLabel != null && mapApiNameToLabel.get(field.tolowercase()) != null ? mapApiNameToLabel.get(field.tolowercase()): '' ;
                updatedValue =  (destinationObject != null && destinationObject.get(field) != null )? String.ValueOf(destinationObject.get(field)) : '';
                compareresult = new GPComparisonStructure(updatedValue, '', objectType, field, fieldLabel);
                comparisonResult.add(compareresult);
            }
        }

        return comparisonResult;
    }

    private string formateddate(string updatedValue){
        
        if(String.isNOTBlank(updatedValue)){
            Date d = date.valueOf(updatedValue);
            Datetime myDT = datetime.newInstance(d.year(), d.month(),d.day());
            return myDT.format('dd-MMM-yy');
        }
        return updatedValue;
    }

}