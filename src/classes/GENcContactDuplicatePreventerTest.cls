@isTest
private class GENcContactDuplicatePreventerTest
{
    public static testMethod void RunTest()
    {
        try
        {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Contact oContact1 = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        }
        catch(Exception ex){}
    }
    public static testMethod void RunTest1()
    {
        try
        {
        List<Contact> oContactList =new List<Contact>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Contact oContact = new Contact();
        oContact.FirstName = 'Mohit';
        oContact.LastName = 'Jain';
        oContact.AccountId = oAccount.Id;
        oContact.Title = 'test';
        oContact.LeadSource = 'Cross-Sell';
        oContact.Email = 'test@gmail.com';  
        oContact.Phone = '9891798737';
        oContactList.add(oContact);   
        Contact oContact1 = new Contact();
        oContact1.FirstName = 'Mohit';
        oContact1.LastName = 'Jain';
        oContact1.AccountId = oAccount.Id;
        oContact1.Title = 'test';
        oContact1.LeadSource = 'Cross-Sell';
        oContact1.Email = 'test@gmail.com';  
        oContact1.Phone = '9891798737';
        oContactList.add(oContact1);
        insert oContactList;
        }
        catch(Exception ex){}
    }
}