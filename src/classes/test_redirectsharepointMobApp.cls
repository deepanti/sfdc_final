@istest (SeeAllData=true)
Public class test_redirectsharepointMobApp{

  static testMethod void method1()
    {  
    
    
    
     List <items_Sharepoint_Files__x > mockedRequests = new List <items_Sharepoint_Files__x >();
     
     
     
     items_Sharepoint_Files__x request  =new items_Sharepoint_Files__x (
     DisplayUrl='test'
     
     );
     
        mockedRequests.add(request ); 
    
    
     //public static items_Sharepoint_Files__x  findById(Id requestId) 
     items_Sharepoint_Files__x  req=new  items_Sharepoint_Files__x ();
        if(Test.isRunningTest()) {
           req=( (mockedRequests.size() > 0) ? mockedRequests[0] : null);
        }
        else
        {

        req = [
            SELECT  Id, DisplayUrl, Name__c
            FROM items_Sharepoint_Files__x 
            WHERE Id =: request.Id
        ];
}
        
    
     ID hardid='x00N0000000CderIAC';
     
    ApexPages.StandardController std = new ApexPages.StandardController(req );
        redirectsharepointMobApp controller = new redirectsharepointMobApp(std);
        controller.Name='Google.pdf';
        controller.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/Banking and Financial Services/Deal Enablers/ESSENTIALS & FAQs/Resource management slides write up.pptx';
        controller.showdoc();
        
        
        ApexPages.StandardController std1 = new ApexPages.StandardController(req );
        redirectsharepointMobApp controller1 = new redirectsharepointMobApp(std);
        controller1.Name='Google.doc';
        controller1.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/Banking and Financial Services/Deal Enablers/ESSENTIALS & FAQs/Resource management slides write up.pptx';
        controller1.showdoc();
        
        
        ApexPages.StandardController std2 = new ApexPages.StandardController(req );
        redirectsharepointMobApp controller2 = new redirectsharepointMobApp(std);
        controller2.Name='Google.pptx';
        controller2.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/Banking and Financial Services/Deal Enablers/ESSENTIALS & FAQs/Resource management slides write up.pptx';
        controller2.showdoc();
        
        
        ApexPages.StandardController std3 = new ApexPages.StandardController(req );
        redirectsharepointMobApp controller3 = new redirectsharepointMobApp(std);
        controller3.Name='Google.xls';
        controller3.URL='https://genpactonline.sharepoint.com/sites/presales_km/practice_approved/Banking and Financial Services/Deal Enablers/ESSENTIALS & FAQs/Resource management slides write up.pptx';
        controller3.showdoc();
        
        
    }

}