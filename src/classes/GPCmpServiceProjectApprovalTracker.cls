@isTest 
public class GPCmpServiceProjectApprovalTracker {
    public static GP_Project__c prjObj;
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;

    static void setupCommonData() {
	
		GP_Global_Value_Set__c gvsProductCodes = new GP_Global_Value_Set__c();		
        gvsProductCodes.Name = 'GE_Product_Code';		
        gvsProductCodes.GP_Description__c = 'Comma separated Product Code.';		
        gvsProductCodes.GP_Value_Set_1__c = 'GE,Product,Code';		
        insert gvsProductCodes;
		
		GP_Approver_List__c bprApprover = new GP_Approver_List__c();
        bprApprover.Name = 'System Admin';
        bprApprover.GP_Is_Active__c = true;
        bprApprover.GP_Is_BPR_Approver__c = true;
        bprApprover.GP_Is_Avalara_Approver__c = false;
        bprApprover.GP_Approver_Id__c = UserInfo.getUserId();
        insert bprApprover;
        
        GP_Approver_List__c avalaraApprover = new GP_Approver_List__c();
        avalaraApprover.Name = 'Frej Rostom';
        avalaraApprover.GP_Is_Active__c = true;
        avalaraApprover.GP_Is_BPR_Approver__c = false;
        avalaraApprover.GP_Is_Avalara_Approver__c = true;
        avalaraApprover.GP_Approver_Id__c = UserInfo.getUserId();
        insert avalaraApprover;
		
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_isChina__c = true;
        insert objSdo;

        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c prjObjold = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObjold.OwnerId = objuser.Id;
        prjObjold.GP_Oracle_Status__c = 'S';
        prjObjold.GP_TAX_Code__c = '12345';
        insert prjObjold;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();

        prjObj.GP_Parent_Project__c = prjObjold.Id;
        prjObj.OwnerId = loggedInUser;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Project_Stages_for_Approver__c = 'Draft';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser;
        prjObj.GP_Controller_User__c = loggedInUser;

        insert prjObj;

        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;

        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;

        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;

        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;

        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;

        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj;

        String strFieldSetName = '';
        for (Schema.FieldSet f: SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }

        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj, prjObj, timesheettrnsctnObj);
        insert timeshtentryObj;

        GP_Project_Version_History__c projectVersionHistory = GPCommonTracker.getProjectVersionHistory(prjObj.Id);
        insert projectVersionHistory;

        GP_Project_Classification__c projectClassification = GPCommonTracker.getProjectClassification(prjObj.Id);
        insert projectClassification;
    }

    @isTest public static void testGPCmpServiceProjectDetail() {
        setupCommonData();
        testgetProjectDetail();
        testProjectStatusToApprove();
        testProjectStatusToApproveForClosure();
        testProjectStatusToReject();
        testProjectStatusToRejectForClosure();
        testProjectReassignToUser();
    }

    public static void testgetProjectDetail() {
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment', 'approver');
        System.runAs(objuser) {
            GPAuraResponse response = GPCmpServiceProjectApproval.getProjectDetail(prjObj.Id);
            response = GPCmpServiceProjectApproval.getProjectDetail(null);
        }
    }

    public static void testProjectStatusToApprove() {
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment', 'approver');
        
        System.runAs(objuser) {
            prjObj.GP_Band_3_Approver_Status__c = 'Pending';
            prjObj.GP_Additional_Approval_Required__c = true;
            prjObj.GP_Primary_SDO__c = objSdo.id;

            update prjObj;
            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectStatusToApprove(prjObj.Id, 'Draft', 'Comment', '12345', null, 'Product_Code');
            response = GPCmpServiceProjectApproval.ProjectStatusToApprove(null, 'Draft', 'Comment', '12345', null, 'Product_Code');
        }
    }

    @isTest public static void testProjectStatusToApprove1() {
        setupCommonData();
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment', 'approver');
        System.runAs(objuser) {
            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectStatusToApprove(prjObj.Id, 'Draft', 'Comment', '12345', null, 'Product_Code');
        }
    }


    public static void testProjectStatusToApproveForClosure() {
        System.runAs(objuser) {
            GPAuraResponse obj = GPCmpServiceProjectSubmission.submitforClosure(prjObj.id, 'Cost Charging', 'Test Comment', objuser.Id);

            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectStatusToApproveForClosure(prjObj.Id, 'Comment Test', 'Product_Code');
            response = GPCmpServiceProjectApproval.ProjectStatusToApproveForClosure(null, 'Comment Test', 'Product_Code');
        }
    }

    public static void testProjectStatusToReject() {
        System.runAs(objuser) {
            GPAuraResponse obj = GPCmpServiceProjectSubmission.submitforClosure(prjObj.id, 'Cost Charging', 'Test Comment', objuser.Id);
            prjObj.GP_Approval_Status__c = 'Pending for Approval';
            update prjObj;
            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectStatusToReject(prjObj.Id, 'Stage', 'Comment', 'Product_Code');
            response = GPCmpServiceProjectApproval.ProjectStatusToReject(null, 'Stage', 'Comment', 'Product_Code');
        }
    }

    public static void testProjectStatusToRejectForClosure() {
        System.runAs(objuser) {
            GPAuraResponse obj = GPCmpServiceProjectSubmission.submitforClosure(prjObj.id, 'Cost Charging', 'Test Comment', objuser.Id);

            prjObj.GP_Approval_Status__c = 'Pending for Approval';
            update prjObj;
            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectStatusToRejectForClosure(prjObj.Id, 'Test', 'Product_Code');
            response = GPCmpServiceProjectApproval.ProjectStatusToRejectForClosure(null, 'Test', 'Product_Code');
        }
    }

    public static void testProjectReassignToUser() {
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment', 'approver');
        System.runAs(objuser) {

            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectReassignToUser(prjObj.Id, objuser.Id, 'Comment');
            response = GPCmpServiceProjectApproval.ProjectReassignToUser(null, null, 'Comment');
        }
    }

    @isTest public static void testProjectReassignToUser1() {
        setupCommonData();
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment', 'approver');
        System.runAs(objuser) {
            GPAuraResponse response = GPCmpServiceProjectApproval.ProjectReassignToUser(prjObj.Id, objuser.Id, 'Comment');
        }
    }
}