@istest  
public class GPControllerGenerateProjectFileTracker {
    
    private static GP_Project__c objProject;
    public static boolean isRun = true;
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    public Static User objuser ;
    
    @IsTest
    public static void TestPEFPRFFile(){
        createTestData();
        //prjObj.GP_Oracle_Status__c ='S';
        system.debug('prjObj.GP_Approval_status__C::'+prjObj.GP_Approval_status__C);
        Test.startTest();
        prjObj.GP_Approval_status__c = 'Approved';
        prjObj.Name ='PRF Generator PID';        
        update prjObj;
        
        system.assert(prjObj.GP_Approval_status__C == 'Approved','Project Not Approved'  );
        
        prjObj.GP_Oracle_Status__c ='S';
        prjObj.GP_Oracle_PID__c = '99998888';
        prjObj.GP_Oracle_Process_Log__c = 'Success';
        update prjObj;
        
        
        GPCheckRecursive.stopVerionCreation = false;
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new set<Id>{prjObj.id}, 'system', false,false);
        //GPControllerProjectClone projectCloneController = new GPControllerProjectClone(setProjectId, 'System', false,false);
        String strCloneProjectId = projectCloneController.cloneProject();
        system.debug('==Status1=='+GPDomainProject.skipRecursiveCall);
        GP_Project__c objClone = new GP_Project__c(id=strCloneProjectId );
        GPDomainProject.skipRecursiveCall = false;
        objClone.GP_Approval_status__c = 'Approved';
        objClone.ownerid = objuser.id;
        
        //GPDomainProject.skipRecursiveCall = false;
        update objClone; 
        system.debug('==Status2=='+GPDomainProject.skipRecursiveCall);
        objClone.GP_Oracle_Status__c ='S';
        GPDomainProject.skipRecursiveCall = false;
        update objClone; 
        Test.stopTest();
        //system.debug('==Version size==' + [Select id from GP_Project_Version_History__c where GP_Project__c  = :objClone.id ].size());
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objClone);
        PageReference pageRef = Page.GPGenerateProjectFileEXL;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('id',objClone.id);
        
        GPControllerGenerateProjectFile objFile = new GPControllerGenerateProjectFile(sc);        
    }    
    
    public static Void createTestData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_resource_allocation__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Sobject_Controller__c gettriggerSwitch1 = new GP_Sobject_Controller__c();
        gettriggerSwitch1.name = 'gp_project__c';
        gettriggerSwitch1.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch1;
        
        GP_Sobject_Controller__c gettriggerSwitch2 = new GP_Sobject_Controller__c();
        gettriggerSwitch2.name = 'GP_Project__Delete__c';
        gettriggerSwitch2.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch2;
        
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        insert objuser;  
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Project_count__c = 0;
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        //prjObj.RecordTypeId = Schema.SObjectType.GP_Project__c.getRecordTypeInfosByName().get('OTHER PBB').getRecordTypeId();
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_TCV__c = 234;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Project_type__c = 'Fixed monthly';
        prjObj.GP_Approval_status__c ='Pending for approval';
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-employee';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = false;
        update empObj;
        
        GP_Resource_Allocation__c objResourceAllocation1 = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation1.GP_Bill_Rate__c = 0;
        objResourceAllocation1.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation1.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation1.GP_Percentage_allocation__c = 1;
        insert objResourceAllocation1;
        
        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.Id);
        objResourceAllocation.GP_Bill_Rate__c = 0;
        objResourceAllocation.GP_Start_Date__c = date.newInstance(2018, 12, 12);
        objResourceAllocation.GP_End_Date__c = date.newInstance(2018, 01, 01);
        objResourceAllocation.GP_Percentage_allocation__c = 1;
        objResourceAllocation.GP_Parent_Resource_Allocation__c = objResourceAllocation1.id;
        insert objResourceAllocation;
        Update objResourceAllocation;
        system.debug('objResourceAllocation :'+objResourceAllocation);
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        objPrjBdgt.GP_TCV__c = 900000;
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Date__c = System.today();
        billingMilestone.GP_Milestone_start_date__c = System.today();
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Address__c gpAddr=new GP_Address__c();
        //gpAddr.CurrencyIsoCode='USD - U. S. Dollar';
        gpAddr.GP_City__c='test';
        gpAddr.GP_Country__c='test';
        gpAddr.GP_State__c='test';
        gpAddr.GP_Site_Use_Code__c='BILL_TO';
        insert gpAddr;
		
		GP_Address__c gpShipToAddr=new GP_Address__c();
        gpShipToAddr.GP_City__c='test';
        gpShipToAddr.GP_Country__c='test';
        gpShipToAddr.GP_State__c='test';
        gpShipToAddr.GP_Site_Use_Code__c='SHIP_TO';
        insert gpShipToAddr;
        
        GP_Project_Address__c projectAddress = new GP_Project_Address__c();
        projectAddress.GP_Project__c  = prjObj.Id;
        projectAddress.GP_Bill_To_Address__c = gpAddr.id;
        projectAddress.GP_Ship_To_Address__c = gpShipToAddr.id;        
        insert projectAddress;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
    } 
}