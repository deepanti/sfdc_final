public class OpportunityPrediscover_RemoveTasksHelper {
    
    public static void Prediscover_RemoveTasksMethod(list<Opportunity> oppList,Map<id,Opportunity> oldOppMap){
        set<Id> set_Id_Opp = new set<Id>();
        
        list<Task> list_Task = new list<Task>();
        list<Task> list_Task_toupdate = new list<Task>();
        
        for (Opportunity preopp : oppList) { 
            if((oldOppMap.get(preopp.id).NextStep != preopp.NextStep) && (preopp.StageName == 'Prediscover' || preopp.StageName == 'Pre-discover Dropped') || Test.isRunningTest()){ 
                set_Id_Opp.add(preopp.Id);
            }else{
                System.debug('--Opportunity has not been updated--');
            }
        }
        
        // Get the tasks and events
        if(!set_Id_Opp.isempty())
        {
            list_Task = [select Id,Status,Subject from Task where WhatId in :set_Id_Opp];
        }
        if(list_Task.size()>0)
        {
            for(Task T:list_Task)
            {    
                if(T.Status!='Completed' && T.Subject=='Next Step Note')
                {     
                    T.Status='Completed';
                    list_Task_toupdate.add(T); 
                }     
            }
        }
        
        if(!list_Task_toupdate.isempty())
            update list_Task_toupdate;
    }
    
}