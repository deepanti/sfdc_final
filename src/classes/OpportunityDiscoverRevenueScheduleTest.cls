/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing OpportunityDiscoverRevenueSchedule class
**************************************************************************************************************************/
@isTest (SeeAllData = false)
public class OpportunityDiscoverRevenueScheduleTest{
    public static User u;
    public static User testUser;
    public static Account acc;
    public static Contact con1;
    public static Opportunity oppList;
    public static List<Product2> pList;
    public static PricebookEntry standardPrice; 
    public static  Id pricebookId;
    public static List<OpportunityLineItem> oppLineItem; 
    public static List<OpportunityLineItemSchedule> oppLineItemSchedules;
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    public static void setUpData()
    {	
        acc = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
        insert acc;
        con1 = TestDataFactoryUtility.CreateContact('First','Last',acc.id,'Test','Test','fed@gtr.com','78342342343');
        insert con1;
        oppList = TestDataFactoryUtility.CreateOpportunity('Test',acc.id,con1.id);
        insert oppList;
        pList = TestDataFactoryUtility.createTestProducts2(1);
        insert pList;
        system.debug('pList'+ pList);
        pricebookId = Test.getStandardPricebookId();
        standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000, 
            IsActive = true);
        insert standardPrice;
        oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);
        insert oppLineItem;
        
        
        
    }
    
    
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverRevenueSchedule.getCampaignMembers mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void getOLISMapTest1()
    {
        setUpData();
        
        Test.startTest();
        oppLineItemSchedules = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItem OLI : oppLineItem)
        {
            oppLineItemSchedules.add(new OpportunityLineItemSchedule(
                OpportunityLineItemId = OLI.Id,
                ScheduleDate = System.today(),
                Revenue = 50000000.00,
                Type='Revenue'
            ));
            
        }
        insert oppLineItemSchedules;
        List<OpportunityDiscoverRevenueSchedule.ProductWrapper> wrapList = OpportunityDiscoverRevenueSchedule.getProductWrapperList(oppList.Id);
        /*testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac@bc.com','ac@bc.com');
insert testUser;

System.runAs(testUser){

OpportunityDiscoverAddPricing.saveOLI(oppLineItem);

List<OpportunityDiscoverRevenueSchedule.ProductWrapper> wrapList = OpportunityDiscoverRevenueSchedule.getOLISMap(oppList.Id);

Id oppId = [Select Id,Name From Opportunity Limit 1].id;
List<OpportunityLineItem> oliLst = OpportunityDiscoverRevenueSchedule.fetchOpportunityLineItems(oppId);
System.assertEquals(1, oliLst.size());

}*/
        Test.stopTest();
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles testing this mtd getProductWrapperList
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void getOLISMapTest2()
    {
        
        setUpData();
        
        Test.startTest();
        oppLineItemSchedules = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItem OLI : oppLineItem)
        {
            oppLineItemSchedules.add(new OpportunityLineItemSchedule(
                OpportunityLineItemId = OLI.Id,
                ScheduleDate = System.today(),
                Revenue = 50000000.00,
                Type='Revenue'
            ));
            
        }
        insert oppLineItemSchedules;
        OpportunityDiscoverRevenueSchedule.updateOpportunityLineItems(oppLineItem);
        Test.stopTest();
        
        /*   setUpData();
System.runAs(testUser){
Test.startTest();
OpportunityDiscoverAddPricing.saveOLI(oppLineItem);
List<OpportunityDiscoverRevenueSchedule.ProductWrapper> wrapList = OpportunityDiscoverRevenueSchedule.getProductWrapperList(oppList.Id);
Id oppId = [Select Id,Name From Opportunity Limit 1].id;
List<OpportunityLineItem> oliLst = OpportunityDiscoverRevenueSchedule.fetchOpportunityLineItems(oppId);
System.assertEquals(1, oliLst.size());
Test.stopTest();
}*/
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles testing this mtd SaveProductRevenueSchedule
* @return - void
**************************************************************************************************************************/
    public static testMethod void saveProductRevenueSchedule()
    {
        setUpData();
        
        Test.startTest();
        oppLineItemSchedules = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItem OLI : oppLineItem)
        {
            oppLineItemSchedules.add(new OpportunityLineItemSchedule(
                OpportunityLineItemId = OLI.Id,
                ScheduleDate = System.today(),
                Revenue = 50000000.00,
                Type='Revenue'
            ));
            
        }
        insert oppLineItemSchedules;
        List<OpportunityLineItem> oliLst = OpportunityDiscoverRevenueSchedule.fetchOpportunityLineItems(oppList.Id);
        string createList='[{"Id":"'+oppLineItem[0].Id+'","ScheduleDate":"2019-05-23","Revenue":"14","OpportunityLineItemId":"'+oppLineItem[0].Id+'","Type":"Revenue","OpportunityLineItem":{"Product2Id":"'+pList[0].Id+'","UnitPrice":12,"OpportunityId":"'+oppList.Id+'","Id":"'+oppLineItem[0].Id+'","Product2":{"Name":"3RD PARTY EARLY & LATE STAGE OUTBOUND COLLECTIONS","Id":"'+pList[0].Id+'","Nature_of_Work__c":"Managed Services","Service_Line__c":"Omni Channel Customer Acquisition and Servicing"}}}]';
        OpportunityDiscoverRevenueSchedule.SaveProductRevenueSchedule(createList);
        //OpportunityDiscoverRevenueSchedule.SaveProductRevenueSchedule([{'Id':'oppLineItem.Id','ScheduleDate':'2019-05-23','Revenue':'14','OpportunityLineItemId':'oppLineItem.Id','Type':'Revenue','OpportunityLineItem':{'Product2Id':'prod.Id','UnitPrice':12,'OpportunityId':'oppList[0].Id','Id':'oppLineItem.Id','Product2':{'Name':'3RD PARTY EARLY & LATE STAGE OUTBOUND COLLECTIONS','Id':'prod.Id','Nature_of_Work__c':'Managed Services','Service_Line__c':'Omni Channel Customer Acquisition and Servicing'}}}]);

        Test.stopTest();
        /* setUpData();
System.runAs(testUser){
Test.startTest();
Id oppId = [Select Id,Name From Opportunity Limit 1].id;
List<OpportunityLineItem> oliLst = OpportunityDiscoverRevenueSchedule.fetchOpportunityLineItems(oppId);
System.assertEquals(1, oliLst.size());
// OpportunityDiscoverRevenueSchedule.SaveProductRevenueSchedule(JSON.serialize(oppLineItem));
//OpportunityDiscoverRevenueSchedule.SaveProductRevenueSchedule([{'Id':'oppLineItem.Id','ScheduleDate':'2018-11-25','Revenue':0.5,'OpportunityLineItemId':'00kp0000004z2sPAAQ','Type':'Revenue','OpportunityLineItem':{'Product2Id':'prod.Id','UnitPrice':1,'Id':'oppLineItem.Id','Product2':{'Name':'Genpact Cora SeQuence-Professional Services','Id':'prod.Id','Nature_of_Work__c':'Digital','Service_Line__c':'F&A Multi-tower Consulting'}}}]);
Test.stopTest();
}*/
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles testing this mtd fetchOpportunityLineItems
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void fetchOpportunityLineItemsTest()
    {
         setUpData();
        
        Test.startTest();
        oppLineItemSchedules = new List<OpportunityLineItemSchedule>();
        for(OpportunityLineItem OLI : oppLineItem)
        {
            oppLineItemSchedules.add(new OpportunityLineItemSchedule(
                OpportunityLineItemId = OLI.Id,
                ScheduleDate = System.today(),
                Revenue = 50000000.00,
                Type='Revenue'
            ));
            
        }
        insert oppLineItemSchedules;
        List<OpportunityDiscoverRevenueSchedule.ProductWrapper> wrapList = OpportunityDiscoverRevenueSchedule.getOLISMap(oppList.Id);
        
        /* setUpData();
Test.startTest();
Id oppId = [Select Id,Name From Opportunity Limit 1].id;
List<OpportunityLineItem> oliLst = OpportunityDiscoverRevenueSchedule.fetchOpportunityLineItems(oppId);
for(OpportunityLineItem oli: oliLst)
{
oli.EBIT_OLI_CPQ__c = '35';
}
OpportunityDiscoverRevenueSchedule.updateOpportunityLineItems(oliLst);
System.assertEquals(1, oliLst.size());
Test.stopTest();*/
    }  
}