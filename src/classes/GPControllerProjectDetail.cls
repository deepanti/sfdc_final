/**
 * @author Anmol.kumar
 * @date 22/10/2017
 *
 * @group ProjectDetails
 * @group-content ../../ApexDocContent/ProjectDetGPControllerProjectWorkLocationails.html
 *
 * @description Apex Service for project Detail module,
 * has aura enabled method to fetch project Detail data.
 */
public without sharing class GPControllerProjectDetail {

    private static final String EMPTY_DEAL_ERROR_LABEL = 'Please provide deal';
    private static final String SUCCESS_LABEL = 'SUCCESS';

    private GP_Project__c project;
    private JSONGenerator gen;
    private Id projectId;
    /**
     * @description Returns Account record with 
     *            Name, 
     *            Business_Group__c,
     *            Business_Segment__r.Name,
     *            Sub_Business__r.Name.
     * @param Id accountID
     * 
     * @return GPAuraResponse json of Account data
     * 
     * @example
     * GPCmpServiceProjectDetail.getAccountRecord(<accountID>);
     */
    public static GPAuraResponse getAccountRecord(Id accountID) {
        GPSelectorAccount accountSelector;
        Account accountObj;

        try {
            accountSelector = new GPSelectorAccount();
            accountObj = accountSelector.getAccountWithBusiness(accountID);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(accountObj));
    }

    /**
     * @description returns Customer master record in order to auto populate (L1, L2, L3, Vertical & Sub-Vertical).
     * List of filtered service line, Map of service line to Nature of work and map of nature of work to product 
     * from product master based on vertical of customer L4
     *
     * @param Id accountID
     * 
     * @return GPAuraResponse json of Account data
     * 
     * @example
     * GPCmpServiceProjectDetail.getVerticalSubVerticalForAccount(<accountID>);
     */
    public static GPAuraResponse getVerticalSubVerticalForAccount(Id accountID) {
        GPSelectorAccount accountSelector;
        Account accountObj;

        try {
            accountSelector = new GPSelectorAccount();
            accountObj = accountSelector.getVerticalSubVertical(accountID);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        Id loggedInUserId = UserInfo.getUserId();
        GP_Pinnacle_Master__c globalSetting;
        Date today = System.today();


        Set < String > setOfServiceLine = new Set < String > ();

        Map < String, Set < String >> mapOfServiceLineToNatureOfWork = new Map < String, Set < String >> ();
        Map < String, List < GPOptions >> mapOfNatureOfWorkToProduct = new Map < String, List < GPOptions >> ();
        Map < String, Boolean > mapOfConcatenatedNatureOfWorkAndProduct = new Map < String, Boolean > ();

        for (GP_Product_Master__c productMaster: GPSelectorProductMaster.getAllProductMaster(accountObj.Industry_Vertical__c)) {
            if (productMaster.GP_Service_Line__c == null) {
                continue;
            }

            setOfServiceLine.add(productMaster.GP_Service_Line__c);

            String concatenatedServiceLineAndNatureOfWork = productMaster.GP_Service_Line__c + '--' + productMaster.GP_Nature_of_Work__c;

            if (productMaster.GP_Service_Line__c != null) {
                //add map of service line to nature of work.
                if (!mapOfServiceLineToNatureOfWork.containsKey(productMaster.GP_Service_Line__c)) {
                    mapOfServiceLineToNatureOfWork.put(productMaster.GP_Service_Line__c, new Set < String > ());
                }

                mapOfServiceLineToNatureOfWork.get(productMaster.GP_Service_Line__c)
                    .add(productMaster.GP_Nature_of_Work__c);

            }

            if (productMaster.GP_Nature_of_Work__c != null) {
                //add map of nature of work to product family
                if (!mapOfNatureOfWorkToProduct.containsKey(concatenatedServiceLineAndNatureOfWork)) {
                    mapOfNatureOfWorkToProduct.put(concatenatedServiceLineAndNatureOfWork, new List < GPOptions > ());
                }

                String concatenatedNatureOfWorkAndProduct = productMaster.GP_Service_Line__c + '--' + productMaster.GP_Nature_of_Work__c + '--' + productMaster.GP_Product_Name__c;

                if (!mapOfConcatenatedNatureOfWorkAndProduct.containsKey(concatenatedNatureOfWorkAndProduct)) {
                    mapOfNatureOfWorkToProduct.get(concatenatedServiceLineAndNatureOfWork)
                        .add(new GPOptions(productMaster.GP_Product_ID__c, productMaster.GP_Product_Name__c));
                    mapOfConcatenatedNatureOfWorkAndProduct.put(concatenatedNatureOfWorkAndProduct, true);
                }
            }
        }

        JSONGenerator gen = JSON.createGenerator(true);

        gen.writeStartObject();

        if (setOfServiceLine != null) {
            gen.writeObjectField('setOfServiceLine', setOfServiceLine);
        }

        if (mapOfServiceLineToNatureOfWork != null) {
            gen.writeObjectField('mapOfServiceLineToNatureOfWork', mapOfServiceLineToNatureOfWork);
        }

        if (mapOfNatureOfWorkToProduct != null) {
            gen.writeObjectField('mapOfNatureOfWorkToProduct', mapOfNatureOfWorkToProduct);
        }


        if (accountObj != null) {
            gen.writeObjectField('accountData', accountObj);
        }

        gen.writeEndObject();

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description Returns Deal data to be set as default on project.
     * @param Id dealId
     * 
     * @return GPAuraResponse json of Deal data
     * 
     * @example
     * GPCmpServiceProjectDetail.getDefaultProjectData(<dealId>);
     */
    public static GPAuraResponse getDefaultProjectData(Id dealId) {
        GP_Deal__c deal;
        GPSelectorDeal dealSelectorObj;

        try {
            dealSelectorObj = new GPSelectorDeal();
            deal = dealSelectorObj.selectDealWithOpportunity(dealId);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(deal));
    }

    /**
     * @description Saves/Updates project.
     * @param String strProjectRecord
     * 
     * @return GPAuraResponse status if project is saved successfully or not
     * 
     * @example
     * GPCmpServiceProjectDetail.saveProject(<strProjectRecord>);
     */
    public static GPAuraResponse saveProject(String strProjectRecord) {
        GP_Project__c projectRecord;

        try {
            projectRecord = (GP_Project__c) JSON.deserialize(strProjectRecord, GP_Project__c.class);
            // Update project short name to project name to keep both the fields in sync. 
            // as short name is used in integration
            projectRecord.GP_Project_Short_Name__c = projectRecord.Name;

            upsert projectRecord;
            //GPServiceProject.updateEmployeeEndDateInOldVersion(projectRecord);
            //check if the project type is BPM then update the deal Nusiness name to BPM 
            //so that other projects can be created under ths deal.
            Id bpmRecordTypeId = GPCommon.getRecordTypeId('GP_Project__c', 'BPM');

            if (projectRecord.RecordTypeId == bpmRecordTypeId) {
                GPDomainDeal.isBPM = true;
                GP_Deal__c bpmDeal = new GP_Deal__c(Id = projectRecord.GP_Deal__c, GP_Business_Name__c = 'BPM');
                update bpmDeal;
            }

        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(projectRecord.Id));
    }

    /**
     * @description Saves project and deal in case of legacy project creation.
     * @param String strProjectRecord
     * @param String strDealRecord
     * 
     * @return GPAuraResponse status if project is saved successfully or not
     * 
     * @example
     * GPCmpServiceProjectDetail.saveLegacyProject(<strProjectRecord>, <strDealRecord>);
     */
    public static GPAuraResponse saveLegacyProject(String strProjectRecord, String strDealRecord) {
        GP_Project__c projectRecord;
        GP_Deal__c dealRecord;

        Savepoint savePoint;

        try {
            projectRecord = (GP_Project__c) JSON.deserialize(strProjectRecord, GP_Project__c.class);
            dealRecord = (GP_Deal__c) JSON.deserialize(strDealRecord, GP_Deal__c.class);
            dealRecord.Name = projectRecord.Name;

            savePoint = Database.setSavepoint();

            insert dealRecord;

            //GP_Project_Template__c defaultProjectTemplate = GPSelectorProjectTemplate.getProjectTemplateByType(dealRecord);

            projectRecord.GP_Deal__c = dealRecord.Id;
            //projectRecord.GP_Project_Template__c = defaultProjectTemplate.Id;
            //projectRecord.GP_Business_Type__c = dealRecord.GP_Business_Type__c;
            //projectRecord.GP_Business_Name__c = dealRecord.GP_Business_Name__c;
            projectRecord.RecordType = null;
            projectRecord.GP_Project_Short_Name__c = projectRecord.Name;
            upsert projectRecord;
            GPServiceProject.updateEmployeeEndDateInOldVersion(projectRecord);
        } catch (Exception ex) {
            Database.rollback(savePoint);
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(projectRecord.Id));
    }

    /**
     * @description Returns record types under project, for the API Name(GP_Project__c).
     * @param String sObjectName
     * 
     * @return GPAuraResponse status if project is saved successfully or not
     * 
     * @example
     * GPCmpServiceProjectDetail.getRecordType(<sObjectName>);
     */
    public static GPAuraResponse getRecordType(String sObjectName) {
        List < RecordType > lstOfRecordType;

        try {
            lstOfRecordType = GPCommon.getRecordTypes(sObjectName);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(lstOfRecordType, true));
    }

    /**
     * @description Returns Currency for Operating unit.
     * @param String operatingUnitID
     * 
     * @return GPAuraResponse Currency for Operating unit.
     * 
     * @example
     * GPCmpServiceProjectDetail.getOUCurrency(<operatingUnitID>);
     */
    public static GPAuraResponse getOUCurrency(Id operatingUnitID) {
        GP_Pinnacle_Master__c operatingUnit;

        try {
            operatingUnit = GPSelectorPinnacleMasters.selectByOperatingUnitId(operatingUnitID);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(operatingUnit, true));
    }

    /**
     * @description Returns Deal data to be used for template selection.
     * @param String dealId
     * 
     * @return GPAuraResponse Deal data to be used for template selection.
     * 
     * @example
     * GPCmpServiceProjectDetail.getDealData(<dealId>);
     */
    public static GPAuraResponse getDealData(Id dealId) {
        GP_Deal__c dealData;

        try {
            dealData = GPSelectorDeal.getDealDataForTemplateSelection(dealId);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(dealData, true));
    }

    /**
     * @description Returns all Approved projects under a deal.
     * @param String dealId
     * 
     * @return GPAuraResponse all Approved projects under a deal.
     * 
     * @example
     * GPCmpServiceProjectDetail.getProjectsForDeal(<dealId>);
     */
    public static GPAuraResponse getProjectsForDeal(Id dealId) {
        List < GP_Project__c > listOfProjectsUnderDeal;

        try {
            listOfProjectsUnderDeal = GPSelectorProject.getApprovedProjectUnderDeal(dealId);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfProjectsUnderDeal, true));
    }

    /**
     * @description Clones a BPM Project.
     * @param String projectId
     * 
     * @return GPAuraResponse Clones a BPM Project.
     * 
     * @example
     * GPCmpServiceProjectDetail.cloneBPMProject(<projectId>);
     */
    public static GPAuraResponse cloneBPMProject(Id projectId, Id dealId) {
        
        // 09-04-19:DS: Modified the clone source param to clone from manual.
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new Set < Id > { projectId }, 'clone', true, false);

        String response = projectCloneController.cloneProject(); //GPServiceProject.cloneProject(, 'manual', true);

        if (response.containsIgnoreCase('error')) {
            return new GPAuraResponse(false, response, null);
        } else {
            copyDealDataToClonedProject(response, dealId);
            return new GPAuraResponse(true, SUCCESS_LABEL, response);
        }
    }
	
    private static void copyDealDataToClonedProject(String clonedProjectId, Id dealId){
        GP_Deal__c dealData =  new GPSelectorDeal().selectDealWithOpportunity(dealId);
        GP_Project__c projectRecord = new GP_Project__c(Id = clonedProjectId);
        projectRecord.GP_Deal__c = dealData.Id;
        projectRecord.GP_Opportunity_Name__c = dealData.GP_Opportunity_Name__c;
        projectRecord.GP_Customer_Hierarchy_L4__c = dealData.GP_Account_Name_L4__c;

        projectRecord.GP_Business_Group_L1__c = dealData.GP_Business_Group_L1__c;
        projectRecord.GP_Business_Segment_L2__c = dealData.GP_Business_Segment_L2__c;
        projectRecord.GP_Sub_Business_L3__c = dealData.GP_Sub_Business_L3__c;

        projectRecord.GP_Delivery_Org__c = dealData.GP_Delivery_Org__c;
        projectRecord.GP_Sub_Delivery_Org__c = dealData.GP_Sub_Delivery_Org__c;

        projectRecord.GP_Vertical__c = dealData.GP_Vertical__c;
        projectRecord.GP_Sub_Vertical__c = dealData.GP_Sub_Vertical__c;
        projectRecord.GP_Product_Family__c = dealData.GP_Product_Family__c;
        projectRecord.GP_Nature_of_Work__c = dealData.GP_Nature_of_Work__c;
		projectRecord.GP_Product__c = dealData.GP_Product__c;
        projectRecord.GP_Product_Id__c = dealData.GP_Product_Id__c;
        
        projectRecord.GP_Revenue_Description__c = dealData.GP_Revenue_Description__c;
        projectRecord.GP_OMS_Deal_ID__c = dealData.GP_OMS_Deal_ID__c;
        
        projectRecord.GP_Service_Line__c = dealData.GP_Service_Line__c;
        projectRecord.GP_Sales_Opportunity_Id__c = dealData.GP_Sales_Opportunity_Id__c;
        projectRecord.GP_OLI_SFDC_Id__c = dealData.GP_OLI_SFDC_Id__c;
        
        system.debug('==dealData=='+dealData);
		update projectRecord;
    }
    
    /**
     * @description returns map of picklist options to be used in case of legacy project creation.
     * 
     * @return GPAuraResponse map of picklist options to be used in case of legacy project creation.
     * 
     * @example
     * GPCmpServiceProjectDetail.getDealPicklist();
     */
    public static GPAuraResponse getDealPicklist() {

        Id loggedInUserId = UserInfo.getUserId();
        GP_Pinnacle_Master__c globalSetting;
        Date today = System.today();

        Map < String, Boolean > mapOfConcatenatedNatureOfWorkAndProduct = new Map < String, Boolean > ();
        Set < String > setOfBusinessType = new Set < String > ();

        Map < String, Set < String >> mapOfBusinessTypeToBusinessName = new Map < String, Set < String >> ();

        Set < String > setOfServiceLine = new Set < String > ();
        Map < String, Set < String >> mapOfServiceLineToNatureOfWork = new Map < String, Set < String >> ();
        Map < String, List < GPOptions >> mapOfNatureOfWorkToProduct = new Map < String, List < GPOptions >> ();

        for (GP_Product_Master__c productMaster: GPSelectorProductMaster.getAllProductMaster()) {
            if (productMaster.GP_Service_Line__c == null) {
                continue;
            }

            setOfServiceLine.add(productMaster.GP_Service_Line__c);

            //add map of service line to nature of work.
            if (!mapOfServiceLineToNatureOfWork.containsKey(productMaster.GP_Service_Line__c)) {
                mapOfServiceLineToNatureOfWork.put(productMaster.GP_Service_Line__c, new Set < String > ());
            }

            mapOfServiceLineToNatureOfWork.get(productMaster.GP_Service_Line__c)
                .add(productMaster.GP_Nature_of_Work__c);

            if (productMaster.GP_Nature_of_Work__c != null) {
                String concatenatedServiceLineAndNatureOfWork = productMaster.GP_Service_Line__c + '--' + productMaster.GP_Nature_of_Work__c;
                //add map of nature of work to product family
                if (!mapOfNatureOfWorkToProduct.containsKey(concatenatedServiceLineAndNatureOfWork)) {
                    mapOfNatureOfWorkToProduct.put(concatenatedServiceLineAndNatureOfWork, new List < GPOptions > ());
                }

                String concatenatedNatureOfWorkAndProduct = productMaster.GP_Service_Line__c + '--' + productMaster.GP_Nature_of_Work__c + '--' + productMaster.GP_Product_Name__c;

                if (!mapOfConcatenatedNatureOfWorkAndProduct.containsKey(concatenatedNatureOfWorkAndProduct)) {
                    mapOfNatureOfWorkToProduct.get(concatenatedServiceLineAndNatureOfWork)
                        .add(new GPOptions(productMaster.GP_Product_ID__c, productMaster.GP_Product_Name__c));
                    mapOfConcatenatedNatureOfWorkAndProduct.put(concatenatedNatureOfWorkAndProduct, true);
                }
            }
        }
        system.debug('mapOfConcatenatedNatureOfWorkAndProduct' + JSON.serialize(mapOfConcatenatedNatureOfWorkAndProduct));
        system.debug('mapOfNatureOfWorkToProduct' + JSON.serialize(mapOfNatureOfWorkToProduct));

        try {
            globalSetting = GPSelectorPinnacleMasters.selectByRecordTypeName('Global Settings');
        } catch (Exception ex) {
            System.debug('Error: ' + ex.getMessage());
        }

        for (GP_User_Role__c pidCreatorRole: GPSelectorUserRole.selectPIDCreatorRoles()) {
            String businessType = pidCreatorRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c;
            String businessName = pidCreatorRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Name__c;

            if (businessType == null) {
                continue;
            }

            setOfBusinessType.add(businessType);

            if (!mapOfBusinessTypeToBusinessName.containsKey(businessType)) {
                mapOfBusinessTypeToBusinessName.put(businessType, new Set < String > ());
            }

            mapOfBusinessTypeToBusinessName.get(businessType).add(businessName);
        }

        JSONGenerator gen = JSON.createGenerator(true);

        gen.writeStartObject();

        if (setOfServiceLine != null)
            gen.writeObjectField('setOfServiceLine', setOfServiceLine);

        if (mapOfServiceLineToNatureOfWork != null)
            gen.writeObjectField('mapOfServiceLineToNatureOfWork', mapOfServiceLineToNatureOfWork);

        if (mapOfNatureOfWorkToProduct != null)
            gen.writeObjectField('mapOfNatureOfWorkToProduct', mapOfNatureOfWorkToProduct);

        if (globalSetting != null)
            gen.writeObjectField('deliverySubDeliveryMapping', globalSetting);

        if (setOfBusinessType != null)
            gen.writeObjectField('setOfBusinessType', setOfBusinessType);

        if (mapOfBusinessTypeToBusinessName != null)
            gen.writeObjectField('mapOfBusinessTypeToBusinessName', mapOfBusinessTypeToBusinessName);

        gen.writeEndObject();

        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
     * @description returns CRN Data for CRN NumberId.
     * @param Id CRNNumberId
     * 
     * @return GPAuraResponse CRN Data for CRN NumberId.
     * 
     * @example
     * GPCmpServiceProjectDetail.getCRNData(<CRNNumberId>);
     */
    public static GPAuraResponse getCRNData(Id CRNNumberId) {
        GPSelectorIconMaster iconMasterSelector = new GPSelectorIconMaster();
        GP_Icon_Master__c crnData;

        try {
            crnData = iconMasterSelector.selectICONRecord(CRNNumberId);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(crnData));
    }

    /**
     * @description returns Parent project for group invoice.
     * @param Id serializedProject
     * 
     * @return GPAuraResponse Parent project for group invoice.
     * 
     * @example
     * GPCmpServiceProjectDetail.getParentProjectForGroupInvoice(<serializedProject>);
     */
    public static GPAuraResponse getParentProjectForGroupInvoice(String serializedProject) {
        GP_Project__c project;
        List < GPOptions > listOfParentProjectForGroupInvoiceOptions = new List < GPOptions > ();


        try {
            project = (GP_Project__c) JSON.deserialize(serializedProject, GP_Project__c.class);
            GP_Project_Address__c directProjectAddress = GPSelectorProjectAddress.selectDirectProjectAddressUnderProject(project.Id);

            for (GP_Project__c parentProject: GPSelectorProject.getParentProjectForGroupInvoice(directProjectAddress)) {
                listOfParentProjectForGroupInvoiceOptions.add(new GPOptions(parentProject.GP_Oracle_PID__c, parentProject.Name));
            }

        } catch (Exception ex) {
            System.System.debug('ex.getMessage()' + ex.getMessage());
            return new GPAuraResponse(true, '', JSON.serialize(listOfParentProjectForGroupInvoiceOptions));
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfParentProjectForGroupInvoiceOptions));
    }

    /**
     * @description Checks if the user is allowed to create legacy project.
     * 
     * @return GPAuraResponse if the user is allowed to create legacy project
     * 
     * @example
     * GPCmpServiceProjectDetail.getIsAvailableForLegacyProjectCreation();
     */
    public static GPAuraResponse getIsAvailableForLegacyProjectCreation() {
        Boolean isAvailableForLegacyProjectCreation = false;
        Id loggedInUserId = UserInfo.getUserId();
        Date today = System.today();

        List < GP_User_Role__c > listOfUserRole = GPSelectorUserRole.selectPIDCreatorRoles();

        isAvailableForLegacyProjectCreation = listOfUserRole.size() > 0;

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(isAvailableForLegacyProjectCreation));
    }

    /**
     * @description returns filtered SDO for legacy project.
     * @param Id isLegacyProject
     * 
     * @return GPAuraResponse filtered SDO for legacy project.
     * 
     * @example
     * GPCmpServiceProjectDetail.getFilteredSOD(isLegacyProject);
     */
    public static GPAuraResponse getFilteredSOD(Boolean isLegacyProject, Boolean isInterCOEProject) {
        List < GPOptions > listOfSDOOptions = new List < GPOptions > ();
        Id loggedInUserId = UserInfo.getUserId();
        List < GP_User_Role__c > listOfUserRole;

        if (isLegacyProject) {
            return getGlobalSDO();
        } else {
            return getUserSDO(isInterCOEProject);
        }
    }

    private static GPAuraResponse getGlobalSDO() {

        List < GPOptions > listOfSDOOptions = new List < GPOptions > ();
        Id loggedInUserId = UserInfo.getUserId();
        List < GP_User_Role__c > listOfUserRole = GPSelectorUserRole.selectLegacyPIDCreatorRoles();

        for (GP_User_Role__c userRole: listOfUserRole) {
            listOfSDOOptions.add(
                new GPOptions(userRole.GP_Role__r.GP_Work_Location_SDO_Master__c,
                    userRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Unique_Name__c,
                    userRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c));
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfSDOOptions));
    }

    private static GPAuraResponse getUserSDO(Boolean isInterCOEProject) {
        List < GPOptions > listOfSDOOptions = new List < GPOptions > ();
        Id loggedInUserId = UserInfo.getUserId();
        List < GP_User_Role__c > listOfUserRole = GPSelectorUserRole.selectPIDCreatorRoles();

        for (GP_User_Role__c userRole: listOfUserRole) {
            if ((isInterCOEProject && userRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c == 'PBB') || !isInterCOEProject)
                listOfSDOOptions.add(
                    new GPOptions(userRole.GP_Role__r.GP_Work_Location_SDO_Master__c,
                        userRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Unique_Name__c,
                        userRole.GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c));
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfSDOOptions));
    }

    /**
     * @description returns filtered list of TAX code.
     * @param Id operatingUnitOracleID
     * 
     * @return GPAuraResponse filtered list of TAX code.
     * 
     * @example
     * GPCmpServiceProjectDetail.getFilteredListOfTaxCode(operatingUnitOracleID);
     */
    public static GPAuraResponse getFilteredListOfTaxCode(String operatingUnitOracleID) {

        if (operatingUnitOracleID == null) {
            return new GPAuraResponse(false, 'Please provide operating Unit Id', null);
        }

        List < GPOptions > listOfTaxCode = new List < GPOptions > ();

        for (GP_Pinnacle_Master__c taxCode: GPSelectorPinnacleMasters.getTaxCodeRecord(operatingUnitOracleID)) {
            listOfTaxCode.add(new GPOptions(taxCode.Name, taxCode.Name));
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfTaxCode));
    }

    public static GPAuraResponse getFinalProjectTemplate(String sdoId, String businessGroupL1, String businessSegmentL2, Boolean isIndirect, Boolean isWithoutSFDC, Boolean isInterCOEProject) {
        GP_Project_Template__c projectTemplate;

        GP_Work_Location__c sdo;
        String projectTemplateQueryString;

        try {
            if (!isIndirect) {
                sdo = [SELECT Id, GP_Business_Type__c, GP_Business_Name__c
                    FROM GP_Work_Location__c
                    where Id =: sdoId
                ];
            }

            projectTemplate = getProjectTemplate(sdo, businessGroupL1, businessSegmentL2, isIndirect, isWithoutSFDC, isInterCOEProject);

            if (projectTemplate == null) {
                return new GPAuraResponse(false, 'No template is available, please contact Admin/Template Configurator', null);
            }


        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(projectTemplate));
    }

    //Template Selection Logic based on Business Type, Business Name L1 & L2
    //Field Name  Business Type   Business Name   businessGroupL1  businessSegmentL2  Template Name
    //Case 1        Indirect        Indirect            All             All             Indirect
    //Case 2        BPM             BPM                 All             All             BPM All
    //Case 6        PBB             CMITS               GC              Care Mark       CMITS - GC - Care Mark
    //Case 7        PBB             CMITS               GC              All             CMITS - GC - All
    //Case 5        PBB             CMITS               GC              MS              CMITS - GC - MS
    //Case 4        PBB             CMITS               GE              All             CMITS - GE - All
    //Case 3        PBB             Consulting          All             All             Other PBB
    private static GP_Project_Template__c getProjectTemplate(GP_Work_Location__c sdo, String businessGroupL1, String businessSegmentL2, Boolean isIndirect, Boolean isWithoutSFDC, Boolean isInterCOEProject) {

        Map < string, GP_Project_Template__c > mapBTBNL1L2toPrjTemplate = new Map < string, GP_Project_Template__c > ();
        List < GP_Project_Template__c > lstOfProjectTemplate;

        String projectTemplateQueryString = 'SELECT Id,GP_Business_Type__c, GP_Business_Name__c, GP_Business_Group_L1__c, GP_Business_Segment_L2__c, GP_Final_JSON_1__c, GP_Final_JSON_2__c, GP_Final_JSON_3__c';
        projectTemplateQueryString += ' FROM GP_Project_Template__c';
        projectTemplateQueryString += ' WHERE GP_Active__c = true';


        String businessType,
        businessName;

        if (isIndirect) {
            businessType = 'Indirect';
            businessName = 'Indirect';
            businessGroupL1 = 'All';
            businessSegmentL2 = 'All';
        } else if (isInterCOEProject) {
            businessType = 'PBB';
            businessName = 'All';
            businessGroupL1 = 'All';
            businessSegmentL2 = 'All';
            businessType = sdo.GP_Business_Type__c;
            businessName = sdo.GP_Business_Name__c;
        } else {
            businessType = sdo.GP_Business_Type__c;
            businessName = sdo.GP_Business_Name__c;
        }

        projectTemplateQueryString += ' AND GP_Business_Type__c = \'' + businessType + '\'';

        if (isWithoutSFDC) {
            projectTemplateQueryString += ' AND GP_Without_SFDC__c = true AND GP_Inter_COE__c = false'; //commented by amitppt//20032018
        } else if (isInterCOEProject) {
            projectTemplateQueryString += ' AND GP_Without_SFDC__c = false AND GP_Inter_COE__c = true'; //commented by amitppt//20032018
        } else {
            projectTemplateQueryString += ' AND GP_Without_SFDC__c = false AND GP_Inter_COE__c = false'; //commented by amitppt//20032018
        }

        //projectTemplateQueryString += ' AND GP_Business_Name__c = \'' + businessName + '\'';//commented by amitppt//20032018

        //system.assert(Database.query(projectTemplateQueryString).size() > 0);
        for (GP_Project_Template__c projectTemplate: Database.query(projectTemplateQueryString)) {
            string strKey = projectTemplate.GP_Business_Type__c + '_' + projectTemplate.GP_Business_Name__c;
            strKey += '_' + projectTemplate.GP_Business_Group_L1__c + '_' + projectTemplate.GP_Business_Segment_L2__c;

            if (!mapBTBNL1L2toPrjTemplate.containsKey(strKey)) {
                mapBTBNL1L2toPrjTemplate.put(strKey, projectTemplate);
            }
        }

        string strProjectKey = businessType + '_' + businessName + '_' + businessGroupL1 + '_' + businessSegmentL2;
        //system.assertEquals(strProjectKey,'');    
        if (mapBTBNL1L2toPrjTemplate != null && mapBTBNL1L2toPrjTemplate.containsKey(strProjectKey)) {
            return mapBTBNL1L2toPrjTemplate.get(strProjectKey);
        }

        strProjectKey = businessType + '_' + businessName + '_' + businessGroupL1 + '_All';

        if (mapBTBNL1L2toPrjTemplate != null && mapBTBNL1L2toPrjTemplate.containsKey(strProjectKey)) {
            return mapBTBNL1L2toPrjTemplate.get(strProjectKey);
        }

        strProjectKey = businessType + '_' + businessName + '_All_All';

        if (mapBTBNL1L2toPrjTemplate != null && mapBTBNL1L2toPrjTemplate.containsKey(strProjectKey)) {
            return mapBTBNL1L2toPrjTemplate.get(strProjectKey);
        }

        strProjectKey = businessType + '_All_All_All';

        if (mapBTBNL1L2toPrjTemplate != null && mapBTBNL1L2toPrjTemplate.containsKey(strProjectKey)) {
            return mapBTBNL1L2toPrjTemplate.get(strProjectKey);
        }

        return null;
    }

    public static GPAuraResponse getSDOData(Id sdoId) {
        GP_Work_Location__c sdo;

        try {
            sdo = GPSelectorWorkLocationSDOMaster.selectSODById(sdoId);
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(sdo));
    }

    public static GPAuraResponse getDefaultOUForIndirectProject() {
        GP_Pinnacle_Master__c defaultIndirectProjectOperatingUnit;
        try {
            defaultIndirectProjectOperatingUnit = GPSelectorPinnacleMasters.getGenpactInternationalIncOperatingUnit();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(defaultIndirectProjectOperatingUnit));
    }
}