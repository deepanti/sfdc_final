@isTest
private class GPDomainProjectExpenseTracker {
    
    private static GP_Project__c project;
    
    @testSetup static void setupCommonData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_expense__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMasterForIcon = new GP_Icon_Master__c();
        objIconMasterForIcon.GP_CONTRACT__c = 'Test Contract';
        objIconMasterForIcon.GP_Status__C = 'Expired';
        objIconMasterForIcon.GP_END_DATE__c = date.newInstance(2017, 12, 25);
        objIconMasterForIcon.GP_START_DATE__c = system.today();
        insert objIconMasterForIcon;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        GP_Role__c objroleforShare = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objroleforShare.GP_Type_of_Role__c = 'Delivery Org';
        objroleforShare.GP_HSL_Master__c = null;
        objroleforShare.GP_Work_Location_SDO_Master__c = null;
        insert objroleforShare;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        User objuserForProjet = GPCommonTracker.getUser();
        objuserForProjet.LastName = 'Test User For Project';
        objuserForProjet.Email = 'xyz@gmail.com';
        objuserForProjet.Username = 'sal@sales.com';
        insert objuserForProjet; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        insert prjObj ;
    }
    
    @isTest
    private static void testGPDomainProjectExpense() {
        fetchData();
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(project.Id);
        insert projectExpense;
        GP_Project_Expense__c projectExpense1 = GPCommonTracker.getProjectExpense(project.Id);
        insert projectExpense1;
        projectExpense.GP_Parent_Project_Expense__c = projectExpense1.Id;
        update projectExpense;
    }
    
    public static void fetchData() {
        project = [select id, Name from GP_Project__c limit 1];
    }
}