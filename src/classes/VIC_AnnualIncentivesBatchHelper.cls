/*
* @author: Prashant Kumar1
* @since: 11/06/2018
* @description: 
* 
*/
public class VIC_AnnualIncentivesBatchHelper {
    
   /*
    * @params: List of UserIds(List<Id>)
    * @return: Map of userId Vs Plan(Map<Id, String>)
    * @description: This method returns the userId vs PlanCodeMap
    * 
    */
    public Map<Id, String> getUsersVsPlanMap(Set<Id> userIds){
        Map<Id, String> usersVsPlanCodes = new Map<Id, String>();
        Map<Id, Id> usersVsMasterVICRole = new Map<Id, Id>();
        Set<Id> masterVICRoles = new Set<Id>();
        Map<Id, String> masterVICRoleVsPlanCode = new Map<Id, String>();
        
        LIst<User_VIC_Role__c> userVICRoleMappings = [SELECT Id, Master_VIC_Role__c, User__c 
                                                      FROM User_VIC_Role__c 
                                                      WHERE User__c =: userIds 
                                                      AND vic_For_Previous_Year__c = false
                                                      AND User__r.isActive = true
                                                      AND Not_Applicable_for_VIC__c = false];
        
        for(User_VIC_Role__c obj: userVICRoleMappings){
            usersVsMasterVICRole.put(obj.User__c, obj.Master_VIC_Role__c);
            masterVICRoles.add(obj.Master_VIC_Role__c);
        }
        
        //Current year will be taken from custome settings later
        String currentYearStr = String.valueOf(System.today().year());
        
        List<VIC_Role__c> vicRoles = [SELECT Id, Plan__r.vic_Plan_Code__c, Master_VIC_Role__c 
                                      FROM VIC_Role__c 
                                      WHERE Master_VIC_Role__c =:masterVICRoles 
                                      AND Plan__r.Year__c =: currentYearStr 
                                      AND  Plan__r.Is_Active__c = true
                                      AND Plan__r.vic_Plan_Code__c != null];
        
        for(VIC_Role__c obj: vicRoles){
            masterVICRoleVsPlanCode.put(obj.Master_VIC_Role__c, obj.Plan__r.vic_Plan_Code__c);
        }
        
        // creating the required map
        for(Id userId : usersVsMasterVICRole.keySet()){
            usersVsPlanCodes.put(userId, masterVICRoleVsPlanCode.get(usersVsMasterVICRole.get(userId)));
        }
        
        return usersVsPlanCodes;
    }
    
}