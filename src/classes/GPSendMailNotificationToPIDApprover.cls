public class GPSendMailNotificationToPIDApprover {
    //GP_SDO_Code__c,
    // GP_Project_type__c,
    // GP_Business_Type__c,
    // GP_Start_Date__c,
    // GP_End_Date__c,
    // GP_Project_Organization__r.Name, 
    
    public static void SentEmailNotification(List<GP_Project__c> projectLst)
    {       
        system.debug('===projectLst==='+projectLst);
        Map < String, String > mapOfPIDNotificationActive = (Map < String, String > ) JSON.deserialize(System.label.GP_PID_Email_Notofication, Map < String, String > .class);
        if(projectLst == null)
        {
            
            List<GP_Project__c> lstProject = [Select Id, Name, 
                                              GP_Project_Submission_Date__c, 
                                              GP_EP_Project_Number__c,
                                              GP_Approval_Status__c, 
                                              GP_Approver_Ids__c,                                             
                                              GP_Primary_SDO__r.Name,
                                              GP_Project_type__c,
                                              GP_Business_Type__c,
                                              GP_Business_Name__c,
                                              GP_Start_Date__c,
                                              GP_End_Date__c,
                                              GP_Project_Organization__r.Name,
                                              GP_Current_Working_User__r.Name,
                                              GP_Current_Working_User__r.Email,
                                              GP_Current_Working_User__r.OHR_ID__c,
                                              GP_PID_Approver_User__r.Name,
                                              GP_PID_Approver_User__r.OHR_ID__c,
                                              GP_Oracle_PID__c,
                                              GP_Approval_Requester_Comments__c
                                              From GP_Project__c 
                                              where GP_Approval_Status__c = 'Pending for Approval'
                                              and GP_Project_Submission_Date__c != null
                                              and GP_Approver_Ids__c != null    
                                              
                                             ];
            //get all project who has completed 7 working days after Submission of project
            List<GP_Project__c> sevenDayfromSubLstProject = new List<GP_Project__c>();
            List<GP_Project__c> fourteenDayfromSubLstProject = new List<GP_Project__c>();
            List<GP_Project__c> twentyOneDayfromSubLstProject = new List<GP_Project__c>();
            for(GP_Project__c prj :  lstProject)
            {
                integer WorkingDayCount=0;
                for(integer i=0;i<=45;i++)
                {
                    if(prj.GP_Project_Submission_Date__c.addDays(i) <= System.today())
                    {
                        Datetime dt = (DateTime)prj.GP_Project_Submission_Date__c.addDays(i);
                        //system.debug('===dt==='+dt);
                        String dayOfWeek = dt.format('EEEE');                          
                        //system.debug('==dayOfWeek='+dayOfWeek);
                        
                        if(dayOfWeek != 'Sunday' && dayOfWeek != 'Saturday')
                        {
                            WorkingDayCount = WorkingDayCount+1;
                            // system.debug('==WorkingDayCount='+WorkingDayCount);
                            if(WorkingDayCount == 7 && prj.GP_Project_Submission_Date__c.addDays(i) == System.today())
                            {                               
                                sevenDayfromSubLstProject.add(prj);
                            }
                            if(WorkingDayCount == 14 && prj.GP_Project_Submission_Date__c.addDays(i) == System.today())
                            {
                                fourteenDayfromSubLstProject.add(prj);
                            }
                            if(WorkingDayCount ==21 && prj.GP_Project_Submission_Date__c.addDays(i) == System.today())
                            {
                                twentyOneDayfromSubLstProject.add(prj);
                            }    
                        }
                    }
                }
                
            }
            
            system.debug('===sevenDayfromSubLstProject==='+sevenDayfromSubLstProject.size());
            system.debug('===fourteenDayfromSubLstProject==='+fourteenDayfromSubLstProject.size());
            system.debug('===twentyOneDayfromSubLstProject==='+twentyOneDayfromSubLstProject.size());
            
            //=========================================Send Mail for 7 working days========================================//        
            
            List<String> sevenPidApproversIds=new List<String>();         
            Map<String,Map<String,String>> sevenMapApproverAndEmailIds = new Map<String,Map<String,String>>(); 
            Map<String,List<GP_Project__c>> sevenapproverLstProjectMap = new Map<String,List<GP_Project__c>>();
            for(GP_project__c seven : sevenDayfromSubLstProject)
            { 
                List<String> lstSeven = seven.GP_Approver_Ids__c.split(';');
                for(String str : lstSeven)
                {
                    sevenPidApproversIds.add(str);               
                }
                
            }
            
            for(String st : sevenPidApproversIds)
            {
                List<GP_Project__c> sevLstPrj=new List<GP_Project__c>();
                for(GP_project__c sev : sevenDayfromSubLstProject)
                {   
                    if(sev.GP_Approver_Ids__c.indexof(st) != -1)
                    {
                        sevLstPrj.add(sev);
                    }
                }
                sevenapproverLstProjectMap.put(st,sevLstPrj);
            }
            if(sevenPidApproversIds.size()>0)
                sevenMapApproverAndEmailIds = GetApproverAndSupervisorAndSupEmail(sevenPidApproversIds,7);
            
            
            if(sevenDayfromSubLstProject.size()>0 && mapOfPIDNotificationActive.get('7')=='true')
            {
                SendEmail(sevenapproverLstProjectMap,sevenMapApproverAndEmailIds,7);
            }
            
            //=========================================Send Mail for 14 working days=========================================//
            
            
            List<String> fourteenPidApproversIds=new List<String>();
            Map<String,Map<String,String>> fourteenMapApproverAndEmailIds = new Map<String,Map<String,String>>();   
            Map<String,List<GP_Project__c>> fourteenapproverLstProjectMap = new Map<String,List<GP_Project__c>>();
            for(GP_project__c fourteen : fourteenDayfromSubLstProject)
            { 
                List<String> lstfourteen = fourteen.GP_Approver_Ids__c.split(';');
                for(String str : lstfourteen)
                {
                    fourteenPidApproversIds.add(str);
                }
                
            }
            
            for(String st : fourteenPidApproversIds)
            {
                List<GP_Project__c> fourteenLstPrj=new List<GP_Project__c>();
                for(GP_project__c four : fourteenDayfromSubLstProject)
                {   
                    if(four.GP_Approver_Ids__c.indexof(st) != -1)
                    {
                        fourteenLstPrj.add(four);
                    }
                }
                fourteenapproverLstProjectMap.put(st,fourteenLstPrj);
            }
            if(fourteenPidApproversIds.size()>0)
                fourteenMapApproverAndEmailIds = GetApproverAndSupervisorAndSupEmail(fourteenPidApproversIds,14);
            if(fourteenDayfromSubLstProject.size()>0 && mapOfPIDNotificationActive.get('14')=='true')
            {
                SendEmail(fourteenapproverLstProjectMap,fourteenMapApproverAndEmailIds,14);
            }
            
            
            //=========================================Send Mail for 21 working days==========================================//
            
            
            List<String> twentyonePidApproversIds=new List<String>();        
            Map<String,Map<String,String>> twentyoneMapApproverAndEmailIds = new Map<String,Map<String,String>>();
            Map<String,List<GP_Project__c>> twentyoneapproverLstProjectMap = new Map<String,List<GP_Project__c>>();        
            for(GP_project__c twentyone : twentyoneDayfromSubLstProject)
            { 
                List<String> lsttwentyone = twentyone.GP_Approver_Ids__c.split(';');
                for(String str : lsttwentyone)
                {
                    twentyonePidApproversIds.add(str);
                }
                
            }
            for(String st : twentyonePidApproversIds)
            {
                List<GP_Project__c> twentyoneLstPrj=new List<GP_Project__c>();
                for(GP_project__c twe : twentyoneDayfromSubLstProject)
                {   
                    if(twe.GP_Approver_Ids__c.indexof(st) != -1)
                    {
                        twentyoneLstPrj.add(twe);
                    }
                }
                twentyoneapproverLstProjectMap.put(st,twentyoneLstPrj);
            }
            if(twentyonePidApproversIds.size()>0)
                twentyoneMapApproverAndEmailIds = GetApproverAndSupervisorAndSupEmail(twentyonePidApproversIds,21);
            if(twentyoneDayfromSubLstProject.size()>0 && mapOfPIDNotificationActive.get('21')=='true')
            {
                SendEmail(twentyoneapproverLstProjectMap,twentyoneMapApproverAndEmailIds,21);
            }
            
            //============================================================================================//
        }
        else
        {
            if(projectLst.size()>0)
            {
                system.debug('===projectLst==='+projectLst);  
                List<String> thirtyonePidApproversIds=new List<String>();         
                Map<String,Map<String,String>> thirtyoneMapApproverAndEmailIds = new Map<String,Map<String,String>>(); 
                Map<String,List<GP_Project__c>> thirtyoneapproverLstProjectMap = new Map<String,List<GP_Project__c>>();
                for(GP_project__c thirtyone : projectLst)
                { 
                    system.debug('===thirtyone.GP_Approver_Ids__c==='+thirtyone.GP_Approver_Ids__c);
                    if(thirtyone.GP_Approver_Ids__c != null)
                    {
                        List<String> lstthirtyone = thirtyone.GP_Approver_Ids__c.split(';');
                        for(String str : lstthirtyone)
                        {
                            thirtyonePidApproversIds.add(str);               
                        }
                    }
                    
                }
                
                for(String st : thirtyonePidApproversIds)
                {
                    List<GP_Project__c> thirLstPrj=new List<GP_Project__c>();
                    for(GP_project__c thir : projectLst)
                    {   
                        if(thir.GP_Approver_Ids__c.indexof(st) != -1)
                        {
                            thirLstPrj.add(thir);
                        }
                    }
                    thirtyoneapproverLstProjectMap.put(st,thirLstPrj);
                }
                for(String key:thirtyoneapproverLstProjectMap.KeySet())
                {
                    system.debug('===Key==='+key+ '==='+ thirtyoneapproverLstProjectMap.get(key));
                }
                
                thirtyoneMapApproverAndEmailIds = GetApproverAndSupervisorAndSupEmail(thirtyonePidApproversIds,31);
                if(projectLst.size()>0 && mapOfPIDNotificationActive.get('31')=='true')
                {
                    SendEmail(thirtyoneapproverLstProjectMap,thirtyoneMapApproverAndEmailIds,31);
                }
            }
            
        }
        
    }
    
    public static void SendEmail(Map<String,List<GP_Project__c>> approverAndlstProjectMap, 
                                 Map<String,Map<String,String>> MapApproverAndEmailIds,
                                 Integer DayCount)
    {
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        // List<GP_Project__c> lstproject=new List<GP_Project__c>();
        String DeveloperName='';
        DeveloperName = DayCount != 31 ? 'GP_PID_Approval_Workflow_Alert_For_Days':'GP_PID_Approval_Workflow_Rejected_Alert';
        EmailTemplate eTemplate = [SELECT id, DeveloperName, body, subject, htmlvalue
                                   FROM EmailTemplate 
                                   WHERE developername =: DeveloperName
                                   LIMIT 1];        
        
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();
        
        if(approverAndlstProjectMap != null)
        {
            for(String key : approverAndlstProjectMap.KeySet())
            {
                if(eTemplate != null)
                {
                    if(String.isNotBlank(eTemplate.HtmlValue) && MapApproverAndEmailIds != null) {
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        Map<String,String> toCCEmailsMap = MapApproverAndEmailIds.get(key);
                        List<String> toAddress=new List<String>();
                        Set<String> ccAddress=new Set<String>();
                        if(toCCEmailsMap != null)
                        {
                            for(String toMailKey : toCCEmailsMap.KeySet())
                            {
                                system.debug('===toMailKey==='+toMailKey + '==='+toCCEmailsMap.get(toMailKey));
                                if(toMailKey != null && toMailKey != '')
                                {
                                    
                                    toAddress.add(toMailKey);
                                    email.setToAddresses(toAddress);  
                                    if(toCCEmailsMap.get(toMailKey) != null && toCCEmailsMap.get(toMailKey) != '')
                                    {                                       
                                        ccAddress = new Set<String>(toCCEmailsMap.get(toMailKey).split(','));
                                    }
                                    for(GP_Project__c pr : approverAndlstProjectMap.get(key))
                                    {
                                        ccAddress.add(pr.GP_Current_Working_User__r.Email);
                                    }                                   
                                    //ccAddress.add('shivani.khullar@genpact.com');
                                    system.debug('===ccAddress==='+ccAddress);
                                    if(ccAddress.size()>0)
                                    {
                                        email.setccAddresses(new List<String>(ccAddress));  
                                    }
                                    
                                    //'shivani.khullar@genpact.com',
                                    Map < String, String > mapOfPIDNotificationActive = (Map < String, String > ) JSON.deserialize(System.label.GP_PID_Email_Notofication, Map < String, String > .class);
                                    if(mapOfPIDNotificationActive.get('bcc')=='true')
                                    {
                                        email.setBccAddresses(mapOfPIDNotificationActive.get('bccAdd').split(','));
                                    }
                                    email.sethtmlBody(eTemplate.HtmlValue);
                                    if(DayCount == 31)
                                    {
                                        //auto rejected workflow subject line
                                        if(approverAndlstProjectMap.get(key).size()==1)
                                        {
                                            email.setSubject('Workflow ' + approverAndlstProjectMap.get(key)[0].GP_EP_Project_Number__c + ' - Auto-Rejected Due to No Action');   
                                        }
                                        if(approverAndlstProjectMap.get(key).size()> 1)
                                        {
                                            email.setSubject(eTemplate.Subject);   
                                        }
                                        
                                    }
                                    else{
                                        email.setSubject(eTemplate.Subject.replace('Day_Count',String.valueOf(DayCount)));                    
                                    }
                                    
                                    
                                    //email.sethtmlBody((email.getHtmlBody().replace('Day_Count',String.valueOf(DayCount))).replace('LIST_PID_RECORDS', SetMultipleWorkflowDetails(approverAndlstProjectMap.get(key))));
                                    
                                    email.sethtmlBody((email.getHtmlBody().replace('Day_Count',String.valueOf(DayCount))));
                                    
                                    Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                                    blob csvBlob = Blob.valueOf(GetAttachmentDetails(approverAndlstProjectMap.get(key)));
                                    string csvname= 'Workflows_'+DayCount+'_Day'+'.csv';
                                    csvAttc.setFileName(csvname);
                                    csvAttc.setBody(csvBlob);
                                    
                                    email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                                    
                                    if (oweaId != null) {
                                        email.setOrgWideEmailAddressId(oweaId);
                                    }
                                    
                                    emailNotifications.add(email);
                                }
                                
                            }
                        }                        
                        
                    }
                    
                }
            }
        }
        system.debug('===emailNotifications==='+emailNotifications.size());
        if(!emailNotifications.isEmpty()) {
            Messaging.sendEmail(emailNotifications);
        }
        
    }   
    
    public static String GetAttachmentDetails(List<GP_Project__c> lstPrj)
    {   
        string header = 'SN , WF , PID Number , Workflow Type , SDO , Project Type , Business Name, Start Date, End Date , Org Name , Project Submission Date , PID Creator OHR ID , PID Creator Name , PID Approver OHR ID , PID Approver Name , Requestor Comments , Workflow Status    \n';
        string finalstr = header ;
        Integer i=1;
        for(GP_Project__c prj : lstPrj)
        {
            String ProjectStartDate='';
            if(prj.GP_Start_Date__c != null)
            { ProjectStartDate = String.valueOf(prj.GP_Start_Date__c).split(' ')[0]; } 
            
            String ProjectEndDate='';
            if(prj.GP_End_Date__c != null)
            { ProjectEndDate = String.valueOf(prj.GP_End_Date__c).split(' ')[0]; } 
            
            String PrimarySDOName='';
            if(prj.GP_Primary_SDO__c != null)
            { PrimarySDOName = prj.GP_Primary_SDO__r.Name.replace('–','-'); }
            
            String ProjectOrgName='';
            if(prj.GP_Project_Organization__c != null)
            { ProjectOrgName = prj.GP_Project_Organization__r.Name; }
            
            
            
            string recordString = i +','+ prj.GP_EP_Project_Number__c+','
                + prj.GP_Oracle_PID__c+','
                + (prj.GP_Oracle_PID__c == 'NA' ? 'Creation': 'Modification') +','
                + '"'+ PrimarySDOName +'"' +','
                + '"'+ prj.GP_Project_type__c+'"' +','
                + '"'+ prj.GP_Business_Name__c+'"' +','
                + ProjectStartDate+','
                + ProjectEndDate+','
                + '"'+ ProjectOrgName +'"' +','                
                + String.ValueOf(prj.GP_Project_Submission_Date__c).split(' ')[0] +','      
                + prj.GP_Current_Working_User__r.OHR_ID__c + ','
                + '"'+ prj.GP_Current_Working_User__r.Name +'"' +','  
                + prj.GP_PID_Approver_User__r.OHR_ID__c + ',' 
                + '"'+ prj.GP_PID_Approver_User__r.Name   +'"' +',' 
                + '"'+ prj.GP_Approval_Requester_Comments__c +'"' +','
                + prj.GP_Approval_Status__c +'\n';
            
            finalstr = finalstr +recordString;
            i++;
        }
        return finalstr;
    }   
    
    
    
    public static Map<String,Map<String,String>> GetApproverAndSupervisorAndSupEmail(List<String> pidApproverIds,Integer Days)
    {
        //system.debug('===pidApproverIds==='+pidApproverIds);
        //system.debug('===Days==='+Days);
        Map<String,Map<String,String>> MapApproverAndEmailIds=new Map<String,Map<String,String>>(); 
        
        List<GP_Employee_Master__c> lstEmpMaster=[Select Id,
                                                  GP_OFFICIAL_EMAIL_ADDRESS__c,                                                   
                                                  GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c , 
                                                  GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c, 
                                                  GP_SFDC_User__c 
                                                  from GP_Employee_Master__c												  
                                                  Where GP_SFDC_User__c in: pidApproverIds and GP_Person_ID__c != null];
        
        system.debug('===lstEmpMaster==='+lstEmpMaster.size());
        Map < String, String > mapOfPIDNotificationActive = (Map < String, String > ) JSON.deserialize(System.label.GP_PID_Email_Notofication, Map < String, String > .class);
        
        for(GP_Employee_Master__c emp : lstEmpMaster)
        {
            String toemailIds='';
            String ccemailIds='';
            if(emp.GP_OFFICIAL_EMAIL_ADDRESS__c != null && (Days==7 || Days == 14|| Days == 21 || Days==31) && mapOfPIDNotificationActive.get('SetApp')=='true')
            {
                toemailIds = emp.GP_OFFICIAL_EMAIL_ADDRESS__c;
            }
            if(emp.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c  != null && (Days==7 || Days == 14 || Days == 21 || Days==31) && mapOfPIDNotificationActive.get('SetSup')=='true')
            {
                ccemailIds = emp.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c;
            }
            if(emp.GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c  != null && Days == 21 && mapOfPIDNotificationActive.get('SetSupSup')=='true')
            {
                ccemailIds = ccemailIds + ',' + emp.GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c;
            }
            Map<String,String> MapToCcEmailIds=new Map<String,String>();
            if(toemailIds != '' || ccemailIds !='')
            {                
                MapToCcEmailIds.put(toemailIds,ccemailIds);
            }
            
            if(MapToCcEmailIds != null && !MapToCcEmailIds.isEmpty())
            {
                MapApproverAndEmailIds.put(emp.GP_SFDC_User__c, MapToCcEmailIds );
            }
            
        }      
        
        
        return MapApproverAndEmailIds;
    }
}