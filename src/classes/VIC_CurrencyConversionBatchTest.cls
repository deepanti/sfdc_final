@isTest
public class VIC_CurrencyConversionBatchTest {
Public static Master_Plan_Component__c masterPlanComponentObj;
Public static vic_Currency_Exchange_Rate__c obj;    
static testmethod void ValidateVIC_CurrencyConversionBatch()
{
    LoadData();
    VIC_CurrencyConversionBatch obj= new VIC_CurrencyConversionBatch(true);
    database.executeBatch(obj);
    System.AssertEquals(200,200);

    
}
static testmethod void ValidateVIC_CurrencyConversionBatchNew()
{
    LoadData();
    masterPlanComponentObj.vic_Component_Category__c='Annual';
    update masterPlanComponentObj;
   
    VIC_CurrencyConversionBatch obj= new VIC_CurrencyConversionBatch(false);
    database.executeBatch(obj);
    System.AssertEquals(200,200);

    
}    
static void LoadData()
{
VIC_Process_Information__c vicInfo =new  VIC_Process_Information__c();
vicInfo.VIC_Process_Year__c=2018;
vicInfo.VIC_Annual_Process_Year__c=2018;
insert vicInfo;

user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
insert objuser;

user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='BDE';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;

OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.vic_Exchange_Rate__c=10;
objOLI.Product_BD_Rep__c=objuser.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id;   
objOLI.vic_Is_Split_Calculated__c=false;
insert objOLI;
update objOLI;

system.debug('objOLI'+objOLI);

Target__c targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
targetObj.Start_Date__c=DATE.newInstance(2018,1,1);
insert targetObj;

masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_IO'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;
    
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
insert PlanComponentObj;
    
Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
targetComponentObj.Target__C=targetObj.id;
targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
insert targetComponentObj;

Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
ObjInc.Opportunity__c=objopp.id;
ObjInc.vic_Incentive_In_Local_Currency__c=null;
ObjInc.Achievement_Date__c =system.today();  
insert objInc;
    


 obj = new vic_Currency_Exchange_Rate__c();
 obj.vic_Year__c=string.valueof(system.today().year());
 obj.vic_Month__c=string.valueof(system.today().month());  
 obj.vic_ISO_Code__c='CNY';
 insert obj;

    
   
    



}
     


}