/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle QSRM in Discover stage of opportunity
**************************************************************************************************************************/
public class OpportunityDiscoverQSRM{
/*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for fetching QSRM__c of Opportunity
* @param   OpportunityId - Opportunity Id
* @return  QSRM__c
**************************************************************************************************************************/    
    @AuraEnabled
    public static Id fetchApprovedOrInApprovalQSRM(Id oppId)
    {
        try
        {
            List<QSRM__c> qsrms = [Select Id, Status__c from QSRM__c where Opportunity__c =:oppId ORDER BY CreatedDate DESC LIMIT 1];
            if (qsrms.size() > 0)
            {
                QSRM__c  qsrm = qsrms[0];
                return  qsrm.Id;
            }
            else
                return null;
            
        }catch(Exception ex){
              //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(OppId),ex.getMessage(), '', ex.getStackTraceString(),'OpportunityDiscoverQSRM', 'fetchApprovedOrInApprovalQSRM','Fail','',String.valueOf(ex.getLineNumber())); 
            system.debug(ex.getMessage()+ ' Trace===>' + ex.getCause()+ ' LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());   
        }
    }
    
    

    
   
/*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for fetching QSRM Owner Name of Opportunity
* @param   OpportunityId - QSRM__c Id
* @return  List<String>
**************************************************************************************************************************/    
    @AuraEnabled
    public static List<String> fetchQSRMOwnerName(Id qsrmId)
    {
        try{
            List<QSRM_Leadership_Approvals__c> qsrmsApprovals = [Select Id,QSRM_Owner__c,QSRM_Owner__r.Name from QSRM_Leadership_Approvals__c where
                                                                 QSRM__c =:qsrmId
                                                                 ORDER BY CreatedDate];
            List<String> OwnerLIst = new List<String>();
            if (qsrmsApprovals.size() > 0)
            {  
                for(QSRM_Leadership_Approvals__c qsrmAppr: qsrmsApprovals){ 
                    OwnerLIst.add(qsrmAppr.QSRM_Owner__r.Name);
                }
                return  OwnerLIst;
            }
            else
                return null;
            
        
        }catch(Exception ex){
            system.debug(ex.getMessage()+ ' Trace===>' + ex.getCause()+ ' LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
            throw new AuraHandledException(ex.getMessage());   
        }
    }
    
  /*************************************************************************************************************************
* @author  Vikas Chand
* @description  - Show to Text box on Ts Form 
* @param   OpportunityId -  Id
* @return  Opportunity
**************************************************************************************************************************/    
    @AuraEnabled
    public static oppDetailsWrapper fetchOppDetail(Id oppId){
        oppDetailsWrapper opp = new oppDetailsWrapper();
        list<OpportunityLineItem> opplineitem= new list<OpportunityLineItem>();
        list<opportunity> opplst = [Select Id, Account_ArchType__c,(select Service_Line__c from opportunityLineItems) from opportunity where ID  =:oppId limit 1];
        system.debug('OpportunityDiscoverQSRm === : '+opplst);
        for(opportunity op : opplst){
            opp.Archtype = op.Account_ArchType__c;
            opplineitem = op.opportunityLineItems;
        }
        for(OpportunityLineItem oli : opplineitem){
            if(oli.Service_Line__c =='Others'){
                opp.ServiceLine = 'Others';
                break;
            }else {
                opp.ServiceLine = '';
            }
        }
        system.debug(':==oppwraper==:'+opp);
        return opp;
    }
    
    public class oppDetailsWrapper{
        @auraenabled
        public String Archtype;
        @auraEnabled
        public String ServiceLine;
     }
}