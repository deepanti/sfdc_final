public class VIC_HRScoreBoardCtrl {
    
    static Map<String, String> mapUserVicRoles = vic_CommonUtil.getUserVICRoleNameinMap();
    static VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    static Map<String, String> mapDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap(); 
    
    @AuraEnabled
    public static HRScoreViewWrapper getViewdata(){
        
        map<string,List<HRScoreDataWrapper>>  mapDomToHrsdw = new map<string,List<HRScoreDataWrapper>>();
        mapDomToHrsdw=getHRScoreData();
        HRScoreViewWrapper objSVW = new HRScoreViewWrapper(); 
        if(mapDomToHrsdw!=null && mapDomToHrsdw.size()>0){
            
            objSVW.lstUkDtW=mapDomToHrsdw.get('UK');
            objSVW.lstUsDtW=mapDomToHrsdw.get('US');
            objSVW.lstOtDtW=mapDomToHrsdw.get('OTHERS');
            
            
        }
        
        return objSVW; 
        
        
    }
    
    
    
    
    @AuraEnabled
    public static map<string,List<HRScoreDataWrapper>> getHRScoreData(){
        List<HRScoreDataWrapper> lstHrsw = new List<HRScoreDataWrapper>();
        map<string, decimal> mapUserIdtoScoreCard = new map<string, decimal>(); 
        map<string,List<HRScoreDataWrapper>>  mapDomToHrsdw = new map<string,List<HRScoreDataWrapper>>();
        String strUSRegionDomiciles = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('US_Region_Domiciles');
        String strUKRegionDomiciles = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('UK_Region_Domiciles');
        mapDomToHrsdw.put('US', new List<HRScoreDataWrapper>());
        mapDomToHrsdw.put('UK', new List<HRScoreDataWrapper>());
        mapDomToHrsdw.put('OTHERS', new List<HRScoreDataWrapper>());
        
        
        mapUserIdtoScoreCard=getmapusertoscorecard();
        
        if(processInfo != null && processInfo.VIC_Process_Year__c != null){
            List<Target__c> lstTargets = getTargets();
            
            if(lstTargets!=null && lstTargets.size()>0){
                for(Target__c objTarget:lstTargets){
                    
                    if(mapUserVicRoles!=null && mapUserVicRoles.size()>0 && mapUserVicRoles.containskey(objTarget.User__c)){
                        
                        HRScoreDataWrapper objSDW = new HRScoreDataWrapper();
                        objSDW.userId=objTarget.User__c;
                        objSDW.userName=objTarget.User__r.Name;
                        objSDW.strDom=objTarget.Domicile__c;
                        objSDW.vicRole=mapUserVicRoles.get(objTarget.User__r.Id);
                        
                        objSDW.totalSrCd=mapUserIdtoScoreCard!=null?mapUserIdtoScoreCard.get(objTarget.User__r.Id):0;
                        objSDW.totalDtPayt=mapUserIdtoScoreCard!=null?mapUserIdtoScoreCard.get(objTarget.User__r.Id+'Discretionary_Payment'):0;
                        
                        objSDW.totalPayt=objSDW.totalSrCd+objSDW.totalDtPayt;
                        
                        lstHrsw.add(objSDW);
                        
                    }
                    
                }
                if(lstHrsw!=null && lstHrsw.size()>0){
                    for(HRScoreDataWrapper objHrsw:lstHrsw){
                        
                        if(strUSRegionDomiciles.ContainsIgnoreCase(objHrsw.strDom)){
                            List<HRScoreDataWrapper> lstHrsdw = mapDomToHrsdw.get('US');
                            lstHrsdw.add(objHrsw);
                            mapDomToHrsdw.put('US', lstHrsdw);
                        }
                        else if(strUKRegionDomiciles.ContainsIgnoreCase(objHrsw.strDom)){
                            List<HRScoreDataWrapper> lstHrsdw = mapDomToHrsdw.get('UK');
                            lstHrsdw.add(objHrsw);
                            mapDomToHrsdw.put('UK', lstHrsdw);
                        }
                        else{
                            List<HRScoreDataWrapper> lstHrsdw = mapDomToHrsdw.get('OTHERS');
                            lstHrsdw.add(objHrsw);
                            mapDomToHrsdw.put('OTHERS', lstHrsdw);
                        }
                        
                    }	
                    
                } 
            }
            
        }
        return mapDomToHrsdw;
    }
    
    public class HRScoreViewWrapper{
        
        @Auraenabled public list<HRScoreDataWrapper> lstUkDtW;
        @Auraenabled public list<HRScoreDataWrapper> lstUsDtW;
        @Auraenabled public list<HRScoreDataWrapper> lstOtDtW;
        
        
    }
    
    public class HRScoreDataWrapper{
        
        @Auraenabled public boolean isUserSelected;
        @Auraenabled public String userId;
        @Auraenabled public String userName;
        @Auraenabled public String strDom;
        @Auraenabled public String vicRole;
        @Auraenabled public decimal totalSrCd;
        @Auraenabled public decimal totalDtPayt;
        @Auraenabled public decimal totalPayt;
        @Auraenabled public String strCom;
        @Auraenabled public String strQry;
        
    }
    
    public Static List<Target__c> getTargets(){
        
        Date targetDate = date.newinstance(Integer.valueOf(processInfo.VIC_Process_Year__c), 1, 1);
        
        List<Target__c> lstTarget = [SELECT Id,User__r.Id, User__r.Name, Domicile__c,vic_HR_Comments__c,vic_Is_Payment_Hold_By_HR__c, 
                                     (Select Id, Master_Plan_Component__r.vic_Component_Code__c, vic_HR_Pending_Amount__c 
                                      FROM Target_Components__r) 
                                     FROM Target__c 
                                     WHERE Is_Active__c = TRUE 
                                     AND User__r.IsActive = true
                                     AND Start_date__c=:targetDate
                                     AND Domicile__c != null
                                     Order BY Domicile__c];
        
        return lstTarget;
    }
    
    @AuraEnabled
    public Static map<string, decimal> getmapusertoscorecard(){
        
        map<string, decimal> mapUserIdtoScoreCard = new map<string, decimal>();
        decimal bonusamountSum;
        string inckey;
        
        list<Target_Achievement__c> lstTrAc =[select id,Target_Component__r.Target__r.User__c,Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,Bonus_Amount__c 
                                              from Target_Achievement__c 
                                              where Target_Component__r.Target__r.User__c!=null and 
                                              Target_Component__r.Target__r.Is_Active__c=true and 
                                              Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c in ('OP','PM','MBO','TCV','IO_TCV','TS_TCV','Discretionary_Payment')];
        
        if(lstTrAc!=null && lstTrAc.size()>0){
            for(Target_Achievement__c objInc:lstTrAc){
                
                if(objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'Discretionary_Payment'){
                    
                    inckey=objInc.Target_Component__r.Target__r.User__c+objInc.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c;
                    
                    if(mapUserIdtoScoreCard!=null && mapUserIdtoScoreCard.size()>0 &&               mapUserIdtoScoreCard.containskey(objInc.Target_Component__r.Target__r.User__c)){
                        
                        bonusamountSum=mapUserIdtoScoreCard.get(objInc.Target_Component__r.Target__r.User__c);
                        if(bonusamountSum!=null){
                            
                            bonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                            mapUserIdtoScoreCard.put(inckey,BonusamountSum);
                            
                        }
                        
                    }
                    else{
                        mapUserIdtoScoreCard.put(inckey,objInc.Bonus_Amount__c);
                    }
                    
                }
                else{
                    
                    if(mapUserIdtoScoreCard!=null && mapUserIdtoScoreCard.size()>0 &&               mapUserIdtoScoreCard.containskey(objInc.Target_Component__r.Target__r.User__c)){
                        
                        bonusamountSum=mapUserIdtoScoreCard.get(objInc.Target_Component__r.Target__r.User__c);
                        if(bonusamountSum!=null){
                            
                            bonusamountSum= BonusamountSum+objInc.Bonus_Amount__c;
                            mapUserIdtoScoreCard.put(objInc.Target_Component__r.Target__r.User__c,BonusamountSum);
                            
                        }
                        
                    }
                    else{
                        
                        mapUserIdtoScoreCard.put(objInc.Target_Component__r.Target__r.User__c,objInc.Bonus_Amount__c);
                    }
                }
            }
            
        }
        
        return mapUserIdtoScoreCard;
    }
    
    
}