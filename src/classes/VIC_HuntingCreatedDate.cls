public class VIC_HuntingCreatedDate{
    
    public static void updateHuntingCreatedDate(Map<Id,Account> accNewMap,Map<Id,Account> accOldMap){
        
        System.debug('\n\n ---->> Entry Class ');
        try{
            
            Map<Id,Account> huntingAccMap = new Map<Id,Account>();
            for(Account acc:accNewMap.values()){
                
                System.debug('\n\n ----- >>> acc.Hunting_Mining__c '+acc.Hunting_Mining__c);
                
                if(acc.Hunting_Mining__c != accOldMap.get(acc.Id).Hunting_Mining__c && 
                    acc.Hunting_Mining__c == 'Hunting'){
                    
                    huntingAccMap.put(acc.Id,acc);
                }
            
            }
            
            System.debug('\n\n ----- >>> huntingAccMap '+huntingAccMap);
                
            if(huntingAccMap != null && huntingAccMap.size() > 0 && huntingAccMap.values() != null && 
                huntingAccMap.values().size() > 0){
                
                updateFirstOppClosedDate(huntingAccMap);
                
            }
            
        }catch(Exception e){
            System.debug('----------->>> ERROR '+e.getMessage());
        }
    }
    
    public static void updateFirstOppClosedDate(Map<Id,Account> accHuntingMap){
        //ADDED BY RISHABH PILANI ON 13-09-2018
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        
        //ADD Actual_Close_Date__c & INNER WHERE CONDITION IN QUERY BY RISHABH PILANI ON 13-09-2018
        List<Account> accOppList = [Select Id,name,VIC_Hunting_Start_Date__c,(Select Id,name,Actual_Close_Date__c,CloseDate,AccountId 
                                                    from Opportunities 
                                                     where Actual_Close_Date__c != null 
                                                     AND Actual_Close_Date__c >=:processingYearStart
                                                     order by CloseDate asc) 
                                    from Account 
                                    where Id IN:accHuntingMap.keyset()];
        
        List<Account> accToUpdate = new List<Account>();
        
        if(accOppList != null && accOppList.size() > 0){
            for(Account acc:accOppList){
                
                List<Opportunity> oppList = acc.Opportunities;
                System.debug('\n\n -------->> Opportunities '+oppList);
                
                if(oppList != null && oppList.size() > 0){
                    for(Opportunity opp:oppList){
                        //ADDED BELOW CODE BY RISHABH PILANI ON 13-09-2018
                        
                        acc.VIC_Hunting_Start_Date__c = opp.closeDate;  
                        accToUpdate.add(acc);
                        break;
                    }
                }
            }
            
            System.debug('\n\n -------->> accToUpdate '+accToUpdate);
                
            if(accToUpdate != null && accToUpdate.size() > 0){
                update accToUpdate;
            }
        }
        
    }
}