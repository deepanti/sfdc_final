/* **********************************************************************************
@Original Author : Abhishek Maurya
@TL :Mandeep Kaur
@date Started :9/27/2018
@Description :this test is used to cover all code for MarkFieldUpFromAccount trigger

@Modified BY    : 
@Modified Date  : 
@Revisions      : 
********************************************************************************** */
@isTest
public class MarkFieldUpFromAccountTest {
       
    @isTest static void testBatch()
        
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
         User u1 = new User(Alias = 'standt', Email='standarduser1@testorg.com', 
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, OHR_ID__c ='800070247',
         TimeZoneSidKey='America/Los_Angeles', UserName='standardusertestgen1@testorg.com');
         insert u1;
        
         final Account oldAcc=new Account();
         
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         
        Lead l = new Lead(lastname='Test1',firstname='Lead1', company='TestCo', email='testlead1'+ '@testco.com',
        status='Engaged', CurrencyIsoCode='USD', Account__c=oAccount.Id, Authority_or_P_to_p__c='Decision Maker', Need__c='Asset optimization'); 
        oldAcc.Id=oAccount.Id;
        Contact oldContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                          'test121@xyz.com','99999999999');
        Map<ID,Account> accMap=new Map<ID,Account>();
        accMap.put(oldAcc.Id,oldAcc);
        oAccount.Primary_Account_GRM__c=u1.Id;
        oAccount.ownerId=u1.id;
        oAccount.Industry_Vertical__c='BFS';
        update oAccount;
        
        List<Account> newAccLst=new List<Account>();
        newAccLst.add(oAccount);        
        test.startTest();
        Database.executeBatch(new LinkAccountWithAccountCrationRequestHl(newAccLst,accMap),5);
        test.stopTest();
   
       
    }
}