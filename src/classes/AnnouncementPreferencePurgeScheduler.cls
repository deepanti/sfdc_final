global class AnnouncementPreferencePurgeScheduler implements Schedulable {
     /*************************************************************************************************************************
* @author  Persistent
* @description  - To delete all the inactive prefered user announcements

**************************************************************************************************************************/
    global void execute(SchedulableContext ctx) { 
        try{
        List<User_Announcements_Preference__c> userPref =[select id from User_Announcements_Preference__c where Announcement__r.isActive__c=false];
        delete userPref;
        }
        catch(DmlException e){
             System.debug('The following exception has occurred: ' + e.getMessage());
           
        }
    }
}