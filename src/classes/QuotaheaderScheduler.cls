/*   ==============================================================
    Developer       :   Arjun Srivastava
    Client          :   Genpact
    Created Date    :   12/August/2014
    Modified Date   :   12/August/2014  
    Description     :   This Controller is used to schedule the quota header batch class.
====================================================================== */
global class QuotaheaderScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {  
        QuotaHeaderCreationClass reassign = new QuotaHeaderCreationClass ('');        

        ID batchprocessid = Database.executeBatch(reassign,1);
    }
}