@istest
public class TestProfileUpdate {
    
    public static testMethod void test1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Test.startTest();
		ProfileUpdate.updateProfile(u.id, p.id);
        Test.stopTest();
    }
    
}