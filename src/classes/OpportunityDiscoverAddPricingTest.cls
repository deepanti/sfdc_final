/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing OpportunityDiscoverAddPricing class
**************************************************************************************************************************/
@isTest //(SeeAllData = false)
public class OpportunityDiscoverAddPricingTest{
    public static User u;
    public static User testUser;
    public static Account acc;
    public static Contact con1;
    public static Opportunity oppList;
    public static List<Product2> pList;
    public static PricebookEntry standardPrice; 
    public static  Id pricebookId;
    public static List<OpportunityLineItem> oppLineItem; 
    public static List<OpportunityLineItem> opprtunityLineItem; 
    public static List<OpportunityLineItemSchedule> oppLineItemSchedules;
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    public static void setUpData()
    {	
        acc = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
        insert acc;
        con1 = TestDataFactoryUtility.CreateContact('First','Last',acc.id,'Test','Test','fed@gtr.com','78342342343');
        insert con1;
        oppList = TestDataFactoryUtility.CreateOpportunity('Test',acc.id,con1.id);
        insert oppList;
        pList = TestDataFactoryUtility.createTestProducts2(1);
        insert pList;
        system.debug('pList'+ pList);
        pricebookId = Test.getStandardPricebookId();
        standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000, 
            IsActive = true);
        insert standardPrice;
        oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);
        insert oppLineItem;
        
        
        testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','acZ@bc.com','acy@bc.com');
        insert testUser;
        
        
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing OpportunityDiscoverAddPricing.getCampaignMembers mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void getOLISMapTest1()
    {
        setUpData();
        System.runAs(testUser){
            Test.startTest();
            List<OpportunityLineItem> oli = OpportunityDiscoverAddPricing.getOLISMap(oppList.Id);
            system.assertNotEquals(null, OpportunityDiscoverAddPricing.getOLISMap(oppList.Id));
            Test.stopTest();
        }
    }
    
    public static testMethod void getOLISMapTest2()
    {
        setUpData();
        System.runAs(testUser){
            try
            {
                Test.startTest();
                List<OpportunityLineItem> OLIList = new List<OpportunityLineItem>();
                OLIList = null;
                OpportunityDiscoverAddPricing.saveOLI(OLIList);
                Test.stopTest();
                
            }
            catch(Exception ex)
            {
                system.debug('exeption'+ex.getMessage());
            }
        }
    }
    
    
    public static testMethod void getConversionRatesTest()
    {
        setUpData();
        System.runAs(testUser){
            Test.startTest();
            OpportunityDiscoverAddPricing.getConversionRates();
            OpportunityDiscoverAddPricing.saveOLI(oppLineItem);
            //OpportunityDiscoverAddPricing.saveOLI(oppLineItem[0].Id, 3,4.44,'2015-03-08',50.99);
            Test.stopTest();
        }
    } 
    
    public static testMethod void getinsertOLI()
    {
        system.debug(':---getinsertOLI--:');
        setUpData();
        System.runAs(testUser){
            Test.startTest();
            oppList.Opportunity_Source__c = 'Renewal';
            update oppList;
            oppLineItem[0].Type_of_Deal__c = 'Renewal';
              update oppLineItem;
           // OpportunityLineItem oppLN = TestDataFactoryUtility.createOpportunityLineItem(pList[0],oppList.Id,pricebookId);
           // insert oppLN;
            string abc = oppLineItem[0].Id;
            string axyz = '{"sobjectType":"OpportunityLineItem","Product2Id":'+'"'+pList[0].Id+'"'+',"TCV__c":"445","OpportunityId":'+'"'+oppList.Id+'"'+',"Type_of_Deal__c":"Booking","Contract_Term__c":"33","Local_Currency__c":"AUD","Delivering_Organisation__c":"ANALYTICS","Sub_Delivering_Organisation__c":"Analytics","Delivery_Location__c":"India","FTE__c":null,"Product_BD_Rep__c":'+'"'+testUser.id+'"}';
            system.debug(':---axyz-----:'+axyz); //string.join(sList,',');
            OpportunityDiscoverAddPricing.insertOLI(axyz, abc);
            Test.stopTest();
        }
    }
    
}