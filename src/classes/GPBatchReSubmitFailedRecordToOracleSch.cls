global class GPBatchReSubmitFailedRecordToOracleSch implements schedulable {
    /**
     * 
     * GPBatchReSubmitFailedRecordToOracleSch  scheduler = new GPBatchReSubmitFailedRecordToOracleSch();
	 * String interval = '0 0 8 13 2 ?'; //schedule interval time
	 * system.schedule('Re submit failed jobs', interval, scheduler);
     * 
     * */
    global void execute(SchedulableContext sc) {
        GPBatchReSubmitFailedRecordToOracle reSubmitFailedRecordToOracle = new GPBatchReSubmitFailedRecordToOracle();
        Database.executebatch(reSubmitFailedRecordToOracle, 1);
    }
}