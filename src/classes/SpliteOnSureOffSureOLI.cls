// always run this batch job with size as 1
/*
String StrQuery = 'SELECT Id, Name,TCVLocal__c,Mode__c,FTE__c,Quarterly_FTE_1st_month__c,FTE_4th_month__c,FTE_7th_month__c, FTE_10th_month__c,FTE_OffShore__c,FTE_Onsite__c, (select id,name,RollingYearRevenueLocal__c,OpportunityProduct__c from RevenueSchedule__r),(select id,name,RevenueLocal__c,OpportunityProductId__c,RevenueScheduleId__c from ProductSchedules__r) FROM OpportunityProduct__c WHERE  Splited_Records__c = false And (Mode__c=\'Onsite & India Offshore\' Or Mode__c=\'Onsite & Manila Offshore\')';

SpliteOnSureOffSureOLI reassign = new SpliteOnSureOffSureOLI(StrQuery);

ID batchprocessid = Database.executeBatch(reassign,1);


*/
// always run this batch job with size as 1
global class SpliteOnSureOffSureOLI implements Database.Batchable<sObject>
{
   global String Query;

   global SpliteOnSureOffSureOLI(String q){

      Query=q;
   }

   global Database.QueryLocator start(Database.BatchableContext BC)
   {
        return Database.getQueryLocator(query);
   }
   global void execute(Database.BatchableContext BC, List<sObject> scope) // always run this batch job with size as 1
   {
        Map<String,OpportunityProduct__c> MapOfOppProductToUpsert = new Map<String,OpportunityProduct__c>();
        Map<String,RevenueSchedule__c> MapOfRevenueScheduleToUpsert = new Map<String,RevenueSchedule__c>();
        Map<String,ProductSchedule__c> MapOfProductScheduleToUpsert = new Map<String,ProductSchedule__c>();
        
        List<sObject> originalRevenueScheduleSObjectsTempLst = new List<sObject>();
        List<sObject> originalProductScheduleSObjectsTempLst = new List<sObject>();
        
        for(sObject sObjectTempOppProductRecord : scope)
        {
            OpportunityProduct__c ExistingOppProductRecord = (OpportunityProduct__c)sObjectTempOppProductRecord;
            for(RevenueSchedule__c TempRevenueScheduleObj : ExistingOppProductRecord.RevenueSchedule__r)
            {
                originalRevenueScheduleSObjectsTempLst.add(TempRevenueScheduleObj);
            }
            for(ProductSchedule__c TempProductScheduleObj : ExistingOppProductRecord.ProductSchedules__r)
            {
                originalProductScheduleSObjectsTempLst.add(TempProductScheduleObj);
            }
        }
             Map<id,sObject> ClonedOppProductSObjectsMap = new Map<id,sObject>();
             Map<Id,sObject> ClonedRevenueScheduleSObjectsMap = new Map<Id,sObject>();
             Map<Id,sObject> ClonedProductScheduleSObjectsMap =  new Map<Id,sObject>(); 
            
            ClonedOppProductSObjectsMap = SObjectAllFieldCloner.cloneObjects(scope, scope[0].getsObjectType());
           
            if(originalRevenueScheduleSObjectsTempLst.size()>0)
                ClonedRevenueScheduleSObjectsMap = SObjectAllFieldCloner.cloneObjects(originalRevenueScheduleSObjectsTempLst, originalRevenueScheduleSObjectsTempLst[0].getsObjectType());
            if(originalProductScheduleSObjectsTempLst.size()>0)
                ClonedProductScheduleSObjectsMap = SObjectAllFieldCloner.cloneObjects(originalProductScheduleSObjectsTempLst, originalProductScheduleSObjectsTempLst[0].getsObjectType());
        
        for(sObject TempOppProductSObj : scope)
        {
            OpportunityProduct__c ExistingOppProductRecord = (OpportunityProduct__c)TempOppProductSObj;
            OpportunityProduct__c ClonedOppProductRecord = (OpportunityProduct__c)ClonedOppProductSObjectsMap.get(ExistingOppProductRecord.id);
            Decimal FullTCV = ExistingOppProductRecord.TCVLocal__c;
           
            //system.debug('#!@#!$!@#@@$#ExistingOppProductRecord.TCVLocal__c'+ExistingOppProductRecord.TCVLocal__c);
            //system.debug('#!@#!$!@#@@$#'+FullTCV);
            //system.debug('#!@#!$!@#@@$#'+ClonedOppProductRecord.TCVLocal__c);
           // system.debug('#!@#!$!@#@@$#'+ExistingOppProductRecord.id);
            
           // if(FullTCV != null) // if TCV is null we need to spl;ite it with null
           // {
               // system.debug('#!@#!$!@#@@$#FullTCV = '+FullTCV);
               if(FullTCV != null && FullTCV != 0){
                ExistingOppProductRecord.TCVLocal__c = (70*FullTCV)/100;
                ClonedOppProductRecord.TCVLocal__c = (30*FullTCV)/100;
                }
               else{
                ExistingOppProductRecord.TCVLocal__c = null;
                ClonedOppProductRecord.TCVLocal__c = null;
                }
               // system.debug('#!@$@#@#!@$#'+ExistingOppProductRecord.TCVLocal__c);
               // system.debug('#!@#!$!@#@@$#ClonedOppProductRecord.TCVLocal'+ClonedOppProductRecord.TCVLocal__c);
                
                //ExistingOppProductRecord.Mode__c = 'Onsite'; // we are updatating mode so this record will not come again in batch
                //ClonedOppProductRecord.Mode__c = 'India Offshore';
                
                // This is to identify that the splited record is Onsite ot Off Shore 
                ExistingOppProductRecord.CMIT_Onsite_Off_Shore_Split__c = 'On Site Split'; 
                ClonedOppProductRecord.CMIT_Onsite_Off_Shore_Split__c = 'Off Shore Split';
                
                // For the Splited record onsite record All 4 field will be populated from FTE_Onsite__c  
               // ExistingOppProductRecord.FTE__c = ExistingOppProductRecord.FTE_Onsite__c;
                ExistingOppProductRecord.Quarterly_FTE_1st_month__c = ExistingOppProductRecord.FTE_Onsite__c;
                ExistingOppProductRecord.FTE_4th_month__c = ExistingOppProductRecord.FTE_Onsite__c;
                ExistingOppProductRecord.FTE_7th_month__c = ExistingOppProductRecord.FTE_Onsite__c;
                ExistingOppProductRecord.FTE_10th_month__c = ExistingOppProductRecord.FTE_Onsite__c;
                
                // For the Splited record Off Shore record for all 4 field will be populated from FTE_OffShore__c  
               // ClonedOppProductRecord.FTE__c = ExistingOppProductRecord.FTE_OffShore__c;
                ClonedOppProductRecord.Quarterly_FTE_1st_month__c = ExistingOppProductRecord.FTE_OffShore__c;
                ClonedOppProductRecord.FTE_4th_month__c = ExistingOppProductRecord.FTE_OffShore__c;
                ClonedOppProductRecord.FTE_7th_month__c = ExistingOppProductRecord.FTE_OffShore__c;
                ClonedOppProductRecord.FTE_10th_month__c = ExistingOppProductRecord.FTE_OffShore__c;
                
                // we are setting up updatating mode so this record will not come again in batch
                ExistingOppProductRecord.Splited_Records__c = true; 
                ClonedOppProductRecord.Splited_Records__c= true;
                
                //ExistingOppProductRecord.FTE_OffShore__c = 0;
                //ClonedOppProductRecord.FTE_Onsite__c = 0;
                
                
                ClonedOppProductRecord.Name = 'Line Id-2';
                ClonedOppProductRecord.Product_Autonumber__c = 'Line Id-2';
                
                /* As discuss with pankaj we just need to make FTE_OffShore__c to zero for onsite record and FTE_Onsite__c to zero for offShore record
                if(ExistingOppProductRecord.FTE__c != 0 && ExistingOppProductRecord.FTE__c != null)
                    ExistingOppProductRecord.FTE_Onsite__c = (70*ExistingOppProductRecord.FTE__c)/100;
                if(ClonedOppProductRecord.FTE__c != 0 && ClonedOppProductRecord.FTE__c != null)
                    ClonedOppProductRecord.FTE_OffShore__c = (30*ClonedOppProductRecord.FTE__c)/100; 
              */
              
               // system.debug('#!@#!@#!@#!@ExistingOppProductRecord = '+  ExistingOppProductRecord);
               // system.debug('#!@#!@#!@#!@ClonedOppProductRecord = '+  ClonedOppProductRecord);
                MapOfOppProductToUpsert.put(ExistingOppProductRecord.id,ExistingOppProductRecord);
                MapOfOppProductToUpsert.put(ExistingOppProductRecord.id+'Cloned',ClonedOppProductRecord);
                                
                for(RevenueSchedule__c ExistingRevenueScheduleObj : ExistingOppProductRecord.RevenueSchedule__r)
                {
                  //  system.debug('#!@$@#@#!@$#ExistingRevenueScheduleObj.RollingYearRevenueLocal__c'+ExistingRevenueScheduleObj.RollingYearRevenueLocal__c);
                  //  system.debug('#!@$@#@#!@$#ExistingRevenueScheduleObj'+ ExistingRevenueScheduleObj);
                    
                    RevenueSchedule__c ClonedRevenueScheduleObj = (RevenueSchedule__c)ClonedRevenueScheduleSObjectsMap.get(ExistingRevenueScheduleObj.id);
                    Decimal RollingYearRevenueLocal = ExistingRevenueScheduleObj.RollingYearRevenueLocal__c;
                     // system.debug('#!@$@#@#!@$#ClonedRevenueScheduleObj '+ ClonedRevenueScheduleObj );
                  
                   //  system.debug('#!@$@#@#!@$#ExistingRevenueScheduleObj.RollingYearRevenueLocal__c'+ExistingRevenueScheduleObj.RollingYearRevenueLocal__c);
                    // system.debug('#!@#!$!@#@@$#ClonedRevenueScheduleObj.RollingYearRevenueLocal__c'+ClonedRevenueScheduleObj.RollingYearRevenueLocal__c);
                    
                    if(RollingYearRevenueLocal != null && RollingYearRevenueLocal != 0){
                        ExistingRevenueScheduleObj.RollingYearRevenueLocal__c = (70*RollingYearRevenueLocal)/100;
                        ClonedRevenueScheduleObj.RollingYearRevenueLocal__c = (30*RollingYearRevenueLocal)/100;
                    }
                    else
                    {
                        ExistingRevenueScheduleObj.RollingYearRevenueLocal__c = null;
                        ClonedRevenueScheduleObj.RollingYearRevenueLocal__c = null;
                    }
                   //  system.debug('#!@$@#@#!@$#ExistingRevenueScheduleObj'+ ExistingRevenueScheduleObj);
                    //  system.debug('#!@$@#@#!@$#ClonedRevenueScheduleObj '+ ClonedRevenueScheduleObj );
                  
                    MapOfRevenueScheduleToUpsert.put(ExistingRevenueScheduleObj.id,ExistingRevenueScheduleObj);
                    MapOfRevenueScheduleToUpsert.put(ExistingRevenueScheduleObj.id+'Cloned',ClonedRevenueScheduleObj);
                }
                for(ProductSchedule__c ExistingProductScheduleObj : ExistingOppProductRecord.ProductSchedules__r)
                {
                    ProductSchedule__c ClonedProductScheduleObj = (ProductSchedule__c)ClonedProductScheduleSObjectsMap.get(ExistingProductScheduleObj.id);
                    Decimal RevenueLocal = ExistingProductScheduleObj.RevenueLocal__c;
                    if(RevenueLocal != null && RevenueLocal != 0){
                        ExistingProductScheduleObj.RevenueLocal__c = (70*RevenueLocal)/100;
                        ClonedProductScheduleObj.RevenueLocal__c = (30*RevenueLocal)/100;
                    }
                    else
                    {
                        ExistingProductScheduleObj.RevenueLocal__c = null;
                        ClonedProductScheduleObj.RevenueLocal__c = null;
                    }
                    MapOfProductScheduleToUpsert.put(ExistingProductScheduleObj.id,ExistingProductScheduleObj);
                    MapOfProductScheduleToUpsert.put(ExistingProductScheduleObj.id+'Cloned',ClonedProductScheduleObj);
                }
            //}
        }
        //system.debug('#!@#!@$!@#!@'+MapOfOppProductToUpsert);
        upsert MapOfOppProductToUpsert.values();
        if(MapOfRevenueScheduleToUpsert.size()>0)
        {
            for(String KeyForMap :MapOfRevenueScheduleToUpsert.keyset())
            {
                if((KeyForMap.contains('Cloned')))
                {
                    RevenueSchedule__c TempObj = MapOfRevenueScheduleToUpsert.get(KeyForMap);
                    String KeyForOppProductMap = TempObj.OpportunityProduct__c+'Cloned';
                    if(MapOfOppProductToUpsert.containsKey(KeyForOppProductMap))
                    {
                        TempObj.OpportunityProduct__c = MapOfOppProductToUpsert.get(KeyForOppProductMap).id;
                        MapOfRevenueScheduleToUpsert.put(KeyForMap,TempObj);
                    }
                }
            }
            upsert MapOfRevenueScheduleToUpsert.values();
        }
        if(MapOfProductScheduleToUpsert.size()>0)
        {
            for(String KeyForMap :MapOfProductScheduleToUpsert.keyset())
            {
                if((KeyForMap.contains('Cloned')))
                {
                    ProductSchedule__c TempProductScheduleObj = MapOfProductScheduleToUpsert.get(KeyForMap);
                    String KeyForOppProductMap = TempProductScheduleObj.OpportunityProductId__c+'Cloned';
                    String KeyForRevenueScheduleMap = TempProductScheduleObj.RevenueScheduleId__c+'Cloned';
                    if(MapOfOppProductToUpsert.containsKey(KeyForOppProductMap) && MapOfRevenueScheduleToUpsert.containsKey(KeyForRevenueScheduleMap))
                    {
                        TempProductScheduleObj.OpportunityProductId__c = MapOfOppProductToUpsert.get(KeyForOppProductMap).id;
                        TempProductScheduleObj.RevenueScheduleId__c = MapOfRevenueScheduleToUpsert.get(KeyForRevenueScheduleMap).id;
                        MapOfProductScheduleToUpsert.put(KeyForMap,TempProductScheduleObj);
                    }
                }
            }
            upsert MapOfProductScheduleToUpsert.values();
        }
   }
   global void finish(Database.BatchableContext BC)
   {
        
   }
}