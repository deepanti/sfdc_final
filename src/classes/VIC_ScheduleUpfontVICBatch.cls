global class VIC_ScheduleUpfontVICBatch implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        VIC_CalculateUpfrontIncentivesBatch upfrontBatch = new VIC_CalculateUpfrontIncentivesBatch(false);
        Database.executeBatch(upfrontBatch, 2);
    }   
}