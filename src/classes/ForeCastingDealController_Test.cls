@isTest
public class ForeCastingDealController_Test {
    public static Opportunity opp,opp1;
    public static User u, u1;
      public static testMethod void testGetVerticalPicklist(){
        ForeCastingDealController.getVerticalPicklist();
    }
    public static testMethod void testGetforcastStage(){
        ForeCastingDealController.getforcastStage();
    }
    public static testMethod void testGetForecastCategorycustomsetting(){
        ForeCastingDealController.getForecastCategorycustomsetting();
    }
    public static testMethod void testGetOpportunitycloseMonth(){
        ForeCastingDealController.getOpportunitycloseMonth();
    }
    public static testMethod void testGetforcastpicklist(){
        ForeCastingDealController.getforcastpicklist();
    }
    public static testMethod void testGetOpportunities(){
        ForeCastingDealController.getOpportunities();
    }
    public static testMethod void testControllerMethods(){
        setupTestData();
        string vertical = '×BFS×Capital Markets×CPG×Healthcare×High Tech×Life Sciences';
        string quater = '2';
        string cloasedateyear = 'CurrentYear';
        string supersl = u.id;
        string dealValue = '2';
        string sl = u1.id;
        //string updateopp = '[{"Id":"006O000000CBcNh","ForeCastingCategoryName":"Solid"},{"Id":"00690000014LlL4","ForeCastingCategoryName":"Solid"}]';
        string updateopp = '[{"Id":"'+opp.id+'","ForeCastingCategoryName":"Solid"},{"Id":"'+opp1.id+'","ForeCastingCategoryName":"Solid"}]';
       
        ForeCastingDealController.getfilterOpportunity(dealValue,vertical,quater,cloasedateyear,supersl,sl);
        ForeCastingDealController.updateopportunity(updateopp);
    }
    public static testMethod void testControllerMethods1(){
       setupTestData();
        string vertical = '';
        string quater = '2';
        string dealValue = '1';
        string supersl = u.id;
        string  cloasedateyear ='NextYear';
        string sl = u1.id;
        ForeCastingDealController.getfilterOpportunity(dealValue,vertical,quater,cloasedateyear,supersl,sl);
    }
    public static testMethod void testControllerMethods2(){
       setupTestData();
        string vertical = '';
        string quater = '1';
        string dealValue = '2';
        string  cloasedateyear ='NextYear';
        string supersl = u.id;
        string sl = u1.id;
        ForeCastingDealController.getfilterOpportunity(dealValue,vertical,quater,cloasedateyear,supersl,sl);
    }
  
    static void setupTestData(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        System.runAs(u){
            Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                                oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
            oAccount.Sales_Unit__c = salesunit.id;
            
            
            Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                                'test121@xyz.com','99999999999');
            
            
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            Test.startTest();
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            opp1 =new opportunity(name='1235',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+3,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp1;
        } 
    }
}