public class GPSelectorBudgetMaster extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Budget_Master__c.Name,
            GP_Budget_Master__c.Id
        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Budget_Master__c.sObjectType;
    }

    public List < GP_Budget_Master__c > selectById(Set < ID > idSet) {
        return (List < GP_Budget_Master__c > ) selectSObjectsById(idSet);
    }
    
    public List < GP_Budget_Master__c > selectAllMasterBudgetRecords() {
        return [SELECT Id, GP_Band__c, GP_Country__c, GP_Product__c, GP_Cost_Per_Hour__c
            FROM GP_Budget_Master__c where GP_Band__c != null and GP_Country__c != null and GP_Product__c != null
        ];

    }
}