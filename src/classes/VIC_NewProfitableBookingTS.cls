/*
 * @author: Prashant Kumar1
 * @since: 24/05/2018
 * @description: This calculation returns the insentive for plans
 * associated with NewProfitableBookingTS plan component
 * 
*/

public class VIC_NewProfitableBookingTS implements VIC_CalculationInterface{
    /*
     * @params: OpportunityProduct, planCode
     * @return: Incentive for New Profitable Booking TS component for 
     * BDE, Capital_Market_GRM and EnterpriseGRM plan codes. Developer need to make specific implementation 
     * while calling for anniversary calculation of BDE plan. Actual Incentive in this case will:
     * returned_value - Incentive_given_at_POS (if greater than zero, otherwise 0)
     *
    */
    public Decimal calculate(OpportunityLineItem oppProd, String planCode){
        Decimal incentive = 0;
        
        if(oppProd != null && planCode != null && oppProd.VIC_NPV__c != null){
            if(oppProd.vic_IO_TS__c == 'TS'){
                if(oppProd.Opportunity.CloseDate != null
                   && oppProd.VIC_NPV__c != null){
                       if(planCode == 'BDE'){
                           incentive = calculateBDE(oppProd);
                       }
                       else if(planCode == 'Capital_Market_GRM'){
                           incentive = calculateCapitalMarketGRM(oppProd);
                       }
                       else if(planCode == 'EnterpriseGRM'){
                           incentive = calculateEnterpriseGRM(oppProd);
                       }
                   }
                
            }
        }
        else{
            System.debug('Any one from Opportunity product, plan code and NPV(VIC_NPV__c) is null,'
                          + ' so New Profitable Booking TS component can not be calculated!');
        }
        return incentive;
    }
    
    /*
     * @params: opportunityProduct
     * @return: Incentive(Decimal)
     * @description: This method returns Incentive for new profitable booking TS component for BDE plan.
     * 
    */
    @TestVisible
    private Decimal calculateBDE(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        String bdeTSIncentivePercentStr = 
                VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB BDE TS Incentive Percent');
        Decimal bdeTSIncentivePercent = 
            VIC_GeneralChecks.isDecimal(bdeTSIncentivePercentStr) ? Decimal.valueOf(bdeTSIncentivePercentStr) : 0;
        
        incentive = oppProd.VIC_NPV__c * bdeTSIncentivePercent / 100;
        oppProd.vic_VIC_Grid__c = bdeTSIncentivePercent;
        return incentive;
    }
    
    /*
     * @params: opportunityProduct
     * @return: Incentive(Decimal)
     * @description: This method returns Incentive for new profitable booking TS component for Capital_Market_GRM plan.
     * 
    */
    @TestVisible
    private Decimal calculateCapitalMarketGRM(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        String strOppSource = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Source');
        if(oppProd.Opportunity.Opportunity_Source__c != null && oppProd.Opportunity.Opportunity_Source__c.equalsIgnoreCase(strOppSource)){
            incentive = 0;
        }else{
            String cmGRMTSIncentivePercentStr = 
                    VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB TS CM GRM Incentive Percent');
            Decimal cmGRMTSIncentivePercent = 
                VIC_GeneralChecks.isDecimal(cmGRMTSIncentivePercentStr) ? Decimal.valueOf(cmGRMTSIncentivePercentStr) : 0;
            
            incentive = oppProd.VIC_NPV__c * cmGRMTSIncentivePercent / 100;
            oppProd.vic_VIC_Grid__c = cmGRMTSIncentivePercent;
        }
        return incentive;
    }
    
    /*
     * @params: opportunityProduct
     * @return: Incentive(Decimal)
     * @description: This method returns Incentive for new profitable booking TS component for EnterpriseGRM plan.
     * 
    */
    @TestVisible
    private Decimal calculateEnterpriseGRM(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        String eGRMTSIncentivePercentStr = 
                VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB TS E GRM Incentive Percent');
        Decimal eGRMTSIncentivePercent = 
            VIC_GeneralChecks.isDecimal(eGRMTSIncentivePercentStr) ? Decimal.valueOf(eGRMTSIncentivePercentStr) : 0;
        
        incentive = oppProd.VIC_NPV__c * eGRMTSIncentivePercent / 100;
         oppProd.vic_VIC_Grid__c = eGRMTSIncentivePercent;
        return incentive;
    }
}