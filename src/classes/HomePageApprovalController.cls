public without sharing class HomePageApprovalController {
    /*
        This Method fetch all opportunity which are pending for Contract Approval or QSRM Approval
    */
    @AuraEnabled //HomePageApprovalController.fetchOpportunityPendingForApporval()
    public static List<ApprovalDetailsWrapper> fetchOpportunityPendingForApporval()
    {
        List<ApprovalDetailsWrapper> approvalWrapper = new List<ApprovalDetailsWrapper>();
        try{
            Set<Id> opportunityIds = new Set<Id>();
            Set<Id> qsrmApprovalIds = new Set<Id>();
            Set<Id> qsrmAppID = new set<Id>();
            Map<Id,Id> qsrmToOppMap = new Map<Id,Id>();
            Map<Id,Opportunity> opportunityDetailsMap = new Map<Id,Opportunity>();
            List<ProcessInstanceWorkItem> approvalItemsLst = new List<ProcessInstanceWorkItem>();
            
            approvalItemsLst = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                WHERE ProcessInstance.Status = 'Pending' AND ActorId =: UserInfo.getUserId()];
            Set<string> pendingQSRMApprovalIds = new Set<string>();
            system.debug('HomePage --:'+approvalItemsLst.size());
            
            for (ProcessInstanceWorkItem workItem : approvalItemsLst) 
            {
                if(workItem != null)
                {
                    if(workItem.ProcessInstance.TargetObjectId.getsobjecttype() == getoidSobjectType('QSRM__c')){
                        pendingQSRMApprovalIds.add(workItem.ProcessInstance.TargetObjectId);
                    }
                }
            }
            system.debug('HomePage --pendingQSRMApprovalIds:'+pendingQSRMApprovalIds);
            list<qsrm__c> qsrmList = [Select id,Opportunity__c from QSRM__C Limit 5];
            system.debug('HomePage --qsrmAppID:'+qsrmList.size());
            Map<id,QSRM__C> mapqsrms = new map<id,QSRM__c>([Select id,Opportunity__c from QSRM__C where ID IN : pendingQSRMApprovalIds]);
            qsrmAppID = mapqsrms.keyset();
            system.debug('HomePage --qsrmAppID:'+mapqsrms);
          
            if(qsrmAppID.size() > 0){
                for(QSRM__C qs : mapqsrms.values()){
                    opportunityIds.add(qs.Opportunity__c);
                    qsrmToOppMap.put(qs.id,qs.Opportunity__c);
                }
            }
            if(opportunityIds.size() > 0)
            {
                opportunityDetailsMap = new Map<Id,Opportunity>([Select id,AccountId,Account.Name,Name,Opportunity_ID__c,TCV1__c,StageName
                                                                 ,(Select Product_Name__c from OpportunityLineItems),(SELECT ID,name,QSRM_Type__c From Qsrms__r) From Opportunity Where Id in: opportunityIds
                                                                 And StageName not in ('6. Signed Deal','7. Lost','8. Dropped') AND QSRM_Status__c = 'Your QSRM is submitted for Approval']);    
            }
            system.debug('HomePage --qsrmToOppMap:'+opportunityDetailsMap.keyset());
            for(ProcessInstanceWorkItem approvalItem : approvalItemsLst)
            {
                ApprovalDetailsWrapper temp = new ApprovalDetailsWrapper();
                Integer days = approvalItem.CreatedDate.Date().daysBetween(System.today());
                
                if(qsrmAppID.contains(approvalItem.ProcessInstance.TargetObjectId))
                {
                    temp.recId = qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId);
                    if(opportunityDetailsMap.keySet().contains(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)))
                    {
                        
                        temp.opp_Id = opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).Opportunity_ID__c;
                        temp.oppName = opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).Name;
                        temp.accountName = opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).Account.Name;
                        temp.accountId = opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).AccountId;
                        temp.status = 'QSRM Approval Pending For '+days+' days';
                        temp.approvalType = 'QSRM';
                        temp.approvalItemId = approvalItem.Id;
                        temp.qsrmId=opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).qsrms__r[0].ID;
                        temp.qsrmName=opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).qsrms__r[0].name;
                        temp.qsrmNature=opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).qsrms__r[0].QSRM_Type__c;
                        if(opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).OpportunityLineItems.size() > 0)
                            temp.prodList = HomePageApprovalController.convertToString(opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).OpportunityLineItems);
                        else
                            temp.prodList = '';
                        temp.TCV = opportunityDetailsMap.get(qsrmToOppMap.get(approvalItem.ProcessInstance.TargetObjectId)).TCV1__c;
                        approvalWrapper.add(temp);
                    }
                }
            }
            system.debug('Homepage==:'+approvalWrapper.size()+'home p==='+approvalWrapper);
            return approvalWrapper;
            
        }catch(exception e){
            system.debug('HomePageApprovalController==error Msg=:'+e.getMessage()+':===error line==:'+e.getLineNumber());
            CreateErrorLog.createErrorRecord('HomePageApprovalController',e.getMessage(), '', e.getStackTraceString(),'HomePageApprovalController', 'fetchOpportunityPendingForApporval','Fail','',String.valueOf(e.getLineNumber()));  
            return approvalWrapper;
        }
    }
    /****************************************************************************
        Method to fetch sObject Type 
    ****************************************************************************/
    private static Schema.SObjectType getoidSobjectType(String objectAPIName)
    {
        Map<String, Schema.SObjectType> globalDescription =  Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = globalDescription.get(objectAPIName); 
        return sObjType;
    }
    private static String convertToString(List<OpportunityLineItem> prodList)
    {
        String str = '';
        if(prodList.size() > 0)
        {
            for(OpportunityLineItem prod: prodList)
            {
                str += prod.Product_Name__c;
                str += ',';
            }
        }
        
        return str.substring(0, str.length()-1);
    }
    /****************************************************************************
        Method to Approve or Reject Pending Items
    ****************************************************************************/
    @AuraEnabled 
    public static boolean approvePendingItems(Id approvalPendingItemId,String comments,String action){
        try{
            system.debug(':HomePageApp====:'+approvalPendingItemId+':==comments==:'+comments+':==action==:'+action);
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments(comments);
            if(action =='Rework'){
                req.setAction('Removed');
            }else {
                req.setAction(action);
            }
            req.setWorkitemId(approvalPendingItemId);
            
            ProcessInstanceWorkItem approvalItemsLst =[SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem where id =:approvalPendingItemId Limit 1];
            
            system.debug('HomePageapproval ==approvalItemsLst='+approvalItemsLst);
            Approval.ProcessResult result =  Approval.process(req);
            QSRM__c objqsrm = [Select id,Status__c,CreatedById,Service_Line_leader_SL_Approval__c from qsrm__c where id =: approvalItemsLst.ProcessInstance.TargetObjectId ];
           system.debug('HomePageapproval ==='+objqsrm.Service_Line_leader_SL_Approval__c);
            system.debug('HomePageapproval ==='+objqsrm.Status__c);
            system.debug('HomePageapproval ==='+objqsrm.Status__c);
            system.debug('HomePageapproval ==action='+action);
            if(action =='Rework'){  
                ApprovalQsrmController.sendReworkQSRMEmailTemplate('',objqsrm.CreatedById,'Rework Email To Seller',objqsrm.id);
                system.debug(':HomePage==rework=:'+action);
                
            }else if(action =='Approve'){
                if(objqsrm.Status__c =='Approved'){
                    system.debug('HomePage==Status__c=:'+action);
                    ApprovalQsrmController.sendReworkQSRMEmailTemplate('',objqsrm.CreatedById,'Email To Seller when QSRM is approved',objqsrm.id);
                }else if(objqsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                    system.debug('HomePage==Service_Line_leader_SL_Approval__c=:'+action);
                    ApprovalQsrmController.sendReworkQSRMEmailTemplate('',objqsrm.CreatedById,'Email To Seller when QSRM is approved by service line leader',objqsrm.id);
                }
            }else if(action =='Reject'){
                system.debug('HomePage==action=:'+action);
                ApprovalQsrmController.sendReworkQSRMEmailTemplate('',objqsrm.CreatedById,'Email Alert to Seller when QSRM is rejected',objqsrm.id);
            }
            return result.isSuccess(); 
        }catch(exception e){
            system.debug(':HomePageapproval error ===:'+e.getMessage()+':====error ===:'+e.getLineNumber());
            return false;
        }
    }
    /****************************************************************************
        Method to fetch Opportunity which are pending for QSRM approval
    ****************************************************************************/
    @AuraEnabled
    public static List<ApprovalDetailsWrapper> fetchOpportunityPendingForQSRMApproval()
    {
        List<ApprovalDetailsWrapper> wrapparList = new List<ApprovalDetailsWrapper>();
        List<Opportunity> oppsList = [Select Id,AccountId,Account.Name,Name,Opportunity_ID__c,TCV1__c,StageName,(Select Product_Name__c 
                                     from OpportunityLineItems),(Select ID,name,Status__c from QSRMS__r limit 1) From Opportunity Where OwnerId =:userInfo.getUserId()
                                     And (QSRM_status2__c > 0 OR Roll_Up_QSRM_Rework__c > 0)And (QSRM_Status__c = 'Your QSRM is submitted for Approval' OR QSRM_Status__c = 'Rework')
                                     And StageName not in ('6. Signed Deal','7. Lost','8. Dropped')];
        for(Opportunity opp: oppsList)
        {
            ApprovalDetailsWrapper temp = new ApprovalDetailsWrapper();
            List<QSRM__C> qsrmLst=opp.qsrms__r;
            temp.recId = opp.Id;
            temp.opp_Id = opp.Opportunity_ID__c;
            temp.oppName = opp.Name;
            temp.accountName = opp.Account.Name;
            temp.accountId = opp.AccountId;
            system.debug('HomePageApprovalController====:Rework'+qsrmLst[0].status__C);
            system.debug('HomePageApprovalController====:Rework'+qsrmLst[0].name);
            if(qsrmLst[0].status__C == 'Rework'){
                system.debug('HomePageApprovalController====:Rework');
                temp.Status = 'Rework';
            }else{
                temp.Status = 'QSRM Approval Pending';
            }
            temp.approvalType = 'QSRM';
            temp.approvalItemId = opp.Id;
            temp.qsrmId = qsrmLst[0].Id;
            temp.qsrmName=qsrmLst[0].name;
            temp.prodList = HomePageApprovalController.convertToString(opp.OpportunityLineItems);
            temp.TCV=opp.TCV1__c;
            wrapparList.add(temp);
        }
        return wrapparList;
    }
    public class ApprovalDetailsWrapper
    {
        @AuraEnabled public Id recId;
        @AuraEnabled public String opp_Id;
        @AuraEnabled public String oppName;
        @AuraEnabled public String accountName;
        @AuraEnabled public Id accountId;
        @AuraEnabled public String status;
        @AuraEnabled public String approvalType;
        @AuraEnabled public Id approvalItemId;
        @AuraEnabled public string prodList;
        @AuraEnabled public Decimal TCV;
        @AuraEnabled public Id qsrmId;
        @AuraEnabled public String qsrmName;
        @AuraEnabled public String qsrmNature;
    }
    public static void iterateMethod(){
      integer i =0;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
    }
}