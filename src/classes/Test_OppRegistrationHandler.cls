@isTest 
private class Test_OppRegistrationHandler {
    static testMethod void validateGAManagerInsert() {
        // Assign Values on OppReg Test Record
        Opportunity_Registration__c oppReg 			= new Opportunity_Registration__c();
        oppReg.If_No_Account_Match_Found__c			= 'XYZ Account';
        oppReg.Primary_Contact_Text__c				= 'John Smith';
        oppReg.Department__c						= 'Healthcare';
        oppReg.Deal_Region__c						= 'North America';
        oppReg.Deal_Country__c						= 'United States';
        oppReg.Status__c							= 'In Process';
        oppReg.Deal_Pursuit_Expectation__c			= 'Joint deal pursuit';
        oppReg.Project_Delivery_Expectation__c		= 'Genpact is acting as Prime';
        oppReg.Opportunity_Registration_Description__c = 'Healthcare OppReg';
        oppReg.Deal_Pursuit_Activities__c			= 'Provide new sales lead to Genpact';
        oppReg.Other_Comments__c					= 'No Other Comments';
        // Insert OppReg Record
        insert oppReg;
    }
}