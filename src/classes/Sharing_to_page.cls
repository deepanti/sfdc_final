public class Sharing_to_page{

  public String selectedOption{get; set;}
  public Sharing_SP__c taggedSP{ get; set; }
  public string Extrnlid{get; set;}
  public string Opptyid{get; set;}
  public string DSRid{get; set;}
  Public items_Sharepoint_Files__x Extobj{get; set;}
  public string ExtItem_id {get; set;} 
  Public List<String> Ids {get; set;}
 
   
    
 public Sharing_to_page(){    
    taggedSP = new Sharing_SP__c (); 
     
    
     if (ApexPages.currentPage().getParameters().get('Extrnlid') != NULL) 
        {
        Extrnlid= ApexPages.currentPage().getParameters().get('Extrnlid');
        }
        
        if (ApexPages.currentPage().getParameters().get('Opid') != NULL) 
        {
        Opptyid= ApexPages.currentPage().getParameters().get('Opid');
        }
       
        if (ApexPages.currentPage().getParameters().get('dsrid') != NULL) 
        {
        DSRid= ApexPages.currentPage().getParameters().get('dsrid');
        } 
        
       if(!Test.isRunningTest()) 
                 Extobj=[select ExternalId,DisplayUrl,DownloadUrl__c from items_Sharepoint_Files__x where id=:Extrnlid];
        
  }
  
  Public Void Reset(){
          taggedSP = new Sharing_SP__c ();
  }
  
  Public void save(){
  
  if(Opptyid != null) taggedSP.Opportunity__c= Opptyid;
  if(DSRid != null)   taggedSP.DSR__c=DSRid;
  if(!Test.isRunningTest())      taggedSP.Sharepoint_files__c=Extobj.ExternalId;
 
 
  Insert taggedSP ;
  
       FeedItem contentPost = new FeedItem();
          
          if(selectedOption=='Account')
          {
                contentPost.ParentId = taggedSP.Account__c;
                contentPost.Type = 'LinkPost';
                contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + Extrnlid; 
                System.Debug('Commentbody: ' + taggedSP.Comments__c );
                if (taggedSP.Comments__c != null && taggedSP.Comments__c != '') {
                    string key= this.taggedSP.Comments__c;
                    contentPost.Body = key;
                     }
      }
      
      else if(selectedOption=='Contact')
          {
                contentPost.ParentId = taggedSP.Contact__c;
                contentPost.Type = 'LinkPost';
                contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + Extrnlid; 
                System.Debug('Commentbody: ' + taggedSP.Comments__c );
                if (taggedSP.Comments__c != null && taggedSP.Comments__c != '') {
                    string key= this.taggedSP.Comments__c;
                    contentPost.Body = key;
                     }
          }
      
          else if(selectedOption=='Lead')
          {
                contentPost.ParentId = taggedSP.Lead__c;
                contentPost.Type = 'LinkPost';
                contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + Extrnlid; 
                System.Debug('Commentbody: ' + taggedSP.Comments__c );
                if (taggedSP.Comments__c != null && taggedSP.Comments__c != '') {
                    string key= this.taggedSP.Comments__c;
                    contentPost.Body = key;
                     }
          }
          
          else if(selectedOption=='DSR')
          {
                contentPost.ParentId = taggedSP.DSR__c;
                contentPost.Type = 'LinkPost';
                contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + Extrnlid; 
                System.Debug('Commentbody: ' + taggedSP.Comments__c );
                if (taggedSP.Comments__c != null && taggedSP.Comments__c != '') {
                    string key= this.taggedSP.Comments__c;
                    contentPost.Body = key;
                     }
                     
            }         
           try {
                         
                insert contentPost;
                } catch (DmlException e) { System.debug(LoggingLevel.ERROR, e.getMessage()); }
                
                
          if(selectedOption=='All')
                {
                  Ids = new List<String>();
                  if(taggedSP.Account__c !=NULL) Ids.add(taggedSP.Account__c);
                      
                  if(taggedSP.Contact__c!=NULL)Ids.add(taggedSP.Contact__c);
                  
                  if(taggedSP.Lead__c!=NULL) Ids.add(taggedSP.Lead__c);
                      
                  if(taggedSP.DSR__c!=NULL) Ids.add(taggedSP.DSR__c);
                      
                          
               
         List<FeedItem> Post_to_Objects = new List<FeedItem>();
         
           IF(!Ids.isempty() ){ 
              for(String Str: Ids)
                {
                         FeedItem Post = new FeedItem();
                         Post.ParentId = Str;
                         Post.Type = 'LinkPost';
                         Post.LinkUrl = 'https://genpact.my.salesforce.com/' + Extrnlid;    
                        
                        if (taggedSP.Comments__c != null && taggedSP.Comments__c != '') {
                            string key= this.taggedSP.Comments__c;
                            Post.Body = key;
                         }
                
                        Post_to_Objects.add(Post);
                        
                        }
                        
                        }
                        
                         if(!Post_to_Objects.isempty()) { 
                             Insert Post_to_Objects;
                         }
      
            // Attempt to create the Feed post
               
                }
                
                Reset();
                
                        
    }
    
    
    Public PageReference Done(){
    
            PageReference pageRef = new PageReference('/' + Extrnlid);
            pageRef.setRedirect(true);
            return pageRef; 
    
    }
    
  }