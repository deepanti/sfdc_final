public class GPCmpServiceProjectSubmission {
    @AuraEnabled
    public static GPAuraResponse getProjectStatus(Id projectId) {
        system.debug('submit record');
        GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(projectId);
        return submissionController.getProjectStatus();
    }

    @AuraEnabled
    public static GPAuraResponse getProjectStatusForClosure(Id projectId) {
        GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(projectId);
        return submissionController.getProjectStatusForClosure();
    }

    @AuraEnabled
    public static GPAuraResponse submitforApproval(Id projectId, string strStageValues, String userComments,String bandApprover) {
        GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(projectId);
        return submissionController.submitforApproval(strStageValues, userComments,bandApprover);
    }

    @AuraEnabled 
    public static GPAuraResponse submitforClosure(Id projectId, string strStageValues, String userComments,String bandApprover) {
        GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(projectId);
        return submissionController.submitforApprovalForClosure(strStageValues, userComments,bandApprover);
    }

    @AuraEnabled
    public static GPAuraResponse recallRequest(Id projectId) {
        GPControllerProjectSubmission submissionController = new GPControllerProjectSubmission(projectId);
        return submissionController.recallApprovalProcess(projectId);
    }
}