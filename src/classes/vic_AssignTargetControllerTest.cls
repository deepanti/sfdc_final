@isTest(SeeAllData=false)
public class vic_AssignTargetControllerTest{
    static testMethod void validateTargetUserComponentAuraController() {
        Profile pId=[Select id from Profile where name='System Administrator'];
        
        User u = new User(
            ProfileId = pId.Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            OHR_ID__c='326001234'
        );
        insert u;
        
        APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
        insert objConga;
        
        Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
        insert masterVICRoleObj;
        
        Plan__c planObj = new Plan__c();
        planObj.Is_Active__c = false;
        planObj.Year__c = '2017';
        insert planObj;
        
        Plan__c planObj1 = new Plan__c();
        planObj1.Is_Active__c = true;
        planObj1.Year__c = '2017';
        planObj1.Conga_Template__c=objConga.Id;
        insert planObj1;
       
        VIC_Role__c VICRoleObj = new VIC_Role__c();
        VICRoleObj.Master_VIC_Role__c=masterVICRoleObj.Id;
        VICRoleObj.Plan__c=planObj1.Id;
        insert VICRoleObj;
        
        User_VIC_Role__c userVICRole = new User_VIC_Role__c();
        userVICRole.User__c=u.Id;
        userVICRole.Master_VIC_Role__c=masterVICRoleObj.Id;
        insert userVICRole;
        
        Target__c targetObj2 = new Target__c();
        targetObj2.Start_Date__c = system.today();
        targetObj2.user__c =u.Id;
        
        Target__c targetObj = new Target__c();
        targetObj.Start_Date__c = system.today();
        targetObj.user__c =u.Id;
        targetObj.Status__c = 'Released';
        insert targetObj;
        
        Master_Plan_Component__c masterPlanComponentObj = new Master_Plan_Component__c();
        masterPlanComponentObj.Name = 'ABC';
        //masterPlanComponentObj.Target_Frequency__c = 'Monthly';
        //masterPlanComponentObj.Payout_Frequency__c   = 'Monthly';
        masterPlanComponentObj.Point_type__c = 'Currency';
        insert masterPlanComponentObj;
        
        Plan_Component__c planComponentObj = new Plan_Component__c();
        planComponentObj.Master_Plan_Component__c=masterPlanComponentObj.Id;
        planComponentObj.Plan__c=planObj1.Id;
        insert planComponentObj;
        
        vic_AssignTargetController.fetchTargetUserDetails(targetObj.Id);
        List<Target_Component__c> targetComponentObjList = new List<Target_Component__c>();
        
        Target_Component__c targetComponentObj = new Target_Component__c();
        targetComponentObj.Target__c = targetObj.Id;
        targetComponentObj.Target_Status__c = 'Active';
        targetComponentObj.Is_Weightage_Applicable__c = true;
        targetComponentObj.Target_In_Currency__c = 1;
        targetComponentObjList.add(targetComponentObj);
        insert targetComponentObjList;
        
        vic_AssignTargetController.saveTargetUser(targetObj2, 2017);
        vic_AssignTargetController.updatingTargetComponent(targetObj, targetComponentObjList);
        vic_AssignTargetController.getFinancialYear();
        vic_AssignTargetController.getCurrentYear();
        vic_AssignTargetController.saveingTargetComponent(targetComponentObjList);
        vic_AssignTargetController.checkUserStatus(userVICRole.User__c,'2017');
        vic_AssignTargetController.checkUserStatus(null,'');
        system.assertequals(200,200);
    }
    
}