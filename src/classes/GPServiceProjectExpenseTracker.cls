@isTest
private class GPServiceProjectExpenseTracker {
    private static GP_Project__c prjObj, objChildProj,prjObj2;
    private static list < GP_Project_Expense__c > lstofProjExpense;
    
    static void setupCommonData() {
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        insert prjObj;
        
        prjObj2 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj2.OwnerId = objuser.Id;
        insert prjObj2;
        
        objChildProj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        objChildProj.OwnerId = objuser.Id;
        objChildProj.GP_Parent_Project__c = prjObj.Id;
        insert objChildProj;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        insert projectLeadership;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.Id, prjObj.Id);
        insert resourceAllocationObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        
        projectExpense.GP_Amount__c = 12;
        projectExpense.GP_Data_Source__c = 'OMS';
        projectExpense.GP_Expenditure_Type__c = 'Billable';
        projectExpense.GP_Expense_Category__c = 'Hardware';
        projectExpense.GP_OMS_ID__c = '1';
        projectExpense.GP_Remark__c = '2 Remark update';
        insert projectExpense;
        
        GP_Project_Expense__c projectExpense1 = new GP_Project_Expense__c();
        projectExpense1.GP_Amount__c = 12;
        projectExpense1.GP_Data_Source__c = 'OMS';
        projectExpense1.GP_Expenditure_Type__c = 'Billable';
        projectExpense1.GP_Expense_Category__c = 'Hardware';
        projectExpense1.GP_OMS_ID__c = '2';
        projectExpense1.GP_Remark__c = '2 Remark update';
        projectExpense1.GP_Project__c = prjObj.ID;
        insert projectExpense1;
        
        GP_Project_Expense__c projectExpense2 = new GP_Project_Expense__c();
        projectExpense2.GP_Amount__c = 0;
        //projectExpense2.GP_Data_Source__c = 'OMS';
        projectExpense2.GP_Expenditure_Type__c = 'Billable';
        projectExpense2.GP_Expense_Category__c = 'Hardware';
        //projectExpense2.GP_OMS_ID__c = '3';
        projectExpense2.GP_Remark__c = '2 Remark update';
        projectExpense2.GP_Project__c = prjObj.ID;
        insert projectExpense2;
        
    }
    
    @isTest
    private static void testProject() {
        test.startTest();
        setupCommonData();
        fetchData();
        GPServiceProjectExpense.UpdateProjectExpense(prjObj.ID, lstofProjExpense);
        GPServiceProjectExpense.validateProjectExpense(lstofProjExpense);
        GPServiceProjectExpense.isFormattedLogRequired = false;
        GPServiceProjectExpense.validateProjectExpense(lstofProjExpense);
        test.stopTest();
    }
    
    @isTest
    private static void testProject2() {
        test.startTest();
        setupCommonData();
        fetchData();
        GPServiceProjectExpense.UpdateProjectExpense(prjObj2.ID, lstofProjExpense);
        GPServiceProjectExpense.UpdateProjectExpense(prjObj.ID, new list<GP_Project_Expense__c>());
        test.stopTest();
        
    }
    public static void fetchData() {
        prjObj2 = [select id, Name, GP_Delivery_Org__c, GP_Sub_Delivery_Org__c, GP_TCV__c, RecordTypeId, RecordType.Name, GP_Last_Temporary_Record_Id__c from GP_Project__c where id=:prjObj2.id limit 1];
        prjObj = [select id, Name, GP_Delivery_Org__c, GP_Sub_Delivery_Org__c, GP_TCV__c, RecordTypeId, RecordType.Name, GP_Last_Temporary_Record_Id__c from GP_Project__c limit 1];
        lstofProjExpense = [select id, Name, GP_OMS_ID__c,GP_Amount__c,GP_Location__c,GP_Data_Source__c,GP_Expenditure_Type__c,GP_Expense_Category__c,GP_Remark__c,GP_Last_Temporary_Record_Id__c from GP_Project_Expense__c];
    }
    
    
}