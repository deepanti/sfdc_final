/*
    @Author: Vikas Rajput
    @Description: It will bind user and opportunity related data and Target related data
*/
public class VIC_DomicileUserOppDataWrapCtlr{
    @AuraEnabled public String strHrPendingCount; 
    @AuraEnabled public String strDomicile;
    @AuraEnabled public List<VIC_UserOpportunityDataWrapCtlr> lstUserOppData;
    //@Constructor
    public VIC_DomicileUserOppDataWrapCtlr(String strHrPendingCount, String strDomicile, List<VIC_UserOpportunityDataWrapCtlr> lstUserOppData){
        this.strHrPendingCount = strHrPendingCount;
        this.strDomicile = strDomicile;
        this.lstUserOppData = lstUserOppData;
    }

}