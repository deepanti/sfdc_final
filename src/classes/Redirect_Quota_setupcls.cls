/*-------------
        Class Description : This Class helps to redirect user to Sales leader/Bd Rep edit layout based on their profile.
        Organisation        : Tech Mahindra NSEZ
        Created by          : Arjun Srivastava
        Location            : Genpact
        Created Date        : 5 July 2014  
        Last modified date  : 21 July 2014
---------------*/
public class Redirect_Quota_setupcls 
{
    PageReference ReturnPage {get;set;}
    public Redirect_Quota_setupcls(ApexPages.StandardController controller) 
    {       
        Map<String,String> recmap=new Map<String,String>();
        String RecordTypeid,namefldid,QuotaEntityid,username,quotaid;
        RecordTypeid=namefldid=QuotaEntityid=username=quotaid='';
        
        //Creating Recordtype map
        for(recordtype rec : [select id,DeveloperName from recordtype where sobjecttype='Set_Quotas__c'])
            recmap.put(rec.DeveloperName,rec.id);
            
        //Getting Field ids from Custom Setting
        Map<String, Quota_fieldid__c> Quota_fieldidmap= Quota_fieldid__c.getAll(); 
        if(Quota_fieldidmap.keyset().size()>0)
        {
            namefldid = Quota_fieldidmap.get('QuotaName').Field_ID__c;
            QuotaEntityid = Quota_fieldidmap.get('QuotaEntityid').Field_ID__c;       
        
            username=ApexPages.currentPage().getParameters().get(namefldid);
            quotaid=ApexPages.currentPage().getParameters().get(namefldid+'_lkid');
        }
        
        
        List<Quota_header__c> QH_record=[select Sales_Person__r.Sales_Leader__c from Quota_header__c where id=:quotaid];        
        
        IF(QH_record.size()>0)
        {
            if(QH_record[0].Sales_Person__r.Sales_Leader__c)
                RecordTypeid = recmap.get('Opportunity_Line_Owner');
            else                
                RecordTypeid = recmap.get('Account_GRM');      
        }
        
        ReturnPage = new PageReference('/a0L/e?'); 
        ReturnPage.getParameters().put(namefldid,username);
        ReturnPage.getParameters().put(namefldid+'_lkid',quotaid);
        ReturnPage.getParameters().put('RecordType',RecordTypeid);
        ReturnPage.getParameters().put('ent',QuotaEntityid);
        ReturnPage.getParameters().put('retURL',quotaid);
        ReturnPage.getParameters().put('nooverride','1');// Important to avoid recursion  
    }
    
    //Called onload to redirect to standard quota setup edit page
    public pagereference onloadredirect()
    {       
        ReturnPage.setRedirect(true);
        return ReturnPage;
    }
}