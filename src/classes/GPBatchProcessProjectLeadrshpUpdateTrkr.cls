@isTest public class GPBatchProcessProjectLeadrshpUpdateTrkr {
    
    public static GP_Job__c objJobForUpdateProjectLeadership;
    public static String jsonresp;
    public static set<String> setOfProjectOracleIds;
    
    static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Final_OHR__c = '123456778';
        empObj.GP_Person_ID__c = 'EmpP-Id-0';
        empObj.GP_isActive__c = true;
        empObj.GP_ACTUAL_TERMINATION_Date__c = system.today().addDays(30);
        insert empObj;
        
        GP_Employee_Master__c empObj1 = GPCommonTracker.getEmployee();
        empObj1.GP_Final_OHR__c = '877654321';
        empObj1.GP_Person_ID__c = 'EmpP-Id-1';
        empObj1.GP_isActive__c = true;
        empObj1.GP_ACTUAL_TERMINATION_Date__c = system.today().addDays(30);
        insert empObj1;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Oracle_Id__c = '1234';
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Work_Location__c objworkLocation = GPCommonTracker.getWorkLocation();
        objworkLocation.GP_Oracle_Id__c = '5678';
        objworkLocation.GP_Status__c = 'Active and Visible';
        objworkLocation.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation;
        
        GP_Work_Location__c objworkLocation2 = GPCommonTracker.getWorkLocation();
        objworkLocation2.GP_Oracle_Id__c = '9012';
        objworkLocation2.GP_Status__c = 'Active and Visible';
        objworkLocation2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation2;
        
        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Without SFDC';
        insert dealObj;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        insert prjObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBs();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.Id);
        insert objSB;
        
        Account accountObj = GPCommonTracker.getAccount(objBS.Id, objSB.Id);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Leadership_Role__c = 'Billing SPOC';
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.GP_Leadership_Role_Name__c = 'Billing SPOC';
        projectLeadership.GP_Start_Date__c = system.today();
        projectLeadership.GP_Active__c = true;
        projectLeadership.GP_Employee_ID__c = empObj.Id;
        insert projectLeadership;
        
        GP_Leadership_Master__c leadershipMaster1 = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster1.GP_Leadership_Role__c = 'Global Project Manager';
        insert leadershipMaster1;
        
        GP_Project_Leadership__c projectLeadership1 = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership1.GP_Leadership_Role_Name__c = 'Global Project Manager';
        projectLeadership1.GP_Start_Date__c = system.today();
        projectLeadership1.GP_Active__c = true;
        projectLeadership1.GP_Employee_ID__c = empObj.Id;
        insert projectLeadership1;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId(); 
        insert objProjectWorkLocationSDO;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation.Id);
        objProjectWorkLocation.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation2.Id);
        objProjectWorkLocation1.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation1;
        
        GP_Job__c objJobForUpdateProject = GPCommonTracker.getJobRecord('Update Project');
        insert objJobForUpdateProject;
        
        GP_Job__c objJobForTimeSheetUpload = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJobForTimeSheetUpload;
        
        GP_Job__c objJobForUpdateWorkLocation = GPCommonTracker.getJobRecord('Update Project Work Location & SDOs');
        insert objJobForUpdateWorkLocation;
        
        objJobForUpdateProjectLeadership = GPCommonTracker.getJobRecord('Update Project Leadership');
        insert objJobForUpdateProjectLeadership;
        
        GP_Project__c prjObj1 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj1.OwnerId = objuser.Id;
        insert prjObj1;
        
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Parent_Project__c = prjObj1.Id;
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        update prjObj;
        
        List<GP_Temporary_Data__c> listOfJobLineItems = new List<GP_Temporary_Data__c>();
        
        GP_Temporary_Data__c objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJobForTimeSheetUpload.Id,
                                                                                                  'EMP-001',
                                                                                                  '4532',
                                                                                                  '');
        //insert objTemporaryTimeSheet;
        
        GP_Temporary_Data__c objTemporaryProject = GPCommonTracker.getTemporaryDataForProject(objJobForUpdateProject.Id,
                                                                                              '4532');
        //insert objTemporaryProject;
        
        GP_Temporary_Data__c objTemporaryProjectWorkLocation = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
                                                                                                                      '4532',
                                                                                                                      objworkLocation.GP_Oracle_Id__c);
        //insert objTemporaryProjectWorkLocation;
        
        GP_Temporary_Data__c objTemporaryProjectWorkLocation2 = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
                                                                                                                       '4532',
                                                                                                                       objworkLocation2.GP_Oracle_Id__c);
        //insert objTemporaryProjectWorkLocation2;
        
        GP_Temporary_Data__c objTemporaryProjectWorkLocation3 = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
                                                                                                                       '4532',
                                                                                                                       '3452');
        //insert objTemporaryProjectWorkLocation3;
        
        GP_Temporary_Data__c objTemporaryProjectLeadership = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
                                                                                                                  '4532', 'Billing SPOC');
        objTemporaryProjectLeadership.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership.GP_PL_Leadership_Role__c = 'Billing SPOC';
        objTemporaryProjectLeadership.GP_PL_Employee_OHR__c = '877654321';
        //insert objTemporaryProjectLeadership;
        
        GP_Temporary_Data__c objTemporaryProjectLeadership2 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
                                                                                                                   '4532', 'Global Project Manager');
        objTemporaryProjectLeadership2.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership2.GP_PL_Leadership_Role__c = 'Global Project Manager';
        objTemporaryProjectLeadership2.GP_PL_Employee_OHR__c = '877654321';
        //insert objTemporaryProjectLeadership2;
        
        GP_Temporary_Data__c objTemporaryProjectLeadership3 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
                                                                                                                   '4532', 'PL');
        objTemporaryProjectLeadership3.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership3.GP_PL_Leadership_Role__c = 'PL';
        objTemporaryProjectLeadership3.GP_PL_Employee_OHR__c = '877654321';
        //insert objTemporaryProjectLeadership3;
        
        listOfJobLineItems.add(objTemporaryProjectLeadership);
        listOfJobLineItems.add(objTemporaryProjectLeadership2);
        listOfJobLineItems.add(objTemporaryProjectLeadership3);
        
        insert listOfJobLineItems;
        
        setOfProjectOracleIds = new Set<string>();
        setOfProjectOracleIds.add(objTemporaryProjectLeadership.GP_Project__c);
        //setOfProjectOracleIds.add(objTemporaryProjectLeadership2.GP_Project__c);
        //setOfProjectOracleIds.add(objTemporaryProjectLeadership3.GP_Project__c);        
        
        /*GP_Temporary_Data__c objTemporaryProjectLeadership1 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
        '4531', 'Billing SPOC');
        objTemporaryProjectLeadership1.GP_PL_Start_Date__c = system.today().addDays(2);
        insert objTemporaryProjectLeadership1;*/
    }
    
    @isTest public static void testForbatch() {
        Test.StartTest();
        setupCommonData();
        
        GPBatchProcessProjectLeadershipUpdate batcher = new GPBatchProcessProjectLeadershipUpdate(objJobForUpdateProjectLeadership.Id);
        Id batchprocessid = Database.executeBatch(batcher, 10);       
        
        Test.StopTest();
    }
    
    @isTest public static void testForbatch2() {
        Test.StartTest();
        setupCommonData();
        
        GPBatchProcessProjectLeadershipUpdate batcher = new GPBatchProcessProjectLeadershipUpdate(setOfProjectOracleIds,objJobForUpdateProjectLeadership.Id);
        Id batchprocessid1 = Database.executeBatch(batcher, 10);
        
        Test.StopTest();
    }
}