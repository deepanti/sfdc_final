public class GPDomainUserRole extends fflib_SObjectDomain {

    public GPDomainUserRole(List < GP_User_Role__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainUserRole(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_User_Role__c.SObjectType
    };

    public override void onBeforeInsert() {}

    public override void onAfterInsert() {
        addGroupMember(null);
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {}

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        addGroupMember(oldSObjectMap);
        removeGroupMember(oldSObjectMap);

    }
    public override void onValidate() {
        // Validate GP_User_Role__c
        for (GP_User_Role__c ObjUserRole: (List < GP_User_Role__c > ) Records) {}
    }


    public override void onBeforeDelete() {
        prevantActiveUserRoleDeletion();
    }


    public override void onApplyDefaults() {

        // Apply defaults to GP_User_Role__c
        //for(GP_User_Role__c objUserRole : (List<GP_User_Role__c>) Records) { 
        //}               
    }

    public void prevantActiveUserRoleDeletion() {
        for (GP_User_Role__c objUserRole: (List < GP_User_Role__c > ) records) {
            if (objUserRole.GP_Active__c == true) {
                objUserRole.adderror('Active user role cannot be deleted! Please deactivate first and then delete.');
                return;
            }
        }
    }


    public void addGroupMember(Map < Id, SObject > oldSObjectMap) {
        set < ID > SetofUserRoleId = new Set < ID > ();
        GP_User_Role__c objOldGR;
        for (GP_User_Role__c objUserRole: (List < GP_User_Role__c > ) records) {
            if (oldSObjectMap != null)
                objOldGR = (GP_User_Role__c) oldSObjectMap.get(objUserRole.Id);
            else
                objOldGR = null;

            if (objUserRole.GP_Active__c == true && (objOldGR == null || (objOldGR != null && objOldGR.GP_Active__c == false))) {
                SetofUserRoleId.add(objUserRole.id);
            }
        }
        if (SetofUserRoleId != null && SetofUserRoleId.size() > 0) {
            GPCommon.addUserRole(SetofUserRoleId);
        }

    }
    public void removeGroupMember(Map < Id, SObject > oldSObjectMap) {
        set < ID > SetofUserRoleId = new Set < ID > ();
        GP_User_Role__c objOldGR;
        for (GP_User_Role__c objUserRole: (List < GP_User_Role__c > ) records) {
            if (oldSObjectMap != null)
                objOldGR = (GP_User_Role__c) oldSObjectMap.get(objUserRole.Id);
            else
                objOldGR = null;

            if (objUserRole.GP_Active__c == false && (objOldGR == null || (objOldGR != null && objOldGR.GP_Active__c == true))) {
                SetofUserRoleId.add(objUserRole.id);
            }
        }
        if (SetofUserRoleId != null && SetofUserRoleId.size() > 0) {
            GPCommon.removeUserRole(SetofUserRoleId);
        }
    }
}