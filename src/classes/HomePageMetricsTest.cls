@isTest
public class HomePageMetricsTest {
    @testSetup
    public static void createTestData()
    {
        User testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac@bc.com','ac@bc.com');
        testUser.ForecastEnabled = true;
        insert testUser;
        System.runAs(testUser){
            //creating account
            Account acc=TestDataFactoryUtility.createTestAccountRecord();
            acc.Client_Partner__c = testUser.Id;
            insert acc;
            //creating contact
            Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
            insert con;
            //creating opportunity
            List<Opportunity> opp=TestDataFactoryUtility.createTestOpportunitiesRecordsDiscover(acc.Id, 1, system.today(), con);
            insert opp;
            //creating product
            List<Product2> prod=TestDataFactoryUtility.createTestProducts3(1);
            insert prod;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry standardPrice = new PricebookEntry(
                Pricebook2Id = pricebookId, 
                Product2Id = prod[0].Id,
                UnitPrice = 10, 
                IsActive = true);
            insert standardPrice;
            //creating opportunity lineitem
            List<OpportunityLineItem> oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(prod,opp[0].Id,standardPrice.id);
            insert oppLineItem;
            test.startTest();
            //updating the stages 
            opp[0].StageName='2. Define';
            update opp;
            opp[0].StageName='3. On Bid';
            opp[0].Margin__c=1;
            opp[0].Proposed_pricing_model__c='FTE based';
            opp[0].Summary_of_opportunity__c='test';
            opp[0].Current_challenges_client_facing__c ='no challenges';
            opp[0].End_objective_the_client_trying_to_meet__c='value';
            opp[0].Win_Theme__c='win theme';
            update opp;
            opp[0].StageName='4. Down Select';
            opp[0].W_L_D__c='Signed';
            
            update opp;
            
            test.stopTest();
        }
    }
    @isTest
    //function to check negative test condition
    public static void testHomePageMetricsFunctionsNegative()
    {   
        test.startTest();
        User testUser=[select id from user where email='ac@bc.com'];
        System.runAs(testUser){
            try
            {
                HomePageMetrics.getQuotaInformation();
            }
            catch(Exception exp)
            {
                system.assertEquals('Script-thrown exception', exp.getMessage());
            }
            test.stopTest();
        }
    }
    @isTest
    //function to check positive  test condition
    public static void testHomePageMetricsFunctionsPositive()
    {       
        test.startTest();
        User testUser=[select id from user where email='ac@bc.com'];
        System.runAs(testUser){
            //test.startTest();
            ForecastingQuota quotaRecord=TestDataFactoryUtility.createTestQuota(UserInfo.getUserId());
            //creating Quota Record
            insert quotaRecord;
            HomePageMetrics.getQuotaInformation();
          //  test.stopTest();
        }
        test.stopTest();
    }
    
    @isTest
    public static void testMetricsForSalesLeaderNegative()
    {
        test.startTest();
        User testUser=[select id from user where email='ac@bc.com'];
        System.runAs(testUser){
            try
            {
                HomePageMetrics.getMetricValues();
            }
            catch(Exception exp)
            {
                system.assertEquals('Script-thrown exception', exp.getMessage());
            }
        }
        test.stopTest();
    }
    
    @isTest static void testMetricsForSalesLeader()
    {    
        test.startTest();
        User testUser=[select id from user where email='ac@bc.com'];
        System.runAs(testUser){
            Opportunity oppList=[select id,StageName,Contract_type__c,Pricer_Name__c,AccountId from opportunity];
            
            ForecastingQuota quotaRecord=TestDataFactoryUtility.createTestQuota(UserInfo.getUserId());
            //creating Quota Record
            insert quotaRecord;
            
            oppList.StageName='5. Confirmed';
            oppList.Contract_type__c='MSA or over-arching agreement';
            //creating pricer for contract
            Pricer__c pricerRecord=TestDataFactoryUtility.createTestPricer();
            insert pricerRecord;
            oppList.Pricer_Name__c=pricerRecord.id;
            
            update oppList;
           // OpportunitySplitType splitType = [select id from OpportunitySplitType where DeveloperName='Overlay' limit 1];
            String splitType = system.label.OpportunitySplitType;
            OpportunitySplit sp = new OpportunitySplit(OpportunityId=oppList.Id,SplitOwnerId=testUser.id,SplitPercentage=100,SplitTypeId = splitType);
           // insert sp;
            system.assertNotEquals(null, HomePageMetrics.getMetricValues());
        }
        test.stopTest();
    }
}