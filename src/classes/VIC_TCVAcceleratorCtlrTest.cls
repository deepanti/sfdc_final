/*
    @Author: Rajan Saini
    @Company: SaaSFocus
    @Description: Test class for the controller of VIC_TCVAcceleratorCtlr
*/

@isTest
 public class VIC_TCVAcceleratorCtlrTest{
   
      static testmethod void validatemethod(){
    
         APXTConga4__Conga_Template__c obj= new VIC_CommonTest().initAllcongaData('GRM - CM VIC Letter');
         insert obj;
        
         List<Plan__c> plan = new VIC_CommonTest().initAllPlanData(obj.ID);
         insert plan;
        
        List<Master_Plan_Component__c >lstMaster= new VIC_CommonTest().initAllMasterPlanComponent();
        insert lstMaster;
        List<Plan_Component__c> planComponent= new VIC_CommonTest().initAllPlanComponent(plan,lstMaster);
        
        VIC_Calculation_Matrix__c objMatrix = VIC_CommonTest.fetchMatrixComponentData('BDE');
        insert objMatrix;
        
        List<VIC_Calculation_Matrix_Item__c> calcMatrix = new VIC_CommonTest().fetchListMatrixItemTCVAcceleratorData(objMatrix.Name);
      
        Target__c target = new Target__c();
        target.Target_Bonus__c = 8000;
        target.Plan__c = plan[5].id;
        target.Plan__r = plan[5];
        insert target;
       
        
        Target_Component__c  targetComp = new Target_Component__c();
        targetComp.Weightage__c =10;
        targetComp.vic_Achievement_Percent__c =70;
        targetComp.Target_Status__c = 'Active';
        targetComp.Target_In_Currency__c =100;
        targetComp.Is_Weightage_Applicable__c = true;
        targetComp.Target__c = target.id;
        targetComp.Target__r = target;
        insert targetComp;
        
        
         
        Test.StartTest();
        
 
        VIC_TCVAcceleratorCtlr ctrl = new VIC_TCVAcceleratorCtlr();
       
       ctrl.isEnableIO = True;
       ctrl.percentOP =150;
       ctrl.percentPM= 200;
       ctrl.totalTCVAmount=10000000;
       Map<String, vic_Incentive_Constant__mdt> mapData= new VIC_CommonTest().fetchMetaDataValue();
       ctrl.mapValueToObjIncentiveConst= mapData;
       ctrl.calculate(targetComp,calcMatrix );
       
        target.Plan__c = plan[1].id;
        target.Plan__r = plan[1];
        update target;
        ctrl.calculate(targetComp,calcMatrix);
          
        target.Plan__c = plan[4].id;
        target.Plan__r = plan[4];
        update target;
        ctrl.calculate(targetComp,calcMatrix);

        target.Plan__c = plan[0].id;
        target.Plan__r = plan[0];
        update target;
        ctrl.calculate(targetComp,calcMatrix);
        CTRL.calculateTCVTS(targetComp,calcMatrix,10000000);
          System.AssertEquals(200,200);
       Test.StopTest();
       
  }

}