/**
* @Description: Class used as controller for Lightning Components VIC_CustomScreenForEditOLI and VIC_OpportunityLineItemViewComponent 
* @author: Bashim Khan
* @date: Jun 2018
*/
public class VIC_OpportunityLineItemController {
  @AuraEnabled
    public static Wrapper getOLI(Id oliId){
        
        Wrapper wp = new Wrapper();
        opportunitylineitem oli = [select id, Name, vic_TCV_Accelerator_Variable_Credit__c,opportunityid, opportunity.ownerid, vic_Product_BD_Rep_Split__c, opportunity.name, vic_VIC_User_3__r.name, vic_VIC_User_3__c, vic_VIC_User_3_Split__c,
                 vic_VIC_User_4__r.name, vic_VIC_User_4__c, vic_VIC_User_4_Split__c, vic_Primary_User__c, vic_Primary_Sales_Rep_Split__c,Contract_Term__c,CYR__c,
                 Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, vic_TCV_EBIT_Consideration_ForNPV__c, vic_CPQ_Deal_Status__c,
                 Y1_TCV_USD_OLI_CPQ__c, Y2_TCV_USD_OLI_CPQ__c, Y3_TCV_USD_OLI_CPQ__c, Y4_TCV_USD_OLI_CPQ__c, Y5_TCV_USD_OLI_CPQ__c,
                 Y6_TCV_USD_OLI_CPQ__c, Y7_TCV_USD_OLI_CPQ__c, Y8_TCV_USD_OLI_CPQ__c, Y9_TCV_USD_OLI_CPQ__c, Y10_TCV_USD_OLI_CPQ__c,
                 Y1_AOI_USD_OLI_CPQ__c, Y2_AOI_USD_OLI_CPQ__c, Y3_AOI_USD_OLI_CPQ__c, Y4_AOI_USD_OLI_CPQ__c, Y5_AOI_USD_OLI_CPQ__c,
                 Y6_AOI_USD_OLI_CPQ__c, Y7_AOI_USD_OLI_CPQ__c, Y8_AOI_USD_OLI_CPQ__c, Y9_AOI_USD_OLI_CPQ__c, Y10_AOI_USD_OLI_CPQ__c,
                 Product_BD_Rep__c, Product_BD_Rep__r.name, VIC_NPV__c, VIC_IsIncentiveCalculatedForPrimaryRep__c,VIC_IsIncentiveCalculatedForBDRep__c, vic_Sales_Rep_Approval_Status__c, vic_Product_BD_Rep_Approval_Status__c,vic_Is_Split_Calculated__c from OpportunityLineItem where id =: oliId limit 1];
      
   
        wp.user3Id = oli.vic_VIC_User_3__c;
        wp.user4Id = oli.vic_VIC_User_4__c;
        wp.ContTerm= oli.Contract_Term__c;
        wp.CYR=oli.CYR__c;
        
        Schema.DescribeFieldResult cpqDealStausPickVal = OpportunityLineItem.vic_CPQ_Deal_Status__c.getDescribe();
        List<Schema.PicklistEntry> pleCPQDealStatus = cpqDealStausPickVal.getPicklistValues();
        Schema.DescribeFieldResult salesRepStausPickVal = OpportunityLineItem.vic_Sales_Rep_Approval_Status__c.getDescribe();
        List<Schema.PicklistEntry> salesRepStatus = salesRepStausPickVal.getPicklistValues();
       
        
        List<String> cpqDealStatusValues = new List<String>();
        List<String> salesRepStatusValues = new List<String>();
        
        
        for( Schema.PicklistEntry ds : pleCPQDealStatus)
       {
            cpqDealStatusValues.add(ds.getLabel());
       }
        for( Schema.PicklistEntry rs : salesRepStatus)
       {
            salesRepStatusValues.add(rs.getLabel());
       }
     
        
        wp.cpqDealStatusValues = cpqDealStatusValues;
        wp.SalesrepApproValValues = salesRepStatusValues;
       
        Schema.DescribeFieldResult tcvEbitConPickVal = OpportunityLineItem.vic_TCV_EBIT_Consideration_ForNPV__c.getDescribe();
        List<Schema.PicklistEntry> pleTcvEbitCon = tcvEbitConPickVal.getPicklistValues();
        List<String> tcvEbitConValues = new List<String>();
        for( Schema.PicklistEntry tec : pleTcvEbitCon)
       {
           tcvEbitConValues.add(tec.getLabel());
       }
        wp.tcvEbitConValues = tcvEbitConValues;
        wp.oli = oli;
        return wp;
    }
    @AuraEnabled
    public static void setOLI(String cpqDealStatus, String tcvEbitCon, Id user3Id, Id user4Id, Opportunitylineitem oliToUpdate,decimal TvAcV){
        oliToUpdate.vic_TCV_EBIT_Consideration_ForNPV__c = tcvEbitCon;
        oliToUpdate.vic_CPQ_Deal_Status__c = cpqDealStatus;
        oliToUpdate.vic_VIC_User_3__c = user3Id;
        oliToUpdate.vic_VIC_User_4__c = user4Id;
        oliToUpdate.vic_TCV_Accelerator_Variable_Credit__c=TvAcV;
        update oliToUpdate;
    }
    @AuraEnabled
    public static List<Opportunity> fetchOpportunities(String searchKeyWord, String searchKeyWordOLI){
        String searchKey;
        if(searchKeyWord!=null && searchKeyWord!=''){
           searchKey = searchKeyWord + '%';
          }
         String searchKeyOLI; 
        if(searchKeyWordOLI!=null && searchKeyWordOLI!=''){     
             searchKeyOLI= searchKeyWordOLI + '%';
         }
         system.debug(searchKey + ' = ' + searchKeyOLI);
       List <Opportunity> returnList = new List <Opportunity> ();
            List <Opportunity> lstOfOpportunities = new List <Opportunity>();
         List <Opportunity> lstOfOpportunitiesN = new List <Opportunity>();
     if(searchKeyWord != null && searchKeyWord != '' ){
             lstOfOpportunities = [select id, Name, Closedate, CloseDatewithInsight__c, AccountId, ownerid, owner.name, account.name,Opportunity_ID__c,
                                                  (select name,id from OpportunityLineItems) from Opportunity
                                                where ( id = : searchKeyWord or Name LIKE: searchKey or Opportunity_ID__c LIKE: searchKey) LIMIT 500];
         }
         if((searchKeyWordOLI != null && searchKeyWordOLI != '') || (searchKeyOLI!=null && searchKeyOLI!='') ){
            lstOfOpportunitiesN = [select id, Name, Closedate, CloseDatewithInsight__c, owner.name, account.name,
                                                  (select name,id from OpportunityLineItems where id =: searchKeyWordOLI or Name LIKE: searchKeyOLI or VIC_ProductShortName__c LIKE: searchKeyOLI) from Opportunity
                                                where id IN (select opportunityid from OpportunityLineItem where id =: searchKeyWordOLI or name LIKE: searchKeyOLI or VIC_ProductShortName__c LIKE: searchKeyOLI) LIMIT 500];
         }
         system.debug('lstOfOpportunities'+lstOfOpportunities);
         for (Opportunity acc: lstOfOpportunities) {
              returnList.add(acc);
         }
        for (Opportunity acc: lstOfOpportunitiesN) {
              returnList.add(acc);
         }
         return returnList;                
    }
    public class Wrapper{
        @AuraEnabled Id user3Id  {get;set;}
        @AuraEnabled Id user4Id  {get;set;}
        @AuraEnabled decimal ContTerm{get;set;}
        @AuraEnabled decimal CYR{get;set;}
       
        @AuraEnabled List<String> cpqDealStatusValues  {get;set;}
        @AuraEnabled List<String> tcvEbitConValues  {get;set;}
        @AuraEnabled List<String> SalesrepApproValValues  {get;set;}
       
        @AuraEnabled Opportunitylineitem oli  {get;set;}
    }
}