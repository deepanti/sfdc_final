@isTest
private class TestUpdateClient 
{

   static testMethod void testTrigger()
   {
   RMG_Employee_Master__c rmg = new RMG_Employee_Master__c(Name='Test'); 
      insert rmg;
      
      RMG_Employee_Allocation__c rmgallo = new RMG_Employee_Allocation__c(
                                     Name='Test', Client__c='Unit1',Status__c = 'Current',Client_Interview__c='Y',Start_Date__c = date.parse('01/03/2014'), End_Date__c = date.parse('11/10/2015'),
                                     RMG_Employee_Code__c=rmg.id);
                                     
    RMG_Employee_Allocation__c rmgallo1 = new RMG_Employee_Allocation__c(
                                     Name='Test', Client__c='Unit2',Status__c = 'Current',Client_Interview__c='Y',Start_Date__c = date.parse('01/03/2014'), End_Date__c = date.parse('11/10/2015'),
                                     RMG_Employee_Code__c=rmg.id);                                  
      insert rmgallo;
      update rmgallo;
      insert rmgallo1;
      update rmgallo1;
      
  
      
      RMG_Employee_Allocation__c newrmg=[select Name,Client_Interview__c, Client__c from RMG_Employee_Allocation__c where id = :rmgallo.id];

      System.assertEquals('Unit1', newrmg.Client__c);
    
      }
      }