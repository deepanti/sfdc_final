public with sharing class GPLWCControllerCustomComponent {
    private String sobjectName;
    private String searchKey;
    private String searchfield;
    private String filterString;
    private String fieldapi;
    private String selectedValue;
    private String displayTextSeparator;
    private String displayTextFields;
    private String type;
    private final String LOOKUP_RECORD_DATA = 'lookupRecordData';
    private List<GPWrapRecord> objectRecordList = new List<GPWrapRecord>();
    private JSONGenerator gen;
    private Map<Schema.DisplayType, String> mapDisplayTypeToStringVal = new Map<Schema.DisplayType, String>
    {
        Schema.DisplayType.TextArea => 'TextArea',
        Schema.DisplayType.Boolean => 'Boolean',
        Schema.DisplayType.Currency => 'Currency',
        Schema.DisplayType.Date => 'Date',
        Schema.DisplayType.DateTime => 'DateTime',
        Schema.DisplayType.Double => 'Double',
        Schema.DisplayType.Email => 'Email',
        Schema.DisplayType.Percent => 'Percent',
        Schema.DisplayType.Phone => 'Phone',
        Schema.DisplayType.Picklist => 'Picklist',
        Schema.DisplayType.Reference => 'Reference',
        Schema.DisplayType.anytype => 'anytype',
        Schema.DisplayType.String => 'String'
    };
    public GPLWCControllerCustomComponent() {}

    public GPLWCControllerCustomComponent(String sobjectName, String searchKey, String searchfield, String filterString, 
    String displayTextSeparator, String displayTextFields, String type) {
        this.sobjectName = sobjectName;
        this.searchKey = searchKey;
        this.searchfield = searchfield;
        this.filterString = filterString;
        this.displayTextSeparator = displayTextSeparator;
        this.displayTextFields = displayTextFields;
        this.type = type;
    }

    public GPLWCControllerCustomComponent(String sObjectName, String fieldapi, String selectedValue) {
        this.sobjectName = sobjectName;
        this.fieldapi = fieldapi;
        this.selectedValue = selectedValue;
    }

    public GPAuraResponse fetchLookupRecords() {
        if(String.isNotBlank(this.searchKey) 
           && String.isNotBlank(this.sobjectName) 
           && String.isNotBlank(this.searchfield) 
           && String.isNotBlank(this.displayTextFields)
           && String.isNotBlank(this.type)) {               
            
            String query = 'SELECT Id, ' + this.displayTextFields + ' FROM ' + this.sobjectName + ' WHERE ';

            if(this.type == 'lookup') {
                this.searchKey = '%' + this.searchKey + '%';
                query += this.searchfield + ' LIKE :searchKey';
            } else if (this.type == 'reference') {
                query += 'Id =:searchKey';
            } else {
                query += this.searchfield + ' =:searchKey';
            }
            if(String.isNotBlank(this.filterString)) {
                query +=' AND '+ this.filterString;
            }
            system.debug('==query=='+query);
            // GPWrapRecord
            for(SObject so : Database.query(query)) {
                String name = '';

                for(String str : this.displayTextFields.split(',')) {
                    name = name + String.valueOf(so.get(str)) + this.displayTextSeparator;
                }
                objectRecordList.add(new GPWrapRecord((String)so.get('Id'), name.removeEnd(this.displayTextSeparator)));
            }
            setJson();
        }
        return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    public GPAuraResponse fetchPicklistValues() {

        List<GPWrapPicklist> listOfPickListValues = new List<GPWrapPicklist>();        
        Boolean isDefault;        
        if(String.isNotBlank(this.sobjectName) && String.isNotBlank(this.fieldapi)){
            // Get a map of fields for the SObject
            Map < String, Schema.SObjectField > fieldMap = Schema.getGlobalDescribe().get(this.sobjectName).getDescribe().fields.getMap();
            Schema.DescribeFieldResult fieldResult  = fieldMap.get(this.fieldapi).getDescribe();
            
            if(fieldResult.getType() == Schema.DisplayType.PICKLIST 
               || fieldResult.getType() == Schema.DisplayType.COMBOBOX 
               || String.valueof(fieldResult.getType()) == 'MULTIPICKLIST') {
                   
                   for (Schema.PicklistEntry pickListEntry:  fieldResult.getPickListValues()) {
                       isdefault = String.isNotBlank(this.selectedValue) ? (this.selectedValue == (String) pickListEntry.getValue()) : false;
                       listOfPickListValues.add(new GPWrapPicklist((String) pickListEntry.getValue(), (String)pickListEntry.getLabel(), isDefault));
                   }
               }
        }
        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(listOfPickListValues));
    }
    
    public GPAuraResponse fetchFieldSetPerObject(String fieldSetName, String objectName) {
        return new GPAuraResponse(true, 'SUCCESS', getFieldsetDetails(fieldSetName, objectName));
    }
    
    public String getFieldsetDetails(String fieldSetName, String objectName) {
        List < GPWrapField > listOfWrapFields = new List < GPWrapField > ();
        List < Schema.FieldSetMember > fieldsSetMembers = getFieldSet(fieldSetName, objectName);
        if (fieldsSetMembers != null && fieldsSetMembers.size() > 0) {
            for (Schema.FieldSetMember fields: fieldsSetMembers) {
                // For PID Approver, we need to get Object name in case type is reference.
                if(mapDisplayTypeToStringVal.get(fields.getType()) != 'Reference') {
                    listOfWrapFields.add(new GPWrapField(fields.getFieldPath(), fields.getLabel(), mapDisplayTypeToStringVal.get(fields.getType()), null));
                } else {
                    Schema.SObjectField sf = fields.getSObjectField();
                    Schema.DescribeFieldResult sfDesc = sf.getDescribe();
                    if(!sfDesc.isNamePointing()) {
                        List<Schema.SObjectType> referenceList = sfDesc.getReferenceTo();
                        
                        listOfWrapFields.add(new GPWrapField(fields.getFieldPath(), fields.getLabel(), 
                                             mapDisplayTypeToStringVal.get(fields.getType()), 
                                             referenceList[0].getDescribe().getName()));
                    }
                }
            }
        }
        return JSON.serialize(listOfWrapFields);
    }
    
    public static List < Schema.FieldSetMember > getFieldSet(String fieldSetName, String ObjectName) {
        
        Map < String, Schema.SObjectType > GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
        if (fieldSetObj != null) {
            return fieldSetObj.getFields();
        } else {
            return null;
        }
    }

    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
            if(objectRecordList != null){
                gen.writeObjectField(LOOKUP_RECORD_DATA, objectRecordList);
            }          
        gen.writeEndObject();
    }

    public class GPWrapPicklist {
        string apival;
        string labelval;
        boolean isdefault;
        
        public GPWrapPicklist(String apivalue, String labelvalue, Boolean isdefault) {
            this.apival = apivalue;
            this.labelval = labelvalue;
            this.isdefault = isdefault;
        }
    }

    public class GPWrapRecord {
        string Id;
        string Name;
        
        public GPWrapRecord(String id, String name) {
            this.Id = id;
            this.Name = name;
        }
    }
    
    public class GPWrapField {
        string fieldName;
        string fieldLabel;
        string dataType;
        string referenceName;
        
        public GPWrapField(String fieldName, String fieldLabel, String dataType, String referenceName) {
            this.fieldName = fieldName;
            this.fieldLabel = fieldLabel;
            this.dataType = dataType;
            this.referenceName = referenceName;
        }
    }
}