@isTest 
private class GPDomainProjectTracker {
	
    public static GP_Project__c prjObj,objChildProj;
    
    @isTest
    public static void testProject() {
        
        GPDomainProject.skipVersionHistoryCreation = true;        
        createData();
        
        GPServiceOMSPricing objServiceOMS = new GPServiceOMSPricing( new set<id>{prjObj.id});
        objServiceOMS.createProjectRelatedDataForOMSDeal();
        
        //GPDomainProject obj = new GPDomainProject(new List<GP_Project__c>{new GP_Project__c()});
        GPDomainProject.skipVersionHistoryCreation = false;
        
        prjObj.GP_Approval_Status__c = 'Approved';
        update prjObj;
        /*objChildProj.GP_Approval_Status__c = 'Approved';
        update objChildProj;
        prjObj.GP_Temp_Approval_Status__c = '{"0050l000000p9HMAAY":{"userId":"0050l000000p9HMAAY","status":"Pending For Approval","remarks":"Submitting request for PID approval"},"0050l000000p8UuAAI":{"userId":"0050l000000p8UuAAI","status":"Pending For Approval","remarks":"Submitting request for Controller approval"}}';
        update prjObj;*/
    }
    
    @isTest
    public static void testProject2() {
        GPDomainProject.skipVersionHistoryCreation = true;
        createData();
        
        //GPDomainProject obj = new GPDomainProject(new List<GP_Project__c>{new GP_Project__c()});
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Temp_Approval_Status__c = '{"0050l000000p9HMAAY":{"userId":"0050l000000p9HMAAY","status":"Approved","remarks":"Submitting request for PID approval"},"0050l000000p8UuAAI":{"userId":"0050l000000p8UuAAI","status":"Approved","remarks":"Submitting request for Controller approval"}}';
        update prjObj;        
        
        objChildProj.GP_Approval_Status__c = 'Approved';
        update objChildProj;
        
        //prjObj.GP_Temp_Approval_Status__c = '{"0050l000000p9HMAAY":{"userId":"0050l000000p9HMAAY","status":"Approved","remarks":"Submitting request for PID approval"},"0050l000000p8UuAAI":{"userId":"0050l000000p8UuAAI","status":"Approved","remarks":"Submitting request for Controller approval"}}';
        //update prjObj;
    }
    
    @isTest
    public static void testProject3() {       
        GPDomainProject.skipVersionHistoryCreation = true;
        createData();
        //GPDomainProject obj = new GPDomainProject(new List<GP_Project__c>{new GP_Project__c()});
        try{
            prjObj.GP_Approval_Status__c = 'Approved';
            prjObj.GP_Temp_Approval_Status__c = '{"0050l000000p9HMAAY":{"userId":"0050l000000p9HMAAY","status":"Rejected","remarks":"Submitting request for PID approval"},"0050l000000p8UuAAI":{"userId":"0050l000000p8UuAAI","status":"Rejected","remarks":"Submitting request for Controller approval"}}';
            update prjObj;
        } catch(Exception ex) {
            
        }
        
        /*objChildProj.GP_Approval_Status__c = 'Approved';
		update objChildProj;*/
        //prjObj.GP_Temp_Approval_Status__c = '{"0050l000000p9HMAAY":{"userId":"0050l000000p9HMAAY","status":"Rejected","remarks":"Submitting request for PID approval"},"0050l000000p8UuAAI":{"userId":"0050l000000p8UuAAI","status":"Rejected","remarks":"Submitting request for Controller approval"}}';
        //update prjObj;
    }
    
    //@testSetup
    private static void createData() {    
        //GPTriggerSwitch__c gettriggerSwitch = new GPTriggerSwitch__c();
        //gettriggerSwitch.name = 'gp_project__c';
        //gettriggerSwitch.GP_Enable_Trigger__c = true;
        //insert gettriggerSwitch;
        
        GP_Sobject_Controller__c objCustomSetting = new GP_Sobject_Controller__c();
        objCustomSetting.Name = 'gp_project__c';
        objCustomSetting.GP_Enable_Sobject__c = true;
        insert objCustomSetting;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Business_Type__c =  'BPM';
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Oracle_Id__c = 'yo';
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMasterForShare.GP_Oracle_Id__c = '5';
        insert objpinnacleMasterForShare ;
        
        insertPinnacleMaster();
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c  = null;
        insert objrole;
        
        objrole.GP_Type_of_Role__c = 'Delivery Org';
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Work_Location_SDO_Master__c = null;
        update objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        User objuser1 = GPCommonTracker.getUser();
        objuser1.username = 'newUserPranav123@saasforce.com';
		objuser1.FederationIdentifier = '1234560';
        objuser1.alias = 'nuser12';
        insert objuser1;
        
        User objuserForProjet = GPCommonTracker.getUser();
        objuserForProjet.LastName = 'Test User For Project';
		objuserForProjet.FederationIdentifier = '987654';
        objuserForProjet.Email = 'xyz@gmail.com';
        objuserForProjet.Username = 'sal@sales.com';
        insert objuserForProjet; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        objuserrole.GP_Role__c = objrole.id;
        update objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_OMS_Detail__c objOSMDetail = new GP_OMS_Detail__c();
        objOSMDetail.GP_Deal__c = dealObj.id;
        objOSMDetail.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Expense').getRecordTypeId();
        objOSMDetail.GP_Expense_Type__c = 'Test';
        objOSMDetail.GP_Expense_Amount__c = 2;
        objOSMDetail.GP_Location__c = 'Test';
        objOSMDetail.GP_Category__c = 'Test';
        insert objOSMDetail;
        
        GP_OMS_Detail__c objOSMDetail2 = new GP_OMS_Detail__c();
        objOSMDetail2.GP_Deal__c = dealObj.id;
        objOSMDetail2.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Pricing View').getRecordTypeId();
        objOSMDetail2.GP_Expense_Type__c = 'Test';
        objOSMDetail2.GP_Expense_Amount__c = 2;
        objOSMDetail2.GP_Location__c = 'Test';
        objOSMDetail2.GP_Category__c = 'Test';
        insert objOSMDetail2;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
		Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);        
        insert accobj ;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        insert oppproobj;
        
        Test.startTest();
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Opportunity_Project__c = oppproobj.Id;
        prjObj.GP_Primary_SDO__c = objSDO.id;
        prjObj.GP_CRN_Status__c = 'Test';
        prjObj.GP_MOD__c = 'Hard Copy';
        prjObj.GP_Project_type__c = 'Account';
        prjObj.GP_Business_Segment_L2__c = 'test';
        prjObj.GP_Current_Working_User__c = objuser1.Id;
        //prjObj.GP_Oracle_Status__c = 'S';
        insert prjObj ;
        
        sendApproval(prjObj.Id, objuser1.Id, 'GP_Project_Approval');
        
        prjObj.GP_Project_Access__c = 'Private';
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Project_type__c = 'Account';
        update prjObj;
        
        prjObj.GP_Approval_Status__c = 'Pending for Approval';
        update prjObj;
        
        prjObj.GP_Approval_Status__c = 'Pending for Closure';
        update prjObj;
        
        //prjObj.GP_Approval_Status__c = 'Rejected';
        //update prjObj;
        
        /*Account accountObj1 = GPCommonTracker.getAccount(null, null);
		insert accountObj1;*/
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        objChildProj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        objChildProj.OwnerId=objuser.Id;
        objChildProj.GP_Parent_Project__c = prjObj.Id;
        objChildProj.GP_Customer_Hierarchy_L4__c = accountObj.id;
        objChildProj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        objChildProj.GP_Project_type__c = 'Account';
        objChildProj.GP_MOD__c = 'Hard Copy';
        objChildProj.GP_Business_Segment_L2__c = 'test';
        //objChildProj.GP_Oracle_Status__c = 'S';
        objChildProj.GP_Primary_SDO__c = objSDO.id;
        insert objChildProj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Type_of_Leadership__c = 'Billing Approver'; 
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.recordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        insert projectLeadership;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.Id, prjObj.Id);
        insert resourceAllocationObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        prjObj.GP_CRN_Status__c = 'Signed Contract Received';
        update prjObj;
        
        Test.stopTest();
        
        /*GP_Project__c projectObj = new GP_Project__c();
        projectObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObj.GP_Primary_SDO__c = objSDO.id;
        projectObj.OwnerId = objuser.Id;
        projectObj.GP_HSL__c = objpinnacleMaster.id;
        projectObj.GP_PID_Approver_User__c = objuserForProjet.Id;
        projectObj.GP_Parent_Project__c = null;
        projectObj.GP_OMS_Deal_ID__c = dealObj.id;
        insert projectObj;
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        GP_Project__c projectObjforApexShare = new GP_Project__c();
        projectObjforApexShare = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        //projectObjforApexShare.id = projectObj.id;
        projectObjforApexShare.GP_Customer_Hierarchy_L4__c = accountObj1.id; 
        projectObjforApexShare.Id = projectObjForIcon.id;
        projectObjforApexShare.GP_HSL__C = objpinnacleMasterForShare.id;
        projectObjforApexShare.GP_PID_Creator_Role__c = objrole.id;
        //projectObjforApexShare.GP_Deal__c = dealObj.id;
        update projectObjforApexShare;*/
    }
    
    private static void sendApproval(string SobjectId, string strUserId, string strApprovalProcessName) {
        // Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(SobjectId);
        req1.setNextApproverIds(new Id[] {userinfo.getUserId()});
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(strUserId); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId(strApprovalProcessName);
        req1.setSkipEntryCriteria(true);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);        
    }
    
    private static void insertPinnacleMaster() {
        GP_Pinnacle_Master__c objPM = new GP_Pinnacle_Master__c();
        objPM.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Mode of Dispatch').getRecordTypeId();
        objPM.GP_Status__c = 'Active';
        objPM.GP_Oracle_Id__c = '1';
        objPM.GP_Description__c = 'PBB';
        insert objPM;
        
        GP_Pinnacle_Master__c objPM1 = new GP_Pinnacle_Master__c();
        objPM1.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Oracle Template').getRecordTypeId();
        objPM1.GP_Project_Type__c = 'Account';
        objPM1.GP_Entity_Id__c = 'yo';
        objPM1.GP_Oracle_Id__c = '2';
        objPM1.GP_Description__c = 'PBB';
        insert objPM1;
    }
}