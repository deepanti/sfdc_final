public without Sharing class GPSelectorOpportunity extends fflib_SObjectSelector {
  public List<Schema.SObjectField> getSObjectFieldList() {
       return new List<Schema.SObjectField> {
            Opportunity.Name
               
        };
    }

    public Schema.SObjectType getSObjectType() {
        return Opportunity.sObjectType;
    }

    public List<Opportunity> selectById(Set<ID> idSet) {
        return (List<Opportunity>) selectSObjectsById(idSet);
    }
    
       
    public List<Opportunity> selectOpp(Set<Id> setOfOppId) {
        return (List<Opportunity>) [Select Id,Opportunity_ID__c from Opportunity where ID IN:setofOppID and Probability >=33];
    }
  
}