public with sharing class PromoterSearchController {

    public PromoterSearchController(ApexPages.StandardController controller) {
    soql = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c,Ability_to_Influence__c,Influence_Quotient_Score__c,Can_Make_a_Reference__c,Can_intro_to_New_internal_buying_center__c,Can_make_Public_Appearances__c,Can_Make_External_Introduction__c,Suitable_for_Analyst_Reference__c,Origination__c,Commercial_Structure__c,Offerings__c,Engagement_Model__c,Transition_Model__c,Unique_Client_Geos_Services__c,G_Service_Delivery_Locations__c,  Business_Drivers__c,Lean_Digital__c,Transformation__c,X2015_NPS_Disposition__c,Current_Disposition__c,Unique_about_this_engagement__c from Promoter_info__c where id != null';
    //runQuery();

    }




  // the soql without the order and limit
  private String soql {get;set;}
  
    public String name {get;set;}
    
   // public String title {get;set;}
    
    public String vertical {get;set;}
    
    public String servicelinevar {get;set;}
    
    public String subvertical {get;set;}
    
   // public String subserviceLine {get;set;}
    
    public String accountName {get;set;}
    
    public String canMakeAReferencevar {get;set;}
    
    public String canMakePublicAppearancesvar {get;set;}
    
    public String canIntroToNewInternalBuyingCentervar {get;set;}
    
    public String canMakeExternalIntroductionvar {get;set;}
    
    public String suitableForAnalystReferencevar {get;set;}
    
   // public String originationvar {get;set;}
    
  //  public String commercialStructurevar {get;set;}
    
   // public String offeringsvar {get;set;}
    
   // public String engagementModelvar {get;set;}
    
   // public String transitionModelvar {get;set;}
    
    public String uniqueClientGeosServicesvar {get;set;}
    
    public String gServiceDeliveryLocationsvar {get;set;}
    
    public String businessDriversvar {get;set;}
    
    public String leanDigitalvar {get;set;}
    
   // public String transformationvar {get;set;}
    
//    public String x2015npsDispositionvar {get;set;}
    
    public String currentDispositionvar {get;set;}
    
   // public String uniqueAboutThisEngagement {get;set;}
        
        
  
  
  // the collection of contacts to display
  public List<Promoter_info__c> promoter {get;set;}

  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir ; }
    set;
  }

  // init the controller and display some sample data when the page loads
  public PromoterSearchController() {
    soql = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c,Ability_to_Influence__c,Influence_Quotient_Score__c,Can_Make_a_Reference__c,Can_intro_to_New_internal_buying_center__c,Can_make_Public_Appearances__c,Can_Make_External_Introduction__c,Suitable_for_Analyst_Reference__c,Origination__c,Commercial_Structure__c,Offerings__c,Engagement_Model__c,Transition_Model__c,Unique_Client_Geos_Services__c,G_Service_Delivery_Locations__c,  Business_Drivers__c,Lean_Digital__c,Transformation__c,X2015_NPS_Disposition__c,Current_Disposition__c,Unique_about_this_engagement__c from Promoter_info__c where id != null';
    runQuery();
  }

  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }

  // runs the actual query
  public void runQuery() {

    System.Debug('SOQL: ' + soql);
      promoter= Database.query(soql + ' order by ' + sortField + ' ' + sortDir );
      
    try{ 
      
    Promoter_Search_Click__c psc = new Promoter_Search_Click__c();
    
    psc.Account_Name__c = accountName;
  //  if (!uniqueAboutThisEngagement.equals('none'))
  //      psc.Anything_unique_about_this_engagement__c = uniqueAboutThisEngagement;
        
    if (!businessDriversvar.equals('none'))
        psc.Business_Drivers__c= businessDriversvar;
        
   // if (!commercialStructurevar.equals('none'))
   //     psc.Commercial_Structure__c= commercialStructurevar;
    
    if (!currentDispositionvar.equals('none'))
        psc.Current_Disposition__c= currentDispositionvar;
    
    psc.CXO_Name__c= name;
  //  psc.CXO_Title__c= title;
    
  //  if (!engagementModelvar.equals('none'))
     //   psc.Engagement_Model__c= engagementModelvar;
    
    if (!gServiceDeliveryLocationsvar.equals('none'))
        psc.G_Service_Delivery_Locations__c= gServiceDeliveryLocationsvar;
    
    if (!leanDigitalvar.equals('none'))
        psc.Lean_Digital__c= leanDigitalvar;
    
   // if (!offeringsvar.equals('none'))
   //     psc.Nature_of_G_Offerings__c= offeringsvar;
    
  //  if (!originationvar.equals('none'))
  //      psc.Origination__c= originationvar;
    
    if (!servicelinevar.equals('none'))
        psc.Service_Line__c= servicelinevar;
    
  //  if (!subserviceLine.equals('none'))
  //      psc.Sub_Service_Line__c= subserviceLine;
    
    if (!subvertical.equals('none'))
        psc.Sub_Vertical__c= subvertical;
    
    if (!suitableForAnalystReferencevar.equals('none'))
        psc.Suitable_for_Analyst_Reference__c= suitableForAnalystReferencevar;
    
   // if (!transformationvar.equals('none'))
    //    psc.Transformation__c= transformationvar;
    
   // if (!transitionModelvar.equals('none'))
   //     psc.Transition_Model__c= transitionModelvar;
    
    if (!uniqueClientGeosServicesvar.equals('none'))
        psc.Unique_Client_Geos_Serviced__c= uniqueClientGeosServicesvar;
    
    if (!vertical.equals('none'))
        psc.Vertical__c= vertical;
    
    if (!canIntroToNewInternalBuyingCentervar.equals('none'))
        psc.Will_intro_to_New_Internal_Buying_Center__c= canIntroToNewInternalBuyingCentervar;
    
    if (!canMakeAReferencevar.equals('none'))
        psc.Will_Make_A_Reference__c= canMakeAReferencevar;
    
    if (!canMakeExternalIntroductionvar.equals('none'))
        psc.Will_Make_External_Introduction__c= canMakeExternalIntroductionvar;
    
    if (!canMakePublicAppearancesvar.equals('none'))
        psc.Will_make_Public_Appearances__c= canMakePublicAppearancesvar;
    
    if (Userinfo.getUiThemeDisplayed().equals('Theme3'))
        psc.User_Login_Type__c ='System';
    else if (Userinfo.getUiThemeDisplayed().equals('Theme4t'))
        psc.User_Login_Type__c ='Mobile';
        
        insert psc;
    }
    catch(DmlException e)
    {
        System.debug('The following exception has occurred: ' + e.getMessage());
    }
     

  }

public PageReference reset() {
        PageReference pg = new PageReference(System.currentPageReference().getURL());
        pg.setRedirect(true);
        return pg;
    }

  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {

    
System.debug('account name ' + accountName);
    soql = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Text_Promoter_Name__c,Job_Title__c,Account_Owner__c,Account_Name__c,Ability_to_Influence__c,Influence_Quotient_Score__c,Can_Make_a_Reference__c,Can_intro_to_New_internal_buying_center__c,Can_make_Public_Appearances__c,Can_Make_External_Introduction__c,Suitable_for_Analyst_Reference__c,Origination__c,Commercial_Structure__c,Offerings__c,Engagement_Model__c,Transition_Model__c,Unique_Client_Geos_Services__c,G_Service_Delivery_Locations__c,  Business_Drivers__c,Lean_Digital__c,Transformation__c,X2015_NPS_Disposition__c,Current_Disposition__c,Unique_about_this_engagement__c from Promoter_info__c where id != null';
    
     if (!name.equals(''))
      soql += ' and Text_Promoter_Name__c LIKE '+ '\'%'+String.escapeSingleQuotes(name)+'%' + '\'';
    
  //   if (!title.equals(''))
  //    soql += ' and Job_Title__c LIKE '+ '\'%'+String.escapeSingleQuotes(title )+'%' + '\'';
     if (!vertical.equals('none'))
      soql += ' and vertical__c LIKE '+ '\'%'+String.escapeSingleQuotes(vertical )+'%' + '\'';

         if (!servicelinevar.equals('none'))
      soql += ' and Service_Line__c  includes ('+ '\''+ servicelinevar+ '\''+')';
     if (!subvertical.equals('none'))
      soql += ' and Sub_Vertical__c LIKE '+ '\'%'+String.escapeSingleQuotes(subvertical )+'%' + '\'';
  //   if (!subserviceLine.equals(''))
  //    soql += ' and Sub_Service_Line_short__c LIKE '+ '\'%'+String.escapeSingleQuotes(subserviceLine )+'%' + '\'';
    if (!accountName.equals(''))
      soql += ' and Account_Name__c LIKE '+ '\'%'+String.escapeSingleQuotes(accountName )+'%' + '\'';
    //if (!abilityToInfluence.equals(''))
     // soql += ' and Ability_to_Influence__c LIKE '+ '\'%'+String.escapeSingleQuotes(abilityToInfluence )+'%' + '\'';
    //if (!influenceQuotientScore.equals(''))
      //soql += ' and Influence_Quotient_Score__c = '+ influenceQuotientScore ;
    if (!canMakeAReferencevar.equals('none'))
      soql += ' and Can_Make_a_Reference__c LIKE '+ '\'%'+String.escapeSingleQuotes(canMakeAReferencevar )+'%' + '\'';
    if (!canMakePublicAppearancesvar.equals('none'))
      soql += ' and Can_make_Public_Appearances__c LIKE '+ '\'%'+String.escapeSingleQuotes(canMakePublicAppearancesvar )+'%' + '\'';
    if (!canIntroToNewInternalBuyingCentervar.equals('none'))
      soql += ' and Can_intro_to_New_internal_buying_center__c LIKE '+ '\'%'+String.escapeSingleQuotes(canIntroToNewInternalBuyingCentervar )+'%' + '\'';
    if (!canMakeExternalIntroductionvar.equals('none'))
      soql += ' and Can_Make_External_Introduction__c LIKE '+ '\'%'+String.escapeSingleQuotes(canMakeExternalIntroductionvar )+'%' + '\'';
    if (!suitableForAnalystReferencevar.equals('none'))
      soql += ' and Suitable_for_Analyst_Reference__c LIKE '+ '\'%'+String.escapeSingleQuotes(suitableForAnalystReferencevar )+'%' + '\'';
  //  if (!originationvar.equals('none'))
  //    soql += ' and Origination__c includes ('+ '\''+ originationvar+ '\''+')';
   // if (!commercialStructurevar.equals('none'))
  //    soql += ' and Commercial_Structure__c includes ('+ '\''+ commercialStructurevar+ '\''+')';
   // if (!offeringsvar.equals('none'))
  //    soql += ' and Offerings__c includes ('+ '\''+ offeringsvar+ '\''+')';
  //  if (!engagementModelvar.equals('none'))
  //    soql += ' and Engagement_Model__c includes ('+ '\''+ engagementModelvar+ '\''+')';
   // if (!transitionModelvar.equals('none'))
   //   soql += ' and Transition_Model__c includes ('+ '\''+ transitionModelvar+ '\''+')';
    if (!uniqueClientGeosServicesvar.equals('none'))
      soql += ' and Unique_Client_Geos_Services__c includes ('+ '\''+ uniqueClientGeosServicesvar+ '\''+')';
    if (!gServiceDeliveryLocationsvar.equals('none'))
      soql += ' and G_Service_Delivery_Locations__c includes ('+ '\''+ gServiceDeliveryLocationsvar+ '\''+')';
    if (!businessDriversvar.equals('none'))
      soql += ' and Business_Drivers__c includes ('+ '\''+ businessDriversvar+ '\''+')';
    if (!leanDigitalvar.equals('none'))
      soql += ' and Lean_Digital__c includes ('+ '\''+ leanDigitalvar+ '\''+')';
   // if (!transformationvar.equals('none'))
   //   soql += ' and Transformation__c includes ('+ '\''+ transformationvar+ '\''+')';
   // if (!uniqueAboutThisEngagement.equals(''))
    //  soql += ' and Unique_about_this_engagement__c LIKE '+ '\'%'+String.escapeSingleQuotes(uniqueAboutThisEngagement )+'%' + '\'';
    /*if (!x2015npsDispositionvar.equals('none'))
      soql += ' and X2015_NPS_Disposition__c LIKE '+ '\'%'+String.escapeSingleQuotes(x2015npsDispositionvar )+'%' + '\''; */
    if (!currentDispositionvar.equals('none'))
      soql += ' and Current_Disposition__c LIKE '+ '\'%'+String.escapeSingleQuotes(currentDispositionvar)+'%' + '\'';

   
    // run the query again
    runQuery();

    return null;
  }

  // use apex describe to build the picklist values
  
    public List<selectOption> x2015npsDisposition {
    get {
      if (x2015npsDisposition == null) {

        x2015npsDisposition = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.X2015_NPS_Disposition__c.getDescribe();
        x2015npsDisposition.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          x2015npsDisposition.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return x2015npsDisposition;          
    }
    set;
  }
  
    public List<selectOption> currentDisposition {
    get {
      if (currentDisposition== null) {

        currentDisposition= new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Current_Disposition__c.getDescribe();
        currentDisposition.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          currentDisposition.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return currentDisposition;          
    }
    set;
  }

  
   public List<selectOption> serviceLine{
    get {
      if (serviceLine== null) {

        serviceLine= new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Service_Line__c.getDescribe();
        serviceLine.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          serviceLine.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return serviceLine;          
    }
    set;
  }
  
  /*public List<selectOption> abilityToInfluence {
    get {
      if (abilityToInfluence == null) {

        abilityToInfluence = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Ability_to_Influence__c.getDescribe();
        abilityToInfluence.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          abilityToInfluence.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return abilityToInfluence;          
    }
    set;
  } */
    public List<selectOption> canMakeAReference {
    get {
      if (canMakeAReference == null) {

        canMakeAReference = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Can_Make_a_Reference__c.getDescribe();
        canMakeAReference.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          canMakeAReference.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return canMakeAReference;          
    }
    set;
  }
  
  public List<selectOption> canMakePublicAppearances {
    get {
      if (canMakePublicAppearances == null) {

        canMakePublicAppearances = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Can_make_Public_Appearances__c.getDescribe();
        canMakePublicAppearances.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          canMakePublicAppearances.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return canMakeAReference;          
    }
    set;
  }
  
  public List<selectOption> canIntroToNewInternalBuyingCenter {
    get {
      if (canIntroToNewInternalBuyingCenter == null) {

        canIntroToNewInternalBuyingCenter = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Can_intro_to_New_internal_buying_center__c.getDescribe();
        canIntroToNewInternalBuyingCenter.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          canIntroToNewInternalBuyingCenter.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return canIntroToNewInternalBuyingCenter;          
    }
    set;
  }
  
  public List<selectOption> canMakeExternalIntroduction {
    get {
      if (canMakeExternalIntroduction == null) {

        canMakeExternalIntroduction = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Can_Make_External_Introduction__c.getDescribe();
        canMakeExternalIntroduction.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          canMakeExternalIntroduction.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return canMakeExternalIntroduction;          
    }
    set;
  }
  
  public List<selectOption> suitableForAnalystReference {
    get {
      if (suitableForAnalystReference == null) {

        suitableForAnalystReference = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Suitable_for_Analyst_Reference__c.getDescribe();
        suitableForAnalystReference.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          suitableForAnalystReference.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return suitableForAnalystReference;          
    }
    set;
  }
  
  public List<selectOption> origination {
    get {
      if (origination == null) {

        origination = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Origination__c.getDescribe();
        origination.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          origination.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return origination;          
    }
    set;
  }
  
  public List<selectOption> commercialStructure {
    get {
      if (commercialStructure == null) {

        commercialStructure = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Commercial_Structure__c.getDescribe();
        commercialStructure.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          commercialStructure.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return commercialStructure;          
    }
    set;
  }
  
  public List<selectOption> offerings {
    get {
      if (offerings == null) {

        offerings = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Offerings__c.getDescribe();
        offerings.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          offerings.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return offerings;          
    }
    set;
  }
  
  public List<selectOption> engagementModel {
    get {
      if (engagementModel == null) {

        engagementModel = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Engagement_Model__c.getDescribe();
        engagementModel.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          engagementModel.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return engagementModel;          
    }
    set;
  }
  
  public List<selectOption> transitionModel {
    get {
      if (transitionModel == null) {

        transitionModel = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Transition_Model__c.getDescribe();
        transitionModel.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          transitionModel.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return transitionModel;          
    }
    set;
  }
  
  public List<selectOption> uniqueClientGeosServices {
    get {
      if (uniqueClientGeosServices == null) {

        uniqueClientGeosServices = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Unique_Client_Geos_Services__c.getDescribe();
        uniqueClientGeosServices.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          uniqueClientGeosServices.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return uniqueClientGeosServices;          
    }
    set;
  }
  
  public List<selectOption> gServiceDeliveryLocations {
    get {
      if (gServiceDeliveryLocations == null) {

        gServiceDeliveryLocations = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.G_Service_Delivery_Locations__c.getDescribe();
        gServiceDeliveryLocations.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          gServiceDeliveryLocations.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return gServiceDeliveryLocations;          
    }
    set;
  }
  
  public List<selectOption> businessDrivers {
    get {
      if (businessDrivers == null) {

        businessDrivers = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Business_Drivers__c.getDescribe();
        businessDrivers.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          businessDrivers.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return businessDrivers;          
    }
    set;
  }
  
  public List<selectOption> leanDigital {
    get {
      if (leanDigital == null) {

        leanDigital = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Lean_Digital__c.getDescribe();
        leanDigital.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          leanDigital.add(new selectOption(f.getLabel(),f.getLabel()));
          
      }
      return leanDigital;          
    }
    set;
  }
  
  public List<selectOption> transformation {
    get {
      if (transformation == null) {

        transformation = new List<selectOption>();
        Schema.DescribeFieldResult field = Promoter_info__c.Transformation__c.getDescribe();
        transformation.add(new selectOption('none', ''));
        for (Schema.PicklistEntry f : field.getPicklistValues())
          transformation.add(new selectOption(f.getLabel(),f.getLabel()));

      }
      return transformation;          
    }
    set;
  }

}