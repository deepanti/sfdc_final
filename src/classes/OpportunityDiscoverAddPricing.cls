/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle Product Pricing  in Discover stage of opportunity
**************************************************************************************************************************/

public class OpportunityDiscoverAddPricing {
    
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for displaying the list of OpportunityLineItem of Products
* @param   OpportunityId - Opportunity Id
* @return  List<OpportunityLineItem>
**************************************************************************************************************************/  
    
    
    @AuraEnabled    
    public static List<OpportunityLineItem> getOLISMap(String OppId){
        try{
            return [Select Id, Name,
                    Product2Id,
                    OpportunityId,
                    Delivering_Organisation__c,
                    Delivery_Location__c,
                    FTE__c,Resell__c,
                    Local_Currency__c,
                    Opportunity_Id__c,
                    Product_BD_Rep__c,
                    Revenue_Start_Date__c,
                    Sub_Delivering_Organisation__c,
                    UnitPrice,
                    TotalPrice,
                    TCV__c,
                    Contract_Term__c,
                    Revenue_Exchange_Rate__c,
                    Pricing_Deal_Type_OLI_CPQ__c,
                    Type_of_Deal__c,
                    Product2.Name,
                    Product2.Id,
                    Product2.Industry_Vertical__c	,
                    Product2.Nature_of_work__c,
                    Product2.Service_line__c,
                    Opportunity.Opportunity_Source__c,
                    Opportunity.CloseDate,
                    Opportunity.QSRM_Status__c
                    from OpportunityLineItem 
                    where OpportunityId =:OppId
                    Order By CreatedDate DESC
                    LIMIT:limits.getLimitQueryRows()]; 
        }
        catch(dmlexception e){
             //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(OppId),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddPricing', 'getOLISMap','Fail','',String.valueOf(e.getLineNumber())); 
            system.debug('excetion '+ e.getMessage()+' '+ e.getLineNumber());
            return null;
        }
    }
    
    
    @auraEnabled
    public static List<DatedConversionRate> getConversionRates(){
            return [select id, isoCode, conversionrate, nextStartDate, startDate from DatedConversionRate  WHERE NextStartDate >= today];
    }
    
    
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic after saving OLI to create revenue schedule
* @param   OpportunityId - OLIList
* @return  void
**************************************************************************************************************************/  
    @auraEnabled
    public static void  saveOLI(List<OpportunityLineItem> OLIList){
        try{
            List<DatedConversionRate> comnnversionList = getConversionRates();
            System.debug('OLIList------>'+OLIList.size()+'---------->'+OLIList);
            //Database.update(OLIList, false);
            Set<id> opportunityLineItemsids = new Set<Id>();
            List<OpportunityLineItem> filteredLineItemsToUpdate = new List<OpportunityLineItem>();
            for(OpportunityLineItem OLIItem: OLIList)
            {
                opportunityLineItemsids.add(OLIItem.Id);                
            }
            Map<Id,OpportunityLineItem> previousOLIList;
            if(opportunityLineItemsids.size() > 0)
            {
                previousOLIList = new Map<Id,OpportunityLineItem>([select Id,TCV__c,Revenue_Start_Date__c,Contract_Term__c,Local_Currency__c
                                                                   from OpportunityLineItem where id in: opportunityLineItemsids]);
            }
            for(OpportunityLineItem lineItem: OLIList){
                if(lineItem.TCV__c != previousOLIList.get(lineItem.Id).TCV__c || 
                   lineItem.Revenue_Start_Date__c != previousOLIList.get(lineItem.Id).Revenue_Start_Date__c ||
                  lineItem.Contract_Term__c != previousOLIList.get(lineItem.Id).Contract_Term__c ||
                  lineItem.Local_Currency__c != previousOLIList.get(lineItem.Id).Local_Currency__c )
                {
                    for(DatedConversionRate conversionRate : comnnversionList)
                    {
                        if(lineItem.Local_Currency__c == conversionRate.IsoCode)
                        {
                            lineItem.UnitPrice = (lineItem.TCV__c /conversionRate.ConversionRate).setScale(2, RoundingMode.HALF_UP);
                        }
                    }
                    filteredLineItemsToUpdate.add(lineItem);
                }
            }
            System.debug('Update Records----->'+filteredLineItemsToUpdate);
            //update OLIList;
            update OLIList;
            List<OpportunityLineItemSchedule> OLISListToDelete = [Select ID FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId IN:filteredLineItemsToUpdate];
            if(OLISListToDelete!=null){
                Delete OLISListToDelete;
            }
            AddProductController.createRevenueSchedule(filteredLineItemsToUpdate);
        }
        
        catch(exception e){
            //creating error log
            CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddPricing', 'saveOLI','Fail','',String.valueOf(e.getLineNumber())); 
        }
       
    }
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for inserting OLI if type of deal is renewal
* @param   OpportunityId - String of OpportunityLineItem
* @return  - void
**************************************************************************************************************************/      
    @auraEnabled
    public static void insertOLI(String OLIStr, String OLIIdToUpdate){
        try{
             OpportunityLineItem OLI =(OpportunityLineItem)JSON.deserialize((OLIStr),OpportunityLineItem.class);
            
            insert OLI;
            OpportunityLineItem OLIToUpdate = [Select ID,OpportunityId,Revenue_Start_Date__c,Contract_Term__c,Local_Currency1__c,
                                               Local_Currency__c,Revenue_Exchange_Rate__c,
                                               Delivering_Organisation__c,Sub_Delivering_Organisation__c,
                                               Delivery_Location__c,Product_BD_Rep__c,FTE__c,Resell__c
                                               FROM OpportunityLineItem WHERE Id =:OLIIdToUpdate];
            Opportunity opp =  [Select LatestTabId__c FROM Opportunity WHERE Id =:OLIToUpdate.OpportunityId];
            opp.LatestTabId__c = '4';
            update opp;
            update OLIToUpdate;
        }
        catch(dmlexception e){
             //creating error log
            CreateErrorLog.createErrorRecord(OLIStr,e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddPricing', 'insertOLI','Fail','',String.valueOf(e.getLineNumber())); 
            system.debug('excetion '+ e.getMessage()+' '+ e.getLineNumber());
        }
        
    }
    
}