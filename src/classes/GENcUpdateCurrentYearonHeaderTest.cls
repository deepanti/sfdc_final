@isTest
public class GENcUpdateCurrentYearonHeaderTest 
{
    static testMethod void test() 
    {
        Database.QueryLocator QL;
        Database.BatchableContext BC;
        List<Opportunity> oppWithProducts = new List<Opportunity>();

        GENcUpdateCurrentYearonHeader uCYR = new GENcUpdateCurrentYearonHeader();

        QL = uCYR.start(bc);
  
        Database.QueryLocatorIterator QIT =  QL.iterator();
        while (QIT.hasNext())
        {
            Opportunity Opp = (Opportunity)QIT.next();            
            System.debug(Opp);
        }        

        uCYR.execute(BC, oppWithProducts);
        uCYR.finish(BC);        
    }
}