/*************************************************************************************************************************
        * @name : HomePageAnnouncementsTest
        * @author : Persistent
        * @description  : To Test HomePageAnnouncements
**************************************************************************************************************************/
@isTest
public class HomePageAnnouncementsTest {
    /*************************************************************************************************************************
        * @name : init
        * @author : Persistent
        * @description  : setup the test data
        * @param  : na
        * @return : na
**************************************************************************************************************************/
    
	@TestSetup
    public static void init()
    {
        //Create announcement records without file
        Announcement__c Ann1 = new Announcement__c(Title__c='Test 1',valid_from__c=DateTime.now().addDays(-1),valid_till__c=DateTime.now().addDays(4));
        insert Ann1;
        Announcement__c Ann2 = new Announcement__c(Title__c='Test 2',valid_from__c=DateTime.now().addDays(-1),valid_till__c=DateTime.now().addDays(4 ));
        insert Ann2;
        
        //Add a content document to Announcement 2
        ContentVersion contentVersion = new ContentVersion(
          Title = 'Test',
          PathOnClient = 'Test.pdf',
          VersionData = Blob.valueOf('JVBERi0xLjMKJcTl8uXrp/Og0MTGCjQgMCBvYmoKPDwgL0xlbmd0aCA1IDAgUiAvRmlsdGVyIC9GbGF0ZURlY29kZSA+PgpzdHJlYW0KeAFFjE0KwjAQhfc9xbfUhelMbJtmq3iAwoBrCS1WrFDT+2PEhfDg8X5XBlaCJ7TBadc1NK06j3oX/LHI98iVF/U5Kykj5FQG4mLoQ6OxGMLhL79PXXR9rNLCyUpRRBRLqP66hVtUsIXaTCnhxM7uc6bgxjbmjWl+jm6PPbhYNTB8AA+EJngKZW5kc3RyZWFtCmVuZG9iago1IDAgb2JqCjEzNAplbmRvY='),
          IsMajorVersion = true
        );
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = Ann2.Id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
         
        //Create announcement record with file
        Announcement__c Ann3 = new Announcement__c(Title__c='Test 3',valid_from__c=DateTime.newInstance(2018, 1, 1),valid_till__c=DateTime.newInstance(2018, 1, 2));
        insert Ann3;
    }
    
    /*************************************************************************************************************************
        * @name : testAnnouncementFlow
        * @author : Persistent
        * @description  : Test the whole announcment flow with visibility, user preference update
        * @param  : na
        * @return : na
**************************************************************************************************************************/
    @isTest
    public static void testAnnouncementFlow()
    {
        Test.startTest();
        System.assertEquals(2, HomePageAnnouncements.getActiveAnnouncements().size());
        System.assertEquals(0, [select count() from User_Announcements_Preference__c]);
        
        HomePageAnnouncements.updateUserPreference([select id from Announcement__c where isActive__c=true limit 1].id);
        System.assertEquals(1, HomePageAnnouncements.getActiveAnnouncements().size());
        System.assertEquals(1, [select count() from User_Announcements_Preference__c]);
        Test.stopTest();
    }
}