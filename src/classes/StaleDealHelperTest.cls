@istest(seeAllData = true)
public class StaleDealHelperTest {
    
    static opportunity opp31,opp, opp1, opp2, opp3, opp4, opp5,opp6, opp7, opp8, opp9, opp10, opp11, opp12, opp13, opp14, opp15, opp16,opp21,opp22;
    static User u1;
    
    Static testmethod void testHelperMethod(){
        setUpTestMethod();
        StaleDealHelper.fetch_Non_TS_Ageing_Opps_new();
        StaleDealHelper.fetch_TS_Ageing_Opps_new();
    }
    
    Static testmethod void testHelperMethodForStaleUser(){
        setUpTestMethod();
        System.runAs(u1){
            StaleDealHelper.fetch_Non_TS_Ageing_Opps_new();
            StaleDealHelper.fetch_TS_Ageing_Opps_new();
        }
    }
    
    private static void setUpTestMethod(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        list<opportunity> oliList= new List<opportunity>();
        opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                            amount = 2000000);
        
        opp15=new opportunity(name='12348',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                            amount = 20000000);
        
        opp2=new opportunity(name='1236',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp3=new opportunity(name='1237',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp5=new opportunity(name='12311',StageName='3. On Bid',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp7=new opportunity(name='1239',StageName='4. Down Select',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        
         opp14=new opportunity(name='12324',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 200000001);
        
        opp8=new opportunity(name='12312',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 6000000);
        opp9=new opportunity(name='12313',StageName='3. On Bid',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 6000000);
        opp10=new opportunity(name='12314',StageName='4. Down Select',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000000);
        opp11=new opportunity(name='12315',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000000);
        opp31=new opportunity(name='12331',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000);
        oliList.add(opp);
        oliList.add(opp2);
        oliList.add(opp3);
        oliList.add(opp5);
        oliList.add(opp7);
        oliList.add(opp8);
        oliList.add(opp9);
        oliList.add(opp10);
        oliList.add(opp11);
        oliList.add(opp14);
        oliList.add(opp15);
        oliList.add(opp31);
        
        insert oliList;
        
        System.runAs(u1){
            list<opportunity> oliList1= new List<opportunity>();
            opp4=new opportunity(name='1238',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 2000005);
             opp16=new opportunity(name='12387',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 20000000);
            
            opp6=new opportunity(name='12310',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, 
                                 Type_Of_Opportunity__c = 'Non Ts' ,Amount = 2000005);
            
            opp12=new opportunity(name='12322',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000000);
            
            opp13=new opportunity(name='12323',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 60000000);
            opp21=new opportunity(name='12331',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 600);
            opp22=new opportunity(name='12332',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Digital', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 600);
            
            oliList1.add(opp4);
            oliList1.add(opp6);          
            oliList1.add(opp12);
            oliList1.add(opp13);
            oliList1.add(opp16);
             oliList1.add(opp21);
            oliList1.add(opp22);
            insert oliList1;
        }  
        
    }
}