@isTest
public class VIC_TargetAccessOfUserSupervisorTest {
     
     
     public static User objuser,objuser1;
     public static Account objAccount;
     public static Opportunity objOpp;
     public static Target__c targetObj;
     
     static testmethod void VIC_TargetAccessToUserSupervisorTest(){
        
        loadData();
        targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        objuser.Supervisor__c = objuser1.Id;
        update objuser;
        
        objuser.Supervisor__c = null;
        update objuser;
         
     }
     static void loadData(){
       
       objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
       insert objuser;
       
       objuser1= VIC_CommonTest.createUser('Test1','Test Name1','jdncjn@jdjhdg1.com','Genpact Super Admin','China');
       insert objuser1;
       
     }
}