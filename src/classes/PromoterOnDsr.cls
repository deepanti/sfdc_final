public with sharing class PromoterOnDsr {
    public Opportunity oppty {get; set;}
    public Set<Id> PromoterIdSet = new Set<Id>();
    public Boolean chkProduct=false;
    public String ID= '';
    public String DsrId='';
    public PromoterOnDsr(ApexPages.StandardController controller) {
        
        DsrID = controller.getRecord().Id;
        List<GW1_DSR__c> dsrrec=[Select id, GW1_Opportunity__r.id from GW1_DSR__c where id=: DsrId];
        ID= dsrrec[0].GW1_Opportunity__r.id;
        List<ID> PromoterIdList=new List<ID>();
        List<Promoter_Info__c> allPromoter = new List<Promoter_Info__c>();
        //List <OpportunityContactRole> oppcr=[Select ContactId from OpportunityContactRole where OpportunityId=: ID];
        //system.debug('oppcr:'+ oppcr);
        
        /*for(OpportunityContactRole ocr: oppcr)
{
PromoterIdList.add(ocr.ContactId);
}*/
        String accquery='';
        Account acc = new Account();
        String oppquery = 'select id,Accountid,name,ownerid,owner.name,Opportunity_ID__c,Service_Line__c,Industry_Vertical__c,Formula_Account_Name__c,Product_Family__c from opportunity where id = \'' +ID+ '\'';
        oppty = database.query(oppquery);
        System.debug('Oppty size: '+ oppty);
        if(oppty.AccountId != NULL)
        {
            accquery='Select Name,Industry_Vertical__c from Account where id=\''+oppty.AccountId+'\'';
            acc=database.query(accquery);
        }
        List<String>productList=new List<String>();
        if(oppty.Product_Family__c!=NULL)
        {
            productList=oppty.Product_Family__c.split(',');
            chkProduct=true;
        }
        Boolean chk=false;
        Set<String>productSet = new Set<String>();
        
        for(string s: productList)
        {
            productSet.add(s.trim());
            if(s.trim().equals('Core-Ops'))
                chk=true;
        }
        System.Debug('productSet: '+ productSet);
        String productQuery='';
        String accq='';
        for(string p:productSet)
        {
            if(productQuery=='')
            {
                productQuery = ' AND Service_Line__c INCLUDES(\'' + p +'\'';
            }
            else
            {
                productQuery = productQuery + ',\'' + p +'\'';
            }
        }
        if (chk==true && productQuery!='' )
            productQuery= productQuery = productQuery + ',\'Multi Channel Contact Centre Management\'';
        
        if(productQuery!='')
        {
            productQuery = productQuery + ')';
        }
        
        System.Debug('productQuery: '+ productQuery);
        
        if(oppty.AccountId != NULL)
        {
            accq = 'where Name_Promoter__r.Account.Industry_Vertical__c = \'' + oppty.Industry_Vertical__c + '\' ' ;
        }
        //soqlcontactrole = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Name_Promoter__c IN('+cidSet+ ')';
        //soql = 'Select Name_Promoter__c from Promoter_info__c where Name_Promoter__r.Account.Industry_Vertical__c = \''+ acc.Industry_Vertical__c +'\' OR Account_Name__c  = \''+acc.Name+'\'';
        //allPromoter= [Select Name_Promoter__c from Promoter_info__c where Name_Promoter__r.Account.Industry_Vertical__c =:oppty.Industry_Vertical__c OR Account_Name__c  =: oppty.Formula_Account_Name__c OR Service_Line__c=: oppty.Service_Line__c];
        String allPromotersoql= 'Select Name_Promoter__c from Promoter_info__c ' + accq  + productQuery;
        System.Debug('allPromotersoql: '+ allPromotersoql);
        allPromoter=Database.query(allPromotersoql);
        for(Promoter_info__c pi: allPromoter)
        {
            PromoterIdList.add(pi.Name_Promoter__c);
        }
        
        for(ID x:PromoterIdList)
        {
            PromoterIdSet.add(x);
        }
        
        //soql = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Name_Promoter__c IN: PromoterIdSet ;
        runQuery();
        
    }
    
    // the soql without the order and limit
    private String soql {get;set;}
    
    // the collection of contacts to display
    public List<Promoter_info__c> promoter {get;set;}
    
    public List<Promoter_info__c> promoterFromContactRole {get;set;}
    public Set<Promoter_Info__c>promoterSet{get;set;}
    
    // the current sort direction. defaults to asc
    public String sortDir {
        get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
        set;
    }
    
    // the current field to sort by. defaults to last name
    public String sortField {
        get  { if (sortField == null) {sortField = 'name'; } return sortField;  }
        set;
    }
    
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return soql + ' order by ' + sortField + ' ' + sortDir ; }
        set;
    }
    
    // init the controller and display some sample data when the page loads
    public PromoterOnDsr() {
        //soql = 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c,Ability_to_Influence__c,Influence_Quotient_Score__c,Can_Make_a_Reference__c,Can_intro_to_New_internal_buying_center__c,Can_make_Public_Appearances__c,Can_Make_External_Introduction__c,Suitable_for_Analyst_Reference__c,Origination__c,Commercial_Structure__c,Offerings__c,Engagement_Model__c,Transition_Model__c,Unique_Client_Geos_Services__c,G_Service_Delivery_Locations__c,  Business_Drivers__c,Lean_Digital__c,Transformation__c,X2015_NPS_Disposition__c,Current_Disposition__c,Unique_about_this_engagement__c from Promoter_info__c where id != null';
        //runQuery();
    }
    
    // toggles the sorting of query from asc<-->desc
    public void toggleSort() {
        // simply toggle the direction
        sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
        // run the query again
        runQuery();
    }
    
    // runs the actual query
    public void runQuery() {
        
        if(chkProduct==true)
        {
            System.Debug('SOQL: ' + 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Name_Promoter__c IN '+ PromoterIdSet + 'order by '+ sortField + ' '+ sortDir );
            promoter= [select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Name_Promoter__c IN: PromoterIdSet ];    
            System.debug('Promoter list Size: '+ promoter.size());
            if(promoter.size()>20)
                promoter=[select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Ability_to_Influence__c = 'High' AND Can_Make_a_Reference__c = 'Yes' AND Name_Promoter__c IN: PromoterIdSet LIMIT 20];
        }
        else
        {
            System.Debug('SOQL: ' + 'select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Name_Promoter__c IN '+ PromoterIdSet + 'order by '+ sortField + ' '+ sortDir );
            promoter= [select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Ability_to_Influence__c = 'High' AND Can_Make_a_Reference__c = 'Yes' AND Name_Promoter__c IN: PromoterIdSet LIMIT 20 ];    
            System.debug('Promoter list Size: '+ promoter.size());
            //if(promoter.size()>20)
                //promoter=[select id,Vertical__c,Sub_Vertical__c,Relationship_Holder__c,Service_Line__c,Sub_Service_Line__c,Sub_Service_Line_short__c,name,Job_Title__c,Text_Promoter_Name__c,Account_Owner__c,Account_Name__c from Promoter_info__c where Ability_to_Influence__c = 'High' AND Can_Make_a_Reference__c = 'Yes' AND Name_Promoter__c IN: PromoterIdSet LIMIT 20];
            
        }
        
    }
    
}