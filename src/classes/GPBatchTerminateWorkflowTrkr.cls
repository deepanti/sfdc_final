@isTest
public class GPBatchTerminateWorkflowTrkr {
    
    public static GP_Project__c prjObj; 
    public static GP_Project__c prjObjSeven; 
    public static GP_Project__c prjObjFourteen;
    public static GP_Project__c prjObjTwentyOne; 
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = UserInfo.getUserId();   
        insert empObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;
        
        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
       
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; 
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        prjObj.GP_Approver_Ids__c= loggedInUser;
        prjObj.GP_Approval_Status__c = 'Rejected';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        prjObj.GP_Start_Date__c= System.today().addDays(-365);
        prjObj.GP_End_Date__c= System.today().addDays(60);
        prjObj.GP_Project_Submission_Date__c = System.Today().addDays(-Integer.valueof(System.Label.GP_Project_Submission_Valid_days));
        
        insert prjObj;
    }
    static testmethod void testbatch() {
        Test.startTest();       
        
        GPBatchTerminateWorkflow objTerminatedWF = new GPBatchTerminateWorkflow();
        Database.executebatch(objTerminatedWF); 
        
        Test.stopTest();
    }
    
}