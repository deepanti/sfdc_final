@istest
public class ExpertSearchExtensiontest
{
    public static testMethod void testController()
    {
 Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
       Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
   oContact.title='Mr.';
   
   update oContact;
Expert__c expertobject=new Expert__c(Name='Test',Expert_Type__c='External',Industrial_Vertical__c='BFS',Product_Family__c='IT Services',Service_Line__c='IT Services',
Geo_Served__c='North America',Sub_Industry_Vertical__c='BFS',Product__c='Platforms');
        insert expertobject;
//PageReference ref= new PageReference('/apex/ExpertSerachPage’);

// start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        PageReference tpageRef = Page.ExpertSerachPage;

// call the constructor
 ApexPages.StandardController std = new ApexPages.StandardController(expertobject);
        ExpertSearchExtension controller = new ExpertSearchExtension(std);
      //  tpageRef.getParameters().put('expid', String.valueOf(expertobject.Id));
      //  tpageRef.getParameters().put('IV', 'BFS');
      //  tpageRef.getParameters().put('PF', 'IT Services');
		Test.setCurrentPage(tpageRef);
          // test action methods on your controller and verify the output with assertions
        controller.expertname ='Manoj Pandey';
        controller.industryverticalvar = 'BFS';
        controller.prodfamilyvar = 'IT Services';
        controller.serlinevar ='IT Services';
        controller.experttypevar='External';
        controller.productvar = 'Platforms';
        controller.geoservedvar= 'North America';
        controller.subindustryverticalvar= 'BFS';
        controller.Geo='North America';
        controller.SIV='BFS';
        controller.exptype='External';
        controller.expname='Manoj Pandey';
       
        
        controller.toggleSort();
        controller.reset();
        controller.runSearch();
        String soql=controller.debugSoql;

      
List<selectOption> serviceLine= controller.serviceLine;
List<selectOption> industryvertical = controller.industryvertical ;
List<selectOption> prodfamily= controller.prodfamily;
List<selectOption> expertType= controller.expertType;
List<selectOption> product= controller.product;
List<selectOption> geoserved= controller.geoserved;
List<selectOption> subindustryvertical= controller.subindustryvertical;

    
    //Test.stopTest();                  
        
            

       PageReference result = controller.runSearch();
    System.assertEquals(null, result);
      // controller.runSearch();
      
        // stop the test
        Test.stopTest();
    }
}