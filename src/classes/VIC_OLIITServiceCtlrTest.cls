@isTest
public class VIC_OLIITServiceCtlrTest 
{
    Public static user objuser;
    Public static user objuser1;
    Public static user objuser2;
    Public static user objuser3;
    Public static Map <Id, User_VIC_Role__c> mapvicrole;
    Public static OpportunityLineItem  objOLI;
    Public static OpportunityLineItem  objOLI1;    
    
    Public static List<OpportunityLineItem>  objOLIList=new List<OpportunityLineItem>();
    Public static Opportunity objOpp;
    Public static Opportunity objOpp1;    
    Public static Map<Id,Opportunity> mapIdToObjOppARG = new map<Id,Opportunity>();
    Public static Master_VIC_Role__c masterVICRoleObj;
    Public static Master_VIC_Role__c masterVICRoleObj1;
    Public static User_VIC_Role__c objuservicrole;
    Public static User_VIC_Role__c objuservicrole1;
    Public static Plan__c planObj1;
    
    static  testmethod void validateVIC_OLIITServiceCtlr()
    {
        LoadData();
        
        mapvicrole=vic_CommonUtil.getUserVICRoleinMap();
        objOLIList.add(objOLI);
        
        new VIC_OLIITServiceCtlr().serviceManagerForIT(objOLIList,mapvicrole,mapIdToObjOppARG,null);
        System.AssertEquals(200,200);
        
        
        
    }
    
    static  testmethod void validateVIC_OLIITServiceNCtlr()
    {
        LoadData();
        masterVICRoleObj.Horizontal__c='ITO';
        update masterVICRoleObj;
        mapvicrole=vic_CommonUtil.getUserVICRoleinMap();
        
        objOLIList.add(objOLI);
        
        new VIC_OLIITServiceCtlr().serviceManagerForIT(objOLIList,mapvicrole,mapIdToObjOppARG,null);
        masterVICRoleObj1.Horizontal__c='ITO';   
        update masterVICRoleObj1;   
        mapvicrole=vic_CommonUtil.getUserVICRoleinMap();
        
        
        new VIC_OLIITServiceCtlr().serviceManagerForIT(objOLIList,mapvicrole,mapIdToObjOppARG,null);   
        System.AssertEquals(200,200);
        
        
        
    }
    
    
    static void LoadData()
    {
        
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
        insert objuser;
        
        objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
        
        objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        
        
        
        
        
        
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
        
        planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';    
        insert planobj1;
        
        masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        masterVICRoleObj.Horizontal__c='Enterprise sales';
        masterVICRoleObj.Role__c='BD';
        insert masterVICRoleObj;
        
        masterVICRoleObj1 =  VIC_CommonTest.getMasterVICRole();
        masterVICRoleObj1.Horizontal__c='Enterprise sales';
        masterVICRoleObj1.Role__c='BD';
        insert masterVICRoleObj1;    
        
        objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        objuservicrole1=VIC_CommonTest.getUserVICRole(objuser1.id);
        objuservicrole1.vic_For_Previous_Year__c=false;
        objuservicrole1.Not_Applicable_for_VIC__c = false;
        objuservicrole1.Master_VIC_Role__c=masterVICRoleObj1.id;
        insert objuservicrole1;    
        
        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        
        
        
        objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser.id;
        objopp.Sales_country__c='Canada';
        
        insert objOpp;
        
        objOpp1=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp1.Actual_Close_Date__c=system.today();
        objopp1.ownerid=objuser1.id;
        objopp1.Sales_country__c='Canada';
        
        insert objOpp1;
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp1.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_Contract_Term__c=24;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;   
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.vic_CPQ_Deal_Status__c= 'Active';  
        
        objOLI.vic_TCV_EBIT_Consideration_ForNPV__c='5';
        insert objOLI;
        
        objOLI =[select id,vic_Owner__c,Product_BD_Rep__c from opportunitylineitem where id=:objOLI.id];
        
        
        
        //  objOLI.vic_Owner__c=objuser.id;
        
        system.debug('objOLI'+objOLI);
        
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_IO'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
        
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
        
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        
        Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        insert objInc;
        mapIdToObjOppARG.put(objOpp.id,objOpp);
        
        system.debug('targetComponentObj'+targetComponentObj);
        system.debug('objuser'+objuser);    
        
        
        
        
    }
}