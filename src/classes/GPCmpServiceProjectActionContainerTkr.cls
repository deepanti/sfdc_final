@isTest public class GPCmpServiceProjectActionContainerTkr {
    public static GP_Project__c prjObj;
    
    static void setupCommonData() {
         GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
         Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
 
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
  
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Business_Type__c = 'PBB';
        dealObj.GP_Business_Name__c = 'Consulting';
        dealObj.GP_Business_Group_L1__c = 'group1';
        dealObj.GP_Business_Segment_L2__c = 'segment2';
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c bpmTemplate = GPCommonTracker.getProjectTemplate();
        bpmTemplate.Name = 'BPM-BPM-ALL';
        bpmTemplate.GP_Active__c = true;
        bpmTemplate.GP_Business_Type__c = 'BPM';
        bpmTemplate.GP_Business_Name__c = 'BPM';
        bpmTemplate.GP_Business_Group_L1__c = 'All';
        bpmTemplate.GP_Business_Segment_L2__c = 'All';
        insert bpmTemplate ;
        
        GP_Project_Template__c bpmWithoutSFDCTemplate = GPCommonTracker.getProjectTemplate();
        
        bpmWithoutSFDCTemplate.GP_Active__c = true;
        bpmWithoutSFDCTemplate.Name = 'BPM-BPM-ALL-Without-SFDC';
        bpmWithoutSFDCTemplate.GP_Business_Type__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Name__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Group_L1__c = 'All';
        bpmWithoutSFDCTemplate.GP_Business_Segment_L2__c = 'All';
        bpmWithoutSFDCTemplate.GP_Without_SFDC__c = true;
        
        insert bpmWithoutSFDCTemplate;
        
        GP_Project_Template__c indirectTemp = GPCommonTracker.getProjectTemplate();
        indirectTemp.GP_Active__c = true;
        indirectTemp.Name = 'INDIRECT_INDIRECT';
        indirectTemp.GP_Business_Type__c = 'Indirect';
        indirectTemp.GP_Business_Name__c = 'Indirect';
        indirectTemp.GP_Business_Group_L1__c = 'All';
        indirectTemp.GP_Business_Segment_L2__c = 'All';
        insert indirectTemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',indirectTemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        prjObj.Ownerid = UserInfo.getUserId();
        insert prjObj;

        GP_Resource_Allocation__c prjResource=GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        prjResource.GP_IsUpdated__c=true;
        insert prjResource;
        
        GP_Resource_Allocation__c prjResource1 =GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        prjResource1.GP_IsUpdated__c=true;
        prjResource1.GP_Oracle_Status__c='';
        insert prjResource1;
        
        prjResource.GP_Oracle_Status__c='S';
        prjResource.GP_Oracle_Id__c='123456';
        update prjResource;
    }
    
    @isTest
    public static void testgetUserAccess()
    {
        setupCommonData();
        GPAuraResponse objGPAuraResponse = GPCmpServiceProjectActionContainer.getUserAccess(prjObj.Id);
        
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_Oracle_Status__c='S';   
        prjObj.GP_Oracle_PID__c='Test';  
        update prjObj;
        objGPAuraResponse = GPCmpServiceProjectActionContainer.getUserAccess(prjObj.Id);
         
         //Added for Code coverage
        string S =GPCmpServiceProjectActionContainer.isResourceSync(prjObj.Id);
        
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Oracle_PID__c = 'Test';
        update prjObj;
        objGPAuraResponse = GPCmpServiceProjectActionContainer.getUserAccess(prjObj.Id);
        
        prjObj.GP_Approval_Status__c = 'Pending for Approval';
        update prjObj;
        objGPAuraResponse = GPCmpServiceProjectActionContainer.getUserAccess(prjObj.Id);
    }
}