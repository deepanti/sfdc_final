public class GPSelectorProjectWorkLocationSDO extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project_Work_Location_SDO__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Work_Location_SDO__c.sObjectType;
    }

    public List < GP_Project_Work_Location_SDO__c > selectById(Set < ID > idSet) {
        return (List < GP_Project_Work_Location_SDO__c > ) selectSObjectsById(idSet);
    }

    public List < GP_Project_Work_Location_SDO__c > selectProjectWorkLocationAndSDORecords(Id projectId) {
        return [SELECT Id, Name, GP_Project__c, GP_Work_Location__c, RecordTypeId, GP_Work_Location__r.GP_Legal_Entity_Code__c,GP_is_Active__c,GP_BCP__c,
            RecordType.Name, GP_Work_Location__r.RecordType.Name, GP_Work_Location__r.Name, GP_Work_Location__r.GP_Oracle_Id__c,GP_Effective_End_Date__c
            FROM GP_Project_Work_Location_SDO__c
            where GP_Project__c =: projectId and GP_Work_Location__c != null
        ];
    }
    public List < GP_Project_Work_Location_SDO__c > selectProjectWorkLocationAndSDORecords(Set < Id > lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Project_Work_Location_SDO__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Work_Location_SDO__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }

    public static List < GP_Project_Work_Location_SDO__c > selectProjectWorkLocationAndSDORecordsUnderProject(Id projectId) {
        return [SELECT Id,
                Name,
                GP_Project__c,
                GP_Primary__c,
                GP_is_Active__c,
                RecordType.Name,
                GP_Work_Location__c,
                GP_BCP__c, RecordTypeId,
                GP_Work_Location__r.Name,
                GP_Work_Location__r.GP_Unique_Name__c,
                GP_Work_Location__r.GP_Country__c,
                GP_Work_Location__r.GP_COE_Name__c,
                GP_Work_Location__r.GP_COE_Code__c,
                GP_Work_Location__r.GP_Oracle_Id__c,
                GP_Parent_Project_Work_Location_SDO__c,
                GP_Work_Location__r.GP_Lending_SDO_Code__c,
                GP_Work_Location__r.GP_Legal_Entity_Code__c,
                GP_Work_Location__r.GP_Legal_Entity_Name__c
                FROM GP_Project_Work_Location_SDO__c
                where GP_Project__c =: projectId
               ];
    }

    public static List < GP_Project_Work_Location_SDO__c > selectProjectWorkLocationforProjectIds(set < Id > setprojectId) {
        return [SELECT Id,
            Name,
            GP_Project__c,
            GP_Primary__c,
            RecordTypeId, GP_Work_Location_LE_Code__c,
            GP_is_Active__c,GP_BCP__c,
            GP_Project_Record_Type__c
            FROM GP_Project_Work_Location_SDO__c
            where GP_Project__c =: setprojectId
            and recordtypeId =: gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'Work Location')
        ];
    }

    public static List < GP_Project_Work_Location_SDO__c > selectProjectSDOforProjectIds(set < Id > setprojectId) {
        return [SELECT Id,
            Name,
            GP_Project__c,
            GP_Work_Location__c,
            GP_is_Active__c,GP_BCP__c,
            GP_Primary__c,
            RecordTypeId, GP_Work_Location_LE_Code__c,
            GP_Project_Record_Type__c
            FROM GP_Project_Work_Location_SDO__c
            where GP_Project__c =: setprojectId
            and recordtypeId =: gpCommon.getRecordTypeId('GP_Project_Work_Location_SDO__c', 'SDO')
        ];
    }
}