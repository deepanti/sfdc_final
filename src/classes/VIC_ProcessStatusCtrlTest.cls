@isTest
public class VIC_ProcessStatusCtrlTest
{
    public static Master_Plan_Component__c masterPlanComponentObj;
    public static Master_Plan_Component__c masterPlanComponentObj1;
    public static testMethod void runTestVIC_ProcessStatusCtrl()
    {
        loadData();
        Test.startTest();
        
        SchedulableContext sc;
        VIC_ScheduleSendNotToSupnHrBatch obj = new VIC_ScheduleSendNotToSupnHrBatch();
        obj.execute(sc);
        VIC_ProcessStatusCtrl.runUpTCVAcceleratorIOTSBatchServerController();
        List<String> getFinancialYear= VIC_ProcessStatusCtrl.getFinancialYear();
        String getCurrentVICYear= VIC_ProcessStatusCtrl.getCurrentVICYear();
        String getCurrentAnnualYear= VIC_ProcessStatusCtrl.getCurrentAnnualYear();
        boolean setVICProcessYear= VIC_ProcessStatusCtrl.setVICProcessYear(1997);
        boolean setAnnualProcessYear= VIC_ProcessStatusCtrl.setAnnualProcessYear(2000);
        System.assertEquals(true,setAnnualProcessYear);
        VIC_ProcessStatusCtrl.saveHRDay(18);
        VIC_ProcessStatusCtrl.saveSLDay(19);
        boolean getRunningStatusOfJob= VIC_ProcessStatusCtrl.getRunningStatusOfJob('abc');
        // Date getlastdeactivatedOfJob= VIC_ProcessStatusCtrl.getlastdeactivatedOfJob('VIC Job1');
        VIC_ProcessStatusCtrl.enaqueSchedulerJob('abc',true);
        VIC_ProcessStatusCtrl.enaqueSchedulerJob('abc',false);
        VIC_ProcessStatusCtrl.enaqueSendSchedulerJob('abc',true,'Sunday',0.0);
        VIC_ProcessStatusCtrl.enaqueSendSchedulerJob('abc',false,'Monday',5.0);
        VIC_ProcessStatusCtrl.runUpfrontBatchServerController();
        VIC_ProcessStatusCtrl.runUpfrontBatchAnnualServerController();
        VIC_ProcessStatusCtrl.runUpTCVBatchServerController();
        VIC_ProcessStatusCtrl.runCalculateGuaranteedAmountController();
        VIC_ProcessStatusCtrl.runCurrConvUpfrController();
        VIC_ProcessStatusCtrl.runCurrConvAnnualController();
        VIC_ProcessStatusCtrl.runUpScoreCardBatchServerController();
        
        system.assertequals(200,200);
        Test.stopTest();
        
    }
    
    static void loadData(){
        user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
        insert objuser;
        masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Guarantee','Currency');
        masterPlanComponentObj.vic_Component_Code__c='TCV_Accelerators'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
        masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Guarantee','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='Guarantee'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
        insert(new VIC_Process_Information__c(VIC_Process_Year__c=1996,VIC_Annual_Process_Year__c=2018));
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';  
        planObj1.Year__c='2018';
        insert planobj1;
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        targetObj.Plan__c=planobj1.id;
        targetObj.vic_TCV_IO__c=1000;
        targetObj.vic_TCV_TS__c=1000;
        
        insert targetObj;
        targetObj=[select id,user__c,Plan__c,vic_Total_TCV__c from Target__c where id=:targetObj.id];
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;       
        
        
    }
}