/**
* @author Anmol.kumar
* @date 22/12/2017
*
* @group GPAuraResponse
* @group-content ../../ApexDocContent/GPAuraResponse.html
*
* @description Wrapper class to return Aura response.
*/
global class GPAuraResponse {
    @AuraEnabled @testVisible
    private Boolean isSuccess{get; set;}
    @AuraEnabled @testVisible
    private String message{get; set;}
    @AuraEnabled @testVisible
    private String response{get; set;}
    
    /**
    * @description Parameterized Constructor.
    * @param Boolean isSuccess 
    * @param String message
    * @param String response
    * 
    * @example
    * new GPAuraResponse(<true/false>, <message>, <Serialized reponse>);
    */
    
    
    public GPAuraResponse(Boolean isSuccess, String message, String response) {
        this.isSuccess = isSuccess;
        this.message   = message;
        this.response  = response;
    }
    
    public boolean getisSuccess {
    	get {return isSuccess ;}
    }
    public String getMessage {
    	get {return message ;}
    }
}