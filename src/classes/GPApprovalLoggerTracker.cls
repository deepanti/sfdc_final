@isTest
private class GPApprovalLoggerTracker {
	@isTest
    static void testApprovalLogger() {
        GPApprovalLogger approvalLogger = new GPApprovalLogger();
        approvalLogger = new GPApprovalLogger(true);
        List<String> listOfError = new List<String>();
        listOfError.add('error 1');
        approvalLogger.addError('error 1');
        approvalLogger.isValid();
        approvalLogger.addError(listOfError);
        approvalLogger.addError('section', 'sub section', listOfError);
        approvalLogger.addError('section1', 'sub section1', listOfError);
        approvalLogger.addError('section2', 'sub section2', 'error');
        approvalLogger.addError('section3', 'sub section4', 'error');
        approvalLogger.addError(userInfo.getUserId(), 'error');
        approvalLogger.addError(userInfo.getUserId(), listOfError);
        approvalLogger.toString();
        approvalLogger = new GPApprovalLogger(false);
        approvalLogger.toString();
    }
}