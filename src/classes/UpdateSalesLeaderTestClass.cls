@isTest
public class UpdateSalesLeaderTestClass
{
    public static testMethod void UpdateSalesLeaderTestMethod() 
    { 
        list<Profile> SalesLeadProfile = new list<Profile>();
        SalesLeadProfile = [SELECT Id FROM Profile WHERE Name='Genpact SL / CP'];
        Sales_Unit__c  SalesUnitObj = new Sales_Unit__c();
        Account_Creation_Request__c AccountCreationReqObj =GEN_Util_Test_Data.CreateNewAccountCreationRequest(userinfo.getuserId(),'TestReqtest','TestDescription');
        Account_Creation_Request__c AccountCreationReqObj2 =GEN_Util_Test_Data.CreateNewAccountCreationRequest(userinfo.getuserId(),'TestReqtest22','TestDescription');
        list<Account_Creation_Request__c> LstToUpdate = new list<Account_Creation_Request__c>();
        if(SalesLeadProfile.size()>0)
        {
            User SalesLead = GEN_Util_Test_Data.CreateUser('TesetUser@testorg.com',SalesLeadProfile[0].Id,'TesetUser@testorg.com' );
            list<UserRole> RoleLst = new list<UserRole>();
            SalesLead.Sales_Leader__c = true;
            update SalesLead;
            SalesUnitObj = GEN_Util_Test_Data.CreateSalesUnit('TestSalesUnit',SalesLead.id,'Sales Unit Head');
            AccountCreationReqObj.Sales_Unit__c = SalesUnitObj.id;
            AccountCreationReqObj2.Sales_Unit__c  = SalesUnitObj.id;
            LstToUpdate.add(AccountCreationReqObj);
            LstToUpdate.add(AccountCreationReqObj2); 
        }
        if(LstToUpdate.size()>0)
        update LstToUpdate;
     }
     public static testMethod void UpdateManagerFromParentRoleTestMethod() 
     {
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];  
            User oUser = new User();
            oUser.Alias = 'Test';
            oUser.Email = 'TesetUser@genpact.com';
            oUser.EmailEncodingKey='UTF-8';
            oUser.LastName='Test';
            oUser.LanguageLocaleKey='en_US';
            oUser.LocaleSidKey='en_US';
            Ouser.ProfileId=p.Id;
            oUser.OHR_ID__c='700060235';
            oUser.TimeZoneSidKey='America/Los_Angeles';
            oUser.UserName='TesetUser@testorg.com';
             list<UserRole> RoleLst = new list<UserRole>();
            RoleLst = [select id,name from userRole where Name = 'CEO']; // as discuss with DC i am hard coding the role name
            if(RoleLst.size()>0)
            {
                oUser.userroleid = RoleLst[0].id;
            }
           
            insert oUser ;
            //update oUser;
     }
    
     public static testMethod void ShareAllOLIOfAnAccountWithGRMTestMethod() 
     {
          
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
     
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
       
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
       
         Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,25,24000);
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,12,12000);
        RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,13,1,12000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
       
        User u2 =GEN_Util_Test_Data.CreateUser('ewrwe@testorg.com',p.Id,'test1@genpact.com' );
        User u3 =GEN_Util_Test_Data.CreateUser('werwe@testorg.com',p.Id,'test2@genpact.com' );
        User u4 =GEN_Util_Test_Data.CreateUser('tereerr@testorg.com',p.Id,'tereerr@genpact.com' );
        test.starttest();
        list<AccountArchetype__c> LstAccountArchetype = new list<AccountArchetype__c>();
        AccountArchetype__c arrch= new  AccountArchetype__c();
        arrch.Account__c = oAccount.id;
        arrch.User__c = u2.id;
        arrch.Archetype__c=oAT.id;
        arrch.Team_Member_Role__c = 'GRM';
        
        LstAccountArchetype.add(arrch);
        AccountArchetype__c arrch2= new  AccountArchetype__c();
        arrch2.Account__c = oAccount.id;
        arrch2.User__c = u4.id;
        arrch2.Archetype__c=oAT.id;
        arrch2.Team_Member_Role__c = 'GRM';
        
        LstAccountArchetype.add(arrch2);
        
        insert LstAccountArchetype;
     
       
           arrch.User__c = u3.id;
           update arrch;
          test.stoptest();
          
         delete  oOpportunityProduct;
     }
}