@isTest(seealldata=true)
public class olddata_splitupdate_test {
   /* @testsetup
    static void setup() {
        List<opportunity> opportunity = new List<opportunity>();
        List<OpportunityTeamMember> otm = new List<OpportunityTeamMember>();
        list<OpportunitySplit> osplit = new list<OpportunitySplit>();
        list<user> us=new list<user>();
        list<opportunitylineitem> olitest= new list<opportunitylineitem>();
        // insert 10 opps
        for (Integer i=0;i<10;i++) {
            opportunity.add(new opportunity(name='opp '+i,stagename='prediscover',amount=123456,accountid='00190000011PwSF',Sales_Region__c='africa',Sales_country__c='india',CloseDate=system.today(),Target_Source__c='Existing G Relationship',Opportunity_Source__c	
                                            ='Renewal'));
        }
     
        insert opportunity;
        boolean a;
        a=true;
        // find the account just inserted. add contact for each
        for (opportunity o : [select id from opportunity]) {
            otm.add(new OpportunityTeamMember(TeamMemberRole='SL',UserId='0059000000UPg8N',OpportunityId=o.id));
            otm.add(new OpportunityTeamMember(TeamMemberRole='Super SL',UserId='00590000001ICb5',OpportunityId=o.id));
            otm.add(new OpportunityTeamMember(TeamMemberRole='BDRep',UserId='00590000001GWwY',OpportunityId=o.id));
            osplit.add(new OpportunitySplit(OpportunityId = o.Id,SplitPercentage = 50,SplitTypeId = '14990000000fxXIAAY',SplitOwnerId = '0059000000UPg8N'));
        //    osplit.add(new OpportunitySplit(OpportunityId = o.Id,SplitPercentage = 30,SplitTypeId = '14990000000fxXIAAY',SplitOwnerId = '00590000001ICb5'));
        }
        system.debug('otm'+otm.size());
        insert otm;
        list<opportunitysplit> delsplit=new list<opportunitysplit>();
        delsplit=[select id from opportunitysplit where SplitOwnerId='0059000000UPg8N' or SplitOwnerId='00590000001ICb5' or SplitOwnerId='00590000001GWwY' ];
        system.debug('delsplit='+delsplit.size());
        delete delsplit;
        insert osplit;
    }
*/
    static testmethod void test() {        
        Test.startTest();
        olddata_splitupdate be = new olddata_splitupdate();
        Id batchJobId = Database.executeBatch(new olddatateamupdate(), 200);
        Id batchJobI = Database.executeBatch(new olddata_splitupdate(), 500);
        Id batchJoId = Database.executeBatch(new deletesplit(), 500);
        Test.stopTest();
    }
    
}