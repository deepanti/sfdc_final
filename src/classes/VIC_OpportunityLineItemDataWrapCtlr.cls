/*
    @Author: Vikas Rajput
    @Description: It will bind opportunity line item, Incentive and Target related data
    @Note: Add new variable to show another Component data
*/
public class VIC_OpportunityLineItemDataWrapCtlr{
    @AuraEnabled public Boolean isOLIChecked{get;set;}
    @AuraEnabled public OpportunityLineItem objOLI;
    @AuraEnabled public String strOLITCV;
    @AuraEnabled public String strUpfrontVIC;
    @AuraEnabled public String strITKicker;
    @AuraEnabled public String strVICPayable; //Sum of Incentive's bonous ammount whose Incentive's status is Supervisor-Pending
    @AuraEnabled public String strVICCredit;
    @AuraEnabled public String strApprovedIncentive;
    @AuraEnabled public String strNewLogoBonus;
    @AuraEnabled public String strRenewal;
    @AuraEnabled public String strComment;
    @AuraEnabled public Boolean isCommentRequired;
    @AuraEnabled public String strActionStatus;
    @AuraEnabled public List<Target_Achievement__c> lstUpfrontIncentive;
    @AuraEnabled public List<Target_Achievement__c> lstITKicker;
    @AuraEnabled public List<Target_Achievement__c> lstNewLogoBonus;
    @AuraEnabled public List<Target_Achievement__c> lstRenewal;
    @AuraEnabled public String strTCVBooked;
    @AuraEnabled public String strDiscretionary;
    @AuraEnabled public String strTempDiscretionary = '0';
    @AuraEnabled public Target_Achievement__c objAchievement;
    @AuraEnabled public Map<Id,VIC_OpportunityLineItemDataWrapCtlr> mapCaseIdToOLIWrap;
    @AuraEnabled public String strCaseId;
    @AuraEnabled public List<VIC_OpportunityLineItemDataWrapCtlr> lstCaseDataWrap;
    @AuraEnabled public String strDescription;
    @AuraEnabled public String strCaseNumber;
    @AuraEnabled public String strTotalVICPayable;
    @AuraEnabled public Boolean isAdjustmentExist;
    @AuraEnabled public String isITKickerAdjustExist;
    @AuraEnabled public String isUpfrontVICAdjustExist;
    @AuraEnabled public String isNewLogoAdjustExist;
    @AuraEnabled public String isRenewalAdjustExist;
    @AuraEnabled public String strTotalUpfrontInLocal = '0';
    @AuraEnabled public Boolean isPopoverVisible = false;
    @AuraEnabled public Decimal dcmPPP;
    @AuraEnabled public Decimal dcmExchangeRate;
    
    //@Constructor
    public VIC_OpportunityLineItemDataWrapCtlr(Boolean isOLIChecked,OpportunityLineItem objOLI,String strOLITCV,
            String strUpfrontVIC,String strITKicker,String strVICPayable,String strVICCredit,
                String strApprovedIncentive,String strNewLogoBonus,String strTCVBooked,String strRenewal,String strComment,
                Boolean isCommentRequired,String strActionStatus,String strDiscretionary,Target_Achievement__c objAchievement,
                List<Target_Achievement__c> lstUpfrontIncentive,List<Target_Achievement__c> lstITKicker,
                List<Target_Achievement__c> lstNewLogoBonus,List<Target_Achievement__c> lstRenewal, 
                Map<Id,VIC_OpportunityLineItemDataWrapCtlr> mapCaseIdToOLIWrap){
                
        this.isOLIChecked = isOLIChecked;
        this.objOLI = objOLI;
        this.strOLITCV = strOLITCV;
        this.strUpfrontVIC = strUpfrontVIC;
        this.strITKicker = strITKicker;
        this.strVICPayable = strVICPayable;
        this.strVICCredit = strVICCredit;
        this.strApprovedIncentive = strApprovedIncentive;
        this.strNewLogoBonus = strNewLogoBonus;
        this.strComment = strComment;
        this.isCommentRequired = isCommentRequired;
        this.strActionStatus = strActionStatus;
        this.lstUpfrontIncentive = lstUpfrontIncentive;
        this.lstITKicker = lstITKicker;
        this.lstNewLogoBonus = lstNewLogoBonus;
        this.lstRenewal = lstRenewal;
        this.strTCVBooked = strTCVBooked;
        this.strRenewal = strRenewal;
        this.strDiscretionary = strDiscretionary;
        this.objAchievement = objAchievement;
        this.mapCaseIdToOLIWrap = mapCaseIdToOLIWrap;
    }
}