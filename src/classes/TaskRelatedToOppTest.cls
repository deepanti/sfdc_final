@istest
public class TaskRelatedToOppTest {
    public static Opportunity oppty;
    public static Opportunity opp;
    public static Opportunity opp1;
    public static Lead L;
    public static Task oTask,oTask1,oTask2;
    public static Contact oContact;
    public static OpportunityContactRole Ocr;
    public static Note Nte;
    public static User u, u1, u3;
    public static OpportunityLineItem opp_item;
    public static OpportunityLineItem opp_item1;
    public   static List<opportunity> updateopplist;
    public static list<Task> tskList;
    public static QSRM__c qsrm;
    
    public static testMethod void setupTestData()
    {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
        L = new Lead(lastname='Test1_',firstname='Lead1',Title='TestLname', company='TestCo', email='testlead1@testco.com',
                     status='Engaged', CurrencyIsoCode='USD', Account__c=oAccount.Id);
        insert L;
        
        //System.runAs(u){
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
        Id RecordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pre-discover Opportunity').getRecordTypeId();
        
        opp1 =new opportunity(name='123428937892',StageName='Prediscover',CloseDate=system.today()+1, recordTypeId = RecordTypeId1,
                              Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id);
        insert opp1;
        
        
        opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                             Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,Amount=11000000,GCI_Coach__c='Greg Wilk',
                             Submit_for_SL_approval_on_GCI__c=true,Annuity_Project__c='Project',Margin__c=12,Summary_of_opportunity__c='TestSummary',Contract_type__c='SOW/LOE/Work Order',Number_of_Contract__c=1);
        
        CheckRecursive.run=true;
        insert opp;
        
        tskList = new list<task>();
        oTask = new Task(ringdna__Created_by_RingDNA__c=True,Whoid=oContact.id,Account_Name__c=oAccount.Id,Ownerid=u.Id,Industry_Vertical__c='Manufacturing',URL__c='www.test.com',Sub_Industry_Vertical__c='Commercial Banks',Status='Not Started',Priority='Normal');
        oTask1 = new Task(ringdna__Created_by_RingDNA__c=True,Whoid=l.id,Account_Name__c=oAccount.Id,Ownerid=u.Id,Industry_Vertical__c='Manufacturing',URL__c='www.test.com',Sub_Industry_Vertical__c='Commercial Banks',Status='Not Started',Priority='Normal'); 
        oTask2 = new Task(ringdna__Created_by_RingDNA__c=True,WhatId =opp.id,Account_Name__c=oAccount.Id,Ownerid=u.Id,Industry_Vertical__c='Manufacturing',URL__c='www.test.com',Sub_Industry_Vertical__c='Commercial Banks',Status='Not Started',Priority='Normal'); 
        tskList.add(oTask);
        tskList.add(oTask1);
        tskList.add(oTask2);
        insert tskList;
        
        oTask = GEN_Util_Test_Data.CreateTask(oAccount.Name,'Manufacturing','www.test.com','Commercial Banks','Not Started');
        
        
        Ocr = new OpportunityContactRole(ContactId= oContact.Id,OpportunityId=opp.Id,Role='Decision Maker',IsPrimary=true);   
        insert Ocr;
        
    }
    
    public static testMethod void TestMethod1(){
        setupTestData();
        test.starttest();
        
        L.firstname='Lead2';
        update L;
        
        oContact.FirstName='Test2';
        update oContact;
        
        Ocr.Role='Executive';
        update Ocr;
        
        oTask.Industry_Vertical__c='Healthcare';
        //oTask.ringdna__Created_by_RingDNA__c=True;
        update oTask;
        //system.assertEquals(L.Id, oTask.WhoId);
         test.stoptest();
    }  
}