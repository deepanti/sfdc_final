/**
 * @group Mass Update/Upload. 
 *
 * @description Class for Mass Upload/Update functionality.Inherits the Base Project Util Class.
 *              This class will be inherited by all Mass Upload/Update Batches.
 */
public virtual class GPBatchMassUpdateAndUploadHelper extends GPBaseProjectUtil {

    protected final String PROJECT_NOT_FOUND_FOR_ORACLE_PID_ERROR_LABEL = 'No Project found for Oracle PID specified';
    protected final String PROJECT_LOCKED_VALIDATION_LABEL = 'Project is Locked. Another Workflow Version in Progress';
    protected final String PROJECT_ACCESS_VALIDATION_LABEL = 'You are not authorised to update this project';

    protected Set < String > setOfNonConsiderateOracleIdsWrtToCurrentWorkingUser = new Set < String > ();    
    protected List < GP_Project__c > lstOfNewProjectCreated = new List < GP_Project__c > ();
    protected Set < String > setOfWorkLocationAndSDOOracleIds = new Set < String > ();
    protected Set < String > setOfProjectOracleIdsForBatch = new Set < String > ();
    protected Set < String > setOfNonConsiderateOracleIds = new Set < String > ();
    protected Set < String > setOfPinnacleMasterOracleIds = new Set < String > ();
    protected Set < String > setOfEmployeeMasterOracleIds = new Set < String > ();
    
    // Mass Update Change
    protected Set<String> setOfProducts = new Set<String>();
    //protected Set<String> setOfProductIds = new Set<String>();
    protected Set<String> setOfNatureOfWorks = new Set<String>();
    protected Set<String> setOfServiceLines = new Set<String>();
    protected Set<String> setOfAccountIds= new Set<String>();
    protected Set<String> setOfBusinessSegmentIds = new Set<String>();
    protected Set<String> setOfSubBusinessIds = new Set<String>();
    protected Set<String> setOfL1s = new Set<String>();
    
    protected List < Sobject > listOfTemporaryRecords = new List < Sobject > ();
    protected Set < String > setOfProjectOracleIds = new Set < String > ();
    protected Set < string > setOfOracleIds = new Set < String > ();
    protected Set < Id > setOfErrorIds = new Set < Id > ();

    protected Map < String, List < GP_Temporary_Data__c >> mapOfProjectOracleIdVsTemporaryRecordList = new Map < String, List < GP_Temporary_Data__c >>();
    protected Map < String, GP_Employee_Master__c > mapOfEmployeePersonIdVsEmployeeRecord = new Map < String, GP_Employee_Master__c > ();
    
    // Mass Update Change
    protected Map <String, List<GP_Product_Master__c>> mapOfProductMasters = new Map <String, List<GP_Product_Master__c>> ();
    protected Map <String, Account> mapOfAccounts = new Map <String, Account> ();
    
    protected Map < String, GP_Temporary_Data__c > mapOfTemporaryDataWithUniqueKey = new Map < String, GP_Temporary_Data__c > ();
    protected Map < String, Set < String >> mapOfProjectOracleIdVsUniqueKeyOfChild = new map < String, Set < String >> ();
    protected Map < String, GP_Temporary_Data__c > mapOfInvalidEntries = new Map < String, GP_Temporary_Data__c > ();
    protected Map < String, GP_Temporary_Data__c > mapOfTemporaryData = new Map < String, GP_Temporary_Data__c > ();
    protected Map < String, Set < Id >> mapOfProjectOracleIdVsTemporaryRecordsId = new map < String, Set < Id >> ();
    protected Map < String, List < String >> mapOfTemporaryIdVsValidation = new Map < String, List < String >> ();
    protected Map < String, Id > mapOfWorkLocationMasterOracleIdVsSFDCId = new Map < String, Id > ();
    protected Map < String, Id > mapOfPinnacleMasterOracleIdVsSFDCId = new Map < String, Id > ();
    protected Map < String, Boolean > mapToCheckValidRecords = new Map < String, Boolean > ();
    protected Map < String, Id > mapOfEmployeePersonIdVsSFDCId = new Map < String, Id > ();
    protected Map < Id, Id > mapOfOldProjectIdVsNewProjectId = new Map < Id, Id > ();


    /*protected Map<Id,List<GP_Project_Leadership__c>> mapOfProjectLeadership = new Map<Id,List<GP_Project_Leadership__c>>();
    protected Map<Id,List<GP_Project_Address__c>> mapOfProjectAdress = new Map<Id,List<GP_Project_Address__c>>();
    protected Map<Id,List<GP_Project_Work_Location_SDO__c>> mapOfProjectWorkLocation = new Map<Id,List<GP_Project_Work_Location_SDO__c>>();
    protected Map<Id,List<GP_Resource_Allocation__c>> mapOfResourceAllocation = new Map<Id,List<GP_Resource_Allocation__c>>();
    protected Map<Id,List<GP_Billing_Milestone__c>> mapOfBillingMilestone = new Map<Id,List<GP_Billing_Milestone__c>>();
    protected Map<Id,List<GP_Project_Budget__c>> mapOfProjectBudget = new Map<Id,List<GP_Project_Budget__c>>();
    protected Map<Id,List<GP_Project_Expense__c>> mapOfProjectExpense = new Map<Id,List<GP_Project_Expense__c>>();
    protected Map<Id,List<GP_Project_Version_History__c>> mapOfProjectVersionHistory = new Map<Id,List<GP_Project_Version_History__c>>();
    protected Map<Id,List<GP_Project_Document__c>> mapOfProjectDocument = new Map<Id,List<GP_Project_Document__c>>();
    protected Map<Id,List<GP_Profile_Bill_Rate__c>> mapOfProjectProfileBillRate = new Map<Id,List<GP_Profile_Bill_Rate__c>>();*/


    protected GPWrapperApprovalValidatorConfig projectApprovalValidatorConfig = new GPWrapperApprovalValidatorConfig(false);
    protected List < GP_Temporary_Data__c > lstOfValidatedTemporaryData = new List < GP_Temporary_Data__c > ();
    protected List < GP_Project__c > listOfValidatedProjectRecords = new List < GP_Project__c > ();
    protected List < GP_Project__c > listOfProjectRecords;

    protected GPProjectValidatorConfig validationConfig = new GPProjectValidatorConfig(false);
    protected GPServiceProjectClone cloneProjectService = new GPServiceProjectClone();

    protected GP_Job__c jobRecord;

    protected String projectQuery, jobType, jobId, systemThrownExceptionValue;

    protected Database.SaveResult[] saveResultList;
    

    /**
     * @description Create set for Project Oracle Ids from Temporary Records.
     * @param listOfTemporaryRecords : List Of temporary records that are process in batch.
     * 
     */
    protected void setOracleIdSet(List < GP_Temporary_Data__c > listOfTemporaryRecords) {
        for (GP_Temporary_Data__c objTemporaryData: listOfTemporaryRecords) {
            setOfProjectOracleIds.add(objTemporaryData.GP_Project__c);
        }
    }

    /**
     * @description Query Project Records using set of Oracle PID's.
     * 
     */
    protected void setListOfProjectRecords() {
        listOfProjectRecords = new GPSelectorProject().getProjectListForOracleId(setOfProjectOracleIds);
    }

    /**
     * @description Filter the PID's which can't be processed by Mass Update/Upload depening upon their Appproval Status and OMS status.
     * 
     */
    protected void setStatusWiseProjectSet() {
        for (GP_Project__c objProject: listOfProjectRecords) {
            //if (objProject.GP_Current_Working_User__c != UserInfo.getUserId()) 
            {
              //  setOfNonConsiderateOracleIdsWrtToCurrentWorkingUser.add(objProject.GP_Oracle_PID__c);
            } if ((objProject.GP_Approval_Status__c != 'Approved' || objProject.GP_Oracle_Status__c != 'S') || (objProject.RecordType.Name == 'CMITS' && objProject.GP_OMS_Status__c != 'Approved' && (objProject.GP_Approval_Status__c != 'Approved' || objProject.GP_Oracle_Status__c != 'S'))) {
                setOfNonConsiderateOracleIds.add(objProject.GP_Oracle_PID__c);
            }
        }
    }

    /**
     * @description Create map of Project Oracle Ids Vs its status of processing for mass upload and update.
     * 
     */
    protected void setMapToCheckValidRecordsForProjectStatus(List < GP_Temporary_Data__c > listOfTemporaryRecords) {
        for (GP_Temporary_Data__c objTemporaryData: listOfTemporaryRecords) {
            mapToCheckValidRecords.put(objTemporaryData.GP_Project__c, isValidForProjectStatus(objTemporaryData.GP_Project__c));
        }
    }

    /**
     * @description Return true/false for considering the Project Oracle PID for processing.
     * @param oracleId : Project Oracle PID.
     * 
     */
    private Boolean isValidForProjectStatus(String oracleId) {
        return !(setOfNonConsiderateOracleIds.contains(oracleId) || setOfNonConsiderateOracleIdsWrtToCurrentWorkingUser.contains(oracleId));
    }

    /**
     * @description Filter the list of temporary data depending upon the project's approval status.
     * @param listOfTemporaryRecords : List Of temporary records that are process in batch.
     * 
     */
    protected void setLstOfValidatedTemporaryData(List < GP_Temporary_Data__c > listOfTemporaryRecords) {
        for (GP_Temporary_Data__c objTemporaryData: listOfTemporaryRecords) {
            // To Update Project Id to Temporary Record.
            if(!mapOfProjectOracleIdVsTemporaryRecordList.containsKey(objTemporaryData.GP_Project__c)) {
              mapOfProjectOracleIdVsTemporaryRecordList.put(objTemporaryData.GP_Project__c, new List < GP_Temporary_Data__c >());  
            }
            mapOfProjectOracleIdVsTemporaryRecordList.get(objTemporaryData.GP_Project__c).add(objTemporaryData);
            
            //shifted as some fail but other pass the scenarios.
            mapOfTemporaryData.put(objTemporaryData.Id, objTemporaryData);
            mapOfTemporaryDataWithUniqueKey.put(getUniqueKey(objTemporaryData), objTemporaryData);

            if (!mapOfProjectOracleIdVsTemporaryRecordsId.containsKey(objTemporaryData.GP_Project__c)) {
                mapOfProjectOracleIdVsTemporaryRecordsId.put(objTemporaryData.GP_Project__c, new Set < Id > {});
                mapOfProjectOracleIdVsUniqueKeyOfChild.put(objTemporaryData.GP_Project__c, new Set < String > {});
            }

            mapOfProjectOracleIdVsTemporaryRecordsId.get(objTemporaryData.GP_Project__c).add(objTemporaryData.Id);
            mapOfProjectOracleIdVsUniqueKeyOfChild.put(objTemporaryData.GP_Project__c, new Set < String > { getUniqueKey(objTemporaryData) });

            if (mapToCheckValidRecords.containsKey(objTemporaryData.GP_Project__c) &&
                mapToCheckValidRecords.get(objTemporaryData.GP_Project__c)) {
                lstOfValidatedTemporaryData.add(objTemporaryData);
                setOfOracleIds.add(objTemporaryData.GP_Project__c);
                setLookupRecordSet(objTemporaryData);
            } else {
                objTemporaryData.GP_Validation_Result__c = setOfNonConsiderateOracleIds.contains(objTemporaryData.GP_Project__c) ? PROJECT_LOCKED_VALIDATION_LABEL : PROJECT_ACCESS_VALIDATION_LABEL;
                objTemporaryData.GP_Is_Failed__c = true;
                mapOfInvalidEntries.put(objTemporaryData.Id, objTemporaryData);
            }
        }
    }

    /*public virtual void setProjectQuery(){
        projectQuery = GPServiceProjectClone.setCloneProjectQuery(setOfOracleIds);
    }
    
    public virtual void setlistOfProjectRecordsWithChildQueried(){
        projectQuery += ' from GP_Project__c where GP_Oracle_PID__c IN :setOfOracleIds';
        listOfProjectRecords = Database.query(projectQuery);  
    }
    
    public virtual void setCommitedProjectAndChildRecords(){
        for(GP_Project__c objProject : listOfProjectRecords){
            setProjectWorkLocation(objProject);
            setProjectLeadership(objProject);
            setResourceAllocation(objProject);
            setBillingMilestone(objProject);
            setProjectBudget(objProject);
            setProjectExpense(objProject);
        }
    }
    
    private virtual void setProjectWorkLocation(GP_Project__c objProject) {
        if(objProject.GP_Project_Work_Location_SDOs__r.size() > 0)
        {
            List<GP_Project_Work_Location_SDO__c> lstOfWorkLocations = (List<GP_Project_Work_Location_SDO__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                                             objProject.GP_Project_Work_Location_SDOs__r);
            mapOfProjectWorkLocation.put(objProject.Id,lstOfWorkLocations); 
        }
    }
    
    private virtual void setProjectLeadership(GP_Project__c objProject) {
        if(objProject.Project_Leaderships__r.size() > 0){
            List<GP_Project_Leadership__c> lstOfProjectLeaderShip = (List<GP_Project_Leadership__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                                   objProject.Project_Leaderships__r);
            
            mapOfProjectLeadership.put(objProject.Id,lstOfProjectLeaderShip); 
        }
    }
    
    private virtual void setResourceAllocation(GP_Project__c objProject) {
        if(objProject.GP_Resource_Allocations__r.size() > 0){
            List<GP_Resource_Allocation__c> lstOfProjectResourceAllocation = (List<GP_Resource_Allocation__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                                             objProject.GP_Resource_Allocations__r);
            
            mapOfResourceAllocation.put(objProject.Id,lstOfProjectResourceAllocation); 
        }
    }
    
    private virtual void setBillingMilestone(GP_Project__c objProject) {
        if(objProject.GP_Billing_Milestones__r.size() > 0){
            List<GP_Billing_Milestone__c> lstOfBillingMilestone = (List<GP_Billing_Milestone__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                                objProject.GP_Billing_Milestones__r);
            
            mapOfBillingMilestone.put(objProject.Id,lstOfBillingMilestone);
        }
    }
    
    private virtual void setProjectBudget(GP_Project__c objProject) {
        if(objProject.GP_Project_Budgets__r.size() > 0){
            List<GP_Project_Budget__c> lstOfProjectBudget = (List<GP_Project_Budget__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                       objProject.GP_Project_Budgets__r);
            
            mapOfProjectBudget.put(objProject.Id,lstOfProjectBudget); 
        }
    }
    
    private virtual void setProjectExpense(GP_Project__c objProject) {
        if(objProject.GP_Project_Expenses__r.size() > 0){
            List<GP_Project_Expense__c> lstOfProjectExpense = (List<GP_Project_Expense__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                          objProject.GP_Project_Expenses__r);
            
            mapOfProjectExpense.put(objProject.Id,lstOfProjectExpense); 
        }
    }
    
    private virtual void setProjectDocument(GP_Project__c objProject) {
        if(objProject.GP_Project_Documents__r.size() > 0){
            List<GP_Project_Document__c> lstOfProjectDocument = (List<GP_Project_Document__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                             objProject.GP_Project_Documents__r);
            
            mapOfProjectDocument.put(objProject.Id,lstOfProjectDocument); 
        }
    }
    
    private virtual void setProjectProfileBillRate(GP_Project__c objProject) {
        if(objProject.Profile_Bill_Rates__r.size() > 0){
            List<GP_Profile_Bill_Rate__c> lstOfProfileBillRate = (List<GP_Profile_Bill_Rate__c>)setTemporaryId(objProject.GP_Last_Temporary_Record_Id__c , 
                                                                                                               objProject.Profile_Bill_Rates__r);
            
            mapOfProjectProfileBillRate.put(objProject.Id,lstOfProfileBillRate);
        }
    }
    
    private virtual void setProjectVersionHistory(GP_Project__c objProject) {
        if(objProject.Project_Version_History__r.size() > 0)
            mapOfProjectVersionHistory.put(objProject.Id,objProject.Project_Version_History__r); 
    }
    */
    /**
     * @description Clone Project and Validate depending upon the Validation Configuration.
     * 
     */
    protected void cloneAndValidateProject() {
        cloneProjectService = cloneProjectService.setProjectApprovalValidatorConfig(projectApprovalValidatorConfig);
        mapOfTemporaryIdVsValidation = cloneProjectService.cloneProject(lstProject, 'Job Batch',
            validationConfig.validateProject, false);
    }

    /**
     * @description Clone Project Child Records and Validate depending upon the Validation Configuration.
     * 
     */
    protected void cloneAndValidateProjectChildRecords() {
        cloneProjectService = cloneProjectService.setMapOfOldProjectIdVsNewProjectId(mapOfOldProjectIdVsNewProjectId);
		
        try {
            for (GP_Project__c objInsertedProject: listOfValidatedProjectRecords) {
                GPDomainProject.skipVersionHistoryCreation = true;
                
                cloneProjectService.relatedProjectOracleId = objInsertedProject.GP_Oracle_PID__c;
                cloneProjectService = cloneProjectService.setRelatedProjectRecord(objInsertedProject);
                
                cloneProjectService.cloneProjectExpense(mapOfProjectExpense.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateProjectExpense, false);
                
                cloneProjectService.cloneProjectDocument(mapOfProjectDocument.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateDocumentUpload, false);
                
                cloneProjectService.cloneProjectBillRate(mapOfProjectProfileBillRate.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateProfileBillRate, false);
                
                cloneProjectService.cloneProjectBudget(mapOfProjectBudget.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateProjectBudget, false);
                
                cloneProjectService.cloneProjectBillingMileStone(mapOfBillingMilestone.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateBillingMilestone, false);
                
                cloneProjectService.cloneProjectWorkLocationAndSDO(mapOfProjectWorkLocation.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateWorkLocationAndSDO, false);
                
                cloneProjectService.cloneProjectResourceAllocations(mapOfResourceAllocation.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateResourceAllocation, false);
                
                cloneProjectService.cloneProjectLeadership(mapOfProjectLeadership.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateProjectLeadership, false);
                
                if (mapOfProjectVersionHistory.containsKey(objInsertedProject.GP_Parent_Project__c)) {
                    cloneProjectService.cloneProjectVersionHistory((mapOfProjectVersionHistory.get(objInsertedProject.GP_Parent_Project__c))[0],
                                                                   mapOfProjectVersionHistory.get(objInsertedProject.GP_Parent_Project__c).size());
                }
                
                GPDomainProject.skipVersionHistoryCreation = false;
                
                cloneProjectService.cloneProjectAddress(mapOfProjectAdress.get(objInsertedProject.GP_Parent_Project__c), validationConfig.validateProjectAddress, false);
                
                objInsertedProject.GP_Approval_Status__c = 'Approved';
                lstOfNewProjectCreated.add(objInsertedProject);
            }
        } catch(GPServiceProjectClone.GPServiceProjectCloneException cloneException) {
            // Mass Update Change
            System.debug('==Exception on 308=='+cloneException);
            if(!Test.isRunningTest())
            	throw cloneException;
        }
    }

    /**
     * @description Create map Of Parent Project Id Vs Child Project Id.
     * 
     */
    protected void setMapOfOldProjectIdVsNewProjectId() {
        for (GP_Project__c objInsertedProject: listOfValidatedProjectRecords) {
            mapOfOldProjectIdVsNewProjectId.put(objInsertedProject.GP_Parent_Project__c, objInsertedProject.Id);
        }
    }

    /**
     * @description Concatenate list of strings to a single string.
     * @param lstOfStrings : list of strings which need to be concatenated.
     *
     * @return String : Concatenated String.
     */
    private String joinStrings(List < string > lstOfStrings) {
        String concatenatedString = '';

        for (String str: lstOfStrings) {
            concatenatedString += str + ';';
        }

        return concatenatedString;
    }

    /**
     * @description Update Job Record and the temporary records with validation result.
     * 
     */
    /*protected void updateJobRecord() {
        
        
        GP_Job__c jobRecord = new GP_Job__c(Id = jobId);
        jobRecord.GP_Status__c = mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0 ? 'Failed' : 'Completed';
        jobRecord.GP_No_of_Unsuccessful_Records__c = mapOfInvalidEntries.values().size();
        update jobRecord;
    }
    */
    /**
     * @description Update Job Record with Exception Message.
     * @param exceptionString : Error String to be updated on job record.
     * 
     */
    /*protected void updateJobRecord(String exceptionString) {
        GP_Job__c jobRecord = new GP_Job__c(Id = jobId);
        jobRecord.GP_Error_Description__c = exceptionString;
        jobRecord.GP_Status__c = 'Failed';
        update jobRecord;
    }*/

    /**
     * @description Update Validation Result of record in Invalid Entries map.
     * @param exceptionString : Error String to be updated on Validation result field.
     * @param recordId : temporary record on which validation string needs to be appended.
     * 
     */
    protected void setExceptionMessageToRelatedRecord(String recordId, String exceptionString) {
        if (mapOfInvalidEntries.containsKey(recordId)) {
            mapOfInvalidEntries.get(recordId).GP_Validation_Result__c = exceptionString + ';';
            mapOfInvalidEntries.get(recordId).GP_Is_Failed__c = true;
        } else {
            GP_Temporary_Data__c objTemporaryData = mapOfTemporaryData.get(recordId);
            objTemporaryData.GP_Validation_Result__c = exceptionString;
            objTemporaryData.GP_Is_Failed__c = true;
            mapOfInvalidEntries.put(recordId, objTemporaryData);
        } 
    }

    /**
     * @description Update Validation Result of record in Invalid Entries map.
     * @param mapOfValidations : Validation Map returned from Service Class.
     * 
     */
    protected void setExceptionMessageToRelatedRecord(Map < String, List < String >> mapOfValidations) {
        for (String tempId: mapOfValidations.KeySet()) {
            if (mapOfInvalidEntries.containsKey(tempId)) {
                mapOfInvalidEntries.get(tempId).GP_Validation_Result__c = ';' + mapOfValidations.get(tempId);
                mapOfInvalidEntries.get(tempId).GP_Is_Failed__c = true;
            } else {
                GP_Temporary_Data__c objTemporaryData = mapOfTemporaryData.get(tempId);
                objTemporaryData.GP_Validation_Result__c = ' ' + mapOfValidations.get(tempId);
                objTemporaryData.GP_Is_Failed__c = true;
                mapOfInvalidEntries.put(tempId, objTemporaryData);
            }
        }
    }

    /**
     * @description Filter the Project Records which have passed the validations.
     * 
     */
    protected void setListOfValidatedProjectRecords() {
        for (GP_Project__c objProject: lstProject) {
            if (!mapOfTemporaryIdVsValidation.containsKey(objProject.GP_Last_Temporary_Record_Id__c))
                listOfValidatedProjectRecords.add(objProject);
            else {
                GP_Temporary_Data__c objTemporaryData = mapOfTemporaryData.get(objProject.GP_Last_Temporary_Record_Id__c);
                objTemporaryData.GP_Validation_Result__c = joinStrings(mapOfTemporaryIdVsValidation.get(objProject.GP_Last_Temporary_Record_Id__c));
                objTemporaryData.GP_Is_Failed__c = true;
                mapOfInvalidEntries.put(objProject.GP_Last_Temporary_Record_Id__c, objTemporaryData);
            }
        }
    }

    /**
     * @description Update Job Record with 'Pending' Status.
     * 
     */
    /*public virtual void updateJobStatusToPending() {
        GP_Job__c objJob = new GP_Job__c(id = jobId);
        objJob.GP_Status__c = 'Pending';
        upsert objJob;
    }*/

    /**
     * @description Return Unique key in temorary Data depending on the job Type.
     * @param objTemporaryData : Temporary Data record.
     * 
     * @return String : Unique string depending upon job type.
     */
    private String getUniqueKey(GP_Temporary_Data__c objTemporaryData) {
        if (jobType == 'Update Project')
            return objTemporaryData.GP_Project__c;
        else if (jobType == 'Update Project LeaderShip')
            return objTemporaryData.GP_Project__c + (objTemporaryData.GP_PL_Leadership_Role__c).toLowerCase();
        else if (jobType == 'Update Project WorkLocation')
            return objTemporaryData.GP_Project__c + (objTemporaryData.GP_PWL_Work_Location__c);
        return null;
    }

    /**
     * @description Return List of SObjects from list of list of SObjects.
     * @param compositeList : composite list which needs to be converted to a single list.
     * 
     * @return List<Sobject> : Single List from composite List.
     */
    protected List < Sobject > convertlistOfListToAList(List < List < SObject >> compositeList) {
        List < Sobject > commonList = new List < Sobject > ();

        for (List < Sobject > lstOfSobjects: compositeList) {
            for (Sobject sobj: lstOfSobjects)
                commonList.add(sobj);
        }

        return commonList;
    }

    /**
     * @description Return List of SObjects with temporary Id added.
     * @param lstOfSobject : list of records in which temporary id needs to be added.
     * @param TemporaryId : temporary Record Id that is to be set on the list.
     * 
     * @return List<Sobject> : Temporary Id appended List od records.
     */
    private List < Sobject > setTemporaryId(Id TemporaryId, List < Sobject > lstOfSobject) {
        for (Sobject sobj: lstOfSobject) {
            sobj.put('GP_Last_Temporary_Record_Id__c', TemporaryId);
        }
        return lstOfSobject;
    }

    /**
     * @description Append temporary RecordId to project Record.
     * 
     */
    protected void putTemporaryIdToProjectRecord() {
        for (GP_Project__c objProject: lstProject) {
            objProject.GP_Last_Temporary_Record_Id__c = (new list < Id > (mapOfProjectOracleIdVsTemporaryRecordsId.get(objProject.GP_Oracle_PID__c)))[0];
        }
    }

    /**
     * @description Create set for fields in which we have oracle id in temporary records.
     * @param objTemporaryData : Temporaray Record whose value will be used.
     * 
     */
    private void setLookupRecordSet(GP_Temporary_Data__c objTemporaryData) {
        if (jobType == 'Update Project')
            setLookupRecordSetForProject(objTemporaryData);
        else if (jobType == 'Update Project LeaderShip')
            setLookupRecordSetForLeaderShip(objTemporaryData);
        else if (jobType == 'Update Project WorkLocation')
            setLookupRecordSetForProjectWorkLocation(objTemporaryData);
    }

    /**
     * @description Create set for Leadership fields in which we have oracle id in temporary records.
     * @param objTemporaryData : Temporaray Record whose value will be used.
     * 
     */
    private void setLookupRecordSetForLeaderShip(GP_Temporary_Data__c objTemporaryData) {
        if (objTemporaryData.GP_PL_Employee_OHR__c != null)
            setOfEmployeeMasterOracleIds.add(objTemporaryData.GP_PL_Employee_OHR__c);
    }

    /**
     * @description Create set for Project fields in which we have oracle id in temporary records.
     * @param objTemporaryData : Temporaray Record whose value will be used.
     * 
     */
    private void setLookupRecordSetForProject(GP_Temporary_Data__c objTemporaryData) {
        if (objTemporaryData.GP_PRJ_HSL__c != null)
            setOfPinnacleMasterOracleIds.add(objTemporaryData.GP_PRJ_HSL__c);
        if (objTemporaryData.GP_PRJ_Primary_SDO__c != null)
            setOfWorkLocationAndSDOOracleIds.add(objTemporaryData.GP_PRJ_Primary_SDO__c);
        if (objTemporaryData.GP_PRJ_SILO__c != null)
            setOfPinnacleMasterOracleIds.add(objTemporaryData.GP_PRJ_SILO__c);
        if (objTemporaryData.GP_PRJ_Portal_Name__c != null)
            setOfPinnacleMasterOracleIds.add(objTemporaryData.GP_PRJ_Portal_Name__c);
        // Mass Update Change : 05-12-19.
        /*if (objTemporaryData.GP_PRJ_Portal_Name__c != null)
            setOfPinnacleMasterOracleIds.add(objTemporaryData.GP_PRJ_Portal_Name__c);*/
        
        // Mass Update Change
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Product__c))
            setOfProducts.add(objTemporaryData.GP_PRJ_Product__c);
        /*if(String.isNotBlank(objTemporaryData.GP_PRJ_Product_Id__c))
            setOfProductIds.add(objTemporaryData.GP_PRJ_Product_Id__c);*/
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Nature_of_Work__c))
            setOfNatureOfWorks.add(objTemporaryData.GP_PRJ_Nature_of_Work__c);        
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Service_Line__c)) 
            setOfServiceLines.add(objTemporaryData.GP_PRJ_Service_Line__c);        
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Business_Hierarchy_L4__c))
            setOfAccountIds.add(objTemporaryData.GP_PRJ_Business_Hierarchy_L4__c);        
        /*if(String.isNotBlank(objTemporaryData.GP_PRJ_Business_Hierarchy_L3__c))
            setOfSubBusinessIds.add(objTemporaryData.GP_PRJ_Business_Hierarchy_L3__c);        
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Business_Hierarchy_L2__c))
            setOfBusinessSegmentIds.add(objTemporaryData.GP_PRJ_Business_Hierarchy_L2__c);        
        if(String.isNotBlank(objTemporaryData.GP_PRJ_Business_Hierarchy__c))
            setOfL1s.add(objTemporaryData.GP_PRJ_Business_Hierarchy__c);*/
    }

    /**
     * @description Create set for Project Worklocation fields in which we have oracle id in temporary records.
     * @param objTemporaryData : Temporaray Record whose value will be used.
     * 
     */
    private void setLookupRecordSetForProjectWorkLocation(GP_Temporary_Data__c objTemporaryData) {
        if (objTemporaryData.GP_PWL_Work_Location__c != null)
            setOfWorkLocationAndSDOOracleIds.add(objTemporaryData.GP_PWL_Work_Location__c);
    }

    /**
     * @description Create map of all masters using their oracle PID's.
     * 
     */
    protected void setMasterDataMap() {
        if (setOfPinnacleMasterOracleIds.size() > 0) {
            for (GP_Pinnacle_Master__c objPinnacleMaster: GPSelectorPinnacleMasters.selectRecordsFromOracleIdSet(setOfPinnacleMasterOracleIds)) {
                mapOfPinnacleMasterOracleIdVsSFDCId.put(objPinnacleMaster.GP_Oracle_Id__c, objPinnacleMaster.Id);
            }
        }

        if (setOfWorkLocationAndSDOOracleIds.size() > 0) {
            for (GP_Work_Location__c objWorkLocation: new GPSelectorWorkLocationSDOMaster().selectByOracleIdofWOrkLocationAndSDO(setOfWorkLocationAndSDOOracleIds)) {
                mapOfWorkLocationMasterOracleIdVsSFDCId.put(objWorkLocation.GP_Oracle_Id__c, objWorkLocation.Id);
            }
        }

        if (setOfEmployeeMasterOracleIds.size() > 0) {
            for (GP_Employee_Master__c objEmployeeMaster: new GPSelectorEmployeeMaster().getEmployeeListForOHRId(setOfEmployeeMasterOracleIds)) {
                mapOfEmployeePersonIdVsSFDCId.put(objEmployeeMaster.GP_Final_OHR__c, objEmployeeMaster.Id);
                mapOfEmployeePersonIdVsEmployeeRecord.put(objEmployeeMaster.GP_Final_OHR__c, objEmployeeMaster);
            }
        }
        
        // Mass Update Change
		// Product Hierarchy
        if(setOfProducts.size() > 0 || /*setOfProductIds.size() > 0 ||*/ setOfNatureOfWorks.size() > 0 || setOfServiceLines.size() > 0) {
            for(GP_Product_Master__c pm : new GPSelectorProductMaster().getProductMasterDataForMassUpdate(setOfProducts, setOfNatureOfWorks, setOfServiceLines)) {
                String key = '';
				                
                if(String.isNotBlank(pm.GP_Product_Name__c)) {
					key += pm.GP_Product_Name__c + '_';
                }
                /*if(String.isNotBlank(pm.GP_Product_ID__c)) {
					key += pm.GP_Product_ID__c + '_';
                }*/
                if(String.isNotBlank(pm.GP_Service_Line__c)) {
					key += pm.GP_Service_Line__c + '_';
                }
                if(String.isNotBlank(pm.GP_Nature_of_Work__c)) {
					key += pm.GP_Nature_of_Work__c + '_';
                }
                key = key.removeEnd('_');
                
                if(!mapOfProductMasters.containsKey(key)) {
                    mapOfProductMasters.put(key, new List<GP_Product_Master__c>());
                }                
                mapOfProductMasters.get(key).add(pm);
            }
        }
        
        // Business Hierarchy
        if(setOfAccountIds.size() > 0) { // || setOfBusinessSegmentIds.size() > 0 || setOfSubBusinessIds.size() > 0 || setOfL1s.size() > 0) {
            //for(Account acc : new GPSelectorAccount().getAccountDetailsForMassUpdate(setOfAccountIds, setOfBusinessSegmentIds, setOfSubBusinessIds, setOfL1s)) {
            for(Account acc : new GPSelectorAccount().getAccountDetailsForMassUpdate(setOfAccountIds)) {
                /*String key = '';
                
                if(String.isNotBlank(acc.Business_Group__c)) {
                    key += acc.Business_Group__c + '_';
                }                
                if(String.isNotBlank(acc.Business_Segment__c)) {
                    key += acc.Business_Segment__c + '_';
                }                
                if(String.isNotBlank(acc.Sub_Business__c)) {
                    key += acc.Sub_Business__c + '_';
                }                
                if(String.isNotBlank(acc.Id)) {
                    key += acc.Id + '_';
                }
                
                key = key.removeEnd('_');
                
                if(!mapOfAccounts.containsKey(key)) {
                    mapOfAccounts.put(key, new List<Account>());
                }
                
                mapOfAccounts.get(key).add(acc);*/
                
                mapOfAccounts.put(acc.Id, acc);
            }
        }
    }

    /**
     * @description Update Job Record.
     * @param status : Job Status.
     * @param noOfUnsuccessfulRecords : Failed Records count.
     * @param exceptionString : Gloabl error in Job.
     * 
     */
    protected void updateJobRecord(String status, Integer noOfUnsuccessfulRecords, String exceptionString) {
        if (mapOfInvalidEntries.size() > 0)
            upsert mapOfInvalidEntries.values();

        jobRecord = new GP_Job__c(Id = jobId);
        jobRecord.GP_Status__c = status;
        jobRecord.GP_No_of_Unsuccessful_Records__c = noOfUnsuccessfulRecords;
        jobRecord.GP_Error_Description__c = exceptionString;
        if (status == 'In Progress')
            jobRecord.GP_Job_Process_Started_On__c = system.now();
        else if (status == 'Completed' || status == 'Failed')
            jobRecord.GP_Job_Process_Completed_On__c = system.now();
        update jobRecord;
    }
    
    
    
    /**
     * @description Update Job LineItems Records.
     * 
     */
    protected void updateJobLineItemRecord() {
        if(lstOfNewProjectCreated != null && lstOfNewProjectCreated.size() > 0){
            List < GP_Temporary_Data__c > lstOfSuccesfulTemporaryRecords = new List < GP_Temporary_Data__c > ();
            for(GP_Project__c projectRecord : lstOfNewProjectCreated){
                if(mapOfProjectOracleIdVsTemporaryRecordList.containsKey(projectRecord.GP_Oracle_PID__c)){
                    for(GP_Temporary_Data__c tempRecord : mapOfProjectOracleIdVsTemporaryRecordList.get(projectRecord.GP_Oracle_PID__c)){
                        tempRecord.GP_PID__c = projectRecord.Id;
                        lstOfSuccesfulTemporaryRecords.add(tempRecord); 
                    }
                }
            }
            system.debug('lstOfSuccesfulTemporaryRecords'+lstOfSuccesfulTemporaryRecords);
            if(lstOfSuccesfulTemporaryRecords.size() > 0)
                update lstOfSuccesfulTemporaryRecords;
        }
        
    }

    protected void setUpdatedValueDependingOnColumnSelected(String columnNames, SObject sourceObject, String sourceFieldName, SObject destinationObject, String destinationFieldName) {
        if (columnNames.contains(sourceFieldName)) {
            destinationObject.put(destinationFieldName, sourceObject.get(sourceFieldName));
        }
    }

    protected void setUpdatedValueDependingOnColumnSelectedFromMasters(String columnNames, SObject sourceObject, String sourceFieldName, SObject destinationObject, String destinationFieldName, Map < String, Id > mapOfMaster) {
        if (columnNames.contains(sourceFieldName) && mapOfMaster.containsKey(String.ValueOf(sourceObject.get(sourceFieldName)))) {
            destinationObject.put(destinationFieldName, mapOfMaster.get(String.ValueOf(sourceObject.get(sourceFieldName))));
        }
    }

    protected void getJobRecord(String jobId) {
        jobRecord = new GPSelectorJob().SelectJobRecord(jobId);
    }

    protected String parseSaveResultChildRecords(List < Sobject > lstOfObjectToBeInserted, GPServiceProjectClone.stringWrapper temporaryRecordId) {
        String errorMessage;
        Integer count = 0;

        for (Database.SaveResult sr: saveResultList) {
            if (!sr.isSuccess()) {
                for (Database.Error err: sr.getErrors()) {
                    errorMessage = err.getMessage() + ';';
                }
                temporaryRecordId.str = String.ValueOf(lstOfObjectToBeInserted[count].get('GP_Last_Temporary_Record_Id__c'));
                return errorMessage;
            }
            count++;
        }
        return null;
    }    
 }