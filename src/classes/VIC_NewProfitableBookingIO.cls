/*
* @author: Prashant Kumar1
* @since: 23/05/2018
* @description: This calculation returns the insentive for plans
* associated with NewProfitableBookingIO plan component
* 
*/
public class VIC_NewProfitableBookingIO implements VIC_CalculationInterface {
    
    @TestVisible
    private Boolean isAnniversaryCalculation;
    
    
    public VIC_NewProfitableBookingIO(Boolean isAnniversaryCalculation){
        this.isAnniversaryCalculation = isAnniversaryCalculation;
    }
    
    /*
    * @params: OpportunityProduct, planCode
    * @return: Incentive for New Profitable Booking IO component for 
    * BDE, Capital_Market_GRM and EnterpriseGRM plan codes. Developer need to make specific implementation 
    * while calling for anniversary calculation of BDE plan. Actual Incentive in this case will:
    * returned_value - Incentive_given_at_POS (if greater than zero, otherwise 0)
    *
    */
    public Decimal calculate(OpportunityLineItem oppProd, String planCode){
        System.debug('====>>New Profitable booking IO 1 , planCode: '+ planCode+ ', oppProd.VIC_NPV__c: '+ oppProd.VIC_NPV__c);
        Decimal incentive = 0;
        if(oppProd != null && planCode != null && oppProd.VIC_NPV__c != null){
            System.debug('====>>New Profitable booking IO 2');
            if(oppProd.vic_IO_TS__c == 'IO'){
                if(/*oppProd.Pricing_Deal_Type_OLI_CPQ__c != null && */ oppProd.Opportunity.Actual_Close_Date__c != null
                   && oppProd.VIC_NPV__c != null){
                       System.debug('====>>New Profitable booking IO 3');
                       if(planCode == 'BDE'){
                           System.debug('====>>New Profitable booking IO 4 BDE');
                           incentive = calculateBDE(oppProd);
                       }
                       else if(planCode == 'Capital_Market_GRM'){
                           System.debug('====>>New Profitable booking IO 5 BDE');
                           incentive = calculateCapitalMarketGRM(oppProd);
                       }
                       else if(planCode == 'EnterpriseGRM'){
                           System.debug('====>>New Profitable booking IO 6 BDE');
                           incentive = calculateEnterpriseGRM(oppProd);
                       }
                   }
                
            }
        }
        else{
            System.debug('Any one from Opportunity product, plan code and NPV(VIC_NPV__c) is null,'
                         + ' so New Profitable Booking IO component can not be calculated!');
        }
        return incentive;
    }
    /*
    * @params: opportunityProduct
    * @return: Incentive(Decimal)
    * @description: This method returns Incentive for new profitable booking IO component for BDE plan.
    * 
    */
    @TestVisible
    private Decimal calculateBDE(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        System.debug('===>>calculateBDE 2 test passed, vic_Contract_Term__c: '+ oppProd.vic_Contract_Term__c);
        if(oppProd.vic_Contract_Term__c!= null){
            System.debug('===>>calculateBDE 3 test passed');
            if(isAnniversaryCalculation){
                if(oppProd.vic_Contract_Term__c >= 1){
                    String incentivePercentSecondInstallmentStr = 
                        VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO BDE MoreThanYearSecondInstallment');
                    Decimal incentivePercentSecondInstallment = 
                        VIC_GeneralChecks.isDecimal(incentivePercentSecondInstallmentStr) ? Decimal.valueOf(incentivePercentSecondInstallmentStr) : 0;
                    
                    incentive = oppProd.VIC_NPV__c * incentivePercentSecondInstallment / 100;
                    oppProd.vic_VIC_Grid__c = incentivePercentSecondInstallment;
                }
            }
            else{
                System.debug('===>>calculateBDE 4 else block, vic_Contract_Term__c: '+ oppProd.vic_Contract_Term__c );
                if(oppProd.vic_Contract_Term__c <= 1){
                    String incentivePercentStr = 
                        VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO BDE LessThanYearIncentivePercent');
                    Decimal incentivePercent = VIC_GeneralChecks.isDecimal(incentivePercentStr) ? Decimal.valueOf(incentivePercentStr) : 0;
                    
                    incentive = oppProd.VIC_NPV__c * incentivePercent / 100;
                    oppProd.vic_VIC_Grid__c = incentivePercent;
                    System.debug('===>>calculateBDE 5 less than 1 year, incentive: ' + incentive); 
                }
                else if(oppProd.vic_Contract_Term__c >= 1){
                    String incentivePercentFirstInstallmentStr = 
                        VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO BDE MoreThanYearFirstInstallment');
                    Decimal incentivePercentFirstInstallment = 
                        VIC_GeneralChecks.isDecimal(incentivePercentFirstInstallmentStr) ? Decimal.valueOf(incentivePercentFirstInstallmentStr) : 0;
                    
                    incentive = oppProd.VIC_NPV__c * incentivePercentFirstInstallment / 100;
                    oppProd.vic_VIC_Grid__c = incentivePercentFirstInstallment;
                    System.debug('===>>calculateBDE 6 more than 1 year, incentive: ' + incentive); 
                }
            }
        }
        
        return incentive;
    }
    /*
    * @params: opportunityProduct
    * @return: Incentive(Decimal)
    * @description: This method returns Incentive for new profitable booking IO component for Capital_Market_GRM plan code.
    * 
    */
    @TestVisible
    private Decimal calculateCapitalMarketGRM(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        String strOppSource = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Source');
        if(oppProd.Opportunity.Opportunity_Source__c != null && oppProd.Opportunity.Opportunity_Source__c.equalsIgnoreCase(strOppSource)){
            incentive = 0;
        }else{
            Decimal npv = oppProd.VIC_NPV__c;
            
            if(isOneYearSOWAccount(oppProd) && oppProd.vic_Contract_Term__c <= 1){
                String sowAccountMultiplierStr = 
                    VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO One Year SOW Account Multiplier');
                Decimal sowAccountMultiplier = 
                    VIC_GeneralChecks.isDecimal(sowAccountMultiplierStr) ? Decimal.valueOf(sowAccountMultiplierStr) : 0;
                oppProd.vic_BPM_Multiplier__c = sowAccountMultiplier;
                npv = npv * sowAccountMultiplier;
            }
            
            String firstHalfCYRStr, secondHalfCYRStr;
            Decimal firstHalfCYR, secondHalfCYR;
            
            firstHalfCYRStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO First Half CYR Limit For CMGRM');
            secondHalfCYRStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO Second Half CYR Limit For CMGRM');
            
            firstHalfCYR = VIC_GeneralChecks.isDecimal(firstHalfCYRStr) ? Decimal.valueOf(firstHalfCYRStr) : 0;
            secondHalfCYR = VIC_GeneralChecks.isDecimal(secondHalfCYRStr) ? Decimal.valueOf(secondHalfCYRStr) : 0;
            
            if((oppProd.Opportunity.VIC_Total_CYR_IO__c!= null && oppProd.Opportunity.Actual_Close_Date__c != null) 
               && ((oppProd.Opportunity.Actual_Close_Date__c.month() >= 1 && oppProd.Opportunity.Actual_Close_Date__c.month() <= 6 && oppProd.Opportunity.VIC_Total_CYR_IO__c>= firstHalfCYR) ||
                   (oppProd.Opportunity.Actual_Close_Date__c.month() >= 7 && oppProd.Opportunity.Actual_Close_Date__c.month() <= 12 && oppProd.Opportunity.VIC_Total_CYR_IO__c>= secondHalfCYR))){
                       
                       String incentivePercentCM_GRMStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO CM GRM Inscentive percent');
                       Decimal incentivePercentCM_GRM = 
                           VIC_GeneralChecks.isDecimal(incentivePercentCM_GRMStr) ? Decimal.valueOf(incentivePercentCM_GRMStr) : 0;
                       incentive = npv * incentivePercentCM_GRM / 100;
                       oppProd.vic_VIC_Grid__c = incentivePercentCM_GRM;
                   }
        }
        return incentive;
    }
    
    /*
    * @params: opportunityProduct
    * @return: Incentive(Decimal)
    * @description: This method returns Incentive for new profitable booking IO component for EnterpriseGRM plan code.
    * 
    */
    @TestVisible
    private Decimal calculateEnterpriseGRM(OpportunityLineItem oppProd){
        Decimal incentive = 0;
        Decimal npv = oppProd.VIC_NPV__c;
        
        if(isOneYearSOWAccount(oppProd) && oppProd.vic_Contract_Term__c <= 1){
            String sowAccountMultiplierStr = 
                VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_IO_One_Year_SOW_Account_Multiplier');
            Decimal sowAccountMultiplier = VIC_GeneralChecks.isDecimal(sowAccountMultiplierStr) ? Decimal.valueOf(sowAccountMultiplierStr) : 0;
            oppProd.vic_BPM_Multiplier__c = sowAccountMultiplier;
            npv = npv * sowAccountMultiplier;
        }
        
        String otherAccountLimitOfTCVStr, sowAccountLimitOfTCVStr;
        Decimal otherAccountLimitOfTCV, sowAccountLimitOfTCV;
        
        otherAccountLimitOfTCVStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_IO_EGRM_Limit_For_All_Account');
        sowAccountLimitOfTCVStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_IO_EGRM_Limit_For_SOW_Account');
        
        otherAccountLimitOfTCV = VIC_GeneralChecks.isDecimal(otherAccountLimitOfTCVStr) ? Decimal.valueOf(otherAccountLimitOfTCVStr) : 0;
        sowAccountLimitOfTCV = VIC_GeneralChecks.isDecimal(sowAccountLimitOfTCVStr) ? Decimal.valueOf(sowAccountLimitOfTCVStr) : 0;
        
        if((oppProd.Opportunity.VIC_TCV_USD_Sum__c != null && oppProd.Opportunity.Actual_Close_Date__c != null) 
           && (oppProd.Opportunity.VIC_TCV_USD_Sum__c >= otherAccountLimitOfTCV || (isOneYearSOWAccount(oppProd)) && oppProd.Opportunity.VIC_TCV_USD_Sum__c >= sowAccountLimitOfTCV))
        {
            String incentivePercentE_GRMStr = 
                VIC_IncentiveConstantCtlr.getIncentiveConstantByLabel('NPB IO E GRM Incentive percent');
            Decimal incentivePercentE_GRM 
                = VIC_GeneralChecks.isDecimal(incentivePercentE_GRMStr) ? Decimal.valueOf(incentivePercentE_GRMStr) : 0;
            incentive = npv * incentivePercentE_GRM / 100; 
            oppProd.vic_VIC_Grid__c = incentivePercentE_GRM;
        }
        
        return incentive;
    }
    
    /*
    * @params: Account
    * @return: Boolean
    * @description: This method test for opportunity account that whether or not a 1 year sow account
    * 
    */
    @TestVisible
    private Boolean isOneYearSOWAccount(OpportunityLineItem oppProd){
        String businessGroup, businessSegmentName;
        businessGroup = oppProd.Opportunity.Account.Business_Group__c;
        businessSegmentName = oppProd.Opportunity.Account.Business_Segment__r.Name;
        
        Boolean result = false;
        if(businessGroup == 'GE'){
            result = true;
        }
        else if(businessGroup == 'Global Clients' && businessSegmentName != null){
            List<String> oneYearSOWAccounts = 
                VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('One_Year_SOW_Accounts').toLowerCase().split(',');
            result = oneYearSOWAccounts.contains(businessSegmentName.toLowerCase());
        }
        return result;
    }
}