@isTest
public class GPProjectTemplateFieldWrapperTracker {
	@isTest 
    public static void testGPProjectTemplateFieldWrapper() {
        GPProjectTemplateFieldWrapper projectTemplateFieldWrapper = new GPProjectTemplateFieldWrapper();
        
        projectTemplateFieldWrapper.ApiName = 'Name';
        projectTemplateFieldWrapper.isReadOnly = true;
        projectTemplateFieldWrapper.isreadOnlyAfterApproval = true;
        projectTemplateFieldWrapper.isRequired = true;
        projectTemplateFieldWrapper.isSectionVisible = true;
        projectTemplateFieldWrapper.isVisible = true;
        projectTemplateFieldWrapper.Label = 'Name';
        projectTemplateFieldWrapper.sectionName = 'Project Info';        
    }
}