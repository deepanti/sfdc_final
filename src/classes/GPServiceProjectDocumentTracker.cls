@isTest
public class GPServiceProjectDocumentTracker {
    public static GP_Project__c prjObj;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_Business_Group_L1__c = 'GE';
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Is_Customer_PO_Available__c ='Yes';
        prjObj.GP_Start_Date__c = system.today(); 
        prjObj.GP_End_Date__c = system.today().adddays(10);
		prjObj.GP_Tax_Exemption_Certificate_Available__c = true;
		// Gainshare Change
        prjObj.GP_Project_type__c='Gainshare';
        insert prjObj;
		
		GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        billingMilestone.GP_Project__c = prjObj.id;
        billingMilestone.CurrencyIsoCode = 'INR';
        billingMilestone.GP_Date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_start_date__c = System.today().adddays(3);
        billingMilestone.GP_Milestone_end_date__c = System.today().adddays(1);
        insert billingMilestone;
        // Gainshare Change
        List<GP_Project_Document__c> projectDocuments=new List<GP_Project_Document__c>();
        GP_Project_Document__c projectDocument=new GP_Project_Document__c();
        projectDocument.GP_Type__c='Customer SIGN OFF EMAIL';
        projectDocument.GP_Project__c=prjObj.id;
        projectDocuments.add(projectDocument);
        insert projectDocuments;
    }
    
    @isTest static void testGPServiceProjectDocument() {
        fetchData();
        GPServiceProjectDocument.validateProjectDocument(prjObj,null);
        GPServiceProjectDocument.isFormattedLogRequired = false; 
        GPServiceProjectDocument.validateProjectDocument(prjObj,null);
        GPServiceProjectDocument.isFormattedLogRequired = false; 
        GPServiceProjectDocument.isLogForTemporaryId = false; 
        GPServiceProjectDocument.validateProjectDocument(prjObj,null);
    }
    public static void fetchData() {
	// Gainshare Change : added GP_No_of_Billing_Milestone__c, GP_TCV__c and GP_Project_type__c
        prjObj = [select id, Name, GP_Business_Group_L1__c,GP_Is_Customer_PO_Available__c, GP_No_of_Billing_Milestone__c,
                  GP_Tax_Exemption_Certificate_Available__c,RecordTypeId, GP_Project_type__c, GP_TCV__c,
                  RecordType.Name, GP_Last_Temporary_Record_Id__c from GP_Project__c limit 1];
    }
    
}