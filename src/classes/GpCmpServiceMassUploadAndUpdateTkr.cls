@isTest public class GpCmpServiceMassUploadAndUpdateTkr {
    public static GP_Job__c objJobForBulkUpload;
    public static string serializedTemporaryData;
    static void setupCommonData()
    {
        objJobForBulkUpload = GPCommonTracker.getJobRecord('Upload Time Sheet Entries');
        insert objJobForBulkUpload;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Person_ID__c = 'EMP-002';
        empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        insert empObj; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        GP_Project__c prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_GPM_Employee__c = empObj.id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_Oracle_PID__c = '12345';
        prjobj.GP_Approval_Status__c = 'Approved';
        prjobj.GP_OMS_Status__c = 'Approved';
        prjobj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjobj.GP_End_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Days_Prior_Project_End_Date));
        insert prjobj;
        
        GP_Temporary_Data__c objTemporaryData = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForBulkUpload.Id,prjobj.GP_Oracle_PID__c,'RoleName');
        objTemporaryData.GP_PL_Employee_OHR__c = 'Test OHR';
        //insert objTemporaryData;
        
        List<GP_Temporary_Data__c> lstTemporaryData = new List<GP_Temporary_Data__c>();
        lstTemporaryData.add(objTemporaryData);
        
        serializedTemporaryData = JSON.serialize(lstTemporaryData);
    }
    
    @isTest private static void testinsertTemporaryRecordsService(){
        setupCommonData();
        GPControllerMassUploadAndUpdate obj = new GPControllerMassUploadAndUpdate();
        GpCmpServiceMassUploadAndUpdate.insertTemporaryRecordsService(serializedTemporaryData,'Upload project','Test FileName',null);
        
        GpCmpServiceMassUploadAndUpdate.insertTemporaryRecordsService(serializedTemporaryData,'Upload Time Sheet Entries','Test FileName',objJobForBulkUpload.Id);
        GpCmpServiceMassUploadAndUpdate.insertTemporaryRecordsService(serializedTemporaryData,'Upload project','Test FileName',objJobForBulkUpload.Id);
        GpCmpServiceMassUploadAndUpdate.insertTemporaryRecordsService(null,'Upload Time Sheet Entries','Test FileName',objJobForBulkUpload.Id);
        GpCmpServiceMassUploadAndUpdate.getJobConfigurationDataService(objJobForBulkUpload.Id);
        GpCmpServiceMassUploadAndUpdate.getJobConfigurationDataService(null);
    }

}