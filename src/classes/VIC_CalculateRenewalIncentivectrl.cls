/**
* @Description: Class used to calculate Renewal incentive 
* @author: Bashim Khan
* @date: May 2018
*/
public class VIC_CalculateRenewalIncentivectrl implements VIC_CalculationInterface {
  public static Decimal calculate(OpportunityLineItem oli, String planCode){
        Decimal incentive = 0;
        Set<String> plansRenewalIncentive = new Set<String>{'BDE','EnterpriseGRM','SL_CP_Enterprise','SEM','Capital_Market_GRM','IT_GRM'};
          
        if(plansRenewalIncentive.contains(planCode.trim()) && oli.Contract_Term__c >= Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Min_Contract_Year_Consider_For_Renewal'))
        && oli.Opportunity.vic_Total_Renewal_TCV__c >= Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Min_TCV_for_Renewal_Component'))){
            
            String strOppSource = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Source');
            if(planCode.equalsIgnorecase('Capital_Market_GRM') && oli.Opportunity.Opportunity_Source__c != null && oli.Opportunity.Opportunity_Source__c.equalsIgnoreCase(strOppSource)){
                incentive = 0;
            }else{
                Decimal splitPart = Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Split_Part_For_Renewal_Component'));
                Decimal firstPartRate = Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Before_Split_Part_Inc_per_For_Renewal'));
                Decimal secontPartRate = Decimal.valueof(VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('After_Split_Part_Inc_per_For_Renewal'));
                /*
                    For UPTO 50M RATE IS 1%
                    FOR MORETHEN 50M RATE IS 0.5%
                */
                Decimal totalTCV = oli.Opportunity.vic_Total_Renewal_TCV__c;
                Decimal nvp = oli.VIC_NPV__c;
                System.debug('totalTCV : ' + totalTCV);
                incentive = totalTCV > splitPart ? (((splitPart/totalTCV) * nvp) * (firstPartRate/100)) + ((((totalTCV - splitPart)/totalTCV ) * nvp) * (secontPartRate/100)) : (nvp * (firstPartRate/100));
            }
        }else{
            System.Debug('Condition not match.');
        }
        return incentive;
  }
}