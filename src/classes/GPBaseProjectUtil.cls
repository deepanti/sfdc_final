/**
 * @group Project 
 *
 * @description Class to query and create map Project and its associated child records.
 *              This class will be inherited for cloning, mass upload, functionality.
 */
public virtual class GPBaseProjectUtil {

    protected List < GP_Project_Work_Location_SDO__c > lstProjectWorkLocationAndSDO = new List < GP_Project_Work_Location_SDO__c > ();
    protected List < GP_Resource_Allocation__c > lstProjectResourceAllocation = new List < GP_Resource_Allocation__c > ();
    protected List < GP_Billing_Milestone__c > lstProjectBillingMilestone = new List < GP_Billing_Milestone__c > ();
    protected List < GP_Profile_Bill_Rate__c > lstProjectProfileBillRate = new List < GP_Profile_Bill_Rate__c > ();
    protected List < GP_Project_Version_History__c > lstProjectVerionHistory = new List < GP_Project_Version_History__c > ();
    protected List < GP_Project_Leadership__c > lstProjectLeadership = new List < GP_Project_Leadership__c > ();
    protected List < GP_Project_Document__c > lstProjectDocument = new List < GP_Project_Document__c > ();
    protected List < GP_Project_Expense__c > lstProjectExpense = new List < GP_Project_Expense__c > ();
    protected List < GP_Project_Address__c > lstProjectAddress = new List < GP_Project_Address__c > ();
    protected List < GP_Project_Budget__c > lstProjectBudget = new List < GP_Project_Budget__c > ();
    public List < GP_Project__c > lstProject = new List < GP_Project__c > ();

    public Map < Id, List < GP_Project_Work_Location_SDO__c >> mapOfProjectWorkLocation = new Map < Id, List < GP_Project_Work_Location_SDO__c >> ();
    public Map < Id, List < GP_Project_Version_History__c >> mapOfProjectVersionHistory = new Map < Id, List < GP_Project_Version_History__c >> ();
    public Map < Id, List < GP_Resource_Allocation__c >> mapOfResourceAllocation = new Map < Id, List < GP_Resource_Allocation__c >> ();
    public Map < Id, List < GP_Profile_Bill_Rate__c >> mapOfProjectProfileBillRate = new Map < Id, List < GP_Profile_Bill_Rate__c >> ();
    public Map < Id, List < GP_Project_Leadership__c >> mapOfProjectLeadership = new Map < Id, List < GP_Project_Leadership__c >> ();
    public Map < Id, List < GP_Billing_Milestone__c >> mapOfBillingMilestone = new Map < Id, List < GP_Billing_Milestone__c >> ();
    public Map < Id, List < GP_Project_Document__c >> mapOfProjectDocument = new Map < Id, List < GP_Project_Document__c >> ();
    public Map < Id, List < GP_Project_Expense__c >> mapOfProjectExpense = new Map < Id, List < GP_Project_Expense__c >> ();
    public Map < Id, List < GP_Project_Address__c >> mapOfProjectAdress = new Map < Id, List < GP_Project_Address__c >> ();
    public Map < Id, List < GP_Project_Budget__c >> mapOfProjectBudget = new Map < Id, List < GP_Project_Budget__c >> ();

    private Map < String, Schema.SObjectField > MapOfSchema;
    private Set < String > setOfOracleIds;
    private Set < Id > setOfProjectId;
    private String strQuery;
    public Boolean fetchErrorRecord = false;

    /**
     * @description Query all project and associated child records.
     * @param setOfOracleIds : Set of project OraclePIDs.
     * 
     */
    protected void queryProjectAndChilRecordsUsingOraclePID(Set < String > setOfOracleIds) {
        this.setOfOracleIds = setOfOracleIds;

        setProjectWorkLocationAndSDOWithOraclePID();
        setProjectLeadershipWithOraclePID();
        setResourcAllocationWithOraclePID();
        setBillingMilestoneWithOraclePID();
        setProjectDocumentWithOraclePID();
        setProfileBillRateWithOraclePID();
        setProjectAddressWithOraclePID();
        setProjectVersionWithOraclePID();
        setProjectExpenseWithOraclePID();
        setProjectBudgetWithOraclePID();
        setProjectDataWithOraclePID();
    }

    /**
     * @description Build Project Version History Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectVersionWithOraclePID() {
        strQuery = ' select ';
        MapOfSchema = Schema.SObjectType.GP_Project_Version_History__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Version_History__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by GP_Version_No__c desc';
        lstProjectVerionHistory = Database.query(strQuery);
    }

    /**
     * @description Build Project Address Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectAddressWithOraclePID() {
        strQuery = ' select ';
        MapOfSchema = Schema.SObjectType.GP_Project_Address__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Address__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc ';
        lstProjectAddress = Database.query(strQuery);
    }

    /**
     * @description Build Project Profile Bill Rate Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProfileBillRateWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Profile_Bill_Rate__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Profile_Bill_Rate__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectProfileBillRate = Database.query(strQuery);
    }

    /**
     * @description Build Project Document Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectDocumentWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Project_Document__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Document__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectDocument = Database.query(strQuery);
    }

    /**
     * @description Build Project Expense Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectExpenseWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Project_Expense__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Expense__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectExpense = Database.query(strQuery);
    }

    /**
     * @description Build Project Budget Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectBudgetWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Project_Budget__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Budget__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectBudget = Database.query(strQuery);
    }

    /**
     * @description Build Project BilingMilestone Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setBillingMilestoneWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Billing_Milestone__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Billing_Milestone__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectBillingMilestone = Database.query(strQuery);
    }

    /**
     * @description Build Project Worklocation and SDO Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectWorkLocationAndSDOWithOraclePID() {
		// Mass_Update_Change added RecordType.Name.
        strQuery = 'select GP_Project__r.GP_Oracle_PID__c, RecordType.Name, GP_Project__r.GP_Last_Temporary_Record_Id__c,GP_Work_Location__r.GP_Start_Date_Active__c,GP_Work_Location__r.GP_End_Date_Active__c,';
        strQuery += 'GP_Work_Location__r.Name,GP_Work_Location__r.GP_Billable__c,GP_Work_Location__r.GP_Hide_For_BPM__c,';
        strQuery += 'GP_Work_Location__r.GP_Country__c,GP_Work_Location__r.RecordType.Name,GP_Work_Location__r.GP_Status__c ,GP_Work_Location__r.GP_Oracle_Id__c, ';
        MapOfSchema = Schema.SObjectType.GP_Project_Work_Location_SDO__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Work_Location_SDO__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectWorkLocationAndSDO = Database.query(strQuery);
    }

    /**
     * @description Build Resource Allocation Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setResourcAllocationWithOraclePID() {
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Resource_Allocation__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Project__r.GP_Oracle_Status__c =\'S\' and GP_Project__r.GP_Approval_Status__c = \'Approved\') OR (GP_Project__r.GP_Approval_Status__c = \'Draft\')) order by Name desc';
        lstProjectResourceAllocation = Database.query(strQuery);
    }

    /**
     * @description Build Project Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectDataWithOraclePID() {
		// Mass Update Change added GP_Deal__r.GP_No_Pricing_in_OMS__c.
        strQuery = 'select ';
        MapOfSchema = Schema.SObjectType.GP_Project__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ',GP_Deal__r.GP_Deal_Category__c, RecordType.Name, GP_Project_Template__r.GP_Stage_Wise_Field_Project__c,GP_Primary_SDO__r.GP_IsChina__c,';
        strQuery += 'GP_CRN_Number__r.GP_Status__c, GP_Project_Template__r.GP_Final_JSON_1__c,GP_Deal__r.GP_Business_Group_L1__c,';
        strQuery += 'GP_Project_Template__r.GP_Final_JSON_2__c, GP_Project_Template__r.GP_Final_JSON_3__c, GP_Deal__r.GP_No_Pricing_in_OMS__c, ';
        strQuery += 'GP_Primary_SDO__r.GP_COE_Code__c, GP_HSL__r.Parent_HSL__c,GP_Customer_Hierarchy_L4__r.GP_Mapped_SDO_for_Pinnacle__c ';
        strQuery += ' from GP_Project__c where GP_Oracle_PID__c IN : setOfOracleIds and ((GP_Oracle_Status__c =\'S\' and GP_Approval_Status__c = \'Approved\') OR (GP_Approval_Status__c = \'Draft\')) ORDER BY LastModifiedDate Desc';
        lstProject = Database.query(strQuery);
    }

    /**
     * @description Build Project Leadership Object Query and Query the records using set of Project Oracle PID.
     * 
     */
    private void setProjectLeadershipWithOraclePID() {
		// Mass Update Change
        strQuery = ' select GP_Project__r.GP_Oracle_PID__c, GP_Project__r.GP_Last_Temporary_Record_Id__c, RecordType.Name, GP_Employee_ID__r.GP_HIRE_Date__c,GP_Employee_ID__r.GP_ACTUAL_TERMINATION_Date__c,GP_Employee_ID__r.Name, GP_Project__r.RecordType.Name, ';
        MapOfSchema = Schema.SObjectType.GP_Project_Leadership__c.fields.getMap();
        strQuery = appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Leadership__c where GP_Project__r.GP_Oracle_PID__c IN : setOfOracleIds and GP_Active__c = true and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc';
        lstProjectLeadership = Database.query(strQuery);
    }

    /**
     * @description Returns concatenated string of Objects Field.
     * @param query : query in which fields will be appended.
     * @param MapOfSchema : map Of Objects Fields.
     * 
     * @return String Concatenated Query.
     * 
     * @example
     * GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(<Query String>,<Schema Map of the Object>);
     */
    public static String appendToSelectQueryWithSchemaMap(String query, Map < String, Schema.SObjectField > MapOfSchema) {
        for (String fieldAPIName: MapOfSchema.keySet()) {
            query += fieldAPIName + ',';
        }
        query = query.removeEnd(',');
        return query;
    }

    /**
     * @description Query all project and associated child records using Project Id.
     * @param setOfProjectIds : Set of project SFDC Ids.
     * 
     */
    public void queryProjectAndChilRecords(set < Id > setOfProjectId) {
        this.setOfProjectId = setOfProjectId;

        setProjectWorkLocationAndSDO();
        setProjectVersionHistory();
        setProjectLeadership();
        setResourcAllocation();
        setBillingMilestone();
        setProjectDocument();
        setProfileBillRate();
        setProjectExpense();
        setProjectAddress();
        setProjectBudget();
        setProjectData();
    }

    /**
     * @description Build Project Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectData() {
        string strQuery = 'Select ';
        strQuery += getSobjectQuery('GP_Project__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project__c where id in :setOfProjectId and GP_Oracle_Status__c !=\'S\'';
        } else {
            strQuery += ' from GP_Project__c where id in :setOfProjectId and GP_Oracle_Status__c =\'S\'';
        }
        lstProject = Database.query(strQuery);
    }

    /**
     * @description Build Project Leadership Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectLeadership() {
        strQuery = ' select  GP_Project__r.GP_Oracle_PID__c,';
        strQuery += getSobjectQuery('GP_Project_Leadership__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Leadership__c where GP_Project__c in :setOfProjectId and GP_Active__c = true and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc';
        } else {
            strQuery += ' from GP_Project_Leadership__c where GP_Project__c in :setOfProjectId and GP_Active__c = true and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc';
        }
        lstProjectLeadership = Database.query(strQuery);
    }

    /**
     * @description Build Resource Allocation Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setResourcAllocation() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Resource_Allocation__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Resource_Allocation__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc';
        } else {
            strQuery += ' from GP_Resource_Allocation__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc';
        }
        lstProjectResourceAllocation = Database.query(strQuery);
    }

    /**
     * @description Build Project Budget Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectBudget() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Budget__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Budget__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Budget__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectBudget = Database.query(strQuery);
    }

    /**
     * @description Build Project Expense Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectExpense() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Expense__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Expense__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Expense__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectExpense = Database.query(strQuery);
    }

    /**
     * @description Build Project Work Location and SDO Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectWorkLocationAndSDO() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Work_Location_SDO__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Work_Location_SDO__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Work_Location_SDO__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectWorkLocationAndSDO = Database.query(strQuery);
    }

    /**
     * @description Build Project Billing Milestone Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setBillingMilestone() {
        strQuery = 'select ';
        strQuery += getSobjectQuery('GP_Billing_Milestone__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Billing_Milestone__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name ';
        } else {
            strQuery += ' from GP_Billing_Milestone__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name ';
        }
        lstProjectBillingMilestone = Database.query(strQuery);
    }

    /**
     * @description Build Project Document Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectDocument() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Document__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Document__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Document__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectDocument = Database.query(strQuery);
    }

    /**
     * @description Build Project Address Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectAddress() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Address__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Address__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Address__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectAddress = Database.query(strQuery);
    }

    /**
     * @description Build Project Version Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProjectVersionHistory() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Project_Version_History__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Project_Version_History__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Project_Version_History__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectVerionHistory = Database.query(strQuery);
    }

    /**
     * @description Build Project Profile Bill Rate Object Query and Query the records using set of Project SFDC ID.
     * 
     */
    private void setProfileBillRate() {
        strQuery = ' select ';
        strQuery += getSobjectQuery('GP_Profile_Bill_Rate__c');
        if (fetchErrorRecord) {
            strQuery += ' from GP_Profile_Bill_Rate__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c !=\'S\' order by Name desc ';
        } else {
            strQuery += ' from GP_Profile_Bill_Rate__c where GP_Project__c in :setOfProjectId and GP_Project__r.GP_Oracle_Status__c =\'S\' order by Name desc ';
        }
        lstProjectProfileBillRate = Database.query(strQuery);

        /*strQuery += ' (select id from ProcessInstances ORDER BY CreatedDate DESC)';*/

        /* strQuery += ' (select id, Name, GP_Status__c from Project_Version_History__r)';*/
    }

    /**
     * @description Set Map Of project and its Associated child Records.
     * 
     */
    public void setMapOfProjectChildRecords() {
        mapOfProjectWorkLocation = getParentIdVsChildRecordSet(lstProjectWorkLocationAndSDO);
        mapOfProjectProfileBillRate = getParentIdVsChildRecordSet(lstProjectProfileBillRate);
        mapOfResourceAllocation = getParentIdVsChildRecordSet(lstProjectResourceAllocation);
        mapOfProjectVersionHistory = getParentIdVsChildRecordSet(lstProjectVerionHistory);
        mapOfBillingMilestone = getParentIdVsChildRecordSet(lstProjectBillingMilestone);
        mapOfProjectLeadership = getParentIdVsChildRecordSet(lstProjectLeadership);
        mapOfProjectDocument = getParentIdVsChildRecordSet(lstProjectDocument);
        mapOfProjectExpense = getParentIdVsChildRecordSet(lstProjectExpense);
        mapOfProjectAdress = getParentIdVsChildRecordSet(lstProjectAddress);
        mapOfProjectBudget = getParentIdVsChildRecordSet(lstProjectBudget);

    }

    /**
     * @description Returns Map of Project Id vs List of Child objects.
     * @param lstOfChildRecords : list of child records whose map is to created.
     * 
     * @return Map<Id,List<SObject>> : Map of Project Id vs List of Child objects.
     * 
     */
    private Map < Id, List < SObject >> getParentIdVsChildRecordSet(List < SObject > lstOfChildRecords) {
        Map < Id, List < Sobject >> mapofProjectIdVsChildRecordList = new Map < Id, List < Sobject >> ();

        for (SObject sObj: lstOfChildRecords) {
            if (!mapofProjectIdVsChildRecordList.containsKey((Id) sObj.get('GP_Project__c'))) {
                mapofProjectIdVsChildRecordList.put((Id) sObj.get('GP_Project__c'), new List < Sobject > ());
            }

            mapofProjectIdVsChildRecordList.get((Id) sObj.get('GP_Project__c')).add(sObj);
        }

        return mapofProjectIdVsChildRecordList;
    }

    /**
     * @description Returns concatenated string of Objects Field and also concatenate lookup field names.
     * @param objectAPIName : Object API Name.
     * 
     * @return String Concatenated Query.
     * 
     */
    private string getSobjectQuery(string objectAPIName) {
        Set < String > setOfStandardFields = new Set < String > { 'connectionreceivedid', 'connectionsentid' };
        Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
        String query = '';

        Schema.SObjectType projectSchema = schemaMap.get(objectAPIName);
        Map < String, Schema.SObjectField > fieldMap = projectSchema.getDescribe().fields.getMap();


        for (String fieldAPIName: fieldMap.keySet()) {
            query += fieldAPIName + ',';
            if (fieldMap.get(fieldAPIName).getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                if (fieldAPIName.contains('__c')) {
                    query += fieldAPIName.replace('__c', '__r.Name');
                    query += ',';
                } else if (fieldAPIName.contains('id') && !setOfStandardFields.contains(fieldAPIName)) {
                    query += fieldAPIName.replace('id', '.Name');
                    query += ',';
                }
            }
        }

        query = query.removeEnd(',');
        return query;
    }

    public list < GP_Project_Version_Line_Item_History__c > getListOfVersionLineItemForJSON(String serializedJSON, String relatedRecordType, String uniqueExternalId) {
        list < GP_Project_Version_Line_Item_History__c > listOfVersionLineItem = new list < GP_Project_Version_Line_Item_History__c > ();

        Integer offset = 0;
        Integer fieldLimit = 131072;
        Integer numberOfFieldsPerObject = 12;

        Integer lengthOfVersionJson = serializedJSON.length();
        Integer numberOfRelatedObjects = (Integer) Math.floor((Double) lengthOfVersionJson / (fieldLimit * numberOfFieldsPerObject) + 1);

        String fieldName;
        Integer startIndex, endIndex;

        for (Integer i = 0; i < numberOfRelatedObjects; i++) {
            GP_Project_Version_Line_Item_History__c currentVersionLineItem = new GP_Project_Version_Line_Item_History__c();

            for (Integer j = 0; j < numberOfFieldsPerObject && offset <= lengthOfVersionJson; j++) {
                fieldName = 'GP_JSON' + (j + 1) + '__c';

                startIndex = Math.min(offset, lengthOfVersionJson);
                endIndex = Math.min(offset + fieldLimit, lengthOfVersionJson);

                currentVersionLineItem.put(fieldName, serializedJSON.substring(startIndex, endIndex));
                offset += fieldLimit;

                currentVersionLineItem.GP_Related_Record_Type__c = relatedRecordType;
                currentVersionLineItem.GP_Project_Version_History__r = new GP_Project_Version_History__c();
                currentVersionLineItem.GP_Project_Version_History__r.GP_Unique_Version_History__c = uniqueExternalId;
            }


            listOfVersionLineItem.add(currentVersionLineItem);
        }

        return listOfVersionLineItem;
    }

	public static String getSerializedVersionLineItemJSON(GP_Project_Version_Line_Item_History__c listOfVersionLineItem) {
        String serializedJSON = '';
        Integer numberOfFieldsPerObject = 12;

        String fieldName;
        //for (GP_Project_Version_Line_Item_History__c currentVersionLineItem: listOfVersionLineItem) {
            for (Integer i = 0; i < numberOfFieldsPerObject; i++) {
                fieldName = 'GP_JSON' + (i + 1) + '__c';
                String versionLineItemChunk = (String) listOfVersionLineItem.get(fieldName);
                if (versionLineItemChunk != null) {
                    serializedJSON += versionLineItemChunk;
                }
            }
        //}

        return serializedJSON;
    }
    
    public static String getSerializedVersionLineItemJSON(list < GP_Project_Version_Line_Item_History__c > listOfVersionLineItem) {
        String serializedJSON = '';
        Integer numberOfFieldsPerObject = 12;

        String fieldName;
        for (GP_Project_Version_Line_Item_History__c currentVersionLineItem: listOfVersionLineItem) {
            for (Integer i = 0; i < numberOfFieldsPerObject; i++) {
                fieldName = 'GP_JSON' + (i + 1) + '__c';
                String versionLineItemChunk = (String) currentVersionLineItem.get(fieldName);
                if (versionLineItemChunk != null) {
                    serializedJSON += versionLineItemChunk;
                }
            }
        }

        return serializedJSON;
    }
}