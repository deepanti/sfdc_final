@isTest
private class GPControllerProjectApprovalTracker {
    private static GP_Project__c project;
    private static List<GP_Project_Leadership__c> listOfProjectLeadership;
    private static List<GP_Project_Work_Location_SDO__c> listOfProjectWorkLocation;
    private static List<GP_Project_Document__c> listOfProjectDocument;
    private static List<GP_Resource_Allocation__c> listOfResourceAllocation;
    private static List<GP_Billing_Milestone__c> listOfBillingMilestone;
    private static List<GP_Project_Budget__c> listOfProjectBudget;
    private static List<GP_Project_Expense__c> listOfProjectExpense;
    private static List<GP_Project_Address__c> listOfProjectAddress;
    private static GP_Work_Location__c workLocationMaster;
    private static GP_Leadership_Master__c ledershipMaster;
    private static GP_Employee_Master__c employeeMaster;
    private static GP_Budget_Master__c budgetMaster;
    
    @testSetup
    static void createData() {
        GP_Employee_Master__c employee = GPCommonTracker.getEmployee();
        insert employee; 
        
        Business_Segments__c businessSegment = GPCommonTracker.getBS();
        insert businessSegment ;
        
        Sub_Business__c subBusinessSegment = GPCommonTracker.getSB(businessSegment.id);
        insert subBusinessSegment ;
        
        Account accobj = GPCommonTracker.getAccount(businessSegment.id,subBusinessSegment.id);
        accobj.Industry_Vertical__c = 'Sample Vertical';
        insert accobj ;
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Pinnacle_Master__c pinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert pinnacleMaster ;
        
        GP_Role__c role = GPCommonTracker.getRole(sdo,pinnacleMaster );
        role.GP_Work_Location_SDO_Master__c = sdo.Id;
        role.GP_HSL_Master__c = null;
        role.GP_Role_Category__c = 'PID Creation';
        
        insert role;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(role ,objuser);
        objuserrole.GP_Active__c = true;
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  employeeHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert employeeHours ;  
        
        GP_Timesheet_Transaction__c  timesheetTransaction = GPCommonTracker.getTimesheetTransaction(employee);
        insert timesheetTransaction;
        
        GP_Deal__c deal = GPCommonTracker.getDeal();
        deal.GP_Deal_Type__c = 'CMITS';
        deal.GP_Business_Type__c = 'PBB';
        deal.GP_Business_Name__c = 'Consulting';
        deal.GP_Business_Group_L1__c = 'group1';
        deal.GP_Business_Segment_L2__c = 'segment2';
        insert deal ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c bpmTemplate = GPCommonTracker.getProjectTemplate();
        bpmTemplate.Name = 'BPM-BPM-ALL';
        bpmTemplate.GP_Active__c = true;
        bpmTemplate.GP_Business_Type__c = 'BPM';
        bpmTemplate.GP_Business_Name__c = 'BPM';
        bpmTemplate.GP_Business_Group_L1__c = 'All';
        bpmTemplate.GP_Business_Segment_L2__c = 'All';
        insert bpmTemplate;
        
        GP_Project_Template__c bpmWithoutSFDCTemplate = GPCommonTracker.getProjectTemplate();
        
        bpmWithoutSFDCTemplate.GP_Active__c = true;
        bpmWithoutSFDCTemplate.Name = 'BPM-BPM-ALL-Without-SFDC';
        bpmWithoutSFDCTemplate.GP_Business_Type__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Name__c = 'BPM';
        bpmWithoutSFDCTemplate.GP_Business_Group_L1__c = 'All';
        bpmWithoutSFDCTemplate.GP_Business_Segment_L2__c = 'All';
        bpmWithoutSFDCTemplate.GP_Without_SFDC__c = true;
        
        insert bpmWithoutSFDCTemplate;
        
        GP_Project_Template__c indirectTemplate = GPCommonTracker.getProjectTemplate();
        indirectTemplate.GP_Active__c = true;
        indirectTemplate.Name = 'INDIRECT_INDIRECT';
        indirectTemplate.GP_Business_Type__c = 'Indirect';
        indirectTemplate.GP_Business_Name__c = 'Indirect';
        indirectTemplate.GP_Business_Group_L1__c = 'All';
        indirectTemplate.GP_Business_Segment_L2__c = 'All';
        insert indirectTemplate;
        
        
        GP_Project__c project = GPCommonTracker.getProject(deal,'CMITS',bpmTemplate,objuser,role);
        project.OwnerId=objuser.Id;
        project.GP_CRN_Number__c = iconMaster.Id;
        project.GP_Operating_Unit__c = pinnacleMaster.Id;
        project.GP_Project_type__c = 'Fixed monthly';
        insert project ;
        
        GP_Timesheet_Entry__c timesheetEntry = GPCommonTracker.getTimesheetEntry(employee,project,timesheetTransaction);
        insert timesheetEntry;
        
        GP_Customer_Master__c customerMaster = new GP_Customer_Master__c();
        insert customerMaster;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        address.GP_Customer__c = customerMaster.Id;
        address.GP_Billing_Entity__c = pinnacleMaster.Id;
        address.GP_Status__c = 'Active';
        address.GP_BILL_TO_FLAG__c = true;
        address.GP_SHIP_TO_FLAG__c = true;
        
        insert address ;
        
        GP_Address__c  dummyAddress = GPCommonTracker.getAddress();
        dummyAddress.GP_Customer__c = customerMaster.Id;
        dummyAddress.GP_Billing_Entity__c = pinnacleMaster.Id;
        dummyAddress.GP_Status__c = 'Active';
        dummyAddress.GP_BILL_TO_FLAG__c = true;
        dummyAddress.GP_SHIP_TO_FLAG__c = true;
        dummyAddress.GP_City__c = 'GENPACT Dummy';
        
        insert dummyAddress;
        
        GP_Product_Master__c productMaster = new GP_Product_Master__c();
        productMaster.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster.GP_Product_Family__c = 'Sample Family';
        productMaster.GP_Service_Line__c = 'Sample Service';
        
        insert productMaster;
        
        GP_Product_Master__c productMaster2 = new GP_Product_Master__c();
        productMaster2.GP_Industry_Vertical__c = 'Sample Vertical';
        productMaster2.GP_Nature_of_Work__c = 'Sample Nature Of Work';
        productMaster2.GP_Product_Family__c = 'Sample Family';
        
        insert productMaster2;
    }
    
    @isTest
    static void testGPControllerProjectApprovalTracker() {
        fetchData();
        testConstructor();
    }
    
    static void fetchData() {
        project = [Select Id, GP_Project_Template__c from GP_Project__c LIMIT 1];
    }
    
    static void testConstructor() {
       
        employeeMaster = [SELECT ID From GP_Employee_Master__c LIMIT 1];
        
        GPWrapperApprovalValidatorConfig validatorConfig = new GPWrapperApprovalValidatorConfig();
        
        GPControllerProjectApprovalValidator approvalValidator = new GPControllerProjectApprovalValidator(project.Id); 
        
        approvalValidator.validateProject();
        
        //create related record.
        createProjectLeadership();
        createProjectWorklocation();
        createProjectDocument();
        createResourceAllocation();
        createBillingMilestone();
        createProjectBudget();
        createProjectExpense();
        createProjectAddress();   
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id, validatorConfig); 
        
        approvalValidator = new GPControllerProjectApprovalValidator(project); 
        approvalValidator = new GPControllerProjectApprovalValidator(project, validatorConfig); 
        
        
        Date todayDate = System.today();
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectLeadership);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectLeadership, validatorConfig); 
             
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectWorkLocation);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectWorkLocation, validatorConfig); 
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectDocument);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectDocument, validatorConfig); 
       
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfResourceAllocation);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfResourceAllocation, validatorConfig); 
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfBillingMilestone);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfBillingMilestone, validatorConfig); 
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectBudget);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectBudget, validatorConfig); 
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectExpense);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectExpense, validatorConfig); 
    	
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectAddress);
        approvalValidator = new GPControllerProjectApprovalValidator(listOfProjectAddress, validatorConfig);
        
        approvalValidator.setProjectId(project.Id)
            .setProject(project)
            .setProjectTemplate(null)
            .setProjectWorkLocation(listOfProjectWorkLocation)
            .setProjectLeadership(listOfProjectLeadership)
            .setBillingMilestone(listOfBillingMilestone)
            .setProjectDocument(listOfProjectDocument)
            .setResourceAllocation(listOfResourceAllocation)
            .setProjectBudget(listOfProjectBudget)
            .setProjectExpense(listOfProjectExpense)
            .setProjectAddress(listOfProjectAddress)
            .setIsFormattedLogRequired(true)
            .setIsLogForTemporaryId(false)
            .setFetchData(false);
        
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
        approvalValidator.validateProject();
        
		Test.startTest();
		
        project.GP_Last_Temporary_Record_Id__c = project.Id; //'12356';
        update project;
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
        approvalValidator
            .setIsFormattedLogRequired(false)
            .setIsLogForTemporaryId(true)
            .validateProject();

        
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
        approvalValidator
            .setIsFormattedLogRequired(false)
            .setIsLogForTemporaryId(false)
            .validateProject();
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
        approvalValidator
            .setIsFormattedLogRequired(false)
            .setIsLogForTemporaryId(true)
            .validateProjectForClosure();
        
        project.GP_SOW_Start_Date__c = System.today();
        project.GP_SOW_End_Date__c = System.today().addDays(110);
        project.GP_End_Date__c = System.today().addDays(100);
        update project;
        
        approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
        approvalValidator
            .setIsFormattedLogRequired(false)
            .setIsLogForTemporaryId(true)
            .validateProjectForClosure();
        
        approvalValidator.validateProjectSOWDateForClosure();
        
        approvalValidator.validateISApprovalRequired();
        approvalValidator.validateISApprovalRequiredForClosure();
		
        Test.stopTest();
    }
    /*
static void test2() {
project = [Select Id, GP_Project_Template__c from gp_project__c LIMIT 1];

Test.startTest();
project.RecordTypeId = GPCommon.getRecordTypeId('gp_project__c', 'OTHER PBB');
project.GP_Project_type__c = 'Fixed Price';
update project;
GP_Project_Template__c projectTemplate = [Select Id, GP_Stage_Wise_Field_Project__c  
from GP_Project_Template__c 
where Id = :project.GP_Project_Template__c];

staticResource stageWiseFieldResource = [SELECT id, Body, Name
FROM staticResource
WHERE Name = 'StageWiseField'
];

String stageWiseField = stageWiseFieldResource.Body.toString();
projectTemplate.GP_Stage_Wise_Field_Project__c = stageWiseField;
update projectTemplate;

approvalValidator = new GPControllerProjectApprovalValidator(project.Id);
approvalValidator
.setIsFormattedLogRequired(false)
.setIsLogForTemporaryId(false)
.validateProject();
Test.stopTest();
}
*/
    
    private static void createProjectLeadership() {
        Date todayDate = System.today();
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        ledershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        ledershipMaster.GP_Category__c = 'Mandatory Key Members';
        insert ledershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(project.Id, ledershipMaster.Id, todayDate, todayDate.addDays(100), employeeMaster.Id);
        projectLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
        
        List<GP_Project_Leadership__c> listOfProjectLeadership = new List<GP_Project_Leadership__c>();
        listOfProjectLeadership.add(projectLeadership);
        
        insert listOfProjectLeadership;
    }
    
    private static void createProjectWorklocation() {
        workLocationMaster = GPCommonTracker.getWorkLocation();
        GP_Project_Work_Location_SDO__c projectWorkLocation = GPCommonTracker.getProjectWorkLocation(project.Id, workLocationMaster.Id, true); 
        projectWorkLocation.GP_Last_Temporary_Record_Id__c = project.Id;
        
        listOfProjectWorkLocation = new List<GP_Project_Work_Location_SDO__c>();
        listOfProjectWorkLocation.add(projectWorkLocation);
        insert listOfProjectWorkLocation;
    }
    
    private static void createProjectDocument() {
        GP_Project_Document__c projectDocument = GPCommonTracker.getProjectDocument(project.Id);
        
        List<GP_Project_Document__c> listOfProjectDocument = new List<GP_Project_Document__c>();
        listOfProjectDocument.add(projectDocument);
        insert listOfProjectDocument;
    }
    
    private static void createResourceAllocation() {
         
        GP_Resource_Allocation__c resourceAllocation = GPCommonTracker.getResourceAllocation(null, project.Id);
        resourceAllocation.GP_Last_Temporary_Record_Id__c = project.Id;
        
        List<GP_Resource_Allocation__c> listOfResourceAllocation = new List<GP_Resource_Allocation__c>();
        listOfResourceAllocation.add(resourceAllocation);
        insert listOfResourceAllocation;
    }
    
    private static void createBillingMilestone() {
        
        GP_Billing_Milestone__c  billingMilestone = GPCommonTracker.getBillingMilestone(project.Id, workLocationMaster.Id);
        billingMilestone.GP_Last_Temporary_Record_Id__c = project.Id;
        
        List<GP_Billing_Milestone__c> listOfBillingMilestone = new List<GP_Billing_Milestone__c>();
        listOfBillingMilestone.add(billingMilestone);
        insert listOfBillingMilestone;
    }
    
    private static void createProjectBudget() {
        budgetMaster = GPCommonTracker.getBudgetMaster();
        insert budgetMaster;
        
        GP_Project_Budget__c projectBudget = GPCommonTracker.getProjectBudget(project.Id, budgetMaster.Id);
        projectBudget.GP_Last_Temporary_Record_Id__c = project.Id;
        
        List<GP_Project_Budget__c> listOfProjectBudget = new List<GP_Project_Budget__c>();
        listOfProjectBudget.add(projectBudget);
        insert listOfProjectBudget;
    }
    
    private static void createProjectExpense() {
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(project.Id);
        projectExpense.GP_Last_Temporary_Record_Id__c = project.Id;
               
        List<GP_Project_Expense__c> listOfProjectExpense = new List<GP_Project_Expense__c>();
        listOfProjectExpense.add(projectExpense);
        insert listOfProjectExpense;
    }
    
    private static void createProjectAddress() {
            
        GP_Project_Address__c projectAddress = GPCommonTracker.getProjectAddress(project.Id, 'Direct', null);
        projectAddress.GP_Last_Temporary_Record_Id__c = project.Id; 
        List<GP_Project_Address__c> listOfProjectAddress = new List<GP_Project_Address__c>();
        listOfProjectAddress.add(projectAddress);
		insert listOfProjectAddress;
    }
}