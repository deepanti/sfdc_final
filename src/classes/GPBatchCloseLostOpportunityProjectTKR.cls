//================================================================================================================
//  Description: Test Class for GPBatchCloseLostOpportunityProject
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          8-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
private class GPBatchCloseLostOpportunityProjectTKR {
    Public Static List<GP_Project_Address__c> listOfAddress = new List<GP_Project_Address__c>();
    Public Static GP_Project__c prjobj;
    Public Static GP_Billing_Milestone__c billingMilestone;
    Public Static GP_Work_Location__c objSdo;
    Public Static GP_Customer_Master__c customerObj;
    private Static User objuser;
    
    static void setupCommonData() {
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
         objuser = GPCommonTracker.getUser(); 
        Profile p = [select id, name from profile where name = 'Genpact Super Admin'
                     limit 1
                    ];
        objuser.ProfileID = p.id;        
        insert objuser;
        system.runAS(objuser)
        {  
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        //oppobj.Opportunity_ID__c = '12345';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-10);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;
        oppobj.Bypass_Validations__c = true;
        oppobj.StageName = '7. Lost';
        oppobj.W_L_D__c='Lost';
        oppobj.Actual_Close_Date__c = System.Today().adddays(-10);
        update oppobj;
        
        
        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id=:oppobj.id];
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        insert oppproobj;
        }
    }
    
    @isTest
    public static void testGPBatchCloseLostOpportunityProject() {
        setupCommonData();
        date dateofOppClosure = System.Today().adddays(-10);
       system.debug('QQQ'+ [Select Id, Name, stagename, Probability, Opportunity_ID__c from Opportunity where(stagename = '7. Lost'
            or stagename = '8. Dropped') and Actual_Close_Date__c =: dateofOppClosure]);
      
        GPBatchCloseLostOpportunityProject batcher = new GPBatchCloseLostOpportunityProject();
        Id batchprocessid = Database.executeBatch(batcher);
    }
}