public without sharing class GPServiceOMSPricing {

    private set<Id> setProjectIds ;
    private list<GP_Project__c> lstProjects;
    private fflib_SObjectUnitOfWork uow;
    private set<Id> setDealIDs;
    private list<GP_OMS_Detail__c> lstOMSDetails ;
    private map < string, list < GP_OMS_Detail__c >> mapDealKeytoOMSDetailList ;
    private list < GP_Project_Budget__c > lstProjectBudgetToInsert ;
    private list < GP_Project_Expense__c > lstProjectExpenseToInsert ;

    private list < GP_Project_Budget__c > lstProjectBudgetToDelete ;
    private list < GP_Project_Expense__c > lstProjectExpenseToDelete ;
    


    private static final List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
        GP_Project_Classification__c.SObjectType,
        GP_Project_Budget__c.SObjectType,
        GP_Project_Expense__c.SObjectType
    };

    public GPServiceOMSPricing(set<Id> setProjectIds) {
        this.setProjectIds = setProjectIds;
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);

    }

    public GPServiceOMSPricing(list<GP_Project__c> lstProjects) {
        this.lstProjects = lstProjects;
       
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);

    }
    public GPServiceOMSPricing(list<GP_Project__c> lstProjects , set<Id> setDealIDs) {
        this.lstProjects = lstProjects;
       
        uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        this.setDealIDs = setDealIDs;
    }

    public void createProjectRelatedDataForOMSDeal()
    {
        try
        {
            getOMSdetails();
            system.debug('lstOMSDetails::'+lstOMSDetails);
            if (lstOMSDetails == null || lstOMSDetails.isEmpty()) {
                return;
            }
            mapDealKeytoOMSDetailList = new map < string, list < GP_OMS_Detail__c >> (); 

            for (GP_OMS_Detail__c objOMSDetail: lstOMSDetails) {
                if (objOMSDetail.GP_Deal__c != null && objOMSDetail.RecordTypeId != null) {
                    string strKey = objOMSDetail.GP_Deal__c + '@@' + objOMSDetail.RecordType.Name;

                    if (mapDealKeytoOMSDetailList.get(strKey) == null) {
                        mapDealKeytoOMSDetailList.put(strKey, new list < GP_OMS_Detail__c > ());
                    }

                    mapDealKeytoOMSDetailList.get(strKey).add(objOMSDetail);
                }
            }

            map < string, list < GP_OMS_Detail__c >> mapProjectIdtoOMSDetailListofBudget = new map < string, list < GP_OMS_Detail__c >> ();
            map < string, list < GP_OMS_Detail__c >> mapProjectIdtoOMSDetailListofExpense = new map < string, list < GP_OMS_Detail__c >> ();
            lstProjectBudgetToInsert = new list < GP_Project_Budget__c > ();
            lstProjectExpenseToInsert = new list < GP_Project_Expense__c > ();

            String budgetKey, expenseKey;

            for (GP_Project__c objProject: lstProjects) {
                if (objProject.GP_Deal__c == null ||
                    (objProject.GP_Deal_Category__c != 'Sales SFDC' &&
                        objProject.GP_Deal_Category__c != 'OMS')) {
                    continue;
                }

                budgetKey = objProject.GP_Deal__c + '@@Pricing View';
                expenseKey = objProject.GP_Deal__c + '@@Expense';

                if (mapDealKeytoOMSDetailList.containsKey(budgetKey) && !mapDealKeytoOMSDetailList.get(budgetKey).isEmpty()) {

                    mapProjectIdtoOMSDetailListofBudget.put(objProject.id, new list < GP_OMS_Detail__c > ());

                    for (GP_OMS_Detail__c objOMSDetailforBudget: mapDealKeytoOMSDetailList.get(budgetKey)) {
                        mapProjectIdtoOMSDetailListofBudget.get(objProject.id).add(objOMSDetailforBudget);
                    }

                    createProjectBudget(uow, mapProjectIdtoOMSDetailListofBudget);
                }
            
             if (mapDealKeytoOMSDetailList.containsKey(expenseKey) && !mapDealKeytoOMSDetailList.get(expenseKey).isEmpty()) {

                    mapProjectIdtoOMSDetailListofExpense.put(objProject.id, new list < GP_OMS_Detail__c > ());

                    for (GP_OMS_Detail__c objOMSDetailforExpense: mapDealKeytoOMSDetailList.get(expenseKey)) {
                        mapProjectIdtoOMSDetailListofExpense.get(objProject.id).add(objOMSDetailforExpense);
                    }
                    createProjectExpense(uow, mapProjectIdtoOMSDetailListofExpense);
                }
            }

           system.debug('lstProjectExpenseToInsert::'+lstProjectExpenseToInsert);
           system.debug('lstProjectBudgetToInsert::'+lstProjectBudgetToInsert);

            if (lstProjectExpenseToInsert.size() > 0) {
                insert lstProjectExpenseToInsert;
                //uow.registerNew(lstProjectExpenseToInsert);
            }
            
            if (lstProjectBudgetToInsert.size() > 0) {
                insert lstProjectBudgetToInsert;
                //uow.registerNew(lstProjectBudgetToInsert);
            }
            
            deleteProjectExistingRecord();
        }
        catch(exception e){
            system.debug(e.getMessage());
        }
    }
     private void getOMSdetails(){
         if(setDealIDs == null){
                setDealIDs = new set<Id>();
             if(lstProjects == null){
                 lstProjects = [Select id, GP_Deal__c,GP_Deal_Category__c ,GP_Parent_Project__c ,(Select id from GP_Project_Budgets__r),
                                    (Select id from GP_Project_Expenses__r)
                                            from GP_Project__c where id in:setProjectIds ];
             }
            if(lstProjects == null){
                return;
            }
            
            lstProjectExpenseToDelete = new list<GP_Project_Expense__c>();
            lstProjectBudgetToDelete = new list<GP_Project_Budget__c>();
            for (GP_Project__c objProject: lstProjects) 
            {
                if (objProject.GP_Deal__c != null && 
                            (objProject.GP_Deal_Category__c == 'Sales SFDC' || objProject.GP_Deal_Category__c == 'OMS')) {
                    setDealIDs.add(objProject.GP_Deal__c);

                }
                if(objProject.GP_Project_Expenses__r.size()>0 ){
                    lstProjectExpenseToDelete.addAll(objProject.GP_Project_Expenses__r);
                }
                
                if(objProject.GP_Project_Budgets__r.size()>0 ){
                    lstProjectBudgetToDelete.addAll(objProject.GP_Project_Budgets__r);
                }
            }        
         }

        if (setDealIDs.isEmpty()) {
            return;
        }
         
        lstOMSDetails = [Select RecordTypeId, RecordType.Name, Name, Id, GP_Work_Location_Name__c, GP_Work_Location_Code__c, GP_Track__c,
						            GP_TotalCost__c, GP_TCVInUSD__c, GP_Primary__c, GP_Location__c, GP_Expense_Type__c,
						            GP_Expense_Amount__c, GP_Effort_in_Hours__c, GP_Country__c, GP_CostRateUSDPerHour__c,
						            GP_Category__c, GP_Band__c, GP_BCP__c, GP_Deal__c, GP_OMS_Deal_ID__c
						            From GP_OMS_Detail__c
						            WHERE GP_Deal__c in: setDealIDs ];
     }

    private void deleteProjectExistingRecord(){

        if(lstProjectBudgetToDelete.size()>0){
            delete lstProjectBudgetToDelete ;
        }
        if(lstProjectExpenseToDelete.size()>0){
            delete lstProjectExpenseToDelete ;
        }
    }

    private void createProjectExpense(fflib_SObjectUnitOfWork uow,
        map < string, list < GP_OMS_Detail__c >> mapProjectIdtoOMSDetailListofExpense) {

        if (mapProjectIdtoOMSDetailListofExpense == null ||
            mapProjectIdtoOMSDetailListofExpense.size() == 0) {
            return;
        }

        for (string projectId: mapProjectIdtoOMSDetailListofExpense.keyset()) {

            for (GP_OMS_Detail__c omsDetail: mapProjectIdtoOMSDetailListofExpense.get(projectId)) {

                GP_Project_Expense__c objPrjExpense = new GP_Project_Expense__c();

                objPrjExpense.GP_Project__c = projectId;
                objPrjExpense.GP_Expenditure_Type__c = omsDetail.GP_Expense_Type__c;
                objPrjExpense.GP_Amount__c = omsDetail.GP_Expense_Amount__c;
                objPrjExpense.GP_Location__c = omsDetail.GP_Location__c;
                objPrjExpense.GP_Expense_Category__c = omsDetail.GP_Category__c;
                objPrjExpense.GP_Data_Source__c = 'OMS';
                lstProjectExpenseToInsert.add(objPrjExpense);
            }
        }

        
    }

    private void createProjectBudget(fflib_SObjectUnitOfWork uow,
        map < string, list < GP_OMS_Detail__c >> mapProjectIdtoOMSDetailListofBudget) {

        if (mapProjectIdtoOMSDetailListofBudget == null ||
            mapProjectIdtoOMSDetailListofBudget.size() == 0) {
            return;
        }

        for (string projectId: mapProjectIdtoOMSDetailListofBudget.keyset()) {

            for (GP_OMS_Detail__c omsDetail: mapProjectIdtoOMSDetailListofBudget.get(projectId)) {

               GP_Project_Budget__c objPrjBudget = new GP_Project_Budget__c();

                objPrjBudget.GP_Project__c = projectId;
                //objPrjBudget.GP_Budget_Master__c = mapOfBudgetCodeToWorkLocation.get(omsDetail.GP_Work_Location_Code__c).id;
                objPrjBudget.GP_Track__c = omsDetail.GP_Track__c;
                objPrjBudget.GP_Band__c = omsDetail.GP_Band__c;
                objPrjBudget.GP_Country__c = omsDetail.GP_Country__c;
                objPrjBudget.GP_Effort_Hours__c = omsDetail.GP_Effort_in_Hours__c;
                objPrjBudget.GP_Total_Cost__c = omsDetail.GP_TotalCost__c;

                objPrjBudget.GP_Data_Source__c = 'OMS';
                //objPrjBudget.GP_TCV__c = omsDetail.GP_TCVInUSD__c;
                objPrjBudget.GP_TCV__c = omsDetail.GP_TotalCost__c;
                objPrjBudget.GP_Cost_Rate__c = omsDetail.GP_CostRateUSDPerHour__c;
                objPrjBudget.GP_Product__c = omsDetail.GP_Track__c;
                lstProjectBudgetToInsert.add(objPrjBudget);
            }
        }

    }
}