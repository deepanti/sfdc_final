@isTest
public class VIC_AdminApprovalSRepContrlTest {

    public static OpportunityLineItem  objOLI;     
    public static user objuser;
    public static list<id> lstUserid= new list<id>();
    public static list<Target_Achievement__c> lstInc = new list<Target_Achievement__c>();
    static  testmethod void VIC_AdminApprovalSRepContrl(){
        LoadData();
        lstUserid.add(objuser.id);
        VIC_AdminApprovalSRepContrl obj = new VIC_AdminApprovalSRepContrl();
        
        VIC_AdminApprovalSRepContrl.fetchOLIDataByUser(lstUserid); 
        VIC_AdminApprovalSRepContrl.fetchUser('test');
        VIC_AdminApprovalSRepContrl.getAdminUserDatawrapper();
        VIC_AdminApprovalSRepContrl.getoppdatawrapper();
       System.assertEquals(200,200);

    }
     static  testmethod void VIC_AdminApprovalDataManagerNCtlr(){
        LoadData();
        VIC_AdminApprovalDataManagerCtlr obj = new VIC_AdminApprovalDataManagerCtlr();
       
         VIC_AdminApprovalDataManagerCtlr.getInitLoadPageData('false','HR - OnHold');
       
         system.assertequals(200,200);

    }
     static void LoadData()
    {
         VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
         vicInfo.VIC_Process_Year__c=2018;
        vicInfo.VIC_Annual_Process_Year__c=2018;
         insert vicInfo;
        
         objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn11@jdjhdg.com','System administrator','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
            
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
            
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
            
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
            
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
            
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';    
        insert planobj1;
                
            
        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser1.id;
        objopp.Sales_country__c='Canada';
        
        insert objOpp;
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_Contract_Term__c=24;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;   
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.vic_IO_TS__c ='TS'; 
        objOLI.TCV__c=100;
        insert objOLI;
                
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_TS'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
        
        Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='Profitable_Bookings_IO'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
        
        Master_Plan_Component__c masterPlanComponentObj2=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj2.vic_Component_Code__c='New_Logo_Bonus'; 
        masterPlanComponentObj2.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj2;
        
        Master_Plan_Component__c masterPlanComponentObj3=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj3.vic_Component_Code__c='Kickers'; 
        masterPlanComponentObj3.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj3;
            
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
            
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        
        Target_Component__c targetComponentObj1=VIC_CommonTest.getTargetComponent();
        targetComponentObj1.Target__C=targetObj.id;
        targetComponentObj1.Master_Plan_Component__c=masterPlanComponentObj1.id;
        insert targetComponentObj1;
        
        Target_Component__c targetComponentObj2=VIC_CommonTest.getTargetComponent();
        targetComponentObj2.Target__C=targetObj.id;
        targetComponentObj2.Master_Plan_Component__c=masterPlanComponentObj2.id;
        insert targetComponentObj2;
        
        Target_Component__c targetComponentObj3=VIC_CommonTest.getTargetComponent();
        targetComponentObj3.Target__C=targetObj.id;
        targetComponentObj3.Master_Plan_Component__c=masterPlanComponentObj3.id;
        insert targetComponentObj3;
        
        Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc.vic_Status__c ='HR - OnHold';
        insert objInc;
        
        
        
        Target_Achievement__c ObjInc1= VIC_CommonTest.fetchIncentive(targetComponentObj1.id,1000);
        ObjInc1.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc1.vic_Status__c ='HR - OnHold';
        insert objInc1;
        
        Target_Achievement__c ObjInc2= VIC_CommonTest.fetchIncentive(targetComponentObj2.id,1000);
        ObjInc2.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc2.vic_Status__c ='HR - OnHold';
        insert objInc2;
        
        Target_Achievement__c ObjInc3= VIC_CommonTest.fetchIncentive(targetComponentObj3.id,1000);
        ObjInc3.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc3.vic_Status__c ='HR - OnHold';
        insert objInc3;
        lstInc.add(objInc);
    }
}