/* Created by Abhishek Maurya
   
   Use- this batch class is used to update field(Sync_to_Marketo__c) avilable on Account,Contact,Opportunity,Lead and Campaign in one go for all records.   
   
   Reviewed by- Amit Bhardwaj, Mandeep Kaur 
*/ 


global class marketoUpdateBatch implements Database.Batchable<sObject>{
    
    global String query;
    String email='Amit.Bhardwaj@genpact.com';
    
    //Query all the record of specific object
    global Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);}
    
    //Execute method to update field
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        List<sObject> recordList= new List<sObject>();
        if(!scope.isEmpty())
        {
            if(Schema.Account.SObjectType==scope[0].getSobjectType())
            {
                for(sObject s : scope){
                    Account acc = (Account)s;
                    acc.Sync_to_Marketo__c=true;
                    recordList.add(acc);
                }
            }
            else if(Schema.Lead.SObjectType==scope[0].getSobjectType())
            {
                for(sObject s : scope){
                    Lead ld = (Lead)s;
                    ld.Sync_to_Marketo__c=true;
                    recordList.add(ld);
                }
            }
            else if(Schema.Contact.SObjectType==scope[0].getSobjectType())
            {
                for(sObject s : scope){
                    Contact con = (Contact)s;
                    con.Sync_to_Marketo__c=true;
                    recordList.add(con);
                }
            }
             else if(Schema.Opportunity.SObjectType==scope[0].getSobjectType())
            {
                for(sObject s : scope){
                    Opportunity opp = (Opportunity)s;
                    opp.Sync_to_Marketo__c=true;
                    recordList.add(opp);
                }
            }
             else if(Schema.Campaign.SObjectType==scope[0].getSobjectType())
            {
                for(sObject s : scope){
                    Campaign camp = (Campaign)s;
                    camp.Sync_to_Marketo__c=true;
                    recordList.add(camp);
                }
            }
            else{
                system.debug('Invalid Object Type');
            }
            if(!recordList.isEmpty())
            {
                system.debug('List of Record to Update'+recordList);
                update recordList;
            }
            else
            {
                system.debug('List has no record to update');
            }
        }
        else
        {
            system.debug('Database loactor did not find any recocrd to update');
        }
        
    }
    // finish method to send email after completion of Batch Job
    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setSenderDisplayName('Batch Processing');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Batch Process has completed');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}