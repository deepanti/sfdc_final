Public Class RemovePackagelicense{
        
        Public static void doRemoveLicense(Set<id> inactiveUser) {
            if(inactiveUser != null && inactiveUser.size() > 0 ){
                Set<Id> packLicnSet = new Set<Id>();           
                //SOQL to get the package license/installed package Id
                List<PackageLicense> pckgLicenseList = [SELECT Id, NamespacePrefix FROM PackageLicense WHERE NamespacePrefix = 'RevenueStormRB' OR NamespacePrefix = 'RevenueStormPP'];
                system.debug('pckgLicenseList===' +pckgLicenseList);
                
                if(pckgLicenseList != null && pckgLicenseList.size() > 0){
                    List<UserPackageLicense> uplList = new List<UserPackageLicense>([select id,PackageLicenseid,Userid from UserPackageLicense where PackageLicenseid IN :pckgLicenseList AND userId IN :inactiveUser]);
                    
                    List<UserPackageLicense> upl_removeList = new List<UserPackageLicense>();
                    
                    for(UserPackageLicense ux : uplList) {
                        //Removing the license to the current user
                        upl_removeList.add(ux);
                    }
                                        
                    if(!upl_removeList.isEmpty()) {
                        try{
                            delete upl_removeList; //DML operation
                        }catch(Exception e){
                            system.debug(' delete exception: '+ ' \n exception message :'+e.getMessage());
                        }
                    }
                }// end of Inactive user list
            }   
        }
}