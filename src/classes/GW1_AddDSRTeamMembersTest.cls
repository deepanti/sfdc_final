/*
  This class is used to test GW1_AddDSRTeamMembers
  --------------------------------------------------------------------------------------
  Name                 Date                 Version                      
  --------------------------------------------------------------------------------------
  Rishi Patel                    3/17/2016                    1.0  
  --------------------------------------------------------------------------------------
 */
@istest
public class GW1_AddDSRTeamMembersTest {


    public static testMethod void testMyController() {
       LoadData();
        GW1_CommonTracker.createTriggerCustomSetting();
        PageReference pageRef = Page.GW1_AddDSRTeamMembers;
        Test.setCurrentPage(pageRef);
        GW1_DSR__c objDsr = new GW1_DSR__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(objDsr);
        GW1_AddDSRTeamMembers objController = new GW1_AddDSRTeamMembers(sc);
        objController.team = 'Team 1';
        objController.Geography = 'G1';
        objController.vertical = 'Test';
objController.searchemployeeID='400221179,12121212';
        //////////innitialising picklist filters////////////////
    
        ////////////////////////////////////////////////////////

        objController.fetchUsers(); //testing search functionality
        
        //Shubham 6/4/2016
        list<GW1_AddDSRTeamMembers.WrapperOfUser> listWrapperOfUser = objController.listWrapperOfUser;
        system.debug('listWrapperOfUser (in test) ' + listWrapperOfUser);
        objController.shortlistedUserIndex = '0';
        /// Shubham End

        objController.shortListUser();
        //objController.checkedRow = 0;
        //objController.checked();


        //objcontroller.Unchecked();


        //objController.checkedRow = 0; //user select first row
        
        
        objController.saveAndClose();
        objController.delRowNum = 0;
        objController.delRow();
        objController.caseCreation();


    }
       public static testMethod void testMyController2() {
       LoadData();
        GW1_CommonTracker.createTriggerCustomSetting();
        PageReference pageRef = Page.GW1_AddDSRTeamMembers;
        Test.setCurrentPage(pageRef);
        GW1_DSR__c objDsr = new GW1_DSR__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(objDsr);
        GW1_AddDSRTeamMembers objController = new GW1_AddDSRTeamMembers(sc);
        objController.team = 'Team 1';
        objController.Geography = 'G1';
        objController.vertical = 'Test';
objController.searchemployeeID='';
        //////////innitialising picklist filters////////////////
    
        ////////////////////////////////////////////////////////

        objController.fetchUsers(); //testing search functionality
        
        //Shubham 6/4/2016
        list<GW1_AddDSRTeamMembers.WrapperOfUser> listWrapperOfUser = objController.listWrapperOfUser;
        system.debug('listWrapperOfUser (in test) ' + listWrapperOfUser);
        objController.shortlistedUserIndex = '0';
        /// Shubham End

        objController.shortListUser();
        //objController.checkedRow = 0;
        //objController.checked();


        //objcontroller.Unchecked();


        //objController.checkedRow = 0; //user select first row
        
        
        objController.saveAndClose();
        objController.delRowNum = 0;
        objController.delRow();
        objController.caseCreation();


    }

    static void LoadData()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name = 'Quest Super Admin'];
        User u = new User(Alias = 'standt', Email = 'standarduser@testorg.com',
                          EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                          LocaleSidKey = 'en_US', ProfileId = p.id,
                          TimeZoneSidKey = 'America/Los_Angeles', UserName = 'standarduser345@testorg.com');
        u.GW1_Team__c = 'Team 1';
        u.GW1_Geography__c = 'G1';
        u.Industry_Vertical__c = 'Test';
        u.GW1_Role__c='Test Role';
        
        insert u;
		
    }




}