public class TypeAheadHelper {
    
    @AuraEnabled 
    public static String searchDB(String objectName, String fld_API_Text, String fld_API_Val, 
                                  Integer lim,String fld_API_Search,String searchText, String whereClause){
                                      
                                      searchText='\'%' + String.escapeSingleQuotes(searchText.trim()) + '%\'';
                                      
                                      
                                      String query = 'SELECT '+fld_API_Text+' ,'+fld_API_Val+
                                          ' FROM '+objectName+
                                          ' WHERE '+fld_API_Search+' LIKE '+searchText;
                                      if(whereClause != null && whereClause!='')
                                          query += ' AND ' + whereClause;
                                      query +=' LIMIT '+lim;
                                      
                                      System.debug(query);
                                      
                                      List<sObject> sobjList = Database.query(query);
                                      List<ResultWrapper> lstRet = new List<ResultWrapper>();
                                      
                                      for(SObject s : sobjList){
                                          ResultWrapper obj = new ResultWrapper();
                                          obj.objName = objectName;
                                          obj.text = String.valueOf(s.get(fld_API_Text)) ;
                                          obj.val = String.valueOf(s.get(fld_API_Val))  ;
                                          lstRet.add(obj);
                                      } 
                                      return JSON.serialize(lstRet) ;
                                  }
    
    public class ResultWrapper{
        public String objName {get;set;}
        public String text{get;set;}
        public String val{get;set;}
    }
    
}