/*   ==============================================================
    Developer       :   Arjun Srivastava
    Client          :   Genpact
    Created Date    :   12/June/2014
    Modified Date   :   24/June/2014  
    Description     :   This Controller is used to schedule the quota attainment batch class.
====================================================================== */
global class QuotaScheduler implements Schedulable
{
    global void execute(SchedulableContext SC)
    {  
        QuotaDeleteBatchclass reassign = new QuotaDeleteBatchclass('');        
       
        ID batchprocessid = Database.executeBatch(reassign,200);   // running a batch with batch size 200
    }
}