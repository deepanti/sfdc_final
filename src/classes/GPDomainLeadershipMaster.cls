public class GPDomainLeadershipMaster extends fflib_SObjectDomain {

    public GPDomainLeadershipMaster(List < GP_Leadership_Master__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainLeadershipMaster(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Leadership_Master__c.SObjectType
    };

    public override void onBeforeInsert() {
        onApplyDefaults(null);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updatePIDApproverFinance(uow, null);
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        onApplyDefaults(oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //updatePIDApproverFinance(uow, oldSObjectMap);
        //uow.commitWork();

    }
    /*public override void onValidate() {
        // Validate GP_Leadership_Master__c 
        //for(GP_Leadership_Master__c  ObjProject : (List<GP_Leadership_Master__c >) Records) {
          //  }
    }
    */
    public void onApplyDefaults(Map < Id, SObject > oldSObjectMap) {

        // Apply defaults to GP_Leadership_Master__c 
        set < string > setSDOOracleId = new set < String > ();
        GP_Leadership_Master__c objOldProject;
        for (GP_Leadership_Master__c objLeaderShipMaster: (List < GP_Leadership_Master__c > ) Records) {
            objOldProject = oldSObjectMap != null ? (GP_Leadership_Master__c) oldSObjectMap.get(objLeaderShipMaster.id) : null;
            if (!string.isBlank(objLeaderShipMaster.GP_SDO_Oracle_Id__c) &&
                (objOldProject == null || objOldProject.GP_SDO_Oracle_Id__c != objLeaderShipMaster.GP_SDO_Oracle_Id__c)) {
                setSDOOracleId.add(objLeaderShipMaster.GP_SDO_Oracle_Id__c);
            }
        }

        if (setSDOOracleId.size() > 0) {
            map < String, GP_Work_Location__c > mpOracleIdToSDO = new map < String, GP_Work_Location__c > ();
            list < GP_Work_Location__c > lstSDOs = GPSelectorWorkLocationSDOMaster.selectWOrkLocationAndSDOByOracleID(setSDOOracleId);
            for (GP_Work_Location__c objSDO: lstSDOs) {

                if (!String.isBlank(objSDO.GP_Oracle_Id__c)) {
                    mpOracleIdToSDO.put(objSDO.GP_Oracle_Id__c, objSDO);
                }
            }

            for (GP_Leadership_Master__c objLeaderShipMaster: (List < GP_Leadership_Master__c > ) Records) {
                objOldProject = oldSObjectMap != null ? (GP_Leadership_Master__c) oldSObjectMap.get(objLeaderShipMaster.id) : null;
                if (!string.isBlank(objLeaderShipMaster.GP_SDO_Oracle_Id__c) && mpOracleIdToSDO.get(objLeaderShipMaster.GP_SDO_Oracle_Id__c) != null) {
                    objLeaderShipMaster.GP_Work_Location_SDO_Master__c = mpOracleIdToSDO.get(objLeaderShipMaster.GP_SDO_Oracle_Id__c).id;
                }
            }
        }

    }
}