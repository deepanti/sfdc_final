@istest
public class clonetestslass
{
    public static testMethod void method()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
       Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct1 =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,25,24000);
        Integer ar = [Select count() from OpportunityProduct__c where Opportunityid__c = : oOpportunity.id]; 
        RevenueSchedule__c revenueschedlue= GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct1.Id,1,12,12,10000);
        ProductSchedule__c ProductSchedule=GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct1.Id,revenueschedlue.id);
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Service_Line_Category_Oli__c=oOpportunityProduct1.Service_Line_Category_Oli__c;
        oOpportunityProduct.Service_line_OLI__c=oOpportunityProduct1.Service_line_OLI__c;
        oOpportunityProduct.Product_Autonumber__c='OLI - '+(ar+1);  
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = oOpportunityProduct1.Product_Family_OLI__c;
        oOpportunityProduct.Product__c = oOpportunityProduct1.Product__c;
        oOpportunityProduct.COE__c = oOpportunityProduct1.COE__c;
        oOpportunityProduct.P_L_SUB_BUSINESS__c = oOpportunityProduct1.P_L_SUB_BUSINESS__c;
        oOpportunityProduct.DeliveryLocation__c = oOpportunityProduct1.DeliveryLocation__c;
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = oOpportunityProduct1.LocalCurrency__c;
        oOpportunityProduct.RevenueStartDate__c = oOpportunityProduct1.RevenueStartDate__c;
        oOpportunityProduct.ContractTerminmonths__c = oOpportunityProduct1.ContractTerminmonths__c;
        oOpportunityProduct.SalesExecutive__c = oOpportunityProduct1.SalesExecutive__c;
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = oOpportunityProduct1.Quarterly_FTE_1st_month__c;
        oOpportunityProduct.FTE_4th_month__c =oOpportunityProduct1.FTE_4th_month__c;
        oOpportunityProduct.FTE_7th_month__c =oOpportunityProduct1.FTE_7th_month__c;
        oOpportunityProduct.FTE_10th_month__c =oOpportunityProduct1.FTE_10th_month__c;
        oOpportunityProduct.OpportunityId__c = oOpportunityProduct1.OpportunityId__c;
        oOpportunityProduct.TCVLocal__c =oOpportunityProduct1.TCVLocal__c;
        oOpportunityProduct.CurrentYearRevenue_CYR__c=97.32928443510083;
        oOpportunityProduct.NextYearRevenue_NYR__c=117.92437477770076;
        oOpportunityProduct.CurrentYearRevenueLocal__c=18750;
        oOpportunityProduct.NextYearRevenueLocal__c=6250;
        
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        insert oOpportunityProduct;
         test.startTest();
        Test.setCurrentPage(Page.GENvEditOpportunityProduct_Clone);
        ApexPages.currentPage().getParameters().put('Id', oOpportunityProduct.Id);
        GENcOpportunityProductClone1 clas=new GENcOpportunityProductClone1(new ApexPages.StandardController(oOpportunityProduct));
        PageReference pageRef = Page.GENvEditOpportunityProduct_Clone;
        clas.revenueRollingYearIndex =0;
        clas.revenueRollingYearToEdit=1;
        //GENcOpportunityProductClone1.RevenueRollingYearWrapper  oRevenueRollingYearWrapper1  = new GENcOpportunityProductClone1.RevenueRollingYearWrapper(1);
        //oRevenueRollingYearWrapper1.revSchedule = oRevenueSchedule;
        //clas.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 18000;
        //clas.revenueRYWrapperList[1].revSchedule.RollingYearRevenueLocal__c= 19000;
        
        clas.PFamily_lst();
        clas.ServiceLine_lst();
        clas.ProductFamilyFromCatalog();
        clas.disableProduct();
        clas.ProfilesOptionsFetch();
        clas.getautoNumber();
        clas.getBDRepOptions();
        clas.getCurrencyIsoCodes();
        clas.validateSchedule();
        clas.updateSchedule();
        clas.createRevProdList(clas.revenueRYWrapperList);
        clas.revenuedate_update();
        Decimal d=clas.getNoOfInstallment();
        clas.deleteProductSchedule1();
        clas.deleteProductSchedule();
        boolean b=clas.getPricemarginRendered();
        clas.setPricemarginRendered(true,true);
        Boolean bb=clas.getPriorityTypeRendered();
        clas.setPriorityTypeRendered(true);
        boolean bo=clas.getHasScheculeSectionRendered();
        clas.setHasScheculeSectionRendered(true);
        //clas.setHasAutoPopulateDisabled (true);
       // boolean boo=clas.getHasAutoPopulateDisabled  () ;
        //clas.createSchedule();
         test.stopTest();
    }
    
    
     public static testMethod void method2()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
       Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct1 =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,25,24000);
        Integer ar = [Select count() from OpportunityProduct__c where Opportunityid__c = : oOpportunity.id]; 
        RevenueSchedule__c revenueschedlue= GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct1.Id,1,12,12,10000);
        ProductSchedule__c ProductSchedule=GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct1.Id,revenueschedlue.id);
        ProductSchedule__c ProductSchedule2=GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct1.Id,revenueschedlue.id);
        
        
        try {
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        
        
        oOpportunityProduct.Service_Line_Category_Oli__c=oOpportunityProduct1.Service_Line_Category_Oli__c;
        oOpportunityProduct.Service_line_OLI__c=oOpportunityProduct1.Service_line_OLI__c;
        oOpportunityProduct.Product_Autonumber__c='OLI - '+(ar+1);  
        //oOpportunityProduct.Product_Group_OLI__c='Others';
        oOpportunityProduct.Product_Family_OLI__c = oOpportunityProduct1.Product_Family_OLI__c;
        oOpportunityProduct.Product__c = oOpportunityProduct1.Product__c;
        oOpportunityProduct.COE__c = oOpportunityProduct1.COE__c;
        oOpportunityProduct.P_L_SUB_BUSINESS__c = oOpportunityProduct1.P_L_SUB_BUSINESS__c;
        oOpportunityProduct.DeliveryLocation__c = oOpportunityProduct1.DeliveryLocation__c;
        //oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = oOpportunityProduct1.LocalCurrency__c;
        oOpportunityProduct.RevenueStartDate__c = oOpportunityProduct1.RevenueStartDate__c;
        oOpportunityProduct.ContractTerminmonths__c = oOpportunityProduct1.ContractTerminmonths__c;
        oOpportunityProduct.SalesExecutive__c = oOpportunityProduct1.SalesExecutive__c;
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = oOpportunityProduct1.Quarterly_FTE_1st_month__c;
        oOpportunityProduct.FTE_4th_month__c =oOpportunityProduct1.FTE_4th_month__c;
        oOpportunityProduct.FTE_7th_month__c =oOpportunityProduct1.FTE_7th_month__c;
        oOpportunityProduct.FTE_10th_month__c =oOpportunityProduct1.FTE_10th_month__c;
        oOpportunityProduct.OpportunityId__c = oOpportunityProduct1.OpportunityId__c;
        oOpportunityProduct.TCVLocal__c =oOpportunityProduct1.TCVLocal__c;
        oOpportunityProduct.CurrentYearRevenue_CYR__c=97.32928443510083;
        oOpportunityProduct.NextYearRevenue_NYR__c=117.92437477770076;
        oOpportunityProduct.CurrentYearRevenueLocal__c=18750;
        oOpportunityProduct.NextYearRevenueLocal__c=6250;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        
        
       
        list<ProductSchedule__c> opp = new list<ProductSchedule__c>();
        opp.add(ProductSchedule);
        opp.add(ProductSchedule2);
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        //insert oOpportunityProduct;
            


       //update oOpportunityProduct;
        Test.setCurrentPage(Page.GENvEditOpportunityProduct_Clone);
        ApexPages.currentPage().getParameters().put('Id', oOpportunityProduct.Id);
        GENcOpportunityProductClone1 clas=new GENcOpportunityProductClone1(new ApexPages.StandardController(oOpportunityProduct));
        PageReference pageRef = Page.GENvEditOpportunityProduct_Clone;
        oOpportunityProduct.CurrentYearRevenue_CYR__c = 0;
        oOpportunityProduct.NextYearRevenue_NYR__c = 0;
                        oOpportunityProduct.CurrentYearRevenueLocal__c = 0;
                        oOpportunityProduct.NextYearRevenueLocal__c = 0;       
       // update oOpportunityProduct;
        
         test.startTest();
        clas.saveopty(opp);
         test.stopTest();
        
        }
        catch(Exception e){
            System.debug(e);
        }

    }
    
}