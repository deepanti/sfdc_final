@isTest
public class GPSetKeyForEmployeeHRHistoryTrkr {
    @testSetup
    static void testData(){
        //Select Id,GP_Attribute2__c,GP_ASSIGNMENT_ID__c, 
        //GP_ASGN_EFFECTIVE_START__c from GP_Employee_HR_History__c where GP_Attribute2__c=null
        GP_Employee_HR_History__c objEmp=new GP_Employee_HR_History__c();
        objEmp.GP_Attribute2__c=null;
        objEmp.GP_ASSIGNMENT_ID__c='123456';
        objEmp.GP_ASGN_EFFECTIVE_START__c=system.today();
        insert objEmp;
    }
    
    @isTest
    public static   void test(){
        
        Test.startTest();
        GPSetKeyForEmployeeHRHistory obj=new GPSetKeyForEmployeeHRHistory();
        DataBase.executeBatch(obj);
        Test.stopTest();
        
    }
    
    
    
}

