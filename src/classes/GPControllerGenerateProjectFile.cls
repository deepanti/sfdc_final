// Add Limits.getHeapSize() & Limits.getLimitHeapSize() and determine the pain area. 
public without sharing class GPControllerGenerateProjectFile {    
    private string projectId;
    private string selectedStatus;
    private List<GP_Project_Version_History__c> listOfProjectVersionHistory;
    private GP_Project__C objProject,oldProject;
    
    private Static Map<String,String> mapOfDateNames;
    
    //public ProjectData objProjectData {get;set;}
    public String xmlheader {get;set;} 
    public GP_Project__c objProjectCurrent {set;get;}
    public static Map<String,String> mapOfProjectStage{get;set;}    
    
    private List<map<string,string>> lstResourceAllocationCurrent;
    private List<map<string,string>> lstResourceAllocationOld;
    private map<string, Schema.DisplayType> mapFieldtoType;
    
    private Map<String, List<map<string,string>>> mapOfListOfResourcesOld;
    private Map<String, List<map<string,string>>> mapOfListOfResourcesNew;
    
    // START: Variables for showing data on VF page.
    public list<GPComparisonStructure> lstProjectdiff {get;set;}    
    public List<map<string,string>> projectBudgetCurrent;
    //public List<GP_Project_Budget__c> projectBudgetCurrentS {get;set;}
    public List<map<string,string>> projectBudgetCurrentDS {get;set;}
    public List<map<string,string>> projectBudgetOld;
    //public List<GP_Project_Budget__c> projectBudgetOlds {get;set;}
    public List<map<string,string>> projectBudgetOldDS {get;set;}
    public List<map<string,string>> projectBillingMilestoneCurrent {get;set;}
    //public List<GP_Billing_Milestone__c> projectBillingMilestoneCurrentS {get;set;}
    public List<map<string,string>> projectBillingMilestoneCurrentDS {get;set;}
    public List<map<string,string>> projectBillingMilestoneOld;
    //public List<GP_Billing_Milestone__c> projectBillingMilestoneOldS {get;set;}
    public List<map<string,string>> projectBillingMilestoneOldDS {get;set;}
    public list<BudgetDetails> lstprojectBudetDetails {get;set;}
    public list<BudgetDetails> lstprojectBudetDetailsOld {get;set;}    
    public List<GP_Project_Leadership__c> projectLeadershipCurrent {get;set;}
    //public List<map<string,string>> lstprojectAddress {get;set;}
    public List<GP_Project_Address__c> lstprojectAddress {get;set;}
    //public List<map<string,string>> projectWorkLocation {get;set;}
    public List<GP_Project_Work_Location_SDO__c> projectWorkLocation {get;set;}
    public List<GP_Project_Work_Location_SDO__c> projectWorkLocationS {get;set;}
    public List<list<GPComparisonStructure>> lstresourceAllocation {get;set;}    
    public List<map<string,string>> projectDocument {get;set;}
    public List<GP_Project_Document__c> projectDocuments {get;set;}
    // END: Variables for showing data on VF page.
    
    public GPControllerGenerateProjectFile(ApexPages.StandardController stdController) {
        initialization();
    }
    
    public GPControllerGenerateProjectFile() {
        initialization();
    }
    
    private void initialization(){
        xmlheader ='<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
        mapFieldtoType = new map<string,Schema.DisplayType>();
        mapOfProjectStage = new Map<String,String>{'APPROVED' => 'Approved',
            '1026' => 'Revenue and Cost Charging',
            '1009' => 'Billing and Cost Charging',
            '1010' => 'Cost Charging only',
            '1008' => 'Approved Support',
            'CLOSED' => 'Closed'};
        
        mapOfDateNames = new Map<String,String>{'01' => 'Jan',
            '02' => 'Feb',
            '03' => 'Mar',
            '04' => 'Apr',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Aug',
            '09' => 'Sep',
            '10' => 'Oct',
            '11' => 'Nov',
            '12' => 'Dec'};
                
        getParameters();
        fetchVersionHistoryrecords();
        
        getHeapLimitDebugs(82);
        
        createProjectFileData();
    }
    
    private void getParameters(){
        projectId =  Apexpages.currentPage().getParameters().get('id'); 
        selectedStatus =  Apexpages.currentPage().getParameters().get('selectedStatus');
    }    
    
    private void createProjectFileData(){
        GP_Project_Version_History__c objCurrentVerSion, objLastVersion;
        
        GPComparisonGenerator objGPComparisonGenerator = new GPComparisonGenerator();
        
        if(listOfProjectVersionHistory.size()>0){
            objCurrentVerSion = listOfProjectVersionHistory[0];
            
            if(listOfProjectVersionHistory.size()>1){ 
                objLastVersion = listOfProjectVersionHistory[1];
            }
            
            // Compare Project
            objProject = (GP_Project__c)JSON.deserialize(objCurrentVerSion.GP_Project_JSON__c, GP_Project__c.class);
            
            getHeapLimitDebugs(108);
            
            if(objLastVersion != null)
                oldProject = (GP_Project__c)JSON.deserialize(objLastVersion.GP_Project_JSON__c, GP_Project__c.class);
            
            getHeapLimitDebugs(113);
            
            if(objProject != null && oldProject != null) {
                //system.debug('q');
                objGPComparisonGenerator.setSourceObjectList((map<string,string>)JSON.deserialize(objLastVersion.GP_Project_JSON__c, map<string,string>.class));
                objGPComparisonGenerator.setDestinationObjectList((map<string,string>)JSON.deserialize(objCurrentVerSion.GP_Project_JSON__c, map<string,string>.class));
            } else {
                objGPComparisonGenerator.setSourceObjectList((map<string,string>)JSON.deserialize(objCurrentVerSion.GP_Project_JSON__c, map<string,string>.class));
                objGPComparisonGenerator.setDestinationObjectList(null);
            }
            
            objGPComparisonGenerator.setFieldSet(GPCommon.getFieldSetAsSetOfString('GP_Project_Comparison_fields','GP_Project__C'));
            objGPComparisonGenerator.setObjectType('GP_Project__c');
            
            objGPComparisonGenerator.sefieldLabelMap(getfieldSetMap('GP_Project__c')); 
            objGPComparisonGenerator.sefieldTypeMap(mapFieldtoType); 
            lstProjectdiff = objGPComparisonGenerator.generateComparisonResult();
            
            getHeapLimitDebugs(124);
            
            fetchProjectRelatedDetails(objCurrentVerSion,objLastVersion );
            projectLeadershipCurrent = getProjectLeadership();
            
            lstresourceAllocation = getListResource();
            
            if(objCurrentVerSion.GP_Address_JSON__c != null) {
            	// Replace by query
            	lstprojectAddress = getProjectAddress();                
            }
        } 
        else{
            //ERROR  Version history
        }        
    }
    
    private  void fetchProjectWorkLocation() {}
    
    private void fetchProjectRelatedDetails(GP_Project_Version_History__c  objCurrentVerSion,  GP_Project_Version_History__c objLastVersion){
        
        for(GP_Project_Version_Line_Item_History__c objVersionLineItem : objCurrentVerSion.GP_Project_Version_Line_Item_History__r) {
            
            if(objVersionLineItem.GP_Related_Record_Type__c == 'Work Location') {               
                
                // Replace with query.
                projectWorkLocation = getProjectWorkLocations();
                getHeapLimitDebugs(151);
                //System.debug('==projectWorkLocation=='+projectWorkLocation);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Document') {
                                
                // Replace with query.
                projectDocuments = getProjectDocuments();
                getHeapLimitDebugs(158);
                //System.debug('==projectDocuments=='+projectDocuments);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Budget'){                
                
                projectBudgetCurrent = ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                                                              Map<String,List<Map<String,String>>>.class)).get('Project Budget');
                
                //system.debug('==projectBudgetCurrent=='+projectBudgetCurrent);
                
                projectBudgetCurrentDS = displayData(projectBudgetCurrent, 'Project Budget');
                //System.debug('==projectBudgetCurrentDS=='+projectBudgetCurrentDS);
                getHeapLimitDebugs(170);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Billing Milestone'){
                                
                projectBillingMilestoneCurrent = ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                                                                        Map<String,List<Map<String,String>>>.class)).get('Billing Milestone');                
                
                projectBillingMilestoneCurrentDS = displayData(projectBillingMilestoneCurrent, 'Billing Milestone');
                //System.debug('==projectBillingMilestoneCurrentDS=='+ projectBillingMilestoneCurrentDS);
                getHeapLimitDebugs(179);
                
            }
            else if(objVersionLineItem.GP_Related_Record_Type__c == 'Expense') {
                
                lstprojectBudetDetails = createBudgetDetails (
                    ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                            Map<String,List<Map<String,String>>>.class)).get('Project Expense'));
                getHeapLimitDebugs(186);
                
            }
            else if(objVersionLineItem.GP_Related_Record_Type__c == 'Resource Allocation'){
                                
                Map<String, String> mapOfResourceAllocationNew = (Map<String, String>)JSON.deserialize(getVersionLineItems(objVersionLineItem, 'Resource Allocation'), Map<String, String>.class);
                //System.debug('==mapOfResourceAllocationNew=='+mapOfResourceAllocationNew);
                mapOfListOfResourcesNew = new Map<String, List<map<string,string>>>();
                
                for(String fieldName : mapOfResourceAllocationNew.keySet()) {
                    if(mapOfResourceAllocationNew.get(fieldName) != null) {
                        List<map<string,string>> lstResourceAllocationNew = ((Map<String,List<Map<String,String>>>)JSON.deserialize(mapOfResourceAllocationNew.get(fieldName), Map<String,List<Map<String,String>>>.class)).get('Resource Allocation');
                        mapOfListOfResourcesNew.put(fieldName, lstResourceAllocationNew);
                    }
                }
                getHeapLimitDebugs(202);
                //System.debug('==mapOfListOfResourcesNew=='+mapOfListOfResourcesNew);
            }
        }
        
        if(objLastVersion == null) return;
        
        for(GP_Project_Version_Line_Item_History__c objVersionLineItem : objLastVersion.GP_Project_Version_Line_Item_History__r) {
            
            if(objVersionLineItem.GP_Related_Record_Type__c == 'Budget'){
                
                projectBudgetOld = ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                                                          Map<String,List<Map<String,String>>>.class)).get('Project Budget');                
                
                projectBudgetOldDS = displayData(projectBudgetOld, 'Project Budget');
                getHeapLimitDebugs(217);
                //System.debug('==projectBudgetOldDS=='+projectBudgetOldDS);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Billing Milestone'){                
                
                projectBillingMilestoneOld = ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                                                                    Map<String,List<Map<String,String>>>.class)).get('Billing Milestone');
                
                projectBillingMilestoneOldDS = displayData(projectBillingMilestoneOld, 'Billing Milestone');
                getHeapLimitDebugs(226);
                //System.debug('==projectBillingMilestoneOldDS=='+projectBillingMilestoneOldDS);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Expense'){
                
                lstprojectBudetDetailsOld = createBudgetDetails (
                    ((Map<String,List<Map<String,String>>>)JSON.deserialize(GPBaseProjectUtil.getSerializedVersionLineItemJSON(objVersionLineItem), 
                                                                            Map<String,List<Map<String,String>>>.class)).get('Project Expense'));
                
                getHeapLimitDebugs(235);
                
            } else if(objVersionLineItem.GP_Related_Record_Type__c == 'Resource Allocation'){
                
                Map<String, String> mapOfResourceAllocationOld = (Map<String, String>)JSON.deserialize(getVersionLineItems(objVersionLineItem, 'Resource Allocation'), Map<String, String>.class);
                
                mapOfListOfResourcesOld = new Map<String, List<map<string,string>>>();
                
                for(String fieldName : mapOfResourceAllocationOld.keySet()) {
                    if(mapOfResourceAllocationOld.get(fieldName) != null) {
                        List<map<string,string>> lstResourceAllocationOld = ((Map<String,List<Map<String,String>>>)JSON.deserialize(mapOfResourceAllocationOld.get(fieldName), Map<String,List<Map<String,String>>>.class)).get('Resource Allocation');
                        mapOfListOfResourcesOld.put(fieldName, lstResourceAllocationOld);
                    }
                }
                getHeapLimitDebugs(249);
                //System.debug('==mapOfListOfResourcesOld=='+mapOfListOfResourcesOld);
            }
        }
    }
    
    public void fetchVersionHistoryrecords(){
        
        if(selectedStatus=='Create'){
            listOfProjectVersionHistory = new list<GP_Project_Version_History__c>([SELECT Id,Name,GP_Project_JSON__c, GP_Work_Location_JSON__c,GP_Additional_SDO_JSON__c, 
                                                                                   GP_Leadership_JSON__c,  
                                                                                   GP_Budget_JSON__c, GP_Billing_Milestones_JSON__c,GP_Address_JSON__c , GP_Resource_Allocation_JSON__c, GP_Expenses_JSON__c,
                                                                                   GP_Project_Document_JSON__c,(SELECT Id, GP_Related_Record_Type__c, GP_JSON1__c, GP_JSON2__c, GP_JSON3__c, GP_JSON4__c, GP_JSON5__c, 
                                                                                                                GP_JSON7__c, GP_JSON8__c, GP_JSON9__c, GP_JSON10__c, GP_JSON11__c, GP_JSON12__c, GP_JSON6__c FROM GP_Project_Version_Line_Item_History__r)
                                                                                   FROM GP_Project_Version_History__c WHERE GP_Project__c =: projectId and GP_Project__r.GP_Oracle_Status__c = 'S' and GP_Oracle_Status__c ='S' Order by Createddate ASC Limit 1] );
        }
        else
        {
            listOfProjectVersionHistory = new list<GP_Project_Version_History__c>([SELECT Id,Name,GP_Project_JSON__c, GP_Work_Location_JSON__c,GP_Additional_SDO_JSON__c, 
                                                                                   GP_Leadership_JSON__c,  
                                                                                   GP_Budget_JSON__c, GP_Billing_Milestones_JSON__c,GP_Address_JSON__c , GP_Resource_Allocation_JSON__c, GP_Expenses_JSON__c,
                                                                                   GP_Project_Document_JSON__c,(SELECT Id, GP_Related_Record_Type__c, GP_JSON1__c, GP_JSON2__c, GP_JSON3__c, GP_JSON4__c, GP_JSON5__c, 
                                                                                                                GP_JSON7__c, GP_JSON8__c, GP_JSON9__c, GP_JSON10__c, GP_JSON11__c, GP_JSON12__c, GP_JSON6__c FROM GP_Project_Version_Line_Item_History__r)
                                                                                   FROM GP_Project_Version_History__c WHERE GP_Project__c =: projectId and GP_Project__r.GP_Oracle_Status__c = 'S' and GP_Oracle_Status__c ='S' Order by Createddate DESC Limit 2]);
        }
    }
    
    private list<GP_Project_Leadership__c> getProjectLeadership() {        
        return [ Select id , GP_Type_of_Leadership__c,GP_Employee_ID__c, GP_Employee_ID__r.Name, GP_Employee_ID__r.GP_Final_OHR__c, GP_Employee_ID__r.GP_Business_Group_NAME__c
                , GP_Start_Date__c , GP_End_Date__c from GP_Project_Leadership__c where GP_Project__c =: projectId AND (GP_Active__c = true or GP_IsUpdated__c = true)];        
    }
    
    private List<GP_Project_Address__c> getProjectAddress() {
        return [Select id, gp_relationship__c, gp_customer_name__c, gp_bill_to_address_formula__c, gp_ship_to_address_formula__c from GP_Project_Address__c 
                where GP_Project__c =: projectId];
    }
    
    private List<GP_Project_Work_Location_SDO__c> getProjectWorkLocations() {
        return [Select id, gp_work_location__c, RecordTypeId, gp_bcp_flag__c, gp_primary__c, gp_oracle_id__c from GP_Project_Work_Location_SDO__c 
                where GP_Project__c =: projectId];
    }
    // MG Changes
    private List<GP_Project_Document__c> getProjectDocuments() {
        return [Select id, gp_po_amount__c, gp_payment_terms__c, gp_po_end_date__c, gp_po_necessary_on_invoice__c, gp_po_number__c, gp_po_remarks__c from GP_Project_Document__c 
                where GP_Project__c =: projectId];
    }
    
    private List<list<GPComparisonStructure>> getListResource(){        
        List<list<GPComparisonStructure>> lstResourceAllcationCompare = new List<list<GPComparisonStructure>> ();
        map<string,map<string,string>> mpIdtoPldResourceAll = new map<string,map<string,string>> ();
        set<String> setSResAllFS = GPCommon.getFieldSetAsSetOfString('GP_Fields_to_compare','GP_Resource_Allocation__c');
        
        if(mapOfListOfResourcesNew == null) {
            return lstResourceAllcationCompare;
        }
        
        lstResourceAllocationCurrent = new List<map<string,string>>();               
        
        //Map<String, List<GP_Resource_Allocation__c>> mapOfResources = new Map<String, List<GP_Resource_Allocation__c>>();
        Map<String, List<Id>> mapOfResourcesIds = new Map<String, List<Id>>();

        //System.debug('==mapOfListOfResourcesOld=='+mapOfListOfResourcesOld);
        if(mapOfListOfResourcesOld != null && mapOfListOfResourcesOld.keySet().size() > 0) {
            for(String fieldName : mapOfListOfResourcesOld.keySet()) {
                
                if(mapOfListOfResourcesOld.containsKey(fieldName)) {
                    List<Id> resourceIds = new List<Id>();
                    for(Integer i=0; i<mapOfListOfResourcesOld.get(fieldName).size(); i++) {
                        if(mapOfListOfResourcesOld.get(fieldName)[i].get('id') != null) {
                            resourceIds.add(mapOfListOfResourcesOld.get(fieldName)[i].get('id'));
                        }                        
                    }
                    mapOfResourcesIds.put(fieldName, resourceIds);
                }
            }
            
            //System.debug('==mapOfResourcesIds=='+mapOfResourcesIds);
            
            for(String fieldName : mapOfResourcesIds.keySet()) {
                //List<GP_Resource_Allocation__c> listOfResources = mapOfResources.get(fieldName);
                List<Id> listOfResources = mapOfResourcesIds.get(fieldName);
                List<map<string,string>> listOfMapOfResources = mapOfListOfResourcesOld.get(fieldName);
                
                for(Integer i=0; i< listOfMapOfResources.size(); i++) {
                    if(listOfResources[i] != null) {
                        mpIdtoPldResourceAll.put(listOfResources[i],listOfMapOfResources[i]);
                    }                    
                }
            }
            
            //System.debug('==mpIdtoPldResourceAll=='+mpIdtoPldResourceAll);
        }        
        
        GPComparisonGenerator objGPComparisonGenerator;
        
        //System.debug('==mapOfResourcesNew=='+mapOfResourcesNew);
        
        for(String fieldName : mapOfListOfResourcesNew.keySet()) {
            if(mapOfListOfResourcesNew.get(fieldName) != null) {
                List<Map<String, String>> listOfNewResourseData = mapOfListOfResourcesNew.get(fieldName);

                for(Integer i=0; i<listOfNewResourseData.size(); i++) {                    
                    
                    //System.debug('==listOfNewResourseData[i]=='+listOfNewResourseData[i]);
                    //System.debug('==gp_isupdated__c=='+ (listOfNewResourseData[i].get('gp_isupdated__c') == 'true'));
                    //System.debug('==gp_parent_resource_allocation__c=='+ listOfNewResourseData[i].get('gp_parent_resource_allocation__c'));
                    
                    if(listOfNewResourseData[i].get('gp_isupdated__c') == 'true' && mpIdtoPldResourceAll != null 
                        && mpIdtoPldResourceAll.containsKey(listOfNewResourseData[i].get('gp_parent_resource_allocation__c'))) {
                    //if(obj.GP_IsUpdated__c && lstResourceAllocationOld != null) {
                        objGPComparisonGenerator = new GPComparisonGenerator();
                        objGPComparisonGenerator.setSourceObjectList((Map<string,string>)JSON.deserialize(Json.serialize(listOfNewResourseData[i]), Map<string,string>.class));
                        if(mpIdtoPldResourceAll.get(listOfNewResourseData[i].get('gp_parent_resource_allocation__c')) != null){
                            objGPComparisonGenerator.setDestinationObjectList(mpIdtoPldResourceAll.get(listOfNewResourseData[i].get('gp_parent_resource_allocation__c')));
                        }
                        objGPComparisonGenerator.setFieldSet(setSResAllFS);
                        objGPComparisonGenerator.sefieldTypeMap(mapFieldtoType); 
                        objGPComparisonGenerator.setObjectType('GP_Resource_Allocation__c');
                        
                        lstResourceAllcationCompare.add(objGPComparisonGenerator.generateComparisonResult());
                    } else if(listOfNewResourseData[i].get('gp_isupdated__c') == 'true') {
                        objGPComparisonGenerator = new GPComparisonGenerator();
                        objGPComparisonGenerator.setSourceObjectList((Map<string,string>)JSON.deserialize(Json.serialize(listOfNewResourseData[i]), Map<string,string>.class));
                        objGPComparisonGenerator.setDestinationObjectList(null);
                        
                        objGPComparisonGenerator.setFieldSet(setSResAllFS);
                        objGPComparisonGenerator.sefieldTypeMap(mapFieldtoType); 
                        objGPComparisonGenerator.setObjectType('GP_Resource_Allocation__c');
                        
                        lstResourceAllcationCompare.add(objGPComparisonGenerator.generateComparisonResult());
                    }
                }
            }
        }
        
        //system.debug('==lstResourceAllcationCompare=='+lstResourceAllcationCompare);
        return lstResourceAllcationCompare;        
    }
    
    private list<BudgetDetails> createBudgetDetails( List<map<string,string>> mapProjectInvoice) {        
        decimal decBillableAmnt = 0.0;
        decimal decNonBillableAmnt = 0.0;
        string prijectId; 
        
        for(map<string,string> objProjectExpence:mapProjectInvoice ){
            if(objProjectExpence.get('gp_expenditure_type__c')!= null &&  objProjectExpence.get('gp_expenditure_type__c') =='Billable'){
                decBillableAmnt += objProjectExpence.get('gp_amount__c')!= null?Decimal.valueof(objProjectExpence.get('gp_amount__c')):0.0;
            }
            if(objProjectExpence.get('gp_expenditure_type__c')!= null &&  objProjectExpence.get('gp_expenditure_type__c') =='Non Billable'){
                decNonBillableAmnt += objProjectExpence.get('gp_amount__c')!= null?Decimal.valueof(objProjectExpence.get('gp_amount__c')):0.0;
            }
            prijectId = objProjectExpence.get('gp_project__c');
        }
        
        list<BudgetDetails> lstBudgetDetails = new list<BudgetDetails>();      
        
        decimal labourTCV =0.0;
        if(objProject.id == prijectId){
            labourTCV = objProject.GP_TCV__c != null? objProject.GP_TCV__c - decNonBillableAmnt: objProject.GP_Resource_TCV__c - decNonBillableAmnt;
        } else if(oldProject.id == prijectId){
            labourTCV = oldProject.GP_TCV__c != null? oldProject.GP_TCV__c - decNonBillableAmnt: oldProject.GP_Resource_TCV__c - decNonBillableAmnt;
        }        
        
        lstBudgetDetails.add(new BudgetDetails('LABOR',labourTCV,''));
        lstBudgetDetails.add(new BudgetDetails('BILLABLE EXPENSE',decBillableAmnt,''));
        lstBudgetDetails.add(new BudgetDetails('NON BILLABLE EXPENSE',decNonBillableAmnt,''));
        lstBudgetDetails.add(new BudgetDetails('CONTINGENCY',0.0,''));
        
        return lstBudgetDetails;
    }    
    
    class BudgetDetails {        
        public String TASK_NAME {get;set;}
        public Decimal AMOUNT {get;set;}
        public String CURRENCYTYPE {get;set;}
        
        public BudgetDetails(string TASK_NAME ,Decimal AMOUNT, String CURRENCYTYPE) {
            this.TASK_NAME = TASK_NAME;
            this.AMOUNT = AMOUNT;
            this.CURRENCYTYPE = CURRENCYTYPE;
        }        
    }
    
    private map<String,String> getfieldSetMap(string strObjectName) {
        map<String,String> mapfieldApiName = new map<String,String>();
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType objOBject = schemaMap.get(strObjectName);
        Map<String, Schema.SObjectField> fieldMap = objOBject.getDescribe().fields.getMap();
        
        for (String fieldName: fieldMap.keySet()) {  
            Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();
            
            mapFieldtoType.put(fieldName ,fielddataType );
            String fName = fieldMap.get(fieldName).getDescribe().getLabel();
            mapfieldApiName.put(fieldName, fName);            
        }
        
        return mapfieldApiName;
    }
    
    private string getVersionLineItems(GP_Project_Version_Line_Item_History__c listOfVersionLineItem, String objectName) {
        Integer numberOfFieldsPerObject = 12;
        Map<String, String> mapOfFieldWithString = new Map<String, String>();
        
        for (Integer i = 0; i < numberOfFieldsPerObject; i++) {
            String fieldName = 'GP_JSON' + (i + 1) + '__c';
            String previousFieldName = '';
            
            if(i > 0) {
                previousFieldName = 'GP_JSON' + i + '__c';
            }
            //system.debug('==previousFieldName=='+previousFieldName);
            String versionLineItemChunk = (String) listOfVersionLineItem.get(fieldName);
            
            if (versionLineItemChunk != null) {
                if(mapOfFieldWithString.keySet().size() == 0) {
                    mapOfFieldWithString.put(fieldName, versionLineItemChunk);
                } else if(mapOfFieldWithString.containsKey(previousFieldName) && mapOfFieldWithString.get(previousFieldName) != null) {                    
                    if(versionLineItemChunk.startsWith('{')) {
                        mapOfFieldWithString.put(fieldName, returnJSON(mapOfFieldWithString, versionLineItemChunk, true, fieldName, previousFieldName, objectName));
                    } else {
                        String jsonString = returnJSON(mapOfFieldWithString, versionLineItemChunk, false, fieldName, previousFieldName, objectName);
                        if(String.isNotBlank(jsonString)) {
                            mapOfFieldWithString.put(fieldName, jsonString);
                        }                        
                    }
                }
            }
        }
        //system.debug('==mapOfFieldWithString=='+mapOfFieldWithString);
        return JSON.serialize(mapOfFieldWithString);
    }
    
    private String returnJSON(Map<String, String> mapOfFieldWithString, String versionLineItemChunk, Boolean isJSONStart, 
                              String fieldName, String previousFieldName, String objectName) {
        String jsonStr = '';
        
        if(isJSONStart) {
            String str1 = '';
            
            if(mapOfFieldWithString.get(previousFieldName).endsWith(',')) {
				str1 = mapOfFieldWithString.get(previousFieldName).removeEnd(',');
            } else if(mapOfFieldWithString.get(previousFieldName).endsWith(', ')) {
                str1 = mapOfFieldWithString.get(previousFieldName).removeEnd(', ');
            }
            mapOfFieldWithString.put(previousFieldName, str1 + ']}');
            
            jsonStr = '{"' + objectName + '" : [' + versionLineItemChunk;
        } else {
            Integer indexOf = versionLineItemChunk.indexOf('{');
            //System.debug('==indexOf 1=='+indexOf);
            if(indexOf != -1) {
                String substring = versionLineItemChunk.substring(0, indexOf-1);
                //System.debug('==substring=='+substring);
                String substring1 = mapOfFieldWithString.get(previousFieldName) + substring;
                //System.debug('==substring_1=='+substring1);
                String str2 = '';
                if(substring1.endsWith(',')) {
                    str2 = substring1.removeEnd(',');
                } else if(substring1.endsWith(', ')) {
                    str2 = substring1.removeEnd(', ');
                }
                //System.debug('==str2 001=='+str2);
                mapOfFieldWithString.put(previousFieldName, str2 + ']}');
                
                jsonStr = '{"' + objectName + '" : [' + versionLineItemChunk.substring(indexOf);
            } else if(indexOf == -1) {
                indexOf = versionLineItemChunk.indexOf('}]') == -1 ? versionLineItemChunk.indexOf('} ]') : -1;
                //System.debug('==indexOf 2=='+indexOf);
                //System.debug('==versionLineItemChunk=='+versionLineItemChunk);
                if(indexOf != -1) {
                    mapOfFieldWithString.put(previousFieldName, mapOfFieldWithString.get(previousFieldName) + versionLineItemChunk);
                }
            }
        }
        return jsonStr;
    }
    
    // STARTS : Billing Milestones and budget parsing.
    private List<Map<String,String>> displayData(List<Map<String,String>> listOfData, String objectName) {
        List<Map<String,String>> listOfDataToDisplay = new List<Map<String,String>>();
        
        for(Map<String,String> lbm : listOfData) {
            if(objectName == 'Billing Milestone') {
                listOfDataToDisplay.add(getBMDetails(lbm));
            } else if(objectName == 'Project Budget') {
                listOfDataToDisplay.add(getPBDetails(lbm));
            }
        }
        
        return listOfDataToDisplay;
    }
    
    private Map<String,String> getBMDetails(Map<String,String> bmObj) {
        
        Map<String,String> mapOfBMs = new Map<String,String>();
        mapOfBMs.put('name', bmObj.get('name') != null ? bmObj.get('name') : '');
        mapOfBMs.put('gp_amount__c', bmObj.get('gp_amount__c') != null ? bmObj.get('gp_amount__c') : '');
        mapOfBMs.put('currencyisocode', bmObj.get('currencyisocode') != null ? bmObj.get('currencyisocode') : '');
        mapOfBMs.put('gp_date__c', bmObj.get('gp_date__c') != null ? formatDate(bmObj.get('gp_date__c')) : '');
        mapOfBMs.put('gp_entry_type__c', bmObj.get('gp_entry_type__c') != null ? bmObj.get('gp_entry_type__c') : '');
        mapOfBMs.put('gp_milestone_start_date__c', bmObj.get('gp_milestone_start_date__c') != null ? formatDate(bmObj.get('gp_milestone_start_date__c')) : '');
        mapOfBMs.put('gp_milestone_end_date__c', bmObj.get('gp_milestone_end_date__c') != null ? formatDate(bmObj.get('gp_milestone_end_date__c')) : '');
        mapOfBMs.put('gp_work_location__r.name', bmObj.get('gp_work_location__r.name') != null ? bmObj.get('gp_work_location__r.name') : '');
        
        return mapOfBMs;
    }
    
    private Map<String,String> getPBDetails(Map<String,String> pbObj) {
        
        Map<String,String> mapOfPBs = new Map<String,String>();
        mapOfPBs.put('gp_band__c', pbObj.get('gp_band__c') != null ? pbObj.get('gp_band__c') : '');
        mapOfPBs.put('gp_country__c', pbObj.get('gp_country__c') != null ? pbObj.get('gp_country__c') : '');
        mapOfPBs.put('gp_product__c', pbObj.get('gp_product__c') != null ? pbObj.get('gp_product__c') : '');
        mapOfPBs.put('gp_effort_hours__c', pbObj.get('gp_effort_hours__c') != null ? pbObj.get('gp_effort_hours__c') : '');
        mapOfPBs.put('gp_cost_rate__c', pbObj.get('gp_cost_rate__c') != null ? pbObj.get('gp_cost_rate__c') : '');
        mapOfPBs.put('gp_tcv__c', pbObj.get('gp_tcv__c') != null ? pbObj.get('gp_tcv__c') : '');
        mapOfPBs.put('gp_total_cost_functional_currency__c', pbObj.get('gp_total_cost_functional_currency__c') != null ? pbObj.get('gp_total_cost_functional_currency__c') : '');        
        
        return mapOfPBs;
    }
    
    private String formatDate(String dateStr) {
        
        dateStr = dateStr.trim();
        List<String> splittedDate = dateStr.split('-');
        return splittedDate[2] + '-' + (mapOfDateNames.get(splittedDate[1]) != null ? mapOfDateNames.get(splittedDate[1]) : splittedDate[1]) + '-' + splittedDate[0].right(2);
    }
    // ENDS : Billing Milestones and budget parsing.
    
    private void getHeapLimitDebugs(Integer lineNo) {
        System.debug('== Line no==:'+ lineNo + ':==Current Heap size==:'+ Limits.getHeapSize() + '"==Total Heap Size==:'+ Limits.getLimitHeapSize());
    }
}