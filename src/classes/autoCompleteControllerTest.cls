@istest
public class autoCompleteControllerTest {

    public static testMethod void testController()
    {
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
       Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
   oContact.title='Mr.';
   oContact.Promoter_info__c = TRUE;
   update oContact;
Promoter_info__c promoterobject=new Promoter_info__c(Ability_to_Influence__c='yes',Can_Make_a_Reference__c='yes',Can_make_Public_Appearances__c='yes',Can_Make_External_Introduction__c='yes',Suitable_for_Analyst_Reference__c='yes',Origination__c='Sole source',
Commercial_Structure__c='Acquisition',Engagement_Model__c='Other',Offerings__c='Other',Transition_Model__c='Complex',Unique_Client_Geos_Services__c='Other',
Business_Drivers__c='Other',G_Service_Delivery_Locations__c='India',Lean_Digital__c='Systems of Engagement',Transformation__c='Other',
Personal_Agenda_Career_Aspirations__c='test',Professional_Agenda_Areas_of_Interest__c='test',Current_Disposition__c='NA',Topics_To_Avoid__c='test',
Last_updated_disposition_date__c=system.today(),Social_Style__c='Driving',Nurturer__c='test',Name_Promoter__c=oContact.id,Service_Line__c='HRO',Sub_Service_Line_short__c='tests',
                                                    Can_intro_to_New_internal_buying_center__c='Yes',Unique_about_this_engagement__c='test',X2015_NPS_Disposition__c='Promoter');
        insert promoterobject;
        
        
        
        Test.startTest();
        
        Set <String> s= autoCompleteController.findSObjects('promoter_info__c','M','', '', 'Text_Promoter_Name__c');
        Set <String> s1= autoCompleteController.findSObjects('promoter_infot__c','M','', '', 'Text_Promoter_Name__c');
        Set <String> s2= autoCompleteController.findSObjects('promoter_info__c','M','Job_Title__c', '', 'Job_Title__c');
        Set <String> s3= autoCompleteController.findSObjects('promoter_info__c','te','', '', 'Sub_Service_Line_short__c');
        Set <String> s4= autoCompleteController.findSObjects('promoter_info__c','test','', '', 'Account_Name__c');
        Set <String> s5= autoCompleteController.findSObjects('promoter_info__c','test','', '', 'Unique_about_this_engagement__c');
        Test.stopTest();
    }
}