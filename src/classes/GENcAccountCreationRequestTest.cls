@isTest
private class GENcAccountCreationRequestTest
{
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'IPS0005','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        Account_Creation_Request__c oAccountCreationRequest  = GEN_Util_Test_Data.CreateNewAccountCreationRequest(Userinfo.getUserId(),'Test Account','Test' );
        try
        {
            Account_Creation_Request__c oAccountCreationRequestNew  = new Account_Creation_Request__c();
            oAccountCreationRequestNew.Name = 'Test Account';
            oAccountCreationRequestNew.Account_Owner__c=Userinfo.getUserId();
            insert oAccountCreationRequestNew;
        }   
        catch(exception exp)
        {
            system.debug('Error'+exp);
        }     
    }
 }