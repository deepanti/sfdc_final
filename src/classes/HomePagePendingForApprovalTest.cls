@isTest
public class HomePagePendingForApprovalTest {
    @testSetup
    public static void createData()
    {
        Account accList = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
        insert accList;
        Contact conList = TestDataFactoryUtility.CreateContact('First','Last',accList.id,'Test','Test','fed@gtr.com','78342342343');
        insert conList;
        Opportunity oppList = TestDataFactoryUtility.CreateOpportunity('Test',accList.id,conList.id);
        insert oppList;
        List<Product2> pList = TestDataFactoryUtility.createTestProducts(1);
        insert pList;
        test.startTest();
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = pList[0].Id,
            UnitPrice = 1000000000, 
            IsActive = true);
        insert standardPrice;
        List<OpportunityLineItem> oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);
        insert oppLineItem;
        User testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac@bc.com','ac@bc.com');
        insert testUser;
        oppList.Assocaie_RS_barometer__c =true;
        oppList.RevenueStormPP__RevenueStorm_Associate_Pursuit_Profiler__c = true;
        oppList.Sales_Leader__c =  testUser.id;
        update oppList;
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(oppList.id);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(UserInfo.getUserId()); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('RS_PP_Tool');
        req1.setSkipEntryCriteria(true);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        test.stopTest();
    }
    
    public static testMethod void fetchOpportunityPendingForApporvalTest()
    {
        try
        {
            User salesLeader = [Select id,Name from User where email = 'ac@bc.com'];
            
            System.runAs(salesLeader)
            {
                Test.startTest();
                List<HomePagePendingForApproval.ApprovalDetailsWrapper> wrapperLst = HomePagePendingForApproval.fetchOpportunityPendingForApporval();
                
                Test.stopTest();
            }
        }
        catch(Exception e)
        {
            system.debug(e.getMessage());
        }
    }
    public static testMethod void approvePendingItemsTest()
    {
        User salesLeader = [Select id,Name from User where email = 'ac@bc.com'];
        Id itemPendingForApproval = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                     WHERE ProcessInstance.Status = 'Pending' AND ActorId =:salesLeader.id Limit 1].Id;
        System.runAs(salesLeader)
        {
            Test.startTest();
            Boolean approve = HomePagePendingForApproval.approvePendingItems(itemPendingForApproval,'Accept','Approve');
            System.assertEquals(true, approve);            
            Test.stopTest(); 
        }
    }
    
    
}