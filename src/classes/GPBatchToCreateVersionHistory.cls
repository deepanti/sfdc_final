global class GPBatchToCreateVersionHistory implements Database.Batchable < sObject > , Database.AllowsCallouts, Database.Stateful {
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String projectQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Project__c.fields.getMap();
        projectQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(projectQuery, MapOfSchema);
        projectQuery += ' from GP_Project__c where GP_Create_Version_History__c = true';
        return Database.getQueryLocator(projectQuery);
    }
    global void execute(Database.BatchableContext bc, List < GP_Project__c > lstOfProject) {
        GPServiceProjectVersionHistory.fetchError= false;
        GPServiceProjectVersionHistory.createApprovedProjectVersionHistoryService(null, JSON.serialize(lstOfProject));
    }
    global void finish(Database.BatchableContext bc) {}
}