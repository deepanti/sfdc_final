public class Redirect_opp_from_stale
{
    Public opportunity Opp {get; set;}
    public string opportunityid {get; set;}

    public Redirect_opp_from_stale()
    {
        
        
        if (ApexPages.currentPage().getParameters().get('oppid') != NULL) 
        {
            opportunityid = ApexPages.currentPage().getParameters().get('oppid');
        }
        
        Opp=[select id,Transformation_stale_deal_check__c from opportunity where id= :opportunityid ];
    }

    
    Public pageReference update_TS_flag()
    {
        TRY{
                Opp.Transformation_stale_deal_check__c =true;
                
                Update Opp;
                
                 PageReference pageRef = new PageReference('/'+ Opp.id);
                                                            pageRef.setRedirect(true);
                                                            return pageRef; 
           }
           
           Catch(Exception e)
             {    
                 
                     ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
                     
                     return null;
             
             }
    
    }

}