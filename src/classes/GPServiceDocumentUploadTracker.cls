@isTest
public class GPServiceDocumentUploadTracker {
    private static GP_Project__c prjObj;
    private static List<GP_Project_Document__c> lstOfProjectDocument;
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        prjObj.GP_Is_PO_Required__c = true;
        prjObj.GP_Tax_Exemption_Certificate_Available__c = true;
        insert prjObj;
        
        //GP_Project_Document__c objPODocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'PO Document');
        //insert objPODocument;
        
        GP_Project_Document__c objOtherDocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'Other Document');
        insert objOtherDocument;
        
    }
    @isTest
    public static void testGPServiceDocumentUpload() {
        fetchData();
        GPServiceDocumentUpload documentUploadService = new GPServiceDocumentUpload(new Set<Id>{prjObj.Id});
        documentUploadService.validatePODocumentUpload();
        GPServiceDocumentUpload documentUploadService1 = new GPServiceDocumentUpload(new List<GP_Project__c>{prjObj});
        documentUploadService1.validatePODocumentUpload();
        GPServiceProjectDocument.validateProjectDocument(prjObj,null);
        GPServiceProjectDocument.isFormattedLogRequired = false;
        GPServiceProjectDocument.validateProjectDocument(prjObj,null);
    }
    public static void fetchData() {
        prjObj = [select id, Name, GP_Delivery_Org__c,GP_Is_PO_Required__c,GP_Is_Customer_PO_Available__c ,GP_Business_Group_L1__c,GP_Tax_Exemption_Certificate_Available__c, GP_Sub_Delivery_Org__c, GP_TCV__c, RecordTypeId, RecordType.Name, GP_Last_Temporary_Record_Id__c from GP_Project__c limit 1];
    	lstOfProjectDocument = [select id ,name from GP_Project_Document__c];
    }
}