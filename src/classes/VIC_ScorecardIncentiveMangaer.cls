public class VIC_ScorecardIncentiveMangaer{
     static VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
     static Integer currentYear = integer.valueof(processInfo.VIC_Annual_Process_Year__c);
     static Date startFYDate = Date.newInstance(currentYear, 1, 1);
     static Date endFYDate = Date.newInstance(currentYear, 12, 31);
     static String strPlanYear = String.valueOf(currentYear);
     
     /*
        @Author: Vikas Rajput
        @Description: This function will return incentive sum by its component against user.
        @Note: Param "casePrint" shows that we 
    */
    public static Map<Id,VIC_ComponentIncentiveSumWrapper> fetchIncentiveWrapByUserId(Set<Id> setUserId, Set<String> setIncentiveStatus, Boolean isAmountInLocal){
        
        Set<String> setScorecardComponentCode = new Set<String>((List<String>)(Label.VIC_ScorecardComponentCode).split(','));
        Map<Id,VIC_ComponentIncentiveSumWrapper> mapUserIDToIncentiveWrap = new Map<Id,VIC_ComponentIncentiveSumWrapper>();
        for(Target_Component__c objTargetCmp : [SELECT Id,Master_Plan_Component__r.vic_Component_Code__c,Target__r.User__c,
                                        (SELECT Id,Bonus_Amount__c,vic_Incentive_In_Local_Currency__c,vic_Incentive_Type__c,
                                        Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c,
                                        vic_Deal_Type__c,vic_Status__c,Achievement_Date__c FROM Target_Achievements__r WHERE 
                                        vic_Status__c IN : setIncentiveStatus) FROM Target_Component__c WHERE 
                                        Target__r.User__c IN :setUserId AND Master_Plan_Component__r.vic_Component_Code__c 
                                        IN :setScorecardComponentCode AND 
                                        Target__r.Plan__r.Year__c =:strPlanYear AND   
                                        Target__r.Start_date__c >= :startFYDate AND 
                                        Target__r.Start_date__c <= :endFYDate AND 
                                        Target__r.Is_Active__c =:true AND Target_Status__c ='Active']){
            
            if(!mapUserIDToIncentiveWrap.containsKey(objTargetCmp.Target__r.User__c)){
                mapUserIDToIncentiveWrap.put(objTargetCmp.Target__r.User__c, new VIC_ComponentIncentiveSumWrapper());
            }
            VIC_ComponentIncentiveSumWrapper objCmpIncWrap = mapUserIDToIncentiveWrap.get(objTargetCmp.Target__r.User__c);//Declaring variable for incerasing readability of code
            for(Target_Achievement__c eachTargetAch : objTargetCmp.Target_Achievements__r){
                addIncentiveSumByComponent(objCmpIncWrap, eachTargetAch, isAmountInLocal);
            }
        }
        return mapUserIDToIncentiveWrap;
    }
    
    public static void addIncentiveSumByComponent(VIC_ComponentIncentiveSumWrapper objCmpIncWrap, Target_Achievement__c objIncentive, Boolean isAmountInLocal){
        Decimal dcmAmount = 0;
        if(isAmountInLocal){
            dcmAmount = objIncentive.vic_Incentive_In_Local_Currency__c != null?objIncentive.vic_Incentive_In_Local_Currency__c:0; //It will be helpful changing amount field in future
        }else{
            dcmAmount = objIncentive.Bonus_Amount__c != null?objIncentive.Bonus_Amount__c:0; //It will be helpful changing amount field in future
        }
        Decimal dcmBonousAmtInLocal = objIncentive.vic_Incentive_In_Local_Currency__c != null?objIncentive.vic_Incentive_In_Local_Currency__c:0; //It will be helpful changing local amount field in future
        Boolean isAdjustment = objIncentive.vic_Incentive_Type__c == 'Adjustment'?true:false;
        if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'TCV_Accelerators' && objIncentive.vic_Deal_Type__c == 'IO'){
            //IO
            objCmpIncWrap.strTCVAcceleratorIO = ''+(Decimal.valueOf(objCmpIncWrap.strTCVAcceleratorIO) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstTCVAcceleratorIO.add(objIncentive); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id);
            if(isAdjustment){
                objCmpIncWrap.adjustmentTCVAcceleratorIO = 'true';
            }
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'TCV_Accelerators' && objIncentive.vic_Deal_Type__c == 'TS'){
            //TS
            objCmpIncWrap.strTCVAcceleratorTS = ''+(Decimal.valueOf(objCmpIncWrap.strTCVAcceleratorTS) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstTCVAcceleratorTS.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            if(isAdjustment){
                objCmpIncWrap.adjustmentTCVAcceleratorTS = 'true';
            }
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'OP'){
            objCmpIncWrap.strOP = ''+(Decimal.valueOf(objCmpIncWrap.strOP) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstOP.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentAdjustmentOP = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'PM'){
            objCmpIncWrap.strPM = ''+(Decimal.valueOf(objCmpIncWrap.strPM) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstPM.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentPM = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'MBO'){
            objCmpIncWrap.strMBO = ''+(Decimal.valueOf(objCmpIncWrap.strMBO) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstMBO.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentMBO = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'TCV'){
            objCmpIncWrap.strTCV = ''+(Decimal.valueOf(objCmpIncWrap.strTCV) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstTCV.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentTCV = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'IO_TCV'){
            objCmpIncWrap.strIOTCV = ''+(Decimal.valueOf(objCmpIncWrap.strIOTCV) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstIOTCV.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentIOTCV = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'TS_TCV'){
            objCmpIncWrap.strTSTCV = ''+(Decimal.valueOf(objCmpIncWrap.strTSTCV) + dcmAmount);
            objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstTSTCV.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentTSTCV = ''+(dcmAmount);
        }else if(objIncentive.Target_Component__r.Master_Plan_Component__r.vic_Component_Code__c == 'Discretionary_Payment'){
            objCmpIncWrap.strDiscretionaryPayment = ''+(Decimal.valueOf(objCmpIncWrap.strDiscretionaryPayment) + dcmAmount);
            //objCmpIncWrap.strTotalAmtScorecardInLocal = ''+((Decimal.valueOf(objCmpIncWrap.strTotalAmtScorecardInLocal))+dcmBonousAmtInLocal);
            objCmpIncWrap.lstDiscretionaryPayment.add(objIncentive);
            objCmpIncWrap.lstIncentiveId.add(objIncentive.Id); //It can be removed in future if there is no use of code still.
            objCmpIncWrap.adjustmentDiscretionary = ''+(dcmAmount);
        }
        objCmpIncWrap.strTotalScorecard = ''+(Decimal.valueOf(objCmpIncWrap.strOP) + Decimal.valueOf(objCmpIncWrap.strPM) + 
                                       Decimal.valueOf(objCmpIncWrap.strMBO) + Decimal.valueOf(objCmpIncWrap.strTCV) + 
                                       Decimal.valueOf(objCmpIncWrap.strIOTCV) + Decimal.valueOf(objCmpIncWrap.strTSTCV)) ;
        
    }
    
}