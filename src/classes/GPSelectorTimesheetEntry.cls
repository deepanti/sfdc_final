public class GPSelectorTimesheetEntry extends fflib_SObjectSelector {
    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Timesheet_Entry__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Timesheet_Entry__c.sObjectType;
    }

    public List < GP_Timesheet_Entry__c > selectById(Set < ID > idSet) {
        return (List < GP_Timesheet_Entry__c > ) selectSObjectsById(idSet);
    }

    public List < GP_Timesheet_Entry__c > selectTimeSheetEntriesRecord(Id timeSheetTransactionId, Integer month) {
        return [select id, GP_Project__c, GP_Project__r.Name, GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name
            from GP_Timesheet_Entry__c
            where GP_Timesheet_Transaction__c =: timeSheetTransactionId
            and GP_Date__c != NULL and GP_Project__c != null and
            CALENDAR_MONTH(GP_Date__c) =: month and GP_Project__r.GP_Is_Closed__c = false 
        ];
    }
    public List < GP_Timesheet_Entry__c > getTimeSheetEntriesEmployeeMonthYear(Set < String > setOfEmployeeMonthYear) {
        return [select id, GP_Project__c, GP_Project__r.Name, GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c
            where GP_Employee_Month_Year_Unique__c in: setOfEmployeeMonthYear and GP_Project__r.GP_Is_Closed__c = false 
                and GP_Project_Oracle_PID__c != null and GP_Project_Task_Oracle_Id__c != null
                and GP_Timesheet_Transaction__r.GP_Approval_Status__c != 'Approved' and GP_Timesheet_Transaction__r.GP_Approval_Status__c != 'Pending'
                and GP_Timesheet_Transaction__r.GP_Approval_Status__c != 'Rejected'
        ];
    }

    public List < GP_Timesheet_Entry__c > getTimeSheetEntriesForTimesheetTransactionRecord(Id timesheetTransactionId) {
        return [select id, GP_Project__c, GP_Project__r.Name, GP_Actual_Hours__c, GP_Modified_Hours__c, GP_Project_Task__r.GP_Start_Date__c,
            GP_Project_Task__r.GP_End_Date__c, GP_Project_Task__r.GP_Active__c,GP_Project_Oracle_PID__c,GP_Project_Task_Oracle_Id__c,
            GP_Employee__r.GP_Person_ID__c, GP_Project_Task__r.GP_Task_Number__c, GP_Project__r.GP_Oracle_PID__c, GP_Project__r.RecordType.Name,
            GP_Ex_Type__c, GP_Date__c, GP_Employee__c, GP_Project_Task__c, GP_Project_Task__r.Name, GP_Timesheet_Transaction__c
            from GP_Timesheet_Entry__c
            where GP_Timesheet_Transaction__c = :timesheetTransactionId
        ];
    }
}