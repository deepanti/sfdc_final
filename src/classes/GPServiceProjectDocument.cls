/**
 * @group ServiceLayer. 
 *
 * @description Service layer for Project Document.
 */
public with sharing class GPServiceProjectDocument {
    public class GPServiceProjectDocumentException extends Exception {}

    private static final String PO_DOCUMENT_REQUIRED_ERROR_LABEL = 'PO Document is required.';
    private static final String TAX_CERTIFICATE_REQUIRED_ERROR_LABEL = 'Tax Certificate is required.';
	// Avinash : Valueshare Changes 
	// Gainshare Change
    @TestVisible private static final String GAINSHARE_DOCUMENT_REQUIRED_LABEL = 'Please add  Customer signoff mail document first before adding Billing Milestones for this project.';
    public static Boolean isFormattedLogRequired = true;
    public static Boolean isLogForTemporaryId = true;

    /**
     * @description Validate Project Document.
     * 
     * @param listOfProjectDocuments : List of Project Document that needs to be validated.
     * @param project : Project Record whose project document needs to be validated.
     *
     * @return Map < String, List < String >> : Return Map of Sub Section with List of errors.
     */
    public static Map < String, List < String >> validateProjectDocument(GP_Project__c project, List < GP_Project_Document__c > listOfProjectDocuments) {
		
		// Gainshare Change
        // For Gainshare document count.
        Integer gainshareDocumentCount = 0;
		
        Map < String, List < String >> mapOfSectionToListOfErrors = new Map < String, List < String >> ();

        String validationMessage, key;
        //Is Customer PO Available” (Yes/No) If it is “Yes” it will force PO upload if it is “NO” it will not force.
        // by Pankaj 13-Aug-2018 .
        /*if (((project.RecordType.Name == 'OTHER PBB' && project.GP_Is_PO_Required__c) || 
            (project.RecordType.Name == 'CMITS' && (project.GP_Business_Group_L1__c == 'GE' ||
                                                   project.GP_Is_PO_Required__c))) && 
            (listOfProjectDocuments == null || listOfProjectDocuments.isEmpty()))
          */  
            
            
        if (((project.RecordType.Name == 'OTHER PBB' && project.GP_Is_Customer_PO_Available__c =='Yes') || 
            (project.RecordType.Name == 'CMITS' && (project.GP_Is_Customer_PO_Available__c =='Yes'))) && 
            (listOfProjectDocuments == null || listOfProjectDocuments.isEmpty()))     {
            validationMessage = PO_DOCUMENT_REQUIRED_ERROR_LABEL;

            if (isFormattedLogRequired) {
                key = 'Document Upload';
            } else if (isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if (!isLogForTemporaryId) {
                key = project.Id;
            }

            if (!mapOfSectionToListOfErrors.containsKey(key)) {
                mapOfSectionToListOfErrors.put(key, new List < String > ());
            }

            mapOfSectionToListOfErrors.get(key).add(validationMessage);
        } 
        if (project.GP_Tax_Exemption_Certificate_Available__c) {
            Boolean isTaxCertficateExist = false;
            if(listOfProjectDocuments != null && listOfProjectDocuments.size() > 0){
            for (GP_Project_Document__c projectDocument: listOfProjectDocuments) {
                if (projectDocument.GP_Type__c == 'Tax Exemption certificate') {
                    isTaxCertficateExist = true;
                    break;

                }
            }
            }
            if (!isTaxCertficateExist) {
                validationMessage = TAX_CERTIFICATE_REQUIRED_ERROR_LABEL;

                if (isFormattedLogRequired) {
                    key = 'Document Upload';
                } else if (isLogForTemporaryId) {
                    key = project.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = project.Id;
                }

                if (!mapOfSectionToListOfErrors.containsKey(key)) {
                    mapOfSectionToListOfErrors.put(key, new List < String > ());
                }

                mapOfSectionToListOfErrors.get(key).add(validationMessage);
            }
        }		
		// Gainshare Change
        // Gainshare document required when project type is Gainshare in the PID and milestones are defined.
        // Avinash : Valueshare changes. Value Share -Outcome,Value Share-SLA OD
        if(gainshareDocumentCount == 0
           && (project.GP_Project_Type__c == 'Gainshare' 
            || project.GP_Project_type__c == 'Value Share -Outcome' 
            || project.GP_Project_type__c == 'Value Share-SLA OD'
           )
           && project.RecordType.name != 'BPM' 
           && project.GP_TCV__c != 0
           && project.GP_No_of_Billing_Milestone__c != 0) {
		   // Get the Gainshare document count.
		   if(listOfProjectDocuments != null && listOfProjectDocuments.size() > 0){
			   for (GP_Project_Document__c projectDocument: listOfProjectDocuments) {
				   if(projectDocument.GP_Type__c == 'Customer SIGN OFF EMAIL') {
					   gainshareDocumentCount++;
				   }
			   }
		   } 
		   // Show error when no Gainshare document for Gainshare PID is present.
		   if(gainshareDocumentCount == 0) {
			   validationMessage = GAINSHARE_DOCUMENT_REQUIRED_LABEL;
			   
			   if (isFormattedLogRequired) {
				   key = 'Document Upload';
			   } else if (isLogForTemporaryId) {
				   key = project.GP_Last_Temporary_Record_Id__c;
			   } else if (!isLogForTemporaryId) {
				   key = project.Id;
			   }
			   
			   if (!mapOfSectionToListOfErrors.containsKey(key)) {
				   mapOfSectionToListOfErrors.put(key, new List < String > ());
			   }
			   
			   mapOfSectionToListOfErrors.get(key).add(validationMessage);
		   }
		}
        return mapOfSectionToListOfErrors;
    }
}