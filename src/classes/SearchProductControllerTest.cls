@isTest
public class SearchProductControllerTest {
	public static Opportunity opp;
    
    public static testMethod void testControllerMethods(){
        setupTestData();
        SearchProductController.getProducts(opp.Id);
        SearchProductController.getProductFields();
        SearchProductController.getSearchedProduct(opp.Id, 'BY_KEYWORD', 'Opex');
        SearchProductController.getSearchedProduct(opp.Id, 'BY_FILED_FILTER', 'Opex');
        SearchProductController.getOpportunityClosedDate(opp.Id);
        
        String filterList = '[{\"fieldName\":\"Service Line\",\"operator\":\"contains\",\"value\":\"Information\"}]';
       	SearchProductController.getSearchedProduct2(opp.Id, 'BY_BOTH', 'Genpact', filterList);
        
       
    }
     public static testMethod void testSearchMethod(){
     	setupTestData();   
         
         String filterList = '[{\"fieldName\":\"Service Line\",\"operator\":\"does not contains\",\"value\":\"Information\"}]';
       	SearchProductController.getSearchedProduct2(opp.Id, 'BY_BOTH', 'Genpact', filterList);
         ProductListWrapper wrapper = new ProductListWrapper();
         wrapper.page = 0;
     }   
    
    
    
    private static void setupTestData(){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
       User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        oAccount.Industry_Vertical__c = 'BFS';
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        System.runAs(u){
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(), recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');

            insert opp;
            Test.stopTest();
            
        }
    }
}