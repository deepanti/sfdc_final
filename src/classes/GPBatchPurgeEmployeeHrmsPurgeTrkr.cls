@isTest
public class GPBatchPurgeEmployeeHrmsPurgeTrkr {
    @testSetup
    static void testData(){
        
        EmployeeMasterHRMSAPI__c objEmp=new EmployeeMasterHRMSAPI__c();
        objEmp.RequestType__c='EmployeeMaster';
        objEmp.Request_Data__c='200';
        insert objEmp;
    }
    
    @isTest
    public static   void test(){
        
        Test.startTest();
        
        GPBatchPurgeEmployeeHrmsPurge obj=new GPBatchPurgeEmployeeHrmsPurge();
        
        DataBase.executeBatch(obj);
        
        
        Test.stopTest();
        
    }
    
    
    
}