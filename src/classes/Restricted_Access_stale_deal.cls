Public class Restricted_Access_stale_deal{
    
    Public List<opportunity> oppty{get; set;}
    Public Integer nonTsOppCount{get; set;}
    Public Integer dealsCount{get; set;}
    Public Integer dealsCountTSProf{get; set;}
    Public Integer staleDealprofsize{get; set;}  
    Public Integer staleDealNorsize{get; set;}
    Public Integer dealsCountTS{get; set;} 
    Public Integer dealsCountNor{get; set;}
    Public Integer dealsCountNonTS{get; set;}
    public string ExtItem_id {get; set;} 
    public String count { get;set;}
    Public Opportunity Extobj{get; set;}
    Public List<opportunity> oppty_non_ts{get; set;}
    Public List<opportunity> Final_oppty{get; set;}
    public List<SelectOption> lstnamesel {get;set;}
    
    Public List<opportunity> Final_non_ts_oppty{get; set;}
    Public List<opportunity> Stale_oppty{get; set;}
    Public List<opportunity> Stale_oppty_nonts{get; set;}
    Public string Loggedinuser{get; set;}
    public String Commentbody {get;set;}
    Public List<Deal_Cycle__c> DC {get; set;}
    Public string shortlistedOppIndex {get; set;}
    Public string shortlistedOppIndex1 {get; set;}
    Public string selectedname{get;set;}
    public list<WrapperOfOpp> shortlistedOppWrapper { get; set; }
    public list<WrapperOfOpp> shortlistedOppWrapper1 { get; set; }
    Public Boolean dropdeal{get; set;}
    Public Boolean Movedeal{get; set;}
    Public Boolean profFlag{get; set;}
  
    public date var3{get; set;}
    public string Move_deal_ahead{get; set;}
    public string Move_deal_ahead_selectlst{get; set;}
    
    Public opportunity opt {get; set;}
    Public  Map<ID, String> MSaVAL {get; set;}
    Public string Insights {get; set;}
    
    public String myDate { get; set; }
    public String myDate1 { get; set; }
    public String myDate2 { get; set; }
    Public String selectedValue {get; set;}
    Public String Errormsg{get; set;}
    Public String Errormsg1{get; set;}
    Public String Errormsg2{get; set;}
    
    Public string ExtItem_oppid{get; set;}
    Public string ExtItem_oppid1{get; set;}
    Public string ExtItem_oppid2{get; set;}
    
    Public Non_TS_restrcited_access__c discover_medium;
    Public Non_TS_restrcited_access__c define_medium;
    Public Non_TS_restrcited_access__c onBid_medium;
    Public Non_TS_restrcited_access__c downSelect_medium;
    
    Public Non_TS_restrcited_access__c discover_large;
    Public Non_TS_restrcited_access__c define_large;
    Public Non_TS_restrcited_access__c onBid_large;
    Public Non_TS_restrcited_access__c downSelect_large;
    Public Non_TS_restrcited_access__c confirmed_large;
    
    Public Non_TS_restrcited_access__c discover_extra_large;
    Public Non_TS_restrcited_access__c define_extra_large;
    Public Non_TS_restrcited_access__c onBid_extra_large;
    Public Non_TS_restrcited_access__c downSelect_extra_large;
    Public Non_TS_restrcited_access__c confirmed_extra_large;
    
    public List<NonTsWrapper> nonTsWrapperList{get;set;}
    
    Public String proposedClosedDate{get;set;}    
    
    Public Restricted_Access_stale_deal()
        
    {         
        Commentbody='';
        shortlistedOppIndex ='';
        shortlistedOppIndex1 ='';
        shortlistedOppWrapper = new list<WrapperOfOpp> ();
        shortlistedOppWrapper1 = new list<WrapperOfOpp> ();
        MSaVAL=new map<Id, String>();
        Final_oppty=new List<Opportunity>();
        Final_non_ts_oppty=new List<Opportunity>();
        oppty_non_ts=new List<Opportunity>();
        Move_deal_ahead='';
        oppty=new List<opportunity>();
        Loggedinuser=UserInfo.getUserId();
        Errormsg='';
        Errormsg1='';
        Errormsg2='';
        if(UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners){
            profFlag=true;
        }
        else{
            profFlag=false;
        }
        
        Fetch_TS_Ageing_Opps();
        
        Insights='Basis analysis, avg. cycle time for this deal from  Discover to Closure is 150 days. MSA/SOW Closure date should be at least 75 days ahead';
        
        Fetch_Non_TS_Ageing_Opps(); 
    }    
    
    Public Void Fetch_TS_Ageing_Opps()
    {    
        stale_oppty = new List<Opportunity>();
        Final_oppty = new List<Opportunity>();
        
        Map<String, Map<String, Set<Opportunity>>> ownerWise_user_DealMap_TS = StaleDealHelper.fetch_TS_Ageing_Opps_new();
            
        
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
        { 
            Map<String, Set<Opportunity>> deal_map_TS = ownerWise_user_DealMap_TS.get('STALE_USER');
            System.debug('deal_map_TS=='+deal_map_TS);
            dealsCountTS = deal_map_TS.get('STALLED_DEALS').size();
            staleDealprofsize = deal_map_TS.get('STALE_DEALS').size();
            
            stale_oppty.addAll(deal_map_TS.get('STALE_DEALS'));
            Final_oppty.addAll(deal_map_TS.get('STALLED_DEALS'));
            
        }
        else{
            Map<String, Set<Opportunity>> deal_map_TS = ownerWise_user_DealMap_TS.get('NORMAL_USER');
            staleDealNorsize=deal_map_TS.get('STALE_DEALS').size();
            
            Final_oppty.addAll(deal_map_TS.get('STALLED_DEALS'));
            stale_oppty.addAll(deal_map_TS.get('STALE_DEALS'));
        }
        dealsCountTSProf=Final_oppty.size();
        System.debug('stale_oppty==='+stale_oppty.size());
        System.debug('Final_oppty.==='+Final_oppty.size());
    }
    
    Public Void Fetch_Non_TS_Ageing_Opps()
    {        
        Stale_oppty_nonts = new List<Opportunity>();
        Final_non_ts_oppty = new List<Opportunity>();   
        Map<String, Map<String, Set<Opportunity>>> ownerWise_user_DealMap_Non_TS = StaleDealHelper.fetch_Non_TS_Ageing_Opps_new();
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
        { 
            Map<String, Set<Opportunity>> deal_map_Non_TS = ownerWise_user_DealMap_Non_TS.get('STALE_USER');
            Stale_oppty_nonts.addAll(deal_map_Non_TS.get('STALE_DEALS'));
            Final_non_ts_oppty.addAll(deal_map_Non_TS.get('STALLED_DEALS'));
            
            dealsCount = Stale_oppty_nonts.size()+staleDealprofsize;
            dealsCountNonTS = Final_non_ts_oppty.size();
            
            system.debug('Final_non_ts_oppty=='+Final_non_ts_oppty);
            
        }   
        else{
            Map<String, Set<Opportunity>> deal_map_Non_TS = ownerWise_user_DealMap_Non_TS.get('NORMAL_USER');
            Stale_oppty_nonts.addAll(deal_map_Non_TS.get('STALE_DEALS'));
            Final_non_ts_oppty.addAll(deal_map_Non_TS.get('STALLED_DEALS'));
        }
        Stale_oppty_nonts.addAll(Stale_oppty);
        dealsCountNor=Stale_oppty_nonts.size();
        dealsCountNonTS=Final_non_ts_oppty.size();
        nonTsOppCount=Final_non_ts_oppty.size();
        System.debug('Stale_oppty_nonts==='+Stale_oppty_nonts.size());
        System.debug('Final_non_ts_oppty.==='+Final_non_ts_oppty.size());
        
        discover_medium = Non_TS_restrcited_access__c.getValues('Discover_medium');
        define_medium = Non_TS_restrcited_access__c.getValues('Define_medium');
        onBid_medium = Non_TS_restrcited_access__c.getValues('On-bid_medium');
        downSelect_medium = Non_TS_restrcited_access__c.getValues('Down Select_medium');
        
        discover_large = Non_TS_restrcited_access__c.getValues('Discover_large');
        define_large = Non_TS_restrcited_access__c.getValues('Define_large');
        onBid_large = Non_TS_restrcited_access__c.getValues('On-bid_large');
        downSelect_large = Non_TS_restrcited_access__c.getValues('Down Select_large');
        confirmed_large = Non_TS_restrcited_access__c.getValues('Confirmed_large');
        
        discover_extra_large = Non_TS_restrcited_access__c.getValues('Discover_extra_large');
        define_extra_large = Non_TS_restrcited_access__c.getValues('Define_Extra_large');
        onBid_extra_large = Non_TS_restrcited_access__c.getValues('On-bid_Extra_large');
        downSelect_extra_large = Non_TS_restrcited_access__c.getValues('Down Select_Extra_large');
        confirmed_extra_large = Non_TS_restrcited_access__c.getValues('Confirmed_Extra_large');
        
        nonTsWrapperList = new List<NonTsWrapper>();
        
        for(Opportunity o : Final_non_ts_oppty){
            System.debug('Final_non_ts_oppty=='+Final_non_ts_oppty);
           if(o.Type_of_deal_for_non_ts__c == 'Medium'){
               if(o.stagename=='1. Discover'){
                   nonTsWrapperList.add(new NonTsWrapper(o, (discover_medium.days__c - o.Transformation_deal_ageing_for_non_ts__c)));}else if(o.stagename=='2. Define'){  nonTsWrapperList.add(new NonTsWrapper(o, (define_medium.days__c - o.Transformation_deal_ageing_for_non_ts__c)));    }  else if(o.stagename=='3. On Bid'){ nonTsWrapperList.add(new NonTsWrapper(o, (onBid_medium.days__c - o.Transformation_deal_ageing_for_non_ts__c)));}else if(o.stagename=='4. Down Select'){ nonTsWrapperList.add(new NonTsWrapper(o, (downSelect_medium.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
            }
            
            if(o.Type_of_deal_for_non_ts__c == 'Large'){
               if(o.stagename=='1. Discover'){
                   nonTsWrapperList.add(new NonTsWrapper(o, (discover_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));}else if(o.stagename=='2. Define'){ nonTsWrapperList.add(new NonTsWrapper(o, (define_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));} else if(o.stagename=='3. On Bid'){  nonTsWrapperList.add(new NonTsWrapper(o, (onBid_large.days__c - o.Transformation_deal_ageing_for_non_ts__c))); } else if(o.stagename=='4. Down Select'){  nonTsWrapperList.add(new NonTsWrapper(o, (downSelect_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));} else if(o.StageName == '5. Confirmed'){   nonTsWrapperList.add(new NonTsWrapper(o, (confirmed_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
            }
            
            if(o.Type_of_deal_for_non_ts__c == 'Extra Large'){
               if(o.stagename=='1. Discover'){
                   nonTsWrapperList.add(new NonTsWrapper(o, (discover_extra_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
               if(o.stagename=='2. Define'){ nonTsWrapperList.add(new NonTsWrapper(o, (define_extra_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
               if(o.stagename=='3. On Bid'){ nonTsWrapperList.add(new NonTsWrapper(o, (onBid_extra_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
               if(o.stagename=='4. Down Select'){ nonTsWrapperList.add(new NonTsWrapper(o, (downSelect_extra_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               }
               else if(o.StageName == '5. Confirmed'){ nonTsWrapperList.add(new NonTsWrapper(o, (confirmed_extra_large.days__c - o.Transformation_deal_ageing_for_non_ts__c)));
               } 
            }
        }
        
    }
    
    public class NonTsWrapper{
        public Opportunity opp{get;set;}
        public integer ageing{get;set;}
        public Date proposedClosedDate{get;set;}
        
        
        public NonTsWrapper(opportunity nonTsOpp, decimal oppAgeing){
            opp = nonTsOpp;
            Integer diffInDays = Integer.valueOf(oppAgeing);
            ageing = Integer.valueOf(nonTsOpp.Transformation_deal_ageing_for_non_ts__c);
            if(diffInDays >=0 && diffInDays <=7){ proposedClosedDate = (opp.CloseDate).addDays(7);
            }
            else{
                proposedClosedDate = (opp.CloseDate).addDays(diffInDays);
            }
            
        }
    }
    
    Public void refreshButton()
    {
        Errormsg2='';
        Errormsg1='';       
        Fetch_TS_Ageing_Opps();
        Fetch_Non_TS_Ageing_Opps();
    }
    
    
    Public void Move_to_prediscover()
    {
        Opportunity op=[select id,stagename,Transformation_stale_deal_timestamp__c from Opportunity where id=:ExtItem_oppid];
        op.StageName='Prediscover';
        op.Transformation_stale_deal_timestamp__c=system.today();
        try{
            Update op; Fetch_TS_Ageing_Opps();  Fetch_Non_TS_Ageing_Opps();
        }
        Catch(Exception e)
        {   
                if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Contact admin.'));
                else 
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
          
        }
    }
    
    Public Void shortListOpps()
    {
       
        IF(shortlistedOppIndex != null)        
        {   
            Opportunity opp=[Select Id,Move_Deal_Ahead__c,StallByPassFlag__c,Revenue_Start_Date__c,Closedate,Transformation_stale_deal_timestamp__c from Opportunity where ID =: shortlistedOppIndex];        
            try{
            if(!string.isempty(myDate))
                {       
                 IF(date.valueOf(myDate) >= system.today() && Move_deal_ahead =='')
                 {
                     Opp.Closedate= date.valueOf(myDate) ;
                     Opp.Revenue_Start_Date__c=date.valueOf(myDate) ;
                     Opp.Transformation_stale_deal_timestamp__c=System.Today();
                     Opp.StallByPassFlag__c='';
                     Errormsg1='';
                     update Opp;
                 }
                 IF(date.valueOf(myDate) < system.today() && Move_deal_ahead ==''){     Errormsg1='MSA/SOW closure date CANNOT be earlier than \"Today\"'; shortlistedOppIndex='';
                 }
                 else
                 {
                     Errormsg1='';
                 }
                }
           
            
                system.debug('to be updated opp=='+opp);
                //update Opp;
                myDate ='';
                Move_deal_ahead ='';
                Fetch_TS_Ageing_Opps();
                Fetch_Non_TS_Ageing_Opps();
                
            }
             Catch(Exception e)
             {    
                    system.debug('Error=='+e.getMessage()+'=='+e.getLineNumber());
                     if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Contact admin.'));
                     else 
                         ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
              }
            }               
    }
    
    Public void shortListOpps1()
    {
        System.debug('myDate1=='+myDate1);
        Date var=date.valueOf(myDate1);
        Opportunity  opp=[Select Id,Move_Deal_Ahead__c,Revenue_Start_Date__c,Insight__c,CloseDatewithInsight__c,Closedate,Transformation_stale_deal_timestamp__c from Opportunity where ID =:ExtItem_oppid2]; 
        System.debug('proposedClosedDate in sh=='+Date.valueOf(proposedClosedDate));
        
        if(!string.isempty(myDate1))
        {      
            IF(var < Date.valueOf(proposedClosedDate) && var>=system.today())
            {    
                String st= String.valueof(proposedClosedDate).left(10);
                Errormsg1='MSA/SOW closure Date cannot be earlier than Proposed MSA/SOW closure date' +'('+st+'). For more queries please contact SFDCHelpdesk.';
                mydate1='';
            }
            IF(var >= Date.valueOf(proposedClosedDate))
            {
                opp.Closedate= date.valueOf(myDate1) ;
                opp.Revenue_Start_Date__c=date.valueOf(myDate1) ;
                opp.Transformation_stale_deal_timestamp__c=System.Today();
                try{
                    system.debug('to be updated non ts=='+opp);
                    update opp;
                    Errormsg1='';
                    mydate1='';
                    Fetch_Non_TS_Ageing_Opps();
                }
                Catch(Exception e)
                {    
                   // String st= String.valueof(opp.CloseDatewithInsight__c).left(10);
                    if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error,'please contact SFDCHelpdesk.'));
                    else 
                        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
               
                }
            }
            
            if(var<system.today()){ Errormsg1='MSA/SOW closure date CANNOT be earlier than Today'; mydate1='';
            }
        }
    }               
    
    
    
    Public void Update_non_Ts_Closure()
    {
        Opportunity op=[select id,stagename,CloseDatewithInsight__c,Transformation_stale_deal_timestamp__c,closedate from Opportunity where id=:ExtItem_oppid1];
        System.debug('proposedClosedDate=='+proposedClosedDate);
        op.Closedate = Date.valueOf(proposedClosedDate);//date.valueOf(op.CloseDatewithInsight__c) ;
        op.Revenue_Start_Date__c = Date.valueOf(proposedClosedDate);//date.valueOf(op.CloseDatewithInsight__c) ;
        op.Transformation_stale_deal_timestamp__c = System.Today();
        try{    
            Update op; Fetch_Non_TS_Ageing_Opps();
        }
        Catch(Exception e)
        {   
            if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))  ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Contact SFDC helpdesk'));
            else 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
        
        }
    }
    
    public class WrapperOfOpp
    {
        public Opportunity Opps { get;  set; }
        Public string Msa_check {get; set;}
        public string Oppid { get; set; }
        public WrapperOfOpp(Opportunity objOpp)
        {
            this.Opps = objOpp;
            this.Msa_check =objOpp.Move_Deal_Ahead__c ;
            this.Oppid = objOpp.id; } public WrapperOfOpp(){}
    }
    
    public List<SelectOption> getCountries()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult =
            Opportunity.Remind_in__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(),f.getValue()));
        }       
        return options;
    }
    
    public List<SelectOption> getCountriesOne()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult =
            Opportunity.Remind_in_define__c.getDescribe();
        
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(),f.getValue()));
        }       
        return options;
    }
    
    
    public void save()
    {
        try{
            refreshButton();
            Errormsg1='';
            Errormsg2='';
            opportunity oo = [select ID,counterOne__c, Type_Of_Opportunity__c, Nature_of_Work_highest__c,stageName,Remind_in__c,Remind_in_define__c,Exception_Comments__c from opportunity where ID =: ExtItem_id];
            if(oo.counterOne__c<=1)
            {
                oo.Exception_Comments__c=Commentbody;
                if(oo.Type_Of_Opportunity__c == 'TS')
                {
                    system.debug('Nature_of_Work_highest__c  == '+oo.Nature_of_Work_highest__c);
                    if(oo.stageName=='1. Discover')
                    {
                        oo.Remind_in__c=count; }else{  oo.Remind_in_define__c=count;
                    }
                }
                oo.ExceptionDate__c=system.today();
                oo.counterOne__c=oo.counterOne__c+1;    
                oo.Bypass_Validations__c=True;
                update oo;
                Commentbody = '';
                Errormsg1='';
                Errormsg2='';
                system.debug('nat== '+oo.Nature_of_Work_highest__c);
                if(oo.Type_Of_Opportunity__c == 'Non Ts' || oo.Type_Of_Opportunity__c == 'Non TS')
                {
                    Integer index;
                    
                    for(Integer i=0;i<Final_non_ts_oppty.size();i++) {if(Final_non_ts_oppty.get(i).id==ExtItem_id){ index=i;
                        }
                    }
                    if(index!=NULL){ Final_non_ts_oppty.remove(index); if(oo.NonTsByPassUI__c!='True') { oo.NonTsByPassUI__c='True'; update oo;  } refreshButton();
                    }
                }
                if(oo.Type_Of_Opportunity__c == 'TS')
                {
                    Integer indexTS;
                    for(Integer i=0;i<Final_oppty.size();i++)
                    {
                        if(Final_oppty.get(i).id==ExtItem_id) { indexTS=i;
                        }
                    }
                    if(indexTS!=NULL){ Final_oppty.remove(indexTS); if(oo.CloseDate < System.Today())  { oo.StallByPassFlag__c='True'; } if(oo.CloseDate >= System.Today()) {  oo.StallByPassFlag__c='';  }  update oo;  refreshButton(); }  } }  else { if(oo.Type_Of_Opportunity__c == 'TS'){  Errormsg2='You have already set the reminder twice on this deal. Please update the deal to get SFDC access.';}  else{ Errormsg1='You have already set the reminder twice on this deal. Please update the deal to get SFDC access.';}
            }
             Commentbody = '';
        }
        Catch(Exception e)
        {  
            if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Contact admin.'));
            else 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
            Commentbody = '';
           
        }
    }
    public void cancel()
    {
        Commentbody='';
    }
    public void saveTs()
    {
        try{
            refreshButton();
            Errormsg1='';
            Errormsg2='';
            opportunity oo=new opportunity();
            oo=[select ID,counterOne__c, Type_Of_Opportunity__c, Nature_of_Work_highest__c,closedate,stageName,Remind_in__c,Remind_in_define__c,Exception_Comments__c from opportunity where ID =: ExtItem_id];
            
            if(oo.counterOne__c<=1)
            {
                oo.Exception_Comments__c=Commentbody;
                if(oo.stageName=='1. Discover')
                {
                    oo.Remind_in__c=count;}else { oo.Remind_in_define__c=count;
                }
                
                oo.ExceptionDate__c=system.today();
                oo.counterOne__c=oo.counterOne__c+1;    
                oo.Bypass_Validations__c=True;
                update oo;
                
                Errormsg1='';
                Errormsg2='';
                
                if(oo.Type_Of_Opportunity__c == 'TS')
                {
                    Integer indexTS;
                    for(Integer i=0;i<Final_oppty.size();i++)
                    {
                        if(Final_oppty.get(i).id==ExtItem_id)
                        {
                            indexTS=i;
                        }
                    }
                    if(indexTS!=NULL){
                        Final_oppty.remove(indexTS);
                        if(oo.CloseDate < System.Today())
                        {
                            oo.StallByPassFlag__c='True';
                        }
                        if(oo.CloseDate >= System.Today())
                        {
                            oo.StallByPassFlag__c='';
                        }
                        update oo;
                        refreshButton();
                    }
                    system.debug('size after removal'+Final_oppty.size());
                }
            }
            
            else
            {
                if(oo.Type_Of_Opportunity__c == 'TS')
                {
                    Errormsg2='You have already set the reminder twice on this deal. Please update the deal to get SFDC access.';
                }
                else{
                    Errormsg1='You have already set the reminder twice on this deal. Please update the deal to get SFDC access.';}
            }
             Commentbody='';
        }
        Catch(Exception e)
        {   
            if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please Contact admin.'));
            else 
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
             Commentbody='';
        
        }
    }
    
    Public void Droping()
    {
        dropdeal=true;
    }
    
    Public void Moving()
    {
        movedeal=true;
    }
    Public pageReference redirecthome()
    {
        PageReference pageRef = new PageReference('/one/one.app#/home');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    
    
}