/*
    @Author: Rajan Saini
    @Company: SaaSFocus
    @Description: Test class for the controller of VIC_CalculateOPIncentivectrl
*/

@isTest
 public class VIC_CalculateOPIncentivectrlTest{
   
      static testmethod void validatemethod(){
    
        VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
         vicInfo.VIC_Process_Year__c=2018;
         insert vicInfo;      
          
          user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
          insert objuser;
          
          Master_VIC_Role__c  masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        masterVICRoleObj.Horizontal__c='CM/ITO';
        masterVICRoleObj.Role__c='SL';
        insert masterVICRoleObj;

        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.User__c = objuser.id;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
          
          Map <Id, User_VIC_Role__c> mapUserIdToUserVicRole=vic_CommonUtil.getUserVICRoleinMap();
        APXTConga4__Conga_Template__c obj= new VIC_CommonTest().initAllcongaData('GRM - CM VIC Letter');
         insert obj;
        
         List<Plan__c> plan = new VIC_CommonTest().initAllPlanData(obj.ID);
         insert plan;
        
        List<Master_Plan_Component__c >lstMaster= new VIC_CommonTest().initAllMasterPlanComponent();
        insert lstMaster;
        List<Plan_Component__c> planComponent= new VIC_CommonTest().initAllPlanComponent(plan,lstMaster);
        
        VIC_Calculation_Matrix__c objMatrix = VIC_CommonTest.fetchMatrixComponentData('Capital Market GRM');
        insert objMatrix;
        
        List<VIC_Calculation_Matrix_Item__c> calcMatrix = new VIC_CommonTest().fetchListMatrixItemOPData(objMatrix.Name);
        calcMatrix[0].Is_Additional_Payout_Applicable__c=true;
        calcMatrix[1].Is_Additional_Payout_Applicable__c=true;
        List<VIC_Calculation_Matrix_Item__c> calcMatrix1 = new VIC_CommonTest().fetchListMatrixItemOPData(objMatrix.Name);  
       Target__c target = new Target__c();
        target.Target_Bonus__c = 3000;
        target.Plan__c = plan[1].id;
        target.Plan__r = plan[1];
        target.user__c =objuser.id ;
        insert target;
        
        Target_Component__c  targetComp = new Target_Component__c();
        targetComp.Weightage__c =20;
        targetComp.vic_Achievement_Percent__c =105;
        targetComp.Target_Status__c = 'Active';
        targetComp.Target_In_Currency__c =1000;
        targetComp.Is_Weightage_Applicable__c = true;
        targetComp.Target__c = target.id;
        targetComp.Target__r = target;
        
        insert targetComp;   
      
        Test.StartTest();
        Map<String, vic_Incentive_Constant__mdt> mapData= new VIC_CommonTest().fetchMetaDataValue();
        
        VIC_CalculateOPIncentivectrl ctrl = new VIC_CalculateOPIncentivectrl();
        ctrl.mapValueToObjIncentiveConst = mapData;
        ctrl.mapUserIdToUserVicRoles=mapUserIdToUserVicRole; 
          ctrl.pmAcheivmentPercent=100;
          ctrl.calculate(targetComp,calcMatrix);
        ctrl.pmAcheivmentPercent=0;
        ctrl.calculate(targetComp,calcMatrix1);
        target.Plan__c = plan[3].id;
        target.Plan__r = plan[3];
        update target;
        
        ctrl.mapValueToObjIncentiveConst = mapData;
        ctrl.calculate(targetComp,calcMatrix);
        
        target.Plan__c = plan[0].id;
        target.Plan__r = plan[0];
        update target;
        
        ctrl.mapValueToObjIncentiveConst = mapData;
        ctrl.calculate(targetComp,calcMatrix);
        
        target.Plan__c = plan[2].id;
        target.Plan__r = plan[2];
        update target;
        
        ctrl.mapValueToObjIncentiveConst = mapData;
        ctrl.calculate(targetComp,calcMatrix);
        
        target.Plan__c = plan[4].id;
        target.Plan__r = plan[4];
        update target;
        
        ctrl.mapValueToObjIncentiveConst = mapData;
        ctrl.calculate(targetComp,calcMatrix);
        
        System.AssertEquals(200,200);

        Test.StopTest();
         
       
  }


}