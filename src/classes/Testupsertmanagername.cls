@isTest
private class Testupsertmanagername 
{

   static testMethod void testTrigger()
   {
      RMG_Employee_Master__c rmg = new RMG_Employee_Master__c(Name='Test'); 
      insert rmg;
      
      RMG_Employee_Allocation__c rmgallo = new RMG_Employee_Allocation__c(
                                     Name='Test', Manager_Name__c='Unit Test',Client__c='Non Bench',Status__c = 'Current',Start_Date__c = date.parse('01/03/2011'), End_Date__c = date.parse('05/12/2011'),
                                     Project__c='Test',Project_Business_Group__c='Test',Project_Code__c='1001',Project_HSL__c='Test',HR_DU_Name__c = 'Test',RMG_Employee_Code__c=rmg.id);
      insert rmgallo;
      update rmgallo;
      
      RMG_Employee_Allocation__c newrmg=[select Name, Manager_Name__c,Client__c,Status__c,is_Zero_PRF__c,Active__c,Project__c,Project_Code__C,Project_HSL__c,HR_DU_Name__c,Project_Business_Group__c from RMG_Employee_Allocation__c where id = :rmgallo.id];
      System.assertEquals('Unit Test', newrmg.Manager_Name__c);
      System.assertEquals('Test', newrmg.Project__c);
      System.assertEquals('1001', newrmg.Project_Code__c);
      System.assertEquals('Test', newrmg.Project_Business_Group__c);
      System.assertEquals('Test', newrmg.Project_HSL__c);
      System.assertEquals('Test', newrmg.HR_DU_Name__c);

      }
      }