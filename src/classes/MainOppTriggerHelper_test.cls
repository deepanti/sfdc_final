@isTest
public class MainOppTriggerHelper_test {
	public static Opportunity opp, opp1, opp2, opp3,opp4;
    public static User u, u1, u3,u4,u5,u6;
   	public static OpportunityLineItem opp_item,opp_item2,opp_item3,opp_item4;
    public static Product2 product_obj;
    public static PricebookEntry entry;
    
    public static testMethod void Setupmethod1(){
         Test.startTest();
        Setupmethod();
        opp2.StageName ='2. Define';
        opp2.Deal_Type__c='test';
        update opp2;
         Test.stopTest();
    }
    public static testMethod void Setupmethod2(){ 
        Test.startTest();
    	Setupmethod();
                opp3.StageName ='7. Lost';
        		//opp3.Reason__c='test';
               update opp3;
         Test.stopTest();
    }
    public static testMethod void Setupmethod3(){
        Test.startTest();
        Setupmethod();
        
        opp.stageName = '7. Lost';
        update opp;
          Test.stopTest();
    }
    public static testMethod void deletemethod1(){
        Setupmethod();
       
        delete opp4;
    }
     public static testMethod void insertOpportunityTest(){  
        Setupmethod();
        List<Opportunity>opportunity_list = new List<Opportunity>();
        opportunity_list.add(opp);
        OpportunityTriggerHelper.insertContactRole(opportunity_list);
        OpportunityTriggerHelper.opportunityRestrictMethod(opportunity_list);
        MainOppTriggerHelper.beforeInsertMethod(opportunity_list);
        MainOppTriggerHelper.afterUndeleteMethod(opportunity_list);
        
    }
    
     public static testMethod void OpportunityExceptionTest(){
        Test.startTest();
        OpportunityTriggerHelper.deleteContactRole(null);
         OpportunityTriggerHelper.insertContactRole(null);
        Test.stopTest();
    }
    
    public static testMethod void Setupmethod(){  
        
       list<MainOpportunityHelperFlag__c> custmSettingList = new list<MainOpportunityHelperFlag__c>();
 
        GP_Sobject_Controller__c settingGPS=new GP_Sobject_Controller__c(Name='opportunity',GP_Enable_Sobject__c=true);
        insert settingGPS;
         MainOpportunityHelperFlag__c setting = new MainOpportunityHelperFlag__c(Name = 'opportunity',HelperName__c = true);
        	MainOpportunityHelperFlag__c settingOli = new MainOpportunityHelperFlag__c(Name = 'MainOpportunityLineItemTrigger',HelperName__c = true);
            MainOpportunityHelperFlag__c setting1 = new MainOpportunityHelperFlag__c(Name = 'beforeInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting2 = new MainOpportunityHelperFlag__c(Name = 'beforeUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting3 = new MainOpportunityHelperFlag__c(Name = 'beforeDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting4 = new MainOpportunityHelperFlag__c(Name = 'afterInsertMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting5 = new MainOpportunityHelperFlag__c(Name = 'afterUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting6 = new MainOpportunityHelperFlag__c(Name = 'afterDeleteMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting7 = new MainOpportunityHelperFlag__c(Name = 'holdOpportunityTeam',HelperName__c = true);
            MainOpportunityHelperFlag__c setting8 = new MainOpportunityHelperFlag__c(Name = 'UpdatePrediscoverMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c setting9 = new MainOpportunityHelperFlag__c(Name = 'opportunityRestrictMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingq = new MainOpportunityHelperFlag__c(Name = 'insertContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingw = new MainOpportunityHelperFlag__c(Name = 'saveDealCycleAging',HelperName__c = true);
            MainOpportunityHelperFlag__c settinge = new MainOpportunityHelperFlag__c(Name = 'createTaskforOpp',HelperName__c = true);
            MainOpportunityHelperFlag__c settingr = new MainOpportunityHelperFlag__c(Name = 'createTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingt = new MainOpportunityHelperFlag__c(Name = 'dateUpdateMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingy = new MainOpportunityHelperFlag__c(Name = 'staleDealMethod',HelperName__c = false);
            MainOpportunityHelperFlag__c settinga = new MainOpportunityHelperFlag__c(Name = 'dealTypeMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settings = new MainOpportunityHelperFlag__c(Name = 'updateContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingd = new MainOpportunityHelperFlag__c(Name = 'removeTaskforOpponUpdate',HelperName__c = true);
            MainOpportunityHelperFlag__c settingf = new MainOpportunityHelperFlag__c(Name = 'Prediscover_RemoveTasksMethod',HelperName__c = true);
            MainOpportunityHelperFlag__c settingg = new MainOpportunityHelperFlag__c(Name = 'deleteOppSplitOwneronOppOwnerchange',HelperName__c = true);
            MainOpportunityHelperFlag__c settingh = new MainOpportunityHelperFlag__c(Name = 'deleteContactRole',HelperName__c = true);
            MainOpportunityHelperFlag__c settingcon = new MainOpportunityHelperFlag__c(Name = 'contactLifeCycleMethod',HelperName__c = true);
       		custmSettingList.add(setting); custmSettingList.add(setting1); custmSettingList.add(setting2);custmSettingList.add(setting3);
            custmSettingList.add(setting4); custmSettingList.add(setting5); custmSettingList.add(setting6);custmSettingList.add(setting7);
            custmSettingList.add(setting8); custmSettingList.add(setting9); custmSettingList.add(settingq);custmSettingList.add(settingw);
            custmSettingList.add(settinge); custmSettingList.add(settingr); custmSettingList.add(settingt);custmSettingList.add(settingy);//setting2,setting3,setting4,setting5,setting6,setting7,setting8,setting9,settingq,settingw,settinge,settingr,settingt,settingy);
    		custmSettingList.add(settinga); custmSettingList.add(settings); custmSettingList.add(settingd);custmSettingList.add(settingf);
            custmSettingList.add(settingg); custmSettingList.add(settingh); custmSettingList.add(settingcon);custmSettingList.add(settingOli);
            Insert custmSettingList;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        u4 = GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        u5 =GEN_Util_Test_Data.CreateUser('standarduser2079@testorg.com',p.Id,'standardusertestgen2079@testorg.com' );
        u6 = GEN_Util_Test_Data.CreateUser('standarduser2070@testorg.com',p.Id,'standardusertestgen2070@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        //oAccount.Sales_Unit__c = salesunit.id;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
        
         opp=new opportunity(name='1234',StageName='1. Discover',Nature_of_Work_highest__c = 'Consulting', CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                        Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                        Competitor__c='Accenture',contact1__c = oContact.ID, role__c = 'Other');
        
         opp2=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                         Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                         Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
         opp3=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                         Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                         Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        opp4=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                         Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='', Insight__c = 30,
                                         Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
        
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(opp);
        oppList.add(opp2);
        oppList.add(opp3);
        oppList.add(opp4);
        CheckRecursive.run = true;
        insert oppList;
         product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
             entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;           
            
            
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID,
                                                                   vic_VIC_User_4__c = u4.ID
                                                                   ); 
            insert opp_item;
            
            opp_item4 = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID,
                                                                   vic_VIC_User_4__c = u4.ID
                                                                   ); 
            insert opp_item4;
            
            delete opp_item4;
            
        
    }
  
}