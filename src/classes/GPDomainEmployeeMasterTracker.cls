@isTest
public class GPDomainEmployeeMasterTracker {
    public static boolean isRun = true;
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    
    @testSetup
    public static void buildDependencyData() {
        system.debug('Inside Method');
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_employee_master__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMasterForShare.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Training Master').getRecordTypeId();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        objuser.IsActive = true;
        
        insert objuser;  
        
         User objuser2 = new User();
        Profile p = [select id, name from profile where name = 'System Administrator'
            limit 1
        ];
        objuser2.profileId = p.Id;
        objuser2.username = 'newUserPranav2@saasforce.com';
        objuser2.email = 'pb@ff2.com';
        objuser2.emailencodingkey = 'UTF-8';
        objuser2.localesidkey = 'en_US';
        objuser2.languagelocalekey = 'en_US';
        objuser2.timezonesidkey = 'America/Los_Angeles';
        objuser2.alias = 'nuser2';
        objuser2.lastname = 'test Last Name';
        objuser2.isActive = true;
        objuser2.FederationIdentifier = '12345688';
        insert objuser2;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Work_Location__c objSdo4 = GPCommonTracker.getWorkLocation();
        objSdo4.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        objSdo4.GP_Oracle_Id__c = '1111';
        insert objSdo4;
        
        GP_Employee_HR_History__c empHrObj = new GP_Employee_HR_History__c();
        empHrObj.GP_Employee_Person_Id__c = 'EMP-001'; 
		empHrObj.GP_Attribute2__c='11223|03-JAN-20';
        insert empHrObj;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Employee_Type__c = 'Ex-employee';
        empObj.GP_Employee_HR_History__c = null;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_Batch_Number__c = '1234';
        empObj.GP_SDO_OracleId__c ='oracle';
        empObj.GP_Work_Location_Code__c='1111';
        empObj.GP_SUPERVISOR_PERSON_ID__c='1234567';
        empObj.GP_Final_OHR__c='123456';
        insert empObj;
        empObj.GP_SFDC_User__c = objuser.id;
        empObj.GP_isActive__c = false;
        update empObj;
        
        
        GP_Employee_Master__c supervisorempObj = GPCommonTracker.getEmployee();
        supervisorempObj.GP_Employee_Type__c = 'Ex-employee';
        supervisorempObj.GP_Employee_HR_History__c = null;
        supervisorempObj.GP_SFDC_User__c = objuser.id;
        supervisorempObj.GP_Person_ID__c = '1234567';
        insert supervisorempObj;
        //empObj2.GP_SFDC_User__c = objuser.id;
        //update empObj2;
        
       
        
        GP_Work_Location__c objSdo2 = GPCommonTracker.getWorkLocation();
        objSdo2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo2.GP_Oracle_Id__c = '1234';
        insert objSdo2;
        
        
        GP_Employee_HR_History__c objEmpHRHistory = GPCommonTracker.getEmployeeHR(empObj.id);
        insert objEmpHRHistory;
        
        GP_Employee_Master__c empObj2 = GPCommonTracker.getEmployee();
        empObj2.GP_Employee_Type__c = 'Ex-employee';
        empObj2.GP_Employee_HR_History__c = objEmpHRHistory.id;
        empObj2.GP_SFDC_User__c = objuser2.id;
        empObj2.GP_SUPERVISOR_PERSON_ID__c = '1234567';
        empObj2.GP_SDO_OracleId__c = '1234';
        //empObj2.GP_Batch_Number__c = '12345';
        empObj2.Name = 'Testemployee2';
        empObj2.GP_EMPLOYEE_TYPE__c = 'Full Time';
        empObj2.GP_Person_ID__c = 'EMP-002';
        empObj2.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test2@test.com';
        insert empObj2;
        empObj2.GP_SUPERVISOR_PERSON_ID__c = '1234';
        empObj2.GP_SDO_OracleId__c = '123';
        update empObj2;
        empObj2.GP_SUPERVISOR_PERSON_ID__c = null;
        empObj2.GP_SDO_OracleId__c = null;
        update empObj2;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        projectLeadership.GP_End_Date__c = system.today();
        projectLeadership.GP_Project__c = projectObjForIcon.id;
        insert projectLeadership;
        
    }
    @istest
    public static void testData() {
        
        User userData = [SELECT ID,isActive from User limit 1];
        GP_Employee_Master__c empData = [SELECT Id,GP_isActive__c from GP_Employee_Master__c limit 1];
        //system.assertEquals(userData.IsActive, empData.GP_isActive__c, 'employee inactive user is inactive');
        
    }
    @isTest
    public static void testGPEmployeemaster()
    {
        //buildDependencyData();
        GPDomainEmployeeMaster obj = new GPDomainEmployeeMaster(new List<GP_Employee_Master__c>{new GP_Employee_Master__c()});
    }
    
}