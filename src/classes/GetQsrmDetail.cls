/*
Created By - Abhishek Maurya
Date -15/10/208 
Lead - Mandeep Kaur
Description - this class is used to send the QSRM data to client side controller in QsrmQuickAction Lightning component .  
 */

public with sharing class GetQsrmDetail {

     @auraEnabled
    public static List<wrapperClass> getQsrm(String oppId)
    {
        List<wrapperClass> wrapperLst=new List<wrapperClass>();
         for( QSRM__C qsm:[SELECT Id,Name,Account_Name__c,(SELECT QsrmOwnerName__c,Status__c FROM QSRM_Leadership_Approvals__r) FROM QSRM__C WHERE Opportunity__c=:oppId])
         {
             wrapperClass returnwrapperClass = new  wrapperClass();
             returnwrapperClass.accountName=qsm.Account_Name__c;
             returnwrapperClass.qsrmName=qsm.Name;
             returnwrapperClass.qsrmId=qsm.Id;
             returnwrapperClass.lstQsrmLdApprov=qsm.QSRM_Leadership_Approvals__r;  
             wrapperLst.add(returnwrapperClass);
         }
        return wrapperLst;        
    }
     public class wrapperClass{
        @AuraEnabled public  String qsrmName{get;set;}
        @AuraEnabled public  String qsrmId{get;set;}
        @AuraEnabled public String accountName{get;set;}
        @AuraEnabled public List<QSRM_Leadership_Approvals__c> lstQsrmLdApprov{get;set;}
         
       
    }
}