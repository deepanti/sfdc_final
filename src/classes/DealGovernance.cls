public class DealGovernance {
    @AuraEnabled
    public static List <Opportunity> chkStage(String parentId) {
        
        return [select id,StageName,QSRM_Status__c,TCV1__c,define_solution__c,define_pricing__c,onbid_solution__c,onbid_pricing__c,downselect_solution__c,downselect_pricing__c,Is_RS_parameter_approve__c from Opportunity where id =: parentId LIMIT 1 ];
        
    }
    
    @AuraEnabled
    public static Boolean savefields(String solutionval,String pricingval, String parentId) {

        Opportunity oppList = new Opportunity();
        oppList = [select id,StageName,QSRM_Status__c,TCV1__c,define_solution__c,define_pricing__c,onbid_solution__c,onbid_pricing__c,downselect_solution__c,downselect_pricing__c,Is_RS_parameter_approve__c from Opportunity where id =: parentId LIMIT 1 ];
        System.debug('oppList'+oppList);
        if(oppList.StageName == '2. Define')
        {
           
            oppList.Define_pricing__c=pricingval;
            oppList.Define_solution__c=solutionval;
            
        }
        else if(oppList.StageName == '3. On Bid')
        {
            
            oppList.Onbid_Pricing__c = pricingval;
            oppList.Onbid_Solution__c=solutionval;
        }
         else if(oppList.StageName == '4. Down Select')
        {
            
            oppList.Downselect_Pricing__c=pricingval;
            oppList.Downselect_Solution__c=solutionval;
        }
        
        //update oppList;
        //System.debug('oppList'+oppList);
        
        try{
            
            update oppList;
        }
        catch(Exception E)
        {
             throw new AuraHandledException('There is an error with one or more Opportunity.');
        }
        
        return true;
    }
}