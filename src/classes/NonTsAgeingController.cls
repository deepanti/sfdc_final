public class NonTsAgeingController {
    
     @AuraEnabled
    public static List <Opportunity> dealAgeing(String parentId) {
        system.debug(':---parentId---:'+parentId);
       return [select id, StageName, Insight__c, Type_Of_Opportunity__c, Opportunity_Source__c, Transformation_deal_ageing_for_non_ts__c, CloseDate from Opportunity where id =: parentId LIMIT 1 ];
       
    }

}