/*
  This class is used to create common data which is used by the tracker classes  
  --------------------------------------------------------------------------------------
  Name                                Date                                Version                     
  --------------------------------------------------------------------------------------
  Jubin                             3/17/2016                               1.0   
  --------------------------------------------------------------------------------------
*/
public class GW1_CommonTracker
{
    /*
      This Method is used to create Account  
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/17/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    public static Account createAccount(string strName)
    {
        Account objAccount = new Account();
        objAccount.Name = strName;
        insert objAccount;

        System.assertEquals(objAccount.Name, strName);
        return objAccount;
    }

    /*
      This Method is used to create Opportunty  
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/17/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    public static Opportunity createOpportunity(string strName, string strStageName, string strSource, boolean isDSRCreated, boolean dontCreateDSR, boolean dsrCreatedManually,
                                                id acccountId)
    {
        Opportunity objOpportunity = new Opportunity();
        objOpportunity.AccountId = acccountId;
        objOpportunity.Name = strName;
        objOpportunity.StageName = strStageName;
        objOpportunity.Opportunity_Source__c = strSource;
        objOpportunity.CloseDate = System.today().adddays(1);
        objOpportunity.Revenue_Start_Date__c = System.today().adddays(180);
		objOpportunity.Contract_Term_in_months__c = 2;
        objOpportunity.Opportunity_Origination__c = 'Proactive';
        objOpportunity.Sales_Region__c = 'Asia';
        objOpportunity.Deal_Type__c = 'Competitive';
        objOpportunity.Win_Loss_Dropped_reason1__c='test';
     
        objOpportunity.GW1_Dont_Create_DSR__c = dontCreateDSR;
        objOpportunity.GW1_DSR_Created__c = isDSRCreated;
        objOpportunity.GW1_DSR_Created_Manually__c=dsrCreatedManually;
        objOpportunity.GW1_Include_in_pilot__c=true;

        
        
        insert objOpportunity;

        System.assertEquals(objOpportunity.Name, strName);
        system.debug(' objOpportunity.CloseDate'+ objOpportunity.CloseDate);
        system.debug(' objOpportunity.Revenue_Start_Date__c'+ objOpportunity.Revenue_Start_Date__c);
        return objOpportunity;
    }

    /*
      This Method is used to create QSRM  
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/17/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    public static QSRM__c createQSRM(Id opportuintyId)
    {
        QSRM__c objQSRM = new QSRM__c();
        objQSRM.Opportunity__c = opportuintyId;
        objQSRM.Named_Competitors__c = 'Accenture';
        objQSRM.Justification_for_defying_BP_guidance__c = 'asdfnmkl';
        objQSRM.Due_date_for_bid_submission__c=system.today();


        insert objQSRM;

        System.assertEquals(objQSRM.Opportunity__c, opportuintyId);
        return objQSRM;
    }

    /*
      This Method is used to create User  
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
     */


    public static User createUser(String Alias, String LastName, string strUserName)
    {
        Profile objProfile = [SELECT Id FROM Profile WHERE Name = 'Quest Super Admin'];


        User objUser = new User(Alias = Alias, Email = 'standarduser11@testorg.com',
                                EmailEncodingKey = 'UTF-8', LastName = LastName, LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = objProfile.id,
                                TimeZoneSidKey = 'America/Los_Angeles', UserName = strUserName);



        system.debug('new USer ' + objUser.username);


        insert objUser;
        // System.assertEquals(objUser.Alias, objUser.LastName);
        return objUser;
    }


    /*
      This Method is used to create a CollaborationGroup
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Shubham                             3/30/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    public static CollaborationGroup createCollaborationGroup(string strName)
    {
        CollaborationGroup objCollaborationGroup = new CollaborationGroup();
        objCollaborationGroup.name = strName;
        objCollaborationGroup.CollaborationType = 'Private';
        insert objCollaborationGroup;
        return objCollaborationGroup;
    }


    /*
      This Method is used to create GW1_DSR__c
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    public static GW1_DSR__c createGW1DSR(string strName)
    {
        GW1_DSR__c objGWDSR = new GW1_DSR__c();
        //objGWDSR.GW1_Opportunity_Origination__c = strName;
        //objGWDSR.GW1_Primary_Chatter_ID__c = '123456789';
        // Shubham 30 March
        objGWDSR.Name = strName;

        insert objGWDSR;
        return objGWDSR;
    }

    /*
      This Method is used to create GW1_DSR_Team__c  
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
     */

    Public static GW1_DSR_Team__c createGW1DSRTeam(GW1_DSR__c objDSR, User ObjUser, boolean isActive, String role)
    {
        GW1_DSR_Team__c objGWDSRTeamc = new GW1_DSR_Team__c();
        objGWDSRTeamc.GW1_DSR__c = objDSR.Id;
        objGWDSRTeamc.GW1_Is_active__c = isActive;
        objGWDSRTeamc.GW1_Role__c = role;
        objGWDSRTeamc.GW1_User__c = ObjUser.id;

        insert objGWDSRTeamc;
        return objGWDSRTeamc;
    }

    /*
      This Method is used to create GW1_DSR_Group__c
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
     */

    Public static GW1_DSR_Group__c createGW1DSRGroup(String strName, GW1_DSR__c objDSR, boolean primaryGroup)
    {

        GW1_DSR_Group__c objGWDSRGroupc = new GW1_DSR_Group__c();
        objGWDSRGroupc.Name = strName;
        //objGWDSRGroupc.GW1_Chatter_Group_Id__c = '123456789012345678';
        objGWDSRGroupc.GW1_DSR__c = objDSR.Id;
        objGWDSRGroupc.GW1_Is_Primary_Group__c = primaryGroup;

        insert objGWDSRGroupc;
        return objGWDSRGroupc;
    }


    /*
      This Method is used to create Trigger Custom Setting , used to active or inactive
      the triger on production 
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Pankaj Adhikari                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
      parameters: strName - name Of the trigger  
      --------------------------------------------------------------------------------------
     */
    public static void createTriggerCustomSetting(string strName)
    {
        GW1_triggerSettings__c objcustomSetting = new GW1_triggerSettings__c();
        objcustomSetting.name = strName;
        objcustomSetting.GW1_Is_Active__c = true;

        insert objcustomSetting;
    }
    // 
    public static void createTriggerCustomSetting()
    {
        List<GW1_triggerSettings__c> listCSData = new List<GW1_triggerSettings__c> ();

        GW1_triggerSettings__c objcustomSetting = new GW1_triggerSettings__c();
        objcustomSetting.name = 'GW1_DSRTeamTrigger';
        objcustomSetting.GW1_Is_Active__c = true;
        listCSData.add(objcustomSetting);

        objcustomSetting = new GW1_triggerSettings__c();
        objcustomSetting.name = 'GW1_DSRTrigger';
        objcustomSetting.GW1_Is_Active__c = true;
        listCSData.add(objcustomSetting);

        objcustomSetting = new GW1_triggerSettings__c();
        objcustomSetting.name = 'GW1_trgOnDSRGroup';
        objcustomSetting.GW1_Is_Active__c = true;
        listCSData.add(objcustomSetting);

        objcustomSetting = new GW1_triggerSettings__c();
        objcustomSetting.name = 'GW1_TriggerOnOpportunityCreateDSR';
        objcustomSetting.GW1_Is_Active__c = true;
        listCSData.add(objcustomSetting);

        insert listCSData;
    }

    /*
      This Method is used to create oppproduct
      --------------------------------------------------------------------------------------
      Name                                Date                                Version                     
      --------------------------------------------------------------------------------------
      Jubin                             3/21/2016                               1.0   
      --------------------------------------------------------------------------------------
     */
    /*
      Public static OpportunityProduct__c createOppProduct(Opportunity ObjOpp)
      {
      Nature_Of_Work__c objNature=new Nature_Of_Work__c();
      objNature.Name='Delivery';
      insert objNature;
      Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
      Pricebook2 oPriceBook = GEN_Util_Test_Data.CreatePricebook2();
      ID objPbID = Test.getStandardPricebookId();
     
     
      PricebookEntry objPriceBookEntry =  new PricebookEntry();// Line Commented by Shubhi      
      //List<Pricebook2> lstPriceBook = [select Id , name from Pricebook2 where isStandard=true];
      //objPriceBookEntry =new PriceBookEntry();
      objPriceBookEntry.isActive=true;
      objPriceBookEntry.Product2id=oProduct.id;
      objPriceBookEntry.UnitPrice=122.00;
     
      //objPriceBookEntry.PriceBook2id=lstPriceBook!=null && lstPriceBook.size()>0 ?lstPriceBook[0].id : objpricebook2.Id;
      objPriceBookEntry.PriceBook2id = objPbID;
      objPriceBookEntry.UseStandardPrice = false;
      insert objPriceBookEntry;
     
      OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
      oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
      oOpportunityProduct.Product__c = oProduct.Id;
      oOpportunityProduct.Product_Autonumber__c = 'OLI';
      oOpportunityProduct.COE__c = 'ANALYTICS';
      oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
      oOpportunityProduct.DeliveryLocation__c = 'Americas';
      oOpportunityProduct.SEP__c = 'SEP Opportunity';
      oOpportunityProduct.LocalCurrency__c = 'INR';
      oOpportunityProduct.RevenueStartDate__c = System.today();
      oOpportunityProduct.ContractTerminmonths__c = 26;
      oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId();
      oOpportunityProduct.Nature_Of_Work_Lookup__c=objNature.Id;
      //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
      //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
      oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
      oOpportunityProduct.FTE_4th_month__c = 2;
      oOpportunityProduct.FTE_7th_month__c = 2;
      oOpportunityProduct.FTE_10th_month__c = 2; 
      oOpportunityProduct.OpportunityId__c = ObjOpp.Id;
      oOpportunityProduct.TCVLocal__c = 18000;
      //oOpportunityProduct.TNYR__c=0.00;
      //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
      oOpportunityProduct.HasRevenueSchedule__c = true;
      oOpportunityProduct.HasSchedule__c = true;
      oOpportunityProduct.HasQuantitySchedule__c = false;
      insert oOpportunityProduct;
      return oOpportunityProduct;
      }
     
     */
    Public static OpportunityProduct__c createOppProduct(Opportunity ObjOpp, string natureOfWork)
    {
        Nature_Of_Work__c objNature = new Nature_Of_Work__c();
        objNature.Name = 'Delivery';
        insert objNature;
        
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Pricebook2 oPriceBook = GEN_Util_Test_Data.CreatePricebook2();
        ID objPbID = Test.getStandardPricebookId();


        PricebookEntry objPriceBookEntry = new PricebookEntry(); // Line Commented by Shubhi      
        //List<Pricebook2> lstPriceBook = [select Id , name from Pricebook2 where isStandard=true];
        //objPriceBookEntry =new PriceBookEntry();
        objPriceBookEntry.isActive = true;
        objPriceBookEntry.Product2id = oProduct.id;
        objPriceBookEntry.UnitPrice = 122.00;

        //objPriceBookEntry.PriceBook2id=lstPriceBook!=null && lstPriceBook.size()>0 ?lstPriceBook[0].id : objpricebook2.Id;
        objPriceBookEntry.PriceBook2id = objPbID;
        objPriceBookEntry.UseStandardPrice = false;
        insert objPriceBookEntry;

        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.Product_Autonumber__c = 'OLI';
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today();
        oOpportunityProduct.ContractTerminmonths__c = 26;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId();
        oOpportunityProduct.Nature_Of_Work_Lookup__c = objNature.Id;
        //oOpportunityProduct.TransitionBillingMilestoneDate__c = System.today();
        //oOpportunityProduct.TransitionRevenueLocal__c = 40000;
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c = 2;
        oOpportunityProduct.FTE_7th_month__c = 2;
        oOpportunityProduct.FTE_10th_month__c = 2;
        oOpportunityProduct.OpportunityId__c = ObjOpp.Id;
        oOpportunityProduct.TCVLocal__c = 18000;
        //oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c = true;
        oOpportunityProduct.HasSchedule__c = true;
        oOpportunityProduct.HasQuantitySchedule__c = false;
        insert oOpportunityProduct;
        return oOpportunityProduct;
    }

    public static GW1_Bid_Manager__c createBidManager()
    {
        GW1_Bid_Manager__c objBidManager = new GW1_Bid_Manager__c();
        objBidManager.Name = 'Test Bid Manager';
        objBidManager.GW1_Bid_Manager__c = Userinfo.getUserId();
        objBidManager.GW1_Tower_lead__c = Userinfo.getUserId();

        insert objBidManager;
        return objBidManager;
    }

    public static GW1_Tower_Leader_Mapping__c createTL()
    {
        GW1_Tower_Leader_Mapping__c objTL= new GW1_Tower_Leader_Mapping__c();
        objTL.GW1_Distribution_List__c='Test@test.com';
        objTL.GW1_Industry_Vertical__c='test';
        objTL.GW1_Product_Family__c='test';
        insert objTL;
        return objTL;
    }
}