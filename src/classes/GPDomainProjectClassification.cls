public class GPDomainProjectClassification extends fflib_SObjectDomain {

    public GPDomainProjectClassification(List < GP_Project_Classification__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainProjectClassification(sObjectList);
        }
    }

    //SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] { GP_Project_Classification__c.SobjectType };

    public override void onBeforeInsert() {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateIsUpdatedOnOpportunityProject(oldSObjectMap);
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateIsUpdatedOnChange(oldSObjectMap);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }

    private void updateIsUpdatedOnChange(Map < Id, SObject > oldSObjectMap) {
        
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Project_Classification__c objProjectClassification: (List < GP_Project_Classification__c > ) Records) {
            GP_Project_Classification__c objOldobjProjectClassification = (GP_Project_Classification__c) oldSObjectMap.get(objProjectClassification.id);
            if (isRecodChnaged(objProjectClassification, objOldobjProjectClassification)) {
                objProjectClassification.GP_IsUpdated__c = true;
            }
        }
    }

    private void updateIsUpdatedOnOpportunityProject(Map < Id, SObject > oldSObjectMap) {
        
        if (GPCommon.isOpportunityProjectInsertionEvent && !GPDomainOpportunityProject.isOpportunityProjectUpdated)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Project_Classification__c objProjectClassification: (List < GP_Project_Classification__c > ) Records) {
            GP_Project_Classification__c objoldProjectClassification = (GP_Project_Classification__c) oldSObjectMap.get(objProjectClassification.id);
            // change in IsUpdated CheckBox
            if (objProjectClassification.GP_IsUpdated__c && oldSObjectMap != null && !objoldProjectClassification.GP_IsUpdated__c &&
                objProjectClassification.GP_Opp_Project__c != null) {
                setOppProjectId.add(objProjectClassification.GP_Opp_Project__c);
            }
        }
        if (setOppProjectId.size() > 0) {
            list < GP_Opportunity_Project__c > lstOpportunityProject = new list < GP_Opportunity_Project__c > ();
            for (id OpportunityProjectID: setOppProjectId) {
                lstOpportunityProject.add(new GP_Opportunity_Project__c(id = OpportunityProjectID, GP_IsUpdated__c = true));
            }
            update lstOpportunityProject;
        }
    }
	// T7 Change
    private boolean isRecodChnaged(GP_Project_Classification__c objProjectClassification, GP_Project_Classification__c objOldobjProjectClassification) {

        if ((!String.isBlank(objProjectClassification.GP_ATTRIBUTE1__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE1__c != objProjectClassification.GP_ATTRIBUTE1__c) ||

            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE10__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE10__c != objProjectClassification.GP_ATTRIBUTE10__c) ||

            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE11__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE11__c != objProjectClassification.GP_ATTRIBUTE11__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE12__c) &&

                objOldobjProjectClassification.GP_ATTRIBUTE12__c != objProjectClassification.GP_ATTRIBUTE12__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE13__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE13__c != objProjectClassification.GP_ATTRIBUTE13__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE14__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE14__c != objProjectClassification.GP_ATTRIBUTE14__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE15__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE15__c != objProjectClassification.GP_ATTRIBUTE15__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE2__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE2__c != objProjectClassification.GP_ATTRIBUTE2__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE3__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE3__c != objProjectClassification.GP_ATTRIBUTE3__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE4__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE4__c != objProjectClassification.GP_ATTRIBUTE4__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE5__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE5__c != objProjectClassification.GP_ATTRIBUTE5__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE6__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE6__c != objProjectClassification.GP_ATTRIBUTE6__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE7__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE7__c != objProjectClassification.GP_ATTRIBUTE7__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE8__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE8__c != objProjectClassification.GP_ATTRIBUTE8__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE9__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE9__c != objProjectClassification.GP_ATTRIBUTE9__c) ||
            (!String.isBlank(objProjectClassification.GP_CLASS_CODE__c) &&
                objOldobjProjectClassification.GP_CLASS_CODE__c != objProjectClassification.GP_CLASS_CODE__c) ||
            (!String.isBlank(objProjectClassification.GP_ATTRIBUTE1__c) &&
                objOldobjProjectClassification.GP_ATTRIBUTE1__c != objProjectClassification.GP_ATTRIBUTE1__c) ||
            (!String.isBlank(objProjectClassification.GP_EP_Project_Number__c) &&
                objOldobjProjectClassification.GP_EP_Project_Number__c != objProjectClassification.GP_EP_Project_Number__c)
           )
        {
            return true;
        }

        return false;
    }
}