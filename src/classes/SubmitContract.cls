Public without sharing class SubmitContract{
    
    @InvocableMethod
    Public static void Submitcontractforaproval( List<id> opptyid){
    List<contract> Con= new List<contract>();
    set<id> Conid = new set<id>();
            Con=[select id,name,Survey_Initiated_contract__c,Opportunity_tagged__c from contract where Opportunity_tagged__c in :opptyid ];
            
            if(!Con.isempty())
            {    
                for(Contract c : Con)
                {
                    if(c.Survey_Initiated_contract__c==true )
                      {               
                        Conid.add(c.id);
                      }
                }
                    ProcessInstance[] oPIs = [select Id, TargetObjectID from ProcessInstance WHERE  TargetObjectId IN :Conid];
                    Approval.ProcessSubmitRequest[] lRequests = New Approval.ProcessSubmitRequest[]{};
                    
                    Map<id, ProcessInstance> mPI = New Map<id, ProcessInstance>();

                   //Build Map
                   For(ProcessInstance gPI : oPIs)
                       mPI.put(gPI.TargetObjectID, gPI);
                   
                   
                   For(Contract c: Con)
                   {
                        if(mPI.get(c.ID) == NULL)
                        {
                            Approval.ProcessSubmitRequest req   = new   Approval.ProcessSubmitRequest();            
                            //req.setComments('Submitted for Approval');            
                            req.setObjectId(c.Id); 
                            Approval.ProcessResult results =Approval.process(req);
                        }
                   
                   }
                   
                   
            }
            
    
    }
}