@isTest
public class GPCmpServiceProjectCloneTracker{
    public static GP_Project__c prjObj;
    
    @testSetup static void setupCommonData(){
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Work_Location__c objworkLocation = GPCommonTracker.getWorkLocation();
        objworkLocation.GP_Oracle_Id__c = '5678';
        objworkLocation.GP_Status__c = 'Active and Visible';
        objworkLocation.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation;
        
        GP_Work_Location__c objworkLocation2 = GPCommonTracker.getWorkLocation();
        objworkLocation2.GP_Oracle_Id__c = '9012';
        objworkLocation2.GP_Status__c = 'Active and Visible';
        objworkLocation2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation2;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj;
        
        GP_OMS_Detail__c objOSMDetail = new GP_OMS_Detail__c();
        objOSMDetail.GP_Deal__c = dealObj.id;
        objOSMDetail.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Expense').getRecordTypeId();
        objOSMDetail.GP_Expense_Type__c = 'Test';
        objOSMDetail.GP_Expense_Amount__c = 2;
        objOSMDetail.GP_Location__c = 'Test';
        objOSMDetail.GP_Category__c = 'Test';
        insert objOSMDetail;
        
        GP_OMS_Detail__c objOSMDetail2 = new GP_OMS_Detail__c();
        objOSMDetail2.GP_Deal__c = dealObj.id;
        objOSMDetail2.RecordTypeId = Schema.SObjectType.GP_OMS_Detail__c.getRecordTypeInfosByName().get('Pricing View').getRecordTypeId();
        objOSMDetail2.GP_Expense_Type__c = 'Test';
        objOSMDetail2.GP_Expense_Amount__c = 2;
        objOSMDetail2.GP_Location__c = 'Test';
        objOSMDetail2.GP_Category__c = 'Test';
        insert objOSMDetail2;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        insert prjObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accobj.Id, 'Active');
        leadershipMaster.GP_Leadership_Role__c = 'Billing SPOC';
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.GP_Leadership_Role_Name__c = 'Billing SPOC';
        insert projectLeadership;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId(); 
        insert objProjectWorkLocationSDO;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation.Id);
        objProjectWorkLocation.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation2.Id);
        objProjectWorkLocation1.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation1;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Document__c objPODocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'PO Document');
        insert objPODocument;
        
        GP_Project_Document__c objOtherDocument = GPCommonTracker.getProjectDocumentWithRecordType(prjObj.Id, 'Other Document');
        insert objOtherDocument;
        
        GP_Project_Expense__c objPrjExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert objPrjExpense;
        
        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert objPrjBillingMilestone;
        
        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id, prjObj.id);
        insert objResourceAllocation;
        
        GP_Profile_Bill_Rate__c objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Profile__c = 'QA';
        objProfileBillRate.GP_Project__c = prjObj.id;
        objProfileBillRate.GP_Bill_Rate__c = 23;
        insert objProfileBillRate;
        
        GP_Project_Address__c projectAddress = GPCommonTracker.getProjectAddress(prjObj.Id, 'Direct', null); 
        insert projectAddress;
        
    }
    
    @isTest static void testGPCmpServiceProjectClone() {
        fetchData();
        new GPControllerProjectClone();
        //GPCmpServiceProjectClone.skipParentTagging = true;
        GPCmpServiceProjectClone.cloneProject(prjObj.Id);
        GPCmpServiceProjectClone.cloneProject(prjObj.Id);
        GPCmpServiceProjectClone.cloneProject(null);
        
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new Set<Id> {prjObj.Id}, 'Manual', true, false);
        Id clonedProjectId = projectCloneController.cloneProject();
        projectCloneController = new GPControllerProjectClone(new Set<Id> {clonedProjectId}, 'Manual', true, false);
        projectCloneController.cloneProject();
        projectCloneController = new GPControllerProjectClone(null, 'Manual', true, false);
        projectCloneController.cloneProject();
        GPCommon.parseErrorMessage('Insert failed. First exception on row 0; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Work Locations for BPM projects should be of same LE code.: []');
    }
    
    public static void fetchData() {
        prjObj = [select id, Name from GP_Project__c limit 1];
    }
    
}