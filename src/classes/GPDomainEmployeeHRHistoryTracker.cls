@isTest
public class GPDomainEmployeeHRHistoryTracker {
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    
    @testSetup
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_employee_hr_history__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;  
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        insert prjObj ;
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        
        GP_Employee_Master__c empObjForEmpHR = GPCommonTracker.getEmployee();
        empObjForEmpHR.GP_Employee_Type__c = 'Ex-employee';
        empObjForEmpHR.GP_Employee_HR_History__c = null;
        empObjForEmpHR.GP_SFDC_User__c = objuser.id;
        empObjForEmpHR.GP_Person_ID__c = '1234567';
        insert empObjForEmpHR;
        
        GP_Employee_Master__c empObjForEmpHR2 = GPCommonTracker.getEmployee();
        empObjForEmpHR2.GP_Employee_Type__c = 'Ex-employee';
        empObjForEmpHR2.GP_Employee_HR_History__c = null;
        empObjForEmpHR2.GP_SFDC_User__c = objuser.id;
        empObjForEmpHR2.GP_Person_ID__c = '12345678';
        insert empObjForEmpHR2;
        
        GP_Employee_HR_History__c empHR = new GP_Employee_HR_History__c();
        empHR.GP_Employee_Person_Id__c = '1234567';
        empHR.GP_SUPERVISOR_PERSON_ID__c = '1234567';
		 empHR.GP_Attribute2__c = '1234567|04-JAN-20';
        insert empHR;
        empHR.GP_Employee_Person_Id__c = '12345678';
        update empHR;
        empHR.GP_Employee_Person_Id__c = null;
        empHR.GP_SUPERVISOR_PERSON_ID__c = null;
		
        update empHR;
        
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
        
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        projectLeadership.GP_End_Date__c = system.today();
        projectLeadership.GP_Project__c = projectObjForIcon.id;
        insert projectLeadership;
        
    }
    @isTest
    public static void testGPDomainEmployeeHRHistoryTracker()
    {
       GPDomainEmployeeHRHistory  obj = new GPDomainEmployeeHRHistory(new List<GP_Employee_HR_History__c >{new GP_Employee_HR_History__c()});  
        
    }
}