@isTest
public class GPCmpServicePrjctBllngMilestnsTracker {

    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static GP_Project__c prjObj;
    public static String jsonresp;

    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Status__c = 'Active and Visible';
        insert objSdo;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        insert prjObj;

        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;

        GP_Billing_Milestone__c objPrjBillingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert objPrjBillingMilestone;

    }

    @isTest static void testGPCmpServiceProjectBillingMilestones() {
        fetchData();
        testgetBillingMilestone();
        testsaveBillingMilestones();
        testdeleteBillingMilestone();
        testdeleteBillingMilestoneBulk();
    }

    public static void fetchData() {
        objPrjBillingMilestone = [select id, GP_Project__c,Name from GP_Billing_Milestone__c limit 1];
        prjObj = [select id, Name from GP_Project__c limit 1];
        jsonresp = (String) JSON.serialize(new List < GP_Billing_Milestone__c > { objPrjBillingMilestone });
    }

    public static void testgetBillingMilestone() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBillingMilestones.getBillingMilestone(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testsaveBillingMilestones() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBillingMilestones.saveBillingMilestones(jsonresp);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteBillingMilestone() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBillingMilestones.deleteBillingMilestone(objPrjBillingMilestone.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteBillingMilestoneBulk() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectBillingMilestones.deleteBillingMilestoneBulk(new List < Id > { objPrjBillingMilestone.id });
        System.assertEquals(true, returnedResponse.isSuccess);
    }
}