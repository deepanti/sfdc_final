@isTest
private class GPBatchtoExistingEmployeeTracker {

    @testSetup static void setupCommonData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_Employee_Master__c empObjforsupervisor4 = GPCommonTracker.getEmployee();
        empObjforsupervisor4.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(30);
        empObjforsupervisor4.GP_SFDC_User__c = objuser.id;
        empObjforsupervisor4.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test@test.com';
        empObjforsupervisor4.GP_Person_ID__c = '1433';
        insert empObjforsupervisor4;

        GP_Employee_Master__c empObjforsupervisor3 = GPCommonTracker.getEmployee();
        empObjforsupervisor3.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(30);
        empObjforsupervisor3.GP_SFDC_User__c = objuser.id;
        empObjforsupervisor3.GP_Supervisor_Employee__c = empObjforsupervisor4.id;
        empObjforsupervisor3.GP_OFFICIAL_EMAIL_ADDRESS__c = 'test@test.com';
        empObjforsupervisor3.GP_Person_ID__c = '1233';
        insert empObjforsupervisor3;

        GP_Employee_HR_History__c emphistoryobj3 = new GP_Employee_HR_History__c();
        emphistoryobj3.GP_Employees__c = empObjforsupervisor3.id;
        emphistoryobj3.GP_Supervisor_Employee__c = empObjforsupervisor3.id;
        insert emphistoryobj3;

        GP_Employee_Master__c empObjforsupervisor = GPCommonTracker.getEmployee();
        empObjforsupervisor.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(30);
        empObjforsupervisor.GP_OFFICIAL_EMAIL_ADDRESS__c = '123@abc.com';
        empObjforsupervisor.GP_Supervisor_Employee__c = empObjforsupervisor3.id;
        empObjforsupervisor.GP_Employee_HR_History__c = emphistoryobj3.ID;
        empObjforsupervisor.GP_Person_ID__c = '123';
        insert empObjforsupervisor;

        GP_Employee_Master__c exceptionemp = GPCommonTracker.getEmployee();
        exceptionemp.GP_Person_ID__c = '1230';
        exceptionemp.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(30));
        insert exceptionemp;

        GP_Employee_HR_History__c emphistoryobj = new GP_Employee_HR_History__c();
        emphistoryobj.GP_Employees__c = empObjforsupervisor.id;
        emphistoryobj.GP_Supervisor_Employee__c = empObjforsupervisor.id;
        insert emphistoryobj;

        GP_Employee_Master__c empObjforsupervisor2 = GPCommonTracker.getEmployee();
        empObjforsupervisor2.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforsupervisor2.GP_SFDC_User__c = objuser.id;
        empObjforsupervisor2.GP_Person_ID__c = '1232';
        insert empObjforsupervisor2;

        GP_Employee_HR_History__c emphistoryobj2 = new GP_Employee_HR_History__c();
        emphistoryobj2.GP_Employees__c = empObjforsupervisor2.id;
        emphistoryobj2.GP_Supervisor_Employee__c = empObjforsupervisor2.id;
        insert emphistoryobj2;

        GP_Employee_Master__c empObjforsecondary = GPCommonTracker.getEmployee();
        empObjforsecondary.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(15));
        empObjforsecondary.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforsecondary.GP_Supervisor_Employee__c = empObjforsupervisor3.id;
        empObjforsecondary.GP_Person_ID__c = '1235';
        insert empObjforsecondary;

        GP_Employee_Master__c empObjforsecondary2 = GPCommonTracker.getEmployee();
        empObjforsecondary2.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(15));
        empObjforsecondary2.GP_Employee_HR_History__c = emphistoryobj2.ID;
        empObjforsecondary2.GP_Person_ID__c = '12329';
        insert empObjforsecondary2;

        GP_Employee_Master__c empObjforthird = GPCommonTracker.getEmployee();
        empObjforthird.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(7));
        empObjforthird.GP_Employee_HR_History__c = emphistoryobj.ID;
        empObjforthird.GP_Person_ID__c = '12356';
        insert empObjforthird;

        GP_Employee_Master__c empObjforthird2 = GPCommonTracker.getEmployee();
        empObjforthird2.GP_ACTUAL_TERMINATION_Date__c = System.Today().adddays(integer.valueOf(7));
        empObjforthird2.GP_Employee_HR_History__c = emphistoryobj2.ID;
        empObjforthird2.GP_Person_ID__c = '12357';
        insert empObjforthird2;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        GP_Project__c project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert project;
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        leadershipMaster.GP_Category__c = 'Mandatory Key Members';
        
        Date todayDate = System.today();
        
        GP_Project_Leadership__c projectClonedLeadership = GPCommonTracker.getProjectLeadership(project.Id, leadershipMaster.Id, todayDate, todayDate.addDays(100), empObjforsecondary2.Id);
        projectClonedLeadership.GP_Last_Temporary_Record_Id__c = project.Id;
        projectClonedLeadership.RecordTypeId = GPCommon.getRecordTypeId('GP_Project_leadership__c', 'Mandatory Key Members');
        projectClonedLeadership.GP_Leadership_Role_Name__c = 'Mandatory Key Members';
        projectClonedLeadership.Is_Cloned_from_Parent__c = true;
        projectClonedLeadership.GP_Employee_ID__r = empObjforsecondary2;
        projectClonedLeadership.GP_Active__c = true;
        insert projectClonedLeadership;

    }

    @isTest
    public static void testGPBatchtoExistingEmployee() {
        testForInitialDays();
        //testForSecondaryDays();
        //testForThirdDays();
    }

    public static void testForInitialDays() {
        GPBatchtoExistingEmployee batcher = new GPBatchtoExistingEmployee();
        Id batchprocessid = Database.executeBatch(batcher, 10);
    }

    public static void testForSecondaryDays() {
        GPBatchtoExistingEmployee batcher = new GPBatchtoExistingEmployee();
        Id batchprocessid = Database.executeBatch(batcher, 10);
    }

    public static void testForThirdDays() {
        GPBatchtoExistingEmployee batcher = new GPBatchtoExistingEmployee();
        Id batchprocessid = Database.executeBatch(batcher, 10);
    }
}