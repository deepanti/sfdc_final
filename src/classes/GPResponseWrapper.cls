public class GPResponseWrapper {
    public Integer code;
    public String message;
    
    public GPResponseWrapper() {}
    
    public GPResponseWrapper(Integer code, String message) {
    	this.code = code;
    	this.message = message;
    }
}