/*

* This class is used to get MOR details from Oracle and insert into 
* vic_Currency_Exchange_Rate__c object for VIC calculation
* created date - 2018/11/21
* created by - Madhuri Sharma

*  */
public class VIC_MORRatesCallout {
    
    @future(callout=true)
    public static void getMORDetails(){
        
        try{
            List<vic_Currency_Exchange_Rate__c> morRatesToInsert = new List<vic_Currency_Exchange_Rate__c>();
            Map<String,Exception__c> exceptionMap = new Map<String,Exception__c>();
            Map<String,vic_Currency_Exchange_Rate__c> finalMORRatesMap = new Map<String,vic_Currency_Exchange_Rate__c>();
            
            String endpointURL = Label.MORRatesEndpointURL;
            String calloutMethod = Label.CalloutMethodGET;
            String calloutHeader = Label.CalloutHeader;
            String[] calloutHeaderValues = calloutHeader.split(','); 
            Date MORDate = Date.newInstance(System.today().year(),System.today().month()+1,29);
            String MORDateFormat = String.valueOf(MORDate);
            System.debug('\n\n ------>> Date Format '+MORDateFormat);
            
            String requestParams = '?CONVERSION_DATE='+MORDateFormat+'&FROM_CURRENCY=USD'+'&CONVERSION_TYPE=MOR'+'&TO_CURRENCY=';
            
            endpointURL = endpointURL +requestParams;
            
            HttpRequest req = IntegrationHelper.sendhttpRequest(endpointURL,calloutMethod,calloutHeaderValues[0],calloutHeaderValues[1],requestParams); 
            
            
            
            String responseBody = IntegrationHelper.getResponse(req);
            System.debug('\n\n -------------->>> Response '+responseBody);
            
            if(responseBody != null){
                
                String status = '';
                String reason = '';
                Exception__c exc = new Exception__c();
                Integer pageSize = 0;
                JSONParser jsonParser = JSON.createParser(responseBody);
                
                Set<String> VICISOCodes = new Set<String>();
                String isoCodes = Label.MORISOCodes;
                String[] isoCodesArray = isoCodes.split(',');
                if(isoCodesArray != null && isoCodesArray.size() > 0){
                    for(String code:isoCodesArray){
                        VICISOCodes.add(code);
                    }
                }
                System.debug('\n\n VIC ISO Codes ----------- '+VICISOCodes);
                
                while(jsonParser.nextToken() != null){
                    if(jsonParser.getCurrentToken() == JSONToken.START_OBJECT){
                            while(jsonParser.nextToken() != JSONToken.END_OBJECT){
                               
                                if(jsonParser.getCurrentToken() == JSONToken.FIELD_NAME && jsonParser.getText() == 'PageSize'){
                                    jsonParser.nextToken();
                                    pageSize = Integer.valueOf(jsonParser.getText());
                                }
                            }
                    }
                }
                System.debug('\n\n ----------->> PageSize '+pageSize);
                
                if(pageSize == 1){
                    
                    finalMORRatesMap = processResults(responseBody,VICISOCodes);
                    System.debug('\n\n ---------->> finalMORRatesMap IF '+finalMORRatesMap);
                    
                }else if(pageSize > 1){
                        
                    
                    
                    Map<String,vic_Currency_Exchange_Rate__c> initialMorRatesMap = new Map<String,vic_Currency_Exchange_Rate__c>();
                    initialMorRatesMap = processResults(responseBody,VICISOCodes);
                    System.debug('\n\n -------- initialMorRatesMap 0'+initialMorRatesMap );
                            
                            
                    for(Integer i=2 ; i<=pageSize ; i++){
                        
                        Map<String,vic_Currency_Exchange_Rate__c> tempMorRatesMap = new Map<String,vic_Currency_Exchange_Rate__c>();
                    
                        String endpointURLPageNo = endpointURL+'&PAGE_NUMBER='+i;
                        
                        HttpRequest req1 = IntegrationHelper.sendhttpRequest(endpointURLPageNo,calloutMethod,calloutHeaderValues[0],calloutHeaderValues[1],requestParams); 
                        String responseBody1 = IntegrationHelper.getResponse(req1);
                        System.debug('\n\n -------------->>> Response '+responseBody1);
                        
                        if(responseBody1 != null){
                            
                            JSONParser jsonParser1 = JSON.createParser(responseBody1);
                            tempMorRatesMap = processResults(responseBody1,VICISOCodes);
                            System.debug('\n\n -------- tempMorRatesMap '+tempMorRatesMap );
                            
                            if(tempMorRatesMap != null && tempMorRatesMap.size() > 0){
                            
                                initialMorRatesMap.putAll(tempMorRatesMap);
                            }
                            System.debug('\n\n -------- initialMorRatesMap 1'+initialMorRatesMap);
                        }else{
                            
                            retryCallout();
                            System.debug('\n\n -------->>> Pagination Exit Retry ');
                        }
                            
                      
                    }
                    finalMORRatesMap.putAll(initialMorRatesMap);
                }else{
                    insertExceptionDetails(responseBody);
                }
                System.debug('\n\n ---------->>> finalMORRatesMap '+finalMORRatesMap.size());
                if(finalMORRatesMap != null && finalMORRatesMap.size() > 0 && finalMORRatesMap.values() != null && finalMORRatesMap.values().size() > 0){
                    //System.debug('\n\n ---------->>> finalMORRatesMap'+finalMORRatesMap);
                    insert finalMORRatesMap.values();
                }
            }else{
                
                retryCallout();
                
            }
        }catch(Exception e){
            
            System.debug('\n\n ----------->>>> Exception in MOR Callout '+e.getMessage());
            Exception__c exc = new Exception__c();
            
            exc.Exception_Class__c = 'VIC_MORRatesCallout';
            exc.Exception_Message__c = e.getMessage();
            exc.Method__c = 'getMORDetails';
            exc.Status__c = 'Fail';
            insert exc;
        }
    }
    
    public static Map<String,vic_Currency_Exchange_Rate__c> processResults(String responseBody,Set<String> VICISOCodes){
        
        
        Map<String,vic_Currency_Exchange_Rate__c> morRatesMap = new Map<String,vic_Currency_Exchange_Rate__c>();
        
        JSONParser jsonParser = JSON.createParser(responseBody);
        
        while(jsonParser.nextToken() != null){
         
         if(jsonParser.getCurrentToken() == JSONToken.START_ARRAY ){
                
                String to_currency = '';
                String conversionDate = '';
                Date finalConversionDate = null;
                Double conversionRate = 0.0;
                vic_Currency_Exchange_Rate__c ex ;
                
                while(jsonParser.nextToken() != JSONToken.END_ARRAY){
                    
                    if(jsonParser.getCurrentToken() == JSONToken.START_OBJECT){
                        
                        while(jsonParser.nextToken() != JSONToken.END_OBJECT){
                            
                            
                            if(jsonParser.getCurrentToken() == JSONToken.FIELD_NAME && jsonParser.getText() == 'TO_CURRENCY'){
                                 
                                 jsonParser.nextToken();
                                 to_currency = jsonParser.getText();
                                 System.debug('\n\n TO CURRENCY --->>> '+to_currency);
                                 jsonParser.nextToken();
                            }
                            if(jsonParser.getCurrentToken() == JSONToken.FIELD_NAME && jsonParser.getText() == 'CONVERSION_DATE'){
                                 
                                 jsonParser.nextToken();
                                 conversionDate = jsonParser.getText();
                                 System.debug('\n\n Conversion Date 1  --->>> '+conversionDate);
                                 
                                 conversionDate = conversionDate.subString(0,conversionDate.indexOf('T'));
                                 System.debug('\n\n Converted Date 2 --->>> '+conversionDate);
                                 
                                 finalConversionDate = Date.valueOf(conversionDate);
                                 System.debug('\n\n finalConversionDate  --->>> '+finalConversionDate );
                            }
                            if(jsonParser.getCurrentToken() == JSONToken.FIELD_NAME && jsonParser.getText() == 'CONVERSION_RATE'){
                                 
                                 jsonParser.nextToken();
                                 String conversionRateStr = jsonParser.getText();
                                 System.debug('\n\n conversionRateStr --->>> '+conversionRate);
                                 conversionRate = Double.valueOf(conversionRateStr);
                                 System.debug('\n\n conversionRate --->>> '+conversionRate);
                                 jsonParser.nextToken();
                            }
                        
                        }
                        if(conversionRate != null && finalConversionDate != null && to_currency != null && VICISOCodes != null && VICISOCodes.size() > 0 && VICISOCodes.contains(to_currency)){
                            
                            
                            String key = to_currency+conversionRate;
                            if(morRatesMap != null && (morRatesMap.Keyset() == null || 
                                    (morRatesMap.keyset() != null && !morRatesMap.Keyset().contains(key)))){
                                //Create a Record
                                ex = new vic_Currency_Exchange_Rate__c();
                                Decimal converionRateWithDecimal = Decimal.valueOf(conversionRate).setScale(4);
                                ex.vic_Exchange_Rate__c = Double.valueOf(converionRateWithDecimal);                                    
                                                                
                                ex.vic_ISO_Code__c = to_currency;                                    
                                ex.vic_Month__c = String.valueOf(finalConversionDate.month());                                   
                                ex.vic_Year__c = String.valueOf(finalConversionDate.year());
                                morRatesMap.put(key,ex);
                                System.debug('\n\n ----->>> morRatesMap '+morRatesMap);  
                            }else{
                                continue;
                            }
                            
                            
                        }
                    }
                
                }
                System.debug('\n\n ---------->>> Final morRatesMap '+morRatesMap);
            }      
                
        }
        return morRatesMap;
        
    }
    
    public static void insertExceptionDetails(String responseBody){
        
        
        ExceptionWrapper exceObject = (ExceptionWrapper)System.JSON.deserialize(responseBody, ExceptionWrapper.class);
        system.debug('@@@'+exceObject);        
        
        if(exceObject.Status != '' && exceObject.Reason != ''){
            
            if(exceObject.Status == 'Fail'){
                retryCallout();
                
            }else{
                System.debug('\n\n ------>> RESPONSE - STATUS : '+exceObject.Status+' REASON: '+exceObject.Reason);
                Exception__c exc = new Exception__c();
                exc.Exception_Class__c = 'VIC_MORRatesCallout';
                exc.Exception_Message__c = exceObject.Reason;
                exc.Method__c = 'getMORDetails';
                exc.Status__c = exceObject.Status;
                String key = exceObject.Status+'-'+exceObject.Reason;
                insert exc;
            }
        }
        
    }
    
    public static void retryCallout(){
        
        System.debug('\n\n -------->>> Entry Retry ');
        String day = string.valueOf(system.now().day());
        String month = string.valueOf(system.now().month());
        String hour = string.valueOf(system.now().hour()+3);
        String minute = string.valueOf(system.now().minute());
        String second = string.valueOf(system.now().second());
        String year = string.valueOf(system.now().year());
        
        String strJobName = 'Job-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
        String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
        System.schedule(strJobName, strSchedule, new VIC_MORRatesCalloutScheduler());
        System.debug('\n\n -------->>> Exit Retry ');
    }
    
    public class ExceptionWrapper{
        public string Status;
        public string Reason;
    }
}