public class identifyStaleUsers {
    
    @AuraEnabled
    public static Boolean chkSF1() 
    {
        Boolean isSF1=false;
        if (UserInfo.getUiThemeDisplayed()=='Theme4t')
            isSF1=true;
        
        system.debug('isSF1: '+ UserInfo.getUiThemeDisplayed() );
        return  isSF1;
    }
    
    @AuraEnabled
    public static Boolean chkProfile() 
    {
        Boolean isStaleProfile=false;
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
            isStaleProfile=true;
        
        system.debug('isStaleProfile: '+ UserInfo.getProfileId() + '   ' + Label.Profile_Stale_Deal_Owners );
        return  isStaleProfile;
    }
    
    @AuraEnabled
   Public Static DealWrapper Fetch_TS_Ageing_Opps()
    {    
        List<Opportunity> stale_oppty = new List<Opportunity>();
    	List<Opportunity> Final_oppty = new List<Opportunity>();
         
        Map<String, Map<String, Set<Opportunity>>> ownerWise_user_DealMap_TS = StaleDealHelper.fetch_TS_Ageing_Opps_new();
        
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
        { 
            Map<String, Set<Opportunity>> deal_map_TS = ownerWise_user_DealMap_TS.get('STALE_USER');
            System.debug('deal_map_TS=='+deal_map_TS);
            
            stale_oppty.addAll(deal_map_TS.get('STALE_DEALS'));
            Final_oppty.addAll(deal_map_TS.get('STALLED_DEALS'));
            
        }
        else{
            Map<String, Set<Opportunity>> deal_map_TS = ownerWise_user_DealMap_TS.get('NORMAL_USER');
            
            Final_oppty.addAll(deal_map_TS.get('STALLED_DEALS'));
            stale_oppty.addAll(deal_map_TS.get('STALE_DEALS'));
        }
        System.debug('stale_oppty=='+stale_oppty.size());
        System.debug('Final_oppty=='+Final_oppty.size());
        DealWrapper dealWrap =  new DealWrapper();
        dealWrap.staleDealsCount = stale_oppty.size();
        dealWrap.stalledDealsCount = Final_oppty.size();
        dealWrap.staleDealsCount_nonTs = 0;
        dealWrap.stalledDealsCount_nonTs = 0;
        return dealWrap;
    }
    
    @AuraEnabled
    Public static DealWrapper Fetch_Non_TS_Ageing_Opps()
    {        
        List<Opportunity> Stale_oppty_nonts =	new List<Opportunity>();
     	List<Opportunity> Final_non_ts_oppty = new List<Opportunity>();	
        Map<String, Map<String, Set<Opportunity>>> ownerWise_user_DealMap_Non_TS = StaleDealHelper.fetch_Non_TS_Ageing_Opps_new();
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
        { 
            Map<String, Set<Opportunity>> deal_map_Non_TS = ownerWise_user_DealMap_Non_TS.get('STALE_USER');
            Stale_oppty_nonts.addAll(deal_map_Non_TS.get('STALE_DEALS'));
            Final_non_ts_oppty.addAll(deal_map_Non_TS.get('STALLED_DEALS'));
         }   
        else{
            Map<String, Set<Opportunity>> deal_map_Non_TS = ownerWise_user_DealMap_Non_TS.get('NORMAL_USER');
            Stale_oppty_nonts.addAll(deal_map_Non_TS.get('STALE_DEALS'));
            Final_non_ts_oppty.addAll(deal_map_Non_TS.get('STALLED_DEALS'));
        }
        System.debug('Stale_oppty_nonts=='+Stale_oppty_nonts.size());
        System.debug('Final_non_ts_oppty=='+Final_non_ts_oppty.size());
        
        DealWrapper dealWrap =  new DealWrapper();
        dealWrap.staleDealsCount_nonTs = Stale_oppty_nonts.size();
        dealWrap.stalledDealsCount_nonTs = Final_non_ts_oppty.size();
        dealWrap.staleDealsCount = 0;
        dealWrap.stalledDealsCount = 0;
        return dealWrap;        
    }
    
    
    public class DealWrapper{
        @AuraEnabled
        public Integer staleDealsCount;
        @AuraEnabled
        public Integer stalledDealsCount;
        @AuraEnabled
        public Integer staleDealsCount_nonTs;
        @AuraEnabled
        public Integer stalledDealsCount_nonTs;
        
    }
}