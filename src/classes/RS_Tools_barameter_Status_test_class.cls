@isTest
public class RS_Tools_barameter_Status_test_class 
{
    static testMethod void runPositiveTestCases() 
    {
        UserRole Obj = new UserRole();
        Obj.Name='TestRole';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id); 
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
                AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test1@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        RevenueStormRB__RevenueStorm_Relationship_Barometer__c	RSobject=new RevenueStormRB__RevenueStorm_Relationship_Barometer__c	(RevenueStormRB__Contact__c= oContact.id,RevenueStormRB__Opportunity__c=oOpportunity.id);
        insert RSobject;
        opportunity opportuniyobject=[select id ,Assocaie_RS_barometer__c from opportunity where id=:oOpportunity.id];
        System.assertEquals(true, opportuniyobject.Assocaie_RS_barometer__c);
    }
    	static testMethod void runPositiveTestCases1() 
    {
        UserRole Obj = new UserRole();
        Obj.Name='TestRole';
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id); 
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
                AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test1@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
       RevenueStormRB__RevenueStorm_Relationship_Barometer__c	RSobject=new RevenueStormRB__RevenueStorm_Relationship_Barometer__c	(RevenueStormRB__Contact__c= oContact.id,RevenueStormRB__Opportunity__c=oOpportunity.id);
        insert RSobject;
        delete RSobject;
        opportunity opportuniyobject=[select id ,Assocaie_RS_barometer__c from opportunity where id=:oOpportunity.id];
        System.assertEquals(false, opportuniyobject.Assocaie_RS_barometer__c);
    }
}