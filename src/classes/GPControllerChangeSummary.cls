/**
 * @author Anmol.kumar
 * @date 22/10/2017
 *
 * @group ChangeSummary
 * @group-content ../../ApexDocContent/ChangeSummary.html
 *
 * @description Apex Controller for Change Summary module
 */
public class GPControllerChangeSummary {

    private final String MAP_OF_FIELDNAME_TO_WRAPPER_LABEL = 'mapOfFieldNameToFieldWrapper';
    private final String PROJECT_VERSION_LABEL = 'listOfProjectVersions';
    private final String SUCCESS_LABEL = 'SUCCESS';

    private Map < String, Map < String, FieldWrapper >> mapOfObjectNameToFieldWrapper = new Map < String, Map < String, FieldWrapper >> ();
    private List < GP_Project_Version_History__c > listOfProjectVersionHistory, listOfSanatizedProjectVersionHistory;
    private Map < String, FieldWrapper > mapOfFieldNameToFieldWrapper;
    private GP_Project_Template__c projectTemplate;
    private GP_Project__c project;
    private JSONGenerator gen;
    private Id projectId;

    /**
     * @description Parameterized constructor to accept project Id.
     * @param projectId SFDC 18/15 digit Project Id
     * 
     * @example
     * new GPControllerChangeSummary(<SFDC 18/15 digit Project Id>);
     */
    public GPControllerChangeSummary(Id projectId) {
        this.projectId = projectId;
    }

    /**
     * @description method to return project version.
     * 
     * @example
     * new GPControllerChangeSummary(<SFDC 18/15 digit Project Id>).getProjectVersions();
     */
    public GPAuraResponse getProjectVersions() {
        try {
            setProject();
            setListOfProjectVersions();
            setMapOfFieldNameToLabel('GP_Project__c');
            setMapOfFieldNameToLabel('GP_Project_Work_Location_SDO__c');
            setMapOfFieldNameToLabel('GP_Resource_Allocation__c');
            setJson();
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }


    private void setProject() {
        project = [SELECT Id, GP_Parent_Project__c,
            GP_Project_Template__r.GP_Final_JSON_1__c,
            GP_Project_Template__r.GP_Final_JSON_2__c,
            GP_Project_Template__r.GP_Final_JSON_3__c
            FROM GP_Project__c
            WHERE Id =: projectId
        ];
    }

    
    private void setMapOfFieldNameToLabel(String objectName) {

        List < Schema.DescribeSObjectResult > describeSobjectsResult = Schema.describeSObjects(new List < String > { objectName });
        mapOfFieldNameToFieldWrapper = new Map < String, FieldWrapper > ();

        Map < String, Schema.SObjectType > schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(objectName);
        Map < String, Schema.SObjectField > fieldMap = leadSchema.getDescribe().fields.getMap();

        for (String fieldName: fieldMap.keySet()) {
            FieldWrapper FieldWrapperObject = new FieldWrapper(fieldName,
                fieldMap.get(fieldName).getDescribe().getLabel().toLowerCase(),
                String.valueOf(fieldMap.get(fieldName).getDescribe().getType()));
            mapOfFieldNameToFieldWrapper.put(fieldName, FieldWrapperObject);
        }
        mapOfObjectNameToFieldWrapper.put(objectName, mapOfFieldNameToFieldWrapper);
    }

    private void setListOfProjectVersions() {

        List<GP_Project_Version_Line_Item_History__c> listOfSDOVersionLineItem,
                                                        listOfBudgetVersionLineItem,
                                                        listOfExpenseVersionLineItem,
                                                        listOfDocumentVersionLineItem,
                                                        listOfLeadershipVersionLineItem,
                                                        listOfWorkLocationVersionLineItem,
                                                        listOfApproverCommentVersionLineItem,
                                                        listOfProfileBillRateVersionLineItem,
                                                        listOfBillingMilestoneVersionLineItem,
                                                        listOfResourceAllocationVersionLineItem;

        String query = 'SELECT Id, Name,GP_Project_JSON__c, GP_Work_Location_JSON__c,GP_Additional_SDO_JSON__c, GP_Leadership_JSON__c,';
        query += 'GP_Budget_JSON__c, GP_Billing_Milestones_JSON__c,GP_Address_JSON__c , GP_Resource_Allocation_JSON__c, GP_Expenses_JSON__c,';
        query += ' GP_Project_Document_JSON__c, ';
        
        query += '(SELECT Id, GP_Related_Record_Type__c, GP_JSON1__c, GP_JSON2__c, GP_JSON3__c, GP_JSON4__c, GP_JSON5__c, ';
        query += 'GP_JSON7__c, GP_JSON8__c, GP_JSON9__c, GP_JSON10__c, GP_JSON11__c, GP_JSON12__c, GP_JSON6__c FROM GP_Project_Version_Line_Item_History__r) ';

        query += 'FROM GP_Project_Version_History__c WHERE ';

        if (project.GP_Parent_Project__c != null) {
            query += 'GP_Project__c = \'' + project.GP_Parent_Project__c + '\'';
        } else {
            query += 'GP_Project__c =\'' + project.Id + '\'';
        }

        query += 'ORDER By CreatedDate Desc';
        listOfProjectVersionHistory = DataBase.Query(query);
        listOfSanatizedProjectVersionHistory = new List<GP_Project_Version_History__c>();

        for(GP_Project_Version_History__c currentVersionHistory: listOfProjectVersionHistory) {
            listOfSDOVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            listOfLeadershipVersionLineItem         = new List<GP_Project_Version_Line_Item_History__c>();
            listOfWorkLocationVersionLineItem       = new List<GP_Project_Version_Line_Item_History__c>();
            listOfBillingMilestoneVersionLineItem   = new List<GP_Project_Version_Line_Item_History__c>();
            listOfResourceAllocationVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();

            listOfBudgetVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            listOfExpenseVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            listOfDocumentVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            listOfProfileBillRateVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            listOfApproverCommentVersionLineItem = new List<GP_Project_Version_Line_Item_History__c>();
            
            for(GP_Project_Version_Line_Item_History__c currentVersionLineItem : currentVersionHistory.GP_Project_Version_Line_Item_History__r) {

                if(currentVersionLineItem.GP_Related_Record_Type__c == 'Work Location') {
                    listOfWorkLocationVersionLineItem.add(currentVersionLineItem);
                } else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Project Leadership') {
                    listOfLeadershipVersionLineItem.add(currentVersionLineItem);
                } else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Billing Milestone') {
                    listOfBillingMilestoneVersionLineItem.add(currentVersionLineItem);
                } else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Resource Allocation') {
                    listOfResourceAllocationVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Budget') {
                    listOfBudgetVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Expense') {
                    listOfExpenseVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Document') {
                    listOfDocumentVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Profile Bill Rate') {
                    listOfProfileBillRateVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'Approver Comment') {
                    listOfApproverCommentVersionLineItem.add(currentVersionLineItem);
                }  else if(currentVersionLineItem.GP_Related_Record_Type__c == 'SDO') {
                    listOfSDOVersionLineItem.add(currentVersionLineItem);
                } 
            }

            if(listOfWorkLocationVersionLineItem != null && !listOfWorkLocationVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Work_Location_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfWorkLocationVersionLineItem);
            } 
            
            if(listOfLeadershipVersionLineItem != null && !listOfLeadershipVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Leadership_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfLeadershipVersionLineItem);
            } 
            
            if(listOfBillingMilestoneVersionLineItem != null && !listOfBillingMilestoneVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Billing_Milestones_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfBillingMilestoneVersionLineItem);
            } 
            
            if(listOfResourceAllocationVersionLineItem != null && !listOfResourceAllocationVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Resource_Allocation_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfResourceAllocationVersionLineItem);
            } 
            
            if(listOfBudgetVersionLineItem != null && !listOfBudgetVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Budget_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfBudgetVersionLineItem);
            } 
            
            if(listOfExpenseVersionLineItem != null && !listOfExpenseVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Expenses_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfExpenseVersionLineItem);
            } 
            
            if(listOfDocumentVersionLineItem != null && !listOfDocumentVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Project_Document_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfDocumentVersionLineItem);
            } 
            
            if(listOfProfileBillRateVersionLineItem != null && !listOfProfileBillRateVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Profile_Bill_Rate_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfProfileBillRateVersionLineItem);
            } 
            
            if(listOfApproverCommentVersionLineItem != null && !listOfApproverCommentVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Approver_Comments__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfApproverCommentVersionLineItem);
            }

            if(listOfSDOVersionLineItem != null && !listOfSDOVersionLineItem.isEmpty()) {
                currentVersionHistory.GP_Additional_SDO_JSON__c = GPBaseProjectUtil.getSerializedVersionLineItemJSON(listOfSDOVersionLineItem);
            }

            listOfSanatizedProjectVersionHistory.add(currentVersionHistory);
        }
    }

    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (listOfSanatizedProjectVersionHistory != null)
            gen.writeObjectField(PROJECT_VERSION_LABEL, listOfSanatizedProjectVersionHistory);
        if (project != null)
            gen.writeObjectField('project', project);

        if (mapOfObjectNameToFieldWrapper != null)
            gen.writeObjectField(MAP_OF_FIELDNAME_TO_WRAPPER_LABEL, mapOfObjectNameToFieldWrapper);

        gen.writeEndObject();
    }

    class FieldWrapper {
        public String name;
        public String label;
        public String fieldType;

        public FieldWrapper(String name, String label, String fieldType) {
            this.name = name;
            this.label = label;
            this.fieldType = fieldType;
        }
    }
}