// APC Change
@isTest public class GPDomainProjectDeleteTracker {
                @testSetup static void setupCommonData() {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'GP_Project__Delete__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;
        
        GP_Project__c prjObj1 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj1.OwnerId = objuser.Id;
        insert prjObj1;
               
               GP_Project__c prjObj11 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj11.OwnerId = objuser.Id;
        insert prjObj11;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
               prjObj.GP_Clone_Source__c = 'First Account PID';
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Parent_Project__c = prjObj1.id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Oracle_Status__c = 'NA';
        prjObj.GP_is_Send_Notification__c = True;
               prjObj.GP_Customer_Hierarchy_L4__c = accobj.Id;
        insert prjObj;
      
               GP_Project__c prjObj2 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj2.OwnerId = objuser.Id;
               prjObj2.GP_Clone_Source__c = 'Second Account PID';
        prjObj2.GP_CRN_Number__c = iconMaster.Id;
        prjObj2.GP_Parent_Project__c = prjObj11.id;
        prjObj2.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj2.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj2.GP_Approval_Status__c = 'Draft';
        prjObj2.GP_Oracle_Status__c = 'NA';
        prjObj2.GP_is_Send_Notification__c = True;
               prjObj2.GP_Customer_Hierarchy_L4__c = accobj.Id;
               insert prjObj2;
      
               // BPH Change
               GP_Project__c prjObj3 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj3.OwnerId = objuser.Id;
               prjObj3.GP_Clone_Source__c = 'Batch - Hierarchy Update';
        prjObj3.GP_CRN_Number__c = iconMaster.Id;
        prjObj3.GP_Parent_Project__c = prjObj11.id;
        prjObj3.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj3.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj3.GP_Approval_Status__c = 'Draft';
        prjObj3.GP_Oracle_Status__c = 'NA';
        prjObj3.GP_is_Send_Notification__c = True;
               prjObj3.GP_Customer_Hierarchy_L4__c = accobj.Id;
               insert prjObj3;

        GP_Project_Version_History__c projectVersionHistory = GPCommonTracker.getProjectVersionHistory(prjObj1.Id);
        insert projectVersionHistory;

        GP_Project_Classification__c projectClassification = GPCommonTracker.getProjectClassification(prjObj1.Id);
        insert projectClassification;
    }

    static testmethod void testbatch() {
        Test.StartTest();
        
        GP_Project__c objProject = [Select Id From GP_Project__c where GP_Parent_Project__c != null 
                                    AND GP_Clone_Source__c = 'Second Account PID' limit 1];
        
        objProject.GP_Approval_Status__c = 'Closed';
        objProject.GP_Oracle_Status__c = 'S';
        update objProject;
        
        Test.StopTest();       
    }
    
    static testmethod void testbatchSecond() {
        Test.StartTest();
        
        GP_Project__c objProject = [Select Id From GP_Project__c where GP_Parent_Project__c != null 
                                    AND GP_Clone_Source__c = 'First Account PID' limit 1];
        
        objProject.GP_Approval_Status__c = 'Approved';
        objProject.GP_Oracle_Status__c = 'S';
        update objProject;
        
        Test.StopTest();       
    }
    
    static testmethod void testBPHChange() {
        Test.StartTest();
        
        GP_Project__c objProject = [Select Id From GP_Project__c where GP_Parent_Project__c != null 
                                    AND GP_Clone_Source__c = 'Batch - Hierarchy Update' limit 1];
        
        objProject.GP_Approval_Status__c = 'Approved';
        objProject.GP_Oracle_Status__c = 'S';
        update objProject;
        
        Test.StopTest();       
    }
}
