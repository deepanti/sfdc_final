@isTest
public class redirectToPanel_Test {

    @isTest static void testRedirect(){
        try{
           
             Profile p = [SELECT Id FROM Profile WHERE Name = 'Genpact Sales Rep' OR Name = 'Genpact SL / CP' OR Name = 'Genpact SL/CP without IP' LIMIT 1];
             User u = new User(Alias = 'SiAbh', Email='sinha.abhishek@genpact.com.preprod', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='sinha.abhishek@testorg.com');
            insert u;
            
            APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
            insert objConga;
            
            Master_VIC_Role__c masterVicRole = new Master_VIC_Role__c();
            insert masterVicRole;
            
            Plan__c planObj = new Plan__c();
            planObj.Is_Active__c = false;
            planObj.Year__c = '2017';
            insert planObj;    
        
            User_VIC_Role__c userVicRole = new User_VIC_Role__c();
            userVicRole.User__c = u.Id;
            userVicRole.Master_VIC_Role__c = masterVicRole.Id;
            insert userVicRole;
            
            VIC_Role__c vicRole = new VIC_Role__c();
            vicRole.Name = 'testing Vic ROle';
            vicRole.Master_VIC_Role__c = masterVicRole.Id;
            vicRole.Plan__c = planObj.Id;
            insert vicRole;
        System.runAs(u){
            redirectToPanel.redirectToUserPanel();
        }
        }catch(Exception e){
            System.debug('Line Number  == '+e.getLineNumber()+' Error == '+e.getMessage());
        }
    }
    
    @isTest static void testRedirectElse(){
    	try{
           
             Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
             User u = new User(Alias = 'SiAbh', Email='sinha.abhishek@genpact.com.preprod', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='sinha.abhishek@testorg.com');
            insert u;
            
            APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
            insert objConga;
            
            Master_VIC_Role__c masterVicRole = new Master_VIC_Role__c();
            insert masterVicRole;
            
            Plan__c planObj = new Plan__c();
            planObj.Is_Active__c = false;
            planObj.Year__c = '2017';
            planObj.Name = 'SL CP Enterprise';
            insert planObj; 
            
            Plan__c planObj1 = new Plan__c();
            planObj1.Is_Active__c = false;
            planObj1.Year__c = '2017';
            planObj1.Name = 'SEM';
            insert planObj1;
        
            User_VIC_Role__c userVicRole = new User_VIC_Role__c();
            userVicRole.User__c = u.Id;
            userVicRole.Master_VIC_Role__c = masterVicRole.Id;
            insert userVicRole;
            
            VIC_Role__c vicRole = new VIC_Role__c();
            vicRole.Name = 'testing Vic ROle';
            vicRole.Master_VIC_Role__c = masterVicRole.Id;
            vicRole.Plan__c = planObj.Id;
            insert vicRole;
            redirectToPanel.redirectToUserPanel();
        
        }catch(Exception e){
            System.debug('Line Number  == '+e.getLineNumber()+' Error == '+e.getMessage());
        }
    }
    
     @isTest static void testRedirectElse1(){
    	try{
           
             Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
             User u = new User(Alias = 'SiAbh', Email='sinha.abhishek@genpact.com.preprod', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='sinha.abhishek@testorg.com');
            insert u;
            
            APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
            insert objConga;
            
            Master_VIC_Role__c masterVicRole = new Master_VIC_Role__c();
            insert masterVicRole;
            
            redirectToPanel.redirectToUserPanel();
        
        }catch(Exception e){
            System.debug('Line Number  == '+e.getLineNumber()+' Error == '+e.getMessage());
        }
    }

        @isTest static void testRedirectElse3(){
    	try{
             Profile p = [SELECT Id FROM Profile];
             user u = new user();
             insert u;
             redirectToPanel.redirectToUserPanel();
        
        }catch(Exception e){
            System.debug('Line Number  == '+e.getLineNumber()+' Error == '+e.getMessage());
        }
    }
}