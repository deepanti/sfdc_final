@isTest
public class VIC_ScorecardBatchTest{
static testMethod void ValidateScoreCard()
{

LoadData();

VIC_ScorecardBatch obj =new VIC_ScorecardBatch();
DataBase.executeBatch(obj);
System.AssertEquals(200,200);

}
static void LoadData()
{
 VIC_Process_Information__c processInfo = new VIC_Process_Information__c();
 processInfo.VIC_Annual_Process_Year__c=system.today().year();
 processInfo.VIC_Process_Year__c=system.today().year();
 insert processInfo;
    
    

user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','Genpact Super Admin','China');
insert objuser;

user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='IT_GRM';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
//objopp.Number_of_Contract__c=2;
objopp.Sales_country__c='Canada';

insert objOpp;


OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.Product_BD_Rep__c=objuser1.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id;   
objOLI.TCV__c=10000;
objOLI.vic_Primary_Sales_Rep_Split__c=10;
insert objOLI;

objopp.Number_of_Contract__c=2;    
objOpp.stagename='6. Signed Deal';
test.starttest();
system.runas(objuser){
update objOpp;
}
test.stoptest();
    
system.debug('objOLI'+objOLI);

Target__c targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
targetObj.Start_date__c=system.today();

targetObj.Plan__C= planObj1.id;  
insert targetObj;

Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('TCV Accelerator','Currency');
masterPlanComponentObj.vic_Component_Code__c='OP'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;

Master_Plan_Component__c masterPlanComponentObjnew=VIC_CommonTest.fetchMasterPlanComponentData('OP','Currency');
masterPlanComponentObjnew.vic_Component_Code__c='PM'; 
masterPlanComponentObjnew.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObjnew;
    
Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('TCV Accelerator','Currency');
masterPlanComponentObj1.vic_Component_Code__c='MBO'; 
masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj1;
 
VIC_Calculation_Matrix__c vicMatrix1=VIC_CommonTest.fetchMatrixComponentData('IO');
insert vicMatrix1;
VIC_Calculation_Matrix__c vicMatrix2=VIC_CommonTest.fetchMatrixComponentData('SEM');
insert vicMatrix2;   
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
PlanComponentObj.VIC_IO_Calculation_Matrix_1__c=vicMatrix1.id;
PlanComponentObj.VIC_TS_Calculation_Matrix_2__c=vicMatrix2.id;    
insert PlanComponentObj;
    
Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
targetComponentObj.Target__C=targetObj.id;
targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
targetComponentObj.vic_Achievement_Percent__c=10;
insert targetComponentObj;

Target_Component__c targetComponentObjNew=VIC_CommonTest.getTargetComponent();
targetComponentObjNew.Target__C=targetObj.id;
targetComponentObjNew.Master_Plan_Component__c=masterPlanComponentObjNew.id;
targetComponentObjNew.vic_Achievement_Percent__c=10;
insert targetComponentObjNew;
    
Target_Component__c targetComponentObj1=VIC_CommonTest.getTargetComponent();
targetComponentObj1.Target__C=targetObj.id;
targetComponentObj1.Master_Plan_Component__c=masterPlanComponentObj1.id;
targetComponentObj1.vic_Achievement_Percent__c=10;
insert targetComponentObj1;


Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
insert objInc;
    
system.debug('targetComponentObj'+targetComponentObj);
system.debug('objuser'+objuser);    
    



}    

}