/** 
	Description  : This class will cover test cases for GPQueueableCreateVersionLineItems class which creates version line items of PID.
	Created by   : Dhruv Singh.
	OHR 	     : 703248825.
	Created On   : 15-10-19. 
*/
@isTest
public class GPQueueableCreateVersionLineItemsTracker {
	@testSetup
    static void testData() {        
        // SDO.
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        objSdo.GP_Business_Type__c =  'PBB';
        insert objSdo;
        
        // OU.
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Billing Entity').getRecordTypeId();
        objpinnacleMaster.GP_Oracle_Id__c = '10001';
        insert objpinnacleMaster;
        
        // Business hierarchy
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        BSobj.For__c = 'GE';
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        accobj.Name = 'Test Account Automation';
        accobj.Business_Group__c = 'GE';
        accobj.Industry_Vertical__c = 'BFS';
        accobj.Sub_Industry_Vertical__c = 'BFS';
        insert accobj ;        
        
        // PID data
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Support';
        dealObj.GP_Deal_Category__c = 'Support';
        dealObj.GP_Status__c = 'Qualified';
        insert dealObj;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c  = null;
        insert objrole;
        
        // PT
        GP_Project_Template__c pt = new GP_Project_Template__c();
        pt.Name = 'INDIRECT-INDIRECT';
        pt.GP_Active__c = true;
        pt.GP_Business_Group_L1__c = 'All';
        pt.GP_Business_Segment_L2__c = 'All';
        pt.GP_Business_Name__c = 'Indirect';
        pt.GP_Business_Type__c = 'Indirect';
        insert pt;
        
        GP_Project__c prjObjAccount = GPCommonTracker.getProject(dealObj,'Indirect PID',pt,objuser,objrole);
        prjObjAccount.Name = 'ACNT - IO/MS - Test';
        prjObjAccount.GP_Deal__c = dealObj.id;
        prjObjAccount.GP_Primary_SDO__c = objSdo.id;
        prjObjAccount.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObjAccount.GP_CRN_Status__c = 'Test';
        prjObjAccount.GP_MOD__c = 'Hard Copy';
        prjObjAccount.GP_Project_type__c = 'Account';
        prjObjAccount.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObjAccount.GP_Oracle_Internal_Id__c = '011';
        prjObjAccount.GP_Oracle_PID__c = '850040951';//
        prjObjAccount.GP_Approval_Status__c = 'Draft';
        prjObjAccount.GP_Oracle_Status__c = '';
        insert prjObjAccount;
       	     
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accobj.Id, 'Active');
        leadershipMaster.GP_Type_of_Leadership__c = 'Billing Approver'; 
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObjAccount.Id, leadershipMaster.Id);
        projectLeadership.recordTypeId = GPCommon.getRecordTypeId('GP_Project_Leadership__c', 'Mandatory Key Members');
        insert projectLeadership;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.Id, prjObjAccount.Id);
        insert resourceAllocationObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObjAccount.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObjAccount.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObjAccount.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObjAccount.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObjAccount.Id);
        insert objDoc;
        
        String uniqueKey = GPCommon.getUniqueKey(prjObjAccount.GP_EP_Project_Number__c);        
        GP_Project_Version_History__c objVersion = new GP_Project_Version_History__c();
        objVersion.GP_Unique_Version_History__c = uniqueKey;
        objVersion.GP_Project__c = prjObjAccount.id;
        objVersion.Name = 'Version - '+ 1;
        objVersion.GP_Version_No__c = 1;
        
        insert objVersion;
    }
    
    @isTest
    static void testQueueable() {
        List<GP_Project__c> listOfPIDs = [Select id, GP_Approval_Status__c, GP_Oracle_Status__c, GP_EP_Project_Number__c from GP_Project__c 
                                          where GP_Oracle_PID__c = '850040951'];
        
        Map<Id,Id> mapOfProjectIdVsVersionHistoryParentId = new Map<Id,Id>();
        mapOfProjectIdVsVersionHistoryParentId.put(listOfPIDs[0].Id, listOfPIDs[0].Id);
        
        GPBaseProjectUtil baseProjectUtil = new GPBaseProjectUtil();
        baseProjectUtil.fetchErrorRecord = true;
        baseProjectUtil.queryProjectAndChilRecords(new Set<Id>{listOfPIDs[0].Id});
        baseProjectUtil.setMapOfProjectChildRecords();
        
        Test.startTest();
        GPQueueableCreateVersionLineItems gq = new GPQueueableCreateVersionLineItems(baseProjectUtil,mapOfProjectIdVsVersionHistoryParentId);
        System.enqueueJob(gq);
        Test.stopTest();
    }
}