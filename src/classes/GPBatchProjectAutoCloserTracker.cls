// ------------------------------------------------------------------------------------------------ 
// Description: Tracker Class for GPBatchProjectAutoCloser
// ------------------------------------------------------------------------------------------------
//Created By : Ved Prakash
// ------------------------------------------------------------------------------------------------ 
@isTest public class GPBatchProjectAutoCloserTracker {
     @testSetup
    static void setupCommonData() {

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_End_Date__c = System.Today().adddays(-integer.valueOf(system.label.GP_Days_Within_Project_End_Date_For_Autoclosure));
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Is_Closed__c = false;
        insert prjObj;
    }
    
    static testmethod void testbatch() {
        Test.StartTest();
        GPBatchProjectAutoCloser batcher = new GPBatchProjectAutoCloser();
        Id batchprocessid = Database.executeBatch(batcher, 10);
        Test.StopTest();
    }

}