@isTest
public class VIC_SupervisorDataManagerCtlrTest{
    public static OpportunityLineItem  objOLI;     
    public static user objuser;
    public static Master_Plan_Component__c masterPlanComponentObj;
    public static list<Target_Achievement__c> lstInc = new list<Target_Achievement__c>();
    public static list<Target_Achievement__c> lstInc1 = new list<Target_Achievement__c>();
    public static list<Target_Achievement__c> lstInc2 = new list<Target_Achievement__c>();
    public static list<Target_Achievement__c> lstInc3 = new list<Target_Achievement__c>();
    public static Target_Achievement__c ObjInc;
    public static Target_Achievement__c ObjInc1;
    public static Target_Achievement__c ObjInc2;
    public static Target_Achievement__c ObjInc3;
    public static Target_Achievement__c ObjInc4;
    public static Target_Component__c targetComponentObj;
    public static case objcase;
    public static Map<Id,VIC_OpportunityLineItemDataWrapCtlr> mapCaseIdToOLIWrap= new Map<Id,VIC_OpportunityLineItemDataWrapCtlr>();
    public static user ObjUser1;
    public static list<VIC_OpportunityLineItemDataWrapCtlr> lstWrap= new list<VIC_OpportunityLineItemDataWrapCtlr>();
    public static list<case> listcase=new list<case>();
    public static Map<Target_Achievement__c,Case > mapTargetAchievementToCase= new Map<Target_Achievement__c,Case >();
    public static VIC_OpportunityDataWrapCtlr OpportunityDataWrapCtlr;
    static void LoadData()
    {
        VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Process_Year__c=2018;
        vicInfo.VIC_Annual_Process_Year__c=2018;
        insert vicInfo;
        
        objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
        
        
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn11@jdjhdg.com','Genpact Super Admin','China');
        objuser.Supervisor__c=objuser1.id;
        insert objuser;
        
        
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
        
        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
        
        Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';  
        planObj1.Year__c ='2018';
        insert planobj1;
        
        
        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser1.id;
        objopp.Sales_country__c='Canada';
        
        insert objOpp;
        
        
        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_Contract_Term__c=24;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;   
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.vic_IO_TS__c ='TS';    
        insert objOLI;
        
        objopp.Number_of_Contract__c=2;    
        objOpp.stagename='6. Signed Deal';
        test.starttest();
system.runas(objuser){
update objOpp;
VIC_SupervisorDataManagerCtlr.fetchUser();
            VIC_SupervisorDataManagerCtlr.getFinancialYear();
            VIC_SupervisorDataManagerCtlr.submitBonusLetter('2018', UserInfo.getUserId());
             VIC_SupervisorDataManagerCtlr.getInitLoadPageData('Supervisor Pending');

}
            
test.stoptest();
        
        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        targetObj.Plan__c=planobj1.id;
        insert targetObj;
        
        masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Discretionary_Payment'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
        Master_Plan_Component__c masterPlanComponentObj1=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj1.vic_Component_Code__c='kickers'; 
        masterPlanComponentObj1.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj1;
        Master_Plan_Component__c masterPlanComponentObj2=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj2.vic_Component_Code__c='New_logo_Bonus'; 
        masterPlanComponentObj2.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj2;
        Master_Plan_Component__c masterPlanComponentObj3=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj3.vic_Component_Code__c='Profitable_Bookings_TS'; 
        masterPlanComponentObj3.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj3;
        
        Master_Plan_Component__c masterPlanComponentObj4=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj4.vic_Component_Code__c='Renewal'; 
        masterPlanComponentObj4.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj4;
        
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
        
        targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.target__r=targetObj;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        Target_Component__c targetComponentObj1=VIC_CommonTest.getTargetComponent();
        targetComponentObj1.Target__C=targetObj.id;
        targetComponentObj1.target__r=targetObj;
        targetComponentObj1.Master_Plan_Component__c=masterPlanComponentObj1.id;
        insert targetComponentObj1;
        Target_Component__c targetComponentObj2=VIC_CommonTest.getTargetComponent();
        targetComponentObj2.Target__C=targetObj.id;
        targetComponentObj2.target__r=targetObj;
        targetComponentObj2.Master_Plan_Component__c=masterPlanComponentObj2.id;
        insert targetComponentObj2;
        Target_Component__c targetComponentObj3=VIC_CommonTest.getTargetComponent();
        targetComponentObj3.Target__C=targetObj.id;
        targetComponentObj3.target__r=targetObj;
        targetComponentObj3.Master_Plan_Component__c=masterPlanComponentObj3.id;
        insert targetComponentObj3;
        
        Target_Component__c targetComponentObj4=VIC_CommonTest.getTargetComponent();
        targetComponentObj4.Target__C=targetObj.id;
        targetComponentObj4.target__r=targetObj;
        targetComponentObj4.Master_Plan_Component__c=masterPlanComponentObj4.id;
        insert targetComponentObj4;
        
        
        ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.Target_Component__r=targetComponentObj;
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc.vic_Status__c ='Supervisor Pending';
        ObjInc.VIC_Description__c='Tests';
        ObjInc.vic_Incentive_Type__c='Adjustment';
        insert objInc;
        
        ObjInc1= VIC_CommonTest.fetchIncentive(targetComponentObj1.id,1000);
        ObjInc1.Target_Component__r=targetComponentObj1;
        ObjInc1.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc1.vic_Status__c ='Supervisor Pending';
        ObjInc1.vic_Incentive_Type__c='Adjustment';
        insert objInc1;
        
        ObjInc2= VIC_CommonTest.fetchIncentive(targetComponentObj2.id,1000);
        ObjInc2.Target_Component__r=targetComponentObj2;
        ObjInc2.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc2.vic_Status__c ='Supervisor Pending';
        ObjInc2.vic_Incentive_Type__c='Adjustment';
        insert objInc2;
        ObjInc3= VIC_CommonTest.fetchIncentive(targetComponentObj3.id,1000);
        ObjInc3.Target_Component__r=targetComponentObj3;
        ObjInc3.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc3.vic_Status__c ='Supervisor Pending';
        ObjInc3.vic_Incentive_Type__c='Adjustment';
        insert objInc3;
        
        ObjInc4= VIC_CommonTest.fetchIncentive(targetComponentObj4.id,1000);
        ObjInc4.Target_Component__r=targetComponentObj3;
        ObjInc4.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc4.vic_Status__c ='Supervisor Pending';
        ObjInc4.vic_Incentive_Type__c='Adjustment';
        insert objInc4;
        
        lstInc.add(objInc);
        lstInc1.add(objInc1);
        lstInc2.add(objInc2);
        lstInc3.add(objInc3);
        
        
        listcase.add(vic_CommonUtil.createCase('New','Low',objuser.id,'case',ObjOLI.id,objOpp.id));
        
        mapTargetAchievementToCase.put(ObjInc,listcase[0]);
        
        lstWrap.add(new VIC_OpportunityLineItemDataWrapCtlr(true,null,'','',
                                                            '','','','','','','','',true,'','',null,null,null,null,null,null));
        
        OpportunityDataWrapCtlr=new VIC_OpportunityDataWrapCtlr(false,null,'', '', '', '', '', '','','','','', '',null);
    }
    
    static testmethod void Test1(){
        
        LoadData();
        Map<String, String> mapUserIdToVICRole = new Map<string, string>();
        Map<Id,User> mapUserIdToObjUser= new Map<Id,user>();
        mapUserIdToVICRole=vic_CommonUtil.getUserVICRoleNameinMap();
        
       
        mapUserIdToObjUser = VIC_SupervisorDataManagerCtlr.fetchUserBySupervisor(ObjUser1.id,mapUserIdToVICRole);
        
        Map<Id,Id> mapUserIdToDiscreComponentIdARG = new map<id,id>();
        
        mapUserIdToDiscreComponentIdARG.put(targetComponentObj.Target__r.User__r.Id,targetComponentObj.id);
        
        Map<String,OpportunityLineItem> mapStrIDToObjOLI = new Map<String,OpportunityLineItem>();
        mapStrIDToObjOLI.put(objoli.id,objoli);  
        system.runas(objuser1){
           
            
            VIC_SupervisorDataManagerCtlr.initIncentiveByUserOppOLI(mapStrIDToObjOLI,mapUserIdToObjUser,mapUserIdToDiscreComponentIdARG);
            
            
            VIC_SupervisorDataManagerCtlr.fetchCumulativeIncetiveByUser(mapUserIdToVICRole,mapUserIdToObjUser);
            
            
            VIC_OpportunityDataWrapCtlr wrapopp=new VIC_OpportunityDataWrapCtlr(true,null,'', '', '', '', '', '','','','','', '',null); 
            
            List<VIC_OpportunityDataWrapCtlr> listOpportunityDataWrapCtlr=new list<VIC_OpportunityDataWrapCtlr>();
            listOpportunityDataWrapCtlr.add(wrapopp);
            VIC_UserOpportunityDataWrapCtlr wrapuseropp=new VIC_UserOpportunityDataWrapCtlr(true,null,'', '', '', '', '', '','','','','', listOpportunityDataWrapCtlr);
            
            wrapuseropp.objUser=objuser;
            wrapuseropp.strTotalVICComputed='1000'; 
            
            List<VIC_UserOpportunityDataWrapCtlr> listUserOpportunityDataWrapCtlr=new list<VIC_UserOpportunityDataWrapCtlr>();
            
            listUserOpportunityDataWrapCtlr.add(wrapuseropp);
            
            String jsonUserOpportunityDataWrapCtlr= json.serialize(listUserOpportunityDataWrapCtlr);
            String json= json.serialize(wrapuseropp);
            Set<String> setIncentiveStatus = new Set<String>();
            setIncentiveStatus.add('HR - Pending');
            
            Map<ID,VIC_UserOpportunityDataWrapCtlr> mapUserIDToObjUserOppDataWrapARG= new  Map<ID,VIC_UserOpportunityDataWrapCtlr>();
            mapUserIDToObjUserOppDataWrapARG.put(wrapuseropp.objUser.id,wrapuseropp);
            VIC_SupervisorDataManagerCtlr.addingScorecardCmpIncentiveSum(mapUserIDToObjUserOppDataWrapARG,setIncentiveStatus);
            
            VIC_SupervisorDataManagerCtlr.insertCases(listcase);
            VIC_SupervisorDataManagerCtlr.approvOLIIncentive(json);
            VIC_SupervisorDataManagerCtlr.pendingOLIIncentive(json);
            VIC_SupervisorDataManagerCtlr.approvSubmitIncentive(jsonUserOpportunityDataWrapCtlr) ;
            // VIC_SupervisorDataManagerCtlr.settingIncentiveStatus(mapTargetAchievementToCase,lstWrap[0],'New',listcase[0]);
            VIC_SupervisorDataManagerCtlr.refactorIncentiveByCase(mapTargetAchievementToCase);
            
            
        }
        
        System.assertEquals(200,200);
       
    }
}