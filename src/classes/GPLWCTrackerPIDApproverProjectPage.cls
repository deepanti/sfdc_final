@isTest(seealldata=true)
public class GPLWCTrackerPIDApproverProjectPage {    
	@isTest
    static void testPIDData() {
        // Create PID full data
        GPCommonUtil.createLWCTestData(true);
        
        List<GP_Project__c> listOfPID = [Select id, GP_Current_Working_User__c from GP_Project__c where Name = 'Mobile App PID'];
        
        List<GP_Project_Template__c> listOfPTemp = [Select id from GP_Project_Template__c where name = 'PBB-CMITS-GC-MS'];
        
        listOfPID[0].GP_Project_Template__c = listOfPTemp[0].Id;
        listOfPID[0].GP_Project_Type__c = 'FTE';
        update listOfPID[0];
        
        
        Test.startTest();
        GPLWCServicePIDApproverProjectPage.getProjectHeaderDetails(listOfPID[0].Id);
        GPLWCServicePIDApproverProjectPage.getProjectDetail(listOfPID[0].Id);
        GPLWCServicePIDApproverProjectPage.fetchApprovalHistoryData(listOfPID[0].Id);
        GPLWCServicePIDApproverProjectPage.submissionByPIDApprover(listOfPID[0].Id, '', 
                                                                   'approve', '1010', 'Comment', 
                                                                   'Tax code', 'Report', 'Product Code');
        
        // Cover PID, PID Summary, budget & Expenses, Work location & SDO and project document.
        GPLWCServicePIDApproverProjectPage.fetchPIDData(listOfPID[0].Id, 'GP_Project__c');
        GPLWCServicePIDApproverProjectPage.fetchPIDSummaryData(listOfPID[0].Id, 'GP_Project__c');
        GPLWCServicePIDApproverProjectPage.fetchBudgetAndExpensesData(listOfPID[0].Id, 'GP_Project__c');
        GPLWCServicePIDApproverProjectPage.fetchWorkLocationSDOData(listOfPID[0].Id, 'GP_Project__c');
        GPLWCServicePIDApproverProjectPage.fetchDocumentData(listOfPID[0].Id, 'GP_Project__c');
		// Cover resource allocations        
        GPLWCServicePIDApproverProjectPage.fetchResourceAllocationData(listOfPID[0].Id, 'GP_Resource_Allocation__c');
        // Cover billing milestones
        GPLWCServicePIDApproverProjectPage.fetchBillingMilestoneData(listOfPID[0].Id, 'GP_Billing_Milestone__c');
        // Cover project leadership
        GPLWCControllerPIDApproverProjectPage glcpapp3 = new GPLWCControllerPIDApproverProjectPage(listOfPID[0].Id, 'GP_Project_Leadership__c');
        GPLWCServicePIDApproverProjectPage.fetchProjectLeadershipSDOData(listOfPID[0].Id, 'GP_Project_Leadership__c');
        // Cover profile bill rate and Change Summary
        GPLWCControllerPIDApproverProjectPage glcpapp4 = new GPLWCControllerPIDApproverProjectPage(listOfPID[0].Id, 'GP_Profile_Bill_Rate__c');
        GPLWCServicePIDApproverProjectPage.fetchProfileBillRateData(listOfPID[0].Id, 'GP_Profile_Bill_Rate__c');
        GPLWCServicePIDApproverProjectPage.fetchChangeSummaryData(listOfPID[0].Id, 'GP_Profile_Bill_Rate__c');
        
        listOfPID[0].GP_Approval_Status__c = 'Pending For Approval';
        update listOfPID[0];

		GPLWCServicePIDApproverProjectPage.getProjectDetail(listOfPID[0].Id);
        
        /*List<GP_Project__c> listOfPIDC = [Select id, GP_Current_Working_User__c from GP_Project__c where Name = 'Child Mobile App'];
        listOfPIDC[0].GP_Parent_Project__c = listOfPID[0].Id;
        listOfPIDC[0].GP_Clone_Source__c = 'Test Class';
        listOfPIDC[0].GP_Approval_Status__c = 'Draft';
        listOfPIDC[0].GP_Project_Description__c = 'Child Mobile App. Parent PID is tagged now.';
        
        update listOfPIDC[0];*/
        Test.stopTest();
    }
}