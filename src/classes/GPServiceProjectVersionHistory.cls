public class GPServiceProjectVersionHistory {
    Static GPBaseProjectUtil baseProjectUtil; // = new GPBaseProjectUtil();
    static List < SObject > records;
    public static Boolean fetchError = true;
    /**
    * @description Create/Update Project Version Hisory.
    * @param uow : Unit Of Work Instance
    * @param oldSObjectMap : triggerOldMap
    */
    @future
    public static void createApprovedProjectVersionHistory(String serializedMapOfOldObject, String serializedListOfRecords) {
        createApprovedProjectVersionHistoryService(serializedMapOfOldObject, serializedListOfRecords);
        
    }
    
    public static void createApprovedProjectVersionHistoryService(String serializedMapOfOldObject, String serializedListOfRecords) {
        baseProjectUtil = new GPBaseProjectUtil();
        List < SObject > records = (List < SObject > ) JSON.deserialize(serializedListOfRecords, List < SObject > .class);
        map < id, Sobject > oldSObjectMap = serializedMapOfOldObject != null ? (map < id, Sobject > ) JSON.deserialize(serializedMapOfOldObject, map < id, Sobject > .class) : null;
        
        if (records != null && records.size() > 0) {
            List < GP_Project_Version_History__c > lstVersionToInsert = new List < GP_Project_Version_History__c > ();
            List < GP_Project_Version_Line_Item_History__c > listOfVersionLineItemToInsert = new List < GP_Project_Version_Line_Item_History__c > ();
            
            Map < String, list < GP_Project_Version_History__c >> mapOfDraftVersion = new Map < String, list < GP_Project_Version_History__c >> ();
            Map < Id, ProcessInstance > mapOfProcessInstance = new Map < Id, ProcessInstance > ();
            Map < Id, Id > mapOfProjectIdVsVersionHistoryParentId = new Map < Id, Id > ();
            
            Set < Id > setOfProjectIdsForVersionHistory = new Set < Id > ();
            Set < String > setOfParentProjects = new Set < String > ();
            Set < Id > setProcessInstanceId = new Set < Id > ();
            Boolean isValidForVersionCreation = false;
            Set < Id > setOfOldVersionHistoryId = new Set < Id > ();
            
            Id versionHistoryParentId;
            GP_Project__c objOldProj;
            system.debug('records'+records);
            
            for (GP_Project__c project: (List < GP_Project__c > ) records) {
                objOldProj = oldSObjectMap != null ? (GP_Project__c) oldSObjectMap.get(project.Id) : null;
                
                if (project.GP_Approval_Status__c == 'Approved' && (oldSObjectMap == null || (oldSObjectMap != null &&
                                                                                              objOldProj.GP_Approval_Status__c != project.GP_Approval_Status__c))) {
                                                                                                  versionHistoryParentId = project.GP_Parent_Project__c == null ? project.Id : project.GP_Parent_Project__c;
                                                                                                  mapOfProjectIdVsVersionHistoryParentId.put(project.Id, versionHistoryParentId);
                                                                                                  isValidForVersionCreation = true;
                                                                                              } else if (project.GP_Approval_Status__c == 'Draft' && project.GP_Parent_Project__c != null) {
                                                                                                  mapOfProjectIdVsVersionHistoryParentId.put(project.Id, project.GP_Parent_Project__c);
                                                                                                  isValidForVersionCreation = true;
                                                                                              }
                
                if (project.GP_Parent_Project__c != null) {
                    setOfParentProjects.add(project.GP_Parent_Project__c);
                }
            }
            system.debug('mapOfProjectIdVsVersionHistoryParentId'+mapOfProjectIdVsVersionHistoryParentId);
            
            if (mapOfProjectIdVsVersionHistoryParentId.keySet().Size() > 0) {
                if (setOfParentProjects.size() > 0) {
                    list < GP_Project_Version_History__c > lstParentVersion = new list < GP_Project_Version_History__c > (
                        new GPSelectorProjectVersionHistory().selectByVerionDraft(setOfParentProjects));
                    
                    if (lstParentVersion != null && lstParentVersion.size() > 0) {
                        for (GP_Project_Version_History__c projectVersionHistory: lstParentVersion) {
                            if (mapOfDraftVersion.get(projectVersionHistory.GP_Project__c) == null) {
                                mapOfDraftVersion.put(projectVersionHistory.GP_Project__c, new list < GP_Project_Version_History__c > ());
                            }
                            mapOfDraftVersion.get(projectVersionHistory.GP_Project__c).add(projectVersionHistory);
                        }
                    }
                }
                
                if (setProcessInstanceId != null && setProcessInstanceId.size() > 0) {
                    mapOfProcessInstance = new map < Id, ProcessInstance > ([SELECT Id, Status, CreatedDate, CompletedDate, TargetObjectId,
                                                                             (SELECT Id, StepStatus, Actor.Name, ElapsedTimeInDays, ElapsedTimeInMinutes,
                                                                              CreatedDate, ProcessNodeId, ProcessNode.Name, Comments FROM StepsAndWorkitems order by CreatedDate)
                                                                             FROM ProcessInstance
                                                                             WHERE Id IN: setProcessInstanceId
                                                                             ORDER BY CreatedDate DESC
                                                                            ]);
                }
                
                Set < Id > setOfProjectId = mapOfProjectIdVsVersionHistoryParentId.keySet();
                baseProjectUtil.fetchErrorRecord = fetchError;
                baseProjectUtil.queryProjectAndChilRecords(setOfProjectId);
                baseProjectUtil.setMapOfProjectChildRecords();
                //queryProjectAndRelatedData(setOfProjectId);
                //String strQuery = getQuery(setOfProjectId);
                //strQuery += ' from GP_Project__c where id in: setOfProjectId';
                //List<GP_Project__c> lstProject = Database.query(strQuery);
                for (GP_Project__c EachProj: baseProjectUtil.lstProject) {
                    GP_Project_Version_History__c objVersion = new GP_Project_Version_History__c();
                    objVersion.GP_Project__c = mapOfProjectIdVsVersionHistoryParentId.get(EachProj.Id);
                    
                    decimal versionNumber = 1;
                    if (mapOfDraftVersion != null && mapOfDraftVersion.get(objVersion.GP_Project__c) != null) {
                        for (GP_Project_Version_History__c prjVH: mapOfDraftVersion.get(objVersion.GP_Project__c)) {
                            if (prjVH.GP_Status__c == 'Draft') {
                                setOfOldVersionHistoryId.add(prjVH.id);
                                objVersion.Id = prjVH.id;
                                versionNumber = prjVH.GP_Version_No__c != null ? prjVH.GP_Version_No__c : 1;
                                break;
                            } else {
                                versionNumber = prjVH.GP_Version_No__c != null ? prjVH.GP_Version_No__c + 1 : 1;
                                break;
                            }
                        }
                    }
                    
                    String uniqueKey = GPCommon.getUniqueKey(EachProj.GP_EP_Project_Number__c);
                    
                    objVersion.Name = Label.GP_Project_Version_Name + ' ' + (versionNumber);
                    objVersion.Name += ' - ' + (string) system.now().format('MMM dd, yyyy');
                    objVersion.GP_Version_No__c = versionNumber;
                    objVersion.GP_Status__c = EachProj.GP_Approval_Status__c;
                    objVersion.GP_Unique_Version_History__c = uniqueKey;
                    
                    JSONGenerator gen = JSON.createGenerator(true);
                    generateJSONOfPreviousVersion(EachProj, Schema.SObjectType.GP_Project__c.fields.getMap(), gen);
                    objVersion.GP_Project_JSON__c = gen.getAsString();
                    
                    if (baseProjectUtil.mapOfProjectLeadership.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectLeadership.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectLeadership.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String leadershipJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Leadership__c.fields.getMap(), gen, 'Leadership', baseProjectUtil.mapOfProjectLeadership.get(EachProj.Id));
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfProjectLeadershipVersion = baseProjectUtil.getListOfVersionLineItemForJSON(leadershipJSON, 'Project Leadership', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfProjectLeadershipVersion);
                    }
                    
                    if (baseProjectUtil.mapOfResourceAllocation.containsKey(EachProj.Id) && baseProjectUtil.mapOfResourceAllocation.get(EachProj.Id) != null && baseProjectUtil.mapOfResourceAllocation.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        //objVersion.GP_Resource_Allocation_JSON__c = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap(), gen, 'Resource Allocation', baseProjectUtil.mapOfResourceAllocation.get(EachProj.Id));
                        String resourceAllocationJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Resource_Allocation__c.fields.getMap(), gen, 'Resource Allocation', baseProjectUtil.mapOfResourceAllocation.get(EachProj.Id));
                        List < GP_Project_Version_Line_Item_History__c > listOfResourceAllocationVersion = baseProjectUtil.getListOfVersionLineItemForJSON(resourceAllocationJSON, 'Resource Allocation', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfResourceAllocationVersion);
                    }
                    
                    if (baseProjectUtil.mapOfProjectBudget.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectBudget.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectBudget.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String budgetJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Budget__c.fields.getMap(), gen, 'Project Budget', baseProjectUtil.mapOfProjectBudget.get(EachProj.Id));
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfBudgetVersion = baseProjectUtil.getListOfVersionLineItemForJSON(budgetJSON, 'Budget', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfBudgetVersion);
                    }
                    
                    if (baseProjectUtil.mapOfProjectExpense.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectExpense.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectExpense.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String expenseJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Expense__c.fields.getMap(), gen, 'Project Expense', baseProjectUtil.mapOfProjectExpense.get(EachProj.Id));
                        
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfExpenseVersion = baseProjectUtil.getListOfVersionLineItemForJSON(expenseJSON, 'Expense', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfExpenseVersion);
                    }
                    
                    if (baseProjectUtil.mapOfProjectWorkLocation.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectWorkLocation.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectWorkLocation.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String workLocationJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Work_Location_SDO__c.fields.getMap(), gen, 'Work Location', baseProjectUtil.mapOfProjectWorkLocation.get(EachProj.Id));
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfWorkLocationVersion = baseProjectUtil.getListOfVersionLineItemForJSON(workLocationJSON, 'Work Location', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfWorkLocationVersion);
                    }
                    
                    if (baseProjectUtil.mapOfBillingMilestone.containsKey(EachProj.Id) && baseProjectUtil.mapOfBillingMilestone.get(EachProj.Id) != null && baseProjectUtil.mapOfBillingMilestone.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String billingMilestone = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Billing_Milestone__c.fields.getMap(), gen, 'Billing Milestone', baseProjectUtil.mapOfBillingMilestone.get(EachProj.Id));
                        
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfBillingMilestoneVersion = baseProjectUtil.getListOfVersionLineItemForJSON(billingMilestone, 'Billing Milestone', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfBillingMilestoneVersion);
                    }
                    
                    if (baseProjectUtil.mapOfProjectDocument.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectDocument.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectDocument.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        
                        String documentJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Document__c.fields.getMap(), gen, 'Project Document', baseProjectUtil.mapOfProjectDocument.get(EachProj.Id));
                        
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfDocumentVersion = baseProjectUtil.getListOfVersionLineItemForJSON(documentJSON, 'Document', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfDocumentVersion);
                    }
                    
                    if (baseProjectUtil.mapOfProjectAdress.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectAdress.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectAdress.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        objVersion.GP_Address_JSON__c = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Project_Address__c.fields.getMap(), gen, 'Project Address', baseProjectUtil.mapOfProjectAdress.get(EachProj.Id));
                    }
                    
                    if (baseProjectUtil.mapOfProjectProfileBillRate.containsKey(EachProj.Id) && baseProjectUtil.mapOfProjectProfileBillRate.get(EachProj.Id) != null && baseProjectUtil.mapOfProjectProfileBillRate.get(EachProj.Id).size() > 0) {
                        gen = JSON.createGenerator(true);
                        String billRateJSON = generateJSONOfChildRecordsOfPreviousVersion(Schema.SObjectType.GP_Profile_Bill_Rate__c.fields.getMap(), gen, 'Project Bill Rate', baseProjectUtil.mapOfProjectProfileBillRate.get(EachProj.Id));
                        
                        
                        List < GP_Project_Version_Line_Item_History__c > listOfBillRateVersion = baseProjectUtil.getListOfVersionLineItemForJSON(billRateJSON, 'Profile Bill Rate', uniqueKey);
                        
                        listOfVersionLineItemToInsert.addAll(listOfBillRateVersion);
                    }
                    
                    if (mapOfProcessInstance != null && mapOfProcessInstance.size() > 0 && EachProj.ProcessInstances.size() > 0) {
                        objVersion.GP_Approver_Comments__c = generateJSONForProcessInstance(EachProj, mapOfProcessInstance);
                    }
                    
                    lstVersionToInsert.add(objVersion);
                }
            }
            //delete all previously created version line items
            List < GP_Project_Version_Line_Item_History__c > listOfOldVersionLineItem = [Select Id
                                                                                         from GP_Project_Version_Line_Item_History__c
                                                                                         where GP_Project_Version_History__c in: setOfOldVersionHistoryId
                                                                                        ];
            
            system.debug('listOfOldVersionLineItem'+listOfOldVersionLineItem);
            system.debug('lstVersionToInsert'+lstVersionToInsert);
            system.debug('listOfVersionLineItemToInsert'+listOfVersionLineItemToInsert);
            
            if (listOfOldVersionLineItem != null && !listOfOldVersionLineItem.isEmpty()) {
                delete listOfOldVersionLineItem;
            }
            
            if (lstVersionToInsert != null && lstVersionToInsert.size() > 0) {
                upsert lstVersionToInsert;
            }
            
            if (listOfVersionLineItemToInsert != null && !listOfVersionLineItemToInsert.isEmpty()) {
                insert listOfVersionLineItemToInsert;
            }
        }
    }
    /**
    * @description generate JSON of Child Records Version
    * @param objectName : object Name whoose JSON needs to be generated.
    * @param MapOfSchema : Map of Schema of the object whose JSON is to be generated.
    * @param gen : JSON generator Instance.
    * @param lstOfChildRecord : list Of record whoose JSON is to be created.
    */
    private static String generateJSONOfChildRecordsOfPreviousVersion(Map < String, Schema.SObjectField > MapOfSchema, JSONGenerator gen, String objectName, List < Sobject > lstOfChildRecord) {
        gen.writeStartObject();
        gen.writeFieldName(objectName);
        gen.writeStartArray();
        for (Sobject sobj: lstOfChildRecord) {
            generateJSONOfPreviousVersion(sobj, MapOfSchema, gen);
        }
        gen.writeEndArray();
        gen.writeEndObject();
        return gen.getAsString();
    }
    
    /**
    * @description generate JSON of Previous Version
    * @param objectToGenerateJSON : object whoose JSON needs to be generated.
    * @param MapOfSchema : Map of Schema of the object whose JSON is to be generated.
    * @param gen : JSON generator Instance.
    */
    private static void generateJSONOfPreviousVersion(SObject objectToGenerateJSON, Map < String, Schema.SObjectField > MapOfSchema, JSONGenerator gen) {
        gen.writeStartObject();
        for (String EachField: MapOfSchema.keySet()) {
            if (objectToGenerateJSON.get(EachField) != null) {
                createSobjectJSON(MapOfSchema, EachField, gen, objectToGenerateJSON);
            } else {
                gen.writeNullField(EachField);
            }
        }
        gen.writeEndObject();
    }
    
    /**
    * @description generate JSON for Process Instance records
    * @param mapOfProcessInstance : Map of Process Instances .
    * @param project : Project whise Process Instance JSON is to be generated.
    */
    private Static String generateJSONForProcessInstance(GP_Project__c project, Map < Id, ProcessInstance > mapOfProcessInstance) {
        JSONGenerator genApproval = JSON.createGenerator(true);
        
        genApproval.writeStartObject();
        genApproval.writeFieldName('Approvals');
        genApproval.writeStartArray();
        
        for (ProcessInstance projectProcessInstance: project.ProcessInstances) {
            ProcessInstance objProcessInstance = mapOfProcessInstance.get(projectProcessInstance.Id);
            genApproval.writeStartObject();
            genApproval.writeStringField('Status', (string) objProcessInstance.Status);
            genApproval.writeDateTimeField('CreatedDate', (DateTime) objProcessInstance.CreatedDate);
            if (objProcessInstance.CompletedDate != null)
                genApproval.writeDateTimeField('CompletedDate', (DateTime) objProcessInstance.CompletedDate);
            genApproval.writeFieldName('StepsAndWorkitems');
            genApproval.writeStartArray();
            if (objProcessInstance.StepsAndWorkitems != null && objProcessInstance.StepsAndWorkitems.size() > 0) {
                for (ProcessInstanceHistory workItem: objProcessInstance.StepsAndWorkitems) {
                    genApproval.writeStartObject();
                    genApproval.writeStringField('StepStatus', (string) workItem.StepStatus);
                    genApproval.writeStringField('Actor.Name', (string) workItem.Actor.Name);
                    genApproval.writeDateTimeField('CreatedDate', (DateTime) workItem.CreatedDate);
                    
                    if (workItem.Comments != null) {
                        genApproval.writeStringField('Comments', (string) workItem.Comments);
                    } else {
                        genApproval.writeNullField('Comments');
                    }
                    genApproval.writeEndObject();
                }
            }
            genApproval.writeEndArray();
            genApproval.writeEndObject();
        }
        
        genApproval.writeEndArray();
        genApproval.writeEndObject();
        return genApproval.getAsString();
        
    }
    
    private static void createSobjectJSON(Map < String, Schema.SObjectField > MapOfSchema, string field, JSONGenerator genJson, Sobject obj) {
        Set < String > setOfStandardFields = new Set < String > { 'connectionreceivedid', 'connectionsentid' };
            
            if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.BOOLEAN)
            genJson.writeBooleanField(field, (BOOLEAN) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DATETIME)
            genJson.writeDateTimeField(field, (DATETIME) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DATE)
            genJson.writeDateField(field, (DATE) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.PERCENT)
            genJson.writeNumberField(field, (decimal) obj.get(field));
        else if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.DOUBLE ||
                 MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.CURRENCY ||
                 MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.Integer)
            genJson.writeNumberField(field, (DOUBLE) obj.get(field));
        else
            genJson.writeStringField(field, (string) obj.get(field));
        
        if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.REFERENCE) {
            if (MapOfSchema.get(field).getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                if (field.contains('__c')) {
                    field = field.replace('__c', '__r');
                } else if (field.contains('id') && !setOfStandardFields.contains(field)) {
                    field = field.replace('id', '');
                }
                genJson.writeStringField(field + '.name', (string) obj.getSobject(field).get('name'));
            }
        }
    }
 
    @future(callout = true)   
    public static void attachComparisonProjectFiles(Set < Id > setOfProjectIdToCreateFiles) {
        List < ContentVersion > listofContentVersion = new List < ContentVersion > ();
        List < ContentDocumentLink > listofContentDocumentLink = new List < ContentDocumentLink > ();
        Map < Id, ContentVersion > mapOfLinkedEntityIdVsContentversion = new Map< Id, ContentVersion >();
        Set < Id > setOfContentversionIds = new Set< Id >();
        PageReference pageExcel;
        ContentVersion objContentVersion;
        List < GP_Project_Version_History__c > lstOfVersionHistory = [Select id, name, GP_Project__r.GP_Oracle_PID__c, GP_Oracle_Status__c, GP_Status__c, GP_Version_No__c, GP_Project__c 
                                                                      from GP_Project_Version_History__c where GP_Oracle_Status__c = 'S' and
                                                                      GP_Project__c in : setOfProjectIdToCreateFiles order by GP_Version_No__c desc limit 1];
        
        for(GP_Project_Version_History__c versionHistoryRecord : lstOfVersionHistory) {
            pageExcel = new PageReference('/apex/GPGenerateProjectFileEXL');
            pageExcel.getParameters().put('id', versionHistoryRecord.GP_Project__c);
            pageExcel.getParameters().put('selectedStatus', 'modify');
            objContentVersion = new ContentVersion();
            if(Test.isRunningTest()){
             	objContentVersion.versionData = Blob.valueof('test');  
            }
            else
            { 
            	objContentVersion.versionData = pageExcel.getContent();
            }
            objContentVersion.PathOnClient = versionHistoryRecord.GP_Project__r.GP_Oracle_PID__c + '-' + system.now() + '.xls';
            mapOfLinkedEntityIdVsContentversion.put(versionHistoryRecord.GP_Project__c,objContentVersion);            
            listofContentVersion.add(objContentVersion);    
        }
        
        if (listofContentVersion.size() > 0) {
            insert mapOfLinkedEntityIdVsContentversion.values();
            
            for(ContentVersion contentVersion : listofContentVersion) {
                setOfContentversionIds.add(contentVersion.Id);
            }
            
            Map < Id, Id > mapOfContentVersionIDVsContentDocumentID = new Map < Id, Id > ();
            
            for (ContentVersion contentversionobj: [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: setOfContentversionIds]) {
                mapOfContentVersionIDvsContentDocumentID.put(contentversionobj.Id, contentversionobj.ContentDocumentId);
            }
            
            for(GP_Project_Version_History__c versionHistoryRecord : lstOfVersionHistory) {
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.LinkedEntityId = versionHistoryRecord.id;
                versionHistoryRecord.GP_Content_Version_Document_Id__c = mapOfLinkedEntityIdVsContentversion.get(versionHistoryRecord.GP_Project__c).Id;
                versionHistoryRecord.GP_Document_Creation_Date__c = system.today();
                cdl.ContentDocumentId = mapOfContentVersionIDvsContentDocumentID.get(mapOfLinkedEntityIdVsContentversion.get(versionHistoryRecord.GP_Project__c).Id);
                cdl.ShareType = 'V';
                listofContentVersion.add(objContentVersion);
                listofContentDocumentLink.add(cdl);     
            }
            
            insert listofContentDocumentLink;
            update lstOfVersionHistory;
        }        
    }
}