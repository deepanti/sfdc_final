/*
 * This is the test class for GEN_Util_Test_Data
 * @ Kumar Pulkesin 
*/

@isTest
public class GEN_Util_Test_Data_Test {
    
    public static testMethod void RunTest1(){
        test.startTest();
        Profile gpactSalesRep = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User gpactSaleUser =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',gpactSalesRep.Id,'standardusertestgen2015@testorg.com' );
        
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];        
        // Covered Createuser method
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');    
        
        // Covered CreateSalesUnit method
        GEN_Util_Test_Data.CreateSalesUnit('Test Unit',genuser.Id,'Test Group');        
        // Covered Price book
        GEN_Util_Test_Data.CreatePricebook2();
        // Covered Quota Headers
        GEN_Util_Test_Data.CreateQuotaheader(genuser.Id);
        
        // Covered CreatePriceBookEntry
        //GEN_Util_Test_Data.CreatePriceBookEntry();
        // Covered CreateProduct
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Test Product');        
        // Covered CreateArchetype
        Archetype__c arChtyp = GEN_Util_Test_Data.CreateArchetype('Test');
        // Covered Bussiness Segment
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('GE','GE');
        // Covered Sub_Business
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','Test',oBS.Id);
        // Covered Account
        Account oAccount = GEN_Util_Test_Data.CreateAccount(genuser.Id,gpactSaleUser.Id,arChtyp.Id,'GE','GE',oBS.Id,oSB.Id,'Test','Test','Test','Test','Test');
        // Covered CreateInactiveAccount
        //GEN_Util_Test_Data.CreateInactiveAccount();
        // Covered CreateAccountTeamMember
        GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.Id,genuser.id);
        // Covered CreateContact
        Contact ocontact = GEN_Util_Test_Data.CreateContact('Test','Test',oAccount.Id,'Test','Test','fed@gtr.com','78342342343');
        // Covered CreateOpportunity
        Opportunity oOppertunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,ocontact.Id);
        // Covered CreateErrorOpportunity
        //GEN_Util_Test_Data.CreateErrorOpportunity('Test',oAccount.Id,ocontact.Id);
        // Covered CreateOpportunityTeamMember
        // GEN_Util_Test_Data.CreateOpportunityTeamMember();
        // Covered CreatePre_discover
        GEN_Util_Test_Data.CreatePre_discover('Test');
        // Covered NatureOfWork
        GEN_Util_Test_Data.NatureOfWork();
        // Covered ServiceLineLookup
        GEN_Util_Test_Data.ServiceLineLookup();
        // Covered ProductFamilyLookup
        GEN_Util_Test_Data.ProductFamilyLookup();
        // Covered CreateOpportunityProduct
        OpportunityProduct__c oOpportunityProduct = GEN_Util_Test_Data.CreateOpportunityProduct(oOppertunity.Id,oProduct.Id,5,5);
        // Covered CreateErrorOpportunityProduct
        //GEN_Util_Test_Data.CreateErrorOpportunityProduct(oOppertunity.Id,oProduct.Id,5,5);
        // Covered CreateTask
        GEN_Util_Test_Data.CreateTask('Test','Test','Test','Test','Test');
        // Covered CreateEvent
        // GEN_Util_Test_Data.CreateEvent();
        // Covered CreateRevenueSchedule
        GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.Id,1,3,2,2.0);
        // Covered CreateProductSchedule
        // GEN_Util_Test_Data.CreateProductSchedule();
        // Covered CreateProductScheduleDummy
        // GEN_Util_Test_Data.CreateProductScheduleDummy();
        // Covered CreateRevenuePricingHeader
        // GEN_Util_Test_Data.CreateRevenuePricingHeader();
        // Covered Revenue Pricing
        // GEN_Util_Test_Data.CreateRevenuePricing();
        // Covered CreateRevenuePricingYear
        // GEN_Util_Test_Data.CreateRevenuePricingYear();
        // Covered CreateQSRM
        GEN_Util_Test_Data.CreateQSRM(oOppertunity.ID, genuser.ID);
        // Covered CreateNewAccountCreationRequest
        // GEN_Util_Test_Data.CreateNewAccountCreationRequest();
        // Covered createNewAccountArchetype
        // GEN_Util_Test_Data.createNewAccountArchetype();
        // Covered createShareOpportunityProduct
        // GEN_Util_Test_Data.createShareOpportunityProduct();
        test.stopTest();
    }
    
    public static testMethod void RunTest2(){
        test.startTest();
        Profile gpactSalesRep = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User gpactSaleUser =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',gpactSalesRep.Id,'standardusertestgen2015@testorg.com' );
        
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];        
        // Covered Createuser method
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');    
        
        // Covered CreateQuotaGRM
        //GEN_Util_Test_Data.CreateQuotaGRM();
        // Covered CreatePriceBookEntry
        //GEN_Util_Test_Data.CreatePriceBookEntry();
        // Covered CreateProduct
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Test Product');        
        // Covered CreateArchetype
        Archetype__c arChtyp = GEN_Util_Test_Data.CreateArchetype('Test');
        // Covered Bussiness Segment
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('GE','GE');
        // Covered Sub_Business
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','Test',oBS.Id);
        // Covered Account
        Account oAccount = GEN_Util_Test_Data.CreateAccount(genuser.Id,gpactSaleUser.Id,arChtyp.Id,'GE','GE',oBS.Id,oSB.Id,'Test','Test','Test','Test','Test');
        // Covered CreateInactiveAccount
        //GEN_Util_Test_Data.CreateInactiveAccount();
        // Covered CreateAccountTeamMember
        GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.Id,genuser.id);
        // Covered CreateContact
        Contact ocontact = GEN_Util_Test_Data.CreateContact('Test','Test',oAccount.Id,'Test','Test','fed@gtr.com','78342342343');
        // Covered CreateOpportunity
        Opportunity oOppertunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,ocontact.Id);
        // Covered CreateErrorOpportunity
        GEN_Util_Test_Data.CreateErrorOpportunity('Test',oAccount.Id,ocontact.Id);
        // Covered CreateOpportunityTeamMember
        GEN_Util_Test_Data.CreateOpportunityTeamMember(oOppertunity.Id,genuser.id);
        // Covered CreateOpportunityProduct
        OpportunityProduct__c oOpportunityProduct = GEN_Util_Test_Data.CreateOpportunityProduct(oOppertunity.Id,oProduct.Id,5,5);
        // Covered CreateErrorOpportunityProduct
        //GEN_Util_Test_Data.CreateErrorOpportunityProduct(oOppertunity.Id,oProduct.Id,5,5);
        // Covered CreateEvent
        // GEN_Util_Test_Data.CreateEvent();
        // Covered CreateRevenueSchedule
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.Id,1,3,2,2.0);
        // Covered CreateProductSchedule
        GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.Id,oRevenueSchedule.Id);
        // Covered CreateProductScheduleDummy
        GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.Id,3,5);
        // Covered CreateRevenuePricingHeader
        RevenuePricingHeader__c oRevenuePricingHeader = GEN_Util_Test_Data.CreateRevenuePricingHeader(oAccount.Id,genuser.id,ocontact.Id,oOppertunity.Id);
        // Covered Revenue Pricing
        Revenue_Pricing__c oRevenuePricing = GEN_Util_Test_Data.CreateRevenuePricing(oRevenuePricingHeader.Id);
        // Covered CreateRevenuePricingYear
        GEN_Util_Test_Data.CreateRevenuePricingYear(oRevenuePricing.Id);
        
        // Covered CreateQSRM
        //GEN_Util_Test_Data.CreateQSRM(oOppertunity.Id,genuser.id);
        // Covered CreateNewAccountCreationRequest
        GEN_Util_Test_Data.CreateNewAccountCreationRequest(genuser.Id,'Test','Test');
        // Covered createNewAccountArchetype
        //GEN_Util_Test_Data.createNewAccountArchetype(gpactSaleUser.Id,oAccount.Id,arChtyp.Id);
        // Covered createShareOpportunityProduct
        //GEN_Util_Test_Data.createShareOpportunityProduct(oOppertunity.Id,genuser.id);
        test.stopTest();
    }
}