@isTest//(SeeAllData=true)
public class dateUpdate_Test {
    public static Opportunity opp;
    public static User u, u1, u3;
    public static OpportunityLineItem opp_item;
    public static testMethod void RunTest1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );

        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        System.runAs(u){
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+2, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+2,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            
            Product2 product_obj = new Product2(name='test pro', canUseRevenueSchedule=true, isActive=true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
            PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                               priceBookEntryId = entry.ID, product_bd_rep__c = u.ID,
                                               unitPrice = 100.00, Revenue_Start_Date__c =System.today() - 8 ); 
            CheckRecursiveForOLI.runAfter1=True;
            insert opp_item;
            opp.CloseDate=System.today().adddays(10);
            CheckRecursiveForOLI.runAfter1=True;
            CheckRecursiveForOLI.dateUpdateFlag=True;
            update opp;
            //CheckRecursiveForOLI.runOnceAfter_1();
            CheckRecursiveForOLI.runAfter1=True;
            CheckRecursiveForOLI.dateUpdateFlag=True;
            
            
            
        }
    }
        
    }