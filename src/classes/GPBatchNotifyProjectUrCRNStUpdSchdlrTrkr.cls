//Description : tracker class for GPBatchNotifyProjectUsrCRNStgUpdtSchdulr
//Created By : mandeep.singh@saasfous.com
@isTest
public class GPBatchNotifyProjectUrCRNStUpdSchdlrTrkr 
{
    public static testmethod  void testschedule() {
        Test.StartTest();
        GPBatchNotifyProjectUsrCRNStgUpdtSchdulr obj = new GPBatchNotifyProjectUsrCRNStgUpdtSchdulr();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, obj);
        Test.stopTest();
    }
}