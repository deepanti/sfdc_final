public class GPCmpServiceDeleteAndCloneProject {
    
	@AuraEnabled
    public static GPAuraResponse cloneAndDeleteProjectService(string strProjectId) {
        system.debug('strProjectId'+strProjectId);
        GPControllerDeleteAndCloneProject deleteAndCloneProjectController = new GPControllerDeleteAndCloneProject(strProjectId);
        return deleteAndCloneProjectController.cloneAndDeleteProject();
    }
}