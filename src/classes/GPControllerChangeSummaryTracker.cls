// ------------------------------------------------------------------------------------------------ 
// Description: Tracker Class for GPControllerChangeSummary
// ------------------------------------------------------------------------------------------------
//Created By : Ved Prakash
// ------------------------------------------------------------------------------------------------ 
@isTest public class GPControllerChangeSummaryTracker {
    static GP_Project__c prjObj;
    static GP_Project_Version_Line_Item_History__c projectVersionLineItemHistory;
	static void setupCommonData() {

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Rejected';
        insert prjObj;

        GP_Project_Version_History__c projectVersionHistory = GPCommonTracker.getProjectVersionHistory(prjObj.Id);
        insert projectVersionHistory;
        
        projectVersionLineItemHistory = GPCommonTracker.getProjectVersionLineItemHistory(projectVersionHistory.Id);
        projectVersionLineItemHistory.GP_Related_Record_Type__c = 'Work Location';
        insert projectVersionLineItemHistory;
    }

    static testmethod void testbatch() {
        Test.StartTest();
        setupCommonData();
        GPControllerChangeSummary controller = new GPControllerChangeSummary(prjObj.Id);
        GPAuraResponse objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Project Leadership');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Billing Milestone');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Resource Allocation');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Budget');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Expense');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Document');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Profile Bill Rate');
        objGPAuraResponse = controller.getProjectVersions();
        
        UpdateProjectVersionLineItemHistory('Approver Comment');
        objGPAuraResponse = controller.getProjectVersions();
        
        Test.StopTest();
    }
    
    static void UpdateProjectVersionLineItemHistory(string relatedRecordType)
    {
        projectVersionLineItemHistory.GP_Related_Record_Type__c = relatedRecordType;
        update projectVersionLineItemHistory;
    }
}