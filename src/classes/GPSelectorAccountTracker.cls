@isTest
public class GPSelectorAccountTracker {
    @testSetup
    public static void buildDependencyData() {
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
    }
    
    @isTest
    public static void testSelectById() {
        GPSelectorAccount accountSelector = new GPSelectorAccount();
        Account expectedAccount = [SELECT Id FROM Account LIMIT 1];
        Account returnedAccount = accountSelector.selectById(expectedAccount.Id);
        System.assertEquals(expectedAccount.Id, returnedAccount.Id);
        
        accountSelector.selectById(new Set<Id> {expectedAccount.Id});
        accountSelector.selectaccount(new Set<Id> {expectedAccount.Id});
        
        accountSelector.getAccountWithBusiness(expectedAccount.Id);
        accountSelector.getVerticalSubVertical(expectedAccount.Id);
		// Mass Update Change
        accountSelector.getAccountDetailsForMassUpdate(new Set<String>{expectedAccount.Id});
    }
}