/**
 * @group TimeSheet. 
 *
 * @description Helper Timesheet class to create timesheet entries using timesheet object,timesheet transaction and timesheet entry records.
 */
public class GPHelperTimesheet {

    public static Set < String > setOfProjectNumbers;
    public static Set < String > setOfTaskNames;
    public static Set < String > setOfPersonIds;

    //Employee Person Id uniqueness
    /**
     * @description Get TimeSheet Data for an Employee of a month-year.
     * 
     * @param setOfEmployeeMonthYear : Set of "Employee-Month-Year".
     *
     * @return List < GP_Timesheet__c > : List of Timesheet records in pinnacle.
     */
    public static List < GP_Timesheet__c > getTimeSheetData(Set < String > setOfEmployeeMonthYear) {
        return new GPSelectorTimeSheet().selectByEmployeeMonthYearUniqueField(setOfEmployeeMonthYear);
    }

    /**
     * @description Get TimeSheet Entry Data for an Employee of a month-year.
     * 
     * @param setOfEmployeeMonthYear : Set of "Employee-Month-Year".
     *
     * @return List < GP_Timesheet_Entry__c > : List of Timesheet Entry records in pinnacle.
     */
    public static List < GP_Timesheet_Entry__c > getTimeSheetEntryData(Set < String > setOfEmployeeMonthYear) {
        return new GPSelectorTimesheetEntry().getTimeSheetEntriesEmployeeMonthYear(setOfEmployeeMonthYear);
    }

    /**
     * @description Get TimeSheet Transaction Data for an Employee of a month-year.
     * 
     * @param setOfEmployeeMonthYear : Set of "Employee-Month-Year".
     *
     * @return List < GP_Timesheet_Transaction__c > : List of Timesheet Transaction records in pinnacle.
     */
    public static List < GP_Timesheet_Transaction__c > getTimeSheetTransactionData(Set < String > setOfEmployeeMonthYear) {
        return new GPSelectorTimesheetTransaction().getTimeSheetTransactionEmployeeMonthYear(setOfEmployeeMonthYear);
    }

    /**
     * @description Get Map Of Project Tasks using task number.
     * 
     * @param setOfProjectTaskNumber : Set of Project Task Numbers.
     *
     * @return Map < String, GP_Project_Task__c > : Map Of Task Number Vs it's record.
     */
    public static void setMapOfProjectTasks(Set < String > setOfProjectTaskNumber, Map < String, GP_Project_Task__c > mapOfProjectTasks) {
        //Map < String, GP_Project_Task__c > mapOfProjectTasks = new Map < String, GP_Project_Task__c > ();
        system.debug('setOfProjectTaskNumber'+setOfProjectTaskNumber);
        for (GP_Project_Task__c objProjectTask: new GPSelectorProjectTask().getListOfProjectTask(setOfProjectTaskNumber)) {
            mapOfProjectTasks.put(objProjectTask.GP_Task_Number__c + '@@' + objProjectTask.GP_Project_Number__c, objProjectTask);
        }
    }

    /**
     * @description Get Map Of Project using Oracle PID.
     * 
     * @param setOfProjectNumber : Set of Project Oracle PID's.
     * @param mapOfProject : Map Of Project used as reference variable.
     */
    public static void getMapOfProject(Set < String > setOfProjectNumber, map < String, GP_Project__c > mapOfProject) {
        //map<String,GP_Project__c> mapOfProject = new map<String,GP_Project__c>();
        for (GP_Project__c objProject: new GPSelectorProject().getProjectListForOracleId(setOfProjectNumber)) {
            system.debug('objProject-------'+objProject);
            mapOfProject.put(objProject.GP_Oracle_PID__c, objProject);
        }
        //return mapOfProject;
    }

    /**
     * @description Get Map Of Employee using Person ID.
     * 
     * @param setOfEmployeeNumber : Set of Employee Person Id's.
     * @param mapOfEmployee : Map Of Employee used as reference variable.
     */
    public static void getMapOfEmployee(Set < String > setOfEmployeeNumber, map < String, GP_Employee_Master__c > mapOfEmployee) {
        //map<String,GP_Employee_Master__c> mapOfEmployee = new map<String,GP_Employee_Master__c>();
        for (GP_Employee_Master__c objEmployee: new GPSelectorEmployeeMaster().getEmployeeListForPersonId(setOfEmployeeNumber)) {
            mapOfEmployee.put(objEmployee.GP_Person_ID__c, objEmployee);
        }
        //return mapOfEmployee;
    }

    /**
     * @description Get Map Of Unique Timesheets (add the hours if duplicate data exists).
     * 
     * @param lstOfTimeSheetData : List Of timesheet data from which unique data is to be created.
     *
     * @return Map < String, GP_Timesheet__c > : Return map of unique timesheet data.
     */
    public static Map < String, GP_Timesheet__c > getUniqueTimeSheetData(List < GP_Timesheet__c > lstOfTimeSheetData) {
        Map < String, GP_Timesheet__c > mapOfUniqueTimeSheets = new Map < String, GP_Timesheet__c > ();
        for (GP_Timesheet__c objTimeSheet: lstOfTimeSheetData) {
            if (objTimeSheet.GP_Expenditure_Item_Date__c != null) {
                String key = objTimeSheet.GP_Project_Number__c + objTimeSheet.GP_Person_Id__c + objTimeSheet.GP_Task_Number__c +
                    objTimeSheet.GP_Expenditure_Type__c + objTimeSheet.GP_Expenditure_Item_Date__c;
                if (mapOfUniqueTimeSheets.containsKey(key)) {
                    mapOfUniqueTimeSheets.get(key).GP_Hours__c += objTimeSheet.GP_Hours__c;
                } else
                    mapOfUniqueTimeSheets.put(key, objTimeSheet);
            }
        }
        return mapOfUniqueTimeSheets;
    }

    /**
     * @description Get Map Of Unique temporary data (add the hours if duplicate data exists).
     * 
     * @param lstOfTemporaryTimeSheetData : List Of temporary data from which unique data is to be created.
     *
     * @return Map < String, GP_Temporary_Data__c > : Return map of unique temporary data.
     */
    public static map < String, GP_Temporary_Data__c > getUniqueTemporaryTimeSheetData(List < GP_Temporary_Data__c > lstOfTemporaryTimeSheetData) {
        map < String, GP_Temporary_Data__c > mapOfUniqueTemporaryTimeSheets = new map < String, GP_Temporary_Data__c > ();
        for (GP_Temporary_Data__c objTemporaryTimeSheet: lstOfTemporaryTimeSheetData) {

            setOfTaskNames.add(objTemporaryTimeSheet.GP_TSC_Project_Task__c);
            setOfProjectNumbers.add(objTemporaryTimeSheet.GP_Project__c);
            setOfPersonIds.add(objTemporaryTimeSheet.GP_Employee__c);

            String key = objTemporaryTimeSheet.GP_Project__c + objTemporaryTimeSheet.GP_Employee__c + objTemporaryTimeSheet.GP_TSC_Project_Task__c +
                objTemporaryTimeSheet.GP_TSC_Ex_Type__c + objTemporaryTimeSheet.GP_TSC_Date__c;
            if (mapOfUniqueTemporaryTimeSheets.containsKey(key)) {
                mapOfUniqueTemporaryTimeSheets.get(key).GP_TSC_Modified_Hours__c += objTemporaryTimeSheet.GP_TSC_Modified_Hours__c;
            } else
                mapOfUniqueTemporaryTimeSheets.put(key, objTemporaryTimeSheet);
        }
        return mapOfUniqueTemporaryTimeSheets;
    }

    /**
     * @description Set class members.
     * 
     * @param lstOfTimeSheetData : List Of timesheet data.
     */
    public static void setStaticSetValues(List < GP_Timesheet__c > lstOfTimeSheetData, List < GP_Timesheet_Entry__c > listOfTimeSheetEntryData) {
        for (GP_Timesheet__c objTimeSheet: lstOfTimeSheetData) {
            setOfTaskNames.add(objTimeSheet.GP_Task_Number__c);
            setOfProjectNumbers.add(objTimeSheet.GP_Project_Number__c);
            setOfPersonIds.add(objTimeSheet.GP_Person_Id__c);
        }
        for (GP_Timesheet_Entry__c objTimeSheetEntry: listOfTimeSheetEntryData) {
            setOfTaskNames.add(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c);
            setOfProjectNumbers.add(objTimeSheetEntry.GP_Project_Oracle_PID__c);
        }

    }

    /**
     * @description Get Map Of Unique timesheet entry data (add the hours if duplicate data exists).
     * 
     * @param lstOfTimeSheetEntries : List Of timesheet entry data from which unique data is to be created.
     *
     * @return Map < String, GP_Timesheet_Entry__c > : Return map of unique timesheet entry data.
     */
    public static map < String, GP_Timesheet_Entry__c > getUniqueTimeSheetEntriesData(List < GP_Timesheet_Entry__c > lstOfTimeSheetEntries) {
        map < String, GP_Timesheet_Entry__c > mapOfUniqueTimeSheetEntries = new map < String, GP_Timesheet_Entry__c > ();
        for (GP_Timesheet_Entry__c objTimeSheetEntry: lstOfTimeSheetEntries) {
            String key = objTimeSheetEntry.GP_Project_Oracle_PID__c + objTimeSheetEntry.GP_Employee__r.GP_Person_ID__c +
                objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + objTimeSheetEntry.GP_Ex_Type__c +
                objTimeSheetEntry.GP_Date__c;
            mapOfUniqueTimeSheetEntries.put(key, objTimeSheetEntry);
        }
        return mapOfUniqueTimeSheetEntries;
    }

    /**
     * @description Get Merged timesheet Entries for timesheet correction UI.
     * 
     * @param mapOfUniqueTimeSheetEntries : Map Of unique Timesheet entries.
     * @param mapOfUniqueTimeSheets : Map Of unique Timesheets.
     * @param mapOfEmployee : Map Of employee.
     * @param mapOfProject : Map Of project.
     * @param mapOfProjectTasks : Map Of project tasks.
     *
     * @return List < GP_Timesheet_Entry__c > : List of merged timsheet entries using timesheet and timesheet entry object.
     */
    public static List < GP_Timesheet_Entry__c > getMergedTimeSheetEntriesData(map < String, GP_Timesheet_Entry__c > mapOfUniqueTimeSheetEntries,
        map < String, GP_Timesheet__c > mapOfUniqueTimeSheets,
        map < String, GP_Employee_Master__c > mapOfEmployee,
        map < String, GP_Project__c > mapOfProject,
        map < String, GP_Project_Task__c > mapOfProjectTasks) {
        List < GP_Timesheet_Entry__c > lstTimeSheetEntries = new List < GP_Timesheet_Entry__c > ();
        Set < String > setOfKeysPushedToList = new Set < String > ();

        for (String key: mapOfUniqueTimeSheets.KeySet()) {
            GP_Timesheet_Entry__c objTimeSheetEntry = new GP_Timesheet_Entry__c();
            if (mapOfUniqueTimeSheetEntries.containsKey(key)) {
                objTimeSheetEntry = mapOfUniqueTimeSheetEntries.get(key);
                if (mapOfProject.containsKey(objTimeSheetEntry.GP_Project_Oracle_PID__c) &&
                    mapOfProjectTasks.containsKey(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c) &&
                    isEligibleForCreation(Date.ValueOf(objTimeSheetEntry.GP_Date__c),
                        mapOfProjectTasks.get(objTimeSheetEntry.GP_Project_Task_Oracle_Id__c + '@@' + objTimeSheetEntry.GP_Project_Oracle_PID__c))) {
                    objTimeSheetEntry = mapOfUniqueTimeSheetEntries.get(key);
                    objTimeSheetEntry.GP_Actual_Hours__c = mapOfUniqueTimeSheets.get(key).GP_Hours__c;
                }
            } else {
                GP_Timesheet__c objTimeSheet = mapOfUniqueTimeSheets.get(key);
                if (mapOfEmployee.containsKey(objTimeSheet.GP_Person_Id__c) &&
                    mapOfProject.containsKey(objTimeSheet.GP_Project_Number__c) &&
                    mapOfProjectTasks.containsKey(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c) &&
                    isEligibleForCreation(Date.ValueOf(objTimeSheet.GP_Expenditure_Item_Date__c),
                        mapOfProjectTasks.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c))) {
                    objTimeSheetEntry.GP_Actual_Hours__c = objTimeSheet.GP_Hours__c;
                    objTimeSheetEntry.GP_Date__c = Date.ValueOf(objTimeSheet.GP_Expenditure_Item_Date__c);
                    objTimeSheetEntry.GP_Employee__r = mapOfEmployee.get(objTimeSheet.GP_Person_Id__c);
                    objTimeSheetEntry.GP_Employee__r.Name = mapOfEmployee.get(objTimeSheet.GP_Person_Id__c).Name;
                    objTimeSheetEntry.GP_Ex_Type__c = objTimeSheet.GP_Expenditure_Type__c;
                    //objTimeSheetEntry.GP_Modified_Hours__c = objTimeSheet.GP_Hours__c;
                    //objTimeSheetEntry.GP_Project__r = mapOfProject.get(objTimeSheet.GP_Project_Number__c);
                    //objTimeSheetEntry.GP_Project__r.Name = mapOfProject.get(objTimeSheet.GP_Project_Number__c).Name;
                    objTimeSheetEntry.GP_Project_Oracle_PID__c = mapOfProject.get(objTimeSheet.GP_Project_Number__c).GP_Oracle_PID__c;
                    //objTimeSheetEntry.GP_Project__r.RecordType.Name = mapOfProject.get(objTimeSheet.GP_Project_Number__c).RecordType.Name;
                    //objTimeSheetEntry.GP_Project_Task__r = mapOfProjectTasks.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c);
                    //objTimeSheetEntry.GP_Project_Task__r.Name = mapOfProjectTasks.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c).Name;
                    objTimeSheetEntry.GP_Project_Task_Oracle_Id__c = mapOfProjectTasks.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c).GP_Task_Number__c;
                }
                //mapOfUniqueTimeSheetEntries.put(key,objTimeSheetEntry);
            }
            setOfKeysPushedToList.add(key);

            if (objTimeSheetEntry.GP_Project_Oracle_PID__c != null)
                lstTimeSheetEntries.add(objTimeSheetEntry);
        }
        for (String key: mapOfUniqueTimeSheetEntries.KeySet()) {
            if (!setOfKeysPushedToList.contains(key))
                lstTimeSheetEntries.add(mapOfUniqueTimeSheetEntries.get(key));
        }
        system.debug('getMergedTimeSheetEntriesData' + lstTimeSheetEntries);
        return lstTimeSheetEntries;
    }
    private static Boolean isEligibleForCreation(Date expenditureItemDate, GP_Project_Task__c projectTask) {
        return ((expenditureItemDate >= projectTask.GP_Start_Date__c &&
                expenditureItemDate <= projectTask.GP_End_Date__c) ||
            (expenditureItemDate >= projectTask.GP_Start_Date__c &&
                projectTask.GP_End_Date__c == null));
    }

    /**
     * @description Get Merged timesheet Entries for timesheet Mass Upload.
     * 
     * @param mapOfUniqueTemporaryTimeSheets : Map Of unique temporary entries.
     * @param mapOfUniqueTimeSheets : Map Of unique Timesheets.
     * @param mapOfEmployee : Map Of employee.
     * @param mapOfProject : Map Of project.
     * @param mapOfProjectTasks : Map Of project tasks.
     *
     * @return List < GP_Timesheet_Entry__c > : List of merged timsheet entries using timesheet and temporary data.
     */
    public static List < GP_Timesheet_Entry__c > getMergedTimeSheetEntriesDataForBulk(map < String, GP_Temporary_Data__c > mapOfUniqueTemporaryTimeSheets,
        map < String, GP_Timesheet__c > mapOfUniqueTimeSheets,
        map < String, GP_Employee_Master__c > mapOfEmployees,
        map < String, GP_Project__c > mapOfProjects,
        map < String, GP_Project_Task__c > mapOfProjectTask,
        Map < String, GP_Temporary_Data__c > mapOfTemporaryData) {
        List < GP_Timesheet_Entry__c > lstTimeSheetEntries = new List < GP_Timesheet_Entry__c > ();
        Set < String > setOfKeysPushedToList = new Set < String > ();
            String keyOfTemporaryData;
        for (String key: mapOfUniqueTimeSheets.KeySet()) {
            GP_Timesheet_Entry__c objTimeSheetEntry = new GP_Timesheet_Entry__c();
            GP_Timesheet__c objTimeSheet = mapOfUniqueTimeSheets.get(key);
            keyOfTemporaryData = objTimeSheet.GP_Employee_Number__c + '@@' + objTimeSheet.GP_Project_Number__c + '@@';
            keyOfTemporaryData += String.valueOf(Date.valueOf(objTimeSheet.GP_Expenditure_Item_Date__c)) + '@@' + objTimeSheet.GP_Expenditure_Type__c;
            if (mapOfEmployees.containsKey(objTimeSheet.GP_Person_Id__c) &&
                mapOfProjects.containsKey(objTimeSheet.GP_Project_Number__c) &&
                isEligibleForCreation(Date.ValueOf(objTimeSheet.GP_Expenditure_Item_Date__c),
                    mapOfProjectTask.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c))) {
                objTimeSheetEntry.GP_Actual_Hours__c = objTimeSheet.GP_Hours__c;
                objTimeSheetEntry.GP_Date__c = Date.ValueOf(objTimeSheet.GP_Expenditure_Item_Date__c);
                objTimeSheetEntry.GP_Employee__r = mapOfEmployees.get(objTimeSheet.GP_Person_Id__c);
                objTimeSheetEntry.GP_Employee__r.Name = mapOfEmployees.get(objTimeSheet.GP_Person_Id__c).Name;
                objTimeSheetEntry.GP_Ex_Type__c = objTimeSheet.GP_Expenditure_Type__c;
                //objTimeSheetEntry.GP_Modified_Hours__c = objTimeSheet.GP_Hours__c;
                //objTimeSheetEntry.GP_Project__r = mapOfProjects.get(objTimeSheet.GP_Project_Number__c);
                //objTimeSheetEntry.GP_Project__r.Name = mapOfProjects.get(objTimeSheet.GP_Project_Number__c).Name;
                objTimeSheetEntry.GP_Project_Oracle_PID__c = mapOfProjects.get(objTimeSheet.GP_Project_Number__c).GP_Oracle_PID__c;
                //objTimeSheetEntry.GP_Project_Task__r = mapOfProjectTask.get(objTimeSheet.GP_Task_Number__c);
                //objTimeSheetEntry.GP_Project_Task__r.Name = mapOfProjectTask.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c).Name;
                objTimeSheetEntry.GP_Project_Task_Oracle_Id__c = mapOfProjectTask.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c).GP_Task_Number__c;
                //objTimeSheetEntry.GP_Project__r.RecordType.Name = mapOfProjects.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c).RecordType.Name;
            }
            else if(mapOfProjectTask.containsKey(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c) &&
                    !isEligibleForCreation(Date.ValueOf(objTimeSheet.GP_Expenditure_Item_Date__c),
                    mapOfProjectTask.get(objTimeSheet.GP_Task_Number__c + '@@' + objTimeSheet.GP_Project_Number__c))){
                if(mapOfTemporaryData.containsKey(key)){
                    mapOfTemporaryData.get(keyOfTemporaryData).GP_Validation_Result__c += 'Project Task is Inactive'; 
                    mapOfTemporaryData.get(keyOfTemporaryData).GP_Is_Failed__c = true;
                }else if(mapOfUniqueTemporaryTimeSheets.containsKey(key))
                {
                    mapOfUniqueTemporaryTimeSheets.get(key).GP_Validation_Result__c = 'Project Task is Inactive'; 
                    mapOfUniqueTemporaryTimeSheets.get(key).GP_Is_Failed__c = true;
                    mapOfTemporaryData.put(keyOfTemporaryData,mapOfUniqueTemporaryTimeSheets.get(key));
                }
            }
            if (mapOfUniqueTemporaryTimeSheets.containsKey(key))
                objTimeSheetEntry.GP_Modified_Hours__c = mapOfUniqueTemporaryTimeSheets.get(key).GP_TSC_Modified_Hours__c;

            setOfKeysPushedToList.add(key);

            if (objTimeSheetEntry.GP_Project_Oracle_PID__c != null)
                lstTimeSheetEntries.add(objTimeSheetEntry);
        }
        for (String key: mapOfUniqueTemporaryTimeSheets.KeySet()) {
            if (!setOfKeysPushedToList.contains(key)) {
                GP_Temporary_Data__c objTemporaryTimeSheetData = mapOfUniqueTemporaryTimeSheets.get(key);
                GP_Timesheet_Entry__c objTimeSheetEntry = new GP_Timesheet_Entry__c();
                keyOfTemporaryData = objTemporaryTimeSheetData.GP_Employee__c + '@@' + objTemporaryTimeSheetData.GP_Project__c + '@@';
            keyOfTemporaryData += String.valueOf(Date.valueOf(objTemporaryTimeSheetData.GP_TSC_Date__c)) + '@@' + objTemporaryTimeSheetData.GP_TSC_Ex_Type__c;
                if (mapOfEmployees.containsKey(objTemporaryTimeSheetData.GP_Employee__c) &&
                    mapOfProjects.containsKey(objTemporaryTimeSheetData.GP_Project__c) &&
                    mapOfProjectTask.containsKey(objTemporaryTimeSheetData.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheetData.GP_Project__c) &&
                    isEligibleForCreation(Date.ValueOf(objTemporaryTimeSheetData.GP_TSC_Date__c),
                        mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheetData.GP_Project__c))) {
                    objTimeSheetEntry.GP_Modified_Hours__c = objTemporaryTimeSheetData.GP_TSC_Modified_Hours__c;
                    objTimeSheetEntry.GP_Date__c = Date.ValueOf(objTemporaryTimeSheetData.GP_TSC_Date__c);
                    objTimeSheetEntry.GP_Employee__c = mapOfEmployees.get(objTemporaryTimeSheetData.GP_Employee__c).Id;
                    objTimeSheetEntry.GP_Employee__r = mapOfEmployees.get(objTemporaryTimeSheetData.GP_Employee__c);
                    objTimeSheetEntry.GP_Employee__r.Name = mapOfEmployees.get(objTemporaryTimeSheetData.GP_Employee__c).Name;
                    objTimeSheetEntry.GP_Ex_Type__c = objTemporaryTimeSheetData.GP_TSC_Ex_Type__c;
                    //objTimeSheetEntry.GP_Modified_Hours__c = objTimeSheet.GP_Hours__c;
                    //objTimeSheetEntry.GP_Project__c = mapOfProjects.get(objTemporaryTimeSheetData.GP_Project__c).Id;
                    //objTimeSheetEntry.GP_Project__r = mapOfProjects.get(objTemporaryTimeSheetData.GP_Project__c);
                    //objTimeSheetEntry.GP_Project__r.Name = mapOfProjects.get(objTemporaryTimeSheetData.GP_Project__c).Name;
                    objTimeSheetEntry.GP_Project_Oracle_PID__c = mapOfProjects.get(objTemporaryTimeSheetData.GP_Project__c).GP_Oracle_PID__c;
                    //objTimeSheetEntry.GP_Project_Task__c = mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c).Id;
                    //objTimeSheetEntry.GP_Project_Task__r = mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c);
                    //objTimeSheetEntry.GP_Project_Task__r.Name = mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c).Name;
                    objTimeSheetEntry.GP_Project_Task_Oracle_Id__c = mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheetData.GP_Project__c).GP_Task_Number__c;
                    lstTimeSheetEntries.add(objTimeSheetEntry);
                }
			else if(mapOfProjectTask.containsKey(objTemporaryTimeSheetData.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheetData.GP_Project__c) 
                    && !isEligibleForCreation(Date.ValueOf(objTemporaryTimeSheetData.GP_TSC_Date__c),
                    mapOfProjectTask.get(objTemporaryTimeSheetData.GP_TSC_Project_Task__c + '@@' + objTemporaryTimeSheetData.GP_Project__c))){
                if(mapOfTemporaryData.containsKey(keyOfTemporaryData)){
                    mapOfTemporaryData.get(keyOfTemporaryData).GP_Validation_Result__c += 'Project Task is Inactive'; 
                    mapOfTemporaryData.get(keyOfTemporaryData).GP_Is_Failed__c = true;
                }else 
                {
                    mapOfUniqueTemporaryTimeSheets.get(key).GP_Validation_Result__c = 'Project Task is Inactive'; 
                    mapOfUniqueTemporaryTimeSheets.get(key).GP_Is_Failed__c = true;
                    mapOfTemporaryData.put(keyOfTemporaryData,mapOfUniqueTemporaryTimeSheets.get(key));
                }
            }
            }
        }
            system.debug('mapOfTemporaryData'+mapOfTemporaryData);
        system.debug('getMergedTimeSheetEntriesData' + lstTimeSheetEntries);
        return lstTimeSheetEntries;
    }

    /**
     * @description Get All timesheet entries for timesheet correction UI.
     * 
     * @param employeeMonthYear : Set of Employee-month-year whose timesheet entries needs to be fetched.
     *
     * @return List < GP_Timesheet_Entry__c > : List of timesheet entries.
     */
    public static List < GP_Timesheet_Entry__c > getAllTimeSheetEntries(Set < String > employeeMonthYear,
        Map < String, GP_Project__c > mapOfProject,
        Map < String, GP_Project_Task__c > mapOfProjectTasks) {

        setOfTaskNames = new Set < String > ();
        setOfProjectNumbers = new Set < String > ();
        setOfPersonIds = new Set < String > ();
        system.debug('setOfEmployeeMonthYear' + employeeMonthYear);
        List < GP_Timesheet__c > lstOfTimeSheetData = getTimeSheetData(employeeMonthYear);
        system.debug('lstOfTimeSheetData' + lstOfTimeSheetData);
        List < GP_Timesheet_Entry__c > lstOfTimeSheetEntries = getTimeSheetEntryData(employeeMonthYear);
        system.debug('lstOfTimeSheetEntries' + lstOfTimeSheetEntries);
        Map < String, GP_Timesheet__c > mapOfUniqueTimeSheets = getUniqueTimeSheetData(lstOfTimeSheetData);
        system.debug('mapOfUniqueTimeSheets' + mapOfUniqueTimeSheets);
        Map < String, GP_Timesheet_Entry__c > mapOfUniqueTimeSheetEntries = getUniqueTimeSheetEntriesData(lstOfTimeSheetEntries);
        system.debug('mapOfUniqueTimeSheetEntries' + mapOfUniqueTimeSheetEntries);
        setStaticSetValues(lstOfTimeSheetData, lstOfTimeSheetEntries);
        setMapOfProjectTasks(setOfTaskNames, mapOfProjectTasks);
        system.debug('mapOfProjectTasks' + mapOfProjectTasks);
        Map < String, GP_Employee_Master__c > mapOfEmployee = new Map < String, GP_Employee_Master__c > ();
        getMapOfEmployee(setOfPersonIds, mapOfEmployee);

        system.debug('mapOfEmployee' + mapOfEmployee);
        system.debug('setOfProjectNumbers' + setOfProjectNumbers);
        //mapOfProject = new Map < String, GP_Project__c > ();
        getMapOfProject(setOfProjectNumbers, mapOfProject);
        system.debug('mapOfProject' + mapOfProject);
        return getMergedTimeSheetEntriesData(mapOfUniqueTimeSheetEntries, mapOfUniqueTimeSheets, mapOfEmployee, mapOfProject, mapOfProjectTasks);
    }

    /**
     * @description Get All timesheet entries for timesheet correction UI.
     * 
     * @param employeeMonthYear : Set of Employee-month-year whose timesheet entries needs to be fetched.
     *
     * @return List < GP_Timesheet_Entry__c > : List of timesheet entries.
     */
    public static List < GP_Timesheet_Entry__c > getAllTimeSheetEntriesInSFDC(Map < String, GP_Project__c > mapOfProject,
        Map < String, GP_Project_Task__c > mapOfProjectTasks,
        GP_Timesheet_Transaction__c objTimeSheetTransaction) {

        setOfTaskNames = new Set < String > ();
        setOfProjectNumbers = new Set < String > ();
        setOfPersonIds = new Set < String > ();
        List < GP_Timesheet_Entry__c > lstOfTimeSheetEntries = new GPSelectorTimesheetEntry().getTimeSheetEntriesForTimesheetTransactionRecord(objTimeSheetTransaction.Id);
        setStaticSetValues(new List < GP_Timesheet__c >(), lstOfTimeSheetEntries);
        setMapOfProjectTasks(setOfTaskNames, mapOfProjectTasks);
        system.debug('mapOfProjectTasks' + mapOfProjectTasks);
        Map < String, GP_Employee_Master__c > mapOfEmployee = new Map < String, GP_Employee_Master__c > ();
        getMapOfEmployee(setOfPersonIds, mapOfEmployee);
        getMapOfProject(setOfProjectNumbers, mapOfProject);
        return lstOfTimeSheetEntries;
    }

    /**
     * @description Get All timesheet entries for timesheet batch.
     * 
     * @param lstOfTemporaryDataForTimeSheet : List of temporary data that is bieng uploaded.
     * @param mapOfEmployees : Map Of employees.
     * @param mapOfProjects : Map of projects.
     *
     * @return List < GP_Timesheet_Entry__c > : List of timesheet entries.
     */
    public static List < GP_Timesheet_Entry__c > getAllTimeSheetEntriesForBatch(List < GP_Temporary_Data__c > lstOfTemporaryDataForTimeSheet,
        Map < String, GP_Employee_Master__c > mapOfEmployees,
        Map < String, GP_Project__c > mapOfProjects,
        Map < String, GP_Project_Task__c > mapOfProjectTasks,
        Map < String, GP_Temporary_Data__c > mapOfTemporaryData) {
        setOfTaskNames = new Set < String > ();
        setOfProjectNumbers = new Set < String > ();
        setOfPersonIds = new Set < String > ();
        system.debug('lstOfTemporaryDataForTimeSheet' + lstOfTemporaryDataForTimeSheet);
        //setOfEmployeeMonthYear Person Id of employee...
        Set < String > setOfEmployeeMonthYear = getEmployeeMonthYear(lstOfTemporaryDataForTimeSheet);
        system.debug('setOfEmployeeMonthYear' + setOfEmployeeMonthYear);
        List < GP_Timesheet__c > lstOfTimeSheetData = getTimeSheetData(setOfEmployeeMonthYear);
        system.debug('lstOfTimeSheetData' + lstOfTimeSheetData);
        Map < String, GP_Timesheet__c > mapOfUniqueTimeSheets = getUniqueTimeSheetData(lstOfTimeSheetData);
        system.debug('mapOfUniqueTimeSheets' + mapOfUniqueTimeSheets);
        Map < String, GP_Temporary_Data__c > mapOfUniqueTemporaryTimeSheetEntries = getUniqueTemporaryTimeSheetData(lstOfTemporaryDataForTimeSheet);
        system.debug('mapOfUniqueTemporaryTimeSheetEntries' + mapOfUniqueTemporaryTimeSheetEntries);
        List < GP_Timesheet_Entry__c > lstOfTimeSheetEntries = getTimeSheetEntryData(setOfEmployeeMonthYear);
        system.debug('lstOfTimeSheetEntries' + lstOfTimeSheetEntries);
        setStaticSetValues(lstOfTimeSheetData, lstOfTimeSheetEntries);
        setMapOfProjectTasks(setOfTaskNames, mapOfProjectTasks);
        system.debug('mapOfProjectTasks' + mapOfProjectTasks);
        getMapOfEmployee(setOfPersonIds, mapOfEmployees);
        system.debug('mapOfEmployee' + mapOfEmployees);
        getMapOfProject(setOfProjectNumbers, mapOfProjects);
        system.debug('mapOfProject' + mapOfProjects);
        Map < String, GP_Timesheet_Entry__c > mapOfUniqueTimeSheetEntriesInDB = getUniqueTimeSheetEntriesData(lstOfTimeSheetEntries);
        system.debug('mapOfUniqueTimeSheetEntries' + mapOfUniqueTimeSheetEntriesInDB);
        return getMergedTimeSheetEntriesDataForBulk(mapOfUniqueTemporaryTimeSheetEntries, mapOfUniqueTimeSheets,
            mapOfEmployees, mapOfProjects, mapOfProjectTasks,mapOfTemporaryData);
    }

    /**
     * @description Get Employee month year Set.
     * 
     * @param lstOfTemporaryDataForTimeSheet : List of temporary data that is bieng uploaded.
     *
     * @return Set < String > : Set of Employee month year.
     */
    private static Set < String > getEmployeeMonthYear(List < GP_Temporary_Data__c > lstOfTemporaryDataForTimeSheet) {
        Set < String > setOfEmployeeMonthYear = new Set < String > ();
        for (GP_Temporary_Data__c objTemporaryTimeSheet: lstOfTemporaryDataForTimeSheet) {
            if (objTemporaryTimeSheet.GP_Employee__c != null || objTemporaryTimeSheet.GP_TSC_Month_Year__c != null)
                setOfEmployeeMonthYear.add(objTemporaryTimeSheet.GP_Employee__c + objTemporaryTimeSheet.GP_TSC_Month_Year__c);
            setOfTaskNames.add(objTemporaryTimeSheet.GP_TSC_Project_Task__c);
            setOfProjectNumbers.add(objTemporaryTimeSheet.GP_Project__c);
            setOfPersonIds.add(objTemporaryTimeSheet.GP_Employee__c);
        }
        return setOfEmployeeMonthYear;
    }
}