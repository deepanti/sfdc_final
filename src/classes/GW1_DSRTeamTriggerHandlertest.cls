/*
      This class is used to test GW1_DSRTeamTriggerHandler class
      --------------------------------------------------------------------------------------
      Name                 Date                 Version                      
      --------------------------------------------------------------------------------------
      Rishi Patel                    3/17/2016                    1.0  
      --------------------------------------------------------------------------------------
      Shubham Saxena                 30/3/2016                    1.0
      --------------------------------------------------------------------------------------
  */
@IsTest
public class GW1_DSRTeamTriggerHandlertest {
    
    @testSetup static void addteamMember() {
        GW1_CommonTracker.createTriggerCustomSetting('GW1_DSRTeamTrigger');
        //GW1_CommonTracker.createTriggerCustomSetting('GW1_DSRTrigger');
        user objuser=GW1_CommonTracker.createUser('standt','testing','awesome@www.com');
        system.debug('user'+objuser);
        GW1_Bid_Manager__c objbidmanager=GW1_CommonTracker.createBidManager();
        GW1_DSR__c objgw1dsrc = GW1_CommonTracker.createGW1DSR('abcd');
        objGW1DSRc.GW1_Bid_Manager__c=objbidmanager.id;
        update objgw1dsrc;
        system.debug('(in test)objgw1dsrc ' + objgw1dsrc);
        
        
        
        //Shubham 30/3/2016
        GW1_DSR__c objgw1dsrc1 = [Select id, Name , GW1_Primary_Chatter_ID__c, GW1_DSR_Number__c from GW1_DSR__c where GW1_DSR_Number__c != null limit 1];
        system.debug('objgw1dsrc1 ' + objgw1dsrc1);
        CollaborationGroup objCollaborationGroup = GW1_CommonTracker.createCollaborationGroup(objgw1dsrc1.Name +'-'+ objgw1dsrc1.GW1_DSR_Number__c);
        system.debug('objCollaborationGroup ' + objCollaborationGroup); 
        //End
        
        createGW1_DSR_Group(objCollaborationGroup.id);
        system.debug('(in test)objgw1dsrc ' + objgw1dsrc );
        GW1_DSR_Team__c objDSRTeam = GW1_CommonTracker.createGW1DSRTeam(objgw1dsrc , objuser, true, 'BD');
        system.debug('@objDSRTeam______'+objDSRTeam);
        
        //Shubham 6/4/2016
        Test.startTest();
        update objDSRTeam;
        Test.stopTest();
        //End Shubham
	}

    
    static testMethod void update_removeMember()
    {
        
        //objDSRTeam.GW1_Role__c = 'Head';
        GW1_DSR_Team__c objDSRTeam = [select GW1_DSR__c, GW1_Role__c, GW1_User__c, GW1_Is_active__c from GW1_DSR_Team__c where GW1_Is_active__c=true limit 1];
        objDSRTeam.GW1_Is_active__c = false;
        // Shubham 6/4/2016
        update objDSRTeam;
        // End shubham
        system.debug('@objDSRTeam 2 ______'+objDSRTeam);    
    }
    
    static testMethod void update_deleteTeam()
    {
        GW1_DSR_Team__c objDSRTeam = [select GW1_DSR__c, GW1_Role__c, GW1_User__c, GW1_Is_active__c from GW1_DSR_Team__c where GW1_Is_active__c=true limit 1];
        objDSRTeam.GW1_Is_active__c = false;
    	delete objDSRTeam;    
    }
    
    
    private static void createGW1_DSR_Group(Id chatterId)
    {
        GW1_DSR__c objgw1dsrc = [Select id, Name , GW1_DSR_Number__c from GW1_DSR__c where GW1_DSR_Number__c != null limit 1];
        GW1_DSR_Group__c obj = GW1_CommonTracker.createGW1DSRGroup('DSR group', objgw1dsrc, true);
        obj.GW1_Chatter_Group_Id__c = chatterId;
        update obj;
        system.debug('(in test)GW1_DSR_Group__c obj ' + obj);
    }
    
}