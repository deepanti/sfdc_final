public class GPServiceProjectAddress {
    public static Boolean isFormattedLogRequired = true;
    public static Boolean isLogForTemporaryId;
    public static String userDefinedStage;

    private static final String CUSTOMER_NAME_EMPTY_ERROR_LABEL = 'Customer Name can\'t be empty';
    private static final String CUSTOMER_NAME_DUMMY_ERROR_LABEL = 'Please add Valid Bill To Details to create PID in Billing & Cost Charging Stage/Revenue & Cost Charging Stage/Approved Stage';
    private static final String BILL_TO_ADDRESS_EMPTY_ERROR_LABEL = 'Bill to Address can\'t be empty';
    private static final String SHIP_TO_ADDRESS_EMPTY_ERROR_LABEL = 'Ship to Address can\'t be empty';
    private static final String SHIP_TO_ADDRESS_INVALID_ERROR_LABEL = 'Ship to Address is not valid';
    private static final String BILL_TO_ADDRESS_INVALID_ERROR_LABEL = 'Bill to Address is not valid';

    private static final String BILL_TO_LABEL = 'BILL_TO';
    private static final String SHIP_TO_LABEL = 'SHIP_TO';
    private static Map<String, Set<Id>> mapOfCustomerIdToSetOfValidBillToAddress;
    private static Map<String, Set<Id>> mapOfCustomerIdToSetOfValidShipToAddress;
    private static List<Id> listOfCustomerId;
    private static Set<Id> setOfCustomerId;
    private static Id billingEntityId;

    public static Map < String, List < String >> validateProjctAddress(GP_Project__c project, List < GP_Project_Address__c > listOfProjectAdress) {
        Map < String, List < String >> mapOfSectionToListOfErrors = new Map < String, List < String >> ();

        if(project.RecordType.Name == 'Indirect PID') {
            return mapOfSectionToListOfErrors;
        }
        
        billingEntityId = project.GP_Operating_Unit__c;
        setOfCustomerId = new Set<Id>();
        listOfCustomerId = new List<Id>();

        String validationMessage;
        Integer directAddressRecordCount = 0, nonDummyCustomerCount = 0;
        String key;

        if (listOfProjectAdress != null && listOfProjectAdress.size() > 0) {
            for (GP_Project_Address__c projectAddress: listOfProjectAdress) {

                if (projectAddress.GP_Relationship__c == 'Direct') {
                    directAddressRecordCount++;                    
                }

                if (isFormattedLogRequired) {
                    key = 'Project Address';
                } else if (isLogForTemporaryId) {
                    key = projectAddress.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = projectAddress.Id;
                }

                setOfCustomerId.add(projectAddress.GP_Customer_Name__c);

                if (projectAddress.GP_Customer_Name__c == null) {
                    validationMessage = projectAddress.GP_Relationship__c + ': ' + CUSTOMER_NAME_EMPTY_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }

                if (!projectAddress.GP_Customer_Name__r.GP_Is_Dummy__c) {
                    nonDummyCustomerCount++;
                }

                if (projectAddress.GP_Bill_To_Address__c == null) {
                    validationMessage = projectAddress.GP_Relationship__c + ': ' + BILL_TO_ADDRESS_EMPTY_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }

                if (projectAddress.GP_Ship_To_Address__c == null) {
                    validationMessage = projectAddress.GP_Relationship__c + ': ' + SHIP_TO_ADDRESS_EMPTY_ERROR_LABEL;

                    if (!mapOfSectionToListOfErrors.containsKey(key)) {
                        mapOfSectionToListOfErrors.put(key, new List < String > ());
                    }

                    mapOfSectionToListOfErrors.get(key).add(validationMessage);
                }
            }
            
            String uniqueKey;

            setMapOfCustomerIdToListOfValidAddress();
            
            for (GP_Project_Address__c projectAddress: listOfProjectAdress) {

                if(projectAddress.GP_Bill_To_Address__r != null &&
                   projectAddress.GP_Bill_To_Address__r.Name != null &&
                   !projectAddress.GP_Bill_To_Address__r.Name.contains('GENPACT Dummy')) {
                    uniqueKey = projectAddress.GP_Customer_Name__c + '--' + billingEntityID;

                    Set<Id> setOfBillToAddress = mapOfCustomerIdToSetOfValidBillToAddress.get(uniqueKey);

                    if(setOfBillToAddress == null || !setOfBillToAddress.contains(projectAddress.GP_Bill_To_Address__c)) {
                        validationMessage = projectAddress.GP_Relationship__c + ': ' + SHIP_TO_ADDRESS_INVALID_ERROR_LABEL;
                    
                        if (isFormattedLogRequired) {
                            key = 'Project Address';
                        } else if (isLogForTemporaryId) {
                            key = projectAddress.GP_Last_Temporary_Record_Id__c;
                        } else if (!isLogForTemporaryId) {
                            key = projectAddress.Id;
                        }
                        
                        if (!mapOfSectionToListOfErrors.containsKey(key)) {
                            mapOfSectionToListOfErrors.put(key, new List < String > ());
                        }

                        mapOfSectionToListOfErrors.get(key).add(validationMessage);                        
                    }
                }

                if(projectAddress.GP_Ship_To_Address__r != null &&
                   projectAddress.GP_Ship_To_Address__r.Name != null &&
                   !projectAddress.GP_Ship_To_Address__r.Name.contains('GENPACT Dummy')) {
                    uniqueKey = projectAddress.GP_Customer_Name__c + '--' + billingEntityID;

                    Set<Id> setOfShipToAddress = mapOfCustomerIdToSetOfValidShipToAddress.get(uniqueKey);

                    if(setOfShipToAddress == null ||
                       !setOfShipToAddress.contains(projectAddress.GP_Ship_To_Address__c)) {
                        validationMessage = projectAddress.GP_Relationship__c + ': ' + BILL_TO_ADDRESS_INVALID_ERROR_LABEL;
                    
                        if (isFormattedLogRequired) {
                            key = 'Project Address';
                        } else if (isLogForTemporaryId) {
                            key = projectAddress.GP_Last_Temporary_Record_Id__c;
                        } else if (!isLogForTemporaryId) {
                            key = projectAddress.Id;
                        }
                        
                        if (!mapOfSectionToListOfErrors.containsKey(key)) {
                            mapOfSectionToListOfErrors.put(key, new List < String > ());
                        }

                        mapOfSectionToListOfErrors.get(key).add(validationMessage);                        
                    }
                }
            }
        }
        System.debug('userDefinedStage: ' + userDefinedStage);
        
        GPWrapFieldValue fieldValueWraper = (GPWrapFieldValue)JSON.deserialize(userDefinedStage, GPWrapFieldValue.class);
        System.debug('fieldValueWraper: '+ fieldValueWraper.strStageName);
        if (directAddressRecordCount == 0) {
            validationMessage = 'No Direct Address Found.';
            
            if (isFormattedLogRequired) {
                key = 'Project Address';
            } else if (isLogForTemporaryId) {
                key = project.GP_Last_Temporary_Record_Id__c;
            } else if (!isLogForTemporaryId) {
                key = project.Id;
            }
            
            if (!mapOfSectionToListOfErrors.containsKey(key)) {
                mapOfSectionToListOfErrors.put(key, new List < String > ());
            }
            
            mapOfSectionToListOfErrors.get(key).add(validationMessage);
        } else if(fieldValueWraper.strStageName != null &&
                  fieldValueWraper.strStageName != 'Cost Charging' &&
                  project.RecordType.Name != 'Indirect PID' && 
                  nonDummyCustomerCount == 0) {

                if (isFormattedLogRequired) {
                    key = 'Project Address';
                } else if (isLogForTemporaryId) {
                    key = project.GP_Last_Temporary_Record_Id__c;
                } else if (!isLogForTemporaryId) {
                    key = project.Id;
                }
                
                validationMessage = CUSTOMER_NAME_DUMMY_ERROR_LABEL; // + ' for ' + fieldValueWraper.strStageName + ' Stage ';

                if (!mapOfSectionToListOfErrors.containsKey(key)) {
                    mapOfSectionToListOfErrors.put(key, new List < String > ());
                }

                mapOfSectionToListOfErrors.get(key).add(validationMessage);
        }

        return mapOfSectionToListOfErrors;
    }

    static void setMapOfCustomerIdToListOfValidAddress() {

        mapOfCustomerIdToSetOfValidShipToAddress = new Map<String, Set<Id>>();
        mapOfCustomerIdToSetOfValidBillToAddress = new Map<String, Set<Id>>();
        listOfCustomerId.addAll(setOfCustomerId);

        List<GP_Address__c> listOfValidAddress = GPSelectorAddress.getAddress(listOfCustomerId, billingEntityID);
        String siteCode, key;

        for(GP_Address__c address : listOfValidAddress) {
            key = address.GP_Customer__c + '--' + address.GP_Billing_Entity__c;

            siteCode = address.GP_Site_Use_Code__c != null ? address.GP_Site_Use_Code__c.trim() : '';

            if(siteCode.equalsIgnoreCase(BILL_TO_LABEL)) {            
                if(!mapOfCustomerIdToSetOfValidBillToAddress.containsKey(key)) {
                    mapOfCustomerIdToSetOfValidBillToAddress.put(key, new Set<Id>());
                }
                
                mapOfCustomerIdToSetOfValidBillToAddress.get(key).add(address.Id);
            } else if(siteCode.equalsIgnoreCase(SHIP_TO_LABEL)) {            
                if(!mapOfCustomerIdToSetOfValidShipToAddress.containsKey(key)) {
                    mapOfCustomerIdToSetOfValidShipToAddress.put(key, new Set<Id>());
                }
                
                mapOfCustomerIdToSetOfValidShipToAddress.get(key).add(address.Id);
            }
        }
    }
}