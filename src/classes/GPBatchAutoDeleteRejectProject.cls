// ------------------------------------------------------------------------------------------------ 
// Description: Auto Delete projects Whose GP Auto Reject Date is Ahead from today by 
// the integer value of the cutom label GP_Days_Ahead_Auto_Rejection_Date_For_Deleting_Project 
// and the Approval Status is Rejected
// ------------------------------------------------------------------------------------------------
// Created Date::21-Nov -2017  Created By : Mandeep Singh Chauhan
// ------------------------------------------------------------------------------------------------ 
global class GPBatchAutoDeleteRejectProject implements
Database.Batchable < sObject > , Database.Stateful {
    global date dateofprojectRejection = System.Today().adddays(-(integer.valueOf(system.label.GP_Days_Ahead_Auto_Rejection_Date_For_Deleting_Project)));

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select Id, Name, (Select id from Project_Classifications__r),
            (Select id from Project_Version_History__r) from GP_Project__c where
            GP_Auto_Reject_Date__c =: dateofprojectRejection and
            GP_Approval_Status__c =: 'Rejected'
        ]);
    }

    global void execute(Database.BatchableContext bc, List < GP_Project__c > listofRejectedprojectstobedeleted) {

        savepoint sp = database.setSavepoint();
        try {

            list < GP_Project_Version_History__c > lstProjectVersionHistory = new list < GP_Project_Version_History__c > ();
            list < GP_Project_Classification__c > lstProjectClassification = new list < GP_Project_Classification__c > ();

            if (listofRejectedprojectstobedeleted != null && listofRejectedprojectstobedeleted.size() > 0) {

                for (GP_Project__c objProject: listofRejectedprojectstobedeleted) {
                    if (objProject.Project_Version_History__r.size() > 0) {
                        lstProjectVersionHistory.add(objProject.Project_Version_History__r);
                    }
                    if (objProject.Project_Classifications__r.size() > 0) {
                        lstProjectClassification.add(objProject.Project_Classifications__r);
                    }
                }

                if (lstProjectVersionHistory != null && lstProjectVersionHistory.size() > 0) {
                    delete(lstProjectVersionHistory);
                }

                if (lstProjectClassification != null && lstProjectClassification.size() > 0) {
                    delete(lstProjectClassification);
                }

                delete listofRejectedprojectstobedeleted;
            }

        } catch (exception ex) {

            database.rollback(sp);
        }


    }

    global void finish(Database.BatchableContext bc) {}
}