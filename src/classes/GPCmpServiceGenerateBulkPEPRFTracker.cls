@isTest 
public class GPCmpServiceGenerateBulkPEPRFTracker{
    
    public static testMethod void testApprovedProject() {        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        gp_Project__c obj = new gp_Project__c ();
        obj.GP_Oracle_Status__c = 'S';
        obj.GP_Oracle_PID__c = 'aa';
        obj.GP_Business_Type__c ='PBB';
        obj.GP_Approval_DateTime__c =  system.today();
        obj.GP_Deal__c = dealObj.id;
        
        insert obj;
        
        string startdate = string.valueof(system.today()); 
        GP_Project_Version_History__c obj1 = new GP_Project_Version_History__c(GP_Project__c =obj.id,GP_Document_Creation_Date__c = system.today(),
                                                                               GP_Content_Version_Document_Id__c = '000qq',GP_Oracle_Status__c = 'S');
        insert obj1;
        string enddate = string.valueof(system.today()+1);
        GPCmpServiceGenerateBulkPEPRF.getApprovedPinnaclePIDForDuration(startdate , enddate ,'update', 'All');
        GPCmpServiceGenerateBulkPEPRF.getBusinessNames();
    }
}