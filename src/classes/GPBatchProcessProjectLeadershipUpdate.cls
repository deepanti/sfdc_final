/**
 * @group Mass Update/Upload. 
 *
 * @description Batch class for Mass Update Project Leadership.
 *              Inherits GPBatchMassUpdateAndUploadHelper Class.
 */
global class GPBatchProcessProjectLeadershipUpdate extends GPBatchMassUpdateAndUploadHelper implements Database.Batchable < sObject > {

    private static Final String CHANGE_IN_EMPLOYEE_START_DATE_REQUIRED_VALIDATION_MESSAGE = 'Employee is being updated you need to fill the start date';
    private final String PROJECT_LEADERSHIP_NOT_FOUND_ERROR_LABEL = 'Project LeaderShip Not Found In Database which you are trying to update !';
    private static Final String START_DATE_VALIDATION_MESSAGE = 'Start date should be greater than previous start date';
    private final String INAVLID_EMPLOYEE_ID_ERROR_LABEL = 'Invalid Id of Employee!';

    private List < GP_Project_Leadership__c > lstOfProjectLeadership = new List < GP_Project_Leadership__c > ();
    private List < GP_Project_Leadership__c > lstOfProjectLeadershipInactiveRecords = new List < GP_Project_Leadership__c > ();
	// Mass Update Change
	private Map < Id,GP_Employee_Master__c > mapOfEmployeeIdToEmployeeMaster = new Map < Id,GP_Employee_Master__c > ();
	// Mass Update Change
	private final String START_DATE_END_DATE_ERROR_LABEL = 'Start Date should be greater than previous start Date'; 
	// Mass Update Change
	private final String INVALID_EMPLOYEE_ID_ERROR_LABEL = 'Invalid Id of Employee!';

    /**
     * @description Constructor with jobId whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * 
     */
    global GPBatchProcessProjectLeadershipUpdate(String jobRecordId) {
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
        jobType = 'Update Project LeaderShip';
    }

    /**
     * @description Constructor with jobId and setOf Project Oracle PID's whose temporary records need to be processed.
     * @param jobRecordId : Job record Id.
     * @param setOfProjectOracleIds : Set of Project Oracle PID's.
     * 
     */
    global GPBatchProcessProjectLeadershipUpdate(Set < String > setOfProjectOracleIds, String jobRecordId) {
        setOfProjectOracleIdsForBatch = setOfProjectOracleIds;
        jobId = jobRecordId; //Job Record Id whose Child Records are to be Processed.
        jobType = 'Update Project LeaderShip';
    }

    /**
     * @description Batch start method.
     * @param BC : BatchableContext.
     * 
     */
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // Update Job Record with 'Pending' Status.
        updateJobRecord('In Progress', 0, ''); // Class member of GPBatchMassUpdateAndUploadHelper.

        //Query Project Records to manage one by one project processing of temporary Data.
        String query = 'SELECT id, GP_Oracle_PID__c from GP_Project__c where GP_Oracle_PID__c in :setOfProjectOracleIdsForBatch ORDER BY LastModifiedDate Desc';

        /*String Query = 'SELECT id,GP_PL_Employee_OHR__c,GP_PL_End_Date__c,GP_PL_Leadership_Role__c,GP_PL_Start_Date__c,';
Query += 'GP_Project__c from GP_Temporary_Data__c where GP_Job_Id__c = \''+jobId+'\'';*/

        return Database.getQueryLocator(Query);
    }

    /**
     * @description Batch execute method.
     * @param BC : BatchableContext.
     * @param lstProjectsfromBatch : list of project records that will be processed.
     * 
     */
    global void execute(Database.BatchableContext BC, List < Sobject > lstProjectsfromBatch) {

        getJobRecord(jobId); // Class member of GPBatchMassUpdateAndUploadHelper.

        Savepoint sp = Database.setSavepoint();

        String oraclePID = String.ValueOf(lstProjectsfromBatch[0].get('GP_Oracle_PID__c'));

        String query = 'SELECT id, GP_Is_Failed__c,GP_PL_Employee_OHR__c, GP_PL_End_Date__c, GP_PL_Leadership_Role__c, GP_PL_Start_Date__c,';
        query += 'GP_Project__c from GP_Temporary_Data__c where GP_Job_Id__c = \'' + jobId + '\' and GP_Project__c = :oraclePID ';

        listOfTemporaryRecords = Database.query(query);

        try {
            // Set Validation configuration for project leadership to validate updated project leadership records.
            validationConfig.validateProjectLeadership = true; // Class member of GPBatchMassUpdateAndUploadHelper.

            // Create Project Oracle PID set.
            setOracleIdSet(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
            // Query Project Records using project Oracle PID set.
            setListOfProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper. 

            if (listOfProjectRecords.size() > 0) {
                // Filter the PID's which can't be processed by Mass Update/Upload depening upon their Appproval Status and OMS status.
                setStatusWiseProjectSet(); // Class member of GPBatchMassUpdateAndUploadHelper. 
                // Create map of Project Oracle Ids Vs its status of processing for mass upload and update.
                setMapToCheckValidRecordsForProjectStatus(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Filter the list of temporary data depending upon the project's approval status.
                setLstOfValidatedTemporaryData(listOfTemporaryRecords); // Class member of GPBatchMassUpdateAndUploadHelper.
                // Create map of all masters using their oracle PID's.
                setMasterDataMap(); // Class member of GPBatchMassUpdateAndUploadHelper.

                if (lstOfValidatedTemporaryData.size() > 0) {

                    // Query all project and associated child records.
                    queryProjectAndChilRecordsUsingOraclePID(setOfOracleIds); // Class member of GPBaseProjectUtil.
                    // Set Map Of project and its Associated child Records.
                    setMapOfProjectChildRecords(); // Class member of GPBaseProjectUtil.

                    //setProjectQuery();//get project records.
                    //setlistOfProjectRecordsWithChildQueried();

                    // Append temporary RecordId to project Record.
                    putTemporaryIdToProjectRecord(); // Class member of GPBatchMassUpdateAndUploadHelper.

                    //setCommitedProjectAndChildRecords();

                    // Clone Project and Validate depending upon the Validation Configuration.
                    cloneAndValidateProject(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    // Filter the Project Records which have passed the validations.
                    setListOfValidatedProjectRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.

                    if (listOfValidatedProjectRecords.size() > 0) {
                        // Create map Of Parent Project Id Vs Child Project Id.
                        setMapOfOldProjectIdVsNewProjectId(); // Class member of GPBatchMassUpdateAndUploadHelper.
                        // Set Project Leadership Records from Map Of Project Leadership(member of parent class).
                        setLstOfProjectLeadership(); // Class method.
                        // Merge Temporary record content with queried project leadership records.
                        mergeTemporaryFieldValuesWithProjectLeaderShipRecord(); // Class method.
                        // Check for leadership records for the associated project in pinnacle system that needs to be updated from Mass Update.
                        checkForNonExistantRecordsToBeUpated(); // Class method.
                        // Clone Project Child Records and Validate depending upon the Validation Configuration.
                        cloneAndValidateProjectChildRecords(); // Class member of GPBatchMassUpdateAndUploadHelper.
                    }
                }
                //Update Source Leaderships to inactive.
				// Mass Update Change
                if (lstOfProjectLeadershipInactiveRecords.size() > 0) {
                    saveResultList = Database.insert(lstOfProjectLeadershipInactiveRecords, false);
                    GPServiceProjectClone.stringWrapper relatedTemporaryId = new GPServiceProjectClone.stringWrapper();
                    String errorMessage;
                    errorMessage = parseSaveResultChildRecords(lstOfProjectLeadershipInactiveRecords, relatedTemporaryId);
                    if (errorMessage != null)
                        throw new GPServiceProjectClone.GPServiceProjectCloneException('Project LeaderShip  : ' + errorMessage, relatedTemporaryId.str);
                }
				system.debug('mapOfInvalidEntries'+mapOfInvalidEntries);
                // Update Job Record and the temporary records with validation result.
                String status = mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0 ? 'Failed' : 'Completed';
                updateJobRecord(status, mapOfInvalidEntries.values().size(), ''); // Class member of GPBatchMassUpdateAndUploadHelper.
				Database.update(lstOfNewProjectCreated, false); // Class member of GPBatchMassUpdateAndUploadHelper.
                updateJobLineItemRecord(); // Class member of GPBatchMassUpdateAndUploadHelper.
            } else {
				// Mass Update Change
				Database.rollback(sp);
                // Update Job Record with Exception Message.
                updateJobRecord('Failed', listOfTemporaryRecords.size(), PROJECT_NOT_FOUND_FOR_ORACLE_PID_ERROR_LABEL); // Class member of GPBatchMassUpdateAndUploadHelper.
            }
        } catch (GPServiceProjectClone.GPServiceProjectCloneException cloneException) {
            Database.rollback(sp);

            if (cloneException.mapOfValidations == null && cloneException.relatedOrcaleId != null) // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.relatedOrcaleId, cloneException.exceptionString); // Class member of GPBatchMassUpdateAndUploadHelper.
            else if(cloneException.relatedOrcaleId == null) // Update Validation Result of record in Invalid Entries map.
				systemThrownExceptionValue = cloneException.exceptionString;
            else // Update Validation Result of record in Invalid Entries map.
                setExceptionMessageToRelatedRecord(cloneException.mapOfValidations); // Class member of GPBatchMassUpdateAndUploadHelper.
            // Update Job Record and the temporary records with validation result.
            String status = ((mapOfInvalidEntries != null && mapOfInvalidEntries.values().size() > 0) || systemThrownExceptionValue != null || systemThrownExceptionValue != '' ) ? 'Failed' : 'Completed';
            updateJobRecord(status, mapOfInvalidEntries.values().size(), systemThrownExceptionValue); // Class member of GPBatchMassUpdateAndUploadHelper.
        } catch (Exception E) {
            Database.rollback(sp);
            // Update Job Record with Exception Message.
            updateJobRecord('Failed', listOfTemporaryRecords.size(), E.getMessage()); // Class member of GPBatchMassUpdateAndUploadHelper.
        }
    }

    /**
     * @description Batch finish method.
     * @param BC : BatchableContext.
     * 
     */
    global void finish(Database.BatchableContext BC) {
        //updateJobRecord('Completed', 0, '');
        System.debug('Success');
    }

    /**
     * @description Merge Temporary record content with queried project leadership records.
     * 
     */
    private void mergeTemporaryFieldValuesWithProjectLeaderShipRecord() {
        //map clear leadership and then reassign with updated content.
        mapOfProjectLeadership = new Map < Id, List < GP_Project_Leadership__c >> ();
        GP_Temporary_Data__c objUpdatedProjectLeaderShipValues;
        GP_Project_Leadership__c objInactiveLeadership;
        String uniqueKey;
		Set < Id > setOfEmployeeIds = new Set < Id > ();

        for (GP_Project_Leadership__c objProjectLeaderShip: lstOfProjectLeadership) {
            if (objProjectLeaderShip.GP_Employee_ID__c != null)
                setOfEmployeeIds.add(objProjectLeaderShip.GP_Employee_ID__c);
        }

        mapOfEmployeeIdToEmployeeMaster = new Map < Id, GP_Employee_Master__c > ([SELECT Id, Name, GP_ACTUAL_TERMINATION_Date__c, GP_HIRE_Date__c
            FROM GP_Employee_Master__c
            where Id in: setOfEmployeeIds
        ]);
		
        for (GP_Project_Leadership__c objProjectLeaderShip: lstOfProjectLeadership) {

            uniqueKey = objProjectLeaderShip.GP_Project__r.GP_Oracle_PID__c + (objProjectLeaderShip.GP_Leadership_Role_Name__c).toLowerCase();

            if (mapOfTemporaryDataWithUniqueKey.containsKey(uniqueKey)) {
                objUpdatedProjectLeaderShipValues = mapOfTemporaryDataWithUniqueKey.get(uniqueKey);                
                
                if (objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c < objProjectLeaderShip.GP_Start_Date__c)
                    throw new GPServiceProjectClone.GPServiceProjectCloneException(START_DATE_VALIDATION_MESSAGE, objUpdatedProjectLeaderShipValues.Id);
                if (objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c != null && !mapOfEmployeePersonIdVsSFDCId.containsKey(objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c))
                    throw new GPServiceProjectClone.GPServiceProjectCloneException(INVALID_EMPLOYEE_ID_ERROR_LABEL, objUpdatedProjectLeaderShipValues.Id);

                //EMPLOYEE CHANGE IN LEADERSHIP        
                if (objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c != null && objProjectLeaderShip.GP_Employee_ID__c != mapOfEmployeePersonIdVsSFDCId.get(objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c)) {
                    if (objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c == null)
                        throw new GPServiceProjectClone.GPServiceProjectCloneException(CHANGE_IN_EMPLOYEE_START_DATE_REQUIRED_VALIDATION_MESSAGE, objUpdatedProjectLeaderShipValues.Id);

                    objInactiveLeadership = getInactiveRecord(objProjectLeaderShip, objUpdatedProjectLeaderShipValues, true);
                    
                    if (objInactiveLeadership.GP_End_Date__c < objInactiveLeadership.GP_Start_Date__c)
                        throw new GPServiceProjectClone.GPServiceProjectCloneException(START_DATE_VALIDATION_MESSAGE, objUpdatedProjectLeaderShipValues.Id);

                    lstOfProjectLeadershipInactiveRecords.add(objInactiveLeadership);
                } else if (objUpdatedProjectLeaderShipValues.GP_PL_End_Date__c != null && objProjectLeaderShip.GP_End_Date__c != objUpdatedProjectLeaderShipValues.GP_PL_End_Date__c) {
                    if (objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c == null)
                        throw new GPServiceProjectClone.GPServiceProjectCloneException(CHANGE_IN_EMPLOYEE_START_DATE_REQUIRED_VALIDATION_MESSAGE, objUpdatedProjectLeaderShipValues.Id);

                    objInactiveLeadership = getInactiveRecord(objProjectLeaderShip, objUpdatedProjectLeaderShipValues, false);


                    if (objInactiveLeadership.GP_End_Date__c < objInactiveLeadership.GP_Start_Date__c)
                        throw new GPServiceProjectClone.GPServiceProjectCloneException(START_DATE_VALIDATION_MESSAGE, objUpdatedProjectLeaderShipValues.Id);

                    lstOfProjectLeadershipInactiveRecords.add(objInactiveLeadership);
                }
                updateValueWithTemporaryRecord(objProjectLeaderShip, objUpdatedProjectLeaderShipValues); //Class method.
                objProjectLeaderShip.GP_Active__c = true;
            }

            if (mapOfProjectLeadership.containsKey(objProjectLeaderShip.GP_Project__c))
                mapOfProjectLeadership.get(objProjectLeaderShip.GP_Project__c).add(objProjectLeaderShip);
            else
                mapOfProjectLeadership.put(objProjectLeaderShip.GP_Project__c, new List < GP_Project_Leadership__c > { objProjectLeaderShip });

            mapOfProjectOracleIdVsUniqueKeyOfChild.get(objProjectLeaderShip.GP_Project__r.GP_Oracle_PID__c).remove(uniqueKey);
        }
    }
	// Mass Update Change
	private GP_Project_Leadership__c getInactiveRecord(GP_Project_Leadership__c objProjectLeaderShip, GP_Temporary_Data__c objUpdatedProjectLeaderShipValues, Boolean isEmployeeChange) {
        GP_Project_Leadership__c objInactiveLeadership = objProjectLeaderShip.clone(false, true, true, true);
        objInactiveLeadership.GP_Active__c = false;
        objInactiveLeadership.GP_Project__c = mapOfOldProjectIdVsNewProjectId.get(objProjectLeadership.GP_Project__c);
        objInactiveLeadership.GP_Parent_Project_Leadership__c = objProjectLeadership.Id;
        objInactiveLeadership.GP_Last_Temporary_Record_Id__c = objUpdatedProjectLeaderShipValues.Id;
        objInactiveLeadership.GP_End_Date__c = getEndDateOfLeadership(objProjectLeaderShip, objUpdatedProjectLeaderShipValues, isEmployeeChange);
        return objInactiveLeadership;
    }
	// Mass Update Change
    private Date getEndDateOfLeadership(GP_Project_Leadership__c objProjectLeaderShip, GP_Temporary_Data__c objUpdatedProjectLeaderShipValues, Boolean isEmployeeChange) {
        GP_Employee_Master__c newSelectedEmployee = mapOfEmployeePersonIdVsEmployeeRecord.get(objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c);
        GP_Employee_Master__c oldSelectedEmployee = mapOfEmployeeIdToEmployeeMaster.get(objProjectLeaderShip.GP_Employee_ID__c);
        Date leadershipEndDate;
        
        try {
            if (isEmployeeChange) {
                System.debug('==newSelectedEmployee=='+ newSelectedEmployee);
                System.debug('==oldSelectedEmployee=='+ oldSelectedEmployee);
                if (oldSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c < objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c) {
                    leadershipEndDate = oldSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c.addDays(-1);
                } else {
                    leadershipEndDate = objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c.addDays(-1);
                }
                
                if (objProjectLeaderShip.GP_Leadership_Role__c == 'PROJECT MANAGER' && oldSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c < objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c) {
                    throw new GPServiceProjectClone.GPServiceProjectCloneException(START_DATE_END_DATE_ERROR_LABEL, objUpdatedProjectLeaderShipValues.Id);
                } else {
                    leadershipEndDate = objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c.addDays(-1);
                }
                
                if (newSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c != null) {
                    objUpdatedProjectLeaderShipValues.GP_PL_End_Date__c = newSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c.addDays(-1);
                }
            } else {
                leadershipEndDate = objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c.addDays(-1);
                if (newSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c != null) {
                    objUpdatedProjectLeaderShipValues.GP_PL_End_Date__c = newSelectedEmployee.GP_ACTUAL_TERMINATION_Date__c.addDays(-1);
                }
            }
        } catch(Exception ex) {
            // Mass Update Change
            system.debug('==Exception at line 301=='+ ex.getMessage());
            if(!Test.isRunningTest())
            	throw ex;
        }

        return leadershipEndDate;
    }
    /**
     * @description Update field values with temporary record and Master data Map.
     * @param objProjectLeaderShip : project Leadership Record.
     * @param objUpdatedProjectLeaderShipValues : temporary data record with new values of project leadership record.
     * 
     */
    private void updateValueWithTemporaryRecord(GP_Project_Leadership__c objProjectLeaderShip, GP_Temporary_Data__c objUpdatedProjectLeaderShipValues) {

        objProjectLeaderShip.GP_Last_Temporary_Record_Id__c = objUpdatedProjectLeaderShipValues.Id;

        if (objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c != null && !mapOfEmployeePersonIdVsSFDCId.containsKey(objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c))
            throw new GPServiceProjectClone.GPServiceProjectCloneException(INVALID_EMPLOYEE_ID_ERROR_LABEL, objUpdatedProjectLeaderShipValues.Id);

        //setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectLeaderShipValues, 'GP_PL_Employee_OHR__c', objProjectLeaderShip, 'GP_Employee_ID__c');
        if (jobRecord.GP_Column_s_To_Update_Upload__c.contains('GP_PL_Employee_OHR__c'))
            objProjectLeaderShip.GP_Employee_ID__c = mapOfEmployeePersonIdVsSFDCId.get(objUpdatedProjectLeaderShipValues.GP_PL_Employee_OHR__c);
        objProjectLeaderShip.GP_Leadership_Role_Name__c = objUpdatedProjectLeaderShipValues.GP_PL_Leadership_Role__c;
        //objProjectLeaderShip.GP_Leadership_Role_Name__c = objUpdatedProjectLeaderShipValues.GP_PL_Leadership_Role__c;

        //objProjectLeaderShip.GP_End_Date__c = objUpdatedProjectLeaderShipValues.GP_PL_End_Date__c;
        setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectLeaderShipValues, 'GP_PL_End_Date__c', objProjectLeaderShip, 'GP_Pinnacle_End_Date__c');

        //objProjectLeaderShip.GP_Start_Date__c = objUpdatedProjectLeaderShipValues.GP_PL_Start_Date__c;
        setUpdatedValueDependingOnColumnSelected(jobRecord.GP_Column_s_To_Update_Upload__c, objUpdatedProjectLeaderShipValues, 'GP_PL_Start_Date__c', objProjectLeaderShip, 'GP_Start_Date__c');

    }

    /**
     * @description Set Project Leadership Records from Map Of Project Leadership(member of parent class).
     * 
     */
    private void setLstOfProjectLeadership() {
        lstOfProjectLeadership = (List < GP_Project_Leadership__c > ) convertlistOfListToAList(mapOfProjectLeadership.Values());
    }

    /**
     * @description Check for leadership records for the associated project in pinnacle system that needs to be updated from Mass Update.
     * 
     */
    private void checkForNonExistantRecordsToBeUpated() {
        Boolean isError = false;
        Id tempId;
        
        try {
            for (String projectOracleId: mapOfProjectOracleIdVsUniqueKeyOfChild.KeySet()) {
                if (mapOfProjectOracleIdVsUniqueKeyOfChild.get(projectOracleId).size() > 0) {
                    for (String uniqueKey: mapOfProjectOracleIdVsUniqueKeyOfChild.get(projectOracleId)) {
                        if (!mapOfTemporaryDataWithUniqueKey.containsKey(uniqueKey))
                            break;
                        
                        tempId = mapOfTemporaryDataWithUniqueKey.get(uniqueKey).Id;
                        isError = true;
                        
                        if (mapOfInvalidEntries.containsKey(tempId)) {
                            mapOfInvalidEntries.get(tempId).GP_Validation_Result__c = PROJECT_LEADERSHIP_NOT_FOUND_ERROR_LABEL;
                            mapOfInvalidEntries.get(tempId).GP_Is_Failed__c = true;
                        } else {
                            GP_Temporary_Data__c objTemporaryData = mapOfTemporaryData.get(tempId);
                            objTemporaryData.GP_Validation_Result__c = PROJECT_LEADERSHIP_NOT_FOUND_ERROR_LABEL;
                            objTemporaryData.GP_Is_Failed__c = true;
                            mapOfInvalidEntries.put(tempId, objTemporaryData);
                        }
                    }
                }
            }
            
            if (isError) {
                throw new GPServiceProjectClone.GPServiceProjectCloneException(PROJECT_LEADERSHIP_NOT_FOUND_ERROR_LABEL, tempId);
            }
        } catch(Exception ex) {
			System.debug('==Exception at 377=='+ex.getMessage());
        }
    }
}