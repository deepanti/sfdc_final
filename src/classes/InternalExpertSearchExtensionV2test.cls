@istest
public class InternalExpertSearchExtensionV2test
{
    public static testMethod void testController()
    {
 Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
       Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
         AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
   oContact.title='Mr.';
   
   update oContact;
Expert__c expertobject=new Expert__c(Name='Test',Expert_Type__c='Internal',Industrial_Vertical__c='BFS',Product_Family__c='IT Services',Service_Line__c='IT Services',
Geo_served_Internal__c='Asia',Sub_Industry_Vertical__c='BFS',Product__c='Platforms', Title__c='Consultant',Department__c='Technology',Office_Location__c='Gurgaon');
        insert expertobject;
//PageReference ref= new PageReference('/apex/ExpertSerachPage’);

// start the test execution context
        Test.startTest();

        // set the test's page to your VF page (or pass in a PageReference)
        PageReference tpageRef = Page.InternalExpertSerachPageV2;

// call the constructor
 ApexPages.StandardController std = new ApexPages.StandardController(expertobject);
        InternalExpertSearchExtensionV2 controller = new InternalExpertSearchExtensionV2(std);
      //  tpageRef.getParameters().put('expid', String.valueOf(expertobject.Id));
      //  tpageRef.getParameters().put('IV', 'BFS');
      //  tpageRef.getParameters().put('PF', 'IT Services');
    Test.setCurrentPage(tpageRef);
          // test action methods on your controller and verify the output with assertions
        controller.expertname ='Manoj Pandey';
        controller.industryverticalvar = '[BFS,CPG]';
        controller.prodfamilyvar = '[IT Services,F & A]';
        controller.serlinevar ='[IT Services, Core F & A]';
        controller.experttypevar='Internal';
        controller.productvar = '[Platforms,test]';
        controller.geoservedvar= '[North America,Asia]';
        controller.subindustryverticalvar= 'BFS';
        controller.Geo='Asia';
        controller.SIV='BFS';
        controller.exptype='Internal';
        controller.expname='Manoj Pandey';
        controller.Officelocvar='Gurgaon';
        controller.Deptvar='Technology';
        controller.Jobtitlevar='Consultant';
        
       
        
        controller.toggleSort();
        controller.reset();
        controller.runSearch();
        String soql=controller.debugSoql;

      
List<selectOption> serviceLine= controller.serviceLine;
List<selectOption> industryvertical = controller.industryvertical ;
List<selectOption> prodfamily= controller.prodfamily;
List<selectOption> expertType= controller.expertType;
List<selectOption> product= controller.product;
List<selectOption> geoserved= controller.geoserved;
List<selectOption> subindustryvertical= controller.subindustryvertical;
List<selectOption> DepartmentVal= controller.DepartmentVal;
List<selectOption> OfficeLocaVal= controller.OfficeLocaVal;
List<selectOption> JobTitleVal= controller.JobTitleVal;

    
    //Test.stopTest();                  
        
            

       PageReference result = controller.runSearch();
    System.assertEquals(null, result);
      // controller.runSearch();
      
        // stop the test
        Test.stopTest();
    }
}