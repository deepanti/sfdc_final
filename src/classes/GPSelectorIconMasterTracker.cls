@isTest
public class GPSelectorIconMasterTracker {
    
    @isTest
    static void testIconMasterSelector() {
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GPSelectorIconMaster iconMasterSelector = new GPSelectorIconMaster();
        iconMasterSelector.selectById(new Set<Id> {iconMaster.Id});
        iconMasterSelector.selectICONRecord(iconMaster.Id);
        iconMasterSelector.getIconMasterDetail(null);
    }
}