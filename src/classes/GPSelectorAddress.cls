public class GPSelectorAddress extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Address__c.Name,
            GP_Address__c.Id, 
            GP_Address__c.GP_Billing_Entity__c,
            GP_Address__c.GP_Customer__c,
            GP_Address__c.GP_Address_Line_1__c,
            GP_Address__c.GP_Address_Line_2__c,
            GP_Address__c.GP_Address_Line_3__c,
            GP_Address__c.GP_Address_Line_4__c,
            GP_Address__c.GP_City__c,
            GP_Address__c.GP_State__c,
            GP_Address__c.GP_Province__c,
            GP_Address__c.GP_Postal_Code__c,
            GP_Address__c.GP_Country__c
        };
    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Address__c.sObjectType;
    }
    
    public List<GP_Address__c> selectById(Set<ID> idSet) {
        return (List<GP_Address__c>) selectSObjectsById(idSet);
    }
    
    public GP_Address__c selectById(ID addressId) {
        Set<Id> idSet = new Set<Id> {addressId};
        return (GP_Address__c) selectSObjectsById(idSet).get(0);
    }
    
    public static List<GP_Address__c> getAddress(List<Id> listOfCustomerID, Id billingEntityID) {
        return  [SELECT Id, 
                        Name, 
                        GP_Address__c, 
                        GP_Customer__c, 
                        GP_SITE_NUMBER__c,
                        GP_Site_Use_Code__c,
                 		GP_Billing_Entity__c
                    FROM GP_Address__c
                    WHERE ((GP_Customer__c in :listOfCustomerID 
                            AND GP_Billing_Entity__c = :billingEntityID) 
                            OR  GP_Address__c LIKE '%GENPACT Dummy%')
                            AND GP_Status__c = 'Active'
                ];
    }
    
    public static List<GP_Address__c> getAddressBasedOnOracleId(Set<string> setOracleIds) {
        return  [SELECT Id, 
                        Name, GP_ADDRESS_ID__c,
                        GP_Address__c, 
                        GP_Customer__c, 
                        GP_Site_Use_Code__c
                    FROM GP_Address__c
                    WHERE GP_ADDRESS_ID__c in :setOracleIds];
    }
}