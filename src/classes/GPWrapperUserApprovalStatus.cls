public class GPWrapperUserApprovalStatus {
	public Id userId;
    public String status;
    public String remarks;
    public GPWrapperUserApprovalStatus(Id userId,String status,String remarks){
       this.userId = userId; 
        this.status  = status;
        this.remarks = remarks;
    }
}