/*-------------
        Class Description : Class is being used in batch class for quota attainment purposes.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 11 June 2014  
        Last modified date: 20 August 2014
---------------*/
public class CalculateAttainment
{
    List<QuotaLineOpportunityJunction__c> oQLIOppJunctionList {get;set;}        // list to upsert  attainment records
    public CalculateAttainment()
    {   
        oQLIOppJunctionList = new List<QuotaLineOpportunityJunction__c>();    
    }
    
    /*-------------
        Method Description : Takes user id and OLI id,check if that oli qualifies then provide quotaattainment for that user.
    ---------------*/
    public boolean AddQuotaAttainment(String userid,String Oliid,String UserRole,String Quota_Year,String recordtypename)   
    { 
      
     OpportunityProduct__c oliobj=[SELECT Opportunityid__r.accountid,OpportunityId__r.Account.Account_Headquarter_Genpact_Regions__c,OpportunityId__r.Account.Account_Headquarters_Country__c,OpportunityId__r.Sales_Region__c,OpportunityId__r.Sales_Country__c,OpportunityId__r.Account.Industry_Vertical__c,OpportunityId__r.Account.Sub_Industry_Vertical__c,OpportunityId__r.Account.Sales_Unit__c,OpportunityId__r.Account.Sales_Sub_Unit_IT__c,OpportunityId__r.Account.SalesSub_Unit_Vertical__c,OpportunityId__r.Account.SalesSub_Unit_Analytics__c,COE__c,Service_Line_OLI__c,Product_Family_OLI__c,Product__c,Total_TCV__c,Total_ACV__c,Total_CYR__c FROM OpportunityProduct__c where id=:Oliid  LIMIT:(limits.getLimitQueryRows() - limits.getQueryRows())];
    
     system.debug('userid==='+'  '+userid+'  '+Oliid+'  '+UserRole); 
     system.debug('tttttttt='+'  '+oliobj);        
    
    //flushing the attainment corresponds to this oli.
    //Delete [select id from QuotaLineOpportunityJunction__c where Quota_Header__r.Sales_Person__c=:userid AND Products_Revenue__c=:Oliid AND  Quota_Header__r.Year__c=:Quota_Year];   
        
    // fetching quotaline corresponds to Quota-header sorted by account lookup not null 
    for(Set_Quotas__c sqt :[Select id,recordtype.developername,Account_Name__c,Account_Name__r.Client_Partner__c,Account_Name__r.Primary_Account_GRM__c,Identified_UnIdentified__c,Quota_Header__c,Quota_Header__r.Sales_Person__c,Account_HQ_Region__c,Account_HQ_Country__c,Sales_Region__c,Sales_Country__c,Industry_Vertical__c,Sub_industry_Vertical__c,Sales_Unit__c,Sales_Sub_Unit_IT__c,SalesSub_Unit_Vertical__c,SalesSub_Unit_Analytics__c,Delivering_Organisation__c,Service_Line__c,Product_Family__c,Product__c from Set_Quotas__c where Quota_Header__r.Sales_Person__c=:userid AND Quota_Header__r.Year__c=:Quota_Year order by Account_Name__c NULLS LAST])
    {
        system.debug('sqt======'+sqt.id);
        if(sqt.recordtype.developername=='Account_GRM')  // this recordtype has been assigned to non-sales leader
        {
          if(UserRole=='Enterprise GRM' || UserRole=='Opportunity Owner' || UserRole=='Product BD Rep' || UserRole=='Manager')
          {
            if(sqt.Account_Name__c!=null)
            {
                if(String.valueof(oliobj.Opportunityid__r.accountid)==String.valueof(sqt.Account_Name__c) && sqt.Identified_UnIdentified__c==false) // check under identified account           
                {
                    System.debug('Identified_UnIdentified__c==false');
                    QuotaLineOpportunityJunction__c opj = new QuotaLineOpportunityJunction__c();
                    opj.Products_Revenue__c=Oliid;
                    opj.Quota_Header__c=sqt.Quota_Header__c;
                    opj.Quota_Line__c=sqt.id;
                    opj.Source__c=UserRole;
                    opj.ACVtoRollup__c = oliobj.Total_ACV__c;
                    opj.TCVtoRollup__c = oliobj.Total_TCV__c;
                    opj.CYRtoRollup__c = oliobj.Total_CYR__c;
                    oQLIOppJunctionList.add(opj);
                    if(oQLIOppJunctionList.size()>0)     upsert oQLIOppJunctionList; 
                    return false;
                }
            }
            else        
            {
                if(sqt.Identified_UnIdentified__c==true)    // check under unidentified account
                {
                    System.debug('Identified_UnIdentified__c==true');
                    QuotaLineOpportunityJunction__c opj = new QuotaLineOpportunityJunction__c();     
                    opj.Products_Revenue__c=Oliid;
                    opj.Quota_Header__c=sqt.Quota_Header__c;
                    opj.Quota_Line__c=sqt.id;
                    opj.Source__c=UserRole;
                    opj.ACVtoRollup__c = oliobj.Total_ACV__c;
                    opj.TCVtoRollup__c = oliobj.Total_TCV__c;
                    opj.CYRtoRollup__c = oliobj.Total_CYR__c;
                    oQLIOppJunctionList.add(opj);
                    if(oQLIOppJunctionList.size()>0)      upsert oQLIOppJunctionList; 
                    return false;
                }
            }
          }
        }
        else                                    //salesleader layout
        {  
            if(UserRole=='Manager')     // Incase manager is a Sales Leader
            {
                System.debug('UserRole==Manager SL');
                QuotaLineOpportunityJunction__c opj = new QuotaLineOpportunityJunction__c();     
                opj.Products_Revenue__c=Oliid;
                opj.Quota_Header__c=sqt.Quota_Header__c;
                opj.Quota_Line__c=sqt.id;
                opj.Source__c=UserRole;         
                opj.ACVtoRollup__c  = oliobj.Total_ACV__c;
                opj.TCVtoRollup__c  = oliobj.Total_TCV__c;
                opj.CYRtoRollup__c  = oliobj.Total_CYR__c;
                oQLIOppJunctionList.add(opj);
                if(oQLIOppJunctionList.size()>0)      upsert oQLIOppJunctionList; 
                return false;
            } 
            
            Boolean temp_bool1,temp_bool2,temp_bool3,temp_bool4,temp_bool5,temp_bool6,temp_bool7,temp_bool8,temp_bool9,temp_bool10,temp_bool11,temp_bool12,temp_bool13,temp_bool14; // boolean to check if the NOT Null fields of OLI & Quota-line matches.
            temp_bool1=temp_bool2=temp_bool3=temp_bool4=temp_bool5=temp_bool6=temp_bool7=temp_bool8=temp_bool9=temp_bool10=temp_bool11=temp_bool12=temp_bool13=temp_bool14=true;
            
            if(sqt.Account_HQ_Region__c==null || sqt.Account_HQ_Region__c=='')  {}  
            else
            {
                if(oliobj.OpportunityId__r.Account.Account_Headquarter_Genpact_Regions__c==null || oliobj.OpportunityId__r.Account.Account_Headquarter_Genpact_Regions__c=='') temp_bool1=false;
                else if(oliobj.OpportunityId__r.Account.Account_Headquarter_Genpact_Regions__c==sqt.Account_HQ_Region__c)   temp_bool1=true;    
                else   temp_bool1=false;
            }
            
            if(sqt.Account_HQ_Country__c==null || sqt.Account_HQ_Country__c=='') {}
            else
            {
                if(oliobj.OpportunityId__r.Account.Account_Headquarters_Country__c==null || oliobj.OpportunityId__r.Account.Account_Headquarters_Country__c=='') temp_bool2=false;
                else if(oliobj.OpportunityId__r.Account.Account_Headquarters_Country__c==sqt.Account_HQ_Country__c) temp_bool2=true;
                else temp_bool2=false;
            }
            
            
            if(sqt.Sales_Region__c==null || sqt.Sales_Region__c=='')    {} 
            else
            {
                if(oliobj.OpportunityId__r.Sales_Region__c==null || oliobj.OpportunityId__r.Sales_Region__c=='')  
                temp_bool3=false;
                else    if(oliobj.OpportunityId__r.Sales_Region__c==sqt.Sales_Region__c)    temp_bool3=true;
                else    temp_bool3=false;
            }
            
            if(sqt.Sales_country__c==null || sqt.Sales_country__c=='') {} 
            else
            {
                if(oliobj.OpportunityId__r.Sales_Country__c==null || oliobj.OpportunityId__r.Sales_Country__c=='') 
                temp_bool4=false;
                else    if(oliobj.OpportunityId__r.Sales_Country__c==sqt.Sales_country__c)  temp_bool4=true;
                else    temp_bool4=false;
            }
            
            if(sqt.Industry_Vertical__c==null || sqt.Industry_Vertical__c=='')  {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.Industry_Vertical__c==null || oliobj.OpportunityId__r.Account.Industry_Vertical__c=='')  temp_bool5=false;
                else    if(oliobj.OpportunityId__r.Account.Industry_Vertical__c==sqt.Industry_Vertical__c)  temp_bool5=true;
                else    temp_bool5=false;
            }       
            
            if(sqt.Sub_industry_Vertical__c==null || sqt.Sub_industry_Vertical__c=='')  {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.Sub_Industry_Vertical__c==null || oliobj.OpportunityId__r.Account.Sub_Industry_Vertical__c=='')  temp_bool14=false;
                else    if(oliobj.OpportunityId__r.Account.Sub_Industry_Vertical__c==sqt.Sub_industry_Vertical__c)  temp_bool14=true;
                else    temp_bool14=false;
            }      
            
            
            if(sqt.Sales_Unit__c==null)    {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.Sales_Unit__c==null)   temp_bool6=false;
                else    if(oliobj.OpportunityId__r.Account.Sales_Unit__c==sqt.Sales_Unit__c) temp_bool6=true;
                else    temp_bool6=false;
            }   
            
            if(sqt.Sales_Sub_Unit_IT__c==null)    {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.Sales_Sub_Unit_IT__c==null)           temp_bool7=false;
                else    if(oliobj.OpportunityId__r.Account.Sales_Sub_Unit_IT__c==sqt.Sales_Sub_Unit_IT__c) temp_bool7=true;
                else    temp_bool7=false;
            }   
            
            if(sqt.SalesSub_Unit_Vertical__c==null)   {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.SalesSub_Unit_Vertical__c==null)      temp_bool8=false;
                else if(oliobj.OpportunityId__r.Account.SalesSub_Unit_Vertical__c==sqt.SalesSub_Unit_Vertical__c) temp_bool8=true;
                else temp_bool8=false;
            }
            
            if(sqt.SalesSub_Unit_Analytics__c==null)    {} 
            else
            {
                if(oliobj.OpportunityId__r.Account.SalesSub_Unit_Analytics__c==null)    temp_bool9=false;
                else if(oliobj.OpportunityId__r.Account.SalesSub_Unit_Analytics__c==sqt.SalesSub_Unit_Analytics__c) 
                temp_bool9=true;
                else     temp_bool9=false;
            }
            
            if(sqt.Delivering_Organisation__c==null || sqt.Delivering_Organisation__c=='')  {} 
            else
            {
                if(oliobj.COE__c==null || oliobj.COE__c=='')            temp_bool10=false;
                else if(oliobj.COE__c==sqt.Delivering_Organisation__c)  temp_bool10=true;
                else                                                    temp_bool10=false;
            }
            
            if(sqt.Service_Line__c==null || sqt.Service_Line__c=='') {} 
            else
            {
                if(oliobj.Service_Line_OLI__c==null || oliobj.Service_Line_OLI__c=='') temp_bool11=false;
                else if(oliobj.Service_Line_OLI__c==sqt.Service_Line__c)    temp_bool11=true;
                else                                                        temp_bool11=false;
            }
            
            if(sqt.Product_Family__c==null || sqt.Product_Family__c=='')    {} 
            else 
            {
                if(oliobj.Product_Family_OLI__c==null || oliobj.Product_Family_OLI__c=='') temp_bool12=false;
                else if(oliobj.Product_Family_OLI__c==sqt.Product_Family__c)    temp_bool12=true;   
                else                                                 temp_bool12=false;
            }
            
            if(sqt.Product__c==null) {} 
            else
            {
                if(oliobj.Product__c==null) temp_bool13=false;
                else if(oliobj.Product__c==sqt.Product__c)     temp_bool13=true;
                else                                           temp_bool13=false;
            }
            
            System.debug('qstring===='+temp_bool1 +' 1 '+ temp_bool2 +' 2 '+ temp_bool3 +' 3 '+ temp_bool4 +' 4 '+ temp_bool5 +' 5 '+ temp_bool6 +' 7 '+ temp_bool7 +' 8 '+ temp_bool8 +' 8 '+ temp_bool9 +' 9 '+ temp_bool10 +' 10 '+ temp_bool11 +' 11 '+ temp_bool12 +' 12 '+ temp_bool13+' 13 '+temp_bool14);            
           
             if(temp_bool1 && temp_bool2 && temp_bool3 && temp_bool4 && temp_bool5 && temp_bool6 && temp_bool7 && temp_bool8 && temp_bool9 && temp_bool10 && temp_bool11 && temp_bool12 && temp_bool13 && temp_bool14)                         // Not Null Fields matches with each other
             {
                // providing attainment for Salesleader
                System.debug('salesleader layout');
                QuotaLineOpportunityJunction__c opj = new QuotaLineOpportunityJunction__c();     
                opj.Products_Revenue__c=oliobj.id;
                opj.Quota_Header__c=sqt.Quota_Header__c;
                opj.Quota_Line__c=sqt.id;
                opj.Source__c='Sales Leader';
                opj.ACVtoRollup__c = oliobj.Total_ACV__c;
                opj.TCVtoRollup__c = oliobj.Total_TCV__c;
                opj.CYRtoRollup__c = oliobj.Total_CYR__c;
                oQLIOppJunctionList.add(opj);
                if(oQLIOppJunctionList.size()>0)   upsert oQLIOppJunctionList; 
                return false;                   
             }
        }           
    }
       return false;
    }  
    
    /*Method to get list of manager's id of a user upto 7 level*/   
    public Set<String> getusermanagerlist(String uid)
    {
        Set<String> ids=new Set<String>();
        system.debug('uid==='+uid);
        ids.add(uid);
        for(user uid1:[SELECT id,ManagerId FROM User where id=:uid])
        {
            if(uid1.ManagerId==null) {} else
            {
                ids.add(uid1.ManagerId);
                for(user uid2:[SELECT id,ManagerId FROM User where id=:uid1.ManagerId])
                {
                    if(uid2.ManagerId==null) {} else
                    {
                        ids.add(uid2.ManagerId);
                        for(user uid3:[SELECT id,ManagerId FROM User where id=:uid2.ManagerId])
                        {
                            if(uid3.ManagerId==null) {} else
                            {
                                ids.add(uid3.ManagerId);                 
                                for(user uid4:[SELECT id,ManagerId FROM User where id=:uid3.ManagerId])
                                {
                                    if(uid4.ManagerId==null) {} else
                                    {
                                        ids.add(uid4.ManagerId);
                                        for(user uid5:[SELECT id,ManagerId FROM User where id=:uid4.ManagerId])
                                        {
                                            if(uid5.ManagerId==null) {} else
                                            {         
                                                ids.add(uid5.ManagerId);
                                                for(user uid6:[SELECT id,ManagerId FROM User where id=:uid5.ManagerId])
                                                {
                                                    if(uid6.ManagerId==null) {} else
                                                    {
                                                        ids.add(uid6.ManagerId);
                                                        for(user uid7:[SELECT id,ManagerId FROM User where id=:uid6.ManagerId])
                                                        {
                                                            if(uid7.ManagerId==null) {} else
                                                                ids.add(uid7.ManagerId);                                                                
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        system.debug('ids=='+'--'+ids);
        return ids;
    }       
}