/**
* @group ServiceLayer. 
*
* @description Service layer for Document Upload.
*/
public  class GPServiceDocumentUpload {
    
    private final String PO_ERROR_MSG = 'Please uplaod PO document.' ;
    private list<GP_Project__c> lstProject ;
    private set<id> setProjectId ; 
    private map<string,List<string>> mapProjectIdtoListError;
    
    public GPServiceDocumentUpload(set<id> setProjectId){
        this.setProjectId = setProjectId;
    }
    
    public GPServiceDocumentUpload(list<GP_Project__c> lstProjects){
        this.lstProject = lstProjects;
    }
    
    public map<string,List<string>> validatePODocumentUpload(){
        setProject();
        getErrors();
        
        return mapProjectIdtoListError;
    }
    
    private void getErrors() {
        mapProjectIdtoListError = new map< string,List<string>>();
        for(GP_Project__c objProject :  lstProject) {
            if(objProject.GP_Is_PO_Required__c  && objProject.GP_Project_Documents__r.size() == 0) {
                setErrors(objProject,PO_ERROR_MSG );
            }
        }
    }
    
    private void setErrors(GP_Project__c objProject , string strError) {
        if(mapProjectIdtoListError.get(objProject.id) == null) {
            mapProjectIdtoListError.put(objProject.id, new list<String>());
        }
        mapProjectIdtoListError.get(objProject.id).add(strError );
    }
    
    private void setProject() {
        if(setProjectId != null && setProjectId.size() > 0) {
            lstProject = fetchProjectDetils();
        } else {
            setProjectId = new set<id>();
            for(GP_Project__c objProject: lstProject) {
                setProjectId.add( objProject.id);
            }
            lstProject = fetchProjectDetils();
        }        
    }
    
    private list<GP_Project__c> fetchProjectDetils() {
        if(setProjectId != null && setProjectId.size() > 0) {
            return  new GPSelectorProject().getProjectRecordsWithPODocument(setProjectId);
        } else {
            return null;
        }
    }
}