/*
    @Description: Class will be used for handling all NON IT SERVICE, Opportunity Line Item
    @Test Class: OLINonITServiceCtlrTest
    @Param: List<OpportunityLineItem>, Map<Id,User_VIC_Role__c> (It will conatain all triggered(OLI)'s ownerid, primary bd sales rep)
    @Author: Vikas Rajput
*/
public class VIC_OLINonITServiceCtlr{
    String strBD = 'BD';
    String strGRM = 'GRM';
    String strEnterpriseSales = 'Enterprise sales';
    String strCM = 'CM';
    public void serviceManagerForNonIT(List<OpportunityLineItem> lstNONServiceOpportunityLineItemARG, Map<Id,User_VIC_Role__c> mapUserIdTOUserVICRoleARG, Map<String, VIC_Split_Percentage__mdt> mapKeyToSplitPercentARG){
        //Map<Id,User_VIC_Role__c> mapUserIdTOUserVICRoleLCL = new VIC_UserPlanComponentCtlr().fetchMasterVicRoleBySetUserId(UserInfo.getUSerID());
        //@IterationLogic: 
        for(OpportunityLineItem objOLIITR : lstNONServiceOpportunityLineItemARG){
            //Logic for BD Rep
            if(String.isNOTBlank(objOLIITR.Product_BD_Rep__c)){                
                String key = 'NonITServicesProductBDRepEligible';
                objOLIITR.vic_Product_BD_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
            }
            
            //Logic for Primary Rep
            if(mapUserIdTOUserVICRoleARG.containsKey(objOLIITR.vic_Owner__c) && 
                  (mapUserIdTOUserVICRoleARG.get(objOLIITR.vic_Owner__c).Master_VIC_Role__r.Role__c == strBD || 
                      mapUserIdTOUserVICRoleARG.get(objOLIITR.vic_Owner__c).Master_VIC_Role__r.Role__c == strGRM ) &&  
                         (mapUserIdTOUserVICRoleARG.get(objOLIITR.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strEnterpriseSales || 
                             mapUserIdTOUserVICRoleARG.get(objOLIITR.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strCM)){ //rlole != BD OR GRM && subrole != Enterprise, Result == Yes
                
                String key = 'NonITServicesForRoleBDEORGRM';
                objOLIITR.vic_Primary_Sales_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
            }else{                
                String key = 'NonITServicesForRoleNotBDEORGRM';                
                objOLIITR.vic_Primary_Sales_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;                
            }
            if(objOLIITR.vic_Final_Data_Received_From_CPQ__c){
                objOLIITR.vic_Sales_Rep_Approval_Status__c = objOLIITR.vic_Primary_Sales_Rep_Split__c > 0 ? 'Pending' : 'Not Required';
                objOLIITR.vic_Product_BD_Rep_Approval_Status__c = objOLIITR.vic_Product_BD_Rep_Split__c > 0 ? 'Pending' : 'Not Required';
            }
        }
    }
}