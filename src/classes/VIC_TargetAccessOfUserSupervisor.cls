public class VIC_TargetAccessOfUserSupervisor {
    
    
    
    public static void updateCurrentUserSupervisiorAccessOnTargets(Map<Id,Id> userSuperVisorMap,Set<Id> oldSupervisorIds){
            
        try{
            if(userSuperVisorMap != null && userSuperVisorMap.size() > 0 && userSuperVisorMap.values() != null && 
                userSuperVisorMap.values().size() > 0){
                
                List<Target__c> userTargets = new List<Target__c>();
                List<Target_Share__c> targetShareList = new List<Target_Share__c>();
                Date todaysDate = Date.today();
                Integer thisYear  = todaysDate.year();
                
                List<Target__c> usersTargets = new List<Target__c>();
                userTargets = [Select Id,name,Start_date__c,Is_Active__c,User__c from Target__c
                                                where User__c IN:userSuperVisorMap.keyset() and
                                                CALENDAR_YEAR(Start_date__c) =: thisYear and
                                                Is_Active__c = true];
                
                System.debug('\n\n ---------->>> userTargets '+userTargets);
                                                
                if(userTargets != null && userTargets.size() > 0){
                
                    
                    for(Target__c t:userTargets){
                    
                        Target_Share__c ts = new Target_Share__c();
                        ts.ParentId__c = t.Id;
                        ts.UserOrGroupId__c  = userSuperVisorMap.get(t.User__c);
                        ts.AccessLevel__c = 'Edit';
                        targetShareList.add(ts);
                        
                    }
                    
                    System.debug('\n\n ---------->>> oldSupervisorIds '+oldSupervisorIds);
                    if(oldSupervisorIds != null && oldSupervisorIds.size() > 0){
                        
                        List<Target_Share__c> existingOldTargetShare = new List<Target_Share__c>();
                        existingOldTargetShare = [Select Id,ParentId__c,UserOrGroupId__c from Target_Share__c where UserOrGroupId__c IN:oldSupervisorIds];
                        if(existingOldTargetShare != null && existingOldTargetShare.size() > 0){
                            delete existingOldTargetShare;
                        }
                    }
                    
                    System.debug('\n\n ---------->>> targetShareList '+targetShareList);
                    if(targetShareList != null && targetShareList.size() > 0){
                        insert targetShareList;
                    }
                    
                    
                }
            }
        }
        catch(Exception e){
            system.debug('\n\n Exception =='+e.getMessage()+'=='+e.getLineNumber());   
            CreateErrorLog.createErrorRecord(null,e.getMessage(), '',e.getStackTraceString(),'VIC_TargetAccessForSupervisor','updateCurrentUserSupervisiorAccessOnTargets','Fail','',String.valueOf(e.getLineNumber())); 
   
        }
        
        
    }
    
    public static void deleteAccessForOldSupervisors(Set<Id> oldSupervisorIds){
        List<Target_Share__c> existingOldTargetShare = new List<Target_Share__c>();
        
        try{
            if(oldSupervisorIds != null && oldSupervisorIds.size() > 0){
                existingOldTargetShare = [Select Id,ParentId__c,UserOrGroupId__c from Target_Share__c where UserOrGroupId__c IN:oldSupervisorIds];
                if(existingOldTargetShare != null && existingOldTargetShare.size() > 0){
                    delete existingOldTargetShare;
                }
            }
        }catch(Exception e){
            system.debug('\n\n Exception =='+e.getMessage()+'=='+e.getLineNumber());   
            CreateErrorLog.createErrorRecord(null,e.getMessage(), '',e.getStackTraceString(),'VIC_TargetAccessForSupervisor','deleteAccessForOldSupervisors','Fail','',String.valueOf(e.getLineNumber())); 
   
        }

    }
}