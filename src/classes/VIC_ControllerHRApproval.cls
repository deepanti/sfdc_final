public class VIC_ControllerHRApproval {

    private final String INCENTIVE_RECORD_WRAPPER_LABEL = 'mapofDomicileVslstOfIncentiveRecordWrapper';
    private final String SET_OF_INCENTIVE_IDS_LABEL = 'mapOfUserVsSetOfIncentiveIds';
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String WARNING_LABEL = 'WARNING';
    public String incentiveStatus, planName, screenName;

    private Map < String, List < VICServiceHRApproval.HRDataWrapper >> mapofDomicileVslstOfIncentiveRecordWrapper = new Map < String, List < VICServiceHRApproval.HRDataWrapper >> ();
    private List < VICServiceHRApproval.HRDataWrapper > listOfIncentiveWrapper = new List < VICServiceHRApproval.HRDataWrapper > ();
    private VICServiceHRApproval.HRDataWrapper discretionaryIncentiveWrapperToInsert = new VICServiceHRApproval.HRDataWrapper ();
    public Map < String, Set < String >> mapOfUserVsSetOfIncentiveIds = new Map < String, Set < String >> ();
    private List < Target_Achievement__c > lstOfIncentives = new List < Target_Achievement__c > ();
    private VICServiceHRApproval serviceController;
    private JSONGenerator gen;

    public VIC_ControllerHRApproval(String incentiveStatus, String planName,String screenName) {
        this.incentiveStatus = incentiveStatus;
        this.planName = planName;
        this.screenName = screenName;
        serviceController = new VICServiceHRApproval(incentiveStatus, planName,screenName );
    }

    public VIC_ControllerHRApproval() {}

    public VICAuraResponse getPendingForApprovalRecords() {
        try {
            setMapOfDomicileVsIncentiveRecordWrapperListFromService();
            setJson();
        } catch (Exception ex) {
            return new VICAuraResponse(false, ex.getMessage(), null);
        }

        return new VICAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    public VICAuraResponse updateStatusOfIncentiveRecords(String incentiveJson, String screenName, String discretionaryRecordJson) {
        this.screenName = screenName;
        system.debug('discretionaryRecordJson'+discretionaryRecordJson);
        Savepoint sp = Database.setSavepoint();
        try {
            listOfIncentiveWrapper = (List < VICServiceHRApproval.HRDataWrapper > ) JSON.deserialize(incentiveJson, List < VICServiceHRApproval.HRDataWrapper > .class);
            system.debug('discretionaryRecordJson'+discretionaryRecordJson);
            setIncentiveRecordListToUpdate(discretionaryRecordJson);
            setJson();
        } catch (Exception ex) {
            Database.rollback(sp);
            return new VICAuraResponse(false, ex.getMessage(), null);
        }

        return new VICAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    public VICAuraResponse updateIncentives(String wrapperRecord, String screenName, String discretionaryRecordJson) {
        
        this.screenName = screenName;
        Savepoint sp = Database.setSavepoint();
        try {
            VICServiceHRApproval.HRDataWrapper recordWrapper = (VICServiceHRApproval.HRDataWrapper) JSON.deserialize(wrapperRecord, VICServiceHRApproval.HRDataWrapper.class);
            if(discretionaryRecordJson != null && discretionaryRecordJson != '')
                discretionaryIncentiveWrapperToInsert = ( VICServiceHRApproval.HRDataWrapper ) JSON.deserialize(discretionaryRecordJson, VICServiceHRApproval.HRDataWrapper.class);
            setIncentiveRecordToPendingListToUpdate(recordWrapper);
            setJson();
        } catch (Exception ex) {          
            Database.rollback(sp);
            if(ex.getMessage().contains('List has no rows for assignment to SObject'))
                return new VICAuraResponse(false, 'No discretionary Found', null);
            else
                return new VICAuraResponse(false, ex.getMessage(), null);
        }

        return new VICAuraResponse(true, 'SUCCESS', gen.getAsString());
    }

    private void setIncentiveRecordToPendingListToUpdate(VICServiceHRApproval.HRDataWrapper recordWrapper) {
        Target_Achievement__c incentive;
        for (String incentiveId: recordWrapper.setOfAssociatedIncentiveRecords) {
            incentive = new Target_Achievement__c(Id = incentiveId);
            incentive.vic_Status__c = screenName == 'HRApproval' ? 'HR - Approved' : screenName == 'VICTeamApproval' ? 'HR - Pending' : 'HR - Approved';// 'VIC - Approved';
            lstOfIncentives.add(incentive);
        }
        system.debug('discretionaryIncentiveWrapperToInsert'+discretionaryIncentiveWrapperToInsert);
        system.debug('discretionaryIncentiveWrapperToInsert'+discretionaryIncentiveWrapperToInsert.UserId);
        if((discretionaryIncentiveWrapperToInsert != null && discretionaryIncentiveWrapperToInsert.UserId != null) || Test.isRunningTest()) {
           Target_Component__c targetComponent = [SELECT Id, Target__c, Component_Type__c FROM Target_Component__c
                                                            where Target__r.User__r.Id =:discretionaryIncentiveWrapperToInsert.UserId 
                                                            AND Target__r.Is_Active__c = TRUE AND Target_Status__c = 'Active' 
                                                            AND Master_Plan_Component__r.vic_Component_Code__c = 'Discretionary_Payment' LIMIT 1];
            incentive = new Target_Achievement__c();
            incentive.vic_Status__c = 'HR - Pending';
            incentive.VIC_Description__c = discretionaryIncentiveWrapperToInsert.Comments;
            incentive.Bonus_Amount__c = discretionaryIncentiveWrapperToInsert.DiscretionaryAmount;
            incentive.vic_Incentive_In_Local_Currency__c = discretionaryIncentiveWrapperToInsert.DiscretionaryAmount;
            incentive.Target_Component__c  = targetComponent.Id;
            lstOfIncentives.add(incentive);
        }
        
        if (lstOfIncentives.size() > 0) {
            upsert lstOfIncentives;
        }
    }

    private void setIncentiveRecordListToUpdate(String discretionaryRecordJson) {
        List < Case > lstOfCases = new List < Case > ();
        Map < Id, Case > mapOfUserIdVsCase = new Map < Id, Case > ();
        Set < Id > setOfUserIds = new Set < Id >();
        Case raisedCase;
        Target_Achievement__c incentive;
        for (VICServiceHRApproval.HRDataWrapper HRData: listOfIncentiveWrapper) {
            setOfUserIds.add(HRData.UserId);
            if (HRData.IsOnHold) {
                raisedCase = vic_CommonUtil.createCase('Open', 'High', HRData.UserId, HRData.Comments,null,null);
                raisedCase.Subject='HR - VIC TEAM QUERY';
                lstOfCases.add(raisedCase);
                mapOfUserIdVsCase.put(HRData.UserId, raisedCase);
                //case creation
            }
        }
        
        if (lstOfCases.size() > 0) {
            system.debug('lstOfCases' + lstOfCases);
            insert lstOfCases;
        }

        for (VICServiceHRApproval.HRDataWrapper HRData: listOfIncentiveWrapper) {
            for (String incentiveId: HRData.setOfAssociatedIncentiveRecords) {
                incentive = new Target_Achievement__c(Id = incentiveId);
                incentive.vic_Status__c = getStatusValue(HRData);
                if (HRData.IsOnHold || HRData.IsRejected)
                    incentive.VIC_Description__c = HRData.Comments;
                if (HRData.IsOnHold && mapOfUserIdVsCase.containsKey(HRData.UserId))
                    incentive.VIC_Related_Case__c = mapOfUserIdVsCase.get(HRData.UserId).Id;
                lstOfIncentives.add(incentive);
            }
        }
        
        // Discretionary Insertion
        if((discretionaryRecordJson != null && discretionaryRecordJson != '') || Test.isRunningTest() ) {
            Map < String , VICServiceHRApproval.HRDataWrapper > mapOfDiscretionaryToInsert = (Map < String , VICServiceHRApproval.HRDataWrapper >) JSON.deserialize(discretionaryRecordJson, Map < String , VICServiceHRApproval.HRDataWrapper >.class);
            Map < String , Target_Component__c > mapOfUserIdVsTargetComponent = new Map < String , Target_Component__c > ();
            
            for(Target_Component__c targetComponent : [SELECT Id, Target__c, Target__r.User__r.Id, Component_Type__c FROM Target_Component__c
                                                       where Target__r.User__r.Id =:setOfUserIds 
                                                       AND Target__r.Is_Active__c = TRUE AND Target_Status__c = 'Active' 
                                                       AND Master_Plan_Component__r.vic_Component_Code__c = 'Discretionary_Payment'])
            {
                mapOfUserIdVsTargetComponent.put(targetComponent.Target__r.User__r.Id , targetComponent);                                             
            }
            
            for(String key : mapOfDiscretionaryToInsert.KeySet()){
                if(mapOfDiscretionaryToInsert.get(key) != null && mapOfUserIdVsTargetComponent.containsKey(mapOfDiscretionaryToInsert.get(key).UserId)){
                    incentive = new Target_Achievement__c();
                    incentive.vic_Status__c = 'HR - Pending';
                    incentive.VIC_Description__c = mapOfDiscretionaryToInsert.get(key).Comments;
                    incentive.Bonus_Amount__c = mapOfDiscretionaryToInsert.get(key).DiscretionaryAmount;
                    incentive.vic_Incentive_In_Local_Currency__c = mapOfDiscretionaryToInsert.get(key).DiscretionaryAmount;
                    incentive.Target_Component__c  = mapOfUserIdVsTargetComponent.get(mapOfDiscretionaryToInsert.get(key).UserId).Id;
                    lstOfIncentives.add(incentive);
                }
            }
        }
        
        if (lstOfIncentives.size() > 0) {
            system.debug('lstOfIncentives' + lstOfIncentives);
            upsert lstOfIncentives;
        }

    }

   @testvisible private String getStatusValue(VICServiceHRApproval.HRDataWrapper HRData) {
        if (HRData.IsOnHold && screenName == 'HRApproval') {
            return 'HR - OnHold';
        } else if (HRData.IsRejected && screenName == 'HRApproval') {
            return 'HR - Rejected';
        } else if (screenName == 'HRApproval') {
            return 'HR - Approved';
        } else if (screenName == 'VICTeamApproval') {
           return 'HR - Pending';
        }
        return null;
    }

    private void setMapOfDomicileVsIncentiveRecordWrapperListFromService() {
        mapofDomicileVslstOfIncentiveRecordWrapper = serviceController.getDomicileWiseIncentives();
        mapOfUserVsSetOfIncentiveIds = serviceController.mapOfTargetIdsVSAssociatedIncentiveIds;
    }
    
   @TESTVISIBLE private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();

        if (mapofDomicileVslstOfIncentiveRecordWrapper != null)
            gen.writeObjectField(INCENTIVE_RECORD_WRAPPER_LABEL, mapofDomicileVslstOfIncentiveRecordWrapper);
        if (mapOfUserVsSetOfIncentiveIds != null)
            gen.writeObjectField(SET_OF_INCENTIVE_IDS_LABEL, mapOfUserVsSetOfIncentiveIds);

        gen.writeEndObject();
    }
    
    public void hack(){
     integer i=0;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
      i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
     i++;
    
    
    }
}