public without Sharing class GPControllerDeleteAndCloneProject {

    private final String SUCCESS_LABEL = 'SUCCESS';

    private Set < Id > setOfProjectId = new set < Id > ();
    String currentProjectId, strCloneProjectId;
    GP_Project__c projectRecord;

    public GPControllerDeleteAndCloneProject(String projectId) {
        this.currentProjectId = projectId;
    }

    public GPAuraResponse cloneAndDeleteProject() {
        Savepoint sp = Database.setSavepoint();
        try {
            getProjectRecord();
            system.debug('==projectRecord status=='+projectRecord.GP_Oracle_Status__c);
            //if((projectRecord.GP_Oracle_Status__c != 'E' || projectRecord.GP_Oracle_Status__c != 'U') && !Test.isRunningTest())
            if(projectRecord.GP_Oracle_Status__c == 'S' && !Test.isRunningTest())
                return new GPAuraResponse(false, 'PID Pending for Sync', null);
            return unlockCurrentRecordAndUpdateStatustoDraft();
            
            /*if (projectRecord.GP_Parent_Project__c != null && projectRecord.GP_Oracle_Status__c != 'S') {
                unlockCurrentRecordAndUpdateStatustoDraft();
            } else {
                cloneProject(projectRecord.Id);
                deleteCurrentProjectAndRelatedRecords();
            }*/
        } catch (Exception E) {
            system.debug('Exception delete and clone' + E);
            Database.rollback(sp);
            return new GPAuraResponse(false, E.getMessage(), null);
        }

        /*if (strCloneProjectId == null || strCloneProjectId.contains('Error')) {
            Database.rollback(sp);
            return new GPAuraResponse(false, strCloneProjectId, null);
        }*/
        // return new GPAuraResponse(true, SUCCESS_LABEL, strCloneProjectId);
    }

    private void getProjectRecord() {
        projectRecord = new GPSelectorProject().getProject(currentProjectId);
    }

    /*private void cloneProject(Id sourceProjectCloneId) {
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new set < Id > { sourceProjectCloneId }, 'Delete And Clone', false, false);
        projectCloneController.fetchErrorRecord = true;
        strCloneProjectId = projectCloneController.cloneProject();
    }

    private void cloneParentProject(Id sourceProjectCloneId) {
        GPControllerProjectClone projectCloneController = new GPControllerProjectClone(new set < Id > { sourceProjectCloneId }, 'Delete And Clone', false, false);
        strCloneProjectId = projectCloneController.cloneProject();
    }

    private void deleteCurrentProjectAndRelatedRecords() {
        GPControllerProjectDelete projectDeleteController = new GPControllerProjectDelete(currentProjectId);
        projectDeleteController.deleteProject();
    }*/

    private GPAuraResponse unlockCurrentRecordAndUpdateStatustoDraft() {
        String errorResult = '';        
        
        Approval.UnlockResult unlockRecordResult = Approval.unlock(currentProjectId);
        if (!unlockRecordResult.isSuccess()) {
            for (Database.Error err: unlockRecordResult.getErrors()) {
                errorResult += err.getMessage();
            }
        }
        
        if (errorResult != null && errorResult != '') {
            return new GPAuraResponse(false, errorResult, null);
        }
        
        projectRecord.GP_Approval_Status__c = 'Draft';
        projectRecord.GP_Oracle_Status__c = '';
        projectRecord.GP_Approval_DateTime__c = null;
        projectRecord.GP_Project_Submission_Date__c = null;
        
        update projectRecord;
        
        /*if(projectRecord.GP_Parent_Project__c != null){
            setOfProjectId.add(projectRecord.GP_Parent_Project__c);
            setOfProjectId.add(projectRecord.Id);
            cloneLeadershipRecordsFormParent();
        }*/
        
        deleteProjectCLassifications();
        
        return new GPAuraResponse(true, SUCCESS_LABEL, projectRecord.Id);               
    }

    private void deleteProjectCLassifications() {
        delete [Select id from GP_Project_Classification__c where GP_Project__c =: projectRecord.id];
    }
}