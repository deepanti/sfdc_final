public class GPDomainIconMaster extends fflib_SObjectDomain {

    public GPDomainIconMaster(List < GP_Icon_Master__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainIconMaster(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Icon_Master__c.SObjectType
    };

    public override void onBeforeInsert() {}

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);

        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {}

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateProjectfromIConMaster(uow, oldSObjectMap);
        uow.commitWork();
    }
    public override void onValidate() {
        // Validate GP_Icon_Master__c 
        //for (GP_Icon_Master__c ObjProject: (List < GP_Icon_Master__c > ) Records) {}
    }

    public override void onApplyDefaults() {

        // Apply defaults to GP_Icon_Master__c 
        //for(GP_Icon_Master__c  ObjProject : (List<GP_Icon_Master__c >) Records) { 
        //}               
    }

    public void updateProjectfromIConMaster(fflib_SObjectUnitOfWork uow, Map < Id, SOBject > oldSObjectMap) {

        Map < Id, GP_Icon_Master__c > mapofIconIdToIconMaster = new Map < Id, GP_Icon_Master__c > ();
        Map < ID, GP_Project__c > mapofIdToobjProject = new Map < Id, GP_Project__c > ();
        list < GP_Project__c > lstofProject = new list < GP_Project__c > ();
        set < id > setofApprovedProjctID = new set < id > ();
        set < id > setofDraftProjctID = new set < id > ();
        Set < String > fieldSet = new Set < String > ();
        GP_Icon_Master__c oldIConMaster;

        //dynamically get the fields from the field set and then use the same for comparison in the trigger. 
        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Icon_Master__c.fieldSets.getMap().get('GP_Update_Project_Feilds').getFields()) {
            fieldSet.add(fields.getFieldPath());
        }

        if (fieldSet != null && fieldSet.size() > 0) {
            for (GP_Icon_Master__c objIconMaster: (List < GP_Icon_Master__c > ) records) {
                for (string eachField: fieldSet) {
                    if (oldSObjectMap != null) {
                        oldIConMaster = (GP_Icon_Master__c) oldSObjectMap.get(objIconMaster.Id);
                        if (objIconMaster.get(eachField) != oldIConMaster.get(eachField)) {
                            mapofIconIdToIconMaster.put(objIconMaster.Id, objIconMaster);
                        }
                    }
                }
            }
        }

        if (mapofIconIdToIconMaster != null && mapofIconIdToIconMaster.size() > 0) {
            for (GP_Project__c objProj: new GPSelectorProject().getApprovedProjectApprovedFieldDetail(mapofIconIdToIconMaster.keySet())) {
                if (objProj.GP_Approval_Status__c == 'Approved') {
                    setofApprovedProjctID.add(objProj.ID);
                } else if (objProj.GP_Approval_Status__c == 'Draft') {
                    setofDraftProjctID.add(objProj.ID);
                    if (objProj.GP_Parent_Project__c != null) {
                        setofApprovedProjctID.remove(objProj.GP_Parent_Project__c);
                    }
                }

            }
        }

        if (setofDraftProjctID != null && setofDraftProjctID.size() > 0) {
            
            if(setofApprovedProjctID != null && setofApprovedProjctID.size() > 0)
                setofDraftProjctID.addAll(setofApprovedProjctID);

            for (GP_Project__c objProj: new GPSelectorProject().getProjectDeatil(setofDraftProjctID)) {
                if (mapofIconIdToIconMaster.containsKey(objProj.GP_CRN_Number__c)) {
                    objProj.GP_CRN_Status__c = mapofIconIdToIconMaster.get(objProj.GP_CRN_Number__c).GP_Status__c;
                    objProj.GP_Contract_Start_Date__c = mapofIconIdToIconMaster.get(objProj.GP_CRN_Number__c).GP_START_DATE__c;
                    objProj.GP_Contract_End_Date__c = mapofIconIdToIconMaster.get(objProj.GP_CRN_Number__c).GP_END_DATE__c;
                    lstofProject.add(objProj);
                }

            }

        }

        if (lstofProject != null && lstofProject.size() > 0) {
          //  uow.registerDirty(lstofProject);
          
            Database.SaveResult[] srList = Database.update(lstofProject, false);
            
     }

        //if (setofApprovedProjctID != null && setofApprovedProjctID.size() > 0) {
        //          GPBatchCloneSubmitProject obj = new GPBatchCloneSubmitProject(setofApprovedProjctID);
        //database.executeBatch(obj,1);
        //}
    }
}