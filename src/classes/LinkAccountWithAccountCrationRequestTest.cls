@IsTest
public class LinkAccountWithAccountCrationRequestTest {
 public static Opportunity opp;
    public static Account oAccount;
    public static OpportunitySplit opp_split;
    public static User u, u1, u3;
    public static OpportunitySplitType oppSplitType;
    
    
  
    
     public static testMethod void setupTestData(){
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
       
        u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
         u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        oAccount.Client_Partner__c=u1.id;
        update oAccount;
        
        
         Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                    'test121@xyz.com','99999999999');
       
        System.runAs(u){
           
           opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1,
                                Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                Competitor__c='Accenture',Deal_Type__c='Competitive',Super_SL_overlay_created__c=False,SL_overlay_created__c=false,Target_Source__c='Ramp up');
            insert opp;
            
            
            
            oppSplitType = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
            opp_split = new OpportunitySplit(SplitPercentage =100,SplitTypeId =oppSplitType.id, SplitOwnerId =u3.id,OpportunityId =opp.id);
                insert opp_split;
            
            test.startTest();
            List<Account> accList = new List<Account>();
            accList.add(oAccount);
            
             Map<ID, Account> old_acc_map = new Map<ID, Account>();
            old_acc_map.put(oAccount.ID, oAccount);
            
            oAccount.Client_Partner__c=u1.id;
            update oAccount;
            
            
           
            
            DeleteOppSPlit.deleteOppSplitOwner(accList, old_acc_map);
             DeleteOppSPlit.deleteOppTeamMember(accList, old_acc_map);
            
           // delete opp_split;
              test.stopTest();
        }
    }
}