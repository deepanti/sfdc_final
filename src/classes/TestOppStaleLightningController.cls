@isTest
public class TestOppStaleLightningController {



        static TestMethod void Test1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Sales Rep']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );

        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        //Quota__c oQuota = GEN_Util_Test_Data.CreateQuota();
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);   
        oOpportunity.OwnerId = u.id;
        oOpportunity.CloseDate = System.today().addDays(-2);
        oOpportunity.Revenue_Start_Date__c = System.today().addDays(-2);
        update oOpportunity;
        Test.startTest();
  		List<Opportunity> list1 = OppStaleLightningController.getOppRecord();
        String recIds = '["' + (String)oOpportunity.id + '"]';
        String Status = '1 Month';
		List<Opportunity> list2 = OppStaleLightningController.massUpdateOpps(recIds, Status);
        Test.stopTest();
        
       
    }

}