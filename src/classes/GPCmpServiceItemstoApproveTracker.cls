@isTest
public class  GPCmpServiceItemstoApproveTracker {
    
    public static GP_Project__c prjObj;  
    public static User objuser;
    public static GP_Work_Location__c objSdo;
    public static GP_Pinnacle_Master__c objpinnacleMaster;
    
    public static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'Global';
        objrole.GP_Role_Category__c = 'BPR Approver';
        objrole.GP_Work_Location_SDO_Master__c = null; //objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        Id loggedInUser = userInfo.getUserId();
        
        prjObj.OwnerId = loggedInUser; 
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_BPR_Approval_Required__c = true;
        prjObj.GP_PID_Approver_Finance__c = false;
        prjObj.GP_Additional_Approval_Required__c = false;
        prjObj.GP_Controller_Approver_Required__c = false;
        prjObj.GP_FP_And_A_Approval_Required__c = false;
        prjObj.GP_MF_Approver_Required__c = false;
        prjObj.GP_Old_MF_Approver_Required__c = false;
        prjObj.GP_Product_Approval_Required__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Auto_Approval__c = false;
        prjObj.GP_Current_Working_User__c = loggedInUser; 
        prjObj.GP_Controller_User__c = loggedInUser; 
        prjObj.GP_PID_Approver_User__c = loggedInUser; 
        prjObj.GP_FP_A_Approver__c = loggedInUser; 
        prjObj.GP_New_SDO_s_MF_User__c = loggedInUser; 
        prjObj.GP_Primary_SDO__c = objSdo.Id;
        
        insert prjObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj ;
    }
    
    @isTest
    static void testItemsToApprove() {    
        
      String strFieldSetName = 'GP_Items_to_Approval';
        /*for(Schema.FieldSet f : SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }*/
        
        setupCommonData();
        prjObj.GP_PID_Approver_User__c = UserInfo.getUserId();
        update prjObj;
        
        GPAuraResponse approvalResponse = GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment','approver');
        
        System.runAs(objuser) { 
            System.debug('approvalResponse: ' + approvalResponse);
            
            GPAuraResponse pendingApprovalResponse = GPCmpServiceItemstoApprove.getPendingApprovals('GP_Project__c' , strFieldSetName);
            system.debug('pendingApprovalResponse.response::'+pendingApprovalResponse.response);
            List<JSON2Apex> lstOfResponse = (List<JSON2Apex>) System.JSON.deserialize(pendingApprovalResponse.response, List<JSON2Apex>.class);
         
            System.debug('lstOfResponse: ' +lstOfResponse[0].Pending_WI );

            //Map<String, Object> meta = (Map<String, Object>) JSON.deserializeUntyped(pendingApprovalResponse.response);
            //system.debug('meta::'+meta);
            
            list<GPControllerItemstoApprove.PendingWorkItems> lstPendingWorkItems = 
                        (list<GPControllerItemstoApprove.PendingWorkItems>)JSON.deserialize(lstOfResponse[0].Pending_WI,list<GPControllerItemstoApprove.PendingWorkItems>.class);
            
            
            for(GPControllerItemstoApprove.PendingWorkItems obj:lstPendingWorkItems){
                obj.isSelected = true;
            }
            
            System.debug('lstPendingWorkItems: ' + lstPendingWorkItems);
            System.debug('lstPendingWorkItems isSelected: ' + lstPendingWorkItems[0].isSelected);
            
            GPAuraResponse pendingApprovePendingItems = GPCmpServiceItemstoApprove.approvePendingItems(Json.serialize(lstPendingWorkItems));
        }
    }
    
    

	public class JSON2Apex {

	public String Pending_WI;
	public String Field_Set;
}    
    @isTest
    static void testItemsToReject() {    
        
        String strFieldSetName = 'GP_Items_to_Approval';
        /*for(Schema.FieldSet f : SObjectType.GP_Project__c.fieldsets.getMap().values()) {
            strFieldSetName = f.getName();
        }*/
        
        setupCommonData();
        GPCmpServiceProjectSubmission.submitforApproval(prjObj.Id, 'Cost Charging', 'test Comment','approver');
        
        System.runAs(objuser) {
            GPAuraResponse pendingApprovalResponse = GPCmpServiceItemstoApprove.getPendingApprovals('GP_Project__c' , strFieldSetName);
            
             List<JSON2Apex> lstOfResponse = (List<JSON2Apex>) System.JSON.deserialize(pendingApprovalResponse.response, List<JSON2Apex>.class);
            System.debug('lstOfResponse: ' + lstOfResponse);
            
            list<GPControllerItemstoApprove.PendingWorkItems> lstPendingWorkItems = 
                        (list<GPControllerItemstoApprove.PendingWorkItems>)JSON.deserialize(lstOfResponse[0].Pending_WI,list<GPControllerItemstoApprove.PendingWorkItems>.class);
            
            
            for(GPControllerItemstoApprove.PendingWorkItems obj:lstPendingWorkItems){
                obj.isSelected = true;
            }
           
            GPAuraResponse pendingApprovePendingItems = GPCmpServiceItemstoApprove.rejectPendingItems(Json.serialize(lstPendingWorkItems));
        }
    }
}