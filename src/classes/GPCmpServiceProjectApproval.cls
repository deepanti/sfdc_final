/**
 * @author Sumit.Sukla
 * @date 22/10/2017
 *
 * @group ProjectApproval
 * @group-content ../../ApexDocContent/ProjectApproval.html
 *
 * @description Apex Service for ProjectApproval module,
 * has aura enabled method for ProjectApproval.
 */
public class GPCmpServiceProjectApproval {
    /**
     * @description Aura enabled method to get Project detail.
     * @param Id projectId
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.getProjectDetail(<projectId>);
     */
    @AuraEnabled
    public static GPAuraResponse getProjectDetail(Id projectId) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.getProjectDetail();
    }

    /**
     * @description Aura enabled method to set Project Status to approve.
     * @param Id projectId
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.ProjectStatusToApprove(<projectId>);
     */
    @AuraEnabled
    public static GPAuraResponse ProjectStatusToApprove(Id projectId, string strSelectedStage, string strComment, string taxCode, string reports, String productCode) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.ProjectStatusChange(strSelectedStage, strComment, 'Approve', taxCode, reports, productCode);
    }

    /**
     * @description Aura enabled method to set Project Status to approve For Closure.
     * @param Id projectId
     * @param String strComment
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.ProjectStatusToApproveForClosure(<projectId>, <strComment>);
     */
    @AuraEnabled
    public static GPAuraResponse ProjectStatusToApproveForClosure(Id projectId, string strComment, String productCode) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.ProjectStatusChange('', strComment, 'Approve', null, null, productCode);
    }

    /**
     * @description Aura enabled method to set Project Status to rejected.
     * @param Id projectId
     * @param string strSelectedStage
     * @param string strComment
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.ProjectStatusToReject(<projectId>, <strUserId>, <strComment>);
     */
    @AuraEnabled
    public static GPAuraResponse ProjectStatusToReject(Id projectId, string strSelectedStage, string strComment, String productCode) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.ProjectStatusChange(strSelectedStage, strComment, 'Reject', null, null, productCode);
    }

    /**
     * @description Aura enabled method to set Project Status to rejected For Closure.
     * @param Id projectId
     * @param string strComment
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.ProjectStatusToRejectForClosure(<projectId>, <strComment>);
     */
    @AuraEnabled
    public static GPAuraResponse ProjectStatusToRejectForClosure(Id projectId, string strComment, String productCode) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.ProjectStatusChange('', strComment, 'Reject', null, null, productCode);
    }

    /**
     * @description Aura enabled method to reassign project to another user.
     * @param Id projectId
     * @param Id strUserId
     * @param string strComment
     * 
     * @return GPAuraResponse
     * 
     * @example
     * GPCmpServiceProjectApproval.ProjectReassignToUser(<projectId>, <strUserId>, <strComment>);
     */
    @AuraEnabled
    public static GPAuraResponse ProjectReassignToUser(Id projectId, string strUserId, string strComment) {
        GPControllerProjectApproval objController = new GPControllerProjectApproval(projectId);
        return objController.ProjectReAssign(strUserId, strComment);
    }
	
	//Avalara Changes
    @AuraEnabled
    public static String CheckAvalaraLECodeExist(String lecode) {
       return GPCmpServiceProjectDetail.CheckAvalaraLECodeExists(lecode);
    }
}