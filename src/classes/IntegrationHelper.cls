/*

* Integration class for MOR callouts from Oracle 
* created date - 2018/11/21
* created by - Madhuri Sharma

*  */
public class IntegrationHelper {
    
    // Method is Override If using third party Service without Certificate
     public static HttpRequest sendhttpRequest(string url,string method,string headerType,string headerValue1,string reqBody){
        
        String userName = Label.MORUsername;
        String password = Label.MORPassword;
        
        Blob headerValue = Blob.valueOf(userName + ':' + password);
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);

        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setEndpoint(url);
        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization', authorizationHeader);
        req.setTimeout(60000);
        
        return req;
    }
    
    //Method to send request and get response
    public static string getResponse(HttpRequest req){
        
        System.debug('\n\n Request With Body 3 ------>>> '+req);
        
        try{
            Http h = new Http();
            HttpResponse res = h.send(req);
            if(res!=null && res.getStatusCode() == 200){
                
                return res.getbody();
            }
        }catch(Exception e){
            System.debug('\n\n -------->>> Integration Helper Entry Retry ');
            String day = string.valueOf(system.now().day());
            String month = string.valueOf(system.now().month());
            String hour = string.valueOf(system.now().hour()+3);
            String minute = string.valueOf(system.now().minute());
            String second = string.valueOf(system.now().second());
            String year = string.valueOf(system.now().year());
            
            String strJobName = 'Job-' + second + '_' + minute + '_' + hour + '_' + day + '_' + month + '_' + year;
            String strSchedule = '0 ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ?' + ' ' + year;
            System.schedule(strJobName, strSchedule, new VIC_MORRatesCalloutScheduler());
            System.debug('\n\n -------->>> Integration Helper Exit Retry ');
            return null;
        }
        return '';
        
    }
    
}