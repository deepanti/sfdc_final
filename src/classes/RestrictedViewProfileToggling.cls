@RestResource(urlMapping='/ProfileToggling/*')
global class RestrictedViewProfileToggling {
	@HttpPatch
    global static String toggleProfile(String userId, String profileId) {
        
        update new User(Id=userId, ProfileId = profileId,Has_Stale_Deals__c=false);
           
        return 'Success';
	}
}