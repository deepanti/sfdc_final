/**
 * @author: kiran Kumar
 * @since: 09/08/2018
 * @description: This class is used to clone plan and its childs with respect to the year selected. 
 */
public class VIC_ClonePlanCtrl {
    @AuraEnabled
    public static list<String> getFinancialYear(){
        return vic_CommonUtil.getYearForDisplay();   
    }
    @AuraEnabled
    public static boolean getYearValid(id recrdId,string strYear){
        boolean isValid;
        string thisYear=string.valueof(system.today().year());
        list<Plan__C>  lstPlan=[
                                select id,Year__c 
                                from Plan__c 
                                where id =: recrdId and Is_Active__c=true and Year__c=:strYear
                               ];
        system.debug('lstPlan'+lstPlan);    
        if((lstPlan!=null && lstPlan.size()>0) || (strYear<thisYear)){
                isValid=true;
            }
            else{
                 isValid=false; 
            }
        system.debug('isValid'+isValid);
        return isValid;   
    }
    @AuraEnabled
    public static string savePlan(id recrdId,string strYear){
        string planid;
        list<Plan__C>  lstPlan=[select id,Name,Is_Active__c,Year__c,vic_Plan_Code__c,Conga_Email_Template_For_Target__c,Conga_Template__c,Conga_Email_Template__c,Conga_Template_For_Bonus__c from Plan__c where id =: recrdId];
        system.debug('lstPlan'+lstPlan);
        if(lstPlan!=null && lstPlan.size()>0){
            Plan__C objplan = new Plan__C();
            objplan.Name=lstPlan[0].Name;
            objplan.Is_Active__c=false;
            objplan.Year__c=strYear;
            objplan.vic_Plan_Code__c=lstPlan[0].vic_Plan_Code__c;
            objplan.Conga_Email_Template_For_Target__c=lstPlan[0].Conga_Email_Template_For_Target__c;
            objplan.Conga_Template__c=lstPlan[0].Conga_Template__c;
            objplan.Conga_Email_Template__c=lstPlan[0].Conga_Email_Template__c;
            objplan.Conga_Template_For_Bonus__c=lstPlan[0].Conga_Template_For_Bonus__c;
            insert objplan;
            planid=objplan.id;
            
        }
        if(planid!=null && recrdId!=null){
            insertPlanComponent(recrdId,planid);
            insertVicRole(recrdId,planid);
        }  
    return planid ;  
    }
 
    public static void insertPlanComponent(string strPlanId,string strClPanId){
          list<Plan_Component__c> lstPlcToInsert= new list<Plan_Component__c>();
          list<Plan_Component__c> lstPlc=[
                                          select id,Name,Plan__c,Weightage_Applicable__c,Master_Plan_Component__c,VIC_IO_Calculation_Matrix_1__c,    VIC_TS_Calculation_Matrix_2__c 
                                          from Plan_Component__c 
                                          where Plan__c=:strPlanId 
                                          ];
          
        if(lstPlc!=null && lstPlc.size()>0){
               for(Plan_Component__c objOPc:lstPlc){
                       Plan_Component__c objPc= new Plan_Component__c();
                       objPc.Master_Plan_Component__c=objOPc.Master_Plan_Component__c;
                       objPc.VIC_IO_Calculation_Matrix_1__c=objOPc.VIC_IO_Calculation_Matrix_1__c;
                       objPc.VIC_TS_Calculation_Matrix_2__c=objOPc.VIC_TS_Calculation_Matrix_2__c;
                       objPc.Plan__c=strClPanId;
                       objPc.Weightage_Applicable__c=objOPc.Weightage_Applicable__c;
                       lstPlcToInsert.add(objPc);
                }
            }

        if(lstPlcToInsert!=null && lstPlcToInsert.size()>0){
              insert lstPlcToInsert;
          
            }
    }
    
    public static void insertVicRole(string strPlanId,string strClPanId){
        list<VIC_Role__c> lstVlCToInsert= new list<VIC_Role__c>();
        list<VIC_Role__c> lstVlC=[
                                select id,Name,Plan__c,Master_VIC_Role__c 
                                from VIC_Role__c 
                                where Plan__c=:strPlanId 
                               ];

        if(lstVlC!=null && lstVlC.size()>0){
             for(VIC_Role__c objOViC:lstVlC){
               VIC_Role__c objViC= new VIC_Role__c();
               objViC.Name=objOViC.Name;
               objViC.Plan__c=strClPanId;
               objViC.Master_VIC_Role__c=objOViC.Master_VIC_Role__c;
               
               lstVlCToInsert.add(objViC);
            }
        }

      if(lstVlCToInsert!=null && lstVlCToInsert.size()>0){
               insert lstVlCToInsert;
        }
    }   
    
}