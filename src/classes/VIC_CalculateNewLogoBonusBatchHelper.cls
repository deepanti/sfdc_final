/*
 * @author: Prashant Kumar1
 * @since: 15/06/2018
 * @description: This class contains methods which help in calculation of incentive for 'New Logo Bonous component'
 *  calculation and act as helper for class VIC_CalculateNewLogoBonusBatch
 * 
*/
public class VIC_CalculateNewLogoBonusBatchHelper {
    
    public List<Target_Achievement__c> getIncentiveRecordsForAccount(Account acc,
                                                                     Map<Id, List<Opportunity>> accountsWithOrderedOpportunities,
                                                                     Map<Id, Target_Component__c> userIdvsTargetComponent,
                                                                     Map<Id, String> userIdVsPlanCode,
                                                                     Map<Id, User> usersMap,
                                                                     Boolean isAfterYearCalculation,
                                                                     IncentiveConstantWrapper incentiveConstantsWrap)
    {
        List<Target_Achievement__c> incentiveRecords;
        List<Opportunity> orderedOpps = accountsWithOrderedOpportunities.get(acc.Id);
        Opportunity firstOpp = orderedOpps[0];
        System.debug('====>>firstOpp: '+firstOpp);
        
        //next line comented byprashant kumar1 on 02/08/2018
        //Map<Id, Decimal> userIdVsTCV = new Map<Id, Decimal>();
        
        //this map stores total tcv and first OLI Id userwise for first opportunity users
        //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
        Map<Id, TCVOliWrapper> userIdVsTCVOliWrapperObj = new Map<Id, TCVOliWrapper>();
        //initialize user's indivisual TCV with value 0 from first opportunity
        for(OpportunityLineItem oppProd: firstOpp.OpportunityLineItems){
            List<Id> userIds = this.getOpportunityProductUsers(oppProd);
            for(Id id: userIds){
                //next block added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                if(!userIdVsTCVOliWrapperObj.containsKey(id)){
                    TCVOliWrapper tcvOLIWrapperObj = new TCVOliWrapper(0, oppProd.Id);
                    userIdVsTCVOliWrapperObj.put(id, tcvOLIWrapperObj);
                }
                //next line comented byprashant kumar1 on 02/08/2018
                //userIdVsTCV.put(id,0);
            }
        }
        
        Decimal sixMonthTCV = 0;
        Decimal twelveMonthTCV = 0;
        //below declared 3 opportunity defines the Opportunity responsible for TCV limit cross, closer date of these opportunity
        //is used as incentive achievement date
        Opportunity oppResForMaxTCVLimitCross, oppResForSixMonthTCVLimitCross, oppResForTwelveMonthTCVLimitCross;
        //calculate tcv for 6 months and 12 months from VIC_Hunting_Start_Date__c
        for(Opportunity opp: orderedOpps){
            for(OpportunityLineItem oppProd: opp.OpportunityLineItems){
               List<Id> userIds = this.getOpportunityProductUsers(oppProd);
                for(Id userId: userIds){
                    //next block comented byprashant kumar1 on 02/08/2018
                    /*
                    if(userIdVsTCV.containsKey(id)){
                        Decimal prevTCV = userIdVsTCV.get(id);
                        Decimal totalTCV = prevTCV + (oppProd.TCV__c * this.getIncentivePercent(id, oppProd) / 100);
                        userIdVsTCV.put(id, totalTCV);
                    }
                    */
                    //next block added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                    if(userIdVsTCVOliWrapperObj.containsKey(userId)){
                        if((oppProd.Opportunity.OwnerId == userId && oppProd.vic_Sales_Rep_Approval_Status__c != null && oppProd.vic_Sales_Rep_Approval_Status__c.containsIgnoreCase('Approved') ) 
                            || (oppProd.Product_BD_Rep__c != null && oppProd.Product_BD_Rep__c == userId && oppProd.vic_Product_BD_Rep_Approval_Status__c != null && oppProd.vic_Product_BD_Rep_Approval_Status__c.containsIgnoreCase('Approved')) 
                            || (oppProd.vic_VIC_User_3__c != null && oppProd.vic_VIC_User_3__c == userId) 
                            || (oppProd.vic_VIC_User_4__c != null && oppProd.vic_VIC_User_4__c == userId)){
                         
                            Decimal prevTCV = userIdVsTCVOliWrapperObj.get(userId).tcvValue;
                            userIdVsTCVOliWrapperObj.get(userId).tcvValue = prevTCV + (oppProd.TCV__c * this.getIncentivePercent(userId, oppProd) / 100);    
                        }                        
                    }
                }
                if(opp.Actual_Close_Date__c <= acc.VIC_Hunting_Start_Date__c.addMonths(6)){
                    Decimal oldSixMonthTCV = sixMonthTCV;
                    sixMonthTCV += oppProd.TCV__c;
                    if(oldSixMonthTCV < incentiveConstantsWrap.tcvHalfYearLimit && sixMonthTCV >= incentiveConstantsWrap.tcvHalfYearLimit){
                        oppResForSixMonthTCVLimitCross = opp;
                    }
                }
                if(opp.Actual_Close_Date__c <= acc.VIC_Hunting_End_Date__c.addDays(-1)){
                    Decimal oldTwelveMonthTCV = twelveMonthTCV;
                    twelveMonthTCV += oppProd.TCV__c; 
                    if(oldTwelveMonthTCV < incentiveConstantsWrap.maxIncentiveCap && twelveMonthTCV >= incentiveConstantsWrap.maxIncentiveCap){
                        oppResForMaxTCVLimitCross = opp;
                    }
                    if(oldTwelveMonthTCV < incentiveConstantsWrap.afterYearIncentive && twelveMonthTCV >= incentiveConstantsWrap.afterYearIncentive){
                        oppResForTwelveMonthTCVLimitCross = opp;
                    }
                }
            }
        }
       //next block comented byprashant kumar1 on 02/08/2018 
       //System.debug('===>>first OLI users: '+userIdVsTCV.keySet());
       //System.debug('===>>userIdVsTCV: '+userIdVsTCV);
       
       //next block added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
       System.debug('===>>first OLI users: '+userIdVsTCVOliWrapperObj.keySet());
       System.debug('===>>userIdVsTCV: '+userIdVsTCVOliWrapperObj);
            
             if(twelveMonthTCV >= incentiveConstantsWrap.tcvFullYearLimit 
                   || sixMonthTCV >= incentiveConstantsWrap.tcvFullYearLimit)
                {
                    //greater than 5 million in 6 or 12 months give total incentive of 25000
                    // or if 10000 previously given give 15000
                    incentiveRecords = this.getIncentiveRecordsForUsers(
                                                                    //next line comented byprashant kumar1 on 02/08/2018 
                                                                    //userIdVsTCV,
                                                                    //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                    userIdVsTCVOliWrapperObj,
                                                                    incentiveConstantsWrap.maxIncentiveCap,
                                                                    acc,
                                                                    firstOpp,
                                                                    userIdVsPlanCode,
                                                                    usersMap,
                                                                    userIdvsTargetComponent,
                                                                    incentiveConstantsWrap,
                                                                    isAfterYearCalculation,
                                                                    oppResForMaxTCVLimitCross);
                }
                else if(twelveMonthTCV >= incentiveConstantsWrap.tcvAfterYearLimit
                       && !(sixMonthTCV >= incentiveConstantsWrap.tcvHalfYearLimit))
                {
                    //greater than 1 million in 12 months give total incentive of of 10000
                        incentiveRecords = this.getIncentiveRecordsForUsers(
                                                                    //next line comented byprashant kumar1 on 02/08/2018 
                                                                    //userIdVsTCV,
                                                                    //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                    userIdVsTCVOliWrapperObj,
                                                                    incentiveConstantsWrap.afterYearIncentive,
                                                                    acc,
                                                                    firstOpp,
                                                                    userIdVsPlanCode,
                                                                    usersMap,
                                                                    userIdvsTargetComponent,
                                                                    incentiveConstantsWrap,
                                                                    isAfterYearCalculation,
                                                                    oppResForTwelveMonthTCVLimitCross);
                }
                else if(sixMonthTCV >= incentiveConstantsWrap.tcvHalfYearLimit){
                    //greater than 500k in 6 months give total incentive of 10000
                    incentiveRecords = this.getIncentiveRecordsForUsers(
                                                                    //next line comented byprashant kumar1 on 02/08/2018 
                                                                    //userIdVsTCV,
                                                                    //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                    userIdVsTCVOliWrapperObj,
                                                                    incentiveConstantsWrap.halfYearIncentive,
                                                                    acc,
                                                                    firstOpp,
                                                                    userIdVsPlanCode,
                                                                    usersMap,
                                                                    userIdvsTargetComponent,
                                                                    incentiveConstantsWrap,
                                                                    isAfterYearCalculation,
                                                                    oppResForSixMonthTCVLimitCross);
                }
                else{
                    //condition of getting 0 incentive
                    incentiveRecords = this.getIncentiveRecordsForUsers(
                                                                    //next line comented byprashant kumar1 on 02/08/2018 
                                                                    //userIdVsTCV,
                                                                    //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                    userIdVsTCVOliWrapperObj,
                                                                    0,
                                                                    acc,
                                                                    firstOpp,
                                                                    userIdVsPlanCode,
                                                                    usersMap,
                                                                    userIdvsTargetComponent,
                                                                    incentiveConstantsWrap,
                                                                    isAfterYearCalculation,
                                                                    null);                    
                }
        
        return incentiveRecords;
    }
    
    /*
     * @params:
     * @returns:
     * @description:
     * 
     */
    @TestVisible
    private List<Target_Achievement__c> getIncentiveRecordsForUsers(
                                                                    //next line comented byprashant kumar1 on 02/08/2018 
                                                                    //Map<Id, Decimal> userIdVsTCV,
                                                                    //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                    Map<Id, TCVOliWrapper> userIdVsTCVOliWrapperObj,
                                                                    Decimal IncentiveCap,
                                                                    Account acc,
                                                                    Opportunity firstOpp,
                                                                    Map<Id, String> userIdVsPlanCode,
                                                                    Map<Id, User> usersMap,
                                                                    Map<Id, Target_Component__c> userIdvsTargetComponent,
                                                                    IncentiveConstantWrapper incentiveConstantsWrap,
                                                                    Boolean isAfterYearCalculation,
                                                                    Opportunity resOppForTCVLimitCross)
    {
        List<Target_Achievement__c> incentiveRecords = new List<Target_Achievement__c>();
        //for(Id userId: userIdVsTCV.keySet()){
        for(Id userId: userIdVsTCVOliWrapperObj.keySet()){
            System.debug('====>>userId from incentives: '+userId);
            if(userIdVsPlanCode.get(userId) == 'BDE'){//if user have plan BDE
                System.debug('====>>Targer Componnent from incentives: '+userIdvsTargetComponent.get(userId));
                if(userIdvsTargetComponent.get(userId) != null){// if target component is available for user
                    
                    Decimal pppMultiplier = 1;
                    //checking whether user met condition for PPP multiplier
                    if(VIC_CountryCostTypesCtlr.isQualifiedForPPP(usersMap.get(userId).Domicile__c, firstOpp.Sales_country__c)){
                        String PPPStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('NPB_BDE_Purchase_Power_Parity');
                        pppMultiplier = VIC_GeneralChecks.isDecimal(PPPStr) ? Decimal.valueOf(PPPStr) : 0;
                    }
                    List<Target_Achievement__c> oldIncentives 
                        = this.getUserOldIncentivesForTargetComponentRelatedToAccount(userIdvsTargetComponent.get(userId),
                                                                                     acc);
                    
                    Decimal incentiveAmt = this.getIncentiveAmount(
                                                                   //next line comented byprashant kumar1 on 02/08/2018 
                                                                   //userIdVsTCV.get(userId),
                                                                   //next line added byprashant kumar1 on 02/08/2018 to store opportunity and oli id on incentive records
                                                                   userIdVsTCVOliWrapperObj.get(userId).tcvValue,
                                                                   IncentiveCap, 
                                                                   incentiveConstantsWrap,
                                                                   isAfterYearCalculation);
                    
                    Target_Achievement__c incentiveRecord = new Target_Achievement__c();
                    incentiveRecord.Target_Component__c = userIdvsTargetComponent.get(userId).Id;
                    incentiveRecord.vic_PPP__c = pppMultiplier;
                    incentiveRecord.vic_Account__c = acc.Id;
                    incentiveRecord.Opportunity__c = firstOpp.Id;
                    incentiveRecord.vic_Status__c = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Default_Incentive_Status');
                    incentiveRecord.vic_Opportunity_Product_Id__c = userIdVsTCVOliWrapperObj.get(userId).firstOLIId;
                    incentiveRecord.Achievement_Date__c 
                        = resOppForTCVLimitCross != null ? resOppForTCVLimitCross.Actual_Close_Date__c : System.today();
                    incentiveAmt = incentiveAmt * pppMultiplier;
                    
                    if(oldIncentives.isEmpty()){
                        incentiveRecord.vic_Incentive_Type__c = 'New';
                        if(incentiveAmt == 0){
                            incentiveRecord = null;
                        }
                        else{
                            incentiveRecord.Bonus_Amount__c = incentiveAmt;
                        }
                    }
                    else{
                        incentiveRecord.vic_Incentive_Type__c = 'Adjustment';
                        Decimal previouslyReleaseIncentive = 0;
                        for(Target_Achievement__c oldIncentive: oldIncentives){
                            previouslyReleaseIncentive += oldIncentive.Bonus_Amount__c;
                        }
                        if(incentiveAmt - previouslyReleaseIncentive == 0){
                            incentiveRecord = null;
                        }
                        else{
                            incentiveRecord.Bonus_Amount__c = incentiveAmt - previouslyReleaseIncentive;
                            incentiveRecord.vic_Incentive_Details__c = 'Current Incentive amount is ' + incentiveAmt
                                + ' and previously given was ' + previouslyReleaseIncentive + ' So current incentive amount is '
                                + (incentiveAmt - previouslyReleaseIncentive);
                        }
                    }
                    System.debug('====>>incentiveRecord: '+incentiveRecord);
                    if(incentiveRecord != null){
                        incentiveRecords.add(incentiveRecord); 
                    }
                }
            }
            
        }
        return incentiveRecords;
    }
    
    /*
     * @params:  Total TCV of USer(Decimal), Available Incentive (Decimal),
     *           Wrapper of Incentive Constants (incentiveConstantsWrap),
     *           Whether It is After Year Calculation or not (Boolean)
     * @return: Incentive amount as per provided parameters (Decimal)
     * @description: This method test for whether user is eligible for available incentive or not, 
     * if not how much incentive can be given
     * 
     */
    @TestVisible
    private Decimal getIncentiveAmount(Decimal userTotalTCV, Decimal incentiveCapForUser,
                                       IncentiveConstantWrapper incentiveConstantsWrap,
                                       Boolean isAfterYearCalculation)
    {
        Decimal incentiveAmount = 0;
        if(incentiveCapForUser == incentiveConstantsWrap.maxIncentiveCap){
            //if user is eligible for 25000
            if(userTotalTCV >= incentiveConstantsWrap.tcvFullYearLimit){
                //user have indivisual TCV greater than 5 million
                incentiveAmount = incentiveCapForUser;
            }
            //Note: Ask Rishbh Pilani sir about else condition which is: If user eligible for 25000 but user's Total TCV
            //is either more than 500k or 1 million but less than 50 million (Satisfy condition to get 10000 incentive)
           
            else{
                //if user is eligible for 10000
                if(isAfterYearCalculation){
                    if(userTotalTCV >= incentiveConstantsWrap.afterYearIncentive){
                        //user have indivisual TCV greater than 1 million (After a Year)
                        incentiveAmount = incentiveCapForUser;
                    }
                }
                else{
                    //user have indivisual TCV greater than 1 million 
                    if(userTotalTCV >= incentiveConstantsWrap.halfYearIncentive){
                        //user have indivisual TCV greater than 500k (Within 6 month)
                        incentiveAmount = incentiveCapForUser;
                    }
                    
                }
            }
        }
        else if(incentiveCapForUser == incentiveConstantsWrap.halfYearIncentive
               || incentiveCapForUser == incentiveConstantsWrap.afterYearIncentive)
        {
            //if user is eligible for 10000
            if(isAfterYearCalculation){
                if(userTotalTCV >= incentiveConstantsWrap.afterYearIncentive){
                    //user have indivisual TCV greater than 1 million (After a Year)
                    incentiveAmount = incentiveCapForUser;
                }
            }
            else{
               //user have indivisual TCV greater than 1 million 
                if(userTotalTCV >= incentiveConstantsWrap.halfYearIncentive){
                    //user have indivisual TCV greater than 500k (Within 6 month)
                    incentiveAmount = incentiveCapForUser;
                }
            }
        }
        return incentiveAmount;
    }
    
    /*
     * @params: Set of Opportunity Ids
     * @return: VIC_Hunting_Start_Date__c
     * @description: This method fetch ordered opportunity by its closing date in accending order
     * and returns a wrapper object wich consist of a map and a set. Map is of Account id and List of ordered opportunity,
     * and set is of all users present on the Opportunity products.
     * 
     */
    public AccountsAndUsersDetailWrapper getAccountsWithOrderedOpportunitiesAndUsersMap(Set<Id> oppIds){
        String CPQDealStatus 
            = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('CPQ_Deal_Status');
        String opportunityStagename 
            = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Eligible_Stage_For_VIC');
        
        Map<Id, List<Opportunity>> accountsWithOrderedOpportunities = new Map<Id, List<Opportunity>>();
        Set<Id> userIds = new Set<Id>();
        
        List<Opportunity> listOfOrderedOpportunity = [SELECT Id, AccountId,Actual_Close_Date__c,Sales_country__c,
                                                      (
                                                        SELECT Id, TCV__c, Opportunity.OwnerId, 
                                                        Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c,
                                                        vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c,
                                                        vic_VIC_User_3_Split__c, vic_VIC_User_4_Split__c, vic_Sales_Rep_Approval_Status__c,  vic_Product_BD_Rep_Approval_Status__c 
                                                        FROM OpportunityLineItems
                                                        WHERE Opportunity.StageName =: opportunityStagename  //AND Pricing_Deal_Type_OLI_CPQ__c =: dealType 
                                                        AND vic_CPQ_Deal_Status__c =: CPQDealStatus AND vic_Final_Data_Received_From_CPQ__c = true 
                                                        AND (
                                                            (vic_Sales_Rep_Approval_Status__c = 'Approved' OR vic_Product_BD_Rep_Approval_Status__c = 'Approved')
                                                            //OR (vic_Sales_Rep_Approval_Status__c = 'Approved' AND vic_Product_BD_Rep_Approval_Status__c = 'Not Required')
                                                        )
                                                      )
                                                      FROM Opportunity
                                                      WHERE Id =: oppIds
                                                      ORDER BY Actual_Close_Date__c ASC];
        
        for(Opportunity opp: listOfOrderedOpportunity){
            for(OpportunityLineItem oppProd: opp.OpportunityLineItems){
                userIds.addAll(this.getOpportunityProductUsers(oppProd));
            }
            if(accountsWithOrderedOpportunities.containsKey(opp.AccountId)){
                accountsWithOrderedOpportunities.get(opp.AccountId).add(opp);
            }
            else{
                List<Opportunity> opps = new List<Opportunity>();
                opps.add(opp);
                accountsWithOrderedOpportunities.put(opp.AccountId, opps);
            }
        }
        
        Map<Id, User> usersMap = new Map<Id, User>([
            SELECT Id, Domicile__c FROM User WHERE Id =:userIds
        ]);
        
        return new AccountsAndUsersDetailWrapper(accountsWithOrderedOpportunities, usersMap);
    }
    
    /*
    * @params: OpprtunityProduct
    * @return: List Of User Ids of OpprtunityProduct
    * @decription: This method returns the list of users involved in sales process of Opportunity product
    * 
    */
    public List<Id> getOpportunityProductUsers(OpportunityLineItem oppProd){
        List<Id> userIds = new List<Id>();
        System.debug('====>>oppProd: '+oppProd);
        if(oppProd.Opportunity.OwnerId != null){
            userIds.add(oppProd.Opportunity.OwnerId);
        }
        if(oppProd.Product_BD_Rep__c != null){
            if(oppProd.Product_BD_Rep__c != oppProd.Opportunity.OwnerId){
                userIds.add(oppProd.Product_BD_Rep__c);
            }
        }
        if(oppProd.vic_VIC_User_3__c != null){
            userIds.add(oppProd.vic_VIC_User_3__c);
        }
        if(oppProd.vic_VIC_User_4__c != null){
            userIds.add(oppProd.vic_VIC_User_4__c);
        }
        System.debug('===>>userIds: '+userIds);
        return userIds;
    }
    
    /*
    * @params: List of UserIds(List<Id>)
    * @return: Map of userId Vs Plan(Map<Id, String>)
    * @description: This method returns the userId vs PlanCodeMap
    * 
    */
    public Map<Id, String> getUsersVsPlanMap(Set<Id> userIds){
        Map<Id, String> usersVsPlanCodes = new Map<Id, String>();
        Map<Id, Id> usersVsMasterVICRole = new Map<Id, Id>();
        Set<Id> masterVICRoles = new Set<Id>();
        Map<Id, String> masterVICRoleVsPlanCode = new Map<Id, String>();
        
        LIst<User_VIC_Role__c> userVICRoleMappings = [SELECT Id, Master_VIC_Role__c, User__c 
                                                      FROM User_VIC_Role__c 
                                                      WHERE User__c =: userIds 
                                                      AND vic_For_Previous_Year__c = false
                                                      AND User__r.isActive = true
                                                      AND Not_Applicable_for_VIC__c = false];
        
        for(User_VIC_Role__c obj: userVICRoleMappings){
            usersVsMasterVICRole.put(obj.User__c, obj.Master_VIC_Role__c);
            masterVICRoles.add(obj.Master_VIC_Role__c);
        }
        
        //Current year will be taken from custome settings later
        String currentYearStr = String.valueOf(System.today().year());
        
        List<VIC_Role__c> vicRoles = [SELECT Id, Plan__r.vic_Plan_Code__c, Master_VIC_Role__c 
                                      FROM VIC_Role__c 
                                      WHERE Master_VIC_Role__c =:masterVICRoles 
                                      AND Plan__r.Year__c =: currentYearStr 
                                      AND  Plan__r.Is_Active__c = true
                                      AND Plan__r.vic_Plan_Code__c != null];
        
        for(VIC_Role__c obj: vicRoles){
            masterVICRoleVsPlanCode.put(obj.Master_VIC_Role__c, obj.Plan__r.vic_Plan_Code__c);
        }
        
        // creating the required map
        for(Id userId : usersVsMasterVICRole.keySet()){
            usersVsPlanCodes.put(userId, masterVICRoleVsPlanCode.get(usersVsMasterVICRole.get(userId)));
        }
        
        return usersVsPlanCodes;
    }
    
    
    /*
    * @params: Set of userIds for which target components are required (Set<Id>),
    *           Set of Opportunity products ids for which incentive records on target component is queried (Set<Id>)
    * @return: Map of user Id vs List of target components of user (Map<Id, List<Target_Component__c>>)
    * @description: This method fetch target components of user and it's child incentve records filtered with opportunity id
    * 
    */
    public Map<Id, Target_Component__c> getTargetComponents(Set<Id> userIds, Set<Id> accIds){
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        List<Target_Component__c> targetComponents = [ SELECT Id, Master_Plan_Component__c, Target__c, Target__r.User__c, 
                                                      Target__r.vic_TCV_IO__c, Target__r.vic_TCV_TS__c,
                                                      Master_Plan_Component__r.vic_Component_Code__c,
                                                        (
                                                            SELECT Id, vic_Account__c, Bonus_Amount__c 
                                                            FROM Target_Achievements__r
                                                            WHERE vic_Account__c =: accIds
                                                        )
                                                        FROM Target_Component__c
                                                        WHERE Target__r.User__c =:userIds
                                                        AND Target__r.Start_Date__c =:processingYearStart
                                                        AND Master_Plan_Component__r.vic_Component_Code__c ='New_Logo_Bonus'
                                                    ];
        
        Map<Id, Target_Component__c> userIdVsTargetComponentMap = new Map<Id, Target_Component__c>();
        
        // preparing userIdVsTargetComponentsMap
        for(Target_Component__c obj: targetComponents){
            userIdVsTargetComponentMap.put(obj.Target__r.User__c, obj);
        }
        return userIdVsTargetComponentMap;
    }
    
    
    /*
    * @params: Target Component, Account
    * @return: List Of Incentives (List<Target_Achievement__c>)
    * @description: This method returns list of old incentive 
    * records creared for user in this target component filtered by Account Id
    */
    public List<Target_Achievement__c>
        getUserOldIncentivesForTargetComponentRelatedToAccount(Target_Component__c targetComponent, Account acc)
    {
        List<Target_Achievement__c> oldIncentiveRecords = new List<Target_Achievement__c>();
        if(targetComponent != null){
            for(Target_Achievement__c incentiveReocord: targetComponent.Target_Achievements__r){
                if(incentiveReocord.vic_Account__c == acc.Id){
                    oldIncentiveRecords.add(incentiveReocord);
                }
            }
        }
        
        
        return oldIncentiveRecords;
    }
    
    /*
     * @author: Prashant  Kumar1
     * @since: 15/06/2018
     * @description: This wrapper is used to return map of users and accounts for method: getAccountsWithOrderedOpportunities
     * 
    */
    public class AccountsAndUsersDetailWrapper{
        public Map<Id, List<Opportunity>> accountsWithOrderedOpportunities;
        public Map<Id, User> mapOfUsers;
        public AccountsAndUsersDetailWrapper(Map<Id, List<Opportunity>> accountsWithOrderedOpportunities,
                                            Map<Id, User> mapOfUsers)
        {
            this.accountsWithOrderedOpportunities = accountsWithOrderedOpportunities;
            this.mapOfUsers = mapOfUsers;
        }
    }
    
    /*
     * @author: Prashant Kumar1
     * @since: 17/06/2018
     * @description:
     * 
     */
    public class IncentiveConstantWrapper{
        public Decimal maxIncentiveCap, halfYearIncentive, afterYearIncentive, 
            tcvHalfYearLimit, tcvFullYearLimit, tcvAfterYearLimit;
        public IncentiveConstantWrapper(Decimal maxIncentiveCap, Decimal halfYearIncentive, Decimal afterYearIncentive, 
                                        Decimal tcvHalfYearLimit, Decimal tcvFullYearLimit, Decimal tcvAfterYearLimit)
        {
            this.maxIncentiveCap = maxIncentiveCap;
            this.halfYearIncentive = halfYearIncentive;
            this.afterYearIncentive = afterYearIncentive;
            this.tcvHalfYearLimit = tcvHalfYearLimit;
            this.tcvFullYearLimit = tcvFullYearLimit;
            this.tcvAfterYearLimit = tcvAfterYearLimit;
        }
    }
    
    /*
    * @params: UserId (Id), OpportunityLineItem
    * @return: Incentive split percent(Decimal)
    * @description: This method returns incentive split percent of sales user on Opportunity Product
    * 
    */
    @TestVisible
    private Decimal getIncentivePercent(Id userId, OpportunityLineItem oppProd){
         Decimal incentivePercent = 0;
        if(userId == oppProd.Opportunity.OwnerId && oppProd.vic_Primary_Sales_Rep_Split__c != null){
            incentivePercent = oppProd.vic_Primary_Sales_Rep_Split__c;
        }
        else if(userId == oppProd.Product_BD_Rep__c && oppProd.vic_Product_BD_Rep_Split__c != null){
            incentivePercent = oppProd.vic_Product_BD_Rep_Split__c;
        }
        else if(oppProd.vic_VIC_User_3__c != null && userId == oppProd.vic_VIC_User_3__c && oppProd.vic_VIC_User_3_Split__c != null){
            incentivePercent = oppProd.vic_VIC_User_3_Split__c;
        }
        else if(oppProd.vic_VIC_User_4__c != null && userId == oppProd.vic_VIC_User_4__c && oppProd.vic_VIC_User_4_Split__c != null){
            incentivePercent = oppProd.vic_VIC_User_4_Split__c;
        }
        System.debug('incentivePercent===>>'+incentivePercent);
        return incentivePercent == null ? 0 : incentivePercent;
    }
    
    /*Wrapper class*/
    public class TCVOliWrapper{
        Decimal tcvValue;
        Id firstOLIId;
        TCVOliWrapper(Decimal tcvValue, Id firstOLIId){
            this.tcvValue = tcvValue;
            this.firstOLIId = firstOLIId;
        }
    }
}