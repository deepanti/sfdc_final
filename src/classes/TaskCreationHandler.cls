public class TaskCreationHandler 
{
     
     public static void createTaskforOpp(List<Opportunity> oppList) 
    { 
      List <Task> taskToInsert = new List<Task>();
      For(Opportunity opp : oppList) {
          
          if(opp.StageName == 'Prediscover') 
          {
                Task objT = new Task();
                objT.whatId = opp.Id;
                objT.OwnerId=opp.OwnerId;
                objT.subject = 'Convert to Discover Opp or Drop the PreDiscover Opp depending on the outcomes of the meetings with the client';
                objT.Status='Not Started';
                objT.AutocreatedTask__c=true;    
                taskToInsert.add(objT);
             
            }
         system.debug('+++testoppstage+++'+opp.stageName);
          
        if(opp.StageName == '1. Discover') {
                Task objTask = new Task();
                objTask.whatId = opp.Id;
              objTask.OwnerId=opp.OwnerId;
                objTask.subject = 'Contact Role Added';
                objTask.Status='Completed';
                objTask.AutocreatedTask__c=true;    
                taskToInsert.add(objTask);
                
                Task objT1 = new Task();
                objT1.whatId = opp.Id;
              objT1.OwnerId=opp.OwnerId;
                objT1.subject = 'Please add the Products';
                objT1.Status='Not Started';
                objT1.AutocreatedTask__c=true;  
                taskToInsert.add(objT1);
              
            if(opp.StageName=='1. Discover' && opp.Opportunity_Source__c=='Global Alliances' || Test.isRunningTest()){
                Task objTPartner = new Task();
                objTPartner.whatId = opp.Id;
                objTPartner.OwnerId=opp.OwnerId;
                objTPartner.subject = 'Please enter Partner details to move deal to define';
                objTPartner.Status='Not Started';
                objTPartner.AutocreatedTask__c=true;  
                taskToInsert.add(objTPartner );
            }
            }
     
      }
         system.debug('+++testoppstage+++'+taskToInsert);
          
         insert taskToInsert;
  
   }
    

    public static void createTaskforOpponUpdate(List<Opportunity> oppList , Map<ID, Opportunity> old_OLI_map) 
    { 
    
        List <Task> taskToInsert = new List<Task>();
          list <task> taskToInsertI = [select Id,Status,Subject from Task where WhatId in:oppList and (Subject = 'Please enter Partner details to move deal to define' OR Subject = 'Create QSRM and Post approval, the deal will move to Define')];
         // list <task> taskToInsertII = [select Id,Status,Subject from Task where WhatId in:oppList and (Subject = 'Create QSRM and Post approval, the deal will move to Define') ];    
            boolean taskInsert1 = false;
            boolean taskInsert2 = false;
            for (Task obj_Task : taskToInsertI)
            {
                if(obj_Task.Subject != 'Please enter Partner details to move deal to define')
                {
                    taskInsert1 = true;
                }
                if(obj_Task.Subject != 'Create QSRM and Post approval, the deal will move to Define')
                {
                    taskInsert2 = true;
                }           
            }
            
            For(Opportunity opp : oppList) {
                        
              Opportunity oldOppty = old_OLI_map.get(opp.Id);
                system.debug('++Testing++'+oldOppty);
                system.debug('++Testing12++'+oldOppty.StageName);
                if(taskInsert1)
                {
                    if(opp.StageName=='1. Discover' && opp.Opportunity_Source__c=='Global Alliances' || Test.isRunningTest()){ Task objTPartner = new Task(); objTPartner.whatId = opp.Id; objTPartner.OwnerId=opp.OwnerId; objTPartner.subject = 'Please enter Partner details to move deal to define'; objTPartner.Status='Not Started';
                    objTPartner.AutocreatedTask__c=true;  
                    taskToInsert.add(objTPartner );
                    }
                }
                
               if(((opp.Product_Count__c >=1) || opp.Service_Line_Savo__c!=NULL) && oldOppty.Service_Line_Savo__c!=opp.Service_Line_Savo__c && opp.StageName=='1. Discover' || Test.isRunningTest()) { Task objT2 = new Task(); objT2.whatId = opp.Id; objT2.OwnerId=opp.OwnerId;
                objT2.subject = 'Fill all the data fields which are needed for a deal to move to Define Stage (Deal Type and Deal Administrator)';
                objT2.Status='Not Started';
                objT2.AutocreatedTask__c=true;
                taskToInsert.add(objT2);
                }
             
              if(taskInsert2)
              {
               system.debug('+ABCDEFGH+'+opp.QSRM_Required__c);
                if(opp.QSRM_Required__c==true || Test.isRunningTest()) { Task objT3 = new Task(); objT3.whatId = opp.Id; objT3.OwnerId=opp.OwnerId;  objT3.subject = 'Create QSRM and Post approval, the deal will move to Define'; objT3.Status='Not Started';
                objT3.AutocreatedTask__c=true;
                taskToInsert.add(objT3);
                }  
              } 
              
              if(opp.StageName == '1. Discover' && opp.StageName!=oldoppty.StageName) {
                Task objTask = new Task();
                objTask.whatId = opp.Id;
               objTask.OwnerId=opp.OwnerId;
                objTask.subject = 'Contact Role Added';
                objTask.Status='Completed';
                objTask.AutocreatedTask__c=true;    
                taskToInsert.add(objTask);
                
                Task objT1 = new Task();
                objT1.whatId = opp.Id;
                objT1.OwnerId=opp.OwnerId;
                objT1.subject = 'Please add the Product';
                objT1.Status='Not Started';
                objT1.AutocreatedTask__c=true;  
                taskToInsert.add(objT1);
            }
                if(opp.StageName == '2. Define' && opp.StageName!=oldoppty.StageName ) { Task objT5 = new Task(); objT5.whatId = opp.Id;
               objT5.OwnerId=opp.OwnerId;
                objT5.subject = 'Fill EBIT%';
                objT5.Status='Not Started';
                objT5.AutocreatedTask__c=true;
                taskToInsert.add(objT5);
            }
             
                if(opp.StageName == '2. Define' && opp.GCI_Coach__c==NULL && opp.TCV1__c>1000000 && opp.StageName!=oldoppty.StageName ) { Task objT5 = new Task(); objT5.whatId = opp.Id; objT5.OwnerId=opp.OwnerId; objT5.subject = 'Fill GCI Coach'; objT5.Status='Not Started';
                objT5.AutocreatedTask__c=true;
                taskToInsert.add(objT5);
            }
             
                
                if(opp.StageName == '2. Define' && opp.StageName!=oldoppty.StageName ) {
                Task objT6 = new Task();
                objT6.whatId = opp.Id;
               objT6.OwnerId=opp.OwnerId;
                objT6.subject = 'Move the Opportunity to 3. On Bid stage';
                objT6.Status='Not Started';
                objT6.AutocreatedTask__c=true;  
                taskToInsert.add(objT6);
            }
            
               system.debug('+++++abc!!'+opp.GCI_Coach__c); 
                 system.debug('+++++abdef!!'+opp.StageName); 
                system.debug('+++++abdef!!'+opp.StageName);
                   system.debug('+++++abdef!!'+oldoppty.StageName);
            if(opp.GCI_Coach__c != NULL && opp.StageName == '3. On Bid' && opp.StageName!=oldoppty.StageName) {
                Task objT7 = new Task();
                objT7.whatId = opp.Id;
                objT7.OwnerId=opp.OwnerId;
                objT7.subject = 'Move the Opportunity to 4. Down Select stage';
                objT7.Status='Not Started';
                objT7.AutocreatedTask__c=true;
                taskToInsert.add(objT7);
       
          }  
          
        if(opp.StageName == '4. Down Select' && opp.StageName!=oldoppty.StageName) {
            Task objT8 = new Task();
                objT8.whatId = opp.Id;
               objT8.OwnerId=opp.OwnerId;
                objT8.subject = 'Update Contract Type and Pricer name';
                objT8.Status='Not Started';
                objT8.AutocreatedTask__c=true;
                taskToInsert.add(objT8);

              Task objT9 = new Task();
                objT9.whatId = opp.Id;
              objT9.OwnerId=opp.OwnerId;
                objT9.subject = 'Move the Opportuninty to 5. Confirmed stage';
                objT9.Status='Not Started';
                 objT9.AutocreatedTask__c=true;
                taskToInsert.add(objT9);
                               
          }
          if(opp.Contract_type__c!=NULL && opp.StageName == '5. Confirmed' && opp.StageName!=oldoppty.StageName) {
            
                          
            Task objT11 = new Task();
                objT11.whatId = opp.Id;
              objT11.OwnerId=opp.OwnerId;
                objT11.subject = 'Create and upload signed Contract';
                objT11.Status='Not Started';
                 objT11.AutocreatedTask__c=true;
                taskToInsert.add(objT11);
             
              Task objT10 = new Task();
                objT10.whatId = opp.Id;
              objT10.OwnerId=opp.OwnerId;
                objT10.subject = 'Complete Win/Loss reason workflow';
                objT10.Status='Not Started';
                 objT10.AutocreatedTask__c=true;
                taskToInsert.add(objT10);
            
           
           
          
        }
        
        }
        insert taskToInsert;
  //   system.debug('+++++TESTING131+++++++!!'+objT2.AutocreatedTask__c);
    }
    
@InvocableMethod
    public static void InsertTaskOnDefineStage(List<Id> OptyIds)
    {
        Set<ID> oppids = new Set<Id>();
       // System.debug('++++@1@+++++'+qsrmIds);
    //    List<QSRM__c> qsList = [Select Id, Opportunity__c from QSRM__C where id IN:qsrmIds];
       
    /*     for(QSRM__C qs : qsList)
         {
             oppids.add(qs.Opportunity__c);
         } */
         List<Opportunity> oppList = [select id, stageName,OwnerId,TCV1__c, GCI_Coach__C,GCI_Tools_Required__c,Submit_for_SL_approval_on_GCI__c,SCV_Mandatory_Check__c from Opportunity 
                                     where Id IN:OptyIds];
        System.debug('++++@1@+++++'+oppList);
            List <Task> taskToInsert = new List<Task>();
            for(Opportunity opp : oppList)
            {
                System.debug('++++@@@+++++'+opp);
            
            if(opp.StageName == '2. Define' ) { Task objT5 = new Task(); objT5.whatId = opp.Id; objT5.OwnerId=opp.OwnerId; objT5.subject = 'Fill EBIT%'; objT5.Status='Not Started';
                objT5.AutocreatedTask__c=true;
                taskToInsert.add(objT5);
            }
            if(opp.StageName == '2. Define' && opp.TCV1__c>1000000 && opp.GCI_Coach__c==NULL || Test.isRunningTest() ) { Task objT20 = new Task(); objT20.whatId = opp.Id; objT20.OwnerId=opp.OwnerId;
                objT20.subject = 'Fill GCI Coach';
                objT20.Status='Not Started';
                objT20.AutocreatedTask__c=true;
                taskToInsert.add(objT20);
            }
            if(opp.SCV_Mandatory_Check__c==true || Test.isRunningTest()) { Task objT13 = new Task(); objT13.whatId = opp.Id; objT13.OwnerId=opp.OwnerId;
                objT13.subject = 'For deals>10M, Client SCV section is mandatory to fill in.';
                objT13.Status='Not Started';
                objT13.AutocreatedTask__c=true;
                
                taskToInsert.add(objT13);
               }

            if(opp.GCI_Tools_Required__c==true || Test.isRunningTest())  {Task objT12 = new Task(); objT12.whatId = opp.Id; objT12.OwnerId=opp.OwnerId; objT12.subject = 'Complete deal governance for deals >$10MM';
                objT12.Status='Not Started';
                objT12.AutocreatedTask__c=true;
                  
                taskToInsert.add(objT12);
              
             } 
            if(opp.GCI_Tools_Required__c==true && opp.Submit_for_SL_approval_on_GCI__c==false || Test.isRunningTest()) {  Task objT14 = new Task();  objT14.whatId = opp.Id; objT14.subject = 'Submit GCI tools for SL review for deals >$10MM';
                objT14.OwnerId=opp.OwnerId;                                                                                  
                objT14.Status='Not Started';
                objT14.AutocreatedTask__c=true;
                taskToInsert.add(objT14);
              
             }
                

            if(opp.StageName == '2. Define' || Test.isRunningTest()) {
                Task objT6 = new Task();
                objT6.whatId = opp.Id;
               objT6.OwnerId=opp.OwnerId;
                objT6.subject = 'Move the Opportunity to 3. On Bid stage';
                objT6.Status='Not Started';
                objT6.AutocreatedTask__c=true;  
                taskToInsert.add(objT6);
            }
            



             
            }
            insert taskToInsert;
         
            
        
        
    }
  
             
             
        
        public static void removeTaskforOpponUpdate(List<Opportunity> oppList) 
        {         
            system.debug('ABC!!'+oppList);          
           list <task> taskToUpdate = [select Id,Status,Subject,AutocreatedTask__c from Task where WhatId in:oppList];
             system.debug('ABCDEF!!'+taskToUpdate);             
             List <Task> tsk_Update = new List<Task>();
            for(Opportunity oppty : oppList)
            {          
                if(oppty.StageName=='1. Discover')
                {                               
                     for(Task t:taskToUpdate)
                     {                     
                          if(t.Status!='Completed' && t.subject=='Convert to Discover Opportunity or Drop the PreDiscover Opportunity depending on the outcomes of the meetings with the client')
                          { 
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                         }
                    }
               }
           
            system.debug('+++++TESTING131+++++++!!'+oppty.Product_Count__c);  
              if(oppty.Product_Count__c>=1 || oppty.Service_Line_Savo__c!= null)
              {              
                   for(Task t:taskToUpdate)
                     {
                        system.debug('+++++TESTING121+++++++!!'+t.AutocreatedTask__c);
                        if(t.Status!='Completed' && t.subject=='Please add the Products')
                        {
                            t.status='Completed';
                            tsk_Update.add(t);
                            //update t;
                        }
                    }
              } 
            if(oppty.Partner_Count__c>0)
            {
                for(Task t:taskToUpdate)
                {
                     if(t.Status!='Completed' && t.subject=='Please enter Partner details to move deal to define')
                     { 
                        t.status='Completed'; 
                        tsk_Update.add(t);
                        //update t;
                     }
                }                
            }    
                         
              if(oppty.StageName=='2. Define')
              { 
                for(Task t:taskToUpdate) 
                  {
                    if(t.Status!='Completed' && t.subject=='Create QSRM and Post approval, the deal will move to Define')
                        { 
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                        }                                
                  }
               }                 
                             
              if(oppty.Annuity_Project__c!=NULL)
              {              
                 for(Task t:taskToUpdate)
                 {                       
                      if(t.Status!='Completed' && t.subject=='Fill all the data fields which are needed for a deal to move to Define Stage (Deal Type and Deal Administrator)')
                        { 
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                        }                                
                 }
               }          
                             
              if(oppty.Submit_for_SL_approval_on_GCI__c==true)
              {              
                 for(Task t:taskToUpdate)
                 {                 
                      if(t.Status!='Completed' && t.subject=='Complete deal governance for deals >$10MM')
                        {
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                        }                           
                 }
               }
            
            if(oppty.Submit_for_SL_approval_on_GCI__c==true)
            {
                 for(Task t:taskToUpdate)
                 {                 
                      if(t.Status!='Completed' && t.subject=='Submit GCI tools for SL review for deals >$10MM')
                        {
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                        }               
                }
           }
            if(oppty.Summary_of_opportunity__c!=NULL)
            {         
                 for(Task t:taskToUpdate)
                 {                 
                      if(t.Status!='Completed' && t.subject=='For deals>10M, Client SCV section is mandatory to fill in.')
                        { 
                            t.status='Completed'; 
                            tsk_Update.add(t);
                            //update t;
                        }                       
                 }
            }                
              if(oppty.Margin__c!=NULL)
              { 
                    for(Task t:taskToUpdate)
                    { 
                        if(t.Status!='Completed' && t.subject=='Fill EBIT%')
                            { 
                                t.status='Completed'; 
                                tsk_Update.add(t);
                                //update t;
                            }                                
                    }
                       if(oppty.GCI_Coach__c!=NULL)
                       { 
                            for(Task t:taskToUpdate) 
                                { 
                                    if(t.Status!='Completed' && t.subject=='Fill GCI Coach')
                                    { 
                                        t.status='Completed'; 
                                        tsk_Update.add(t);
                                        //update t;
                                    }                               
                                }
                       }
                      system.debug('+++++TESTING11+++++++!!'+oppty.StageName);   
                      if(oppty.StageName=='3. On Bid')
                      {
                        for(Task t:taskToUpdate)
                         {
                            system.debug('+++++TESTING11+++++++!!'+t.subject); 
                              if(t.Status!='Completed' && t.subject=='Move the Opportunity to 3. On Bid stage')
                              { 
                                  t.status='Completed'; 
                                  tsk_Update.add(t);
                                  //update t;
                              }
                          }
                        }
            
                    if(oppty.StageName=='4. Down Select')
                        { 
                            for(Task t:taskToUpdate) 
                                {
                                    if(t.Status!='Completed' && t.subject=='Move the Opportunity to 4. Down Select stage')
                                        { 
                                            t.status='Completed'; 
                                            tsk_Update.add(t);
                                            //update t;
                                        }
                                }
                       }                        
          
                      if(oppty.Contract_type__c!=NULL)
                      {               
                         for(Task t:taskToUpdate)
                         {
                           
                              if(t.Status!='Completed' && t.subject=='Update Contract Type and Pricer name')
                                { 
                                    t.status='Completed'; 
                                    tsk_Update.add(t);
                                    //update t;
                                }
                        }
                     }
                  if(oppty.StageName=='5. Confirmed')
                    { 
                        for(Task t:taskToUpdate) 
                            { 
                                if(t.Status!='Completed' && t.subject=='Move the Opportuninty to 5. Confirmed stage')
                                    { 
                                        t.status='Completed'; 
                                        tsk_Update.add(t);
                                        //update t;
                                    }
                            }
                       } 
              }
              if(oppty.Number_of_Contract__c>0)
              {              
                     for(Task t:taskToUpdate)
                     {                       
                          if(t.Status!='Completed' && t.subject=='Create and upload signed Contract')
                            {
                                t.status='Completed'; 
                                tsk_Update.add(t);
                                //update t;
                            }
                     }
               }             
            
            if(oppty.stageName=='6. Signed Deal')
            { 
                for(Task t:taskToUpdate) 
                    { 
                        if(t.Status!='Completed' && t.subject=='Complete Win/Loss reason workflow')
                            { 
                                t.status='Completed'; 
                                tsk_Update.add(t);
                                //update t;
                            }
                    }
           } 
          //  update taskToUpdate;
        }
         update tsk_Update;
    
    }
}