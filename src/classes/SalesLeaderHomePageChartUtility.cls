public class SalesLeaderHomePageChartUtility 
{
    public static List<ChartData> fetchChartDataForOpportunityTeamMembers()
    {
        string userId =  userInfo.getUserId().substring(0, 15);
        List<Opportunity> oppList = [select Id,stageName,(Select Id ,UserId from OpportunityTeamMembers) from Opportunity 
                                     where (Opportunity.Account.Sales_Leader_User_id__c=:userId or 
                                     Opportunity.Account.Client_Partner__c=:userId ) and stagename = '6. Signed Deal'
                                     And CreatedDate = THIS_YEAR limit: Limits.getLimitQueryRows()];
        Set<Id> teamMemberUserIds = new Set<Id>();
        List<AggregateResult> opportunitySplitResult;
        List<AggregateResult> quotaResults;
        List<ChartData> chartDataObjList = new List<ChartData>();
        Map<id,User> userIdMap;
        if(oppList.size() > 0)
        {
            for(Opportunity opp: oppList)
            {
                for(OpportunityTeamMember oppTeamMember : opp.OpportunityTeamMembers)
                {
                    teamMemberUserIds.add(oppTeamMember.UserId);
                }          
            } 
        }
        if(teamMemberUserIds.size() > 0)
        {
         	opportunitySplitResult = [Select SplitOwnerId,SUM(SplitAmount) sumSplitAmount From OpportunitySplit where SplitOwnerId in: teamMemberUserIds group by SplitOwnerId];
			quotaResults = [SELECT QuotaOwnerId,SUM(QuotaAmount) sumQuotaAmount FROM ForecastingQuota where QuotaOwnerId in: teamMemberUserIds And startdate = THIS_YEAR group by QuotaOwnerId];
            userIdMap = new Map<Id,User>([Select Id,Name from User where id in: teamMemberUserIds]);
        }
		if(opportunitySplitResult.size() > 0)
        {
            for(AggregateResult arSplit: opportunitySplitResult)
            {
                ChartData chartDataObj = new chartData();
                chartDataObj.name = userIdMap.get((Id)arSplit.get('SplitOwnerId')).Name;
                chartDataObj.TCV = (Decimal) arSplit.get('sumSplitAmount');
                chartDataObj.Quota = 0;
                if(quotaResults.size() > 0)
                {
                    for(AggregateResult arQuota: quotaResults)
                    {
                        if(arSplit.get('SplitOwnerId') == arQuota.get('QuotaOwnerId'))
                        {
                            chartDataObj.total = (Decimal) arSplit.get('sumSplitAmount');
                            chartDataObj.Quota = (Decimal) arQuota.get('sumQuotaAmount')*10 - (Decimal) arSplit.get('sumSplitAmount');
                        }                            
                    }
                }
                chartDataObjList.add(chartDataObj);
            }
        }
        return chartDataObjList;
    }
    @AuraEnabled
    public static ChartDataWrapper fetchChartDataWrapper()
    {
        ChartDataWrapper wrapper = new ChartDataWrapper();
        wrapper.dataWrapper = SalesLeaderHomePageChartUtility.fetchChartDataForOpportunityTeamMembers();
        wrapper.chartTitle = 'Team Performance';
        System.debug(wrapper);
        return wrapper;
    }
    public class ChartData{
        @AuraEnabled public String name;
        @AuraEnabled public Decimal TCV;
        @AuraEnabled public Decimal Quota;
        @AuraEnabled public Decimal total;
    }
    public class ChartDataWrapper{
        @AuraEnabled public List<ChartData> dataWrapper;
        @AuraEnabled public String chartTitle;
    }
}