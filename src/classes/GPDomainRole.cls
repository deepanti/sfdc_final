public class GPDomainRole extends fflib_SObjectDomain {

    public GPDomainRole(List < GP_Role__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainRole(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Role__c.SObjectType,
            GP_User_Role__c.SObjectType
    };

    public override void onBeforeInsert() {
        setDealAccessGroupName(null);
    }

    public override void onAfterInsert() {
        createGroup(null);
        //addGroupMembers(null);
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        setDealAccessGroupName(oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        createGroup(oldSObjectMap);
        //addGroupMembers(oldSObjectMap);
        activateandDeactivateUserRole(oldSObjectMap, uow);

        uow.commitWork();
    }
    public override void onValidate() {
        // Validate GP_Role__c 
        //for(GP_Role__c  ObjProject : (List<GP_Role__c >) Records) {
        //  }
    }

    public override void onApplyDefaults() {

        // Apply defaults to GP_Role__c 
        //for(GP_Role__c  ObjProject : (List<GP_Role__c >) Records) { 
        //}               
    }

    public void FuntionalMethod1(Map < Id, SOBject > oldSObjectMap) {
        //for(GP_Role__c  ObjProject : (List<GP_Role__c >)records){ 
        //}
    }

    private void setDealAccessGroupName(Map < Id, SOBject > oldSObjectMap) {
        /*boolean doProceed = false;
        for(GP_Role__c objRole : (List<GP_Role__c >)records)
        {
            if(oldSObjectMap != null && objRole.GP_Work_Location_SDO_Master__c != oldSObjectMap.get(objRole.id).get('GP_Work_Location_SDO_Master__c'))
            {
                doProceed = true;
                break;
            }
        }
        
        if(!doProceed) return;*/

        list < GP_Business_Type_Name_Mapping__mdt > lstBusinessMapping = [select id, DeveloperName, Masterlabel, GP_Business_Type__c, GP_Business_Name__c
            from GP_Business_Type_Name_Mapping__mdt
        ];

        map < string, GP_Business_Type_Name_Mapping__mdt > mapMapping = new map < string, GP_Business_Type_Name_Mapping__mdt > ();
        if (lstBusinessMapping != null && lstBusinessMapping.size() > 0) {
            for (GP_Business_Type_Name_Mapping__mdt objMDT: lstBusinessMapping) {
                mapMapping.put(objMDT.GP_Business_Name__c != null ? objMDT.GP_Business_Name__c : '', objMDT);
            }
        }

        string key;
        for (GP_Role__c objRole: (List < GP_Role__c > ) records) {
            key = objRole.GP_Business_Name__c != null ? objRole.GP_Business_Name__c : '';

            if (mapMapping != null && mapMapping.get(key) != null) {
                objRole.GP_Deal_Access_Group_Name__c = 'GP_Deal_' + mapMapping.get(key).DeveloperName + system.Label.GP_Group_Read_Label;
            }

            if (objRole.GP_Type_of_Role__c == 'SDO' && objRole.GP_Work_Location_SDO_Master__c != null) {
                if (objRole.GP_Role_Category__c == 'PID Creation' || objRole.GP_Role_Category__c == 'Read Only') {
                    objRole.GP_Group_Name_for_Read_Access__c = 'GP_SDO_' + objRole.GP_Work_Location_SDO_Master__c + '_RA';
                } else if (objRole.GP_Role_Category__c == 'PID Approver' || objRole.GP_Role_Category__c == 'FP&A Approver') {
                    objRole.GP_Group_Name_for_Read_Access__c = 'GP_SDO_APPROVER_' + objRole.GP_Work_Location_SDO_Master__c + '_RA';
                }
            } else if (objRole.GP_Type_of_Role__c == 'Global') {
                if (objRole.GP_Role_Category__c == 'BPR Approver' || objRole.GP_Role_Category__c == 'MF Approver' || objRole.GP_Role_Category__c == 'Read Only' ||
                    objRole.GP_Role_Category__c == 'Controllership Approver' || objRole.GP_Role_Category__c == 'Band 3 Approver' ||
                    objRole.GP_Role_Category__c == 'Product Approver') {
                    objRole.GP_Group_Name_for_Read_Access__c = 'GP_GLOBAL_APPROVER_RA';
                }
                if (objRole.GP_Role_Category__c == 'TSC') {
                    objRole.GP_Group_Name_for_Read_Access__c = 'GP_TIMESHEET_CREATOR_RA';
                }
            } else if (objRole.GP_Type_of_Role__c == 'Customer L4' && objRole.GP_Customer_L4__c != null && objRole.GP_Role_Category__c == 'Read Only') {
                objRole.GP_Group_Name_for_Read_Access__c = 'GP_L4_' + objRole.GP_Customer_L4__c + '_RA';
            } else if (objRole.GP_Type_of_Role__c == 'HSL Master' && objRole.GP_HSL_Master__c != null && objRole.GP_Role_Category__c == 'Read Only') {
                objRole.GP_Group_Name_for_Read_Access__c = 'GP_HSL_' + objRole.GP_HSL_Master__c + '_RA';
            }
        }
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: Method is used to Create a Group Member when a role gets Active       
    // ------------------------------------------------------------------------------------------------
    // Input Param:: Map<id, GP_Role__c> oldSObjectMap
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-OCT-2017  Created By::Salil  Email:: Salil.Sharma@saasfocus.com 
    // ------------------------------------------------------------------------------------------------
    // 
    // As this method is not called from anywhere hence it is commented on 9- May -2018 By Mandeep Chauhan
    /*public void addGroupMembers(Map < Id, SObject > oldSObjectMap) {
        set < ID > SetofId = new Set < ID > ();
        GP_Role__c objOldGR;

        for (GP_Role__c objRole: (List < GP_Role__c > ) records) {
            if (oldSObjectMap != null)
                objOldGR = (GP_Role__c) oldSObjectMap.get(objRole.Id);
            else
                objOldGR = null;

            if (objRole.GP_Active__c && (objOldGR == null || (objOldGR != null && !objOldGR.GP_Active__c))) {
                Setofid.add(objRole.id);
            }
        }
        if (Setofid != null && Setofid.size() > 0) {
            //map<Id, GP_User_Role__c> mapUserRole = new map<Id, GP_User_Role__c>([select id from GP_User_Role__c where 
            //GP_Active__c = true and 
            //GP_Role__c in: Setofid]);
            map < Id, GP_User_Role__c > mapUserRole = new map < Id, GP_User_Role__c > (new GPSelectorUserRole().selectByRoleId(Setofid));
            if (mapUserRole != null && mapUserRole.size() > 0) {
                GPCommon.addUserRole(mapUserRole.keySet());
            }
        }
    }*/

    // ------------------------------------------------------------------------------------------------ 
    // Description: Method is used to Inactive al the user role when role gets inactive       
    // ------------------------------------------------------------------------------------------------
    // Input Param:: fflib_SObjectUnitOfWork uow, Map<id, GP_Role__c> oldSObjectMap
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::30-OCT-2017  Created By::Salil  Email: Salil.Sharma@saasfocus.com 
    // ------------------------------------------------------------------------------------------------ 

    public void activateandDeactivateUserRole(Map < Id, SObject > oldSObjectMap, fflib_SObjectUnitOfWork uow) {
        set < ID > SetofId = new Set < ID > ();

        for (GP_Role__c objRole: (List < GP_Role__c > ) records) {
            GP_Role__c objOldGR = (GP_Role__c) oldSObjectMap.get(objRole.Id);

            if ((!objRole.GP_Active__c && objOldGR.GP_Active__c) || (objRole.GP_Active__c && !objOldGR.GP_Active__c)) {
                Setofid.add(objRole.id);
            }
        }
        if (Setofid != null && Setofid.size() > 0) {
            list < GP_User_Role__c > lstUserRole = new list < GP_User_Role__c > (new GPSelectorUserRole().selectByRoleId(Setofid));
            if (lstUserRole != null && lstUserRole.size() > 0) {
                map < id, GP_Role__c > mpNewRoleMap = new map < id, GP_Role__c > ((List < GP_Role__c > ) records);
                for (GP_User_Role__c objUserRole: lstUserRole) {
                    if (mpNewRoleMap.get(objUserRole.GP_Role__c) != null) {
                        objUserRole.GP_Active__c = mpNewRoleMap.get(objUserRole.GP_Role__c).GP_Active__c;
                    }
                }
                //update lstUserRole;
                uow.registerDirty(lstUserRole);
            }
        }
    }

    private void createGroup(Map < Id, SObject > oldSObjectMap) {
        if (records != null && records.size() > 0) {
            set < Id > setOfRoleId = new set < Id > ();
            GP_Role__c objOldRole;
            for (GP_Role__c objRole: (List < GP_Role__c > ) records) {
                if (oldSObjectMap != null)
                    objOldRole = (GP_Role__c) oldSObjectMap.get(objRole.Id);
                else
                    objOldRole = null;

                if (objRole.GP_Work_Location_SDO_Master__c != null && (objOldRole == null || (objOldRole != null &&
                        objRole.GP_Work_Location_SDO_Master__c != objOldRole.GP_Work_Location_SDO_Master__c))) {
                    setOfRoleId.add(objRole.Id);
                }

                if (objRole.GP_Customer_L4__c != null && (objOldRole == null || (objOldRole != null &&
                        objRole.GP_Customer_L4__c != objOldRole.GP_Customer_L4__c))) {
                    setOfRoleId.add(objRole.Id);
                }

                if (objRole.GP_HSL_Master__c != null && (objOldRole == null || (objOldRole != null &&
                        objRole.GP_HSL_Master__c != objOldRole.GP_HSL_Master__c))) {
                    setOfRoleId.add(objRole.Id);
                }

                // Added for GP_Business_Type__c and GP_Business_Name__c
                //if(objRole.GP_Business_Type__c != null && objRole.GP_Business_Name__c != null && (objOldRole == null || (objOldRole != null && 
                //    (objRole.GP_Business_Type__c != objOldRole.GP_Business_Type__c || objRole.GP_Business_Name__c != objOldRole.GP_Business_Name__c ))))
                //{
                //     setOfRoleId.add(objRole.Id);
                //}


            }

            if (setOfRoleId != null && setOfRoleId.size() > 0) {
                GPServiceRole.insertGroup(setOfRoleId);
            }
            //map<Id, GP_Role__c> mapOfNew = (map<Id, GP_Role__c>)records;
            //GPServiceRole.insertGroup(mapOfNew.keySet());
        }
    }
}