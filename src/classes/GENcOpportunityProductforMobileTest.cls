/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class GENcOpportunityProductforMobileTest { 

    public static testMethod void myUnitTest() {
    test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test111',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunity);
        GENcOpportunityProductforMobile  oGENcOpportunityProduct  = new GENcOpportunityProductforMobile(std);
        test.stoptest();
    
        
    }
   
    public static testMethod void RunTest2()
    {test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test121',oAccount.Id,oContact.Id);
        
        OpportunityProduct__c oOpportunityProduct   =GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,18,18000);
        /*
        OpportunityProduct__c oOpportunityProduct = new OpportunityProduct__c();
        oOpportunityProduct.Product_Family_OLI__c = 'Analytics';
        oOpportunityProduct.Product__c = oProduct.Id;
        oOpportunityProduct.COE__c = 'ANALYTICS';
        oOpportunityProduct.P_L_SUB_BUSINESS__c = 'Analytics';
        oOpportunityProduct.DeliveryLocation__c = 'Americas';
        oOpportunityProduct.SEP__c = 'SEP Opportunity';
        oOpportunityProduct.LocalCurrency__c = 'INR';
        oOpportunityProduct.RevenueStartDate__c = System.today().adddays(1);
        oOpportunityProduct.ContractTerminmonths__c = 18;
        oOpportunityProduct.SalesExecutive__c = UserInfo.getUserId ();
        oOpportunityProduct.Quarterly_FTE_1st_month__c = 2;
        oOpportunityProduct.FTE_4th_month__c =2;
        oOpportunityProduct.FTE_7th_month__c =2;
        oOpportunityProduct.FTE_10th_month__c =2;
        oOpportunityProduct.OpportunityId__c = oOpportunity.Id;
        oOpportunityProduct.TCVLocal__c =18000;
       // oOpportunityProduct.TNYR__c=0.00;
        //oOpportunityProduct.I_have_reviewed_the_Monthly_Breakups__c =true;
        oOpportunityProduct.HasRevenueSchedule__c=true;
        oOpportunityProduct.HasSchedule__c=true;
        oOpportunityProduct.HasQuantitySchedule__c=false;
        insert oOpportunityProduct;
        */
        
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunity);
        GENcOpportunityProductforMobile  oGENcOpportunityProduct  = new GENcOpportunityProductforMobile(std);
        ApexPages.currentpage().getParameters().put('id',oOpportunity.id);
        oGENcOpportunityProduct.getNoOfInstallment();
        RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,6,12000);
        RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,6,1,6000);
        ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
        
        oGENcOpportunityProduct.theOpportunity = new Opportunity();
        oGENcOpportunityProduct.theOpportunity = oOpportunity;
        
        GENcOpportunityProductforMobile.RevenueRollingYearWrapper  oRevenueRollingYearWrapper1  = new GENcOpportunityProductforMobile.RevenueRollingYearWrapper(1);
        oRevenueRollingYearWrapper1.revSchedule = oRevenueSchedule;
        GENcOpportunityProductforMobile.ProductScheduleWrapper oProductScheduleWrapper= new GENcOpportunityProductforMobile.ProductScheduleWrapper(null,1);
        oGENcOpportunityProduct.getRevenueRollingYear();
        oRevenueRollingYearWrapper1.rollYear= 12000;
        GENcOpportunityProductforMobile.RevenueRollingYearWrapper  oRevenueRollingYearWrapper2  = new GENcOpportunityProductforMobile.RevenueRollingYearWrapper(2);
        oRevenueRollingYearWrapper2.revSchedule = oRevenueSchedule1;
        oRevenueRollingYearWrapper2.rollYear= 6000;
        oGENcOpportunityProduct.revenueRYWrapperList.add(oRevenueRollingYearWrapper1);
        oGENcOpportunityProduct.revenueRYWrapperList.add(oRevenueRollingYearWrapper2);
        oRevenueRollingYearWrapper1.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper1.setHasAutoPopulateDisabled(true);
        oRevenueRollingYearWrapper2.getHasAutoPopulateDisabled();
        oRevenueRollingYearWrapper2.setHasAutoPopulateDisabled(true);
        //oGENcOpportunityProduct.getUnitPrice();
        oGENcOpportunityProduct.ProdObj = new product2();
        oGENcOpportunityProduct.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct.ProdObj.Service_Line__c = '33';
        oGENcOpportunityProduct.opportunityProduct.Product_Autonumber__c='OLI';
        //oGENcOpportunityProduct.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct.ProdObj.Product_Family__c='IT Managed Services';
        oGENcOpportunityProduct.disableProduct();
        oGENcOpportunityProduct.ProfilesOptionsFetch();
        //oGENcOpportunityProduct.getautoNumber();
        //oGENcOpportunityProduct.getServiceOptions();
        oGENcOpportunityProduct.getCurrencyIsoCodes();
        oGENcOpportunityProduct.showConvertedValue();
        oGENcOpportunityProduct.convertCurrency();
        oGENcOpportunityProduct.updateSchedule();
        oGENcOpportunityProduct.revenueRollingYearToDelete =1;
        oGENcOpportunityProduct.deleteProductSchedule();
        oGENcOpportunityProduct.revenueRollingYearIndex=1;
        oGENcOpportunityProduct.createSchedule();
        oGENcOpportunityProduct.revenuedate_update();
        //oGENcOpportunityProduct.getTransitionRevenueUSD();
        oGENcOpportunityProduct.getHasScheculeSectionRendered();
        oGENcOpportunityProduct.setHasScheculeSectionRendered(true);
        oGENcOpportunityProduct.getRevenue(12.00,13.00);
        oGENcOpportunityProduct.getSkillSetRendered();
        oGENcOpportunityProduct.setSkillSetRendered(true);
        //oGENcOpportunityProduct.getPriorityTypeRendered();
        //oGENcOpportunityProduct.setPriorityTypeRendered(true);
        test.stoptest();
    }
    public static testMethod void RunTest3()
    {    
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test1011',oAccount.Id,oContact.Id);
        
        OpportunityTeamMember oOpportunityTeamMember= GEN_Util_Test_Data.CreateOpportunityTeamMember(oOpportunity.id,Userinfo.getUserId());
        ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunity);
        GENcOpportunityProductforMobile  oGENcOpportunityProduct1  = new GENcOpportunityProductforMobile(std1);
        test.starttest();
        oGENcOpportunityProduct1.theOpportunity = new Opportunity();
        oGENcOpportunityProduct1.theOpportunity = oOpportunity;
        oGENcOpportunityProduct1.ProdObj = new product2();
        oGENcOpportunityProduct1.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct1.ProdObj.Service_Line_Category__c = '1';
      
        oGENcOpportunityProduct1.ProdObj.Service_Line__c = '33';
        //oGENcOpportunityProduct1.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct1.ProdObj.Product_Family__c='IT Managed Services';
        oGENcOpportunityProduct1.disableProduct();
        oGENcOpportunityProduct1.ProfilesOptionsFetch();
        oGENcOpportunityProduct1.getBDRepOptions();
        oGENcOpportunityProduct1.getCurrencyIsoCodes();
        oGENcOpportunityProduct1.opportunityProduct.LocalCurrency__c='INR';
        oGENcOpportunityProduct1.opportunityProduct.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        oGENcOpportunityProduct1.opportunityProduct.RevenueStartDate__c = System.today().adddays(1);
        oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c= 18000;
        oGENcOpportunityProduct1.showConvertedValue();
        oGENcOpportunityProduct1.opportunityProduct.ContractTerminmonths__c = 12;
        oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
        oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 18000;
        oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToEdit=1;
        oGENcOpportunityProduct1.updateSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToDelete=1;
        oGENcOpportunityProduct1.deleteProductSchedule();

        oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
        oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 18000;
        oGENcOpportunityProduct1.createSchedule();
       // oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.opportunityProduct.TransitionRevenueLocal__c=5000;
        oGENcOpportunityProduct1.totalScheduleValue = math.round(oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c);
        oGENcOpportunityProduct1.saveLineItem();
        oGENcOpportunityProduct1.createRevProdList(oGENcOpportunityProduct1.revenueRYWrapperList);
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        oGENcOpportunityProduct1.saveAndNewLineItem();
        //oGENcOpportunityProduct1.saveandnew();
        oGENcOpportunityProduct1.addServiceLine();
        oGENcOpportunityProduct1.revenuedate_update();
        test.stoptest();
    }
    
    public static testMethod void RunTest4()
    {    
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('IT Managed Services');
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test11100',oAccount.Id,oContact.Id);
        ApexPages.StandardController std1 = new ApexPages.StandardController(oOpportunity);
        
        GENcOpportunityProductforMobile  oGENcOpportunityProduct1  = new GENcOpportunityProductforMobile(std1);
        oGENcOpportunityProduct1.theOpportunity = new Opportunity();
        oGENcOpportunityProduct1.theOpportunity = oOpportunity ;
        oGENcOpportunityProduct1.ProdObj = new product2();
        oGENcOpportunityProduct1.ProdObj.Industry_Vertical__c='BFS';
        oGENcOpportunityProduct1.ProdObj.Service_Line_Category__c = '1';
        oGENcOpportunityProduct1.ProdObj.Service_Line__c = '33';
       // oGENcOpportunityProduct1.ProdObj.Product_Group__c = 'IT Services';
        oGENcOpportunityProduct1.ProdObj.Product_Family__c='IT Managed Services';
        
        
        oGENcOpportunityProduct1.opportunityProduct.LocalCurrency__c='INR';
        oGENcOpportunityProduct1.opportunityProduct.Product_Autonumber__c='OLI';
        oGENcOpportunityProduct1.opportunityProduct.Product__c = oProduct.Id;
        oGENcOpportunityProduct1.opportunityProduct.RevenueStartDate__c = System.today();
        oGENcOpportunityProduct1.opportunityProduct.TCVLocal__c= 24000;
        oGENcOpportunityProduct1.opportunityProduct.ContractTerminmonths__c = 24;
      //  oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
        
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
      //  oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearIndex =1;
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
       // oGENcOpportunityProduct1.createSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToEdit=1;
        oGENcOpportunityProduct1.updateSchedule();
        oGENcOpportunityProduct1.revenueRollingYearToDelete=1;
        oGENcOpportunityProduct1.deleteProductSchedule();

        oGENcOpportunityProduct1.getRevenueRollingYear();
        oGENcOpportunityProduct1.revenueRollingYearIndex =0;
       // oGENcOpportunityProduct1.revenueRYWrapperList[0].revSchedule.RollingYearRevenueLocal__c= 12000;
        oGENcOpportunityProduct1.createSchedule();
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        //oGENcOpportunityProduct1.opportunityProduct.TransitionRevenueLocal__c=5000;
        //oGENcOpportunityProduct1.getTransitionRevenueUSD();
        oGENcOpportunityProduct1.saveLineItem();
        //oGENcOpportunityProduct1.opportunityProduct.I_have_reviewed_the_Monthly_Breakups__c = true;
        oGENcOpportunityProduct1.saveAndNewLineItem();
        oGENcOpportunityProduct1.revenueScheduleList = new List<RevenueSchedule__c>();
        oGENcOpportunityProduct1.tempServiceCategory = 'a';
        oGENcOpportunityProduct1.ServiceLine_lst();
        oGENcOpportunityProduct1.ProductFamilyFromCatalog();
        //oGENcOpportunityProduct1.getautoNumber();
        List<ProductSchedule__c> ps  = new List<ProductSchedule__c>();
       // ps.add(new ProductSchedule__c(OpportunityProductId__c = oGENcOpportunityProduct1 ));
       // oGENcOpportunityProduct1.saveopty(ps);
        oGENcOpportunityProduct1.deleteProductSchedule1();
        oGENcOpportunityProduct1.getPricemarginRendered();
        oGENcOpportunityProduct1.setPricemarginRendered(true, true);
        oGENcOpportunityProduct1.getPriorityTypeRendered();
        oGENcOpportunityProduct1.setPriorityTypeRendered(true);
        
    }
    

}