public class GPDomainPinnacleMaster extends fflib_SObjectDomain {

    public GPDomainPinnacleMaster(List < GP_Pinnacle_Master__c  > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainPinnacleMaster(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
            GP_Project__c.SObjectType,
            GP_Pinnacle_Master__c.SObjectType
    };

    public override void onBeforeInsert() {
    
     fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
     assigneRecordType();
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);

        //uow.commitWork();
    }

    public override void onValidate() {
        // Validate GP_Pinnacle_Master__c 
        //for (GP_Pinnacle_Master__c ObjProject: (List < GP_Pinnacle_Master__c > ) Records) {}
    }

    public override void onApplyDefaults() {

        // Apply defaults to GP_Pinnacle_Master__c 
        //for(GP_Pinnacle_Master__c  ObjProject : (List<GP_Pinnacle_Master__c >) Records) { 
        //}               
    }
    
    
    // ------------------------------------------------------------------------------------------------ 
    // Description: Method is used to assign Standard RecordTypeId based o Record Type Name   
    // ------------------------------------------------------------------------------------------------
    // Input Param: NA
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::16-May -2018  Created By:Sumit
    // ------------------------------------------------------------------------------------------------ 
   
    private void assigneRecordType() {
        for (GP_Pinnacle_Master__c objPM: (List < GP_Pinnacle_Master__c > ) Records) {
            
            if (objPM.GP_Record_Type_Name__c != null) {
                
               objPM.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get(objPM.GP_Record_Type_Name__c).getRecordTypeId();

            }
        }
    }

   
}