public class Redirect_Qsrm_cls {
    PageReference ReturnPage {get;set;}
    public Redirect_Qsrm_cls(ApexPages.StandardController controller) 
    {       
        Map<String,String> recmap=new Map<String,String>();
        String RecordTypeid,namefldid,QsrmEntityid,username,oppid;
        RecordTypeid=namefldid=QsrmEntityid=username=oppid='';
        Boolean ContactRole = false;
        
        //Creating Recordtype map
        for(recordtype rec : [select id,DeveloperName from recordtype where sobjecttype='QSRM__c'])
            recmap.put(rec.DeveloperName,rec.id);
        
        //Getting Field ids from Custom Setting
        Map<String, QSRM_fieldid__c> QSRM_fieldidmap= QSRM_fieldid__c.getAll(); 
        if(QSRM_fieldidmap.keyset().size()>0)
        {
            namefldid = QSRM_fieldidmap.get('QSRMName').Field_ID__c;
            QsrmEntityid = QSRM_fieldidmap.get('QSRMEntityid').Field_ID__c;       
            
            username=ApexPages.currentPage().getParameters().get(namefldid);
            oppid=ApexPages.currentPage().getParameters().get(namefldid+'_lkid');
        }
        
        
        List<Opportunity> QH_record=[select id,TCV1__c,QSRM_Type__c,Type_Of_Opportunity__c,Product_Count__c from Opportunity where id=:oppid];        
        
        //system.debug('QH_record:'QH_record[0].OpportunityContactRoles[0]);
        
        system.debug('loop1else');
        
        if (QH_record[0].QSRM_Type__c == 'TSQSRM')
        {
            //if (QH_record[0].Type_Of_Opportunity__c == 'TS'){
            system.debug('need to come');
            RecordTypeid = recmap.get('TS_QSRM');   
        } 
        else
            RecordTypeid = recmap.get('Detail_Record_Type');
        
        
        
        
        IF(QH_record.size()>0)
        {
            
            if(QH_record[0].Product_Count__c < 1)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'No product line found in opportunity'));
                // return null;
                ReturnPage = new PageReference('/apex/errorPage?id='+QH_record[0].id); 
            }
            
            else
            {   
                if(!test.isRunningTest())
                {
                    system.debug('loop2else');
                    ReturnPage = new PageReference('/a07/e?'); 
                    ReturnPage.getParameters().put(namefldid,username);
                    ReturnPage.getParameters().put(namefldid+'_lkid',oppid);
                    ReturnPage.getParameters().put('RecordType',RecordTypeid);
                    ReturnPage.getParameters().put('ent',QsrmEntityid);
                    ReturnPage.getParameters().put('retURL',oppid);
                    ReturnPage.getParameters().put('nooverride','1');// Important to avoid recursion  
                    system.debug('ReturnPage:' + ReturnPage);
                    
                }
            }
        }
        
       if (true)
       {
          if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
        if (true)
       {
           
       }   
       }   
       }   
       }   
       }   
       }   
       }   
       }   
       }   
       } 
       }
    }
    //Called onload to redirect to standard quota setup edit page
    public pagereference onloadredirect()
    {  
        Try
        {

            system.debug('ReturnPageonloadredirect:' + ReturnPage);
            ReturnPage.setRedirect(true);
            return ReturnPage;
        }
        Catch(exception e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Enter Product Information'));
            return null;
        }
        
     }
}