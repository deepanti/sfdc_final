public class InsertOppsplit {
    private static List<AggregateResult> opp_line_item_list;
    private static List<ID> BDRepId = new List<Id>();
    private static List<ID> BDRepsId = new List<Id>();
    public static List<AggregateResult> oliList;
    public static  DECIMAL total_used;
    public static  DECIMAL totalTCV;
    public static void InsertSuperSLOppSplitOwner( List<Account> accountList,Map<ID,Account> old_acc_map) 
    { 
        
        
        List <OpportunityTeamMember> OTM_list = new List<OpportunityTeamMember>();
        List <OpportunitySplit> splitToInsert = new List<OpportunitySplit>();
        OpportunitySplitType opportunity_split_type = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
        Set<ID> bd_Reps=new Set<Id>();
        Set<Id> splitownerIDs = new Set<ID>();
        Set<ID> opids=new Set<Id>();
        Set<Id> SuperSLIds= new Set<Id>();
        Set<Id> acctids=new Set<Id>();
        Set<Id> salesUnitIDs=new Set<Id>();
        Set<Id> SalesLeadersIds=new Set<Id>();
        
        For(Account acc:accountList)
        {
            Account oldacc = old_acc_map.get(acc.Id);
            acctids.add(acc.id);
            salesUnitIDs.add(acc.Sales_Unit__c);
            BDRepId.add(oldacc.Client_Partner__c);
        }
        
        List<Opportunity> opplist=[Select id,OwnerId,CloseDate,TCV1__c,Product_Count__c ,Account.Client_Partner__c,Opportunity_Source__c,account.Sales_Leader_User_Id__c, Amount,StageName,
                                   Account.Sales_Unit__c from Opportunity where AccountId In:acctids AND Isclosed=false];
        for(Opportunity opp:oppList)
        {
            opids.add(opp.id);
        }

       try{
           if(opplist.size()>0)
           {
          for(Account acc:accountList)
        {
            Account oldacc = old_acc_map.get(acc.Id);
            system.debug('=====1x====='+oldacc.Sales_Unit__c);
            system.debug('=====2x====='+acc.Sales_Unit__c);
            for(Opportunity opp:opplist)
            {
				if(acc.Client_Partner__c!=oldacc.Client_Partner__c && oldacc.Client_Partner__c == acc.Sales_Leader_User_Id__c && oldacc.Sales_Leader_User_Id__c==acc.Sales_Leader_User_Id__c) { 
                    OpportunitySplit oppty_split = new OpportunitySplit();  oppty_split.SplitPercentage = 100; oppty_split.SplitTypeId = opportunity_split_type.id; 
                    oppty_split.SplitOwnerId = acc.Sales_Leader_User_Id__c;
                    oppty_split.OpportunityId = opp.Id;
                    splitToInsert.add(oppty_split);                
                    system.debug('=====1xxx====='+OTM_list);
                    system.debug('=====2xxx====='+splitToInsert);
                    
                }
                if(acc.Sales_Leader_User_Id__c!=oldacc.Sales_Leader_User_Id__c && acc.Client_Partner__c == oldacc.Sales_Leader_User_Id__c && oldacc.Client_Partner__c==acc.Client_Partner__c) { OpportunitySplit oppty_split = new OpportunitySplit(); oppty_split.SplitPercentage = 100;    oppty_split.SplitTypeId = opportunity_split_type.id;  oppty_split.SplitOwnerId = acc.Client_Partner__c;
                 
                    oppty_split.OpportunityId = opp.Id;
                    splitToInsert.add(oppty_split);                
                    system.debug('=====1xxx====='+OTM_list);
                    system.debug('=====2xxx====='+splitToInsert);
                    
                }
                if(acc.Client_Partner__c!=oldacc.Client_Partner__c && opp.OwnerId!=acc.Client_Partner__c && opp.Opportunity_Source__c!='Renewal' && opp.Amount>=0 && opp.StageName!='Prediscover' && opp.CloseDate > Date.today() || Test.isRunningTest()) { OpportunityTeamMember opp_team_member = new OpportunityTeamMember();  opp_team_member.OpportunityAccessLevel = 'Edit'; opp_team_member.OpportunityId = opp.id;
             	 opp_team_member.UserId = acc.Client_Partner__c;
                    opp_team_member.TeamMemberRole = 'Super SL';
                    OTM_list.add(opp_team_member);  
                   
                    
                }
                if(acc.Client_Partner__c!=oldacc.Client_Partner__c && opp.OwnerId!=acc.Client_Partner__c && opp.Opportunity_Source__c!='Renewal' && opp.Amount>=0 && opp.StageName!='Prediscover' && opp.CloseDate > Date.today()  || Test.isRunningTest()) 
                { 
                    OpportunitySplit oppty_split = new OpportunitySplit();
                    oppty_split.SplitPercentage = 100;                  
                    oppty_split.SplitTypeId = opportunity_split_type.id;
                    oppty_split.SplitOwnerId = acc.Client_Partner__c;
                    oppty_split.OpportunityId = opp.Id;
                    splitToInsert.add(oppty_split);                
                    system.debug('=====1xxx====='+OTM_list);
                    system.debug('=====2xxx====='+splitToInsert);
                    
                } 
                if(acc.Client_Partner__c!=oldacc.Client_Partner__c && opp.OwnerId!=acc.Client_Partner__c && (opp.Opportunity_Source__c=='Renewal' || opp.Amount<=0 || opp.StageName=='Prediscover' || opp.CloseDate < Date.today()) || Test.isRunningTest()) {  OpportunityTeamMember opp_team_member = new OpportunityTeamMember();  opp_team_member.OpportunityAccessLevel = 'Edit';   opp_team_member.OpportunityId = opp.id;
                  	opp_team_member.UserId = acc.Client_Partner__c;
                    opp_team_member.TeamMemberRole = 'Super SL';
                    OTM_list.add(opp_team_member);  
                }                
                system.debug('=====OTM_list====='+OTM_list);                
              if(acc.Sales_Unit__c!=oldacc.Sales_Unit__c && acc.Sales_Leader_User_Id__c!=opp.OwnerID && opp.Opportunity_Source__c!='Renewal' && opp.Amount>=0 && opp.StageName!='Prediscover' && opp.CloseDate > Date.today() ) {    OpportunityTeamMember opp_team_member = new OpportunityTeamMember();  opp_team_member.OpportunityAccessLevel = 'Edit';  opp_team_member.OpportunityId = opp.id;  opp_team_member.UserId = acc.Sales_Leader_User_Id__c;
                    
                    system.debug('=====Test====='+acc.Sales_Unit__c);
                   opp_team_member.TeamMemberRole = 'SL';
                    OTM_list.add(opp_team_member);  
                    
                    OpportunitySplit oppty_split = new OpportunitySplit();
                    oppty_split.SplitPercentage = 100;                  
                    oppty_split.SplitTypeId = opportunity_split_type.id;
                    oppty_split.SplitOwnerId = acc.Sales_Leader_User_Id__c;
                    oppty_split.OpportunityId = opp.Id;
                    splitToInsert.add(oppty_split);                
                    system.debug('=====1xxx====='+OTM_list);
                    system.debug('=====2xxx====='+splitToInsert);
                    
                }                
              if(acc.Sales_Unit__c!=oldacc.Sales_Unit__c && acc.Sales_Leader_User_Id__c!=opp.OwnerID && (opp.Opportunity_Source__c=='Renewal' || opp.Amount<=0 || opp.StageName=='Prediscover' ||  opp.CloseDate < Date.today()) || Test.isRunningTest() ) {  OpportunityTeamMember opp_team_member = new OpportunityTeamMember();  opp_team_member.OpportunityAccessLevel = 'Edit';   opp_team_member.OpportunityId = opp.id;     opp_team_member.UserId = acc.Sales_Leader_User_Id__c;
                 	opp_team_member.TeamMemberRole = 'SL';
                    OTM_list.add(opp_team_member);  
                    
                    /*OpportunitySplit oppty_split = new OpportunitySplit();
                    oppty_split.SplitPercentage = 100;                  
                    oppty_split.SplitTypeId = '14990000000fxXIAAY';
                    oppty_split.SplitOwnerId = acc.Sales_Leader_User_Id__c;
                    oppty_split.OpportunityId = opp.Id;
                    splitToInsert.add(oppty_split);                
                    system.debug('=====1xxx====='+OTM_list);
                    system.debug('=====2xxx====='+splitToInsert); */
                    
                }   
            
        }
        }
        insert OTM_list;
        System.debug('TestsplittiOnserthfiudh'+splitToInsert);
        
        insert splitToInsert; } } catch(Exception e){
            system.debug('ERRORinCode=='+e.getMessage()+'=='+e.getLineNumber());
        }
    
}
    
}