/**
* @author Pankaj.adhikari
* @date 22/10/2017
*
* @group ItemstoApprove
* @group-content ../../ApexDocContent/ItemstoApprove.html
*
* @description Apex Service for Items to Approve module,
* has aura enabled method for Items To Approve.
*/
public without sharing class GPCmpServiceItemstoApprove {
    
    
    /**
    * @description Aura enabled method to returns Pending Approvals.
    * @param string strObjectName
    * @param string fieldSetName
    * 
    * @return GPAuraResponse Pending Approvals
    * 
    * @example
    * GPCmpServiceItemstoApprove.getPendingApprovals(strObjectName, fieldSetName);
    */
    @AuraEnabled
    public static GPAuraResponse getPendingApprovals(string strObjectName, String fieldSetName) {
    	GPControllerItemstoApprove objItemsToApprove = new GPControllerItemstoApprove();
    	return objItemsToApprove.getPendingApproval(strObjectName,fieldSetName);
    }
    
    /**
    * @description Aura enabled method to Approve Pending items.
    * @param string strPendingWorkItems
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceItemstoApprove.approvePendingItems(<strPendingWorkItems>);
    */
    @AuraEnabled
    public static GPAuraResponse approvePendingItems(string strPendingWorkItems) {
    	GPControllerItemstoApprove objItemsToApprove = new GPControllerItemstoApprove();
    	return objItemsToApprove.approvePendingItems(strPendingWorkItems);
    }

    /**
    * @description Aura enabled method to reject pending items.
    * @param string strPendingWorkItems
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceItemstoApprove.rejectPendingItems(<string strPendingWorkItems>);
    */
    @AuraEnabled
    public static GPAuraResponse rejectPendingItems(string strPendingWorkItems) {
    	GPControllerItemstoApprove objItemsToApprove = new GPControllerItemstoApprove();
    	return objItemsToApprove.rejectPendingItems(strPendingWorkItems);
    }
}