/**
* @group Project Approval. 
*
* @description Wrapper class for Project Approval to identify which child records nee to be validated.
*/
public class GPWrapperApprovalValidatorConfig {

    //Project
    public Boolean validateProjectAgainstTemplate;
    public Boolean validateCustomFieldsOnProject;

    //Leadership
    public Boolean validateCustomFieldsOnLeadership;
    public Boolean validateLeadershipAgainstRole;
    public Boolean validateActiveEmployeeOnLeadership;

    //WorkLocation
    public Boolean validateCustomFieldsOnWorkLocation;
    public Boolean validateWorkLocation;

    //Additional SDO
    public Boolean validateCustomFieldsOnAdditionalSDO;
    public Boolean validateAdditionalSDO;

    //ResourceAllocation
    public Boolean validateResourceAllocationEffortCalculationCheckAgainstProject;
    public Boolean validateCustomFieldsOnResourceAllocation;
    public Boolean validateResourceAllocationTimesheetCheck;
    public Boolean validateResourceAllocationForEmployee;
    public Boolean validateResourceAllocationILearnCheck;
    public Boolean validateResourceAllocationForProject;
    public Boolean validateResourceAllocationGEGDCCheck;
    public Boolean validateResourceAllocationBGCCHECK;

    //Billing Milestone
    public Boolean validateCustomFieldsOnBillingMilestone;
    public Boolean validateBillingMilestoneForProject;

    //Project Budget
    public Boolean validateCustomFieldsOnProjectBudget;

    //Document Upload - PO Upload.
    public Boolean validateCustomFieldsOnDocumentUpload;

    //Project Expense
    public Boolean validateProjectExpense;

    //Project Address
    public Boolean validateProjectAddress;
    public Boolean validateUserStage;

    public GPWrapperApprovalValidatorConfig() {
        //Project
        validateProjectAgainstTemplate = true;
        validateCustomFieldsOnProject = true;

        //Leadership
        validateCustomFieldsOnLeadership = true;
        validateLeadershipAgainstRole = true;
        validateActiveEmployeeOnLeadership = true;

        //WorkLocation
        validateCustomFieldsOnWorkLocation = true;
        validateWorkLocation = true;

        //Additional SDO
        validateCustomFieldsOnAdditionalSDO = true;
        validateAdditionalSDO = true;

        //ResourceAllocation
        validateResourceAllocationEffortCalculationCheckAgainstProject = true;
        validateCustomFieldsOnResourceAllocation = true;
        validateResourceAllocationTimesheetCheck = true;
        validateResourceAllocationILearnCheck = false;
        validateResourceAllocationForEmployee = true;
        validateResourceAllocationForProject = true;
        validateResourceAllocationGEGDCCheck = true;
        validateResourceAllocationBGCCHECK = false;

        //Billing Milestone
        validateCustomFieldsOnBillingMilestone = true;
        validateBillingMilestoneForProject = true;

        //Project Budget
        validateCustomFieldsOnProjectBudget = true;

        //Document Upload - PO Upload.
        validateCustomFieldsOnDocumentUpload = true;

        //Project Expense
        validateProjectExpense = true;

        //Project Address
        validateProjectAddress = true;

        //set user defined stage
        validateUserStage = true;
    }

    public GPWrapperApprovalValidatorConfig(Boolean validate) {
        //Project
        validateProjectAgainstTemplate = validate;
        validateCustomFieldsOnProject = validate;

        //Leadership
        validateCustomFieldsOnLeadership = validate;
        validateLeadershipAgainstRole = validate;
        validateActiveEmployeeOnLeadership = validate;

        //WorkLocation
        validateCustomFieldsOnWorkLocation = validate;
        validateWorkLocation = validate;

        //Additional SDO
        validateCustomFieldsOnAdditionalSDO = validate;
        validateAdditionalSDO = validate;

        //ResourceAllocation
        validateResourceAllocationEffortCalculationCheckAgainstProject = validate;
        validateResourceAllocationTimesheetCheck = validate;
        validateCustomFieldsOnResourceAllocation = validate;
        validateResourceAllocationForEmployee = validate;
        validateResourceAllocationILearnCheck = validate;
        validateResourceAllocationForProject = validate;
        validateResourceAllocationGEGDCCheck = validate;
        validateResourceAllocationBGCCHECK = validate;

        //Billing Milestone
        validateCustomFieldsOnBillingMilestone = validate;
        validateBillingMilestoneForProject = validate;

        //Project Budget
        validateCustomFieldsOnProjectBudget = validate;

        //Document Upload - PO Upload.
        validateCustomFieldsOnDocumentUpload = validate;

        //project expense
        validateProjectExpense = validate;

        //Project Address
        validateProjectAddress = validate;
        validateUserStage = validate;
    }
}