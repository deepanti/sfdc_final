public class GPDomainWorkLocationSDOMaster extends fflib_SObjectDomain {
    private set < Id > setofWorkLocationSDOId = new set < id > ();
    private map < Id, GP_Work_Location__c > mapofWorkLocationSDOIdToWorklocationSDO = new map < Id, GP_Work_Location__c > ();

    public GPDomainWorkLocationSDOMaster(List < GP_Work_Location__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainWorkLocationSDOMaster(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] { GP_Work_Location__c.SObjectType };

    public override void onBeforeInsert() {
        
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        assigneRecordType();
    }

    public override void onAfterInsert() {}

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        disableChangestostatusofWorkLocationSDO( oldSObjectMap);
        validateChangesToStartAndEndDate(oldSObjectMap);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
    }

    public override void onValidate() {
        // Validate GP_Work_Location__c 
        //for (GP_Work_Location__c ObjProject: (List < GP_Work_Location__c > ) Records) {}
    }

    public override void onApplyDefaults() {
        // Apply defaults to GP_Work_Location__c 
        //for(GP_Work_Location__c  ObjProject : (List<GP_Work_Location__c >) Records) { 
        //}               
    }


    //throws an error when the status of the workLocationAndSDO is changed from 'ACtive and Visible'  
    //because it contains some unclosed projects 
    //mandeep.singh@saasfocus.com
    public void disableChangestostatusofWorkLocationSDO(Map < Id, SOBject > oldSObjectMap) {
        Integer flag = 0;
        GP_Work_Location__c oldworkLocationSDO;
        set < id > setofworklocationsdoid = new set < id > ();
        map < id, integer > mapofworklocationsdoidandinteger = new map < id, integer > ();

        for (GP_Work_Location__c workLocationSDO: (List < GP_Work_Location__c > ) records) {
            if (workLocationSDO.GP_Status__c != null && oldSObjectMap != null) {
                oldworkLocationSDO = (GP_Work_Location__c) oldSObjectMap.get(workLocationSDO.id);
                if (oldworkLocationSDO.GP_Status__c == 'Active and Visible' && workLocationSDO.GP_Status__c != oldworkLocationSDO.GP_Status__c) {
                    setofworklocationsdoid.add(workLocationSDO.id);
                }
            }
        }
        if (setofworklocationsdoid != null && setofworklocationsdoid.size() > 0) {

            for (GP_Work_Location__c workLocationSDOobj: new GPSelectorWorkLocationSDOMaster().selectByIdofWOrkLocationAndSDO(setofworklocationsdoid)) {
                flag = 0;
                if (workLocationSDOobj != null && workLocationSDOobj.Projects__r != null) {
                    for (GP_Project__c prj: workLocationSDOobj.Projects__r) {
                        if (prj.GP_Is_Closed__c != true) {
                            flag = flag + 1;
                        }
                    }
                }
                mapofworklocationsdoidandinteger.put(workLocationSDOobj.id, flag);
            }
            for (GP_Work_Location__c eachWorkLocationSDO: (List < GP_Work_Location__c > ) records) {
                if (mapofworklocationsdoidandinteger.get(eachWorkLocationSDO.id) > 0) {
                    eachWorkLocationSDO.adderror('Status cannot be changed as this record contains some unclosed projects.');
                }
            }
        }
    }

    //Start date cannot be changed to higher than and end date of WorkLocationAndSDO cannot be changed to lower than that of 
    //the child projects lowest and highest Start Date and end date respectively  when 
    //there are child projects present i.e. the start date and end date of child projects should always lie in between the start and
    //end date of the parent worklocationAndSDOMaster
    //mandeep.singh@saasfocus.com
    public void validateChangesToStartAndEndDate(Map < Id, SOBject > oldSObjectMap) {
        Boolean isWorkLocationDateValid;

        setMapOfWorkLocation(oldSObjectMap);

        for (GP_Work_Location__c worklSDOobj: new GPSelectorWorkLocationSDOMaster().selectByIdofWOrkLocationAndSDO(setofWorkLocationSDOId)) {

            for (GP_Project__c childprjobj: worklSDOobj.Projects__r) {
                if (isDateInvalid(childprjobj, worklSDOobj)) {
                    isWorkLocationDateValid = true;
                    mapofWorkLocationSDOIdToWorklocationSDO.get(worklSDOobj.id).addError('Date is Invalid!!');
                    break;
                }
            }
        }
    }

    public Boolean isDateInvalid(GP_Project__c childprjobj, GP_Work_Location__c worklSDOobj) {
        return (childprjobj.GP_Start_Date__c != null && childprjobj.GP_End_Date__c != null &&
            mapofWorkLocationSDOIdToWorklocationSDO.get(worklSDOobj.id) != null &&
            (childprjobj.GP_Start_Date__c < (mapofWorkLocationSDOIdToWorklocationSDO.get(worklSDOobj.id)).GP_Start_Date_Active__c ||
                childprjobj.GP_End_Date__c > (mapofWorkLocationSDOIdToWorklocationSDO.get(worklSDOobj.id)).GP_End_Date_Active__c));
    }

    private void setMapOfWorkLocation(Map < Id, SOBject > oldSObjectMap) {
        mapofWorkLocationSDOIdToWorklocationSDO = new Map < Id, GP_Work_Location__c > ();
        setofWorkLocationSDOId = new Set < Id > ();
        GP_Work_Location__c oldWorkLocationSDO;


        for (GP_Work_Location__c newWorkLocationSDO: (List < GP_Work_Location__c > ) records) {
            if (oldSObjectMap == null) {
                continue;
            }

            oldWorkLocationSDO = (GP_Work_Location__c) oldSObjectMap.get(newWorkLocationSDO.Id);
            if (newWorkLocationSDO.GP_Start_Date_Active__c != oldWorkLocationSDO.GP_Start_Date_Active__c ||
                newWorkLocationSDO.GP_End_Date_Active__c != oldWorkLocationSDO.GP_End_Date_Active__c) {
                setofWorkLocationSDOId.add(newWorkLocationSDO.Id);
                mapofWorkLocationSDOIdToWorklocationSDO.put(newWorkLocationSDO.Id, newWorkLocationSDO);
            }
        }
    }
    
    // ------------------------------------------------------------------------------------------------ 
    // Description: Method is used to assign Standard RecordTypeId based o Record Type Name   
    // ------------------------------------------------------------------------------------------------
    // Input Param: NA
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::16-May -2018  Created By:Sumit
    // ------------------------------------------------------------------------------------------------ 
   
    private void assigneRecordType() {
        for (GP_Work_Location__c objWL: (List < GP_Work_Location__c > ) Records) {
            
            if (objWL.GP_Record_Type_Name__c != null) {
                
               objWL.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get(objWL.GP_Record_Type_Name__c).getRecordTypeId();

            }
        }
    }

}