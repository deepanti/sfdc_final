/**
* @Description: Class used to calculate PM Component Incentive 
* @author: Bashim Khan
* @date: May 2018
*/
public class VIC_CalculatePMIncentivectrl implements VIC_ScorecardCalculationInterface{
    public Decimal opAcheivmentPercent{get;set;}
    public Map<String, vic_Incentive_Constant__mdt> mapValueToObjIncentiveConst{get;set;}
    
    public Decimal calculate(Target_Component__c tc, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        Decimal incentive = 0;
        Set<String> planPMIncentive = new Set<String>{'Capital_Market_GRM','IT_GRM'};  //    PM_Percent_To_OP = 96
        if(planPMIncentive.contains(tc.Target__r.Plan__r.vic_Plan_Code__c)){            
          if(tc.Target__r.Target_Bonus__c !=null &&  tc.Weightage__c !=null &&  tc.vic_Achievement_Percent__c !=null && 
              opAcheivmentPercent > Decimal.valueOf(mapValueToObjIncentiveConst.get('PM_Percent_To_OP').vic_Value__c)){  
            Decimal targetedIncentive = tc.Target__r.Target_Bonus__c * tc.Weightage__c/100;
            Decimal payoutParcent = calculatePayOutPercantagePM(tc.vic_Achievement_Percent__c, lstCalcMatrixItem);
            incentive = targetedIncentive * payoutParcent/100;
          }
        }
        return incentive;
     }   
    public Decimal calculatePayOutPercantagePM(Decimal achievementParcent, List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem){
        if(getFilterdCalcMatrixItem(lstCalcMatrixItem, achievementParcent)!=null){
            return getFilterdCalcMatrixItem(lstCalcMatrixItem, achievementParcent).vic_Payout__c;
        }else{
            return 0.0;
        }
    }
    /*
        @Description: get Filterd VIC Calculation Matrix Item corresponding to Achievement Percentage
        @Author: Bashim Khan
        @Date: May 2018
    */
    public VIC_Calculation_Matrix_Item__c getFilterdCalcMatrixItem(List<VIC_Calculation_Matrix_Item__c> lstCalcMatrixItem, Decimal achievementPer){
        VIC_Calculation_Matrix_Item__c selectedItem; 
        if(lstCalcMatrixItem!=null && !lstCalcMatrixItem.isEmpty()){  
            for(VIC_Calculation_Matrix_Item__c item : lstCalcMatrixItem){
                if((achievementPer>item.vic_Start_Value__c && achievementPer<=item.vic_End_Value__c) 
                   || (achievementPer>item.vic_Start_Value__c && item.vic_End_Value__c ==null)
                   || (achievementPer==item.vic_Start_Value__c && achievementPer==item.vic_End_Value__c)){
                       
                       selectedItem = item;
                       break;
                   }
            }
        }
        return selectedItem;
    }
}