public class StaleDealHelper {
    public static double Ts_Discover_retail_con;
    public static double Ts_Discover_medium_con;
    public static double Ts_Discover_large_con;
    public static double Ts_Define_retail_con;
    public static double Ts_Define_medium_con;
    public static double Ts_Define_large_con;
    
    public static double Ts_Discover_retail_anal;
    public static double Ts_Discover_medium_anal;
    public static double Ts_Discover_large_anal;
    public static double Ts_Define_retail_anal;
    public static double Ts_Define_medium_anal;
    public static double Ts_Define_large_anal;
    
    public static double Ts_Discover_retail_dig;
    public static double Ts_Discover_medium_dig;
    public static double Ts_Discover_large_dig;
    public static double Ts_Define_retail_dig;
    public static double Ts_Define_medium_dig;
    public static double Ts_Define_large_dig;
     
    
    public static Map<String, Map<String, Set<Opportunity>>> fetch_Non_TS_Ageing_Opps_new(){
        Set<Opportunity> non_Ts_stalledDeal_oppIDs = new Set<Opportunity>();
        Set<Opportunity> non_Ts_staleDeal_OppIDs = new Set<Opportunity>();
        Map<String, Map<String,Set<Opportunity>>> ownerWise_deals_map = new Map<String, Map<String,Set<Opportunity>>>();
        Map<String,Set<Opportunity>> deal_map = new Map<String,Set<Opportunity>>();
        
        List<opportunity> oppty_non_ts = [Select id, Opportunity_ID__c, Opportunity_Source__c, stagename, Transformation_deal_ageing_for_non_ts__c, deal_cycle_ageing__c,
                                         Type_of_deal_for_non_ts__c, Formula_Hunting_Mining__c, Deal_Type__c, closedate, 
                                         QSRM_status2__c, Insight__c, CloseDatewithInsight__c, NonTsByPassUI__c, 
                                         Ageing_large_retail__c, Name, Opportunity_Age__c, Stalled_Deal_Check__c,
                                         Type_of_deal__c, TSInights__c, Amount, Accountid, Account.Name, Last_stage_change_date__c,
                                         Previous_Stage__c, TCV1__c, Transformation_ageing__c, QSRM_Type__c, Move_Deal_Ahead__c,
                                         Cycle_Time__c, Nature_of_Work_highest__c, Discover_MSA_CT_Check__C,
                                         Define_MSA_CT_Check__C, Onbid_MSA_CT_Check__c, DownSelect_MSA_CT_Check__C,
                                         Confirmed_MSA_CT_Check__C, Contract_Status__c from opportunity where (Type_Of_Opportunity__c = 'Non Ts' OR Type_Of_Opportunity__c = 'Non TS')
                                         and Transformation_14_days_check_age__c='True' and Ownerid =:UserInfo.getUserId() 
                                         and (StageName ='1. Discover' OR StageName = '2. Define' OR 
                                         StageName ='3. On Bid' OR StageName ='4. Down Select' OR 
                                         StageName ='5. Confirmed')];    
         system.debug('oppty_non_ts=='+oppty_non_ts);
        Date today = Date.today();
        
         if(UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners){ 
            if(oppty_non_ts.size() > 0 )
            {
                for(opportunity o: oppty_non_ts){
                                            
                        if(o.NonTsByPassUI__c!='True' && o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
                                   System.debug('o.Type_of_deal_for_non_ts__c =='+o.Type_of_deal_for_non_ts__c );
                            if(o.Type_of_deal_for_non_ts__c == 'Medium'){   
                                
                                Non_TS_restrcited_access__c discover_medium = Non_TS_restrcited_access__c.getValues('Discover_medium');
                                Non_TS_restrcited_access__c define_medium = Non_TS_restrcited_access__c.getValues('Define_medium');
                                Non_TS_restrcited_access__c onBid_medium = Non_TS_restrcited_access__c.getValues('On-bid_medium');
                                Non_TS_restrcited_access__c downSelect_medium = Non_TS_restrcited_access__c.getValues('Down Select_medium');
                              
                                System.debug('discover_medium=='+discover_medium.days__c);
                                System.debug('define_medium=='+define_medium.days__c);
                                System.debug('onBid_medium=='+onBid_medium.days__c);
                                System.debug('downSelect_medium=='+downSelect_medium.days__c);
                                
                                if(o.stagename=='1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_medium.days__c){
                                    if(o.QSRM_status2__c==0 ){
                                           non_Ts_stalledDeal_oppIDs.add(o);     }} else if(o.stagename=='2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_medium.days__c){non_Ts_stalledDeal_oppIDs.add(o);   }else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_medium.days__c){ non_Ts_stalledDeal_oppIDs.add(o);} else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_medium.days__c){ non_Ts_stalledDeal_oppIDs.add(o);
                                }
                             }
                             if(o.Type_of_deal_for_non_ts__c == 'Large'){ 
                                Non_TS_restrcited_access__c discover_large = Non_TS_restrcited_access__c.getValues('Discover_large');
                                Non_TS_restrcited_access__c define_large = Non_TS_restrcited_access__c.getValues('Define_large');
                                Non_TS_restrcited_access__c onBid_large = Non_TS_restrcited_access__c.getValues('On-bid_large');
                                Non_TS_restrcited_access__c downSelect_large = Non_TS_restrcited_access__c.getValues('Down Select_large');
                               	Non_TS_restrcited_access__c confirmed_large = Non_TS_restrcited_access__c.getValues('Confirmed_large');
                                 
                                if(o.stagename=='1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_large.days__c){
                                    if(o.QSRM_status2__c==0 ){
                                        non_Ts_stalledDeal_oppIDs.add(o);     
                                    }
                                }
                                else if(o.stageName == '2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_large.days__c){
                                    non_Ts_stalledDeal_oppIDs.add(o);  
                                }
                                else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_large.days__c){
                                    non_Ts_stalledDeal_oppIDs.add(o); 
                                }
                                else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_large.days__c){
                                    non_Ts_stalledDeal_oppIDs.add(o);
                                } 
                               else if(o.StageName == '5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))
                                        && o.Transformation_deal_ageing_for_non_ts__c <= confirmed_large.days__c){
                                    non_Ts_stalledDeal_oppIDs.add(o);
                                }
                             }
                            if(o.Type_of_deal_for_non_ts__c == 'Extra Large'){ 
                                system.debug(':----Extra Large----Opportunity_ID__c:'+o.Opportunity_ID__c);
                                Non_TS_restrcited_access__c discover_extra_large = Non_TS_restrcited_access__c.getValues('Discover_extra_large');
                                Non_TS_restrcited_access__c define_extra_large = Non_TS_restrcited_access__c.getValues('Define_Extra_large');
                                Non_TS_restrcited_access__c onBid_extra_large = Non_TS_restrcited_access__c.getValues('On-bid_Extra_large');
                                Non_TS_restrcited_access__c downSelect_extra_large = Non_TS_restrcited_access__c.getValues('Down Select_Extra_large');
                                Non_TS_restrcited_access__c confirmed_extra_large = Non_TS_restrcited_access__c.getValues('Confirmed_Extra_large');
                                
                                if(o.stagename=='1. Discover' && o.Transformation_deal_ageing_for_non_ts__c <= discover_extra_large.days__c){
                                    if(o.QSRM_status2__c==0 ){
                                        non_Ts_stalledDeal_oppIDs.add(o);     }} else if(o.stageName == '2. Define' && o.Transformation_deal_ageing_for_non_ts__c <= define_extra_large.days__c){ non_Ts_stalledDeal_oppIDs.add(o);  } else if(o.StageName == '3. On Bid' && o.Transformation_deal_ageing_for_non_ts__c <= onBid_extra_large.days__c){non_Ts_stalledDeal_oppIDs.add(o);} else if(o.StageName == '4. Down Select' && o.Transformation_deal_ageing_for_non_ts__c <= downSelect_extra_large.days__c){non_Ts_stalledDeal_oppIDs.add(o);} else if(o.StageName == '5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval'))) && o.Transformation_deal_ageing_for_non_ts__c <= confirmed_extra_large.days__c){ non_Ts_stalledDeal_oppIDs.add(o);
                                }
                            }
                        } 
                    if(o.CloseDate < System.Today() && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){ non_Ts_staleDeal_OppIDs.add(o);
                    } 
                }   
            }           
             
            deal_map.put('STALLED_DEALS', non_Ts_stalledDeal_oppIDs);
            deal_map.put('STALE_DEALS',non_Ts_staleDeal_OppIDs);
            ownerWise_deals_map.put('STALE_USER', deal_map);
            System.debug('in if ownerWise_deals_map=='+ownerWise_deals_map);
        }
        else{       
            for(opportunity o: oppty_non_ts){
                    if( o.NonTsByPassUI__c!='True' && o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
                           
                             if(o.Type_of_deal_for_non_ts__c == 'Large'){ 
                                 
                                Non_TS_Ageing_Rules__c discover_large_ageing = Non_TS_Ageing_Rules__c.getValues('Discover_large');
                                Non_TS_Ageing_Rules__c define_large_ageing = Non_TS_Ageing_Rules__c.getValues('Define_large');
                                 
                                 if(o.stagename=='1. Discover'){
                                     if(o.Transformation_ageing__c >= discover_large_ageing.ageing__c){if(o.QSRM_status2__c==0 ){ non_Ts_stalledDeal_oppIDs.add(o);    
                                        }
                                     }                                     
                                 } 
                                 else if(o.stageName == '2. Define'){
                                     if(o.Transformation_ageing__c >= define_large_ageing.ageing__c){ non_Ts_stalledDeal_oppIDs.add(o); 
                                     }
                                 } 
                                
                             }
                        
                        if(o.Type_of_deal_for_non_ts__c == 'Extra Large'){ 
                                                               
                                Non_TS_Ageing_Rules__c discover_extra_large_ageing = Non_TS_Ageing_Rules__c.getValues('Discover_Extra_large');
                                Non_TS_Ageing_Rules__c define_extra_large_ageing = Non_TS_Ageing_Rules__c.getValues('Define_Extra_large');
                            
                                if(o.stagename=='1. Discover'){if(o.Transformation_ageing__c >= discover_extra_large_ageing.ageing__c){if(o.QSRM_status2__c==0 ){ non_Ts_stalledDeal_oppIDs.add(o);    
                                        }
                                     }                                     
                                 } 
                                 else if(o.stageName == '2. Define'){
                                     if(o.Transformation_ageing__c >= define_extra_large_ageing.ageing__c){ non_Ts_stalledDeal_oppIDs.add(o); 
                                     }
                                 } 
                           }
                        }
                    if(o.CloseDate <= System.Today() +7 && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){if(!non_Ts_stalledDeal_oppIDs.contains(o)){ non_Ts_staleDeal_OppIDs.add(o); }        }
            }
            deal_map.put('STALLED_DEALS', non_Ts_stalledDeal_oppIDs);
            deal_map.put('STALE_DEALS',non_Ts_staleDeal_OppIDs);
            ownerWise_deals_map.put('NORMAL_USER', deal_map);
        }
         System.debug('ownerWise_deals_map ==='+ownerWise_deals_map);
        return ownerWise_deals_map;
    }
    
    public static Map<String, Map<String, Set<Opportunity>>> fetch_TS_Ageing_Opps_new(){ 
       dealAgeing();
        Set<Opportunity> ts_staleDeal_OppIDs = new Set<Opportunity>();
        Set<Opportunity> ts_stalledDeal_oppIDs = new Set<Opportunity>();
        Map<String, Map<String,Set<Opportunity>>> ownerWise_deals_map = new Map<String, Map<String,Set<Opportunity>>>();
        Map<String,Set<Opportunity>> deal_map = new Map<String,Set<Opportunity>>();
        map<id,opportunity> oppMap = new map<id,opportunity>();
        double Ts_Deal_Aging;
        List<Opportunity> oppty = [Select id, ownerid,Opportunity_ID__c, Opportunity_Source__c,Deal_Nature__c,Insight__c, Name,Type_of_deal_for_non_ts__c, stagename, deal_cycle_ageing__c, Transformation_ageing__c, Type_of_deal__c,
                                  Type_Of_Opportunity__c,StallByPassFlag__c, QSRM_status2__c, closedate, Ageing_large_retail__c, Opportunity_Age__c,
                                  TSInights__c, Amount, Accountid, Account.Name, Last_stage_change_date__c, 
                                  Previous_Stage__c, TCV1__c, QSRM_Type__c, Move_Deal_Ahead__c, Cycle_Time__c,
                                  Formula_Hunting_Mining__c, Deal_Type__c, Nature_of_Work_highest__c, Discover_MSA_CT_Check__C,
                                  Define_MSA_CT_Check__C, Contract_Status__c, Onbid_MSA_CT_Check__c, DownSelect_MSA_CT_Check__C,
                                  Confirmed_MSA_CT_Check__C from opportunity where Type_Of_Opportunity__c = 'TS'  and Transformation_14_days_check_age__c='True' and Ownerid =:UserInfo.getUserId() and 
                                   (StageName ='1. Discover' OR StageName = '2. Define' OR 
                                         StageName ='3. On Bid' OR StageName ='4. Down Select' OR 
                                         StageName ='5. Confirmed')];
      
        system.debug(':---ownerWise_deals_map---: '+oppty.size());
        if (UserInfo.getProfileId() == Label.Profile_Stale_Deal_Owners)
        { 
            if(oppty.size() > 0){ 
                For(opportunity o: oppty)
                {
                    System.debug('o.Transformation_ageing__c=='+o.Transformation_ageing__c);
                   // System.debug('o.Insight__c=='+o.Insight__c);
                    System.debug('o.Insight__c=='+o.Opportunity_ID__c);
                    if(o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
                    	if(o.Type_of_deal_for_non_ts__c == 'Retail' ){ if( o.StallByPassFlag__c!='True' ){ if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){   if(o.Transformation_ageing__c >= Ts_Discover_retail_dig){ if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   } } } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){    if(o.Transformation_ageing__c >= Ts_Define_retail_dig){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ 
                                if(o.Transformation_ageing__c >= Ts_Discover_retail_anal){
                                    if(o.QSRM_status2__c==0){  
                                        ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){ 
                                if(o.Transformation_ageing__c >= Ts_Define_retail_anal){
                                    ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ 
                                if(o.Transformation_ageing__c >= Ts_Discover_retail_con){
                                    if(o.QSRM_status2__c==0){  
                                        ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){ 
                                if(o.Transformation_ageing__c >= Ts_Define_retail_con){
                                    ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                    } else if(o.Type_of_deal_for_non_ts__c == 'Medium'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){  if(o.Transformation_ageing__c >= Ts_Discover_medium_dig){ if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){   if(o.Transformation_ageing__c >= Ts_Define_medium_dig){  ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){  if(o.Transformation_ageing__c >= Ts_Discover_medium_anal){ if(o.QSRM_status2__c==0){    ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){  if(o.Transformation_ageing__c >= Ts_Define_medium_anal){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){   if(o.Transformation_ageing__c >= Ts_Discover_medium_con){ if(o.QSRM_status2__c==0){    ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){  if(o.Transformation_ageing__c >= Ts_Define_medium_con){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                        
                    }else if(o.Type_of_deal_for_non_ts__c == 'Large'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){  if(o.Transformation_ageing__c >= Ts_Discover_large_dig){ if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){  if(o.Transformation_ageing__c >= Ts_Define_large_dig){  ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){  if(o.Transformation_ageing__c >= Ts_Discover_large_anal){ if(o.QSRM_status2__c==0){     ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){  if(o.Transformation_ageing__c >= Ts_Define_large_anal){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){  if(o.Transformation_ageing__c >= Ts_Discover_large_con){ if(o.QSRM_status2__c==0){    ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){  if(o.Transformation_ageing__c >= Ts_Define_large_con){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                        
                    }    
                    }
                    
                    
                    if( o.CloseDate < System.Today() && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){ if(!ts_stalledDeal_oppIDs.contains(o)){  ts_staleDeal_OppIDs.add(o);    
                        }
                    }
                }
            }
            
            deal_map.put('STALLED_DEALS', ts_stalledDeal_oppIDs);
            deal_map.put('STALE_DEALS',ts_staleDeal_OppIDs);
            ownerWise_deals_map.put('STALE_USER', deal_map);
            system.debug(':---ownerWise_deals_map---: '+ownerWise_deals_map);
        }
        else
        {
            for(opportunity o: oppty)
            { 
                if(o.Opportunity_Source__c != 'Ramp Up' && o.Opportunity_Source__c != 'Renewal'){
                	if(o.Type_of_deal_for_non_ts__c == 'Retail'){
                        if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){ if(o.Transformation_ageing__c >= Ts_Discover_retail_dig){  if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){   if(o.Transformation_ageing__c >= Ts_Define_retail_dig){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Discover_retail_anal){ if(o.QSRM_status2__c==0){     ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){  if(o.Transformation_ageing__c >= Ts_Define_retail_anal){  ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ 
                                if(o.Transformation_ageing__c >= Ts_Discover_retail_con){
                                    if(o.QSRM_status2__c==0){  
                                        ts_stalledDeal_oppIDs.add(o);    }} } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){   if(o.Transformation_ageing__c >= Ts_Define_retail_con){  ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                    } else if(o.Type_of_deal_for_non_ts__c == 'Medium'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){   if(o.Transformation_ageing__c >= Ts_Discover_medium_dig){  if(o.QSRM_status2__c==0){      ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){   if(o.Transformation_ageing__c >= Ts_Define_medium_dig){    ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){    if(o.Transformation_ageing__c >= Ts_Discover_medium_anal){   if(o.QSRM_status2__c==0){      ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){     if(o.Transformation_ageing__c >= Ts_Define_medium_anal){    ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){    if(o.Transformation_ageing__c >= Ts_Discover_medium_con){     if(o.QSRM_status2__c==0){     ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){  if(o.Transformation_ageing__c >= Ts_Define_medium_con){   ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                        
                    }else if(o.Type_of_deal_for_non_ts__c == 'Large'){
						if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Digital'){  if(o.Transformation_ageing__c >= Ts_Discover_large_dig){ if(o.QSRM_status2__c==0){  ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Digital'){  if(o.Transformation_ageing__c >= Ts_Define_large_dig){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Discover_large_anal){ if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'Analytics'){ if(o.Transformation_ageing__c >= Ts_Define_large_anal){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                          if( o.StallByPassFlag__c!='True' ){
                            if(o.StageName == '1. Discover' && o.Deal_Nature__c == 'conslting'){ if(o.Transformation_ageing__c >= Ts_Discover_large_con){ if(o.QSRM_status2__c==0){   ts_stalledDeal_oppIDs.add(o);   
                                    }
                                }
                            } else if(o.stagename=='2. Define' && o.Deal_Nature__c == 'conslting'){  if(o.Transformation_ageing__c >= Ts_Define_large_con){ ts_stalledDeal_oppIDs.add(o);    
                                }
                            }
                        }
                        
                    }   
                }
                
                    
                if(o.CloseDate <= System.Today()+7 && (o.stagename=='1. Discover' || o.stagename=='2. Define' || o.stagename=='3. On Bid' || o.stagename=='4. Down Select' || (o.stagename=='5. Confirmed' && (String.isBlank(o.Contract_Status__c)|| !(o.Contract_Status__c.contains('Approval')))))){
                    if(!ts_stalledDeal_oppIDs.contains(o)){ ts_staleDeal_OppIDs.add(o);    
                    }
                }
            }
            deal_map.put('STALLED_DEALS', ts_stalledDeal_oppIDs);
            deal_map.put('STALE_DEALS',ts_staleDeal_OppIDs);
            ownerWise_deals_map.put('NORMAL_USER', deal_map);
            system.debug(':---ownerWise_deals_map---: '+ownerWise_deals_map);
        }
        system.debug(':---ownerWise_deals_map---: '+ownerWise_deals_map);
        return ownerWise_deals_map;
      
    }
    private static Void dealAgeing(){
        for(Non_Ts_cycle_Time__c Dc : Non_Ts_cycle_Time__c.getall().values()){
          if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_dig = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'Digital'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Define_medium_dig = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_dig = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_anal = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'Analytics'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Define_medium_anal = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_anal = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '1. Discover' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Discover_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){
                    Ts_Discover_medium_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Discover_large_con = Dc.Ageing__c;
                }
            }
            if(Dc.Stage__c == '2. Define' && Dc.Name__c == 'consulting'){
                if(Dc.Type_of_deal_for_non_ts__c == 'Retail'){
                    Ts_Define_retail_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Medium'){ Ts_Define_medium_con = Dc.Ageing__c;
                }
                if(Dc.Type_of_deal_for_non_ts__c == 'Large'){
                    Ts_Define_large_con = Dc.Ageing__c;
                }
            }          
        }
    }
}