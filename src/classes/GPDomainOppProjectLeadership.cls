public class GPDomainOppProjectLeadership extends fflib_SObjectDomain {
    public GPDomainOppProjectLeadership(List < GP_Opp_Project_Leadership__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainOppProjectLeadership(sObjectList);
        }
    }

    //SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] { GP_Opp_Project_Leadership__c.SobjectType };

    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateEmployeeonLeaderShip(null);
        validateNewLeaderShipInsert();
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateEmployeeonLeaderShip(oldSObjectMap);
        updateIsUpdatedOnOpportunityProject(oldSObjectMap);
        uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        updateIsUpdatedOnChange(oldSObjectMap);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }
    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the GP_Employee_Id__c from  GP_Emp_Person_Id__c
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------     
    private void updateEmployeeonLeaderShip(Map < Id, SObject > oldSObjectMap) {
        set < string > setPersonId = new set < string > ();
        for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
            GP_Opp_Project_Leadership__c objOldOppProjectLeaderShip =
                oldSObjectMap != null ? (GP_Opp_Project_Leadership__c) oldSObjectMap.get(objOppProjectLeadership.id) : null;

            if (!String.isBlank(objOppProjectLeadership.GP_Emp_Person_Id__c) &&
                (objOldOppProjectLeaderShip == null || objOldOppProjectLeaderShip.GP_Emp_Person_Id__c != objOppProjectLeadership.GP_Emp_Person_Id__c)) {
                setPersonId.add(objOppProjectLeadership.GP_Emp_Person_Id__c);
            }
        }

        if (setPersonId != null && setPersonId.size() > 0) {
            Map < String, GP_Employee_Master__c > mpPersonIdToEmployeeMaster = new map < String, GP_Employee_Master__c > ();
            list < GP_Employee_Master__c > lstEmployeeMaster = new GPSelectorEmployeeMaster().getEmployeeListForOHRId(setPersonId);
            for (GP_Employee_Master__c objEmployeeMaster: lstEmployeeMaster) {
                mpPersonIdToEmployeeMaster.put(objEmployeeMaster.GP_Final_OHR__c, objEmployeeMaster);
            }

            for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
                if (!String.isBlank(objOppProjectLeadership.GP_Emp_Person_Id__c) && mpPersonIdToEmployeeMaster.get(objOppProjectLeadership.GP_Emp_Person_Id__c) != null) {
                    objOppProjectLeadership.GP_Employee_Id__c = mpPersonIdToEmployeeMaster.get(objOppProjectLeadership.GP_Emp_Person_Id__c).id;
                }
            }
        }
    }



    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the Enddate of the eixist leadership on same role.        
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------     
    private void validateNewLeaderShipInsert() {
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < String > setRoleid = new set < String > ();
        set < String > setOppProjectId = new set < String > ();
        String strKey = '';
        map < string, GP_Opp_Project_Leadership__c > mpRoleOppProjetcToLeaderShip = new map < string, GP_Opp_Project_Leadership__c > ();
        for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
            if (!String.isBlank(objOppProjectLeadership.GP_Leadership_Role__c) && objOppProjectLeadership.GP_Opportunity_Project__c != null) {
                setRoleid.add(objOppProjectLeadership.GP_Leadership_Role__c);
                setOppProjectId.add(objOppProjectLeadership.GP_Opportunity_Project__c);
            }
        }

        if (setRoleid.size() > 0 && setOppProjectId.size() > 0) {
            list < GP_Opp_Project_Leadership__c > lstExistingLeaderShip = GPSelectorOppProjectLeadership.getLeaderShipBaseonRole(setRoleid, setOppProjectId);

            for (GP_Opp_Project_Leadership__c objOppProjectLeadership: lstExistingLeaderShip) {
                strKey = objOppProjectLeadership.GP_Leadership_Role__c + '-' + objOppProjectLeadership.GP_Opportunity_Project__c;
                mpRoleOppProjetcToLeaderShip.put(strKey, objOppProjectLeadership);
            }
        }

        list < GP_Opp_Project_Leadership__c > lstOpportunityLeaderShiptoUpdate = new list < GP_Opp_Project_Leadership__c > ();
        for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
            if (!String.isBlank(objOppProjectLeadership.GP_Leadership_Role__c) && objOppProjectLeadership.GP_Opportunity_Project__c != null) {
                strKey = objOppProjectLeadership.GP_Leadership_Role__c + '-' + objOppProjectLeadership.GP_Opportunity_Project__c;
                if (mpRoleOppProjetcToLeaderShip.get(strKey) != null) {
                    if (mpRoleOppProjetcToLeaderShip.get(strKey).GP_Start_Date__c != null &&
                        mpRoleOppProjetcToLeaderShip.get(strKey).GP_Start_Date__c > = objOppProjectLeadership.GP_Start_Date__c) {
                        objOppProjectLeadership.addError('Start Date of leadership of same role cannot be same.');
                    } else if (mpRoleOppProjetcToLeaderShip.get(strKey) != null &&
                        mpRoleOppProjetcToLeaderShip.get(strKey).id == null) {
                        objOppProjectLeadership.addError('More than one leadership for same role cannot be insert.');
                    } else if (mpRoleOppProjetcToLeaderShip.get(strKey).GP_Start_Date__c != null && mpRoleOppProjetcToLeaderShip.get(strKey).id != Null) {
                        lstOpportunityLeaderShiptoUpdate.add(new GP_Opp_Project_Leadership__c(id = mpRoleOppProjetcToLeaderShip.get(strKey).id,
                            GP_End_Date__c = objOppProjectLeadership.GP_Start_Date__c.addDays(-1),
                            GP_Active__c = false));
                    }
                } else { // for insert 
                    mpRoleOppProjetcToLeaderShip.put(strKey, objOppProjectLeadership);
                }

                objOppProjectLeadership.GP_IsUpdated__c = true;
            }
        }

        if (lstOpportunityLeaderShiptoUpdate.size() > 0) {
            update lstOpportunityLeaderShiptoUpdate;
        }
    }
    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the isUpdated Checkbox on GP_Opp_Project_Leadership__c          
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------    
    private void updateIsUpdatedOnChange(Map < Id, SObject > oldSObjectMap) {
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
            GP_Opp_Project_Leadership__c objOldOppProjectLeadership = (GP_Opp_Project_Leadership__c) oldSObjectMap.get(objOppProjectLeadership.id);
            // change in LeaderShipRole
            if (!String.isBlank(objOppProjectLeadership.GP_Leadership_Role__c) && oldSObjectMap != null &&
                objOldOppProjectLeadership.GP_Leadership_Role__c != objOppProjectLeadership.GP_Leadership_Role__c) {
                objOppProjectLeadership.GP_IsUpdated__c = true;
            }
            // change in Employee
            if (objOppProjectLeadership.GP_Employee_ID__c != null && oldSObjectMap != null &&
                objOldOppProjectLeadership.GP_Employee_ID__c != objOppProjectLeadership.GP_Employee_ID__c) {
                objOppProjectLeadership.GP_IsUpdated__c = true;
            }
            // change in StartDate
            if (objOppProjectLeadership.GP_Start_date__c != null && oldSObjectMap != null &&
                objOldOppProjectLeadership.GP_Start_date__c != objOppProjectLeadership.GP_Start_date__c) {
                objOppProjectLeadership.GP_IsUpdated__c = true;
            }
            // change in EndDate
            if (objOppProjectLeadership.GP_End_date__c != null && oldSObjectMap != null &&
                objOldOppProjectLeadership.GP_End_date__c != objOppProjectLeadership.GP_End_date__c) {
                objOppProjectLeadership.GP_IsUpdated__c = true;
            }
        }
    }

    // ------------------------------------------------------------------------------------------------ 
    // Description: method will update the isUpdated Checkbox on Opportunity Project          
    // ------------------------------------------------------------------------------------------------
    // Input Param:: none
    // ------------------------------------------------------------------------------------------------
    // return  ::  void 
    // ------------------------------------------------------------------------------------------------
    // Created Date::23-02-2018   Created By::Pankaj Adhikari  Email:: pankaj.adhikari@saasfocus.com 
    // ------------------------------------------------------------------------------------------------    
    private void updateIsUpdatedOnOpportunityProject(Map < Id, SObject > oldSObjectMap) {
        if (GPCommon.isOpportunityProjectInsertionEvent)
            return;

        set < id > setOppProjectId = new set < id > ();
        for (GP_Opp_Project_Leadership__c objOppProjectLeadership: (List < GP_Opp_Project_Leadership__c > ) Records) {
            GP_Opp_Project_Leadership__c objOldOppProjectLeadership = (GP_Opp_Project_Leadership__c) oldSObjectMap.get(objOppProjectLeadership.id);
            // change in IsUpdated CheckBox
            if (objOppProjectLeadership.GP_IsUpdated__c && oldSObjectMap != null && !objOldOppProjectLeadership.GP_IsUpdated__c &&
                objOppProjectLeadership.GP_Opportunity_Project__c != null) {
                setOppProjectId.add(objOppProjectLeadership.GP_Opportunity_Project__c);
            }
        }
        if (setOppProjectId.size() > 0) {
            list < GP_Opportunity_Project__c > lstOpportunityProject = new list < GP_Opportunity_Project__c > ();
            for (id OpportunityProjectID: setOppProjectId) {
                lstOpportunityProject.add(new GP_Opportunity_Project__c(id = OpportunityProjectID, GP_IsUpdated__c = true));
            }
            update lstOpportunityProject;
        }
    }
}