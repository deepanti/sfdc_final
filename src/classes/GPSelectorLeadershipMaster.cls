public class GPSelectorLeadershipMaster extends fflib_SObjectSelector{
  public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Leadership_Master__c.Name
           
            };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Leadership_Master__c.sObjectType;
    }

    public List<GP_Leadership_Master__c> selectById(Set<ID> idSet) {
        return (List<GP_Leadership_Master__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Leadership_Master__c> getLeadershipMaster(GP_Project__c projectObj) {
        Id mandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Mandatory Key Members');
        Id accountLeadershipRecordTypeId   = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Account');
        Id hslLeadershipRecordTypeId       = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'HSL');
        Id roleMasterRecordTypeId          = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT  Id, 
                        GP_Sequence__c, 
                        GP_Category__c, 
                        GP_Mandatory__c,
                        RecordType.Name,
                        GP_Oracle_Id__c, //added by AMITPPT//15032018
                        GP_PID_Category__c,
                        GP_Employee_Master__c, 
                        GP_Leadership_Role__c, 
                        GP_Type_of_Leadership__c,
                        GP_Employee_Field_Type__c, 
                        GP_Mandatory_for_stage__c, 
                        GP_Employee_Master__r.Name,
                        GP_Mandatory_Service_Line__c,
                        GP_Editable_after_PID_Creation__c
                FROM GP_Leadership_Master__c
                WHERE (GP_Status__c = 'Active'
                    AND GP_Customer_L2_Name__c = :projectObj.GP_Business_Segment_L2__c
                    AND GP_SDO_Oracle_Id__c = :projectObj.GP_Primary_SDO__r.GP_Oracle_Id__c
                    AND GP_Customer_Sub_Division__c = :projectObj.GP_Parent_Customer_Sub_Division__c
                    AND RecordTypeId = :accountLeadershipRecordTypeId)
                OR(GP_Status__c = 'Active'
                   AND GP_HSL__c =:projectObj.GP_HSL__c
                   AND GP_Customer_L2_Name__c = :projectObj.GP_Business_Segment_L2__c
                   AND RecordTypeId = :hslLeadershipRecordTypeId)
                OR(GP_Status__c = 'Active' 
                   AND RecordTypeId = :roleMasterRecordTypeId)
                ORDER BY GP_Sequence__c
               ];

               // return [SELECT  Id, 
               //         GP_Sequence__c, 
               //         GP_Category__c, 
               //         GP_Mandatory__c,
               //         RecordType.Name,
               //         GP_Employee_Master__c, 
               //         GP_Leadership_Role__c, 
               //         GP_Oracle_Id__c, //added by AMITPPT//15032018
               //         GP_Type_of_Leadership__c,
               //         GP_Employee_Field_Type__c, 
               //         GP_Mandatory_for_stage__c, 
               //         GP_Employee_Master__r.Name,
               //         GP_Mandatory_Service_Line__c ,
               //         GP_PID_Category__c
               // FROM GP_Leadership_Master__c
               // WHERE (GP_Status__c = 'Active'
               //     AND GP_Customer_L2_Name__c = :projectObj.GP_Business_Segment_L2__c
               //     AND GP_Work_Location_SDO_Master__c = :projectObj.GP_Primary_SDO__c
               //     AND GP_Customer_Sub_Division__c = :projectObj.GP_Parent_Customer_Sub_Division__c
               //     AND RecordTypeId = :accountLeadershipRecordTypeId)
               // OR(GP_Status__c = 'Active'
               //    AND GP_HSL__c =:projectObj.GP_HSL__c
               //    AND GP_Customer_L2_Name__c = :projectObj.GP_Business_Segment_L2__c
               //    AND RecordTypeId = :hslLeadershipRecordTypeId)
               // OR(GP_Status__c = 'Active' 
               //    AND RecordTypeId = :roleMasterRecordTypeId)
               // ORDER BY GP_Sequence__c
               //];
    }
    
    public List<GP_Leadership_Master__c> getRoleMasterLeadership() {
        Id roleMasterRecordTypeId          = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT Id, 
                GP_PID_Category__c,
                GP_Leadership_Role__c, 
                RecordType.Name,
                GP_Employee_Master__c, 
                GP_Employee_Master__r.Name,
                GP_Mandatory_for_stage__c,
                GP_Sequence__c, GP_Category__c, 
                GP_Mandatory_Service_Line__c
                FROM GP_Leadership_Master__c
                WHERE RecordTypeId = :roleMasterRecordTypeId
                ORDER BY GP_Sequence__c
               ];
    }


    public List<GP_Leadership_Master__c> getBillableRoleMasterLeadership() {
        Id roleMasterRecordTypeId          = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT Id, 
                GP_PID_Category__c,
                GP_Leadership_Role__c, 
                RecordType.Name,
                GP_Employee_Master__c, 
                GP_Employee_Master__r.Name,
                GP_Mandatory_for_stage__c,
                GP_Sequence__c, GP_Category__c, 
                GP_Mandatory_Service_Line__c
                FROM GP_Leadership_Master__c
                WHERE RecordTypeId = :roleMasterRecordTypeId
                and GP_PID_Category__c = 'Billable'
                ORDER BY GP_Sequence__c
               ];
    }

    public List<GP_Leadership_Master__c> getNonBillableRoleMasterLeadership() {
        Id roleMasterRecordTypeId          = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT Id, 
                GP_PID_Category__c,
                GP_Leadership_Role__c, 
                RecordType.Name,
                GP_Employee_Master__c, 
                GP_Employee_Master__r.Name,
                GP_Mandatory_for_stage__c,
                GP_Sequence__c, GP_Category__c, 
                GP_Mandatory_Service_Line__c
                FROM GP_Leadership_Master__c
                WHERE RecordTypeId = :roleMasterRecordTypeId
                and GP_PID_Category__c = 'Support'
                ORDER BY GP_Sequence__c
               ];
    }
    
    public List<GP_Leadership_Master__c> getRoleMasterLeadership(set<String> setOracleRoleIDs) {
        Id roleMasterRecordTypeId          = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT Id, 
                GP_PID_Category__c,
                GP_Leadership_Role__c, 
                RecordType.Name,
                GP_Employee_Master__c, 
                GP_Employee_Master__r.Name,
                GP_Oracle_Id__c,
                GP_Mandatory_for_stage__c,
                GP_Sequence__c, GP_Category__c, 
                GP_Mandatory_Service_Line__c
                FROM GP_Leadership_Master__c
                WHERE RecordTypeId = :roleMasterRecordTypeId 
                AND GP_Oracle_Id__c in : setOracleRoleIDs
                ORDER BY GP_Sequence__c
               ];
    }
    
    public List<GP_Leadership_Master__c> getNonRoleMasterLeadership() {
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');

        return [SELECT Id, 
                       GP_PID_Category__c, 
                       GP_Employee_Master__c,
                       GP_Type_of_Leadership__c, 
                       GP_Mandatory_for_stage__c,
                       GP_Mandatory_Service_Line__c
                FROM GP_Leadership_Master__c
                WHERE RecordTypeId != :roleMasterRecordTypeId
               ];
    }


    public List<GP_Leadership_Master__c> getMandatoryKeyMemberLeadership(GP_Project__c projectObj) {
        Id mandatoryKeyMembersRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Mandatory Key Members');
        return [SELECT  Id, 
                        GP_Sequence__c, 
                        GP_Category__c, 
                        GP_Oracle_Id__c,
                        GP_Mandatory__c,
                        RecordType.Name,
                        GP_PID_Category__c,
                        GP_SDO_Oracle_Id__c,
                        GP_Employee_Master__c, 
                        GP_Leadership_Role__c, 
                        GP_Type_of_Leadership__c,
                        GP_Employee_Field_Type__c, 
                        GP_Mandatory_for_stage__c, 
                        GP_Employee_Master__r.Name,
                        GP_Mandatory_Service_Line__c                        
                    FROM GP_Leadership_Master__c
                    WHERE (GP_Status__c = 'Active'
                           AND RecordTypeId = :mandatoryKeyMembersRecordTypeId
                           AND GP_SDO_Oracle_Id__c = :projectObj.GP_Primary_SDO__r.GP_Oracle_Id__c)
                    ORDER BY GP_Sequence__c
                ];
    }
}