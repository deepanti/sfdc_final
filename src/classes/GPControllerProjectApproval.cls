// ------------------------------------------------------------------------------------------------ 
// Description: Class is controller of the GPCmpServiceProjectApproval, handles all the operation
// ------------------------------------------------------------------------------------------------
// Created Date::24-OCT-2017  Created By::Adarsh  Email:: adarsha.nanda@saasfocus.com 
// ------------------------------------------------------------------------------------------------ 
public without sharing class GPControllerProjectApproval {
    private Id projectId;
    private JSONGenerator gen;
    private fflib_SObjectUnitOfWork uow;
    private String approvalType;
    private String taxCodeId, taxCodeName, reports;
    private Boolean isRevenueStageValid = false;
    private Boolean isIndirectProject = false;
    private GP_Project__c projectObj;
	// PC Change
    private List<String> listOfProductCodes;
    
    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            ProcessInstanceWorkitem.SObjectType
            };
                
                public GPControllerProjectApproval(Id projectId) {
                    this.projectId = projectId;
                    uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
                }
    
    public GPAuraResponse getProjectDetail() {
        
        
        try {
            projectObj = [select id, GP_Parent_Project__c,
                          Name, GP_Parent_Project__r.GP_TAX_Code__c,
                          RecordType.Name, GP_Reports__c,
                          GP_Approval_Status__c,
                          GP_PID_Approver_User__c,
                          GP_Operating_Unit__r.GP_Is_GST_Enabled__c,
						  GP_Operating_Unit__r.GP_LE_Code__c,
						  GP_Operating_Unit__r.GP_Oracle_ID__c,
                          GP_Primary_SDO__r.GP_Oracle_ID__c,
                          GP_CRN_Number__r.GP_Status__c,						  
                          GP_Project_Stages_for_Approver__c, 
						  GP_Project_Status__c,
						  GP_Product_Code__c,// PC Change
                          GP_Business_Group_L1__c,// PC Change
						  GP_Service_Deliver_in_US_Canada__c,//Avalara Changes
                          (select id, SubmittedById from ProcessInstances ORDER BY CreatedDate DESC)
                          from GP_Project__c
                          where id =: projectId
                         ];
            
            reports = projectObj.GP_Reports__c;
			
			// PC Change
            Map<String,String> mapOfProductCodes = GPCommonUtil.getGlobalValues(new List<String>{'GE_Product_Code'});
            listOfProductCodes = mapOfProductCodes.values()[0].split(',');
            
            if (projectObj.GP_Parent_Project__c != null && projectObj.GP_Parent_Project__r.GP_TAX_Code__c != null) {
                List < GP_Pinnacle_Master__c > taxMaster = GPSelectorPinnacleMasters.getTaxMasterCodeRecord(projectObj.GP_Parent_Project__r.GP_TAX_Code__c);
                taxCodeId = taxMaster.size() > 0 ? taxMaster[0].Id : null;
                taxCodeName = taxMaster.size() > 0 ? taxMaster[0].Name : null;
                system.debug('taxCodeId---' + taxCodeId);
            }
            
            GPControllerProjectApprovalValidator projectApprovalValidator = new GPControllerProjectApprovalValidator(projectId);
            GPValidationResponse validationResponse;
            if (projectObj.GP_Approval_Status__c == 'Pending for Approval') {
                validationResponse = projectApprovalValidator.validateISApprovalRequired();
                approvalType = 'Approval';
            } else {
                validationResponse = projectApprovalValidator.validateISApprovalRequiredForClosure();
                approvalType = 'Closure';
            }
            if (validationResponse.isSuccess) {
                isIndirectProject = projectObj.RecordType.Name == 'Indirect PID';
                Map < String, Boolean > mapOfStageToIsApproved = (Map < String, Boolean > ) JSON.deserialize(System.label.GP_Approved_Icon_Status, Map < String, Boolean > .class);
                
                if (mapOfStageToIsApproved.containsKey(projectObj.GP_CRN_Number__r.GP_Status__c) && mapOfStageToIsApproved.get(projectObj.GP_CRN_Number__r.GP_Status__c)) {
                    isRevenueStageValid = true;
                }
                isRevenueStageValid = isIndirectProject || isRevenueStageValid;
                createJSON(projectObj);
            } else {
                return new GPAuraResponse(false, validationResponse.message, null);
            }
            
            return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }
    
    
    private void createJSON(GP_Project__c objProject) {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        if (objProject.GP_Project_Stages_for_Approver__c != null) {
            gen.writeStringField('strStageValue', objProject.GP_Project_Stages_for_Approver__c);
        }
        
        if (isRevenueStageValid != null) {
            gen.writeBooleanField('isRevenueStageValid', isRevenueStageValid);
        }
        
        if (isIndirectProject != null) {
            gen.writeBooleanField('isIndirectProject', isIndirectProject);
        }
        
        if (objProject.GP_PID_Approver_User__c == Userinfo.getUserId()) {
            gen.writeBooleanField('isPIDUser', true);
        } else {
            //Avalara - changes
            
            Set<String> avalaraUserIds = new Set<String>();
        
            //Get the list of Avalara Approvers from the custom settings.
            for(GP_Approver_List__c ap : GP_Approver_List__c.getAll().values()) {
                if(ap.GP_Is_Active__c && (ap.GP_Is_Avalara_Approver__c)) {
                    avalaraUserIds.add(ap.GP_Approver_Id__c);
                }
            }  
            if (avalaraUserIds != null && avalaraUserIds.size() > 0 && avalaraUserIds.contains(UserInfo.getUserId())) {
            gen.writeBooleanField('isPIDUser', true);
            }else{
            gen.writeBooleanField('isPIDUser', false);
            }
        }
        
        if (approvalType != null) {
            gen.writeStringField('approvalType', approvalType);
        }
        
        if (taxCodeName != null) {
            gen.writeObjectField('taxCodeName', taxCodeName);
        }
        
        if (projectObj != null) {
            gen.writeObjectField('projectObj', projectObj);
        }
        
        if (taxCodeId != null) {
            gen.writeObjectField('taxCodeId', taxCodeId);
        }
        
        if (reports != null) {
            gen.writeObjectField('reports', reports);
        }
		// PC Change
        if(listOfProductCodes != null) {
            gen.writeObjectField('listOfProductCodes', listOfProductCodes);
        }
        
        gen.writeEndObject();
    }
    
    public GPAuraResponse ProjectStatusChange(string strSelectedStage, string strComment, string strStatus, string taxCode, string reports, String productCode) {
        
        SavePoint s = database.setSavepoint();
        try {
            List < Approval.ProcessWorkitemRequest > lstRequests = getRequestList(projectId, strStatus, strComment);
            
            if (lstRequests != null && lstRequests.size() > 0) {
                
                //GP_Project__c objProject = new GP_Project__c();
                //objProject.Id = projectId;
                GP_Project__c objProject = new GPSelectorProject().selectProjectRecord(projectId);
                if (strStatus == 'Approve') {
                    objProject.GP_Project_Status__c = strSelectedStage;
                    if (taxCode != null) {
                        objProject.GP_TAX_Code__c = taxCode;
                    }
                    if (reports != null) {
                        objProject.GP_Reports__c = reports;
                    }
                }
				
				// PC Change
                if(String.isNotBlank(productCode)) {
                    objProject.GP_Product_Code__c = productCode;
                }
                
                // for china SDO
                string strMsg = strStatus == 'Approve' ? 'Project Approved.' : 'Project Rejected.';
                GPCheckRecursive.ApprovedfromScreen = true; 
                system.debug(objProject.GP_Band_3_Approver_Status__c);
                if (objProject.GP_Band_3_Approver_Status__c == 'Pending' &&
                    objProject.GP_Additional_Approval_Required__c && objProject.GP_Primary_SDO__r.GP_isChina__c) {
                        Approval.ProcessResult[] processResults = Approval.process(lstRequests);
                        if (strStatus == 'Approve') {
                            new GPServiceSubmitforApproval(new set < id > { objProject.id }).submitforApproval();
                        }
                        
                        return new GPAuraResponse(true, strStatus, null);
                    }
                
                Approval.ProcessResult[] processResults = Approval.process(lstRequests);
                //update objProject;
                if (objProject.GP_Approval_Status__c == 'Pending for Approval') {
                    GPServiceProjectApproval.setApprovalStageForProject(objProject, strStatus, strComment);
                    update(objProject);
                }
                //uow.commitWork();
                system.debug('lstRequests' + lstRequests);
                
                //string strMsg = strStatus == 'Approve' ? 'Project Approved.' : 'Project Rejected.';
                
                return new GPAuraResponse(true, strMsg, null);
            }
            return new GPAuraResponse(false, 'No Approvals Found.', null);
        } catch (exception ex) {
            database.rollback(s);
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
    }
    
    private List<Approval.ProcessWorkitemRequest> getRequestList(Id targetRecordId, String strStatus, String strComment) {
        List < Approval.ProcessWorkitemRequest > lstRequests = new List < Approval.ProcessWorkitemRequest > ();
        List < ProcessInstanceWorkitem > lstWorkItems = [SELECT Id, ProcessInstanceId, ProcessInstance.Status, ActorId
                                                         FROM ProcessInstanceWorkitem
                                                         WHERE
                                                         ProcessInstance.TargetObjectId =: targetRecordId and
                                                         ProcessInstance.Status =: 'Pending'
                                                        ];
        
        if (lstWorkItems != null && lstWorkItems.size() > 0) {
            set < Id > setOfGroupId = new set < Id > ();
            boolean isFound = false;
            
            // START: changes by Shankar Dutt on 18-Dec-2018.
            Set<Id> approverIds = new Set<Id>();
            
            //Get the list of BPR Approvers from the custom settings.
            for(GP_Approver_List__c ap : GP_Approver_List__c.getAll().values()) {
                if(ap.GP_Is_Active__c && ap.GP_Is_BPR_Approver__c) {
                    approverIds.add(ap.GP_Approver_Id__c);
                }
            }
            system.debug('==<<<<<Set of approverIds>>>>>=='+approverIds);
            // Check if logged in user is BPR Approver.
            Boolean isLoggedInUserBPRApprover = approverIds.size() > 0 && approverIds.contains(UserInfo.getUserId());
            
            for (ProcessInstanceWorkitem EachWorkItem: lstWorkItems) {
                if(string.valueOf(EachWorkItem.ActorId).startsWith('005')) {
                    // If logged-in user a BPR approver then collect all the BPR approval work items of the PID and approve/reject all.
                    if (isLoggedInUserBPRApprover && approverIds.contains(EachWorkItem.ActorId)) {
                        isFound = true;
                        Approval.ProcessWorkitemRequest req = ApprovalProcessChange(EachWorkItem, strStatus, strComment);
                        lstRequests.add(req);
                    } else {
                        // If logged-in user not a BPR approver then just collect the approval work items of logged-in user.
                        if (EachWorkItem.ActorId == UserInfo.getUserId()) {
                            isFound = true;
                            Approval.ProcessWorkitemRequest req = ApprovalProcessChange(EachWorkItem, strStatus, strComment);
                            lstRequests.add(req);
                        }
                    }
                } else if (string.valueOf(EachWorkItem.ActorId).startsWith('00G')) {
                    setOfGroupId.add(EachWorkItem.ActorId);
                }
            }
            
            if (!isFound || Test.isRunningTest()) {   
                Set<id> userOrGroupSet = new Set<id>();
                
                if(isLoggedInUserBPRApprover) {
                    // If logged-in user a BPR approver then add all the BPR approvers in the set 
                    // so that all the approval work items in the PID belonging to their groups are collected.
                    userOrGroupSet = approverIds;
                } else {
                    userOrGroupSet.add(UserInfo.getUserId());
                }
                system.debug('==userOrGroupSet@=='+userOrGroupSet);   
                
                if (setOfGroupId != null && setOfGroupId.size() > 0) {
                    list < GroupMember > lstMembers = [select id, UserOrGroupId, GroupId
                                                       from GroupMember
                                                       where GroupId in: setOfGroupId
                                                       and UserOrGroupId in: userOrGroupSet
                                                      ];
                    // END: changes by shankar Dutt on 18-Dec-2018.
                    set < Id > setOfSelectedGroupId = new set < Id > ();
                    if (lstMembers != null && lstMembers.size() > 0) {
                        for (GroupMember EachGroupMember: lstMembers) {
                            setOfSelectedGroupId.add(EachGroupMember.GroupId);
                        }
                        
                        for (ProcessInstanceWorkitem EachWorkItem: lstWorkItems) {
                            if (string.valueOf(EachWorkItem.ActorId).startsWith('00G') && setOfSelectedGroupId.contains(EachWorkItem.ActorId)) {
                                Approval.ProcessWorkitemRequest req = ApprovalProcessChange(EachWorkItem, strStatus, strComment);
                                lstRequests.add(req);
                            }
                        }
                    }
                }
            }
        }
        return lstRequests;
    }
    
    private static Approval.ProcessWorkitemRequest ApprovalProcessChange(ProcessInstanceWorkitem EachWorkItem, string strStatus, string strComment) {
        Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
        req.setWorkitemId(EachWorkItem.Id);
        req.setAction(strStatus);
        req.setComments(strComment);
        return req;
    }
    
    public GPAuraResponse ProjectReAssign(string strUserId, string strComment) {
        try {
            List < ProcessInstanceWorkitem > lstWorkItems = [SELECT Id, ProcessInstanceId,
                                                             ProcessInstance.Status, ActorId
                                                             FROM ProcessInstanceWorkitem WHERE
                                                             ProcessInstance.TargetObjectId =: projectId and
                                                             ProcessInstance.Status =: 'Pending'
                                                            ];
            
            if (lstWorkItems != null && lstWorkItems.size() > 0) {
                set < Id > setOfGroupId = new set < Id > ();
                list < ProcessInstanceWorkitem > lst2Update = new list < ProcessInstanceWorkitem > ();
                
                for (ProcessInstanceWorkitem EachWorkItem: lstWorkItems) {
                    if (string.valueOf(EachWorkItem.ActorId).startsWith('005')) {
                        if (EachWorkItem.ActorId == UserInfo.getUserId()) {
                            EachWorkItem.ActorId = strUserId;
                            lst2Update.add(EachWorkItem);
                        }
                    }
                }
                
                if (lst2Update != null && lst2Update.size() > 0) {
                    //update lst2Update;
                    uow.registerDirty(lst2Update);
                    uow.commitWork();
                    return new GPAuraResponse(true, 'Successfully re-Assigned.', null);
                } else {
                    return new GPAuraResponse(false, 'Cannot re-assign the approval.', null);
                }
                
            }
            return new GPAuraResponse(false, 'Something went wrong, Please contact to system admin.', null);
        } catch (exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }
}