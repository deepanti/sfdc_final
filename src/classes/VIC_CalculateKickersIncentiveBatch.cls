global class VIC_CalculateKickersIncentiveBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String dealType = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Deal_Type');
        String strCPQDealStatus = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('CPQ_Deal_Status');
        String opportunityStagename = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Opportunity_Eligible_Stage_For_VIC');
        
        VIC_Process_Information__c vicInfo = vic_CommonUtil.getVICProcessInfo();
        Date processingYearStart = DATE.newInstance(Integer.ValueOf(vicInfo.VIC_Process_Year__c),1,1);
        Date processingYearEnd =  processingYearStart.addYears(1).addDays(-1);  
                
        // getting ature of works for it services
        Set<String> naturOfWorks = VIC_NatureOfWorkToDealTypeCtlr.getNatureOfWorkListForDealType('IT Services');
        
        String strMinContractTerm = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Min_Contract_Term_For_Kickers_Calc');
        Decimal minContractTerm = VIC_GeneralChecks.IsNotNullAndBlank(strMinContractTerm) && VIC_GeneralChecks.isDecimal(strMinContractTerm) ? Decimal.valueof(strMinContractTerm) : 24;
        System.debug('====>>strCPQDealStatus: '+strCPQDealStatus+', strMinContractTerm: '+ strMinContractTerm+ ', naturOfWorks: '+ naturOfWorks);
        System.Debug('ST---->' + processingYearStart );
        System.Debug('ST---->' + processingYearEnd );
        
        return Database.getQueryLocator([
            Select Id, vic_Is_Kickers_Calculated__c, Sales_country__c, AccountId, Actual_Close_Date__c, (
                SELECT Id, Nature_of_Work__c, Pricing_Deal_Type_OLI_CPQ__c, Opportunity.Actual_Close_Date__c, Opportunity.CloseDate,
                VIC_NPV__c, vic_Contract_Term__c, Contract_Term__c,  Opportunity.Id,
                CYR__c, TCV__c, Opportunity.OwnerId, Product_BD_Rep__c, vic_VIC_User_3__c, vic_VIC_User_4__c, 
                vic_Primary_Sales_Rep_Split__c, vic_Product_BD_Rep_Split__c, vic_VIC_User_3_Split__c, 
                vic_VIC_User_4_Split__c, vic_Sales_Rep_Approval_Status__c, vic_Product_BD_Rep_Approval_Status__c,
                Opportunity.Sales_country__c,
                Opportunity.Account.Business_Group__c, Opportunity.Account.Business_Segment__r.Name 
                FROM OpportunityLineItems 
                WHERE Opportunity.StageName =: opportunityStagename
                AND VIC_NPV__c >=0
                AND Pricing_Deal_Type_OLI_CPQ__c =: dealType 
                AND vic_CPQ_Deal_Status__c =: strCPQDealStatus 
                AND vic_Final_Data_Received_From_CPQ__c = true
                AND Nature_of_Work__c IN :naturOfWorks
                AND Contract_Term__c >=: minContractTerm
                AND (
                    (vic_Sales_Rep_Approval_Status__c = 'Approved' OR vic_Product_BD_Rep_Approval_Status__c = 'Approved')
                    //OR (vic_Sales_Rep_Approval_Status__c = 'Approved' AND vic_Product_BD_Rep_Approval_Status__c = 'Not Required')
                ) ) 
            FROM Opportunity 
            WHERE Actual_Close_Date__c >= :processingYearStart
            AND Actual_Close_Date__c <= :processingYearEnd
        ]);
        
        /*
AND (vic_is_CPQ_Value_Changed__c = true OR 
(vic_is_CPQ_Value_Changed__c = false AND vic_is_First_Time_Incentive_Calculated__c = false)) 
*/
    }
    
    global void execute(Database.BatchableContext BC, List<Opportunity> opps) {
        System.debug(opps);
        try{
            VIC_KickersIncentiveBatchHelper batchHelper = new VIC_KickersIncentiveBatchHelper();
            Set<Id> userIds = new Set<Id>();
            Set<Id> oppIds = new Set<Id>();
            Map<Id, String> userIdVsPlanCode;
            Map<Id, Target_Component__c> userIdVsTargetComponent;
            
            for(Opportunity opp: opps){
                System.debug('===>>Inside Opportunity Loop:  oppId: '+opp.Id);
                oppIds.add(opp.Id);
                for(OpportunityLineItem oppProd: opp.OpportunityLineItems){
                     System.debug('===>>Inside Opportunity Line Item Loop');
                    List<Id> userIdList = batchHelper.getOpportunityProductUsers(oppProd);
                    System.debug('===>>'+userIdList);
                    userIds.addAll(userIdList);
                }
            }
            System.debug('====>>userIds: '+userIds);
            userIdVsPlanCode = batchHelper.getUsersVsPlanMap(userIds);
            userIdVsTargetComponent = batchHelper.getTargetComponents(userIds, oppIds);
            String tcvLimitStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Kickers_TCV_Limit');
            String incentiveAmountStr = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Kickers_Incentive_Amount');
            
            Decimal tcvLimit = VIC_GeneralChecks.isDecimal(tcvLimitStr) ? Decimal.valueOf(tcvLimitStr) : 0;
            Decimal incentiveAmount = VIC_GeneralChecks.isDecimal(incentiveAmountStr) ? Decimal.valueOf(incentiveAmountStr) : 0;
            
            List<Target_Achievement__c> incentivesToUpdate = new List<Target_Achievement__c>();
            
            Map<Id, User> userIdVsUserMap = new Map<Id, User>([SELECT Id, Domicile__c 
                                                               FROM User 
                                                               WHERE Id =: userIds
                                                               AND isActive = true
                                                              ]);
            
            for(Opportunity opp: opps){
                /*System.debug('====>>opp: '+ opp);
                System.debug('====>>userIdVsPlanCode: '+userIdVsPlanCode);
                System.debug('====>>userIdVsTargetComponent: '+ userIdVsTargetComponent);
                System.debug('====>>tcvLimit: '+ tcvLimit);
                System.debug('====>>userIdVsUserMap: '+ userIdVsUserMap);*/
                if(!opp.OpportunityLineItems.isEmpty()){
                    System.debug('OLI available for Opportunity: '+ opp);
                    List<Target_Achievement__c> partOfIncentiveRecords =  batchHelper.getIncentiveRecordsForOpportunity(opp, 
                                                                                            userIdVsPlanCode, 
                                                                                            userIdVsTargetComponent, 
                                                                                            tcvLimit, 
                                                                                            incentiveAmount, 
                                                                                           userIdVsUserMap);
                    System.debug('====>>partOfIncentiveRecords: '+partOfIncentiveRecords);
                    if(partOfIncentiveRecords != null && !partOfIncentiveRecords.isEmpty()){
                        incentivesToUpdate.addAll(partOfIncentiveRecords);
                    }
                    //opp.vic_Is_Kickers_Calculated__c = true;
                    //opp.vic_Is_CPQ_Value_Changed__c = false;
                }
                else{
                    System.debug('No OLI available for Opportunity: '+ opp);
                }
            }
            
            INSERT incentivesToUpdate;
            //UPDATE opps;
        }
        catch(Exception ex){
            LoggerUtility.logException('VIC_CalculateKickersIncentiveBatch', 'execute', LoggerUtility.LogLevel.EXC, '', '', '', LoggerUtility.RefCategory.Batch, ex);
            LoggerUtility.commitLog();
        }
    }   
    
    global void finish(Database.BatchableContext BC) {
        System.debug('Kickers job finished!');
        //calling new logo bonous on finish
        if(!Test.isRunningTest()){
            VIC_CalculateNewLogoBonusBatch newLogoBonusBatch = new VIC_CalculateNewLogoBonusBatch();
            Database.executeBatch(newLogoBonusBatch, 2);
        }
    }
}