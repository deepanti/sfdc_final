/*************************************************************************************************************************
        * @name : HomePageHighlightReportTest
        * @author : Persistent
        * @description  : To Test HomePageHighlightReport
**************************************************************************************************************************/
@isTest(SeeAllData='true')
public class HomePageHighlightReportTest {
/*************************************************************************************************************************
        * @name : testReportWithTestData
        * @author : Persistent
        * @description  : Tests the apex report running logic, reports need all data to be true
        * @param  : na
        * @return : na
**************************************************************************************************************************/
   // @isTest(SeeAllData='true')
    public static testMethod void testReportWithTestData() {
        
        //Fetch the report
        List <Report> reportList = [SELECT Id,DeveloperName FROM Report where DeveloperName = 'Pipeline_Report_for_TCV_Lightning_20181'];
       system.debug(':==reportList--:'+reportList.size()+':==reportList--:'+reportList);
        // String reportId = (String)reportList.get(0).get('Id');
        String reportId = string.valueof(reportList[0].id);
        //Run test
        Test.startTest();
        HomePageHighlightReport.getReportData(reportId,false);
        Test.stopTest();
    }
    
}