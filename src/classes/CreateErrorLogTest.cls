@isTest
public class CreateErrorLogTest {

    @TestSetup static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE'); 
        
    }
    
    @isTest static void testCreateRecord()
    {
        Business_Segments__c bs=[SELECT Id from Business_Segments__c WHERE Name='Test'];
        try{
            bs.Name='test1';
            update bs;
            integer i=5/0;
           }
        catch(Exception e)
        { 
           CreateErrorLog.createErrorRecord(bs.Id, e.getMessage(), '', e.getStackTraceString(), '','', '', '', String.valueOf(e.getLineNumber()));
        }
       
    }
    
}