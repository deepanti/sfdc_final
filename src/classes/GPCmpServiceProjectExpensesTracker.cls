@isTest
public class GPCmpServiceProjectExpensesTracker {

    public static GP_Project_Expense__c objPrjExpense;
    public static GP_Project__c prjObj;
    public static String jsonresp;
    public static Id InvalidIdForCoveringCatch;

    @testSetup static void setupCommonData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        objuserrole.GP_Role__c = objrole.id;
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        InvalidIdForCoveringCatch = dealObj.id;
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        insert prjObj;

        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;

        GP_Project_Expense__c objPrjExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert objPrjExpense;


    }

    @isTest static void testGPCmpServiceProjectExpense() {
        fetchData();
        testgetProjectExpenseData();
        testsaveProjectExpenseService();
        testsaveProjectExpenseService();
        testdeleteProjectExpense();
        testgetOMSProjectExpenseData();
        testgetProjectExpenseRecordForCatch();
        testdeleteProjectExpenseForCatch();
        testgetOMSProjectExpenseRecordForCatch();
    }

    public static void fetchData() {
        prjObj = [select id, Name from GP_Project__c limit 1];
        objPrjExpense = [select id, Name from GP_Project_Expense__c limit 1];
        jsonresp = (String) JSON.serialize(new List < GP_Project_Expense__c > { objPrjExpense });
    }

    public static void testgetProjectExpenseData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectExpenses.getProjectExpenseData(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testsaveProjectExpenseService() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectExpenses.saveProjectExpenseService(jsonresp, 1, prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testdeleteProjectExpense() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectExpenses.deleteProjectExpense(objPrjExpense.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetOMSProjectExpenseData() {
        GPAuraResponse returnedResponse = GPCmpServiceProjectExpenses.getOMSProjectExpenseData(prjObj.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    public static void testgetProjectExpenseRecordForCatch() {
        GPControllerProjectExpenses prjExpns = new GPControllerProjectExpenses();
        GPAuraResponse returnedResponse = prjExpns.getProjectExpenseRecord();
        System.assertEquals(false, returnedResponse.isSuccess);
    }

    public static void testdeleteProjectExpenseForCatch() {
        GPControllerProjectExpenses prjExpns = new GPControllerProjectExpenses();
        GPAuraResponse returnedResponse = prjExpns.deleteProjectExpense(InvalidIdForCoveringCatch);
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    public static void testgetOMSProjectExpenseRecordForCatch() {
        GPControllerProjectExpenses prjExpns = new GPControllerProjectExpenses();
        GPAuraResponse returnedResponse = prjExpns.getOMSProjectExpenseRecord();
        System.assertEquals(false, returnedResponse.isSuccess);
    }
}