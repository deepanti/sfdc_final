public class DeleteOppSPlit
{    
    public static List<OpportunitySplit> opp_Split_List ;   
    public static void deleteOppSplitOwner( List<Account> accList,Map<ID, Account> old_acc_map) 
    {        
        system.debug('=====accList ====='+accList );
        system.debug('=====old_acc_map====='+old_acc_map);
        List<ID> toBeDeletedSplit = new List<ID>();
        Set<Id> acctIds = new Set<Id>();
        Set<Id> oppIds = new Set<ID>();
        Set<Id> oOwnerIds = new Set<ID>();
        Set<Id> oliIds = new Set<Id>();
        Set<Id> oppsplittypeIds = new Set<ID>();
        Set<ID> oppOwnerIds = new Set<Id>();
        Set<Id> bd_reps= new Set<ID>();
        
        List <OpportunitySplit> splitToInsert = new List<OpportunitySplit>();
        for(Account acct : accList) 
        {        
            system.debug('=========1=============');
            acctIds.add(acct.Id);  
        }
        system.debug('=======acctIds====='+acctIds);
        
        List<Opportunity> optyList = [SELECT id,OwnerId,Account.Client_Partner__c, account.Sales_Unit__c,Opportunity_Source__c, Amount, StageName, CloseDate FROM Opportunity WHERE AccountId in :acctIds AND IsClosed = False];
        system.debug('=====optyList========'+optyList);
        if(optyList.size() > 0)
        {
                for(Opportunity oppty : optyList)
                {
                    system.debug('=========2=============' );
                    // oppOwnerIds.add(oppty.OwnerId);
                    oppIds.add(oppty.Id);        
                }
                List<OpportunityLineItem> OLI_list = [select id, OpportunityId,Product_BD_Rep__c from OpportunityLineItem where OpportunityId IN:oppIds];       
                
                List<OpportunitySplitType> opportunity_split_type = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
               // OpportunitySplitType opportunity_split_typeID = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
          
            system.debug('=======opportunity_split_type====='+opportunity_split_type);
            for(OpportunitySplitType splittype : opportunity_split_type)
            {
                system.debug('=========3=============' );
                oppsplittypeIds.add(splittype.id);                
            }
            for(Opportunity oppty : optyList)
            {
                system.debug('=========2=============' );
                oppOwnerIds.add(oppty.OwnerId);
                oppIds.add(oppty.Id); 
                // salesleaderids.add(oppty.account.Sales_Leader_User_Id__c);
                for(OpportunityLineItem oli:OLI_list){ 
                    oliIds.add(oli.id);
                    bd_reps.add(oli.Product_BD_Rep__c);
                }
            }
           // List <AggregateResult> opp_line_item_list = [Select OpportunityId, Product_BD_Rep__c, SUM(UnitPrice) FROM OpportunityLineItem WHERE opportunityId IN :oppIds AND Product_BD_Rep__c IN:bd_reps  GROUP By Product_BD_Rep__c, OpportunityId];  Query not in use
            
            List<OpportunityLineItem> OppOLI_List =[select id, OpportunityId, Product_BD_Rep__c from OpportunityLineItem where id IN:oliIds];       
            
            system.debug('======oppIds========1========='+oppIds);
            system.debug('======oppsplittypeIds=========1==='+oppsplittypeIds);
            List<OpportunitySplit> optysplitList = [SELECT id,SplitOwnerId,OpportunityId  FROM OpportunitySplit WHERE OpportunityId in :oppIds and splitTypeId in:oppsplittypeIds];
            system.debug('======optysplitList===1===='+optysplitList);
            system.debug('========accList===1======'+accList);
            
            for(Account acct : accList)
            {            
                boolean isExist = false ;              
                Account oldacct = old_acc_map.get(acct.Id);
                system.debug('++1234++++'+acct.Client_Partner__c);
                system.debug('++12345++++'+oldacct.Client_Partner__c);
                system.debug('++12345++++'+oldacct.Sales_Leader_User_Id__c);
                for(Opportunity o:optyList)
                {
                   for(OpportunityLineItem oli:OLI_list) { 
                       oliIds.add(oli.id);
                    bd_reps.add(oli.Product_BD_Rep__c);
                    }
                    for(OpportunitySplit oppsplit :optysplitList)
                    {                
                        if(optysplitList.size()>0)
                        {                       
                            system.debug('++12345678++++'+oppsplit.SplitOwnerId);
                            if(oldacct.Client_Partner__c == oppsplit.SplitOwnerId && !isExist &&(oldacct.Client_Partner__c!=acct.Client_Partner__c)) 
                            { 
                                toBeDeletedSplit.add(oldacct.Client_Partner__c);
                                isExist = true;
                                system.debug('toBeDeletedSplit=='+toBeDeletedSplit);
                            }
                            isExist = false;
                            if(acct.Client_Partner__c == oppsplit.SplitOwnerId && !isExist && (oldacct.Client_Partner__c!=acct.Client_Partner__c)) 
                            { 
                                toBeDeletedSplit.add(acct.Client_Partner__c);
                                isExist = true;
                                system.debug('toBeDeletedSplitabc=='+toBeDeletedSplit);
                            }
                            isExist = false;
                            if(acct.Sales_Leader_User_Id__c == oppsplit.SplitOwnerId && !isExist && (oldacct.Sales_Leader_User_Id__c!=acct.Sales_Leader_User_Id__c)){
                                toBeDeletedSplit.add(acct.Sales_Leader_User_Id__c);
                                isExist = true;
                                system.debug('toBeDeletedSplitdef=='+toBeDeletedSplit);
                            }  
                            isExist = false;
                            
                            if(oldacct.Sales_Leader_User_Id__c == oppsplit.SplitOwnerId && !isExist && (oldacct.Sales_Leader_User_Id__c!=acct.Sales_Leader_User_Id__c)){
                                toBeDeletedSplit.add(oldacct.Sales_Leader_User_Id__c);
                                isExist = true;
                                system.debug('toBeDeletedSplit123=='+toBeDeletedSplit);
                            }   
                            isExist = false;                      
                        }               
                    }
                }           
            }      
            system.debug('toBeDeletedSplit 1=='+toBeDeletedSplit);
            system.debug('oppIds1=='+oppIds);    
        }
        
        if(toBeDeletedSplit.size() >0 && oppIds.size() >0)
        {
            delete [SELECT id FROM OpportunitySplit WHERE SplitOwnerId in :toBeDeletedSplit AND OpportunityID IN :oppids AND splitTypeId in:oppsplittypeIds];
           
        }        
    }
    public static void deleteOppTeamMember( List<Account> accList,Map<ID, Account> old_acc_map) 
    { 
        
        system.debug('=====accList ====='+accList );
        system.debug('=====old_acc_map====='+old_acc_map);
        List<ID> toBeDeletedOTM = new List<ID>();
        Set<Id> acctIds = new Set<Id>();
        Set<Id> oppIds = new Set<ID>();
        Set<Id> salesleaderids = new Set<ID>();
        Set<Id> oliIds = new Set<Id>();
        Set<Id> oppsplittypeIds = new Set<ID>();
        Set<ID> oppOwnerIds = new Set<Id>();
        Set<Id> bd_reps= new Set<ID>();
         Set<Id> bd_repIds= new Set<ID>();
        Set<ID> SuperSLIDS = new Set<ID>();
        List<OpportunityLineItem> newoliList = new List<OpportunityLineItem>();
        
        for(Account acct : accList) 
        {        
            system.debug('=========1=============');
            acctIds.add(acct.Id);  
           
        }
        system.debug('=======acctIds====='+acctIds);
        
        List<Opportunity> optyList = [SELECT id,OwnerId,Account.Client_Partner__c,StageName,account.Sales_Unit__c,account.Sales_Leader_User_Id__c  FROM Opportunity WHERE AccountId in :acctIds AND IsClosed = False AND StageName!='Prediscover'];
        system.debug('=====optyList========'+optyList);
        if(optyList.size() > 0)
        {
             for(Opportunity oppty : optyList)
            {
                system.debug('=========2=============' );
                // oppOwnerIds.add(oppty.OwnerId);
                oppIds.add(oppty.Id);        
            }
            List<OpportunityLineItem> OLI_list = [select id, OpportunityId, Product_BD_Rep__c from OpportunityLineItem where OpportunityId IN:oppIds];       
        
            List<OpportunitySplitType> opportunity_split_type = [SELECT ID FROM OpportunitySplitType WHERE MasterLabel = 'Overlay' LIMIT 1];
            for(OpportunitySplitType oppsplittype:opportunity_split_type)
            {
                oppsplittypeIds.add(oppsplittype.id);
            }
            List<OpportunitySplit> optysplitList = [SELECT id,SplitOwnerId,OpportunityId  FROM OpportunitySplit WHERE OpportunityId in :oppIds and splitTypeId in:oppsplittypeIds];
        
           for(Opportunity oppty : optyList)
            {
                system.debug('=========2=============' );
                oppOwnerIds.add(oppty.OwnerId);
                oppIds.add(oppty.Id); 
                salesleaderids.add(oppty.account.Sales_Leader_User_Id__c);
                for(OpportunityLineItem oli:OLI_list)
                { 
                    oliIds.add(oli.id);
                    bd_reps.add(oli.Product_BD_Rep__c);
                }
            }
           // List <AggregateResult> opp_line_item_list = [Select OpportunityId, Product_BD_Rep__c, SUM(UnitPrice) FROM OpportunityLineItem WHERE opportunityId IN :oppIds AND Product_BD_Rep__c IN:bd_reps  GROUP By Product_BD_Rep__c, OpportunityId]; query not in use
            
            List<OpportunityLineItem> OppOLI_List =[select id, OpportunityId, Product_BD_Rep__c from OpportunityLineItem where id IN:oliIds];       
        
            List<OpportunityTeamMember> OTM_List = [select id,userid,opportunityid from opportunityteammember where opportunityid in:oppids];
            for(Account acct : accList)
            {            
                boolean isExist = false ;              
                Account oldacct = old_acc_map.get(acct.Id);
                system.debug('++1234++++'+acct.Client_Partner__c);
                system.debug('++12345++++'+oldacct.Client_Partner__c);
                system.debug('++12345++++'+oldacct.Sales_Leader_User_Id__c);
                 salesleaderids.add(acct.Sales_Leader_User_Id__c);
                SuperSLIDS.add(acct.Client_Partner__c);
                for(OpportunityTeamMember OTMEmber:OTM_List)
                {
                    system.debug('@@@@@@@'+OTM_List.size());                
                    if(OTM_List.size()>0)
                    {                    
                        system.debug('++12345678++++'+OTMEmber.userid);
                        if(oldacct.Client_Partner__c == OTMEmber.userid && !isExist &&(oldacct.Client_Partner__c!=acct.Client_Partner__c)) 
                        { 
                            toBeDeletedOTM.add(oldacct.Client_Partner__c);
                            isExist = true;
                            system.debug('toBeDeletedOTM=='+toBeDeletedOTM);
                        }
                        isExist = false;
                        if(acct.Client_Partner__c == OTMEmber.userid && !isExist && (oldacct.Client_Partner__c!=acct.Client_Partner__c)) 
                        { 
                            toBeDeletedOTM.add(acct.Client_Partner__c);
                            isExist = true;
                            system.debug('toBeDeletedOTMabc=='+toBeDeletedOTM);
                        }
                        isExist = false;
                        if(acct.Sales_Leader_User_Id__c == OTMEmber.userid && !isExist && (oldacct.Sales_Leader_User_Id__c!=acct.Sales_Leader_User_Id__c)){
                            toBeDeletedOTM.add(acct.Sales_Leader_User_Id__c);
                            isExist = true;
                            system.debug('toBeDeletedOTMdef=='+toBeDeletedOTM);
                        }  
                        isExist = false;
                        
                        if(oldacct.Sales_Leader_User_Id__c == OTMEmber.userid && !isExist && (oldacct.Sales_Leader_User_Id__c!=acct.Sales_Leader_User_Id__c)){ 
                            toBeDeletedOTM.add(oldacct.Sales_Leader_User_Id__c);
                            isExist = true;
                            system.debug('toBeDeletedOTM123=='+toBeDeletedOTM);
                        }   
                        isExist = false;  
                        
                    }
                }
                for(Opportunity o:optyList)
                {         
                    for(OpportunityLineItem ol:OppOLI_List) 
                    {
                        System.debug('bdrepsizete=='+bd_repIds);
                        bd_repIds.add(ol.Product_BD_Rep__c);
                    }
                }        
                system.debug('toBeDeletedSplit 1=='+toBeDeletedOTM);
                system.debug('oppIds1=='+oppIds);
            }
            
            if(toBeDeletedOTM.size() >0 && oppIds.size() >0)
            {            
               // List<OpportunityTeamMember> otm =  [SELECT id FROM OpportunityTeamMember WHERE UserId in :toBeDeletedOTM AND OpportunityID IN :oppids AND (TeamMemberRole='Super SL' or TeamMemberRole='SL' or TeamMemberRole='BD Rep')];  //query not in use
                
                delete [SELECT id FROM OpportunityTeamMember WHERE UserId in :toBeDeletedOTM AND OpportunityID IN :oppids AND UserId NOT IN:oppOwnerIds AND (TeamMemberRole='Super SL' or TeamMemberRole='SL' or TeamMemberRole='BD Rep')];
               // System.debug('bdrepsize=='+bd_repIds.size());
               if(bd_repIds.size()>0 )
                {
                    System.debug('bdrepsizetest==');           
                    delete [SELECT id FROM OpportunitySplit WHERE SplitOwnerID IN:bd_repIds AND SplitOwnerID NOT IN:SuperSLIDS AND SplitownerId NOT IN :salesleaderids AND OpportunityID IN :oppids AND splitTypeId in:oppsplittypeIds];
                }
                if(OppOLI_List.size()>0)
                {
                    //This OpportunityLineItemTriggerHelper is used to call Krati's logic for insertion of BD rep again.
                    OpportunityLineItemTriggerHelper.insertOpportunityTeamMember(OppOLI_List);     
                }
            }              
          }          
        }
}