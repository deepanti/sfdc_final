@isTest
public class GPSelectorBudgetMasterTracker {
    @testSetup
    public static void buildDependencyData() {
        GP_Budget_Master__c budgetMaster = GPCommonTracker.getBudgetMaster();
        insert budgetMaster;
    }
    
    @isTest
    public static void testSelectById() {
        GPSelectorBudgetMaster budgetSelector = new GPSelectorBudgetMaster();
        GP_Budget_Master__c expectedBudgetMaster = [SELECT Id FROM GP_Budget_Master__c LIMIT 1];
        List<GP_Budget_Master__c> returnedBudgetMasters = budgetSelector.selectById(new Set<Id> {expectedBudgetMaster.Id});
        System.assertEquals(expectedBudgetMaster.Id, returnedBudgetMasters[0].Id);
    }
    
    @isTest
    public static void testSelectAllMasterBudgetRecords() {
        GPSelectorBudgetMaster budgetSelector = new GPSelectorBudgetMaster();
        GP_Budget_Master__c expectedBudgetMaster = [SELECT Id FROM GP_Budget_Master__c LIMIT 1];
        List<GP_Budget_Master__c> returnedBudgetMasters = budgetSelector.selectAllMasterBudgetRecords();
        System.assertEquals(expectedBudgetMaster.Id, returnedBudgetMasters[0].Id);
    }
}