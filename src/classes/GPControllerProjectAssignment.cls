public without Sharing Class GPControllerProjectAssignment
{
    private Id strProjectID;
    private Boolean changeOwner;

    public GPControllerProjectAssignment(Id strProjectID) {
        this.strProjectID = strProjectID;
    }
    
    public void setChangeOwner(Boolean changeOwner) {
        this.changeOwner = changeOwner;
    }

    public GPAuraResponse getUserlist() {
        
        set<Id> setOfRoleID = new set<Id>(); 
        WrapperValidationResponse authenticationResponse = validateProjectAssignment();
        
        if(!authenticationResponse.isValid) {
            return new GPAuraResponse(false, authenticationResponse.message, null);    
        }
        
        GP_Project__c projectObj = new GPSelectorProject().getProjectPrimarySDO(strProjectID); 

        /*[Select Id, GP_Primary_SDO__c, 
                                    ownerId, GP_Current_Working_User__c 
                                    from GP_Project__c
                                    where ID =:strProjectID 
                                    and GP_Primary_SDO__c != null
                                    LIMIT 1];*/
        if(projectObj.OwnerId == projectObj.GP_Current_Working_User__c) {
            return getListOfUserRoles(projectObj);
        } else {
            return new GPAuraResponse(true, 'AssignToCreator', null);
        }
        
    }
    private GPAuraResponse getListOfUserRoles(GP_Project__c projectObj) {
        
        List<GP_User_Role__c> listOfUserRole = new  List<GP_User_Role__c>();
        List<GP_User_Role__c> listOfAdminUserRoleForLoggedInUser;
        Id loggedInUserId = UserInfo.getUserId(); 
        Boolean isAdminUser;

        try {
            GPSelectorUserRole userRoleSelector = new GPSelectorUserRole();

            listOfAdminUserRoleForLoggedInUser = userRoleSelector.selectUserofAdminCategory();

            isAdminUser = listOfAdminUserRoleForLoggedInUser != null && listOfAdminUserRoleForLoggedInUser.size() > 0;

            if(isAdminUser) {
                listOfUserRole = userRoleSelector.selectUserofPIDCreationCategory(projectObj);
            } else {
                listOfUserRole = userRoleSelector.selectUserofResourceAllocationCategory(projectObj);
            }

            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            
            if(listOfUserRole != null) {
                gen.writeObjectField('listOfUserRole', listOfUserRole);
            }

            if(projectObj != null) {
                gen.writeObjectField('projectData', projectObj);
            }
            
            if(isAdminUser != null) {
                gen.writeBooleanField('isAdminUser', isAdminUser);
            }

            gen.writeEndObject();
            
            return new GPAuraResponse(true, 'SUCCESS', gen.getAsString());
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }     
    }
    
    
    public GPAuraResponse assignToOwner()
    {
        GP_Project__c objP;
        
        try {
            objP = new GPSelectorProject().getProject(strProjectID) ; //[Select Id,GP_Current_Working_User__c,OwnerId from GP_Project__c where Id = :strProjectId];
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);             
        }
        return updateCurrentWorkingUser(objP.OwnerId);
    }
    
    
    public GPAuraResponse updateCurrentWorkingUser(string strUserID)
    {
        GP_Project__c objP = new GP_Project__c(ID = strProjectId);
        objP.GP_Current_Working_User__c = strUserID;

        if(changeOwner != null && changeOwner) {
            objP.OwnerId = strUserID;            
        }
        
        try {
            update objP;
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);            
        }
        
        return new GPAuraResponse(true, 'SUCCESS', JSON.serialize(objP));
    }
    
    
    private WrapperValidationResponse validateProjectAssignment()
    {
        Id loginUserID = UserInfo.getUserId();
        set<Id> setofUserID = new set<Id>();
        Map<Id, Boolean> mapOfUserIdToAvailable = new Map<Id, Boolean>(); 
        
        if(strProjectID !=null)
        {
            GP_Project__c objProject = new GPSelectorProject().getProject(strProjectID); /*[Select Id,GP_Current_Working_User__c, 
                                       OwnerID, GP_Approval_Status__c
                                       from GP_Project__c
                                       where ID =:strProjectID];*/
                                       
                                      
                                           
            if(objProject.GP_Approval_Status__c != 'Draft') {
                return new WrapperValidationResponse(false, 'You can only assign a draft project.');                
            }
            
            mapOfUserIdToAvailable.put(objProject.GP_Current_Working_User__c, true);
            
        }
        /*[Select ID, GP_Active__c,GP_Role__c,
                                   GP_Role__r.GP_Role_Category__c,
                                   GP_User__c,GP_User__r.Name
                                   from GP_User_Role__c 
                                   where GP_Role__r.GP_Role_Category__c = 'Admin' and
                                   GP_Active__c = true]*/
        for(GP_User_Role__c objUR: new GPSelectorUserRole().selectUserfromAdminUserRole()) 
        {
            mapOfUserIdToAvailable.put(objUR.GP_User__c, true);
        }
        
        for(User objUser:[Select Id
                          from User
                          where Profile.Name ='System Administrator']) 
        {
            mapOfUserIdToAvailable.put(objUser.Id, true);
        }
        
        if(!mapOfUserIdToAvailable.containsKey(loginUserID)) {
            return new WrapperValidationResponse(false, 'You are not authorized to assign project');
        }
        
        return new WrapperValidationResponse(true, 'SUCCESS');
    }
    
    public class WrapperValidationResponse{
        Boolean isValid;
        String message;
        public WrapperValidationResponse(Boolean isValid, String message) {
            this.isValid = isValid;
            this.message = message;
        }
    }
}