@isTest(SeeAllData = true)
Public class TSRULES_test
{
	static opportunity opp31,opp, opp1, opp2, opp3, opp4, opp5,opp6, opp7, opp8, opp9, opp10, opp11, opp12, opp13, opp14, opp15, opp16,opp21,opp22;
    static User u1;
    
    Static testmethod void testmethodWithStaleUser()
    {
        setUpTestMethod();
        system.runAs(u1){
            Test.startTest();
            
            Restricted_Access_stale_deal controller = new Restricted_Access_stale_deal();
            //controller.Final_non_ts_oppty.add(opp);
            controller.ExtItem_oppid=opp.id;
            controller.myDate=string.valueof(system.today()); 
            controller.Stale_oppty.add(opp);
            controller.shortlistedOppIndex =opp.ID;
            controller.Stale_oppty_nonts.add(opp);
            controller.ExtItem_oppid1 = opp.ID;
            controller.ExtItem_id = opp.ID;
            list<Restricted_Access_stale_deal.WrapperOfOpp> shortlistedOppWrapper=controller.shortlistedOppWrapper;
            Restricted_Access_stale_deal.WrapperOfOpp wrap = new Restricted_Access_stale_deal.WrapperOfOpp(opp); 
            controller.shortlistedOppWrapper.add(wrap);
           	controller.shortListOpps();
            controller.getCountries();
            controller.getCountriesOne();
            
            controller.Move_to_prediscover();
            
            controller.Droping();
            controller.Moving();
            controller.redirecthome();   
            controller.ExtItem_oppid2 = opp.id;
            controller.myDate1 = String.valueOf(system.today().addDays(3));
            controller.proposedClosedDate = String.valueOf(system.today().addDays(7));
            
            controller.shortListOpps1();
            controller.Update_non_Ts_Closure();
            controller.save();
            controller.cancel();
            controller.saveTs();
            Test.stopTest();
        }
    }
    
   Static testmethod void testmethodWithoutStaleUser()
    {
        setUpTestMethod();
        Test.startTest();
            Restricted_Access_stale_deal controller = new Restricted_Access_stale_deal();
        //controller.Final_non_ts_oppty.add(opp);
            controller.ExtItem_oppid=opp.id;
            controller.myDate=string.valueof(system.today()); 
            controller.Stale_oppty.add(opp);
            controller.shortlistedOppIndex =opp.Id;
        	controller.Stale_oppty_nonts.add(opp);
        	controller.ExtItem_id = opp.ID;
            
            list<Restricted_Access_stale_deal.WrapperOfOpp> shortlistedOppWrapper=controller.shortlistedOppWrapper;
            Restricted_Access_stale_deal.WrapperOfOpp wrap = new Restricted_Access_stale_deal.WrapperOfOpp(opp); 
            controller.shortlistedOppWrapper.add(wrap);
           	controller.shortListOpps();
            
            controller.Move_to_prediscover();
            controller.save();
            controller.Droping();
            controller.Moving();
            controller.redirecthome(); 
        	controller.refreshButton();
        	controller.getCountries();
        	controller.getCountriesOne();
        	
        Test.stopTest();
    }
    
    private static void setUpTestMethod(){
        /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        
        list<opportunity> oliList= new List<opportunity>();
        opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, Amount = 1000001);
        opp1=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 40, counterOne__c = 0, Amount = 20000003);
        oliList.add(opp);
        oliList.add(opp1);
        insert oliList;
        
        
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
        insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='SAP',Product_Family_OLI__c='Analytics',LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        opp.Nature_of_Work_highest__c='Consulting';
        Update opp;
        
        Deal_Cycle__c   DC= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='1. Discover',Type_of_Deal__c='Retail',Ageing__c=30);
        Deal_Cycle__c   DC1= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='1. Discover',Type_of_Deal__c='Large',Ageing__c=45);            
        Deal_Cycle__c   DC2= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Retail',Ageing__c=20); 
        Deal_Cycle__c   DC3= new Deal_Cycle__c(Transformation_Deal__c='Yes',Stage__c='2. Define',Type_of_Deal__c='Large',Ageing__c=30);                       
        
        Insert DC;
        Insert DC1;
        Insert DC2;
        Insert DC3;*/
        
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        list<opportunity> oliList= new List<opportunity>();
        opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                            amount = 2000000);
        
        opp15=new opportunity(name='12348',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                            amount = 20000000);
        
        opp2=new opportunity(name='1236',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp3=new opportunity(name='1237',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp5=new opportunity(name='12311',StageName='3. On Bid',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        opp7=new opportunity(name='1239',StageName='4. Down Select',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 2000000);
        
         opp14=new opportunity(name='12324',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 200000001);
        
        opp8=new opportunity(name='12312',StageName='2. Define',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 6000000);
        opp9=new opportunity(name='12313',StageName='3. On Bid',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                             amount = 6000000);
        opp10=new opportunity(name='12314',StageName='4. Down Select',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000000);
        opp11=new opportunity(name='12315',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000000);
        opp31=new opportunity(name='12331',StageName='1. Discover',CloseDate=system.today()-1,Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                              amount = 6000);
        oliList.add(opp);
        oliList.add(opp2);
        oliList.add(opp3);
        oliList.add(opp5);
        oliList.add(opp7);
        oliList.add(opp8);
        oliList.add(opp9);
        oliList.add(opp10);
        oliList.add(opp11);
        oliList.add(opp14);
        oliList.add(opp15);
        oliList.add(opp31);
        
        insert oliList;
        
        System.runAs(u1){
            list<opportunity> oliList1= new List<opportunity>();
            opp4=new opportunity(name='1238',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 2000005);
             opp16=new opportunity(name='12387',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 20000000);
            
            opp6=new opportunity(name='12310',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, 
                                 Type_Of_Opportunity__c = 'Non Ts' ,Amount = 2000005);
            
            opp12=new opportunity(name='12322',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000000);
            
            opp13=new opportunity(name='12323',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 60000000);
            opp21=new opportunity(name='12331',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Analytics', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 600);
            opp22=new opportunity(name='12332',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Digital', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 600);
            
            oliList1.add(opp4);
            oliList1.add(opp6);          
            oliList1.add(opp12);
            oliList1.add(opp13);
            oliList1.add(opp16);
             oliList1.add(opp21);
            oliList1.add(opp22);
            insert oliList1;
        }  
        
    }

}