/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle Revendue Schedules in Discover stage of opportunity
**************************************************************************************************************************/
public class OpportunityDiscoverRevenueSchedule{
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Wrapper based on Product of OpportunityLineItemSchedules
**************************************************************************************************************************/
    public class ProductWrapper{  
        @AuraEnabled  public Integer wrapperIndex;
        @AuraEnabled  public List<OpportunityLineItemSchedule> opportunityLineItemSchedules;
        @AuraEnabled  public Id oLIId;
        @AuraEnabled  public String productName;
        @AuraEnabled  public String productId;
        @AuraEnabled  public String natureOfWork;
        @AuraEnabled  public Decimal unitPrice;
        @AuraEnabled  public String serviceLine;
    }
/*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for displaying the list of OpportunityLineItem of Products
* @param   OpportunityId - Opportunity Id
* @return  List<ProductWrapper>
**************************************************************************************************************************/  
       
    
    @AuraEnabled    
    public static List<ProductWrapper> getProductWrapperList(Id OppId){
        
        try{
            List<ProductWrapper> productWrapList = new List<ProductWrapper>();
            ProductWrapper wrapper;
            //map of OLI Id and List of OLI
            Map<Id, List<OpportunityLineItemSchedule>> productOLIScheduleListMap = new Map<Id, List<OpportunityLineItemSchedule>>();
            for(OpportunityLineItemSchedule objOLIS:[Select Id, ScheduleDate, Revenue,
                                                     OpportunityLineItemId,Type,
                                                     OpportunityLineItem.Product2.Name,
                                                     OpportunityLineItem.Product2.Id,
                                                     OpportunityLineItem.UnitPrice,
                                                     OpportunityLineItem.Product2.Nature_of_work__c,
                                                     OpportunityLineItem.Product2.Service_line__c,
                                                     Description
                                                     from OpportunityLineItemSchedule where OpportunityLineItem.Opportunity.Id=:OppId
                                                     Order By OpportunityLineItem.CreatedDate DESC
                                                     LIMIT:limits.getLimitQueryRows()])
                
            {
                
                if(productOLIScheduleListMap.containsKey(objOLIS.OpportunityLineItemId)){
                    List<OpportunityLineItemSchedule> lstOfOLIS = productOLIScheduleListMap.get(objOLIS.OpportunityLineItemId); 
                    lstOfOLIS.add(objOLIS);
                    productOLIScheduleListMap.put(objOLIS.OpportunityLineItemId,lstOfOLIS);
                }                
                else{
                    productOLIScheduleListMap.put(objOLIS.OpportunityLineItemId,new List<OpportunityLineItemSchedule>{objOLIS});
                    
                }            
            }
            
            //to display wrapper
            if(productOLIScheduleListMap.keySet().size() > 0)
            {
                integer i=0;
                for(Id OLIId: productOLIScheduleListMap.keySet())
                {    
                    
                    wrapper =  new ProductWrapper();
                    wrapper.productName = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Name;
                    wrapper.serviceLine = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Service_line__c;
                    wrapper.natureOfWork = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Nature_of_work__c;
                    wrapper.unitPrice = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.UnitPrice;
                    wrapper.productId = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Id;
                    wrapper.opportunityLineItemSchedules = productOLIScheduleListMap.get(OLIId); 
                    wrapper.oLIId = OLIId;
                    wrapper.wrapperIndex=i;
                    productWrapList.add(wrapper);
                    i++;
                }            
            }         
            return productWrapList;  
        }
        catch(dmlexception e){
            //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(OppId),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverRevenueSchedule', 'getProductWrapperList','Fail','',String.valueOf(e.getLineNumber()));
            return null;
        }
        
    }
    
    /*************************************************************************************************************************
* @author  Persistent
* @description  - Handle all the logic for displaying the list of OpportunityLineItem of Products
* @param   OpportunityId - Opportunity Id
* @return  List<ProductWrapper>
**************************************************************************************************************************/  
    @AuraEnabled    
    public static List<ProductWrapper> getOLISMap(String OppId){
        try{
            List<ProductWrapper> productWrapList = new List<ProductWrapper>();
            ProductWrapper wrapper;
            //map of OLI Id and List of OLI
            Map<Id, List<OpportunityLineItemSchedule>> productOLIScheduleListMap = new Map<Id, List<OpportunityLineItemSchedule>>();
            for(OpportunityLineItemSchedule objOLIS:[Select Id, ScheduleDate, Revenue,
                                                     OpportunityLineItemId,Type,
                                                     OpportunityLineItem.Product2.Name,
                                                     OpportunityLineItem.Product2.Id,
                                                     OpportunityLineItem.UnitPrice,
                                                     OpportunityLineItem.Product2.Nature_of_work__c,
                                                     OpportunityLineItem.Product2.Service_line__c,
                                                     Description
                                                     from OpportunityLineItemSchedule 
                                                     where OpportunityLineItem.OpportunityId =:OppId LIMIT:limits.getLimitQueryRows()])
                
            {
                
                if(productOLIScheduleListMap.containsKey(objOLIS.OpportunityLineItemId)){
                    List<OpportunityLineItemSchedule> lstOfOLIS = new list<OpportunityLineItemSchedule>();
                    lstOfOLIS = productOLIScheduleListMap.get(objOLIS.OpportunityLineItemId); 
                    lstOfOLIS.add(objOLIS);
                    productOLIScheduleListMap.put(objOLIS.OpportunityLineItemId,lstOfOLIS);
                }                
                else{
                    productOLIScheduleListMap.put(objOLIS.OpportunityLineItemId,new List<OpportunityLineItemSchedule>{objOLIS});
                }            
            }
            
            //to display wrapper
            if(productOLIScheduleListMap.keySet().size() > 0)
            {
                for(Id OLIId: productOLIScheduleListMap.keySet())
                {    
                    wrapper =  new ProductWrapper();
                    wrapper.productName = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Name;
                    wrapper.serviceLine = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Service_line__c;
                    wrapper.natureOfWork = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Nature_of_work__c;
                    wrapper.unitPrice = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.UnitPrice;
                    wrapper.productId = productOLIScheduleListMap.get(OLIId)[0].OpportunityLineItem.Product2.Id;
                    wrapper.opportunityLineItemSchedules = productOLIScheduleListMap.get(OLIId); 
                    productWrapList.add(wrapper);
                }            
            }         
            return productWrapList;
        }
        catch(dmlexception e){
            //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(OppId),e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverRevenueSchedule', 'getOLISMap','Fail','',String.valueOf(e.getLineNumber()));
            return null;
        }
        
    }
    /************************************************************************************************************************
* @author  Persistent
* @description  This method will save revenue schedule for a product
* @Param  - string containing list of OpportunityLineItemSchedules
* @return  void
*************************************************************************************************************************/  
    @AuraEnabled    
    public static void saveProductRevenueSchedule(String oLIList)        
    {System.debug('>>>>>>oLIList'+oLIList);
       
        try{
            List<OpportunityLineItemSchedule> OLISList = (List<OpportunityLineItemSchedule>)System.JSON.deserialize(oLIList, List<OpportunityLineItemSchedule>.class);
            List<DatedConversionRate> comnnversionList = OpportunityDiscoverAddPricing.getConversionRates();
            Set<id> oppLineItemsIds = new Set<id>();   
            List<OpportunityLineItem> oppLineItemLst = new List<OpportunityLineItem>();
            if(!Test.isRunningTest())
            {
                update OLISList;
            }
            for(OpportunityLineItemSchedule revenuerRec: OLISList)
            {
                oppLineItemsIds.add(revenuerRec.OpportunityLineItemId);
            }
            if(oppLineItemsIds.size() > 0)    
            {
                
                oppLineItemLst = [Select ID,Local_Currency1__c, Local_Currency__c,UnitPrice,TCV__c 
                                  FROM OpportunityLineItem WHERE Id =:oppLineItemsIds LIMIT:limits.getLimitQueryRows()];
                for(OpportunityLineItem lineItem: oppLineItemLst){
                    for(DatedConversionRate conversionRate : comnnversionList)
                    {
                        if(lineItem.Local_Currency__c == conversionRate.IsoCode)
                        { 
                            lineItem.TCV__c = (lineItem.UnitPrice * conversionRate.ConversionRate).setScale(2, RoundingMode.HALF_UP);
                        }
                    }
                }
                update oppLineItemLst;
            }
            
        }
        catch(Exception ex){
            //creating error log
            CreateErrorLog.createErrorRecord(oLIList,ex.getMessage(), '', ex.getStackTraceString(),'OpportunityDiscoverRevenueSchedule', 'saveProductRevenueSchedule','Fail','',String.valueOf(ex.getLineNumber()));
            system.debug(ex.getMessage()+ ' Trace===>' + ex.getCause()+ ' LINE NO == '+ex.getLineNumber()+ ex.getMessage() + ex.getStackTraceString());
             if(ex.getMessage().contains('MSA/SOW closure date cannot be earlier than today'))
            {
               throw new AuraHandledException('MSA/SOW closure date cannot be earlier than today');
            }
            else
            	throw new AuraHandledException(ex.getMessage());   
        }
    }
    
    /************************************************************************************************************************
* @author  Persistent
* @description  This method will fetch Opportunity Line Items for a product
* @Param  - Opportunity Id
* @return  List<OpportunityLineItem>
*************************************************************************************************************************/  
    @AuraEnabled
    public static List<OpportunityLineItem> fetchOpportunityLineItems(Id oppId)
    {
        
        List<OpportunityLineItem> oppLineItems = [Select Id,Product_Name__c,UnitPrice,Type_of_Deal__c,EBIT_OLI_CPQ__c
                                                  from OpportunityLineItem where OpportunityId =: oppId LIMIT:limits.getLimitQueryRows()];
        
        return oppLineItems;
        
    }
    /************************************************************************************************************************
* @author  Persistent
* @description  This method will update Opportunity Line Items for a product
* @Param  - List<OpportunityLineItem>
* @return  - void
*************************************************************************************************************************/ 
    
    @AuraEnabled
    public static void updateOpportunityLineItems(List<OpportunityLineItem> oppLineItems)
    {
        try{
            if (!Schema.sObjectType.OpportunityLineItem.isUpdateable()){
                update oppLineItems; 
            }
        }
        
        catch(Exception ex){
            //creating error log
            CreateErrorLog.createErrorRecord('',ex.getMessage(), '', ex.getStackTraceString(),'OpportunityDiscoverRevenueSchedule', 'updateOpportunityLineItems','Fail','',String.valueOf(ex.getLineNumber()));
        }
       
    }
    
}