@isTest //(seeAllData=true)
public class Test_InsertQSRMApprovalLeadership 
{
    static testMethod void TestMethod1()
    {
       // try
       // {
            system.debug('test====1====');                
            Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; //Standard User
            
            User u = new User(alias = 'nkhatri', email='neha1@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='neha1@genpact.com',
                                IsActive = true);    

            User u1 = new User(alias = 'MandeepK', email='Mandeep.Kaur10@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='Mandeep.Kaur10@genpact.com',
                                IsActive = true);
                                
            User u2 = new User(alias = 'Amit', email='amit.chaturvedi1@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='amit.chaturvedi1@genpact.com',
                                IsActive = true);
            
            Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
            Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
            
            Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
            
            Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
            insert salesunit;
            
            System.runAs(u)   
            {
                Account oAccount = new Account();
                oAccount.Name = 'strName';          
                oAccount.Business_group__c= 'ge';
                oAccount.business_segment__c = oBS.ID;
                oAccount.sub_business__c= oSB.ID;
                oAccount.industry_vertical__c= 'BFS';
                oAccount.sub_industry_vertical__c= 'BFS';           
                oAccount.website = 'www.genpact.com';
                oAccount.sic= 'test code';
                oAccount.hunting_mining__c= 'test mining';
                oAccount.TAM__c=5000;
                oAccount.PTAM__c=6000;
                oAccount.Archetype__c=oAT.ID;           
                oAccount.Client_Partner__c=u.id;
                oAccount.Primary_Account_GRM__c=u.Id;
                oAccount.TickerSymbol= 'test ticker';
                oAccount.AnnualRevenue= 50000;          
                oAccount.Ownership= 'test Owner';
                oAccount.Account_headquarters_country__c= 'test Account headquarters';
               // oAccount.Account_Owner_from_ACR__c=u.Id;
                
                Sales_Unit__c su = new Sales_Unit__c(Name='Sales Unit',Sales_Leader__c = u.id);
                insert su;
                
                Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id,Sales_Unit_Group__c='Sales Unit Head');
                insert salesunitobject;
                
                oAccount.Sales_Unit__c = salesunitobject.id;
                //oAccount.Account_Owner_from_ACR__c = u.id;
                INSERT oAccount;
                        
             /*   Product_family__c pf = new Product_family__c(Name='Core Ops',Product_Group__c='TEST');
                insert pf;
                
                Nature_Of_Work__c objNature = new Nature_Of_Work__c(Name='Consulting');
                insert objNature;
                
                Vertical_Leaders__c obj_VL = new Vertical_Leaders__c(Industry_Vertical__c='BFS',Vertical_Leaders__c=u.id);
                insert obj_VL;   
                
               Service_Line_Leader__c obj_SLL = new Service_Line_Leader__c(Industry_Vertical__c='BFS',Service_Line_Leader__c=u.id,Nature_of_Work__c=objNature.id, Product_family__c= PF.id ,Delivery_organization__c='Endeavour');
               insert obj_SLL;   
                    
                Exception_Approval_QSRM__c obj_EAQSRML2 = new Exception_Approval_QSRM__c(Approver_Name__c=u.id,Nature_of_Work__c='Consulting', Product_family__c= PF.id);
                system.debug('obj_EAQSRML2====test======'+obj_EAQSRML2);
                insert obj_EAQSRML2;   
                
                Exception_Approval_QSRM_L3__c obj_EAQSRML3 = new Exception_Approval_QSRM_L3__c(Business_Leaders__c=u.id,Industry_Vertical__c='BFS');
                system.debug('obj_EAQSRML3========test===='+obj_EAQSRML3);
                insert obj_EAQSRML3;   
                
                */
                
                 TS_Mapping__c ts=new TS_Mapping__c(Approver_Type__c='Vertical Leader',Approver_name__c=u1.id);
                insert ts;
               
                  
                TS_Mapping__c ts1=new TS_Mapping__c(Approver_Type__c='L2 Escalation Leader',Approver_name__c=u1.id,Nature_of_work__c='Analytics');
                insert ts1;
                
                 TS_Mapping__c ts2=new TS_Mapping__c(Approver_Type__c='Service Line Leader',Approver_name__c=u1.id,Service_line__c='Accounts Payable',Industry_vertical__c='Healthcare');
                insert ts2;
                
                TS_Mapping__c ts3=new TS_Mapping__c(Approver_Type__c='L3 Escalation Leader',Approver_name__c=u1.id,Industry_vertical__c='Healthcare');
                insert ts3;
                
                TS_Mapping__c ts4=new TS_Mapping__c(Approver_Type__c='RPA',Approver_name__c=u2.id,Industry_Vertical__c='BFS');
                insert ts4;
               
                opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Competitor__c='Accenture',Deal_Type__c='Competitive',Target_Source__c='Existing G Relationship',  Opportunity_Source__c='Transformational Services Pull through ');
                insert opp;
                
                Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=oAccount.Id,Email='test1@gmail.com');
                insert oContact;
                
                OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
                insert oppcontactrole;
                
              
                Product2 oProduct = New Product2(name='Chk2',nature_of_work__c='Consulting',Service_line__c='Accounts Payable',Industry_vertical__c='Healthcare');
                insert oProduct;
                
                 Id pricebookId = Test.getStandardPricebookId();
                 
                 PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = oProduct.Id,
            UnitPrice = 15000, IsActive = true);
        insert standardPrice;
                   
               OpportunityLineItem OLi=new OpportunityLineItem(Product2=oProduct,opportunityid=opp.id,Delivery_Location__c = 'Americas',Local_Currency__c='INR',Revenue_Start_Date__c = System.today().adddays(1),FTE__c= 2,Contract_Term__c=5,UnitPrice=6770,TCV__c=6790,Delivering_Organisation__c='Americas',Sub_Delivering_Organisation__c='Akritiv', Product_BD_Rep__c=u.id,pricebookentryid=standardprice.id); //,OwnerId=UserInfo.getUserId (),  SalesExecutive__c = u.id
                insert OLi;  
                
                Test.StartTest();    
                    QSRM__c obj_QSRM = new QSRM__c(Opportunity__c=opp.id,Is_this_is_a_RPA_deal__c = 'No',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id  );
                    system.debug('obj_QSRM====test====='+obj_QSRM);
                    insert obj_QSRM;
                        
                   /* Id rtId = [select Id, name from RecordType where name = 'TS QSRM' limit 1].Id;        
                    system.debug('rtId=======test===='+rtId);
                    Case c = new Case();        
                    c.AccountId = oAccount.id;
                    c.ContactId = oContact.Id;
                    c.RecordTypeId = rtId ;
                    c.Type = 'My Type';
                    c.Origin = 'My Origin';
                    c.Status = 'My Status';
                    system.debug('c=====test===='+c);
                    insert c;*/
                
                   // System.assert(obj_QSRM.id <> null);
                Test.stoptest();
            }
       // }
       /* catch(Exception e)
        { 
            system.debug('====test catch====='+e.getmessage());
            System.debug('eline:'+e.getLineNumber());
            System.debug('stacktrace:'+e.getStackTraceString());
        } */ 
        
    }  

    static testMethod void TestMethod2()
    {
       // try
       // {
            system.debug('test====1====');                
            Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; //Standard User
            
            User u = new User(alias = 'nkhatri', email='neha.khatri@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='neha1@genpact.com',
                                IsActive = true);    

           User u1 = new User(alias = 'MandeepK', email='Mandeep.Kaur10@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='Mandeep.Kaur10@genpact.com',
                                IsActive = true);
                                
            User u2 = new User(alias = 'Amit', email='amit.chaturvedi1@genpact.com',
                                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='fr',
                                localesidkey='fr_FR_EURO', profileid = p.Id, country='India',
                                timezonesidkey='Europe/Paris', username='amit.chaturvedi1@genpact.com',
                                IsActive = true); 
            
            Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
            Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
            
            Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
            
            Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
            insert salesunit;
            
            System.runAs(u)   
            {
                Account oAccount = new Account();
                oAccount.Name = 'strName';          
                oAccount.Business_group__c= 'ge';
                oAccount.business_segment__c = oBS.ID;
                oAccount.sub_business__c= oSB.ID;
                oAccount.industry_vertical__c= 'BFS';
                oAccount.sub_industry_vertical__c= 'BFS';           
                oAccount.website = 'www.genpact.com';
                oAccount.sic= 'test code';
                oAccount.hunting_mining__c= 'test mining';
                oAccount.TAM__c=5000;
                oAccount.PTAM__c=6000;
                oAccount.Archetype__c=oAT.ID;           
                oAccount.Client_Partner__c=u.id;
                oAccount.Primary_Account_GRM__c=u.Id;
                oAccount.TickerSymbol= 'test ticker';
                oAccount.AnnualRevenue= 50000;          
                oAccount.Ownership= 'test Owner';
                oAccount.Account_headquarters_country__c= 'test Account headquarters';
                oAccount.QSRM_Approver__c=u2.id;
                //oAccount.Account_Owner_from_ACR__c=u.Id;
                
                Sales_Unit__c su = new Sales_Unit__c(Name='Sales Unit',Sales_Leader__c = u.id);
                insert su;
                
                Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id,Sales_Unit_Group__c='Sales Unit Head');
                insert salesunitobject;
                
                oAccount.Sales_Unit__c = salesunitobject.id;
                //oAccount.Account_Owner_from_ACR__c = u.id;
                INSERT oAccount;
                        
             /*   Product_family__c pf = new Product_family__c(Name='Core Ops',Product_Group__c='TEST');
                insert pf;
                
                Nature_Of_Work__c objNature = new Nature_Of_Work__c(Name='Consulting');
                insert objNature;
                
                Vertical_Leaders__c obj_VL = new Vertical_Leaders__c(Industry_Vertical__c='BFS',Vertical_Leaders__c=u1.id);
                insert obj_VL;   
                
               Service_Line_Leader__c obj_SLL = new Service_Line_Leader__c(Industry_Vertical__c='BFS',Service_Line_Leader__c=u1.id,Nature_of_Work__c=objNature.id, Product_family__c= PF.id ,Delivery_organization__c='Endeavour');
               insert obj_SLL;   
                    
                Exception_Approval_QSRM__c obj_EAQSRML2 = new Exception_Approval_QSRM__c(Approver_Name__c=u.id,Nature_of_Work__c='Consulting', Product_family__c= PF.id);
                system.debug('obj_EAQSRML2====test======'+obj_EAQSRML2);
                insert obj_EAQSRML2;   
                
                Exception_Approval_QSRM_L3__c obj_EAQSRML3 = new Exception_Approval_QSRM_L3__c(Business_Leaders__c=u.id,Industry_Vertical__c='BFS');
                system.debug('obj_EAQSRML3========test===='+obj_EAQSRML3);
                insert obj_EAQSRML3;   
                
                */
                
                TS_Mapping__c ts=new TS_Mapping__c(Approver_Type__c='Vertical Leader',Approver_name__c=u2.id,Industry_Vertical__c='BFS');
                insert ts;
               
                  
                TS_Mapping__c ts1=new TS_Mapping__c(Approver_Type__c='L2 Escalation Leader',Approver_name__c=u1.id,Nature_of_work__c='Consulting');
                insert ts1;
                
                 TS_Mapping__c ts2=new TS_Mapping__c(Approver_Type__c='Service Line Leader',Approver_name__c=u1.id,Service_line__c='Risk Management',Industry_vertical__c='BFS');
                insert ts2;
                
                TS_Mapping__c ts3=new TS_Mapping__c(Approver_Type__c='L3 Escalation Leader',Approver_name__c=u1.id,Industry_vertical__c='BFS');
                insert ts3;
                
                TS_Mapping__c ts4=new TS_Mapping__c(Approver_Type__c='RPA',Approver_name__c=u2.id,Industry_Vertical__c='BFS');
                insert ts4;
                
                
                
                opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',Competitor__c='Accenture',Deal_Type__c='Competitive',Delivering_Organisation__c='Endeavour',Highest_Revenue_Product__c='IT Services', Nature_of_Work_highest__c = 'Consulting', Highest_ServiceLine__c='Risk Management');
                insert opp;
                
                Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=oAccount.Id,Email='test1@gmail.com');
                insert oContact;
                
                OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
                insert oppcontactrole;
                
              
                Product2 oProduct = New Product2(name='Chk2',nature_of_work__c='Consulting',Service_line__c='Others',Industry_vertical__c='BFS');
                insert oProduct;
                
                 Id pricebookId = Test.getStandardPricebookId();
                 
                 PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = oProduct.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
                   
               OpportunityLineItem OLi=new OpportunityLineItem(Product2=oProduct,opportunityid=opp.id,Delivery_Location__c = 'Americas',Local_Currency__c='INR',Revenue_Start_Date__c = System.today().adddays(1),FTE__c= 2,Contract_Term__c=5,UnitPrice=6770,TCV__c=6770,Delivering_Organisation__c='Americas',Sub_Delivering_Organisation__c='Akritiv', Product_BD_Rep__c=u.id,pricebookentryid=standardprice.id); //,OwnerId=UserInfo.getUserId (),  SalesExecutive__c = u.id
                insert OLi;  
                
                Test.StartTest();    
                    QSRM__c obj_QSRM = new QSRM__c(Opportunity__c=opp.id,Is_this_is_a_RPA_deal__c = 'Yes',Service_Line_Leaders__c=u1.id,Vertical_Leaders__c=u2.id ,User_sales_leader__c=u.id );
                    system.debug('obj_QSRM====test====='+obj_QSRM);
                    insert obj_QSRM;
                        
                   /* Id rtId = [select Id, name from RecordType where name = 'TS QSRM' limit 1].Id;        
                    system.debug('rtId=======test===='+rtId);
                    Case c = new Case();        
                    c.AccountId = oAccount.id;
                    c.ContactId = oContact.Id;
                    c.RecordTypeId = rtId ;
                    c.Type = 'My Type';
                    c.Origin = 'My Origin';
                    c.Status = 'My Status';
                    system.debug('c=====test===='+c);
                    insert c;*/
                
                   // System.assert(obj_QSRM.id <> null);
                Test.stoptest();
            }
       // }
       /* catch(Exception e)
        { 
            system.debug('====test catch=1===='+e.getmessage());
            System.debug('eline:'+e.getLineNumber());
            System.debug('stacktrace:'+e.getStackTraceString());
        } */ 
        
    }  
    
}