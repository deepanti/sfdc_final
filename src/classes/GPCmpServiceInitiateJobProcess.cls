/**
 * @group InitiateJobProcess
 *
 * @description Apex Service for Job module,
 *              has aura enabled method to queue jobs.
 */
public class GPCmpServiceInitiateJobProcess {

    /**
     * @description Aura enabled method to returns Job Status.
     * @param projectId SFDC 18/15 digit Job Id
     * 
     * @return GPAuraResponse Job Status
     * 
     * @example
     * GPCmpServiceInitiateJobProcess.queueJob(<SFDC 18/15 digit Job Id>);
     */
    @AuraEnabled
    public static GPAuraResponse queueJob(Id jobId) {
        GPControllerInitiateJobProcess initiateJobProcessController = new GPControllerInitiateJobProcess(jobId);
        return initiateJobProcessController.getQueuedJobResponse();
    }

}