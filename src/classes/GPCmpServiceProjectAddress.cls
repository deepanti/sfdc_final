/**
* @author Anmol.Kumar
* @date 22/10/2017
*
* @group ProjectAddress
* @group-content ../../ApexDocContent/ProjectAddress.html
*
* @description Apex Service for ProjectAddress module,
* has aura enabled method for ProjectAddress.
*/
public class GPCmpServiceProjectAddress {
	/**
    * @description Aura enabled method to get Project Address Record.
    * @param Id projectId
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAddress.getProjectAddress(<projectId>);
    */
    @AuraEnabled
    public static GPAuraResponse getProjectAddress(Id projectId) {
        GPControllerProjectAddress projectAddressController = new GPControllerProjectAddress(projectId);
        return projectAddressController.getProjectAddress();
    }
    
    /**
    * @description Aura enabled method to insert Project Address Records.
    * @param String serializedListOfProjectAddress
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAddress.saveProjectAddress(<serializedListOfProjectAddress>);
    */
    @AuraEnabled
    public static GPAuraResponse saveProjectAddress(String serializedListOfProjectAddress) {
        GPControllerProjectAddress projectAddressController = new GPControllerProjectAddress();
        return projectAddressController.saveProjectAddress(serializedListOfProjectAddress);
    }
    
    /**
    * @description Aura enabled method to delete Project Address Records.
    * @param Id projectAddressId
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAddress.deleteProjectAddress(<projectAddressId>);
    */
    @AuraEnabled
    public static GPAuraResponse deleteProjectAddress(Id projectAddressId) {
        GPControllerProjectAddress projectAddressController = new GPControllerProjectAddress();
        return projectAddressController.deleteProjectAddress(projectAddressId);
    }
    
    /**
    * @description Aura enabled method to get Filtered List Of Address.
    * @param String serializedListOfCustomerId
    * @param Id billingEntityID
    * 
    * @return GPAuraResponse
    * 
    * @example
    * GPCmpServiceProjectAddress.getFilteredListOfAddress(<serializedListOfCustomerId>, <billingEntityID>);
    */
    @AuraEnabled
    public static GPAuraResponse getFilteredListOfAddress(String serializedListOfCustomerId,Id billingEntityID) {
        GPControllerProjectAddress projectAddressController = new GPControllerProjectAddress(serializedListOfCustomerId, billingEntityID);
        return projectAddressController.getFilteredListOfAddress();
    }
}