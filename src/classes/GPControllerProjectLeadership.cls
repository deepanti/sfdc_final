/**
* @author Anmol.kumar
* @date 11/09/2017
*
* @group ProjectLeadership
* @group-content ../../ApexDocContent/ProjectLeadership.htm
*
* @description Apex Controller for project leadership module,
* has bussiness method to fetch project leadership data.
*/
public class GPControllerProjectLeadership {
    
    private final String SUCCESS_LABEL = 'SUCCESS';
    private final String PROJECT_LABEL = 'project';
    
    private final String PROJECT_LEADERSHIP_API_NAME = 'GP_Project_Leadership__c';

    private final String RECORD_TYPE_VISIBILITY_LABEL             = 'mapOfRecordTypeVisibility';
    private final String ROLE_TO_LIST_OF_EMPLOYEE_LABEL           = 'mapOfRoleToListOfEmployee';
    private final String MAP_OF_ROLE_TO_IS_MANDATORY_LABEL        = 'mapOfRoleToIsMandatory';
    private final String CATEGORY_TO_LIST_OF_ROLES_LABEL          = 'mapOfCategoryToListOfRoles';
    private final String MAP_OF_EMPLOYEE_ID_TO_EMPLOYEE_LABEL     = 'mapOfEmployeeIdToEmployee';
    private final String MAP_OF_CATEGORY_NAME_TO_LEADERSHIP_LABEL = 'mapOfCategoryNameToLeadership';
    private final String MAP_OF_ROLE_TO_SERVICE_LINES_LABEL       = 'mapOfRoleToListOfMandatoryServiceLines';
    private final String PROJECT_LEADERSHIP_RECORDTYPE_LABEL      = 'mapOfLeaderShipRecordTypeNameToRecordType';
    private final String IS_HSL_LEADERSHIP_EDITABLE_LABEL         = 'isHSLLeadershipEditable';

    private final String VALIDATION_ERROR_LABEL                 = 'Validation Error';
    private final String PROJECT_LEADERSHIP_SECTION_LABEL       = 'ProjectLeadership';

    private final String HSL_LEADERSHIP_LABEL                   = 'listOfHSLLeadership';
    private final String ACCOUNT_LEADERSHIP_LABEL               = 'listOfAccountLeadership';
    private final String REGIONAL_LEADERSHIP_LABEL              = 'listOfRegionalLeadership';
    private final String ADDITIONAL_ACCESS_LEADERSHIP_LABEL     = 'listOfAdditionalAccessLeadership';
    private final String MANDATORY_KEY_MEMBERS_LEADERSHIP_LABEL = 'listOfMandatoryKeyMembersLeadership';
    
    private final String ROLE_MASTER_RECORDTYPE_NAME                      = 'Role Master';
    private final String HSL_LEADERSHIP_RECORDTYPE_NAME                   = 'HSL Leadership';
    private final String ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME     = 'Additional Access';
    private final String ACCOUNT_LEADERSHIP_RECORDTYPE_NAME               = 'Account Leadership';
    private final String REGIONAL_LEADERSHIP_RECORDTYPE_NAME              = 'Regional Leadership';
    private final String MANDATORY_KEY_MEMBERS_LEADERSHIP_RECORDTYPE_NAME = 'Mandatory Key Members';

    private List<GP_Project_Leadership__c> listOfMandatoryKeyMembersLeaderShip;
    private List<GP_Project_Leadership__c> listOfRegionalLeaderShip;
    private List<GP_Project_Leadership__c> listOfAccountLeaderShip;
    private List<GP_Project_Leadership__c> listOfHSLLeaderShip;
    private List<GP_Project_Leadership__c> listOfAdditionalAccessLeaderShip;

    private Map<String, Boolean> mapOfRoleToIsMandatory;
    private Map<String, Id> mapOfCategoryNameToLeadership;
    private Map<String, Boolean> mapOfRecordTypeToVisibility;
    private List<GP_Project_Leadership__c> listOfProjectLeadership;
    private Map<String, Set<String>> mapOfRoleToListOfMandatoryServiceLines;
    private Map<String, RecordType> mapOfprojLeadershipRecordTypeNameToRecordType;

    private Map<String, GPProjectTemplateFieldWrapper> mapOfLeadershipFieldKeyToFieldTemplate;
    
    private List<String> listOfLeadershipCategory = new List<String> {
        'MandatoryKeyMembersLeadership', 'RegionalLeadership', 'AccountLeadership', 'HSLLeadership', 'AdditionalAccessLeadership'
    };
                
    private Id projectId;
    private JSONGenerator gen;
    private GP_Project__c projectObj;
    private Boolean isHSLLeadershipEditable;

    private List<GP_LeaderShip_Master__c> listOfLeadership;
    private List<GP_LeaderShip_Master__c> listOfMandatoryLeadership;
    private Map<String, List<GP_Leadership_Master__c>> mapOfRecordTypeCategoryToListOfRoles;
    
    private Set<Id> setOfEmployeeId;
    private Map<Id, GP_Employee_Master__c> mapOfEmployeeIdToEmployee;
    private Map<String, List<GP_Employee_Master__c>> mapOfRoleToListOfEmployee;

    // SObject's used by the logic in this service, listed in dependency order
    private static List<Schema.SObjectType> DOMAIN_SOBJECTS = new Schema.SObjectType[] { 
        GP_Project_Leadership__c.SObjectType, 
        GP_Project__c.SObjectType
    };
    
    public GPControllerProjectLeadership() {}

    public GPControllerProjectLeadership(Id projectId) {
        this.projectId = projectId;
    }    
    
    public GPControllerProjectLeadership(Id projectId, List<GP_Project_Leadership__c> listOfProjectLeadership) {
        this.projectId = projectId;
        this.listOfProjectLeadership = listOfProjectLeadership;
    }
    
    /**
    * @description Returns Project leadership data for a given project Id
    * @param projectId Salesforce 18/15 digit Id of project
    * 
    * @return GPAuraResponse json of Project leadership data
    * 
    * @example
    * new GPControllerProjectLeadership(<18 or 15 digit project Id>).getProjectLeadershipData();
    */
    public GPAuraResponse getProjectLeadershipData() {
        try {
            setLeadershipData();
            setRecordTypes();
            setProjectRecord();
            setLeadershipMaster(); 
            setEmployeeMap();
            setLeadershipRoleMap();
            setRecordTypeVisibilityMap();
            setJson();
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);   
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    /**
    * @description Deletes Project leadership data for a given projectleadership Id
    * @param projectLeadershipId Salesforce 18/15 digit Id of project leadership
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully deleted
    * 
    * @example
    * new GPControllerProjectLeadership().deleteProjectLeadership(projectLeadershipId, projectId);
    */
    public GPAuraResponse deleteProjectLeadership(Id projectLeadershipId, Id projectId) {
        try {
            fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
            
            GP_Project_Leadership__c projectLeadershipToBeDeleted = new GP_Project_Leadership__c(Id = projectLeadershipId);
            GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, PROJECT_LEADERSHIP_SECTION_LABEL);

            uow.registerDeleted(projectLeadershipToBeDeleted);
            uow.registerDirty(project);

            uow.commitWork();
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);             
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }

    /**
    * @description Saves leadership data
    * @param strListOfProjectLeadership serialized list of project leadership records to be inserted/updated
    * 
    * @return GPAuraResponse json of Falg whether the records are successfully inserted
    * 
    * @example
    * new GPControllerProjectLeadership(strListOfProjectLeadership, projectId).upsertProjectLeadership();
    */
    public GPAuraResponse upsertProjectLeadership() {
        List<GP_Project_Leadership__c> listOfProjectLeadershipToBeDeleted = new List<GP_Project_Leadership__c>();
        Map<String, String> mapOfRecordToError = new Map<String, String>();
        List<GP_Project_Leadership__c> listOfUpdatedProjectLeadership;
        Savepoint savePoint;
        try {
            savePoint = Database.setSavepoint();

            listOfUpdatedProjectLeadership = GPServiceProjectLeadership.getUpdatedProjectLeadership(projectId, listOfProjectLeadership, listOfProjectLeadershipToBeDeleted, mapOfRecordToError);
            
            if(!mapOfRecordToError.isEmpty()) {
                return new GPAuraResponse(false, VALIDATION_ERROR_LABEL, JSON.serialize(mapOfRecordToError));
            }

            if(listOfProjectLeadershipToBeDeleted != null && !listOfProjectLeadershipToBeDeleted.isEmpty()) {
                delete listOfProjectLeadershipToBeDeleted;
            }

            if(listOfUpdatedProjectLeadership != null && !listOfUpdatedProjectLeadership.isEmpty()) {
                GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, PROJECT_LEADERSHIP_SECTION_LABEL);

                upsert listOfUpdatedProjectLeadership;
                update project;
            }
        } catch(Exception ex) {
            system.debug('ex'+ex);
            Database.rollback(savePoint);
            return new GPAuraResponse(false, ex.getMessage(), null);             
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(getMapOfRoleToProjectLeadership(listOfProjectLeadership), true));
    }
	
    @testVisible
    private Map<String, GP_Project_Leadership__c> getMapOfRoleToProjectLeadership(List<GP_Project_Leadership__c> listOfProjectLeadership) {
        Map<String, GP_Project_Leadership__c> mapOfRoleToProjectLeadership = new Map<String, GP_Project_Leadership__c>();

        for(GP_Project_Leadership__c projectLeadership : listOfProjectLeadership) {
            if(projectLeadership.GP_Leadership_Role_Name__c != null)
                mapOfRoleToProjectLeadership.put(projectLeadership.GP_Leadership_Role_Name__c, projectLeadership);            
        }
        return mapOfRoleToProjectLeadership;
    }
	
    @testVisible
    private void setLeadershipData() {
        listOfRegionalLeaderShip = new List<GP_Project_Leadership__c>();
        listOfAccountLeaderShip = new List<GP_Project_Leadership__c>();
        listOfHSLLeaderShip = new List<GP_Project_Leadership__c>();
        listOfAdditionalAccessLeaderShip = new List<GP_Project_Leadership__c>();
        listOfMandatoryKeyMembersLeaderShip = new List<GP_Project_Leadership__c>();
        GPSelectorProjectLeadership leadershipSelector = new GPSelectorProjectLeadership();

        for(GP_Project_Leadership__c leaderShip : leadershipSelector.selectActiveLeadershipByProjectId(projectId)) {
            if(leaderShip.RecordType.Name == REGIONAL_LEADERSHIP_RECORDTYPE_NAME) {
                listOfRegionalLeaderShip.add(leaderShip);
            } else if(leaderShip.RecordType.Name == ACCOUNT_LEADERSHIP_RECORDTYPE_NAME) {
                listOfAccountLeaderShip.add(leaderShip);                
            } else if(leaderShip.RecordType.Name == HSL_LEADERSHIP_RECORDTYPE_NAME) {
                listOfHSLLeaderShip.add(leaderShip);                
            } else if(leaderShip.RecordType.Name == ADDITIONAL_ACCESS_LEADERSHIP_RECORDTYPE_NAME) {
                listOfAdditionalAccessLeaderShip.add(leaderShip);                
            } else if(leaderShip.RecordType.Name == MANDATORY_KEY_MEMBERS_LEADERSHIP_RECORDTYPE_NAME) {
                listOfMandatoryKeyMembersLeaderShip.add(leaderShip);                
            }
        }
    }

    private void setRecordTypes() {
        mapOfprojLeadershipRecordTypeNameToRecordType = new Map<String, RecordType>();
        
        for(RecordType recordTypeObject : GPCommon.getRecordTypes(PROJECT_LEADERSHIP_API_NAME)) {
            mapOfprojLeadershipRecordTypeNameToRecordType.put(recordTypeObject.Name, recordTypeObject);
        }
    }

    private void setProjectRecord() {
        GPSelectorProject projectSelector = new GPSelectorProject();
        projectObj = projectSelector.selectProjectRecord(projectId);
    }

    private void setLeadershipMaster() {
        //query leadership master record for Role master type records and create a map of category to role
        //Filtering is done on the basis of 
        //Record type: RoleMaster
        //GP_Customer_L2: Same as project.customer(L4).Customer(L2)
        //SDO: Same as project SDO
        //Sub Division: Same as project Sub Division.
        GPSelectorLeadershipMaster leadershipSelector = new GPSelectorLeadershipMaster();
        listOfLeadership = leadershipSelector.getLeadershipMaster(projectObj);

        listOfMandatoryLeadership = leadershipSelector.getMandatoryKeyMemberLeadership(projectObj); 
    }

    private void setEmployeeMap() {
        GPSelectorEmployeeMaster employeeSelector = new GPSelectorEmployeeMaster();
        mapOfEmployeeIdToEmployee = new Map<Id, GP_Employee_Master__c>();
        setOfEmployeeId = new Set<Id>();
        
        for(GP_Leadership_Master__c leadership : listOfLeadership) {
            if(leadership.GP_Employee_Master__c != null) {
                setOfEmployeeId.add(leadership.GP_Employee_Master__c);
            }
        }

        for(GP_Leadership_Master__c leadership : listOfMandatoryLeadership) {
            if(leadership.GP_Employee_Master__c != null) {
                setOfEmployeeId.add(leadership.GP_Employee_Master__c);
            }
        }
        
        for(GP_Employee_Master__c employee : employeeSelector.selectById(setOfEmployeeId)) {
            mapOfEmployeeIdToEmployee.put(employee.Id, employee);
        }
    }

    private void setLeadershipRoleMap() {
        mapOfRecordTypeCategoryToListOfRoles = new Map<String, List<GP_Leadership_Master__c>>();
        mapOfRoleToListOfEmployee = new Map<String, List<GP_Employee_Master__c>>();
        mapOfRoleToListOfMandatoryServiceLines = new Map<String, Set<String>>();
        mapOfCategoryNameToLeadership = new Map<String, Id>();
        mapOfRoleToIsMandatory = new Map<String, Boolean>();
        
        isHSLLeadershipEditable = false;

        for(GP_Leadership_Master__c leadership : listOfLeadership) {
            String key = leadership.RecordType.Name + '___' + leadership.GP_Type_of_Leadership__c;
            
            if(!mapOfRoleToIsMandatory.containsKey(key) ||
                   !mapOfRoleToIsMandatory.get(key)) {
                Boolean isMandatory = (leadership.GP_PID_Category__c == 'Billable' && 
                                        leadership.GP_Mandatory_for_stage__c == 'Cost Charging') ||
                                      (leadership.GP_PID_Category__c == 'Support' && 
                                        leadership.GP_Mandatory_for_stage__c == 'Approved Support');


                    mapOfRoleToIsMandatory.put(key, isMandatory);                   
            }
                
            // string uniqueRoleKey = leadership.GP_Category__c + '___' + leadership.GP_Leadership_Role__c;
            if(leadership.GP_Mandatory_Service_Line__c != null &&
               !mapOfRoleToListOfMandatoryServiceLines.containsKey(key)) {

                List<String> listOfMandatoryServiceLines = leadership.GP_Mandatory_Service_Line__c.split(',');
                mapOfRoleToListOfMandatoryServiceLines.put(key, new Set<String>());
                
                mapOfRoleToListOfMandatoryServiceLines.get(key)
                    .addAll(listOfMandatoryServiceLines);

                if(leadership.GP_Leadership_Role__c == 'HSL Delivery Head' &&
                    projectObj.GP_HSL__r != null && 
                    projectObj.GP_HSL__r.Parent_HSL__c != null &&
                    leadership.GP_Mandatory_Service_Line__c.contains(projectObj.GP_HSL__r.Parent_HSL__c)) {

                    isHSLLeadershipEditable = true;
                } 
            }
            
           
            if(leadership.RecordType.Name == ROLE_MASTER_RECORDTYPE_NAME && 
                leadership.GP_Category__c != null) {

                if(leadership.GP_Category__c == 'MandatoryKeyMembers' && 
                    (projectObj.RecordType.Name == 'Indirect PID' && 
                        leadership.GP_PID_Category__c == 'Billable') ||
                    (projectObj.RecordType.Name != 'Indirect PID' && 
                        leadership.GP_PID_Category__c == 'Support')) {
                    continue;
                }

                if(!mapOfRecordTypeCategoryToListOfRoles.containsKey(leadership.GP_Category__c)) {
                    mapOfRecordTypeCategoryToListOfRoles.put(leadership.GP_Category__c, new List<GP_Leadership_Master__c>());
                }
                
                mapOfRecordTypeCategoryToListOfRoles.get(leadership.GP_Category__c)
                    .add(leadership);
                
                mapOfCategoryNameToLeadership.put(leadership.GP_Category__c, leadership.Id);
            } else if(leadership.GP_Employee_Master__c != null) {
                GP_Employee_Master__c currentEmployee = mapOfEmployeeIdToEmployee.get(leadership.GP_Employee_Master__c);
                if(!currentEmployee.GP_isActive__c) {
                    continue;                    
                }
                
                if(!mapOfRoleToListOfEmployee.containsKey(key))
                    mapOfRoleToListOfEmployee.put(key, new List<GP_Employee_Master__c>());
                
                mapOfRoleToListOfEmployee.get(key)
                    .add(currentEmployee);
            }
        }

        setMapOfMandatoryRoleToListOfEmployee();
    }
    
    private void setMapOfMandatoryRoleToListOfEmployee() {
        List<GP_Leadership_Master__c> listOfMandatoryRoleMasterRecords = mapOfRecordTypeCategoryToListOfRoles.get('MandatoryKeyMembers');
        Map<String, Boolean> mapOfRoleToIsSdoFilterRequired = new Map<String, Boolean>();

        if(listOfMandatoryRoleMasterRecords != null && !listOfMandatoryRoleMasterRecords.isEmpty()) {
            for(GP_Leadership_Master__c roleMaster : listOfMandatoryRoleMasterRecords) {
                Boolean isSDOFilterRequired = roleMaster.GP_Employee_Field_Type__c == 'Dropdown_With_SDO_Master';
                mapOfRoleToIsSdoFilterRequired.put(roleMaster.GP_Leadership_Role__c, isSDOFilterRequired);
            }
        }

        if(listOfMandatoryLeadership != null && !listOfMandatoryLeadership.isEmpty()) {
            for(GP_Leadership_Master__c leadership : listOfMandatoryLeadership) {
                
                String key = leadership.RecordType.Name + '___' + leadership.GP_Type_of_Leadership__c;

                GP_Employee_Master__c currentEmployee = mapOfEmployeeIdToEmployee.get(leadership.GP_Employee_Master__c);
                if(currentEmployee == null || !currentEmployee.GP_isActive__c) {
                    continue;                    
                }

                //check if the role master has employe field type set to Dropdown With SDO Master 
                //then only employees whose sdo id is equal to project sdo id will be considered                
                if(mapOfRoleToIsSdoFilterRequired.containsKey(leadership.GP_Type_of_Leadership__c) &&
                    mapOfRoleToIsSdoFilterRequired.get(leadership.GP_Type_of_Leadership__c) &&
                    leadership.GP_SDO_Oracle_Id__c != projectObj.GP_Primary_SDO__r.GP_Oracle_Id__c) {
                    continue;
                }

                if(!mapOfRoleToListOfEmployee.containsKey(key))
                    mapOfRoleToListOfEmployee.put(key, new List<GP_Employee_Master__c>());
                
                mapOfRoleToListOfEmployee.get(key)
                    .add(currentEmployee);
            }
        }
    }
    
    public void setRecordTypeVisibilityMap() {
        mapOfRecordTypeToVisibility = new Map<String, Boolean>();
        String targetKey;
        Boolean isSectionVisible;

        GPProjectTemplateFieldWrapper leadershipFieldTemplate;
        List<GPProjectTemplateFieldWrapper> listOfLeadershipFieldTemplate = new List<GPProjectTemplateFieldWrapper>();
        
        String serializedFinalTemplate  = getConcatenatedFinalJSON(projectObj);

        Map<String, GPProjectTemplateFieldWrapper> mapOfFieldKeyToFieldTemplate = GPServiceProjectLeadership.getDeserializedMapOfProjectTemplate(serializedFinalTemplate);
        mapOfLeadershipFieldKeyToFieldTemplate = new Map<String, GPProjectTemplateFieldWrapper>();
        
        
        for(String fieldKey : mapOfFieldKeyToFieldTemplate.keySet()) {
            for(String leadershipCategory : listOfLeadershipCategory) {
                targetKey = 'ProjectLeadership___' + leadershipCategory;
                if(fieldKey.contains(targetKey)) {
                    leadershipFieldTemplate = mapOfFieldKeyToFieldTemplate.get(fieldKey);
                    isSectionVisible = leadershipFieldTemplate.isSectionVisible;
                    
                    mapOfRecordTypeToVisibility.put(leadershipCategory, isSectionVisible);
                    mapOfLeadershipFieldKeyToFieldTemplate.put(fieldKey, leadershipFieldTemplate);
                }
            }
        }
    }
    
    private String getConcatenatedFinalJSON(GP_Project__c projectObj) {
        String serializedFinalTemplate = projectObj.GP_Project_Template__r.GP_Final_JSON_1__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_2__c;
        serializedFinalTemplate += projectObj.GP_Project_Template__r.GP_Final_JSON_3__c;  
        
        return serializedFinalTemplate;
    }
    
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        
        if(listOfRegionalLeaderShip != null) {
            gen.writeObjectField(REGIONAL_LEADERSHIP_LABEL, listOfRegionalLeaderShip);
        }

        if(listOfMandatoryKeyMembersLeaderShip != null) {
            gen.writeObjectField(MANDATORY_KEY_MEMBERS_LEADERSHIP_LABEL, listOfMandatoryKeyMembersLeaderShip);            
        }

        if(listOfAccountLeaderShip != null) {
            gen.writeObjectField(ACCOUNT_LEADERSHIP_LABEL, listOfAccountLeaderShip);
        }

        if(listOfHSLLeaderShip != null) {
            gen.writeObjectField(HSL_LEADERSHIP_LABEL, listOfHSLLeaderShip);
        }

        if(listOfAdditionalAccessLeaderShip != null) {
            gen.writeObjectField(ADDITIONAL_ACCESS_LEADERSHIP_LABEL, listOfAdditionalAccessLeaderShip);
        }

        if(mapOfprojLeadershipRecordTypeNameToRecordType != null) {
            gen.writeObjectField(PROJECT_LEADERSHIP_RECORDTYPE_LABEL, mapOfprojLeadershipRecordTypeNameToRecordType); 
        }

        if(mapOfRecordTypeToVisibility != null) {
            gen.writeObjectField(RECORD_TYPE_VISIBILITY_LABEL, mapOfRecordTypeToVisibility);
        }

        if(mapOfRecordTypeCategoryToListOfRoles != null) {
            gen.writeObjectField(CATEGORY_TO_LIST_OF_ROLES_LABEL, mapOfRecordTypeCategoryToListOfRoles);
        } 

        if(mapOfRoleToListOfEmployee != null) {
            gen.writeObjectField(ROLE_TO_LIST_OF_EMPLOYEE_LABEL, mapOfRoleToListOfEmployee);
        } 

        if(projectObj != null) {
            gen.writeObjectField(PROJECT_LABEL, projectObj);
        }
        
        if(mapOfEmployeeIdToEmployee != null) {
            gen.writeObjectField(MAP_OF_EMPLOYEE_ID_TO_EMPLOYEE_LABEL, mapOfEmployeeIdToEmployee);            
        }
        
        if(mapOfRoleToListOfMandatoryServiceLines != null) {
            gen.writeObjectField(MAP_OF_ROLE_TO_SERVICE_LINES_LABEL, mapOfRoleToListOfMandatoryServiceLines);
        }
        
        if(mapOfCategoryNameToLeadership != null) {
            gen.writeObjectField(MAP_OF_CATEGORY_NAME_TO_LEADERSHIP_LABEL, mapOfCategoryNameToLeadership);
        }
            
        if(mapOfRoleToIsMandatory != null) {
            gen.writeObjectField(MAP_OF_ROLE_TO_IS_MANDATORY_LABEL, mapOfRoleToIsMandatory);
        }

        if(isHSLLeadershipEditable != null) {
            gen.writeBooleanField(IS_HSL_LEADERSHIP_EDITABLE_LABEL, isHSLLeadershipEditable);
        }
        
        gen.writeEndObject();
    }    
}