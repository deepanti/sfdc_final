public  class GENcOpportunityProductDetail {
    public OpportunityProduct__c opportunityProduct {get;set;}
    public RevenueSchedule__c revenueSchedule {get; set;}
    public List<RevenueSchedule__c> revenueScheduleList {get; set;}
    public string isScheduleClicked {get;set;}
    public string isEditScheduleClicked {get; set;}
    public string revenueSplitRollingYear {get; set;}
    public integer revenueRollingYearIndex {get; set;}
    public List<revenueRollingYearWrapper> revenueRYWrapperList {get; set;}    
    public List<ProductScheduleWrapper> productScheduleWrapperList {get; set;}
    public List<ProductScheduleWrapper> revenueForRollingYearOneWrapperList {get; set;}
    public List<ProductScheduleWrapper> revenueForRollingYearTwoWrapperList {get; set;}
    private static final integer NO_OF_INSTALLMENT = 24;
    public decimal revenueForRollingYearOne {get; set;}
    public decimal revenueForRollingYearTwo {get; set;}
    public decimal revenueToSplitUp {get; set;}
    public integer revenueRollingYearToDelete {get; set;}
    public integer revenueRollingYearToEdit {get; set;}
    public decimal installmentReminder {get; set;}
    
    
    public GENcOpportunityProductDetail(ApexPages.StandardController controller) {
        opportunityProduct = (OpportunityProduct__c)controller.getRecord();
        opportunityProduct = [Select Name,Product_Autonumber__c,Overwrite_values__c,LastModifiedById,OpportunityId__r.StageName,OpportunityId__r.Opportunity_ID__c,OpportunityId__r.Project_Margin__c,OpportunityId__r.Margin__c,OpportunityId__r.Industry_Vertical__c,OpportunityId__r.Ownerid,OpportunityId__r.Id,OpportunityId__r.AccountId, ACV__c,COE__c,ContractTerminmonths__c,CreatedById,CurrentYearRevenue__c, CurrentYearRevenue_CYR__c,DeliveryLocation__c,Description__c,NextYearRevenue__c, NextYearRevenue_NYR__c, OpportunityId__c,Product__c,Quantity__c,
                              RevenueStartDate__c,NYR_LC__c,CYR_LC__c,SalesExecutive__c,SEP__c,TotalContractValue__c,TotalPrice__c,CurrentYearRevenueLocal__c,NextYearRevenueLocal__c,Analytics__c,Product_Family_OLI__c,Product_Group__c,Service_Line_OLI__c,Service_Line_Category_Oli__c,Total_TCV__c,Total_ACV__c,Total_CYR__c,Total_NYR__c,ExchangeRate__c,LocalCurrency__c, TCVLocal__c,UnitPrice__c,ACVLocal__c,P_L_SUB_BUSINESS__c,RevenueForRollingYearOne__c,FTE_10th_month__c,FTE_4th_month__c,FTE_7th_month__c,Quarterly_FTE_1st_month__c,RevenueForRollingYearTwo__c,  FTE__c,EndDate__c,Digital_Offerings__c,Product_family_Lookup__c ,Service_Line_lookup__c ,Nature_of_Work_lookup__c,digital_assets__c,digital_tcv__c,digital_nyr__c,digital_cyr__c from OpportunityProduct__c where Id =: controller.getRecord().Id];
        
        /*revenueScheduleList = [Select id, Name, StartDate__c, ScheduleType__c, Revenue__c, InstallmentPeriod__c, NumberOfInstallments__c from RevenueSchedule__c where OpportunityProduct__c=:opportunityProduct.Id];
        revenueSchedule = (revenueScheduleList <>null && revenueScheduleList.size() > 0) ? revenueScheduleList[0] : new RevenueSchedule__c();
        
        
        
        revenueSchedule.InstallmentPeriod__c = 'Monthly';
        revenueSchedule.ScheduleType__c = 'Divide amount into multiple installment';*/
        
        //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Total TCV for monthly buckets exceeded TCV' ));
        system.debug('schedule $$$$' +revenueSchedule);
        getProductShedule();
    }
    
    public PageReference doCancel(){
        PageReference pageRef = new PageReference('/'+opportunityProduct.OpportunityId__c);
        pageRef.setRedirect(true);
        return pageRef;    
    }
    
    public PageReference doClone(){
         system.debug('clone id');
        //String p = ApexPages.currentPage().getParameters().get('id');
        
        //system.debug('clone id'+p);
        PageReference pageRef = new PageReference('/apex/GENvEditOpportunityProduct_Clone?id='+opportunityProduct.id);             
        pageRef.setRedirect(true);
        return pageRef;    
    }
    public void getRevenueRollingYear() {
        revenueRYWrapperList = new List<RevenueRollingYearWrapper>();
        decimal contractTerm = Math.ceil(opportunityProduct.ContractTerminmonths__c / 12);
        
        if(contractTerm > 0) {
            for(Integer i=0; i<contractTerm; i++) {
                revenueRYWrapperList.add(new RevenueRollingYearWrapper(i));
            }
        } else {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Sorry, Product schedule can not be created when contract term is 0' ));
        }
    
    }
    /*public String getautoNumber()                                
    {
          String URLstr= apexpages.currentpage().getURL(); 
        Integer str = URLstr.indexOf('006');                     
        String oppidstr = URLstr.substring(str,str+15);          
        Opportunity opp = [Select id from Opportunity where id=: oppidstr];
        Integer ar = [Select count() from OpportunityProduct__c where Opportunityid__c = : opp.id];            
        string autonumber = 'OLI - '+(ar+1);  
        opportunityProduct.Product_Autonumber__c = autonumber;                               
        return autonumber;                                                   
    }*/

    
         public void getProductShedule(){
        revenueRYWrapperList = new List<RevenueRollingYearWrapper>();
        integer index;
        boolean isReminderInstallmentToBeSplited = false;
        List<RevenueSchedule__c> revScheduleList = [Select id, Name, StartDate__c, ScheduleType__c,RemainderInstallment__c,RollingYear__c, RollingYearRevenueLocal__c,RollingYearRevenue__c, Revenue__c, InstallmentPeriod__c, NumberOfInstallments__c, (Select Id, Name, ScheduleDate__c, Revenue__c, RevenueLocal__c, Description__c from ProductSchedules__r order by ScheduleDate__c) from RevenueSchedule__c where OpportunityProduct__c=:opportunityProduct.Id order by RollingYear__c];
        for(RevenueSchedule__c rs: revScheduleList ) {
            if(integer.valueOf(rs.RollingYear__c)==NULL)
            rs.RollingYear__c=1;
            index = integer.valueOf(rs.RollingYear__c) - 1;
            RevenueRollingYearWrapper rw = new RevenueRollingYearWrapper(index);
            rw.revSchedule = rs;
            updateProductScheduleWrapperList(rw, rs.ProductSchedules__r); 
            revenueRYWrapperList.add(rw);           
            isReminderInstallmentToBeSplited = (rs.RemainderInstallment__c > 0) ? true : false;         
            //index++;
        }
        if(isReminderInstallmentToBeSplited){
            if(revScheduleList <> null ){
                if(revScheduleList.size() > 1 && revScheduleList.size() <> 0){
                    installmentReminder = revScheduleList[revScheduleList.size()-2].RemainderInstallment__c;
                }else if(revScheduleList.size() == 1){
                    installmentReminder = revScheduleList[0].RemainderInstallment__c;
                }
            }
        }else{
            installmentReminder = 0;
        }
        /*installmentReminder = (revScheduleList.size() > 1 && revScheduleList.size() <> 0) ? revScheduleList[revScheduleList.size()-2].RemainderInstallment__c : revScheduleList[0].RemainderInstallment__c;
        if(isReminderInstallmentToBeSplited){
            RevenueRollingYearWrapper rw = new RevenueRollingYearWrapper(index);
            rw.revSchedule = new RevenueSchedule__c();
            rw.scheduleList = new List<productScheduleWrapper>();
            revenueRYWrapperList.add(rw);
        }*/
    }
    
    /*public pageReference createSchedule() {
        decimal remainder = 0;
        integer rounOffValue = 0;
        decimal balanceByRoundOff = 0;
        decimal totalRemainder = 0;
        decimal roundOffValueTobedivided = 0;
        
        system.debug('revenue split up'+revenueRollingYearIndex);
        
        RevenueRollingYearWrapper rw = revenueRYWrapperList.get(revenueRollingYearIndex);
        RevenueRollingYearWrapper prw; 
              
        try {  
            if(validate()){             
                rw.revSchedule.NumberOfInstallments__c = getNoOfInstallment();
                rw.revSchedule.OpportunityProduct__c = opportunityProduct.Id;
                rw.revSchedule.RemainderInstallment__c = installmentReminder;
                rw.revSchedule.RollingYear__c = rw.rollYear;
                if(revenueRollingYearIndex > 0){
                    prw = revenueRYWrapperList.get(revenueRollingYearIndex-1);
                    if(prw <>  null && (prw.scheduleList <> null && prw.scheduleList.size() > 0) ) {
                        rw.revSchedule.StartDate__c =  prw.scheduleList[prw.scheduleList.size()-1].prodSchedule.ScheduleDate__c.addMonths(1);  
                    }                  
                }else {
                    rw.revSchedule.StartDate__c = opportunityProduct.RevenueStartDate__c;
                }
                
                upsert rw.revSchedule;
                
                system.debug('$$$$$$$$$$$$$$$$$'+rw.revSchedule);
                                
                rounOffValue = integer.valueOf(Math.ceil(rw.revSchedule.RollingYearRevenue__c));
                balanceByRoundOff = rw.revSchedule.RollingYearRevenue__c - rounOffValue ;
                remainder = math.mod(rounOffValue , integer.valueOf(rw.revSchedule.NumberOfInstallments__c));
                totalRemainder = remainder + balanceByRoundOff;
                roundOffValueTobedivided  = rounOffValue - remainder;
                List<ProductSchedule__c> productScheduleList = new List<ProductSchedule__c>();                
                
                for(integer i = 1; i <= rw.revSchedule.NumberOfInstallments__c; i++) {            
                                        
                    ProductSchedule__c ps = new ProductSchedule__c();
                    ps.OpportunityProductId__c = opportunityProduct.Id;
                    ps.RevenueScheduleId__c = rw.revSchedule.Id;
                    ps.Type__c = 'Revenue';
                    ps.ScheduleDate__c = rw.revSchedule.StartDate__c.addMonths(i-1);
                    ps.Revenue__c = (i == rw.revSchedule.NumberOfInstallments__c ) ? getRevenue(roundOffValueTobedivided, rw.revSchedule.NumberOfInstallments__c) + totalRemainder  : getRevenue(roundOffValueTobedivided, rw.revSchedule.NumberOfInstallments__c);
                    productScheduleList.add(ps);                    
                }
                
                insert productScheduleList;
                isScheduleClicked = 'No';
                
                updateProductScheduleWrapperList(rw, productScheduleList);
            }
            
        }catch(Exception e) {
            system.debug('exception accurred'+e.getMessage());
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
        }
        
        return null;
    }*/
    
    public void updateProductScheduleWrapperList(RevenueRollingYearWrapper rw, List<ProductSchedule__c> productScheduleList) {
        List<productScheduleWrapper> pScheduleWrapperList = new List<productScheduleWrapper>();
        integer listItem = 0;
        for(ProductSchedule__c ps: productScheduleList){
            productScheduleWrapper w = new productScheduleWrapper(ps, listItem );
            DateTime myDate = datetime.newInstance(ps.ScheduleDate__c.year(), ps.ScheduleDate__c.month(), ps.ScheduleDate__c.day());
            w.formatedDate  = myDate.format('MMM, yyyy');            
            pScheduleWrapperList .add(w);            
            listItem++;
        }
        rw.scheduleList = pScheduleWrapperList ;        
    }
    
        
   /* public decimal getNoOfInstallment(){
        if(installmentReminder <> null && installmentReminder <> 0) {
            if(installmentReminder > 12){
                installmentReminder = installmentReminder  - 12;
                return 12;
            }else if(installmentReminder == 12){
                installmentReminder = 0;
                return 12;
            }else{
                decimal installmentToReturn =  installmentReminder;
                installmentReminder = 0;
                return installmentToReturn;
            }
        }else{
            if(opportunityProduct.ContractTerminmonths__c > 24) {
                installmentReminder = (opportunityProduct.ContractTerminmonths__c - 12);
                return 12;
            }else if(opportunityProduct.ContractTerminmonths__c <= 24 && opportunityProduct.ContractTerminmonths__c >= 12){
                installmentReminder = (opportunityProduct.ContractTerminmonths__c - 12);
                return 12;
            }else{
                installmentReminder = 0;
                return opportunityProduct.ContractTerminmonths__c;
            }
        }
        
        return 0;
    }
    
    public boolean validate(){
        decimal totalScheduleValue = 0;
        
        for(RevenueRollingYearWrapper rw: revenueRYWrapperList){
            
            if(rw.scheduleList <> null && rw.scheduleList.size() > 0) {
                decimal totalRollingYearRevenue = 0;
                for(productScheduleWrapper w: rw.scheduleList){ 
                    if(w.prodSchedule.Revenue__c <> null){                   
                        totalRollingYearRevenue = totalRollingYearRevenue  + w.prodSchedule.Revenue__c;
                    }
                }
                rw.revSchedule.RollingYearRevenue__c = totalRollingYearRevenue;
            }else{
                rw.revSchedule.RollingYearRevenue__c = rw.revSchedule.RollingYearRevenue__c;
            }
            if(rw.revSchedule.RollingYearRevenue__c <> null) {
                totalScheduleValue = totalScheduleValue  + rw.revSchedule.RollingYearRevenue__c;
            }
        }
        if(totalScheduleValue > opportunityProduct.TotalContractValue__c){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Total TCV for monthly buckets exceeded TCV' ));
            return false;
        }
        return true;
    }
    
    public pageReference updateSchedule(){
        try {
            if(validate()){ 
                RevenueRollingYearWrapper rw = revenueRYWrapperList.get(revenueRollingYearToEdit);
                
                List<ProductSchedule__c> productScheduleListToUpdate = new List<ProductSchedule__c>();
                rw.revSchedule.RollingYearRevenue__c = 0;
                for(productScheduleWrapper w: rw.scheduleList){ 
                    rw.revSchedule.RollingYearRevenue__c = rw.revSchedule.RollingYearRevenue__c + w.prodSchedule.Revenue__c;                  
                    productScheduleListToUpdate.add(w.prodSchedule);
                }
                if(productScheduleListToUpdate<> null && productScheduleListToUpdate.size() > 0) {
                    update productScheduleListToUpdate;
                    if(rw.revSchedule <> null){                    
                        update rw.revSchedule;
                    }
                } 
                isEditScheduleClicked = 'No'; 
            }
        }catch(Exception e){
            System.debug('Exception accurred'+e.getMessage());
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
        }
        return null;
    } 
    */    
    
    /*public decimal getRevenue(decimal val, decimal noOfInstallment) {
        decimal revenue = 0;
        if(revenueSchedule.ScheduleType__c == 'Repeat amount for each installment'){
            revenue = opportunityProduct.TotalContractValue__c;
        }else if(revenueSchedule.ScheduleType__c == 'Divide amount into multiple installment'){
            revenue = val/noOfInstallment;
        }
        return revenue;
    }    
    
    public pageReference deleteProductSchedule() {
        try{
            RevenueRollingYearWrapper rw = revenueRYWrapperList.get(revenueRollingYearToDelete);
            system.debug('Revenue wrapper list' +rw);
            
            List<ProductSchedule__c> productScheduleListTodelete = new List<ProductSchedule__c>();
            for(productScheduleWrapper w: rw.scheduleList){                    
                productScheduleListTodelete.add(w.prodSchedule);
            }
            if(productScheduleListTodelete <> null && productScheduleListTodelete.size() > 0) {
                delete productScheduleListTodelete;
                rw.scheduleList = null;
                if(rw.revSchedule <> null){                      
                    rw.revSchedule.RollingYearRevenue__c = 0;
                    rw.revSchedule.RemainderInstallment__c = 0;
                    rw.revSchedule.NumberOfInstallments__c = 0;
                    update rw.revSchedule;
                }
            }            
                        system.debug('Revenue wrapper list after delete'+rw);
        }catch(Exception e){            
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
        }
        return null;
    }
    
    
    
    public List<SelectOption> getScheduleTypes(){
        List<SelectOption> options = new List<SelectOption>();        
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('DAIMI','Divide amount into multiple installment'));
        options.add(new SelectOption('RAFEI','Repeat amount for each installment'));        
        return options;
    }
    
    public List<SelectOption> getInstallmentPeriods(){
        List<SelectOption> options = new List<SelectOption>();        
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Daily','Daily'));
        options.add(new SelectOption('Weekly','Weekly')); 
        options.add(new SelectOption('Monthly','Monthly'));
        options.add(new SelectOption('Quarterly','Quarterly'));
        options.add(new SelectOption('Yearly','Yearly'));       
        return options;
    }*/
    
    public boolean hasRevenueRollingYearTwoRendered = false;
    public boolean getHasRevenueRollingYearTwoRendered() {
        if(opportunityProduct.ContractTerminmonths__c <> null && opportunityProduct.ContractTerminmonths__c > 12 ) {
            hasRevenueRollingYearTwoRendered = true;
        }else{
            hasRevenueRollingYearTwoRendered = false;
        }
        return hasRevenueRollingYearTwoRendered;
    }
    public void setHasRevenueRollingYearTwoRendered(boolean val) {
        hasRevenueRollingYearTwoRendered = val;
    }
    
    public boolean hasScheculeSectionRendered = false;
    public boolean getHasScheculeSectionRendered() {
        if((isScheduleClicked <> null && isScheduleClicked == 'Yes') ||  revenueRYWrapperList.size() > 0) {
            hasScheculeSectionRendered = true;
        }else{
            hasScheculeSectionRendered = false;
        }
        return hasScheculeSectionRendered;
    }
    public void setHasScheculeSectionRendered(boolean val) {
        hasScheculeSectionRendered = val;
    }
    
    public boolean hasScheculeListRendered = false;
    public boolean getHasScheculeListRendered() {
        
        if(revenueRYWrapperList <> null && revenueRYWrapperList .size() > 0 ) {
            hasScheculeListRendered = true;
        }else{
            hasScheculeListRendered = false;
        }
        return hasScheculeListRendered;
    }
    public void setHasScheculeListRendered(boolean val) {
        hasScheculeListRendered = val;
    }
    
    public boolean hasScheculeListRollingYearOneRendered = false;
    public boolean gethasScheculeListRollingYearOneRendered () {
        
        if(revenueForRollingYearOneWrapperList <> null && revenueForRollingYearOneWrapperList.size() > 0 ) {
            hasScheculeListRollingYearOneRendered  = true;
        }else{
            hasScheculeListRollingYearOneRendered = false;
        }
        return hasScheculeListRollingYearOneRendered ;
    }
    public void sethasScheculeListRollingYearOneRendered (boolean val) {
        hasScheculeListRollingYearOneRendered = val;
    }
    
    public boolean hasScheculeListRollingYearTwoRendered = false;
    public boolean gethasScheculeListRollingYearTwoRendered () {
        
        if(revenueForRollingYearTwoWrapperList <> null && revenueForRollingYearTwoWrapperList.size() > 0 ) {
            hasScheculeListRollingYearTwoRendered  = true;
        }else{
            hasScheculeListRollingYearTwoRendered = false;
        }
        return hasScheculeListRollingYearTwoRendered ;
    }
    public void sethasScheculeListRollingYearTwoRendered (boolean val) {
        hasScheculeListRollingYearTwoRendered = val;
    }
    
    
    
    public class RevenueRollingYearWrapper {
        public RevenueSchedule__c revSchedule {get; set;}
        public List<productScheduleWrapper> scheduleList {get; set;}
        public integer index {get; set;}
        public integer rollYear {get; set;}
        
        public revenueRollingYearWrapper(integer indx){
            index = indx;
            rollYear = index + 1;
            revSchedule = new RevenueSchedule__c();
        }
        
        public boolean hasAutoPopulateDisabled = false;
        public boolean getHasAutoPopulateDisabled  () {
            
            if(scheduleList <> null && scheduleList.size() > 0 ) {
                hasAutoPopulateDisabled   = true;
            }else{
                hasAutoPopulateDisabled  = false;
            }
            return hasAutoPopulateDisabled  ;
        }
        public void setHasAutoPopulateDisabled  (boolean val) {
            hasAutoPopulateDisabled = val;
        }
    }   
    
    
    public class ProductScheduleWrapper {
        public ProductSchedule__c prodSchedule {get; set;}
        public Boolean selected {get; set;}
        public integer index {get; set;}
        public string formatedDate {get; set;}
        
        public productScheduleWrapper(ProductSchedule__c ps, integer indx) {
            prodSchedule = ps;
            selected = false;
            index = indx;
        }
    }
}