/*
    @Author: Rajan Saini
    @Company: SaaSFocus
    @Description: Test class for the controller of VIC_IOTCVCalculationCtlr
*/

@isTest
 public class VIC_IOTCVCalculationCtlrTest{
   
      static testmethod void validatemethod(){
          
        user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
        insert objuser;
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdjh.com','System administrator','China');
        insert objuser1;
         APXTConga4__Conga_Template__c obj= new VIC_CommonTest().initAllcongaData('GRM - CM VIC Letter');
         insert obj;
        
         List<Plan__c> plan = new VIC_CommonTest().initAllPlanData(obj.ID);
         insert plan;
        
          Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
            masterVICRoleObj.role__c='SL';
            masterVICRoleObj.Horizontal__c='CM/ITO';
          insert masterVICRoleObj;
    
           Master_VIC_Role__c masterVICRoleObj1 =  VIC_CommonTest.getMasterVICRole();
            masterVICRoleObj1.role__c='SL';
         //   masterVICRoleObj1.Horizontal__c='CM/ITO';
          insert masterVICRoleObj1; 
            User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
            objuservicrole.vic_For_Previous_Year__c=false;
            objuservicrole.Not_Applicable_for_VIC__c = false;
            objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
            insert objuservicrole;
          
         User_VIC_Role__c objuservicrole1=VIC_CommonTest.getUserVICRole(objuser1.id);
            objuservicrole1.vic_For_Previous_Year__c=false;
            objuservicrole1.Not_Applicable_for_VIC__c = false;
            objuservicrole1.Master_VIC_Role__c=masterVICRoleObj1.id;
            insert objuservicrole1; 
          List<Master_Plan_Component__c >lstMaster= new VIC_CommonTest().initAllMasterPlanComponent();
        insert lstMaster;
        List<Plan_Component__c> planComponent= new VIC_CommonTest().initAllPlanComponent(plan,lstMaster);
        
        VIC_Calculation_Matrix__c objMatrix = VIC_CommonTest.fetchMatrixComponentData('SL CP Enterprise');
        insert objMatrix;
        
        Map <Id, User_VIC_Role__c> mapUserIdToUserVicRole=vic_CommonUtil.getUserVICRoleinMap();
        List<VIC_Calculation_Matrix_Item__c> calcMatrix = new VIC_CommonTest().fetchListMatrixItemIOTCVData(objMatrix.Name);
      
        Target__c target = new Target__c();
        target.Target_Bonus__c = 8000;
        target.Plan__c = plan[2].id;
        target.Plan__r = plan[2];
        target.user__c = objUser.id;
        insert target;
        
        Target__c target1 = new Target__c();
        target1.Target_Bonus__c = 8000;
        target1.Plan__c = plan[2].id;
        target1.Plan__r = plan[2];
        target1.user__c = objUser1.id;
        insert target1; 
          
        Target_Component__c  targetComp = new Target_Component__c();
        targetComp.Weightage__c =10;
        targetComp.vic_Achievement_Percent__c =70;
        targetComp.Target_Status__c = 'Active';
        targetComp.Target_In_Currency__c =100;
        targetComp.Is_Weightage_Applicable__c = true;
        targetComp.Target__c = target.id;
        targetComp.Target__r = target;
        insert targetComp;
          
        Target_Component__c  targetComp1 = new Target_Component__c();
        targetComp1.Weightage__c =10;
        targetComp1.vic_Achievement_Percent__c =70;
        targetComp1.Target_Status__c = 'Active';
        targetComp1.Target_In_Currency__c =100;
        targetComp1.Is_Weightage_Applicable__c = true;
        targetComp1.Target__c = target1.id;
        targetComp1.Target__r = target;
        insert targetComp1; 
        

        Test.StartTest();          
        Map<String, vic_Incentive_Constant__mdt> mapData= new VIC_CommonTest().fetchMetaDataValue();
        VIC_IOTCVCalculationCtlr ctrl = new VIC_IOTCVCalculationCtlr();
        ctrl.pmAcheivmentPercent=70;
        ctrl.opAcheivmentPercent=55;  
        ctrl.mapValueToObjIncentiveConst = mapData;  
        ctrl.mapUserIdToUserVicRoles= mapUserIdToUserVicRole;
        ctrl.calculate(targetComp1,calcMatrix);  
        ctrl.calculate(targetComp,calcMatrix);
          
        ctrl.pmAcheivmentPercent=110;
        ctrl.opAcheivmentPercent=120; 
        ctrl.calculate(targetComp,calcMatrix);
          System.AssertEquals(200,200);
        Test.StopTest();
       
  }

}