/*
 * This is the test class for GENcEditOpportunityProductTest class
 * @ Kumar Pulkesin
*/

@isTest
public class GENcEditOpportunityProductTest_New {
    
    public static testMethod void RunTest1(){
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),genuser.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,UserInfo.getUserId());        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        
        system.runAs(genuser){
            test.starttest();            
            Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
            Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
            OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,24000);
                        
            // Creating two opportunity revenue schedules
            RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,13,2,12000);
            RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,12,1,12000);
            
            ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
                        
            Product_Schedule_Dummy__c oDummyProductSchedule = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,3000,3);
            ApexPages.StandardController stdController = new ApexPages.StandardController(oOpportunityProduct);
            GENcEditOpportunityProduct  oGENcEditOpportunityProduct  = new GENcEditOpportunityProduct(stdController);
            GENcEditOpportunityProduct.ProductScheduleWrapper oProductScheduleWrapper = new GENcEditOpportunityProduct.ProductScheduleWrapper(oProductSchedule,3);
            GENcEditOpportunityProduct.RevenueRollingYearWrapper  oRevenueRollingYearWrapper  = new GENcEditOpportunityProduct.RevenueRollingYearWrapper(3);            
            oGENcEditOpportunityProduct.getProductShedule();
            oGENcEditOpportunityProduct.validateSchedule();           
            oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
            oRevenueRollingYearWrapper.setHasAutoPopulateDisabled(true);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule1);
            oGENcEditOpportunityProduct.revenueRollingYearToEdit = 1;
            oGENcEditOpportunityProduct.installmentReminder = 30;
            List<ProductSchedule__c> productSchedulelist = new List<ProductSchedule__c>();
            productSchedulelist.add(oProductSchedule);
            productSchedulelist.add(oProductSchedule);
            productSchedulelist.add(oProductSchedule);
            oGENcEditOpportunityProduct.prw = oRevenueRollingYearWrapper;
            oGENcEditOpportunityProduct.productScheduleList = productSchedulelist;
            oGENcEditOpportunityProduct.isScheduleClicked = 'Yes';
            oGENcEditOpportunityProduct.checkContractTerrm = True;
            List<string> digitalAssets = new List<string> {'Asset1', 'Asset2', 'Asset3'};
            oGENcEditOpportunityProduct.digitalAsset_lst = digitalAssets;
            oGENcEditOpportunityProduct.isSaveAndNew = 'Yes';
            oGENcEditOpportunityProduct.ProfilesOptionsFetch();
            oGENcEditOpportunityProduct.getCurrencyIsoCodes();
            oGENcEditOpportunityProduct.showConvertedValue();
            oGENcEditOpportunityProduct.convertCurrency();
            oGENcEditOpportunityProduct.updateSchedule();
            oGENcEditOpportunityProduct.deleteProductSchedule();
            oGENcEditOpportunityProduct.validate();
            oGENcEditOpportunityProduct.disableProduct();
            oGENcEditOpportunityProduct.showdigitalOptions();
            oGENcEditOpportunityProduct.convertUSD(33.12);
            oGENcEditOpportunityProduct.deleteExistingSchedule();
            oGENcEditOpportunityProduct.getRevenueRollingYear();
            oGENcEditOpportunityProduct.getNoOfInstallment();
            oGENcEditOpportunityProduct.revenueRollingYearIndex = 0;
            oGENcEditOpportunityProduct.createSchedule();           
            oGENcEditOpportunityProduct.revenuedate_update();
            oGENcEditOpportunityProduct.getRevenue(123.32,2);
            oGENcEditOpportunityProduct.getSkillSetRendered();
            oGENcEditOpportunityProduct.setSkillSetRendered(true);
            oGENcEditOpportunityProduct.saveLineItem();
            oGENcEditOpportunityProduct.getRollingYearRevenueUSD(oRevenueRollingYearWrapper);
            
            test.stoptest();
        }
    }
    
    public static testMethod void RunTest2(){
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];        
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),genuser.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');            
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,24000);
        
        system.runAs(genuser){
            test.starttest();         
                        
            // Creating two opportunity revenue schedules
            RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,13,2,12000.567);
            RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,12,1,1000.67);
            
            ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
                        
            Product_Schedule_Dummy__c oDummyProductSchedule = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,3000,3);
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(oOpportunityProduct);
            GENcEditOpportunityProduct  oGENcEditOpportunityProduct  = new GENcEditOpportunityProduct(stdController);
            GENcEditOpportunityProduct.ProductScheduleWrapper oProductScheduleWrapper = new GENcEditOpportunityProduct.ProductScheduleWrapper(oProductSchedule,3);
            GENcEditOpportunityProduct.RevenueRollingYearWrapper  oRevenueRollingYearWrapper  = new GENcEditOpportunityProduct.RevenueRollingYearWrapper(3);            
            oGENcEditOpportunityProduct.getProductShedule();
            oGENcEditOpportunityProduct.validateSchedule();           
            oRevenueRollingYearWrapper.getHasAutoPopulateDisabled();
            oRevenueRollingYearWrapper.setHasAutoPopulateDisabled(true);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule1);
            oGENcEditOpportunityProduct.revenueRollingYearToEdit = 1;
            oGENcEditOpportunityProduct.installmentReminder = 30;
            List<ProductSchedule__c> productSchedulelist = new List<ProductSchedule__c>();
            productSchedulelist.add(oProductSchedule);
            productSchedulelist.add(oProductSchedule);
            productSchedulelist.add(oProductSchedule);
            oGENcEditOpportunityProduct.prw = oRevenueRollingYearWrapper;
            oGENcEditOpportunityProduct.productScheduleList = productSchedulelist;
            oGENcEditOpportunityProduct.isScheduleClicked = 'Yes';
            oGENcEditOpportunityProduct.checkContractTerrm = True;
            List<string> digitalAssets = new List<string> {'Asset1', 'Asset2', 'Asset3'};
            oGENcEditOpportunityProduct.digitalAsset_lst = digitalAssets;
            oGENcEditOpportunityProduct.isSaveAndNew = 'Yes';            
            oGENcEditOpportunityProduct.revenueRollingYearIndex = 2;
            oGENcEditOpportunityProduct.rollingyearrevenue1 = 20000;
            oGENcEditOpportunityProduct.rollingyearrevenue2 = 10000;
                        
            // Scenario to edit the revenue start date and contract term
            oOpportunityProduct.ContractTerminmonths__c = 7;
            oOpportunityProduct.RevenueStartDate__c = System.today().addDays(3);
            update oOpportunityProduct;
            
            oGENcEditOpportunityProduct.revenuedate_update();            
            oGENcEditOpportunityProduct.saveLineItem();
            oGENcEditOpportunityProduct.getRollingYearRevenueUSD(oRevenueRollingYearWrapper);
            
            test.stoptest();
        }
    }
    
    // Method to cover the Errors & Exceptions where the start Day is changed
    public static testMethod void RunExceptionTest1(){
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),genuser.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,UserInfo.getUserId());        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        
        system.runAs(genuser){
            test.starttest();            
            Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
             
            Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
            OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateErrorOpportunityProduct(oOpportunity.Id,oProduct.Id,30,24000);
                        
            // Creating two opportunity revenue schedules
            RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,12,12,12000);
            RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,13,1,12000);
            
            ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
            Product_Schedule_Dummy__c oDummyProductSchedule = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,3,3);

            ApexPages.StandardController stdController = new ApexPages.StandardController(oOpportunityProduct);
            GENcEditOpportunityProduct  oGENcEditOpportunityProduct  = new GENcEditOpportunityProduct(stdController);
            GENcEditOpportunityProduct.ProductScheduleWrapper oProductScheduleWrapper = new GENcEditOpportunityProduct.ProductScheduleWrapper(oProductSchedule,3);
            GENcEditOpportunityProduct.RevenueRollingYearWrapper  oRevenueRollingYearWrapper1  = new GENcEditOpportunityProduct.RevenueRollingYearWrapper(3);
                        
            oGENcEditOpportunityProduct.getProductShedule();
            oGENcEditOpportunityProduct.validateSchedule();
            oRevenueRollingYearWrapper1.getHasAutoPopulateDisabled();
            oRevenueRollingYearWrapper1.setHasAutoPopulateDisabled(true);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule);
            oGENcEditOpportunityProduct.getRevenueSchedule(oRevenueSchedule1);
            oGENcEditOpportunityProduct.revenueRollingYearToEdit = 1;
            oGENcEditOpportunityProduct.installmentReminder = 30;
            oGENcEditOpportunityProduct.ProfilesOptionsFetch();
            oGENcEditOpportunityProduct.getCurrencyIsoCodes();
            oGENcEditOpportunityProduct.showConvertedValue();
            oGENcEditOpportunityProduct.convertCurrency();
            oGENcEditOpportunityProduct.updateSchedule();
            oGENcEditOpportunityProduct.deleteProductSchedule();
            oGENcEditOpportunityProduct.validate();                                
            test.stoptest();            
        }
    }
    
    // Method to cover the Errors & Exceptions where the opportunity Stage name is changed
    public static testMethod void RunExceptionTest2(){
        Profile genprofile = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin'];
        User genuser =GEN_Util_Test_Data.CreateUser('genpactuser2015@testorg.com',genprofile.Id,'genpactusertestgen2015@testorg.com');
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),genuser.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,UserInfo.getUserId());        
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test121@gmail.com','9891798737');
        
        system.runAs(genuser){
            test.starttest();            
            Opportunity eOpportunity = GEN_Util_Test_Data.CreateErrorOpportunity('Test',oAccount.Id,oContact.Id);
            eOpportunity.StageName = '6. Signed Deal';
            insert eOpportunity;
            
            Product2 oProduct1 = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
            OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(eOpportunity.Id,oProduct1.Id,30,24000);
                        
            // Creating two opportunity revenue schedules
            RevenueSchedule__c oRevenueSchedule = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,1,2017,12,120.23);
            RevenueSchedule__c oRevenueSchedule1 = GEN_Util_Test_Data.CreateRevenueSchedule(oOpportunityProduct.id,2,2018,1,126.45);
            
            ProductSchedule__c oProductSchedule = GEN_Util_Test_Data.CreateProductSchedule(oOpportunityProduct.id,oRevenueSchedule.id);
            Product_Schedule_Dummy__c oDummyProductSchedule1 = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,3,3);

            ApexPages.StandardController stdController1 = new ApexPages.StandardController(oOpportunityProduct);
            GENcEditOpportunityProduct  oGENcEditOpportunityProduct1  = new GENcEditOpportunityProduct(stdController1);
            GENcEditOpportunityProduct.ProductScheduleWrapper oProductScheduleWrapper1 = new GENcEditOpportunityProduct.ProductScheduleWrapper(oProductSchedule,3);
            GENcEditOpportunityProduct.RevenueRollingYearWrapper  oRevenueRollingYearWrapper2  = new GENcEditOpportunityProduct.RevenueRollingYearWrapper(3);
                        
            oGENcEditOpportunityProduct1.getProductShedule();
            oGENcEditOpportunityProduct1.validateSchedule();
            oRevenueRollingYearWrapper2.getHasAutoPopulateDisabled();
            oRevenueRollingYearWrapper2.setHasAutoPopulateDisabled(true);
            oGENcEditOpportunityProduct1.getRevenueSchedule(oRevenueSchedule);
            oGENcEditOpportunityProduct1.getRevenueSchedule(oRevenueSchedule1);
            oGENcEditOpportunityProduct1.revenueRollingYearToEdit = 1;
            oGENcEditOpportunityProduct1.installmentReminder = 30;
            oGENcEditOpportunityProduct1.ProfilesOptionsFetch();
            oGENcEditOpportunityProduct1.getCurrencyIsoCodes();
            oGENcEditOpportunityProduct1.showConvertedValue();
            oGENcEditOpportunityProduct1.convertCurrency();
            oGENcEditOpportunityProduct1.updateSchedule();
            oGENcEditOpportunityProduct1.deleteProductSchedule();
            oGENcEditOpportunityProduct1.validate();                                
            test.stoptest();            
        }
    }
}