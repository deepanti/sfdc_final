@isTest
public class GPSelectorProjectBudgetTracker {
    public static GP_Project_Budget__c projectBudget;
    
    @testSetup
    public static void buildDependencyData() {
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        GP_Project__c prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        insert prjobj;
     
        GP_Project_Budget__c objPrjBdgt1 =  new GP_Project_Budget__c(); 
        objPrjBdgt1.GP_Cost_Rate__c = 11;
        objPrjBdgt1.GP_Country__c =  'India';
        objPrjBdgt1.GP_TCV__c =  111;
        objPrjBdgt1.GP_Total_Cost__c = 11;
        objPrjBdgt1.GP_OMS_Id__c = '2';     
        objPrjBdgt1.GP_Project__c = prjobj.ID;
        
        insert objPrjBdgt1;
    }
    
    @isTest
    public static void testselectById() {
        projectBudget = [Select Id from GP_Project_Budget__c LIMIT 1];
        
        GPSelectorProjectBudget projectBudgetSelector = new GPSelectorProjectBudget();
        projectBudgetSelector.selectById(new Set<Id> {projectBudget.Id});
        projectBudgetSelector.selectProjectBudgetRecord(projectBudget.Id); 
        projectBudgetSelector.selectProjectBudgetRecords(projectBudget.Id);
        projectBudgetSelector.selectProjectBudgetRecords(new Set<Id>());
    }
    
}