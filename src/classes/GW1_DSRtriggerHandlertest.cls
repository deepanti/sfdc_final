/*
      This class is used to test GW1_DSRtriggerHandler  
      --------------------------------------------------------------------------------------
      Name                 Date                 Version                      
      --------------------------------------------------------------------------------------
      Rishi Patel                    3/17/2016                    1.0  
      --------------------------------------------------------------------------------------
  */
@IsTest(SeeAllData=true)
public class GW1_DSRtriggerHandlertest {
    static testMethod void test1(){
    
        //GW1_CommonTracker.createTriggerCustomSetting();
        //user objuser1=GW1_CommonTracker.createUser('standt','Testing','Test@Genpact.com');
        //user objuser2=GW1_CommonTracker.createUser('standt1','Testing1','test@Testnew.com');
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        Profile p2 = [SELECT Id FROM Profile WHERE Name='Genpact Tower Lead']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        User u2 =GEN_Util_Test_Data.CreateUser('standarduser2015ff@testorg.com',p2.Id,'standardusertestgen2015ff@testorg.com' );

        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test111',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
       
        GW1_DSR__c objGWDSR = new GW1_DSR__c();

        objGWDSR.Name = 'strName';
		objGWDSR.GW1_Opportunity__c = oOpportunity.id;
        insert objGWDSR;
        GW1_Bid_Manager__c objbidmanager=GW1_CommonTracker.createBidManager();
        
        objGWDSR.GW1_Bid_Manager__c=objbidmanager.id;
        
        update objGWDSR;
        
        GW1_Bid_Manager__c objBidManager2 = new GW1_Bid_Manager__c();
        objBidManager2.Name = 'Test Bid Manager2';
        objBidManager2.GW1_Bid_Manager__c = u.id;
        objBidManager2.GW1_Tower_lead__c = u.id;

        insert objBidManager2;
        
        test.startTest();
        objGWDSR.GW1_Bid_Manager__c = objBidManager2.id;
        objGWDSR.ownerid = u2.id;
        update objGWDSR;
        test.stopTest();
        /*List<GW1_DSR__c> dsrlist = new List<GW1_DSR__c>();
        dsrlist.add(objgw1dsrc);
        GW1_DSRtriggerHandler gwd = new GW1_DSRtriggerHandler();
        gwd.createDSRGroup(dsrlist);*/
        

       
        
        //GW1_DSR_Group__c objdsrgroup = [Select id, name, GW1_DSR__c from GW1_DSR_Group__c where GW1_DSR__c!=null];
        
       
    }
    public static testmethod void test2()
    {
        //  GW1_Bid_Manager__c objBidManager = GW1_CommonTracker.createBidManager();
       
    }
    
    
}