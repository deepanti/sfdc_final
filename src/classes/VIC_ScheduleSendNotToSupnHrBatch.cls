global class VIC_ScheduleSendNotToSupnHrBatch implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        VIC_SendNotificationToSupnHrBatch objSN= new VIC_SendNotificationToSupnHrBatch();
        Database.executeBatch(objSN, 2);
    }   
}