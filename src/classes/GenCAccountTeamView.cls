public with sharing class GenCAccountTeamView {

    public List<AccountTeamMember> oATMList = new List<AccountTeamMember>();
    public List<String> UserName{get;set;}
    public set<id> Userid{get;set;}
    public GenCAccountTeamView(ApexPages.StandardController controller) {
        userid = new set<id>();
        oATMList=[Select UserId from AccountTeamMember where Accountid =: controller.getid()];
        for(AccountTeamMember oATM: oATMList)
        userid.add(oATM.Userid);
    }
    public List<String> getTeamMembers() {
        UserName = new List<String>();
        for(User oUser: [Select FirstName, LastName from User where Id in:userid] )
        {
            UserName.add(oUser.FirstName +' '+ oUser.Lastname);
        }
        return UserName;
    }

}