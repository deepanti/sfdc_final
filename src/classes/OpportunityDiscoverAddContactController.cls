public class OpportunityDiscoverAddContactController {
    
    @AuraEnabled
    // fetching the picklist data for contact role !
    public static List<String> getContactRolePicklist(){
        try{
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact_Role_Rv__c.Role__c.getDescribe();
        List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pList) {
            options.add(p.getLabel());
        }
        return options;
        }
         catch(Exception e){
             //creating error log
            CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'OpportunityDiscoverAddContactController', 'getContactRolePicklist','Fail','',String.valueOf(e.getLineNumber()));
            return null;
    }
    }
    @AuraEnabled
    // function which fetches contact Role records of given Opportunity 
    public static List<Contact_Role_Rv__c> getContactRoleRecords(String oppId){
        List<Contact_Role_Rv__c> listContactRole =[select Id,Role__c,IsPrimary__c,Contact__c,Contact__r.Name from Contact_Role_Rv__c where Opportunity__c=:oppId];
        
        return listContactRole;
    }
    @AuraEnabled
    public static String saveContactRole(String listOfContactRole,String oppId,String deleteItemList){
        // List containing Role Records which needs to be inserted or updated  
        List<Contact_Role_Rv__c> newContactRoleList = (List<Contact_Role_Rv__c>)System.JSON.deserialize(listOfContactRole, List<Contact_Role_Rv__c>.class);
        // List containing Role Records that needs to be deleted 
        List<Contact_Role_Rv__c> deleteContactRoleList=(List<Contact_Role_Rv__c>)System.JSON.deserialize(deleteItemList, List<Contact_Role_Rv__c>.class); 
        String message='';
        String missingEmailContact='';
        Savepoint sp = Database.setSavepoint();
        try
        {
            set<Id> conSet=new set<Id>();//fetch Id of contact 
            for(Contact_Role_Rv__c Role:newContactRoleList)
        	{
            	conSet.add(Role.Contact__c);
        	}
            List<Contact> missingEmailContactList=[select id,Name,Email from contact where id in:conSet and Email=null];
            for(Contact con:missingEmailContactList)
            {
                missingEmailContact+=con.Name+' ';
            }
            if(deleteContactRoleList.size()>0)
            {  
                if(!schema.sobjecttype.Contact_Role_Rv__c.isdeletable())
                {
                    throw new AuraHandledException('You Dont have delete access!');
                    
                }
                update deleteContactRoleList;//updating the contact role since we cannot delete primary contact directly 
                delete deleteContactRoleList;//deleting the contact role 
            }
            
            if(newContactRoleList.size()>0)
            {
                
                upsert newContactRoleList;
            }
            Contact_Role_Rv__c primaryRole=[select Contact__c,Role__c from Contact_Role_Rv__c where IsPrimary__c=true and Opportunity__c=:oppId limit 1];
            Opportunity updateOpportunity=[select Contact1__c ,Role__c from opportunity where id=:oppId];
            updateOpportunity.Contact1__c=primaryRole.Contact__c;
            updateOpportunity.Role__c=primaryRole.Role__c;
            update updateOpportunity;
            message='Data Saved Successfully';
        }
        catch(Exception ex) 
        {
              //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(oppId),ex.getMessage(), '', ex.getStackTraceString(),'OpportunityDiscoverAddContactController', 'saveContactRole','Fail','',String.valueOf(ex.getLineNumber()));
            if(ex.getMessage().contains('MSA/SOW closure date cannot be earlier than today'))
            {
                message='MSA/SOW closure date cannot be earlier than today';
            }
            else if(ex.getMessage().containsIgnoreCase('script'))
            {
                message = 'You Dont have delete access!';
            }
            else if(ex.getMessage().containsIgnoreCase('Value does not exist or does not match filter criteria'))
            {
                message='Email is missing in contact->'+missingEmailContact;
            }
            else
            {
                message=ex.getMessage();
            }
            Database.rollback(sp);
        }
        return message;
    }
}