// ------------------------------------------------------------------------------------------------ 
// Description: When the exit date of an employee reaches definined notification days then email is sent to the employee and his managers depending on 
// the days left in between present date and the exit date of the employee
// ------------------------------------------------------------------------------------------------
// Modification Date::04-DEC -2017  Created By : Mandeep Singh Chauhan
// ------------------------------------------------------------------------------------------------ 
global class GPBatchtoExistingEmployee implements
Database.Batchable < sObject > , Database.Stateful {
    
    
    EmailTemplate EmpTemplate = [Select id, DeveloperName, body, subject, htmlvalue
                                 from EmailTemplate 
                                 where developername = 'GP_Email_Template_For_Exiting_Employee_Notification'
                                 limit 1
                                ]; 
    
    //global Date initialPriorDays = system.today().addDays(30); 
    //global Date secondPriorDays = system.today().addDays(15);
    //global Date thirdPriorDays = system.today().addDays(7);
    global date initialPriorDays = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Prior_Days_of_exiting_date)); 
    global date secondPriorDays = System.Today().adddays(integer.valueOf(system.label.GP_Second_Prior_Days_of_exiting_date));  
    global date thirdPriorDays = System.Today().adddays(integer.valueOf(system.label.GP_Third_Prior_Days_of_exiting_date));    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select Id, Name, 
                                         GP_SFDC_User__c,
                                         GP_OFFICIAL_EMAIL_ADDRESS__c, 
                                         GP_ACTUAL_TERMINATION_Date__c, 
                                         GP_Supervisor_Employee__r.GP_Supervisor_Employee__c,
                                         GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c,
                                         GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c
                                         From GP_Employee_Master__c 
                                         where GP_ACTUAL_TERMINATION_Date__c =: initialPriorDays or
                                               GP_ACTUAL_TERMINATION_Date__c =: secondPriorDays or 
                                               GP_ACTUAL_TERMINATION_Date__c =: thirdPriorDays
                                        ]);
    }
    
    global void execute(Database.BatchableContext bc, List < GP_Employee_Master__c > lisOfEmployeeMaster) {
    
       
        Map<Id, List<GP_Project_Leadership__c>> mapOfEmployeeIdToListOfProjectLeadership = new Map<Id, List<GP_Project_Leadership__c>>();
        List<Id> listOfEmployeeId = new List<Id>();

        for(GP_Employee_Master__c employeeMaster : lisOfEmployeeMaster) {
            listOfEmployeeId.add(employeeMaster.Id);
        }

        for(GP_Project_Leadership__c projectLeadership : [SELECT Id, Name, 
                                                                    RecordTypeId, 
                                                                    GP_Project__c, 
                                                                    GP_End_Date__c, 
                                                                    RecordType.Name, 
                                                                    GP_Start_Date__c, 
                                                                    GP_Employee_ID__c,
                                                                    GP_Project__r.Name, 
                                                                    GP_Leadership_Role__c, 
                                                                    GP_Employee_ID__r.Name, 
                                                                    GP_Leadership_Master__c, 
                                                                    GP_Project__r.GP_Project_Type__c,
                                                                    GP_Pinnacle_End_Date__c, 
                                                                    GP_Employee_Field_Type__c, 
                                                                    GP_Leadership_Role_Name__c,
                                                                    GP_Leadership_Master__r.Name,
                                                                    GP_Parent_Project_Leadership__c, 
                                                                    GP_Project__r.GP_Approval_Status__c, 
                                                                    GP_Editable_after_PID_Creation__c, 
                                                                    GP_Project__r.GP_Oracle_PID__c,
                                                                    GP_Employee_ID__r.GP_Employee_Unique_Name__c, 
                                                                    GP_Project__r.GP_Project_Short_Name__c 
                                                                  FROM GP_Project_Leadership__c 
                                                                  where GP_Active__c = true
                                                          		  AND GP_Project__r.GP_Approval_Status__c != 'Closed'
                                                                  AND GP_Employee_ID__c in :listOfEmployeeId]) {
            if(!mapOfEmployeeIdToListOfProjectLeadership.containsKey(projectLeadership.GP_Employee_ID__c)) {
                mapOfEmployeeIdToListOfProjectLeadership.put(projectLeadership.GP_Employee_ID__c, new List<GP_Project_Leadership__c>());
            }

            mapOfEmployeeIdToListOfProjectLeadership.get(projectLeadership.GP_Employee_ID__c).add(projectLeadership);
        }
        List<Messaging.SingleEmailMessage> listOfEmailMessage ;
        if (lisOfEmployeeMaster != null && !lisOfEmployeeMaster.isEmpty()) {
            listOfEmailMessage  =  setEmailbody(lisOfEmployeeMaster, mapOfEmployeeIdToListOfProjectLeadership);
        }  

        if (listOfEmailMessage != null && 
            listOfEmailMessage.size() > 0 && 
            GPCommon.allowSendingMail()) {
            Messaging.SendEmailResult[] EHRBP =  Messaging.sendEmail(listOfEmailMessage);
        }
        
    }
    
    global void finish(Database.BatchableContext bc) {
       
      
    }
    
    public List<Messaging.SingleEmailMessage> setEmailbody(List < GP_Employee_Master__c > listofemployees, Map<Id, List<GP_Project_Leadership__c>> mapOfEmployeeIdToListOfProjectLeadership) {
        List<Messaging.SingleEmailMessage> listEmail  = new List<Messaging.SingleEmailMessage>();
        list<string> listofEmails;
        Id orgWideEmailId = GPCommon.getOrgWideEmailId();
        for (GP_Employee_Master__c employee: listofemployees) 
        { 
            GPCommon objcommon = new GPCommon();
            Messaging.SingleEmailMessage message = objcommon.notificationThroughEmailForBatch(EmpTemplate, employee, 'GP_Field_Set_Emp_Batch_For_Email_Notific', 'GP_Employee_Master__c');
            
            if(!mapOfEmployeeIdToListOfProjectLeadership.containsKey(employee.Id)) {
                continue ;
            }
            
            message.setHtmlBody(message.getHtmlBody().replace('LIST_PROJECT_LEADERSHIP_RECORDS', createProjectTableString(employee, 
                mapOfEmployeeIdToListOfProjectLeadership.get(employee.Id))));
            
            listofEmails = new list < string > ();
 
            if (employee.GP_ACTUAL_TERMINATION_Date__c != null) {
                if (employee.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                    listofEmails.add(employee.GP_OFFICIAL_EMAIL_ADDRESS__c);
                    
                    if (employee.GP_ACTUAL_TERMINATION_Date__c == initialPriorDays) {
                        message.setSubject(message.getSubject().replace('REMINDER_DURATION', 'Reminder I'));                        
                    }
                    
                    if (employee.GP_ACTUAL_TERMINATION_Date__c == secondPriorDays) {
                        message.setSubject(message.getSubject().replace('REMINDER_DURATION', 'Reminder II'));
                        //The mail should have been sent to the manager as a cc user but for reducing the impact of mass email send the email has been sent to the manager as the non cc user
                        if (employee.GP_Supervisor_Employee__c != null &&
                            employee.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                                //message.settargetobjectid(emp.GP_Employee_HR_History__r.GP_Supervisor_Employee__r.GP_SFDC_User__c);
                                listofEmails.add(employee.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                            }
                        
                    } else if (employee.GP_ACTUAL_TERMINATION_Date__c == thirdPriorDays) {
                        if (employee.GP_Supervisor_Employee__c != null &&
                            employee.GP_Supervisor_Employee__r.GP_Supervisor_Employee__c != null &&
                            employee.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null &&
                            employee.GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c != null) {
                                message.setSubject(message.getSubject().replace('REMINDER_DURATION', 'Reminder III'));
                                //message.settargetobjectid(emp.GP_Employee_HR_History__r.GP_Supervisor_Employee__r.GP_SFDC_User__c);
                                listofEmails.add(employee.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                                listofEmails.add(employee.GP_Supervisor_Employee__r.GP_Supervisor_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c);
                            }
                    }
                }

                if (listofEmails != null && listofEmails.size() > 0) {
                    message.setToAddresses(listofEmails);
                }

                if (message != null) {
                    // since a single settargetobjectid can only be added in a message so 
                    // they are added individualy in the individual message above
                    message.settargetobjectid(null);
                    if(orgWideEmailId != null)
                    	message.setOrgWideEmailAddressId(orgWideEmailId);
                    listEmail.add(message);
                }
            }
        }
        return listEmail; 
    }
    private String createProjectTableString(GP_Employee_Master__c employee, List<GP_Project_Leadership__c> listOfProjectLeadership) {
        String html = '';
        if (listOfProjectLeadership.size() > 0) {
            html += ' <table  width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">';
            html += '   <thead>';
            html += '       <tr>';
            html += '           <th bgcolor="#005595" style="color:white">PID/Project ID</th>';
            html += '           <th bgcolor="#005595" style="color:white">Process Names</th>';
            html += '           <th bgcolor="#005595" style="color:white">Project/Billing Type</th>';
            html += '           <th bgcolor="#005595" style="color:white" >PID Stage</th>';
            html += '           <th bgcolor="#005595" style="color:white" >Current Key Member Role</th>';
            html += '       </tr>';
            html += '   </thead>';
            html += '   <tbody>';
            
            for (GP_Project_Leadership__c projectLeadership: listOfProjectLeadership) {
                html += '       <tr>';
                html += '           <td>' + projectLeadership.GP_Project__r.GP_Oracle_PID__c + '</td>';
                html += '           <td>' + projectLeadership.GP_Project__r.GP_Project_Short_Name__c + '</td>';
                html += '           <td>' + projectLeadership.GP_Project__r.GP_Project_Type__c + '</td>';
                html += '           <td>' + projectLeadership.GP_Project__r.GP_Approval_Status__c + '</td>';
                html += '           <td>' + projectLeadership.GP_Leadership_Role_Name__c + '</td>';
                html += '       </tr>';
            }
            html += ' </tbody></table>';
        }
        return html;
    }
}