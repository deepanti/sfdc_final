Public class UpdateWLRReason
{
    @AuraEnabled
    public static Opportunity saveWLR(Id oppId, string conid, string SurveyNumber, Map<string,string> maps, Opportunity OppSurvey) 
    {
        try{
        system.debug('OppId=========='+oppId);  
        system.debug('maps=========='+maps);
         system.debug('SurveyNumber=========='+SurveyNumber);
        system.debug('OppSurvey=========='+OppSurvey);
        Integer count=maps.size();
        Opportunity o=[select id,Reason_1__c,Reason_2__c,Reason_3__c,key_win_reasons_and_any_key_learnings__c from opportunity where id=: oppId];
        
        List<opportunity> oppty=new List<opportunity>();
        oppty=[select id,W_L_D__c,Reason_1__c,Reason_2__c,Reason_3__c,key_win_reasons_and_any_key_learnings__c,Incumbent__c,Incumbent_Others__c,Genpact_Involvement_in_deal_with_prospec__c,interactions_did_we_have_with_the_prospe__c,If_advisor_was_involved__c,Closest_Competitor__c,Closest_Competitor_1__c,Closest_Competitor_2__c,Competitor_strengths1__c,Competitor_strengths2__c,Competitor_weaknesses1__c,Competitor_weaknesses2__c,Genpact_s_proposed_price_differ__c,Genpact_s_proposed_price_differ_Percent__c,Deal_Winner__c,Deal_Winner_Others__c,Loss_Deal_Stage__c,SurveyNumber__c from opportunity where id=: oppId];
        
        For(opportunity op: oppty)
        {
            op.Reason_1__c='';
            op.Reason_2__c='';
            op.Reason_3__c='';
            op.key_win_reasons_and_any_key_learnings__c=''; 
            op.Incumbent__c='';
                op.Incumbent_Others__c='';
                op.Genpact_Involvement_in_deal_with_prospec__c='';
                op.interactions_did_we_have_with_the_prospe__c='';
                op.If_advisor_was_involved__c='';
                op.Closest_Competitor__c='';
                op.Closest_Competitor_1__c='';
                op.Closest_Competitor_2__c='';
                op.Competitor_strengths1__c='';
                op.Competitor_strengths2__c='';
                op.Competitor_weaknesses1__c='';
                op.Competitor_weaknesses2__c='';
                op.Genpact_s_proposed_price_differ__c='';
                op.Genpact_s_proposed_price_differ_Percent__c=null;
                op.Deal_Winner__c='';
                op.Deal_Winner_Others__c='';
                op.Loss_Deal_Stage__c='';
                op.Bypass_Validations__c = true;
                op.SurveyNumber__c = SurveyNumber;
            if(op.W_L_D__c=='Signed'){
                op.LatestTabId__c = '19'; 
                system.debug('===== 1 == IF OppSurvey.LatestTabId__c ==='+op.LatestTabId__c);
            }
            else{ op.LatestTabId__c = '1'; 
                system.debug('=====1 == ELSE OppSurvey.LatestTabId__c ==='+op.LatestTabId__c);
            }
              //  op.LatestTabId__c = '19';
        }        
        update oppty;
        Set<string> stringset=new set<string>();
        set<string> val= new set<string>();
        
         for(string key: maps.keyset())
        {
            
            stringset.add(key);
            val.add(maps.get(key));
            system.debug('In acontroller for' + key + maps.get(key));        
        }
         system.debug('In acontroller=============');  
        
        if(count== 1 ) { for(String a: stringset) { OppSurvey.Reason_1__c = a; system.debug('In nested controller for' + a + maps.get(a));   }
        }
        
        if(count== 2 )
        {
              for(String a: stringset)
                {
                    if(val.contains('1') && val.contains('2') )
                        {
                            if(maps.get(a)=='1')
                                OppSurvey.Reason_1__c = a;
                            
                            if(maps.get(a)=='2')
                                OppSurvey.Reason_2__c = a;  
                            
                        }
                        
                    if(val.contains('1') && val.contains('3') )
                        {
                            if(maps.get(a)=='1')
                                OppSurvey.Reason_1__c = a;
                            
                            if(maps.get(a)=='3')
                                OppSurvey.Reason_2__c = a;  
                            
                        }

                    if(val.contains('2') && val.contains('3') )
                        {
                            if(maps.get(a)=='2')
                                OppSurvey.Reason_1__c = a;
                            
                            if(maps.get(a)=='3')
                                OppSurvey.Reason_2__c = a;  
                            
                        }  
                    
                    if(val.contains('1') && val.contains('') )
                        {
                            if(maps.get(a)=='1')
                                OppSurvey.Reason_1__c = a;
                            
                        }  
                    
                    if(val.contains('2') && val.contains('') )
                        {
                            if(maps.get(a)=='2')
                                OppSurvey.Reason_1__c = a;
                            
                            
                        }  

                    if(val.contains('3') && val.contains('') )
                        {
                            if(maps.get(a)=='3')
                                OppSurvey.Reason_1__c = a;
                            
                            
                        }       
                                    
                    system.debug('In nested controller for' + a + maps.get(a));             
                }
        }
        
        if(count== 3 )
        {
              for(String a: stringset)
                {
                    
                    if(maps.get(a)=='1')
                        OppSurvey.Reason_1__c = a;  
                    
                    if(maps.get(a)=='2')
                        OppSurvey.Reason_2__c = a; 
                    
                    if(maps.get(a)=='3')
                        OppSurvey.Reason_3__c = a; 
                            
                                    
                    system.debug('In nested controller for' + a + maps.get(a));             
                }
        }
            OppSurvey.id = oppId;     
            OppSurvey.Bypass_Validations__c = true; 
            OppSurvey.SurveyNumber__c = SurveyNumber; 
        if(OppSurvey.W_L_D__c=='Signed'){
            OppSurvey.LatestTabId__c = '19'; 
            system.debug('=====IF OppSurvey.LatestTabId__c ==='+OppSurvey.LatestTabId__c);
        }
        else{
            OppSurvey.LatestTabId__c = '1'; 
            system.debug('=====ELSE OppSurvey.LatestTabId__c ==='+OppSurvey.LatestTabId__c);
        }
            system.debug('From contract'+conid + OppSurvey.W_L_D__c);              
            update OppSurvey;
            
            if(conid !='dummy123' && OppSurvey.W_L_D__c == 'Signed')
            {
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                                                
                req.setObjectId(conid);
                                                
                Approval.ProcessResult result = Approval.process(req);
            
                return OppSurvey; 
            }
            return OppSurvey; 
        }
         catch(Exception e){
            //creating error log
            CreateErrorLog.createErrorRecord(String.valueOf(oppId),e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'saveWLR','Fail','',String.valueOf(e.getLineNumber())); 
        
             return null;
         }
    }   
   
      public class OpportunityWrapper
      {
        public ID Id;       
        public String Incumbent_Others;        
        public String key_win_reasons_and_any_key_learnings;
        public String interactions_did_we_have_with_the_prospe;        
        public String Genpact_Involvement_in_deal_with_prospec;
        public String Incumbent;
        public String If_advisor_was_involved;        
      }
    
   /* @AuraEnabled
    public static Opportunity saveWLR(Id oppId,Map<string,string> maps,string key_reason) 
    {
        system.debug('oppId==========='+oppId);
        system.debug('maps==========='+maps);
        system.debug('key_reason==========='+key_reason);
        Opportunity o=[select id,Reason_1__c,Reason_2__c,Reason_3__c,key_win_reasons_and_any_key_learnings__c from opportunity where id=: oppid];
        
        List<opportunity> oppty=new List<opportunity>();
        oppty=[select id,Reason_1__c,Reason_2__c,Reason_3__c,key_win_reasons_and_any_key_learnings__c from opportunity where id=: oppid];
        For(opportunity op: oppty)
        {
            o.Reason_1__c='';
            o.Reason_2__c='';
            o.Reason_3__c='';
            o.key_win_reasons_and_any_key_learnings__c='';
            
        }
        
        update oppty;
        
        for(string key: maps.keyset())
        {
            Set<String> stringset=new set<string>();
            stringset.add(key);
            for(String a: stringset)
            {
                if(maps.get(a)=='1')
                    o.Reason_1__c=a;
                
                if(maps.get(a)=='2')
                    o.Reason_2__c=a;
                
                if(maps.get(a)=='3')
                    o.Reason_3__c=a;    
                
                
                system.debug('In nested controller for' + a + maps.get(a));             
            }
            system.debug('In acontroller for' + key + maps.get(key));        
        }
        system.debug('In acontroller=============');
        
        if(key_reason!='')
        {
            o.key_win_reasons_and_any_key_learnings__c=key_reason;
        }
        system.debug('o==========='+o);
        update o;
        return o;
    } */ 
    
    @auraEnabled
    public static List<String> getIncumbent()
    {
       List<String> Opp_Incumbent_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Incumbent__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_Incumbent_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_Incumbent_list======'+Opp_Incumbent_list);
        return Opp_Incumbent_list;    
      //  return null;
    }
    
    @auraEnabled
    public static List<String> getinteractions_did_we_have_with_the_prospe()
    {
        try{
        List<String> Opp_interactions_did_we_have_with_the_prospe_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.interactions_did_we_have_with_the_prospe__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_interactions_did_we_have_with_the_prospe_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_interactions_did_we_have_with_the_prospe_list======'+Opp_interactions_did_we_have_with_the_prospe_list);
        return Opp_interactions_did_we_have_with_the_prospe_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getinteractions_did_we_have_with_the_prospe','Fail','',String.valueOf(e.getLineNumber())); return null; }
       
    }
    
    @auraEnabled
    public static List<String> getGenpact_Involvement_in_deal_with_prospec()
    {
        try{
        List<String> Opp_Genpact_Involvement_in_deal_with_prospec_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Genpact_Involvement_in_deal_with_prospec__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_Genpact_Involvement_in_deal_with_prospec_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_Genpact_Involvement_in_deal_with_prospec_list======'+Opp_Genpact_Involvement_in_deal_with_prospec_list);
        return Opp_Genpact_Involvement_in_deal_with_prospec_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getGenpact_Involvement_in_deal_with_prospec','Fail','',String.valueOf(e.getLineNumber())); return null; }
    }
    
     @auraEnabled
    public static List<String> getClosestCompetitor()
    {
        try{
        List<String> Opp_ClosestCompetitor_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Closest_Competitor__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_ClosestCompetitor_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_ClosestCompetitor_list======'+Opp_ClosestCompetitor_list);
        return Opp_ClosestCompetitor_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getClosestCompetitor','Fail','',String.valueOf(e.getLineNumber())); return null; }
        
    } 
        
    @auraEnabled
    public static List<String> getClosestCompetitor1()
    {
        try{
        List<String> Opp_ClosestCompetitor1_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Closest_Competitor_1__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_ClosestCompetitor1_list.add(f.getValue());
        }   
       
        system.debug('=======Opp_ClosestCompetitor1_list======'+Opp_ClosestCompetitor1_list);
        return Opp_ClosestCompetitor1_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getClosestCompetitor1','Fail','',String.valueOf(e.getLineNumber())); return null; }
    }
    
    @auraEnabled
    public static List<String> getLossDealStage()
    {
        try{
        List<String> Opp_LossDealStage_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Loss_Deal_Stage__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_LossDealStage_list.add(f.getValue());   
        }   
       
        system.debug('=======Opp_LossDealStage_list======'+Opp_LossDealStage_list);
        return Opp_LossDealStage_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getLossDealStage','Fail','',String.valueOf(e.getLineNumber())); return null; }
    }
    
    @auraEnabled
    public static List<String> getDealWinner()
    {
        try{
        List<String> Opp_DealWinner_list = new List<String>();
        Schema.DescribeFieldResult fieldResult =  Opportunity.Deal_Winner__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple)
        {
            Opp_DealWinner_list.add(f.getValue());  
        }   
       
        system.debug('=======Opp_DealWinner_list======'+Opp_DealWinner_list);
        return Opp_DealWinner_list; } catch(Exception e){ CreateErrorLog.createErrorRecord('',e.getMessage(), '', e.getStackTraceString(),'UpdateWLRReason', 'getDealWinner','Fail','',String.valueOf(e.getLineNumber()));  return null;}}  @auraEnabled  public static List<Opportunity> getOpportunityData(ID opportunityID){try{ System.debug('opportunityID=========='+opportunityID);  List<Opportunity> opportunity_list = new List<Opportunity>(); opportunity_list = [SELECT id, name, StageName, Opportunity_Source__c, Target_Source__c, Annuity_Project__c, Deal_Type__c,W_L_D__c,TCV1__c, Nature_Of_Work_OLI__c, key_win_reasons_and_any_key_learnings__c, Reason_1__c, Reason_2__c, Reason_3__c,Incumbent__c, Incumbent_Others__c, Closest_Competitor__c, Genpact_s_proposed_price_differ__c, Genpact_s_proposed_price_differ_Percent__c, Closest_Competitor_Others__c, Closest_Competitor_1__c, Closest_Competitor_2__c, Competitor_strengths1__c, Competitor_weaknesses1__c, Competitor_strengths2__c, Competitor_weaknesses2__c, Genpact_Involvement_in_deal_with_prospec__c, interactions_did_we_have_with_the_prospe__c,If_advisor_was_involved__c, Loss_Deal_Stage__c, Deal_Winner__c, Deal_Winner_Others__c ,SurveyNumber__c,Competitor__c FROM Opportunity WHERE ID =:opportunityID]; system.debug('opportunity_list======result====='+opportunity_list); return opportunity_list;} catch(Exception e){ System.debug('Error======'+e.getStackTraceString()+ '====='+e.getMessage()); } return NULL;} @auraEnabled public static List<WLR_Clone__c> saveWLRClone(Id oppId, string ClosestCompetitor, string ClosestCompetitorOthers, string KeyWinReasons_Learning, String OppSurvey){ system.debug('==saveWLRClone=ClosestCompetitor='+ClosestCompetitor +'===saveWLRClone=KeyWinReasons_Learning====='+KeyWinReasons_Learning); system.debug('==saveWLRClone=oppId='+oppId + '===saveWLRClone=oppId=='+OppSurvey); List<WLR_Clone__c> wlrList = new List<WLR_Clone__c>(); List<SaveWLRCloneWrapper> surveyList= (List<SaveWLRCloneWrapper>)JSON.deserialize(OppSurvey, List<SaveWLRCloneWrapper>.class); List<WLR_Clone__c> wlr_list = [select ID from WLR_Clone__c Where opportunity__c =:oppId]; System.debug('======wlr_list=='+wlr_list);  delete [select ID from WLR_Clone__c Where opportunity__c =:oppId]; For(SaveWLRCloneWrapper survey : surveyList){ System.debug('survey=='+survey); WLR_Clone__c wlr = new WLR_Clone__c(); wlr.Opportunity__c = oppId; wlr.Comments__c = survey.comment; wlr.Reason__c = survey.label; wlr.Closest_Competitor__c = ClosestCompetitor; wlr.Closest_Competitor_Others__c = ClosestCompetitorOthers;  wlr.key_win_reasons_and_any_key_learnings__c = KeyWinReasons_Learning; if(survey.ranking == '0'){ wlr.Ranking__c = ''; } else{ wlr.Ranking__c = survey.ranking; } System.debug('wlr=='+wlr); wlrList.add(wlr); } system.debug('====wlrList======'+wlrList); insert wlrList; return wlrList; } @auraEnabled public static Map<String,String> getWinSurvey1(List<Opportunity> oppList){ System.debug('\n\n --------->>> oppList '+oppList); Map<String,String> reasonRankMap = new Map<String,String>(); List<String> options = new List<String>(); if(oppList != null && oppList.size() > 0) { for(Opportunity opp:oppList) { system.debug('=====opp.SurveyNumber__c======'+opp.SurveyNumber__c); if(opp.SurveyNumber__c == 'Survey1'){ system.debug('=====enter survey 1======'); Schema.DescribeFieldResult fieldResult = Opportunity.Win_Survey_1__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey2') { system.debug('=====enter survey 2======'); Schema.DescribeFieldResult fieldResult = Opportunity.Win_Survey_2__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey3') { system.debug('=====enter survey 3======'); Schema.DescribeFieldResult fieldResult = Opportunity.Win_Survey_3__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey4') { system.debug('=====enter survey 4======'); Schema.DescribeFieldResult fieldResult = Opportunity.Win_Survey_4__c.getDescribe(); system.debug('=======fieldResult======'+fieldResult); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); system.debug('=======pList======'+pList); for (Schema.PicklistEntry p: pList) { system.debug('=======p.getLabel()======='+p.getLabel()); options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey5a') { system.debug('=====enter survey 5a====='); Schema.DescribeFieldResult fieldResult = Opportunity.Loss_Survey_5a__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey5b') { system.debug('=====enter survey 5b======'); Schema.DescribeFieldResult fieldResult = Opportunity.Loss_Survey_5b__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey6') { system.debug('=====enter survey 6======'); Schema.DescribeFieldResult fieldResult = Opportunity.Loss_Survey_6__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey7') { system.debug('=====enter survey 7======'); Schema.DescribeFieldResult fieldResult = Opportunity.Loss_Survey_7__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey8') { system.debug('=====enter survey 8======'); Schema.DescribeFieldResult fieldResult = Opportunity.DroppedByGenpact_Survey8__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } if(opp.SurveyNumber__c == 'Survey9') { system.debug('=====enter survey 9======'); Schema.DescribeFieldResult fieldResult = Opportunity.DroppedByClient_Survey9__c.getDescribe(); List<Schema.PicklistEntry> pList = fieldResult.getPicklistValues(); for (Schema.PicklistEntry p: pList) { options.add(p.getLabel()); } } } } System.debug('options List@@================= '+options); if(oppList != null && oppList.size() > 0) { for(Opportunity opp:oppList) { for(String reason:options){ System.debug('\n\n Reason ::: '+reason); if(opp.Reason_1__c == reason){ System.debug('\n\n Reason ::: '+reason); System.debug('\n\n Reason_1__c ::: '+opp.Reason_1__c); reasonRankMap.put(reason,'1'); } else if(opp.Reason_2__c == reason){ System.debug('\n\n Reason ::: '+reason); System.debug('\n\n Reason_2__c ::: '+opp.Reason_2__c); reasonRankMap.put(reason,'2'); } else if(opp.Reason_3__c == reason){ System.debug('\n\n Reason ::: '+reason); System.debug('\n\n Reason_3__c ::: '+opp.Reason_3__c); reasonRankMap.put(reason,'3'); } else { reasonRankMap.put(reason,'0'); } } } }  system.debug('======reasonRankMap======'+reasonRankMap); return reasonRankMap; }    
    
    public class SaveWLRCloneWrapper
    {        
        String ranking;
        String label;
        String comment;       
    }
}