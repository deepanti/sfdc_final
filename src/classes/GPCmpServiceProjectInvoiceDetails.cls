public class  GPCmpServiceProjectInvoiceDetails {
   
    @AuraEnabled
    public static GPAuraResponse getOracleProjectNumber(Id projectId) {
        GPControllerProjectInvoices invoiceController = new GPControllerProjectInvoices(projectId);
        return invoiceController.getOracleProjectNumber();
    }
    
    @AuraEnabled
    public static GPAuraResponse getProjectInvoiceDetails(Id projectId) {
        GPControllerProjectInvoices invoiceController = new GPControllerProjectInvoices(projectId);
        return invoiceController.pullProjectInvoiceDetails();
    }
    
    @AuraEnabled
    public static GPAuraResponse saveEmailInvoiceRequest(string serializedListOfEmailInvoiceRequest) {
         //List<GP_Invoice_Email_Notification__c> listofEmailNotification = (List<GP_Invoice_Email_Notification__c>) JSON.deserialize(serializedListOfEmailInvoiceRequest, List<GP_Invoice_Email_Notification__c>.class);
        GPControllerProjectInvoices invoiceController = new GPControllerProjectInvoices();
        return invoiceController.sendInvoiceNotification(serializedListOfEmailInvoiceRequest);          
        
    }
    
 
}