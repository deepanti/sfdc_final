public with sharing class InternalExpertSearchExtensionV2 {

    
    
    private String soql {get;set;}
    public String expertname {get;set;}
    public String industryverticalvar{get;set;}
    public String prodfamilyvar {get;set;}
    public String serlinevar {get;set;}
    public String experttypevar{get;set;}
    public String productvar{get;set;}
    public List<Expert__c> experts {get;set;}
    public string geoservedvar{get;set;}
    public string subindustryverticalvar{get;set;}
    public string Officelocvar{get;set;}
    public string Deptvar{get;set;}
    public string Jobtitlevar{get;set;}
    public string exptype{get; set;}
    public string expname{get; set;}
    public string IV{get; set;}
    public string PF{get; set;}
    public string SL{get; set;}
    public string PR{get; set;}
    public string Geo{get; set;}
    public string SIV{get; set;}
   public string expid{get; set;}
   public string Opptyid{get; set;}
   public Expert__c exp{get;set;}
  
   
     public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'DESC'; } return sortDir;  }
    set;
  }

  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'LastModifiedDate '; } return sortField;  }
    set;
  }

  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir ; }
    set;
  }
  
   // init the controller and display some sample data when the page loads
  public InternalExpertSearchExtensionV2 (ApexPages.StandardController controller) {
          
          PF='';
          SL='';
          PR='';
   if (ApexPages.currentPage().getParameters().get('expid') != NULL) 
        {
            expid= ApexPages.currentPage().getParameters().get('expid');
        }
        
        if (ApexPages.currentPage().getParameters().get('Opid') != NULL) 
        {
            Opptyid= ApexPages.currentPage().getParameters().get('Opid');
        }
       
        if (ApexPages.currentPage().getParameters().get('exname') != NULL) 
        {
            expname= ApexPages.currentPage().getParameters().get('exname');
        } 
         if (ApexPages.currentPage().getParameters().get('IVval') != NULL) 
        {
            IV= ApexPages.currentPage().getParameters().get('IVval');
        } 
         if (ApexPages.currentPage().getParameters().get('PFval') != NULL) 
        {
            PF= ApexPages.currentPage().getParameters().get('PFval');
        } 
         if (ApexPages.currentPage().getParameters().get('SLval') != NULL) 
        {
            SL= ApexPages.currentPage().getParameters().get('SLval');
        } System.debug('SL'+ SL);
        if (ApexPages.currentPage().getParameters().get('PRval') != NULL) 
        {
            PR= ApexPages.currentPage().getParameters().get('PRval');
        } 
        if (ApexPages.currentPage().getParameters().get('Geoval') != NULL) 
        {
        Geo= ApexPages.currentPage().getParameters().get('Geoval');
        } 
        
        if (ApexPages.currentPage().getParameters().get('SIVval') != NULL) 
        {
        SIV = ApexPages.currentPage().getParameters().get('SIVval');
        } 
        if (ApexPages.currentPage().getParameters().get('extype') != NULL) 
        {
        exptype= ApexPages.currentPage().getParameters().get('extype');
        } 
        
        PF=SPlitingKey(PF);
        SL=SPlitingKey(SL);
        PR=SPlitingKey(PR);
        
       soql = 'select id,name,Expert_Type__c,Product__c,Industrial_Vertical__c,Product_Family__c,Service_Line__c,Geo_Served__c,Sub_Industry_Vertical__c from Expert__c where  (Expert_Type__c=\'Internal\') AND(Industrial_Vertical__c INCLUDES (\''+ IV +'\')  OR Service_Line__c INCLUDES('+ SL +') OR Product_Family__c INCLUDES('+ PF +') OR Product__c INCLUDES('+ PR +')) and Designated_Expert__c=\'Yes\'';
     System.debug('SLAfter'+ soql);
     runQuery();
  }
  public void toggleSort() {
    // simply toggle the direction 
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }

  
   public void runQuery() {
     
       System.Debug('SOQL: ' + soql);
      
    try{ 
          
       experts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' LIMIT 900');
       
       
       
  
    }catch(DmlException e){
        System.debug('The following exception has occurred: ' + e.getMessage());
    }

    
    }
     
 
  
  public PageReference reset() {
        PageReference pg = new PageReference(System.currentPageReference().getURL());
        pg.setRedirect(true);
        return pg;
    }

       public PageReference runSearch() {
       
       System.debug('Internal expert' + industryverticalvar);
       
      exp = new Expert__c();
      System.debug('Expert name ' + expertname);
    soql = 'select id,name,Expert_Type__c,Product__c,Industrial_Vertical__c,Product_Family__c,Service_Line__c,Geo_Served__c,Geo_served_Internal__c,Sub_Industry_Vertical__c from Expert__c where id!=null and Designated_Expert__c=\'Yes\'';
        
      if (!expertname.equals(''))
      soql += ' and Name LIKE '+ '\'%'+String.escapeSingleQuotes(expertname)+'%' + '\'';
  
     industryverticalvar=industryverticalvar.substring(1,industryverticalvar.length()-1); 
     system.debug('Verticals' + industryverticalvar+'Final');
     if ( !string.isEmpty(industryverticalvar))
     {   
         
         
       string strng  =SPlitingKey(industryverticalvar); 
       
           soql += ' and Industrial_Vertical__c includes ('+ strng  +')';
     }
     
     serlinevar=serlinevar.substring(1,serlinevar.length()-1);
     system.debug('SL' + serlinevar);
     if ( !string.isempty(serlinevar))
     { 
         
         string strng  =SPlitingKey(serlinevar); 
                    
         soql += ' and Service_Line__c  includes ('+ strng  +')';
     }
     
     prodfamilyvar=prodfamilyvar.substring(1,prodfamilyvar.length()-1);
     if ( !string.isempty(prodfamilyvar))
     {    
              
             string strng  =SPlitingKey(prodfamilyvar); 
                    
          soql += ' and Product_Family__c includes ('+ strng +')';
     }
     
      //if (!experttypevar.equals('none'))
      //soql += ' and Expert_Type__c LIKE '+ '\'%'+String.escapeSingleQuotes(experttypevar)+'%' + '\'';
      
      /*productvar=productvar.substring(1,productvar.length()-1);
      if (!string.isempty(productvar))
      {
              
          string strng  =SPlitingKey(productvar);
                            
          soql += ' and Product__c includes ('+ strng +')';
      }*/
      
      geoservedvar=geoservedvar.substring(1,geoservedvar.length()-1);
      if (!string.isempty(geoservedvar))
      {
              
          string strng  =SPlitingKey(geoservedvar);
                        
          soql += ' and Geo_served_Internal__c Includes('+ strng + ')';
      }
      
      
      if ( !subindustryverticalvar.equals('none'))
       {    
              //string strng  =SPlitingKey(subindustryverticalvar);
                        
          soql += ' and Sub_Industry_Vertical__c LIKE '+ '\'%'+String.escapeSingleQuotes(subindustryverticalvar)+'%' + '\'';
       }
       
       if ( !Officelocvar.equals('none'))
      {    
              //string strng  =SPlitingKey(subindustryverticalvar);
                        
          soql += ' and Office_Location__c LIKE '+ '\'%'+String.escapeSingleQuotes(Officelocvar)+'%' + '\'';
       }
       
       if ( !Deptvar.equals('none'))
      {    
              //string strng  =SPlitingKey(subindustryverticalvar);
                        
          soql += ' and Department__c LIKE '+ '\'%'+String.escapeSingleQuotes(Deptvar)+'%' + '\'';
       }
       
       if ( !Jobtitlevar.equals('none'))
      {    
              //string strng  =SPlitingKey(subindustryverticalvar);
                        
          soql += ' and Title__c LIKE '+ '\'%'+String.escapeSingleQuotes(Jobtitlevar)+'%' + '\'';
       } 
        
      runQuery();

    return null;
  }

 public List<selectOption> serviceLine{
    get {
      if (serviceLine== null) {

        serviceLine= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Service_Line__c.getDescribe();
        serviceLine.add(new selectOption('', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          serviceLine.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return serviceLine;          
    }
    set;
  }
  public List<selectOption> industryvertical {
    get {
      if (industryvertical == null) {

        industryvertical = new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Industrial_Vertical__c.getDescribe();
        industryvertical.add(new selectOption('', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          industryvertical.add(new selectOption(f.getvalue(),f.getLabel()));

      }
      return industryvertical;          
    }
    set;
  }
  
  public List<selectOption> prodfamily{
    get {
      if (prodfamily == null) {

        prodfamily = new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Product_Family__c.getDescribe();
        prodfamily.add(new selectOption('', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          prodfamily.add(new selectOption(f.getvalue(),f.getlabel()));

      }
      return prodfamily;          
    }
    set;
  }
  public List<selectOption> expertType{
    get {
      if (expertType== null) {

        expertType= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Expert_Type__c.getDescribe();
        expertType.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues()) {
          expertType.add(new selectOption(f.getValue(),f.getLabel()));

      }
      }
      return expertType;          
    }
    set;
  } 
  
   
   public List<selectOption> product{
    get {
      if (product== null) {

        product= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Product__c.getDescribe();
        product.add(new selectOption('', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          product.add(new selectOption(f.getvalue(),f.getLabel()));

      }
      return product;          
    }
    set;
  }
  public List<selectOption> geoserved{
    get {
      if (geoserved== null) {

        geoserved= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Geo_served_Internal__c.getDescribe();
        geoserved.add(new selectOption('', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          geoserved.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return geoserved;          
    }
    set;
    }
    
    public List<selectOption> subindustryvertical{
    get {
      if(subindustryvertical== null) {

        subindustryvertical= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Sub_Industry_Vertical__c.getDescribe();
        subindustryvertical.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          subindustryvertical.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return subindustryvertical;          
    }
    set;
    }
    
        public List<selectOption> DepartmentVal{
    get {
      if(DepartmentVal== null) {

        DepartmentVal= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Department__c.getDescribe();
        DepartmentVal.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          DepartmentVal.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return DepartmentVal;          
    }
    set;
    }
    
        public List<selectOption> OfficeLocaVal{
    get {
      if(OfficeLocaVal== null) {

        OfficeLocaVal= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Office_Location__c.getDescribe();
        OfficeLocaVal.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          OfficeLocaVal.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return OfficeLocaVal;          
    }
    set;
    }
    
        public List<selectOption> JobTitleVal{
    get {
      if(JobTitleVal== null) {

        JobTitleVal= new List<selectOption>();
        Schema.DescribeFieldResult field = Expert__c.Title__c.getDescribe();
        JobTitleVal.add(new selectOption('none', ''));

        for (Schema.PicklistEntry f : field.getPicklistValues())
          JobTitleVal.add(new selectOption(f.getvalue(),f.getLabel()));    

      }
      return JobTitleVal;          
    }
    set;
    }
  
  
  Public string SPlitingKey (String key)
{
            String strng='';       
                if(key!= ''){       
                    List<String> Splitstr = key.split(',');
                        for(String str : Splitstr)
                        {
                                String c;
                                
                          c = '\'' + str + '\'' + ',';
                                strng = strng +c;
                        
                        }
                        
                        
                strng=strng.substring(0,(strng.length()-1));
                
                
                }
                else
                {
                    strng='\'None\'';
                }
                System.debug('Where clause' + strng); 
                return strng;
                
                
}   
  
  
  }