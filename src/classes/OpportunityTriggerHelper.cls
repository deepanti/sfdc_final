public class OpportunityTriggerHelper {
   
	static List<Contact_Role_Rv__c> updated_contact_role_list;
    static Set<Contact_Role_Rv__c> new_contact_role_list;
    
    Private static Double Discover_age_retail;
    Private static Double Discover_age_large;
    Private static Double Discover_age_hun_sole_retail_non_ts;
    Private static Double Discover_age_hun_sole_medium_non_ts;
    Private static Double Discover_age_hun_sole_large_non_ts;
    Private static Double Discover_age_hun_comp_retail_non_ts;
    Private static Double Discover_age_hun_comp_medium_non_ts;
    Private static Double Discover_age_hun_comp_large_non_ts;
    Private static Double Discover_age_min_sole_retail_non_ts;
    Private static Double Discover_age_min_sole_medium_non_ts;
    Private static Double Discover_age_min_sole_large_non_ts;
    Private static Double Discover_age_min_comp_retail_non_ts;
    Private static Double Discover_age_min_comp_medium_non_ts;
    Private static Double Discover_age_min_comp_large_non_ts;
    Private static Double Define_age_hun_sole_retail_non_ts;
    Private static Double Define_age_hun_sole_medium_non_ts;
    Private static Double Define_age_hun_sole_large_non_ts;
    Private static Double Define_age_hun_comp_retail_non_ts;
    Private static Double Define_age_hun_comp_medium_non_ts;
    Private static Double Define_age_hun_comp_large_non_ts;
    Private static Double Define_age_min_sole_retail_non_ts;
    Private static Double Define_age_min_sole_medium_non_ts;
    Private static Double Define_age_min_sole_large_non_ts;
    Private static Double Define_age_min_comp_retail_non_ts;
    Private static Double Define_age_min_comp_medium_non_ts;
    Private static Double Define_age_min_comp_large_non_ts;       
    Private static double Define_age_retail;
    Private static double Define_age_large;
    
    //Used to insert new contact role while creation of discover opportunity
    public static void insertContactRole(List<Opportunity> opportunity_list){
        try{
            new_contact_role_list = new Set<Contact_Role_Rv__c>();
            
            for(Opportunity opp : opportunity_list){
               if(opp.contact1__c != NULL){
                    new_contact_role_list.add(getContactRole(opp));
                }
            }
            if(new_contact_role_list.size()>0){
            	insert new List<Contact_Role_Rv__c>(new_contact_role_list);      
            }             
        }
        catch(Exception e){
            system.debug('ERROR=='+e.getMessage()+'=='+e.getLineNumber());
        }        
    }
    
    //This method is used to update contact role while updation of discover opportunity
    public static void updateContactRole(List<Opportunity> updated_opportunity_list, Map<ID,Opportunity> old_opportunity_Map){
        try{
            updated_contact_role_list = new List<Contact_Role_Rv__c>();
            List<ID> contactIds = new List<ID>();
            For(Opportunity opp : updated_opportunity_list){
            	contactIds.add(opp.Contact1__c);    
            }
            System.debug('updated_opportunity_list=='+updated_opportunity_list);
            System.debug('old_opportunity_Map=='+old_opportunity_Map);
            new_contact_role_list = new Set<Contact_Role_Rv__c>();
            
            //Get all the contact roles associated with opportunity of record type discover.
                List<Contact_Role_RV__c> old_contact_roles = [SELECT Id, Contact__c, IsPrimary__c, Opportunity__c, Role__c 
                                                              FROM Contact_Role_Rv__c WHERE 
                                                              opportunity__c IN :old_opportunity_Map.keySet() AND Contact__c IN :contactIds];
                system.debug('old_contact_roles=='+old_contact_roles);
           		system.debug('updated_opportunity_list=='+updated_opportunity_list.size());
            	for(Opportunity updated_opportunity : updated_opportunity_list){
                    boolean isExist =false;
                  if(old_contact_roles.size()>0){
                    for(Contact_Role_RV__c contact_role : old_contact_roles){
                        System.debug('run once=='+contact_role.ID +'=='+updated_opportunity.Id);
                        if(updated_opportunity.ID == contact_role.Opportunity__c && contact_role.Contact__c == updated_opportunity.Contact1__c
                          ){
                            System.debug('in iff===');
                              isExist = true;
                        }
                        else if(updated_opportunity.ID == contact_role.Opportunity__c && contact_role.Contact__c == updated_opportunity.Contact1__c
                               && contact_role.Role__c != updated_opportunity.Role__c){
                            contact_role.Role__c = updated_opportunity.role__c;contact_role.IsPrimary__c = true;  updated_contact_role_list.add(contact_role);
                            isExist = true;
                            break;
                        }              
                    }
                      if(!isExist){
                          if(updated_opportunity.contact1__c != null){
                              Contact_Role_Rv__c new_contact_role = new Contact_Role_Rv__c();
                              new_contact_role.Contact__c = updated_opportunity.contact1__c;new_contact_role.Role__c = updated_opportunity.role__c; new_contact_role.IsPrimary__c = true;  new_contact_role.Opportunity__c = updated_opportunity.ID; new_contact_role_list.add(new_contact_role); 
                          }
                      }
                }
            else{
                System.debug('new contact role==');
                	if(updated_opportunity.contact1__c != null){
                        Contact_Role_Rv__c contact_role = new Contact_Role_Rv__c();
                        contact_role.Contact__c = updated_opportunity.contact1__c;
                        contact_role.Role__c = updated_opportunity.role__c;
                        contact_role.IsPrimary__c = true;
                        contact_role.Opportunity__c = updated_opportunity.ID;
                        new_contact_role_list.add(contact_role);     
                	}
                }   
            }
            if(new_contact_role_list.size() >= 0){
                System.debug('new_contact_role_list=='+new_contact_role_list);
            	insert new List<Contact_Role_Rv__c>(new_contact_role_list);     
            }
            if(updated_contact_role_list.size()>0){ update updated_contact_role_list;
            }
                    
        }
        catch(Exception e){
            system.debug('ERROR=='+e.getMessage()+'=='+e.getLineNumber());
        }     
        
    }
    
    //This method is used to delete contact role while deletion of discover opportunity
    public static void deleteContactRole(Map<ID, Opportunity> deleted_opportunity_map){
        try{
            List<Contact_Role_RV__c> to_be_deleted_contact_roles = [SELECT Id, Contact__c, IsPrimary__c, Opportunity__c, Role__c 
                                                                    FROM Contact_Role_Rv__c WHERE opportunity__c IN :deleted_opportunity_map.keySet()];	
            if(to_be_deleted_contact_roles.size()>0){delete to_be_deleted_contact_roles;     
            }
              
        }
        catch(Exception e){
            system.debug('ERROR=='+e.getMessage()+'=='+e.getLineNumber());
        }        
    }
    
    public static Contact_Role_RV__c getContactRole(Opportunity opp){
        Contact_Role_Rv__c contact_role = new Contact_Role_Rv__c();
        contact_role.Contact__c = opp.Contact1__c;
        contact_role.role__c = opp.role__c;
        contact_role.IsPrimary__c = true;
        contact_role.opportunity__c = opp.Id; 
        return contact_role;
    }
    
    public static void opportunityRestrictMethod(list<opportunity> oppOLdList){
        ID PID= userinfo.getProfileId();
       
            for (Opportunity opp: oppOLdList) 
            {
                if (opp.Opportunity_ID__c != null && PID<>'00e9000000125i2' && PID<>'00e90000001aFVI'  )
                { 
                    opp.addError('You cannot delete an Opportunity.Please contact your Salesforce.com Administrator for assistance.');
                }
            } 
        }

    
    
    public static void UpdatePrediscoverMethod(list<opportunity> oppList,map<id,opportunity> oldOppMap)
    {
        if(!singleexecuion.bool_GENT_OpportunityUpdatePrediscover)
        {
            singleexecuion.bool_GENT_OpportunityUpdatePrediscover=true;
            
            for(Opportunity TempObj : oppList)
            {
                if((TempObj.stageName!='Prediscover' || TempObj.stageName!='6. Signed Deal' || TempObj.stageName !='7. Lost' || TempObj.stageName!='8. Dropped') && TempObj.Nature_of_Work_highest__c =='Consulting' )
                {
                    if(oldOppMap.get(TempObj.id).stageName != TempObj.stageName && TempObj.Transformation_stale_deal_check__c == true && (TempObj.stageName=='1. Discover' || TempObj.stageName=='2. Define' || TempObj.stageName=='3. On Bid' || TempObj.stageName=='4. Down Select' ||TempObj.stageName=='5. Confirmed'))
                    {
                        TempObj.Transformation_stale_deal_timestamp__c=System.Today();
                        TempObj.Transformation_stale_deal_check__c=false;
                    } 
                }
            }   
        }
    }
    
    public static void dateUpdateMethod(list<opportunity> newOppList,Map<id,opportunity> oldOppMap){
        boolean runME = true;
        HelpClass.Rec=false;
        System.debug('1.Number of Queries used in this apex code so far: ' + Limits.getQueries());
        list<OpportunityLineItem> OpportunityProductupdate=new list<OpportunityLineItem>();
        list<OpportunityLineItem> OpportunityProducts=new list<OpportunityLineItem>();
        
        Set<Id> idset= oldOppMap.keySet();
        map<id,opportunity> oppMap = new map<id,opportunity>();
        Integer dueDate;
        Boolean recursiveflaf = CheckRecursiveForOLI.runOnceAfter_1();
        system.debug(':---recursiveflaf---:'+recursiveflaf);
        if(true) {   
            try{
                for(opportunity opportunityobj : newOppList) {
                    system.debug(':----dataupdatenewlist---:'+opportunityobj.closeDate);
                    system.debug(':----dataupdatenewlist---:'+oldOppMap.get(opportunityobj.id).closeDate);
                    opportunity opptyofoldmap=oldOppMap.get(opportunityobj.id);
                    if(opptyofoldmap.closeDate!=null&opptyofoldmap.closeDate!=opportunityobj.closeDate || Test.isRunningTest())  {      
                        dueDate=opptyofoldmap.closeDate.daysBetween(opportunityobj.closeDate); 
                        if(dueDate!=0 && dueDate > 0 || Test.isRunningTest()) {
                            oppMap.put(opportunityobj.id,opportunityobj); 
                        }
                    } 
                }
                if(oppMap.size() > 0){
                    list<OpportunityLineItem> newopp=[select id,Revenue_Start_Date__c,Opportunityid, UnitPrice, Revenue_Exchange_Rate__c, 
                                                      TCV__c, contract_term__c, CYR__c, NYR__c from OpportunityLineItem where Opportunityid=:oppMap.keySet()];// AND Revenue_Start_Date__c < :opportunityobj.closeDate];
                    
                    for(OpportunityLineItem OpportunityProductobject : newopp) {
                        if(OpportunityProductobject.Revenue_Start_Date__c < oppMap.get(OpportunityProductobject.OpportunityId).closeDate)
                            OpportunityProductobject.Revenue_Start_Date__c = OpportunityProductobject.Revenue_Start_Date__c+dueDate;
                        OpportunityProductupdate.add(OpportunityProductobject);
                    } 
                }
            }
            catch(exception e)
            {
                system.debug('ERROR=='+e.getLineNumber()+' == '+e.getMessage()+'=='+e.getStackTraceString());
            }
            
            
            if(OpportunityProductupdate.size()>0)
            {
                try
                {
                    Delete [Select ID FROM OpportunityLineItemSchedule WHERE OpportunityLineItemId =:OpportunityProductupdate];
                    for(OpportunityLineItem oppLineItem : OpportunityProductupdate){
                        if(oppLineItem.Revenue_Exchange_Rate__c != null){
                            oppLineItem.UnitPrice = (oppLineItem.TCV__c / oppLineItem.Revenue_Exchange_Rate__c).setScale(2, RoundingMode.HALF_UP);          
                        }
                        else{
                            oppLineItem.UnitPrice = (oppLineItem.TCV__c).setScale(2, RoundingMode.HALF_UP);            
                        }
                        OpportunityProducts.add(oppLineItem);
                        
                    }  
                    System.debug('OpportunityProducts=='+OpportunityProducts);
                    update OpportunityProducts;
                    AddproductController.createRevenueSchedule(OpportunityProducts);
                }
                catch(exception e)
                {
                    system.debug('error has occured due to=='+e.getLineNumber()+' == '+e.getMessage()+'=='+e.getStackTraceString());
                }
            }
        }
    }
    
    public static void saveDealCycleAging(Set<ID> oppIDs){
       getDealCycleAging();
        List<Opportunity> oppList = [SELECT ID, Nature_of_Work_highest__c, StageName, Type_of_deal__c, deal_cycle_ageing__c,
                                    Hunting_Mining__c, Formula_Hunting_Mining__c, Type_of_deal_for_non_ts__c, Deal_Type__c FROM Opportunity WHERE ID IN :oppIDs];
       System.debug('oppList in cycle==='+oppList);
        List<Opportunity> opportunityList = new List<Opportunity>();
        for(Opportunity opp : oppList){
            //For TS Deals
            system.debug('opp values=='+opp.StageName ==+',,,'+opp.Type_of_deal__c);
            if(opp.Nature_of_Work_highest__c == 'Consulting' || opp.Nature_of_Work_highest__c == 'Digital' 
               || opp.Nature_of_Work_highest__c ==  'Analytics'){
            	if(opp.StageName == '1. Discover'){
                    if(opp.Type_of_deal__c == 'Retail'){
                    	opp.deal_cycle_ageing__c = Discover_age_retail;    
                    }	   
                    else if(opp.Type_of_deal__c == 'Large'){
                    	opp.deal_cycle_ageing__c = Discover_age_large;     
                    }
                		
                }
                else if(opp.StageName == '2. Define'){
                    if(opp.Type_of_deal__c == 'Retail'){
                    	opp.deal_cycle_ageing__c = Define_age_retail;    
                    }	
                    else if(opp.Type_of_deal__c == 'Large'){
                    	opp.deal_cycle_ageing__c = Define_age_large;     
                    }	     
                }
            }
            //For NON TS Deals
            else if(opp.Nature_of_Work_highest__c == 'IT Services' || opp.Nature_of_Work_highest__c == 'Managed Services'){
            	if(opp.StageName == '1. Discover'){
                    if(opp.Formula_Hunting_Mining__c=='Hunting'){
                        if(opp.Deal_Type__c=='Sole Sourced'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_sole_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_sole_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_sole_large_non_ts;
                            }
                        }
                        else if(opp.Deal_Type__c=='Competitive'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_comp_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_comp_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){
                                opp.deal_cycle_ageing__c = Discover_age_hun_comp_large_non_ts;
                            }
                        }
                    }
                    else if(opp.Formula_Hunting_Mining__c=='Mining'){
                    	if(opp.Deal_Type__c=='Sole Sourced'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){
                                opp.deal_cycle_ageing__c = Discover_age_min_sole_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){
                                opp.deal_cycle_ageing__c = Discover_age_min_sole_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){
                                opp.deal_cycle_ageing__c = Discover_age_min_sole_large_non_ts;
                            }
                        }
                        else if(opp.Deal_Type__c=='Competitive'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){
                                opp.deal_cycle_ageing__c = Discover_age_min_comp_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){
                                opp.deal_cycle_ageing__c = Discover_age_min_comp_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){
                                opp.deal_cycle_ageing__c = Discover_age_min_comp_large_non_ts;
                            }
                        }    
                    }                    
                }
                //For NOn TS
                else if(opp.StageName == '2. Define'){
                	if(opp.Hunting_Mining__c=='Hunting'){
                        if(opp.Deal_Type__c=='Sole Sourced'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){   system.debug('Define_age_hun_sole_retail_non_ts =='+Define_age_hun_sole_retail_non_ts);  opp.deal_cycle_ageing__c = Define_age_hun_sole_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){  system.debug('Define_age_hun_sole_retail_non_ts =='+Define_age_hun_sole_retail_non_ts); opp.deal_cycle_ageing__c = Define_age_hun_sole_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){ system.debug('Define_age_hun_sole_large_non_ts =='+Define_age_hun_sole_large_non_ts);  opp.deal_cycle_ageing__c = Define_age_hun_sole_large_non_ts;
                            }
                        }
                        else if(opp.Deal_Type__c=='Competitive'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){  system.debug('Define_age_hun_comp_retail_non_ts =='+Define_age_hun_comp_retail_non_ts);   opp.deal_cycle_ageing__c = Define_age_hun_comp_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){  system.debug('Define_age_hun_comp_medium_non_ts =='+Define_age_hun_comp_medium_non_ts);  opp.deal_cycle_ageing__c = Define_age_hun_comp_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){  system.debug('Define_age_hun_comp_large_non_ts =='+Define_age_hun_comp_large_non_ts);  opp.deal_cycle_ageing__c = Define_age_hun_comp_large_non_ts;
                            }
                        }
                    }
                    else if(opp.Hunting_Mining__c=='Mining'){
                    	if(opp.Deal_Type__c=='Sole Sourced'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){  system.debug('Define_age_min_sole_retail_non_ts =='+Define_age_min_sole_retail_non_ts); opp.deal_cycle_ageing__c = Define_age_min_sole_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){  system.debug('Define_age_min_sole_medium_non_ts =='+Define_age_min_sole_medium_non_ts); opp.deal_cycle_ageing__c = Define_age_min_sole_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){  system.debug('Define_age_min_sole_large_non_ts =='+Define_age_min_sole_large_non_ts); opp.deal_cycle_ageing__c = Define_age_min_sole_large_non_ts;
                            }
                        }
                        else if(opp.Deal_Type__c=='Competitive'){
                            if(opp.Type_of_deal_for_non_ts__c=='Retail'){ system.debug('Define_age_min_comp_retail_non_ts =='+Define_age_min_comp_retail_non_ts); opp.deal_cycle_ageing__c = Define_age_min_comp_retail_non_ts; 
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Medium'){ system.debug('Define_age_min_comp_medium_non_ts =='+Define_age_min_comp_medium_non_ts);  opp.deal_cycle_ageing__c = Define_age_min_comp_medium_non_ts;
                            }
                            else if(opp.Type_of_deal_for_non_ts__c=='Large'){ system.debug('Define_age_min_comp_large_non_ts =='+Define_age_min_comp_large_non_ts);  opp.deal_cycle_ageing__c = Define_age_min_comp_large_non_ts;
                            }
                        }                         
                    }                        
                }
            }
            opportunityList.add(opp);
        }
       update opportunityList;
    }
    
    private static void getDealCycleAging(){
         List<Deal_Cycle__c> DC=[Select Stage__c,Ageing__c,Type_of_Deal__c,Type_of_deal_for_non_ts__c,
                                 Transformation_Deal__c,Hunting_Mining__c,Nature_Of_Work__c,Deal_Type__c,
                                 Average_cycle_time__c,Cycle_Time__c from Deal_Cycle__c  ];
        
         For(Deal_Cycle__c deal_cycle : DC)
        {
            if(deal_cycle.Stage__c=='1. Discover' )
            {
                if(deal_cycle.Transformation_Deal__c=='Yes'){
                    if(deal_cycle.Type_of_Deal__c=='Retail'){
                    	Discover_age_retail =   deal_cycle.Ageing__c;    
                    }
                    else if(deal_cycle.Type_of_Deal__c=='Large'){
                    	Discover_age_large =   deal_cycle.Ageing__c;    
                    }
                }
                else if(deal_cycle.Hunting_Mining__c=='Hunting'){
                    if(deal_cycle.Deal_Type__c=='Sole Sourced'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Discover_age_hun_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Discover_age_hun_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Discover_age_hun_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                    else if(deal_cycle.Deal_Type__c=='Competitive'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Discover_age_hun_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Discover_age_hun_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Discover_age_hun_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                }
                else if(deal_cycle.Hunting_Mining__c=='Mining'){
                    if(deal_cycle.Deal_Type__c=='Sole Sourced'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Discover_age_min_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Discover_age_min_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Discover_age_min_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                    else if(deal_cycle.Deal_Type__c=='Competitive'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Discover_age_min_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Discover_age_min_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Discover_age_min_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                }
            }
			else if(deal_cycle.Stage__c=='2. Define')
            {
                if(deal_cycle.Transformation_Deal__c=='Yes'){
                    if(deal_cycle.Type_of_Deal__c=='Retail'){
                    	Define_age_retail =   deal_cycle.Ageing__c;    
                    }
                    else if(deal_cycle.Type_of_Deal__c=='Large'){
                    	Define_age_large =   deal_cycle.Ageing__c;    
                    }
                }
                else if(deal_cycle.Hunting_Mining__c=='Hunting'){
                    if(deal_cycle.Deal_Type__c=='Sole Sourced'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Define_age_hun_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Define_age_hun_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Define_age_hun_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                    else if(deal_cycle.Deal_Type__c=='Competitive'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Define_age_hun_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Define_age_hun_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Define_age_hun_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                }
                else if(deal_cycle.Hunting_Mining__c=='Mining'){
                    if(deal_cycle.Deal_Type__c=='Sole Sourced'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Define_age_min_sole_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Define_age_min_sole_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Define_age_min_sole_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                    else if(deal_cycle.Deal_Type__c=='Competitive'){
                        if(deal_cycle.Type_of_deal_for_non_ts__c=='Retail'){
                        	 Define_age_min_comp_retail_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Medium'){
                        	Define_age_min_comp_medium_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                        else if(deal_cycle.Type_of_deal_for_non_ts__c=='Large'){
                        	Define_age_min_comp_large_non_ts = (deal_cycle.Average_cycle_time__c/2).round(System.RoundingMode.CEILING);    
                        }
                    }
                }
            }
    	}
	}
}