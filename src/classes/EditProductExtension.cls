public class EditProductExtension {
	public static OpportunityLineItem OLI{get;set;}
    public static ID opportunity_ID{get;set;}
    public static Boolean showError{get;set;}
    public static String errorMessage {get;set;}
    
    public EditProductExtension(ApexPages.StandardController controller){
         system.debug('record ID==='+controller.getRecord().Id);
        OLI = [SELECT OpportunityID, Opportunity.ownerId, opportunity.stageName,opportunity.product_Count__c, Product_BD_Rep__c, QSRM_status__c FROM OpportunityLineItem WHERE ID =:controller.getRecord().Id];
        opportunity_ID = OLI.opportunityID;
        ID loggedInUserID = UserInfo.getUserId();
        
        if(loggedInUserID != OLI.Opportunity.ownerId && loggedInUserID != OLI.Product_BD_Rep__c){
        	showError = true;    
            errorMessage = 'You are not authorized to delete the product.';
        }
        else{
            system.debug('OLI.QSRM_Status__c=='+OLI.QSRM_Status__c);
            System.debug('OLI.opportunity.product_Count__c =='+OLI.opportunity.product_Count__c );
            if((OLI.QSRM_Status__c!='Your QSRM is submitted for Approval' && OLI.QSRM_Status__c != 'Submitted') && (OLI.opportunity.StageName =='Prediscover' || OLI.opportunity.StageName =='1. Discover' || OLI.opportunity.StageName =='2. Define' || OLI.opportunity.StageName =='3. On Bid' ||OLI.opportunity.StageName =='4. Down Select')){
                    if(OLI.opportunity.product_Count__c ==1 && (OLI.opportunity.StageName =='2. Define' || OLI.opportunity.StageName =='3. On Bid' ||OLI.opportunity.StageName =='4. Down Select')){ showError=true; errorMessage = 'Atleast One Product is Mandatory.';	    
                    }
                    else{
                    	System.debug('opp stage=='+OLI.opportunity.StageName);
                		showError = false;    
                    }
                    
            } 
            else{ 
                System.debug('in else==='); 
                showError=true; 
                if(OLI.QSRM_Status__c=='Your QSRM is submitted for Approval' || OLI.QSRM_Status__c == 'Submitted'){
                	errorMessage = 'Product deletion is not allowed when QSRM is in approval. Please come back once QSRM is approved.';     
                }
                if(OLI.opportunity.StageName =='5. Confirmed' || OLI.opportunity.StageName =='6. Signed Deal' 
                   || OLI.opportunity.StageName =='7. Lost' || OLI.opportunity.StageName =='8. Dropped' ){
                    errorMessage = 'Product deletion is not allowed at Confirmed stage. Please contact SFDC helpdesk.';
                }
                
            }
        }
        User loggedInUser = [SELECT ID, Name, Profile.Name FROM USER WHERE ID =: UserInfo.getUserId()];
            if(loggedInUser.Profile.Name == 'Genpact Super Admin'){
                showError = false;    
        }   
        System.debug('showError=='+showError);
    }
    
    public static PageReference deleteOppProduct(){
        try{
            if(showError == false){
                OpportunityLineItem oppProduct = [SELECT ID FROM OpportunityLineItem WHERE ID = :OLI.ID];
                system.debug('oli=='+oppProduct);
                DELETE oppProduct;
                List<OpportunityLineItem> oli_list = [Select ID, OpportunityID FROM OpportunityLineItem Where OpportunityID =: opportunity_ID ];
                OpportunityLineItemTriggerHelper.setContractTermField(oli_list); PageReference pf = new PageReference('/'+opportunity_ID); pf.setRedirect(true); return pf;      } } catch(Exception e){ System.debug('ERROR==='+e.getMessage()+ '' +e.getLineNumber()); }  return null;
    }
}