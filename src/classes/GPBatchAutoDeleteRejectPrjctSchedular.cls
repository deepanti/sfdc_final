//Description : Schedular Class for GPBatchAutoDeleteRejectProject
global class GPBatchAutoDeleteRejectPrjctSchedular implements schedulable {
    global void execute(SchedulableContext sc) {
        GPBatchAutoDeleteRejectProject objGBAutoDltRejectPrj = new GPBatchAutoDeleteRejectProject(); //ur batch class
        database.executebatch(objGBAutoDltRejectPrj, 200);
    }
}