public class GPSelectorProjectBudget extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project_Budget__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Budget__c.sObjectType;
    }

    public List < GP_Project_Budget__c > selectById(Set < ID > idSet) {
        return (List < GP_Project_Budget__c > ) selectSObjectsById(idSet);
    }

    public GP_Project_Budget__c selectProjectBudgetRecord(Id projectBudgetId) {
        return [Select Id,GP_Project__c from GP_Project_Budget__c where Id =: projectBudgetId];
    }

    public List < GP_Project_Budget__c > selectProjectBudgetRecords(Id projectId) {
        return [SELECT Id,
            GP_Track__c, GP_Total_Cost__c, GP_Project__c, GP_OMS_Id__c, GP_Budget_Master__c, GP_Parent_Project_Budget__c, GP_Man_Days__c,
            GP_Band__c, GP_Country__c, GP_Product__c, GP_Effort_Hours__c, GP_TCV__c, GP_Data_Source__c
            FROM GP_Project_Budget__c
            where GP_Project__c =: projectId and GP_IsSoftDeleted__c = false
            ORDER By GP_Data_Source__c asc Nulls last
        ];
    }
    
    public List < GP_Project_Budget__c > selectProjectBudgetRecords(Set < Id > lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Project_Budget__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Budget__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }
}