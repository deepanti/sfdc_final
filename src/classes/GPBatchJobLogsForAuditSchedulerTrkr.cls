//AvinashN
@isTest(seealldata=true)
public class GPBatchJobLogsForAuditSchedulerTrkr {

    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void testScheduledJob()
    {
        Test.startTest();
        System.schedule('ScheduledApexTest',
                        CRON_EXP, 
                        new GPBatchJobLogsForAuditScheduler());  
        Test.stopTest();
    }
       
}