public class UploadAttachmentController {
    
    public String selectedType {get;set;}
    public Boolean selectedAwesomeness {get;set;}
    public String description {get;set;}
    private Contract Contract {get;set;} 
    public String fileName {get;set;}
    public transient Blob fileBody {get;set;}
    
    
    public UploadAttachmentController(ApexPages.StandardController controller) { 
        this.Contract = (Contract)controller.getRecord();
    }   
    
    // creates a new Attachment_Contract__c record
    private Database.SaveResult saveCustomAttachment() {
        Attachment_Contract__c obj = new Attachment_Contract__c();
        obj.Contract__c = Contract.Id; 
        obj.description__c = description;
        //obj.type__c = selectedType;
        //obj.awesome__c = selectedAwesomeness;
        // fill out cust obj fields
        return Database.insert(obj);
    }
    
    // create an actual Attachment record with the Attachment_Contract__c as parent
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = this.fileName;
        attachment.parentId = parentId;
        
        
        // inser the attahcment
        result = Database.insert(attachment);
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        attachment.body=null;
        return result;
    }
    
    /**
    * Upload process is:
    *  1. Insert new Attachment_Contract__c record
    *  2. Insert new Attachment with the new Attachment_Contract__c record as parent
    *  3. Update the Attachment_Contract__c record with the ID of the new Attachment
    **/
    public PageReference processUpload() {
           
           try {
                    Database.SaveResult customAttachmentResult; 
                    if(this.fileName==''|| this.fileName==null )
                       {
                           customAttachmentResult=null;

                       }
                    else
                    {    customAttachmentResult = saveCustomAttachment();
                                            
                    }
                        if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                              'Could not save attachment.'));
                            return null;
                        }
                    
                        Database.SaveResult attachmentResult = saveStandardAttachment(customAttachmentResult.getId());
                    
                        if (attachmentResult == null || !attachmentResult.isSuccess()) {
                            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                              'Could not save attachment.'));            
                            return null;
                        } else {
                            // update the custom attachment record with some attachment info
                            Attachment_Contract__c customAttachment = [select id from Attachment_Contract__c where id = :customAttachmentResult.getId()];
                            customAttachment.File_Name__c = this.fileName;
                            customAttachment.Attachment_ID__c = attachmentResult.getId();
                            update customAttachment;
                        }
                    
                     
            }
        catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        
        return new PageReference('/'+Contract.Id);
    }
    
    public PageReference back() {
        return new PageReference('/'+Contract.Id);
    }  
    
    
       

}