/*
    @Description: Class will be used for handling all Opportunity Line Item which are IT SERVICE type
    @Test Class: OLIITServiceCtlrTest
    @Author: Vikas Rajput
*/
public class VIC_OLIITServiceCtlr{
    String strPending = 'Pending';
    String strNotRequired = 'Not Required'; 
    String strEnterpriseSales = 'Enterprise sales';
    String strITO = 'ITO';
    String strCM = 'CM';
    public void serviceManagerForIT(List<OpportunityLineItem> lstNONServiceOpportunityLineItemARG, Map<Id,User_VIC_Role__c> mapUserIdTOUserVICRoleARG, Map<Id,Opportunity> mapIdToObjOppARG, Map<String, VIC_Split_Percentage__mdt> mapKeyToSplitPercentARG){
        for(OpportunityLineItem itrOLI : lstNONServiceOpportunityLineItemARG){
            //Using below logic for Bd sales rep
            if(!String.isEmpty(itrOLI.Product_BD_Rep__c) && mapUserIdTOUserVICRoleARG.containsKey(itrOLI.Product_BD_Rep__c) && 
                mapUserIdTOUserVICRoleARG.get(itrOLI.Product_BD_Rep__c).Master_VIC_Role__r.Horizontal__c == strITO){
                if(mapUserIdTOUserVICRoleARG.containsKey(itrOLI.vic_Owner__c) && 
                         mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c != strITO){
                    //itrOLI.vic_Product_BD_Rep_Split__c = 100.00;  //@Line is commented, Now it can be changed in future
                    String key = 'ITServiceProductBDRepSubRoleIT';
                    itrOLI.vic_Product_BD_Rep_Split__c =  (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
                    //next If/else added by Prashant Kumar1 // code is remove from here
                }else{
                    itrOLI.vic_Product_BD_Rep_Split__c =  0;    
                }
                initOLIApprovalStatus(itrOLI);
                // || mapUserIdTOUserVICRoleARG.get(itrOLI.Product_BD_Rep__c).Master_VIC_Role__r.Horizontal__c == strCM
            }else if(!String.isEmpty(itrOLI.Product_BD_Rep__c) && mapUserIdTOUserVICRoleARG.containsKey(itrOLI.Product_BD_Rep__c) && 
                        (mapUserIdTOUserVICRoleARG.get(itrOLI.Product_BD_Rep__c).Master_VIC_Role__r.Horizontal__c == strEnterpriseSales) && 
                            mapUserIdTOUserVICRoleARG.containsKey(itrOLI.vic_Owner__c) && 
                                 mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strITO){ //New check is added for CM
                //itrOLI.vic_Product_BD_Rep_Split__c = 50.00;     //@Line is commented, Now it can be changed in future
                String key = 'ITServiceProductBDRepSubRoleEnterprise';
                itrOLI.vic_Product_BD_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
                //next If/else added by Prashant Kumar1 //Code has been removed
               if(mapIdToObjOppARG.containsKey(itrOLI.OpportunityId) && mapIdToObjOppARG.get(itrOLI.OpportunityId).Account.Hunting_Mining__c == 'Mining'){
                    //itrOLI.vic_TCV_Accelerator_Variable_Credit__c = 100.00;     //@Line is commented, Now it can be changed in future
                    String keyAcc = 'ITServicesAccountIsMinning';
                    itrOLI.vic_TCV_Accelerator_Variable_Credit__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(keyAcc))? mapKeyToSplitPercentARG.get(keyAcc).vic_Split_Value__c:null;
                }else{
                    //itrOLI.vic_TCV_Accelerator_Variable_Credit__c = 50.00;     //@Line is commented, Now it can be changed in future
                    String keyAcc = 'ITServicesAccountIsNotMinning';
                    itrOLI.vic_TCV_Accelerator_Variable_Credit__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(keyAcc))? mapKeyToSplitPercentARG.get(keyAcc).vic_Split_Value__c:null;
                }
                initOLIApprovalStatus(itrOLI);   
            }else if(!String.isEmpty(itrOLI.Product_BD_Rep__c) && mapUserIdTOUserVICRoleARG.containsKey(itrOLI.Product_BD_Rep__c) && 
                        (mapUserIdTOUserVICRoleARG.get(itrOLI.Product_BD_Rep__c).Master_VIC_Role__r.Horizontal__c == strEnterpriseSales) && 
                            mapUserIdTOUserVICRoleARG.containsKey(itrOLI.vic_Owner__c) && 
                                 mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c != strITO){
                itrOLI.vic_Product_BD_Rep_Split__c = 0;
                itrOLI.vic_TCV_Accelerator_Variable_Credit__c = 0;
                initOLIApprovalStatus(itrOLI); 
            }
            //Using below logic for primary sales rep
            // || mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strCM)
            if(mapUserIdTOUserVICRoleARG.containsKey(itrOLI.vic_Owner__c) && 
                (mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strEnterpriseSales || mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strCM)){ //ADDED ON 27092018 REQUESTED BY SIDDHARTH ON LAST MINUTE
                //itrOLI. = 100.00;     //@Line is commented, Now it can be changed in future
                String key = 'ITServPrimarySalesRepSubRoleIsEnterpris';
                itrOLI.vic_Primary_Sales_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
                //next If/else added by Prashant Kumar1
                if(itrOLI.vic_Primary_Sales_Rep_Split__c > 0){
                    itrOLI.vic_Sales_Rep_Approval_Status__c = strPending;
                }
            }else if(mapUserIdTOUserVICRoleARG.containsKey(itrOLI.vic_Owner__c) && 
                    mapUserIdTOUserVICRoleARG.get(itrOLI.vic_Owner__c).Master_VIC_Role__r.Horizontal__c == strITO){
                //itrOLI.vic_Primary_Sales_Rep_Split__c = 100.00;     //@Line is commented, Now it can be changed in future
                String key = 'ITServicePrimarySalesRepSubRoleIsIT';
                itrOLI.vic_Primary_Sales_Rep_Split__c = (mapKeyToSplitPercentARG != null && mapKeyToSplitPercentARG.containsKey(key))? mapKeyToSplitPercentARG.get(key).vic_Split_Value__c:null;
                //next If/else added by Prashant Kumar1
                if(itrOLI.vic_Primary_Sales_Rep_Split__c > 0){
                    itrOLI.vic_Sales_Rep_Approval_Status__c = strPending;
                }
            }
        }  
    }
    //Stting approval status // Written by prashant kumar
    public void initOLIApprovalStatus(OpportunityLineItem itrOLI){
        if(itrOLI.vic_Product_BD_Rep_Split__c > 0){
            itrOLI.vic_Product_BD_Rep_Approval_Status__c = strPending;
        }
        else{
            itrOLI.vic_Product_BD_Rep_Approval_Status__c = strNotRequired;
        }
    }
    
}