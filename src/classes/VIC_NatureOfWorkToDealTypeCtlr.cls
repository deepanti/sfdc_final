/*
    @Description: This class will be used for getting Service Type
    @Test Class: DealTypeToServiceCtlrTest
    @Author: Vikas Rajput
*/
public with sharing class VIC_NatureOfWorkToDealTypeCtlr{
    public static Map<String, String> mapStrDealTypeToServiceLCLTest{get;set;}    //Need to remove this variable after testing
    @TestVisible
    private static Map<String, String> mapStrDealTypeToServiceLCL;
    @TestVisible
    private static Map<String, vic_Nature_Of_Work_To_Deal_Type__mdt> labelToMetadataMap;
    static{
        mapStrDealTypeToServiceLCL = new Map<String, String>();
        labelToMetadataMap = new Map<String, vic_Nature_Of_Work_To_Deal_Type__mdt>();
        for(vic_Nature_Of_Work_To_Deal_Type__mdt objDTS : [SELECT DeveloperName, Label,vic_Deal_Type__c, vic_Profitable_Booking_Type__c FROM vic_Nature_Of_Work_To_Deal_Type__mdt]){
            mapStrDealTypeToServiceLCL.put(objDTS.Label, objDTS.vic_Deal_Type__c);
            labelToMetadataMap.put(objDTS.Label, objDTS);
        }
        mapStrDealTypeToServiceLCLTest = mapStrDealTypeToServiceLCL;
    }
    /*
        @Description: function will return service type
        @Param: labelName(Lebel of metadata records)
        @Author: Vikas Rajput
    */
    public String fetchDealType(String labelName){
        if(mapStrDealTypeToServiceLCL.containsKey(labelName)){
            return mapStrDealTypeToServiceLCL.get(labelName);
        }
        return null;
    }
    /*
        @Description: function will return map of all metadata with key of nature of work(Label)
        @Param: NAN
        @Author: Vikas Rajput
    */
    public Map<String, vic_Nature_Of_Work_To_Deal_Type__mdt> getAllMetaData(){
        return labelToMetadataMap;
    }
    /*
        @Description: function will return all service type
        @Param: labelName(Lebel of metadata records)
        @Author: Vikas Rajput
    */
    public Map<String, String> fetchAllDealType(){
        return mapStrDealTypeToServiceLCL;
    }
    
     /*
        @Description: function will return deal type corresponding to nature of work
        @Param: nature of work
        @Author: Bashim Khan
    */
    public static String getDealType(String natureOfWork){
        for(String label : labelToMetadataMap.keySet()){
            if(labelToMetadataMap.get(label).DeveloperName == natureOfWork)
                return labelToMetadataMap.get(label).vic_Deal_Type__c;
        }
        return null;
    }
    
    /*
     * @author: Prashant Kumar1
     * @param: vic_Profitable_Booking_Type__c (String)
     * @return: List<String>
     * @description: Method will return list of nature of works 
     * on the basis of profitable booking type i.e. IO/TS
     * 
    */
    public static List<String> getNatureOfWorkList(String profitableBookingType){
        List<String> natureOfWorkList = new List<String>();
        
        for(String label: labelToMetadataMap.keySet()){
            if(labelToMetadataMap.get(label).vic_Profitable_Booking_Type__c == profitableBookingType){
                natureOfWorkList.add(label);
            }
        }   
        return natureOfWorkList;
    }
    
    /*
     * @author: Prashant Kumar1
     * @param: vic_Profitable_Booking_Type__c (String)
     * @return: List<String>
     * @description: Method will return list of nature of works 
     * on the basis of profitable Deal Type type i.e. 	IT Services / Non IT Services
     * 
    */
    public static Set<String> getNatureOfWorkListForDealType(String dealType){
        Set<String> natureOfWorkvalues = new Set<String>();
        for(String label: mapStrDealTypeToServiceLCL.keySet()){
            if(mapStrDealTypeToServiceLCL.get(label) == dealType){
                natureOfWorkvalues.add(label);
            }
        }
        return natureOfWorkvalues;
    }
}