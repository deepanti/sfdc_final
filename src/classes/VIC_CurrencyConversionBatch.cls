/**
* @Description: Batch Class used to calculate Local Currency of Each User 
* @author: Rajan Saini
* @date: Aug 2018
*/


public class VIC_CurrencyConversionBatch implements Database.Batchable<sObject>, Database.Stateful{
   
   private boolean isUpfrontCalculation;
   private integer processingYear = 0;
    public VIC_CurrencyConversionBatch (Boolean isUpfrontCalculation){
        this.isUpfrontCalculation = isUpfrontCalculation != null? isUpfrontCalculation : true;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
      
        String componentCategory = isUpfrontCalculation ? 'Upfront' : 'Annual';
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        processingYear = isUpfrontCalculation?Integer.ValueOf(vicInfo.VIC_Process_Year__c):Integer.ValueOf(vicInfo.VIC_Annual_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        return Database.getQueryLocator([
                                SELECT Id, Bonus_Amount__c, vic_Opportunity_Product_Id__c, Achievement_Date__c,Opportunity__r.Actual_Close_Date__c,
                                Target_Component__r.Target__r.Domicile__c, Target_Component__r.Target__r.User__c 
                                FROM Target_Achievement__c 
                                WHERE vic_Incentive_In_Local_Currency__c = null
                                AND Target_Component__r.Master_Plan_Component__r.vic_Component_Category__c =:componentCategory 
                                AND Target_Component__r.Target__r.Start_Date__c =:targetStartDate
                                AND Target_Component__r.Target__r.Domicile__c != null
                                Order By CreatedDate]);
    }

    
    public void execute(Database.BatchableContext bc, List<Target_Achievement__c> incentives){
        Map<String,Decimal> mapExchangeRate = new Map<String,Decimal>();
        for(vic_Currency_Exchange_Rate__c er : [SELECT  vic_Exchange_Rate__c, 
                                                   vic_ISO_Code__c,vic_Month__c,vic_Year__c
                                                   FROM vic_Currency_Exchange_Rate__c
                                                   WHERE vic_Year__c =:string.valueof(processingYear)])
        {
            String KeyExchngeRate = er.vic_ISO_Code__c + '-' + er.vic_Year__c + '-' + er.vic_Month__c;
            mapExchangeRate.put(KeyExchngeRate,er.vic_Exchange_Rate__c);
        }
        
        // get ISO code from Common Class
        Map<String, String> mapDomisileToISO = vic_CommonUtil.getDomicileToISOCodeMap();
        
        List<Target_Achievement__c> lstUpdateIncentives = new List<Target_Achievement__c>();
        for(Target_Achievement__c inc : incentives){
            System.debug('Inc-> ' + inc);
            String strExchangeRateKey;          
            String strIncISO = mapDomisileToISO != null && mapDomisileToISO.containsKey(inc.Target_Component__r.Target__r.Domicile__c) ? mapDomisileToISO.get(inc.Target_Component__r.Target__r.Domicile__c) : 'USD';
            system.debug('strIncISO'+strIncISO);
            if(isUpfrontCalculation){
                if(inc.Opportunity__c!=null && inc.Opportunity__r.Actual_Close_Date__c!=null){
                    strExchangeRateKey = strIncISO + '-' + inc.Opportunity__r.Actual_Close_Date__c.year() + '-' + inc.Opportunity__r.Actual_Close_Date__c.month();
                }else{
                    system.debug('Exchange rate key not Intialised either opportunity or opportunity closed date is null');
                }
             }else{
                   if(inc.Achievement_Date__c!=null){
                    strExchangeRateKey = strIncISO + '-' + inc.Achievement_Date__c.year() + '-' + inc.Achievement_Date__c.month();
                   }else{
                    system.debug('Exchange rate key not Intialised achievement date is null');
                }
            }
            
            
           if(strExchangeRateKey != null && mapExchangeRate != null && mapExchangeRate.containsKey(strExchangeRateKey)){
                system.debug('check');
                Decimal exchangeRate = mapExchangeRate.get(strExchangeRateKey) != null ? mapExchangeRate.get(strExchangeRateKey) : 0;
                
                Decimal convertedAmt = (inc.Bonus_Amount__c !=null ? inc.Bonus_Amount__c: 0) * exchangeRate;
                lstUpdateIncentives.add(new Target_Achievement__c(
                    id = inc.id,
                    vic_Incentive_In_Local_Currency__c = convertedAmt,
                    vic_Exchange_Rate__c = exchangeRate
                ));
            }
            
        }
        system.debug('lstUpdateIncentives'+lstUpdateIncentives);
        if(lstUpdateIncentives.size() > 0){
            update lstUpdateIncentives;
        }       
    }
    
    
    public void finish(Database.BatchableContext bc){
        
    }
}