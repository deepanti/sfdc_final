public class GPSelectorProjectExpense extends fflib_SObjectSelector {

    public List < Schema.SObjectField > getSObjectFieldList() {
        return new List < Schema.SObjectField > {
            GP_Project_Expense__c.Name

        };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_Project_Expense__c.sObjectType;
    }

    public List < GP_Project_Expense__c > selectById(Set < ID > idSet) {
        return (List < GP_Project_Expense__c > ) selectSObjectsById(idSet);
    }

    public GP_Project_Expense__c selectProjectExpenseRecord(Id projectExpenseId) {
        return [Select Id,GP_Project__c from GP_Project_Expense__c where Id =: projectExpenseId];
    }

    public List < GP_Project_Expense__c > selectProjectExpenseRecords(Id projectId) {
        return [SELECT Id, GP_OMS_ID__c, GP_Expense_Category__c, GP_Expenditure_Type__c, GP_Location__c, GP_Parent_Project_Expense__c,
            GP_Amount__c, GP_Remark__c, GP_Data_Source__c
            FROM GP_Project_Expense__c
            WHERE GP_Project__c =: projectId and GP_IsSoftDeleted__c = false
        ];
    }
    
    public List < GP_Project_Expense__c > selectProjectExpenseRecords(Set < Id > lstOfRecordId) {
        String strQuery = ' select ';
        Map < String, Schema.SObjectField > MapOfSchema = Schema.SObjectType.GP_Project_Expense__c.fields.getMap();
        strQuery = GPBaseProjectUtil.appendToSelectQueryWithSchemaMap(strQuery, MapOfSchema);
        strQuery += ' from GP_Project_Expense__c  where id =:lstOfRecordId';
        return Database.Query(strQuery);
    }
}