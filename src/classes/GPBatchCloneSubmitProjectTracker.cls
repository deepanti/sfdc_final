@isTest 
private class GPBatchCloneSubmitProjectTracker {    
    private static GP_Project__c prjObj;
    private static GP_Deal__c dealObj;
    
    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Final_OHR__c = '850040951';
        empObj.GP_isActive__c = true;
        insert empObj;
        
        GP_Employee_Master__c empObj2 = GPCommonTracker.getEmployee();
        empObj2.GP_Person_ID__c = 'EMP-002';
        empObj2.GP_Final_OHR__c = '703248825';
        empObj2.GP_isActive__c = true;
        insert empObj2;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Oracle_Id__c = '1234';
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Work_Location__c objworkLocation = GPCommonTracker.getWorkLocation();
        objworkLocation.GP_Oracle_Id__c = '5678';
        objworkLocation.GP_Status__c = 'Active and Visible';
        objworkLocation.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation;
        
        GP_Work_Location__c objworkLocation2 = GPCommonTracker.getWorkLocation();
        objworkLocation2.GP_Oracle_Id__c = '9012';
        objworkLocation2.GP_Status__c = 'Active and Visible';
        objworkLocation2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation2;
        
        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objPinnacleMaster;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;
        
        Business_Segments__c objBS = GPCommonTracker.getBs();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.Id);
        insert objSB;
        
        Account accountObj = GPCommonTracker.getAccount(objBS.Id, objSB.Id);
        insert accountObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Without SFDC';
        dealObj.GP_Account_Name_L4__c = accountObj.Id;
        dealObj.GP_Business_Segment_L2__c = objBS.Id;
        dealObj.GP_Sub_Business_L3__c = objSB.Id;
        dealObj.GP_Business_Group_L1__c = 'GE';
        dealObj.GP_Business_Segment_L2_Id__c = objBS.Id;
        dealObj.GP_Sub_Business_L3_Id__c = objSB.Id;
        dealObj.GP_Is_Created_per_Account__c = true;
        dealObj.GP_Sales_Leader_OHR__c = '850040951';
        insert dealObj;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        insert prjObj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Leadership_Role__c = 'Billing SPOC';
        insert leadershipMaster;
        
        GP_Leadership_Master__c leadershipMasterGPM = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMasterGPM.GP_Leadership_Role__c = 'Global Project Manager';
        insert leadershipMasterGPM;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.GP_Leadership_Role_Name__c = 'Billing SPOC';
        insert projectLeadership;
        
        GP_Project_Leadership__c projectLeadershipGPM = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMasterGPM.Id);
        projectLeadershipGPM.GP_Employee_ID__c = empObj2.Id;
        projectLeadershipGPM.GP_Leadership_Role__c = 'PROJECT MANAGER';
        projectLeadershipGPM.GP_Leadership_Role_Name__c = 'Global Project Manager';
        insert projectLeadershipGPM;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId(); 
        insert objProjectWorkLocationSDO;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation.Id);
        objProjectWorkLocation.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation2.Id);
        objProjectWorkLocation1.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation1;
        
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        update prjObj;
    }
    
    @isTest static void testBatch() {
        fetchData();
        GPBatchCloneSubmitProject cloneSubmitProject = new GPBatchCloneSubmitProject(new Set<Id> {prjObj.Id},new Map<Id,GP_Deal__c>{prjObj.Id=>dealObj});
        Database.executeBatch(cloneSubmitProject);
    }
    
    @isTest static void testCatchBlock() {
        fetchData();
        GPBatchCloneSubmitProject cloneSubmitProject = new GPBatchCloneSubmitProject(new Set<Id> {prjObj.Id},new Map<Id,GP_Deal__c>{prjObj.Id=>null});
        //05-12-18: Don't run the clone code in batch due to Internal SFDC error.
        cloneSubmitProject.cloneProject = false;
        Database.executeBatch(cloneSubmitProject);
    }
    
    @isTest static void testApprovalBlock() {
        fetchData();
        GPBatchCloneSubmitProject cloneSubmitProject = new GPBatchCloneSubmitProject(new Set<Id> {prjObj.Id},true);
        //05-12-18: Don't run the clone code in batch due to Internal SFDC error.
        cloneSubmitProject.cloneProject = false;
        Database.executeBatch(cloneSubmitProject);
    }
    
    // Incase of error in any instance comment the test case.
    @isTest static void testApprovalBlockWithClonePIDId() {
        fetchData();
        GPBatchCloneSubmitProject cloneSubmitProject = new GPBatchCloneSubmitProject(new Set<Id> {prjObj.Id},true);        
        Database.executeBatch(cloneSubmitProject);
    }
    
    private static void fetchData() {
        prjObj = [select id, Name from GP_Project__c limit 1];
        
        dealObj = [select id,GP_Business_Group_L1__c,GP_Business_Segment_L2__c,GP_Sub_Business_L3__c,GP_Account_Name_L4__c,
                   GP_Service_Line__c, GP_Business_Segment_L2_Id__c, GP_Sub_Business_L3_Id__c, GP_Vertical__c,
                   GP_Sub_Vertical__c,GP_Nature_of_Work__c,GP_Product_Id__c, GP_Product__c, GP_Is_Created_per_Account__c,
                   GP_Sales_Leader_OHR__c from GP_Deal__c limit 1];
    }
}