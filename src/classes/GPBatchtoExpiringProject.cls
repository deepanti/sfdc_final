// ------------------------------------------------------------------------------------------------ 
// Description: When the end date of the project comes near to the defined notification days between 
// the end date and the present date a notification is sent the defined list of leadership and user members 
// for making them aware of the upcoming end date
// ------------------------------------------------------------------------------------------------
// Modification Date::04-DEC -2017  Created By : Mandeep Singh Chauhan
// ------------------------------------------------------------------------------------------------ 
global class GPBatchtoExpiringProject implements
Database.Batchable < sObject > , Database.Stateful {
    
    Id MandatoryKeyMembersRecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
    Map < GP_Project__c, Set < GP_Project_Leadership__c >> mapofprojectandprjleadrship = new Map < GP_Project__c, set < GP_Project_Leadership__c >> ();
    Map < Id, Map < String, String >> mapOfProjectIDVsRoleNameVsEmailIds = new Map < Id, Map < String, String >> ();
    List < GP_Project__c > listOfProjectRecords = new List < GP_Project__c > ();
    
    global Map < Id, List < GP_Project__c >> mapOfEmployeeVsListOfExpirigProjects = new Map < Id, List < GP_Project__c >> ();
    global Map < Id, GP_Employee_Master__c > mapOfEmployee = new Map < Id, GP_Employee_Master__c > ();
    global List < Messaging.SingleEmailMessage > EmaiTolist;
    global Messaging.SingleEmailMessage message;
    //global Integer noOfDaysOfNotification;
    global List < String > listofemails;
    global Date priorDateValue;
    global Integer days = 0;
    global String Query;
    
    //global Date initialpriordays = system.today().addDays(30);
    //global Date secondpriordays = system.today().addDays(15);
    //global Date thirdpriordays = system.today().addDays(7);
    
    global date initialpriordays=System.Today().adddays(integer.valueOf(system.label.GP_Initial_Days_Prior_Project_End_Date)); 
    global date secondpriordays=System.Today().adddays(integer.valueOf(system.label.GP_Second_Prior_Days_Prior_Project_End_Date));  
    global date thirdpriordays=System.Today().adddays(integer.valueOf(system.label.GP_Third_Prior_Days_Prior_Project_End_Date)); 
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // Get expiring PIDs based on End Date or SOW End Date.
        // PIDs List which are expiring in next 30 or 15 or 7 days from now.
        return Database.getQueryLocator([Select Id, Name, GP_End_Date__c, GP_GPM_Employee_Email__c,
                                         GP_Oracle_PID__c, GP_Customer_Hierarchy_L4__r.Name, GP_Primary_SDO__r.Name, GP_SOW_End_Date__c,
                                         GP_GPM_Employee__r.GP_SFDC_User__c, GP_GPM_Employee__r.GP_OFFICIAL_EMAIL_ADDRESS__c, GP_Project_Short_Name__c,
                                         
                                         (select id, GP_Employee_ID__c, GP_Employee_ID__r.GP_SFDC_User__c, GP_Employee_ID__r.Name, GP_Employee_ID__r.GP_FULL_NAME__c,GP_Leadership_Role__c,
                                          GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c from Project_Leaderships__r where 
                                          RecordTypeID =: MandatoryKeyMembersRecordTypeId AND GP_Active__c = true 
                                          AND (GP_Leadership_Role__c =: System.Label.GP_Billing_Approver_Role_Code 
                                               OR GP_Leadership_Role__c =: System.Label.GP_Billing_SPOC_Leadership_Role_Code 
                                               OR GP_Leadership_Role__c =: System.Label.GP_FP_A_Approver 
                                               OR GP_Leadership_Role__c =: System.Label.GP_PID_Approver_Leadership_Code 
                                               OR GP_Leadership_Role__c =: System.Label.GP_GPM_Leadesrhip_Role_Code
                                              )
                                         )            
                                         From GP_Project__c where
                                         (
                                             ((RecordType.Name = 'OTHER PBB' OR RecordType.Name = 'CMITS') AND 
                                              (GP_End_Date__c =: initialpriordays OR GP_End_Date__c =: secondpriordays OR GP_End_Date__c =: thirdpriordays))
                                             OR(
                                                 GP_SOW_End_Date__c =: initialpriordays OR
                                                 GP_SOW_End_Date__c =: secondpriordays OR GP_SOW_End_Date__c =: thirdpriordays))
                                        ]);
    }
    
    global void execute(Database.BatchableContext bc, List < GP_Project__c > listOfProjectRecords) {
        
        if (listOfProjectRecords != null && listOfProjectRecords.size() > 0) {
            this.listOfProjectRecords = listOfProjectRecords;
            
            // Get the employee's details and expiring PIDs list per employee.
            for (GP_Project__c project: listOfProjectRecords) {                
                if (project.Project_Leaderships__r != null) {
                    for (GP_Project_Leadership__c projectLeadership: project.Project_Leaderships__r) {
                        // Offical email address of employee is mandatory to send email.
                        if (String.isBlank(projectLeadership.GP_Employee_ID__c) 
                            || String.isBlank(projectLeadership.GP_Employee_ID__r.GP_OFFICIAL_EMAIL_ADDRESS__c)) {
                                break;
                            }
                        
                        if (!mapOfEmployeeVsListOfExpirigProjects.containsKey(projectLeadership.GP_Employee_ID__c)) {
                            mapOfEmployeeVsListOfExpirigProjects.put(projectLeadership.GP_Employee_ID__c, new List < GP_Project__c > ());
                        }
                        if(projectLeadership.GP_Leadership_Role__c == System.Label.GP_Billing_Approver_Role_Code) {
                            mapOfEmployeeVsListOfExpirigProjects.get(projectLeadership.GP_Employee_ID__c).add(project);
                        } else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_Billing_SPOC_Leadership_Role_Code) {
                            mapOfEmployeeVsListOfExpirigProjects.get(projectLeadership.GP_Employee_ID__c).add(project);
                        } else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_FP_A_Approver) {
                            mapOfEmployeeVsListOfExpirigProjects.get(projectLeadership.GP_Employee_ID__c).add(project);
                        } else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_PID_Approver_Leadership_Code) {
                            mapOfEmployeeVsListOfExpirigProjects.get(projectLeadership.GP_Employee_ID__c).add(project);
                        } else if (projectLeadership.GP_Leadership_Role__c == System.Label.GP_GPM_Leadesrhip_Role_Code) {
                            mapOfEmployeeVsListOfExpirigProjects.get(projectLeadership.GP_Employee_ID__c).add(project);
                        }
                        
                        mapOfEmployee.put(projectLeadership.GP_Employee_ID__c, projectLeadership.GP_Employee_ID__r);
                    }
                }
            }
        }
        
        //system.debug('==mapOfEmployeeVsListOfExpirigProjects=='+mapOfEmployeeVsListOfExpirigProjects);
        //system.debug('==mapOfEmployee=='+mapOfEmployee);
    }
    
    global void finish(Database.BatchableContext bc) {
        try {
            
            EmaiTolist = new list < Messaging.SingleEmailMessage > ();
            Id orgWideEmailId = GPCommon.getOrgWideEmailId();
            //Quering for Email template
            EmailTemplate EmpTemplate = [Select id, DeveloperName, body, subject, htmlvalue
                                         from EmailTemplate where
                                         developername = 'GP_Email_Template_For_Expiring_Project_Notification'
                                         limit 1
                                        ];
            // Iterate the list of Employees to whom email will go.
            for (Id empId: mapOfEmployeeVsListOfExpirigProjects.keySet()) {
                system.debug('==empId::'+empId);
                listofemails = new list < String > ();
                
                GPCommon objcommon = new GPCommon();                
                message = objcommon.notificationThroughEmailForBatch(EmpTemplate, mapOfEmployee.get(empId), 'GP_Field_Set_Emp_Batch_For_Email_Notific', 'GP_Employee_Master__c');                
                
                if(mapOfEmployee.get(empId) != null && String.isNotBlank(mapOfEmployee.get(empId).GP_FULL_NAME__c)) {
                    message.setHtmlBody(message.getHtmlBody().replace('EMPLOYEE_FULL_NAME', mapOfEmployee.get(empId).GP_FULL_NAME__c));
                }
                
                message.setHtmlBody(message.getHtmlBody().replace('LIST_PROJECT_RECORDS', createProjectTableString(mapOfEmployeeVsListOfExpirigProjects.get(empId))));
                //message.setHtmlBody(message.getHtmlBody().replace('NUMBER_OF_DAYS', String.ValueOf(noOfDaysOfNotification)));
                
                listofemails.add(mapOfEmployee.get(empId).GP_OFFICIAL_EMAIL_ADDRESS__c);
                
                /*Map < String, String > mapOfRoleCodeVsEmailId = mapOfProjectIDVsRoleNameVsEmailIds.get(project.Id);
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_Billing_Approver_Role_Code))
                listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_Billing_Approver_Role_Code));
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_Billing_SPOC_Leadership_Role_Code))
                listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_Billing_SPOC_Leadership_Role_Code));
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_FP_A_Approver))
                listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_FP_A_Approver));
                if (mapOfRoleCodeVsEmailId.containsKey(System.Label.GP_GPM_Leadesrhip_Role_Code))
                listofemails.add(mapOfRoleCodeVsEmailId.get(System.Label.GP_GPM_Leadesrhip_Role_Code));
                */
                
                if (listofemails != null && listofemails.size() > 0) {
                    message.setToAddresses(listofemails);
                }
                
                if (message != null) {                    
                    // No need to specify the target id since we are already creating the 
                    // PID table and replacing the LIST_PROJECT_RECORDS in email template.
                    message.settargetobjectid(null);
                    if(orgWideEmailId != null) {
                        message.setOrgWideEmailAddressId(orgWideEmailId);
                    }
                    
                    EmaiTolist.add(message);
                }
            }
            
            if (EmaiTolist != null && EmaiTolist.size() > 0 && GPCommon.allowSendingMail()) {                
                Messaging.SendEmailResult[] EHRBP = Messaging.sendEmail(EmaiTolist);
            }
            
        } catch (Exception ex) {
            GPErrorLogUtility.logError('GPBatchtoExpiringProject', 'execute', ex, ex.getMessage(), null, null, null, null, ex.getStackTraceString());
        }
    }
    
    private String createProjectTableString(List < GP_Project__c > expiringProjects) {
        
        String html = '';
        
        if (expiringProjects.size() > 0) {
            html += ' <table  width="100%" style="font-family:Arial;font-size:13px;border-collapse: collapse" border="1">';
            html += '   <thead>';
            html += '       <tr>';
            html += '           <th bgcolor="#005595" style="color:white">PID/Project ID</th>';
            html += '           <th bgcolor="#005595" style="color:white">Process Names</th>';
            html += '           <th bgcolor="#005595" style="color:white">Account Name</th>';
            html += '           <th bgcolor="#005595" style="color:white" >SDO</th>';
            html += '           <th bgcolor="#005595" style="color:white" >SOW Expiry DATE</th>';
            html += '       </tr>';
            html += '   </thead>';
            html += '   <tbody>';
            
            for (GP_Project__c project: expiringProjects) {
                html += '       <tr>';
                html += '           <td>' + project.GP_Oracle_PID__c + '</td>';
                html += '           <td>' + project.GP_Project_Short_Name__c + '</td>';
                html += '           <td>' + project.GP_Customer_Hierarchy_L4__r.Name + '</td>';
                html += '           <td>' + project.GP_Primary_SDO__r.Name + '</td>';
                html += '           <td>' + project.GP_SOW_End_Date__c + '</td>';
                html += '       </tr>';
            }
            
            html += ' </tbody></table>';
            //system.debug('==html@@@@' + html);            
        }
        
        return html;
    }
}