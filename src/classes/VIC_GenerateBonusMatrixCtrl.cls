/**
    @Author:        Dheeraj Chawla
    @Date:          30/08/2018
    @description:   To create the pdf of Bonus Letter.
**/
global class VIC_GenerateBonusMatrixCtrl {

    public String lstOfWrapComponentToIncentiveValue { get; set;}

    public Date startDate {get; set;}
    public Date endDate {get; set;}
    private String strUserId {get; set;}
    public IncentiveWrapper incentiveData{get; set;}
    public static  Map<String,String> monthNameMap{get;set;}
    public List<Target_Component__c> lstComponents{get;set;}
    public UserInformation userInfo {get; set;}

    public VIC_GenerateBonusMatrixCtrl() {
        strUserId = ApexPages.currentPage().getParameters().get('userId');
        String strStartDate = ApexPages.currentPage().getParameters().get('startdate');
        String strEndDate = ApexPages.currentPage().getParameters().get('endDate');
        
        if(strUserId == null  || strUserId == ''  || strStartDate == null || strStartDate == '' || strEndDate == null || strEndDate == ''){
            return;
        }
        
        startDate = Date.valueOf(strStartDate);
        endDate = Date.valueOf(strEndDate);
        //Create the Map to show to Month name with year.
        monthNameMap = new Map<String, String>{'1' =>'Jan', '2'=>'Feb', '3'=>'March', '4'=>'April', '5'=>'May',
                                               '6'=>'June', '7'=>'July', '8'=>'Aug', '9'=>'Sept','10'=>'Oct',
                                               '11'=>'Nov', '12'=>'Dec'
                                                                          };
       //fetch the target component and it's incentive amount to show the data.
       getTargetComponentDetails();
    }
    
    /**
    @Author:        Dheeraj Chawla
    @Date:          30/08/2018
    @description:   To get the target component and it's incentive amount to show the data.
        **/
    public void getTargetComponentDetails() {
       
        lstComponents = [SELECT Id, 
                                Name,
                                Component_Type__c,
                                Target_Status__c, 
                                Target__c,
                                Master_Plan_Component__c,
                                Master_Plan_Component__r.Name,
                                Target__r.OHR__c,
                                Target__r.Domicile__c,
                                Target__r.User__r.Name,
                                (SELECT Id, 
                                        Name, 
                                        Target_Component__c, 
                                        Achievement_Date__c,
                                        vic_HR_Approval_Date__c, 
                                        Achievement_Month__c, 
                                        vic_Status__c,
                                        vic_Incentive_In_Local_Currency__c   
                                FROM Target_Achievements__r
                                WHERE vic_Status__c = 'HR - Approved'
                                AND vic_HR_Approval_Date__c >= :startDate
                                AND vic_HR_Approval_Date__c <= :endDate
                                AND vic_Exclude_For_Calculation__c = FALSE
                                ORDER BY vic_HR_Approval_Date__c ASC)
                        FROM Target_Component__c
                        WHERE Target__r.User__c =:strUserId
                        AND Target_Status__c = 'Active'
                        AND Target__r.Start_date__c =: startDate
                        AND Target__r.is_Active__c = TRUE
                        AND Master_Plan_Component__r.Component_Usage__c INCLUDES('Bonus') 
                        AND Master_Plan_Component__r.vic_Component_Code__c != null 
                        ORDER BY Master_Plan_Component__r.vic_Component_Code__c];


        if(lstComponents == null || lstComponents.isEmpty()) {
            return;
        }
        
        Map<String, Map<String, Decimal>> mapOfMonthToMapOfIncentiveComponentToIncentive = new Map<String, Map<String, Decimal>>();
        Map<String, Decimal> mapOfIncentiveComponentToTotalValue = new Map<String, Decimal>();
        Map<String, Decimal> mapOfMonthToTotalIncentive = new Map<String, Decimal>();
        Map<String,date> mapofMonthkeytoDate=new Map<String,date>();
        List<IncentiveRowWrapper> listOfIncentiveRow = new List<IncentiveRowWrapper>(); 
        Set<String> setOfHeader = new Set<String>();

        Map <Id, User_VIC_Role__c> mapOfUserIdToVIC_Role = vic_CommonUtil.getUserVICRoleinMap();
        Map < String, String > mapOfDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap();

        for(Target_Component__c objComponent : lstComponents) {
            if(userInfo == null){
                //fetch the VIC Role with respect to user..
                //Map <Id, User_VIC_Role__c> mapOfUserIdToVIC_Role = vic_CommonUtil.getUserVICRoleinMap();
                //Map < String, String > mapOfDomicileToISOCode = vic_CommonUtil.getDomicileToISOCodeMap();
                userInfo = new UserInformation();
                userInfo.strOHRId = objComponent.Target__r.OHR__c;
                userInfo.strName = objComponent.Target__r.User__r.Name;
                userInfo.strVICRole = mapOfUserIdToVIC_Role.get(strUserId).Master_VIC_Role__r.Name;
                userInfo.strVICDetail = '';
                userInfo.strCurrencyISO = mapOfDomicileToISOCode.get(objComponent.Target__r.Domicile__c); 
            } 
            
            String incentiveName = objComponent.Master_Plan_Component__r.Name;
            
            setOfHeader.add(incentiveName);

            for(Target_Achievement__c objAchievement : objComponent.Target_Achievements__r) {
                Decimal currentIncentive = objAchievement.vic_Incentive_In_Local_Currency__c != null ? objAchievement.vic_Incentive_In_Local_Currency__c : 0;               
                Date currentMonth = objAchievement.vic_HR_Approval_Date__c;
                String currentMonthName = String.valueOf(currentMonth.month())+String.valueOf(currentMonth.year());
                if(!mapofMonthkeytoDate.containskey(currentMonthName)){
                  mapofMonthkeytoDate.put(currentMonthName,currentMonth);
                }
                if(!mapOfMonthToMapOfIncentiveComponentToIncentive.containsKey(currentMonthName)) {
                    mapOfMonthToMapOfIncentiveComponentToIncentive.put(currentMonthName, new Map<String, Decimal>());
                }
                    
                Map<String, Decimal> mapOfIncentiveComponentToIncentive = mapOfMonthToMapOfIncentiveComponentToIncentive.get(currentMonthName);

                if(!mapOfIncentiveComponentToIncentive.containsKey(incentiveName)) {
                    mapOfIncentiveComponentToIncentive.put(incentiveName, 0);
                }

                Decimal incentiveValue = mapOfIncentiveComponentToIncentive.containsKey(incentiveName) ? mapOfIncentiveComponentToIncentive.get(incentiveName) : 0;

                mapOfIncentiveComponentToIncentive.put(incentiveName, incentiveValue + currentIncentive);   

                mapOfMonthToMapOfIncentiveComponentToIncentive.put(currentMonthName, mapOfIncentiveComponentToIncentive);  


                // Update mapOfIncentiveComponentToTotalValue
                Decimal totalIncentivePerComponent = mapOfIncentiveComponentToTotalValue.containsKey(incentiveName) ? mapOfIncentiveComponentToTotalValue.get(incentiveName) : 0;

                mapOfIncentiveComponentToTotalValue.put(incentiveName, totalIncentivePerComponent + currentIncentive);


                 // Update mapOfMonthToTotalIncentive 
                Decimal totalIncentivePerMonth = mapOfMonthToTotalIncentive.containsKey(currentMonthName) ? mapOfMonthToTotalIncentive.get(currentMonthName) : 0;

                mapOfMonthToTotalIncentive.put(currentMonthName, totalIncentivePerMonth + currentIncentive);

            }
                }
                // Set listOfIncentiveRow
        for(String currentMonth : mapOfMonthToMapOfIncentiveComponentToIncentive.keySet()) {

            List<Decimal> listOfIncentives = new List<Decimal>(); 
            //mapOfMonthToMapOfIncentiveComponentToIncentive.get(currentMonth).values();
            for(String incentiveName : setOfHeader) {
                Decimal incentiveValue = mapOfMonthToMapOfIncentiveComponentToIncentive.get(currentMonth).containsKey(incentiveName) ? mapOfMonthToMapOfIncentiveComponentToIncentive.get(currentMonth).get(incentiveName) : 0; 
                listOfIncentives.add(incentiveValue);
            }
            Decimal totalIncentivePerMonth = mapOfMonthToTotalIncentive.get(currentMonth);

            listOfIncentiveRow.add(new IncentiveRowWrapper(currentMonth, listOfIncentives, totalIncentivePerMonth, String.valueOf(startdate.year()),mapofMonthkeytoDate));
        }
                listOfIncentiveRow.sort();
        List<Decimal> listOfTotalIncentivesPerIncentiveComponent = new List<Decimal>(); 
        Decimal totalIncentiveOfTheYear = 0;

        for(String incentiveName : setOfHeader) {
            Decimal totalIncentive = mapOfIncentiveComponentToTotalValue.containsKey(incentiveName) ? mapOfIncentiveComponentToTotalValue.get(incentiveName) : 0;
            listOfTotalIncentivesPerIncentiveComponent.add(totalIncentive);
            totalIncentiveOfTheYear += totalIncentive;
        }
        
        listOfTotalIncentivesPerIncentiveComponent.add(totalIncentiveOfTheYear);
        
        List<String> listOfHeader = new List<String>();
        listOfHeader.addAll(setOfHeader);
        
        incentiveData = new IncentiveWrapper(listOfHeader, listOfIncentiveRow, listOfTotalIncentivesPerIncentiveComponent);
    
    }
    
    global class IncentiveRowWrapper implements Comparable{
        public String incentiveMonth {get;set;}
        public List<Decimal> listOfIncentives {get;set;}
        public Map<String,date> mapofMonthkeytoDate{get;set;}
        public Decimal totalIncentive {get;set;}
        public Integer incentiveMonthNumber {get;set;}
        public IncentiveRowWrapper(String incentiveMonth, List<Decimal> listOfIncentives, Decimal totalIncentive,String strYear,Map<String,date> mapofMonthkeytoDate) {
            //creating the format in Month'year(last 2 digits)
            //this.incentiveMonth = monthNameMap.get(incentiveMonth) + '\' ' + strYear.substring(2,4);
            this.incentiveMonth = monthNameMap.get(string.valueof(mapofMonthkeytoDate.get(incentiveMonth).month())) + '\' ' + string.valueof(mapofMonthkeytoDate.get(incentiveMonth).year());
            this.listOfIncentives = listOfIncentives;
            this.totalIncentive = totalIncentive;
            this.incentiveMonthNumber = integer.valueOf(incentiveMonth);
        }
        //Sort the Incentives month orderwise
        //compareTo method is defined in Comparable Interface.
        global Integer compareTo(Object compareTo) {
            IncentiveRowWrapper compareToIncentiveWrapper = (IncentiveRowWrapper)compareTo;
            if (incentiveMonthNumber == compareToIncentiveWrapper.incentiveMonthNumber) return 0;
            if (incentiveMonthNumber > compareToIncentiveWrapper.incentiveMonthNumber) return 1;
            return -1;        
        }
    }

    public class IncentiveWrapper {
        public List<String> listOfHeader {get;set;}
        public List<IncentiveRowWrapper> listOfIncentiveRow {get; set;}
        public List<Decimal> listOfTotalIncentive {get; set;}

        public IncentiveWrapper(List<String> listOfHeader, List<IncentiveRowWrapper> listOfIncentiveRow, List<Decimal> listOfTotalIncentive) {
            this.listOfHeader = listOfHeader;
            this.listOfIncentiveRow = listOfIncentiveRow;
            this.listOfTotalIncentive = listOfTotalIncentive;
        }
    }
    
    public class UserInformation
    {
        public String strOHRId {get; set;}
        public String strName {get; set;}
        public String strVICRole {get; set;}
        public String strVICDetail {get; set;}
        public String strCurrencyISO {get; set;}
        
        public UserInformation(){
            strOHRId  = strName = strVICRole = strVICDetail = strCurrencyISO  = '';
        }
    }
    
}