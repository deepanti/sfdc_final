public class GPSelectorUserRole extends fflib_SObjectSelector {

    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_User_Role__c.Name,
            //GP_User_Role__c.ID,
            //GP_User_Role__c.GP_Active__c,
            //GP_User_Role__c.GP_Role__c,
            //GP_User_Role__c.GP_User__c,
            GP_User_Role__c.GP_Role__r.Name
           
            };
    }

    public Schema.SObjectType getSObjectType() {
        return GP_User_Role__c.sObjectType;
    }

    public List<GP_User_Role__c> selectById(Set<ID> idSet) {
        return (List<GP_User_Role__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_User_Role__c> selectByRoleAndUser(set<string> setProjectRole, set<string> setOwnerId) {
        return (List<GP_User_Role__c>) [SELECT ID ,NAME, GP_Active__c, GP_Role__r.GP_Active__c, GP_User__c
                                                  FROM GP_User_Role__c where GP_Role__c in: setProjectRole and
                                                  GP_User__c in : setOwnerId and
                                                  GP_Active__c = true and 
                                                  GP_Role__r.GP_Active__c = true];
    }
    
    public List<GP_User_Role__c> selectByActiveStatus(Set<Id> Setofid) {
        return (List<GP_User_Role__c>) [select id from GP_User_Role__c where GP_Active__c = true and GP_Role__c in: Setofid];
    }
    
    public List<GP_User_Role__c> selectByRoleId(Set<Id> Setofid) {
        return (List<GP_User_Role__c>)[select Id,GP_Role__c, Name,GP_Active__c from GP_User_Role__c where GP_Role__c in : Setofid];
    }
    
    public List<GP_User_Role__c> selectByUserId(Id userId) {
        return (List<GP_User_Role__c>)[select Id,Name,GP_Role__c, GP_Role__r.GP_Role_Category__c 
                                       from GP_User_Role__c
                                       where GP_User__c = : userId and 
                                       GP_Role__r.GP_Role_Category__c != null and
                                       GP_Active__c = true
                                      ];
    }
    
    public List<GP_User_Role__c> selectUserfromAdminUserRole() {
        return (List<GP_User_Role__c>)[Select ID, GP_Active__c,GP_Role__c,
                                        GP_Role__r.GP_Role_Category__c,GP_Role__r.GP_Active__c,
                                        GP_User__c,GP_User__r.Name
                                        from GP_User_Role__c 
                                        where GP_Role__r.GP_Role_Category__c = 'Admin'
                                       And GP_Active__c = true
                                      AND GP_Role__r.GP_Active__c =true];
                                     
    }
    
    public List<GP_User_Role__c> selectUserofResourceAllocationCategory(GP_Project__c projectObj) {
        return (List<GP_User_Role__c>)[Select ID, GP_Active__c,
                              GP_Role__c,GP_Role__r.GP_Role_Category__c, 
                              GP_Role__r.GP_Work_Location_SDO_Master__c,
                              GP_User__c,GP_User__r.Name 
                              from GP_User_Role__c 
                              where GP_Active__c = true 
                              AND GP_Role__r.GP_Role_Category__c ='ResourceAllocation' 
                              AND GP_Role__r.GP_Work_Location_SDO_Master__c = :projectObj.GP_Primary_SDO__c];
                                     
    }

    public List<GP_User_Role__c> selectUserofPIDCreationCategory(GP_Project__c projectObj) {
        return (List<GP_User_Role__c>)[Select ID, GP_Active__c,
                              GP_Role__c,GP_Role__r.GP_Role_Category__c, 
                              GP_Role__r.GP_Work_Location_SDO_Master__c,
                              GP_User__c,GP_User__r.Name 
                              from GP_User_Role__c 
                              where GP_Active__c = true 
                              AND GP_Role__r.GP_Role_Category__c ='PID Creation' 
                              AND GP_Role__r.GP_Work_Location_SDO_Master__c = :projectObj.GP_Primary_SDO__c];
                                     
    }
    
    public List<GP_User_Role__c> selectUserofAdminCategory() {
      Id loggedInUserId = UserInfo.getUserId();
      
        return [SELECT Id, GP_Role__c 
                FROM GP_User_Role__c 
                where GP_Role__r.GP_Role_Category__c = 'ADMIN' 
                and GP_User__c = :loggedInUserId 
                and GP_Active__c = true];
                                     
    }
    
    public List<GP_User_Role__c> selectUserRolesForAddUserRoles(Set<ID> SetofUserRoleId) {
        list<GP_User_Role__c> lstUserRole = [SELECT ID, Name, GP_User__c, GP_Role__r.GP_Active__c, 
                                             GP_Role__r.GP_Work_Location_SDO_Master__c, GP_Role__r.GP_Group_Name_for_Read_Access__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c,GP_Role__r.GP_Deal_Access_Group_Name__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Name__c,
                                             GP_Role__r.GP_Customer_L4__c, GP_Role__r.GP_Customer_L4__r.Name,
                                             GP_Role__r.GP_HSL_Master__c, GP_Role__r.GP_HSL_Master__r.name,
                                             GP_Role__r.GP_HSL_Master__r.GP_Unique_Number__c ,GP_Role__r.GP_Role_Category__c
                                             from GP_User_Role__c where 
                                             Id in: SetofUserRoleId and
                                             GP_Role__r.GP_Active__c = true];
        return lstUserRole;
                                     
    }
    
    public List<GP_User_Role__c> selectUserRolesForRemoveUserRoles(Set<ID> SetofUserRoleId) {
        list<GP_User_Role__c> lstUserRole = [SELECT ID, Name, GP_User__c, GP_Role__r.GP_Active__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__c, GP_Role__r.GP_Group_Name_for_Read_Access__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Name__c, 
                                             GP_Role__r.GP_Customer_L4__c,GP_Role__r.GP_HSL_Master__r.GP_Unique_Number__c,
                                             GP_Role__r.GP_Customer_L4__r.Name, GP_Role__r.GP_HSL_Master__c,GP_Role__r.GP_Role_Category__c, 
                                             GP_Role__r.GP_HSL_Master__r.Name,GP_Role__r.GP_Deal_Access_Group_Name__c 
                                             from GP_User_Role__c where 
                                             Id in: SetofUserRoleId];
     
        return lstUserRole;
                                     
    }
    public list<GP_User_Role__c> selectRolesforUser(set<Id> UserIds , set<string> setOfGroupName){
    	return [Select id,GP_User__c, GP_Role__r.GP_Group_Name_for_Read_Access__c,GP_Role__r.GP_Deal_Access_Group_Name__c 
    					from GP_User_Role__c where GP_Active__c = true and GP_Role__r.GP_Active__c =true and GP_User__c in : UserIds 
    					and (GP_Role__r.GP_Deal_Access_Group_Name__c in : setOfGroupName OR GP_Role__r.GP_Group_Name_for_Read_Access__c in :setOfGroupName)];
    }

    public static List<GP_User_Role__c> selectPIDCreatorRoles() {
        Id loggedInUserId = UserInfo.getUserId();
      
        list<GP_User_Role__c> lstUserRole = [SELECT Id,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Name__c,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.Name,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Description__c,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Unique_Name__c,
                                                GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c
                                                FROM GP_User_Role__c
                                                where GP_Role__r.GP_Role_Category__c = 'PID Creation'
                                                AND GP_Active__c = true
                                                AND GP_User__c =: loggedInUserId
                                            ];     
        return lstUserRole;
                                     
    } 
    
    public static List<GP_User_Role__c> selectLegacyPIDCreatorRoles() {
        Id loggedInUserId = UserInfo.getUserId();
        
        list<GP_User_Role__c> lstUserRole = [SELECT Id,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Name__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.Name,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Description__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Unique_Name__c,
                                             GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c
                                             FROM GP_User_Role__c
                                             where GP_Role__r.GP_Role_Category__c = 'PID Creation'
                                             AND GP_Active__c = true
                                             AND GP_User__c =: loggedInUserId
                                             AND(GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Legacy_Expiration_Date__c = null 
                                                 OR GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Legacy_Expiration_Date__c >= today)
                                             AND GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Status__c = 'Active and Visible'
                                            ];     
        return lstUserRole;
        
    } 
    
    public static List<GP_User_Role__c> selectPIDCreatorRolesForLegacyProjects() {
        Id loggedInUserId = UserInfo.getUserId();
        Date today = System.today();
        
        list<GP_User_Role__c> lstUserRole = [SELECT Id,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Legacy_Expiration_Date__c,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.Name,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Description__c,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Unique_Name__c,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Oracle_Id__c,
                                              GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Business_Type__c
                                              FROM GP_User_Role__c
                                              where GP_Role__r.GP_Role_Category__c = 'PID Creation'
                                              AND GP_Active__c = true
                                              AND GP_User__c =: loggedInUserId
                                              AND(GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Legacy_Expiration_Date__c = null 
                                              OR GP_Role__r.GP_Work_Location_SDO_Master__r.GP_Legacy_Expiration_Date__c >= today)
                                             ];  
        return lstUserRole;
    }
}