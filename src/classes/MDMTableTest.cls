/**********************************************************************************
@Original Author : Iqbal
@Modified By Author : Iqbal on 8 Apr 2019
@TL : Madhuri
@date Started : 26 Feb 2019
@Description : Test class to cover "MDMTableControllerV4" class.
@Parameters : 
@Return type :
**********************************************************************************/

@isTest
public class MDMTableTest {
public static Account oAccount;
    public static Contact oContact;
    public static Case oCase;
    public static Lead oLead;
    public static String IndustryVertical = 'Manufacturing';
    public static String title = 'Test';
    public static String buyingcenter = 'Digital';
    public static String levelcontract = 'CXO';
    public static String countrySelect = 'Albania';
    public static String accountArctye = 'test';
    public static String validreason = 'Test';
    public static String campaignName = 'Test';
    
    private static void setupTestData(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        system.debug('AccountID'+oAccount);
        
        
        oLead = new Lead(FirstName='Test',LastName='Test',Company='Test',Email='test@test.com',Country_Of_Residence__c='India',Status='Raw',Title='Test');
        insert oLead;
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        Id RecordTypeIdContact = Schema.SObjectType.Case.getRecordTypeInfosByName().get('MDM Console Report').getRecordTypeId();
        oCase = new Case(Title__c= 'Test' ,AccountId=oAccount.Id,Buying_Centre_multi__c='Digital',Level_Of_Contact__c='CXO',Country_Of_Residence__c='Albania',
                         Account_Archetype__c=oAT.Id,Please_provide_valid_reason_for_MDMcase__c='Test',Industry_Vertical_multi__c='Manufacturing',
                         RecordTypeId=RecordTypeIdContact,Campaign_Name__c='Test');
        insert oCase;
        
       /* Case__c oC = new Case__c();
        oC.PowerBi_Email__c='test@test.com';
        oC.Name = 'Test';
        insert oC;*/
        
    }
    

    //MDM TableV4
    @isTest public static void testMethod1()
    {setupTestData();
        test.startTest();
        
      MDMTableControllerV4.getData('BFS,CPG','CXO,CXO-1','Raw,Aware','Digital,IT','Priority,Growth','All','=10,=10','testTitle','Algeria,Albania');
     
     MDMTableControllerV4.getData('','','','','','All','','','');
     MDMTableControllerV4.getData('','','','','','','','testTitle','');
     MDMTableControllerV4.getData('BFS,CPG','','','','','','','','');
     MDMTableControllerV4.getData('','CXO,CXO-1','','','','','','','');
     MDMTableControllerV4.getData('','','Raw,Aware','','','','','','');
     MDMTableControllerV4.getData('','','','Digital,IT','','','','','');
     MDMTableControllerV4.getData('','','','','Priority,Growth','','','','');
     MDMTableControllerV4.getData('','','','','','','=10,=10','','');
     MDMTableControllerV4.getData('','','','','','','','','Algeria,Albania');
     
     MDMTableControllerV4.getData('BFS','CXO','Aware','Digital','Priority','All','= 10','testTitle,testtitle2','Algeria');
     MDMTableControllerV4.getData('BFS','CXO','Aware','Digital','Priority','All','=10,=50','testTitle','Algeria');
     MDMTableControllerV4.getData('BFS','CXO','Aware','Digital','Priority','All','=50,=80','testTitle','Algeria');
     
     MDMTableControllerV4.getData('','','','','','','= 10','','');
     MDMTableControllerV4.getData('','','','','','','=10,=50','','');
     MDMTableControllerV4.getData('','','','','','','=50,=80','','');
     
     MDMTableControllerV4.getFieldValues();
     
      MDMTableControllerV4.getDataCount('BFS,CPG','CXO,CXO-1','Raw,Aware','Digital,IT','Priority,Growth','All','=10,=10','testTitle','Algeria,Albania');
     MDMTableControllerV4.getDataCount('','','','','','All','','','');
	 MDMTableControllerV4.getDataCount('','','','','','','','testTitle','');
    
     
     MDMTableControllerV4.getDataCount('BFS,CPG','','','','','','','','');
     MDMTableControllerV4.getDataCount('','CXO,CXO-1','','','','','','','');
     MDMTableControllerV4.getDataCount('','','Raw,Aware','','','','','','');
     MDMTableControllerV4.getDataCount('','','','Digital,IT','','','','','');
     MDMTableControllerV4.getDataCount('','','','','Priority,Growth','','','','');
     MDMTableControllerV4.getDataCount('','','','','','','=10,=10','','');
     MDMTableControllerV4.getDataCount('','','','','','','','','Algeria,Albania');
     
     MDMTableControllerV4.getDataCount('BFS','CXO','Aware','Digital','Priority','All','=10','testTitle,testtitle2','Algeria');
     MDMTableControllerV4.getDataCount('BFS','CXO','Aware','Digital','Priority','All','=10,=50','testTitle','Algeria');
     MDMTableControllerV4.getDataCount('BFS','CXO','Aware','Digital','Priority','All','=50,=80','testTitle','Algeria');
     
     MDMTableControllerV4.getDataCount('','','','','','','=10','','');
     MDMTableControllerV4.getDataCount('','','','','','','=10,=50','','');
     MDMTableControllerV4.getDataCount('','','','','','','=50,=80','','');
     
     
     MDMTableControllerV4.DataExport('BFS,CPG','CXO,CXO-1','Raw,Aware','Digital,IT','Priority,Growth','All','=10,=10','testTitle','Algeria,Albania');
     MDMTableControllerV4.DataExport('','','','','','All','','','');
	 MDMTableControllerV4.DataExport('','','','','','','','testTitle','');
    
     
     MDMTableControllerV4.DataExport('BFS,CPG','','','','','','','','');
     MDMTableControllerV4.DataExport('','CXO,CXO-1','','','','','','','');
     MDMTableControllerV4.DataExport('','','Raw,Aware','','','','','','');
     MDMTableControllerV4.DataExport('','','','Digital,IT','','','','','');
     MDMTableControllerV4.DataExport('','','','','Priority,Growth','','','','');
     MDMTableControllerV4.DataExport('','','','','','','=10,=10','','');
     MDMTableControllerV4.DataExport('','','','','','','','','Algeria,Albania');
     
     MDMTableControllerV4.DataExport('BFS','CXO','Aware','Digital','Priority','All','=10','testTitle,testtitle2','Algeria');
     MDMTableControllerV4.DataExport('BFS','CXO','Aware','Digital','Priority','All','=10,=50','testTitle','Algeria');
     MDMTableControllerV4.DataExport('BFS','CXO','Aware','Digital','Priority','All','=50,=80','testTitle','Algeria');
     
     MDMTableControllerV4.DataExport('','','','','','','=10','','');
     MDMTableControllerV4.DataExport('','','','','','','=10,=50','','');
     MDMTableControllerV4.DataExport('','','','','','','=50,=80','','');
     
     
     
     test.stopTest();
    }
    
    
    
   
}