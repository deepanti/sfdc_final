//Created BY : mandeep.singh@saasfocus.com
@isTest
private class GPBatchtoExpiringProjectTracker 
{    
    @testSetup static void setupCommonData()
    {
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_FULL_NAME__c = 'EMP FULL NAME';
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        insert empObjwithsfdcuser;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_FULL_NAME__c = 'EMP FULL NAME';
        empObj.GP_Person_ID__c = 'EMP-002';
        empObj.GP_OFFICIAL_EMAIL_ADDRESS__c ='1234@abc.com';
        insert empObj; 
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        GP_Project__c prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_GPM_Employee__c = empObj.id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_End_Date__c = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Days_Prior_Project_End_Date));
        insert prjobj;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accobj.id, 'Status');
        leadershipMaster.GP_Type_of_Leadership__c = 'Billing Approver';
        insert leadershipMaster;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjobj.id, leadershipMaster.id);
        projectLeadership.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership.GP_Employee_ID__c = empObj.id;
        projectLeadership.GP_Leadership_Role__c = System.Label.GP_Billing_Approver_Role_Code;
        insert projectLeadership;
        
        GP_Project_Leadership__c projectLeadership2 = GPCommonTracker.getProjectLeadership(prjobj.id, leadershipMaster.id);
        projectLeadership2.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership2.GP_Employee_ID__c = empObjwithsfdcuser.id;
        projectLeadership2.GP_Leadership_Role__c = System.Label.GP_Billing_SPOC_Leadership_Role_Code;
        insert projectLeadership2;
        
        GP_Project_Leadership__c projectLeadership3 = GPCommonTracker.getProjectLeadership(prjobj.id, leadershipMaster.id);
        projectLeadership3.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership3.GP_Employee_ID__c = empObjwithsfdcuser.id;
        projectLeadership3.GP_Leadership_Role__c = System.Label.GP_FP_A_Approver;
        insert projectLeadership3;
        
        GP_Project_Leadership__c projectLeadership4 = GPCommonTracker.getProjectLeadership(prjobj.id, leadershipMaster.id);
        projectLeadership4.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership4.GP_Employee_ID__c = empObjwithsfdcuser.id;
        projectLeadership4.GP_Leadership_Role__c = System.Label.GP_GPM_Leadesrhip_Role_Code;
        insert projectLeadership4;
        
        GP_Project_Leadership__c projectLeadership5 = GPCommonTracker.getProjectLeadership(prjobj.id, leadershipMaster.id);
        projectLeadership5.RecordTypeId = Schema.SObjectType.GP_Project_Leadership__c.getRecordTypeInfosByName().get('Mandatory Key Members').getRecordTypeId();
        projectLeadership5.GP_Employee_ID__c = empObjwithsfdcuser.id;
        projectLeadership5.GP_Leadership_Role__c = System.Label.GP_PID_Approver_Leadership_Code;
        insert projectLeadership5;
    } 
    
    @isTest
    public static void testGPBatchtoExpiringProject()
    {
        testForbatch(); 
    }
    
    public static void testForbatch() {     
        Test.StartTest();
        
        GPBatchtoExpiringProject batcher = new GPBatchtoExpiringProject();
        batcher.priorDateValue = System.Today().adddays(integer.valueOf(system.label.GP_Initial_Days_Prior_Project_End_Date));
        //batcher.noOfDaysOfNotification = 7;
        Id batchprocessid = Database.executeBatch(batcher, 10);
        Test.StopTest();
    }
}