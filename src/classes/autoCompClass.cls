public class autoCompClass {
    @AuraEnabled
    public static list<String> autoCompleteCon(String sb)
    {
        system.debug('sb:' + sb);
        String token= '%' + sb + '%'; 
        List<Account> AccList = [Select Id, name from Account where name LIKE :token];
        List <Contact> lstOfCon = [select id, account.name ,name,firstName,LastName, title, (Select id, G_Designation__c,G_Connection_Owner__r.Name from G_Connectors__r) from contact where Id IN (SELECT contact__c from G_Connector__c WHERE contact__c != '') and name like :token ];
  		List <Lead> lstOfLead = [select id,name, firstName,LastName, title, (Select id, G_Designation__c,G_Connection_Owner__r.Name from G_Connectors__r) from Lead where Id IN (SELECT lead__c from G_Connector__c WHERE lead__c != '') and name like : token ];

        List<String> NameList = new List<String>();
        if(AccList != null && AccList.size() > 0)
        {
            for(Account acc : AccList)
            {
                NameList.add(string.valueOf(acc.Name));
            }
        }
        if(lstOfCon != null && lstOfCon.size() > 0)
        {
            for(contact acc : lstOfCon)
            {
                NameList.add(string.valueOf(acc.Name));
            }
        }
        if(lstOfLead != null && lstOfLead.size() > 0)
        {
            for(Lead acc : lstOfLead)
            {
                NameList.add(string.valueOf(acc.Name));
            }
        }
        if(NameList.size() > 0)
        {
            system.debug('NameList: '+ NameList);
             NameList.sort();
            return NameList;
        }
        else 
            Return new List<String>();
    }
    @AuraEnabled
    public static Map<ID,List<String>> searchString(String sb)
    {
        system.debug('sb:' + sb);
        String token= '%' + sb + '%'; 
        List<Account> AccList = [Select Id, Name from Account where name LIKE :token];
        List <Contact> lstOfCon = [select id, account.name ,name,firstName,LastName, title, (Select id, G_Designation__c,G_Connection_Owner__r.Name from G_Connectors__r) from contact where Id IN (SELECT contact__c from G_Connector__c WHERE contact__c != '') and name like :token ];
  		List <Lead> lstOfLead = [select id,name, firstName,LastName, title, (Select id, G_Designation__c,G_Connection_Owner__r.Name from G_Connectors__r) from Lead where Id IN (SELECT lead__c from G_Connector__c WHERE lead__c != '') and name like : token ];

        Map<ID,List<String>> keyMap = new Map<ID,List <String>>();
        if(AccList != null && AccList.size() > 0)
        {
            for(Account acc : AccList)
            {

                keyMap.put(acc.ID,new List<String> {string.valueOf(acc.Name), acc.Id.getSObjectType().getDescribe().getLabel() });
                //NameList.add(string.valueOf(acc.Name));
            }
        }
        if(lstOfCon != null && lstOfCon.size() > 0)
        {
            for(Contact acc : lstOfCon)
            {

                keyMap.put(acc.ID,new List<String> {string.valueOf(acc.Name), acc.Id.getSObjectType().getDescribe().getLabel() });
                //NameList.add(string.valueOf(acc.Name));
            }
        }
        if(lstOfLead != null && lstOfLead.size() > 0)
        {
            for(Lead acc : lstOfLead)
            {

                keyMap.put(acc.ID,new List<String> {string.valueOf(acc.Name), acc.Id.getSObjectType().getDescribe().getLabel() });
                //NameList.add(string.valueOf(acc.Name));
            }
        }
        if(keyMap.size() > 0)
        {
            system.debug('NameList: '+ keyMap);
            return keyMap;
        }
        else 
            return null;
            //Return new Map<ID,List<String>>();
            
    }

}