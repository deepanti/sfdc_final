@isTest
public class OpportunityDiscoverAddContactTest {
    
    @isTest 
    public static void testgetContactRolePicklist()
    {
        //testing picklist values
        OpportunityDiscoverAddContactController.getContactRolePicklist();   
    }
    @isTest 
    public static void testgetContactRoleRecordsAndsaveContactRole()
    {	
        //creating test Account
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        //creating Opportunity Records
        List<Opportunity> oppList=TestDataFactoryUtility.createTestOpportunitiesRecords(acc.Id, 1, 'Prediscover', system.today().addMonths(2));
        insert oppList;
        //creating Contact Role Records
        Contact_Role_Rv__c conRoleCreate=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleCreate;
        Contact_Role_Rv__c conRoleCreate2=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleCreate2;
        Contact_Role_Rv__c conRoleDelete=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleDelete;
        test.startTest();
        system.assertNotEquals(null, OpportunityDiscoverAddContactController.getContactRoleRecords(oppList[0].id));
        //Json data for apex method 
       
        string createList='[{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":false,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleCreate.Id+'"},{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":true,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleCreate2.Id+'"}]';
        string deleteList='[{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":false,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleDelete.Id+'"}]';
        system.assertEquals('Data Saved Successfully', OpportunityDiscoverAddContactController.saveContactRole(createList,oppList[0].id,deleteList));
        
        test.stopTest();
    }
     @isTest  public static void testDelete()
     {
         //creating test Account
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        //creating Opportunity Records
        List<Opportunity> oppList=TestDataFactoryUtility.createTestOpportunitiesRecords(acc.Id, 1, 'Prediscover', system.today().addMonths(2));
        insert oppList;
        //creating Contact Role Records
        Contact_Role_Rv__c conRoleCreate=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleCreate;
        Contact_Role_Rv__c conRoleCreate2=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleCreate2;
        Contact_Role_Rv__c conRoleDelete=TestDataFactoryUtility.createTestContactRole(oppList[0].id);
        insert conRoleDelete;
        //Json data for apex method 
       
        string createList='[{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":false,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleCreate.Id+'"},{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":true,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleCreate2.Id+'"}]';
        string deleteList='[{"sobjectType":"Contact_Role_Rv__c","Role__c":"Evaluator","IsPrimary__c":false,"Opportunity__c":"'+oppList[0].id+'","Id":"'+conRoleDelete.Id+'"}]';
       
         Profile profileName=[Select Name from Profile where Name='Read Only'];
         User testUser=TestDataFactoryUtility.createTestUser(profileName.Name, 'username@avshd.com','email@ghjh.com');
         insert testUser;
         test.startTest();
         system.runAs(testUser)
         {
           system.assertEquals('You Dont have delete access!',OpportunityDiscoverAddContactController.saveContactRole(createList,oppList[0].id,deleteList));
         }
         test.stopTest();
     }
    
}