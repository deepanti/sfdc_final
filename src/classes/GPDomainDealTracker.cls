// APC Change  
@isTest
public class GPDomainDealTracker {
    
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj = new GP_Project__c();
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    
    public static void buildDependencyData() {
        
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_deal__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;  
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        /*GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Access_Group_Name__c = 'test';
        insert dealObj ;*/
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Access_Group_Name__c = 'test';
        dealObj.GP_Business_Group_L1__c = 'test';
        dealObj.GP_Business_Segment_L2__c = 'test';
        dealObj.GP_Sub_Business_L3__c = 'test';
        dealObj.GP_Account_Name_L4__c = accountObj1.id;
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Approval_Status__c = 'Draft';
        insert prjObj ;
        
        dealObj.GP_Deal_Access_Group_Name__c = 'test1';
        dealObj.GP_Business_Group_L1__c = 'test1';
        dealObj.GP_Business_Segment_L2__c = 'test1';
        dealObj.GP_Sub_Business_L3__c = 'test1';
        dealObj.GP_Account_Name_L4__c = accountObj.id;
        update dealObj ;
        
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        insert leadershipMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_SFDC_User__c = objuser.id;
        insert empObj;
        
        GP_Budget_Master__c objPrjBdgtMaster = GPCommonTracker.getBudgetMaster();
        insert objPrjBdgtMaster;
        
        GP_Project_Budget__c objPrjBdgt = GPCommonTracker.getProjectBudget(prjObj.Id, objPrjBdgtMaster.Id);
        insert objPrjBdgt;
        
        GP_Project_Expense__c projectExpense = GPCommonTracker.getProjectExpense(prjObj.Id);
        insert projectExpense;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        insert objProjectWorkLocationSDO;
        
        GP_Billing_Milestone__c billingMilestone = GPCommonTracker.getBillingMilestone(prjObj.Id, objSdo.Id);
        insert billingMilestone;
        
        GP_Project_Document__c objDoc = GPCommonTracker.getProjectDocument(prjObj.Id);
        insert objDoc;
        
        GP_Deal__c ObjDeal3  = new GP_Deal__c();
        ObjDeal3.GP_Business_Name__c = 'CMITS';
        ObjDeal3.GP_Business_Type__c = 'BPM';
        insert ObjDeal3;
        
        Product2 objProduct = GPCommonTracker.getProduct();
        insert objProduct;
        
        Nature_of_Work__c objNOW = new Nature_of_Work__c();
        objNOW.Name = 'TestNOW';
        insert objNOW;
        
        Product_family__c objProductFamily = new Product_family__c();
        objProductFamily.Name = 'TestPF';
        objProductFamily.IsActive__c = True;
        objProductFamily.Product_Group__c = 'Group';
        insert objProductFamily;
        
        Service_Line__c objServiceLine = new Service_Line__c();
        objServiceLine.Name = 'TestSL';
        insert objServiceLine;
        
        Product_Catalogue1__c objProductCatalogue = new Product_Catalogue1__c();
        objProductCatalogue.IsActive__c = true;
        objProductCatalogue.Nature_of_Work__c = objNOW.id;
        objProductCatalogue.Product_family1__c = objProductFamily.id;
        objProductCatalogue.Product_new__c = objProduct.id;
        objProductCatalogue.Service_Line1__c = objServiceLine.Id;
        insert objProductCatalogue;
        
        GP_Deal__c ObjDeal2  = new GP_Deal__c();
        ObjDeal2.Name = 'TestDeal';
        ObjDeal2.GP_Nature_of_Work1_ID__c = objNOW.Id;
        ObjDeal2.GP_Service_Line1_ID__c = objServiceLine.Id;
        ObjDeal2.GP_Product_Family1_ID__c = objProductFamily.Id;
        ObjDeal2.GP_Product_Id__c = objProduct.Id;
        insert ObjDeal2;
        ObjDeal2.Name = 'TestDeal1';
        update ObjDeal2;
        
        GP_Deal__c ObjDeal  = new GP_Deal__c();
        insert ObjDeal;  
        //update ObjDeal ; 
        
        GP_Deal__c deal1Obj = GPCommonTracker.getDeal();
        deal1Obj.id = dealObj.id;
        deal1Obj.GP_Expense_Form_OMS__c = '[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        deal1Obj.GP_Budget_From_OMS__c ='[{"attributes": { "type": "GP_Project_Expense__c","url": "/services/data/v41.0/sobjects/GP_Project_Expense__c/a2J0l00000066ccEAA" }, "GP_Project__c":"a230l0000008hWFAAY", "GP_Expense_Category__c": "Communications", "GP_Expenditure_Type__c": "Billable", "GP_Location__c": "OffShore", "GP_Amount__c": 120.00, "GP_Remark__c": "test", "GP_OMS_ID__c": "12345", "GP_IsSoftDeleted__c": false, "CurrencyIsoCode": "USD" }]';
        update deal1Obj;
        
        GP_Project__c projectObjForIcon = new GP_Project__c();
        projectObjForIcon = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        projectObjForIcon.OwnerId = objuser.Id;
        projectObjForIcon.GP_Customer_Hierarchy_L4__c = accountObj.id;
        projectObjForIcon.GP_HSL__c = objpinnacleMaster.id;
        projectObjForIcon.GP_Deal__c = dealObj.id;
        projectObjForIcon.GP_CRN_Number__c = objIconMaster.id;
        projectObjForIcon.GP_Clone_Source__c = 'system';
        // projectObjForIcon.GP_PID_Creator_Role__c = objroleforShare.id;
        insert projectObjForIcon;
        
        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id); 
        projectLeadership.GP_End_Date__c = system.today();
        projectLeadership.GP_Project__c = projectObjForIcon.id;
        insert projectLeadership;
    }
    
    public static void buildDependencyData2() {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_deal__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Icon_Master__c objIconMaster = new GP_Icon_Master__c();
        objIconMaster.GP_CONTRACT__c = 'Test Contract';
        objIconMaster.GP_Status__C = 'Expired';
        objIconMaster.GP_END_DATE__c = Date.newInstance(2017, 6, 1);
        objIconMaster.GP_START_DATE__c =  Date.newInstance(2017, 6, 20);
        insert objIconMaster;    
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objpinnacleMasterForShare = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMasterForShare ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser;  
        
        Account accountObj1 = GPCommonTracker.getAccount(null, null);
        insert accountObj1;
        
        Account accountObj = GPCommonTracker.getAccount(null, null);
        insert accountObj;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        /*GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Access_Group_Name__c = 'test';
        insert dealObj ;*/
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Delivery_Org__c = 'Delivery Org';
        dealObj.GP_Deal_Type__c = 'CMITS';
        dealObj.GP_Deal_Access_Group_Name__c = 'test';
        dealObj.GP_Business_Group_L1__c = 'test';
        dealObj.GP_Business_Segment_L2__c = 'test';
        dealObj.GP_Sub_Business_L3__c = 'test';
        dealObj.GP_Account_Name_L4__c = accountObj1.id;
        insert dealObj ;
        
        GP_OMS_Detail__c oms = new GP_OMS_Detail__c();
        oms.GP_Deal__c = dealObj.Id;
        oms.GP_OMS_Deal_ID__c = '1234';
        oms.GP_Expense_Type__c = 'Non Billable ODC';
        oms.GP_Expense_Amount__c = 207.50;
        insert oms;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;
        
        prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Deal__c = dealObj.id;
        prjObj.GP_Approval_Status__c = 'Approved';
        insert prjObj ;
        
        dealObj.GP_Deal_Access_Group_Name__c = 'test1';
        dealObj.GP_Business_Group_L1__c = 'test1';
        dealObj.GP_Business_Segment_L2__c = 'test1';
        dealObj.GP_Sub_Business_L3__c = 'test1';
        dealObj.GP_Account_Name_L4__c = accountObj.id;
        dealObj.GP_OMS_Deal_ID__c = '1234';
        update dealObj ;
    }
    
    @isTest
    public static void testGPDomainDeal() 
    {
       buildDependencyData();
    }
    
    @isTest
    public static void testGPDomainDeal2() 
    {
       buildDependencyData2();
    }
}