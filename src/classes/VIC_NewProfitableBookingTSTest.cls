@isTest
public class VIC_NewProfitableBookingTSTest 
{
Public static user objuser;
Public static user objuser1;
Public static user objuser2;
Public static user objuser3;
Public static OpportunityLineItem  objOLI;
Public static Opportunity objOpp;    
    
static testmethod void ValidateVIC_NewProfitableBookingTS()
{
LoadData();
VIC_NewProfitableBookingTS obj = new VIC_NewProfitableBookingTS();
obj.calculate(objOLI,'BDE'); 
obj.calculateBDE(objOLI);
obj.calculateCapitalMarketGRM(objOLI);
obj.calculateEnterpriseGRM(objOLI);
System.AssertEquals(200,200);

}
static void LoadData()
{

objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
insert objuser;

objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='BDE';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;

objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=1;
objOLI.Y1_AOI_USD_OLI_CPQ__c='194478';   
objOLI.Product_BD_Rep__c=objuser.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id;   
objOLI.vic_Is_Split_Calculated__c=false;
objOLI.vic_CPQ_Deal_Status__c= 'Active';  
objOLI.vic_TCV_EBIT_Consideration_ForNPV__c='5';
objOLI.vic_IO_TS__c='IO';
insert objOLI;

objOLI=[select id,vic_is_Eligible_For_NPV_Calculation__c,VIC_NPV__c,vic_IO_TS__c from opportunitylineitem where id=:objOLI.id];
system.debug('objOLI'+objOLI);

Target__c targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
insert targetObj;

Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_IO'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;
    
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
insert PlanComponentObj;
    
Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
targetComponentObj.Target__C=targetObj.id;
targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
insert targetComponentObj;

Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
insert objInc;
    
system.debug('targetComponentObj'+targetComponentObj);
system.debug('objuser'+objuser);    
    



}    
}