@isTest
public class GPCmpServiceInitiateJobProcessTracker {

    public static GP_Job__c objJob;
    public static String jsonresp;
    
    public static Map < String, String > mapOfJobTypeVsColumToBeUpdated = new Map < String, String > 
    {
        'Upload Time Sheet Entries' => 'GP_PL_Employee_OHR__c;GP_TSC_Ex_Type__c;GP_TSC_Modified_Hours__c;GP_Project__c;GP_TSC_Project_Task__c;GP_TSC_Date__c',
        'Update Project Work Location & SDOs' => 'GP_PWL_BCP_Requirement__c;GP_PWL_Primary__c;GP_Project__c;GP_PWL_Work_Location__c',
        'Update Project Leadership' => 'GP_PL_Employee_OHR__c;GP_PL_End_Date__c;GP_PL_Leadership_Role__c;GP_PL_Start_Date__c;GP_Project__c',
        'Update Project' => 'GP_PRJ_Attn_To_Email__c;GP_PRJ_Attn_To_Name__c;GP_PRJ_Billing_Currency__c;GP_PRJ_Business_Hierarchy_L4__c;GP_PRJ_CC1__c;GP_PRJ_CC2__c;GP_PRJ_CC3__c;GP_PRJ_Customer_Contact_Number__c;GP_PRJ_Customer_SPOC_Email__c;GP_PRJ_Customer_SPOC_Name__c;GP_PRJ_Fax_Number__c;GP_PRJ_HSL__c;GP_PRJ_MOD__c;GP_PRJ_Nature_of_Work__c;GP_PRJ_Portal_Name__c;GP_PRJ_Primary_SDO__c;GP_PRJ_Product__c;GP_PRJ_Project_Description__c;GP_PRJ_Project_Long_Name__c;GP_PRJ_Name__c;GP_PRJ_Project_Type__c;GP_PRJ_Service_Line__c;GP_PRJ_SILO__c;GP_PRJ_Start_Date__c;GP_PRJ_End_Date__c;GP_Project__c'
    };

    @testSetup static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_Final_OHR__c = '123456778';
        empObj.GP_Person_Id__c = '123456778';
        insert empObj;
        
        GP_Product_Master__c pm = new GP_Product_Master__c();
        pm.Name = 'Product Name';
        pm.GP_Is_Active__c = true;
        pm.GP_Product_Name__c = 'Product Name';
        pm.GP_Product_ID__c = 'Product Id';
        pm.GP_Nature_of_Work__c = 'Nature of Work';
        pm.GP_Industry_Vertical__c = 'Vertical';
        pm.GP_Service_Line__c = 'Service Line';        
        insert pm;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Oracle_Id__c = '1234';
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Work_Location__c objworkLocation = GPCommonTracker.getWorkLocation();
        objworkLocation.GP_Oracle_Id__c = '5678';
        objworkLocation.GP_Status__c = 'Active and Visible';
        objworkLocation.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation;
        
        GP_Work_Location__c objworkLocation2 = GPCommonTracker.getWorkLocation();
        objworkLocation2.GP_Oracle_Id__c = '9012';
        objworkLocation2.GP_Status__c = 'Active and Visible';
        objworkLocation2.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId();
        insert objworkLocation2;

        GP_Pinnacle_Master__c objPinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        DateTime entryDate = system.today().addMonths(3);
        String monthYear = entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        objpinnacleMaster.GP_Latest_Open_PA_Period__c = monthYear;
        insert objpinnacleMaster ;
        
        GP_Pinnacle_Master__c objPinnacleMasterSilo = GPCommonTracker.GetpinnacleMaster();        
        objPinnacleMasterSilo.GP_Oracle_Id__c = 'silo';
        insert objPinnacleMasterSilo;
        
        GP_Pinnacle_Master__c objPinnacleMasterHSL = GPCommonTracker.GetpinnacleMaster();        
        objPinnacleMasterHSL.GP_Oracle_Id__c = 'hsl';
        insert objPinnacleMasterHSL;
        
        GP_Pinnacle_Master__c objPinnacleMasterPortalName = GPCommonTracker.GetpinnacleMaster();        
        objPinnacleMasterPortalName.GP_Oracle_Id__c = 'Portal Name';
        insert objPinnacleMasterPortalName;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objPinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Without SFDC';
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_TCV__c = 10;
        prjObj.GP_Vertical__c = 'Vertical';
        prjObj.GP_Start_Date__c = system.today();
        prjObj.GP_End_Date__c = system.today().adddays(10);
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        insert prjObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.Id);
        insert objSB;

        Account accountObj = GPCommonTracker.getAccount(objBS.Id, objSB.Id);
        //accountObj.Business_Group__c = 'Global Client';
        insert accountObj;

        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster.GP_Leadership_Role__c = 'Billing SPOC';
        insert leadershipMaster;

        GP_Project_Leadership__c projectLeadership = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership.GP_Leadership_Role_Name__c = 'Billing SPOC';
        projectLeadership.GP_Start_Date__c = system.today();
        projectLeadership.GP_Active__c = true;
        insert projectLeadership;
        
        GP_Leadership_Master__c leadershipMaster1 = GPCommonTracker.getLeadership(accountObj.Id, 'Active');
        leadershipMaster1.GP_Leadership_Role__c = 'Global Project Manager';
        insert leadershipMaster1;

        GP_Project_Leadership__c projectLeadership1 = GPCommonTracker.getProjectLeadership(prjObj.Id, leadershipMaster.Id);
        projectLeadership1.GP_Leadership_Role_Name__c = 'Global Project Manager';
        projectLeadership1.GP_Start_Date__c = system.today();
        projectLeadership1.GP_Active__c = true;
        insert projectLeadership1;

        GP_Project_Work_Location_SDO__c objProjectWorkLocationSDO = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objSdo.Id);
        objProjectWorkLocationSDO.GP_Primary__c = true;
        objProjectWorkLocationSDO.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId(); 
        insert objProjectWorkLocationSDO;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation.Id);
        objProjectWorkLocation.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation;
        
        GP_Project_Work_Location_SDO__c objProjectWorkLocation1 = GPCommonTracker.getProjectWorkLocationSDO(prjObj.Id, objworkLocation2.Id);
        objProjectWorkLocation1.RecordTypeId = Schema.SObjectType.GP_Project_Work_Location_SDO__c.getRecordTypeInfosByName().get('Work Location').getRecordTypeId(); 
        insert objProjectWorkLocation1;
        
        GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        projectTaskObj1.GP_Active__c = true;
        projectTaskObj1.GP_Task_Number__c = 'ORACLE-001';
        projectTaskObj1.GP_Project_Number__c  = '4532';
        projectTaskObj1.GP_Start_Date__c = system.today().addMonths(3);
        insert projectTaskObj1;
        
        GP_Resource_Allocation__c objResourceAllocation = GPCommonTracker.getResourceAllocation(empObj.id, prjObj.id);
        objResourceAllocation.GP_Project__c = prjObj.id;
        objResourceAllocation.GP_Bill_Rate__c = 0;
        objResourceAllocation.GP_Employee__c = empObj.id;
        objResourceAllocation.GP_Work_Location__c = objSdo.id;
        objResourceAllocation.GP_Start_Date__c = system.today().addMonths(3);
        objResourceAllocation.GP_End_Date__c = system.today().addMonths(4);
        objResourceAllocation.GP_Percentage_allocation__c = 1;
        objResourceAllocation.GP_SDO__c = objSdo.Id;
        insert objResourceAllocation;
        
        GP_Pinnacle_Master__c objpinnacleMasterGlobalSetting = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        objpinnacleMasterGlobalSetting.GP_TimeSheet_Financial_Month__c = system.today().addMonths(4);
        insert objpinnacleMasterGlobalSetting ;

        GP_Job__c objJobForUpdateProject = getJobRecord('Update Project');
        insert objJobForUpdateProject;

        GP_Job__c objJobForTimeSheetUpload = getJobRecord('Upload Time Sheet Entries');
        insert objJobForTimeSheetUpload;

        GP_Job__c objJobForUpdateWorkLocation = getJobRecord('Update Project Work Location & SDOs');
        insert objJobForUpdateWorkLocation;

        GP_Job__c objJobForUpdateProjectLeadership = getJobRecord('Update Project Leadership');
        insert objJobForUpdateProjectLeadership;
        
        prjObj.GP_Oracle_PID__c = '4532';
        prjObj.GP_Approval_Status__c = 'Approved';
        prjObj.GP_OMS_Status__c = 'Approved';
        prjObj.GP_Current_Working_User__c = UserInfo.getUserId();
        prjObj.GP_Oracle_Status__c = 'S';
        update prjObj;
         
        GP_Timesheet__c timesheetRecord = new GP_Timesheet__c();
        timesheetRecord.GP_Person_Id__c = '123456778';
        timesheetRecord.GP_Employee_Number__c = '123456778';
        timesheetRecord.GP_Project_Number__c = '4532';
        timesheetRecord.GP_Task_Number__c = 'ORACLE-001';
        //DateTime entryDate = system.today().addMonths(3);
        //String monthYear = entryDate.format('MMM') + '-' +  String.ValueOf(entryDate.year()).substring(2, 4);
        timesheetRecord.GP_Month_Year__c = monthYear;
        timesheetRecord.GP_Expenditure_Item_Date__c = system.today().addMonths(3);
        timesheetRecord.GP_Expenditure_Type__c = 'Billable';
        timesheetRecord.GP_Hours__c = 7;
        timesheetRecord.GP_PersonId_MonthYear__c = timesheetRecord.GP_Person_Id__c + monthYear;
        insert timesheetRecord;

        GP_Temporary_Data__c objTemporaryTimeSheet = GPCommonTracker.getTemporaryDataForTimeSheet(objJobForTimeSheetUpload.Id,
            '123456778',
            '4532',
            'ORACLE-001');
        objTemporaryTimeSheet.GP_TSC_Date__c  = system.today().addMonths(3);
        insert objTemporaryTimeSheet;

        GP_Temporary_Data__c objTemporaryProject = GPCommonTracker.getTemporaryDataForProject(objJobForUpdateProject.Id,
            '4532');        
        //objTemporaryProject.GP_PRJ_Business_Hierarchy__c = 'Global Client';
        insert objTemporaryProject;

        GP_Temporary_Data__c objTemporaryProjectWorkLocation = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
            '4532',
            objworkLocation.GP_Oracle_Id__c);
        insert objTemporaryProjectWorkLocation;
        
         GP_Temporary_Data__c objTemporaryProjectWorkLocation2 = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
            '4532',
            objworkLocation2.GP_Oracle_Id__c);
        insert objTemporaryProjectWorkLocation2;
        
        GP_Temporary_Data__c objTemporaryProjectWorkLocation3 = GPCommonTracker.getTemporaryDataForProjectWorkLocation(objJobForUpdateWorkLocation.Id,
            '4532',
            '3452');
        insert objTemporaryProjectWorkLocation3;

        GP_Temporary_Data__c objTemporaryProjectLeadership = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
            '4532', 'Billing SPOC');
        objTemporaryProjectLeadership.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership.GP_PL_Leadership_Role__c = 'Billing SPOC';
        insert objTemporaryProjectLeadership;
        
        GP_Temporary_Data__c objTemporaryProjectLeadership2 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
            '4532', 'Global Project Manager');
        objTemporaryProjectLeadership2.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership2.GP_PL_Leadership_Role__c = 'Global Project Manager';
        objTemporaryProjectLeadership2.GP_PL_Employee_OHR__c = '123456778';
        insert objTemporaryProjectLeadership2;
        
        GP_Temporary_Data__c objTemporaryProjectLeadership3 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
            '4532', 'PL');
        objTemporaryProjectLeadership3.GP_PL_Start_Date__c = system.today().addDays(2);
        objTemporaryProjectLeadership3.GP_PL_Leadership_Role__c = 'PL';
        objTemporaryProjectLeadership3.GP_PL_Employee_OHR__c = '123456778';
        insert objTemporaryProjectLeadership3;        
               
        /*GP_Temporary_Data__c objTemporaryProjectLeadership1 = GPCommonTracker.getTemporaryDataForProjectLeaderShip(objJobForUpdateProjectLeadership.Id,
        '4531', 'Billing SPOC');
        objTemporaryProjectLeadership1.GP_PL_Start_Date__c = system.today().addDays(2);
        insert objTemporaryProjectLeadership1;*/
    }

    @isTest static void testGPCmpServiceInitiateJobProcessForTimeSheet() {
        fetchData('Upload Time Sheet Entries');
        testInitiateJob();
    }

    @isTest static void testGPCmpServiceInitiateJobProcessForProject() {
        fetchData('Update Project');
        
        Account accountObj = [Select id from Account where name = 'TestAccount' limit 1];
        Business_Segments__c objBS = [Select id from Business_Segments__c where name = 'Test' limit 1];
        GP_Temporary_Data__c objTemporaryProject = [Select id from GP_Temporary_Data__c where GP_Job_Id__c = : objJob.id limit 1];
        Sub_Business__c objSB = [Select id from Sub_Business__c where name = 'Test' limit 1];
        
        objTemporaryProject.GP_PRJ_HSL__c = 'hsl';
        objTemporaryProject.GP_PRJ_Primary_SDO__c = '1234';
        objTemporaryProject.GP_PRJ_SILO__c = 'silo';
        objTemporaryProject.GP_PRJ_Portal_Name__c = 'Portal Name';
        objTemporaryProject.GP_PRJ_Product__c = 'Product Name';
        //objTemporaryProject.GP_PRJ_Product_Id__c = 'Product Id';
        objTemporaryProject.GP_PRJ_Nature_of_Work__c = 'Nature of Work';
        objTemporaryProject.GP_PRJ_Service_Line__c = 'Service Line';
        objTemporaryProject.GP_PRJ_Business_Hierarchy_L4__c = accountObj.id;
        //objTemporaryProject.GP_PRJ_Business_Hierarchy_L3__c = objSB.Id;
        //objTemporaryProject.GP_PRJ_Business_Hierarchy_L2__c = objBS.Id;
        
        update objTemporaryProject;
        
        testInitiateJob();
    }
    
    @isTest static void testBatchProcessProjectUpdateForProjectWithBlankData() {
        fetchData('Update Project');
        testInitiateJob();
    }
    
    @isTest static void testGPCmpServiceInitiateJobProcessForProjectWorkLocation() {
        fetchData('Update Project Work Location & SDOs');
        testInitiateJob();
    }
    
    @isTest static void testGPCmpServiceInitiateJobProcessForProjectLeaderShip() {
        fetchData('Update Project Leadership');
        testInitiateJob();
    }

    public static void fetchData(String jobType) {
        objJob = [select id from GP_Job__c where GP_Job_Type__c =: jobType limit 1];
    }

    public static void testInitiateJob() {
        GPAuraResponse returnedResponse = GPCmpServiceInitiateJobProcess.queueJob(objJob.id);
        System.assertEquals(true, returnedResponse.isSuccess);
    }

    @isTest static void testNoRecords() {
        GPAuraResponse returnedResponse = GPCmpServiceInitiateJobProcess.queueJob(null);
        System.assertEquals(false, returnedResponse.isSuccess);
    }
    
    @isTest static void testBatchConstructor() {
        fetchData('Update Project Leadership');
        GPBatchProcessProjectLeadershipUpdate leaderShipBatch = new GPBatchProcessProjectLeadershipUpdate(objJob.id);
    }
    
    @isTest static void testBatchProcessProjectWorkLocationUpdate(){
        fetchData('Update Project');
        GPBatchProcessProjectWorkLocationUpdate obj = new GPBatchProcessProjectWorkLocationUpdate(objJob.Id);
    }
    
    public static GP_Job__c getJobRecord(String jobType) {
        GP_Job__c objJob = new GP_Job__c();
        objJob.GP_Job_Type__c = jobType;
        objJob.GP_Column_s_To_Update_Upload__c = mapOfJobTypeVsColumToBeUpdated.get(jobType);
        return objJob;
    }
}