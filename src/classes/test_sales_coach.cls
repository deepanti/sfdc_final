@isTest
 Public class test_sales_coach{
 
 static testMethod void method()
    {    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
            insert u;
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
            insert salesunitobject;    
        
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
            insert accountobject;
      
       
        opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
        insert opp;
        Contact oContact =new Contact(firstname='Manoj',lastname='Pandey',accountid=accountobject.Id,Email='test1@gmail.com');
        insert oContact;
        OpportunityContactRole oppcontactrole=new OpportunityContactRole(Opportunityid=opp.id,IsPrimary=True,ContactId=oContact.Id);
        insert oppcontactrole;
        Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
         insert oProduct;
        OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
        insert OLi;
        test.stopTest();
       // system.assertEquals( opp.Service_Line__c,'Multi Channel Customer Service,');
        //system.debug('value check '+opp.Service_Line__c);
       
        
    }
    
 static testMethod void method2()
    {   
        test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test111',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunity);
        sales_Coach  Scoach= new sales_Coach(std);
        Scoach.SaveChecklist();
        Scoach.coverage();
             Test.stoptest();
     
        }
        
        
 static testMethod void method3()
    {   
        test.starttest();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','testingcontact@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test111',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        
        ApexPages.StandardController std = new ApexPages.StandardController(oOpportunity);
        sales_Coach  Scoach= new sales_Coach(std);
        
        System.runas(u){
        try{
                Contact acc =new Contact();
                insert acc;
             }
             
         Catch(Exception e){
          if(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'))
                         ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please fill all the required information in closure section.'));
                     else 
                         ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getStackTraceString() ));
         
         } 
         
         }   
             Test.stoptest();
     
        }       
        
 }