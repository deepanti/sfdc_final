@isTest
public class GPSelectorRoleTracker {
    @isTest
    public static void testselectRoleRecord() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GPSelectorRole selector = new GPSelectorRole();
        selector.getSObjectFieldList();
        selector.getSObjectType();
        selector.selectById(new Set<Id> {objrole.Id});
        selector.selectByRoleId(new Set<Id> {objrole.Id});
        selector.selectByHSL(null);
        selector.selectByRoleDealAccess(null);
        selector.selectByHSL(null);
        selector.selectBySDO(null);
        selector.selectByDealAccess(null);
    }
}