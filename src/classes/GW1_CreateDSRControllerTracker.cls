@isTest
public class GW1_CreateDSRControllerTracker
{
    private static Account objaccount;
    private static Opportunity objOpportunity;
    private static QSRM__c objQsrm;
    private static OpportunityProduct__c objproduct;
    private static user objuser;
    
    private static void Loaddata(boolean isDSRCreated, boolean dontCreateDSR, boolean dsrCreatedManually)
    {       
        GW1_CommonTracker.createTriggerCustomSetting();
        objaccount = GW1_CommonTracker.createAccount('abcd');
        objaccount.Industry_Vertical__c = 'BFS';
        update objaccount;
        objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', isDSRCreated, dontCreateDSR,dsrCreatedManually, objAccount.Id);
        objproduct = GW1_CommonTracker.createOppProduct(objOpportunity,'Delivery'); // 
        objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
    }
    private static void Loaddata_withoutProduct(boolean isDSRCreated, boolean dontCreateDSR, boolean dsrCreatedManually)
    {       
        GW1_CommonTracker.createTriggerCustomSetting();
        objaccount = GW1_CommonTracker.createAccount('abcd');
        objOpportunity = GW1_CommonTracker.createOpportunity('Opp1', '1. Discover', 'Partner', isDSRCreated, dontCreateDSR,dsrCreatedManually, objAccount.Id);
        //objproduct = GW1_CommonTracker.createOppProduct(objOpportunity,'Delivery'); // 
        objQsrm = GW1_CommonTracker.createQSRM(objOpportunity.Id);
    }
    
    static testMethod void testScenario5()
    {
        Loaddata(false,false,false);
        
        
        Test.startTest();
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    
    static testMethod void testScenario6()
    {
        Loaddata(false,false,false);
        
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Nature_of_Work__c='Consulting',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Consulting',Tower_Lead_Name__c='Test');
        Test.startTest();
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Consulting';
        objOpportunity.Product_Family__c='Consulting';
        update objOpportunity;
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        Validator_cls.AllowGenTUpdateAccounArche =true;
        objController.createDSR();
        Test.stoptest();
    }
    
        static testMethod void testScenario7()
    {
        Loaddata(false,false,false);
        
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Nature_of_Work__c='Delivery',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Consulting',Tower_Lead_Name__c='Test');
        Test.startTest();
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Delivery';
        objOpportunity.Product_Family__c='Consulting';
        update objOpportunity;
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        Validator_cls.AllowGenTUpdateAccounArche =true;
        objController.createDSR();
        Test.stoptest();
    }
    
            static testMethod void testScenario8()
    {
        Loaddata(false,false,false);
        Test.startTest();
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Nature_of_Work__c='Delivery',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Consulting',Tower_Lead_Name__c='Test');
        insert tlmapping;

        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Delivery';
        objOpportunity.Product_Family__c='Consulting';
        update objOpportunity;
        String s= objOpportunity.Name + objOpportunity.Opportunity_ID__c;
        CollaborationGroup cg= new CollaborationGroup(name= s +' Leadership roles');
        GW1_DSR_Group__c dg= new GW1_DSR_Group__c(name='Test',GW1_Chatter_Group_Id__c=cg.Id);
        GW1_DSR__c dsr= new GW1_DSR__c(Name=s,GW1_Account__c=objaccount.id,GW1_Opportunity__c=objOpportunity.id);
        Test.stoptest();
    }
    
            static testMethod void testScenario9()
    {
        Loaddata(false,false,false);
        Test.startTest();
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Nature_of_Work__c='Consulting',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Consulting',Tower_Lead_Name__c='Test');

        insert tlmapping;
        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Consulting';
        objOpportunity.Product_Family__c='Consulting';
        objOpportunity.Sales_Region__c='Asia';
        update objOpportunity;
        String s= objOpportunity.Name + objOpportunity.Opportunity_ID__c;
        CollaborationGroup cg= new CollaborationGroup(name= s +' Leadership roles');
        GW1_DSR_Group__c dg= new GW1_DSR_Group__c(name='Test',GW1_Chatter_Group_Id__c=cg.Id);
        GW1_DSR__c dsr= new GW1_DSR__c(Name=s,GW1_Account__c=objaccount.id,GW1_Opportunity__c=objOpportunity.id);
        Test.stoptest();
    }
            static testMethod void testScenario10()
    {
        Loaddata(false,false,false);
        
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Delivery',Tower_Lead_Name__c='Test');
        Test.startTest();
        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Delivery';
        objOpportunity.Product_Family__c='Delivery';
        objOpportunity.Sales_Region__c='Asia';
        update objOpportunity;
        String s= objOpportunity.Name + objOpportunity.Opportunity_ID__c;
        CollaborationGroup cg= new CollaborationGroup(name= s +' Leadership roles');
        GW1_DSR_Group__c dg= new GW1_DSR_Group__c(name='Test',GW1_Chatter_Group_Id__c=cg.Id);
        GW1_DSR__c dsr= new GW1_DSR__c(Name=s,GW1_Account__c=objaccount.id,GW1_Opportunity__c=objOpportunity.id);
        Test.stoptest();
    }


    /*
static testMethod void testScenario1()
{
Loaddata(false,false,false);


Test.startTest();
objqsrm.Status__c = 'Approved';
update objqsrm;
objOpportunity.GW1_DSR_Created__c = false;
update objOpportunity;
PageReference pageRef = Page.GW1_CreateDSR;
Test.setCurrentPage(pageRef);
ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
objController.createDSR();
Test.stoptest();
}
*/
    static testMethod void testScenario2()
    {
        Loaddata(false,true,false);
        
        
        Test.startTest();
        objqsrm.Status__c = 'Approved';
        update objqsrm;
        objOpportunity.GW1_DSR_Created__c = false;
        update objOpportunity;
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    static testMethod void testScenario3()
    {
        Loaddata(false,false,true);
        
        
        Test.startTest();
        objqsrm.Status__c = 'Approved';
        update objqsrm;
        objOpportunity.GW1_DSR_Created__c = false;
        update objOpportunity;
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    static testMethod void testScenario4()
    {
        Loaddata(false,true,false);
        
        
        Test.startTest();
        
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    
    /*  static testMethod void DsralreadyCreated()
{
Loaddata(false,false,false);


Test.startTest();
objqsrm.Status__c = 'Approved';
update objqsrm;

PageReference pageRef = Page.GW1_CreateDSR;
Test.setCurrentPage(pageRef);
ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
objController.createDSR();
Test.stoptest();
}
*/
    static testMethod void noProductFound()
    {
        Loaddata_withoutProduct(false,false,false);
        
        
        Test.startTest();
        
        
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    
    /*  static testMethod void testProductofConsulting()
{
Loaddata(false,false,false);

Nature_Of_Work__c objNature = new Nature_Of_Work__c();
objNature.Name = 'Consulting';
insert objNature;

Test.startTest();
objqsrm.Status__c = 'Approved';
update objqsrm;

objproduct.Nature_Of_Work_Lookup__c=objNature.id;
update objproduct;
objOpportunity.GW1_DSR_Created__c = false;
objOpportunity.StageName='2. Define';
update objOpportunity;

PageReference pageRef = Page.GW1_CreateDSR;
Test.setCurrentPage(pageRef);
ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
objController.createDSR();
Test.stoptest();
}


*/  
    
    static testMethod void OpportunityUpdateException()
    {
        Loaddata(false,true,true);
        
        Nature_Of_Work__c objNature = new Nature_Of_Work__c();
        objNature.Name = 'Consulting';
        insert objNature;
        Test.startTest();
        objqsrm.Status__c = 'Approved';
        update objqsrm;
        objOpportunity.GW1_DSR_Created__c = false;
        update objOpportunity;
        objproduct.Nature_Of_Work_Lookup__c=objNature.id;
        update objproduct;
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    
    static testMethod void OpportunityNotincludeinpilot()
    {
        Loaddata(false,true,true);
        
        
        Test.startTest();
        objqsrm.Status__c = 'Approved';
        update objqsrm;
        objOpportunity.GW1_DSR_Created__c = false;
        objOpportunity.GW1_Include_in_pilot__c=false;
        update objOpportunity;
        
        PageReference pageRef = Page.GW1_CreateDSR;
        Test.setCurrentPage(pageRef);
        ApexPages.CurrentPage().getParameters().put('id', objOpportunity.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objOpportunity);
        GW1_CreateDSRController objController = new GW1_CreateDSRController(sc);
        objController.createDSR();
        Test.stoptest();
    }
    
    
    static testMethod void testScenarioDSRtriggerhandler1()
    {
        Loaddata(false,false,false);
        Test.startTest();
        GW1_Tower_Leader_Mapping__c tlmapping = new GW1_Tower_Leader_Mapping__c(GW1_Distribution_List__c='abc@genpact.com',GW2_Nature_of_Work__c='Consulting',GW2_Sales_Reagion__c='Asia',GW1_Industry_Vertical__c='BFS',GW1_Product_Family__c='Consulting',Tower_Lead_Name__c='Test');

        insert tlmapping;
        objOpportunity.Why_Manual_DSR_created__c = 'Others';
        objOpportunity.Nature_Of_Work_OLI__c='Consulting';
        objOpportunity.Product_Family__c='Consulting';
        objOpportunity.Sales_Region__c='Asia';
        update objOpportunity;
        GW1_Bid_Manager__c objbidmanagerold=new GW1_Bid_Manager__c(Name ='abcd');
        insert objbidmanagerold;
        GW1_DSR__c dsr= new GW1_DSR__c(Name='s',GW1_Account__c=objaccount.id,GW1_Opportunity__c=objOpportunity.id,GW1_Bid_Manager__c=objbidmanagerold.Id);
        insert dsr;
        String s= objOpportunity.Name + objOpportunity.Opportunity_ID__c;
        CollaborationGroup cg= new CollaborationGroup(name= s +' Leadership roles');
        GW1_DSR_Group__c dg= new GW1_DSR_Group__c(name='Test',GW1_Chatter_Group_Id__c=cg.Id);    
       	GW1_Bid_Manager__c objbidmanager=new GW1_Bid_Manager__c(Name ='abc');
        insert objbidmanager;
        dsr.GW1_Bid_Manager__c = objbidmanager.Id;
        dsr.Create_chatter_Group__c=true;
        dsr.DSR_Pseudo_name__c='Pseudoname';
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
		dsr.OwnerId = u.id;
        update dsr;
        
        Test.stoptest();
    }
    
    
}