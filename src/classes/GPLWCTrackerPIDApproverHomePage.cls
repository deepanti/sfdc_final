@isTest
public class GPLWCTrackerPIDApproverHomePage {
    @testSetup
    static void createTestData() {
        GPCommonUtil.createLWCTestData(false);
    }
	@isTest
    static void testAllPendingForApprovalPIDsOfUser() {
        List<GP_Project__c> listOfPID = [Select id, GP_Current_Working_User__c from GP_Project__c where Name = 'Mobile App PID'];
        listOfPID[0].GP_Current_Working_User__c = userinfo.getUserId();
        update listOfPID[0];
        
        sendApproval(listOfPID[0].Id, userinfo.getUserId(), 'GP_Project_Approval');
        
        GPLWCServicePIDApproverHomePage.getAllPendingForApprovalPIDsOfUser();
    }
    
    private static void sendApproval(string SobjectId, string strUserId, string strApprovalProcessName) {
    	// Create an approval request for the account
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(SobjectId);
        req1.setNextApproverIds(new Id[] {userinfo.getUserId()});
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(strUserId); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId(strApprovalProcessName);
        req1.setSkipEntryCriteria(true);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);        
    }
}