@istest(SeeAllData=true)
public class TeststaleDealOwners 
{    
    static opportunity opp, opp1, opp2, opp3, opp4, opp5, opp6;
    static User u1;
    
    Static testmethod void addTSStaleDealsUserTest(){
        setUpTestMethod();
        List<Opportunity> oppIDs = new List<Opportunity>();
        oppIds.add(opp1);
        oppIds.add(opp2);
        oppIds.add(opp6);
        staleDealOwners.addTsDealsUsers(oppIds, 'TS_DEALS');
        
        staleDealOwners.updateStaleUser(u1.id, userinfo.getSessionId());
    }
    
    Static testmethod void addNonTSStaleDealsUserTest(){
        setUpTestMethod();
        List<Opportunity> oppIDs = new List<Opportunity>();
        oppIds.add(opp3);
        oppIds.add(opp4);
         oppIds.add(opp5);
        System.runAs(u1){
            staleDealOwners.addTsDealsUsers(oppIds, 'NON_TS_DEALS');
            
            staleDealOwners.updateStaleUser(u1.id, userinfo.getSessionId());
        }    
    }
    
    private static void setUpTestMethod(){
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        u1 = GEN_Util_Test_Data.CreateUser('standarduser201@testorg.com',Label.Profile_Stale_Deal_Owners,'standardusertestgen201@testorg.com' );
        User u =GEN_Util_Test_Data.CreateUser('standarduser2018@testorg.com',p.Id,'standardusertestgen2018@testorg.com' );
        
        
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test1',Sales_Leader__c=u.id);
        insert salesunitobject;
        account accountobject=new account(name='test1',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        accountobject.Client_Partner__c = u.Id;       
        insert accountobject;
        
        list<opportunity> oliList= new List<opportunity>();
               System.runAs(u1){
            list<opportunity> oliList1= new List<opportunity>();
            opp1=new opportunity(name='1238',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 2000005);
            opp6=new opportunity(name='12386',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 200000);
             
             opp2=new opportunity(name='12387',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, 
                                 Type_Of_Opportunity__c = 'TS',Amount = 20000000);
            
            opp3=new opportunity(name='12310',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, 
                                 Type_Of_Opportunity__c = 'Non Ts' ,Amount = 2000005);
            
            opp4=new opportunity(name='12322',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 6000000);
            
            opp5=new opportunity(name='12323',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='IT Services', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = 2, Type_Of_Opportunity__c = 'Non Ts',
                                  amount = 60000000);
            
            oliList1.add(opp1);
            oliList1.add(opp2);          
            oliList1.add(opp3);
            oliList1.add(opp4);
            oliList1.add(opp5);
           oliList1.add(opp6);
            insert oliList1;
        }  
        
    }
}