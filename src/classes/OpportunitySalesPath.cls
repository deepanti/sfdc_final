public with sharing class OpportunitySalesPath {
    
    /************************************************************************************************************************
* @author  Persistent
* @description  Handle all the bussiness logic for getting Opportunity Stages
* @return  List<String> - Opportunity Stages List
*************************************************************************************************************************/     
    @AuraEnabled
    public static Opportunity getLatestTabId(Id oppId){
            Opportunity opp;
            opp = [select LatestTabId__c,StageName,isClosed from Opportunity where id=:OppId];
            string latestTabId;
            latestTabId = opp.LatestTabId__c;
            system.debug(':---opp.LatestTabId__c '+opp.LatestTabId__c);
            if(latestTabId==null || latestTabId==''){
                opp.LatestTabId__c = '0';
            }
            return opp;
       
    }
    @AuraEnabled
    public static Id getListViews() {
        
            ListView listview =  [SELECT Id, Name FROM ListView WHERE SobjectType = 'Opportunity' and Name =: System.Label.Label_for_Opportuity_Pipeline_ListView Limit 1];
            // Perform isAccessible() check here
            return listview.Id;
       
    }
    
    @AuraEnabled
    public static Opportunity fetchRecord(string oppId){
        try
        {
            Opportunity oppRecord;
            oppRecord=[select id, Name,StageName,Previous_Stage__c
                       from opportunity where id=:oppId];
            return oppRecord;
        }
        catch(Exception ex)
        {
            CreateErrorLog.createErrorRecord('',ex.getMessage(), '', ex.getStackTraceString(),'OpportunitySalesPath', 'fetchRecord','Fail','',String.valueOf(ex.getLineNumber()));
            throw new AuraHandledException(ex.getMessage());
        }
    }
}