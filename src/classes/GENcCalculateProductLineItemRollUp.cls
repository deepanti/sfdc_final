public class GENcCalculateProductLineItemRollUp{
    public static boolean bool_check = true; 

    public static void fakeMethod(){
    Integer i = 0;
    i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
    }
    
    
    public static void updateOpportunityRollUpInfo(List<Opportunity> oppWithProducts, Map<string, AggregateResult> oppProductAggregateResultMap)
    
    {
         String ProdInfo='';
               String ProdFamily='';
               String ServiceLine='';
               String NatureOfWork='';
               String DigitalOfferings='';
  //Set<string> PF_SET = New Set<String>();  
  //Set<string> Service_Line = New Set<String>();    
 List<Opportunity> OpportunityListToUpdate = new List<Opportunity>();
 //List<string> noduplicates=new List<string>();
// dummyclass.dummymethod();
 
            for(Opportunity o: oppWithProducts)
            {    
                    Set<String> PI_SET = New set<String>();
                    Set<string> PF_SET = New Set<String>();  
                    Set<string> Service_Line = New Set<String>(); 
                    Set<string> NatureOfWork_SET = New Set<String>();
                    Set<string> DigitalOfferings_SET = New Set<String>();
                    
                    
                system.debug('opp###' + o);
                o.ProductionTCV__c = 0;
               // o.TransitionTCV__c = 0;
                o.ProductionACV__c = 0;
               // o.TransitionACV__c = 0;
                o.ProductionCYR__c = 0;
               // o.TransitionCYR__c = 0;
                o.ProductionNYR__c = 0;
                
                //o.Service_Line__c='';
               // o.Product_Family__c='';
                
               // o.TransitionNYR__c = 0;
               //String ProdFamily='';
               //String ServiceLine='';
               //String PF1='';
                o.count_no_of_lines__c = 0;     
                o.CMITs_Check__c=False; 
                o.IT_TCV__c = 0;
                Map<String,Double> Opp_ProductGroupTCVSum = new Map<String,Double>();
                Map<String,Double> Opp_ProductFamilyTCVSum = new Map<String,Double>();
                Map<String,Double> Opp_DigitalOffTCV = new Map<String,Double>();
                Map<String,Double> Opp_Natureofwork = new Map<String,Double>();
                Map<String,Double> Opp_ServiceLineTCV= new Map<String,Double>();
                Map<String,Double> Opp_ProductTCV= new Map<String,Double>();
                Map<Decimal,Decimal> Opp_DigitalTCV= new Map<Decimal,Decimal>();
                Map<Decimal,Decimal> Opp_TotalTCV= new Map<Decimal,Decimal>();
                 Map<String,Double> Opp_DeliveringOrganisation = new Map<String,Double>();
                
                 //Variables for storing IT revenue in header
                 Decimal IT_Revenue = 0;
                 IT_Revenue = o.IT_TCV__c;
                    
                
                for(OpportunityProduct__c oprod: o.Opportunity_Products__r)
                {   
                     
                    ProdInfo='';
                    ProdFamily='';
                    ServiceLine='';
                    NatureOfWork='';
                    DigitalOfferings='';
                    String PI1='';
                    String PF1='';
                    String SL1='';
                    String NOW1='';
                    String DO1='';
                     o.Product_Information__c='';
                     o.Service_Line__c='';
                     o.Product_Family__c='';
                     o.Nature_Of_Work_OLI__c='';
                     o.Digital_Offerings__c='';
                  //   o.Product_Family_Savo__c='';
                     o.Service_Line_Savo__c='';
                     
                     
                     
                    system.debug('o product####s' + oprod);
                    
                    if(oprod.COE__c=='CMITS')
                    {
                      o.CMITs_Check__c=True;
                    }
                    if(oprod.Product_Group__c != null)
                    {
                        if(Opp_ProductGroupTCVSum.containskey(oprod.Product_Group__c))
                        {
                            Double Temp = Opp_ProductGroupTCVSum.get(oprod.Product_Group__c);
                            if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c;
                            Opp_ProductGroupTCVSum.put(oprod.Product_Group__c,Temp);
                            
                        }
                        else
                        {
                            Opp_ProductGroupTCVSum.put(oprod.Product_Group__c,oprod.TCVLocal__c);
                        }
                    }
                    
                    //Product family
                     if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                         if(oprod.Product_Family_OLI__c != null)
                    {
                        if(Opp_ProductFamilyTCVSum.containskey(oprod.Product_Family_OLI__c))
                        {
                            Double Temp = Opp_ProductFamilyTCVSum.get(oprod.Product_Family_OLI__c);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_ProductFamilyTCVSum.put(oprod.Product_Family_OLI__c,Temp);
                            
                        }
                        else
                        {
                            Opp_ProductFamilyTCVSum.put(oprod.Product_Family_OLI__c,oprod.TCVLocal__c);
                        }
                    }
                    //DigitalOfferings
                     if(oprod.Digital_Offerings__c != null)
                    {
                        if(Opp_DigitalOffTCV.containskey(oprod.Digital_Offerings__c))
                        {
                            Double Temp = Opp_DigitalOffTCV.get(oprod.Digital_Offerings__c);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_DigitalOffTCV.put(oprod.Digital_Offerings__c,Temp);
                            
                        }
                        else
                        {
                            Opp_DigitalOffTCV.put(oprod.Digital_Offerings__c,oprod.TCVLocal__c);
                        }
                    }
                    //Nature of Work
                     if(oprod.Nature_of_Work_Sharing__c != null)
                    {
                        if(Opp_Natureofwork.containskey(oprod.Nature_of_Work_Sharing__c))
                        {
                            Double Temp = Opp_Natureofwork.get(oprod.Nature_of_Work_Sharing__c);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_Natureofwork.put(oprod.Nature_of_Work_Sharing__c,Temp);
                            
                        }
                        else
                        {
                            Opp_Natureofwork.put(oprod.Nature_of_Work_Sharing__c,oprod.TCVLocal__c);
                        }
                    }
                    
                    //Delivering Organisation 
                    if(oprod.COE__c != null)
                    {
                        if(Opp_DeliveringOrganisation.containskey(oprod.COE__c))
                        {
                            Double Temp = Opp_DeliveringOrganisation.get(oprod.COE__c);
                            Opp_DeliveringOrganisation.put(oprod.COE__c,Temp);
                            
                        }
                        else
                        {
                            Opp_DeliveringOrganisation.put(oprod.COE__c,oprod.TCVLocal__c);
                        }
                    }
                    
                    
                    //Service Line
                    if(oprod.Service_Line_lookup__c != null)
                    {
                        if(Opp_ServiceLineTCV.containskey(oprod.Service_Line_lookup__r.name))
                        {
                            Double Temp = Opp_ServiceLineTCV.get(oprod.Service_Line_lookup__r.name);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_ServiceLineTCV.put(oprod.Service_Line_lookup__r.name,Temp);
                            
                        }
                        else
                        {
                            Opp_ServiceLineTCV.put(oprod.Service_Line_lookup__r.name,oprod.TCVLocal__c);
                        }
                    }
                    
                    //Products
                    if(oprod.Product__c != null)
                    {
                        if(Opp_ProductTCV.containskey(oprod.Product__r.name))
                        {
                            Double Temp = Opp_ProductTCV.get(oprod.Product__r.name);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_ProductTCV.put(oprod.Product__r.name,Temp);
                            
                        }
                        else
                        {
                            Opp_ProductTCV.put(oprod.Product__r.name,oprod.TCVLocal__c);
                        }
                    }
                    
                  //Digital TCV
                  if(oprod.Digital_TCV_dollar__c!= null)
                    {
                        if(Opp_DigitalTCV.containskey(oprod.Digital_TCV_dollar__c))
                        {
                           Decimal Temp = Opp_DigitalTCV.get(oprod.Digital_TCV_dollar__c);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_DigitalTCV.put(oprod.Digital_TCV_dollar__c,Temp);
                            
                        }
                        else
                        {
                            Opp_DigitalTCV.put(oprod.Digital_TCV_dollar__c,oprod.TCVLocal__c);
                        }
                    } 
                    
                   
                  //TOTAL TCV
                  if(oprod.TotalContractValue__c != null)
                    {
                        if(Opp_TotalTCV.containskey(oprod.TotalContractValue__c))
                        {
                           Decimal Temp = Opp_TotalTCV.get(oprod.TotalContractValue__c);
                          /*  if(oprod.TCVLocal__c != null)
                                Temp = Temp + oprod.TCVLocal__c; */
                            Opp_TotalTCV.put(oprod.TotalContractValue__c,Temp);
                            
                        }
                        else
                        {
                            Opp_TotalTCV.put(oprod.TotalContractValue__c,oprod.TCVLocal__c);
                        }
                    }  
                    
                    if(oprod.TotalContractValue__c <> null){
                        o.ProductionTCV__c += oprod.TotalContractValue__c ;
                    }
                                             
                    if(oprod.ACV__c <> null){
                        o.ProductionACV__c += oprod.ACV__c;
                    }
                    
                    if(oprod.CurrentYearRevenue__c <> null){
                        o.ProductionCYR__c += oprod.CurrentYearRevenue__c;
                    }
                    
                    
                    if(oprod.NextYearRevenue__c <> null){
                        o.ProductionNYR__c += oprod.NextYearRevenue__c;
                    }

                     if(oprod.Overwrite_values__c <> null){
                        o.Pricing_Inserted__c = oprod.Overwrite_values__c;
                    }
                    
                    ProdInfo= oprod.Product__r.name;
                    ProdFamily= oprod.Product_family_Lookup__r.name;
                    ServiceLine= oprod.Service_Line_lookup__r.name;
                    NatureOfWork= oprod.Nature_of_Work_lookup__r.name;
                    DigitalOfferings = oprod.Digital_Offerings__c; 
                   
                    if(!PI_SET.contains(ProdInfo))
                     {
                          PI_SET.add(ProdInfo);
                            
                     }
                     if(!PF_SET.contains(ProdFamily))
                        {
                            PF_SET.add(ProdFamily);
                            
                        }
                        
                        if(!Service_Line.contains(ServiceLine))
                        {
                            Service_Line.add(ServiceLine);
                            
                        }
                        
                        if(!NatureOfWork_SET.contains(NatureOfWork))
                        {
                            NatureOfWork_SET.add(NatureOfWork);
                            
                        }
                        
                        
                        if(!DigitalOfferings_SET.contains(DigitalOfferings))
                        {
                            DigitalOfferings_SET.add(DigitalOfferings);
                            
                        }
                        
                        
                     System.debug('family outside SET'+PF_SET);
                     
                      System.debug('PF1 out Loop'+PF_SET.size());
                    
                        if(PI_SET.size()>0)
                        {
                        for(string s : PI_SET)
                           {System.debug('PI1 In s Loop'+s);
                                PI1=PI1+','+s;
                            }
                            System.debug('PI1 In Loop'+PI1);
                        }
                         
                      PI1=PI1.substring(1,PI1.length());
                  //    o.Product_Family_Savo__c=PI1;
                      PI1=PI1.replace('&',' & ');
                      o.Product_Information__c= PI1;
                    
                     if(PF_SET.size()>0)
                        {
                        for(string s : PF_SET)
                            {System.debug('PF1 In s Loop'+s);
                                PF1=PF1+','+s;
                            }
                            System.debug('PF1 In Loop'+PF1);
                        }
                         
                      PF1=PF1.substring(1,PF1.length());
                    //  o.Product_Family_Savo__c=PF1;
                      PF1=PF1.replace('&',' & ');
                      o.Product_Family__c= PF1;
                    
                    
                    
                    if(Service_Line.size()>0)
                        {
                        for(string s : Service_Line)
                            { System.debug('SL1 In s Loop'+s);
                                SL1=SL1+','+s;
                            }
                            System.debug('SL1 In Loop'+SL1);
                        }
                         
                      SL1=SL1.substring(1,SL1.length());
                     
                      o.Service_Line_Savo__c=SL1;
                      SL1=SL1.replace('&',' & ');
                      o.Service_Line__c= SL1;
                    
                    
                    
                     if(NatureOfWork_SET.size()>0)
                        {
                        for(string s : NatureOfWork_SET)
                            {System.debug('NatureOfWork1 In s Loop'+s);
                                NOW1=NOW1+','+s;
                            }
                            System.debug('NatureOfWork1 In Loop'+NOW1);
                        }
                         
                      NOW1=NOW1.substring(1,NOW1.length());
                    o.Nature_Of_Work_OLI__c= NOW1;
                    
                    
                        if(DigitalOfferings_SET.size()>0)
                        {
                        for(string s : DigitalOfferings_SET)
                            {System.debug('DigitalOfferings1 In s Loop'+s);
                                DO1=DO1+','+s;
                            }
                            System.debug('DigitalOfferings1 In Loop'+DO1);
                        }
                         
                      DO1=DO1.substring(1,DO1.length());
                    o.Digital_Offerings__c = DO1;
                    
                    
                    
                    
                    
                     o.count_no_of_lines__c ++;  
                     
                     //Calculation of IT revenue 
                    //Decimal Temp_IT_Revenue = 0;
                    //Temp_IT_Revenue = o.IT_TCV__c;
                    //IT_Revenue = 0;
                    System.debug('COE: ' + oprod.COE__c);
                    
                    if(oprod.COE__c!='GENPACT NL' && oprod.COE__c!='Pharmalink' && oprod.COE__c!='JAWOOD')
                    {
                        System.debug('COEif: ' + oprod.COE__c);
                    
                        if( oprod.Product_family_Lookup__r.name=='IT Services' && oprod.Total_TCV__c != null && o.IT_TCV__c != null)
                        {
                           IT_Revenue = IT_Revenue + oprod.Total_TCV__c;
                            //IT_Revenue = 1000;
                        }
                    }
                     
                     o.IT_TCV__c = IT_Revenue;
                     
                      
                }
                 system.debug('@@@@@--arjun'+o.count_no_of_lines__c);
                if( oppProductAggregateResultMap.containsKey(o.Id)) 
                {
                    AggregateResult ar = oppProductAggregateResultMap.get(o.Id);
                   
                    o.Max_OLI_End_Date__c= Date.valueOf(ar.get('maxenddate')); 
                    system.debug('@@@@@--12334'+o.count_no_of_lines__c);  
                    o.MinimumContractTermInmonths__c = Integer.valueOf(ar.get('maxContractTerm'));
                    
                }
                else{
                system.debug('N&S'+o.count_no_of_lines__c);  
                 o.MinimumContractTermInmonths__c = 0;
                 o.Max_OLI_End_Date__c=null;
                 }
               

                    Double highest_tcv = 0;
                    o.Revenue_Product__c = null;
                    for(String ProductGroupKey : Opp_ProductGroupTCVSum.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_ProductGroupTCVSum.get(ProductGroupKey);
                        if(TempHighestTCV > highest_tcv)
                        {
                            o.Revenue_Product__c = ProductGroupKey;
                            highest_tcv = TempHighestTCV;
                        }
                    
                    }   
                 //Product Family   
                Double Producthighest_tcv = 0;
                    o.Highest_Revenue_Product__c = null;
                    for(String ProductFamilyKey : Opp_ProductFamilyTCVSum.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_ProductFamilyTCVSum.get(ProductFamilyKey);
                        if(TempHighestTCV > Producthighest_tcv)
                        {
                            o.Highest_Revenue_Product__c = ProductFamilyKey;
                            Producthighest_tcv = TempHighestTCV;
                        }
                    
                    } 
                //NatureOfWork  
                    Double NOWhighest_tcv = 0;
                    o.Nature_of_Work_highest__c = null;
                    for(String NOWKey : Opp_Natureofwork.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_Natureofwork.get(NOWKey);
                        if(TempHighestTCV > NOWhighest_tcv)
                        {
                            o.Nature_of_Work_highest__c = NOWKey;
                            NOWhighest_tcv = TempHighestTCV;
                        }   
                  }                  
                //Delivering Organisation
                    Double DeliveringOrg_tcv = 0;
                    o.Delivering_Organisation__c = null;
                    for(String DeliveringOKey : Opp_DeliveringOrganisation.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_DeliveringOrganisation.get(DeliveringOKey);
                        if(TempHighestTCV > DeliveringOrg_tcv)
                        {
                            o.Delivering_Organisation__c = DeliveringOKey;
                            DeliveringOrg_tcv = TempHighestTCV;
                        }   
                  }
                   
                 //Digital off  
                 Double DigitaloffTCV = 0;
                   o.highest_Digital_Offering__c = null;
                    for(String DOKey : Opp_DigitalOffTCV.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_DigitalOffTCV.get(DOKey);
                        if(TempHighestTCV > DigitaloffTCV)
                        {
                            o.highest_Digital_Offering__c = DOKey;
                            DigitaloffTCV = TempHighestTCV;
                        }   
                  }
                
                //ServiceLine 
                 Double ServiceLineTCV = 0;
                   o.Highest_ServiceLine__c = null;
                    for(String SLKey : Opp_ServiceLineTCV.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_ServiceLineTCV.get(SLKey);
                        if(TempHighestTCV > ServiceLineTCV)
                        {
                            o.Highest_ServiceLine__c = SLKey;
                            ServiceLineTCV = TempHighestTCV;
                        }   
                  }
                

                //Product 
                 Double ProductTCV = 0;
                   o.Highest_Revenue_Product_Name__c = null;
                    for(String ProductKey : Opp_ProductTCV.keyset())
                    {
                        Double TempHighestTCV = 0;
                        TempHighestTCV = Opp_ProductTCV.get(ProductKey);
                        if(TempHighestTCV > ProductTCV)
                        {
                            o.Highest_Revenue_Product_Name__c = ProductKey;
                            ProductTCV = TempHighestTCV;
                        }   
                  }
                
                //Digital TCV
                
                Double DigitalTCV = 0;
                   o.Highest_Digital_TCV__c = null;
                    for(Decimal DigitalTCVKey : Opp_DigitalTCV.keyset())
                    {
                        Decimal TempHighestTCV = 0;
                        TempHighestTCV = Opp_DigitalTCV.get(DigitalTCVKey);
                        if(TempHighestTCV > DigitalTCV)
                        {
                            o.Highest_Digital_TCV__c = DigitalTCVKey.toPlainString();
                            DigitalTCV = TempHighestTCV;
                        }   
                  }
                
              //Total TCV
                
                Double TotalTCV = 0;
                   o.Highest_TCV__c = null;
                    for(Decimal TotalTCVKey : Opp_DigitalTCV.keyset())
                    {
                        Decimal TempHighestTCV = 0;
                        TempHighestTCV = Opp_DigitalTCV.get(TotalTCVKey);
                        if(TempHighestTCV > TotalTCV)
                        {
                            o.Highest_TCV__c = TotalTCVKey.toPlainString();
                            TotalTCV = TempHighestTCV;
                        }   
                  }  
            
             OpportunityListToUpdate.add(o); 
             //o.Service_Line__c= '';
              //o.Product_Family__c= '';
            }
            
            system.debug('OpportunityListToUpdate=========='+OpportunityListToUpdate);
            
            update OpportunityListToUpdate;
            
            
    }

}