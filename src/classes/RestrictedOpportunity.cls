public class RestrictedOpportunity {
	
    public static Restricted_Opportunities__c addToRestrictedOpp(Opportunity opp, String ts_Status, String restriction_type){
        if(opp.owner.profileID != Label.Genpact_Super_Admin && opp.owner.profileID != Label.SystemAdminProfileID ){
            Restricted_Opportunities__c res_opp = new Restricted_Opportunities__c();
            res_opp.OpportunityID__c = opp.ID;
            res_opp.ownerID__C = opp.OwnerId;
            res_opp.TS_State__c = ts_Status;
            res_opp.Restriction_Tyepe__c = restriction_type;
            return res_opp;    
        }
        return null;
        
    }
}