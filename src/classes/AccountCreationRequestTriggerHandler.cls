public with sharing class AccountCreationRequestTriggerHandler {
	public static boolean FirstRun = TRUE;
    
    public static void createNewAccount(List<Account_Creation_Request__c> newAccountToCreate) {
        
        List<Account_Creation_Request__c> acrToUpdate = new List<Account_Creation_Request__c>();
        List<Opportunity_Registration__c> orfToUpdate = new List<Opportunity_Registration__c>();
        
        for(Account_Creation_Request__c acr : newAccountToCreate) {
            if(acr.Account_Creation_Status__c == 'Approved By SL' && acr.Account_Created__c == FALSE && acr.Opportunity_Registration__c != null) {
                System.debug('Enter ForLoop');
                // Create account with ACR Values
                Account acct 								= new Account();
                acct.RecordTypeId							= Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer Account').getRecordTypeId();
                acct.Name									= acr.Name;
                acct.Business_Group__c						= acr.Business_Group__c;
                acct.Business_Segment__c					= acr.Business_Segment__c;
                acct.Sub_Business__c						= acr.Sub_Business_Group__c;
                acct.Data_visibility__c						= 'New';
                acct.Account_Creation_ID__c					= acr.Id;
                acct.ACR_15_Digit_ID__c						= acr.Id;
                acct.Industry_Vertical__c					= acr.Indutsry_Vertical__c;
                acct.Sub_Industry_Vertical__c				= acr.Sub_Industry_Vertical__c;
                acct.Hunting_Mining__c						= acr.Hunting_Mining__c;
                acct.Website								= acr.Website__c;
                acct.Is_This_A_Partner_Account__c 			= 'No';
                acct.Account_Headquarter_Genpact_Regions__c = acr.Account_Region__c;
                acct.Account_Headquarters_Country__c		= acr.Account_Headquarter_Country__c;
                acct.Archetype__c							= acr.Archetype__c;
                acct.Named_Account__c						= acr.Named_Account__c;
                acct.Account_Trigger__c						= acr.Account_Trigger__c;
                acct.Account_Owner_from_ACR__c				= acr.User_sales_leader__c;
                acct.Sales_Unit__c							= acr.Sales_Unit__c;
                acct.OwnerId								= acr.Account_Owner__c;
                acct.SalesSub_Unit_Analytics__c				= acr.Sales_Sub_Unit_Analytics__c;
                acct.QSRM_Approver__c					 	= acr.QSRM_Approver__c;
                acct.Sales_Sub_Unit_IT__c				 	= acr.Sales_Sub_Unit_IT__c;
                acct.SalesSub_Unit_Vertical__c				= acr.Sales_Sub_Unit_Vertical__c;
                acct.Primary_Account_GRM__c					= acr.Enterprise_GRm__c;
                acct.Client_Partner__c						= acr.Client_Partner__c;
                acct.GA_Contact__c							= acr.GA_Manager_from_Opp_Registration__c;
                acct.AnnualRevenue							= acr.Revenue__c;
                acct.PTAM__c								= acr.PTAM__c;
                acct.TAM__c									= acr.TAM__c;
                acct.CMIT_BU__c								= acr.CMIT_BU__c;
                acct.CMIT_Sub_BU__c							= acr.CMIT_Sub_BU__c;
                acct.Business_Description__c				= acr.Business_Description__c;
                    
                // Insert account with ACR Values
                insert acct;
                
                // Update Account Created Flag on ACR to TRUE
                Account_Creation_Request__c toUpdate 	= new Account_Creation_Request__c(id = acr.Id);
                toUpdate.Account_Created__c 			= TRUE;
                acrToUpdate.add(toUpdate);
                
                // Update ORF with newly created Account
                Opportunity_Registration__c toUpdateORF = new Opportunity_Registration__c(id = acr.Opportunity_Registration__r.Id);
                toUpdateORF.Prospect_Account__c 		= acct.Id;
                orfToUpdate.add(toUpdateORF);
            }
            
            
            
        }
        database.update(acrToUpdate, false);
        database.update(orfToUpdate, false);
        
    }
}