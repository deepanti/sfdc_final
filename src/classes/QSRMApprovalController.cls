public class QSRMApprovalController {
    public static String processId {set;get;}
    public static list<ProcessInstance> processPendingList  {set;get;}
    public static list<ProcessInstance> processPendingListUserSpecific  {set;get;}
    public static list<ProcessInstance> processHistoryList {set;get;}
    public static ProcessInstance objProcessInstance {set;get;}
    public static QSRM__c  QSRMObj   {set;get;}
    public static List<QSRM__c> QSRMObjList   {set;get;}
    public static List<QSRM__c> qsrmPendingList    {set;get;}
    public static Set<Id>qsrmIds        {set;get;}
    public ID QsrmId   {set;get;}
    public static List<QSRM__c> OpptyList{get;set;}
    
    public QSRMApprovalController(ApexPages.StandardController controller)
    {
        
    } 
    
    //This method is used to fetch the approved and rejected history
    @AuraEnabled
    public static List<qsrmHistory> getProcessInstancesmethod(ID QsrmID, boolean statusCheck){
        system.debug(':-----recordId--:'+QsrmID);
        try{
            list<qsrmHistory> historyList = new list<qsrmHistory>();
            list<String> statusList=new list<String> {'Approved' , 'Rejected','Removed'};
                Id userId = UserInfo.getUserId(); 
            qsrm__c qsrm = [Select id,CreatedBy.Name,Status__c,Vertical_Leaders__c,Service_Line_Leaders__c from qsrm__c where Id =: QsrmID];
            String StatusPending ='';
            if(qsrm.Status__c == 'In Approval' || qsrm.Status__c == 'rework'){
                StatusPending = 'Pending';
            }
            string Action = '';
            String comments ='';
            if(qsrm.Status__c == 'Approved'){
                Action = 'Approve'; 
                comments ='Auto Approve';
            }else if(qsrm.Status__c == 'Rejected'){
                Action = 'Reject';
                comments ='Auto Reject';
            }
            
            if(!String.isBlank(Action)){
                List<ProcessInstanceWorkitem> pw=[SELECT id FROM ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:QsrmID AND ProcessInstance.status ='Pending' limit 1];
                if(pw.size() > 0){
                    system.debug('QSRMApprovalContoller processworkitem ==='+pw);
                    Approval.ProcessWorkitemRequest req=new Approval.ProcessWorkitemRequest();
                    req.setComments(comments);
                    req.setAction(Action);
                    req.setWorkitemId(pw[0].Id);
                    Approval.ProcessResult result=Approval.Process(req);
                    system.debug('QSRMApprovalController ==rework=:'+result);
                }
            }
            
            processHistoryList=[SELECT Id, Status, SubmittedBy.Name,CompletedDate,CreatedDate,CreatedBy.Name,TargetObjectId,TargetObject.Name,(SELECT Actor.Name,ActorId,OriginalActor.Name,Comments  FROM StepsAndWorkitems where (StepStatus IN : statusList OR StepStatus = 'pending')) FROM ProcessInstance WHERE TargetObjectId =:QsrmID  And (Status IN : statusList OR Status =: StatusPending)  ORDER BY CreatedDate DESC];
            system.debug('ProcessHistoryList'+processHistoryList.size());
            for(ProcessInstance pI : processHistoryList){
                qsrmHistory histy = new qsrmHistory(); 
                system.debug('QSRMApprovalController===:'+PI.Status);
                histy.qsrmDate = PI.CreatedDate;
                if(PI.Status == 'Removed'){
                    histy.Status = 'Rework';
                }else{
                    histy.Status = PI.Status;
                }
                
                histy.qsrmOwner = qsrm.CreatedBy.Name;
                if(PI.StepsAndWorkitems.size() > 0){//OriginalActor.Name
                   // histy.qsrmApprover = PI.StepsAndWorkitems[0].Actor.Name;
                    histy.qsrmApprover = PI.StepsAndWorkitems[0].OriginalActor.Name;
                    histy.comments = PI.StepsAndWorkitems[0].Comments;
                    system.debug('QSRMApprovalController==:'+qsrm.Vertical_Leaders__c);
                    system.debug('QSRMApprovalController==:'+qsrm.Service_Line_Leaders__c);
                    if(qsrm.Vertical_Leaders__c == PI.StepsAndWorkitems[0].ActorId){
                        histy.approverRole = 'TS Leader';
                    }else if(qsrm.Service_Line_Leaders__c == PI.StepsAndWorkitems[0].ActorId){
                        histy.approverRole = 'Service Line Leader'; 
                    }
                    if(PI.StepsAndWorkitems[0].ActorId == userId){
                        histy.Approver = true;
                    }else{
                        histy.Approver = false;
                    }
                }else{
                    histy.qsrmApprover = '';
                    histy.Approver = false;
                    histy.approverRole = '';
                    histy.comments = '';
                }
                system.debug('QSRMApprovalController===:'+PI.Steps);
                historyList.add(histy);
            }
            system.Debug('QSRMApprovalController===:'+historyList);
            return historyList;
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            return null;
        }
    }
    @AuraEnabled
    public static List<ProcessInstance> getProcessInstanceNodesmethod(ID QsrmID, boolean statusCheck){
        system.debug(':-----recordId--:'+QsrmID);
        try{
            QSRMObj=[select Id,Name,Status__c,QSRM_Custom_Status__c,Deal_Type__c,(Select id from ProcessInstances) From QSRM__c where id =:QsrmID];
            system.debug('---QSRMObj=====>'+QSRMObj);
            QSRMObjList=[select Id,Name,QSRM_Type__c,Status__c,QSRM_Custom_Status__c,Deal_Type__c From QSRM__c where Status__c = 'In Approval'];
            system.debug('---QSRMObjList=====>'+QSRMObjList);
            if(QSRMObj.Status__c == 'In Approval'){
                processPendingList=[SELECT Id, Status, SubmittedBy.Name,CompletedDate,CreatedDate,CreatedBy.Name,TargetObjectId,TargetObject.Name FROM ProcessInstance where TargetObjectId=:QsrmID And Status ='Pending' ORDER BY CreatedDate DESC];
                system.debug('====processPendingList===>'+processPendingList);
            } else {  
                statusCheck=true;
            }
            return processPendingList;
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            return null;
        }
    }
    @AuraEnabled
    public static List<qsrmHistory>  approverActionMethod(ID QsrmID, String Action, string commentsData){
        system.debug('QSRMApprovalContoller==QsrmID==:'+QsrmID+':==Action===:'+Action+':==commentsData===:'+commentsData);
        try{
            Id userId = UserInfo.getUserId(); 
            list<qsrmHistory> historyList = new list<qsrmHistory>();
            list<String> statusList=new list<String> {'Approved' , 'Rejected','Removed'};
             
                ProcessInstanceWorkitem pw=[SELECT id FROM ProcessInstanceWorkitem where ProcessInstance.TargetObjectId =:QsrmID];
            system.debug('QSRMApprovalContoller processworkitem ==='+pw);
            Approval.ProcessWorkitemRequest req=new Approval.ProcessWorkitemRequest();
            req.setComments(commentsData);
            req.setAction(Action);
            req.setWorkitemId(pw.Id);
            Approval.ProcessResult result=Approval.Process(req);
            system.debug('QSRMApprovalController ==rework=:'+result);
            
                
            qsrm__c qsrm = [Select id,CreatedById ,Status__c,Vertical_Leaders__c,Service_Line_Leaders__c, Service_Line_leader_SL_Approval__c from qsrm__c where Id =: QsrmID];
            system.debug('QSRMApprovalContoller qsrm.Status__c ==='+qsrm.Status__c);
            system.debug('QSRMApprovalContoller qsrm.Status__c ==='+qsrm.Service_Line_leader_SL_Approval__c);
            if(Action == 'Removed'){
                system.debug('QSRMApprovalController ==rework=:'+Action);
                ApprovalQsrmController.sendReworkQSRMEmailTemplate('',qsrm.CreatedById,'Rework Email To Seller',QsrmID);
            } else if(Action == 'Approve'){
                system.debug('QSRMApprovalController ==Approve=:'+Action);
                if(qsrm.status__c =='Approved'){
                    system.debug('QSRMApprovalController = status__c  =Approve=:'+Action);
                    ApprovalQsrmController.sendReworkQSRMEmailTemplate('',qsrm.CreatedById,'Email To Seller when QSRM is approved',QsrmID);
                }else if(qsrm.Service_Line_leader_SL_Approval__c == 'Approved'){
                    system.debug('QSRMApprovalController = Service_Line_leader_SL_Approval__c=Approve=:'+Action);
                    ApprovalQsrmController.sendReworkQSRMEmailTemplate('',qsrm.CreatedById,'Email To Seller when QSRM is approved by service line leader',QsrmID);
                }
            } else if(Action == 'Reject'){
                system.debug('QSRMApprovalController ==Reject=:'+Action);
                ApprovalQsrmController.sendReworkQSRMEmailTemplate('',qsrm.CreatedById,'Email Alert to Seller when QSRM is rejected',QsrmID);
            }
            
                       
            processHistoryList=[SELECT Id, Status, SubmittedBy.Name,CompletedDate,CreatedDate,CreatedBy.Name,TargetObjectId,TargetObject.Name,(SELECT Actor.Name,ActorId,Comments  FROM StepsAndWorkitems where (StepStatus IN : statusList OR StepStatus = 'pending')) FROM ProcessInstance WHERE TargetObjectId =:QsrmID  And Status IN : statusList   ORDER BY CreatedDate DESC];
            for(ProcessInstance pI : processHistoryList){
                qsrmHistory histy = new qsrmHistory(); 
                system.debug('QSRMApprovalController===:'+PI.Status);
                histy.qsrmDate = PI.CreatedDate;
                if(PI.Status == 'Removed'){
                    histy.Status = 'Rework';
                }else{
                    histy.Status = PI.Status;
                }
                
                histy.qsrmOwner = PI.CreatedBy.Name;
                if(PI.StepsAndWorkitems.size() > 0){
                    histy.qsrmApprover = PI.StepsAndWorkitems[0].Actor.Name;
                    histy.comments = PI.StepsAndWorkitems[0].Comments;
                    system.debug('QSRMApprovalController==:'+qsrm.Vertical_Leaders__c);
                    system.debug('QSRMApprovalController==:'+qsrm.Service_Line_Leaders__c);
                    if(qsrm.Vertical_Leaders__c == PI.StepsAndWorkitems[0].ActorId){
                        histy.approverRole = 'TS Leader';
                    }else if(qsrm.Service_Line_Leaders__c == PI.StepsAndWorkitems[0].ActorId){
                        histy.approverRole = 'Service Line Leader'; 
                    }
                    if(PI.StepsAndWorkitems[0].ActorId == userId){
                        histy.Approver = true;
                    }else{
                        histy.Approver = false;
                    }
                }else{
                    histy.qsrmApprover = '';
                    histy.Approver = false;
                    histy.approverRole = '';
                    histy.comments = '';
                }
                system.debug('QSRMApprovalController===:'+PI.Steps);
                historyList.add(histy);
            } 
            return historyList;
            // return null;
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<qsrmOpportunityWrapper> getProcessInstancePendingmethodHomePage(String checkForHomePage){
        try{
            integer i=0;
            integer j=0;
            processPendingListUserSpecific = new List<ProcessInstance>();
            List<qsrmOpportunityWrapper> qsrmOpptyWrapper = new List<qsrmOpportunityWrapper>();
            QSRMObjList=[select Id,Name,QSRM_Type__c,Status__c,QSRM_Custom_Status__c,Deal_Type__c From QSRM__c where Status__c = 'In Approval'];
            system.debug('---QSRMObjList=====>'+QSRMObjList);
            qsrmIds = new Set<ID>();
            for(QSRM__c q: QSRMObjList){
                if(q.Status__c == 'In Approval'){
                    System.debug('qsrmIds === '+q.Id);
                    qsrmIds.add(q.Id);
                }
            }
            processPendingList=[SELECT Id, Status, SubmittedBy.Name,CompletedDate,CreatedDate,CreatedBy.Name,TargetObjectId,TargetObject.Name,(Select ActorId From WorkItems) FROM ProcessInstance where TargetObjectId=:qsrmIds and Status ='Pending' ORDER BY CreatedDate ASC];
            system.debug('processPendingListActorId===>'+processPendingList);
            for(ProcessInstance pr : processPendingList){               
                for(ProcessInstanceWorkItem preWork : pr.WorkItems){
                    System.debug('ActorId = '+preWork.ActorId);
                    system.Debug('UserInfo.getUserId()==>'+UserInfo.getUserId());
                    if(preWork.ActorId == UserInfo.getUserId()){
                        processPendingListUserSpecific.add(pr);
                    }
                }
            } 
            System.debug('processPendingListUserSpecific == '+processPendingListUserSpecific);
            OpptyList=[Select Name,QSRM_Type__c,Opportunity__c,Opportunity__r.Name,Opportunity__r.StageName,Opportunity__r.QSRM_Status__c from QSRM__c 
                       Where Opportunity__r.StageName not IN ('6. Signed Deal','7. Lost','8. Dropped') 
                       AND Opportunity__r.QSRM_Status__c = 'Your QSRM is submitted for Approval' and Id IN : qsrmIds];
           system.debug('QSRMApprovalController ===== '+OpptyList);
            if(processPendingListUserSpecific.size()>2){
                    i = 3;
                }else{
                    i = processPendingListUserSpecific.size();
                }
            if(checkForHomePage == 'homepage'){
                for(QSRM__c q : OpptyList){
                    qsrmOpportunityWrapper currentqsrmWrapper = new qsrmOpportunityWrapper();
                    for(ProcessInstance p : processPendingListUserSpecific){
                        if(p.TargetObject.Name == q.Name && j<i){
                            currentqsrmWrapper.currentProcessInstance = p;
                            currentqsrmWrapper.currentOpptyName = q.Opportunity__r.Name;
                            currentqsrmWrapper.currentOpptyId = q.Opportunity__c;
                            currentqsrmWrapper.days=p.CreatedDate.Date().daysBetween(System.today());
                            currentqsrmWrapper.QsrmType = q.QSRM_Type__c;
                            system.debug('days====>'+currentqsrmWrapper.days);
                            qsrmOpptyWrapper.add(currentqsrmWrapper);
                        }
                    }
                }
            }else{
                for(QSRM__c q : OpptyList){
                    qsrmOpportunityWrapper currentqsrmWrapper = new qsrmOpportunityWrapper();
                    for(ProcessInstance p : processPendingListUserSpecific){
                        if(p.TargetObject.Name == q.Name){
                            currentqsrmWrapper.currentProcessInstance = p;
                            currentqsrmWrapper.currentOpptyName = q.Opportunity__r.Name;
                            currentqsrmWrapper.currentOpptyId = q.Opportunity__c;
                            currentqsrmWrapper.days = p.CreatedDate.Date().daysBetween(System.today());
                            currentqsrmWrapper.QsrmType = q.QSRM_Type__c;
                            system.debug('days====>'+currentqsrmWrapper.days);
                            qsrmOpptyWrapper.add(currentqsrmWrapper);
                        }
                    }
                }
                
            }
            system.debug('====processPendingListOne===>'+qsrmOpptyWrapper);
            system.debug('====processPendingListOne Size===>'+qsrmOpptyWrapper.size());
            if(qsrmOpptyWrapper.size()>0){
                return qsrmOpptyWrapper;
            }else{
                system.debug('Inside Else');
                return null;
            }
            
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            return null;
        }
    }
    
    @AuraEnabled
    public static List<qsrmOpportunityWrapper> getProcessInstanceReworkmethodHomePageSalesRep(String checkForHomePage){
        List<qsrmOpportunityWrapper> wrapparList = new List<qsrmOpportunityWrapper>();
        List<Opportunity> oppsList = [Select Id,AccountId,Account.Name,Name,Opportunity_ID__c,TCV1__c,StageName,(Select Product_Name__c 
                                                                                                                 from OpportunityLineItems),(Select ID,name,Status__c from QSRMS__r limit 1) From Opportunity Where OwnerId =:userInfo.getUserId()
                                      And (QSRM_status2__c > 0 OR Roll_Up_QSRM_Rework__c > 0)And (QSRM_Status__c = 'Your QSRM is submitted for Approval' OR QSRM_Status__c = 'Rework')
                                      And StageName not in ('6. Signed Deal','7. Lost','8. Dropped')];
       system.debug('QSRMApprovalController ===== '+OpptyList);
        integer i=0;
        integer j=0;
        if(oppsList.size()>2){
            i = 3;
        }else{
            i = oppsList.size();
        }
        if(checkForHomePage == 'homePage'){
            
            for(Opportunity opp: oppsList)
            {
                if(j<i){
                    qsrmOpportunityWrapper temp = new qsrmOpportunityWrapper();
                    List<QSRM__C> qsrmLst=opp.qsrms__r;
                    temp.currentOpptyId = opp.Id;
                    temp.currentOpptyName = opp.Name;
                    if(qsrmLst[0].status__C == 'Rework'){
                        system.debug('HomePageApprovalController====:Rework');
                        temp.Status = 'Rework';
                    }else{
                        temp.Status = 'QSRM Approval Pending';
                    }
                    temp.qsrmId=qsrmLst[0].Id;
                    temp.qsrmName=qsrmLst[0].name;
                    wrapparList.add(temp);
                    j++;
                }
            }
        }else{
            for(Opportunity opp: oppsList)
            {
                
                qsrmOpportunityWrapper temp = new qsrmOpportunityWrapper();
                List<QSRM__C> qsrmLst=opp.qsrms__r;
                temp.currentOpptyId = opp.Id;
                temp.currentOpptyName = opp.Name;
                if(qsrmLst[0].status__C == 'Rework'){
                    system.debug('HomePageApprovalController====:Rework');
                    temp.Status = 'Rework';
                }else{
                    temp.Status = 'QSRM Approval Pending';
                }
                temp.qsrmId=qsrmLst[0].Id;
                temp.qsrmName=qsrmLst[0].name;
                wrapparList.add(temp);
            }
            
        }
        return wrapparList;
    }
    @AuraEnabled
    public static List<OpportunityLineItem> getAllProductForQsrm(String qsrmId)
    {
        System.debug('Insode getAllProductForQsrm');
        return [SELECT Id,Product_Name__c,Service_Line__c,Nature_of_Work__c,TCV__c,CYR__c FROM OpportunityLineItem WHERE OpportunityId IN(SELECT Opportunity__C FROM Qsrm__c WHERE Id=:qsrmId )];        
    }
    
    @AuraEnabled
    public static QSRM__c  getQSRMObjMethod(ID QsrmID){
        try{
            QSRMObj=[select Id,Name,Opportunity__r.Name,Deal_Type__c,Allocated_bid_budget__c,Name_of_Pursuit_lead__c,QSRM_Type__c, (Select id from ProcessInstances where Status ='pending') From QSRM__c where id =:QsrmID];
            return QSRMObj;
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            return null;
        }
    } 
    
    @AuraEnabled
    public static List<QSRM__History>  getQSRMObjHistroyMethod(String QsrmID){
        try{
            return [SELECT ParentId,OldValue, NewValue, Field, CreatedBy.Name, CreatedDate FROM QSRM__History where parentId =: QsrmID];
        }catch(exception e){
            system.debug('Error  message'+e.getMessage()+':----errorline-----'+e.getLineNumber());
            return null;
        }
    } 
    
    
    @AuraEnabled
    public static User  getCurrentLogInUser(){ 
        user u=[select id,Profile.Name from user where id=:UserInfo.getUserId()];
        system.debug('user==>'+u.Profile.Name);
        return u;
    }
    
    public class qsrmHistory{
        @AuraEnabled public  DateTime qsrmDate;
        @AuraEnabled public  String Status;
        @AuraEnabled public  String qsrmOwner;
        @AuraEnabled public  String qsrmApprover;
        @AuraEnabled public  String approverRole;
        @AuraEnabled public  String comments;
        @AuraEnabled public  Boolean Approver;
    }
    
    public class qsrmOpportunityWrapper{
        @AuraEnabled public  ProcessInstance currentProcessInstance;
        @AuraEnabled public  String currentOpptyName;
        @AuraEnabled public  Id currentOpptyId;
        @AuraEnabled public  integer days;
        @AuraEnabled public  String QsrmType;
        @AuraEnabled public String qsrmName;
        @AuraEnabled public String Status;
        @AuraEnabled public  Id qsrmId;
        
    }
    public static void iterateMethod(){
      integer i =0;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
        i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
      i++;
    }
}