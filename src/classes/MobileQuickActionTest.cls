@isTest
public class MobileQuickActionTest {

     static opportunity opp;
     public static  QSRM__c qsrm;
     public static OpportunityLineItem opp_item;
     public static Product2 product_obj;
     public static PricebookEntry entry;
     public static User u, u1, u3,u4,u5,u6;
      @testSetup static void setup() {
      
          Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
         User testUser=TestDataFactoryUtility.createTestUser('Genpact Super Admin', 'email@acme.com', 'email@xyzasd.com');
         u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        u1 =GEN_Util_Test_Data.CreateUser('standarduser2075@testorg.com',p.Id,'standardusertestgen2075@testorg.com' );
        u3 =GEN_Util_Test_Data.CreateUser('standarduser2077@testorg.com',p.Id,'standardusertestgen2077@testorg.com' );
          u4 =GEN_Util_Test_Data.CreateUser('standarduser2078@testorg.com',p.Id,'standardusertestgen2078@testorg.com' );
        insert testUser;
        System.runAs(testUser){
            Account accountobject=TestDataFactoryUtility.createTestAccountRecord();
            accountobject.Name='TEST CUSTOMER 1234';
            insert accountobject;
            
            opp=new opportunity(name='1234',RFXStatus__c='No',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today(),accountid=accountobject.id,W_L_D__c='',Nature_of_Work_highest__c='Consulting', Insight__c = 2, counterOne__c = 0, StallByPassFlag__c = 'False', deal_cycle_ageing__c = -1, Type_Of_Opportunity__c = 'TS', 
                                amount = 2000000);

            test.startTest();
            insert opp;
            
            product_obj = new Product2(name='test pro', isActive = true, CanUseRevenueSchedule = true);
            insert product_obj;
            Id pricebookId = Test.getStandardPricebookId();
             entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                      UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
            insert entry;           
            
            
            opp_item = new OpportunityLineItem(opportunityID = opp.ID, product2ID = product_obj.Id,
                                                                   priceBookEntryId = entry.ID, product_bd_rep__c = u3.ID,
                                                                   unitPrice = 100.00, TCV__c = 100,
                                                                   vic_VIC_User_3__c = u1.ID,
                                                                   vic_VIC_User_4__c = u4.ID
                                                                   ); 
            insert opp_item;
            test.stopTest();
            ID QSRMrecordTypeId = Schema.SObjectType.QSRM__c.getRecordTypeInfosByName().get('Detail Record Type').getRecordTypeId();
             qsrm = new QSRM__c(Deal_Administrator__c = 'Analyst/Advisory Firm', Deal_Type__c = 'Project', Named_Advisor__c = 'Accelare',
                                     Named_Analyst__c = 'Jack Calhoun', Opportunity__c = opp.ID, Is_this_is_a_RPA_deal__c = 'Yes', 
                                     Does_the_client_have_a_budget_for_Opp__c = 'Adequate', What_is_likely_decision_date__c = '>3 months',
                                     Does_the_client_have_compelling_need__c = 'No', Do_we_have_the_right_domain_knowledge__c = 'Full capability for entire scope of work',
                                     Do_we_have_client_references__c = 'No references', Do_we_have_connect_with_decision_maker__c = 'No Connect', 
                                    Who_is_the_competition_on_this_deal__c = 'Solesource', What_is_the_type_of_Project__c = 'Fixed Price', 
                                    What_is_the_nature_of_work__c = 'Design/Strategy',Service_Line_Leaders__c=u.id,Vertical_Leaders__c=u.id);
            insert qsrm;
        }

      }
    
    @isTest
     public static void testOpportunityFunctions()
    {
         User testUser=[SELECT Id,Email,FirstName,LastName FROM user WHERE email='email@xyzasd.com'];
        System.runAs(testUser){
           Opportunity opp =[SELECT id FROM opportunity LIMIT 1];
            QSRM__c qsm=[SELECT Id FROM QSRM__c LIMIT 1];
            GetQsrmDetail.wrapperClass wrpCls=new GetQsrmDetail.wrapperClass();
             try
            {
             
               GetQsrmDetail.getQsrm(opp.ID);
                GetProduct.getAllProduct(opp.Id);
               GetOliForQsrm.getAllProductForQsrm(qsm.ID);
             
            }
            catch(Exception e)
            {
                
            }
        }
    }
}