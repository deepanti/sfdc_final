/*
Created By - Abhishek Maurya
Date -15/10/208 
Lead - Mandeep Kaur
Description - this class is used to send the OpportunityLineItem data to client side controller in GetOLIForQsrm Lightning component .  
 */

public with sharing class GetOliForQsrm{

     @auraEnabled
    public static List<OpportunityLineItem> getAllProductForQsrm(String qsrmId)
    {
      
        return [SELECT Id,Product_Name__c,Service_Line__c,Nature_of_Work__c,TCV__c,CYR__c FROM OpportunityLineItem WHERE OpportunityId IN(SELECT Opportunity__C FROM Qsrm__c WHERE Id=:qsrmId )];        
    }
}