/**
 * @group DomainLayer. 
 *
 * @description Domain layer for Timesheet Entry.
 */
public class GPDomainTimesheetEntry extends fflib_SObjectDomain {

    private static Map < String, Integer > mapOfDayVsDaysToBeAdded = new Map < String, Integer > {
        'mon' => 4,
        'tue' => 3,
        'wed' => 2,
        'thu' => 1,
        'fri' => 0,
        'sat' => 6,
        'sun' => 7
    };
    public GPDomainTimesheetEntry(List < GP_Timesheet_Entry__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainTimesheetEntry(sObjectList);
        }
    }
    //SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Timesheet_Entry__c.SobjectType


    };

    public override void onBeforeInsert() {
        // fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        setOrgIdFromHRHistoryAndExpEndDate();
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);


    }
    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //setOrgIdFromHRHistoryAndExpEndDate();
        //uow.commitWork();
    }

    public override void onAfterInsert() {

        fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        uow.commitWork();
    }

    //@method is used to get the org id in timesheet entry from the HR History 
    //if the date lies between the start and end date.

    /**
     * @description Set Org Id from Employee Master on Timesheet Entry record and Expenditure ending date(Friday of current week).
     * 
     */
    public void setOrgIdFromHRHistoryAndExpEndDate() {

        Set < Id > setOfEmpId = new Set < Id > ();
        DateTime entryDate;

        for (GP_Timesheet_Entry__c objTimeSheetEntry: (list < GP_Timesheet_Entry__c > ) records) {
            if (objTimeSheetEntry.GP_Date__c != null && objTimeSheetEntry.GP_Employee__c != null) {
                entryDate = objTimeSheetEntry.GP_Date__c;
                objTimeSheetEntry.GP_Expenditure_Ending_Date__c = Date.ValueOf(entryDate.addDays(mapOfDayVsDaysToBeAdded.get(entryDate.format('E').toLowerCase())));
                setOfEmpId.add(objTimeSheetEntry.GP_Employee__c);
            }
        }
        if (setOfEmpId.size() > 0) {
            Map < Id, GP_Employee_Master__c > mapOfEmployeeMaster = new GPSelectorEmployeeMaster().selectByRecordId(setOfEmpId);
            
            for (GP_Timesheet_Entry__c objTimeSheetEntry: (list < GP_Timesheet_Entry__c > ) records) {
                if (objTimeSheetEntry.GP_Date__c != null && objTimeSheetEntry.GP_Employee__c != null &&
                    mapOfEmployeeMaster.containsKey(objTimeSheetEntry.GP_Employee__c)) {
                    objTimeSheetEntry.GP_Emp_Org_Id__c = mapOfEmployeeMaster.get(objTimeSheetEntry.GP_Employee__c).GP_Org_TSC_Id__c;
                }
            }
            /*map<Id,List<GP_Employee_HR_History__c>> mapOfEmployeeIdVsHRRecords = new map<Id,List<GP_Employee_HR_History__c>>();
            
            for(GP_Employee_HR_History__c objEmployeeHR : new GPSelectorEmployeeHR().getEmployeeHRBasedOnEmp(setOfEmpId)){
                if(!mapOfEmployeeIdVsHRRecords.containsKey(objEmployeeHR.GP_Employees__c))
                    mapOfEmployeeIdVsHRRecords.put(objEmployeeHR.GP_Employees__c,new List<GP_Employee_HR_History__c>{objEmployeeHR});
                else
                    mapOfEmployeeIdVsHRRecords.get(objEmployeeHR.GP_Employees__c ).add(objEmployeeHR);
            }
            
            for(GP_Timesheet_Entry__c objTimeSheetEntry : (list<GP_Timesheet_Entry__c>) records)
            {
                if(mapOfEmployeeIdVsHRRecords.containsKey(objTimeSheetEntry.GP_Employee__c))
                {
                    for(GP_Employee_HR_History__c objEmployeeHR : mapOfEmployeeIdVsHRRecords.get(objTimeSheetEntry.GP_Employee__c)){
                        if(objTimeSheetEntry.GP_Date__c > objEmployeeHR.GP_ASGN_EFFECTIVE_START__c && 
                           objTimeSheetEntry.GP_Date__c < objEmployeeHR.GP_ASGN_EFFECTIVE_END__c){
                               objTimeSheetEntry.GP_Emp_Org_Id__c = objEmployeeHR.GP_Org_Id__c;
                               break;
                           }
                    }
                }
            }*/
        }
    }
}