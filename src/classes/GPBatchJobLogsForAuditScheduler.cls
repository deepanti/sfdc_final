global class GPBatchJobLogsForAuditScheduler implements Schedulable {
    //Avinash N	
	
    global void Execute(SchedulableContext sc)
    {        
        GPBatchJobLogsForAudit objBatch=new GPBatchJobLogsForAudit();
		
        Database.executeBatch(objBatch);
    }
    
}