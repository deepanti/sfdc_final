@isTest
public class GPDomainOpportunityTracker {
    /*@isTest
public static void testGPDomainOpportunity() {
GPDomainOpportunity obj = new GPDomainOpportunity(new List<Opportunity>{new opportunity()});
}*/
    public static Business_Segments__c BSobj;
    public static Sub_Business__c SBobj;
    public static List<Account> accList;
    public static Opportunity oppobj;
    public static GP_Opportunity_Project__c oppproobj;
    public static Product2 prodobj;
    public static OpportunityProduct__c oppprodobj;
    public static GP_Deal__c Dealobj;
    
    @testSetup static void setupCommonData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'opportunity';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;
        
        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;
        
        Account accobj = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj ;
        
        Account accobj1 = GPCommonTracker.getAccount(BSobj.id,SBobj.id);
        insert accobj1;
        
        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.AccountId = accobj.id;
        insert oppobj;
        
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        insert oppproobj;
        
        Product2 prodobj = GPCommonTracker.getProduct();
        insert prodobj;
        
        OpportunityProduct__c oppprodobj = GPCommonTracker.getOpportunityProduct(prodobj.id,oppobj.id);
        insert oppprodobj; 
        
        GP_Deal__c Dealobj = GPCommonTracker.getDeal();
        Dealobj.GP_OLI__c = oppprodobj.ID;
        insert Dealobj;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = oppobj.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 8 * pbEntry.UnitPrice
        );
        insert oli;
    } 
    
    @isTest
    public static void testGPDomainOpportunity()
    {        
        loadData();
        oppproobj.GP_Opportunity_Id__c = oppobj.Opportunity_ID__c;
        //update oppproobj;
        
        Test.startTest();
        update oppproobj;
        oppobj.Probability = 85;
        oppobj.AccountId = accList[0].Id == oppobj.AccountId ? accList[1].Id : accList[0].Id;
        update oppobj; 
        Test.stopTest();
    }
    
    public static void loadData() 
    {
        BSobj = [select id,Name from Business_Segments__c limit 1];
        SBobj = [select id,Name from Sub_Business__c limit 1];
        accList = [select id,Name from Account];              
        oppobj = [select id,Opportunity_ID__c,Name,AccountId from Opportunity limit 1]; 
        oppproobj = [select id,Name from GP_Opportunity_Project__c limit 1]; 
        oppprodobj = [select id,Name from OpportunityProduct__c limit 1];      
        Dealobj = [select id,Name from GP_Deal__c  limit 1];        
    } 
}