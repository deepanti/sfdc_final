@isTest
public class SalesLeaderHomePageChartUtilityTest {
    @testSetup
	public static void createData()
    {
        User salesLeader = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac1@bc.com','ac@bc.com');
        salesLeader.ForecastEnabled = true;
        insert salesLeader;
        User testUser1 = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac2@bc.com','ac@bc.com');
        testUser1.ForecastEnabled = true;
        insert testUser1;
        User testUser2 = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac3@bc.com','ac@bc.com');
        testUser2.ForecastEnabled = true;
        insert testUser2;
        System.runAs(salesLeader){
        //creating account
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        acc.Client_Partner__c = salesLeader.Id;
        insert acc;
        //creating contact
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
        insert con;
        //creating opportunity
        List<Opportunity> opp=TestDataFactoryUtility.createTestOpportunitiesRecordsDiscover(acc.Id, 1, system.today(), con);
        opp[0].Stagename = '6. Signed Deal';
        insert opp;
        List<OpportunityTeamMember> memberList = new List<OpportunityTeamMember>();
        OpportunityTeamMember oppMember1 = new OpportunityTeamMember();
        system.debug(':---opp[0].id;-----:'+opp[0].id);
        oppMember1.OpportunityId = opp[0].id;
        oppMember1.UserId = testUser1.id;
        oppMember1.TeamMemberRole = 'BD Rep';
        oppMember1.OpportunityAccessLevel = 'Read';
        memberList.add(oppMember1);
        OpportunityTeamMember oppMember2 = new OpportunityTeamMember();
        oppMember2.OpportunityId = opp[0].id;
        oppMember2.UserId = testUser2.id;
        oppMember2.TeamMemberRole = 'BD Rep';
        oppMember2.OpportunityAccessLevel = 'Read';
        memberList.add(oppMember2);
        insert memberList;
        List<OpportunitySplit> oppSplitList = new List<OpportunitySplit>();
        OpportunitySplit oppSplit1 = new OpportunitySplit();
        oppSplit1.OpportunityId	 = opp[0].id;
        oppSplit1.SplitPercentage = 50;
        oppSplit1.SplitOwnerId = testUser1.Id;
        oppSplitList.add(oppSplit1);
        OpportunitySplit oppSplit2 = new OpportunitySplit();
        oppSplit2.OpportunityId	 = opp[0].id;
        oppSplit2.SplitPercentage = 50;
        oppSplit2.SplitOwnerId = testUser2.Id;
        oppSplitList.add(oppSplit2);
        insert oppSplitList;
        List<ForecastingQuota> quotaList = new List<ForecastingQuota>();
        ForecastingQuota quotaRecord1 = TestDataFactoryUtility.createTestQuota(testUser2.id);
        ForecastingQuota quotaRecord2 = TestDataFactoryUtility.createTestQuota(testUser1.id);
        quotaList.add(quotaRecord1);
        quotaList.add(quotaRecord2);
        //creating Quota Record
        insert quotaList;
        }
    }
    public static testMethod void check()
    {
        test.startTest();
        User sl = [select Id from User where username = 'ac1@bc.com' Limit 1];
        system.runAs(sl)
        {
            SalesLeaderHomePageChartUtility.ChartDataWrapper chart1 = SalesLeaderHomePageChartUtility.fetchChartDataWrapper();
        }        
        test.stopTest();
    }
}