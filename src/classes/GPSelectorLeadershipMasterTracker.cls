@isTest
public class GPSelectorLeadershipMasterTracker {
    
    public static User objuser;
    public Static GP_Project__c project;
    
    @testSetup
    public static void buildDependencyData() {
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        objpinnacleMaster.GP_Description__c ='Test Description';
        insert objpinnacleMaster;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        empObj.GP_HIRE_Date__c = System.today().addDays(11);
        empObj.GP_ACTUAL_TERMINATION_Date__c = System.today().addDays(-11);
        insert empObj; 
        
        GP_Work_Location__c sdo = GPCommonTracker.getWorkLocation();
        sdo.GP_Status__c = 'Active and Visible';
        sdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert sdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(sdo,objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = sdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        objuser = GPCommonTracker.getUser();
        insert objuser; 
       
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Deal_Category__c = 'Sales SFDC';
        insert dealObj ;
        
        GP_Project_Template__c projectTemplate = GPCommonTracker.getProjectTemplate();
        projectTemplate.Name = 'BPM-BPM-ALL';
        projectTemplate.GP_Active__c = true;
        projectTemplate.GP_Business_Type__c = 'BPM';
        projectTemplate.GP_Business_Name__c = 'BPM';
        projectTemplate.GP_Business_Group_L1__c = 'All';
        projectTemplate.GP_Business_Segment_L2__c = 'All';
        insert projectTemplate ;
        
        GP_Project__c parentProject = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        parentProject.OwnerId=objuser.Id;
        parentProject.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert parentProject;
        
        project = GPCommonTracker.getProject(dealObj,'CMITS',projectTemplate,objuser,objrole);
        project.GP_Parent_Project__c = parentProject.id;
        project.OwnerId=objuser.Id;
        project.GP_Approval_Status__c = 'Draft';
        project.GP_GPM_Start_Date__c = System.today();
        project.GP_Start_Date__c = System.today();
        parentProject.GP_End_Date__c = System.today().addDays(10);
        insert project;
        
        Id roleMasterRecordTypeId = GPCommon.getRecordTypeId('GP_Leadership_Master__c', 'Role Master');
        GP_Leadership_Master__c leadershipMaster = GPCommonTracker.getLeadershipMaster('Global Project Manager', roleMasterRecordTypeId, 'Cost Charging', 'Billable');
        insert leadershipMaster;
    }
    @isTest
    public static void testSelectBySetOfId() {
        GPSelectorLeadershipMaster leadershipSelector = new GPSelectorLeadershipMaster();
        GP_Leadership_Master__c expectedLeadership = [SELECT Id FROM GP_Leadership_Master__c LIMIT 1];
        List<GP_Leadership_Master__c> returnedLeadership = leadershipSelector.selectById(new Set<Id> {expectedLeadership.Id});
        System.assertEquals(expectedLeadership.Id, returnedLeadership[0].Id);
    }
    
    @isTest
    public static void testGetLeadershipMaster() {
        GPSelectorLeadershipMaster leadershipSelector = new GPSelectorLeadershipMaster();
        GP_Project__c project = [SELECT Id, GP_Customer_Hierarchy_L4__c, GP_HSL__c, GP_Primary_SDO__c, 
                                 GP_Parent_Customer_Sub_Division__c, GP_Business_Segment_L2__c,
                                 GP_Primary_SDO__r.GP_Oracle_Id__c
                                 FROM GP_Project__c 
                                 LIMIT 1];
        
        GP_Leadership_Master__c expectedLeadership = [SELECT Id FROM GP_Leadership_Master__c LIMIT 1];
        List<GP_Leadership_Master__c> returnedLeadership = leadershipSelector.getLeadershipMaster(project);
    }
    @isTest
    public static void testGetLeadershipMasterWithoutId() {
        GPSelectorLeadershipMaster leadershipSelector = new GPSelectorLeadershipMaster();
        Try{
            leadershipSelector.selectById(null);
        }
        Catch(Exception Ex){}
        Try{
            //leadershipSelector.getLeadershipMaster(null);
            leadershipSelector.getRoleMasterLeadership();
            leadershipSelector.getRoleMasterLeadership(new Set<String>());
            leadershipSelector.getNonBillableRoleMasterLeadership();
            leadershipSelector.getBillableRoleMasterLeadership();
            leadershipSelector.getNonRoleMasterLeadership();
            leadershipSelector.getMandatoryKeyMemberLeadership(project);
            
        }
        Catch(Exception Ex){}
    }
}