/*-------------
        Class Description : This Class is a batch class for quota header creation purposes.
        Organisation      : Tech Mahindra NSEZ
        Created by        : Arjun Srivastava
        Location          : Genpact
        Created Date      : 10 July 2014  
        Last modified date: 10 July 2014
---------------*/
global class QuotaHeaderCreationClass implements Database.Batchable<sObject>
{
   global String Query;
   global QuotaHeaderCreationClass (String q)
   {
      //Query=q;    
      Query='select id from user where isactive=true';
      if(Test.isRunningTest())
      {
        Query +=' Limit 1';
      }
      system.debug('Query=='+Query);
   }
    
   global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
   {    
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope)           // Execute Logic    
   {    
       system.debug('inside execute method=='+scope.size()+'  '+scope);
       set<Id> userlist=new Set<ID>();
       for(User uid: (List<User>)scope)
       userlist.add(uid.id);
       system.debug('inside execute methoduserlist=='+userlist.size()+'  '+userlist);
       GENcInsertQuota.addQuotaheader1(userlist);            
   }
   
    // Logic to be Executed at finish
   global void finish(Database.BatchableContext BC)
   {
       
   }
}