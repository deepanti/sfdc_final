/**********************************************************************************
@Original Author : Iqbal
@Modified By Author : Iqbal
@TL : Madhuri
@date Started : 18 March 2019
@Description : Test class to cover "ExcelExport" and "ExcelExportLead" class.
@Parameters : 
@Return type :
**********************************************************************************/

@isTest
public class ExcelExportTest {
    public static Account oAccount;
    public static Contact oContact,oContact1;
    public static Case oCase;
    public static Lead oLead,oLead1;

    
    private static void setupTestData1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        
        
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc,as','xyz,re',oAccount.Id,'test,tre','Cross-Sell','test121@xyz.com','99999999999,tr');
        Contact oContact1 = GEN_Util_Test_Data.CreateContact('abcf,asf','xyfz,rfe',oAccount.Id,'tesft,tfre','Cross-Sell','test12f1@xyz.com','99499999999,tr');
        //oContact = new Contact(FirstName='Test4fr,gf',LastName='Test4fr,tr',Email='test@testfsds4.com',Phone='5555555554',Country_of_Residence__c='Angola',Title='Testt,yt',Buying_Center__c='IT,Marketing',Level_of_Contact__c='CXO,CXO-2',Archetype__c='No Named,Primary Hunting',mkto_si__Mkto_Lead_Score__c=20,Status__c='Aware,PDO',Recent_Channel_Detail__c='Tesreft,gf');//,Industry_Vertical__c='Healthcare');
        //insert oContact;
        
        oLead = new Lead(FirstName='Test,gh',LastName='Test',Email='test@test.com',Phone='5555555555',Country_Of_Residence__c='India',Company='Test,fd',Title='Test,re',Buying_Center__c='IT,Finance',Level_of_Contact__c='CXO,CXO-1',Archetype__c='No Named,Growth',mkto71_Lead_Score__c=10 ,Status='Raw,MQL',Recent_Channel_Detail__c='Test,jh',Vertical__c='Healthcare,BFS');
        insert oLead;
        
        oLead1 = new Lead(FirstName='Trest,gh',LastName='Trest',Email='terst@test.com',Phone='5565555555',Country_Of_Residence__c='India',Company='Test,fd',Title='Test,re',Buying_Center__c='IT,Finance',Level_of_Contact__c='CXO,CXO-1',Archetype__c='No Named,Growth',mkto71_Lead_Score__c=10 ,Status='Raw,MQL',Recent_Channel_Detail__c='Test,jh',Vertical__c='Healthcare,BFS');
        insert oLead1;
      
    }
       private static void setupTestData2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                    oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        
        
       // Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell','test121@xyz.com','99999999999');
        
        oContact = new Contact(FirstName='Test4fr',LastName='Test4fr',Email='test@testfsds4.com',Phone='5555555554',Country_of_Residence__c='Angola',Title='Testt',Buying_Center__c='IT',Level_of_Contact__c='CXO',Archetype__c='No Named',mkto71_Lead_Score__c=10,Status__c='Aware',Recent_Channel_Detail__c='Tesreft');//,Industry_Vertical__c='Healthcare');
        insert oContact;
        oContact1 = new Contact(FirstName='Tesrt4fr',LastName='Tesrt4fr',Email='tesrt@testfsds4.com',Phone='5555655554',Country_of_Residence__c='Angola',Title='Testrt',Buying_Center__c='IT',Level_of_Contact__c='CXO',Archetype__c='No Named',mkto71_Lead_Score__c=10,Status__c='Aware',Recent_Channel_Detail__c='Tesreft');//,Industry_Vertical__c='Healthcare');
        insert oContact1;
           
        oLead = new Lead(FirstName='Test',LastName='Test',Email='test@test.com',Phone='5555555555',Country_Of_Residence__c='India',Company='Test',Title='Test',Buying_Center__c='IT',Level_of_Contact__c='CXO',Archetype__c='No Named',mkto71_Lead_Score__c=10 ,Status='Raw',Recent_Channel_Detail__c='Test',Vertical__c='Healthcare');
        insert oLead;
        oLead1 = new Lead(FirstName='Tetst',LastName='Ttest',Email='tetst@test.com',Phone='5555555955',Country_Of_Residence__c='India',Company='Test',Title='Test',Buying_Center__c='IT',Level_of_Contact__c='CXO',Archetype__c='No Named',mkto71_Lead_Score__c=10 ,Status='Raw',Recent_Channel_Detail__c='Test',Vertical__c='Healthcare');
        insert oLead1;
      
    }
    

    static testMethod void BatchTest1(){
        setupTestData1();
        String Querystring = 'SELECT name,Email,Phone,Country_of_Residence__c,DSE__DS_Company__c,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto71_Lead_Score__c,Status__c,Recent_Channel_Detail__c,Industry_Vertical__c FROM Contact  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false';
     String QuerystringLd = 'SELECT name,Email,Phone,Country_of_Residence__c,Company,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto71_Lead_Score__c,Lifecycle_Status__c,Recent_Channel_Detail__c,Vertical__c FROM Lead  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false';
    
        Test.startTest();

            ExcelExport obj = new ExcelExport(Querystring);
            DataBase.executeBatch(obj); 

        ExcelExportLead objLd = new ExcelExportLead(QuerystringLd);
            DataBase.executeBatch(objLd); 
        
        Test.stopTest();
    }
    
    static testMethod void BatchTest2(){
         setupTestData2();
        String Querystring = 'SELECT name,Email,Phone,Country_of_Residence__c,DSE__DS_Company__c,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto71_Lead_Score__c,Status__c,Recent_Channel_Detail__c,Industry_Vertical__c FROM Contact  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false';
     String QuerystringLd = 'SELECT name,Email,Phone,Country_of_Residence__c,Company,Title,Buying_Center__c,Level_of_Contact__c,RecordTypeId,Archetype__c,mkto71_Lead_Score__c,Lifecycle_Status__c,Recent_Channel_Detail__c,Vertical__c FROM Lead  where Marketing_Suspended_Date__c = NULL AND HasOptedOutOfEmail = false AND DoNotCall = false';
    
        Test.startTest();

            ExcelExport obj = new ExcelExport(Querystring);
            DataBase.executeBatch(obj); 

            ExcelExportLead objLd = new ExcelExportLead(QuerystringLd);
            DataBase.executeBatch(objLd); 
        
        Test.stopTest();
    }
    

    
}