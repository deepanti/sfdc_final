@isTest
Public Class VIC_ClonePlanCtrlTest{
    Public static user objuser;
    Public static user objuser1;
    Public static user objuser2;
    Public static user objuser3;
    Public static OpportunityLineItem  objOLI;
    Public static Opportunity objOpp;
    Public static Master_VIC_Role__c masterVICRoleObj;
    Public static Plan__c planObj1;
    static testmethod void  testYear(){
        
         LoadData();
         VIC_ClonePlanCtrl obj = new VIC_ClonePlanCtrl();
         VIC_ClonePlanCtrl.getYearValid(planObj1.id,'2018');
       
         VIC_ClonePlanCtrl.savePlan(planObj1.id,'2018');
         VIC_ClonePlanCtrl.getFinancialYear();
         System.AssertEquals(200,200);

        
        
    }
    
     static testmethod void  testYearNew(){
        
         LoadData();
         planObj1.Year__c='2017';
         update planObj1;
         VIC_ClonePlanCtrl.getYearValid(planObj1.id,'2018');
         System.AssertEquals(200,200);

        
        
    }
    
    static void LoadData()
    {
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
        insert objuser;

        objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;

        objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;

        objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;

        masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        masterVICRoleObj.Horizontal__c='Enterprise sales';
        masterVICRoleObj.Role__c='BD';
        insert masterVICRoleObj;

        User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;

        APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
        insert objConga;
        
        planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='BDE';    
        insert planobj1;

        VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
        insert VICRoleObj;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;  

        objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objopp.ownerid=objuser.id;
        objopp.Sales_country__c='Canada';

        insert objOpp;

        objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.vic_Contract_Term__c=24;
        objOLI.Product_BD_Rep__c=objuser.id;
        objOLI.vic_VIC_User_3__c=objuser2.id;
        objOLI.vic_VIC_User_4__c=objuser3.id;   
        objOLI.vic_Is_Split_Calculated__c=false;
        objOLI.vic_CPQ_Deal_Status__c= 'Active';  
        objOLI.vic_TCV_EBIT_Consideration_ForNPV__c='5';
        insert objOLI;
        update objOLI;

        system.debug('objOLI'+objOLI);

        Target__c targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;

        Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='Profitable_Bookings_IO'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;

        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
        
        
    }
 
    
    


}