@isTest
public class AddProductControllerTest {
    public static Opportunity opp;
    public static List<OpportunityLineItem> oliLst=new List<OpportunityLineItem>();
    
   public static testMethod void testControllerMethods(){
       AddProductController.getLocalCurrency();
       AddProductController.getDeliveringOrganization();
       AddProductController.getDeliveryLocation();
       setupTestData();
       AddProductController.getOpportunityClosedDate(opp.ID);
       AddProductController.getFieldDependencies('Americas');
       AddProductController.getOpportunityProducts(opp.ID);
       AddProductController.getProducts(opp.ID);
       AddProductController.getQSRMDetails(opp.ID);
       
       Product2 product_obj = new Product2(name='test pro',isActive = true,Industry_Vertical__c = 'Manufacturing',
                                            Nature_of_Work__c ='Consulting',Service_Line__c='Invoice To Cash');
        insert product_obj;
        system.debug(':--product-->'+product_obj);
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                    UnitPrice = 0.00, UseStandardPrice = false, isActive=true);
        insert entry;
       
   		AddProductController.getProductDetail(product_obj.ID);
    	AddProductController.getProductsOnload(opp.ID,product_obj.ID);
        
        OpportunityLineItem objItemOld = New OpportunityLineItem(OpportunityId = opp.Id,PricebookEntryId = entry.Id,
        	                                                     Custom_Id__c='test',UnitPrice = 250,Revenue_Start_Date__c = Date.today()+5,
                                                                 Contract_Term__c =4,Revenue_Exchange_Rate__c=6 );
        insert objItemOld;
       
       /* system.debug('oli id'+objItemOld);
        oliLst.add(objItemOld);
        system.debug('oli list'+oliLst);
        AddProductController.createRevenueSchedule(oliLst,opp.Id);
        AddProductController.createRevenueSchedule(oliLst);
        system.debug('createRevenuSch is wokring');*/
        GP_Deal__c objdeal = new GP_Deal__c(GP_Product__c ='test',GP_Product_Id__c =product_obj.id,GP_Service_Line__c = product_obj.Service_Line__c,
                                           GP_Nature_of_Work__c = product_obj.Nature_of_Work__c,GP_Start_Date__c = date.today(),GP_End_Date__c = date.today()+5,GP_TCV__c = 12,CurrencyIsoCode= 'USD',
                                            GP_OLI_SFDC_Id__c = objItemOld.id);
        insert objdeal;
        system.debug(':--test data objItemOld--:'+objItemOld);
       
    	AddProductController.getProductsOnChange(opp.ID,product_obj.Service_Line__c,product_obj.Nature_of_Work__c,product_obj.Name);
        string loginuser = Userinfo.getUserId();
  
           
        AddProductController.SaveEditOliItem(product_obj.Service_Line__c,product_obj.Nature_of_Work__c,product_obj.Name,objItemOld.id,opp.ID,
        									25.02,22.02,'AUD', 40.23, string.valueof(date.today()+5),'IMS','GGO',loginuser,'INDIA',product_obj.id);
           
       	AddProductController.deleteOpportunityProducts(opp.ID);
    	AddProductController.getQSRMDetails(opp.ID);
     	AddProductController.getOppProductDetails(objItemOld.id);
       AddProductController.createRevenueSchedule(oliLst);
       
      
        
        
        

    }
    
    public static testMethod void testSaveOpportunity(){
        setupTestData();    
       
        Product2 product_obj = new Product2(name='test pro', CanUseRevenueSchedule = true,isActive = true,Industry_Vertical__c ='Insurance',Nature_of_Work__c ='Consulting',Service_Line__c='Invoice To Cash');
        insert product_obj;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                                                    UnitPrice = 0.00, UseStandardPrice = false, isActive=true);
        insert entry;
        
        String opportunityProductList = '[{\"Product2Id\" : \"'+product_obj.ID+'\" ,\"ProductName\" :\"'+product_obj.Name+'\",\"OpportunityId\" :\"' 
            +opp.Id+'\",\"ContractTerm\" :\"5\" ,\"LocalCurrency\" : \"USD\",\"TotalPrice\" : \"50000.00\",\"RevenueStartDate\" : \"6/12/2018'
             +'\", \"DeliveringOrganisation\" :\"\" ,\"SubDeliveringOrganisation\" : \"\",\"ProductBDRep\" : \"\",\"DeliveryLocation\" : \"\" '
             +', \"FTE\" : \"\",\"SubDeliveringOrganizationList\" : [],\"subOrganizationFlag\" : true,\"Ebit\" : \"\",\"PricerName\" : \"\" '
            +'}]';
        
         String closedDate = String.valueOf(Date.today()+2);
      AddProductController.saveOpportunityProducts(opportunityProductList, closedDate);
       
    }
  
    
    
    private static void setupTestData(){
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Genpact Super Admin']; 
        
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
                
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Sales_Unit__c salesunit = new Sales_Unit__c(Name='Sales Unit 1',Sales_Leader__c = u.id);
        insert salesunit;
        
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.id,oAT.Id,'Test Account','GE',
                                                            oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676'); 
        oAccount.Sales_Unit__c = salesunit.id;
        oAccount.Industry_Vertical__c = 'Manufacturing';
        update oAccount; 
        Contact oContact = GEN_Util_Test_Data.CreateContact('abc','xyz',oAccount.Id,'test','Cross-Sell',
                                                            'test121@xyz.com','99999999999');
        
        System.runAs(u){
            Id RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Discover Opportunity').getRecordTypeId();
            
            opp =new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today()+1, recordTypeId = RecordTypeId,
                                 Revenue_Start_Date__c=system.today()+1,accountid=oAccount.id,W_L_D__c='',
                                 Competitor__c='Accenture',Deal_Type__c='Competitive', contact1__c = oContact.ID, role__c = 'Other');
            insert opp;
            Test.stopTest();
            
        }
    }
}