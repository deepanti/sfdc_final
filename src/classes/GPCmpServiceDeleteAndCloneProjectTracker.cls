//================================================================================================================
//  Description: Test Class for GPCmpServiceDeleteAndCloneProject
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          09-May-2018                  Ved Prakash              Initial Version
//================================================================================================================
@isTest public class GPCmpServiceDeleteAndCloneProjectTracker {
    public static GP_Project__c prjObj, objChildProj;

    static void setupCommonData() {
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        dealObj.GP_Business_Type__c = 'BPM';
        dealObj.GP_Business_Name__c = 'BPM';
        insert dealObj;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp;

        prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        insert prjObj;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Timesheet_Entry__c timeshtentryObj1 = GPCommonTracker.getTimesheetEntry(empObj, prjObj, timesheettrnsctnObj);
        insert timeshtentryObj1;

        GP_Project_Task__c projectTaskObj1 = GPCommonTracker.getProjectTask(prjObj);
        projectTaskObj1.GP_SOA_External_ID__c = 'test123';
        insert projectTaskObj1;

        GP_Project_Version_History__c projectVersionHistory1 = GPCommonTracker.getProjectVersionHistory(prjObj.Id);
        insert projectVersionHistory1;

        GP_Project_Classification__c projectClassification1 = GPCommonTracker.getProjectClassification(prjObj.Id);
        insert projectClassification1;

        Business_Segments__c BSobj = GPCommonTracker.getBS();
        insert BSobj;

        Sub_Business__c SBobj = GPCommonTracker.getSB(BSobj.id);
        insert SBobj;

        Account accobj = GPCommonTracker.getAccount(BSobj.id, SBobj.id);
        insert accobj;

        Opportunity oppobj = GPCommonTracker.getOpportunity(accobj.id);
        oppobj.Name = 'test opportunity';
        oppobj.StageName = 'Prediscover';
        oppobj.AccountId = accobj.id;
        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        System.debug('objOpp.Actual_close_Date__c 1:' + oppobj.Actual_close_Date__c);
        insert oppobj;

        oppobj.Actual_Close_Date__c = System.Today().adddays(-15);
        update oppobj;

        Opportunity objOpp = [Select id, Opportunity_ID__c from Opportunity where id =: oppobj.id];
        GP_Opportunity_Project__c oppproobj = GPCommonTracker.getoppproject(accobj.id);
        oppproobj.GP_Opportunity_Id__c = objOpp.Opportunity_ID__c;
        oppproobj.GP_Probability__c = 70;
        insert oppproobj;

        prjObj.GP_Opportunity_Project__c = oppproobj.Id;
        prjObj.GP_Oracle_Status__c = 'S';
        UPDATE prjObj;

        GP_Deal__c dealObj1 = GPCommonTracker.getDeal();
        dealObj1.GP_Business_Type__c = 'BPM';
        dealObj1.GP_Business_Name__c = 'BPM';
        insert dealObj1;

        objChildProj = GPCommonTracker.getProject(dealObj1, 'CMITS', objprjtemp, objuser, objrole);
        objChildProj.OwnerId = objuser.Id;
        objChildProj.GP_Parent_Project__c = prjObj.Id;
        objChildProj.GP_Customer_Hierarchy_L4__c = accobj.id;
        objChildProj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        objChildProj.GP_Project_type__c = 'Account';
        objChildProj.GP_Oracle_Status__c = 'E';
        insert objChildProj;

        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj, objChildProj, timesheettrnsctnObj);
        insert timeshtentryObj;

        GP_Project_Version_History__c projectVersionHistory = GPCommonTracker.getProjectVersionHistory(objChildProj.Id);
        insert projectVersionHistory;

        GP_Project_Classification__c projectClassification = GPCommonTracker.getProjectClassification(objChildProj.Id);
        insert projectClassification;
    }

    @isTest public static void testDeleteProject() {
        setupCommonData();
        GPAuraResponse returnedResponse = GPCmpServiceDeleteAndCloneProject.cloneAndDeleteProjectService(prjObj.Id);
        returnedResponse = GPCmpServiceDeleteAndCloneProject.cloneAndDeleteProjectService(null);
    }

    @isTest public static void testcloneService() {
        setupCommonData();
        GPAuraResponse returnedResponse = GPCmpServiceDeleteAndCloneProject.cloneAndDeleteProjectService(objChildProj.Id);
        System.assertEquals(true, returnedResponse.isSuccess);

        returnedResponse = GPCmpServiceDeleteAndCloneProject.cloneAndDeleteProjectService('test123');
    }
}