@RestResource(urlMapping='/ProfileUpdate/*')
global with sharing class ProfileUpdate {

    @HttpPatch
    global static String updateProfile(String userId, String profileId) {
        
        update new User(Id=userId,ProfileId = profileId,Has_Stale_Deals__c=false,Old_Profile_Id__c='');
           
        return 'Profile Updated';
      }

}