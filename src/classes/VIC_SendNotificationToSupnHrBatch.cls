/**
 * @author: kiran Kumar
 * @since: 06/08/2018
 * @description: This batch class sends notification to Supervisor based on Incentives Calculated 
 */
public class VIC_SendNotificationToSupnHrBatch implements Database.Batchable <sObject> , Database.stateful {
    public VIC_Process_Information__c processInfo = vic_CommonUtil.getVICProcessInfo();
    public set<id> setUserId = new set<id>();
    public Map<id,string> mapUserName = new Map<id,string>();
    public Map<id,string> mapUserEmail = new Map<id,string>();
        
    public Database.QueryLocator start(Database.BatchableContext bc){
        Date targetStartDate = DATE.newInstance(Integer.ValueOf(processInfo.VIC_Process_Year__c),1,1);      
        return Database.getQueryLocator([SELECT Id, Target__r.Id, Target__r.User__r.Supervisor__r.Email,Target__r.User__r.Supervisor__c,
                                            Target__r.User__r.Supervisor__r.Name,
                                            (SELECT Id, VIC_Auto_Sweep_Date_for_SL__c 
                                                FROM Target_Achievements__r
                                                WHERE vic_Status__c IN ('Supervisor Pending')
                                                AND VIC_Auto_Sweep_Date_for_SL__c = NULL)
                                            FROM Target_Component__c
                                            WHERE Target__r.Start_Date__c =:targetStartDate
                                            AND Target__r.vic_Incentive_Pending_From_Supervisor__c != null
                                            AND Target__r.vic_Incentive_Pending_From_Supervisor__c > 0
                                            AND Target__r.Is_Active__c = TRUE]);        
    }

    public void execute(Database.BatchableContext bc, List <Target_Component__c> lstComp){ 
        List<Target_Achievement__c> lstAchievement = new List<Target_Achievement__c>();
        for(Target_Component__c c:lstComp){
            if(c.Target__r.User__r.Supervisor__c != null){
                setUserId.add(c.Target__r.User__r.Supervisor__c);
                mapUserName.put(c.Target__r.User__r.Supervisor__c, c.Target__r.User__r.Supervisor__r.Name);
                mapUserEmail.put(c.Target__r.User__r.Supervisor__c, c.Target__r.User__r.Supervisor__r.Email);
            }
            if(c.Target_Achievements__r != null && c.Target_Achievements__r.size() > 0){
                integer autoSweepDays = integer.valueof(processInfo.VIC_Calculate_Sales_Leader_Day__c);
                for(Target_Achievement__c a: c.Target_Achievements__r){
                    lstAchievement.add(new Target_Achievement__c(
                        id = a.id,
                        VIC_Auto_Sweep_Date_for_SL__c = vic_CommonUtil.CheckDates(autoSweepDays)
                    ));
                }
            }           
        }
        
        if(lstAchievement.size() > 0){
            UPDATE lstAchievement;
        }       
    }
    
    public void finish(Database.BatchableContext bc){
        if(setUserId != null && setUserId.size() > 0){
            String orgWideEmail= VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Supervisor_Notification_Email');            
            OrgWideEmailAddress owea = [select id FROM OrgWideEmailAddress WHERE DisplayName=:orgWideEmail];
            
            String strTemplateName = VIC_IncentiveConstantCtlr.getIncentiveConstantByDeveloperName('Supervisor_Notification_Email_DeveloperN');
            EmailTemplate template = [SELECT Id, body, HtmlValue,Subject 
                                      FROM EmailTemplate
                                      WHERE DeveloperName =:strTemplateName];
            
            
            List <Messaging.SingleEmailMessage> mails = new List <Messaging.SingleEmailMessage>();
            for(String s :setUserId){
                String strName = mapUserName.get(s);
                String strEmailId = mapUserEmail.get(s);
                String strHTMLBody = template.HtmlValue;
                strHTMLBody = strHTMLBody.replace('{!User.Name}', strName);
                
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
                msg.setTemplateId(template.Id);
                msg.setSaveAsActivity(false);          
                msg.setTargetObjectId(s);
                msg.setSubject(template.Subject);
                msg.setHtmlBody(strHTMLBody);           
                msg.setOrgWideEmailAddressId(owea.Id);
                
                mails.add(msg);
            }
            
            if(mails != null && mails.size() > 0){
                Messaging.sendEmail(mails);
            }
        }
   }
}