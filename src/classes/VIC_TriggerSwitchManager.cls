/*
    @Discription: This class will be used for getting trigger active status for custom meta data api
    @Test Class: TriggerSwitchManagerTest
    @Author: Vikas Rajput
*/
public with sharing class VIC_TriggerSwitchManager{
    
    @TestVisible
    private static Map<String, Boolean> mapTriggerSwitch;
    static{
        mapTriggerSwitch = new Map<String, Boolean>();
        for(VIC_Trigger_Switches__mdt objTriggerSwitchITR : [select DeveloperName,VIC_Opportunity_Line_Item__c from VIC_Trigger_Switches__mdt]){
            mapTriggerSwitch.put(objTriggerSwitchITR.DeveloperName, objTriggerSwitchITR.VIC_Opportunity_Line_Item__c );
        }
    }
    /*
        @Discription: Common function to identify that trigger is active or not ?
        @Param: switchApiName(developer name of metadata records )
        @Author: Vikas Rajput
    */
    public static Boolean isTriggerSwitchActive(String switchApiName){
        return mapTriggerSwitch.containsKey(switchApiName) && mapTriggerSwitch.get(switchApiName);
    }

}