@isTest
public class GPSelectorResourceAllocationTracker {
    public Static GP_Resource_Allocation__c resourceAllocationObj;
    public Static GP_Employee_Master__c empObj;
    public static set<id> setofemplyeeid;
    public static GP_Resource_Allocation__c objresouceall = new GP_Resource_Allocation__c();
    
    @testSetup
    public static void buildDependencyData() {
       GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetGlobalSettingspinnacleMaster();
        insert objpinnacleMaster;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Type_of_Role__c = 'SDO';
        objrole.GP_Work_Location_SDO_Master__c = objSdo.id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        insert prjObj ;
        
        GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        insert timeshtentryObj ;
        
        GP_Resource_Allocation__c resourceAllocationObj = GPCommonTracker.getResourceAllocation(empObj.id,prjObj.id);
        insert resourceAllocationObj;
        
        GP_Address__c  address = GPCommonTracker.getAddress();
        insert address ;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj;
        
       setofemplyeeid = new set<id>();
        setofemplyeeid.add(empObj.id);
    }
    public static void testData()
    {
         objresouceall = [Select Id,Name from GP_Resource_Allocation__c Limit 1];
    }
    
    @isTest
    public static void testgetSObjectType() {
        GPSelectorResourceAllocation resourceAllocationSelector = new GPSelectorResourceAllocation();
        Schema.SObjectType returnedType = resourceAllocationSelector.getSObjectType();
        Schema.SObjectType expectedType = GP_Resource_Allocation__c.getSobjectType();
        System.assertEquals(returnedType, expectedType);
    }
    
    @isTest
    public static void testselectById()
    {
        set<Id> setofId = new Set<Id>();
        GPSelectorResourceAllocation resourceAllocationSelector = new GPSelectorResourceAllocation();
        setofId.add(objresouceall.id);
        testData();
        List<GP_Resource_Allocation__c> returnedresourceall = resourceAllocationSelector.selectById(setofId);
        //List<GP_Resource_Allocation__c> listofreturnedresourceall = resourceAllocationSelector.selectById(new Set<Id>());
        System.assertEquals(returnedresourceall.size(), 0);
    }
    
      @isTest
    public static void testselectResourceAllocationRecord() {
        try{
     GPSelectorResourceAllocation resourceAllocationSelector = new GPSelectorResourceAllocation();
        //resourceAllocationObj = [Select Id,Name from GP_Resource_Allocation__c Limit 1];
       resourceAllocationSelector.selectResourceAllocationRecord(null);
        //System.assertEquals(returnedType, expectedType);
            }catch(Exception e)
            {}

    }
    
    @isTest    
    public static void testselectResourceAllocationRecordsOfEmployees() {  
        GPSelectorResourceAllocation resourceAllocationSelector = new GPSelectorResourceAllocation();    
        resourceAllocationSelector.selectResourceAllocationRecordsOfEmployees(null);
        //System.assertEquals(returnedType, expectedType);
    }
    
    @isTest    
    public static void testselectResourceAllocationRecordsForAMonth() {
        empObj = [Select Id,Name from GP_Employee_Master__c Limit 1];
        GPSelectorResourceAllocation resourceAllocationSelector = new GPSelectorResourceAllocation();  
        string empid = empObj.id;
        resourceAllocationSelector.selectResourceAllocationRecordsForAMonth(null,7);
        resourceAllocationSelector.selectResourceAllocationRecordsOfProject(null);
        resourceAllocationSelector.selectResourceAllocationRecords(null);
        resourceAllocationSelector.selectResourceAllocationRecordsForAMonth(null, null, null);
        resourceAllocationSelector.selectResourceAllocationRecordsForAProject(null);
        resourceAllocationSelector.selectProjectResourceAllocationRecords(null);
        resourceAllocationSelector.selectResourceAllocationRecordsOfProjectandWorkLocation(null, null);
        resourceAllocationSelector.selectResourceAllocationRecordsOfEmployeesAndProjects(null, null);
        //System.assertEquals(returnedType, expectedType);
    }
}