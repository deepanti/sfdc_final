@isTest 
public class GPCmpServiceProjectAssignmentTracker{
    @isTest static void testPendingApprovals(){
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj;

        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS;

        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB;

        Account accobj = GPCommonTracker.getAccount(objBS.id, objSB.id);
        insert accobj;

        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;

        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;

        GP_Role__c objrole = GPCommonTracker.getRole(objSdo, objpinnacleMaster);
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        insert objrole;

        User objuser = GPCommonTracker.getUser();
        insert objuser;

        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole, objuser);
        insert objuserrole;

        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours;

        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;

        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj;

        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;

        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp;

        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj.OwnerId = objuser.Id;
        prjObj.GP_CRN_Number__c = iconMaster.Id;
        prjObj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj.GP_Approval_Status__c = 'Draft';
        prjObj.GP_Primary_SDO__c = objSDO.id;
        insert prjObj;
        
        GPCmpServiceProjectAssignment.getUserlist(prjObj.Id);
        GPCmpServiceProjectAssignment.assignToOwner(prjObj.Id);
        GPCmpServiceProjectAssignment.updateCurrentWorkingUser(prjObj.Id, UserInfo.getUserId(),true);
        
        GP_Project__c prjObj1 = GPCommonTracker.getProject(dealObj, 'CMITS', objprjtemp, objuser, objrole);
        prjObj1.OwnerId = objuser.Id;
        prjObj1.GP_CRN_Number__c = iconMaster.Id;
        prjObj1.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjObj1.GP_Auto_Reject_Date__c = System.Today().adddays(-4);
        prjObj1.GP_Approval_Status__c = 'Draft';
        prjObj1.GP_Primary_SDO__c = objSDO.id;
        prjObj1.GP_Current_Working_User__c = objuser.Id;
        insert prjObj1;
        
        GPCmpServiceProjectAssignment.getUserlist(prjObj1.Id);
        GPCmpServiceProjectAssignment.assignToOwner(prjObj1.Id);
        GPCmpServiceProjectAssignment.updateCurrentWorkingUser(prjObj1.Id, UserInfo.getUserId(),false);
        
        
    }    

}