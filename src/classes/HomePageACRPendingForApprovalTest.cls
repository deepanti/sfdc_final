@isTest
public class HomePageACRPendingForApprovalTest {
    @testSetup
    public static void createData()
    {
        test.startTest();
        User testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac@bc.com','ac@bc.com');
        insert testUser;
        User saleLeader = TestDataFactoryUtility.createTestUser('Genpact SL / CP','acrandom@bc.com','acrandom123@bc.com');
        insert saleLeader;
        Account_Creation_Request__c ACRRecord=new Account_Creation_Request__c(Website__c='www.testacr.com',Name='randomnameabcd',User_sales_leader__c=saleLeader.id);
        insert ACRRecord;
        Approval.ProcessSubmitRequest req1 = 
            new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(ACRRecord.id);
        
        // Submit on behalf of a specific submitter
        req1.setSubmitterId(UserInfo.getUserId()); 
        
        // Submit the record to specific process and skip the criteria evaluation
        req1.setProcessDefinitionNameOrId('Approval_process_for_Account_Creation2');
        req1.setSkipEntryCriteria(true);
        
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);
        test.stopTest();
    }
    
    public static testMethod void fetchACRPendingForApporvalTest()
    {
        try
        {
            
            User salesLeader = [Select id,Name from User where email = 'acrandom123@bc.com'];
            
            System.runAs(salesLeader)
            {
                Test.startTest();
                List<HomePageACRPendingForApproval.ApprovalDetailsWrapper> wrapperLst = HomePageACRPendingForApproval.fetchACRPendingForApporval();
                
                Test.stopTest();
            }
        }
        catch(Exception e)
        {
            
        }
        
    }
    public static testMethod void approvePendingItemsTest()
    {
        try
        {
            User salesLeader = [Select id,Name from User where email = 'acrandom123@bc.com'];
            Id itemPendingForApproval = [SELECT Id,ProcessInstance.TargetObjectId,CreatedDate FROM ProcessInstanceWorkItem 
                                         WHERE ProcessInstance.Status = 'Pending' AND ActorId =:salesLeader.id Limit 1].Id;
            System.runAs(salesLeader)
            {
                Test.startTest();
                Boolean approve = HomePageACRPendingForApproval.approvePendingItems(itemPendingForApproval,'Accept','Approve');
                System.assertEquals(true, approve);            
                Test.stopTest(); 
            }
        }
        catch(Exception e)
        {
            
        }
    }
}