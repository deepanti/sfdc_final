global class olddatateamupdate implements Database.Batchable<sobject>, Database.Stateful {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        date dd = Date.newInstance(2018, 1, 1);
        string stage='6. Signed Deal';
        //     string i = '0069000000vzu9K';
        //        String query = 'Select id,ownerid,name,TCV1__c,account.Client_Partner__c,account.Sales_Leader_User_Id__c From opportunity where id =:i';
        String query = 'Select id,ownerid,name,TCV1__c,account.ownerid, account.Client_Partner__c,account.Sales_Leader_User_Id__c From opportunity where TCV1__c>0 and actual_close_date__c > :dd and account.Sales_Leader_User_Id__c!=null and account.Client_Partner__c !=null and stagename=:stage';
        system.debug('query='+query);
        if(Test.isRunningTest())
            query = 'Select id,name,ownerid,TCV1__c,account.Client_Partner__c,account.Sales_Leader_User_Id__c,accountid From opportunity limit 100';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List <opportunity> opp) {
        //    datetime d = Datetime.newInstance(2016, 1, 1);
        list<id> oppids = new list<id>();
        set<id> accid = new set<id>();
        Map<String, opportunity> oppmap = new Map<String, opportunity>(opp);
        for(opportunity objCS : opp)
        {
            oppids.add(objCS.id);
            accid.add(objCS.AccountId);
        }
        system.debug('id='+oppids);
        //oppids.add([select id from opportunity where id = '0069000000zvSCL'].id);
        Map<string, OpportunityTeamMember> opptmvalues = new Map<string, OpportunityTeamMember>();
        Map<string, OpportunityTeamMember> opptmbdrepvalues = new Map<string, OpportunityTeamMember>();
        list<string> roles = new list<string>{'Opportunity Owner','Super SL','SL'};
        map<string, OpportunitySplit> addsplit = new map<string, OpportunitySplit>();
        list<OpportunitySplit> addsplitbd = new list<OpportunitySplit>();        
        OpportunitySplit tpsplit =new OpportunitySplit();
        OpportunitySplit tempsplita;
        decimal a,b,c,d;
        string unique,u2,u3,u4,u5,u6,u7,u8,u9,u10;
        list<user> actbdreps = new list<user>();
        actbdreps = [select id from user where isactive=true and profileid!='00e90000000WFRNAA4'];
        system.debug('active users ==='+actbdreps.size());
        //search for team members in account***************************** 
        //Client_Partner__c = Super SL
        //Sales_Leader_User_Id__c = SL
        map<id,account> acct =new map<id,account>();
        map<string,opportunity> otmsl=new  map<string,opportunity>();
        map<string,opportunity> otmssl = new  map<string,opportunity>();
        map<string,opportunity> owner = new  map<string,opportunity>();
        list<string> oppspvaluesa = new list<string>();
        for(OpportunitySplit ots :[select OpportunityId,SplitOwnerId from OpportunitySplit where OpportunityId in :oppids and SplitTypeId = '14990000000fxXIAAY'])
        {
            unique=string.valueOf(ots.OpportunityId);
            u2=string.valueOf(ots.SplitOwnerId);
            u3=unique + u2;
            oppspvaluesa.add(u3);
        }
                set<id> actusr =new set<id>();
        for(user au:actbdreps)
        {
            actusr.add(au.id);
        }
        system.debug('existing splits==='+oppspvaluesa);
        for(opportunity o:opp)
        {
            unique=string.valueOf(o.Id);
            u2=string.valueOf(o.account.Client_Partner__c);
            u3=unique + u2;
            if(!oppspvaluesa.contains(u3)&&!otmssl.containsKey(u3)&&actusr.contains(o.account.Client_Partner__c))
            {
                otmssl.put(u3,o); 
                tpsplit = new opportunitysplit();
            tpsplit.OpportunityId = o.Id;
            tpsplit.SplitPercentage = 100;
            tpsplit.SplitTypeId = '14990000000fxXIAAY';
            tpsplit.SplitOwnerId = o.account.Client_Partner__c;
            if(!addsplit.containskey(u3))
                addsplit.put(u3,tpsplit);
            }
            u4=string.valueOf(o.account.Sales_Leader_User_Id__c);
            u5=unique + u4;
            if(!oppspvaluesa.contains(u5)&&!otmssl.containsKey(u5)&&actusr.contains(o.account.Sales_Leader_User_Id__c))
            {    otmssl.put(u5,o);
             tpsplit = new opportunitysplit();
            tpsplit.OpportunityId = o.Id;
            tpsplit.SplitPercentage = 100;
            tpsplit.SplitTypeId = '14990000000fxXIAAY';
            tpsplit.SplitOwnerId = o.account.Sales_Leader_User_Id__c;
            if(!addsplit.containskey(u5))
                addsplit.put(u5,tpsplit);
            }
            u6=string.valueOf(o.ownerid);
            u7=unique + u6;
            if(!oppspvaluesa.contains(u7)&&!otmssl.containsKey(u7)&&actusr.contains(o.ownerid))
             {    otmssl.put(u7,o);
             tpsplit = new opportunitysplit();
            tpsplit.OpportunityId = o.Id;
            tpsplit.SplitPercentage = 100;
            tpsplit.SplitTypeId = '14990000000fxXIAAY';
            tpsplit.SplitOwnerId = o.ownerid;
            if(!addsplit.containskey(u7))
                addsplit.put(u7,tpsplit);
            }

        }

        list<opportunitysplit> l=new list<opportunitysplit>();       
        for(string spl:addsplit.keyset())
        {
            l.add(addsplit.get(spl));
        }
            
        system.debug('addsplit==='+addsplit);


        try
        {
            //  insert teammember;
            insert l;
        }
        catch(exception e)
        {
            string allstring = string.join(l,',');
            CreateErrorLog.createErrorRecord(null,e.getMessage(), allstring, e.getStackTraceString(),'olddatateamupdate', ' ','Fail','',String.valueOf(e.getLineNumber()));     
            system.debug('error e='+e.getmessage());
        }
    }        
    
    
    global void finish(Database.BatchableContext BC) {
    }
}