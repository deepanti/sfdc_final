global class TsDealsIdentifierBatch implements Database.Batchable<sObject>, Database.Stateful{
    
    global Set<User> stale_user_list = new Set<User>();

     global Database.QueryLocator start(Database.BatchableContext BC)                 // Start Method
    {  
        return Database.getQueryLocator([Select id, ownerid,Opportunity_ID__c, Opportunity_Source__c,Deal_Nature__c,Insight__c, Name,Type_of_deal_for_non_ts__c, stagename, deal_cycle_ageing__c, Transformation_ageing__c, Type_of_deal__c,
                                  Type_Of_Opportunity__c,StallByPassFlag__c, QSRM_status2__c, closedate, Ageing_large_retail__c, Opportunity_Age__c,
                                  TSInights__c, Amount, Accountid, Account.Name, Last_stage_change_date__c, 
                                  Previous_Stage__c, TCV1__c, QSRM_Type__c, Move_Deal_Ahead__c, Cycle_Time__c,
                                  Formula_Hunting_Mining__c, Deal_Type__c, Nature_of_Work_highest__c, Discover_MSA_CT_Check__C,
                                  Define_MSA_CT_Check__C, Contract_Status__c, Onbid_MSA_CT_Check__c, DownSelect_MSA_CT_Check__C,
                                  Confirmed_MSA_CT_Check__C from opportunity where Type_Of_Opportunity__c = 'TS' and (Transformation_14_days_check_age__c='True' OR 
                                      StallByPassFlag__c='True') and (StageName ='1. Discover' OR StageName = '2. Define' OR 
                                      StageName ='3. On Bid' OR StageName ='4. Down Select' OR StageName ='5. Confirmed')]);
    }
    
     global void execute(Database.BatchableContext BC, List<sObject> oppList)           // Execute Logic    
    {  
        list<User> userList = staleDealOwners.addTsDealsUsers(opplist, 'TS_DEALS');
        System.debug('userList in batch=='+userList);
        if(userList != null){
        	stale_user_list.addAll(userList);     
        }
        System.debug('stale_user_list===='+stale_user_list.size());
    }
    
    global void finish(Database.BatchableContext BC)
    {
        System.debug('stale user size=='+stale_user_list.size());
        Database.executeBatch(new NonTsDealsIdentifierBatch(stale_user_list), 100);
    }
}