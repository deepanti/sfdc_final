/**
 * @group ProjectBillingMilestones.
 * @group-content ../../ApexDocContent/ProjectBillingMilestones.htm.
 *
 * @description Apex Service for project Billing milestone module,
 * has aura enabled method to fetch, update/create and delete project billing milestone data.
 */
public class GPCmpServiceProjectBillingMilestones {

    /**
     * @description Returns Project billing milestone data for a given project Id.
     * @param projectId Salesforce 18/15 digit Id of project.
     * 
     * @return GPAuraResponse json of Project billing milestone data.
     * 
     * @example
     * GPCmpServiceProjectBillingMilestones.getBillingMilestone(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getBillingMilestone(Id projectId) {
        GPControllerProjectBillingMilestones billingMilestoneController = new GPControllerProjectBillingMilestones(projectId);
        return billingMilestoneController.getBillingMilestone();
    }

    /**
     * @description Saves Billing Milestone data.
     * @param serializedBillingMilestones serialized list of project billing milestone records to be inserted/updated.
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully inserted.
     * 
     * @example
     * GPCmpServiceProjectBillingMilestones.saveBillingMilestones(<Serialized list of project billing milestone record>);
     */
    @AuraEnabled
    public static GPAuraResponse saveBillingMilestones(String serializedBillingMilestones) {
        List < GP_Billing_Milestone__c > listOfBillingMilestone = (List < GP_Billing_Milestone__c > ) JSON.deserialize(serializedBillingMilestones, List < GP_Billing_Milestone__c > .class);
        GPControllerProjectBillingMilestones billingMilestoneController = new GPControllerProjectBillingMilestones(listOfBillingMilestone);
        return billingMilestoneController.upsertBillingMilestone();
    }

    /**
     * @description delete Billing milestone data.
     * @param billingMilestoneId serialized list of project billing milestone record to be deleted.
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully deleted.
     * 
     * @example
     * GPCmpServiceProjectLeadership.deleteBillingMilestone(<18 or 15 digit project billing milestone Id>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteBillingMilestone(Id billingMilestoneId) {
        GPControllerProjectBillingMilestones billingMilestoneController = new GPControllerProjectBillingMilestones();
        return billingMilestoneController.deleteBillingMilestone(billingMilestoneId);
    }

    /**
     * @description delete Billing milestone data in Bulk.
     * @param List<Id> billingMilestoneIds.
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully deleted.
     * 
     * @example
     * GPCmpServiceProjectLeadership.deleteBillingMilestoneBulk(List<Id> billingMilestoneIds);
     */ 
    @AuraEnabled
    public static GPAuraResponse deleteBillingMilestoneBulk(List < Id > billingMilestoneIds) {
        GPControllerProjectBillingMilestones billingMilestoneController = new GPControllerProjectBillingMilestones();
        return billingMilestoneController.deleteBillingMilestoneBulkService(billingMilestoneIds);
    }
}