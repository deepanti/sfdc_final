@isTest 
private class Test_AccountCreationRequest {
    static testMethod void validateACRInsert() {
        // Assign Values on OppReg Test Record
        Opportunity_Registration__c oppReg          = new Opportunity_Registration__c();
        oppReg.If_No_Account_Match_Found__c         = 'XYZ Account';
        oppReg.Primary_Contact_Text__c              = 'John Smith';
        oppReg.Department__c                        = 'Healthcare';
        oppReg.Deal_Region__c                       = 'North America';
        oppReg.Deal_Country__c                      = 'United States';
        oppReg.Status__c                            = 'In Process';
        oppReg.Deal_Pursuit_Expectation__c          = 'Joint deal pursuit';
        oppReg.Project_Delivery_Expectation__c      = 'Genpact is acting as Prime';
        oppReg.Opportunity_Registration_Description__c = 'Healthcare OppReg';
        oppReg.Deal_Pursuit_Activities__c           = 'Provide new sales lead to Genpact';
        oppReg.Other_Comments__c                    = 'No Other Comments';
        // Insert OppReg Record
        insert oppReg;
        
        oppReg.GA_Contact__c                        = UserInfo.getUserId();
        update oppReg;
        
        // Insert Business Segment Record
        Business_Segments__c busSeg                 = new Business_Segments__c();
        busSeg.Name                                 = 'New Test';
        busSeg.For__c                               = 'GE';
        insert busSeg;
        
        // Insert Sub Business Group Record
        Sub_Business__c subBus                      = new Sub_Business__c();
        subBus.Business_Segment__c                  = busSeg.Id;
        subBus.Name                                 = 'Sub Business Name';
        insert subBus;

        // Insert Archetype Record
        Archetype__c arch                           = new Archetype__c();
        arch.Name                                   = 'Archetype';
        insert arch;
        
        // Insert Sales Unit Record
        Sales_Unit__c su                            = new Sales_Unit__c();
        su.Name                                     = 'New Sales Unit';
        su.Sales_Leader__c                          = UserInfo.getUserId();
        su.Sales_Unit_Group__c                      = 'Sales Unit Head';
        insert su;
        
        // Assign Values on ACR Test Record
        Account_Creation_Request__c acr             = new Account_Creation_Request__c();
        acr.Account_Creation_Status__c              = 'Approved By SL';
        acr.Opportunity_Registration__c             = oppReg.Id;
        acr.Account_Headquarter_Country__c          = 'United States';
        acr.Account_Owner__c                        = UserInfo.getUserId();
        acr.Name                                    = 'New ACR Account';                    
        acr.Business_Group__c                       = 'GE';
        acr.Business_Segment__c                     = busSeg.Id;
        acr.Sub_Business_Group__c                   = subBus.Id;
        acr.Indutsry_Vertical__c                    = 'CPG';
        acr.Sub_Industry_Vertical__c                = 'CPG';
        acr.Hunting_Mining__c                       = 'Hunting';
        acr.Website__c                              = 'https://www.newacraccount.com';
        acr.Account_Region__c                       = 'North America';
        acr.Archetype__c                            = arch.Id;
        acr.Named_Account__c                        = 'No';
        acr.Account_Trigger__c                      = 'No';
        acr.User_sales_leader__c                    = UserInfo.getUserId();
        acr.Sales_Unit__c                           = su.Id;
        acr.Sales_Sub_Unit_Analytics__c             = null;
        acr.QSRM_Approver__c                        = null;
        acr.Sales_Sub_Unit_IT__c                    = null;
        acr.Sales_Sub_Unit_Vertical__c              = null;
        acr.Enterprise_GRm__c                       = null;
        acr.Client_Partner__c                       = null;
        acr.GA_Manager_from_Opp_Registration__c     = UserInfo.getUserId();
        acr.Revenue__c                              = 10;
        acr.PTAM__c                                 = 30;
        acr.TAM__c                                  = 20;
        acr.CMIT_BU__c                              = 'GE MFG';
        acr.CMIT_Sub_BU__c                          = 'GE-Energy';
        acr.Business_Description__c                 = 'New ACR Description';
        // Insert ACR
        insert acr;    
        
    }
}