/**
 *
 * @group Initiate Job
 * @group-content ../../ApexDocContent/InitiateJob.html
 *
 * @description Apex Controller for Initiate Job
 */
public class GPControllerInitiateJobProcess {

    private final String JOB_QUEUED_LABEL = 'Your job has been queued. Please check your email for further information.';
    private final String NO_CHILD_RECORD_ERROR = 'Please upload the Child record of associated Job Type!';
    private final String UPLOAD_PROJECT_LABEL = 'Update Project';
    private final String UPLOAD_PROJECT_LEADERSHIP_LABEL = 'Update Project Leadership';
    private final String UPLOAD_RESOURCE_ALLOCATION_LABEL = 'Upload Resource Allocations';
    private final String UPLOAD_PROJECT_WORK_LOCATION_AND_SDOs_LABEL = 'Update Project Work Location & SDOs';
    private final String UPLOAD_BILLING_MILESTONES_LABEL = 'Upload Billing Milestones';
    private final String UPLOAD_TIME_SHEET_ENTRIES_LABEL = 'Upload Time Sheet Entries';
    private final String RESPONSE_OBJECT = 'responseCodeWithMessage';
    private final String JOB_IN_QUEUE_ERROR = 'Your Job is in ';
    private final String RESPONSE_STRING = 'responseString';
    private final String SUCCESS_LABEL = 'SUCCESS';
    
    
    private Map<String,List<GP_Temporary_Data__c>> mapOfTemporaryData = new Map<String,List<GP_Temporary_Data__c>>();
    private List<GP_Temporary_Data__c> lstOfTemporaryDataForInvalidOraclePIDs = new List<GP_Temporary_Data__c>();
    private GPResponseWrapper responseCodeWithMessage = new GPResponseWrapper();
    private Set < String > setOfProjectOracleIds = new Set < String > ();
    private Boolean childRecordExist = false;
    

    private Boolean isAnyProcessInvoked;
    private String responseString;
    private JSONGenerator gen;
    private String jobStatus;
    private String jobType;
    private Id jobId;

    public GPControllerInitiateJobProcess() {}

    /**
     * @description Parameterized constructor to accept Job Id.
     * @param jobId SFDC 18/15 digit job Id
     * 
     * @example
     * new GPControllerInitiateJobProcess(<SFDC 18/15 digit Job Id>);
     */
    public GPControllerInitiateJobProcess(Id jobId) {
        this.jobId = jobId;
    }

    /**
     * @description method to return job status.
     * 
     * @example
     * new GPControllerInitiateJobProcess(<SFDC 18/15 digit JOb Id>).getQueuedJobResponse();
     */
    public GPAuraResponse getQueuedJobResponse() {
        try {
            setJobTypeAndStatus();
            setChildRecordExist();

            if (!childRecordExist) {
                responseString = NO_CHILD_RECORD_ERROR;
                responseCodeWithMessage.code = 0;
                responseCodeWithMessage.message = NO_CHILD_RECORD_ERROR;
            } else if (jobStatus != null) {
                responseString = JOB_IN_QUEUE_ERROR + jobStatus + ' state !';
                responseCodeWithMessage.code = jobStatus == 'Completed' ? 1 : 0;
                responseCodeWithMessage.message = JOB_IN_QUEUE_ERROR + jobStatus + ' state !';
            } else if (jobType == UPLOAD_TIME_SHEET_ENTRIES_LABEL)
                invokeTimeSheetBatch();
            else if (jobType == UPLOAD_PROJECT_LABEL)
                invokeProjectUpdateBatch();
            else if (jobType == UPLOAD_PROJECT_LEADERSHIP_LABEL)
                invokeProjectLeaderShipUpdateBatch();
            else if (jobType == UPLOAD_PROJECT_WORK_LOCATION_AND_SDOs_LABEL)
                invokeProjectWorkLocationUpdateBatch();

            setJSON();
            return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
        } catch (Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
    }

    /**
     * @description Check if child record exist for the job.
     * 
     */
    private void setChildRecordExist() {
        childRecordExist = ([SELECT Id, Name FROM GP_Temporary_Data__c
            where GP_Job_Id__c =: jobId
        ]).size() > 0 ? true : false;
    }

    /**
     * @description Set job Type and Status.
     * 
     */
    private void setJobTypeAndStatus() {

        GP_Job__c objJob = [SELECT Id, Name, GP_Status__c, GP_Job_Type__c FROM GP_Job__c
            where Id =: jobId
        ];

        jobType = objJob.GP_Job_Type__c;
        jobStatus = objJob.GP_Status__c;

    }

    /**
     * @description Create Project Oracle PID Set.
     * 
     */
    private void getSetOfProjectIds() {
        for (GP_Temporary_Data__c objTempData: [select GP_Project__c,GP_Is_Failed__c,GP_Validation_Result__c from GP_Temporary_Data__c where GP_Job_Id__c =: jobId]) {
            setOfProjectOracleIds.add(objTempData.GP_Project__c);
            if(!mapOfTemporaryData.containsKey(objTempData.GP_Project__c))
                mapOfTemporaryData.put(objTempData.GP_Project__c,new List<GP_Temporary_Data__c>());
            mapOfTemporaryData.get(objTempData.GP_Project__c).add(objTempData);
        }

    }

    /**
     * @description Invoke Time Sheet Batch.
     * 
     */
    private void invokeTimeSheetBatch() {
        responseCodeWithMessage.message = JOB_QUEUED_LABEL;
        responseString = JOB_QUEUED_LABEL;
        responseCodeWithMessage.code = 1;

        Database.executeBatch(new GPBatchProcessTimesheetUpload(jobId), 200);
    }

    /**
     * @description Invoke Project Update Batch.
     * 
     */
    private void invokeProjectUpdateBatch() {
        getSetOfProjectIds();
        
        responseCodeWithMessage.message = JOB_QUEUED_LABEL;
        responseString = JOB_QUEUED_LABEL;
        responseCodeWithMessage.code = 1;
        
        if(lstOfTemporaryDataForInvalidOraclePIDs.size() > 0)
            update lstOfTemporaryDataForInvalidOraclePIDs;

        Database.executeBatch(new GPBatchProcessProjectUpdate(jobId), 1);
    }

    /**
     * @description Invoke Project Leadership Update Batch.
     * 
     */
    private void invokeProjectLeaderShipUpdateBatch() {
        getSetOfProjectIds();

        responseCodeWithMessage.message = JOB_QUEUED_LABEL;
        responseString = JOB_QUEUED_LABEL;
        responseCodeWithMessage.code = 1;
        filterInvalidOraclePIDs();
        
        if(lstOfTemporaryDataForInvalidOraclePIDs.size() > 0)
            update lstOfTemporaryDataForInvalidOraclePIDs;
            
        Database.executeBatch(new GPBatchProcessProjectLeadershipUpdate(setOfProjectOracleIds, jobId), 1);
    }

    /**
     * @description Invoke Project Workloaction Update Batch.
     * 
     */
    private void invokeProjectWorkLocationUpdateBatch() {
        getSetOfProjectIds();

        responseCodeWithMessage.message = JOB_QUEUED_LABEL;
        responseString = JOB_QUEUED_LABEL;
        responseCodeWithMessage.code = 1;
        
        if(lstOfTemporaryDataForInvalidOraclePIDs.size() > 0)
            update lstOfTemporaryDataForInvalidOraclePIDs;

        Database.executeBatch(new GPBatchProcessProjectWorkLocationUpdate(setOfProjectOracleIds, jobId), 1);
    }

    /**
     * @description Set GPAuraResponse object.
     * 
     */
    private void setJson() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if (responseCodeWithMessage != null)
            gen.writeObjectField(RESPONSE_OBJECT, responseCodeWithMessage);
        gen.writeEndObject();
    }
    
    private void filterInvalidOraclePIDs(){
        Map<String,GP_Project__c> mapOfProject = new Map<String,GP_Project__c>();
        for(GP_Project__c project : [Select GP_Oracle_PID__c from GP_Project__c
                                        where GP_Oracle_PID__c IN: setOfProjectOracleIds 
                                         and GP_Approval_Status__c = 'Approved' and GP_Oracle_Status__c = 'S' 
                                     ORDER BY LastModifiedDate Desc])
        {
                       mapOfProject.put(project.GP_Oracle_PID__c,project);                  
                                     }
       
        system.debug('mapOfProject'+mapOfProject);
        for(String oraclePID : setOfProjectOracleIds){
            if(!mapOfProject.containsKey(oraclePID)){
                if(mapOfTemporaryData.containsKey(oraclePID)){
                    mapOfTemporaryData.get(oraclePID)[0].GP_Is_Failed__c = true;
                    mapOfTemporaryData.get(oraclePID)[0].GP_Validation_Result__c = 'Invalid Oracle Id';
                    lstOfTemporaryDataForInvalidOraclePIDs.add(mapOfTemporaryData.get(oraclePID)[0]);
                    setOfProjectOracleIds.remove(oraclePID);
                }
            }
        }
    }
    /*private void invokeResourceAllocationFlow() {
        responseString = JOB_QUEUED_LABEL;
        /* Map<String, Object> jobIdMap = new Map<String, Object>();
jobIdMap.put('JobId', jobId);
Flow.Interview. resourceAllocationFlow = new Flow.Interview.doubler(jobIdMap);
resourceAllocationFlow.start(); 
    }
    private void invokeBillingMileStoneFlow() {
        responseString = JOB_QUEUED_LABEL;
        Map<String, Object> jobIdMap = new Map<String, Object>();
        jobIdMap.put('strJobId', jobId);
        Flow.Interview.Copy_Upload_Billing_Milestones_to_Billing_Milestones resourceAllocationFlow = new Flow.Interview.Copy_Upload_Billing_Milestones_to_Billing_Milestones(jobIdMap);
        resourceAllocationFlow.start(); 
    }
    private Boolean uploadTimeSheetRecordsExist() {
        return ([SELECT Id, Name  FROM GP_Upload_Timesheet_Entry__c
                 where GP_Job_Id__c  = :jobId]).size()>0 ? true :false;
    }
    private Boolean uploadResourceAllocationRecordsExist() {
        return false;
        /*return ([SELECT Id, Name  FROM GP_Job__c
where GP_Job_Id__c  = :jobId]).size()>0 ? true :false;
    }
    private Boolean uploadBillingMileStoneRecordsExist() {
        return([SELECT Id, Name FROM GP_Upload_Billing_Milestone__c
                where GP_Job__c   = :jobId]).size()>0 ? true :false;
    }*/

}