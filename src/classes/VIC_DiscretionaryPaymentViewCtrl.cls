public class VIC_DiscretionaryPaymentViewCtrl {
    
    @AuraEnabled
    public static List<Target_Achievement__c> getIncentiveDetails(String strUserId, String strScreenName){
        List<Target_Achievement__c> returnList = new List<Target_Achievement__c>();
        System.debug('test');
        VIC_Process_Information__c vicInfo = VIC_Process_Information__c.getInstance();
        Integer processingYear = Integer.ValueOf(vicInfo.VIC_Process_Year__c);
        Date targetStartDate = DATE.newInstance(processingYear,1,1);
        Date targetEndDate = DATE.newInstance(processingYear, 12, 31);
        
        String strIncentiveStatus;
        If(strScreenName.containsIgnoreCase('HRApproval')){
            strIncentiveStatus = 'HR - Pending';
        }else If(strScreenName.containsIgnoreCase('Open Cases')){
            strIncentiveStatus = 'HR - OnHold';
        }        
        
        List<Target_Component__c> targetComponentList = [Select id, vic_Achievement__c,vic_Achievement_Percent__c,vic_Already_Paid_Incentive__c,Bonus_Display__c,Bonus_Type__c,
                                                         (Select Id, Bonus_Amount__c, VIC_Description__c,Achievement_Date__c,vic_Incentive_In_Local_Currency__c,
                                                          Opportunity__c,Opportunity__r.Name,vic_Opportunity_Product_Id__c
                                                          From Target_Achievements__r 
                                                          Where vic_Status__c =:strIncentiveStatus)
                                                         From Target_Component__c 
                                                         Where Target__r.User__c =: strUserId
                                                         AND Target__r.Is_Active__c = True 
                                                         AND Target__r.Start_date__c >:targetStartDate AND Target__r.Start_date__c <: targetEndDate                                                      
                                                         AND Master_Plan_Component__r.vic_Component_Code__c ='Discretionary_Payment'
                                                         Limit 1];
        if(targetComponentList != null && targetComponentList.size() > 0){
            for(Target_Component__c t : targetComponentList){
                if(t.Target_Achievements__r != null){
                    returnList.addAll(t.Target_Achievements__r);
                }
                break;
            }
        }
        return returnList;
    }
    
    
}