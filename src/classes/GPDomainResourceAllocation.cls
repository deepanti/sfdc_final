public class GPDomainResourceAllocation extends fflib_SObjectDomain {

    public GPDomainResourceAllocation(List < GP_Resource_Allocation__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainResourceAllocation(sObjectList);
        }
    }

    // SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Project__c.SObjectType,
            GP_Resource_Allocation__c.SObjectType
    };

    public override void onBeforeInsert() {
 
        setDefaultonBeforeInsertUpdate();
        validateResourceEffortToProjectBudget();
		setIsUpdateOnParentFailure();
        //setIsUpdatedField();
        //setOracleStatus();
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        // Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, null);
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //setOracleStatusField(oldSObjectMap); merged with setIsUpateCheckbox method
        setIsUpdateCheckBox();
        setDefaultonBeforeInsertUpdate();
        setIsUpdateOnParentFailure();
        // setOracleStatus();
        validateResourceEffortToProjectBudget();
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //Pankaj 2/4/2018
        //updatePIDApproverFinance(uow, oldSObjectMap);
        //uow.commitWork();
    /*
            Author :Anoop Verma
            Date: 22 June 2020
            CR:CRQ000000076988
            Description: When all the updated resource allocations get response from Oracle, 
                        only then send the email notification to respective audience. 
     
     */
        
        try {
          sendNotificationToPIDAudienceOnResourceSyncWithOracle(oldSObjectMap);
        }
        catch(Exception e){
             
        }
    }
    /* public override void onValidate() {
// Validate GP_Resource_Allocation__c 
//for(GP_Resource_Allocation__c  ObjProject : (List<GP_Resource_Allocation__c >) Records) {
//  }
}
*/
    public override void onApplyDefaults() {
        //validateResourceEffortToProjectBudget();
    }
    /*public void setIsUpdatedField() {
for(GP_Resource_Allocation__c  objResourceAllocation : (List<GP_Resource_Allocation__c >)records){
if(objResourceAllocation.GP_Parent_Resource_Allocation__c != null)
objResourceAllocation.GP_IsUpdated__c = false;
else
objResourceAllocation.GP_IsUpdated__c = true;
}
}*/

    /*
    @Author: Amit Prajapati
    @Description: Sets the Default values in fields on before insert and before update
    */
    private void setDefaultonBeforeInsertUpdate() {
        for (GP_Resource_Allocation__c objRA: (List < GP_Resource_Allocation__c > ) records) {
            objRA.GP_Project_and_Work_Location_ID__c = objRA.GP_Project__c + '@@' + objRA.GP_Work_Location__c;
            if (objRA.GP_Project_Record_Type__c == 'Indirect PID') {
                objRA.GP_Work_Type_Id__c = '10020';
                objRA.GP_Expenditure_Type__c = 'Non-Billable';
            }
            if (objRA.GP_IsUpdated__c && String.isNotBlank( objRA.GP_Oracle_Id__c) ) {
                objRA.GP_Operation_Mode__c = 'UPDATE';
            }
            if(objRA.GP_Project_Record_Type__c != 'Indirect PID' && objRA.GP_Shadow_type__c != null){
                objRA.GP_Expenditure_Type__c = 'Non-Billable';
            }
        }
    }

    //Commented on 10-May-2018 by Mandeep Chauhan As Discussed with the Team As this is not used anywhere
    /*public void setOracleStatusField(Map < Id, SOBject > oldSObjectMap) {
        Set < String > fieldSetForFieldTracking = new Set < String > ();
        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Resource_Allocation__c.fieldSets.getMap().get('GP_Track_Changes_With_Approved_Record').getFields()) {
            fieldSetForFieldTracking.add(fields.getFieldPath());
        }
        Set < Id > setOfOldResourceAllocationRecords = new Set < Id > ();

        for (GP_Resource_Allocation__c objResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
            if (objResourceAllocation.GP_Parent_Resource_Allocation__c != null)
                setOfOldResourceAllocationRecords.add(objResourceAllocation.GP_Parent_Resource_Allocation__c);
        }
        if (setOfOldResourceAllocationRecords.size() > 0) {
            Map < Id, GP_Resource_Allocation__c > mapOfOldResourceAllocations = new GPSelectorResourceAllocation().selectResourceAllocationRecords(setOfOldResourceAllocationRecords);
            for (GP_Resource_Allocation__c objResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
                if (objResourceAllocation.GP_Parent_Resource_Allocation__c != null) {
                    GP_Resource_Allocation__c oldResourceAllocationRecord = mapOfOldResourceAllocations.get(objResourceAllocation.GP_Parent_Resource_Allocation__c);
                    for (string eachField: fieldSetForFieldTracking) {
                        if (objResourceAllocation.get(eachField) != oldResourceAllocationRecord.get(eachField)) {
                            objResourceAllocation.GP_IsUpdated__c = true;
                            break;
                        }
                    }
                }
            }
        }

    }*/
    /* public void updatePIDApproverFinance(fflib_SObjectUnitOfWork uow, Map<Id, SOBject> oldSObjectMap) {

Set<String> fieldSet = new Set<String>();
GP_Resource_Allocation__c oldProjBudget;
Map<ID,GP_Project__c > mapofIdToobjProject = new Map<Id,GP_Project__c>();

//dynamically get the fields from the field set and then use the same for comparison in the trigger. 
for(Schema.FieldSetMember fields :Schema.SObjectType.GP_Resource_Allocation__c.fieldSets.getMap().get('GP_PID_Approver_Finance').getFields()){
fieldSet.add(fields.getFieldPath());
}
if(fieldSet !=null && fieldSet.size()>0)
{
for(GP_Resource_Allocation__c  objPB : (List<GP_Resource_Allocation__c >)records){ 

mapofIdToobjProject.put(objPB.GP_Project__c, new GP_Project__c(id=objPB.GP_Project__c, GP_PID_Approver_Finance__c = false));

if(mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c != true){
for(string eachField: fieldSet){
if(oldSObjectMap != null)
{
oldProjBudget = (GP_Resource_Allocation__c)oldSObjectMap.get(objPB.Id);
if(objPB.get(eachField) != oldProjBudget.get(eachField)){
mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true;
}   
}
else
{
mapofIdToobjProject.get(objPB.GP_Project__c).GP_PID_Approver_Finance__c = true; 
}
}  
}
}
}

if(mapofIdToobjProject !=null && mapofIdToobjProject.size()>0)
{
list<GP_Project__c> lstProjecttoUpdate = new list<GP_Project__c>();
for(GP_Project__c objPrj : mapofIdToobjProject.values())
{
if(objPrj.GP_PID_Approver_Finance__c == true){
lstProjecttoUpdate.add(objPrj);
}
}
if(lstProjecttoUpdate !=null && lstProjecttoUpdate.size()>0)
{
uow.registerDirty(lstProjecttoUpdate);
}
}   
} */
    /*private Boolean checkForProject(map<Id,GP_Project__c> mapOfProject,GP_Resource_Allocation__c objResourceAllocation){
return mapOfProject.containsKey(objResourceAllocation.GP_Project__c) && 
((mapOfProject.get(objResourceAllocation.GP_Project__c).RecordType.Name == 'CMITS' && 
(mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Project_type__c == 'Fixed Price' || 
mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Project_type__c == 'Fixed monthly')) 
|| (mapOfProject.get(objResourceAllocation.GP_Project__c).RecordType.Name == 'OTHER PBB' 
&& mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Project_type__c == 'Fixed Price'));
}*/
	
    private Boolean checkForResource(GP_Resource_Allocation__c objResourceAllocation) {
        return ((objResourceAllocation.GP_Project_Record_Type__c == 'CMITS' && !objResourceAllocation.GP_No_Pricing_in_OMS__c &&
                (objResourceAllocation.GP_Project_type__c == 'Fixed Price' ||
                    objResourceAllocation.GP_Project_type__c == 'Fixed monthly' ||
                	objResourceAllocation.GP_Project_type__c == 'Gainshare')) ||
             (objResourceAllocation.GP_Project_Record_Type__c == 'CMITS' && objResourceAllocation.GP_No_Pricing_in_OMS__c &&
                (objResourceAllocation.GP_Project_type__c == 'Fixed Price')) ||
            (objResourceAllocation.GP_Project_Record_Type__c == 'OTHER PBB' &&
                objResourceAllocation.GP_Project_type__c == 'Fixed Price'));
    }
    
    private void updateResourceTCV(Date endDate,GP_Resource_Allocation__c objResourceAllocation){
        Integer noOfWorkingDays = GPCommon.getWorkingDays(objResourceAllocation.GP_Start_Date__c,endDate);

        objResourceAllocation.GP_Resource_TCV__c = (noOfWorkingDays *  8 * objResourceAllocation.GP_Percentage_allocation__c * (objResourceAllocation.GP_Bill_Rate__c== null?0: objResourceAllocation.GP_Bill_Rate__c)) / (100 * objResourceAllocation.GP_Project_Bill_Rate_Type__c);
    }

    private void validateResourceEffortToProjectBudget() {
        Integer minPerDayHour = Integer.ValueOf(System.Label.GP_Min_Per_Day_Hour);
        Decimal globalDeviationPercent = Decimal.ValueOf(System.Label.GP_Global_Deviation_Percent);
        Set < Id > setOfProjectIds = new Set < Id > ();
        Set < Id > setOfEmployeeIds = new Set < Id > ();
        Set <Id> setOfResourceIdsToBeChanged = new Set<Id>();
        String PIDValue,errorValue;

        if (records != null && records.size() > 0) {
            for (GP_Resource_Allocation__c objResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
                if (checkForResource(objResourceAllocation)){
                    setOfProjectIds.add(objResourceAllocation.GP_Project__c);
                    setOfEmployeeIds.add(objResourceAllocation.GP_Employee__c);
                    if(objResourceAllocation.Id != null)
                        setOfResourceIdsToBeChanged.add(objResourceAllocation.Id);
                }
            }
            if (setOfProjectIds.size() > 0) {
                map < Id, GP_Project__c > mapOfProject = new GPSelectorProject().getProjectMap(setOfProjectIds);
                map < Id, Decimal > mapOfProjectWiseEffortHours = new map < Id, Decimal > ();

                for(GP_Resource_Allocation__c objResourceAllocation : [select id,GP_Parent_Project_SFDC_Id__c, GP_Start_Date__c,GP_No_Pricing_in_OMS__c, GP_End_Date__c,GP_Project__c,GP_Project__r.GP_Parent_Project__c, GP_Project_type__c, GP_Project_Record_Type__c, GP_Percentage_allocation__c,
                                        GP_Working_Hours_Per_Day__c,GP_Employee__c, GP_Employee__r.GP_Actual_Termination_Date__c from GP_Resource_Allocation__c where GP_Project__c in :setOfProjectIds
                                                                      and Id not in :setOfResourceIdsToBeChanged]){
                    if (checkForResource(objResourceAllocation) && objResourceAllocation.GP_Start_Date__c != null &&
                        objResourceAllocation.GP_End_Date__c != null &&
                        objResourceAllocation.GP_Percentage_allocation__c != null) {
                        Date endDate = objResourceAllocation.GP_Employee__r.GP_Actual_Termination_Date__c < objResourceAllocation.GP_End_Date__c ? objResourceAllocation.GP_Employee__r.GP_Actual_Termination_Date__c : objResourceAllocation.GP_End_Date__c;
                        Integer noOfDays = objResourceAllocation.GP_Start_Date__c.daysBetween(endDate) + 1;
                        Decimal totalCost = noOfDays * Decimal.ValueOf(objResourceAllocation.GP_Working_Hours_Per_Day__c) * (objResourceAllocation.GP_Percentage_allocation__c / 100);
                        /*Integer noOfWorkingDays = GPCommon.getWorkingDays(objResourceAllocation.GP_Start_Date__c,objResourceAllocation.GP_End_Date__c );
                        Decimal effortHours = noOfWorkingDays * minPerDayHour * objResourceAllocation.GP_Percentage_allocation__c;
                        Decimal totalCost = effortHours * objResourceAllocation.GP_Bill_Rate__c;*/
                        if (mapOfProjectWiseEffortHours.containsKey(objResourceAllocation.GP_Project__c))
                            mapOfProjectWiseEffortHours.put(objResourceAllocation.GP_Project__c, totalCost + mapOfProjectWiseEffortHours.get(objResourceAllocation.GP_Project__c));
                        else
                            mapOfProjectWiseEffortHours.put(objResourceAllocation.GP_Project__c, totalCost);
                    }
                }
                map<Id,GP_Employee_Master__c> mapOfEmployee = new map<Id,GP_Employee_Master__c>([select id,GP_Actual_Termination_Date__c from GP_Employee_Master__c where id in :setOfEmployeeIds]);

                for (GP_Resource_Allocation__c objResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
                    /*if(checkForProject(mapOfProject,objResourceAllocation) && objResourceAllocation.GP_Start_Date__c !=null && 
                    objResourceAllocation.GP_End_Date__c !=null 
                    && objResourceAllocation.GP_Percentage_allocation__c!=null && objResourceAllocation.GP_Bill_Rate__c!=null)*/
                    if (checkForResource(objResourceAllocation) && objResourceAllocation.GP_Start_Date__c != null &&
                        objResourceAllocation.GP_End_Date__c != null &&
                        objResourceAllocation.GP_Percentage_allocation__c != null) {
                        Date endDate = mapOfEmployee.get(objResourceAllocation.GP_Employee__c).GP_Actual_Termination_Date__c < objResourceAllocation.GP_End_Date__c ? mapOfEmployee.get(objResourceAllocation.GP_Employee__c).GP_Actual_Termination_Date__c : objResourceAllocation.GP_End_Date__c;
                        Integer noOfDays = objResourceAllocation.GP_Start_Date__c.daysBetween(endDate) + 1;
                            system.debug('noOfDays'+noOfDays);
                        Decimal totalCost = noOfDays * Decimal.ValueOf(objResourceAllocation.GP_Working_Hours_Per_Day__c) * (objResourceAllocation.GP_Percentage_allocation__c/100);
                        /*Integer noOfWorkingDays = GPCommon.getWorkingDays(objResourceAllocation.GP_Start_Date__c,objResourceAllocation.GP_End_Date__c );
                        Decimal effortHours = noOfWorkingDays * minPerDayHour * objResourceAllocation.GP_Percentage_allocation__c;
                        Decimal totalCost = effortHours * objResourceAllocation.GP_Bill_Rate__c;*/

                        updateResourceTCV(endDate,objResourceAllocation);

                        if (mapOfProjectWiseEffortHours.containsKey(objResourceAllocation.GP_Project__c))
                            mapOfProjectWiseEffortHours.put(objResourceAllocation.GP_Project__c, totalCost + mapOfProjectWiseEffortHours.get(objResourceAllocation.GP_Project__c));
                        else
                            mapOfProjectWiseEffortHours.put(objResourceAllocation.GP_Project__c, totalCost);
                    } 
                }
                if (mapOfProjectWiseEffortHours.size() > 0) {
                    //map<Id,GP_Project__c> mapOfProject = new GPSelectorProject().getProjectMap(mapOfProjectWiseEffortHours.KeySet());
                    for (GP_Resource_Allocation__c objResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
                        //Decimal projectBudgetAmount = globalDeviationPercent * mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Total_TCV__c;
                        // 03-04-19:DS: If record updated by Pinnacle User, bypass the total effort hours check.
                        if (isEffortHoursComparisonRequired()
                            && mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Total_Effort_Hours__c < mapOfProjectWiseEffortHours.get(objResourceAllocation.GP_Project__c))
                        {
                            //system.debug('GPCheckRecursive.mapProjectSkipBudgetCheck'+GPCheckRecursive.mapProjectSkipBudgetCheck);
                            //system.debug('objResourceAllocation.GP_Project__c::'+objResourceAllocation.GP_Parent_Project_SFDC_Id__c);
                            //system.debug('objResourceAllocation.GP_Project__c::'+GPCheckRecursive.mapProjectSkipBudgetCheck.get(objResourceAllocation.GP_Parent_Project_SFDC_Id__c));
                            if(objResourceAllocation.GP_Parent_Project_SFDC_Id__c != null && 
                               GPCheckRecursive.mapProjectSkipBudgetCheck != null && GPCheckRecursive.mapProjectSkipBudgetCheck.containsKey(objResourceAllocation.GP_Parent_Project_SFDC_Id__c)
                               && GPCheckRecursive.mapProjectSkipBudgetCheck.get(objResourceAllocation.GP_Parent_Project_SFDC_Id__c))
                            {
                                continue;
                            }
                            PIDValue = mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Oracle_PID__c != null ? mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Oracle_PID__c : 'NA';
                            errorValue = 'Actual Hours (' + mapOfProjectWiseEffortHours.get(objResourceAllocation.GP_Project__c) + ') are greater than budgeted hours(' + mapOfProject.get(objResourceAllocation.GP_Project__c).GP_Total_Effort_Hours__c + '). Please update the allocations or modify the budget accordingly.(PID - ' +  PIDValue + ')';
                            objResourceAllocation.addError(errorValue);
                        }
                    }
                }
            }
        }
    }

    public void setIsUpdateCheckBox() {
        List < String > lstOfFieldAPINames = new List < String > ();
        for (Schema.FieldSetMember fields: Schema.SObjectType.GP_Resource_Allocation__c.fieldSets.getMap().get('GP_PID_Update_Fields').getFields()) {
            lstOfFieldAPINames.add(fields.getFieldPath());
        }
        Set < Id > setOfParentRecords = new Set < Id > ();
        for (GP_Resource_Allocation__c projectResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
            if (projectResourceAllocation.GP_Parent_Resource_Allocation__c != null)
                setOfParentRecords.add(projectResourceAllocation.GP_Parent_Resource_Allocation__c);
        }
        if (setOfParentRecords.size() > 0) {
            map < Id, GP_Resource_Allocation__c > mapOfParentRecords = new map < Id, GP_Resource_Allocation__c > ();
            for (GP_Resource_Allocation__c projectResourceAllocation: new GPSelectorResourceAllocation().selectProjectResourceAllocationRecords(setOfParentRecords)) {
                mapOfParentRecords.put(projectResourceAllocation.Id, projectResourceAllocation);
            }
            GPCommon.setIsUpdatedField(records, mapOfParentRecords, lstOfFieldAPINames, 'GP_Parent_Resource_Allocation__c');
        }
         
        //Commented By Anoop  CR:CRQ000000076988
       // for (GP_Resource_Allocation__c projectResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {
       //     if (projectResourceAllocation.GP_Parent_Resource_Allocation__c != null)
      //          setOfParentRecords.add(projectResourceAllocation.GP_Parent_Resource_Allocation__c);
      //  }
    }
    public void setIsUpdateOnParentFailure(){		 
	      for (GP_Resource_Allocation__c projectResourceAllocation: (List < GP_Resource_Allocation__c > ) records) {		
	          if (projectResourceAllocation.GP_Parent_Allocation_Oracle_Status__c != 'S'){		
	              projectResourceAllocation.GP_IsUpdated__c = true;
          }		
        } 
        
	}
    
    // 03-04-19:DS: Determine if logged in user is Pinnacle Integration User or not.
    private Boolean isEffortHoursComparisonRequired() {
        Boolean isRequired = true;
        if(String.isNotBlank(Label.GP_Pinnacle_Integration_User_For_Resource_Allocation) &&
           UserInfo.getUserId() == Label.GP_Pinnacle_Integration_User_For_Resource_Allocation) {
            isRequired = false;
        }
        
        return isRequired;
    }
    
     //Author: Anoop Verma
    // Date:27-Jun-2020
    // CR:CRQ000000076988
    // Description: Send notification to PID audience on resource sync with Oracle PA.
    private void sendNotificationToPIDAudienceOnResourceSyncWithOracle(Map < Id, SObject > oldSObjectMap) {

        Set<Id> setOfPIDs = new Set<Id>();
        List<Messaging.SingleEmailMessage> emailNotifications = new List<Messaging.SingleEmailMessage>();
        Map<Id, GP_Project__c> mapOfPIDOriginals = new Map<Id, GP_Project__c>();
        Map<Id, Integer> mapOfPIDWithRACounts = new Map<Id, Integer>();
        Map<Id, List<GP_Resource_Allocation__c>> mapOfPIDWithRALists = new Map<Id, List<GP_Resource_Allocation__c>>();
        Map<Id, GP_Project__c> mapOfPIDs = new Map<Id, GP_Project__c>();
        List<GP_Resource_Allocation__c> listOfResourceAs = new List<GP_Resource_Allocation__c>();
        list<GP_Project__c> lstProjecttoUpdateNotificationFlag = new list<GP_Project__c>();
        GP_Resource_Allocation__c oldResourceAllocation;
        // Get org wide email address for Pinnacle System.
        Id oweaId = GPCommon.getOrgWideEmailId();        

        if (records != null && records.size() > 0) {
            for (GP_Resource_Allocation__c objRA: (List < GP_Resource_Allocation__c > ) records) {
                if(oldSObjectMap != null) {
                    oldResourceAllocation = (GP_Resource_Allocation__c) oldSObjectMap.get(objRA.Id);
                    // Oracle Status should change
                    if(oldResourceAllocation.GP_Oracle_Status__c != objRA.GP_Oracle_Status__c && objRA.GP_Oracle_Status__c != '') {
                        setOfPIDs.add(objRA.GP_Project__c);
                    }
                }
            }
            if(setOfPIDs.size() > 0) {
                for(GP_Project__c pid : [SELECT Id, Name, GP_Oracle_PID__c, GP_EP_Project_Number__c, GP_Current_Working_User__r.Name,
                                        GP_Send_Email_Notification_Addresses__c,
                                        (Select id, GP_Project__c, GP_Employee__r.name, GP_Employee__r.GP_Final_OHR__c, GP_Start_Date__c, 
                                        GP_End_Date__c, GP_Oracle_Status__c, GP_Oracle_Process_Log__c 
                                        from GP_Resource_Allocations__r Where GP_isupdated__c = true)
                                        FROM GP_Project__c 
                                        where id =: setOfPIDs]) 
                {
                    mapOfPIDOriginals.put(pid.Id, pid);
                    // To avoid below exception: 
                    // System.QueryException: Aggregate query has too many rows for direct assignment, use FOR loop External entry point
                    for(GP_Resource_Allocation__c ra : pid.GP_Resource_Allocations__r) {
                        listOfResourceAs.add(ra);
                    }
                    mapOfPIDWithRALists.put(pid.Id, listOfResourceAs);
                }
                Integer count = 0;
                if(listOfResourceAs.size() > 0) {
                    for(GP_Resource_Allocation__c ra : listOfResourceAs) {
                        if(String.isNotBlank(ra.GP_Oracle_Status__c)) {
                            count++;                            
                        }                        
                        if(count == listOfResourceAs.size() && mapOfPIDOriginals.containsKey(ra.GP_Project__c)) {
                            mapOfPIDs.put(ra.GP_Project__c, mapOfPIDOriginals.get(ra.GP_Project__c));
                            mapOfPIDWithRACounts.put(ra.GP_Project__c, count);
                        }
                    }
                }
                if(mapOfPIDs.keySet().size() > 0) {
                    for(Id pid : mapOfPIDs.keySet()) {

                        GP_Project__c pidObj = mapOfPIDs.get(pid);
                         pidObj.GP_is_Send_Notification__c=false;
                         lstProjecttoUpdateNotificationFlag.add(pidObj);

                        String htmlBody = '';
                        if(mapOfPIDWithRACounts.containsKey(pid) && mapOfPIDWithRACounts.get(pid) > 0) {
                            
                             htmlBody = '<html> <head><style>td {padding-left:5px; text-align:left; border-collapse: collapse;border:solid 1px;}th{text-align:left;padding-left:5px;color:white;background-color: #005595;border-collapse: collapse;border:solid 1px;} </style></head>'
                                + '<tr><td bgcolor="#005595" width="100%" height="30px" color="white" align="left">'
                                + '<b style="color:white;margin-left:10px;">'
                                + '<H4>SFDC-PINNACLE NOTIFICATION!</H4></b>'
                                + '</td></tr><tr><td><div style="color:#333399;margin-left:10px;margin-top:10px">'
                                + 'Dear '+pidObj.GP_Current_Working_User__r.Name+',</div> '
                                + '<div style="color:#333399;margin-left:10px;margin-top:20px">Find below Status of Resource Allocations Processed in Oracle PA on PID '+ pidObj.GP_Oracle_PID__c +'.For any errors in allocations, do correct the errors and resubmit the PID in Pinnacle SFDC. Until error is not solved, resource will not be allocated on PID in Oracle and will not be able to fill timesheet. <br/> <br/>'
                                + '<table  style="padding-left:10 px;border-collapse: collapse;border:solid 1px; width:100%">'
                                + '<tr><th>Employee Name</th>'
                                + '<th>OHRID</th><th>Oracle Status</th>'
                                + '<th>Oracle Message</th><th> Allocation Start Date</th>'
                                + '<th>Allocation End Date</th></tr>';
                            
                            if(mapOfPIDWithRALists.containsKey(pid)) {
                                for(GP_Resource_Allocation__c ra : mapOfPIDWithRALists.get(pid)) {
                                    htmlBody += '</td><td>' + (String.isNotBlank(ra.GP_Employee__r.name) ? ra.GP_Employee__r.name : '')
                                        + '</td><td>'+ (String.isNotBlank(ra.GP_Employee__r.GP_Final_OHR__c) ? ra.GP_Employee__r.GP_Final_OHR__c : '')
                                        +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Status__c) ? ra.GP_Oracle_Status__c : '')
                                        +'</td><td>'+ (String.isNotBlank(ra.GP_Oracle_Process_Log__c) ? ra.GP_Oracle_Process_Log__c : '') 
                                        +'</td><td>'+ (ra.GP_Start_Date__c!=null ? string.valueof(ra.GP_Start_Date__c).removeEnd('00:00:00'):'')
                                        +'</td><td>'+(ra.GP_End_Date__c!=null ? string.valueof(ra.GP_End_Date__c).removeEnd('00:00:00'):'')
                                        +'</td></tr>';
                                }
                            }                            
                            
                            htmlBody += '</table><br/><br/>';
                            htmlBody += 'Please log on to Pinnacle-SFDC : <a href="' + System.Label.GP_Send_Notification_Alert_URL + pidObj.Id +'">'+pidObj.Name+'</a><br/>';
                            htmlBody += 'This is an Auto Generated Mail. Please do not reply.';
                            htmlBody += '<br/><br/>Thanks';
                            htmlBody += '<br/>Pinnacle SFDC.<br/><br/>';
                        }
                        
                        if(String.isNotBlank(htmlBody)) {
                            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                            
                            // Get email addresses per PID.
                            List<String> listOfEmailAddress = getEmailAddresses(pidObj.GP_Send_Email_Notification_Addresses__c);
                            
                            if(!listOfEmailAddress.isEmpty()) {
                                email.setToAddresses(listOfEmailAddress);
                                email.setSubject('Resource Allocation Status on PID '+pidObj.GP_Oracle_PID__c);
                            
                                email.setHtmlBody(htmlBody);
                                
                                if (oweaId != null) {
                                    email.setOrgWideEmailAddressId(oweaId);
                                }
                                
                                emailNotifications.add(email);
                            }
                        }
                    }
					System.debug('==emailNotifications=='+emailNotifications);
                    if(!emailNotifications.isEmpty()) {
                        Messaging.sendEmail(emailNotifications);
                        
                        if(!lstProjecttoUpdateNotificationFlag.isEmpty()) {
                            GPDomainProject.skipRecursiveCall = true;
                            update lstProjecttoUpdateNotificationFlag;
                        }
                    }
                }
            }
        }
    }

    
    //Author: Anoop Verma
    // Date:27-Jun-2020
    // CR:CRQ000000076988
    // Description: Get the email addresses from GP_Send_Email_Notification_Addresses__c
    private List<String> getEmailAddresses(String emailAddresses) {
        List<String> listOfEmails = new List<String>();
        List<String> listOfTempEmails = emailAddresses.split(';');
        
        for(String str : listOfTempEmails) {
            if(String.isNotBlank(str)) {
                listOfEmails.add(str);
            }
        }
        
        return listOfEmails;
    }
}