public class VIC_CalculateUpfrontIncentivesController {
    public static ID oli_ID{get;set;}
    public Boolean calculateBDEbtn{get;set;}
    public String userID{get;set;}
    public Boolean calculatebtn{get;set;}
    
    public VIC_CalculateUpfrontIncentivesController(ApexPages.StandardController controller){
        //system.debug('record ID==='+controller.getRecord().Id);
        calculatebtn = false;
        oli_ID = controller.getRecord().Id;
        
        userID = UserInfo.getUserId();
        
        User u = [Select Id,ProfileId FROM User WHERE Id =: userID];
        Profile p = [Select id,Name from Profile Where Id =: u.ProfileId];
        System.debug('Profile Name == >> '+p.Name);
        
        if(p.Name == 'VIC Team' || p.Name == 'System Administrator'){
            calculatebtn = true;
        }else{
            calculatebtn = false;
        }
    }

    public static void executeBatchApexMethod(){
        
        try{
        system.debug(':--OliId---:'+oli_ID);
        VIC_CalculateUpfrontIncentivesBatch upfrontBatch = new VIC_CalculateUpfrontIncentivesBatch(TRUE); 
        upfrontBatch.strOLIId =oli_ID; 
        Database.executeBatch(upfrontBatch, 1); 
        System.debug('Success');
        }catch(Exception e){
            system.debug(':--error---:'+e.getMessage()+':---error line-----:'+e.getLineNumber());
        }
    }

}