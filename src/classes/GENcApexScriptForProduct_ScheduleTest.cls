@isTest
private class GENcApexScriptForProduct_ScheduleTest
{
    public static testMethod void RunTest1()
    {
        List<Product_Schedule_Dummy__c> lstProductScheduleDummy = new List<Product_Schedule_Dummy__c>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u =GEN_Util_Test_Data.CreateUser('standarduser2015@testorg.com',p.Id,'standardusertestgen2015@testorg.com' );
        Business_Segments__c oBS = GEN_Util_Test_Data.CreateBusiness_Segments('Test','GE');
        Sub_Business__c oSB = GEN_Util_Test_Data.CreateSub_Business('Test','GE',oBS.Id);
        Archetype__c oAT=GEN_Util_Test_Data.CreateArchetype('test');
        Account oAccount = GEN_Util_Test_Data.CreateAccount(Userinfo.getUserId(),u.Id,oAT.Id,'Test Account','GE',oBS.Id,oSB.Id,'Manufacturing','Aerospace & Defense','Mining Priority','12','12345676');
        AccountTeamMember oAccountTeamMember=GEN_Util_Test_Data.CreateAccountTeamMember(oAccount.id,Userinfo.getUserId());
        Contact oContact = GEN_Util_Test_Data.CreateContact('Manoj','Pandey',oAccount.Id,'test','Cross-Sell','test@gmail.com','9891798737');
        Opportunity oOpportunity = GEN_Util_Test_Data.CreateOpportunity('Test',oAccount.Id,oContact.Id);
        Product2 oProduct = GEN_Util_Test_Data.CreateProduct('Additional Client Licenses');
        OpportunityProduct__c oOpportunityProduct =  GEN_Util_Test_Data.CreateOpportunityProduct(oOpportunity.Id,oProduct.Id,12,12000);
        for(integer i=0;i<20;i++)
        {
            Product_Schedule_Dummy__c oProductScheduleDummy = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,null,12);
            Product_Schedule_Dummy__c oProductScheduleDummy1 = GEN_Util_Test_Data.CreateProductScheduleDummy(oOpportunityProduct.id,1000,24);
            lstProductScheduleDummy.add(oProductScheduleDummy);
            lstProductScheduleDummy.add(oProductScheduleDummy1);
        }
         test.startTest();
        insert lstProductScheduleDummy;
         test.stopTest();
    }
    
}