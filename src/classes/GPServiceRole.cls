public with sharing class GPServiceRole {
    //@future
    // to Avoid Mixed-DML-Exception the trigger method is Called from future Annotation 
    // The GPFutureHandlerUtil takes Care of all the future methods
    public Static void insertGroup(set<Id> Setofid)
    {
        if(Setofid.size() > 0)
        {
            list<GP_Role__c> lstRole = new list<GP_Role__c>(new GPSelectorRole().selectByRoleId(Setofid));
            
            set<String> setOfGroupName = new set<String>();
            if(lstRole != null && lstRole.size() > 0)
            {
                 for(GP_Role__c EachRole : lstRole)
                 {
                 	if(EachRole.GP_Group_Name_for_Read_Access__c != null){
                 		setOfGroupName.add(EachRole.GP_Group_Name_for_Read_Access__c);
                 	}
                 	/* // Below code commented by AMIT PPT due to new logic written as per GP_Group_Name_for_Read_Access__c field//07-04-2018
                    if(EachRole.GP_Work_Location_SDO_Master__c != null)
                    {
                        setOfGroupName.add('GP_SDO_'+EachRole.GP_Work_Location_SDO_Master__c + Label.GP_Group_Read_Label);
                    }
                    
                    if(EachRole.GP_Work_Location_SDO_Master__c != null && EachRole.GP_Work_Location_SDO_Master__r.GP_Business_Name__c != null 
                    	&& EachRole.GP_Work_Location_SDO_Master__r.GP_Business_Type__c != null && EachRole.GP_Deal_Access_Group_Name__c != null)
                    {
                        setOfGroupName.add(EachRole.GP_Deal_Access_Group_Name__c);
                    }
                                        
                    if(EachRole.GP_Customer_L4__c != null)
                    {
                        setOfGroupName.add('GP_L4_'+EachRole.GP_Customer_L4__c + Label.GP_Group_Read_Label); 
                    }
                    
                    if(EachRole.GP_HSL_Master__c != null )
                    {  
                        setOfGroupName.add('GP_HSL_'+EachRole.GP_HSL_Master__c + Label.GP_Group_Read_Label);            
                    }*/
                    
                }
            }
            
            if(setOfGroupName != null && setOfGroupName.size() > 0)
            {
              map<string, Group> mapOfGroup = GPCommon.getGroupByName(setOfGroupName);
                system.debug('mapOfGroup'+mapOfGroup);
            }
        }
    }
//Returns the map of Email address of the users related to the respected role id   
//need to check
    public Map<Id, Set<String>> getUserEmailsForRole(set<Id> setofroleid){
        Map<Id,Set<String>> mapOfUsersOfEmailIds = new MAp<Id,Set<String>>();    
        for(GP_User_Role__c objUserrole : [select id,GP_Role__c,GP_User__c, GP_User__r.Email 
                                           from GP_User_Role__c 
                                           where GP_Role__c IN :setofroleid]){
            if(mapOfUsersOfEmailIds.containsKey(objUserrole.GP_Role__c))
            {
                (mapOfUsersOfEmailIds.get(objUserrole.GP_Role__c)).add(objUserrole.GP_User__r.Email);
            }
            
            else{
                mapOfUsersOfEmailIds.put(objUserrole.GP_Role__c,new Set<String>{objUserrole.GP_User__r.Email});
            }
        }
        return mapOfUsersOfEmailIds;
    }

    public class GPServiceRoleException extends Exception {} 
}