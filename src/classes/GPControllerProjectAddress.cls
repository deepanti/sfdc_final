/**
* @author Anmol.kumar
* @date 22/10/2017
*
* @group ProjectAddress
* @group-content ../../ApexDocContent/ProjectAddress.html
*
* @description Apex Service for project Address module,
* has aura enabled method to CRUD project Address data.
*/
public class GPControllerProjectAddress {
    
    private static final String LIST_OF_PROJECT_ADDRESS_LABEL = 'listOfProjectAddress';
    private static final String PROJECT_ADDRESS_NULL_ERROR_LABEL = 'Please specify project address';
    private static final String PROJECT_ADDRESS_ID_NULL_ERROR_LABEL = 'Please specify project address Id';
    private static final String CUSTOMER_ID_NULL_ERROR_LABEL = 'Customer Id and/or Billing Entity Id are null';

    private static final String LIST_OF_BILL_TO_ADDRESS_LABEL = 'listOfBillToAddress';
    private static final String LIST_OF_SHIP_TO_ADDRESS_LABEL = 'listOfShipToAddress';

    private static final String SUCCESS_LABEL = 'SUCCESS';
    private static final String BILL_TO_LABEL = 'BILL_TO';
    private static final String SHIP_TO_LABEL = 'SHIP_TO';
    

    private Map<Id, Map<String, List<GPOptions>>> mapOfCustomerIdToMapOfAddress;
    private Map<String, List<GPOptions>> mapOfAddress;
    
    private List<GP_Project_Address__c> listOfProjectAddress;
    private List<Id> listOfCustomerId;
    
    private GP_Address__c genpactDummy;
    private Id billingEntityID;
    private Id projectId;

    private JSONGenerator gen;

    /**
    * @description overloaded constructor to accept projectId.
    * @param Id projectId
    * 
    * @example
    * new GPControllerProjectAddress(<projectId>);
    */
    public GPControllerProjectAddress(Id projectId) {
        this.projectId = projectId;
    }

    /**
    * @description overloaded constructor to accept serializedListOfCustomerId and billingEntityID.
    * @param Id serializedListOfCustomerId
    * @param Id billingEntityID
    * 
    * @example
    * new GPControllerProjectAddress(<serializedListOfCustomerId, billingEntityID>);
    */
    public GPControllerProjectAddress(String serializedListOfCustomerId, Id billingEntityID) {
        if(serializedListOfCustomerId != null) {
            this.listOfCustomerId = (List<Id>)JSON.deserialize(serializedListOfCustomerId, List<Id>.class);   
        }
        this.billingEntityID = billingEntityID;
    }
    
    public GPControllerProjectAddress() {}
    
    /**
    * @description saves project address for serialized List Of ProjectAddress.
    * @param String serializedListOfProjectAddress
    * 
    * @example
    * new GPControllerProjectAddress().saveProjectAddress(<serializedListOfProjectAddress>);
    */
    public GPAuraResponse saveProjectAddress(String serializedListOfProjectAddress) {
        if(serializedListOfProjectAddress == null) {
            return new GPAuraResponse(false, PROJECT_ADDRESS_NULL_ERROR_LABEL, null);
        }

        try {
            listOfProjectAddress = (List<GP_Project_Address__c>)JSON.deserialize(serializedListOfProjectAddress, List<GP_Project_Address__c>.class);
            upsert listOfProjectAddress; 
			boolean wipeOutUSCanadaAddressfromProject=false;
            //Avalara Check
           projectId = listOfProjectAddress[0].GP_Project__c;
            GPSelectorProjectAddress projectAddressSelector = new GPSelectorProjectAddress();
        	List<GP_Project_Address__c> lstOfProjectAddress = projectAddressSelector.selectProjectAddressUnderProject(projectId);
               
            if(lstOfProjectAddress.size()>0)
            {
                Integer LastAddress=lstOfProjectAddress.size()-1;
                system.debug('===lstOfProjectAddress-size==='+lstOfProjectAddress.size());
                 if( ((lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r != null && lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != null) && 
                    (lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != 'US' && lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c !='CA'))
                    
                    &&
                    
                    ((lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r != null && lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != null) && 
                    (lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != 'US' && lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c !='CA'))
                   
                   )
                    {
                        wipeOutUSCanadaAddressfromProject = true;                       
                    }
                else
                {
                     if( 
                         ((lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r != null &&
                           lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != null) && 
                          (lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c == 'US' || 
                           lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c =='CA')) 
                         
                         ||
                         
                         ((lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r != null &&
                           lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != null) && 
                          (lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c == 'US' || 
                           lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c =='CA'))                         
                   )
                     {
                          wipeOutUSCanadaAddressfromProject = false;                         
                     }
                    
                }
               
                GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'ADDRESS');
                //Avalara Check
				project.GP_Country__c=null;
                project.GP_US_Address__c='';
                project.GP_State__c=null;
                project.GP_Pincode__c='';
                project.GP_City__c='';
                if(wipeOutUSCanadaAddressfromProject)
                {
					project.GP_Sub_Category_Of_Services__c=null;
                    project.GP_Service_Deliver_in_US_Canada__c=false;
                    project.GP_Tax_Exemption_Certificate_Available__c=false;
                   
                    
                }
                else
                {
                     if(CheckIfAvalaraLECode(project.Id)){
                     project.GP_Service_Deliver_in_US_Canada__c=true;
                    }
                }
                update project;  
            }
               
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);              
        }

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(listOfProjectAddress));
    }
	
	public Boolean CheckIfAvalaraLECode(Id Pid)
    {
        String LeCode=GPCmpServiceProjectDetail.getOperatingUnitLECode(Pid);
        If(LeCode!=null)
        {
          String res = GPCmpServiceProjectDetail.CheckAvalaraLECodeExists(LeCode);
            if(res=='Y')
            {
                return true;
            }
        }       
        
            return false;
        
    }
    
    /**
    * @description deletes project address for ProjectAddressId.
    * @param Id projectAddressId
    * 
    * @example
    * new GPControllerProjectAddress().deleteProjectAddress(<projectAddressId>);
    */
    public GPAuraResponse deleteProjectAddress(Id projectAddressId) {

        if(projectAddressId == null) {
            return new GPAuraResponse(false, PROJECT_ADDRESS_ID_NULL_ERROR_LABEL, null);
        }

        try {
            GP_Project_Address__c projectAddress = [select id,GP_Project__c from GP_Project_Address__c where id =: projectAddressId];
            projectId = projectAddress.GP_Project__c;
            delete projectAddress;

            /*Avalara Changes*/
            
            boolean wipeOutUSCanadaAddressfromProject=false;                      
            GPSelectorProjectAddress projectAddressSelector = new GPSelectorProjectAddress();
        	List<GP_Project_Address__c> lstOfProjectAddress = projectAddressSelector.selectProjectAddressUnderProject(projectId);
               
            if(lstOfProjectAddress.size()>0)
            {
                Integer LastAddress=lstOfProjectAddress.size()-1;               
                 if( ((lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r != null && lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != null) && 
                    (lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != 'US' && lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c !='CA'))
                    
                    &&
                    
                    ((lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r != null && lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != null) && 
                    (lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != 'US' && lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c !='CA'))
                   
                   )
                    {
                        wipeOutUSCanadaAddressfromProject = true;                       
                    }
                else
                {
                     if( 
                         ((lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r != null &&
                           lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c != null) && 
                          (lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c == 'US' || 
                           lstOfProjectAddress[LastAddress].GP_Bill_To_Address__r.GP_Country__c =='CA')) 
                         
                         ||
                         
                         ((lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r != null &&
                           lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c != null) && 
                          (lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c == 'US' || 
                           lstOfProjectAddress[LastAddress].GP_Ship_To_Address__r.GP_Country__c =='CA'))                         
                   )
                     {
                          wipeOutUSCanadaAddressfromProject = false;                         
                     }
                    
                }
               
                GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'ADDRESS');
                 //Avalara Check
                project.GP_Country__c=null;
                project.GP_US_Address__c='';
                project.GP_State__c=null;
                project.GP_Pincode__c='';
                project.GP_City__c='';
                if(wipeOutUSCanadaAddressfromProject)
                {                   
					project.GP_Sub_Category_Of_Services__c=null;
                    project.GP_Service_Deliver_in_US_Canada__c=false;
                    project.GP_Tax_Exemption_Certificate_Available__c=false;
                    
                }
                else
                {
                     if(CheckIfAvalaraLECode(project.Id)){
                     project.GP_Service_Deliver_in_US_Canada__c=true;
                    }
                }
           
            
            /*Avalara changes End Here*/            
           // GP_Project__c project = GPCommon.getProjectWithLastUpdatedDateAndSource(projectId, 'ADDRESS');
            update project;  
           
        }
	}		catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);             
        } 
        
        return new GPAuraResponse(true, SUCCESS_LABEL, null);
    }
    
    /**
    * @description returns project address.
    * 
    * @example
    * new GPControllerProjectAddress().getProjectAddress();
    */
    public GPAuraResponse getProjectAddress() {
        try {
            setProjectAddressData();
            setJSON();            
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);
        }
        
        return new GPAuraResponse(true, SUCCESS_LABEL, gen.getAsString());
    }

    private void setProjectAddressData() {
        GPSelectorProjectAddress projectAddressSelector = new GPSelectorProjectAddress();
        listOfProjectAddress = projectAddressSelector.selectProjectAddressUnderProject(projectId);
    }
    
    private void setJSON() {
        gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeObjectField(LIST_OF_PROJECT_ADDRESS_LABEL, listOfProjectAddress);
        gen.writeEndObject();
    }

    public GPAuraResponse getFilteredListOfAddress() {

        if (listOfCustomerId == null ||
            billingEntityID == null) {
            return new GPAuraResponse(false, CUSTOMER_ID_NULL_ERROR_LABEL, null);
        }

        mapOfCustomerIdToMapOfAddress = new Map<Id, Map<String, List<GPOptions>>>();
        mapOfAddress = new Map<String, List<GPOptions>>();

        setMapOfCustomerIdToMapOfAddress();

        return new GPAuraResponse(true, SUCCESS_LABEL, JSON.serialize(mapOfCustomerIdToMapOfAddress, true));
    }
    
    private void setMapOfCustomerIdToMapOfAddress() {
        String key, siteCode;

        for (GP_Address__c address: GPSelectorAddress.getAddress(listOfCustomerId, billingEntityID)) {
            
            //if this is a junk address without customer name 
            //then skip it, to avoid map with null key
            if(address.GP_Customer__c == null) {
                continue;
            }
            
            if(!mapOfCustomerIdToMapOfAddress.containsKey(address.GP_Customer__c)) {
                mapOfAddress = new Map<String, List<GPOptions>>();
            } else {
                mapOfAddress = mapOfCustomerIdToMapOfAddress.get(address.GP_Customer__c);
            }

            siteCode = address.GP_Site_Use_Code__c != null ? address.GP_Site_Use_Code__c.trim() : '';

            if(siteCode.equalsIgnoreCase(BILL_TO_LABEL)) {
                key = LIST_OF_BILL_TO_ADDRESS_LABEL;
            } else if(siteCode.equalsIgnoreCase(SHIP_TO_LABEL)) {
                key = LIST_OF_SHIP_TO_ADDRESS_LABEL;
            } else {
                continue;
            }

            if(!mapOfAddress.containsKey(key)) {
                mapOfAddress.put(key, new List<GPOptions>());
            }
            
            mapOfAddress.get(key)
                        .add(new GPOptions(address.Id, address.GP_SITE_NUMBER__c + ' -- ' + address.GP_Address__c));

            mapOfCustomerIdToMapOfAddress.put(address.GP_Customer__c, mapOfAddress);
        }
    }
}