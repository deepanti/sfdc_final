@isTest
public class VIC_CommonTrTest{
  public static Target_Achievement__c ObjInc;
  public static user objuser1;
  
  public static user objuser;
  public static Master_VIC_Role__c masterVICRoleObj;
  public static User_VIC_Role__c objuservicrole;
  public static Plan__c planObj1;
  public static APXTConga4__Conga_Template__c objConga;
  public static Opportunity objOpp;
  public static Target__c targetObj;
  public static Master_Plan_Component__c masterPlanComponentObj;
  public static list<Plan__c> lstPlan= new list<Plan__c>();
  public static list<Master_Plan_Component__c> lstMasterPlan=new list<Master_Plan_Component__c>();
  static testmethod void VIC_CommonTrTest(){
  
      loadData();
      VIC_CommonTest obj= new VIC_CommonTest();
      VIC_CommonTest.fetchIncentive(ObjInc.id,1000);
      VIC_CommonTest.getTargetPayout();
      VIC_CommonTest.getTargetComponent();
      VIC_CommonTest.gettarget();
      VIC_CommonTest.getUserVICRole(objuser.id);
      VIC_CommonTest.getMasterVICRole();
      VIC_CommonTest.getVICRole(masterVICRoleObj.id,planObj1.id);
      VIC_CommonTest.getPlan(objConga.id);
      VIC_CommonTest.createMasterVICRole();
      VIC_CommonTest.createCongaTemplate();
      VIC_CommonTest.createOpportunityLineItem('Test',objopp.id);
      VIC_CommonTest.createTriggerCustomSetting();
      VIC_CommonTest.createTriggerCustomSetting('test');
      
      VIC_CommonTest.createAccount('test');
      obj.fetchMetaDataValue();
      obj.fetchMatrixItemComponentData(1000, 1000, 'test', 1000, true, 1000, 1000, 'test');
      obj.fetchListMatrixItemTCVAcceleratorData('Test');
      obj.fetchListMatrixItemTCVData('test');
      obj.fetchListMatrixItemTSTCVData('test');
      obj.fetchListMatrixItemIOTCVData('test');
      obj.fetchListMatrixItemOPData('test');
      obj.fetchListMatrixItemMBOData('test');
      obj.fetchListMatrixItemPMData('test');
      VIC_CommonTest.fetchMatrixComponentData('test');
      obj.fetchTargetComponentData(1000,1000,1000,targetObj.id,'Test',masterPlanComponentObj.id,1000);
      obj.fetchTargetData(1000,planObj1.id,1000,1000,objuser1.id,system.today());
      VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planObj1.id);
      lstPlan.add(planObj1);
      lstMasterPlan.add(masterPlanComponentObj);
      obj.initAllPlanComponent(lstPlan,lstMasterPlan);
      VIC_CommonTest.fetchMasterPlanComponentData('Test','Test');
      obj.initAllMasterPlanComponent();
      obj.initAllPlanData(objConga.name);
      obj.initAllcongaData(objConga.name);
      system.assertequals(200,200);
  
  }
  
  static void loadData(){
       VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Process_Year__c=2018;
        insert vicInfo;
        objConga = VIC_CommonTest.createCongaTemplate();
       insert objConga;
        planObj1=VIC_CommonTest.getPlan(objConga.id);
        planObj1.vic_Plan_Code__c='IT_GRM';   
        planObj1.Is_Active__c = true;
        planObj1.Year__c= String.valueOf(System.today().year());
        insert planobj1;
        objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
        insert objuser;
        
        objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        insert objuser1;
            
        user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn13@jdljhd.com','Genpact Sales Rep','China');
        insert objuser2;
        
        user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn14@jkdjhd.com','Genpact Sales Rep','China');
        insert objuser3;
        system.debug('obbbbbjuser'+objuser);
        masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
        insert masterVICRoleObj;
        system.debug('masterVICRoles'+masterVICRoleObj);
        objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
        objuservicrole.vic_For_Previous_Year__c=false;
        objuservicrole.Not_Applicable_for_VIC__c = false;
        
        objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
        insert objuservicrole;
        
        Account objAccount=VIC_CommonTest.createAccount('Test Account');
        insert objAccount;
        VIC_Role__c vrole= VIC_CommonTest.getVICRole(masterVICRoleObj.id,planObj1.id);
         insert  vrole;       
        objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
        objOpp.Actual_Close_Date__c=system.today();
        objOpp.vic_Is_Kickers_Calculated__c=false;
        objOpp.OwnerId=objUser.id;
        
        insert objOpp;
        
        OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
        objOLI.vic_Final_Data_Received_From_CPQ__c=true;
        objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
        objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
        objOLI.Product_BD_Rep__c=objuser3.id;
        objOLI.vic_VIC_User_3__c=objuser1.id;
        objOLI.vic_VIC_User_4__c=objuser2.id;
        objOLI.vic_is_CPQ_Value_Changed__c = true;
        objOLI.Pricing_Deal_Type_OLI_CPQ__c='New Booking';
        objOLI.Contract_Term__c=36;
        objOLI.TCV__c=3500000;
        insert objOLI;
        
        system.debug('objOLI'+objOLI);
        targetObj=VIC_CommonTest.getTarget();
        targetObj.user__c =objuser.id;
        insert targetObj;
        
        masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('Test','Currency');
        masterPlanComponentObj.vic_Component_Code__c='TCV_Accelerators'; 
        masterPlanComponentObj.vic_Component_Category__c='Upfront';    
        insert masterPlanComponentObj;
            
        Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
        insert PlanComponentObj;
            
        Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
        targetComponentObj.Target__C=targetObj.id;
        targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
        insert targetComponentObj;
        
        ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
        ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
        ObjInc.Target_Component__r=targetComponentObj;
        ObjInc.Opportunity__c=objOpp.id;
        objInc.vic_Status__c='HR - Pending';
        //objInc.VIC_Related_Case__R=objcase;
        insert objInc;
        
    
    }
    


}