@isTest
public class OpportunitySalesPathTest {
    
	@isTest
    public static void getListViewsTest()
    {
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
        insert con;
        Opportunity Oppty =TestDataFactoryUtility.CreateOpportunity('Test', acc.Id, con.Id);
        insert Oppty;
        OpportunitySalesPath.getListViews();
        
    }
        
    @isTest
    public static void testGetTabIdPositive()
    {
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
        insert con;        
        Opportunity Oppty =TestDataFactoryUtility.CreateOpportunity('Test', acc.Id, con.Id);
        Oppty.LatestTabId__c = '5';
        insert Oppty;
        Opportunity opntty = OpportunitySalesPath.getLatestTabId(Oppty.Id);
        
    }
    
     @isTest
    public static void testGetTabIdNagative()
    {
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
        insert con;
        //creating opportunity
       Opportunity Oppty =TestDataFactoryUtility.CreateOpportunity('Test', acc.Id, con.Id);
        Oppty.LatestTabId__c = '';
        insert Oppty;
        Opportunity opntty = OpportunitySalesPath.getLatestTabId(Oppty.Id);
    }
      @isTest
    public static void testFetchRecord()
    {
        Account acc=TestDataFactoryUtility.createTestAccountRecord();
        insert acc;
        Contact con=TestDataFactoryUtility.CreateContact('strFirstName', 'strLastName', acc.Id, 'strTitle', 'strLeadSource', 'strEmail@gmail.com', '123456789');
        insert con;
         Opportunity Oppty =TestDataFactoryUtility.CreateOpportunity('Test', acc.Id, con.Id);

       try
            {
                OpportunitySalesPath.fetchRecord(Oppty.Id);
              
            }
            catch(Exception e)
            {
                system.debug('Exception'+e);
            }
    }
               
}