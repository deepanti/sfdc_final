@isTest
private class VIC_CalculateUpfrontIncentivesCtrl_Test {

	private static testMethod void test() {
        
        try{
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = p.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2016ggg@testorg.com');
            insert u;
            
            Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
            insert salesunitobject;
            account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
            insert accountobject;
          
           
            opportunity opp=new opportunity(name='1234',StageName='1. Discover',CloseDate=system.today(),Revenue_Start_Date__c=system.today()+1,accountid=accountobject.id);
            insert opp;
            Product2 oProduct = New Product2(Product_Family__c='Chekc',name='Chk2',Product_Family_Code__c='F0029',ProductCode='1254');
            insert oProduct;
            OpportunityProduct__c OLi=new OpportunityProduct__c(Opportunityid__c=opp.id,Product_Autonumber__c='123456',Legacy_Intenal_ID_DM__c='',Legacy_NS_Internal_ID__c='',Service_Line_OLI__c='Multi Channel Customer Service',Product_Family_OLI__c='Collections',Product__c=oProduct.ID,LocalCurrency__c='JPY',SalesExecutive__c=u.id,GE_GC_for_SR__c='');
            insert OLi;
            
        	
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(OLi);
            VIC_CalculateUpfrontIncentivesController obj = new VIC_CalculateUpfrontIncentivesController(sc);
            VIC_CalculateUpfrontIncentivesController.oli_ID = OLi.id;
            VIC_CalculateUpfrontIncentivesController.executeBatchApexMethod();
        Test.stopTest();
        }catch(Exception e){
            
        }
        
        
	}

}