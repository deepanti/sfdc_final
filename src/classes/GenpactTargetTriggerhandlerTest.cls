@isTest
public class GenpactTargetTriggerhandlerTest{
    
    static testMethod void validateGenpactTargetTriggerhandler() {
        user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn11@jdjhdg.com','System administrator','China');
        insert objuser;
        
        user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn12@jdpjhd.com','Genpact Sales Rep','China');
        objuser1.Supervisor__c=objuser.id;
        insert objuser1;
        
        Target__c targetObj = new Target__c();
        targetObj.User__c=objuser1.id;
        insert targetObj;
        targetObj=[select id,vic_Supervisor_Id__c from Target__c where id=:targetObj.id];
         update targetObj;
        System.assertEquals(200,200);
    }

}