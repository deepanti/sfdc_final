/**
* @author Anoop.Verma
* @date 11/06/2017
*
* @group ProjectInvoices
* @group-content ../../ApexDocContent/ProjectInvoices
*
* @description Apex Controller class  for invoices to fetch data from Orcale instance.
*/
public class GPControllerProjectInvoices
{
	
    public string PO;
    public long amount;
    public string TRXID;
    public Id projectID;    
    private string Email;
    public string dueDate;
    public string invoiceDate;
    public string invoiceNumber;
    private GP_Project__c projectObj;
    public string oracleInvoiceNumber;
    public string serviceDeliveryEndDate;
	public string serviceDeliveryStartDate;
    private List<GPOptions> listOfInvoiceStatus;
    public List<GPControllerProjectInvoices> listofInvoices {get; set;}   
    private List<GP_Invoice_Email_Notification__c> listofEmailNotification;
	
    public GPControllerProjectInvoices() {}

    public GPControllerProjectInvoices(Id projectId) {
		this.projectID = projectId;
	}
    
    public GPControllerProjectInvoices (List<GP_Invoice_Email_Notification__c> listofEmailNotification) {
		this.listofEmailNotification = listofEmailNotification;
	}
	    
	public static GPControllerProjectInvoices parse(String json) {
        return (GPControllerProjectInvoices) System.JSON.deserialize(json, GPControllerProjectInvoices.class);
    }
    
    public  GPAuraResponse pullProjectInvoiceDetails() {   
        String serializedResponse;
        projectObj = new GPSelectorProject().selectProjectRecord(projectId); 
       	//projectObj.GP_Oracle_PID__c='91523457';
        if (String.isNotBlank(projectObj.GP_Oracle_PID__c)) {
            try {
                //Call  ICS webservice to get the invoice data
                serializedResponse = getSampleJSON(projectObj.GP_Oracle_PID__c); 
            } catch(Exception ex) {
                return new GPAuraResponse(false, ex.getMessage(), null);
            }
        }
        return new GPAuraResponse(true, 'SUCCESS', serializedResponse); 
    }

    public GPAuraResponse sendInvoiceNotification(String serializedEmailInvoiceRequest) {
        List<GP_Invoice_Email_Notification__c> emailNotification = (list<GP_Invoice_Email_Notification__c>) JSON.deserialize(serializedEmailInvoiceRequest,list< GP_Invoice_Email_Notification__c>.class);
        // Instantiate a new http object
        String loggedInUserEmail = userInfo.getUserEmail();
        
        
        Date d = date.today();
        String SysDate = DateTime.newInstance(d.year(),d.month(),d.day()).format('d-MMM-YYYY');
        

        Http h = new Http();
        Blob headerValue = Blob.valueOf(system.label.GP_ICS_User_Name+':'+system.label.GP_ICS_Password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);

        String url = system.label.GP_ICS_End_point_for_Invoice_Id_User_Email_ID;
		JSONGenerator objJSONGen = JSON.createGenerator(false);
        
         objJSONGen.writeStartObject();
        	 objJSONGen.writeStringField('CustomerTrxId',emailNotification[0].GP_Oracle_Invoice_ID__c);
        	 objJSONGen.writeStringField('EmailId',loggedInUserEmail);
         	objJSONGen.writeStringField('SysDate',string.valueof(SysDate));
         objJSONGen.writeEndObject();
       
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(objJSONGen.getAsString());
        //req.setCompressed(true); // otherwise we hit a limit of 32000
		 system.debug( req.getBody());
        // Send the request, and return a response
        try {
        	if(system.label.GP_Allow_Email_Invoice_callOut.EqualsIgnorecase('true'))
            {
             	 HttpResponse res = h.send(req);
                String response = res.getBody();
            }
         }catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);   
        }
       	return new GPAuraResponse(true, 'SUCCESS', 'SUCCESS'); 
    }

    public GPAuraResponse  getOracleProjectNumber() {  
        string oracleProjectNumber;
        try {
            projectObj = new GPSelectorProject().selectProjectRecord(projectId);
            oracleProjectNumber=projectObj.GP_Oracle_PID__c;
        } catch(Exception ex) {
            return new GPAuraResponse(false, ex.getMessage(), null);   
        }
        return new GPAuraResponse(true, 'SUCCESS', oracleProjectNumber);
    }
    
   /* private Boolean isExistingEmailRequest( list<GP_Invoice_Email_Notification__c>listofExistingInvoiceEmailRequest) {
        Boolean isExist=true;
        GP_Invoice_Email_Notification__c invoiceRequest=listofExistingInvoiceEmailRequest[0];
        string projectID=invoiceRequest.GP_Project__c;
        string userID=invoiceRequest.GP_User__c;
        string TRXID=invoiceRequest.GP_Oracle_Invoice_ID__c;
        list<GP_Invoice_Email_Notification__c> listofOldInvoiceEmailRequest =[SELECT  Name ,GP_Project__c,GP_User__c,GP_Email__c,GP_Oracle_Invoice_ID__c ,GP_Status__c from GP_Invoice_Email_Notification__c
       	WHERE GP_project__C=:projectID AND GP_User__c=:userID AND GP_Oracle_Invoice_ID__c=:TRXID AND  GP_Status__C in('Not Initiated','Failed')];
        if (listofOldInvoiceEmailRequest.size()<1) {
            isExist=false;
        }
        return isExist;
    }*/
    
    private void setInvoiceStatus() {
        listOfInvoiceStatus = GPInputPicklistService.getPicklistOptions('GP_Status__c', 'GP_Status');   
    }
 
    public static string getSampleJSON(String oraclePID) {
        // Instantiate a new http object
        Http h = new Http();
         Blob headerValue = Blob.valueOf(system.label.GP_ICS_User_Name+':'+system.label.GP_ICS_Password);
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);

        String url = System.label.GP_ICS_End_Point;
        url += '?ProjectNumber=' + oraclePID;

        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Authorization', authorizationHeader);
 
        // Send the request, and return a response
       if(system.label.GP_Allow_Email_Invoice_callOut.EqualsIgnorecase('true'))
            {
             	 HttpResponse res = h.send(req);
                 return res.getBody();
            }
        return '';
       
    }
    
    
}