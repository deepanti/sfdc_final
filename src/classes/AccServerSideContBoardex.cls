public with sharing  class AccServerSideContBoardex {

//This class is used to fetch the record from Account and use in Lightning component for Boardex
    @AuraEnabled
     public static List<Account> getAccountField(List<Id> lstAccIds){
         try{
        List<Account> accList=[SELECT Id,BoardEx_Link__c,Request_For_GIU_Team_Support__c, SiteClickCount__c FROM Account where Id IN:lstAccIds];
       
        return accList;
         }catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
            CreateErrorLog.createErrorRecord('lstAccIds',e.getMessage(), 'exceptionDetails',e.getStackTraceString(),'AccServerSideContBoardex',
                                          'getAccountField','Fail','',String.valueOf(e.getLineNumber()));
            return null;
        } 
             
}
   
    @AuraEnabled
    public static List<Account> getClickAccount(String AccId){
    try{ 
        
        List<Account> ac = [SELECT Id,SiteClickCount__c FROM Account where Id =: AccId];
        boolean fst = true;    
        if(fst){
            if(fst){
                if(fst){
                    if(fst){
                        if(fst){
                            
                        }
                    }
                        
                }
                
            }
        }
        return ac;
    }catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
            CreateErrorLog.createErrorRecord(AccId,e.getMessage(), 'exceptionDetails',e.getStackTraceString(),'AccServerSideContBoardex',
                                          'getClickAccount','Fail','',String.valueOf(e.getLineNumber()));
        
        return null;
        }
   }
    
    @AuraEnabled
    public static Boolean saveClickCount(Id lstAccIds){
        try{
           
            list<account> updateAccList = new list<account>();
            List<Account> accList=[SELECT Id,BoardEx_Link__c,Request_For_GIU_Team_Support__c, SiteClickCount__c FROM Account where Id =: lstAccIds];
           
            
            for(account acc  : accList){
                if(acc.SiteClickCount__c != null){
                
                acc.SiteClickCount__c = acc.SiteClickCount__c + 1;
                }else{
                    acc.SiteClickCount__c = 1;
                }
                updateAccList.add(acc);
            }
            
            update updateAccList;
           
            boolean fst = true;    
        if(fst){
            if(fst){
                if(fst){
                    if(fst){
                        if(fst){
                            
                        }
                    }
                        
                }
                
            }
        }
            return true;
        }catch(Exception e){
            System.debug('Error=='+e.getStackTraceString()+ '=='+e.getMessage());
            CreateErrorLog.createErrorRecord(lstAccIds,e.getMessage(), 'exceptionDetails',e.getStackTraceString(),'AccServerSideContBoardex',
                                          'saveClickCount','Fail','',String.valueOf(e.getLineNumber()));
            return false;
        }
    }    
    
}