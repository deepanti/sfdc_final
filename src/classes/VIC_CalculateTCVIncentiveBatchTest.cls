@isTest
public class VIC_CalculateTCVIncentiveBatchTest
{
Public static Target__c targetObj;    
static testmethod void ValidateVIC_CalculateTCVIncentiveBatch()
{
   LoadData();
   
  VIC_CalculateTCVIncentiveBatch obj = new VIC_CalculateTCVIncentiveBatch();
  database.executeBatch(obj);
  System.AssertEquals(200,200);

    
}
static void LoadData()
{

    VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Annual_Process_Year__c=2018;
        vicInfo.VIC_Process_Year__c=2018;
       insert vicInfo;
user objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjn@jdjhdg.com','System administrator','China');
insert objuser;

user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='IT_GRM';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

Opportunity objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;

OpportunityLineItem  objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.Product_BD_Rep__c=objuser.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id;   
objOLI.vic_Is_Split_Calculated__c=false;

insert objOLI;
update objOLI;

system.debug('objOLI'+objOLI);

targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
targetObj.vic_TCV_IO__c=10000;  
targetObj.vic_TCV_TS__c=10000;   
targetObj.Target_Bonus__c=350000;

targetObj.Plan__C=planobj1.id;
insert targetObj;
targetObj.Start_date__c=system.today();
update targetObj;
  

Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('TCV','Currency');
masterPlanComponentObj.vic_Component_Code__c='TCV'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;
    
VIC_Calculation_Matrix__c vicMatrix1=VIC_CommonTest.fetchMatrixComponentData('IO');
insert vicMatrix1;
VIC_Calculation_Matrix__c vicMatrix2=VIC_CommonTest.fetchMatrixComponentData('SEM');
insert vicMatrix2;       
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
PlanComponentObj.VIC_IO_Calculation_Matrix_1__c=vicMatrix1.id;
insert PlanComponentObj;
    
Target_Component__c targetComponentObj=VIC_CommonTest.getTargetComponent();
targetComponentObj.Target__C=targetObj.id;
targetComponentObj.Target__r=   targetObj; 
targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
targetComponentObj.Target_In_Currency__c=10000;
targetComponentObj.Weightage__c=10; 
  
insert targetComponentObj;
targetComponentObj=[select id,Target__c,Target__r.vic_TCV_IO__c,Target__r.vic_TCV_TS__c,Target__r.Plan__r.vic_Plan_Code__c,Target__r.Is_Active__c,Master_Plan_Component__r.Name,Target_In_Currency__c,Weightage__c,Target__r.Target_Bonus__c,Target__r.Start_date__c,Master_Plan_Component__r.vic_Component_Code__c,Target__r.vic_Total_TCV__c from Target_Component__c where id=:targetComponentObj.id];
system.debug('targetComponentObj'+targetComponentObj);
system.debug('targetComponentObj'+targetComponentObj);    
Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
insert objInc;
 
 System.AssertEquals(200,200);
   
system.debug('targetObj'+targetObj.vic_Total_TCV__c);
system.debug('targetObj1'+targetObj.vic_TCV_IO__c);
system.debug('targetObj2'+targetObj.vic_TCV_TS__c);
system.debug('targetObj3'+targetObj.Plan__r.vic_Plan_Code__c);
system.debug('targetComponentObj1'+targetComponentObj.Master_Plan_Component__r.Name );
system.debug('targetComponentObj2'+targetComponentObj.Target_In_Currency__c);
system.debug('target4'+targetComponentObj.Target__r.Target_Bonus__c);   
system.debug('targetComponentObj2'+targetComponentObj.Target__r.Start_date__c);    



}
     

}