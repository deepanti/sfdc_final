public class GPSelectorTimeSheet extends fflib_SObjectSelector {
    public List<Schema.SObjectField> getSObjectFieldList() {
        return new List<Schema.SObjectField> {
            GP_Timesheet__c.Name
                
                };
                    }
    
    public Schema.SObjectType getSObjectType() {
        return GP_Timesheet__c.sObjectType;
    }
    
    public List<GP_Timesheet__c> selectById(Set<ID> idSet) {
        return (List<GP_Timesheet__c>) selectSObjectsById(idSet);
    }
    
    public List<GP_Timesheet__c> selectByEmployeeMonthYearUniqueField(Set<String> setOfEmployeeMonthYear) {
        return [SELECT Id, GP_Person_Id__c, GP_Employee_Number__c, GP_Orig_Transaction_Reference__c,
                GP_Project_Number__c, GP_Project_Name__c, GP_Expenditure_Item_Date__c,
                GP_Customer_Name__c, GP_Task_Number__c, 
                GP_Status__c, GP_Billing_Type__c, GP_Hours__c, GP_Timesheet_Creation_Date__c, GP_Timesheet_Update_Date__c,
                GP_Expenditure_Type__c, GP_Month_Year__c, GP_PersonId_MonthYear__c FROM GP_Timesheet__c
               where GP_PersonId_MonthYear__c in :setOfEmployeeMonthYear LIMIT 2000];
    }
}