/**
* @author Sumit Shukla
* @date 19/05/2017
*
* @group Error
* @group-content ../../ApexDocContent/Error.htm 
*
* @description Test class for GPErrorLogUtility class
**/
@isTest(seeAllData = false)
public class GPErrorLogUtilityTest {
    /**
    * @description below test all  method
    **/
    @isTest 
    static void testMethodException() {
        // start Test
        test.startTest();
        try {
           integer result = 10/0;
        }
        catch(exception exceptionObject) {
            GPErrorLogUtility.logError('Test Class - GPErrorLogUtilityTest','Test Method - testMethodException',exceptionObject,'','','','','','');
            GPErrorLogUtility.logDebug('Test Class - GPErrorLogUtilityTest','Test Method - testMethodException','Debug Message','','','','','');
            GPErrorLogUtility.logInfo('Test Class - GPErrorLogUtilityTest','Test Method - testMethodException','Log Message','','','','','');
            GPErrorLogUtility.logWarn('Test Class - GPErrorLogUtilityTest','Test Method - testMethodException','Log Message','','','','','');
            GPErrorLogUtility.commitLog();
            GP_Error_Log__c errLog = [SELECT id, GP_Source__c,GP_Source_Function__c FROM GP_Error_Log__c WHERE GP_Source_Function__c = 'Test Method - testMethodException' LIMIT 1];

            // Assert check
            System.assertEquals('Test Class - GPErrorLogUtilityTest',errLog.GP_Source__c);
        }
        
        // Stop Test 
        test.stopTest();
    } 
}