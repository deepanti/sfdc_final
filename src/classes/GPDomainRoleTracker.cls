@isTest
public class GPDomainRoleTracker{
    public static GP_Role__c objrole;
    public static GP_Role__c anotherRole;
    public static GP_Role__c roleForReadAccess;
    public static GP_Role__c roleForEditAccess;
    public static GP_Role__c roleForCustomerl4;
    public static GP_Role__c roleForDeliveryOrg;
    public static GP_User_Role__c userRole;
    public static user Usr;
    public Static Account objAccount;
    @testSetup
    public static void buildDependencyData() 
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_role__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        Insert objBS;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        Insert objSB;
        
        GP_Work_Location__c objSdo1 = GPCommonTracker.getWorkLocation();
        objSdo1.GP_Status__c = 'Active and Visible';
        objSdo1.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo1;
        
        Account objAccount = GPCommonTracker.getAccount(objBS.Id,objSB.Id);
        insert objAccount;
        
        Account objAccount2 = GPCommonTracker.getAccount(objBS.Id,objSB.Id);
        insert objAccount2;
        
        // GP_Pinnacle_Master__c objpinnacleMaster1 = GetpinnacleMasterForTrigger();
        //insert objpinnacleMaster1;
        
        GP_Employee_Master__c empObj = GPCommonTracker.getEmployee();
        insert empObj; 
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster;
        
        objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        objrole.GP_Active__c = true;
        objrole.GP_Type_of_Role__c = 'HSL Master';
        objrole.GP_HSL_Master__c = objpinnacleMaster.id;
        insert objrole;
        
        /*anotherRole = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        anotherRole.GP_Active__c = true;
        anotherRole.GP_Type_of_Role__c ='SDO';
        anotherRole.GP_Work_Location_SDO_Master__c = objSdo.id;
        insert anotherRole;*/
        
        anotherRole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        //anotherRole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        anotherRole.GP_HSL_Master__c = null;
        anotherRole.GP_Role_Category__c = 'PID Creation';
        anotherRole.GP_Type_of_Role__c = 'Global';
        insert anotherRole;
        
        /*roleForReadAccess = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        roleForReadAccess.GP_Active__c = true;
        roleForReadAccess.GP_Type_of_Role__c ='SDO';
        roleForReadAccess.GP_Work_Location_SDO_Master__c = objSdo.id;
        insert roleForReadAccess;*/
        
        roleForReadAccess = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        roleForReadAccess.GP_Work_Location_SDO_Master__c = objSdo.Id;
        roleForReadAccess.GP_HSL_Master__c = null;
        roleForReadAccess.GP_Role_Category__c = 'PID Approver';
        insert roleForReadAccess;
        
        /*roleForEditAccess = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        //roleForEditAccess.GP_Active__c = true;
        roleForEditAccess.GP_Type_of_Role__c = 'Delivery Org';
        roleForEditAccess.GP_Work_Location_SDO_Master__c = null;
        insert roleForEditAccess;*/
        
        roleForEditAccess = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        roleForEditAccess.GP_Work_Location_SDO_Master__c = objSdo.Id;
        roleForEditAccess.GP_HSL_Master__c = null;
        roleForEditAccess.GP_Role_Category__c = 'PID Creation';
        insert roleForEditAccess;
        
        /*roleForDeliveryOrg = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        roleForDeliveryOrg.GP_Type_of_Role__c = 'Delivery Org';
        //roleForDeliveryOrg.GP_Work_Location_SDO_Master__c = null;
        insert roleForDeliveryOrg;*/
        
        roleForDeliveryOrg = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        roleForDeliveryOrg.GP_Work_Location_SDO_Master__c = objSdo.Id;
        roleForDeliveryOrg.GP_HSL_Master__c = null;
        roleForDeliveryOrg.GP_Role_Category__c = 'PID Creation';
        insert roleForDeliveryOrg;
        
        /*roleForCustomerl4 = GPCommonTracker.getRole(objSdo,objpinnacleMaster);
        Account acc = new Account();
        acc.Name = 'Test Acc';
        insert acc;
        roleForCustomerl4.GP_Type_of_Role__c = 'Customer L4';
        roleForCustomerl4.GP_Customer_L4__c = acc.id;
        roleForCustomerl4.GP_Active__c = True;
        insert roleForCustomerl4;*/
        
        roleForCustomerl4 = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        //roleForCustomerl4.GP_Work_Location_SDO_Master__c = objSdo.Id;
        roleForCustomerl4.GP_HSL_Master__c = null;
        roleForCustomerl4.GP_Type_of_Role__c = 'Customer L4';
        roleForCustomerl4.GP_Role_Category__c = 'PID Creation';
        roleForCustomerl4.GP_Customer_L4__c = objAccount.id;
        insert roleForCustomerl4;
        roleForCustomerl4.GP_Customer_L4__c = objAccount2.id;
        update roleForCustomerl4;
        
        User objuser = GPCommonTracker.getUser();
        objuser.username = 'test555555555@jkj.ccc';
        objuser.lastname = 'asassas';
        insert objuser; 
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        insert objprjtemp ;
        
        GP_Timesheet_Transaction__c timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObj);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Project__c prjObj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjObj.OwnerId=objuser.Id;
        prjObj.GP_Approval_Status__c = 'Approved';
        //insert prjObj ;
        
        //GP_Timesheet_Entry__c timeshtentryObj = GPCommonTracker.getTimesheetEntry(empObj,prjObj,timesheettrnsctnObj);
        //insert timeshtentryObj ;
        
        objrole.GP_Active__c = true;
        update objrole;
        
        objrole.GP_Active__c = false;
        update objrole;
    }
    @isTest
    public static void testGPDomainRole() {
        //buildDependencyData();
        testOnAfterInsert();
        testgetUserEmailsForRole();
        
    }
    
    public static void testOnAfterInsert(){
        //GPDomainRole testRole = new GPDomainRole(Role);
        //testRole.onAfterInsert();
        
    }
    @isTest
    public static void testgetUserEmailsForRole()
    {
        GPServiceRole testRole = new GPServiceRole();
        set<Id> SetofRoleid = new Set<id>();
        list<GP_Role__c> listOfId = [Select id from GP_Role__c limit 5];
        for(GP_Role__c EachRole : listOfId)
        {
            SetofRoleid.add(EachRole.id);
            
        }
        testRole.getUserEmailsForRole(SetofRoleid);  
        GPServiceRole.insertGroup(SetofRoleid);
    }
    public static GP_Pinnacle_Master__c GetpinnacleMasterforTrigger()
    {
        GP_Pinnacle_Master__c objpinnacleMaster1 = new GP_Pinnacle_Master__c();
        objpinnacleMaster1.RecordTypeId = Schema.SObjectType.GP_Pinnacle_Master__c.getRecordTypeInfosByName().get('Customer L4').getRecordTypeId();
        objpinnacleMaster1.Name = 'Test Pinnacle Master';
        // objpinnacleMaster.GP_Company__c = 'Saasfocus.inc';
        //objpinnacleMaster1.GP_Description__c ='Test Description';
        objpinnacleMaster1.GP_Status__c = 'Active';
        return objpinnacleMaster1;
    }
    
    /*   public static GP_Role__c getRoleForTrigger(GP_Work_Location__c objSDO1 ,GP_Pinnacle_Master__c objpinnacleMaster1)
    {   
        GP_Role__c objrole = new GP_Role__c();
        objrole.Name = 'Testrole';
        objrole.GP_Active__c = True;
        // objrole.GP_HSL_Master__c = objpinnacleMaster1.id;
        //objrole.GP_Delivery_Org__c= 'xyz';
        //objrole.GP_Work_Location_SDO_Master__c = objSDO.id;
        objrole.GP_Type_of_Role__c = 'Customer L4';
        objrole.GP_Customer_L4__c = objAccount.id;
        objrole.GP_Access__c = 'Read';
        //objrole.GP_Role_Category__c = 'PID Creation';
        Return objrole;
        }
        public static GP_Work_Location__c getWorkLocation(){
        GP_Work_Location__c objSDO1 = new GP_Work_Location__c();
        objSDO1.name = 'TestWL & SDO Master';
        objSDO1.GP_Status__c = 'No';
        return objSDO1;
    }*/
}