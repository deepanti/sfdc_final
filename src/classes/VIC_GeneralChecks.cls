/*
    This class is used as common in other apex classes & used to perform common functions.
    --------------------------------------------------------------------------------------
    Version#     Date                           Author                    Description
    --------------------------------------------------------------------------------------
    1.0          23-May-2017                   Rishabh Pilani            Initial Version
    --------------------------------------------------------------------------------------
*/
public class VIC_GeneralChecks{

    /*
        This method is used to check null and if null then set 0.0.
    */
    public static Decimal CheckNull(Decimal val){
        if(val != null)
            return val;
        else
            return 0.0;
    }
    /*
        This method is used to check null and if null then set 0.
    */
    public static integer CheckNull(integer val){
        if(val != null)
            return val;
        else
            return 0;
    }
    
    /*
        This method is used to check null and if null then set blank.
    */
    public static string CheckNull(string val){
        if(val != null)
            return val;
        else
            return '';
    }
    
    /*
        This method is used to check null for Decimal value.
    */
    public static boolean IsNotNull(Decimal val){
        return val!=null;
    }
    
    /*
        This method is used to check null for DateTime value.
    */
    public static boolean IsNotNull(DateTime val){
        return val!=null;
    }
    
    /*
        This method is used to check null for Date value.
    */
    public static boolean IsNotNull(Date val){
        return val!=null;
    }
    
    /*
        This method is used to check null for integer value.
    */
    public static boolean IsNotNull(integer val){
        return val!=null;
    }
    
    /*
        This method is used to check null for string value.
    */
    public static boolean IsNotNull(string val){
        return val!=null;
    }
    
    /*
        This method is used to check null for any sobject.
    */
    public static boolean IsNotNull(sObject sObj){
        return sObj !=null;
    }
    
    /*
        This method is used to check null for list of any sobject.
    */
    public static boolean IsNotNull(List<sObject> lstSObj){
        return lstSObj !=null && lstSObj.size() >0;
    }
    
    /*
        This method is used to check null & greater then 0 of decimal value.
    */
    public static boolean IsNotNullGreaterThenZero(Decimal val){
        return val!=null && val > 0;
    }
    
    /*
        This method is used to check null & greater then 0 of integer value.
    */
    public static boolean IsNotNullGreaterThenZero(integer val){
        return val!=null && val > 0;
    }
    
    /*
        This method is used to check null & blank of string value.
    */
    public static boolean IsNotNullAndBlank(string val){
        return val!=null && String.isNotBlank(val);
    }
    
    
    /*
        This method is used to get percentage value.  
    */
    public static Decimal calcValueAsPercentage(Decimal percent, Decimal amount) { 
        return Math.round((percent/100) * amount);
    } 
    
    
    /*
        THIS METHOD IS USED FOR CHECK THE VALUE PERCENTAGE OF
    */
    public static Decimal GetPercentageOfResult(Decimal mainValue, Decimal percentageResultValue){      
        return percentageResultValue / (mainValue / 100);
    }
    
    /*
     * @param: String
     * @return: Boolean
     * @description: This method test if string can be converted to decimal
    */
    public static Boolean isDecimal(String dec){
        Boolean result = true;
        try{
            Decimal.valueOf(dec);
        }
        catch(Exception e){
            result = false;
        }
        return result;
    }
}