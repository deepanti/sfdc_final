@isTest
private class GPDomainPinnacleMasterTracker {
	@isTest
    private static void testPinnacleMasterDomain() {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_pinnacle_master__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        GP_Pinnacle_Master__c pinnacleMaster = new GP_Pinnacle_Master__c();
        pinnacleMaster.GP_Record_Type_Name__c = 'HSL Master';
        insert pinnacleMaster;
    }
}