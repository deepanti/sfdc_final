/**
 * @group ProjectBudgetandPricing
 * @group-content ../../ApexDocContent/ProjectBudgetandPricing.htm
 *
 * @description Apex Service for project Budget and Pricing module,
 *              has aura enabled method to fetch, update/create and delete project budget data.
 */
public class GPCmpServiceProjectBudgetandPricing {

    /**
     * @description Returns Project budget data for a given project Id
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse json of Project budget data
     * 
     * @example
     * GPCmpServiceProjectBudgetandPricing.getProjectBudgetData(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getProjectBudgetData(Id projectId) {
        GPControllerProjectBudgetandPricing budgetController = new GPControllerProjectBudgetandPricing(projectId);
        return budgetController.getBudgetRecord();
    }

    /**
     * @description Saves Budget data
     * @param strListOfProjectbudget serialized list of project budget records to be inserted/updated
     * 
     * @return GPAuraResponse json of Falg whether the records are successfully inserted
     * 
     * @example
     * GPCmpServiceProjectBudgetandPricing.saveProjectbudgetData(<Serialized list of project budget record>);
     */
    @AuraEnabled
    public static GPAuraResponse saveProjectbudgetData(String strListOfProjectbudget,String strListOfProjectBudgetToDelete) {
        List < GP_Project_Budget__c > listOfProjectBudget = (List < GP_Project_Budget__c > ) JSON.deserialize(strListOfProjectbudget, List < GP_Project_Budget__c > .class);
        List < GP_Project_Budget__c > listOfProjectBudgetToDelete = strListOfProjectBudgetToDelete != null && strListOfProjectBudgetToDelete != '' ? (List < GP_Project_Budget__c > ) JSON.deserialize(strListOfProjectBudgetToDelete, List < GP_Project_Budget__c > .class) : null;
        
        GPControllerProjectBudgetandPricing budgetController = new GPControllerProjectBudgetandPricing(listOfProjectBudget,listOfProjectBudgetToDelete);
        return budgetController.saveProjectBudgetRecord();
    }

    /**
     * @description Deletes Project budget data for a given projectbudgetId Id
     * @param projectbudgetId Salesforce 18/15 digit Id of project budget
     * @return GPAuraResponse json of Falg whether the records are successfully deleted
     * 
     * @example
     * GPCmpServiceProjectBudgetandPricing.deleteProjectBudgetRecord(<18 or 15 digit project budget Id>);
     */
    @AuraEnabled
    public static GPAuraResponse deleteProjectbudgetData(Id projectbudgetId) {
        GPControllerProjectBudgetandPricing budgetController = new GPControllerProjectBudgetandPricing();
        return budgetController.deleteProjectBudgetRecord(projectbudgetId);
    }

    /**
     * @description Returns Project budget data for a given project Id from OMS
     * @param projectId Salesforce 18/15 digit Id of project
     * 
     * @return GPAuraResponse json of Project budget data
     * 
     * @example
     * GPCmpServiceProjectBudgetandPricing.getProjectBudgetData(<18 or 15 digit project Id>);
     */
    @AuraEnabled
    public static GPAuraResponse getOMSProjectBudgetData(Id projectId) {
        GPControllerProjectBudgetandPricing budgetController = new GPControllerProjectBudgetandPricing(projectId);
        return budgetController.getOMSBudgetRecord();
    }
}