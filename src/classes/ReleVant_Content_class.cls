public class ReleVant_Content_class{

        Public string Vertical='';
        Public Opportunity oppty{get; set;}
      Public List<items_Sharepoint_Files__x> relevent_item{get; set;}
        Public List<items_Sharepoint_Files__x> relevent_Service_line{get; set;}
        Public List<items_Sharepoint_Files__x> relevent_product_family{get; set;}
        Public List<items_Sharepoint_Files__x> relevent_nature_of_work{get; set;}
        Public List<items_Sharepoint_Files__x> relevent_competitors{get; set;}
                Public List<Expert__c> relevent_Internal_expert {get; set;}
                Public List<Expert__c> relevent_External_expert {get; set;}
                Public Boolean RCExternal_expert{get; set;}
                Public Boolean RCInternal_expert{get; set;}
        public string ExtItem_id {get; set;} 
        public String Commentbody {get;set;}
        public String recordSearchString {get; set;}
        Public List<List<items_Sharepoint_Files__x>> relevent_All = new List<List<items_Sharepoint_Files__x>>();
        Public List<items_Sharepoint_Files__x> Show_all_search {get; set;}
        Public Boolean Search_header{get; set;}
        
        
        public string name{get; set;}
        public string display{get; set;}  
        Public string sharepoint_URL{get; set;}
        Public string sharepoint_URL_modal{get; set;}
        Public Boolean Check_mob {get; set;}
        Public Boolean Check_web {get; set;}
        Public Boolean Check_web_lightning {get; set;}
        Public Boolean Search_all_Content {get; set;}
        Public Boolean bool_product_family{get; set;}
        Public Boolean bool_Service_line{get; set;}
        Public Boolean bool_Competitor{get; set;}
        Public String ExternalExpert{get; set;}
        Public items_Sharepoint_Files__x Extobj{get; set;}
        public Sharing_SP__c taggedSP{ get; set; }

        Public ReleVant_Content_class(ApexPages.StandardController controller)
        {
               Check_mob=false;
               Check_web =false;
               Check_web_lightning =false;
               
            oppty =[select id,Competitor__c,Name,Industry_Vertical__c,Service_Line__c,Product_Family__c,Nature_Of_Work__c,Product_Information__c,Sales_Region__c from opportunity where id= :Controller.getrecord().id];
        
            //retrieve_items();
           if(Userinfo.getUiThemeDisplayed().equals('Theme4t') )
           {
               Check_mob= true;
               
           }
           else if(Userinfo.getUiThemeDisplayed().equals('Theme3'))
           {
               Check_web =true;
           }
           else if(Userinfo.getUiThemeDisplayed().equals('Theme4d'))
           {
               Check_web_lightning =true;
           }
           else
           {
               Check_mob=false;
               Check_web =false;
               Check_web_lightning =false;
               
           }
           
           Commentbody ='';
           RCExternal_expert=false;
           RCInternal_expert=false;
           Search_header=false;
           Search_all_Content =false;
           recordSearchString ='';
           bool_product_family=false;
           bool_Service_line=false;
           bool_Competitor=false;
            
        }
        
        Public void retrieve_items()
        {
        
            If(oppty.Industry_Vertical__c != Null )
            {
                String Key = oppty.Industry_Vertical__c;
                
                relevent_item = new list<items_Sharepoint_Files__x>();
                                
                string query='select Name__c,UpdateDate__c,IsFolder__c,Industry_Vertical__c,ContentLength__c,DisplayUrl,DownloadUrl__c,Asset_Type__c,Data_Classification__c,ValidUpto__c,Rating__c,Number_of_Rating__c from items_Sharepoint_Files__x  where IsFolder__c = false AND ( Industry_Vertical__c  like  \''+'%' + Key +'%' +'\'  ) ORDER BY UpdateDate__c DESC limit 500'  ;
                
                
                relevent_item = database.query(query);
                
                 
                
                
                //retrieve_items_Nature_of_Work();
                
            }
        
            retrieve_items_product_family();
        
        }
        
        Public void retrieve_items_service_line()
        {
        
            If(oppty.Service_Line__c != Null )
            {
                String Key = oppty.Service_Line__c;
                
                relevent_Service_line = new list<items_Sharepoint_Files__x>();
                
                String strng='';       
                
                List<String> Splitstr=key.split(',');
                
                for(String str : Splitstr){
                        String c;
                        
                        c= 'Service_Line__c like  \''+'%' + str +'%' +'\'OR ';
                        
                        strng = strng +c;
                
                }
                
                strng=strng.substring(0,(strng.length()-3));
                
                                         
                string query='select Name__c,UpdateDate__c,Industry_Vertical__c,ContentLength__c,DisplayUrl,DownloadUrl__c,Comment__c,Asset_Type__c,Data_Classification__c,ValidUpto__c,IsFolder__c,Rating__c,Number_of_Rating__c from items_Sharepoint_Files__x  where IsFolder__c = false AND ( ' + strng  + ' ) ORDER BY UpdateDate__c DESC limit 500';
                
                relevent_Service_line = database.query(query);
                        
                If(relevent_Service_line.isempty()){
                    bool_Service_line=false;
                }
                else{
                    bool_Service_line=true;
                }
                
                
                // System.debug(SplitStr.size());
                
            }
        
        
            retrieve_items_competitors();
        }
        
         Public void retrieve_items_product_family()
        {
        
            If(oppty.Product_Family__c != Null )
            {
                String Key = oppty.Product_Family__c;
                String strng='';       
                relevent_product_family = new list<items_Sharepoint_Files__x>();
                List<String> Splitstr=key.split(',');
                
                for(String str : Splitstr){
                        String c;
                        
                        c= 'Product_Family__c like  \''+'%' + str +'%' +'\'OR ';
                        
                        strng = strng +c;
                
                }
                
                strng=strng.substring(0,(strng.length()-3));   
                System.debug('Where clause' + strng);     
                string query='select Name__c,MimeType__c,UpdateDate__c,Industry_Vertical__c,ContentLength__c,DisplayUrl,DownloadUrl__c,Asset_Type__c,Data_Classification__c,ValidUpto__c,Rating__c,Number_of_Rating__c from items_Sharepoint_Files__x where IsFolder__c = false AND ( '  + strng + ' ) ORDER BY UpdateDate__c DESC limit 500' ;
                System.debug('Query' + query);
                relevent_product_family = database.query(query);
                
                If(relevent_product_family.isempty()){
                    bool_product_family=false;
                }
                else{
                    bool_product_family=true;
                }
                
                 
                
            }
        
            retrieve_items_service_line();    
        
        }
        
        /*Public void retrieve_items_Nature_of_Work()
        {
        
            If(oppty != Null )
            {
                String Key = oppty.Nature_of_Work__c;
                
                relevent_nature_of_work = new list<items_Sharepoint_Files__x>();
                                
                string query='select Name__c,IsFolder__c,MimeType__c,UpdateDate__c,Industry_Vertical__c,ContentLength__c,DisplayUrl,DownloadUrl__c,Comment__c from items_Sharepoint_Files__x  where IsFolder__c=false And ( Nature_of_Work__c like  \''+'%' + Key +'%' +'\' )  ORDER BY UpdateDate__c DESC '  ;
                relevent_nature_of_work = database.query(query);
                
                 
                
            }
        
        
        
        }*/
        
        Public void retrieve_Internal_Experts()
        {
        
            If(oppty.Industry_Vertical__c != Null && oppty.Service_Line__c !=Null )
            {
                String Key = oppty.Industry_Vertical__c;
                
                String SL = oppty.Service_Line__c;
                
                String strng='';       
                if(oppty.Service_Line__c != Null)
                {        List<String> Splitstr = SL.split(',');
                        
                        for(String str : Splitstr){
                                String c;
                                
                                //c= 'Service_Line__c INCLUDES (' + str + ')' + ' OR ';
                                c = '\'' + str + '\'' + ',';
                                
                                strng = strng +c;
                        
                        }
                        
                        
                
                strng=strng.substring(0,(strng.length()-1));
                System.debug('Where clause' + strng); 
                }
                else
                {
                    strng='\''+'\'';
                }
                relevent_Internal_expert = new list<Expert__c>();
                                
                string query ='select Id,Name,Industrial_Vertical__c,Service_Line__c,Product__c,Current_Role__c,Geo_Served__c,Geo_served_Internal__c,SDO__c,Product_Family__c,Expert_Type__c,Ask_me_about__c,Expert_Role__c,Expertise_Area__c,Expert_Id__c from Expert__c where Expert_Type__c =\'Internal\' AND Designated_Expert__c =\'Yes\' AND  (Service_Line__c INCLUDES(' + strng +   ') AND Industrial_Vertical__c INCLUDES (' + '\'' +Key+ '\'' +') ) ORDER BY LastModifiedDate DESC limit 500' ;
                relevent_Internal_expert = database.query(query);
                
                
                if (relevent_Internal_expert.isempty())
                   {     RCInternal_expert=false;
                   }
                    else
                       {  RCInternal_expert=true;   
                       }
                                             
            }
        
            retrieve_External_Experts();
        
        }
        
        Public void retrieve_External_Experts()
        {
        
            If(oppty.Industry_Vertical__c != Null )
            {
                String Key = oppty.Industry_Vertical__c;
                
                String SL = oppty.Service_Line__c ;
                
                String strng='';       
                if(oppty.Service_Line__c != Null)
                {
                List<String> Splitstr = SL.split(',');
                
                for(String str : Splitstr){
                        String c;
                        
                        //c= 'Service_Line__c INCLUDES (' + str + ')' + ' OR ';
                        c = '\'' + str + '\'' + ',';
                        
                        strng = strng +c;
                
                }
                
                strng=strng.substring(0,(strng.length()-1));
                System.debug('Where clause' + strng); 
                }
                Else
                {
                    strng='\''+'\'';
                
                }
                relevent_External_expert = new list<Expert__c>();
                                
                string query ='select Id,Name,Industrial_Vertical__c,Service_Line__c,Product__c,Current_Role__c,Geo_served_Internal__c,SDO__c,Product_Family__c,Expert_Type__c,Ask_me_about__c,Expert_Role__c,Geo_Served__c,Past_Complanies_Served__c,Expertise_Area__c,Expert_Id__c from Expert__c where Expert_Type__c =\'External\' AND (Service_Line__c INCLUDES(' + strng +   ') AND Industrial_Vertical__c INCLUDES (' + '\'' +Key+ '\'' +') ) ORDER BY LastModifiedDate DESC limit 500' ;
                relevent_External_expert = database.query(query);
                
                if (relevent_External_expert .isempty())
                   {     RCExternal_expert=false;
                   }
                    else
                       {  RCExternal_expert=true;   
                       }                             
            }
        
        
                
        }
        
        
        Public void retrieve_items_competitors()
        {
        
            If(oppty.Competitor__c != Null )
            {
                String Key = oppty.Competitor__c;
                
                relevent_competitors = new list<items_Sharepoint_Files__x>();
                
                String strng='';       
                
                List<String> Splitstr=key.split(';');
                
                for(String str : Splitstr){
                        String c;
                        
                        c= 'Competitor_Name__c like  \''+'%' + str +'%' +'\'OR ';
                        
                        strng = strng +c;
                
                }
                
                strng=strng.substring(0,(strng.length()-3));
                                
                string query='select id,Name__c,MimeType__c,UpdateDate__c,Industry_Vertical__c,ContentLength__c,DisplayUrl,DownloadUrl__c,Comment__c,Asset_Type__c,Data_Classification__c,ValidUpto__c,Rating__c,Number_of_Rating__c from items_Sharepoint_Files__x  where IsFolder__c=false AND ( ' + strng + ' ) ORDER BY UpdateDate__c DESC limit 500';
                relevent_competitors = database.query(query);
                
                 If(relevent_competitors.isempty()){
                    bool_Competitor=false;
                }
                else{
                    bool_Competitor=true;
                }
                
            }
            
                retrieve_Internal_Experts();
                
        }
        
        
        Public PageReference retrieve_All()
        {    
            try{
            
            
        
            If(oppty != Null )
            {
                Show_all_search = new List<items_Sharepoint_Files__x>();
                String Key =  this.recordSearchString;
                
                system.debug('SearchString::'+ Key);
                
                    if (Key == null) {
                        return null; 
                    }
                    Else
                    {
                                /*
                                    Generate a list of keywords for the from the input searchString
                                    Two key variables:
                                        Set<String> keywordSearchSet - Set containing the keywords
                                        String keywordsSearchString - String containing the keywords separated by OR for SOSL
                                */
                                // List of ignored keywords
                                Set<String> stopWords = new Set<String>{'a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 
                                    'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 
                                    'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 
                                    'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 
                                    'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 
                                    'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight',
                                     'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 
                                     'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 
                                     'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 
                                     'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 
                                     'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 
                                     'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 
                                     'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 
                                     'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 
                                     'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed',
                                      'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some',
                                    'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 
                                    'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 
                                    'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 
                                    'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 
                                    'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein',
                                     'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with',
                                      'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the'};
                                // Replace special characters with spaces
                                //  spaces are used in place of removing the characters in case there is a string like "test-string")
                                String cleanKeywords = Key.replaceAll('[\\p{Punct}]',';');
                                system.debug('cleanKeywords::'+cleanKeywords);
                                // Put the space separated keywords into a List for manipulation
                                List<String> workingKeywordList = new List<String>();
                                workingKeywordList = cleanKeywords.split(';');
                                  system.debug('workingKeywordList::'+workingKeywordList);
                                Set<String> keywordSearchSet = new Set<String>();
                                String keywordSearchString = '';
                                // Add the keywords ot the Set and string skipping stop words and blanks
                                for (String s : workingKeywordList) {
                                    if (!stopWords.contains(s) && !s.isWhiteSpace()) {
                                        keywordSearchSet.add(s.toLowerCase());
                                        keywordSearchString += s + ' ';
                                    }
                                }
                                // Strip the trailing ' OR '
                                keywordSearchString = keywordSearchString.removeEnd(' ');
                                
                                if (keywordSearchString.length() > 2) {
                                
                                            //relevent_All =[Find :keywordSearchString in Name fields returning items_Sharepoint_Files__x (Name__c,UpdateDate__c,ContentLength__c,Asset_Type__c,Data_Classification__c,ValidUpto__c where IsFolder__c=false  order by UpdateDate__c DESC limit 500)] ;
                                            String qry = 'select id,DownloadUrl__c,Name__c,UpdateDate__c,ContentLength__c,Asset_Type__c,Data_Classification__c,ValidUpto__c,Rating__c,Number_of_Rating__c from items_Sharepoint_Files__x  where IsFolder__c=false AND (Name__c like \'%' + keywordSearchString + '%\' OR Industry_Vertical__c like \'%'  + keywordSearchString +'%\' OR  Product_Family__c Like \'%' +keywordSearchString+ '%\' OR Service_Line__c like \'%' +keywordSearchString+'%\' OR Competitor_Name__c Like \'%' + keywordSearchString + '%\' ) ORDER BY UpdateDate__c DESC limit 100';
                                            system.debug('Search all query'+qry);
                                            Show_all_search=database.query(qry);
                                            
                                            
                                           /* items_Sharepoint_Files__x  [] objArray = ((List<items_Sharepoint_Files__x >)relevent_All[0]);
                                            
                                            for(integer i=0; i<objArray .size();i++)
                                            {
                                                Show_all_search.add(objArray[i] );    
                                            
                                            }
                                            system.debug('Array Count'+ objArray.size() );*/
                                            
                                            if(Show_all_search.isempty())
                                            {
                                                Search_all_Content =false;
                                            }
                                            Else
                                            {
                                                Search_all_Content =true;
                                            }

                                        return null;
                                
                                }
                    
                            return null;
                    }
                         
            }            
            
            
            
            return null;}
            
            catch(exception e){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'myMsg'));
            return null;
            
            }
        }
        
        Public string Sub_String(string Input)
        {
                String Res='';
                Res = Input.substringafter('.');
                return Res;
        }
        
        
        
    public void postFileToFeed() {
        // Build the Feed post of type ContentPost referencing the existing Content
        
        
        taggedSP = new Sharing_SP__c ();
        taggedSP.Opportunity__c=oppty.id;
          if(!Test.isRunningTest()) {
                        Extobj=[select Name__c,ExternalId,DisplayUrl,DownloadUrl__c from items_Sharepoint_Files__x where id=:ExtItem_id];
                
                system.debug('externalId' + ExtItem_id);
                system.debug('SOQL_result'+ Extobj.ExternalId);
        
        taggedSP.Sharepoint_files__c=Extobj.ExternalId;
        taggedSP.External_file_Id__c=ExtItem_id;
        taggedSP.External_File_Name__c=Extobj.Name__c;
        }
        
        if (Commentbody != null && Commentbody != '') {
                    string key= this.Commentbody;
                    taggedSP.Comments__c=key;
                }
        
        Insert taggedSP;
        
        List< CollaborationGroupRecord > cgid = [Select CollaborationGroupId FROM CollaborationGroupRecord where RecordId=:oppty.id];
        List<FeedItem> Post_to_groups =new List<FeedItem>();
        if(cgid.isempty())
        {
                FeedItem contentPost = new FeedItem();
                contentPost.ParentId = oppty.id;
                contentPost.Type = 'LinkPost';
                contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + ExtItem_id; 
                System.Debug('Commentbody: ' + Commentbody );
                if (Commentbody != null && Commentbody != '') {
                    string key= this.Commentbody;
                    contentPost.Body = key;
                }

                // Attempt to create the Feed post
                try {
                    insert contentPost;
                } catch (DmlException e) { System.debug(LoggingLevel.ERROR, e.getMessage()); }

       }
       else{
                
                for(CollaborationGroupRecord gpid: cgid)
                {
                        FeedItem contentPost = new FeedItem();
                        contentPost.ParentId = gpid.CollaborationGroupId;
                        contentPost.Type = 'LinkPost';
                        contentPost.LinkUrl = 'https://genpact.my.salesforce.com/' + ExtItem_id;    
                        
                        if (Commentbody != null && Commentbody != '') {
                            string key= this.Commentbody;
                            contentPost.Body = key;
                        }
                
                        Post_to_groups.add(contentPost);
                        
                
                }

                if(!Post_to_groups.isempty())
                {
                    Insert Post_to_groups;
                    
                
                }
       
       }
       Commentbody ='';
       
    }  
    
     public pageReference showdoc()
     {
             
                                                    PageReference pageRef = new PageReference('/' +ExtItem_id);
                                                    pageRef.setRedirect(true);
                                                    return pageRef; 
                                                
                                             
                                      
     
     } 
     
             
    public pageReference Shareonpage()
     {      
     
            PageReference pageRef = new PageReference('/apex/Sharetopage?Extrnlid=' + ExtItem_id+'&Opid='+oppty.id);
            pageRef.setRedirect(true);
            return pageRef; 
     }
     
    
     
     Public Void Reset()
     {
         Show_all_search = new List<items_Sharepoint_Files__x>();
     
     }
     
     Public pageReference Requetsanexpert()
     {
         string requesturl='https://genpactonline.sharepoint.com/sites/CFO/GenIE/SitePages/Request%20a%20GenIE.aspx?qs=60';
         ExternalExpert = requesturl + '&opportunityid=' +oppty.id +'&vertical=' +oppty.Industry_Vertical__c +'&Service_Line='+oppty.Service_Line__c  +'&product='+ oppty.Product_Information__c +'&region=' + oppty.Sales_Region__c  ;
         
         PageReference pageRef = new PageReference(ExternalExpert);
         pageRef.setRedirect(true);
         return pageRef;
     }
     
     
     

}