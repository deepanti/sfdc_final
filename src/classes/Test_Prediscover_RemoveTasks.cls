@isTest
public class Test_Prediscover_RemoveTasks 
{
    
    static testMethod void method1()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser2016@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standardusergg2016@testorg.com');
        insert u;
        Sales_Unit__c salesunitobject=new Sales_Unit__c(name='test',Sales_Leader__c=u.id);
        test.startTest();
        insert salesunitobject;
        account accountobject=new account(name='test',Industry_Vertical__c='BFS',Sub_Industry_Vertical__c='BFS',Sales_Unit__c=salesunitobject.id,Account_Owner_from_ACR__c=u.id);
        insert accountobject;
        
        opportunity opp=new opportunity(name='1234',StageName=' Prediscover',CloseDate=system.today(),accountid=accountobject.id, NextStep='test');
        insert opp;
        
        
        opportunity opp1=new opportunity(name='12345',StageName='Prediscover',CloseDate=system.today(),accountid=accountobject.id);
        insert opp1;
        
        opp1.NextStep = 'test2';
        update opp1;
        Task tk= new Task(Subject='Next Step Note', WhatId=opp1.Id, Status__c='Not Started');
        insert tk;
        
        List<task> task_lst = new  List<task>();
        task_lst = [SELECT id,Subject, Status  FROM task WHERE whatid=:opp1.id];                     
        //task_lst[0].Status = 'Completed';
        //upsert task_lst; 
        
        system.debug(' checking a task 1 '+task_lst );
        
        List<task> task_lst2 = new  List<task>();
        task_lst2 = [SELECT id,Subject, Status  FROM task WHERE whatid=:opp1.id];      
        
        opp1.NextStep = 'test3';
        update opp1;
        system.debug(' checking task 2 '+task_lst2 ); 
        //system.assertEquals(task_lst2.size(), 1);
        
        
        
        test.stopTest();
        
        
        
    }
    
}