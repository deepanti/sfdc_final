public class OnchageSuperSl {
    
    
    public static void UpdateSuperSl( Map<ID, Account> new_acc_map, Map<ID, Account> old_acc_map) {
        set<id> accId = new set<id>();
        
        Boolean slchangeflag = false;
        Set<id> salesLeaderId = new set<id>();
        
        try{
            system.debug(':--test onchange--:');
            for(Account acc : new_acc_map.values()){
                accId.add(acc.Id);
                salesLeaderId.add(old_acc_map.get(acc.Id).Sales_Leader_User_Id__c);
                salesLeaderId.add(old_acc_map.get(acc.Id).Client_Partner__c);
                if(acc.OwnerId != old_acc_map.get(acc.Id).ownerID || acc.Sales_Leader_User_Id__c != old_acc_map.get(acc.Id).Sales_Leader_User_Id__c || acc.Client_Partner__c != old_acc_map.get(acc.Id).Client_Partner__c){
                    slchangeflag = true; 
                    system.debug(':---slchangeflag---:'+slchangeflag);
                } 
            }
            if(True){
                system.debug(':---onchange sl or supersl or owner--:');
                Set<ID> toBeDeletedSplit = new Set<ID>();
                Map<Id,opportunityLineItem> BdrepMap = new Map<id,opportunityLineItem>();
                List<OpportunityTeamMember> insertTeamMember = new list<OpportunityTeamMember>();
                List <OpportunitySplit> splitToInsert = new List<OpportunitySplit>();
                map<id,user> umap = new map<id,user>([Select id from User where IsActive = true]);
                Map<id,opportunity> oppMap =new Map<id,opportunity>([SELECT id,OwnerId, amount, AccountId, Account.Client_Partner__c,StageName,account.Sales_Unit__c,account.Sales_Leader_User_Id__c,(SELECT id, UserId, OpportunityId, TeamMemberRole FROM OpportunityTeamMembers ),(SELECT Id, OpportunityID,Product_BD_Rep__c from opportunityLineItems) FROM Opportunity WHERE AccountId in :new_acc_map.keyset() AND IsClosed = False]);
                system.debug('opportunity=='+oppMap.keyset().size());
                list<opportunityTeamMember> deleteOppTeam = New List<opportunityTeamMember>();
                list<opportunityTeamMember> oppteamlist = new list<opportunityTeamMember>();
                List<opportunityLineItem> oliList = new List<opportunityLineItem>();
                Map<id,opportunityLineItem> oliMap = new map<id,OpportunityLineItem>();
               set<id> oppOwnerSet = new set<id>();
                for(opportunity opp : oppMap.values()){
                    oppOwnerSet.add(opp.OwnerId);
                    oppteamlist.addAll(opp.opportunityTeamMembers);
                    oliList.addAll(opp.OpportunityLineItems);
                }
                set<string> BdRepSet = new set<string>();
                for(opportunityLineItem oli : oliList){
                    BdRepSet.add(oli.Product_BD_Rep__c);
                    oliMap.put(oli.opportunityId,oli);
                }
                system.debug(':---salesLeaderId---:'+salesLeaderId +':---oppOwnerSet---:'+oppOwnerSet.size()+':---oppOwnerSet---:'+oppOwnerSet);
                system.debug(':---BdRepSet---:'+BdRepSet.size()+':---BdRepSet---:'+BdRepSet);
                system.debug(':---oppteamlist--:'+oppteamlist.size() +':--oppteamlist--:'+oppteamlist);
                for(opportunityTeamMember team : oppteamlist){
                    if(team.UserId != oppMap.get(team.opportunityId).OwnerId){
                        opportunityLineItem olist = oliMap.get(team.OpportunityId);
                        if(olist != null){
                            if(team.UserId == oliMap.get(team.opportunityId).Product_BD_Rep__c || salesLeaderId.contains(team.UserId) || team.TeamMemberRole == 'SL' || team.TeamMemberRole == 'Super SL' ){
                                deleteOppTeam.add(team);
                            }  
                        } else if(salesLeaderId.contains(team.UserId) || team.TeamMemberRole == 'SL' || team.TeamMemberRole == 'Super SL' ){
                            deleteOppTeam.add(team);
                        }    
                    }
                }
                Id opportunity_split_type = system.Label.OpportunitySplitType;
                
                List<OpportunitySplit> OppSplitList= [SELECT id FROM OpportunitySplit WHERE OpportunityID IN :oppMap.keyset() AND splitTypeId =: opportunity_split_type];
                system.debug(':--OppSplitList---:'+OppSplitList.size());
                if(OppSplitList.size() > 0 ){                  
                    delete OppSplitList;
                }
                system.debug(':--deleteOppTeam---: '+deleteOppTeam.size()+':----deleteOppTeam----:'+deleteOppTeam);
                if(deleteOppTeam.size() > 0){
                    Delete deleteOppTeam;
                }
                
                
                for(opportunity opp : oppMap.values()){
                    if(umap.containsKey(opp.OwnerId)){
                        OpportunitySplit oppty_splitownr = new OpportunitySplit();  
                        oppty_splitownr.SplitPercentage = 100; 
                        oppty_splitownr.SplitTypeId = opportunity_split_type; 
                        oppty_splitownr.SplitOwnerId = opp.OwnerId;
                        oppty_splitownr.OpportunityId = opp.id;
                        splitToInsert.add(oppty_splitownr);
                    }
                    if(opp.OwnerId != new_acc_map.get(opp.AccountId).Client_Partner__c && umap.containsKey(new_acc_map.get(opp.AccountId).Client_Partner__c) || Test.isRunningTest()) {  
                        if(oliMap.containsKey(opp.Id)){
                            OpportunitySplit oppty_split = new OpportunitySplit();   
                            oppty_split.SplitPercentage = 100;    
                            oppty_split.SplitTypeId = opportunity_split_type;  
                            oppty_split.SplitOwnerId = new_acc_map.get(opp.AccountId).Client_Partner__c; 
                            oppty_split.OpportunityId = opp.id;  
                            splitToInsert.add(oppty_split); 
                        }
                        OpportunityTeamMember opp_team_member = new OpportunityTeamMember();    
                        opp_team_member.OpportunityAccessLevel = 'Edit';   
                        opp_team_member.OpportunityId = opp.Id; 
                        opp_team_member.UserId = new_acc_map.get(opp.AccountId).Client_Partner__c; 
                        opp_team_member.TeamMemberRole = 'Super SL'; 
                        insertTeamMember.add(opp_team_member);
                    }
                    if(opp.OwnerId != new_acc_map.get(opp.AccountId).Sales_Leader_User_Id__c && new_acc_map.get(opp.AccountId).Sales_Leader_User_Id__c != new_acc_map.get(opp.AccountId).Client_Partner__c && umap.containsKey(new_acc_map.get(opp.AccountId).Sales_Leader_User_Id__c)) {
                        if(oliMap.containsKey(opp.Id)){
                            OpportunitySplit oppty_split1 = new OpportunitySplit();  
                            oppty_split1.SplitPercentage = 100; 
                            oppty_split1.SplitTypeId = opportunity_split_type; 
                            oppty_split1.SplitOwnerId = new_acc_map.get(opp.AccountId).Sales_Leader_User_Id__c;
                            oppty_split1.OpportunityId = opp.id;
                            splitToInsert.add(oppty_split1);
                        }
                        OpportunityTeamMember opp_team_member1 = new OpportunityTeamMember();  
                        opp_team_member1.OpportunityAccessLevel = 'Edit'; 
                        opp_team_member1.OpportunityId = opp.Id;
                        opp_team_member1.UserId = new_acc_map.get(opp.AccountId).Sales_Leader_User_Id__c;
                        opp_team_member1.TeamMemberRole = 'SL';
                        insertTeamMember.add(opp_team_member1);
                    }  
                }
                system.debug('split to insert  =='+splitToInsert.size());
                system.debug('team to  insert  =='+insertTeamMember.size());
                
                List<AggregateResult> opp_line_item_list = [Select OpportunityId, Product_BD_Rep__c, SUM(UnitPrice) FROM OpportunityLineItem 
                                                            WHERE opportunityId IN : oppMap.keyset() GROUP By Product_BD_Rep__c, OpportunityId];
                if(oliList.size() > 0){
                    for(opportunityLineItem oli : oliList){
                        if(umap.containsKey(oli.Product_BD_Rep__c) && oppMap.get(oli.opportunityId).OpportunityLineItems.Size() > 0){                        
                            IF(oli.Product_BD_Rep__c != oppMap.get(oli.opportunityId).OwnerID &&
                               oli.Product_BD_Rep__c != oppMap.get(oli.opportunityId).Account.Client_Partner__c &&
                               oli.Product_BD_Rep__c != oppMap.get(oli.opportunityId).account.Sales_Leader_User_Id__c ){
                                   OpportunityTeamMember opp_team_member1 = new OpportunityTeamMember();  
                                   opp_team_member1.OpportunityAccessLevel = 'Edit'; 
                                   opp_team_member1.OpportunityId = oli.opportunityId;
                                   opp_team_member1.UserId = oli.Product_BD_Rep__c;
                                   opp_team_member1.TeamMemberRole = 'BD Rep';
                                   insertTeamMember.add(opp_team_member1);
                               }
                        }
                    }
                }
                system.debug('::--team member---:>  '+insertTeamMember.size());
                system.debug('::--Split ---:>'+splitToInsert.size());
               
                if(splitToInsert.size() > 0){
                    insert splitToInsert;
                }
                if(insertTeamMember.size() > 0){ 
                    insert insertTeamMember;
                }
            }            
        } catch (Exception e) {
             CreateErrorLog.createErrorRecord(String.valueOf(accId),e.getMessage(), '', e.getStackTraceString(),'OnchageSuperSl', 'UpdateSuperSl','Fail','',String.valueOf(e.getLineNumber())); 

            system.debug(':---error---: '+e.getMessage()+':---error Line Number--:'+e.getLineNumber());
        }
    }      
}