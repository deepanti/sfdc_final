//================================================================================================================
//  Description: Test Class for GPCmpServiceProjectProfileBillRate
//================================================================================================================
//  Version#     Date                           Author                    Description
//================================================================================================================
//  1.0          15-May-2018             Mandeep Singh Chauhan               Initial Version
//================================================================================================================
@isTest
public class GPCmpServiceProjectProfileBillRateTrKr {
    
    public Static GP_Project_Work_Location_SDO__c objSdo ;
    public Static GP_Project__c parentProject;
    public Static GP_Project__c prjObj;
    public static GP_Billing_Milestone__c objPrjBillingMilestone;
    public static String jsonresp;
    public static GP_Project_Leadership__c projectLeadership;
    public static GP_Profile_Bill_Rate__c objProfileBillRate; 
    
    public static Void buildDependencyData()
    {
        GP_Sobject_Controller__c gettriggerSwitch = new GP_Sobject_Controller__c();
        gettriggerSwitch.name = 'gp_project_budget__c';
        gettriggerSwitch.GP_Enable_Sobject__c = true;
        insert gettriggerSwitch;
        
        User objuser = GPCommonTracker.getUser();
        insert objuser; 
        
        GP_Employee_Master__c empObjwithsfdcuser = GPCommonTracker.getEmployee();
        empObjwithsfdcuser.GP_SFDC_User__c = objuser.id;
        empObjwithsfdcuser.GP_Final_OHR__c = '123456';
        insert empObjwithsfdcuser;
        
        Business_Segments__c objBS = GPCommonTracker.getBS();
        insert objBS ;
        
        Sub_Business__c objSB = GPCommonTracker.getSB(objBS.id);
        insert objSB ;
        
        Account accobj = GPCommonTracker.getAccount(objBS.id,objSB.id);
        insert accobj ;
        
        GP_Work_Location__c objSdo = GPCommonTracker.getWorkLocation();
        objSdo.GP_Status__c = 'Active and Visible';
        objSdo.RecordTypeId = Schema.SObjectType.GP_Work_Location__c.getRecordTypeInfosByName().get('SDO').getRecordTypeId();
        insert objSdo;
        
        GP_Pinnacle_Master__c objpinnacleMaster = GPCommonTracker.GetpinnacleMaster();
        insert objpinnacleMaster ;
        
        GP_Role__c objrole = GPCommonTracker.getRole(objSdo,objpinnacleMaster );
        objrole.GP_Work_Location_SDO_Master__c = objSdo.Id;
        objrole.GP_HSL_Master__c = null;
        objrole.GP_Role_Category__c = 'PID Creation';
        insert objrole;
        
        GP_User_Role__c objuserrole = GPCommonTracker.getUserRole(objrole ,objuser);
        insert objuserrole;
        
        //GP_Employee_Type_Hours__c  empHours = GPCommonTracker.getEmployeeHoursCustomSetting();
        //insert empHours ;  
        
        GP_Timesheet_Transaction__c  timesheettrnsctnObj = GPCommonTracker.getTimesheetTransaction(empObjwithsfdcuser);
        insert timesheettrnsctnObj;
        
        GP_Deal__c dealObj = GPCommonTracker.getDeal();
        insert dealObj ;
        
        GP_Icon_Master__c iconMaster = GPCommonTracker.getIconMaster();
        insert iconMaster;
        
        GP_Project_Template__c objprjtemp = GPCommonTracker.getProjectTemplate();
        objprjtemp.GP_Type__c = 'CMITS';
        objprjtemp.GP_Active__c = true;
        insert objprjtemp ;
        
        prjobj = GPCommonTracker.getProject(dealObj,'CMITS',objprjtemp,objuser,objrole);
        prjobj.OwnerId=objuser.Id;
        prjobj.GP_CRN_Number__c = iconMaster.Id;
        prjobj.GP_Operating_Unit__c = objpinnacleMaster.Id;
        prjobj.GP_CRN_Status__c ='Signed Contract Received';
        prjobj.GP_Next_CRN_Stage_Date__c =  system.now().addHours(2);
        prjobj.GP_Billing_Currency__c = 'AUD';
        prjobj.GP_Delivery_Org__c = 'CMITS';
        prjobj.GP_Sub_Delivery_Org__c = 'ITO';
        insert prjobj;
        
        GP_Budget_Master__c objBudjet = GPCommonTracker.getBudgetMaster();
        insert objBudjet;
        
        GP_Project_Budget__c objPrjBudjet1 = GPCommonTracker.getProjectBudget(prjobj.id,objBudjet.id);
        insert objPrjBudjet1;
        
        GP_Project_Budget__c objPrjBudjet = GPCommonTracker.getProjectBudget(prjobj.id,objBudjet.id);
        objPrjBudjet.GP_Parent_Project_Budget__c = objPrjBudjet1.id;
        insert objPrjBudjet;
        update objPrjBudjet;
        
        objProfileBillRate = new GP_Profile_Bill_Rate__c();
        objProfileBillRate.GP_Project__c = prjObj.id;
        insert objProfileBillRate;
    }
    
    @isTest
    public static void testGPCmpServiceProjectProfileBillRate() {
        buildDependencyData();
        GPCmpServiceProjectProfileBillRate.getProjectProfileBillRateData(prjObj.id);
        List<GP_Profile_Bill_Rate__c> listOfProfileBR = new List<GP_Profile_Bill_Rate__c>(); 
        listOfProfileBR.add(objProfileBillRate);
        String str = JSON.serialize(listOfProfileBR);
        GPCmpServiceProjectProfileBillRate.saveProfileBillRate(str);
        GPCmpServiceProjectProfileBillRate.deleteProfileBillRate(objProfileBillRate.id);
        GPControllerProfileBillRate objGPControllerProfileBR = new GPControllerProfileBillRate(prjObj.id);
    }
}