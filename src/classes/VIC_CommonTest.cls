/* this is common class used for All Annual Component Test class */

public class VIC_CommonTest{


  public APXTConga4__Conga_Template__c  initAllcongaData(String congaTemp){
    
    
       APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
       objConga.APXTConga4__Name__c = congaTemp;
      
       return objConga;
      }


   public List<Plan__c> initAllPlanData(string congaTemp){
        List<Plan__c > lstPlan = new List<Plan__c >();
        lstPlan.add(fetchPlanData('IT_GRM','IT GRM',congaTemp,null,'2018'));
        lstPlan.add(fetchPlanData('Capital_Market_GRM','Capital Market GRM',congaTemp,null,'2018'));
        lstPlan.add(fetchPlanData('SL_CP_Enterprise','SL CP Enterprise',congaTemp,null,'2018'));
        lstPlan.add(fetchPlanData('SEM','SEM',congaTemp,null,'2018'));
        lstPlan.add(fetchPlanData('EnterpriseGRM','Enterprise GRM',congaTemp,null,'2018'));
        lstPlan.add(fetchPlanData('BDE','BDE',congaTemp,null,'2018'));
       
        return lstPlan;
    }
    
    

public Plan__c fetchPlanData(string planCode,String planName, String CongaTemp,String CongaBonus,String yearPlan){

        Plan__c plan = new Plan__c();
        plan.vic_Plan_Code__c = planCode;
        plan.Name = planName;
        plan.Is_Active__c =true;
        plan.Year__c =yearPlan;
        plan.Conga_Template_For_Bonus__c =CongaBonus;
        plan.Conga_Template__c=CongaTemp;
        
       
    return plan;    

    }
    
    public  List<Master_Plan_Component__c >initAllMasterPlanComponent(){

    List<Master_Plan_Component__c > masterPlan = new List<Master_Plan_Component__c >();
    masterPlan.add(fetchMasterPlanComponentData('TCV Accelerator','Currency'));
    masterPlan.add(fetchMasterPlanComponentData('OP','Currency'));
    masterPlan.add(fetchMasterPlanComponentData('PM%','%based'));
    masterPlan.add(fetchMasterPlanComponentData('TCV','%based'));
    masterPlan.add(fetchMasterPlanComponentData('MBO','%based'));
    masterPlan.add(fetchMasterPlanComponentData('TCV Intelligent Operations','Currency'));
    masterPlan.add(fetchMasterPlanComponentData('TCV Transformational Services','Currency'));
    

    return masterPlan;

    }
    
    public static Master_Plan_Component__c fetchMasterPlanComponentData(String Name , String pointType){

        Master_Plan_Component__c masterPlanComponentObj = new Master_Plan_Component__c();
        masterPlanComponentObj.Name = Name;
        masterPlanComponentObj.Point_type__c = pointType;
       
       return masterPlanComponentObj ;    

    }
    
    public List<Plan_Component__c> initAllPlanComponent(list<Plan__c> lstPlan , list<Master_Plan_Component__c> lstMasterPlan){
    
        List<Plan_Component__c> lstPlanComponent = new List<Plan_Component__c>();
        for(Plan__c objPlan : lstPlan ){
            for(Master_Plan_Component__c objMPC : lstMasterPlan){
                lstPlanComponent.add(fetchPlanComponentData(objPlan.Id,objMPC.Id));
            }
        }
        return lstPlanComponent;
    }
    
    
    public static Plan_Component__c fetchPlanComponentData(ID masterCompID, ID planID){

        Plan_Component__c planComponentObj = new Plan_Component__c();
        planComponentObj.Master_Plan_Component__c=masterCompID;
        planComponentObj.Plan__c=planID;
        
       return planComponentObj;    

    }
    
    
    public Target__c fetchTargetData(decimal targetBonus, ID planID, Decimal tcvIO,Decimal tcvTS, String UserID,Date startDate){

        Target__c target = new Target__c();
        target.Target_Bonus__c = targetBonus;
        target.Plan__c = planID;
        target.User__c= UserID;
        target.vic_TCV_TS__c=tcvTS;
        target.vic_TCV_IO__c=tcvIO;
        target.Start_date__c = startDate;
        
        
       return target;    

    }
    
   
    
    
    
    public Target_Component__c  fetchTargetComponentData(decimal weightage, decimal AchievementPercent, Decimal targetCurrency,ID targetID,string targetStatus, ID masterPlanID, decimal achievement){

        Target_Component__c  targetComp = new Target_Component__c();
        targetComp.Weightage__c =weightage;
        targetComp.vic_Achievement_Percent__c =AchievementPercent;
        targetComp.Target_Status__c = targetStatus;
        targetComp.Target_In_Currency__c =targetCurrency;
        targetComp.Is_Weightage_Applicable__c = true;
        targetComp.Target__c = targetID;
        targetComp.vic_Achievement__c=achievement;
        targetComp.Master_Plan_Component__c=masterPlanID;
        
        return targetComp;   

    }
    
    
    public static VIC_Calculation_Matrix__c fetchMatrixComponentData(String MatrixName){

        VIC_Calculation_Matrix__c vicMatrix  = new VIC_Calculation_Matrix__c();
        vicMatrix.name = MatrixName;
        return vicMatrix;   

    }
    
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemPMData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(90,120,'PM',null,False,1000,1000,'PM');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'PM',null,False,1000,1000,'PM');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemMBOData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(90,120,'IO',null,False,null,null,'MBO');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'IO',null,False,null,null,'MBO');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
     public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemOPData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(99,120,'IO',2,false,350,5,'OP');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'IO',null,False,null,null,'OP');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemIOTCVData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(50,70,'IO',2,True,70,5,'TCV Intelligent Operations');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,200,'IO',2,True,1000,100,'TCV Intelligent Operations');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemTSTCVData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(99,120,'IO',2,True,70,5,'TCV Transformational Services');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'IO',2,True,1000,100,'TCV Transformational Services');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemTCVData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(99,120,'IO',null,True,70,5,'TCV');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'IO',null,True,null,null,'TCV');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    
    public List<VIC_Calculation_Matrix_Item__c> fetchListMatrixItemTCVAcceleratorData(string vicMatrix){

        list<VIC_Calculation_Matrix_Item__c> ItemMatrix = new list<VIC_Calculation_Matrix_Item__c>();
        VIC_Calculation_Matrix_Item__c calcMatrix=  fetchMatrixItemComponentData(99,120,'IO',null,True,70,5,'null');
          
          ItemMatrix.add(calcMatrix);
          
          VIC_Calculation_Matrix_Item__c calcMatrixUpdate=  fetchMatrixItemComponentData(120,null,'IO',null,True,null,null,'null');
          
          ItemMatrix.add(calcMatrixUpdate);
          
         return ItemMatrix;
        
    }
    
    
    
  
    public VIC_Calculation_Matrix_Item__c fetchMatrixItemComponentData(decimal startVal, decimal endVal, String tcvBookingType,Decimal forEachVal, boolean payoutApplicable, decimal payout, decimal additionalPayout, string componentType ){

        
         VIC_Calculation_Matrix_Item__c calMatrix = new VIC_Calculation_Matrix_Item__c();
         calMatrix.vic_Start_Value__c = startVal;
         calMatrix.vic_End_Value__c =endVal;
         calMatrix.vic_TCV_Booking_Type__c=tcvBookingType;
         calMatrix.For_Each__c = forEachVal;
         calMatrix.Is_Additional_Payout_Applicable__c =payoutApplicable;
         calMatrix.vic_Payout__c =payout;
         calMatrix.Additional_Payout__c=additionalPayout ;
         calMatrix.vic_Component_Type__c=componentType;
        
         return calMatrix;  

    }
    
     public Map<String, vic_Incentive_Constant__mdt> fetchMetaDataValue(){
      
      Map<String, vic_Incentive_Constant__mdt> mapData= new Map<String, vic_Incentive_Constant__mdt>();
          for(vic_Incentive_Constant__mdt each : [select DeveloperName,vic_Value__c from vic_Incentive_Constant__mdt]){
            mapData.put(each.DeveloperName, each);  
       }
      
      return mapData;
      }
            
      //tracker
      
      public static Account createAccount(string strName)
{
Account objAccount = new Account();
objAccount.Name = strName;

System.assertEquals(objAccount.Name, strName);
return objAccount;
}

/*
This Method is used to create Opportunty  
--------------------------------------------------------------------------------------
Name                                Date                                Version                     
--------------------------------------------------------------------------------------
Jubin                             3/17/2016                               1.0   
--------------------------------------------------------------------------------------
*/
public static Opportunity createOpportunity(string strName, string strStageName, string strSource,
    id acccountId)
{
Opportunity objOpportunity = new Opportunity();
objOpportunity.AccountId = acccountId;
objOpportunity.Name = strName;
objOpportunity.StageName = strStageName;
objOpportunity.Opportunity_Source__c = strSource;
objOpportunity.CloseDate = System.today().adddays(1);
objOpportunity.Revenue_Start_Date__c = System.today().adddays(180);
objOpportunity.Contract_Term_in_months__c = 2;
objOpportunity.Opportunity_Origination__c = 'Proactive';
objOpportunity.Sales_Region__c = 'Asia';
objOpportunity.Deal_Type__c = 'Competitive';
objOpportunity.Win_Loss_Dropped_reason1__c='test';


objOpportunity.GW1_Include_in_pilot__c=true;

System.assertEquals(objOpportunity.Name, strName);
System.assertEquals(200,200);
system.debug(' objOpportunity.CloseDate'+ objOpportunity.CloseDate);
system.debug(' objOpportunity.Revenue_Start_Date__c'+ objOpportunity.Revenue_Start_Date__c);
return objOpportunity;
}



/*
This Method is used to create User  
--------------------------------------------------------------------------------------
Name                                Date                                Version                     
--------------------------------------------------------------------------------------
kiran                              07/09/2018                            1.0   
--------------------------------------------------------------------------------------
*/


public static User createUser(String Alias, String LastName, string strUserName, string strProfName, string domicile)
{
Profile objProfile = [SELECT Id FROM Profile WHERE Name =: strProfName];


User objUser = new User(Alias = Alias, Email = 'standarduser11@testorg.com',
EmailEncodingKey = 'UTF-8', LastName = LastName, LanguageLocaleKey = 'en_US',
LocaleSidKey = 'en_US', ProfileId = objProfile.id,Legal_Person__c=true,
TimeZoneSidKey = 'America/Los_Angeles', UserName = strUserName,Domicile__c=domicile,isactive=true,OHR_ID__c='326001234');


return objUser;
}






public static GW1_triggerSettings__c createTriggerCustomSetting(string strName)
{
GW1_triggerSettings__c objcustomSetting = new GW1_triggerSettings__c();
objcustomSetting.name = strName;
objcustomSetting.GW1_Is_Active__c = true;

return objcustomSetting;
}

public static List<GW1_triggerSettings__c> createTriggerCustomSetting()
{
List<GW1_triggerSettings__c> listCSData = new List<GW1_triggerSettings__c> ();

GW1_triggerSettings__c objcustomSetting = new GW1_triggerSettings__c();
objcustomSetting.name = 'GW1_DSRTeamTrigger';
objcustomSetting.GW1_Is_Active__c = true;
listCSData.add(objcustomSetting);

objcustomSetting = new GW1_triggerSettings__c();
objcustomSetting.name = 'GW1_DSRTrigger';
objcustomSetting.GW1_Is_Active__c = true;
listCSData.add(objcustomSetting);

objcustomSetting = new GW1_triggerSettings__c();
objcustomSetting.name = 'GW1_trgOnDSRGroup';
objcustomSetting.GW1_Is_Active__c = true;
listCSData.add(objcustomSetting);

objcustomSetting = new GW1_triggerSettings__c();
objcustomSetting.name = 'GW1_TriggerOnOpportunityCreateDSR';
objcustomSetting.GW1_Is_Active__c = true;
listCSData.add(objcustomSetting);

return listCSData;
}

Public static OpportunityLineItem createOpportunityLineItem(string strstatus, id OppId)
{

Product2 product_obj = new Product2(name='test pro', canUseRevenueSchedule=true, isActive=true);
product_obj.Nature_of_Work__c='IT Services';
insert product_obj;
    
Id pricebookId = Test.getStandardPricebookId();
PricebookEntry entry = new PricebookEntry(Pricebook2Id = pricebookId,Product2ID = product_obj.Id, 
                          UnitPrice = 100.00, UseStandardPrice = false, isActive=true);
insert entry;


OpportunityLineItem  objopplineitem = new OpportunityLineItem();
objopplineitem.vic_CPQ_Deal_Status__c=strstatus;
objopplineitem.OpportunityId=OppId;
objopplineitem.Product2Id=product_obj.id;
objopplineitem.priceBookEntryId=entry.ID;
objopplineitem.vic_VIC_User_3_Split__c=16;
objopplineitem.vic_VIC_User_4_Split__c=16;
return objopplineitem;



}

Public static APXTConga4__Conga_Template__c createCongaTemplate()
{ 
APXTConga4__Conga_Template__c objConga = new APXTConga4__Conga_Template__c();
return objConga;



}



Public static Master_VIC_Role__c createMasterVICRole()
{
Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
return masterVICRoleObj;

 
}
public static Plan__c getPlan(id CongaId)
{
Plan__c planObj1 = new Plan__c();
planObj1.Is_Active__c = true;
planObj1.Year__c = '2018';
planObj1.Conga_Template__c=CongaId;

return planObj1;
}
public static VIC_Role__c getVICRole(id masterVICRoleObjid,id planObj1Id){

VIC_Role__c VICRoleObj = new VIC_Role__c();
VICRoleObj.Master_VIC_Role__c=masterVICRoleObjid;
VICRoleObj.Plan__c=planObj1Id;
return VICRoleObj;
}

public static Master_VIC_Role__c getMasterVICRole(){
Master_VIC_Role__c masterVICRoleObj = new Master_VIC_Role__c();
masterVICRoleObj.is_Active__c = true;
return masterVICRoleObj;
}

public static User_VIC_Role__c getUserVICRole(id userid){
User_VIC_Role__c userVICRole = new User_VIC_Role__c();
userVICRole.User__c=userid;
userVICRole.Not_Applicable_for_VIC__c = false;
return userVICRole;
}

public static Target__c getTarget(){
Target__c targetObj = new Target__c();
targetObj.Start_Date__c = Date.newInstance(2018, 1, 1);
targetObj.Is_Active__c=true;
return targetObj;
}


public static Target_Component__c getTargetComponent(){
Target_Component__c targetComponentObj = new Target_Component__c();
targetComponentObj.Target_Status__c = 'Active';
return targetComponentObj;
}

public static Target_payout__c getTargetPayout(){
Target_payout__c targetPayout = new Target_payout__c();
targetPayout.Amount__c = 1000;
targetPayout.Payout_Date__c = system.today();
return targetPayout;
}




public static Target_Achievement__c fetchIncentive(id targetcompid, decimal bonusamount)
{

Target_Achievement__c ObjInc = new Target_Achievement__c();
ObjInc.Target_Component__c=targetcompid;
ObjInc.Bonus_Amount__c=bonusamount;

return ObjInc;    

}      
    
    
}