public class GPDomainAddress extends fflib_SObjectDomain {

    public GPDomainAddress(List < GP_Address__c > sObjectList) {
        super(sObjectList);
    }

    public class Constructor implements fflib_SObjectDomain.IConstructable {
        public fflib_SObjectDomain construct(List < SObject > sObjectList) {
            return new GPDomainAddress(sObjectList);
        }
    }

    //SObject's used by the logic in this service, listed in dependency order
    private static List < Schema.SObjectType > DOMAIN_SOBJECTS = new Schema.SObjectType[] {
        GP_Address__c.SobjectType


    };

    public override void onBeforeInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        autoPopulateRelatedCustomer(null);
        autoPopulateRelatedBillingEntity(null);
    }

    public override void onAfterUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }

    public override void onBeforeUpdate(Map < Id, SObject > oldSObjectMap) {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        autoPopulateRelatedCustomer(oldSObjectMap);
        autoPopulateRelatedBillingEntity(oldSObjectMap);
    }

    public override void onAfterInsert() {
        //fflib_SObjectUnitOfWork uow = new fflib_SObjectUnitOfWork(DOMAIN_SOBJECTS);
        //uow.commitWork();
    }


    //method to autopopulate the related customer lookup on the Address object whenever the user inserts a value in the 
    //customer accountID field on the address object
    //mandeep.singh@saasfocus.com
    public void autoPopulateRelatedCustomer(Map < Id, SObject > oldSObjectMap) {
        Map < String, GP_Customer_Master__c > mapofCustomerAccountIDandCustomer = new Map < String, GP_Customer_Master__c > ();
        set < String > setofCustomerAccountID = new set < String > ();
        GP_Address__c oldAddressObject;

        for (GP_Address__c addressObject: (list < GP_Address__c > ) records) {
            if (addressObject != null) {
                if (oldSObjectMap != null)
                    oldAddressObject = (GP_Address__c) oldSObjectMap.get(addressObject.Id);
                else
                    oldAddressObject = null;

                if (addressObject.GP_Customer_Account_ID__c != null &&
                    ((oldAddressObject != null && addressObject.GP_Customer_Account_ID__c != oldAddressObject.GP_Customer_Account_ID__c) ||
                        oldSObjectMap == null)) {
                    setofCustomerAccountID.add(addressObject.GP_Customer_Account_ID__c);
                } else if (addressObject.GP_Customer_Account_ID__c == null &&
                    (oldAddressObject != null &&
                        addressObject.GP_Customer_Account_ID__c != oldAddressObject.GP_Customer_Account_ID__c)) {
                    addressObject.GP_Customer__c = null;
                }
            }
        }

        if (setofCustomerAccountID.size() > 0) {
            for (GP_Customer_Master__c customerObj: GPSelectorCustomerMaster.selectCustomerRecords(setofCustomerAccountID)) {
                mapofCustomerAccountIDandCustomer.put(customerObj.GP_Oracle_Customer_Id__c, customerObj);
            }

            for (GP_Address__c addressObject: (list < GP_Address__c > ) records) {
                if (oldSObjectMap != null)
                    oldAddressObject = (GP_Address__c) oldSObjectMap.get(addressObject.Id);
                else
                    oldAddressObject = null;

                if (addressObject.GP_Customer_Account_ID__c != null &&
                    ((oldAddressObject != null && addressObject.GP_Customer_Account_ID__c != oldAddressObject.GP_Customer_Account_ID__c) ||
                        oldSObjectMap == null)) {
                    if (mapofCustomerAccountIDandCustomer != null && mapofCustomerAccountIDandCustomer.size() > 0 &&
                        mapofCustomerAccountIDandCustomer.get(addressObject.GP_Customer_Account_ID__c) != null &&
                        mapofCustomerAccountIDandCustomer.get(addressObject.GP_Customer_Account_ID__c).id != null) {
                        addressObject.GP_Customer__c = mapofCustomerAccountIDandCustomer.get(addressObject.GP_Customer_Account_ID__c).id;
                    } else {
                        addressObject.GP_Customer__c = null;
                    }
                }
            }
        }
    }

    //method to autopopulate the related Billing Entity lookup on the Address object whenever the user inserts a value in the 
    //Billing Entity ID field on the address object
    ////mandeep.singh@saasfocus.com
    public void autoPopulateRelatedBillingEntity(Map < Id, SObject > oldSObjectMap) {
        Map < String, GP_Pinnacle_Master__c > mapofBillingEntityIDandPinnacleMaster = new Map < String, GP_Pinnacle_Master__c > ();
        set < String > setofBillingEntityID = new set < String > ();
        GP_Address__c oldAddressObject;

        for (GP_Address__c addressObject: (list < GP_Address__c > ) records) {
            if (addressObject != null) {
                if (oldSObjectMap != null)
                    oldAddressObject = (GP_Address__c) oldSObjectMap.get(addressObject.Id);
                else
                    oldAddressObject = null;

                if (addressObject.GP_Billing_Entity_Id__c != null &&
                    ((oldAddressObject != null && addressObject.GP_Billing_Entity_Id__c != oldAddressObject.GP_Billing_Entity_Id__c) ||
                        oldSObjectMap == null)) {
                    setofBillingEntityID.add(addressObject.GP_Billing_Entity_Id__c);
                } else if (addressObject.GP_Billing_Entity_Id__c == null &&
                    (oldAddressObject != null &&
                        addressObject.GP_Billing_Entity_Id__c != oldAddressObject.GP_Billing_Entity_Id__c)) {
                    addressObject.GP_Billing_Entity__c = null;
                }
            }
        }
        if (setofBillingEntityID.size() > 0) {
            for (GP_Pinnacle_Master__c pinnacleObj: GPSelectorPinnacleMasters.selectBillingEntityRecords(setofBillingEntityID)) {
                mapofBillingEntityIDandPinnacleMaster.put(pinnacleObj.GP_Oracle_Id__c, pinnacleObj);
            }

            for (GP_Address__c addressObject: (list < GP_Address__c > ) records) {
                if (oldSObjectMap != null)
                    oldAddressObject = (GP_Address__c) oldSObjectMap.get(addressObject.Id);
                else
                    oldAddressObject = null;

                if (addressObject.GP_Billing_Entity_Id__c != null &&
                    ((oldAddressObject != null && addressObject.GP_Billing_Entity_Id__c != oldAddressObject.GP_Billing_Entity_Id__c) ||
                        oldSObjectMap == null)) {
                    if (mapofBillingEntityIDandPinnacleMaster != null && mapofBillingEntityIDandPinnacleMaster.size() > 0 &&
                        mapofBillingEntityIDandPinnacleMaster.get(addressObject.GP_Billing_Entity_Id__c) != null &&
                        mapofBillingEntityIDandPinnacleMaster.get(addressObject.GP_Billing_Entity_Id__c).id != null) {
                        addressObject.GP_Billing_Entity__c = mapofBillingEntityIDandPinnacleMaster.get(addressObject.GP_Billing_Entity_Id__c).id;
                    } else {
                        addressObject.GP_Billing_Entity__c = null;
                    }
                }
            }
        }
    }
}