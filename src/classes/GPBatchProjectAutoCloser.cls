// ------------------------------------------------------------------------------------------------ 
// Description: If a project is not marked as closed within 90 days of its defined end date, and no draft
//  version of the project exists in the system, then project will be auto-submitted for closure. 
// ------------------------------------------------------------------------------------------------
// Modification Date::06-DEC -2017  Created By : Mandeep Singh Chauhan
// ------------------------------------------------------------------------------------------------ 
global class GPBatchProjectAutoCloser implements 
Database.Batchable<sObject>, Database.Stateful {
    
    List<GP_Project__c> listofprj = new  List<GP_Project__c>(); 
    global Integer daysProjectAutoclosure = integer.valueOf(system.label.GP_Days_Within_Project_End_Date_For_Autoclosure);
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([Select Id, Name,GP_End_Date__c, GP_Auto_Submitted_For_Closer_Approval__c,
                                         (select id,name, GP_Approval_Status__c from Projects__r
                                          where GP_Approval_Status__c =: 'Draft')
                                         From GP_Project__c 
                                         where (GP_End_Date__c =:System.Today().adddays(-daysProjectAutoclosure) and GP_Is_Closed__c =:False )]);      
    }
    
    global void execute(Database.BatchableContext bc, List<GP_Project__c> scope){
        system.debug('scope@@@'+scope);
        if(scope != null && scope.size()>0)
        {
            for (GP_Project__c prj : scope)
            {     
                if(prj.Projects__r.size()==0)
                {
                    system.debug('prj@@@'+prj);
                    listofprj.add(prj);
                }
            }
        }
        system.debug('listofprj@@@'+listofprj);
    }        
    
    global void finish(Database.BatchableContext bc){
        if(listofprj != null && listofprj.size()>0)
        {
            for(GP_Project__c prjobj : listofprj)
            {
                if(prjobj.GP_Auto_Submitted_For_Closer_Approval__c != null)
                {
                    prjobj.GP_Auto_Submitted_For_Closer_Approval__c = true;
                }
            }
            update listofprj;
        }
    }    
}