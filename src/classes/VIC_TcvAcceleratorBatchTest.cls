@istest
public class VIC_TcvAcceleratorBatchTest {
 Public static Target__c targetObj;
    public static user objuser;
    public static OpportunityLineItem  objOLI;
    public static Opportunity objOpp;
    public static list<Target_Achievement__c> lstInc= new list<Target_Achievement__c>();
    public static Target_Component__c targetComponentObj ;
    
    static testmethod void VIC_TcvAcceleratorBatchTest(){
         LoadData();
        
      VIC_TcvAcceleratorBatch obj= new VIC_TcvAcceleratorBatch();
       database.executeBatch(obj);
        
      VIC_TcvAcceleratorBatch obj1= new VIC_TcvAcceleratorBatch(); 
        obj1.initIncentiveLst(targetComponentObj,50,'IO','IO',lstInc,'IO',50);
        
       VIC_TcvAcceleratorBatch.initTargetIncentivRecOBJ(targetComponentObj,1000,'test','test','test',100); 
        System.assertEquals(200,200);
    }
   static void LoadData()
{

    VIC_Process_Information__c vicInfo = new VIC_Process_Information__c();
        vicInfo.VIC_Annual_Process_Year__c=2018;
        vicInfo.VIC_Process_Year__c=2018;
       insert vicInfo;
objuser= VIC_CommonTest.createUser('Test','Test Name','jdncjgn@jdpjhd.com','Genpact Super Admin','China');
insert objuser;

user objuser1= VIC_CommonTest.createUser('Test','Test Name1','jdncjn@jdpjhd.com','Genpact Sales Rep','China');
insert objuser1;
    
user objuser2= VIC_CommonTest.createUser('Test','Test Name2','jdncjn@jdljhd.com','Genpact Sales Rep','China');
insert objuser2;

user objuser3= VIC_CommonTest.createUser('Test','Test Name3','jdncjn@jkdjhd.com','Genpact Sales Rep','China');
insert objuser3;
Master_VIC_Role__c masterVICRoleObj =  VIC_CommonTest.getMasterVICRole();
insert masterVICRoleObj;
    
User_VIC_Role__c objuservicrole=VIC_CommonTest.getUserVICRole(objuser.id);
objuservicrole.vic_For_Previous_Year__c=false;
objuservicrole.Not_Applicable_for_VIC__c = false;
objuservicrole.Master_VIC_Role__c=masterVICRoleObj.id;
insert objuservicrole;
    
APXTConga4__Conga_Template__c objConga = VIC_CommonTest.createCongaTemplate();
insert objConga;
    
Account objAccount=VIC_CommonTest.createAccount('Test Account');
insert objAccount;
    

    
Plan__c planObj1=VIC_CommonTest.getPlan(objConga.id);
planObj1.vic_Plan_Code__c='IT_GRM';    
insert planobj1;
    

    
VIC_Role__c VICRoleObj=VIC_CommonTest.getVICRole(masterVICRoleObj.id,planobj1.id); 
insert VICRoleObj;

objOpp=VIC_CommonTest.createOpportunity('Test Opp','Prediscover','Ramp Up',objAccount.id);
objOpp.Actual_Close_Date__c=system.today();
objopp.ownerid=objuser.id;
objopp.Sales_country__c='Canada';

insert objOpp;
    


objOLI= VIC_CommonTest.createOpportunityLineItem('Active',objOpp.id);
objOLI.vic_Final_Data_Received_From_CPQ__c=true;
objOLI.vic_Sales_Rep_Approval_Status__c = 'Approved';
objOLI.vic_Product_BD_Rep_Approval_Status__c = 'Approved';
objOLI.vic_is_CPQ_Value_Changed__c = true;
objOLI.vic_Contract_Term__c=24;
objOLI.Product_BD_Rep__c=objuser.id;
objOLI.vic_VIC_User_3__c=objuser2.id;
objOLI.vic_VIC_User_4__c=objuser3.id; 
objOLI.vic_Primary_Sales_Rep_Split__c=25;
objOLI.TCV__c=6;
objOLI.vic_Is_Split_Calculated__c=false;

insert objOLI;

objopp.Number_of_Contract__c=2;    
objOpp.stagename='6. Signed Deal';
test.starttest();
system.runas(objuser){
update objOpp;
}
test.stoptest();
targetObj=VIC_CommonTest.getTarget();
targetObj.user__c =objuser.id;
targetObj.vic_TCV_IO__c=10000;  
targetObj.vic_TCV_TS__c=10000;   
targetObj.Target_Bonus__c=350000;

targetObj.Plan__C=planobj1.id;
insert targetObj;
targetObj.Start_date__c=system.today();
update targetObj;
  

Master_Plan_Component__c masterPlanComponentObj=VIC_CommonTest.fetchMasterPlanComponentData('TCV','Currency');
masterPlanComponentObj.vic_Component_Code__c='TCV_Accelerators'; 
masterPlanComponentObj.vic_Component_Category__c='Upfront';    
insert masterPlanComponentObj;
    
VIC_Calculation_Matrix__c vicMatrix1=VIC_CommonTest.fetchMatrixComponentData('IO');
insert vicMatrix1;
VIC_Calculation_Matrix__c vicMatrix2=VIC_CommonTest.fetchMatrixComponentData('SEM');
insert vicMatrix2; 
VIC_Calculation_Matrix__c vicMatrix3=VIC_CommonTest.fetchMatrixComponentData('TCV Accelerator Matrix For IT GRM');
insert vicMatrix3;     
Plan_Component__c PlanComponentObj =VIC_CommonTest.fetchPlanComponentData(masterPlanComponentObj.id,planobj1.id);
PlanComponentObj.VIC_IO_Calculation_Matrix_1__c=vicMatrix1.id;
insert PlanComponentObj;
    
 targetComponentObj=VIC_CommonTest.getTargetComponent();
targetComponentObj.Target__C=targetObj.id;
targetComponentObj.Target__r=   targetObj; 
targetComponentObj.Master_Plan_Component__c=masterPlanComponentObj.id;
targetComponentObj.Target_In_Currency__c=10000;
targetComponentObj.Weightage__c=10;

  
insert targetComponentObj;
targetComponentObj=[select id,Target__c,VIC_Total_Achievement_Scoreboard__c,Target__r.vic_TCV_IO__c,Target__r.vic_TCV_TS__c,Target__r.Plan__r.vic_Plan_Code__c,Target__r.Is_Active__c,Master_Plan_Component__r.Name,Target_In_Currency__c,Weightage__c,Target__r.Target_Bonus__c,Target__r.Start_date__c,Master_Plan_Component__r.vic_Component_Code__c,Target__r.vic_Total_TCV__c from Target_Component__c where id=:targetComponentObj.id];
system.debug('targetComponentObj'+targetComponentObj);
system.debug('targetComponentObj'+targetComponentObj); 
    
Target_Achievement__c ObjInc= VIC_CommonTest.fetchIncentive(targetComponentObj.id,1000);
ObjInc.vic_Opportunity_Product_Id__c=objOLI.id;
ObjInc.vic_Status__c='HR - Pending';
insert objInc;
lstInc.add(objInc);
 
 System.AssertEquals(200,200);
    



} 
}