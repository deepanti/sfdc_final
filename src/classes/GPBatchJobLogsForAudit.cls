//AvinashN
global class GPBatchJobLogsForAudit implements Database.Batchable<sObject>,  Database.stateful, Database.AllowsCallouts {
    string header = 'JobName,    Job Start Date,      Job End Date,      Status,  CreatedBy \n';
    string finalstr = '';
    string recordString='';
    string finalRecordString='';
    String AttachmentName ='Pinnacle Batch Job Logs - ' + System.today().Year();
    Attachment att= new Attachment();
    global Database.QueryLocator Start(Database.BatchableContext bc)
    {  
        return Database.getQueryLocator(System.Label.GP_Batch_Job_Logs_For_Audit_Soql);
    }
    global void Execute(Database.BatchableContext bc, List<AsyncApexJob> scope)
    {
        List<AsynApexJobDetails> lstAsyncApexJobDetails=new List<AsynApexJobDetails>();
        if(scope.size()>0)
        {
            
            for(AsyncApexJob objAsync : scope)
            {
                AsynApexJobDetails objJobDetails=new AsynApexJobDetails();
                objJobDetails.JobId=objAsync.Id;
                objJobDetails.ApexClassName=objAsync.ApexClass.Name;
                objJobDetails.JobType=objAsync.JobType;
                objJobDetails.Status=objAsync.Status;
                objJobDetails.NumberOfErrors=String.ValueOf(objAsync.NumberOfErrors);
                objJobDetails.TotalJobItems=String.ValueOf(objAsync.TotalJobItems);
                objJobDetails.CreatedDate=String.ValueOf(objAsync.CreatedDate);
                objJobDetails.CompletedDate=String.ValueOf(objAsync.CompletedDate);
                objJobDetails.CreatedBy=String.ValueOf(objAsync.CreatedBy.Name) +' - ' + String.ValueOf(objAsync.CreatedBy.OHR_ID__c);
                lstAsyncApexJobDetails.add(objJobDetails);
                
                // 'JobName , CreatedDate,CompletedDate,JobType,Status,CreatedBy \n';
                recordString = objJobDetails.ApexClassName +','+objJobDetails.CreatedDate+','+objJobDetails.CompletedDate +','+objJobDetails.Status + ','+objJobDetails.CreatedBy +'\n';
                finalRecordString = finalRecordString + recordString;
            }
            if(lstAsyncApexJobDetails.size()>0)
            {  
                List<GP_BatchJobLog__c> lstBtchJob=[Select Id from GP_BatchJobLog__c where Name ='Pinnacle Running Batch Jobs Logs Attachments'];
                if(lstBtchJob.size()==0){   
                    GP_BatchJobLog__c obj=new GP_BatchJobLog__c();
                    obj.Name='Pinnacle Running Batch Jobs Logs Attachments';
                    lstBtchJob.add(obj);
                    insert lstBtchJob;
                }
                List<Attachment> ConsoliatedAttachment = [Select Id, Name, Body from Attachment where Name =: AttachmentName order by LastModifiedDate desc limit 1 ];
   
                if(ConsoliatedAttachment.size()>0)
                {
                    if(ConsoliatedAttachment[0].Name.Contains(String.ValueOf(System.today().Year())))
                    {
                        finalstr = ConsoliatedAttachment[0].Body.ToString() + finalRecordString;                        
                        ConsoliatedAttachment[0].Body = Blob.valueOf(finalstr);
                        upsert ConsoliatedAttachment[0];
                    }
                    else
                    {
                        finalstr = header + finalRecordString;                        
                        att.contentType = 'text/plain';
                        att.name = AttachmentName;
                        att.parentId = lstBtchJob[0].Id;                
                        att.body = Blob.valueOf(finalstr);
                        insert att;
                    }
                }
                else
                {
                    finalstr = header + finalRecordString;       
                    att.contentType = 'text/plain';
                    att.name = AttachmentName;
                    att.parentId = lstBtchJob[0].Id;                
                    att.body = Blob.valueOf(finalstr);
                    insert att;
                }
                
                
                
            }
        }
    }
    global void finish(Database.BatchableContext bc)
    { 
        List<Attachment> ConsoliatedAttachmentFin = [Select Id, Name, Body from Attachment where Name =: AttachmentName order by LastModifiedDate desc limit 1 ];
        if(ConsoliatedAttachmentFin.size()>0)
        {
            if(att.Name==null){att=ConsoliatedAttachmentFin[0];}
            Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
            attach.setBody(att.body);        
            attach.setFileName('PinnacleBatchJobLog'+String.valueOf(System.today().adddays(-1)).removeEnd(' 00:00:00')+'.csv');
            String[] receiverAddr=System.Label.GP_Batch_Job_Email.split(',');
            string[] address = receiverAddr;         
            Messaging.singleEmailMessage Emailwithattch = new Messaging.singleEmailMessage();        
            Emailwithattch.setSubject('Batch job Log are attached - PinnacleBatchJobLog'+String.valueOf(System.today().adddays(-1)).removeEnd(' 00:00:00'));        
            Emailwithattch.setToaddresses(address);        
            Emailwithattch.setPlainTextBody('Please find the attached Pinnacle Batch Job logs'); 
            Emailwithattch.setFileAttachments(new Messaging.EmailFileAttachment[]{attach});        
            // Sends the email        
            Id oweaId = GPCommon.getOrgWideEmailId();
            if (oweaId != null) {
                Emailwithattch.setOrgWideEmailAddressId(oweaId);
            }
            Messaging.SendEmailResult [] r =  Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Emailwithattch});
        }
    }
    
    
}