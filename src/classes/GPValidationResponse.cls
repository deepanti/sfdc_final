public class GPValidationResponse {
    public String externalParameter{get; set;}
    public Boolean isSuccess{get; set;}
    public String message{get; set;}
    
    public GPValidationResponse(Boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public GPValidationResponse(Boolean isSuccess, String message, String externalParameter) {
        this.externalParameter = externalParameter;
        this.isSuccess = isSuccess;
        this.message = message;
    }
}