@isTest
public class AnnouncementPreferencePurgeSchedulerTest {
    public static User u, u1, u3;
    public static testMethod void testschedule() {
        Test.StartTest();
        String CRON_EXP = '0 0 0 15 3 ? *';
        String jobId = System.schedule('ScheduledApexTest',CRON_EXP, new AnnouncementPreferencePurgeScheduler());
        system.assertNotEquals('null', jobId);
        Test.StopTest();
    }
}