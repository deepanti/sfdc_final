/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing OpportunityDiscoverAddProduct class
**************************************************************************************************************************/
@isTest (SeeAllData = false)
public class OpportunityDiscoverAddProductTest {
public static User u;
public static User testUser;
public static Account acc;
public static Contact con1;
public static Opportunity oppList;
public static List<Product2> pList;
public static PricebookEntry standardPrice; 
public static  Id pricebookId;
public static List<OpportunityLineItem> oppLineItem; 
public static List<OpportunityLineItemSchedule> oppLineItemSchedules;
/**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
public static void setUpData()
{	
    acc = TestDataFactoryUtility.createTestAccountRecordforDiscoverOpportunity();
    insert acc;
    con1 = TestDataFactoryUtility.CreateContact('First','Last',acc.id,'Test','Test','fed@gtr.com','78342342343');
    insert con1;
    oppList = TestDataFactoryUtility.CreateOpportunity('Test',acc.id,con1.id);
    insert oppList;
    pList = TestDataFactoryUtility.createTestProducts2(1);
    pList[0].Industry_Vertical__c='BFS';
     pList[0].isActive=true;
    insert pList;
    system.debug('pList'+ pList);
    pricebookId = Test.getStandardPricebookId();
    standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, 
        Product2Id = pList[0].Id,
        UnitPrice = 1000000000, 
        IsActive = true);
    insert standardPrice;
    oppLineItem = TestDataFactoryUtility.createOpportunityLineItems(pList,oppList.Id,standardPrice.id);

    insert oppLineItem;
    
    
    testUser = TestDataFactoryUtility.createTestUser('Genpact Super Admin','ac@bc.com','ac@bc.com');
    insert testUser;
   /* list<opportunitylineitem>    insertOlis = new   list<opportunitylineitem>(); 
     opportunitylineitem oli = new opportunitylineitem();
                    oli.product2Id= pList[0].id;
                    oli.product2=pList[0];
                    oli.product2.Nature_of_Work__c=pList[0].Nature_of_Work__c;
                    oli.product2.Service_Line__c=pList[0].Service_Line__c;
                    oli.product2.Name=pList[0].Name;
                    insertOlis.add(oli);
                    insert insertOlis;*/
}
public static testMethod void getProductTest1()
{
    setUpData();
    System.runAs(testUser){
        Test.startTest();
        try{
         OpportunityDiscoverAddProductController.getProducts(oppList.Id);
string createList='[{"sobjectType":"OpportunityLineItem","Nature_of_Work__c":"Consulting","Service_Line__c":"F&A Multi-tower Consulting","Opportunity__c":"'+oppList.id+'","Id":"'+oppLineItem[0].id+'","Product2Id":"'+pList[0].id+'"},{"sobjectType":"OpportunityLineItem","Nature_of_Work__c":"Digital","Service_Line__c":"Commercial Leasing & Lending","Opportunity__c":"'+oppList.id+'","Id":"'+oppLineItem[0].id+'","Product2Id":"'+pList[0].id+'"}]';
OpportunityDiscoverAddProductController.saveOppLineItems(createList,oppList.id);

OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, 'F&A Multi-tower Consulting', 'Digital', pList[0].Name,false);
 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, 'Accounts Payable', '--None--', '--None--',true);
 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, '--None--', 'Consulting', '--None--',true);
 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, '--None--', '--None--', 'ERP Design',true);

 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, '--None--', 'Consulting', 'ERP Design',true);
 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, 'Accounts Payable', 'Consulting', '--None--',true);
 OpportunityDiscoverAddProductController.getSearchedProduct(oppList.Id, 'Accounts Payable', '--None--', 'ERP Design',true);
 system.assertNotEquals(null, OpportunityDiscoverAddProductController.getProducts(oppList.Id));
}
 catch(Exception e)
        { 
           CreateErrorLog.createErrorRecord(oppList.Id, e.getMessage(), '', e.getStackTraceString(), '','', '', '', String.valueOf(e.getLineNumber()));
        }
        Test.stopTest();
    }
}


 public static testMethod void getProductTest2(){
       setUpData();
    System.runAs(testUser){
  Test.startTest();
OpportunityDiscoverAddProductController.getOpportunityClosedDate(oppList.Id);
 OpportunityDiscoverAddProductController.getAddedProducts_LineItems(oppList.Id);
 
OpportunityDiscoverAddProductController.saveChanges(oppList.Id, '--None--', 'Consulting', 'ERP Design');
 OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, 'Accounts Payable', 'Consulting', 'ERP Design');

OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, '--None--', '--None--', '--None--');

OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, 'Accounts Payable', '--None--', '--None--');
OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, '--None--', 'Consulting', '--None--');
 OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, '--None--', '--None--', 'ERP Design');

OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, '--None--', 'Consulting', 'ERP Design');
OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, 'Accounts Payable', 'Consulting', '--None--');
OpportunityDiscoverAddProductController.getProductsOnChange(oppList.Id, 'Accounts Payable', '--None--', 'ERP Design');
  Test.stopTest();
    }

 }

 



}