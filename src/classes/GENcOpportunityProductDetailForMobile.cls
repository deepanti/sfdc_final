public  class GENcOpportunityProductDetailForMobile {
    public OpportunityProduct__c opportunityProduct {get;set;}
    public RevenueSchedule__c revenueSchedule {get; set;}
    public List<RevenueSchedule__c> revenueScheduleList {get; set;}
    public string isScheduleClicked {get;set;}
    public string isEditScheduleClicked {get; set;}
    public string revenueSplitRollingYear {get; set;}
    public integer revenueRollingYearIndex {get; set;}
    public List<revenueRollingYearWrapper> revenueRYWrapperList {get; set;}    
    public List<ProductScheduleWrapper> productScheduleWrapperList {get; set;}
    public List<ProductScheduleWrapper> revenueForRollingYearOneWrapperList {get; set;}
    public List<ProductScheduleWrapper> revenueForRollingYearTwoWrapperList {get; set;}
    private static final integer NO_OF_INSTALLMENT = 24;
    public decimal revenueForRollingYearOne {get; set;}
    public decimal revenueForRollingYearTwo {get; set;}
    public decimal revenueToSplitUp {get; set;}
    public integer revenueRollingYearToDelete {get; set;}
    public integer revenueRollingYearToEdit {get; set;}
    public decimal installmentReminder {get; set;}
    
    
    public GENcOpportunityProductDetailForMobile(ApexPages.StandardController controller) {
        opportunityProduct = (OpportunityProduct__c)controller.getRecord();
        opportunityProduct = [Select Name,Product_Autonumber__c,Overwrite_values__c,LastModifiedById,OpportunityId__r.StageName,OpportunityId__r.Opportunity_ID__c,OpportunityId__r.Project_Margin__c,OpportunityId__r.Margin__c,OpportunityId__r.Industry_Vertical__c,OpportunityId__r.Ownerid,OpportunityId__r.Id,OpportunityId__r.AccountId, ACV__c,COE__c,ContractTerminmonths__c,CreatedById,CurrentYearRevenue__c, CurrentYearRevenue_CYR__c,DeliveryLocation__c,Description__c,NextYearRevenue__c, NextYearRevenue_NYR__c, OpportunityId__c,Product__c,Quantity__c,
                              RevenueStartDate__c,NYR_LC__c,CYR_LC__c,SalesExecutive__c,SEP__c,TotalContractValue__c,TotalPrice__c,CurrentYearRevenueLocal__c,NextYearRevenueLocal__c,Product_Family_OLI__c,Product_Group__c,Service_Line_OLI__c,Service_Line_Category_Oli__c,Total_TCV__c,Total_ACV__c,Total_CYR__c,Total_NYR__c,ExchangeRate__c,LocalCurrency__c, TCVLocal__c,UnitPrice__c,ACVLocal__c,P_L_SUB_BUSINESS__c,RevenueForRollingYearOne__c,FTE_10th_month__c,FTE_4th_month__c,FTE_7th_month__c,Quarterly_FTE_1st_month__c,RevenueForRollingYearTwo__c,  FTE__c,EndDate__c,Nature_of_Work_lookup__c,Nature_of_Work_lookup__r.name,Product__r.name,Product_family_Lookup__c,Product_family_Lookup__r.name,Service_Line_lookup__c,Service_Line_lookup__r.name,Digital_Offerings__c,Analytics__c,digital_assets__c,digital_tcv__c, digital_cyr__c,digital_nyr__c from OpportunityProduct__c where Id =: controller.getRecord().Id];
        
        /*revenueScheduleList = [Select id, Name, StartDate__c, ScheduleType__c, Revenue__c, InstallmentPeriod__c, NumberOfInstallments__c from RevenueSchedule__c where OpportunityProduct__c=:opportunityProduct.Id];
        revenueSchedule = (revenueScheduleList <>null && revenueScheduleList.size() > 0) ? revenueScheduleList[0] : new RevenueSchedule__c();
        
        
        
        revenueSchedule.InstallmentPeriod__c = 'Monthly';
        revenueSchedule.ScheduleType__c = 'Divide amount into multiple installment';*/
        
        //ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Total TCV for monthly buckets exceeded TCV' ));
        system.debug('schedule $$$$' +revenueSchedule);
        getProductShedule();
    }
    
    public PageReference doCancel(){
        PageReference pageRef = new PageReference('/'+opportunityProduct.OpportunityId__c);
        pageRef.setRedirect(true);
        return pageRef;    
    }
    
    public void getRevenueRollingYear() {
        revenueRYWrapperList = new List<RevenueRollingYearWrapper>();
        decimal contractTerm = Math.ceil(opportunityProduct.ContractTerminmonths__c / 12);
        
        if(contractTerm > 0) {
            for(Integer i=0; i<contractTerm; i++) {
                revenueRYWrapperList.add(new RevenueRollingYearWrapper(i));
            }
        } else {
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Sorry, Product schedule can not be created when contract term is 0' ));
        }
    
    }
   public pagereference Edit_Mobile()
    {
     try{
                
           PageReference pageRef = new PageReference('/apex/GENvEditOpportunityProductForMobile?id='+opportunityProduct.Id);
           pageRef.setRedirect(true);
           return pageRef;
                }
                catch(exception ex)
                {
                    return null;
                }
    }
    
         public void getProductShedule(){
        revenueRYWrapperList = new List<RevenueRollingYearWrapper>();
        integer index;
        boolean isReminderInstallmentToBeSplited = false;
        List<RevenueSchedule__c> revScheduleList = [Select id, Name, StartDate__c, ScheduleType__c,RemainderInstallment__c,RollingYear__c, RollingYearRevenueLocal__c,RollingYearRevenue__c, Revenue__c, InstallmentPeriod__c, NumberOfInstallments__c, (Select Id, Name, ScheduleDate__c, Revenue__c, RevenueLocal__c, Description__c from ProductSchedules__r order by ScheduleDate__c) from RevenueSchedule__c where OpportunityProduct__c=:opportunityProduct.Id order by RollingYear__c];
        for(RevenueSchedule__c rs: revScheduleList ) {
            if(integer.valueOf(rs.RollingYear__c)==NULL)
            rs.RollingYear__c=1;
            index = integer.valueOf(rs.RollingYear__c) - 1;
            RevenueRollingYearWrapper rw = new RevenueRollingYearWrapper(index);
            rw.revSchedule = rs;
            updateProductScheduleWrapperList(rw, rs.ProductSchedules__r); 
            revenueRYWrapperList.add(rw);           
            isReminderInstallmentToBeSplited = (rs.RemainderInstallment__c > 0) ? true : false;         
            //index++;
        }
        if(isReminderInstallmentToBeSplited){
            if(revScheduleList <> null ){
                if(revScheduleList.size() > 1 && revScheduleList.size() <> 0){
                    installmentReminder = revScheduleList[revScheduleList.size()-2].RemainderInstallment__c;
                }else if(revScheduleList.size() == 1){
                    installmentReminder = revScheduleList[0].RemainderInstallment__c;
                }
            }
        }else{
            installmentReminder = 0;
        }
   }
  
    public void updateProductScheduleWrapperList(RevenueRollingYearWrapper rw, List<ProductSchedule__c> productScheduleList) {
        List<productScheduleWrapper> pScheduleWrapperList = new List<productScheduleWrapper>();
        integer listItem = 0;
        for(ProductSchedule__c ps: productScheduleList){
            productScheduleWrapper w = new productScheduleWrapper(ps, listItem );
            DateTime myDate = datetime.newInstance(ps.ScheduleDate__c.year(), ps.ScheduleDate__c.month(), ps.ScheduleDate__c.day());
            w.formatedDate  = myDate.format('MMM, yyyy');            
            pScheduleWrapperList .add(w);            
            listItem++;
        }
        rw.scheduleList = pScheduleWrapperList ;        
    }
    
        
  
    
    public boolean hasRevenueRollingYearTwoRendered = false;
    public boolean getHasRevenueRollingYearTwoRendered() {
        if(opportunityProduct.ContractTerminmonths__c <> null && opportunityProduct.ContractTerminmonths__c > 12 ) {
            hasRevenueRollingYearTwoRendered = true;
        }else{
            hasRevenueRollingYearTwoRendered = false;
        }
        return hasRevenueRollingYearTwoRendered;
    }
    public void setHasRevenueRollingYearTwoRendered(boolean val) {
        hasRevenueRollingYearTwoRendered = val;
    }
    
    public boolean hasScheculeSectionRendered = false;
    public boolean getHasScheculeSectionRendered() {
        if((isScheduleClicked <> null && isScheduleClicked == 'Yes') ||  revenueRYWrapperList.size() > 0) {
            hasScheculeSectionRendered = true;
        }else{
            hasScheculeSectionRendered = false;
        }
        return hasScheculeSectionRendered;
    }
    public void setHasScheculeSectionRendered(boolean val) {
        hasScheculeSectionRendered = val;
    }
    
    public boolean hasScheculeListRendered = false;
    public boolean getHasScheculeListRendered() {
        
        if(revenueRYWrapperList <> null && revenueRYWrapperList .size() > 0 ) {
            hasScheculeListRendered = true;
        }else{
            hasScheculeListRendered = false;
        }
        return hasScheculeListRendered;
    }
    public void setHasScheculeListRendered(boolean val) {
        hasScheculeListRendered = val;
    }
    
    public boolean hasScheculeListRollingYearOneRendered = false;
    public boolean gethasScheculeListRollingYearOneRendered () {
        
        if(revenueForRollingYearOneWrapperList <> null && revenueForRollingYearOneWrapperList.size() > 0 ) {
            hasScheculeListRollingYearOneRendered  = true;
        }else{
            hasScheculeListRollingYearOneRendered = false;
        }
        return hasScheculeListRollingYearOneRendered ;
    }
    public void sethasScheculeListRollingYearOneRendered (boolean val) {
        hasScheculeListRollingYearOneRendered = val;
    }
    
    public boolean hasScheculeListRollingYearTwoRendered = false;
    public boolean gethasScheculeListRollingYearTwoRendered () {
        
        if(revenueForRollingYearTwoWrapperList <> null && revenueForRollingYearTwoWrapperList.size() > 0 ) {
            hasScheculeListRollingYearTwoRendered  = true;
        }else{
            hasScheculeListRollingYearTwoRendered = false;
        }
        return hasScheculeListRollingYearTwoRendered ;
    }
    public void sethasScheculeListRollingYearTwoRendered (boolean val) {
        hasScheculeListRollingYearTwoRendered = val;
    }
    
    
    
    public class RevenueRollingYearWrapper {
        public RevenueSchedule__c revSchedule {get; set;}
        public List<productScheduleWrapper> scheduleList {get; set;}
        public integer index {get; set;}
        public integer rollYear {get; set;}
        
        public revenueRollingYearWrapper(integer indx){
            index = indx;
            rollYear = index + 1;
            revSchedule = new RevenueSchedule__c();
        }
        
        public boolean hasAutoPopulateDisabled = false;
        public boolean getHasAutoPopulateDisabled  () {
            
            if(scheduleList <> null && scheduleList.size() > 0 ) {
                hasAutoPopulateDisabled   = true;
            }else{
                hasAutoPopulateDisabled  = false;
            }
            return hasAutoPopulateDisabled  ;
        }
        public void setHasAutoPopulateDisabled  (boolean val) {
            hasAutoPopulateDisabled = val;
        }
    }   
    
    
    public class ProductScheduleWrapper {
        public ProductSchedule__c prodSchedule {get; set;}
        public Boolean selected {get; set;}
        public integer index {get; set;}
        public string formatedDate {get; set;}
        
        public productScheduleWrapper(ProductSchedule__c ps, integer indx) {
            prodSchedule = ps;
            selected = false;
            index = indx;
        }
    }
}