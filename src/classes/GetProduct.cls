/*
Created By - Abhishek Maurya
Date -15/10/208 
Lead - Mandeep Kaur
Description - this class is used to send the OpportunityLineItem data to client side controller in GetOLI Lightning component .  
 */

public with sharing class GetProduct {

    
     @auraEnabled
    public static List<OpportunityLineItem> getAllProduct(String oppId)
    {

       return [SELECT Id,Product_Name__c,Service_Line__c,Nature_of_Work__c,TCV__c,CYR__c FROM OpportunityLineItem WHERE OpportunityId=:oppId];        
    }
}