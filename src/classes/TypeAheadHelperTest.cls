/**************************************************************************************************************************
* @author   Persistent
* @description  - This class will handle testing TypeAheadHelper c
**************************************************************************************************************************/
@isTest
public class TypeAheadHelperTest {
    
    public static User u;
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles setting test data
* @return - void
**************************************************************************************************************************/
    
    public static void setUpData()
    {   
        u = TestDataFactoryUtility.createTestUser('System Administrator','a4c@bc.com','a4c@bc.com');
    }
    /**************************************************************************************************************************
* @author   Persistent
* @description  - Handles  testing TypeAheadHelper.searchDB mtd
* @return - void
**************************************************************************************************************************/
    
    public static testMethod void searchDB()
    {  
        setUpData();
        System.runAs(u){
            Test.startTest();
            String jsonString = TypeAheadHelper.searchDB('User','Name','username',4,'Name','','username=\'a4c@bc.com\'');
            Test.stopTest();
            system.assertNotEquals('',jsonString);
            system.assert(jsonString.contains('a4c@bc.com'));
        }
    }
    
}