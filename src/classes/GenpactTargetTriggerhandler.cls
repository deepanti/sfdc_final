public class GenpactTargetTriggerhandler {
    public static void runTrigger(){
        if(Trigger.isInsert && Trigger.isafter){
            ShareTargetRecorttouser((list<Target__c>) trigger.New);
        }
        if(Trigger.isUpdate && Trigger.isafter){        
            try{
               ShareTargetRecorttouser((list<Target__c>) trigger.New);
            }catch(Exception e){}
        }
    }
    
    public static void ShareTargetRecorttouser(List<Target__c> NewValue){
        List<Target__Share> targetSharesList = new List<Target__Share>();
        Group objGroup = null;
        try{
            objGroup = [SELECT Id, DeveloperName, (SELECT Id, UserOrGroupId FROM GroupMembers) 
                                    FROM Group 
                                    WHERE DeveloperName = 'VIC_HR_Users'];
        }catch(Exception ex){}
        for(Target__c tagetObj : NewValue){
            /*****USER ACCESS******/
            Target__Share targetShares = new Target__Share();
            targetShares.ParentId = tagetObj.Id;
            targetShares.AccessLevel = 'edit';
            if(tagetObj.User__c != null)
            targetShares.UserOrGroupId = tagetObj.User__c;
            targetShares.RowCause = Schema.Target__Share.RowCause.ProvideRecordAccessToUser__c;
            
            targetSharesList.add(targetShares);
            
            /*****SUPERVISOR ACCESS******/
            if(tagetObj.vic_Supervisor_Id__c != null){
                Target__Share objTS = new Target__Share();
                objTS.ParentId = tagetObj.Id;
                objTS.AccessLevel = 'edit';
                objTS.UserOrGroupId = tagetObj.vic_Supervisor_Id__c;
                objTS.RowCause = Schema.Target__Share.RowCause.ProvideRecordAccessToUser__c;
                targetSharesList.add(objTS);
            }
            try{                
                if(objGroup != null && objGroup.GroupMembers != null && objGroup.GroupMembers.size() > 0){
                    for(GroupMember gm: objGroup.GroupMembers){
                        Target__Share objTS = new Target__Share();
                        objTS.ParentId = tagetObj.Id;
                        objTS.AccessLevel = 'edit';
                        objTS.UserOrGroupId = gm.UserOrGroupId;
                        objTS.RowCause = Schema.Target__Share.RowCause.ProvideRecordAccessToUser__c;
                        targetSharesList.add(objTS);
                    }
                }
            }catch(Exception ex){}
        } 
        if(targetSharesList.size() > 0){
            System.debug('DATA-> ' + targetSharesList);
            Database.SaveResult[] sr = Database.insert(targetSharesList,false);   
        }
    }

}