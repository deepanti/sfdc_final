<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Pinnacle Transaction Object- 
GP_Timesheet_Entry__c 
Used for storing Transaction information of Timesheet Entry for Timesheet Transaction.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>false</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <externalSharingModel>ControlledByParent</externalSharingModel>
    <fields>
        <fullName>GP_Actual_Hours__c</fullName>
        <externalId>false</externalId>
        <label>Actual Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Batch_Name__c</fullName>
        <externalId>false</externalId>
        <formula>&apos;PINNORA&apos;+&apos;-&apos;+ GP_Timesheet_Transaction__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Batch Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Date__c</fullName>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>GP_Difference_Hours__c</fullName>
        <externalId>false</externalId>
        <formula>GP_Modified_Hours__c - GP_Actual_Hours__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Emp_Org_Id__c</fullName>
        <externalId>false</externalId>
        <label>Org Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Employee_Month_Year_Unique__c</fullName>
        <externalId>false</externalId>
        <formula>GP_Employee__r.GP_Person_ID__c +   GP_Timesheet_Transaction__r.GP_Month_Year__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Employee-Month-Year Unique</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Employee__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Employee</label>
        <referenceTo>GP_Employee_Master__c</referenceTo>
        <relationshipLabel>Timesheet Entries</relationshipLabel>
        <relationshipName>Timesheet_Entries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GP_Ex_Type__c</fullName>
        <externalId>false</externalId>
        <label>Ex. Type</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Billable</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non-Billable</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Non Billable</fullName>
                    <default>false</default>
                    <isActive>false</isActive>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>GP_Expenditure_Ending_Date__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>This field will always have the Date of coming Friday. If Date is on Saturday then, this field will have date of coming Friday.</inlineHelpText>
        <label>Expenditure Ending Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>GP_Modified_Hours__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Modified Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Oracle_Id__c</fullName>
        <externalId>false</externalId>
        <label>Oracle ID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Oracle_Process_Log__c</fullName>
        <externalId>false</externalId>
        <label>Oracle Process Log</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>GP_Oracle_Status__c</fullName>
        <externalId>false</externalId>
        <label>Oracle Status</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Project_Oracle_PID__c</fullName>
        <externalId>false</externalId>
        <label>Project Oracle PID</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Project_Task_Oracle_Id__c</fullName>
        <externalId>false</externalId>
        <label>Project Task Oracle Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Project_Task__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project Task</label>
        <referenceTo>GP_Project_Task__c</referenceTo>
        <relationshipLabel>Timesheet Entries</relationshipLabel>
        <relationshipName>Timesheet_Entries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GP_Project__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Project</label>
        <referenceTo>GP_Project__c</referenceTo>
        <relationshipLabel>Timesheet Entries</relationshipLabel>
        <relationshipName>Timesheet_Entries</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>GP_Timesheet_Transaction__c</fullName>
        <externalId>false</externalId>
        <label>Timesheet Transaction</label>
        <referenceTo>GP_Timesheet_Transaction__c</referenceTo>
        <relationshipLabel>Timesheet Entries</relationshipLabel>
        <relationshipName>Timesheet_Entries</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>true</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>GP_Transaction_Source__c</fullName>
        <defaultValue>&apos;BPR TSC&apos;</defaultValue>
        <externalId>false</externalId>
        <label>Transaction Source</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>GP_Unmatched_Negative_Txn_Flag__c</fullName>
        <externalId>false</externalId>
        <formula>IF( GP_Difference_Hours__c &lt; 0 ,&apos;Y&apos;,&apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Unmatched Negative Txn Flag</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Timesheet Entry</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <sharedTo>
            <group>GP_All_Pinnacle_SFDC_Users</group>
        </sharedTo>
    </listViews>
    <nameField>
        <displayFormat>TEN-{00000}</displayFormat>
        <label>Timesheet Entry Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Timesheet Entries</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
