import { LightningElement, api } from 'lwc';

export default class GpLWCPIDApproverProjectDetailContainerDetailPage extends LightningElement {
    @api sectionFields;
    @api key;
}