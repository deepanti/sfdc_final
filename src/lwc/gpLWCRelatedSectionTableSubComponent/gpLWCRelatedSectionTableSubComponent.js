import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
//import { navigateToObjectHome } from 'c/gpLWCBaseUtility';

export default class GpLWCRelatedSectionTableSubComponent extends NavigationMixin(LightningElement) {
    @api subTrDetails;
    @api sectionfields;
    @api key;
    @track _recordIdToShowOrHide;
    @api
    get recordIdToShowOrHide() {
        return this._recordIdToShowOrHide;
    }
    set recordIdToShowOrHide(value) {
        if(value) {
            this._recordIdToShowOrHide = value;
            this.handleTRShowHide();
        }
    }

    handleTRShowHide() {
        // toggle fields
        if(this.sectionfields) {
            (this.sectionfields).forEach(secf => {
                let selectDiv = this.template.querySelector('[data-div-id="'+ this.recordIdToShowOrHide + '_' + secf.fieldName + '"]');
                if(selectDiv && selectDiv.classList.contains('hide')) {
                    selectDiv.classList.remove('hide');
                } else if(selectDiv) {
                    selectDiv.classList.add('hide');
                }
            });
        }
        // Last action : so that value changes every time there is a icon click i.e if same one is clicked twice.
        // use custom event
        this.dispatchEvent(new CustomEvent('gpnullselectedrecord', {detail : undefined}));
        // Hence proved that its doesn't bind if 
        //this.recordIdToShowOrHide = undefined;
    }
    // Navigate to content doc.
    navigateToContentDoc(event) {
        var fileURL = event.currentTarget.getAttribute("data-attribute-index");
        const updatedListEvent =  new CustomEvent('gpfileurlsubtable', {detail : fileURL});
        this.dispatchEvent(updatedListEvent);
        //navigateToObjectHome(this, docId, 'ContentDocument', 'view');
    }
}