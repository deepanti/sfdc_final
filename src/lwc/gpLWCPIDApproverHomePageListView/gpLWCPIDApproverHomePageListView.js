import { LightningElement, track, api } from 'lwc';
//import recordCount from '@salesforce/label/c.GP_LWC_Record_Count';
//import recordCountList from '@salesforce/label/c.GP_LWC_Record_Count_List';
import { NavigationMixin } from 'lightning/navigation';
import { navigateToObjectHome } from 'c/gpLWCBaseUtility';

export default class GpLWCPIDApproverHomePageListView extends NavigationMixin(LightningElement) {

    @api _listOfFilterPIDs;
    @api listOfFilterPIDs;
    @track islistNotEmpty = false;
    @track showdata = false;
    @track listOfPaginatedPendingPIDs = [];
    /*@api 
    get listOfFilterPIDs() {
        return this.listOfPaginatedPendingPIDs;
    }
    set listOfFilterPIDs(value) {
        //this.setAttribute('listOfFilterPIDs', value);
        this.listOfOriginalPendingPIDs = value;
        this._listOfFilterPIDs = value;
        //this.getPaginatedPIDList(value);
    }*/
    @api 
    get currentFilterName() {
        return this._currentFilterName;
    }
    set currentFilterName(value) {
        if(value) {
            this._currentFilterName = value;
        } else {
            this._currentFilterName = 'All Projects';
        }
    }
    
    //@track _recordCount = recordCount;
    //@track _recordCountList = recordCountList;
    @track _recordCount = '';
    @track _recordCountList = '';
    @track _currentFilterName;    

    // STUFF START : Pagination stuff    
    @track listOfOriginalPendingPIDs = [];
    @track isPaginationEnabled;
    @track listOfCountPages = [];
    @track listOfCountsToShow = [];
    @track isShowPrevious;
    @track isShowNext;
    @track currentPageNo;
    @track totalPageCount;
    @track startCount = 0;
    @track endCount = 0;
    // STUFF END :Pagination 
    
    connectedCallback() {
        //this.islistNotEmpty = (this.listOfFilterPIDs !== undefined && this.listOfFilterPIDs.length > 0) ? true : false;
    }

    // Have to see to handle change in list here so that Pagination can be done.
    /*getPaginatedPIDList(value) {

        this.listOfPaginatedPendingPIDs = value.slice(0, Number(this._recordCount));

        if(value.length > Number(this._recordCount)) {
            this.isPaginationEnabled = true;
        } else {
            this.isPaginationEnabled = false;
        }

        if(this.isPaginationEnabled) {
            this.handleCounts(value);
        } else {
            // null all the pagination variables
        }

        if(value.length > 0) {
            this.showdata = true;
        } else {
            this.showdata = false;
        }
    }

    handleCounts(value) {

        var tempPageCount = value.length/Number(this._recordCount);
        tempPageCount = Math.floor(tempPageCount);

        if(tempPageCount*Number(this._recordCount) === value.length) {
            this.totalPageCount = tempPageCount;
        } else {
            this.totalPageCount = tempPageCount + 1;
        }

        this.currentPageNo = 1;
        this.startCount = 0;
        this.endCount = Number(this._recordCountList);
        this.isShowPrevious = false;        

        for (let index = this.currentPageNo; index <= this.totalPageCount; index++) {
            this.listOfCountPages.push(index);            
        }

        this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
        // Only two possible, 1- =to and 2- <than
        if(this.listOfCountsToShow.length < this.totalPageCount) {
            this.isShowNext = true;
        } else {
            this.isShowNext = false;
        }
    }

    handlePrevious() {
        this.currentPageNo = this.currentPageNo - 1;
        if(this.currentPageNo === 1) {
            this.isShowPrevious = false;
        }
        // 2, 3, 4 and totalPageCount = 4 then no Next but. Next but only when totalPageCount > 4        
        this.updateListOfCountPages('previous');
        if(this.endCount >= this.totalPageCount) {
            this.isShowNext = false;
        } else {
            this.isShowNext = true;
        }
        if(this.startCount === 0) {
            this.isShowPrevious = false;
        } else {
            this.isShowPrevious = true;
        }
        this.updateListOfPendingPIDs();
    }

    handleNext() {
        this.currentPageNo = Number(this.currentPageNo) + 1;

        if(this.currentPageNo >= this.totalPageCount) {
            //this.isShowNext = false;
            this.currentPageNo = this.totalPageCount;
        } else {
            this.currentPageNo = this.currentPageNo + 1;
        }
        //this.isShowPrevious = true;
        
        // 2, 3, 4 and totalPageCount = 4 then no Next but. Next but only when totalPageCount > 4
        if(this.currentPageNo+Number(this._recordCountList) >= this.totalPageCount) {
            this.isShowNext = false;
        } else {
            this.isShowNext = true;
        }
        this.updateListOfCountPages('next');
        if(this.endCount >= this.totalPageCount) {
            this.isShowNext = false;
        } else {
            this.isShowNext = true;
        }
        if(this.startCount === 0) {
            this.isShowPrevious = false;
        } else {
            this.isShowPrevious = true;
        }
        this.updateListOfPendingPIDs();
    }

    handleCurrentPage(event) {
        var index = event.currentTarget.getAttribute("data-attribute-index");
        var tempPageCount = (index-1)*Number(this._recordCount);
        this.currentPageNo = index;
        this.listOfPaginatedPendingPIDs = this.listOfOriginalPendingPIDs.slice(tempPageCount, tempPageCount + Number(this._recordCount));
    }

    updateListOfPendingPIDs() {
        var tempPageCount = (Number(this.currentPageNo)-1)*Number(this._recordCount);
        this.listOfPaginatedPendingPIDs = this.listOfOriginalPendingPIDs.slice(tempPageCount, tempPageCount + Number(this._recordCount));
    }

    updateListOfCountPages(type) {
        if(type === 'next') {
            if(Number(this.endCount) + 1 <= this.totalPageCount) {
                this.endCount = this.endCount + 1;
                this.startCount = this.startCount + 1;
                this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
            }
        } else {
            if(Number(this.startCount) - 1 !== -1) {
                this.startCount = this.startCount - 1;
                this.endCount = this.endCount - 1;
                this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
            }
        }
    }*/

    handleListUpdate(event) {
        const paginatedList = event.detail;
        this.listOfPaginatedPendingPIDs = paginatedList;

        if(this.listOfPaginatedPendingPIDs.length > 0) {
            this.showdata = true;
            //this.islistNotEmpty = true;
        } else {
            this.showdata = false;
            //this.islistNotEmpty = false;
        }
    }

    navigateToPID(event) {
        var recordId = event.currentTarget.getAttribute("data-attribute-index");
        navigateToObjectHome(this, recordId, 'GP_Project__c', 'view');
    }

    handleTRShowHide(event) {
        var recordId = event.currentTarget.getAttribute("data-attribute-index");

        let lightningIconRight = this.template.querySelector('[data-div-id="'+ recordId + '_utility_right"]');
        let lightningIconLeft = this.template.querySelector('[data-div-id="'+ recordId + '_utility_left"]');
        let selectDiv1 = this.template.querySelector('[data-div-id="'+ recordId + '_Oracle_PID_tr"]');
        let selectDiv2 = this.template.querySelector('[data-div-id="'+ recordId + '_Working_User_tr"]');
        let selectDiv3 = this.template.querySelector('[data-div-id="'+ recordId + '_Date_Submitted_tr"]');

        if(lightningIconRight && lightningIconLeft && lightningIconRight.classList.contains('hide')) {
            lightningIconRight.classList.remove('hide');
            lightningIconLeft.classList.add('hide');
        } else if(lightningIconRight && lightningIconLeft && lightningIconLeft.classList.contains('hide')) {
            lightningIconRight.classList.add('hide');
            lightningIconLeft.classList.remove('hide');
        }

        if(selectDiv1 && selectDiv1.classList.contains('hide')) {
            selectDiv1.classList.remove('hide');
        } else if(selectDiv1) {
            selectDiv1.classList.add('hide');
        }

        if(selectDiv2 && selectDiv2.classList.contains('hide')) {
            selectDiv2.classList.remove('hide');
        } else if(selectDiv2) {
            selectDiv2.classList.add('hide');
        }

        if(selectDiv3 && selectDiv3.classList.contains('hide')) {
            selectDiv3.classList.remove('hide');
        } else if(selectDiv3) {
            selectDiv3.classList.add('hide');
        }
    }
}