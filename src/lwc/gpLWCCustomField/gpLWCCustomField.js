import { LightningElement, api, track} from 'lwc';

export default class GpLWCCustomField extends LightningElement {
    @track textType = false;
    @track textAreaType = false;
    @track dateType = false;
    @track dateTimeType = false;
    @track booleanType = false;
    //@track currencyType = false;
    @track doubleType = false;
    @track emailType = false;
    //@track percentType = false;
    @track phoneType = false;
    @track _type;
    @track placeholder = 'Enter Label..';    
    @api readonly;
    @api editable;
    @api disabled;
    @api label = 'Label';
    @api value = 'value and can take any type, its javascript bro';
    @api fieldId = 'name';    
    @api 
    get type() {
        return this._type;
    }
    set type(value) {
        //this.setAttribute('type', value);
        this._type = value;
        this.determineTypeOfField(value);
    }

    determineTypeOfField(value) {
        
        if(value && value === 'String') {
            this.textType = true;
        }
        if(value && value === 'TextArea') {
            this.textAreaType = true;
        }
        if(value && value === 'Boolean') {
            this.booleanType = true;
            //this.value = this.value ? "checked" : undefined;
        }
        if(value && value === 'Picklist') {
            this.textType = true;
            //this.value = this.value ? "checked" : undefined;
        }
        if(value && value === 'Currency') {
            this.doubleType = true;
        }
        if(value && value === 'Date') {
            this.dateType = true;
        }
        if(value && value === 'DateTime') {
            this.dateTimeType = true;
        }
        if(value && value === 'Double') {
            this.doubleType = true;
        }
        if(value && value === 'Email') {
            this.emailType = true;
        }
        if(value && value === 'Percent') {
            this.doubleType = true;
        }
        if(value && value === 'Phone') {
            this.phoneType = true;
        }
        if(value && value === 'anytype') {
            this.textType = true;
        }
        this.placeholder = 'Enter ' + this.label + 's..';
    }
}