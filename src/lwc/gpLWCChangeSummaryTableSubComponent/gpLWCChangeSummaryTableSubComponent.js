import { LightningElement, track, api } from 'lwc';

export default class GpLWCChangeSummaryTableSubComponent extends LightningElement {
    @api key;
    @api details;
    @track islistNotEmpty = false;
    @track listOfPaginatedRecords = [];

    connectedCallback() {
        this.islistNotEmpty = this.details.length > 0 ? true:false;
    }
    // Handle Pagination list
    handleListUpdate(event) {
        this.listOfPaginatedRecords = event.detail;
    }
}