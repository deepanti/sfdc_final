import { LightningElement, track, api } from 'lwc';

export default class GpLWCPIDApproverProjectChangeSummary extends LightningElement {
    @track message;
    @api changeSummarySections = [];
    @api changeSummarySectionDetails = {};
    @api changeSummaryErrorMessage = '';
    @track relatedSections = [];
    @track changeSummaryToShow = [];
    @track listOfPaginatedRecords = [];
    @track previoussectionname = '';
    @track sectionname = '';
    @track jsonStr;
    @track islistNotEmpty = false;
    @track isDetailed = false;

    connectedCallback() {
        (this.changeSummarySections).forEach(css => {
            let c = {};
            c.isDetailed = true;
            if(css === 'Project') {
                c.key = 'projectChangeList';
                c.keyid = 'projectChangeList_div';
                c.isDetailed = false;
            } else if(css === 'Project Address') {
                c.key = 'projectAddressChangeList';
                c.keyid = 'projectAddressChangeList_div';
            } else if(css === 'Work Location & SDO') {
                c.key = 'projectWorkLocationChangeList';
                c.keyid = 'projectWorkLocationChangeList_div';
            } else if(css === 'Project Leadership') {
                c.key = 'projectLeadershipChangeList';
                c.keyid = 'projectLeadershipChangeList_div';
            } else if(css === 'Billing Milestone') {
                c.key = 'projectBillingChangeList';
                c.keyid = 'projectBillingChangeList_div';
            } else if(css === 'Resource Allocation') {
                c.key = 'projectResourceChangeList';
                c.keyid = 'projectResourceChangeList_div';
            } else if(css === 'Budget') {
                c.key = 'projectBudgetChangeList';
                c.keyid = 'projectBudgetChangeList_div';
            } else if(css === 'Expense') {
                c.key = 'projectExpenseChangeList';
                c.keyid = 'projectExpenseChangeList_div';
            } else if(css === 'Document') {
                c.key = 'projectDocumentChangeList';
                c.keyid = 'projectDocumentChangeList_div';
            } else if(css === 'Profile Bill Rate') {
                c.key = 'projectProfileBillRateChangeList';
                c.keyid = 'projectProfileBillRateChangeList_div';
            }
            c.value = css;
            this.relatedSections.push(c);
        });
    }
    // handle section click
    handleChangeSection(event) {
        let _sectionname = event.currentTarget.getAttribute("data-attribute-index");
        
        if(this.sectionname === '' || this.previoussectionname !== _sectionname) {
            this.sectionname = _sectionname;
            // remove class to show the current section as active and previous as inactive.
            let divIdCurrent = '[data-div-id=\'' + _sectionname + '\']';
            let divIdPre = '[data-div-id=\'' + this.previoussectionname + '\']';
            let selectDivCurrent = this.template.querySelector(divIdCurrent);
            let selectDivPre = this.template.querySelector(divIdPre);
            if(selectDivCurrent) {
                selectDivCurrent.classList.remove('collapsed');
                // Scroll the control to the selected element: not to use as it adds blue section at the bottom of the page.
                //selectDivCurrent.scrollIntoView();
                //location.href = '#';
                //location.href = '#' + _sectionname + '_div';
            }
            if(selectDivPre && this.previoussectionname !== _sectionname) {
                selectDivPre.classList.add('collapsed');
            }            
            (this.relatedSections).forEach(css => {
                if(css.key === this.sectionname) {
                    css.isSelected = true;
                    //css.changeSummaryToShow = this.changeSummarySectionDetails[this.sectionname];
                    this.changeSummaryToShow = this.changeSummarySectionDetails[this.sectionname];
                    this.isDetailed = css.isDetailed;
                } else {
                    css.isSelected = false;
                }
            });
        } else {
            this.sectionname = '';
            this.sectionFields = [];

            (this.relatedSections).forEach(css => {
                css.isSelected = false;
                //css.changeSummaryToShow = [];
            });
            // Empty the array as nothing to show.
            this.changeSummaryToShow = [];
            this.listOfPaginatedRecords = [];
            // add class to show the section as inactive.
            let divId = '[data-div-id=\'' + _sectionname + '\']';
            let selectDiv = this.template.querySelector(divId);
            if(selectDiv) {
                selectDiv.classList.add('collapsed');
            }
        }
        this.previoussectionname = _sectionname;
        this.islistNotEmpty = (!this.isDetailed && this.changeSummaryToShow.length > 0) ? true:false;
    }
    // Handle Pagination list
    handleListUpdate(event) {
        this.listOfPaginatedRecords = event.detail;
    }
}