import { LightningElement, api } from 'lwc';
import customSRImages from '@salesforce/resourceUrl/GPM_Approver_App_Images';
import customSRAssets from '@salesforce/resourceUrl/GPM_Approver_App_Assets';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';

export default class GpTestCustom extends LightningElement {
    //@api footerImageUrl = customSRImages + '/images/GP_footer.png';
    @api backImageUrl = customSRImages + '/images/arrow-left.png';    
    @api recordId;
    
    connectedCallback() {
        Promise.all([
            loadScript(this, customSRAssets + '/assets/jquery.min.javascript.js'),
            loadScript(this, customSRAssets + '/assets/jquery.dataTables.min.js'),
            loadStyle(this, customSRAssets + '/assets/bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/bootstrap2.min.css'),
            loadStyle(this, customSRAssets + '/assets/dataTables.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/responsive.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/font-awesome.min.css'),
            loadStyle(this, customSRAssets + '/assets/jquery.dataTables.min.css'),
            loadStyle(this, customSRAssets + '/assets/ds_custom.css'),
        ])
        .then(() => {
            // eslint-disable-next-line no-alert
            //alert('File uploaded');
        })
        .catch(error => {            
            if(error) {
                // eslint-disable-next-line no-alert
                alert(error);
            }            
        });
    }
}