/* eslint-disable no-undef */
import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { formatDateTime } from 'c/gpLWCBaseUtility';
import { formatNumber } from 'c/gpLWCBaseUtility';
//import { navigateToObjectHome } from 'c/gpLWCBaseUtility';

export default class GpLWCPIDApproverProjectRelatedSectionPage extends NavigationMixin(LightningElement) {
    @api relatedObjectRecordsToShow
    @api sectionfields;
    @api lookupMappings;
    @track firstSectionNameMobile;
    @track secondSectionNameMobile;
    @track _sectionfields = [];
    @track _sectionfieldsMob = [];
    @api _relatedObjectRecordsToShow = [];
    @api _relatedObjectRecordsToShowMob = [];
    @track listOfPaginatedRecords = [];
    @track islistNotEmpty = false;
    @track jsonStr;
    @track showListOfRecords;
    @api recordIdToShowOrHide;

    connectedCallback() {
        this.dataSetForMobile();
        //this.dataSetForDesktop();
        //this.jsonStr = JSON.stringify(this._relatedObjectRecordsToShowMob);
        this.islistNotEmpty = this._relatedObjectRecordsToShowMob.length > 0 ? true:false;
    }

    dataSetForMobile() {
        let count = 0;
        // Add only 2 table cols
        (this.sectionfields).forEach(secf => {
            count++;
            if(count === 1) {
                this.firstSectionNameMobile = secf;
            } else if(count === 2) {
                this.secondSectionNameMobile = secf;
            }
        });

        let _relatedRecords = [];
        this.relatedObjectRecordsToShow = [...this.relatedObjectRecordsToShow];
        (this.relatedObjectRecordsToShow).forEach(rel => {
            let record = rel;
            let countTr = 0;
            let recObj = {};
            recObj.key = record.Id;
            recObj.utilityRight = record.Id + '_utility_right';
            recObj.utilityLeft = record.Id + '_utility_left';
            recObj.fieldsHeader = [];
            recObj.fields = [];
            recObj.showIcon = false;
            recObj.hiddenDivId = record.Id + '_hidden_div';
            
            (this.sectionfields).forEach(sec => {
                countTr++;
                // Here dataType is also in sec
                let secField = {};
                secField.key = sec.fieldName;
                secField.viewDocument = sec.viewDocument;

                // Check if value exist for the field.
                if(record[sec.fieldName]) {
                    secField.value = record[sec.fieldName];
                    // if there is some prefix tagged to the field.
                    if(sec.valuePreFix) {
                        secField.value = sec.valuePreFix + ' ' + formatNumber(secField.value);
                    }
                    // Format date/datetime
                    if(sec.dataType === 'Date') {
                        secField.value = formatDateTime(secField.value, false);
                    } else if(sec.dataType === 'DateTime') {
                        secField.value = formatDateTime(secField.value, true);
                    } else if(sec.dataType === 'Currency' || sec.dataType === 'Double') {
                        secField.value = formatNumber(secField.value);
                    }
                    // for checkbox, value is already boolean type.
                    if(sec.dataType === 'Boolean') {
                        secField.isCheckBox = true;
                        secField.iconname = 'action:check';
                    }
                } else {
                    secField.value = undefined;
                }
                
                if(sec.dataType === 'Reference') {
                    secField.isReference = true;
                    secField.searchfield = 'Name';
                    secField.referenceName = sec.referenceName;
                    secField.displayTextFields = sec.displayTextFields;
                    secField.placeholderValue = sec.placeholderValue;
                } else {
                    secField.isField = true;
                }

                // if value is null and datatype is Boolean then show unchecked icon
                if(!record[sec.fieldName] && sec.dataType === 'Boolean') {
                    secField.isField = false;
                    secField.isReference = false;
                    secField.isNotChecked = true;
                    secField.iconname = 'utility:record';
                }

                // Using Picklist 
                if(sec.fieldName === 'GP_CC2__c') {
                    secField.isPicklist = true;
                    secField.isField = false;
                    secField.isReference = false;
                    secField.isNotChecked = false;
                    secField.isNotReadonly = false;
                    secField.objectname = 'GP_Resource_Allocation__c';
                    secField.fieldname = 'GP_CC2__c';
                    secField.fieldLabel = 'COST CENTER (CC2)';
                }

                // first two as headers and rest will be like table Tr
                if(countTr <= 2) {
                    recObj.fieldsHeader.push(secField);
                } else {
                    secField.fieldTr = record.Id + '_' + sec.fieldName;
                    secField.fieldLabel = sec.fieldLabel;
                    recObj.fields.push(secField);
                    recObj.showIcon = true;
                }
            });
            _relatedRecords.push(recObj);
        });
        this._relatedObjectRecordsToShowMob = _relatedRecords;
    }

    /*dataSetForDesktop() {
        this._sectionfields = this.sectionfields;
        let _relatedRecords = [];
        this.relatedObjectRecordsToShow = [...this.relatedObjectRecordsToShow];
        (this.relatedObjectRecordsToShow).forEach(rel => {
            let record = rel;
            let recObj = {};
            recObj.key = record.Id;
            recObj.fields = [];
            (this._sectionfields).forEach(sec => {
                // Here dataType is also in sec
                let secField = {};
                secField.key = sec.fieldName;
                secField.value = record[sec.fieldName];
                if(sec.dataType === 'Reference') {
                    secField.isReference = true;
                    secField.referenceName = sec.referenceName;
                    secField.displayTextFields = sec.displayTextFields;
                    secField.placeholderValue = sec.placeholderValue;
                } else {
                    secField.isField = true;
                }
                recObj.fields.push(secField);
            });
            _relatedRecords.push(recObj);
        });
        this._relatedObjectRecordsToShow = _relatedRecords;        
    }*/

    /*renderedCallback() {        
        $(document).ready( function () {
            $('#searchTable3').addClass("blue");
            $('.serachTable3').DataTable({
                "dom": "<'row'<'col-sm-3 col-xs-3'l><'col-sm-9 col-xs-9'f>>",
                "language": { "search": "" },
                "searching": true,
                "paging": true, 
                "info": false, 
                "ordering": false, 
                "lengthChange": true
            });
            //alert("Bro in the hole, enjoying the hole");
        } );
    }*/

    handleListUpdate(event) {
        const paginatedList = event.detail;
        this.showListOfRecords = false;
        this.listOfPaginatedRecords = paginatedList;
        this.showListOfRecords = true;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        /*setInterval(function() {
            this.showListOfRecords = true;
        }, 3000);*/
        //this.jsonStr = JSON.stringify(this.listOfPaginatedRecords);
    }

    handleTRShowHide(event) {
        var recordId = event.currentTarget.getAttribute("data-attribute-index");
        this.recordIdToShowOrHide = recordId;

        let lightningIconRight = this.template.querySelector('[data-div-id="'+ recordId + '_utility_right"]');
        let lightningIconLeft = this.template.querySelector('[data-div-id="'+ recordId + '_utility_left"]');

        // toggle icon
        if(lightningIconRight && lightningIconLeft && lightningIconRight.classList.contains('hide')) {
            lightningIconRight.classList.remove('hide');
            lightningIconLeft.classList.add('hide');
        } else if(lightningIconRight && lightningIconLeft && lightningIconLeft.classList.contains('hide')) {
            lightningIconRight.classList.add('hide');
            lightningIconLeft.classList.remove('hide');
        }
        // toggle fields
        /*(this.sectionfields).forEach(secf => {
            let selectDiv = this.template.querySelector('[data-div-id="'+ recordId + '_' + secf.fieldName + '"]');
            if(selectDiv && selectDiv.classList.contains('hide')) {
                selectDiv.classList.remove('hide');
            } else if(selectDiv) {
                selectDiv.classList.add('hide');
            }
        });*/
    }
    // Null the selected record so that when clicked again on the icon, icon and sub Tr toggles.
    handleNullSelectedRecord(event) {
        this.recordIdToShowOrHide = event.detail;
    }
    // Navigate to content doc.
    navigateToContentDoc(event) {
        var fileURL = event.currentTarget.getAttribute("data-attribute-index");
        const updatedListEvent =  new CustomEvent('gpfileurl', {detail : fileURL});
        this.dispatchEvent(updatedListEvent);
        //navigateToObjectHome(this, docId, 'ContentDocument', 'view');
    }
    // handle file url from sub table
    handleFileURLSubtable(event) {
        const updatedListEvent =  new CustomEvent('gpfileurl', {detail : event.detail});
        this.dispatchEvent(updatedListEvent);
    }
}