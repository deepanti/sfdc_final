import { LightningElement, track} from 'lwc';
//import { NavigationMixin } from 'lightning/navigation';
//import { handleSuccessCallBack } from 'c/gpLWCBaseUtility';
import { handleFailedCallBack } from 'c/gpLWCBaseUtility';
import getAllPendingForApprovalPIDsOfUser from '@salesforce/apex/GPLWCServicePIDApproverHomePage.getAllPendingForApprovalPIDsOfUser';
import customSRAssets from '@salesforce/resourceUrl/GPM_Approver_App_Assets';
import { loadStyle } from 'lightning/platformResourceLoader';
// NavigationMixin(LightningElement)
// loadScript
// GPM_Approver_App_Assets
export default class GpLWCPIDApproverHomePage extends LightningElement {
    @track error;
    @track stack;
    @track listOfAllPendingPIDs = [];
    @track isShowSpinner = false;
    @track showComponent = false; 

    /*constructor() {
        super();
        this.currentFilter = 'all'; // all, new, updated
    }*/
    // Life cycle hook fires when a component is inserted into the DOM
    connectedCallback() {
        this.isShowSpinner = true;
        this.getAllLoggedInUsersPendingPIDs();
    }
    // Handle lwc errors in child components.
    errorCallback(error, stack) {
        this.error = error;
        this.stack = stack;
    }
    renderedCallback() {

        Promise.all([
            // NP
            //loadScript(this, customSRAssets + '/assets/jquery.min.javascript.js'),
            // NP
            //loadScript(this, customSRAssets + '/assets/jquery.dataTables.min.javascript.js'),
            loadStyle(this, customSRAssets + '/assets/bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/bootstrap2.min.css'),
            loadStyle(this, customSRAssets + '/assets/dataTables.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/responsive.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/font-awesome.min.css'),
            loadStyle(this, customSRAssets + '/assets/ds_custom.css'),
        ])
        .then(() => {
            // eslint-disable-next-line no-alert
            //alert('File uploaded');
        })
        .catch(error => {
            // eslint-disable-next-line no-alert
            alert(error);
        });
    }
    // Get all pending for Approval PIDs of logged in User.
    getAllLoggedInUsersPendingPIDs() {
        getAllPendingForApprovalPIDsOfUser()
        .then(result => {
                var responseData = result;
                this.isShowSpinner = false;
                if(responseData.isSuccess) {
                    this.getAllPendingForApprovalPIDsOfUserHandler(responseData);
                    this.showComponent = true;
                } else {
                    handleFailedCallBack(this, responseData.message);
                }
           }).catch(error => { handleFailedCallBack(this, error.message); });
    }

    getAllPendingForApprovalPIDsOfUserHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        this.listOfAllPendingPIDs = response; 
    }
}