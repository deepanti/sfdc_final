import { LightningElement, api } from 'lwc';

export default class GpLWCPaginationCount extends LightningElement {
    @api count;
    @api currentPageNo;
    connectedCallback() {
        if(this.count === this.currentPageNo) {
            const callParentLWC = new CustomEvent('gplwccount', {detail : undefined});
            this.dispatchEvent(callParentLWC);
        }        
    }
}