/* eslint-disable no-alert */
/* eslint-disable dot-notation */
/* eslint-disable no-loop-func */
/* 
    Get PID, Addressed, and leadership data
    Get meta data from template of PID
    Get Related object list per PID.
    Get PID detail section sequence from base lwc.
    Highlight Project View section.
    Just show sections per PID. Once user clicks on section, get corresponding fields from apex. We already have data here.
    Iterate to show only visible fields which we will get from metadata.
    ==> To send params, keep var @api in both parent and child and don't use get/set for that @api else loop
    ==> Simply get that in connectedCallBack function and do that honors.
*/
import { LightningElement, track, api} from 'lwc';
import fetchPIDData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchPIDData';
import fetchWorkLocationSDOData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchWorkLocationSDOData';
import fetchProjectLeadershipSDOData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchProjectLeadershipSDOData';
import fetchResourceAllocationData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchResourceAllocationData';
import fetchBillingMilestoneData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchBillingMilestoneData';
import fetchDocumentData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchDocumentData';
import fetchProfileBillRateData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchProfileBillRateData';
import fetchBudgetAndExpensesData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchBudgetAndExpensesData';
import fetchChangeSummaryData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchChangeSummaryData';
import fetchPIDSummaryData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchPIDSummaryData';

import { pinnacleheadersLabel } from 'c/gpLWCBaseUtility';
import { pinnacleSectionSequence } from 'c/gpLWCBaseUtility';
import { pinnacleLookupFieldsMapping } from 'c/gpLWCBaseUtility';
import { pinnacleFieldLabel } from 'c/gpLWCBaseUtility';
//import { projectCurrencyMapping } from 'c/gpLWCBaseUtility';
//import { billingCurrencyMapping } from 'c/gpLWCBaseUtility';
import { handleSpinner } from 'c/gpLWCBaseUtility';
import { formatDateTime } from 'c/gpLWCBaseUtility';
import { formatNumber } from 'c/gpLWCBaseUtility';
import fetchFieldSetPerObject from '@salesforce/apex/GPLWCServiceCustomComponent.fetchFieldSetPerObject';

export default class GpLWCPIDApproverProjectDetailContainer extends LightningElement {
    
    @track error;
    @track relatedsections = [];    
    @track sectionsToShow = [];
    @track isNoSectionPerObject = false;    
    @track defaultsection;    
    @track pidObj;
    @track objectName = 'GP_Project__c';
    @track fieldSetName = '';
    
    @track metadataOfPID ;
    @track mapOfEssentials;
    @track projectconfig = {};
    @track relatedObjectToShow = '';
    @track _relatedObjectToShow_Key = '';
    @track _headerLabelJson = {};
    @track _headerLabelSequenceJson = {};
    @track _lookupMappings = {};
    @track _fieldLabelJson = {};
    @track _projectCurrenyJson = {};
    @track _billingCurrenyJson = {};
    @api sectionFields = [];
    @api recordId;
    @track sectionname = '';
    @track previoussectionname = '';
    @track previousRelatedSection = 'Project';
    @track isForm = true;
    @track objectSelected = '';
    @api relatedObjectRecordsToShow = [];
    @track projectaddress;
    @track projectmandatorykeymembers;
    @track isGstApplicableTaxCodeVisible = false;
    @track projectTCV;
    @track projectCurrency;
    @track projectBillingCurrency;
    @track listOfWorkLocations = [];
    @track listOfSDOs = [];
    @track listOfAccountLeadership = [];
    @track listOfHSLLeadership = [];
    @track listOfRegionalLeadership = [];
    @track listOfAdditionalLeadership = [];
    @track listOfPODocuments = [];
    @track listOfOtherDocuments = [];
    @track listOfExpenses = [];
    @track listOfBudgetAndPricings = [];
    // Change summary : 13-06-2020
    @track isChangeSummary = false;
    @api changeSummarySections = [];
    @api changeSummarySectionDetails = {};
    @api changeSummaryErrorMessage = '';
    @track jsonStr;
    // PID Summary : 01-07-2020
    @track isPIDSummary = false;
    //@api pidSummarySections = [];
    @api pIDSummarySectionDetails = [];
    // View file
    @track fileURL;
    @track isShowPopup = false;
    // Note on Upload view : 10-09-2020
    @track isPIDDocNote = false;
    @track noteOnUploadView = 'ONLY PDF DOCUMENTS CAN WE VIEWED.';    
    @track isBillingMilestonesNote = false;
    @track billingMilestoneNote = '';
    @track isBudgetNote = false;
    @track budgetNote = '';
    @track isExpensesNote = false;
    @track expensesNote = '';
    @track isBudgetAndExpensesNote = false;
    @track pidBillingCurrency = '';

    connectedCallback () {
        this.defaultsection = 'Project';
        this.parseLabels();
        this.fetchPIDDetails();
    }
    // when all component got rendered fully
    renderedCallback() {
        this.highlightSection(this.previousRelatedSection);
    }

    parseLabels() {
        this._headerLabelJson = pinnacleheadersLabel();
        this._headerLabelSequenceJson = pinnacleSectionSequence();
        this._lookupMappings = pinnacleLookupFieldsMapping();
        this._fieldLabelJson = pinnacleFieldLabel();
        //this._projectCurrenyJson = projectCurrencyMapping();
        //this._billingCurrenyJson = billingCurrencyMapping();
    }

    fetchPIDDetails() {
        fetchPIDData({ recordId : this.recordId, objectName : this.objectName})
        .then(result => {
            var responseData = result;
            if(responseData.isSuccess) {
                this.fetchPIDDataHandler(responseData, true);
            }
        })
        .catch(error => {
            this.error = error;
        });
    }

    fetchPIDDataHandler(responseData, isUpdateRelatedSection) {
        var response = JSON.parse(responseData.response) || {};
        
        this.pidObj = response.projectdata;
        this.metadataOfPID = response.mapofmetadata;
        if(isUpdateRelatedSection) {
            this.setRelatedSections(response.relatedsections);
            this.mapOfEssentials = response.mapOfEssentials;
            this.isGstApplicableTaxCodeVisible = response.isGstApplicableTaxCodeVisible;
        }        
        //this.relatedsections = response.relatedsections;
        this.projectaddress = response.projectaddress;
        this.projectmandatorykeymembers = response.projectmandatorykeymembers;        
        this.projectTCV = this.pidObj.GP_TCV__c;

        this.parseMetaData();
    }

    setRelatedSections(relatedsections) {
        (relatedsections).forEach(rs => {
            let rsObj = {};
            rsObj.key = rs;
            rsObj.value = this._headerLabelJson[rs];
            this.relatedsections.push(rsObj);
        });
    }

    parseMetaData() {
        const metadata = this.metadataOfPID;
        let metadataobj = {...metadata};
        let _sectionsToShow = {};
        
        for(let key in metadataobj) {
            if(key === 'Project') {
                this._relatedObjectToShow_Key = key;
                this.relatedObjectToShow = this._headerLabelJson[key];
                let pidObj = {};
                pidObj.key = key;
                pidObj.sections = {};
                for(let sectionkey in metadataobj[key]) {
                    if(sectionkey !== '') {
                        _sectionsToShow[sectionkey] = true;
                        let pidData = {};
                        for(let fieldKey in metadataobj[key][sectionkey]) {
                            if(fieldKey !== '') {
                                pidData[metadataobj[key][sectionkey][fieldKey]] = fieldKey;
                            }
                        }
                        pidObj.sections[sectionkey] = pidData;
                    }
                }
                this.projectconfig = pidObj;
                // Get the visible sequencial section list
                if(this._headerLabelSequenceJson[key]) {
                    let seqSectionsList = this._headerLabelSequenceJson[key].split(',');
                    seqSectionsList = [...seqSectionsList];
                    // null the section list first and then fill it freshly.
                    this.sectionsToShow = [];
                    (seqSectionsList).forEach(sec => {
                        if(_sectionsToShow[sec]) {
                            let secObj = {};
                            secObj.key = sec;
                            secObj.value = this._headerLabelJson[sec];
                            secObj.isSelected = false;
                            this.sectionsToShow.push(secObj);
                        }
                    });
                }                
            }
        }
    }
    // Highlight section
    highlightSection(sectionkey) {
        (this.relatedsections).forEach(rs => {
            let divId = '[data-div-id="' + rs.key + '"]';
            let selectDiv = this.template.querySelector(divId);
            if(selectDiv) {
                selectDiv.classList.remove('active');
            }            
        });

        let divId = '[data-div-id=\'' + sectionkey + '\']';
        let selectDiv = this.template.querySelector(divId);

        if(selectDiv) {
            selectDiv.classList.add('active');
        }
    }
    // Handle when related objects get clicked by user.
    handleRelatedSection(event) {
        //const relatedSection = event.target.value;
        const relatedSection = event.currentTarget.getAttribute("data-attribute-index");
        if(relatedSection !== this.previousRelatedSection) {
            // Flush the existing sections/object details first or show spinner.
            //this.highlightSection(relatedSection);
            this.flushTheData();
            this.fetchRelatedObjectData(relatedSection);
            this.relatedObjectToShow = this._headerLabelJson[relatedSection];
        }
        this.previousRelatedSection = relatedSection;        
    }
    // Flush all related data.
    flushTheData() {
        this.sectionsToShow = [];
        this.isNoSectionPerObject = false;        
        this.relatedObjectRecordsToShow = [];
        this.projectaddress = [];
        this.projectmandatorykeymembers = [];
        this.isGstApplicableTaxCodeVisible = false;
        this.listOfWorkLocations = [];
        this.listOfSDOs = [];
        this.listOfAccountLeadership = [];
        this.listOfHSLLeadership = [];
        this.listOfRegionalLeadership = [];
        this.listOfAdditionalLeadership = [];
        this.listOfPODocuments = [];
        this.listOfOtherDocuments = [];
        this.listOfExpenses = [];
        this.listOfBudgetAndPricings = [];
        this.isChangeSummary = false;
        this.changeSummarySections = [];
        this.changeSummarySectionDetails = {};
        this.changeSummaryErrorMessage = '';
        this.isPIDSummary = false;
        this.pIDSummarySectionDetails = [];
        this.isPIDDocNote = false;
        this.isBillingMilestonesNote = false;
        this.billingMilestoneNote = '';
        this.isBudgetNote = false;
        this.budgetNote = '';
        this.isExpensesNote = false;
        this.expensesNote = '';
        this.isBudgetAndExpensesNote = false;
    }
    // Handle when section get clicked by user.
    handleSectionFields(event) {
        //const sectionname = event.details; - via customevent
        //const relatedSection = event.target.value; - for simple div, input value
        let _sectionname = event.currentTarget.getAttribute("data-attribute-index"); // for specific attribute value
        let _fieldSetName = '';
        let _objectName = '';
        
        //this.sectionname = _sectionname;
        // If click second time on the section, section will be closed.
        // Keep previous clicked section too.
        if(this.sectionname === '' || this.previoussectionname !== _sectionname ) {
            this.sectionname = _sectionname;
            // remove class to show the current section as active and previous as inactive.
            let divIdCurrent = '[data-div-id=\'' + _sectionname + '\']';
            let divIdPre = '[data-div-id=\'' + this.previoussectionname + '\']';
            let selectDivCurrent = this.template.querySelector(divIdCurrent);
            let selectDivPre = this.template.querySelector(divIdPre);
            if(selectDivCurrent) {
                selectDivCurrent.classList.remove('collapsed');
            }
            if(selectDivPre && this.previoussectionname !== _sectionname) {
                selectDivPre.classList.add('collapsed');
            }
        } else {
            this.sectionname = '';
            this.sectionFields = [];

            (this.sectionsToShow).forEach(sec => {
                sec.isSelected = false;
                /*if(sec.key !== 'Address' && sec.key !== 'MandatoryKeyMembersLeadership') {
                    sec.isForm = true;
                    this.isForm = true;
                } else {
                    sec.isForm = false;
                    this.isForm = false;
                }*/
            });
            // add class to show the section as inactive
            let divId = '[data-div-id=\'' + _sectionname + '\']';
            let selectDiv = this.template.querySelector(divId);
            if(selectDiv) {
                selectDiv.classList.add('collapsed');
            }
        }
        this.previoussectionname = _sectionname;
        if(this.sectionname === 'Bussiness_Hierarchy') {
            _fieldSetName = 'GP_LWC_Business_Product_Hierarchy_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'Project_Info') {
            _fieldSetName = 'GP_LWC_Project_Info_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        }        
        else if(this.sectionname === 'Address') {
            _fieldSetName = 'GP_LWC_Project_Address_Fields';
            _objectName = 'GP_Project_Address__c';
            this.isForm = false;
        } else if(this.sectionname === 'MandatoryKeyMembersLeadership') {
            _fieldSetName = 'GP_LWC_Project_Leadership_Fields';
            _objectName = 'GP_Project_Leadership__c';
            this.isForm = false;
        }
        else if(this.sectionname === 'CMITS_Fields') {
            _fieldSetName = 'GP_LWC_CMITS_Specific_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'OtherClassification') {
            _fieldSetName = 'GP_LWC_Other_Classification_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'DispatchInfo') {
            _fieldSetName = 'GP_LWC_Dispatch_Info_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'GEProjectDetails') {
            _fieldSetName = 'GP_LWC_GE_Project_Detail_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'OracleStatus') {
            _fieldSetName = 'GP_LWC_Oracle_Status_Fields';
            _objectName = 'GP_Project__c';
            this.isForm = true;
        } else if(this.sectionname === 'WorkLocation') {
            _fieldSetName = 'GP_LWC_Work_Location_Fields';
            _objectName = 'GP_Project_Work_Location_SDO__c';
            this.isForm = false;
        } else if(this.sectionname === 'AdditionalSDO') {
            _fieldSetName = 'GP_LWC_SDO_Fields';
            _objectName = 'GP_Project_Work_Location_SDO__c';
            this.isForm = false;
        } else if(this.sectionname === 'RegionalLeadership') {
            _fieldSetName = 'GP_LWC_Project_Leadership_Fields';
            _objectName = 'GP_Project_Leadership__c';
            this.isForm = false;
        } else if(this.sectionname === 'AccountLeadership') {
            _fieldSetName = 'GP_LWC_Project_Leadership_Fields';
            _objectName = 'GP_Project_Leadership__c';
            this.isForm = false;
        } else if(this.sectionname === 'HSLLeadership') {
            _fieldSetName = 'GP_LWC_Project_Leadership_Fields';
            _objectName = 'GP_Project_Leadership__c';
            this.isForm = false;
        } else if(this.sectionname === 'AdditionalAccessLeadership') {
            _fieldSetName = 'GP_LWC_Project_Leadership_Fields';
            _objectName = 'GP_Project_Leadership__c';
            this.isForm = false;
        } else if(this.sectionname === 'PODocument') {
            _fieldSetName = 'GP_LWC_PO_Document_Fields';
            _objectName = 'GP_Project_Document__c';
            this.isForm = false;
        } else if(this.sectionname === 'OtherDocument') {
            _fieldSetName = 'GP_LWC_Other_Document_Fields';
            _objectName = 'GP_Project_Document__c';
            this.isForm = false;
        } else if(this.sectionname === 'Expenses') {
            _fieldSetName = 'GP_LWC_Expense_Fields';
            _objectName = 'GP_Project_Expense__c';
            this.isForm = false;
        } else if(this.sectionname === 'Budget_Pricing') {
            _fieldSetName = 'GP_LWC_Budget_Fields';
            _objectName = 'GP_Project_Budget__c';
            this.isForm = false;
        }

        // object we are detailing with.
        this.objectSelected = _objectName;
        // get fields from fieldset
        if(_fieldSetName && _objectName) {
            // Capture fieldset name
            this.fieldSetName = _fieldSetName;
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);

            fetchFieldSetPerObject({ fieldSetName : _fieldSetName, objectName : _objectName })
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.processSectionFields(responseData.response);
                    // Start the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
    }
    // Process the related section in form or table view.
    processSectionFields(response) {
        let _sectionFields = JSON.parse(response) || {};        
        let fieldsToDisplayOnUI = [];
        let _relatedObjectRecordsToShow = [];
        let normalHours = '';

        if((this.mapOfEssentials['projecttype'] === 'FTE' || this.mapOfEssentials['projecttype'] === 'T&M') 
        && this.mapOfEssentials['billratetype'] === 'Daily') {
            normalHours = 'Daily';
        } else if((this.mapOfEssentials['projecttype'] === 'FTE' || this.mapOfEssentials['projecttype'] === 'T&M') 
        && this.mapOfEssentials['billratetype'] === 'Weekly') {
            normalHours = 'Weekly';
        }

        if(this.isForm && this.projectconfig && this.projectconfig.key === this._relatedObjectToShow_Key) {
            let sectionVisibleFields = this.projectconfig.sections[this.sectionname]; // JSON of visible fields            
            let pidObject = this.pidObj; // JSON PID DATA
            _relatedObjectRecordsToShow.push(undefined);

            if(pidObject && _sectionFields.length > 0) {
                // Iterate to add values from 
                (_sectionFields).forEach(sec => {
                    let _setObj = sec;
                    // Update Field Label as in desktop version
                    if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                        _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
                    }
                    // add only when field is visible
                    if(sectionVisibleFields[_setObj.fieldName]) {                        
                        // If not Gainshare/Valueshare then don't add is Bundle field.
                        if(_setObj.fieldName === 'GP_Pure_Gainshare__c'
                        && (pidObject.GP_Project_type__c !== 'Gainshare' && pidObject.GP_Project_type__c !== 'Value Share -Outcome' 
                        && pidObject.GP_Project_type__c !== 'Value Share-SLA OD')) {
                            // do nothing
                        }
                        // If not pure gainshare then don't add parent Gainshare/Valueshare fields.
                        else if(!pidObject.GP_Pure_Gainshare__c && _setObj.fieldName === 'GP_Parent_PID_For_Gainshare__c') {
                            // do nothing
                        }
                        // If service delivery not in US/Canada then don't display below fields.
                        else if(!pidObject.GP_Service_Deliver_in_US_Canada__c && 
                            (_setObj.fieldName === 'GP_Country__c' || _setObj.fieldName === 'GP_State__c' ||
                            _setObj.fieldName === 'GP_City__c' || _setObj.fieldName === 'GP_Pincode__c' || 
                            _setObj.fieldName === 'GP_US_Address__c' || _setObj.fieldName === 'GP_Services_To_Be_Delivered_From__c' ||
                            _setObj.fieldName === 'GP_Tax_Exemption_Certificate_Available__c')) {
                            // do nothing
                        }
                        // Timesheet requirement only for T&M and FTE PIDs.
                        else if(pidObject.GP_Project_type__c !== 'T&M' && pidObject.GP_Project_type__c !== 'FTE' 
                        && _setObj.fieldName === 'GP_Timesheet_Requirement__c') {
                            // do nothing
                        }
                        // Normal Hours
                        else if(normalHours === '' && _setObj.fieldName === 'GP_Daily_Normal_Hours__c') {
                            // do nothing
                        }
                        // Shift Date only when shift allowed is true
                        else if(!pidObject.GP_Shift_Allowed__c && _setObj.fieldName === 'GP_Shift_Start_Date__c') {
                            // do nothing
                        }
                        // Overtime/Holiday Billable only for T&M PIDs
                        else if(pidObject.GP_Project_type__c !== 'T&M' && 
                        (_setObj.fieldName === 'GP_Overtime_Billable__c' || _setObj.fieldName === 'GP_Holiday_Billable__c' 
                        ||  _setObj.fieldName === 'GP_T_M_With_Cap__c')) {
                            // do nothing
                        }
                        // Transition end date only when Transition allowed is true
                        else if(!pidObject.GP_Transition_Allowed__c && _setObj.fieldName === 'GP_Transition_End_Date__c') {
                            // do nothing
                        }
                        // Parent project group invoic only when Group invoice is true
                        else if(!pidObject.GP_Grouped_Invoice__c && _setObj.fieldName === 'GP_Parent_Project_for_Group_Invoice__c') {
                            // do nothing
                        }
                        // Tax code
                        else if(!this.isGstApplicableTaxCodeVisible && _setObj.fieldName === 'GP_TAX_Code__c') {
                            // do nothing
                        }
                        // if L1 not GE then don't display Divine and Product code fields
                        else if(pidObject.GP_Business_Group_L1__c !== 'GE' && 
                        (_setObj.fieldName === 'GP_Divine__c' || _setObj.fieldName === 'GP_Product_Code__c')) {
                            // do nothing
                        } 
                        // if service delivery not in US/Canada and Tax exemption certificate not available then don't display Valid till field.
                        else if((!pidObject.GP_Service_Deliver_in_US_Canada__c || !pidObject.GP_Tax_Exemption_Certificate_Available__c)
                            && _setObj.fieldName === 'GP_Valid_Till__c') {
                            // do nothing
                        } 
                        // if no HSN category Id then don't show HSN fields
                        else if(!pidObject.GP_HSN_Category_Id__c && (_setObj.fieldName === 'GP_HSN_Category_Id__c' 
                            || _setObj.fieldName === 'GP_HSN_Category_Name__c')) {
                            // do nothing
                        }
                        // if HSN Category Id exist then hide Category name field as we are going to merge them later here.
                        else if(pidObject.GP_HSN_Category_Id__c && _setObj.fieldName === 'GP_HSN_Category_Name__c') {
                            // do nothing
                        }
                        // Report type for non Indirect PIDs
                        else if(this.mapOfEssentials['recordtypename'] === 'Indirect PID' && _setObj.fieldName === 'GP_Reports__c') {
                            // do nothing
                        }
                        // if Mode not in below values then don't display display Portal Number field
                        else if(_setObj.fieldName === 'GP_Portal_Name__c' && 
                            (pidObject.GP_MOD__c !== 'Upload to Portal' && pidObject.GP_MOD__c !== 'Email and Upload to Portal'
                            && pidObject.GP_MOD__c !== 'Email, Hard Copy and Upload to Portal' 
                            && pidObject.GP_MOD__c !== 'Email, Hard Copy, FAX and Upload to Portal' 
                            && pidObject.GP_MOD__c !== 'Fax and Upload to Portal'
                            && pidObject.GP_MOD__c !== 'Hard Copy and Upload to Portal')) {
                            // do nothing
                        }
                        // if Mode not in below values then don't display display fax no field
                        else if(_setObj.fieldName === 'GP_FAX_Number__c' && 
                            (pidObject.GP_MOD__c !== 'Email and Fax' && pidObject.GP_MOD__c !== 'Email, Hard Copy, FAX and Upload to Portal'
                            && pidObject.GP_MOD__c !== 'Fax' && pidObject.GP_MOD__c !== 'Fax and Hard Copy' 
                            && pidObject.GP_MOD__c !== 'Fax and Upload to Portal')) {
                            // do nothing
                        } else {
                            // Add billing currency for TCV value
                            if(_setObj.fieldName === 'GP_TCV__c') {
                                _setObj.fieldLabel += '('+this.mapOfEssentials['billingcurrency']+')';
                            }
                            if(pidObject[_setObj.fieldName]) {
                                _setObj.value = pidObject[_setObj.fieldName];
                                // Format date/datetime
                                if(_setObj.dataType === 'Date') {
                                    _setObj.value = formatDateTime(_setObj.value, false);
                                } else if(_setObj.dataType === 'DateTime') {
                                    _setObj.value = formatDateTime(_setObj.value, true);
                                } else if(_setObj.dataType === 'Currency' || _setObj.dataType === 'Double') {
                                    _setObj.value = formatNumber(_setObj.value);
                                }
                                // for checkbox, value is already boolean type.
                                if(_setObj.dataType === 'Boolean') {
                                    _setObj.isCheckBox = true;
                                    _setObj.iconname = 'action:check';
                                }
                            } else {
                                // if field has no value then keep it undefined
                                // for checkbox, value is false, can show icon.
                                _setObj.value = undefined;
                                /*if(_setObj.dataType === 'Boolean') {
                                    _setObj.isNoCheckBox = true;
                                }*/
                            }
                            // Handle Normal hours field
                            if(normalHours === 'Daily' && _setObj.fieldName === 'GP_Daily_Normal_Hours__c') {
                                _setObj.fieldLabel = 'Daily Normal Hour';
                                _setObj.value = 8;
                            } else if(normalHours === 'Weekly' && _setObj.fieldName === 'GP_Daily_Normal_Hours__c') {
                                _setObj.fieldLabel = 'Weekly Normal Hour';
                                _setObj.value = 40;
                            }
                            // Merge HSN fields
                            if(pidObject.GP_HSN_Category_Id__c && _setObj.fieldName === 'GP_HSN_Category_Id__c') {
                                _setObj.fieldLabel = 'HSN Category';
                                _setObj.value = pidObject.GP_HSN_Category_Id__c +'-'+ pidObject.GP_HSN_Category_Name__c;
                            }

                            /*if(_setObj.fieldName === 'GP_Project_Currency__c' ) {
                                _setObj.value = this._projectCurrenyJson[pidObject[_setObj.fieldName]];
                            } else if(_setObj.fieldName === 'GP_Billing_Currency__c' ) {
                                _setObj.value = this._billingCurrenyJson[pidObject[_setObj.fieldName]];
                            }*/                           

                            /*if(_setObj.dataType === 'Picklist') {
                                _setObj.isPicklist = true;
                            } else*/

                            // if field is reference or payment terms, then consider custom lookup.
                            if(_setObj.dataType === 'Reference') {
                                _setObj.isReference = true;
                                _setObj.searchfield = 'Name';
                                _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                                _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
                            } else {
                                _setObj.isField = true;
                            }

                            // For payment term only
                            if(_setObj.fieldName === 'GP_Payment_Terms__c') {
                                _setObj.referencetype = 'others';
                                _setObj.isField = false;
                                _setObj.isReference = true;
                                _setObj.referenceName = 'GP_Pinnacle_Master__c';
                                _setObj.searchfield = 'GP_SOA_External_Id__c';
                                _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                                _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
                            }

                            // if value is null and datatype is Boolean then show unchecked icon
                            if(!pidObject[_setObj.fieldName] && _setObj.dataType === 'Boolean') {
                                _setObj.isField = false;
                                _setObj.isReference = false;
                                _setObj.isNotChecked = true;
                                _setObj.iconname = 'utility:record';
                            }

                            // Using Picklist 
                            if(_setObj.fieldName === 'GP_Project_Currency__c' || _setObj.fieldName === 'GP_Billing_Currency__c') {
                                _setObj.isPicklist = true;
                                _setObj.isField = false;
                                _setObj.isReference = false;
                                _setObj.isNotChecked = false;
                                _setObj.isNotReadonly = false;
                                _setObj.objectname = 'GP_Project__c';
                                _setObj.fieldname = _setObj.fieldName === 'GP_Project_Currency__c' ? 'GP_Project_Currency__c' : 'GP_Billing_Currency__c';
                            }

                            fieldsToDisplayOnUI.push(_setObj);
                        }
                    } else {
                        // there will be some fields to be added based on conditions
                        // service by US/Canada some thing, display address related
                        // fields
                    }
                });
            }
        }
        // Table has to be shown
        else {
            if(_sectionFields.length > 0) {
                (_sectionFields).forEach(sec => {
                    let _setObj = sec;
                    let doNothing = false;

                    if(this.objectSelected === 'GP_Project_Budget__c' && this.sectionname === 'Budget_Pricing' 
                    && (_setObj.fieldName === 'GP_Product__c' || _setObj.fieldName === 'GP_Track__c')) {
                        let omsdealid = this.mapOfEssentials['omsdealid'];
                        let nopricinginoms = this.mapOfEssentials['nopricinginoms'];
                        let recordtypename = this.mapOfEssentials['recordtypename'];
                        let projecttype = this.mapOfEssentials['projecttype'];

                        let isBudgetNativelyAvailable = (recordtypename === 'OTHER PBB' && projecttype === 'Fixed Price')
                        || (recordtypename === 'CMITS' && projecttype === 'Fixed Price' && omsdealid)
                        || (recordtypename === 'CMITS' && nopricinginoms === 'true' && projecttype !== 'Fixed monthly');

                        if(isBudgetNativelyAvailable && _setObj.fieldName === 'GP_Track__c') {
                            // do nothing
                            doNothing = true;
                        } else if(!isBudgetNativelyAvailable && _setObj.fieldName === 'GP_Product__c') {
                            // do nothing
                            doNothing = true;
                        }
                    } else if(this.objectSelected === 'GP_Project_Budget__c' && this.sectionname === 'Budget_Pricing' 
                    && _setObj.fieldName === 'GP_TCV__c') {
                        // do nothing
                        doNothing = true;
                    }
                    if(!doNothing) {
                        // Update Field Label as in desktop version
                        if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                            _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
                        }

                        // If section is RegionalLeadership then instead of ROLE its ROLES as label.                        
                        if(this.sectionname === 'RegionalLeadership'
                        && _setObj.fieldName === 'GP_Type_of_Leadership__c') {
                            _setObj.fieldLabel = 'ROLES';
                        }

                        if(this.objectSelected === 'GP_Project_Expense__c' && this.sectionname === 'Expenses' && _setObj.fieldName === 'GP_Amount__c') {
                            _setObj.fieldLabel += '('+this.mapOfEssentials['billingcurrency']+')';
                            _setObj.valuePreFix = this.mapOfEssentials['billingcurrency'];
                        }
                        // for project document we have to give view doc link to user.
                        if(this.objectSelected === 'GP_Project_Document__c' 
                        && (this.sectionname === 'PODocument' || this.sectionname === 'OtherDocument')
                        && _setObj.fieldName === 'GP_Mob_File_View_URL__c') {
                            _setObj.viewDocument = true;
                        }

                        if(_setObj.dataType === 'Reference') {
                            _setObj.isReference = true;
                            _setObj.searchfield = 'Name';
                            _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                            _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
                        } else {
                            _setObj.isField = true;
                        }
                        fieldsToDisplayOnUI.push(_setObj);
                    }
                });
                
                if(this.objectSelected === 'GP_Project_Address__c') {
                    this.relatedObjectRecordsToShow = this.projectaddress;
                } else if(this.objectSelected === 'GP_Project_Leadership__c' && this.sectionname === 'MandatoryKeyMembersLeadership') {
                    this.relatedObjectRecordsToShow = this.projectmandatorykeymembers;
                } else if(this.objectSelected === 'GP_Project_Work_Location_SDO__c' && this.sectionname === 'WorkLocation') {
                    this.relatedObjectRecordsToShow = this.listOfWorkLocations;
                } else if(this.objectSelected === 'GP_Project_Work_Location_SDO__c' && this.sectionname === 'AdditionalSDO') {
                    this.relatedObjectRecordsToShow = this.listOfSDOs;
                } else if(this.objectSelected === 'GP_Project_Leadership__c' && this.sectionname === 'RegionalLeadership') {
                    this.relatedObjectRecordsToShow = (this.listOfRegionalLeadership.length > 0) ? this.listOfRegionalLeadership : [];
                } else if(this.objectSelected === 'GP_Project_Leadership__c' && this.sectionname === 'AccountLeadership') {
                    this.relatedObjectRecordsToShow = (this.listOfAccountLeadership.length > 0) ? this.listOfAccountLeadership : [];
                } else if(this.objectSelected === 'GP_Project_Leadership__c' && this.sectionname === 'HSLLeadership') {
                    this.relatedObjectRecordsToShow = (this.listOfHSLLeadership.length > 0) ? this.listOfHSLLeadership : [];
                } else if(this.objectSelected === 'GP_Project_Leadership__c' && this.sectionname === 'AdditionalAccessLeadership') {
                    this.relatedObjectRecordsToShow = (this.listOfAdditionalLeadership.length > 0) ? this.listOfAdditionalLeadership : [];
                } else if(this.objectSelected === 'GP_Project_Document__c' && this.sectionname === 'PODocument') {
                    this.relatedObjectRecordsToShow = (this.listOfPODocuments.length > 0) ? this.listOfPODocuments : [];
                } else if(this.objectSelected === 'GP_Project_Document__c' && this.sectionname === 'OtherDocument') {
                    this.relatedObjectRecordsToShow = (this.listOfOtherDocuments.length > 0) ? this.listOfOtherDocuments : [];
                } else if(this.objectSelected === 'GP_Project_Expense__c' && this.sectionname === 'Expenses') {
                    this.relatedObjectRecordsToShow = (this.listOfExpenses.length > 0) ? this.listOfExpenses : [];
                } else if(this.objectSelected === 'GP_Project_Budget__c' && this.sectionname === 'Budget_Pricing') {
                    this.relatedObjectRecordsToShow = (this.listOfBudgetAndPricings.length > 0) ? this.listOfBudgetAndPricings : [];
                }
            }
        }
        
        this.sectionFields = fieldsToDisplayOnUI;
        (this.sectionsToShow).forEach(sec => {
            if(sec.key === this.sectionname) {
                sec.isSelected = true;
            } else {
                sec.isSelected = false;
            }
            if(sec.key !== 'Address' && sec.key !== 'MandatoryKeyMembersLeadership' 
            && sec.key !== 'WorkLocation' && sec.key !== 'AdditionalSDO'
            && sec.key !== 'RegionalLeadership' && sec.key !== 'AccountLeadership'
            && sec.key !== 'HSLLeadership' && sec.key !== 'AdditionalAccessLeadership'
            && sec.key !== 'PODocument' && sec.key !== 'OtherDocument'
            && sec.key !== 'Expenses' && sec.key !== 'Budget_Pricing') {
                sec.isForm = true;
                //this.isForm = true;
            } else {
                sec.isForm = false;
                //this.isForm = false;
            }
        });
    }
    // call apex class to bring related object data per PID.
    // Project related section
    fetchRelatedObjectData(relatedSection) {
        if(relatedSection === 'Project') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchPIDData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchPIDDataHandler(responseData, false);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Work Location SDO related section
        if(relatedSection === 'WorklocationAndSDO') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project_Work_Location_SDO__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchWorkLocationSDOData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchWorkLocationSDODataHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Project Leadership related section
        if(relatedSection === 'ProjectLeadership') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project_Leadership__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchProjectLeadershipSDOData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchProjectLeadershipSDODataHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Billing Milestone related section
        if(relatedSection === 'GP_Billing_Milestone__c') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Billing_Milestone__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchBillingMilestoneData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchBillingMilestoneDataHandler(responseData);
                    // Show subtotals in BMs
                    this.isBillingMilestonesNote = true;
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Budget Expense related 
        if(relatedSection === 'BudgetAndExpenses') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project_Budget__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchBudgetAndExpensesData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchBudgetAndExpensesDataHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Profile Bill Rate related section
        if(relatedSection === 'ProfileBillRates') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Profile_Bill_Rate__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchProfileBillRateData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchProfileBillRateDataHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Project Resource Allocation related section
        if(relatedSection === 'ProjectResourceAllocation') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Resource_Allocation__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchResourceAllocationData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchResourceAllocationDataHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Document related section
        if(relatedSection === 'Documents') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project_Document__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchDocumentData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchDocumentDataHandler(responseData);
                    // Show note on Document click
                    this.isPIDDocNote = true;
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // Change summary related section
        if(relatedSection === 'ChangeSummary') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            //this.objectName = 'GP_Project_Document__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchChangeSummaryData({ recordId : this.recordId, objectName : null})
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchChangeSummaryHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
        // PID summary related section
        if(relatedSection === 'PIDSummary') {
            // call apex class to bring related object data per PID.
            // fetch related sections to show
            this.objectName = 'GP_Project__c';
            // Start the spinner
            handleSpinner(this, 'spinner_parent_div', true);
            fetchPIDSummaryData({ recordId : this.recordId, objectName : this.objectName})
            .then(result => {
                var responseData = result;                
                if(responseData.isSuccess) {
                    this.fetchPIDSummaryHandler(responseData);
                    // End the spinner
                    handleSpinner(this, 'spinner_parent_div', false);
                }
            })
            .catch(error => {
                this.error = error;
                // End the spinner
                handleSpinner(this, 'spinner_parent_div', false);
            });
        }
    }

    fetchWorkLocationSDODataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        let seqSectionsList = response.listOfSections;
        // null the section list first and then fill it freshly.
        this.sectionsToShow = [];
        (seqSectionsList).forEach(sec => {
            let secObj = {};
            secObj.key = sec;
            secObj.value = this._headerLabelJson[sec];
            secObj.isSelected = false;
            this.sectionsToShow.push(secObj);
        });

        this.listOfWorkLocations = (response.listOfWorkLocations !== undefined) ? response.listOfWorkLocations : [];
        this.listOfSDOs = (response.listOfSDOs !== undefined) ? response.listOfSDOs : [];
    }

    fetchProjectLeadershipSDODataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        let metadata = response.mapofmetadata;
        let listOfProjectLeaderships = (response.listOfProjectLeaderships !== undefined) ? response.listOfProjectLeaderships : [];
        let _sectionsToShow = {};

        // Prepare leadership data.
        (listOfProjectLeaderships).forEach(pl => {
            let plObj = pl;

            if(plObj.RecordType.Name === 'Regional Leadership') {
                this.listOfRegionalLeadership.push(plObj);
            } else if(plObj.RecordType.Name === 'HSL Leadership') {
                this.listOfHSLLeadership.push(plObj);
            } else if(plObj.RecordType.Name === 'Account Leadership') {
                this.listOfAccountLeadership.push(plObj);
            } else if(plObj.RecordType.Name === 'Additional Access') {
                this.listOfAdditionalLeadership.push(plObj);
            }
        });

        this.sectionsToShow = [];

        for(let key in metadata) {
            if(key === 'ProjectLeadership') {
                //this._relatedObjectToShow_Key = key;
                // Allready done before apex callout
                //this.relatedObjectToShow = this._headerLabelJson[key];                
                for(let sectionkey in metadata[key]) {
                    if(sectionkey !== '') {
                        _sectionsToShow[sectionkey] = true;                        
                    }
                }
                // Get the visible sequencial section list
                if(this._headerLabelSequenceJson[key]) {
                    let seqSectionsList = this._headerLabelSequenceJson[key].split(',');
                    seqSectionsList = [...seqSectionsList];
                    // null the section list first and then fill it freshly.
                    this.sectionsToShow = [];
                    (seqSectionsList).forEach(sec => {
                        if(_sectionsToShow[sec]) {
                            let secObj = {};
                            secObj.key = sec;
                            secObj.value = this._headerLabelJson[sec];
                            secObj.isSelected = false;
                            this.sectionsToShow.push(secObj);
                        }
                    });
                }
            }
        }
    }

    fetchResourceAllocationDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};

        this.fieldSetName = "GP_LWC_Resource_Allocation_Sequence";

        let pidRecordType = response.recordtype;
        let pidType = response.projecttype;
        let isInterviewedByCustomer = response.isInterviewedByCustomer;
        let isCC2Allowed = response.isCC2Allowed;
        let metadata = response.mapofmetadata;
        //this.listOfResourceAllocations = (response.listOfResourceAllocations !== undefined) ? response.listOfResourceAllocations : [];
        let resourceAllocationFieldSequences = response.resourceAllocationFieldSequences;
        let resourceAllocationField = response.resourceAllocationField;
        let allFieldsForSequence = [];
        let allFieldsToShow = [];
        let fieldsToDisplayOnUI = [];
        if(resourceAllocationFieldSequences.isSuccess) {
            allFieldsForSequence = JSON.parse(resourceAllocationFieldSequences.response);
        }
        if(resourceAllocationField.isSuccess) {
            allFieldsToShow = JSON.parse(resourceAllocationField.response);
        }
        // Extra fields to show
        let _fieldToShow = {};

        for(let key in metadata) {
            if(key === 'ProjectResourceAllocation') {
                for(let sectionkey in metadata[key]) {
                    if(sectionkey !== '' && sectionkey === 'Default') {
                        for(let field in metadata[key][sectionkey]) {
                            if(field !== '') {
                                _fieldToShow[metadata[key][sectionkey][field]] = true;
                            }
                        }
                    }
                }
            }
        }
        // Add the additional fields to show (Morgan Stanely or CVS health)
        (allFieldsForSequence).forEach(affs => {
            if(_fieldToShow[affs.fieldName]) {
                allFieldsToShow.push(affs);
            }
        });
        // prepare table columns
        (allFieldsToShow).forEach(sec => {
            let _setObj = sec;
			// Field to hide for indirect PIDs.
            if((pidRecordType === 'Indirect PID' 
                && (_setObj.fieldName === 'GP_Profile__c' || _setObj.fieldName === 'GP_Committed_SOW__c' 
                || _setObj.fieldName === 'GP_Shadow_type__c' || _setObj.fieldName === 'GP_Bill_Rate__c' 
                || _setObj.fieldName === 'GP_Allocation_Type__c'))
            || (pidRecordType !== 'Indirect PID' && _setObj.fieldName === 'GP_Shadow_type__c' && (pidType !== 'T&M' && pidType !== 'FTE'))) {
                // do nothing
            }
            else if(!isInterviewedByCustomer && _setObj.fieldName === 'GP_Interviewed_by_Customer__c') {
                // do nothing
            }
            else if(!isCC2Allowed && _setObj.fieldName === 'GP_CC2__c') {
                // do nothing
            }
            else {
                // Update Field Label as in desktop version
                if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                    _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
                }
                if(_setObj.fieldName === 'GP_Bill_Rate__c') {
                    _setObj.fieldLabel += '('+this.mapOfEssentials['currencycode']+'/'+this.mapOfEssentials['billratetype']+')';
                }
                if(_setObj.dataType === 'Reference') {
                    _setObj.isReference = true;
                    _setObj.searchfield = 'Name';
                    _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                    _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
                } else {
                    _setObj.isField = true;
                }
                // Using Picklist. CC2 is only for Indirect PIDs
                if(pidRecordType === 'Indirect PID' && _setObj.fieldName === 'GP_CC2__c') {
                    _setObj.isPicklist = true;
                    _setObj.isField = false;
                    _setObj.isReference = false;
                    _setObj.isNotChecked = false;
                    _setObj.isNotReadonly = false;
                    _setObj.objectname = 'GP_Resource_Allocation__c';
                    _setObj.fieldname = 'GP_CC2__c';
                    _setObj.fieldLabel = 'COST CENTER (CC2)';
                }
                fieldsToDisplayOnUI.push(_setObj);
            }
        });

        this.sectionFields = fieldsToDisplayOnUI;
        //this.relatedObjectRecordsToShow = this.listOfResourceAllocations;
        this.relatedObjectRecordsToShow = (response.listOfResourceAllocations !== undefined) ? response.listOfResourceAllocations : [];
        this.isNoSectionPerObject = true;
    }

    fetchBillingMilestoneDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};

        this.fieldSetName = 'GP_LWC_Billing_Milestone_Fields';

        let projectTCV = response.projectTCV;
        let currencyIsoCode = response.currencyIsoCode;
        let billingMilestoneFieldSequences = response.billingMilestoneFieldSequences;
        let allFieldsForSequence = [];
        let fieldsToDisplayOnUI = [];
        let bmTotal = 0.0;

        if(billingMilestoneFieldSequences.isSuccess) {
            allFieldsForSequence = JSON.parse(billingMilestoneFieldSequences.response);
        }

        // prepare table columns
        (allFieldsForSequence).forEach(sec => {
            let _setObj = sec;
            // Update Field Label as in desktop version
            if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
            }
            if(_setObj.fieldName === 'GP_Amount__c') {
                _setObj.fieldLabel += '('+this.mapOfEssentials['currencycode']+')';
                _setObj.valuePreFix = this.mapOfEssentials['currencycode'];
            }
            if(_setObj.dataType === 'Reference') {
                _setObj.isReference = true;
                _setObj.searchfield = 'Name';
                _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
            } else {
                _setObj.isField = true;
            }
            fieldsToDisplayOnUI.push(_setObj);
        });

        this.sectionFields = fieldsToDisplayOnUI;
        this.relatedObjectRecordsToShow = (response.listOfBillingMilestones !== undefined) ? response.listOfBillingMilestones : [];
        
        (this.relatedObjectRecordsToShow).forEach(rec => {
            if(rec.GP_Amount__c) {
                bmTotal = bmTotal + rec.GP_Amount__c;
            }
        });

        this.billingMilestoneNote = ' Project TCV Amount: ' + currencyIsoCode + ' ' + formatNumber(projectTCV)
        + ' and Total Billing Milestone Amount : ' + currencyIsoCode + ' ' + formatNumber(bmTotal) + '.';

        this.isNoSectionPerObject = true;
    }

    fetchDocumentDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        let seqSectionsList = response.listOfSections;
        // null the section list first and then fill it freshly.
        this.sectionsToShow = [];
        (seqSectionsList).forEach(sec => {
            let secObj = {};
            secObj.key = sec;
            secObj.value = this._headerLabelJson[sec];
            secObj.isSelected = false;
            this.sectionsToShow.push(secObj);
        });

        this.listOfPODocuments = (response.listOfPODocuments !== undefined) ? response.listOfPODocuments : [];
        this.listOfOtherDocuments = (response.listOfOtherDocuments !== undefined) ? response.listOfOtherDocuments : [];
    }

    fetchProfileBillRateDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        let profileBillRateFieldSequences = response.profileBillRateFieldSequences;
        let allFieldsForSequence = [];
        let fieldsToDisplayOnUI = [];

        this.fieldSetName = 'GP_LWC_Profile_Bill_Rate_Fields';

        if(profileBillRateFieldSequences.isSuccess) {
            allFieldsForSequence = JSON.parse(profileBillRateFieldSequences.response);
        }

        // prepare table columns
        (allFieldsForSequence).forEach(sec => {
            let _setObj = sec;
            // Update Field Label as in desktop version
            if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
            }
            if(_setObj.fieldName === 'GP_Bill_Rate__c') {
                _setObj.fieldLabel += '('+this.mapOfEssentials['billingcurrency']+')';
            }
            if(_setObj.dataType === 'Reference') {
                _setObj.isReference = true;
                _setObj.searchfield = 'Name';
                _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
            } else {
                _setObj.isField = true;
            }
            fieldsToDisplayOnUI.push(_setObj);
        });

        this.sectionFields = fieldsToDisplayOnUI;
        this.relatedObjectRecordsToShow = (response.listOfProjectBillRates !== undefined) ? response.listOfProjectBillRates : [];
        this.isNoSectionPerObject = true;
    }

    fetchBudgetAndExpensesDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        let seqSectionsList = response.listOfSections;
        // null the section list first and then fill it freshly.
        this.sectionsToShow = [];
        (seqSectionsList).forEach(sec => {
            let secObj = {};
            secObj.key = sec;
            secObj.value = this._headerLabelJson[sec];
            secObj.isSelected = false;
            this.sectionsToShow.push(secObj);
        });

        this.pidBillingCurrency = response.pidBillingCurrency;
        this.listOfExpenses = (response.listOfExpenses !== undefined) ? response.listOfExpenses : [];
        this.listOfBudgetAndPricings = (response.listOfBudgetAndPricings !== undefined) ? response.listOfBudgetAndPricings : [];

        let expTotal = 0.00;
        (this.listOfExpenses).forEach(rec => {
            if(rec.GP_Amount__c) {
                expTotal = expTotal + rec.GP_Amount__c;
            }
        });
        this.expensesNote = ' Total Expense Amount: ' + this.pidBillingCurrency + ' ' +  formatNumber(expTotal) + '.';

        let budTotal = 0.00;
        let effTotal = 0.0;
        (this.listOfBudgetAndPricings).forEach(rec => {
            if(rec.GP_TCV__c) {
                budTotal = budTotal + rec.GP_TCV__c;
            }
            if(rec.GP_Effort_Hours__c) {
                effTotal = effTotal + rec.GP_Effort_Hours__c;
            }
        });
        this.budgetNote = ' Total Budget Effort Hours : ' + effTotal + ' and Total Budget TCV : USD ' +  formatNumber(budTotal) + '.';

        this.isBudgetNote = response.isBudget;
        this.isExpensesNote = true;
        this.isBudgetAndExpensesNote = true;
    }

    fetchChangeSummaryHandler(responseData) {
        if(responseData.message === 'SUCCESS') {
            let response = JSON.parse(responseData.response) || {};
            let parentVersionId = response.parentVersionId;
            let childVersionId = response.childVersionId;
            let listOfSanatizedProjectVersionHistory = response.listOfSanatizedProjectVersionHistory;
            let mapOfRelatedRecordWithMapOfFields = response.mapOfRelatedRecordWithMapOfFields;

            let parentPIDObj = {};
            let childPIDObj = {};
            let parentAddressObj = [];
            let childAddressObj = [];
            let parentWorkLocationObj = [];
            let childWorkLocationObj = [];
            //let parentSDOObj = [];
            //let childSDOObj = [];
            let parentLeadershipObj = [];
            let childLeadershipObj = [];
            let parentBillingObj = [];
            let childBillingObj = [];
            let parentResourceObj = [];
            let childResourceObj = [];
            let parentBudgetObj = [];
            let childBudgetObj = [];
            let parentExpenseObj = [];
            let childExpenseObj = [];
            let parentDocumentObj = [];
            let childDocumentObj = [];
            let parentProfileBillRateObj = [];
            let childProfileBillRateObj = [];

            let changeSummary = {};
            changeSummary.projectChangeList = [];
            changeSummary.projectAddressChangeList = [];
            changeSummary.projectWorkLocationChangeList = [];
            //changeSummary.projectSDOChangeList = [];
            changeSummary.projectLeadershipChangeList = [];
            changeSummary.projectBillingChangeList = [];
            changeSummary.projectResourceChangeList = [];
            changeSummary.projectBudgetChangeList = [];
            changeSummary.projectExpenseChangeList = [];
            changeSummary.projectDocumentChangeList = [];
            changeSummary.projectProfileBillRateChangeList = [];
            //alert('listOfSanatizedProjectVersionHistory - ', JSON.stringify(listOfSanatizedProjectVersionHistory));
            (listOfSanatizedProjectVersionHistory).forEach(lvh => {
                if(lvh.Id === parentVersionId) {
                    parentPIDObj = lvh.GP_Project_JSON__c ? JSON.parse(lvh.GP_Project_JSON__c) : {};
                    parentAddressObj = lvh.GP_Address_JSON__c ? JSON.parse(lvh.GP_Address_JSON__c) : [];
                    parentWorkLocationObj = lvh.GP_Work_Location_JSON__c ? JSON.parse(lvh.GP_Work_Location_JSON__c) : [];
                    //parentSDOObj = lvh.GP_Additional_SDO_JSON__c ? JSON.parse(lvh.GP_Additional_SDO_JSON__c) : [];
                    parentLeadershipObj = lvh.GP_Leadership_JSON__c ? JSON.parse(lvh.GP_Leadership_JSON__c) : [];
                    parentBillingObj = lvh.GP_Billing_Milestones_JSON__c ? JSON.parse(lvh.GP_Billing_Milestones_JSON__c) : [];
                    parentResourceObj = lvh.GP_Resource_Allocation_JSON__c ? JSON.parse(lvh.GP_Resource_Allocation_JSON__c) : [];
                    parentBudgetObj = lvh.GP_Budget_JSON__c ? JSON.parse(lvh.GP_Budget_JSON__c) : [];
                    parentExpenseObj = lvh.GP_Expenses_JSON__c ? JSON.parse(lvh.GP_Expenses_JSON__c) : [];
                    parentDocumentObj = lvh.GP_Project_Document_JSON__c ? JSON.parse(lvh.GP_Project_Document_JSON__c) : [];
                    parentProfileBillRateObj = lvh.GP_Profile_Bill_Rate_JSON__c ? JSON.parse(lvh.GP_Profile_Bill_Rate_JSON__c) : [];
                } else if(lvh.Id === childVersionId) {
                    childPIDObj = lvh.GP_Project_JSON__c ? JSON.parse(lvh.GP_Project_JSON__c) : {};
                    childAddressObj = lvh.GP_Address_JSON__c ? JSON.parse(lvh.GP_Address_JSON__c) : [];
                    childWorkLocationObj = lvh.GP_Work_Location_JSON__c ? JSON.parse(lvh.GP_Work_Location_JSON__c) : [];
                    //childSDOObj = lvh.GP_Additional_SDO_JSON__c ? JSON.parse(lvh.GP_Additional_SDO_JSON__c) : [];
                    childLeadershipObj = lvh.GP_Leadership_JSON__c ? JSON.parse(lvh.GP_Leadership_JSON__c) : [];
                    childBillingObj = lvh.GP_Billing_Milestones_JSON__c ? JSON.parse(lvh.GP_Billing_Milestones_JSON__c) : [];
                    childResourceObj = lvh.GP_Resource_Allocation_JSON__c ? JSON.parse(lvh.GP_Resource_Allocation_JSON__c) : [];
                    childBudgetObj = lvh.GP_Budget_JSON__c ? JSON.parse(lvh.GP_Budget_JSON__c) : [];
                    childExpenseObj = lvh.GP_Expenses_JSON__c ? JSON.parse(lvh.GP_Expenses_JSON__c) : [];
                    childDocumentObj = lvh.GP_Project_Document_JSON__c ? JSON.parse(lvh.GP_Project_Document_JSON__c) : [];
                    childProfileBillRateObj = lvh.GP_Profile_Bill_Rate_JSON__c ? JSON.parse(lvh.GP_Profile_Bill_Rate_JSON__c) : [];
                }
            });

            for(let key in mapOfRelatedRecordWithMapOfFields) {
                if(key === 'Project') {
                    for(let subKey in mapOfRelatedRecordWithMapOfFields[key]) {
                        if(subKey) {
                            let fieldsetDetails = JSON.parse(mapOfRelatedRecordWithMapOfFields[key][subKey]);
                            //fieldsetDetails = [...fieldsetDetails];
                            (fieldsetDetails).forEach(ff => {
                                // Remove some fields from Project section
                                // Process log
                                if(ff.fieldName === 'GP_Oracle_Process_Log__c') {
                                    // do nothing.
                                } else if(ff.fieldName === 'GP_Reports__c' && childPIDObj["recordtype.name"] === 'Indirect PID') {
                                    // do nothing
                                } else {
                                    // Update Field Label as in desktop version for PID
                                    if(this._fieldLabelJson["GP_LWC_Business_Product_Hierarchy_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_Business_Product_Hierarchy_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_Project_Info_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_Project_Info_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_Other_Classification_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_Other_Classification_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_Dispatch_Info_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_Dispatch_Info_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_GE_Project_Detail_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_GE_Project_Detail_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_CMITS_Specific_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_CMITS_Specific_Fields"][ff.fieldName];
                                    } else if(this._fieldLabelJson["GP_LWC_Oracle_Status_Fields"][ff.fieldName]) {
                                        ff.fieldLabel = this._fieldLabelJson["GP_LWC_Oracle_Status_Fields"][ff.fieldName];
                                    }

                                    let field = ff.fieldName.toLowerCase();
                                    // If reference field then use xxx__r.name field instead.
                                    if(ff.referenceName) {
                                        field = field.replace('__c', '__r.name');
                                    }
                                    if(parentPIDObj[field] && childPIDObj[field] 
                                        && parentPIDObj[field] !== childPIDObj[field]) {
                                        ff.newValue = childPIDObj[field];
                                        ff.oldValue = parentPIDObj[field];
                                        ff.key = subKey + '' + field;
                                        changeSummary.projectChangeList.push(ff);
                                    } else if(!parentPIDObj[field] && childPIDObj[field]) {
                                        ff.newValue = childPIDObj[field];
                                        ff.oldValue = undefined;
                                        ff.key = subKey + '' + field;
                                        changeSummary.projectChangeList.push(ff);
                                    }
                                }
                            });
                        }
                    }
                    // if there is change in any field then show the section
                    if(changeSummary.projectChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Project Address' && mapOfRelatedRecordWithMapOfFields[key] && childAddressObj['Project Address'] 
                    && parentAddressObj['Project Address']) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Address__c', changeSummary, 
                    childAddressObj['Project Address'], parentAddressObj['Project Address'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectAddressChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Work Location' && mapOfRelatedRecordWithMapOfFields[key]) {
                    //alert('Work Location : - '+ JSON.stringify(childWorkLocationObj));
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Work_Location_SDO__c', changeSummary, 
                    childWorkLocationObj['Work Location'], parentWorkLocationObj['Work Location'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectWorkLocationChangeList.length > 0) {
                        this.changeSummarySections.push('Work Location & SDO');
                    }
                } /*else if(key === 'SDO' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Work_Location_SDO__c', changeSummary, 
                    childSDOObj['SDO'], parentSDOObj['SDO'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectSDOChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                }*/ else if(key === 'Project Leadership' && mapOfRelatedRecordWithMapOfFields[key]) {
                    //alert('Project Leadership : - '+ JSON.stringify(childLeadershipObj));
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Leadership__c', changeSummary, 
                    childLeadershipObj['Leadership'], parentLeadershipObj['Leadership'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectLeadershipChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Billing Milestone' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Billing_Milestone__c', changeSummary, 
                    childBillingObj['Billing Milestone'], parentBillingObj['Billing Milestone'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectBillingChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Resource Allocation' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Resource_Allocation__c', changeSummary, 
                    childResourceObj['Resource Allocation'], parentResourceObj['Resource Allocation'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectResourceChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Budget' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Budget__c', changeSummary, 
                    childBudgetObj['Project Budget'], parentBudgetObj['Project Budget'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectBudgetChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Expense' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Expense__c', changeSummary, 
                    childExpenseObj['Project Expense'], parentExpenseObj['Project Expense'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectExpenseChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Document' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Project_Document__c', changeSummary, 
                    childDocumentObj['Project Document'], parentDocumentObj['Project Document'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectDocumentChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                } else if(key === 'Profile Bill Rate' && mapOfRelatedRecordWithMapOfFields[key]) {
                    this.compareRelatedRecords(key, 'GP_IsUpdated__c', 'GP_Parent_Profile_Bill_Rate__c', changeSummary, 
                    childProfileBillRateObj['Project Bill Rate'], parentProfileBillRateObj['Project Bill Rate'], mapOfRelatedRecordWithMapOfFields[key]);
                    // if there is change in any field then show the section
                    if(changeSummary.projectProfileBillRateChangeList.length > 0) {
                        this.changeSummarySections.push(key);
                    }
                }
            }
            this.changeSummarySectionDetails = changeSummary;
            this.isChangeSummary = true;
        } else {
            this.changeSummaryErrorMessage = responseData.response;
            this.isChangeSummary = false;
        }
    }

    compareRelatedRecords(key, isUpdateField, parentField, changeSummary, childRecordList, parentRecordList, mapOfFieldSets) {
        let updatedList = [];
        // for Doc and PBR there is no isUpdated field so check all child records and determine the diff.
        if(key !== 'Document' && key !== 'Profile Bill Rate' && childRecordList) {
            (childRecordList).forEach(rec => {
                if(rec[isUpdateField.toLowerCase()]) {
                    updatedList.push(rec);
                }
            });
        } else if(childRecordList) {
            updatedList = childRecordList;
        }
        
        for(let subKey in mapOfFieldSets) {
            if(subKey) {
                (updatedList).forEach(rec => {
                    let fieldsetDetails = JSON.parse(mapOfFieldSets[subKey]);
                    //fieldsetDetails = [...fieldsetDetails];                    
                    let parentRec = (rec[parentField.toLowerCase()] && parentRecordList) ? 
                    parentRecordList.find( record => record.id === rec[parentField.toLowerCase()]) : undefined;
                    let changes = {};
                    changes.key = rec.id;
                    changes.details = [];
                    (fieldsetDetails).forEach(ff => {
                        let field = ff.fieldName.toLowerCase();
                        let mandatoryField = '';
                        // If reference field then use xxx__r.name field instead.
                        if(ff.referenceName) {
                            field = field.replace('__c', '__r.name');
                        }
                        // Get Mandatory field.
                        if(key === 'Project Address' && ff.fieldName === 'GP_Relationship__c'
                        || key === 'Work Location' && ff.fieldName === 'GP_Work_Location__c'
                        || key === 'Project Leadership' && ff.fieldName === 'GP_Type_of_Leadership__c'
                        || key === 'Billing Milestone' && ff.fieldName === 'Name'
                        || key === 'Resource Allocation' && ff.fieldName === 'GP_Employee__c'
                        || key === 'Budget' && ff.fieldName === 'GP_Band__c'
                        || key === 'Expense' && ff.fieldName === 'GP_Expense_Category__c'
                        || key === 'Document' && ff.fieldName === 'Name'
                        || key === 'Profile Bill Rate' && ff.fieldName === 'GP_Profile__c') {
                            mandatoryField = field;
                        }                        
                        if(parentRec && parentRec[field] && rec[field]
                            && parentRec[field] !== rec[field]) {
                            ff.newValue = rec[field];
                            ff.oldValue = parentRec[field];
                            changes.details.push(ff);
                        } else if(parentRec && !parentRec[field] && rec[field]) {
                            ff.newValue = rec[field];
                            ff.oldValue = undefined;
                            changes.details.push(ff);
                        } else if(!parentRec && rec[field]) {
                            ff.newValue = rec[field];
                            ff.oldValue = undefined;
                            changes.details.push(ff);
                        }
                        // If mandatory field is not picked, then also pick it any ways.
                        else if(field === mandatoryField) {
                            ff.newValue = rec[field];
                            ff.oldValue = parentRec && parentRec[field] ? parentRec[field] : undefined;
                            changes.details.push(ff);
                        }
                    });
                    if(changes.details.length > 0) {
                        if(key === 'Project Address') {
                            changeSummary.projectAddressChangeList.push(changes);
                        } else if(key === 'Work Location') {
                            changeSummary.projectWorkLocationChangeList.push(changes);
                        } /*else if(key === 'SDO') {
                            changeSummary.projectSDOChangeList.push(changes);
                        }*/ else if(key === 'Project Leadership') {
                            changeSummary.projectLeadershipChangeList.push(changes);
                        } else if(key === 'Billing Milestone') {
                            changeSummary.projectBillingChangeList.push(changes);
                        } else if(key === 'Resource Allocation') {
                            changeSummary.projectResourceChangeList.push(changes);
                        } else if(key === 'Budget') {
                            changeSummary.projectBudgetChangeList.push(changes);
                        } else if(key === 'Expense') {
                            changeSummary.projectExpenseChangeList.push(changes);
                        } else if(key === 'Document') {
                            changeSummary.projectDocumentChangeList.push(changes);
                        } else if(key === 'Profile Bill Rate') {
                            changeSummary.projectProfileBillRateChangeList.push(changes);
                        }
                    }
                });
            }
        }
    }
    
    fetchPIDSummaryHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        
        let listOfFields = response.listOfFields;
        let projectObj = response.projectObj;
        let projectSummaryFields = response.projectSummaryFields;
        let listOfProjectAddresses = response.listOfProjectAddresses;
        let listOfProjectMandatoryKeyMembers = response.listOfProjectMandatoryKeyMembers;
        let updatedResourceCount = response.updatedResourceCount;
        let allFieldsForSequence = [];

        let listOfValuesToShow = [];

        if(projectObj.RecordType.Name === 'Indirect PID') {
            this.fieldSetName = "GP_LWC_Project_Summary_Fields_Indirect";
        } else if(projectObj.RecordType.Name === 'BPM') {
            this.fieldSetName = "GP_LWC_Project_Summary_Fields_BPM";
        } else {
            this.fieldSetName = "GP_LWC_Project_Summary_Fields_PBB";
        }

        if(projectSummaryFields.isSuccess) {
            allFieldsForSequence = JSON.parse(projectSummaryFields.response);
        }
        // prepare table columns
        (allFieldsForSequence).forEach(sec => {
            if(listOfFields.includes(sec.fieldName)) {
                let _setObj = sec;
                // Update Field Label as in desktop version
                if(this._fieldLabelJson[this.fieldSetName] && this._fieldLabelJson[this.fieldSetName][_setObj.fieldName]) {
                    _setObj.fieldLabel = this._fieldLabelJson[this.fieldSetName][_setObj.fieldName];
                }
                if(!projectObj.GP_Service_Deliver_in_US_Canada__c && 
                    (_setObj.fieldName === 'GP_Country__c' || _setObj.fieldName === 'GP_State__c' ||
                    _setObj.fieldName === 'GP_City__c' || _setObj.fieldName === 'GP_Pincode__c' || 
                    _setObj.fieldName === 'GP_US_Address__c' || _setObj.fieldName === 'GP_Services_To_Be_Delivered_From__c' ||
                    _setObj.fieldName === 'GP_Tax_Exemption_Certificate_Available__c')) {
                        // do nothing
                } else if((!projectObj.GP_Service_Deliver_in_US_Canada__c || !projectObj.GP_Tax_Exemption_Certificate_Available__c)
                    && _setObj.fieldName === 'GP_Valid_Till__c') {
                    // do nothing
                } else if(!projectObj.GP_HSN_Category_Id__c && (_setObj.fieldName === 'GP_HSN_Category_Id__c' 
                    || _setObj.fieldName === 'GP_HSN_Category_Name__c')) {
                    // do nothing
                } 
                // if HSN Category Id exist then hide Category name field as we are going to merge them later here.
                else if(projectObj.GP_HSN_Category_Id__c && _setObj.fieldName === 'GP_HSN_Category_Name__c') {
                    // do nothing
                } 
                // if Project type = FP then only show Total budget
                else if(projectObj.GP_Project_type__c !== 'Fixed Price' && _setObj.fieldName === 'GP_HSN_Category_Name__c') {
                    // do nothing
                } 
                // If non GE PID then no Classification, 
                else if(projectObj.GP_Business_Group_L1__c !== 'GE' && 
                (_setObj.fieldName === 'GP_Divine__c' || _setObj.fieldName === 'GP_Product_Code__c')) {
                    // do nothing
                } else if(sec.fieldName === 'GP_Deal__c') {
                    this.addAdditionalFields(listOfValuesToShow, projectObj, listOfProjectAddresses, listOfProjectMandatoryKeyMembers);
                } else {
                    // Merge HSN fields
                    if(projectObj.GP_HSN_Category_Id__c && _setObj.fieldName === 'GP_HSN_Category_Id__c') {
                        _setObj.fieldLabel = 'HSN Category';
                        _setObj.value = projectObj.GP_HSN_Category_Id__c +'-'+ projectObj.GP_HSN_Category_Name__c;
                    } else if(sec.fieldName === 'GP_Resource_Allocation_Failure_Count__c') {
                        _setObj.fieldLabel = 'Resource Rescheduling';
                        _setObj.value = updatedResourceCount;
                    } else if(sec.fieldName === 'GP_TCV__c' || sec.fieldName === 'GP_Total_TCV__c') {
                        _setObj.value = projectObj.GP_Project_Currency__c + ' ' + formatNumber(projectObj[sec.fieldName]);
                    } /*else if(sec.fieldName === 'GP_Project_Currency__c' ) {
                        _setObj.value = this._projectCurrenyJson[projectObj[sec.fieldName]];
                    } else if(sec.fieldName === 'GP_Billing_Currency__c' ) {
                        _setObj.value = this._billingCurrenyJson[projectObj[sec.fieldName]];
                    }*/ else if(projectObj[sec.fieldName]) {
                        if(sec.dataType === 'Boolean') {
                            _setObj.isCheckBox = true;
                            _setObj.iconname = 'action:check';
                        }
                        _setObj.value = projectObj[sec.fieldName];
                        // Format date/datetime
                        if(_setObj.dataType === 'Date') {
                            _setObj.value = formatDateTime(_setObj.value, false);
                        } else if(_setObj.dataType === 'DateTime') {
                            _setObj.value = formatDateTime(_setObj.value, true);
                        } else if(_setObj.dataType === 'Currency' || _setObj.dataType === 'Double') {
                            _setObj.value = formatNumber(_setObj.value);
                        }
                    }
                    
                    if(_setObj.dataType === 'Reference') {
                        _setObj.isReference = true;
                        _setObj.searchfield = 'Name';
                        _setObj.displayTextFields = this._lookupMappings[_setObj.fieldName].displayTextFields;
                        _setObj.placeholderValue = this._lookupMappings[_setObj.fieldName].placeholderValue;
                    } else {
                        _setObj.isField = true;
                    }
                    // if value is null and datatype is Boolean then show unchecked icon
                    if(!projectObj[sec.fieldName] && _setObj.dataType === 'Boolean') {
                        _setObj.isField = false;
                        _setObj.isReference = false;
                        _setObj.isNotChecked = true;
                        _setObj.iconname = 'utility:record';
                    }
                    // Using Picklist 
                    if(_setObj.fieldName === 'GP_Project_Currency__c' || _setObj.fieldName === 'GP_Billing_Currency__c') {
                        _setObj.isPicklist = true;
                        _setObj.isField = false;
                        _setObj.isReference = false;
                        _setObj.isNotChecked = false;
                        _setObj.isNotReadonly = false;
                        _setObj.objectname = 'GP_Project__c';
                        _setObj.fieldname = _setObj.fieldName === 'GP_Project_Currency__c' ? 'GP_Project_Currency__c' : 'GP_Billing_Currency__c';
                    }
                    _setObj.key = sec.fieldName;
                    listOfValuesToShow.push(_setObj);
                }
            }
        });
        this.pIDSummarySectionDetails = listOfValuesToShow;
        this.isPIDSummary = true;
    }
    // Add additional fields per PID world.
    addAdditionalFields(listOfValuesToShow, projectObj, listOfProjectAddresses, listOfProjectMandatoryKeyMembers) {

        if(listOfProjectAddresses === undefined && listOfProjectMandatoryKeyMembers === undefined) {
            return;
        }

        let pidApprover = '';
        let billingApprover = '';
        let billingSpoc = '';

        if(listOfProjectMandatoryKeyMembers !== undefined) {
            (listOfProjectMandatoryKeyMembers).forEach(lmkm => {
                if(lmkm.GP_Type_of_Leadership__c === 'PID Approver Finance') {
                    pidApprover = lmkm.GP_Employee_ID__c;
                } else if(lmkm.GP_Type_of_Leadership__c === 'Billing Approver') {
                    billingApprover = lmkm.GP_Employee_ID__c;
                } else if(lmkm.GP_Type_of_Leadership__c === 'Billing Spoc') {
                    billingSpoc = lmkm.GP_Employee_ID__c;
                }
            });
        }
        
        if(projectObj.RecordType.Name === 'Indirect PID') {
            listOfValuesToShow.push(this.additionalFieldObj(pidApprover, 'GP_Mob_PID_Approver_Finance__c', 'PID Approver Finance', 
            'GP_Employee_Master__c', 'GP_Employee_Unique_Name__c,Name'));
        } else {
            listOfValuesToShow.push(this.additionalFieldObj(billingApprover, 'GP_Mob_Billing_Approver__c', 'Billing Approver', 
            'GP_Employee_Master__c', 'GP_Employee_Unique_Name__c,Name'));
            listOfValuesToShow.push(this.additionalFieldObj(billingSpoc, 'GP_Mob_Billing_Spoc__c', 'Billing Spoc', 
            'GP_Employee_Master__c', 'GP_Employee_Unique_Name__c,Name'));

            if(projectObj.RecordType.Name === 'BPM') {
                listOfValuesToShow.push(this.additionalFieldObj(pidApprover, 'GP_Mob_PID_Approver_Finance__c', 'PID Approver Finance', 
                'GP_Employee_Master__c', 'GP_Employee_Unique_Name__c,Name'));
            }
            if(listOfProjectAddresses !== undefined) {
                listOfValuesToShow.push(this.additionalFieldObj(listOfProjectAddresses[0].GP_Customer_Name__c, 'GP_Mob_Customer_Name__c', 'Customer Name', 
                'GP_Customer_Master__c', 'GP_CUSTOMER_NUMBER__c,Name'));
                listOfValuesToShow.push(this.additionalFieldObj(listOfProjectAddresses[0].GP_Bill_To_Address__c, 'GP_Mob_Bill_To__c', 'Bill To Name', 
                'GP_Address__c', 'GP_SITE_NUMBER__c,GP_Address__c'));
                listOfValuesToShow.push(this.additionalFieldObj(listOfProjectAddresses[0].GP_Ship_To_Address__c, 'GP_Mob_Ship_To__c', 'Ship To Name', 
                'GP_Address__c', 'GP_SITE_NUMBER__c,GP_Address__c'));
            }
        }
    }
    // additional Field Obj
    additionalFieldObj(value, field, label, referenceName, displayFields) {
        let _setObj = {};
        _setObj.fieldName = field;
        _setObj.fieldLabel = label;
        _setObj.isReference = true;
        _setObj.searchfield = 'Name';
        _setObj.displayTextFields = displayFields;
        _setObj.placeholderValue = 'Search ' + label;
        _setObj.referenceName = referenceName;
        _setObj.value = value;
        _setObj.key = field;

        return _setObj;
    }

    // Update file url to show
    handleViewFile(event) {
        this.fileURL = event.detail;
        this.handlePopupDisplay();
    }
    // handle show and hide of popup
    handlePopupDisplay() {
        let divId = '[data-div-id="myViewFileModal"]';
        let selectDiv = this.template.querySelector(divId);

        if(selectDiv) {
            if(!this.isShowPopup) {
                this.isShowPopup = true;
                selectDiv.classList.remove('fade');
                selectDiv.classList.add('show');
                selectDiv.classList.remove('hide');
            } else {
                this.isShowPopup = false;
                selectDiv.classList.add('fade');
                selectDiv.classList.add('hide');
                selectDiv.classList.remove('show');
            }            
        }
    }
}