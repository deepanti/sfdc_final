import { LightningElement, track, api } from 'lwc';
import fetchPicklistValues from '@salesforce/apex/GPLWCServiceCustomComponent.fetchPicklistValues';

export default class GpLWCCustomPicklist extends LightningElement {
    @api objectname = "GP_Project__c";
    @api fieldname;
    @api valuefield;
    @api label = "";    
    @api disabled;
    @api isNotReadonly = false;
    @track _valueToDisplay = '';
    @track _selectedValue;
    @track picklistvalues = [];
    @track error;
    @track _picklistValueSet;
    @api _isFieldRequired;
    @api errormessage;
    @api inputId;

    @api
    get selectedValue() {
        return this._selectedValue;
    }
    set selectedValue(value) {
        //this.setAttribute('selectedValue', value);
        this._selectedValue = value;
        this.getPicklistValues();
    }

    @api
    get picklistValueSet() {
        return this._picklistValueSet;
    }
    set picklistValueSet(value) {
        //this.setAttribute('picklistValueSet', value);
        this._picklistValueSet = value;
        this.getPicklistValues();
    }

    @api 
    get isFieldRequired() {
        return this._isFieldRequired;
    }
    set isFieldRequired(value) {
        this._isFieldRequired = value;
        this.validateCustomPicklist();
    }

    connectedCallback () {
        this.getPicklistValues();
    }

    getPicklistValues() {
        if(this._picklistValueSet && this._picklistValueSet.length > 0) {
            this.handlePicklistValueSet();
        } else if(this.objectname !== '') {
            fetchPicklistValues(
                {
                    sObjectName : this.objectname,
                    fieldapi : this.fieldname, 
                    selectedValue : this.selectedValue
                }
            )
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchPicklistValuesHandler(responseData);
                }
            });
        }
    }

    fetchPicklistValuesHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        this.picklistvalues = response;
        // Display value readonly
        if(!this.isNotReadonly) {
            (this.picklistvalues).forEach(pickVal => {
                if(pickVal.isdefault) {
                    this._valueToDisplay = pickVal.labelval;
                }
            });            
        }
    }

    handlePicklistValueSet() {
        this.picklistvalues = [];

        (this._picklistValueSet).forEach(pickVal => {
            let picklistvalue = {};
            picklistvalue.apival = pickVal;
            picklistvalue.labelval = pickVal;

            if(this._selectedValue && this._selectedValue === pickVal){
                picklistvalue.isdefault = true;
            } else {
                picklistvalue.isdefault = false;
            }
            this.picklistvalues.push(picklistvalue);
        });
    }

    handleKeyChange(event) {
        var selectedval = event.target.value;
        this.valuefield = selectedval;   
        const picklistSelectEvent =  new CustomEvent('gppicklistselect', {detail: selectedval});
        this.dispatchEvent(picklistSelectEvent);
        //this.validateCustomPicklist();
    }

    validateCustomPicklist() {
        let selectDivErrorDOM = this.template.querySelector('[data-div-id="'+this.inputId+'"]');
        if(selectDivErrorDOM && this._isFieldRequired /*&& (this.valuefield === '--NONE--' || this.valuefield === ' ')*/) {
            selectDivErrorDOM.classList.add('errorLWC');
        } else {
            if(selectDivErrorDOM && selectDivErrorDOM.classList.contains('errorLWC')) {
                selectDivErrorDOM.classList.remove('errorLWC');
            }
        }        
    }
}