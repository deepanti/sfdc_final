import { LightningElement, track, api } from 'lwc';
import fetchLookupRecords from '@salesforce/apex/GPLWCServiceCustomComponent.fetchLookupRecords';

export default class GpLWCCustomPicklist extends LightningElement {
    @track error;
    @track _filterString;
    @track searchString = "";
    @track records;
    @api isNotReadonly = false;
    @api selectedRecord;
    @api _isFieldRequired;
    @api label = "";
    @api iconname = "slds-icon-custom-11";
    @api objectName = 'GP_Project__c';
    @api searchfield = 'GP_Oracle_PID__c';
    @api displayTextSeparator = ' -- ';
    @api displayTextFields = 'GP_Oracle_PID__c,Name';
    @api searchPlaceholderMessage = 'Search Oracle PID..';
    @api referenceId;
    @api index;
    @api referencetype;
    @api 
    get disabled() {
        return this._disabled;
    }
    set disabled(value) {
        //this.setAttribute('disabled', value);
        if(value) {
            this._disabled = true;
        } else {
            this._disabled = false;
        }
    }
    @track _disabled = false;
    //@track textToDisplay;

    @api 
    get filterString() {
        return this._filterString;
    }
    set filterString(value) {
        //this.setAttribute('filterString', value);
        this._filterString = value;
    }

    @api 
    get isFieldRequired() {
        return this._isFieldRequired;
    }
    set isFieldRequired(value) {
        this._isFieldRequired = value;
        this.validateCustomLookup();
    }

    @api searchInputId;

    connectedCallback () {
        // if lookup value already exist in db, show  
        if(this.referenceId && this.referenceId !== '') {
            // call apex to know the value
            fetchLookupRecords(
                {
                    objectName : this.objectName,
                    searchKey : this.referenceId,
                    searchfield : this.searchfield,
                    filterString : this._filterString,
                    displayTextSeparator : this.displayTextSeparator,
                    displayTextFields : this.displayTextFields,
                    type : this.referencetype ? this.referencetype : 'reference'
                }
            )
            .then(result => { 
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchLookupRecordsHandler(responseData, true);
                } else {
                    this.error = responseData.message;
                    this.records = undefined;
                }
            }).catch(error => {  
                this.error = error;
                this.records = undefined;
            });
        }
    }

    handleOnchange(event) {
        const searchKey  = event.target.value;
        //this.searchString = searchKey;
        if(searchKey.length < 2) {
            return;
        }
        if(searchKey.length >= 2) {
            fetchLookupRecords(
                {
                    objectName : this.objectName,
                    searchKey : searchKey,
                    searchfield : this.searchfield,
                    filterString : this._filterString,
                    displayTextSeparator : this.displayTextSeparator,
                    displayTextFields : this.displayTextFields,
                    type : 'lookup'
                }
            )
            .then(result => { 
                var responseData = result;
                if(responseData.isSuccess) {
                    this.fetchLookupRecordsHandler(responseData, false);
                } else {
                    this.error = responseData.message;
                    this.records = undefined;
                }
            }).catch(error => {  
                this.error = error;
                this.records = undefined;
            });
        }
        //this.validateCustomLookup();
    }

    fetchLookupRecordsHandler(responseData, isReference) {
        var response = JSON.parse(responseData.response) || {};
        if(this.referenceId && isReference) {
            let records = response.lookupRecordData;
            if(records.length > 0) {
                this.selectedRecord = records[0];
            } else {
                this.selectedRecord = undefined;
            }
            //this.validateCustomLookup();
        } else {
            this.records = response.lookupRecordData;
        }        
        this.error = undefined;
    }

    handleSelect(event) {
        //const selectedRecordId = event.detail;
        var selectedRecordId = event.currentTarget.getAttribute("data-attribute-index");
        this.selectedRecord = this.records.find( record => record.Id === selectedRecordId);
        const selectedRecordEvent = new CustomEvent("gpselectrecord", { detail : { recordId : selectedRecordId, index : this.index, selectedRecord : this.selectedRecord}});
        this.dispatchEvent(selectedRecordEvent);
        this.selectedRecord = {...this.selectedRecord};
        //this.validateCustomLookup();
        //this.selectedRecord.Name = this.selectedRecord[this.searchfield];
        /*let fieldToDisplay = this.displayTextFields;
        let separator = this.displayTextSeparator;
        this.textToDisplay = '';
        let fieldSet = fieldToDisplay.split(',');

        (fieldSet).forEach(field => {
            this.textToDisplay = this.textToDisplay + this.selectedRecord[field] + separator;
        });

        this.textToDisplay = this.textToDisplay.slice(0, -separator.length);*/
    }

    handleRemove(event) {
        event.preventDefault();
        this.handleClear();
        const selectedRecordEvent = new CustomEvent("gpselectrecord", { detail : { recordId : undefined, index : this.index, selectedRecord : this.selectedRecord} });
        this.dispatchEvent(selectedRecordEvent);
        this.validateCustomLookup();
    }

    @api
    handleClear(){
        this.selectedRecord = undefined;
        this.records = undefined;
        this.error = undefined;
        this.searchString = '';
    }

    validateCustomLookup() {
        let selectDivErrorDOM = this.template.querySelector('[data-div-id="'+this.searchInputId+'"]');
        if(selectDivErrorDOM && this._isFieldRequired){            
            selectDivErrorDOM.classList.add('errorLWC');
        } else {
            if(selectDivErrorDOM && selectDivErrorDOM.classList.contains('errorLWC')) {
                selectDivErrorDOM.classList.remove('errorLWC');
            }
        }
    }
}