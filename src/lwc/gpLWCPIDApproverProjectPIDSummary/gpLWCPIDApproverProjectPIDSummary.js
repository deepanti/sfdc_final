import { LightningElement, track, api } from 'lwc';

export default class GpLWCPIDApproverProjectPIDSummary extends LightningElement {
    @api pidSummarySectionDetails;
    @track listOfPaginatedRecords = [];
    @track islistNotEmpty = false;

    connectedCallback() {
        this.islistNotEmpty = this.pidSummarySectionDetails.length > 0 ? true:false;
    }
    // Handle Pagination list
    handleListUpdate(event) {
        this.listOfPaginatedRecords = event.detail;
    }
}