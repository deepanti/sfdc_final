import { LightningElement, api } from 'lwc';

export default class GpLWCPIDApproverHomePageListViewRow extends LightningElement {
    @api pidInfo;
    @api key;
}