import { LightningElement, track, api } from 'lwc';
import { handleFailedCallBack } from 'c/gpLWCBaseUtility';
import { handleSuccessCallBack } from 'c/gpLWCBaseUtility';
import { navigateToTab } from 'c/gpLWCBaseUtility';
import { formatDateTime } from 'c/gpLWCBaseUtility';
//import { handleSpinner } from 'c/gpLWCBaseUtility';
import { NavigationMixin } from 'lightning/navigation';
import getProjectHeaderDetailsFromApex from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.getProjectHeaderDetails';
import submissionByPIDApprover from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.submissionByPIDApprover';
import getProjectDetail from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.getProjectDetail';
import serializedMapOfStage from '@salesforce/label/c.GP_Final_Project_Stage_Values';

export default class GpLWCPIDApproverProjectHeaderPage extends NavigationMixin(LightningElement) {
    @api backImageUrl;
    @api isApprove = false;
    @api isReject = false;
    @api recordId;
    @track pidObj;
    @track pidObjToShow = {};
    @track isShowSpinner = false;
    @track isShowApproveRejectButton = false;
    @track showComponent = false;
    @track isIndirectPID = false;
    // Approve and reject
    @track strStageValue;
    @track isRevenueStageValid;
    @track isIndirectProject;
    @track isGEPID;
    @track isPIDUser;
    @track approvalType;
    @track isApprovalTypeApproved;
    @track isApprovalTypeClosed;
    @track taxCodeName;
    @track projectObj;
    @track taxCodeId;
    @track reports;
    @track listOfProductCodes;
    @track productCodeField;
    @track isAvalaraEnableApproval;
    @track finalProjectStage;
    @track lstProjectStage;
    @track serializedMapOfStage = serializedMapOfStage;
    @track isShowPopup = false;
    @track requestType;
    @track comments;
    // REQUIRED FIELDS
    @api taxCodeRequired = false;
    @api productCodeRequired = false;
    @api reportToRequired = false;
    @api pidStageRequired = false;
    @track isNoFieldRefresh = false;

    connectedCallback() {
        this.isShowSpinner = true;
        this.getProjectHeaderDetails();
        this.getProjectDetails();        
    }

    getProjectHeaderDetails() {
        // $recordId takes recordId from page itself.
        getProjectHeaderDetailsFromApex({ recordId : this.recordId })
        .then(result => {
            var responseData = result;
            if(responseData.isSuccess) {
                if(result.isSuccess) {
                    this.getProjectHeaderDetailsHandler(responseData);                    
                    this.showComponent = true;
                }
            } else {
                this.showComponent = false;
                this.isShowSpinner = false;
                handleFailedCallBack(this, responseData.message);
            }
        })
       .catch(error => {
            this.showComponent = false;
            this.isShowSpinner = false;
            handleFailedCallBack(this, error.message);
        });
    }

    getProjectHeaderDetailsHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        this.pidObj = response.project;
        this.parsePIDObject();
    }

    parsePIDObject() {
        this.isIndirectPID = this.pidObj.RecordType.Name !== 'Indirect PID' ? false : true;
        this.pidObjToShow.name = this.pidObj.Name;
        this.pidObjToShow.customerName = this.pidObj.GP_Customer_Master__r !== undefined ? this.pidObj.GP_Customer_Master__r.Name : '';
        this.pidObjToShow.primarySDO = this.pidObj.GP_Primary_SDO__r.Name +'/'+ this.pidObj.GP_Primary_SDO__r.GP_Oracle_Id__c;
        this.pidObjToShow.projectNBillTypes = !this.isIndirectPID ? (this.pidObj.GP_Project_type__c +'/'+ this.pidObj.GP_Bill_Rate_Type__c) : this.pidObj.GP_Project_type__c;
        this.pidObjToShow.oracleStatus = this.pidObj.GP_Oracle_Status__c === undefined ? 'NA' : 
        (this.pidObj.GP_Oracle_Status__c === 'S' && this.pidObj.GP_Resource_Allocation_Failure_Count__c === 0 ? 'Success' : 'Failure');         
        this.pidObjToShow.projectDates = (this.pidObj.GP_Start_Date__c !== undefined ? formatDateTime(this.pidObj.GP_Start_Date__c, false) : '') +' - '
        + (this.pidObj.GP_End_Date__c !== undefined ? formatDateTime(this.pidObj.GP_End_Date__c, false) : '');
        this.pidObjToShow.workflowNumber = this.pidObj.GP_EP_Project_Number__c;
        this.pidObjToShow.oraclePID = this.pidObj.GP_Oracle_PID__c;
        this.pidObjToShow.pidStatus = this.pidObj.GP_Project_Status__c;
        this.pidObjToShow.workflowStatus = this.pidObj.GP_Approval_Status__c;
        this.pidObjToShow.gpmName = this.pidObj.GP_GPM_Employee__r !== undefined ? this.pidObj.GP_GPM_Employee__r.Name : '';
        this.pidObjToShow.currentWorkingName = this.pidObj.GP_Current_Working_User__r !== undefined ? this.pidObj.GP_Current_Working_User__r.Name : '';        
    }

    // Project details for Approve or reject the PID
    getProjectDetails() {        
        //a0k9000001BXqW6AAL
        //let record = 'a0kp0000005xXL9AAM';
        //'$recordId'
        getProjectDetail({ recordId : this.recordId })
        .then(result => {
            var responseData = result;
            if(responseData.isSuccess) {
                if(result.isSuccess) {
                    if(responseData.response) {
                        this.getProjectDetailHandler(responseData);
                    } else {
                        //handleFailedCallBack(this, responseData.message);
                    }
                    this.isShowSpinner = false;
                }
            } else {
                handleFailedCallBack(this, responseData.message);
                this.isShowSpinner = false;
            }
        })
       .catch(error => {
            this.isShowSpinner = false;
            handleFailedCallBack(this, error.message);
        });
    }

    getProjectDetailHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};

        this.strStageValue = response.strStageValue !== undefined ? response.strStageValue : '';
        this.isRevenueStageValid = response.isRevenueStageValid !== undefined ? response.isRevenueStageValid : false;
        this.isIndirectProject = response.isIndirectProject !== undefined ? response.isIndirectProject : false;
        this.isGEPID = (response.projectObj !== undefined && response.projectObj.GP_Business_Group_L1__c !== undefined
             && response.projectObj.GP_Business_Group_L1__c === 'GE') ? true : false;
        this.isPIDUser = response.isPIDUser !== undefined ? response.isPIDUser : false;
        this.approvalType = response.approvalType !== undefined ? response.approvalType : '';
        this.taxCodeName = response.taxCodeName !== undefined ? response.taxCodeName : '';
        this.projectObj = response.projectObj !== undefined ? response.projectObj : {};
        this.taxCodeId = response.taxCodeId !== undefined ? response.taxCodeId : '';
        this.reports = response.reports !== undefined ? response.reports : '';
        this.listOfProductCodes = response.listOfProductCodes !== undefined ? response.listOfProductCodes : [];
        this.productCodeField = this.projectObj.GP_Product_Code__c;
        this.isAvalaraEnableApproval = response.isAvalaraEnableApproval !== undefined ? response.isAvalaraEnableApproval : false;
        this.isApprovalTypeApproved = this.approvalType === 'Approval' ? true : false;
        this.isApprovalTypeClosed = this.approvalType === 'Closure' ? true : false;
        this.requestType = this.isApprovalTypeApproved ? 'approval' : (this.isApprovalTypeClosed ? 'close' : '');

        this.parseThePIDDetails();
        // Show button
        this.isShowApproveRejectButton = true;
    }

    parseThePIDDetails() {
        if(this.approvalType === 'Approval') {
            let lstOfAllProjectStage = JSON.parse(this.serializedMapOfStage) || [];
            let lstProjectStage = [];
            let finalProjectStage = '';
            let stageIndex = 0;

            if(this.isIndirectProject) {
                (lstOfAllProjectStage).forEach(stage => {
                    //if the contract is not signed then don't include approved in list of stages.
                    if (stage.label === "Approved Support") {
                        lstProjectStage.push(stage.label);
                        finalProjectStage = stage.text;
                    }
                });
            } else if(this.approvalType === 'Closure') {
                (lstOfAllProjectStage).forEach(stage => {
                    //if the contract is not signed then don't include approved in list of stages.
                    if (stage.label === "Closed") {
                        lstProjectStage.push(stage.label);
                        finalProjectStage = stage.text;
                    }
                });
            } else {
                (lstOfAllProjectStage).forEach(stage => {
                    //if the contract is not signed thend don't include approved in list of stages.
                    if (!(!this.isRevenueStageValid && stage.label === 'Approved')) {
                        lstProjectStage.push(stage.label);
                    }
    
                    if (stage.label === this.strStageValue) {
                        finalProjectStage = stage.text;
                    }
                });
            }

            if (this.strStageValue === "Cost Charging") {
                stageIndex = 0;
            } else if (this.strStageValue === "Billing Cost Charging") {
                stageIndex = 1;
            } else if (this.strStageValue === 'Revenue Cost Charging') {
                stageIndex = 2;
            } else if (this.strStageValue === 'Approved') {
                stageIndex = 3;
            }

            this.lstProjectStage = lstProjectStage.splice(0, stageIndex + 1);
            this.finalProjectStage = finalProjectStage;
        }
    }

    handleOnSelectCustomPicklist(event) {
        const selectedValue = event.detail;
        let lstOfAllProjectStage = JSON.parse(this.serializedMapOfStage) || [];        
        (lstOfAllProjectStage).forEach(stage => {
            if (stage.label === selectedValue) {
                this.finalProjectStage = stage.text;
            }
        });
    }
    
    handleOnSelectCustomPicklistProductCode(event) {
        const selectedValue = event.detail;
        this.productCodeField = selectedValue;
    }

    handleOnSelectCustomPicklistReports(event) {
        const selectedValue = event.detail;
        this.reports = selectedValue;
    }

    handleOnSelectLookup(event) {
        const selectedValue = event.detail.selectedRecord;
        if(selectedValue) {
            this.taxCodeName = event.detail.selectedRecord.Name;
        } else {
            this.taxCodeName = '';
        }
        this.taxCodeId = event.detail.recordId;
    }

    handlePopupDisplay() {
        let divId = '[data-div-id="myApproveRejectModal"]';
        let selectDiv = this.template.querySelector(divId);

        // refresh the fields
        if(!this.isNoFieldRefresh) {
            this.validatePID(true);
        }

        if(selectDiv) {
            if(!this.isShowPopup) {
                this.isShowPopup = true;
                selectDiv.classList.remove('fade');
                selectDiv.classList.add('show');
                selectDiv.classList.remove('hide');
            } else {
                this.isShowPopup = false;
                selectDiv.classList.add('fade');
                selectDiv.classList.add('hide');
                selectDiv.classList.remove('show');
            }            
        }
    }
    
    handleBackButton() {
        this.navigateToHomePage();
    }
    // Validate the popup
    validatePID(toRefresh) {
        // eslint-disable-next-line no-unused-vars
        let result = true;
        //let selectDivErrorDOM = this.template.querySelector('[data-div-id="select-required-comments"]');
        //let parentSelectDivErrorDOM = this.template.querySelector('[data-div-id="parent-select-required-comments"]');

        /*let pidStageInput = this.template.querySelector('[data-div-id="pid_stage_input"]');
        let taxCodeInput = this.template.querySelector('[data-div-id="tax_code_search"]');
        let reportToInput = this.template.querySelector('[data-div-id="report_to_input"]');
        let productCodeInput = this.template.querySelector('[data-div-id="product_code_input"]');*/
        let textAreaComments = this.template.querySelector('[data-div-id="text_area_comments"]');

        if(toRefresh) {
            this.pidStageRequired = false;
            this.productCodeRequired = false;
            this.reportToRequired = false;
            this.taxCodeRequired = false;
            /*if(pidStageInput && pidStageInput.classList.contains('errorLWC')) {
                pidStageInput.classList.remove('errorLWC');
            }
            if(taxCodeInput && taxCodeInput.classList.contains('errorLWC')) {
                taxCodeInput.classList.remove('errorLWC');
            }
            if(reportToInput && reportToInput.classList.contains('errorLWC')) {
                reportToInput.classList.remove('errorLWC');
            }
            if(productCodeInput && productCodeInput.classList.contains('errorLWC')) {
                productCodeInput.classList.remove('errorLWC');
            }*/
            if(textAreaComments && textAreaComments.classList.contains('errorLWC')) {
                textAreaComments.classList.remove('errorLWC');
            }
        } else {
            // PID stage is required for PID Approvers
            if(this.isPIDUser && !this.finalProjectStage) {
                this.pidStageRequired = true;
                result = false;
            } else {
                this.pidStageRequired = false;
            }
            // PC required for GE PIDs (on PID Approval Page)
            if(this.isPIDUser && !this.productCodeField && this.isGEPID && !this.isIndirectProject) {
                this.productCodeRequired = true;
                result = false;
            } else {
                this.productCodeRequired = false;
            }
            // Report to is required
            if(this.isPIDUser && !this.isIndirectProject && this.finalProjectStage !== '1010' && 
                (this.reports === '--NONE--' || this.reports === '')) {
                this.reportToRequired = true;
                result = false;
            } else {
                this.reportToRequired = false;
            }
            // Tax code is required
            if(this.isPIDUser && !this.isIndirectProject && !this.taxCodeName && !this.projectObj.GP_Operating_Unit__r.GP_Is_GST_Enabled__c
            && !this.projectObj.GP_Service_Deliver_in_US_Canada__c) {
                this.taxCodeRequired = true;
                result = false;
            } else {
                this.taxCodeRequired = false;
            }
            // PID Stage
            /*if(this.pidStageRequired && pidStageInput) {
                pidStageInput.classList.add('errorLWC');
            } else {
                if(pidStageInput.classList.contains('errorLWC')) {
                    pidStageInput.classList.remove('errorLWC');
                }
            }
            // Tax code
            if(this.taxCodeRequired && taxCodeInput) {
                taxCodeInput.classList.add('errorLWC');
            } else {
                if(taxCodeInput.classList.contains('errorLWC')) {
                    taxCodeInput.classList.remove('errorLWC');
                }
            }
            // Report To
            if(this.reportToRequired && reportToInput) {
                reportToInput.classList.add('errorLWC');
            } else {
                if(reportToInput.classList.contains('errorLWC')) {
                    reportToInput.classList.remove('errorLWC');
                }
            }
            // Product Code
            if(this.productCodeRequired && productCodeInput) {
                productCodeInput.classList.add('errorLWC');
            } else {
                if(productCodeInput.classList.contains('errorLWC')) {
                    productCodeInput.classList.remove('errorLWC');
                }
            }*/
            // Comments
            if(!textAreaComments.value) {
                /*if(selectDivErrorDOM) {
                    this.errormessage = 'Comments are required';
                    selectDivErrorDOM.classList.add('slds-form-element__help');
                    selectDivErrorDOM.classList.remove('slds-hide');
                    parentSelectDivErrorDOM.classList.add('slds-has-error');
                }*/
                if(textAreaComments) {
                    textAreaComments.classList.add('errorLWC');
                }
                result = false;
            } else {
                /*if(selectDivErrorDOM) {
                    this.errormessage = '';
                    selectDivErrorDOM.classList.remove('slds-form-element__help');
                    selectDivErrorDOM.classList.add('slds-hide');
                    parentSelectDivErrorDOM.classList.remove('slds-has-error');
                }*/
                if(textAreaComments) {
                    // Send the comment to approval history section
                    this.comments = textAreaComments.value;
                    textAreaComments.classList.remove('errorLWC');
                }
            }
        }
        // eslint-disable-next-line no-alert
        //alert(result + '-C-' + textAreaComments.value + '-TC-' + this.taxCodeName + '-R-'+ this.reports + '-PS-' + this.finalProjectStage + '-PC-' + this.productCodeField);
        return result;
    }

    handleApprovePID() {
        this.isShowSpinner = true;
        if(this.validatePID(false)) {
            this.isNoFieldRefresh = true;
            this.handlePopupDisplay();
            this.isNoFieldRefresh = false;
            submissionByPIDApprover(
                {
                    recordId : this.recordId,
                    requestType : this.requestType,
                    submissionType : 'approve',
                    strSelectedStage : this.finalProjectStage,
                    strComment : this.comments,
                    taxCode : this.taxCodeName,
                    reports : this.reports,
                    productCode : this.productCodeField
                }
            )
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    handleSuccessCallBack(this, responseData.message);
                    this.isShowSpinner = false;
                    this.navigateToHomePage();
                } else {
                    this.isShowSpinner = false;
                    handleFailedCallBack(this, responseData.message);
                }
            })
           .catch(error => {
                this.isShowSpinner = false;
                handleFailedCallBack(this, error.message);
            });
        } else {
            this.isShowSpinner = false;
            handleFailedCallBack(this, 'Highlighted fields are mandatory');
        }
    }

    handleRejectPID() {
        this.isShowSpinner = true;
        if(this.validatePID(false)) {
            this.isNoFieldRefresh = true;
            this.handlePopupDisplay();
            this.isNoFieldRefresh = false;
            submissionByPIDApprover(
                {
                    recordId : this.recordId,
                    requestType : this.requestType,
                    submissionType : 'reject',
                    strSelectedStage : this.finalProjectStage,
                    strComment : this.comments,
                    taxCode : this.taxCodeName,
                    reports : this.reports,
                    productCode : this.productCodeField
                }
            )
            .then(result => {
                var responseData = result;
                if(responseData.isSuccess) {
                    handleSuccessCallBack(this, responseData.message);
                    this.isShowSpinner = false;
                    this.navigateToHomePage();
                } else {
                    this.isShowSpinner = false;
                    handleFailedCallBack(this, responseData.message);
                }
            })
           .catch(error => {
                this.isShowSpinner = false;
                handleFailedCallBack(this, error.message);
            });
        } else {
            this.isShowSpinner = false;
            handleFailedCallBack(this, 'Highlighted fields are mandatory');
        }
    }
    // Navigate the page to home page.
    navigateToHomePage() {
        navigateToTab(this, 'Pending_PIDs');
    }
}