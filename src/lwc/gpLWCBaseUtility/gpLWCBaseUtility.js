import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class GpLWCBaseUtility extends NavigationMixin(LightningElement) {}

// Show Toast Message.
const dispatchShowToastEvent = (context , variant , mode, title, message) => {
    const showToastEvent = new ShowToastEvent({
        title : title,
        message : message,
        mode : mode,
        variant : variant
    });
    context.dispatchEvent(showToastEvent); 
};

// Handle Failure and Show Toast Message (Error).
const handleFailedCallBack = (context , message) => {
    var elem = document.createElement('textarea');
    // eslint-disable-next-line @lwc/lwc/no-inner-html
    elem.innerHTML = message;
    let decoded = elem.value;
    dispatchShowToastEvent(context, 'error', 'dismissable', 'Error',decoded );
};

// Handle Sucess and Show Toast Message (Success).
const handleSuccessCallBack = (context , message) => {
    dispatchShowToastEvent(context, 'success', 'dismissable', 'Success', message);
};

// Handle spinner (show/hide)
const handleSpinner = (context, id, isShow) => {
    let divIdCurrent = '[data-div-id=\'' + id + '\']';
    let selectDivCurrent = context.template.querySelector(divIdCurrent);
    if(selectDivCurrent) {
        if(isShow && selectDivCurrent.classList.contains('hide')) {
            selectDivCurrent.classList.remove('hide');
        } else if(!isShow && !selectDivCurrent.classList.contains('hide')) {
            selectDivCurrent.classList.add('hide');
        }
    }
};

// Navigate to standard record page.
const navigateToObjectHome = (context, recordId, objectAPIName, actionName) => {
    context[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            recordId: recordId,
            objectApiName: objectAPIName, 
            actionName: actionName
        }
    });
}

// For more navigation details go to 
//https://developer.salesforce.com/docs/component-library/documentation/en/lwc/lwc.use_navigate_page_types
// Navigate to custom tab.
const navigateToTab = (context, tabAPIName) => {
    context[NavigationMixin.Navigate]({
        type: 'standard__navItemPage',
        attributes: {
            apiName: tabAPIName
        }
    });
}

// Navigate To Component.
const navigateToComponent = (context, componentName, dataToBePassed) => {
    context[NavigationMixin.Navigate]({
        type: "standard__component",
        attributes: {
            componentName: "c__GMCmpAuraRedirectToLWC"
        },
        state : {
            c__componentName : componentName,
            c__dataToBePassed : dataToBePassed
        }
    });
}

// Navigate To Component Direct Aura.
const navigateToComponentDirectAura = (context, componentName, dataToBePassed) => {
    context[NavigationMixin.Navigate]({
        type: "standard__component",
        attributes: {
            componentName: componentName
        },
        state : {
            c__dataToBePassed : dataToBePassed
        }
    });
}
/*pubSub Logic*/
const events = {};

const samePageRef = (pageRef1, pageRef2) => {
    const obj1 = pageRef1.attributes;
    const obj2 = pageRef2.attributes;
    return Object.keys(obj1)
        .concat(Object.keys(obj2))
        .every(key => {
            return obj1[key] === obj2[key];
        });
};

/**
 * Registers a callback for an event
 * @param {string} eventName - Name of the event to listen for.
 * @param {function} callback - Function to invoke when said event is fired.
 * @param {object} thisArg - The value to be passed as the this parameter to the callback function is bound.
 */
const registerListener = (eventName, callback, thisArg) => {
    // Checking that the listener has a pageRef property. We rely on that property for filtering purpose in fireEvent()
    if (!thisArg.pageRef) {
        throw new Error(
            'pubsub listeners need a "@wire(CurrentPageReference) pageRef" property'
        );
    }

    if (!events[eventName]) {
        events[eventName] = [];
    }
    const duplicate = events[eventName].find(listener => {
        return listener.callback === callback && listener.thisArg === thisArg;
    });
    if (!duplicate) {
        events[eventName].push({ callback, thisArg });
    }
};

/**
 * Unregisters a callback for an event
 * @param {string} eventName - Name of the event to unregister from.
 * @param {function} callback - Function to unregister.
 * @param {object} thisArg - The value to be passed as the this parameter to the callback function is bound.
 */
const unregisterListener = (eventName, callback, thisArg) => {
    if (events[eventName]) {
        events[eventName] = events[eventName].filter(
            listener =>
                listener.callback !== callback || listener.thisArg !== thisArg
        );
    }
};

/**
 * Unregisters all event listeners bound to an object.
 * @param {object} thisArg - All the callbacks bound to this object will be removed.
 */
const unregisterAllListeners = thisArg => {
    Object.keys(events).forEach(eventName => {
        events[eventName] = events[eventName].filter(
            listener => listener.thisArg !== thisArg
        );
    });
};

/**
 * Fires an event to listeners.
 * @param {object} pageRef - Reference of the page that represents the event scope.
 * @param {string} eventName - Name of the event to fire.
 * @param {*} payload - Payload of the event to fire.
 */
const fireEvent = (pageRef, eventName, payload) => {
    if (events[eventName]) {
        const listeners = events[eventName];
        listeners.forEach(listener => {
            if (samePageRef(pageRef, listener.thisArg.pageRef)) {
                try {
                    listener.callback.call(listener.thisArg, payload);
                } catch (error) {
                    // fail silently
                }
            }
        });
    }
};

/**
 * Section sequence
 */
const pinnacleSectionSequence = () => {
    let sectionSequence = {
        "Project":"Bussiness_Hierarchy,Project_Info,Address,MandatoryKeyMembersLeadership,CMITS_Fields,OtherClassification,DispatchInfo,GEProjectDetails,OracleStatus",
        "ProjectLeadership":"AccountLeadership,HSLLeadership,RegionalLeadership,AdditionalAccessLeadership",
        "WorklocationAndSDO":"Worklocation,SDO",
        "BudgetAndExpenses":"Expenses,Budget_Pricing",
        "Documents":"PODocument,OtherDocument"
        };
    return sectionSequence;
};

/**
 * Header labels for Pinnacle
 */
const pinnacleheadersLabel = () => {
    let headerlabel = {
        "Project":"Project View",
        "WorklocationAndSDO":"Work Location & Additional SDO",
        "ProjectLeadership":"Project Leaderships",
        "GP_Billing_Milestone__c":"Billing Milestone",
        "BudgetAndExpenses":"Budget & Expense",
        "ProfileBillRates":"Profile Bill Rate",
        "ProjectResourceAllocation":"Resource Allocation",
        "Documents":"Document Upload",
        "ChangeSummary":"Change Summary",
        "PIDSummary":"Summary",
        "Project_Info":"Project/Contract Info",
        "Address":"Bill To & Ship To Address",
        "MandatoryKeyMembersLeadership":"Mandatory Key Members",
        "RegionalLeadership":"Regional Leadership",
        "AccountLeadership":"Account Leadership",
        "HSLLeadership":"HSL Leadership",
        "AdditionalAccessLeadership":"Additional Access Leadership",
        "CMITS_Fields":"CMITS Specific Details",
        "Bussiness_Hierarchy":"Business & Product Hierarchy",
        "GEProjectDetails":"GE Project Details",
        "DispatchInfo":"Dispatch Info",
        "OtherClassification":"Other Classifications",
        "OracleStatus":"Oracle Status",
        "WorkLocation":"Work Location",
        "AdditionalSDO":"Lending SDO",
        "PODocument":"PO Document",
        "OtherDocument":"Other Document",
        "Expenses":"Expense",
        "Budget_Pricing":"Budget And Pricing"
        };
    return headerlabel;
};

const pinnacleLookupFieldsMapping = () => {
    // if problem is mentioned by testing team that same fields is different objects should have different attributes
    // then use object name first in json, for example
    // "GP_Resource_Allocation":{"GP_Work_Location__c":{"displayTextFields":"GP_Oracle_Id__c,Name","placeholderValue":"Search Work Location for Employee"},...},
    // "GP_Project_Work_Location_SDO__c":{"GP_Work_Location__c":{"displayTextFields":"Name","placeholderValue":"Search Work Location for PID."},...},
    
    let lookupMapping = {
        "GP_Customer_Hierarchy_L4__c":{"displayTextFields":"Name","placeholderValue":"Search Customer Hierarchy (L4)"},
        "GP_Parent_PID_For_Gainshare__c":{"displayTextFields":"GP_Oracle_PID__c,Name","placeholderValue":"Search Parent PID for Gainsahare project"},
        "GP_GOL__c":{"displayTextFields":"GP_Employee_Unique_Name__c,Name","placeholderValue":"Search GOL"},
        "GP_GRM__c":{"displayTextFields":"GP_Employee_Unique_Name__c,Name","placeholderValue":"Search GRM"},
        "GP_Collector__c":{"displayTextFields":"GP_Employee_Unique_Name__c,Name","placeholderValue":"Search Collector"},
        "GP_City__c":{"displayTextFields":"Name","placeholderValue":"Search City for PID"},
        "GP_Project_Organization__c":{"displayTextFields":"Name,GP_Oracle_Id__c","placeholderValue":"Search Project Organization"},
        "GP_CRN_Number__c":{"displayTextFields":"Name","placeholderValue":"Search CRN Number"},
        "GP_Operating_Unit__c":{"displayTextFields":"Name,GP_Oracle_Id__c","placeholderValue":"Search Operating unit"},
        "GP_Primary_SDO__c":{"displayTextFields":"GP_Oracle_Id__c,Name","placeholderValue":"Search Primary SDO"},
        "GP_Portal_Name__c":{"displayTextFields":"Name,GP_Oracle_Id__c","placeholderValue":"Search Portal Name"},
        "GP_HSL__c":{"displayTextFields":"Name","placeholderValue":"Search HSL"},
        "GP_VSL__c":{"displayTextFields":"Name","placeholderValue":"Search VSL"},
        "GP_Parent_Customer_Sub_Division__c":{"displayTextFields":"Name","placeholderValue":"Search Sub Division"},
        "GP_SILO_Names__c":{"displayTextFields":"Name","placeholderValue":"Search SILO Name"},
        "GP_Customer_Name__c":{"displayTextFields":"GP_CUSTOMER_NUMBER__c,Name","placeholderValue":"Search Customer"},
        "GP_Bill_To_Address__c":{"displayTextFields":"GP_SITE_NUMBER__c,GP_Address__c","placeholderValue":"Search Bill to Address"},
        "GP_Ship_To_Address__c":{"displayTextFields":"GP_SITE_NUMBER__c,GP_Address__c","placeholderValue":"Search Ship to Address"},
        "GP_Employee_ID__c":{"displayTextFields":"GP_Employee_Unique_Name__c,Name","placeholderValue":"Search Employee with OHR"},
        "GP_Work_Location__c":{"displayTextFields":"GP_Oracle_Id__c,Name","placeholderValue":"Search Work Location"},
        "GP_Employee__c":{"displayTextFields":"GP_Employee_Unique_Name__c,Name","placeholderValue":"Search Employee with OHR"},
        "GP_SDO__c":{"displayTextFields":"GP_Oracle_Id__c,Name","placeholderValue":"Search lending SDO with OHR"},
        "GP_Payment_Terms__c":{"displayTextFields":"Name","placeholderValue":"Search Payment Terms"}
    };
    return lookupMapping;
};
/**
 * Format date time if fields.
 * @param {*} datetime 
 * @param {*} isDateTimeOnly 
 */
const formatDateTime = (dateTime, isDateTimeOnly) => {
    var dateT = new Date(dateTime);
    var month = dateT.getMonth();
    var hr = dateT.getHours();
    var min = dateT.getMinutes();
    var sec = dateT.getSeconds();
    var t = 'AM';
    var dateStr = '';
    var mon = '';

    if(month === 0) {
        mon = 'Jan';
    } else if(month === 1) {
        mon = 'Feb';
    } else if(month === 2) {
        mon = 'Mar';
    } else if(month === 3) {
        mon = 'Apr';
    } else if(month === 4) {
        mon = 'May';
    } else if(month === 5) {
        mon = 'Jun';
    } else if(month === 6) {
        mon = 'Jul';
    } else if(month === 7) {
        mon = 'Aug';
    } else if(month === 8) {
        mon = 'Sep';
    } else if(month === 9) {
        mon = 'Oct';
    } else if(month === 10) {
        mon = 'Nov';
    } else {
        mon = 'Dec';
    }
    dateStr = mon + ' ' + dateT.getDate() + ', ' + dateT.getFullYear();    
    if(isDateTimeOnly) {        
        if(hr > 12) {
            hr = hr - 12;
            t = 'PM';
        }
        if(hr === 12) {
            t = 'PM';
        }
        if(min < 10) {
            min = '0' + min;
        }
        if(sec < 10) {
            sec = '0' + sec;
        }
        dateStr += ' ' + hr + ':' + min + ':' + sec + ' ' + t;
    }
    return dateStr;
};
/**
 * Format number to number with commas.
 * @param {*} number
 */
const formatNumber = (number) => {
    if(number && number !== 0.00) {
        number = number.toLocaleString();
        if(number.indexOf('.') === -1) {
            number = number + '.000';
        } else {
            //var numberList = number.split('.');
        }
    } else {
        number = '0.000';
    }
    return number;
};

/**
 * Field labels for Pinnacle
 */
const pinnacleFieldLabel = () => {
    let fieldLabel = {
        "GP_LWC_Business_Product_Hierarchy_Fields":{"GP_Customer_Hierarchy_L4__c":"Account Name","GP_Sales_Opportunity_Id__c":"Opportunity Id"},
        "GP_LWC_Project_Info_Fields":{"GP_Primary_SDO__c":"SDO","GP_Parent_PID_For_Gainshare__c":"Parent Gainshare/Valueshare PID",
        "GP_CrossSellCharge__c":"Cross Sell Charge","GP_Start_Date__c":"Start Date","GP_End_Date__c":"End Date",
        "GP_Project_Organization__c":"Project Owning Organization"},
        "GP_LWC_CMITS_Specific_Fields":{"GP_Parent_Customer_Sub_Division__c":"Sub Division"},
        "GP_LWC_Other_Classification_Fields":{"GP_Daily_Normal_Hours__c":"Daily Normal Hours","GP_Is_PO_Required__c":"Is PO Required",
        "GP_Invoice_at_Person_Level__c":"Invoice At Person Level","GP_Parent_Project_for_Group_Invoice__c":"Parent Project For Group Invoice",
        "GP_Last_Update_Time_of_Child_Records__c":"Last Update Time Of Child Records","GP_Pincode__c":"Pin Code","GP_Valid_Till__c":"ValidTill",
        "GP_Reports__c":"Invoice Report"},
        "GP_LWC_Dispatch_Info_Fields":{"GP_CC1__c":"Email CC1","GP_CC2__c":"Email CC2","GP_CC3__c":"Email CC3","GP_FAX_Number__c":"Fax Number"},
        "GP_LWC_GE_Project_Detail_Fields":{"GP_Stakeholder_Email_ID__c":"Stakeholder Email"},
        "GP_LWC_Oracle_Status_Fields":{"GP_Oracle_Process_Log__c":"Process Log"},
        "GP_LWC_Project_Leadership_Fields":{"GP_Type_of_Lebadership__c":"ROLE","GP_Employee_ID__c":"EMPLOYEE","GP_Start_Date__c":"START DATE",
        "GP_Pinnacle_End_Date__c":"END DATE"},
        "GP_LWC_Project_Address_Fields":{"GP_Relationship__c":"RELATIONSHIP","GP_Customer_Name__c":"CUSTOMER NAME",
        "GP_Bill_To_Address__c":"BILL TO ADDRESS","GP_Ship_To_Address__c":"SHIP TO ADDRESS"},
        "GP_LWC_Work_Location_Fields":{"GP_Work_Location__c":"WORK LOCATION","GP_Primary__c":"PRIMARY","GP_is_Active__c":"DEACTIVATE",
        "GP_BCP__c":"BCP","GP_Mobile_Legal_Entity__c":"LEGAL ENTITY","GP_Mobile_Country__c":"COUNTRY"},
        "GP_LWC_SDO_Fields":{"GP_Work_Location__c":"ADDITIONAL SDO","GP_Mobile_COE_Code__c":"COE CODE"},
        "GP_LWC_Billing_Milestone_Fields":{"Name":"MILESTONE NAME","GP_Date__c":"MILESTONE DATE","GP_Milestone_start_date__c":"MILESTONE START DATE",
        "GP_Milestone_end_date__c":"MILESTONE END DATE","GP_Work_Location__c":"WORK LOCATION","GP_Entry_type__c":"ENTRY TYPE","GP_Value__c":"VALUE",
        "GP_Amount__c":"AMOUNT"},
        "GP_LWC_Budget_Fields":{"GP_Band__c":"BAND","GP_Country__c":"COUNTRY","GP_Product__c":"PRODUCT","GP_Track__c":"TRACK",
        "GP_Effort_Hours__c":"EFFORT(HOURS)","GP_Mobile_TCV__c":"TOTAL COST(USD)"},
        "GP_LWC_Expense_Fields":{"GP_Expense_Category__c":"EXPENSE CATEGORY","GP_Expenditure_Type__c":"EXPENDITURE TYPE","GP_Location__c":"LOCATION",
        "GP_Amount__c":"AMOUNT","GP_Remark__c":"REMARKS"},
        "GP_LWC_Resource_Allocation_Sequence":{"GP_Employee__c":"EMPLOYEE","GP_Oracle_Status__c":"ORACLE STATUS","GP_Oracle_Id__c":"ORACLE ID",
        "GP_Start_Date__c":"START DATE","GP_End_Date__c":"END DATE","GP_Work_Location__c":"WORK LOCATION","GP_Profile__c":"PROFILE",
        "GP_Bill_Rate__c":"BILL RATE","GP_Shadow_type__c":"SHADOW TYPE","GP_SDO__c":"LENDING SDO","GP_Committed_SOW__c":"ALLOC. STATUS",
        "GP_Allocation_Type__c":"ALLOC. TYPE","GP_Percentage_allocation__c":"% ALLOCATION","GP_MS_Attention_To__c":"ATTENTION TO  (MS)",
        "GP_MS_BusinessUnit_Manager__c":"BUSINESSUNIT MANAGER (MS)","GP_MS_MER_No__c":"MER NO (MS)","GP_MS_Address__c":"ADDRESS (MS)",
        "GP_MS_Email_Id__c":"EMAIL ID (MS)","GP_MS_ID__c":"MS ID (MS)","GP_MS_MANAGER__c":"MANAGER (MS)","GP_COST_CENTER__c":"COST CENTER (MS)",
        "GP_MS_Payment_Mode__c":"PAYMENT MODE (MS)","GP_CM_Address__c":"ADDRESS (CVS HEALTH)","GP_CM_Id__c":"Z ID (CVS HEALTH)",
        "GP_Interviewed_by_Customer__c":"RESOURCE SELECTED"},
        "GP_LWC_Other_Document_Fields":{"Name":"NAME","GP_Type__c":"TYPE","GP_Document_Creation_Date__c":"UPLOAD DATE","GP_Mob_File_View_URL__c":"VIEW FILE"},
        "GP_LWC_PO_Document_Fields":{"GP_PO_Number__c":"PO NUMBER","GP_PO_Start_Date__c":"START DATE","GP_PO_End_Date__c":"END DATE","GP_PO_Amount__c":"AMOUNT",
        "GP_Payment_Terms__c":"PAYMENT TERMS","GP_PO_Remarks__c":"REMARKS","GP_PO_Necessary_On_Invoice__c":"NECESSARY FOR INVOICE","GP_Mob_File_View_URL__c":"VIEW FILE"},
        "GP_LWC_Profile_Bill_Rate_Fields":{"GP_Profile__c":"PROFILE","GP_Bill_Rate__c":"BILL RATE","GP_Bill_Rate_Type__c":"BILL RATE TYPE"},
        "GP_LWC_Project_Summary_Fields_Indirect":{"GP_End_Date__c":"End Date","GP_Start_Date__c":"Start Date","GP_Primary_SDO__c":"SDO","GP_CC1__c":"Email CC1",
        "GP_CC2__c":"Email CC2","GP_CC3__c":"Email CC3","GP_FAX_Number__c":"Fax Number"},
        "GP_LWC_Project_Summary_Fields_BPM":{"GP_Service_Deliver_in_US_Canada__c":"Service Delivery in US/Canada"},
        "GP_LWC_Project_Summary_Fields_PBB":{"GP_Service_Deliver_in_US_Canada__c":"Service Delivery in US/Canada"}
    };
    return fieldLabel;
};

/*const projectCurrencyMapping = () => {
    let pcMap = {
        "UAE":"Dirham", "AUD":"Australian Dollar", "BRL":"Brazilian Real", "CAD":"Canadian Dollar", "CHF":"Swiss Franc", "CNY":"Chinese Yuan", 
        "CZK":"Czech Koruna", "EUR":"Euro", "GBP":"British Pound", "GTQ":"Guatemala Quetzal", "HKD":"Hong Kong Dollar", "INR":"Indian Rupee", 
        "JPY":"Japanese Yen", "MXN":"Mexican Peso", "MYR":"Malaysian Ringgit", "PHP":"Philippine Peso", "SGD":"Singapore Dollar", 
        "THB":"Thai Baht", "TRY":"Turkish Lira (New)", "USD":"U.S. Dollar", "ZAR":"South African Rand", "PLN":"Polish Zloty", 
        "DKK":"Danish Krone", "NZD":"New Zealand Dollar", "HUF":"Hungarian Forint", "ARS":"Argentine Peso", "ILS":"Israeli New shekel"
    };
    return pcMap;
};

const billingCurrencyMapping = () => {
    let bcMap = {
        "AUD":"Australian Dollar", "CAD":"Canadian Dollar", "EUR":"Euro", "GBP":"British Pound", "HKD":"Hong Kong Dollar", 
        "IDR":"Indonesian Rupiah", "INR":"Indian Rupee", "JPY":"Japanese Yen", "MOP":"Macau Pataca", "MYR":"Malaysian Ringgit", 
        "PHP":"Philippine Peso", "SGD":"Singapore Dollar", "THB":"Thai Baht", "TRY":"Turkish Lira (New)", "USD":"U.S. Dollar", 
        "CHF":"Swiss Franc", "BRL":"Brazilian Real", "CNY":"Chinese Yuan", "ZAR":"South African Rand", "HUF":"Hungarian Forint", 
        "AED":"UAE Dirham", "COP":"Colombian Peso", "CZK":"Czech Koruna", "KES":"Kenyan Shilling", "MAD":"Moroccan Dirham", 
        "PEN":"Peruvian Nuevo Sol", "PLN":"Polish Zloty", "DKK":"Danish Krone", "ILS":"Israeli New Shekel", "GTQ":"Guatemala Quetzal", 
        "MXN":"Mexican Peso", "NZD":"New Zealand Dollar", "ARS":"Argentine Peso"
    };
    return bcMap;
};*/

export { navigateToComponentDirectAura };
export { navigateToComponent };
export { dispatchShowToastEvent };
export { handleSuccessCallBack };
export { handleFailedCallBack };
export { navigateToObjectHome };
export { navigateToTab };
export {
    registerListener,
    unregisterListener,
    unregisterAllListeners,
    fireEvent
};
export { pinnacleheadersLabel };
export { pinnacleSectionSequence };
export { pinnacleLookupFieldsMapping };
//export { projectCurrencyMapping };
//export { billingCurrencyMapping };
export { formatDateTime };
export { formatNumber };
export { pinnacleFieldLabel };
export { handleSpinner };