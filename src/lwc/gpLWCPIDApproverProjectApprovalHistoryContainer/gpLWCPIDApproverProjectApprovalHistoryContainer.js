import { LightningElement, track, api } from 'lwc';
import { handleFailedCallBack } from 'c/gpLWCBaseUtility';
import fetchApprovalHistoryData from '@salesforce/apex/GPLWCServicePIDApproverProjectPage.fetchApprovalHistoryData';
import { formatDateTime } from 'c/gpLWCBaseUtility';
//import customSRAssets from '@salesforce/resourceUrl/GPM_Assets';
//import { loadScript, loadStyle } from 'lightning/platformResourceLoader';

export default class GpLWCPIDApproverProjectApprovalHistoryContainer extends LightningElement {
    @api recordId;
    @track approvals;
    @track listOfSteps;

    connectedCallback() {
        this.getProjectApprovalHistoryList();
    }

    renderedCallback() {

        /*Promise.all([
            loadScript(this, customSRAssets + '/assets/jquery-3.5.1.min.js'),
            loadScript(this, customSRAssets + '/assets/jquery.min.js'),
            loadScript(this, customSRAssets + '/assets/bootstrap.min.js'),
            loadScript(this, customSRAssets + '/assets/bootstrap2.min.js'),
            loadScript(this, customSRAssets + '/assets/jquery.dataTables.min.js'),
            loadScript(this, customSRAssets + '/assets/dataTables.bootstrap.min.js'),
            loadScript(this, customSRAssets + '/assets/dataTables.responsive.min.js'),
            loadScript(this, customSRAssets + '/assets/responsive.bootstrap.min.js'),
            loadStyle(this, customSRAssets + '/assets/bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/bootstrap2.min.css'),
            loadStyle(this, customSRAssets + '/assets/dataTables.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/responsive.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/font-awesome.min.css'),
            loadStyle(this, customSRAssets + '/assets/jquery.dataTables.min.css'),
        ])
        .then(() => {
            // eslint-disable-next-line no-alert
            alert('File uploaded');
            // eslint-disable-next-line no-undef
            jQuery(".searchTable2").dataTable({"searching": false, "paging": true, "info": true, "ordering": true, "lengthChange": false});
        })
        .catch(error => {
            if(error) {
                // eslint-disable-next-line no-alert
                alert(error);
            }            
        });*/

        const updatedListEvent =  new CustomEvent('gpAPRendered', { detail : 'Approval Page got rendered' });
        this.dispatchEvent(updatedListEvent);
    }

    getProjectApprovalHistoryList() {
        fetchApprovalHistoryData({ recordId : this.recordId})
        .then(result => {
            var responseData = result;            
            if(responseData.isSuccess) {
                if(result.isSuccess){
                    this.fetchApprovalHistoryDataHandler(responseData);
                }
            } else {
                handleFailedCallBack(this, responseData.message);
            }
       })
       .catch(error => { handleFailedCallBack(this, error.message); });
    }

    fetchApprovalHistoryDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        //this.approvals = JSON.stringify(response.approvals);
        let approvalHistory = [];
        let approvedList = [];
        let pendingList = [];
        let startedList = [];
        let recalledAndOtherList = [];
        this.approvals = response.approvals;

        (this.approvals).forEach(app => {
            let appObj = {};
            appObj.key = app.listOfSteps[0].ProcessNodeId;
            appObj.actionStep = (app.listOfSteps[0].StepStatus !== 'Started' && app.listOfSteps[0].ProcessNode.Name != null) ? 
            ((app.listOfSteps[0].StepStatus !== 'Removed') ? 'Step : ' + app.listOfSteps[0].ProcessNode.Name 
            + ((app.listOfSteps[0].StepStatus === 'Pending') ? ' (Pending for first approval)' : '' )
             : 'Approval Request Recalled') : 'Approval Request Submitted';

             appObj.overAllStatus = (app.listOfSteps[0].StepStatus !== 'Started' && app.listOfSteps[0].StepStatus !== 'NoResponse') 
             ? (app.listOfSteps[0].StepStatus !== 'Removed' ? app.listOfSteps[0].StepStatus : 'Recalled') : '';

            let listOfSteps = app.listOfSteps;

            (listOfSteps).forEach(ss => {
                appObj.Id = ss.Id;
                appObj.date = formatDateTime(ss.CreatedDate, true);
                appObj.status = ss.StepStatus !== 'NoResponse' ? ss.StepStatus : '';
                appObj.assignedTo = ss.OriginalActor.Name;
                appObj.comments = ss.Comments;
                appObj.actualApprover = ss.Actor.Name;

                // Mobile UI set
                appObj.dateTr = ss.Id + '_date_Tr';
                appObj.statusTr = ss.Id + '_status_Tr';
                appObj.commentsTr = ss.Id + '_comments_Tr';
                appObj.actualApproverTr = ss.Id + '_actual_Approver_Tr';
                appObj.overAllStatusTr = ss.Id + '_over_All_Status_Tr';
                appObj.utilityRight = ss.Id + '_utility_right';
                appObj.utilityLeft = ss.Id + '_utility_left';

                if(ss.StepStatus === 'Approved') {
                    approvedList.push(appObj);
                } else if(ss.StepStatus === 'Pending') {
                    pendingList.push(appObj);
                } else if(ss.StepStatus === 'Started') {
                    startedList.push(appObj);
                } else {
                    recalledAndOtherList.push(appObj);
                }
                //approvalHistory.push(appObj);
            });
        });
        // Add all pending list
        approvalHistory = pendingList;
        // Add all pending list
        (approvedList).forEach(al => {
            approvalHistory.push(al);
        });
        // Add all started list
        (startedList).forEach(sl => {
            approvalHistory.push(sl);
        });
        // add rest of list
        (recalledAndOtherList).forEach(raol => {
            approvalHistory.push(raol);
        });
        this.approvals = approvalHistory;
    }

    handleTRShowHide(event) {
        var recordId = event.currentTarget.getAttribute("data-attribute-index");

        let lightningIconRight = this.template.querySelector('[data-div-id="'+ recordId + '_utility_right"]');
        let lightningIconLeft = this.template.querySelector('[data-div-id="'+ recordId + '_utility_left"]');
        let selectDiv1 = this.template.querySelector('[data-div-id="'+ recordId + '_date_Tr"]');
        let selectDiv2 = this.template.querySelector('[data-div-id="'+ recordId + '_status_Tr"]');
        let selectDiv3 = this.template.querySelector('[data-div-id="'+ recordId + '_comments_Tr"]');
        let selectDiv4 = this.template.querySelector('[data-div-id="'+ recordId + '_actual_Approver_Tr"]');
        let selectDiv5 = this.template.querySelector('[data-div-id="'+ recordId + '_over_All_Status_Tr"]');

        if(lightningIconRight && lightningIconLeft && lightningIconRight.classList.contains('hide')) {
            lightningIconRight.classList.remove('hide');
            lightningIconLeft.classList.add('hide');
        } else if(lightningIconRight && lightningIconLeft && lightningIconLeft.classList.contains('hide')) {
            lightningIconRight.classList.add('hide');
            lightningIconLeft.classList.remove('hide');
        }

        if(selectDiv1 && selectDiv1.classList.contains('hide')) {
            selectDiv1.classList.remove('hide');
        } else if(selectDiv1) {
            selectDiv1.classList.add('hide');
        }

        if(selectDiv2 && selectDiv2.classList.contains('hide')) {
            selectDiv2.classList.remove('hide');
        } else if(selectDiv2) {
            selectDiv2.classList.add('hide');
        }

        if(selectDiv3 && selectDiv3.classList.contains('hide')) {
            selectDiv3.classList.remove('hide');
        } else if(selectDiv3) {
            selectDiv3.classList.add('hide');
        }

        if(selectDiv4 && selectDiv4.classList.contains('hide')) {
            selectDiv4.classList.remove('hide');
        } else if(selectDiv4) {
            selectDiv4.classList.add('hide');
        }

        if(selectDiv5 && selectDiv5.classList.contains('hide')) {
            selectDiv5.classList.remove('hide');
        } else if(selectDiv5) {
            selectDiv5.classList.add('hide');
        }
    }
}