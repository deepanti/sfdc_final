/* eslint-disable @lwc/lwc/no-document-query */
import { LightningElement, track, api } from 'lwc';
import recordCount from '@salesforce/label/c.GP_LWC_Record_Count';
import recordCountList from '@salesforce/label/c.GP_LWC_Record_Count_List';

export default class GpLWCPIDApproverHomePageListViewPagination extends LightningElement {
    // STUFF START : Pagination stuff
    @track isPaginationEnabled;
    @track _filterValue;
    @track isNoUpdateCountList = false;    
    @track listOfCountPages = [];
    @track listOfCountsToShow = [];
    @track isShowPrevious;
    @track isShowNext;
    @api currentPageNo;
    @track totalPageCount;
    @track startCount = 0;
    @track endCount = 0;
    // Get from parent LWC only as params
    @track _recordCount = recordCount;
    @track _recordCountList = recordCountList;
    @api listOfPaginatedRecords = [];
    //@api listOfOriginalRecords = [];
    // Not to use
    @track _listOfOriginalRecords;
    // STUFF END :Pagination stuff
    @api 
    get currentFilterName() {
        return this._filterValue;
    } 
    set currentFilterName(value) {
        // Current filter should be different from previous one. 
        // NOT IN USE
        if(value && value !== this._filterValue) {
            this._filterValue = value;
            this.getPaginatedPIDList();
        }        
    }
    // NOT IN USE
    @track _fromHomePageListView = false;
    @api 
    get fromHomePageListView() {
        return this._fromHomePageListView;
    }
    set fromHomePageListView(value) {
        // NOT IN USE
        if(value === 'true') {
            this._fromHomePageListView = true;
        } else if(value === 'false') {
            this._fromHomePageListView = false;
        } else {
            this._fromHomePageListView = false;
        }
    }
    @api 
    get listOfOriginalRecords() {
        return this._listOfOriginalRecords;
    }
    set listOfOriginalRecords(value) {
        // This will end up in infinite loop and will get Max stack size value hit/exception
        // Since you keep setting the values and set-get goes till infinite.
        //this.listOfOriginalRecords = value;
        // Answer to infinte loop is, set another var instead.
        this._listOfOriginalRecords = value;
        this.getPaginatedPIDList(value);
    }
    // Custom Record count to show
    @api customrecordcount;

    connectedCallback() {
        //this.getPaginatedPIDList();
    }

    // Have to see to handle change in list here so that Pagination can be done.
    getPaginatedPIDList() {
        // Get record count
        let recCount = this.customrecordcount ? this.customrecordcount : this._recordCount;

        if(this.listOfOriginalRecords) {
            this.listOfPaginatedRecords = this.listOfOriginalRecords.slice(0, Number(recCount));

            if(this.listOfOriginalRecords.length > Number(recCount)) {
                this.isPaginationEnabled = true;
            } else {
                this.isPaginationEnabled = false;
            }
            
            if(this.isPaginationEnabled) {
                this.handleCounts(this.listOfOriginalRecords);
            } else {
                // null all the pagination variables
            }
            // throw the list via Custom Event
            this.throwTheUpdatedListofRecords();
            this.updateCSSOfSelectedPage();
        }
    }
    // This is no list in pagination section {Pre '1' '2' '3' Nxt}
    handleCounts(value) {
        // Get record count
        let recCount = this.customrecordcount ? this.customrecordcount : this._recordCount;
        
        var tempPageCount = value.length/Number(recCount);
        tempPageCount = Math.floor(tempPageCount);

        if(tempPageCount*Number(recCount) === value.length) {
            this.totalPageCount = tempPageCount;
        } else {
            this.totalPageCount = tempPageCount + 1;
        }

        this.currentPageNo = 1;
        this.startCount = 0;
        this.endCount = Number(this._recordCountList);
        this.isShowPrevious = false;
        // Refresh the list of count pages.
        this.listOfCountPages = [];

        for (let index = this.currentPageNo; index <= this.totalPageCount; index++) {
            this.listOfCountPages.push(index);            
        }

        this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
        // Only two possible, 1- =to and 2- <than
        if(this.listOfCountsToShow.length < this.totalPageCount) {
            this.isShowNext = true;
        } else {
            this.isShowNext = false;
        }
        // stop recursion call
        //this.isNoUpdateCountList = true;
    }

    handlePrevious() {
        this.currentPageNo = this.currentPageNo - 1;
        if(this.currentPageNo === 1) {
            this.isShowPrevious = false;
        }
        // 2, 3, 4 and totalPageCount = 4 then no Next but. Next but only when totalPageCount > 4        
        this.updateListOfCountPages('previous');
        if(this.endCount >= this.totalPageCount) {
            this.isShowNext = false;
        } else {
            this.isShowNext = true;
        }
        if(this.startCount === 0) {
            this.isShowPrevious = false;
        } else {
            this.isShowPrevious = true;
        }        
        this.updateListOfPendingPIDs();
    }

    handleNext() {
        this.currentPageNo = Number(this.currentPageNo) + 1;

        if(this.currentPageNo >= this.totalPageCount) {
            this.currentPageNo = this.totalPageCount;
        }
        
        this.updateListOfCountPages('next');
        if(this.endCount >= this.totalPageCount) {
            this.isShowNext = false;
        } else {
            this.isShowNext = true;
        }
        if(this.startCount === 0) {
            this.isShowPrevious = false;
        } else {
            this.isShowPrevious = true;
        }
        this.updateListOfPendingPIDs();
    }

    handleCurrentPage(event) {
        // Get record count
        let recCount = this.customrecordcount ? this.customrecordcount : this._recordCount;

        var index = event.currentTarget.getAttribute("data-attribute-index");        
        var tempPageCount = (index-1)*Number(recCount);
        this.currentPageNo = index;
        this.listOfPaginatedRecords = this.listOfOriginalRecords.slice(tempPageCount, tempPageCount + Number(recCount));
        this.updateCSSOfSelectedPage();
        // throw the list via Custom Event
        this.throwTheUpdatedListofRecords();
    }

    updateListOfPendingPIDs() {
        // Get record count
        let recCount = this.customrecordcount ? this.customrecordcount : this._recordCount;
        
        var tempPageCount = (Number(this.currentPageNo)-1)*Number(recCount);
        this.listOfPaginatedRecords = this.listOfOriginalRecords.slice(tempPageCount, tempPageCount + Number(recCount));
        // throw the list via Custom Event
        this.throwTheUpdatedListofRecords();
        this.updateCSSOfSelectedPage();
    }

    throwTheUpdatedListofRecords() {
        const updatedListEvent =  new CustomEvent('gpupdatedlistcustomevent', {detail : this.listOfPaginatedRecords});
        this.dispatchEvent(updatedListEvent);
    }

    updateListOfCountPages(type) {
        if(type === 'next') {
            if(Number(this.endCount) + 1 <= this.totalPageCount) {
                this.endCount = this.endCount + 1;
                this.startCount = this.startCount + 1;
                this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
            }
        } else {
            if(Number(this.startCount) - 1 !== -1) {
                this.startCount = this.startCount - 1;
                this.endCount = this.endCount - 1;
                this.listOfCountsToShow = this.listOfCountPages.slice(this.startCount, this.endCount);
            }
        }
    }
    // Update the css of selected count
    updateCSSOfSelectedPage() {
        (this.listOfCountsToShow).forEach(ct => {
            let divId = '[data-div-id="' + ct + '"]';
            let selectDiv = this.template.querySelector(divId);
            if(selectDiv) {
                //selectDiv.classList.remove('slds-box');
                selectDiv.classList.remove('selectedBorder');
            }
        });

        let divId = '[data-div-id=\'' + this.currentPageNo + '\']';
        let selectDiv = this.template.querySelector(divId);

        if(selectDiv) {
            selectDiv.classList.add('selectedBorder');
        }
    }
}