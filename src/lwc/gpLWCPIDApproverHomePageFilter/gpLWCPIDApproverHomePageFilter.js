import { LightningElement, track, api } from 'lwc';

export default class GpLWCPIDApproverHomePageFilter extends LightningElement {    
    @track currentFilter;
    @api listOfAllPendingPIDs;
    @api listOfFilterPIDs = [];
    @track currentFilterName;
    @track allPIDCount = 0;
    @track newPIDCount = 0;
    @track updatedPIDCount = 0;
    @track listOfOriginalList = [];
    @track newPIDList = [];
    @track updatedPIDList = [];

    connectedCallback() {
        this.getPIDCountPerFilter();
    }

    getPIDCountPerFilter () {        
        
        if(this.listOfAllPendingPIDs.length > 0) {
            this.allPIDCount = this.listOfAllPendingPIDs.length;
            (this.listOfAllPendingPIDs).forEach(pid => {
                let pidObj = {...pid};
                pidObj.oraclePIDTr = pidObj.Id + '_Oracle_PID_tr';
                pidObj.workingUserTr = pidObj.Id + '_Working_User_tr';
                pidObj.dateSubmittedTr = pidObj.Id + '_Date_Submitted_tr';
                pidObj.utilityRight = pidObj.Id + '_utility_right';
                pidObj.utilityLeft = pidObj.Id + '_utility_left';
                
                this.listOfOriginalList.push(pidObj);
                if(pidObj.GP_Oracle_PID__c !== 'NA') {
                    this.updatedPIDList.push(pidObj);
                } else {
                    this.newPIDList.push(pidObj);
                }
            });

            this.newPIDCount = this.newPIDList.length;
            this.updatedPIDCount = this.updatedPIDList.length;
            this.currentFilter = 'all';
            
            this.updateFilteredPIDList();
        }
    }

    updateFilteredPIDList() {
        
        if(this.currentFilter === 'all') {
            this.currentFilterName = 'All Projects';
            //this.listOfFilterPIDs = this.listOfAllPendingPIDs;
            this.listOfFilterPIDs = this.listOfOriginalList;
            this.updateFilterCSS('all');
        } else if(this.currentFilter === 'new') {
            this.currentFilterName = 'Created Projects';
            this.listOfFilterPIDs = this.newPIDList;
            this.updateFilterCSS('new');
        } else {
            this.currentFilterName = 'Updated Projects';
            this.listOfFilterPIDs = this.updatedPIDList;
            this.updateFilterCSS('updated');
        }
    }

    clickAllPIDCountSection() {
        this.currentFilter = 'all';
        this.updateFilteredPIDList();
    }

    clickNewPIDCountSection() {
        this.currentFilter = 'new';
        this.updateFilteredPIDList();
    }

    clickUpdatedPIDCountSection() {
        this.currentFilter = 'updated';
        this.updateFilteredPIDList();
    }

    updateFilterCSS(filterDiv) {

        let selectDiv1 = this.template.querySelector('[data-div-id="All_Project_Div"]');
        let selectDiv2 = this.template.querySelector('[data-div-id="New_Created_Projects_Div"]');
        let selectDiv3 = this.template.querySelector('[data-div-id="Updated_Projects_Div"]');

        if(filterDiv === 'all') {
            if(selectDiv1) {
                selectDiv1.classList.add('active');
            }
            if(selectDiv2) {
                selectDiv2.classList.remove('active');
            }
            if(selectDiv3) {
                selectDiv3.classList.remove('active');
            }
        } else if(filterDiv === 'new') {
            if(selectDiv1) {
                selectDiv1.classList.remove('active');
            }
            if(selectDiv2) {
                selectDiv2.classList.add('active');
            }
            if(selectDiv3) {
                selectDiv3.classList.remove('active');
            }
        } else {
            if(selectDiv1) {
                selectDiv1.classList.remove('active');
            }
            if(selectDiv2) {
                selectDiv2.classList.remove('active');
            }
            if(selectDiv3) {
                selectDiv3.classList.add('active');
            }
        }
    }
}