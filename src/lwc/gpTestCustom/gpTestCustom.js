//import { LightningElement, track, api } from 'lwc';
import { LightningElement, track, api } from 'lwc';
import customSRImages from '@salesforce/resourceUrl/GPM_Approver_App_Images';
import customSRAssets from '@salesforce/resourceUrl/GPM_Approver_App_Assets';

//import customSRAssets1 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_1';
/*import customSRAssets2 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_2';
import customSRAssets3 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_3';
import customSRAssets4 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_4';
import customSRAssets5 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_5';
import customSRAssets6 from '@salesforce/resourceUrl/GPM_Approver_App_Assets_6';*/
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';

/*import fetchPIDData from '@salesforce/apex/GPLWCServiceTestCustom.fetchPIDData';
import headerLabel from '@salesforce/label/c.GP_LWC_Header_Label';
import headerLabelSequence from '@salesforce/label/c.GP_LWC_Header_Label_Sequence';
import { pinnacleheadersLabel } from 'c/gpLWCBaseUtility';
import fetchFieldSetPerObject from '@salesforce/apex/GPLWCServiceCustomComponent.fetchFieldSetPerObject';*/

export default class GpTestCustom extends LightningElement {
    @api footerImageUrl = customSRImages + '/images/GP_footer.png';
    @api backImageUrl = customSRImages + '/images/arrow-left.png';    
    @api recordId;
    @track windowSize;
    @track orientation;
    @track availableWidth;
    @track devicePixelRatio;
    
    connectedCallback() {
        /*this.windowSize = window.screen.width * window.devicePixelRatio;
        this.orientation = window.screen.orientation;
        this.availableWidth = window.screen.availWidth;
        this.devicePixelRatio  = window.devicePixelRatio;*/

        Promise.all([
            // NP
            loadScript(this, customSRAssets + '/assets/jquery.min.javascript.js'),
            // P
            //loadScript(this, customSRAssets1),
            // P
            //loadScript(this, customSRAssets + '/assets/bootstrap2.min.javascript.js'),
            //loadScript(this, customSRAssets2),
            // NP
            loadScript(this, customSRAssets + '/assets/jquery.dataTables.min.js'),
            // P
            //loadScript(this, customSRAssets + '/assets/dataTables.bootstrap.min.javascript.js'),
            //loadScript(this, customSRAssets3),
            // P
            //loadScript(this, customSRAssets + '/assets/dataTables.responsive.min.javascript.js'),
            //loadScript(this, customSRAssets4),
            // P
            //loadScript(this, customSRAssets + '/assets/responsive.bootstrap.min.javascript.js'),
            //loadScript(this, customSRAssets5),
            // P
            //loadScript(this, customSRAssets + '/assets/customJS.javascript.js'),
            //loadScript(this, customSRAssets6),
            loadStyle(this, customSRAssets + '/assets/bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/bootstrap2.min.css'),
            loadStyle(this, customSRAssets + '/assets/dataTables.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/responsive.bootstrap.min.css'),
            loadStyle(this, customSRAssets + '/assets/font-awesome.min.css'),
            loadStyle(this, customSRAssets + '/assets/jquery.dataTables.min.css'),
            loadStyle(this, customSRAssets + '/assets/ds_custom.css'),
        ])
        .then(() => {
            // eslint-disable-next-line no-alert
            //alert('File uploaded');
        })
        .catch(error => {            
            if(error) {
                // eslint-disable-next-line no-alert
                alert(error);
            }            
        });
    }

    /*@track error;

    @track selectedValue = undefined;
    @api selectedValueCustom = 'All Projects';
    @track picklistValueSet = undefined;
    @api picklistValueSetCustom = ['--None--', 'All Projects', 'New Created Projects', 'Updated Projects'];
    @track _selectedValue;
    @track _selectedValueCustom;
    // Lookup testing
    @track selectedRecordIdLookupWOD;
    @track selectedValueLookupWOD;
    @track selectedRecordIdLookupWD;
    @track selectedValueLookupWD;
    // Form UI
    @track metadataOfPID;
    @track pidObj;
    @track relatedsections;
    @track projectconfig = {};
    @track jsonstr1;
    @track jsonstr2;
    @track relatedObjectToShow = '';
    @track _headerLabel = headerLabel;
    @track _headerLabelJson = {};
    @track _headerLabelSequence = headerLabelSequence;
    @track _headerLabelSequenceJson = {};
    @track businessProductSectionFields = [];

    handleOnSelectStandard(event) {
        const selectedValue = event.detail;
        this._selectedValue = selectedValue;
    }

    handleOnSelectCustom(event) {
        const selectedValue = event.detail;
        this._selectedValueCustom = selectedValue;
    }

    handleOnSelectLookupWOD(event) {
        const selectedValue = event.detail.selectedRecord;
        if(selectedValue) {
            this.selectedValueLookupWOD = event.detail.selectedRecord.Name;
        } else {
            this.selectedValueLookupWOD = '';
        }
        this.selectedRecordIdLookupWOD = event.detail.recordId;
    }

    handleOnSelectLookupWD(event) {
        const selectedValue = event.detail.selectedRecord;
        if(selectedValue) {
            this.selectedValueLookupWD = event.detail.selectedRecord.Name;
        } else {
            this.selectedValueLookupWD = '';
        }
        this.selectedRecordIdLookupWD = event.detail.recordId;
    }

    connectedCallback () {
        //this.parseLabels();
        //this.fetchPIDDetails();
    }

    parseLabels() {
        this._headerLabelJson = pinnacleheadersLabel();
        this._headerLabelSequenceJson = JSON.parse(this._headerLabelSequence);
    }

    fetchPIDDetails() {
        fetchPIDData()
        .then(result => {
            var responseData = result;
            if(responseData.isSuccess) {
                this.fetchPIDDataHandler(responseData);
            }
        })
        .catch(error => {
            this.error = error;
        });
    }

    fetchPIDDataHandler(responseData) {
        var response = JSON.parse(responseData.response) || {};
        
        this.pidObj = response.projectdata;
        this.metadataOfPID = response.mapOfMetadata;
        this.relatedsections = response.relatedsections;

        this.parseMetaData();
        //this.sequencingProjectMetaData();
    }

    parseMetaData() {
        const metadata = this.metadataOfPID;        
        let metadataobj = {...metadata};
        
        for(let key in metadataobj) {
            if(key === 'Project') {
                let pidObj = {};
                pidObj.key = key;
                pidObj.sections = {};
                for(let sectionkey in metadataobj[key]) {
                    if(sectionkey !== '') {
                        let pidData = {};
                        for(let fieldKey in metadataobj[key][sectionkey]) {
                            if(fieldKey !== '') {
                                pidData[metadataobj[key][sectionkey][fieldKey]] = fieldKey;
                            }
                        }
                        pidObj.sections[sectionkey] = pidData;
                    }
                }
                this.projectconfig = pidObj;
            }
        }
        // default section to show on record load.
        //this.relatedObjectToShow = this._headerLabelJson.Project;
        this.jsonstr1 = JSON.stringify(this.pidObj);
        this.jsonstr2 = JSON.stringify(this.projectconfig);
        //this.jsonstr2 = JSON.stringify(this.projectconfig.Project.Project_Info);
        
        this.callBaseUtilityForFields('Bussiness_Hierarchy');
    }

    callBaseUtilityForFields(sectionname) {        
        if(sectionname === 'Bussiness_Hierarchy') {
            fetchFieldSetPerObject({ fieldSetName : 'GP_LWC_Business_Product_Hierarchy_Fields', objectName : 'GP_Project__c' })
            .then(result => {
                var responseData = result;                
                if(responseData.isSuccess) {
                    this.businessProductSectionFields = JSON.parse(responseData.response) || {};
                    //this.businessProductSectionFields = [...this.businessProductSectionFields];
                    this.businessProductSectionFields = JSON.stringify(this.businessProductSectionFields);
                    // Now start constructing forms
                }
            });
        }        
    }*/
}