import { LightningElement, api } from 'lwc';
import customSRImages from '@salesforce/resourceUrl/GPM_Approver_App_Images';

export default class GpLWCPIDApproverFooterPage extends LightningElement {
    @api customImageUrl = customSRImages + '/images/GP_footer.png';
}