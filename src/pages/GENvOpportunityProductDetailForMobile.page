<apex:page standardController="OpportunityProduct__c" extensions="GENcOpportunityProductDetailForMobile" id="thePage" tabStyle="Opportunity">
    <style>
        #the_box {
        border:1px solid #CFE5EC;
        margin-top:5px;
        width: 90px;
        }
        .box_header {
        padding: 5px 5px;
        font-weight: bold;
        color: #fff;
        background-color: #1797C0;
        float:left;
        width:80px;
        }
        .box_text {
        text-align: left;
        padding:32px 5px 5px 5px;
        
        } 
    </style>
    
    <apex:sectionHeader title="Product & Revenue Edit" />
    
    <apex:form >        
        <apex:pageblock title="Product & Revenue" mode="mainDetail">
            <apex:pageBlockButtons location="top">
                <apex:outputpanel id="buttons" >                
                    <apex:commandButton value="Edit" action="{!edit}" />                
                    <apex:commandButton value="Cancel" action="{!doCancel}" />                    
                </apex:outputpanel>
            </apex:pageBlockButtons>
            
            <apex:pageBlockSection title="Opportunity Header Information" collapsible="true" columns="1"> 
                <apex:pageBlockSectionItem helptext="Denotes the name that the user would assign to the opportunity to allow for easy recollection in case of any need">
                    <apex:outputText value="Opportunity Name"/>
                    <apex:outputField value="{!opportunityProduct.Opportunityid__c}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputText value="Opportunity ID"/>
                    <apex:outputField value="{!opportunityProduct.OpportunityId__r.Opportunity_ID__c}"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helptext="Denotes the Account under which the Opportunity is being created">
                    <apex:outputText value="Account Name (L4)"/>
                    <apex:outputField value="{!opportunityProduct.OpportunityId__r.AccountId}"/>
                </apex:pageBlockSectionItem>
                <apex:outputField value="{!opportunityProduct.OpportunityId__r.Ownerid}" /> 
                <apex:outputField value="{!opportunityProduct.OpportunityId__r.Industry_Vertical__c  }"/>                      
            </apex:pageBlockSection>
            
            <apex:pageblocksection title="Product Header Information" collapsible="true" columns="1">  
                <apex:outputField value="{!opportunityProduct.Product_Autonumber__c}" />
                <apex:outputField value="{!opportunityProduct.Product_family_Lookup__c}"/>
                <apex:outputField value="{!opportunityProduct.Service_Line_lookup__c}"/>
                <apex:outputField value="{!opportunityProduct.Nature_of_Work_lookup__c}"/> 
                <apex:outputField value="{!opportunityProduct.Product__c}" />
                <apex:outputField value="{!opportunityProduct.COE__c}" /> 
                <apex:outputField value="{!opportunityProduct.P_L_SUB_BUSINESS__c}" /> 
                <apex:outputField value="{!opportunityProduct.Digital_Offerings__c}" /> 
                <apex:outputField value="{!opportunityProduct.Analytics__c}" />
                <apex:outputField value="{!opportunityProduct.SalesExecutive__c}" />           
                
            </apex:pageblocksection>
            
            
            <apex:pageblocksection title="Product Financial Information (Read -Only)" id="productionRevenue" columns="1" collapsible="TRUE">                
                <apex:pageBlockSectionItem helptext="Denotes the Annual Contract Value of the Product.">
                    <apex:outputText value="ACV({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.ACVLocal__c}" label="ACV({!opportunityProduct.LocalCurrency__c})"/>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helptext="Denotes the portion of revenue that we expect from the Product in the current calendar / financial year.">
                    <apex:outputText value="CYR({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.CYR_LC__c}" label="CYR({!opportunityProduct.LocalCurrency__c})" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helptext="Denotes the portion of revenue that we expect from the Product in the next (relative to the current system date & hence year) calendar / financial year">
                    <apex:outputText value="NYR({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.NYR_LC__c}" label="NYR({!opportunityProduct.LocalCurrency__c})" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helptext="Denotes the end date of the deal. It is computed field based on the revenue start date and the contract term in months">
                    <apex:outputText value="End Date"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.EndDate__c}" />
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>  
            
            <apex:pageBlockSection columns="1" title="Revenue Profile (Rolling Monthly Split)" id="productSchedule2">
                <apex:pageBlockSectionItem helptext="Denotes the Revenue Start Date of the respective Product and SL offering in the Opportunity">
                    <apex:outputText value="Revenue Start Date"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.RevenueStartDate__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helptext="Denotes the overall duration or term of the opportunity in months. The system computes the end date of the deal by adding the contract term to the revenue start date.">
                    <apex:outputText value="Contract Term(in months)"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.ContractTerminmonths__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="Denotes the total production revenue for the product within the opportunity">
                    <apex:outputText value="TCV({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.TCVLocal__c}" id="unitPrice" label="TCV({!opportunityProduct.LocalCurrency__c})"/>
                </apex:pageBlockSectionItem>   
            </apex:pageBlockSection>  
            
            <apex:outputpanel id="productSchedule">
                <apex:pageMessages ></apex:pageMessages>
                <apex:pageBlock title="Product Schedule" mode="maindetail" rendered="{!hasScheculeSectionRendered}"> 
                    <apex:pageBlockSection collapsible="true" columns="1" > 
                        <apex:repeat value="{!revenueRYWrapperList}" var="rs">
                            <apex:pageBlockSectionItem helpText="Denotes the currency that is applicable on all billing milestones (production and transition) on the product within the opportunity" >
                                <apex:outputlabel for="revenueOne">Revenue For Rolling Year {!rs.rollYear}({!opportunityProduct.LocalCurrency__c})</apex:outputlabel> 
                                <apex:outputField value="{!rs.revSchedule.RollingYearRevenueLocal__c}"  />   
                            </apex:pageBlockSectionItem>
                            <apex:pageblockSectionItem >
                                <apex:pageBlock mode="maindetail" rendered="{!rs.hasAutoPopulateDisabled}">
                                    <table cellspacing="5">
                                        <apex:repeat value="{!rs.scheduleList}" var="ps" >                                
                                            <tr>
                                                <td>     
                                                    <div id="the_box" style="float:left;">
                                                        <div class="box_header">
                                                            {!ps.formatedDate}
                                                        </div>
                                                        <div class="box_text" >
                                                            <apex:outputField value="{!opportunityProduct.LocalCurrency__c}"/>&nbsp;
                                                            <apex:outputField value="{!ps.prodSchedule.RevenueLocal__c}" />                                       
                                                        </div>
                                                        
                                                    </div>                               
                                                </td>
                                            </tr>
                                        </apex:repeat>
                                    </table>
                                </apex:pageBlock>
                            </apex:pageblockSectionItem>                    
                        </apex:repeat> 
                    </apex:pageBlockSection>            
                </apex:pageBlock>
            </apex:outputpanel>
            <apex:pageBlockSection title="Digital Assets Information" collapsible="true" columns="1">            
                <apex:pageBlockSectionItem helpText="Denotes the Digital Asset to be consumed in providing service basis the solution">
                    <apex:outputText value="Digital Assets"></apex:outputText>
                    <apex:outputField label="Digital Assets" value="{!opportunityProduct.Digital_Assets__c}" />
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="Denotes the total production revenue for the Digital Assets within the Opportunity">
                    <apex:outputText value="Digital TCV({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.Digital_TCV__c}" label="Digital TCV({!opportunityProduct.LocalCurrency__c})"/>              
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="Denotes the current year revenue for the Digital Assets within the Opportunity">
                    <apex:outputText value="Digital CYR({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.Digital_CYR__c}" label="Digital CYR({!opportunityProduct.LocalCurrency__c})" />              
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem helpText="Denotes the next year revenue for the Digital Assets within the Opportunity">
                    <apex:outputText value="Digital NYR({!opportunityProduct.LocalCurrency__c})"></apex:outputText>
                    <apex:outputField value="{!opportunityProduct.Digital_NYR__c}" label="Digital NYR({!opportunityProduct.LocalCurrency__c})" />              
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>  
            <apex:pageblocksection title="Full Time Employees (FTE) (Mandatory on '3.On Bid' Stage)" collapsible="true" columns="1">
                <apex:outputField value="{!opportunityProduct.FTE__c}" rendered="{!IF((opportunityProduct.COE__c=='CMITS'),false,true)}"/>
                <apex:outputField value="{!opportunityProduct.Quarterly_FTE_1st_month__c}" rendered="{!IF((opportunityProduct.COE__c=='CMITS'),true,false)}" />
                <apex:outputField value="{!opportunityProduct.FTE_4th_month__c}" rendered="{!IF((opportunityProduct.COE__c=='CMITS'),true,false)}" />
                <apex:outputField value="{!opportunityProduct.FTE_7th_month__c}" rendered="{!IF((opportunityProduct.COE__c=='CMITS'),true,false)}" />
                <apex:outputField value="{!opportunityProduct.FTE_10th_month__c}" rendered="{!IF((opportunityProduct.COE__c=='CMITS'),true,false)}" />  
                <apex:outputField value="{!opportunityProduct.DeliveryLocation__c}" />      
            </apex:pageBlockSection>
            
        </apex:pageBlock>
        
    </apex:form>
</apex:page>